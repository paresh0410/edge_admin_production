export const feature = {
  allowModuleRestrictition: true,
  brandName: 'ILLUME',
  allowDynamicDbLogoImage: false,
  pricingTab: false,
  showSignUpButton: false,
};
