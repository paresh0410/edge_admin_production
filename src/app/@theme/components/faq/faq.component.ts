import { Component, OnInit , ViewEncapsulation, ViewChild} from '@angular/core';

@Component({
  selector: 'ngx-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class FaqComponent implements OnInit {
  frameLoad = false;
  @ViewChild('iFrame') Iframe;
  constructor() {
   }
  ngOnInit() {
    console.log('Iframe ==>', this.Iframe);
    this.Iframe.nativeElement.style.height = 0;
    this.Iframe.nativeElement.style.width = 0;
  }
  onLoad(){
    console.log('Loaded');
    this.frameLoad = true;
    console.log('Iframe ==>', this.Iframe);
    this.Iframe.nativeElement.style.height = '90%';
    this.Iframe.nativeElement.style.width = '100%';
  }
 
}
