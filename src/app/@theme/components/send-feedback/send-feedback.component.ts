import { Component, OnInit, OnChanges, Input, EventEmitter, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { DeviceDetectorService } from 'ngx-device-detector';
import { SendFeedbackService } from './send-feedback.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'ngx-send-feedback',
  templateUrl: './send-feedback.component.html',
  styleUrls: ['./send-feedback.component.scss'],
})
export class SendFeedbackComponent implements OnInit, OnChanges {
  @Input('screencapture') screencapture: string;
  @Output() feedBackSent = new EventEmitter();
  imgSrc: string = '';
  deviceInfo = null;
  message = '';
  showImage: boolean = true;
  constructor(
    private deviceService: DeviceDetectorService, private sendFeedbackService: SendFeedbackService,
    private spinner: NgxSpinnerService, private toastr: ToastrService) {
    this.preapreBrowserData();
  }
  ngOnInit() {

  }
  ngOnChanges() {
    this.imgSrc = this.screencapture;
    // console.log ('Changed Imaged ==>', this.imgSrc);
  }
  toggle(data) {
    // console.log('Toggle Event ===>', data);
    this.showImage = data.target.checked;
  }
  preapreBrowserData() {
    // console.log('hello `Home` component');
    this.deviceInfo = this.deviceService.getDeviceInfo();
    // const isMobile = this.deviceService.isMobile();
    // const isTablet = this.deviceService.isTablet();
    // const isDesktopDevice = this.deviceService.isDesktop();
    // this.deviceInfo.assign
    this.deviceInfo.isMobile = this.deviceService.isMobile();
    this.deviceInfo.isTablet = this.deviceService.isTablet();
    this.deviceInfo.isDesktopDevice = this.deviceService.isDesktop();
    console.log(this.deviceInfo);
    // console.log(isMobile);  // returns if the device is a mobile device (android / iPhone / windows-phone etc)
    // console.log(isTablet);  // returns if the device us a tablet (iPad etc)
    // console.log(isDesktopDevice); // returns if the app is running on a Desktop browser.
  }
  // demoFeedback() {
  //   this.feedBackSent.emit(false);
  // }

  presentToast(type, body) {
    if(type === 'success'){
      this.toastr.success(body, 'Success', {
        closeButton: false,
       });
    } else if(type === 'error'){
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else{
      this.toastr.warning(body, 'Warning', {
        closeButton: false
        })
    }
  }

  // message: any;
  sendFeedback(f) {
    this.spinner.show();
    let currentuserData = JSON.parse(localStorage.getItem('LoginResData'));
    // console.log('UserData Values ==>', currentuserData.data.data.id);
    // console.log('Form Values ==>', f.value.message);
    let data = {
      fbText: this.message,
      base64: this.imgSrc,
      fbPageURL: window.location.href,
      fbMetadata: JSON.stringify(this.deviceInfo),
      uId: currentuserData.data.data.id,
      tenantId: currentuserData.data.data.tenantId,
      status: 1,
      resComments: '',
      resUserName: '',
      resDate: '',
      sourceId: 830,
    };
    console.log('Send Values ==>', data);
    // this.sendFeedbackService.inserthelpfb(data).then()
    this.sendFeedbackService.inserthelpfb(data).then(res => {
      console.log(res);
      if (res['type'] === true) {
        console.log('Data Sent ==>');
        this.feedBackSent.emit(res['type']);
        //success
        this.presentToast('success', 'Feedback sent');
      }
      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      //error
      this.presentToast('error', '');
      console.log(err);
      this.feedBackSent.emit(false);
    });
  }
}
