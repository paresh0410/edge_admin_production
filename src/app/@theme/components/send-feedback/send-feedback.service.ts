import { Injectable,Inject } from '@angular/core';
import { webApi } from '../../../service/webApi';
import { AppConfig } from '../../../app.module';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root',
})
export class SendFeedbackService {
  private _urlInsertHelpFeedback = webApi.domain + webApi.url.sendHelpFeedback;
  constructor(@Inject('APP_CONFIG_TOKEN') private config: AppConfig, private _httpClient: HttpClient) {
    //this.busy = this._http.get('...').toPromise();

  }

  inserthelpfb(param) {
     return new Promise(resolve => {
        this._httpClient.post(this._urlInsertHelpFeedback, param)
        .subscribe(data => {
            resolve(data);
        },
        err => {
            resolve('err');
        });
    });
  }
}
