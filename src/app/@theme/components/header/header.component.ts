import { Component, Input, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { NbMenuService, NbSidebarService, NbMenuItem  } from '@nebular/theme';
import { UserService } from '../../../@core/data/users.service';
import { AnalyticsService } from '../../../@core/utils/analytics.service';
import { LayoutService } from '../../../@core/data/layout.service';
import { Router, NavigationStart } from '@angular/router';
import html2canvas from 'html2canvas';
import { LogoutComponent  } from '../../../pages/logout/logout.component';
import { BrandDetailsService } from '../../../service/brand-details.service';
import { feature } from '../../../../environments/feature-environment';
@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {
  featureConfig = feature;
  @Input() position = 'normal';
  // @ViewChild('list', { read: TemplateRef }) templateList: TemplateRef<any>;
  user: any;
  userData;
  showfaqs: boolean = false;
  showSendFeedback: boolean = false;
  imageSrc: string = '';
  currentBrandData: any;
  theme: any;
  constructor(private sidebarService: NbSidebarService,
              private menuService: NbMenuService,
              private userService: UserService,
              private analyticsService: AnalyticsService,
              private layoutService: LayoutService,
              private router: Router,
              public brandService: BrandDetailsService,
              ) {
  }
  // private dialogService: NbDialogService
  user_menu = [{ title: 'Log out' , link: '../logout' }];
  help_menu =  [{ title: 'Help', data: { id: 'help' } }, { title: 'Feedback', data: { id: 'feedback' } }];
  ngOnInit() {
    // this.userService.getUsers()
    //   .subscribe((users: any) => this.user = users.testUser);
    // this.user = localStorage
    // console.log("Userdata =======>",  this.user);
    this.currentBrandData = this.brandService.getCurrentBrandData();
    this.user = JSON.parse(localStorage.getItem('LoginResData'));
    this.theme = JSON.parse(localStorage.getItem('theme'));
    console.log("Userdata =======>",  this.user);
      this.userData = this.user.data.data;
      console.log("Userdata Content ====>", this.userData);
      if( this.userData.picture_url === null){
          this.userData.picture_url = 'assets/images/avatar.jpg'; // 'assets/images/profile1.png';
      };
      this.menuService.onItemClick().pipe().subscribe((item) => {
        // console.log(item);
        if (item.item.data) {
          if (item.item.data.id === 'help') {
            console.log('Help Method');
            // this.logout();
            this.showfaqs = true;
          }
          var this_ref = this;
          if (item.item.data.id ===  'feedback'){
            console.log('feedback Method');
            setTimeout(function(){
              this_ref.takeScreenCapture();
            }, 1500);
          }

        }
      });
    console.log("Userdata =======>", this.user);
    this.userData = this.user.data.data;
    console.log("Userdata Content ====>", this.userData);
    if (this.userData.picture_url === null) {
      this.userData.picture_url = 'assets/images/avatar.jpg';// "assets/images/profile1.png";
    }
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    this.layoutService.changeLayoutSize();

    return false;
  }

  toggleSettings(): boolean {
    this.sidebarService.toggle(false, 'settings-sidebar');

    return false;
  }

  goToHome() {
    this.menuService.navigateHome();
  }

  // logout() {
  //   localStorage.clear();
  //   sessionStorage.clear();
  //   window.localStorage.clear();
  //   this.router.navigate(['login']);
  // }
  startSearch() {
    this.analyticsService.trackEvent('startSearch');
  }
  closeFaq() {
    this.showfaqs = false;
  }
  closeSendFeedback(){

    this.showSendFeedback = false;
  }
  takeScreenCapture(){
    html2canvas(document.body).then(canvas => {
      // this.saveAs(canvas.toDataURL("image/png"), `canvas.png`)
      console.log(canvas);
      console.log(canvas.toDataURL());
      this.imageSrc = canvas.toDataURL();
      // this.imageSrc = canvas.toDataURL('image/jpeg', 1.0)
      // console.log(canvas.toDataURL('image/jpeg', 1.0));
      // console.log(canvas.toDataURL("image/png"));
    });
    this.showSendFeedback = true;
  }
  sendFeedback(data) {
    // this.closeSendFeedback();
    if (data) {
      this.showSendFeedback = false;
    } else {
       this.showSendFeedback = true;
    }
  }
  themebind() {
    this.theme = JSON.parse(localStorage.getItem('theme'));
    if (this.theme && !this.theme.logo) {
      return this.currentBrandData.logo;
    } else {
      return this.theme.logo;
    }
  }
  errorHandler(event) {
    console.debug(event);
    event.target.src = this.currentBrandData.logo;
 }
}
