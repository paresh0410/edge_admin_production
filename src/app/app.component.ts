/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AnalyticsService } from './@core/utils/analytics.service';
// import { ToasterModule, ToasterService, ToasterConfig } from 'angular2-toaster';
import { AppService } from './app.service';
import { LoginService } from './pages/login/login.service';
import {
  Router,
  Event,
  NavigationStart,
  NavigationEnd,
  NavigationError
} from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { feature } from '../environments/feature-environment';
@Component({
  selector: 'ngx-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AppComponent implements OnInit {
  // public config: ToasterConfig = new ToasterConfig({
  //   showCloseButton: true,
  //   tapToDismiss: true,
  //   timeout: 0,
  //   limit: 5,
  //   // positionClass: 'toast-bottom-center',
  //   positionClass: 'toast-top-center'
  // });
  featureConfig = feature;
  contdata: any;
  userroleData: any;
  features: any = [];
  meniutemp: any = [];
  menus: any = [];
  accdata: any = [];
  userData: any;
  tenantId: any;
  private themes = document.querySelector('body');

  variables: any = {
    button: '--colorBgbtn',
    commonColor: '--colorCommon',
    header: '--colorHeader',
    icon: '--colorIcons',
    pageBackground: '--colorBgPage',
    sidemenu: '--colorSidebar',
  };

  constructor(
    private analytics: AnalyticsService,
    private AppService: AppService,
    private service1: LoginService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) {
    // this.data={
    //       userId:4,
    //       roleId:2
    //     }
    if(localStorage.getItem('userDetails')){
      this.userData = JSON.parse(localStorage.getItem('userDetails'));
      console.log('userData', this.userData.data);
     //  this.userId = this.userData.data.data.id;
      this.tenantId = this.userData.tenantId;
   }
    this.router.events.subscribe((event: Event) => {

      if (event instanceof NavigationEnd) {
        // Hide loading indicator
        this.spinner.hide();
      }

      if (event instanceof NavigationError) {
        // Hide loading indicator

        // Present error to user
        console.log(event.error);
        this.spinner.hide();
      }
    });
    document['title'] = this.featureConfig.pageTitle;
  }

  ngOnInit(): void {
    this.analytics.trackPageViews();
    this.getdata();
    // this.AppService.setuserdata(this.data);
    const themes = JSON.parse(localStorage.getItem('theme'));
    if(themes) {
      for(let key in themes) {
        let value = themes[key];
        this.changeTheme(key,value);
      }
    }
  }

  /* THis function is for featiching user data we right here because when page relode we need to set data again*/
  getdata() {
    // var user = JSON.parse(localStorage.getItem('LoginResData'));
    var user = JSON.parse(localStorage.getItem('currentUser'));
    if (user) {
      var userData = {
        username: user.username,
        tId: this.tenantId
      };
      this.AppService.userData(userData)
        // this.service1.userData(userData)
        .subscribe(
          data => {
            // this.loader =false;
            // var meniutemp=[];
            this.userroleData = data.data;
            if (this.userroleData) {
              this.contdata = {
                roleId: this.userroleData[0][0].roleId,
                userId: this.userroleData[0][0].userId
              };
              if (this.userroleData[1]) {
                this.accdata = this.userroleData[1];
              }
              if (this.userroleData[2]) {
                for (let i = 0; i < this.userroleData[2].length; i++) {
                  this.meniutemp.push(
                    this.userroleData[2][i].featureIdentifier
                  );
                  this.meniutemp.push(this.userroleData[2][i].menuIdentifier);
                  // this.features.push()
                }
                // let uniq = a => [...new Set(a)];
                this.features = Array.from(new Set(this.meniutemp));
              }
              if (this.userroleData[3]) {
                for (let i = 0; i < this.userroleData[3].length; i++) {
                  // this.meniutemp.push(this.userroleData[2][i].featureIdentifier);
                  this.meniutemp.push(this.userroleData[3][i]);
                }
                this.menus = Array.from(new Set(this.meniutemp));
              }
              this.AppService.setuserdata(this.contdata);
              this.AppService.setfeatures(this.features);
              this.AppService.setmenus(this.menus);
              // this.AppService.setmenus(this.menus);
            }
          },
          error => {
            // this.loader =false;
            console.error('Reg Error !', error);
          }
        );
    }
  }

  changeTheme(colorName, colorValue) {
    // Object.keys(this.variables).forEach(function(val) {
      // console.log('themes=========>>>>>>>>>>>', themes[key]);
      for(let val in this.variables) {
      if(val === colorName) {
        if (colorName === 'header') {
          this.themes.style.setProperty(this.variables[val], colorValue);
          const headerTxt = this.getContrastYIQ(colorValue);
          this.themes.style.setProperty('--colorHeadertxt', headerTxt);
        } else if (colorName === 'sidemenu') {
          this.themes.style.setProperty(this.variables[val], colorValue);
          const sidebarTxt = this.getContrastYIQ(colorValue);
          this.themes.style.setProperty('--colorSidebartxt', sidebarTxt);
        } else if (colorName === 'button') {
          this.themes.style.setProperty(this.variables[val], colorValue);
          const btnTxt = this.getContrastYIQ(colorValue);
          this.themes.style.setProperty('--colorBtnTxt', btnTxt);
        } else {
          this.themes.style.setProperty(this.variables[val], colorValue);
        }
      }
    }
    // });
  }

  getContrastYIQ(hexcolor){
    hexcolor = hexcolor.replace("#", "");
    var r = parseInt(hexcolor.substr(0,2),16);
    var g = parseInt(hexcolor.substr(2,2),16);
    var b = parseInt(hexcolor.substr(4,2),16);
    var match = ((r*299)+(g*587)+(b*114))/1000;
    return (match >= 128) ? '#000000' : '#ffffff';
  }
}
