import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { Login } from './pages/login/login.component';
import { PrivacyComponent } from './pages/privacy/privacy/privacy.component';
import { ForgotPasswordComponent } from './pages/forgot-password/forgot-password.component';
import { LogoutComponent } from "./pages/logout/logout.component";
// import { CategoryComponentComponent } from './utils/filter-dashboard-component/filter-component-sidebar/category-component/category-component.component';
import { DomainMapComponent } from './pages/domain-map/domain-map.component';
import { PricePlanComponent } from './pages/price-plan/price-plan.component';
import { SignupComponent } from './pages/signup/signup.component';
// import { PreloadAllModules } from '@angular/router';
import { AppCustomPreloader } from './app-routing-loader';

const routes: Routes = [

  { path: 'logout', component:LogoutComponent },
  { path: 'privacy', component:PrivacyComponent },
  { path:'forgot-password',component:ForgotPasswordComponent},
  { path:'domain-map/:uniqueId', component: DomainMapComponent},
  { path:'plans', component: PricePlanComponent},
  { path:'signUp/:priceId', component: SignupComponent},
  { path: 'pages', loadChildren: 'app/pages/pages.module#PagesModule', data: { preload: true } },
  // { path: 'CategoryComponentComponent', component: CategoryComponentComponent },
  // { path: 'pages', redirectTo: 'pages', pathMatch: 'full' },
  // { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path:'login',component:Login},
  { path: '**', redirectTo: 'login' },

];

const config: ExtraOptions = {
  useHash: true,
  // preloadingStrategy: PreloadAllModules,
  preloadingStrategy: AppCustomPreloader,
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule],
  providers: [AppCustomPreloader],
})
export class AppRoutingModule {
}
