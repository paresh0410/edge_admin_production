import { Component, OnInit, Input , Output , EventEmitter} from '@angular/core';

@Component({
  selector: 'ngx-module-activity-list',
  templateUrl: './module-activity-list.component.html',
  styleUrls: ['./module-activity-list.component.scss']
})
export class ModuleActivityListComponent implements OnInit {
  @Input() courseModules: any =[];
  @Input() showBulkCopy = false;
  @Input() activitySubTypeList: any = [];
  @Output() performAction = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
    console.log('Course Module ==>', this.courseModules);
    console.log('Course activitySubTypeList ==>', this.activitySubTypeList);
  }
  // getActivityIcon(activityData){
  //   if(activityData.supertypeId == 1 || activityData.supertypeId == 2){
  //     switch (Number(activityData.formatId)) {
  //       case 1: // 'video icon';
  //              return ('fa fa-video');
  //             // break;
  //       case 2: // Audio
  //               return ('fa fa-volume-up');
  //             //  break;
  //       case 3:  // PDF
  //                return ('fas fa-file-pdf');
  //         //  break;
  //       case 5:  // Scrom
  //             return ('fas fa-box');

  //           //  break;
  //       case 7: // Image
  //              return ('fas fa-image');
  //         // break;

  //       case 10: // Power point
  //                  return ('fas fa-file-powerpoint');
  //               // break;
  //       case 11: // Excel
  //                return ('fas fa-file-excel');
  //               //  break;
  //       case 12: // Word
  //               return ('fas fa-file-word');
  //               // break;
  //       default: return ('fa fa-file');
  //     }
  //   }else  {
  //     // Quiz or Feedback icon
  //     return ('fa fa-file');
  //   }

  // }



  passDataToParent(action , ...args){
    console.log('action to be performed ==>', action , args);
    const event = {
      'action': action,
      'argument': args,
    };
    console.log('action to be performed ==>', event);
    this.performAction.emit(event);
  }
}
