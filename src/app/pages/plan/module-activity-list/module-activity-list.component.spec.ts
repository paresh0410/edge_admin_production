import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModuleActivityListComponent } from './module-activity-list.component';

describe('ModuleActivityListComponent', () => {
  let component: ModuleActivityListComponent;
  let fixture: ComponentFixture<ModuleActivityListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModuleActivityListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModuleActivityListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
