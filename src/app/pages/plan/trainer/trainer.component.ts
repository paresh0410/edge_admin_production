import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NbMenuService, NbSidebarService } from '@nebular/theme';
import { LayoutService } from '../../../@core/data/layout.service';
import { TrainerServiceService } from './trainer-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { isString } from 'util';
import { ToastrService } from 'ngx-toastr';
import { webApi } from '../../../service/webApi';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { Card } from '../../../models/card.model';
import { SuubHeader } from '../../components/models/subheader.model';
import { BrandDetailsService } from '../../../service/brand-details.service';
import { noData } from '../../../models/no-data.model';
import { Filter } from '../../../models/filter.modal';
@Component({
  selector: 'ngx-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TrainerComponent implements OnInit {
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:'Add a trainer to the organisation.',
    desc:'Trainers or Instructors are able to design their own courses, enroll learners and publish the courses.',
    titleShow:true,
    btnShow:true,
    descShow:true,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/setting-how-to-add-a-new-trainer-who-is-not-a-user',
  }
  header: SuubHeader  = {
    title:'Trainer',
    btnsSearch: true,
    searchBar: true,
    placeHolder: 'Search by name',
    dropdownlabel: ' ',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: '',
    btnName3: '',
    btnAdd: 'Add Trainer',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: true,
    filter: true,
    showBreadcrumb: true,
    breadCrumbList:[]   
  };
  loginUserdata: any = [];
  myInnerHeight: any = window.innerHeight - 120;
  trainerData: any = [];
  cardModify: Card = {
    flag: 'trainer',
    titleProp : 'trainerName',
    image: '',
    hoverlable:   true,
    showImage: true,
    option:   true,
    eyeIcon:   true,
    bottomDiv:   true,
    bottomTitle:   true,
    btnLabel: 'Edit',
    defaultImage:'assets/images/courseicon.jpg'
  };
  count:number=8
  Languages = [];
  Standards = [ ];
  Visibility = [ ];
  trainerType = [];
  displayArray: any = [];
  skeleton=false;
  search:any;
  searchText: any;
  tempReg: any;
  response: any;
  enableDisableTrainerModal: boolean=false;
  // visibledata: { trainStat: number; userId: any; trainerId: any; tId: any; };
  enableTrain: boolean=false;
  enableTrainData: any;
  currentBrandData: any;
// filter
filters: any = [];
filter: boolean = false;
filtersInner: any = [];
filtercon: Filter = {
  ascending: false,
  descending: false,
  showDropdown: false,
  dropdownList: [
    { drpName: 'Enrol Date', val: 1 },
    { drpName: 'Created Date', val: 2 },
  ]
};
  constructor(
    private spinner: NgxSpinnerService,
    // private toasterService: ToasterService,
    private trainerService: TrainerServiceService,
    private sidebarService: NbSidebarService,
    private layoutService: LayoutService,
    public router: Router,
    public routes: ActivatedRoute,
    private toastr: ToastrService,
    private commonFunctionService: CommonFunctionsService,
  ) {
    if (localStorage.getItem('LoginResData')) {
      this.loginUserdata = JSON.parse(localStorage.getItem('LoginResData'));
    }
    this.dropdownPrepare();
  }

  ngOnInit() {
    // this.spinner.show();
    this.displayInstances();
    this.header.breadCrumbList=[{
      'name': 'Settings',
      'navigationPath': '/pages/plan',
      }]
    this.search = {
      trainerName:''
    };
  }

  addEditTrainer(trainerData, value) {
    console.log('Data passed >', trainerData);
    // this.trainerService.addEditTrainer = trainerData;
    console.log('Value passed >', value);
    if (value === 0) {
      const action = 'ADD';
      this.trainerService.addEditTrainer[0] = action;
      this.trainerService.addEditTrainer[1] = null;
      this.router.navigate(['add-edit-trainer'], { relativeTo: this.routes });
    } else {
      const action = 'EDIT';
      this.trainerService.addEditTrainer[0] = action;
      this.trainerService.addEditTrainer[1] = trainerData;
      this.router.navigate(['add-edit-trainer'], { relativeTo: this.routes });
    }
  }

  gotoCardEdit(trainerData) {
    const action = 'EDIT';
    this.trainerService.addEditTrainer[0] = action;
    this.trainerService.addEditTrainer[1] = trainerData;
    this.enableTrainData=trainerData
    this.router.navigate(['add-edit-trainer'], { relativeTo: this.routes });
  }

  back() {
    this.router.navigate(['../'], { relativeTo: this.routes });
  }

  nodata: any;
  noTrainerFound: boolean = false;

  displayInstances() {
    // this.spinner.show();
    const param = {
      tId: this.loginUserdata.data.data.tenantId,
    };
    const get_All_Trainers: string = webApi.domain + webApi.url.get_All_Trainers;
    this.skeleton = false;
    this.commonFunctionService.httpPostRequest(get_All_Trainers,param)
    //this.trainerService.getTrainerList(param)
      .then(resTrainerData => {
        console.log(resTrainerData);
        if (resTrainerData['type'] === true) {
          const trainerData = resTrainerData['data'];
          if (trainerData.length === 0) {
            this.noTrainerFound = true;
            // this.skeleton=false;
          } else {
            this.trainerData = trainerData;

            for(const data of this.trainerData){
              // data.languageId;
              data.visible = data.visibilityId;
              try{
                if(data.languageId){
                  data.languageId = data.languageId.split(',');
                }else {
                  data.languageId = [];
                }

              }
              catch(err){
                this.spinner.hide();
                console.log(err);
              }
            }

            this.displayArray = this.trainerData;
          this.response=this.displayArray;
          // this.cdf.detectChanges()
          this.spinner.hide();
          }
          console.log('Trainer Details >>', this.trainerData);
          console.log('this.displayArray ', this.displayArray );

        } else {
          this.spinner.hide();
          this.presentToast('error', '');
          this.noTrainerFound = true;
        }
        this.skeleton=true;
      }, error => {
        this.spinner.hide();
        this.skeleton=true;
        this.presentToast('error', '');
        this.noTrainerFound = true;
      });
  }

  visibilityDetail: any;
  errorMsg: any;

  disableTrainerCard(i, item) {
    console.log('item', item);
    const visibleData = {
      trainStat: item.visibilityId == 1 ? 0 : 1,
      userId: this.loginUserdata.data.data.id,
      trainerId: item.ID,
      tId: this.loginUserdata.data.data.tenantId,
    };
   const  update_trainer_details = webApi.domain + webApi.url.update_trainer;
    console.log('visibleData', visibleData);
    this.commonFunctionService.httpPostRequest(update_trainer_details,visibleData)
    //this.trainerService.disableTrainer(visibleData)
      .then(resTrainerData => {
        this.spinner.hide();
        this.visibilityDetail = resTrainerData;
        if (this.visibilityDetail.type === false) {
          this.presentToast('error', '');
        } else {
      this.enableDisableTrainerModal = true;
          this.presentToast('success', 'Trainer updated.');
         // this.disablevaluechnage(i, item);
         this.displayInstances();
        }
        this.skeleton=true;
      },
        resUserError => {
          this.spinner.hide();
          if (resUserError.statusText === 'Unauthorized') {
            this.router.navigate(['/login']);
          }
          this.errorMsg = resUserError;
        });
  }

  clickTodisable(i,item) {
    // this.cdf.detectChanges()
    console.log('item', item);
    const visibleData = {
      trainStat: item.visibilityId == 1 ? 0 : 1,
      userId: this.loginUserdata.data.data.id,
      trainerId: item.ID,
      tId: this.loginUserdata.data.data.tenantId,
    };
    // this.displayArray[i].visible==1?0:1
    if(this.displayArray[i].visibilityId==1){
      this.displayArray[i].visibilityId=0
      this.displayArray[i].visible=0
    }else{
      this.displayArray[i].visibilityId=1
       this.displayArray[i].visible=1
    }
    // this.cdf.detectChanges()
// this.visibledata=visibleData
   const  update_trainer_details = webApi.domain + webApi.url.update_trainer;
    console.log('visibleData', visibleData);
    this.commonFunctionService.httpPostRequest(update_trainer_details,visibleData)

      .then(resTrainerData => {
        // this.spinner.hide();
        this.visibilityDetail = resTrainerData;
        if (this.visibilityDetail.type === false) {
          this.presentToast('error', '');
        } else {
          if(visibleData.trainStat == 1){
            // this.displayArray[i].visible=0
          //this.presentToast('success', 'Trainer updated.');
          // this.closeEnableDisableTrainerModal();
          // this.enableTrain = true;
          this.presentToast('success', 'Trainer Enabled Successfully.');
          // this.cdf.detectChanges();
          }else {
            // this.displayArray[i].visible=1
        //  this.enableTrain = true;
        //  this.closeEnableDisableTrainerModal();
          this.presentToast('success', 'Trainer Disabled Successfully.');
          // this.cdf.detectChanges();
          }
        //  this.displayInstances();
        }
        this.skeleton=true;
      },
        resUserError => {
          this.spinner.hide();
          if (resUserError.statusText === 'Unauthorized') {
            this.router.navigate(['/login']);
          }
          this.errorMsg = resUserError;
          // this.closeEnableDisableTrainerModal();
        });
  }



  presentToast(type, body) {
    if(type === 'success'){
      this.toastr.success(body, 'Success', {
        closeButton: false
       });
    } else if(type === 'error'){
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else{
      this.toastr.warning(body, 'Warning', {
        closeButton: false
        })
    }
  }


  /**********************dropdown/checkbox data for filter***********************/

  // errorMsg1: any;

  // languages: any;
  // standard: any;
  employeeCodes: any;
  // visibility: any;
  // isInternal: any;

  dropdownPrepare() {
    this.spinner.show();
    const data = {
      // tId: this.loginUserdata.data.data.tenantId,
      tId: this.loginUserdata.data.data.tenantId,
    };
    console.log(data);
    const dropdown_service = webApi.domain + webApi.url.trainer_dropdowns;
    this.commonFunctionService.httpPostRequest(dropdown_service,data)
    //this.trainerService.trainer_dropDown(data)
      .then(rescompData => {
        this.spinner.hide();
        if (rescompData['type'] == true) {
          console.log('dropdown data', rescompData['data']);
          this.Languages = rescompData['data'][0];
          this.Standards = rescompData['data'][1];
       //   this.employeeCodes = rescompData['data'][2];
          this.Visibility = rescompData['data'][2];
          this.trainerType = rescompData['data'][3];

          this.bindfilter(this.Languages);
          this.bindfilter(this.Standards);
          this.bindfilter(this.Visibility);
          this.bindfilter(this.trainerType);
          console.log('data', this.Standards);
          console.log('code data', this.employeeCodes);
          console.log('Visibility data', this.Visibility);
          console.log('langList data', this.Languages);
          console.log('trainerType data', this.trainerType);
        } else {
          // this.presentToast('success', 'Trainer updated.');
        }
      },

        resUserError => {
          this.spinner.hide();
          this.errorMsg = resUserError;
          this.presentToast('success', 'Trainer updated.');
        });


  }


  // trainerData = this.trainerData;

  fieldValue = [];
  ObjectSetterValueArray = [];

  filterData = {};

  // searchTags:any;



  // subject filteration starts here
  // main array
  newArray = [];

  // array for display on html page
  // Initially without filter it will hold data of trainerData
  // nofilterAsset variable for validation
  noFilterAsset: boolean = false;


  // new array for tags
  tagArray = [];
  //
  filterTag: any = [];
  // number display to n
  SelectedCategoryLength: any = {};
  // search = {};
  SubjectFilterArray = [];
  SubjectName_CheckBoxedStatus: boolean;
  // subj: any;


  /*

  After loading the screen
  ***********************************
  when we select the checkbox SubjectName function will be called.

  After execution of statement takes places

  */

  // tslint:disable-next-line: max-line-length
  filterDataObjectArrayCreation(filterData_CategoryNameForKey: any, FiltersubjectNameForArray: any, checkboxStatus: boolean) {


    let dataValue = null;
    let flag = false;
    for (const key in this.filterData) {
      if (filterData_CategoryNameForKey === key) {
        flag = true;
        dataValue = key;
        break;
      } else {
        flag = false;
      }
    }
    if (checkboxStatus) {
      if (!flag) {
        Object.assign(this.filterData, { [filterData_CategoryNameForKey]: [] });
        (this.filterData[filterData_CategoryNameForKey]).push(FiltersubjectNameForArray[filterData_CategoryNameForKey]);
        // console.log('Filter data after insertion object with array checkbox', this.filterData);
      } else {
        (this.filterData[dataValue]).push(FiltersubjectNameForArray[filterData_CategoryNameForKey]);
        // console.log('Filter data after insertion object with array checkbox', this.filterData[dataValue]);
        // console.log('true');
      }
      // console.log('Filter data after insertion object with array checkbox', this.filterData);
      // console.log(this.filterData);
    } else {
      for (let i = 0; i < this.filterData[dataValue].length; i++) {
        if (this.filterData[dataValue][i] === FiltersubjectNameForArray[dataValue]) {
          this.filterData[dataValue].splice(i, 1);
          if (this.filterData[dataValue].length === 0) {
            delete this.filterData[dataValue];
            this.SelectedCategoryLength[dataValue] = 0;
            delete this.objectSetter[dataValue];
            break;
          }
          // console.log('Filter data after Splicing object with array checkbox', this.filterData);
          // console.log(this.filterData[dataValue]);
        }
      }
    }
  }


  objectSetter = {};

  fieldValueCreatorFunction() {
    // const keys = Object.keys(this.filterData);
    this.fieldValue = Object.keys(this.filterData);
    // console.log('FieldValue Object Data', this.fieldValue);
    for (const ObjectSetterData of this.fieldValue) {
      Object.assign(this.objectSetter, { [ObjectSetterData]: false });
    }

    for (const filterDataCate of this.fieldValue) {
      Object.assign(this.SelectedCategoryLength, { [filterDataCate]: this.filterData[filterDataCate].length });
    }
    // console.log('object length', this.SelectedCategoryLength);
  }

  tagCreatorFunction(CategoryObject: any, StatusToPushOrPop: any) {
    this.filterData = [];
    if (StatusToPushOrPop) {
      this.tagArray.push(CategoryObject);
      for (const data of this.tagArray) {
        for (const key in data) {
          if (isString(data[key])) {
            // console.log('key type',isString(data[key]));
            // console.log(key);
            this.filterTag.push(key);
          }
        }
      }
    }
    console.log('Filter TagArray', this.filterTag);
    console.log('Tag Array', this.tagArray);
  }


  subjectName(subj: any, cat: any) {
    // console.log(subj.name);
    // console.log('Category name', cat);
    // this.SubjectFilterArray.push(subj.name);

    // if (cat === "tags") {
    //   for (const tagArray of this.Tags) {
    //     if (subj === tagArray) {
    //       console.log('tag matches');
    //       tagArray.status = this.SubjectName_CheckBoxedStatus;
    //       break;
    //     }
    //   }
    // }

    this.filterDataObjectArrayCreation(cat, subj, this.SubjectName_CheckBoxedStatus);
    this.fieldValueCreatorFunction();
    this.onFilterSubjectPush(cat);
    // this.tagCreatorFunction(subj, this.SubjectName_CheckBoxedStatus);


  }

  onFilterSubjectPush(filterData_CategoryNameForKey: any) {
    this.newArray = [];
    for (const data of this.trainerData) {

      for (const ObjectSetterData of this.fieldValue) {
        Object.assign(this.objectSetter, { [ObjectSetterData]: false });
      }

      for (const value of this.fieldValue) {
        // this.tagArray = [];
        // console.log('Value', value);
        // console.log('FilterData of particular value', this.filterData[value]);
        // console.log('Length of FilterData of particular value', this.filterData[value].length);
        // tslint:disable-next-line: prefer-for-of
        // if (value === filterData_CategoryNameForKey) {
        if (value === 'languageId') {
          console.log(value);

          for (const languageData of data[value]) {
            for (let i = 0; i < this.filterData[value].length; i++) {

              // Pushing data into tag array
              // this.tagArray.push({[value]:this.filterData[value][i]});

              // if (languageData === undefined || languageData !== this.filterData[value][i]) {
              if (languageData == undefined || languageData != this.filterData[value][i]) {
                // console.log('ifTrue');
                // let checkData = data[value];
              } else {
                // console.log('ifFalse');
                this.objectSetter[value] = true;
                // let checkData = data[value];
                break;
              }
            }
          }

        }
        else {
          console.log('else part executed');

          for (let i = 0; i < this.filterData[value].length; i++) {

            // Pushing data into tag array
            // this.tagArray.push({[value]:this.filterData[value][i]});

            // if (data[value] === undefined || data[value] !== this.filterData[value][i]) {
            if (data[value] == undefined || data[value] != this.filterData[value][i]) {
              // console.log('ifTrue');
              // let checkData = data[value];
            } else {
              // console.log('ifFalse');
              this.objectSetter[value] = true;
              // let checkData = data[value];
              break;
            }
          }

        }
      }
      // }



      // let ObjectDataValue = null;
      let Objectflag = true;
      this.ObjectSetterValueArray = Object.values(this.objectSetter);

      for (const objectSetterValueArrayData of this.ObjectSetterValueArray) {
        if (Objectflag && objectSetterValueArrayData) {
          Objectflag = true;
        } else {
          Objectflag = false;
        }
      }

      if (Objectflag) {
        this.newArray.push(data);
      }
    }

    // console.log(this.tagArray);
    console.log(this.newArray);


    if ((this.fieldValue.length !== 0)) {
      if ((this.newArray.length !== 0)) {
        this.displayArray = this.newArray;
        this.noFilterAsset = false;

      } else {
        this.displayArray = [];
        // this.noFilterAsset = true;
      }
    } else {
      this.noFilterAsset = false;
      this.displayArray = this.trainerData;
    }
  }

  // subject filteration ends here
  searchTrainer(event) {
    this.noTrainerFound=false;
    let val = '';
    if (event) {
      val = event.target.value.toLowerCase(); 
    }
    // this.allregulatorylist(this.addEditCourseService.data.data);
    this.searchText=val
    this.tempReg = this.response;
    // filter our data
    if(val.length>=3||val.length==0){
    const temp = this.tempReg.filter(function (d) {
      return d.trainerName.toLowerCase().indexOf(val) !== -1 ||
        // String(d.dueDays).toLowerCase() === val ||
        // String(d.reminder).toLowerCase().indexOf(val) !== -1 ||
        !val;
    });
    if(temp.length==0){
      this.noTrainerFound=true
      this.noDataVal={
        margin:'mt-5',
        imageSrc: '../../../../../assets/images/no-data-bg.svg',
        title:"Sorry we couldn't find any matches please try again",
        desc:".",
        titleShow:true,
        btnShow:false,
        descShow:false,
        btnText:'Learn More',
        btnLink:''
      }
    }
    // update the rows
    this.displayArray = temp;
  }
    // Whenever the filter changes, always go back to the first page
    // this.tableDataReg.offset = 0;
  }

  onUpdateSubjectName(event: Event) {
    this.SubjectName_CheckBoxedStatus = (<HTMLInputElement>event.target).checked;
    // console.log('Subject Name', this.SubjectName_CheckBoxedStatus);
  }
  // closeEnableDisableTrainerModal() {
  //   this.enableDisableTrainerModal = false;
  // }
  // enableDisableTrainerAction(actionType) {
    // console.log(this.category[this.catDisableIndex]);
    // console.log(this.enableTrainData);
    // if (actionType == true) {
      // if (this.category[this.catDisableIndex].visible == 1) {
      //   this.category[this.catDisableIndex].visible = 0;
      //   var catData = this.category[this.catDisableIndex];
      //   this.enableDisableCategory(catData);
      // } else {
      //   this.category[this.catDisableIndex].visible = 1;
      //   var catData = this.category[this.catDisableIndex];
      //   this.enableDisableCategory(catData);
      // }
  //     if (this.enableTrainData.visible == 1) {
  //       this.enableTrainData.visible = 0;
  //       var TrainerData = this.enableTrainData;
  //       // this.clickTodisable(TrainerData);
  //     } else {
  //       this.enableTrainData.visible = 1;
  //       var TrainerData = this.enableTrainData;
  //       // this.clickTodisable(TrainerData);
  //     }
  //   } else {
  //     this.closeEnableDisableTrainerModal();
  //   }
  // }
  // clickTodis(TrainerData) {
    // if(categoryData.coursecount>0){
    //   this.presentToast('warning', 'Category have courses, not able to hide this category');
    // }
    // else 
  //   if (TrainerData.visible == 1) {
  //     this.enableTrain = true;
  //     this.enableTrainData = TrainerData;
  //     this.enableDisableTrainerModal = true;
  //   } else {
  //     this.enableTrain = false;
  //     this.enableTrainData = TrainerData;
  //     this.enableDisableTrainerModal = true;
  //   }
  // }
  bindfilter(obj) {
    const filtername = obj.length > 0 ? obj[0]['filterId'] : '';
    const filterValueName = obj.length > 0 ? obj[0]['filterValue'] : '';
        const item = {
          count: "",
          value: "",
          tagname: obj.length > 0 ? obj[0]['filterId'] : '',
          isChecked: false,
          list: obj,
          filterValue: filterValueName
        }
        if (filtername) {
          this.filters.push(item);
        }
    // if (result['data'] && result['data'].length > 0) {
    //   result['data'].forEach( (value, index) => {
        
    //   })
    // }
  }
  filteredChanged1(event) {
    console.log('Filtered Event - ', event);
    const obj = event;
    this.filterData = {};
    this.SelectedCategoryLength = {};
    this.objectSetter = {};
    let allempty = true;
    if (!obj.empty) {
      for(const key in obj){
        const list = obj[key];
        list.forEach( (value, index) => {
          allempty = false;
          console.log(key, ' - ', value);
          this.SubjectName_CheckBoxedStatus = true;
          this.subjectName(value, key);
        });
      }
      if (allempty) {
        this.displayArray = this.trainerData;
      }
    } else {
      this.displayArray = this.trainerData;
    }
  }
  gotoFilter() {
    this.filter = !this.filter;
  }
}
