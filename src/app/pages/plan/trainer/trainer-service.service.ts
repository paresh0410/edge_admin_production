import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { webAPIService } from '../../../service/webAPIService';
import { webApi } from '../../../service/webApi';
import { HttpClient } from "@angular/common/http";
import { AuthenticationService } from '../../../service/authentication.service'
import { AppConfig } from '../../../app.module';

@Injectable({
  providedIn: 'root'
})
export class TrainerServiceService {
  addEditTrainer : any = [];

  private get_All_Trainers: string = webApi.domain + webApi.url.get_All_Trainers;
  private update_trainer_details = webApi.domain + webApi.url.update_trainer;
  private url_add_edit_trainer = webApi.domain + webApi.url.add_edit_trainer;
  private dropdown_service = webApi.domain + webApi.url.trainer_dropdowns;
  private get_all_employees = webApi.domain + webApi.url.get_all_employee_trainer;

  constructor( private _http: HttpClient, private authenticationService: AuthenticationService) { }
  getTrainerList(data) {
    let url: any = this.get_All_Trainers;

    return new Promise(resolve => {
      this._http.post(url, data)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  getAllEmployee(data) {
    return new Promise(resolve => {
      this._http.post(this.get_all_employees, data)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  

  disableTrainer(data) {
    return new Promise(resolve => {
      this._http.post(this.update_trainer_details, data)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }


  getAddEditTrainers(data) {
    let url:any = this.url_add_edit_trainer;
    return new Promise(resolve => {
      this._http.post(url, data)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  trainer_dropDown(data) {
    return new Promise(resolve => {
      this._http.post(this.dropdown_service, data)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

}


