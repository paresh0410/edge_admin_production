import { Component, OnInit } from '@angular/core';
import { TrainerServiceService } from '../trainer-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { HttpClient } from '@angular/common/http';
import * as _moment from 'moment';
import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';
import { ToastrService } from 'ngx-toastr';
import { CommonFunctionsService } from '../../../../service/common-functions.service';
import { webApi } from '../../../../service/webApi';
import { SuubHeader } from '../../../components/models/subheader.model';
import { BrandDetailsService } from '../../../../service/brand-details.service';
const moment = (_moment as any).default ? (_moment as any).default : _moment;

export const MY_CUSTOM_FORMATS = {
  fullPickerInput: 'DD-MM-YYYY HH:mm:ss',
  parseInput: 'DD-MM-YYYY HH:mm:ss',
  datePickerInput: 'DD-MM-YYYY',
  timePickerInput: 'LT',
  monthYearLabel: 'MMM YYYY',
  dateA11yLabel: 'LL',
  monthYearA11yLabel: 'MMMM YYYY'
};
@Component({
  selector: 'ngx-add-trainer',
  templateUrl: './add-trainer.component.html',
  styleUrls: ['./add-trainer.component.scss'],
  providers: [
    { provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE] },
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS }]
})
export class AddTrainerComponent implements OnInit {


  // variable declaration spot strictly declare here :|
  getAddEditTrainerData: any = [];
  submitted: boolean;
  addOrEditValue: any;

  employeeCodes: any = [];
  standard: any = [];
  languages: any = [];
  visibility: any = [];
  partners: any = [];
  selectedLanguage: any = [];
  allLanguageAutoDrop: any = [];
  allowTrainerCode: boolean = false;
  loginUserdata: any = [];
  settingsLanguageDrop: any;
  settingsCodeDrop: any;
  trainerData: any = [];
  trainerDetails: any = [];
  selectedCode: any = [];
  allCodeAutoDrop: any = [];
  max = new Date();
  maxDate = new Date(
    this.max.getFullYear(),
    this.max.getMonth(),
    this.max.getDate() - 1,
    this.max.getHours(),
    this.max.getMinutes(),
  );

  d: any;
  title: string;
  currentBrandData: any;



  // constructor is used to fetch the value from the database

  constructor(
    private spinner: NgxSpinnerService,
    // private toasterService: ToasterService,
    private trainerService: TrainerServiceService,
    public router: Router,
    public routes: ActivatedRoute,
    private http1: HttpClient,
    private toastr: ToastrService,
    private commonFunctionService: CommonFunctionsService,
    public brandService: BrandDetailsService,
  ) {
    // this.getHelpContent();
    if (localStorage.getItem('LoginResData')) {
      this.loginUserdata = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('this.loginUserdata', this.loginUserdata);
    }

    console.log('Get Value of add or Edit >>', this.addOrEditValue);

    //calling dropdown function before data
    this.addOrEditValue = this.trainerService.addEditTrainer[0];
    console.log(this.addOrEditValue,"addOrEditValue")
    this.trainerData = this.trainerService.addEditTrainer[1];
    console.log('this.trainerDataCon', this.trainerData);
    this.dropdownPrepare();
    

    this.settingsLanguageDrop = {
      text: 'Select Language',
      singleSelection: false,
      classes: 'common-multi',
      primaryKey: 'languageId',
      labelKey: 'languageName',
      noDataLabel: 'Search Language...',
      enableSearchFilter: true,
      searchBy: ['languageId', 'languageName'],
      maxHeight:250,
      lazyLoading:true,
    };

    this.settingsCodeDrop = {
      text: 'Select Code',
      singleSelection: true,
      classes: 'common-multi',
      primaryKey: 'id',
      labelKey: 'name',
      noDataLabel: 'Search Code...',
      enableSearchFilter: true,
      searchBy: ['id', 'name'],
      maxHeight:250,
      lazyLoading:true,
    };
    
  }
  header: SuubHeader  = {
    title:'',
    btnsSearch: true,
    btnName1: 'Save',
    btnName1show: true,
    btnBackshow: true,
    showBreadcrumb: true,
    breadCrumbList:[]   
  };
  ngOnInit() {
    this.header.breadCrumbList=[
      { 'name': 'Settings',
      'navigationPath': '/pages/plan',
    },
      {
      'name': 'Trainer',
      'navigationPath': '/pages/plan/trainer',
      }]
      this.header.title=this.trainerData?this.trainerData.trainerName:'Add Trainer',
    //this.allowTrainerCode = false;
    // this.prepareDataForAddEdit();
    this.currentBrandData = this.brandService.getCurrentBrandData();
    if(this.currentBrandData.employee=="Employee"){
      this.getHelpContent();
    }else{
      this.getContent();
    }
  }

  gotoBack() {
    this.router.navigate(['../'], { relativeTo: this.routes });
  }

  addAction: boolean = false;
  prepareDataForAddEdit() {
    console.log('this.trainerData', this.trainerData);

    if (this.trainerService.addEditTrainer[0] == 'ADD') {
      this.addAction = true;

      this.trainerDetails = {
        trainerName: '',
        standardId: '',
        dateOfBirth: '',
        trainerId: 0,
        empCode: [],
        isInternal: '',
        trainerStatus:this.visibility[0].visibilityId,
        agencyName: '',
        mobileNo: '',
        alternateMobile: '',
        expYears: '',
        emailId: '',
        languageId: [],
        partner: '',
      };
    } else if (this.trainerService.addEditTrainer[0] == 'EDIT') {
      this.addAction = false;
      let selectedEmployee = [];
      // if (this.trainerData.employeeCode) {
      //   for (let i = 0; i < this.employeeCodes.length; i++) {
      //     if (this.trainerData.employeeCode == this.employeeCodes[i].id) {
      //       selectedEmployee.push(this.employeeCodes[i]);
      //     }
      //   }
      // } else {
      //   selectedEmployee = [];
      // }

      if (this.trainerData.employeeCode) {
        const empObj = {
          id: parseInt(this.trainerData.employeeCode),
          name: this.trainerData.employeeName,
          stat: 1,
        };
        selectedEmployee.push(empObj);
        //this.employeeCodes.push(empObj);
        console.log('selectedEmployee', selectedEmployee);

      } else {
        selectedEmployee = [];
      }

      var selectedLanguage = [];

      if (this.trainerData.languageId) {
        console.log(this.trainerData.languageId,"this.trainerData.languageId")
        console.log(this.languages,"this.languages")
        const langArray = this.trainerData.languageId;
        // for (const lanArrayData of langArray) {
        //   for (const langData of this.languages) {
        //     if (langData.languageId == lanArrayData) {
        //       selectedLanguage.push(langData);
        //     }
        //   }
        // }

         selectedLanguage = this.commonFunctionService.getMatchedArray(this.languages,langArray, 'languageId');
        console.log('selectedLanguage', selectedLanguage);
      }

      // console.log('selectedEmployee', selectedEmployee);
      this.trainerDetails = {
        trainerName: this.trainerData.trainerName,
        standardId: this.trainerData.standardId,
        dateOfBirth: this.trainerData.dateOfBirth,
        trainerId: this.trainerData.ID,
        empCode: selectedEmployee,
        trainerTypeId: this.trainerData.trainerTypeId,
        trainerStatus: this.trainerData.visibilityId,
        agencyName: this.trainerData.agencyName,
        mobileNo: this.trainerData.mobileNo,
        alternateMobile: this.trainerData.alternateMobile,
        expYears: this.trainerData.expYears,
        emailId: this.trainerData.emailId,
        languageId: selectedLanguage,
        partner: this.trainerData.partnerId ? this.trainerData.partnerId : '',
      };
      //console.log(this.trainerDetails.languageId,"languageid")
      console.log(this.trainerDetails,"trainerDetails")


      // to enable the 'is internal button' is selected or not
      if (this.trainerData.trainerTypeId == 1) {
        this.allowTrainerCode = true;
      }
    }
  }

  // validateNum(event: any) {
  //   if (event.target.value < 0) {
  //     event.target.value = 0;
  //   }
  //   if (event.target.value.length > 100) {
  //     event.target.value = 100;
  //   }
  // }

  onChecked(event: Event) {
    const data = (<HTMLInputElement>event.target).checked;
    console.log('checked value >>>', data);
    if (data === true) {
      this.nameDisable = true;
      this.allowTrainerCode = true;
    } else if (data === false) {
      this.allowTrainerCode = false;
      this.nameDisable = false;
      this.trainerDetails.trainerName = null;
    }
  }

  onLanguageSelect(item: any) {
    console.log(item);
    console.log(this.selectedLanguage);
  }

  onLanguageDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedLanguage);
  }

  langList: any;
  onLanguageSearch(evt: any) {
    console.log(evt.target.value);
    const val = evt.target.value;
    // this.langList = [];
    const tuser = this.langList.filter(function (d) {
      return (
        String(d.languageId)
          .toLowerCase()
          .indexOf(val) !== -1 ||
        String(d.languageName).toLowerCase().indexOf(val) !== -1
      );
    });

    // update the rows
    this.languages = tuser;
  }

  warningModal: boolean = false;
  nameDisable: boolean = false;
  onCodeSelect(item: any) {
    console.log('CodeSelect item', item);
    if (item.stat) {
      this.warningModal = true;
    } else {
      this.nameDisable = true;
      this.trainerDetails.trainerName = item.name;
      this.trainerDetails.emailId = item.email;
      this.trainerDetails.mobileNo = item.phone1;
    }
    console.log(this.selectedCode);
  }

  closeWarningModal() {
    this.warningModal = false;
    this.trainerDetails.empCode = [];
  }

  OnCodeDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedCode);
  }

  codeList: any;
  onCodeSearch(evt: any) {
    console.log(evt.target.value);
    const val = evt.target.value;
    this.employeeCodes = [];
    if (val.length > 3) {
      this.searchEmployee(val);
    }
  }

  trainer_Data1: any;
  nodata: any;
  submitRes: any;

  // pushing value to server
  onSubmit(data,form) {
    if(form.valid && form.value.expYrs>1){
    // this.spinner.show();
    this.submitted = true;
    console.log('data.empCode', data.empCode);
    if (data.isInternal) {
      if (data.empCode.length == 0 || data.empCode == undefined) {
        console.log('please select employee.');
        // this.presentToast('error', '', 'please select employee.');
        this.presentToast('error', 'please select employee.');
      } else {
        this.addEditTrainer(data);
      }
    } else {
      this.addEditTrainer(data);
    }
    console.log('form data >> ', data);
  }else{
    Object.keys(form.controls).forEach(key => {
      console.log(key,"key")
      form.controls[key].markAsDirty();
    });

  }
  }

  addEditTrainer(data) {
    this.spinner.show();
    let languageIdArray: any = [];
    console.log('data', data);

    for (const language of data.languageId) {
      console.log(language.id);
      languageIdArray.push(language.languageId);
      console.log(languageIdArray);
    }
    const langString: string = languageIdArray.join();
    console.log(langString);
    this.spinner.show();
    const param = {
      trainerId: this.addAction ? 0 : data.trainerId,
      tName: data.trainerName,
      empId: data.empCode.length === 0 ? null : data.empCode[0].id,
      isInt: data.trainerTypeId ? 1 : 0,
      standId: data.standardId,
      dob: this.formatSendDateTime(data.dateOfBirth),
      ageName: data.agencyName,
      mobile: data.mobileNo,
      altMobile: data.alternateMobile,
      expYear: data.expYears,
      email: data.emailId,
      language: langString,
      trainStat: data.trainerStatus,
      partner: data.partner ? data.partner : null,
      tId: this.loginUserdata.data.data.tenantId,
    };

    console.log('param', param);
    const url_add_edit_trainer = webApi.domain + webApi.url.add_edit_trainer;
    this.commonFunctionService.httpPostRequest(url_add_edit_trainer,param)
    //this.trainerService.getAddEditTrainers(param)
    .then(
      rescompData => {
        this.spinner.hide();
        console.log(rescompData);
        this.submitRes = rescompData;

        if (this.submitRes.type === false) {
          if (this.addAction) {
            this.presentToast('error', 'Unable to add trainer.');
            // this.presentToast(
            //   'error',
            //   'Trainer Details',
            //   'Unable to add trainer.'
            // );
          } else {
            this.presentToast('error', 'Unable to update trainer.');
            // this.presentToast(
            //   'error',
            //   'Trainer Details',
            //   'Unable to update trainer.'
            // );
          }
        } else {
          if (this.addAction) {
            this.presentToast('success', 'Trainer added successfully.');
            // this.presentToast(
            //   'success',
            //   'Trainer Details',
            //   'Trainer added successfully.'
            // );
            this.router.navigate(['../'], { relativeTo: this.routes });
          } else {
            this.presentToast('success', 'Trainer updated successfully.');
            // this.presentToast(
            //   'success',
            //   'Trainer Details',
            //   'Trainer updated successfully.'
            // );
            this.router.navigate(['../'], { relativeTo: this.routes });
          }
        }
      },
      resUserError => {
        this.spinner.hide();
        if (resUserError.statusText === 'Unauthorized') {
        }
        this.errorMsg = resUserError;
      }
    );
  }

  ////////////////////////////////////////////////// Dropdown service ////////////////////////////////////

  errorMsg: any;
  dropdownPrepare() {
    this.spinner.show();
    const data = {
      tId: this.loginUserdata.data.data.tenantId,
    };
    console.log(data);
    const dropdown_service = webApi.domain + webApi.url.trainer_dropdowns;
    this.commonFunctionService.httpPostRequest(dropdown_service,data)
    //this.trainerService.trainer_dropDown(data)
    .then(
      rescompData => {
        this.spinner.hide();
        if (rescompData['type'] === true) {
          console.log('dropdown data', rescompData['data']);
          this.languages = rescompData['data'][0];
          this.langList=this.languages;
          this.standard = rescompData['data'][1];
          //    this.employeeCodes = rescompData['data'][2];
          this.visibility = rescompData['data'][2];
          this.partners = rescompData['data'][4];
          this.prepareDataForAddEdit();
          console.log('data', this.standard);
          //   console.log('code data', this.employeeCodes);
          console.log('visibilty data', this.visibility);
          console.log('langList data', this.languages);
          console.log('partnerList data', this.partners);
        } else {
          this.presentToast('success', 'Trainer updated successfully.');
          // this.presentToast(
          //   'success',
          //   'Trainer Visibility',
          //   'Trainer updated successfully.'
          // );
        }
      },

      resUserError => {
        this.spinner.hide();
        this.errorMsg = resUserError;
        this.presentToast('success', 'Trainer updated successfully.');
        // this.presentToast(
        //   'success',
        //   'Trainer Visibility',
        //   'Trainer updated successfully.'
        // );
      }
    );
  }

  searchEmployee(srcStr) {
    this.spinner.show();
    const data = {
      srcStr: srcStr,
      tId: this.loginUserdata.data.data.tenantId,
    };
    console.log(data);
    const get_all_employees = webApi.domain + webApi.url.get_all_employee_trainer;
    this.commonFunctionService.httpPostRequest(get_all_employees,data)
    //this.trainerService.getAllEmployee(data)
    .then(
      rescompData => {
        this.spinner.hide();
        if (rescompData['type'] === true) {
          console.log('dropdown data', rescompData['data']);
          this.employeeCodes = rescompData['data'][0];
          console.log('code data', this.employeeCodes);
        } else {
          this.presentToast('error', 'Something went wrong. please try again later.');
        }
      },

      resUserError => {
        this.spinner.hide();
        this.errorMsg = resUserError;
        this.presentToast('error', 'Something went wrong. please try again later.');
      }
    );
  }

  formatSendDateTime(Dtime) {
    const t = new Date(Dtime);
    // const formattedDateTime = this.datepipe.transform(t, 'yyyy-MM-dd h:mm');
    const year = t.getFullYear() + '-' + (t.getMonth() + 1) + '-' + t.getDate() + ' ' + '00:00:00';
    // const time = ' ' + t.getHours() + ':' + t.getMinutes();
    const formattedDateTime = year;
    // + time;
    return formattedDateTime;
  }

  // presentToast(type, title, body) {
  //   const toastPop: Toast = {
  //     type: type,
  //     title: title,
  //     body: body,
  //     showCloseButton: true,
  //     timeout: 2000
  //   };
  //   this.toasterService.pop(toastPop);
  // }
  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }

  // Help Code Start Here //

  helpContent: any;
  getHelpContent() {
    return new Promise(resolve => {
      this.http1
        .get('../../../../../../assets/help-content/addEditCourseContent.json')
        .subscribe(
          data => {
            this.helpContent = data;
            console.log('Help Array', this.helpContent);
          },
          err => {
            resolve('err');
          }
        );
    });
    // return this.helpContent;
  }
  getContent() {
    return new Promise(resolve => {
      this.http1
        .get('../../../../../../assets/help-content/help_content.json')
        .subscribe(
          data => {
            this.helpContent = data;
            console.log('Help Array', this.helpContent);
          },
          err => {
            resolve('err');
          }
        );
    });
  }

  // Help Code Ends Here //

}
