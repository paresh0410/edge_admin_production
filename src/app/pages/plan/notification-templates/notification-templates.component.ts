import { Component, OnInit } from '@angular/core';
import { NotificationtemplateServiceService } from './notification-templates.service';
import { NgxSpinnerService } from 'ngx-spinner';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { Router, ActivatedRoute, Routes } from '@angular/router';
import { AddEditNotificationtemplateServiceService } from './add-edit-notification-template/add-edit-notification-template.service'
import { ToastrService } from 'ngx-toastr';
import { webApi } from '../../../service/webApi';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { Card } from '../../../models/card.model';
import { SuubHeader } from '../../components/models/subheader.model';
import{NoDataComponent}from'../../../component/no-data/no-data.component';
import { noData } from '../../../models/no-data.model';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'ngx-notification-templates',
  templateUrl: './notification-templates.component.html',
  styleUrls: ['./notification-templates.component.scss']
})
export class NotificationTemplatesComponent implements OnInit {
  noDataVal:noData={
    imageSrc: '../../../assets/images/no-data-bg.svg',
    margin:'mt-5',
    title:'No notifications at this time',
    descShow:true,
    desc:'Admins will be able to set templates for notification events. Notifications are sent to the learners and instructors by email, SMS or mobile when an event occurs. ',
    titleShow:true,
  }
  header: SuubHeader  = {
    title: 'Notification Templates',
    btnsSearch: true,
    placeHolder:'Search',
    searchBar: true,
    dropdownlabel: ' ',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: '',
    btnName3: '',
    btnAdd: '',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: false,
    filter: false,
    showBreadcrumb: true,
    breadCrumbList:[]
  };
  count:number=8;
  Nodatafound: boolean = false;
  noEvents: boolean = false;
  rows: any;
  noCategory: boolean = false;
  search: any = {};
  cardModify: Card = {
    flag: 'ntfTemplate',
    titleProp : 'eventName',
    image: 'icon',
    // hoverlable:   true,
    showImage: true,
    option:   true,
    sms: true,
    bottomDiv:   true,
    msg: true,
    mbl: true,
    showBottomList:   true,
    bottomTitle:   true,
    bulkCopy : true,
    defaultImage:'assets/images/notification.png'
    // bulkCopyCondition: "cardsList['areaId'] == 2",
    // btnLabel: 'Edit',
  };
  skeletonArray:[1,2,3,4,5,6,7,8]
  tenantId: any;
  skeleton = false;
  btnName='Save'
  notTemp: any = {};
  getDataForAddEdit: any = [];
  addAction: boolean = false;
  // htmlEditor:boolean=false;
  title = '';
  emailTemplateName = '';
  emailTemplate = false;
  config = {
    height: '200px',
    uploader: {
      insertImageAsBase64URI: true,
    },
    allowResizeX: false,
    allowResizeY: false,
    placeholder: 'Email Body',
    limitChars: 3000,
  };
  showTemplateFor = '';
  sectionshow: boolean;
  constructor(public router: Router,
    private notificationtemplateservice: NotificationtemplateServiceService, private spinner: NgxSpinnerService,
    // private toasterService: ToasterService,
    private addeditnotitempservice: AddEditNotificationtemplateServiceService,private addeditnottempservice:NotificationtemplateServiceService,
    private toastr: ToastrService,private commonFunctionService: CommonFunctionsService,private http1: HttpClient) {
    // this.spinner.show();
    this.getHelpContent();
    if (localStorage.getItem('LoginResData')) {
      var userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('user login data:--', userData);
      this.tenantId = userData.data.data.tenantId;
      console.log("tenant id ", this.tenantId)
    }
    this.fetchAllEvents();

  }

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }

  ngOnInit() {
    this.header.breadCrumbList=[{
      'name': 'Settings',
      'navigationPath': '/pages/plan',
      }]
  }
  clear() {
    this.search = {};
  }
  submit(data,form) {

    console.log('Form ===>',form);
    if(form && form.valid){
      this.spinner.show();
      let param = {
        "id": this.addAction ? 0 : this.notTemp.tempId,
        "tname": data.name,
        "tdescription": data.desc,
        "subject": data.subject ? data.subject : null,
        "template": data.content,
        "avPlaceHolders": null,
        "notModeId": this.notTemp.nmId,
        "notEventId": this.notTemp.neId,
        "areaId": this.notTemp.aId,
        "visible": 1,
        "tid": this.tenantId
      }

      console.log('param', param)

      this.addeditnottempservice.addEditNotTemplate(param)
        .then(rescompData => {
          console.log('AddEditNotRes:', rescompData);
          this.spinner.hide();
          var res = rescompData;
          var result = res['data'];
          if (this.getDataForAddEdit[0] == "ADD") {
            if (res['type'] == true) {
              //   var catUpdate : Toast = {
              //     type: 'success',
              //     title: "Notification Template Inserted!",
              //     body: "New template added successfully.",
              //     showCloseButton: true,
              //     timeout: 2000
              // };
              // this.toasterService.pop(catUpdate);
              this.presentToast('success', 'Notification template added');
              // window.history.back();
          this.closesectionModel();
              // this.router.navigate(['/pages/plan/notification-templates']);
            } else {

              //   var catUpdate : Toast = {
              //     type: 'error',
              //     title: "Notification Template Inserted!",
              //     body: "Unable to add template.",
              //     showCloseButton: true,
              //     timeout: 2000
              // };
              // this.toasterService.pop(catUpdate);
              this.presentToast('error', '');
            }
          } else {
            if (res['type'] == true) {

              //   var catUpdate : Toast = {
              //     type: 'success',
              //     title: "Notification Template Updated!",
              //     body: "Template updated successfully.",
              //     showCloseButton: true,
              //     timeout: 2000
              // };
              // this.toasterService.pop(catUpdate);
              this.presentToast('success', 'Notification template updated');
              // window.history.back();
          this.closesectionModel();
              // this.router.navigate(['/pages/plan/notification-templates']);
            } else {
              //   var catUpdate : Toast = {
              //     type: 'error',
              //     title: "Notification Template Updated!",
              //     body: "Unable to update template.",
              //     showCloseButton: true,
              //     timeout: 2000
              // };
              // this.toasterService.pop(catUpdate);
              this.presentToast('error', '');
            }
          }
        }, error => {
          // this.spinner.hide();
          this.closesectionModel();
          console.log('AddEditNotResErr:', error);

          // var toast: Toast = {
          //   type: 'error',
          //   body: "Something went wrong.please try again later.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        });
    }else {
      Object.keys(form.controls).forEach(key => {
        form.controls[key].markAsDirty();
      });
    }

  }
  back() {
    this.router.navigate(['/pages/plan']);
  }
  fetchAllEvents() {
    let param = {
      "tenantId": this.tenantId
    }
    const _urlFetch:string = webApi.domain + webApi.url.fetchnotificationtemplateevents;
    this.commonFunctionService.httpPostRequest(_urlFetch,param)
    //this.notificationtemplateservice.getNotEvents(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('getEventResponse:', rescompData);
        if (result['type'] == true) {
          if (result['data'][0].length == 0) {
            this.noEvents = true;
            this.skeleton = true;
          } else {
            this.rows = result['data'][0]
            console.log('this.rows', this.rows);
          }

        } else {
          this.spinner.hide();
          this.Nodatafound = true;
          //   var toast : Toast = {
          //     type: 'error',
          //     //title: "Server Error!",
          //     body: "Something went wrong.please try again later.",
          //     showCloseButton: true,
          //     timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }
        this.skeleton = true;

      }, error => {
        this.spinner.hide();
        this.Nodatafound = true;
        this.skeleton = true;
        //   var toast : Toast = {
        //     type: 'error',
        //     //title: "Server Error!",
        //     body: "Something went wrong.please try again later.",
        //     showCloseButton: true,
        //     timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
  }

  gotoaddedit(id, eventId, areaId) {
    if (id == 1) {
      this.fetchsmsdata(eventId, areaId);
    } else if (id == 2) {
      this.fetchemaildata(eventId, areaId);
    } else if (id == 3) {
      this.fetchmobiledata(eventId, areaId);
    }
  }

  smsClick(id,item) {
    if (id == 1) {
      this.title='Edit '
      this.sectionshow=true
      this.showTemplateFor = 'sms';
      this.fetchsmsdata(item.notEventId, item.areaId);
    } else if (id == 2) {
      this.title='Edit '
      this.sectionshow=true
      this.showTemplateFor = 'email';
      this.fetchemaildata(item.notEventId, item.areaId);
    } else if (id == 3) {
      this.title='Edit '
      this.sectionshow=true
      this.showTemplateFor = 'appNotification';
      this.fetchmobiledata(item.notEventId, item.areaId);
    }
  }
  closesectionModel(){
    this.sectionshow=false
  }
  fetchsmsdata(eventId, areaId) {
    this.spinner.show();
    let param = {
      "neId": eventId,
      "nmId": 1,
      "tId": this.tenantId,
      "aId": areaId
    }
    const _urlFetchEventTemplate:string = webApi.domain + webApi.url.fetchnotificationtemplate;
    this.commonFunctionService.httpPostRequest(_urlFetchEventTemplate,param)
    //this.notificationtemplateservice.getNotEventsTemplates(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('TemplateData:', result)
        if (result['type'] == true) {
          if (result['data'][0].length == 0) {
            var action = "ADD"
            this.title='Add SMS Template'
            this.addAction=true;
            this.notTemp.tempId = '',
            this.notTemp.name = '',
            this.notTemp.desc = '',
            this.notTemp.subject = '',
            this.notTemp.content = '',
            this.notTemp.neId = eventId,
            this.notTemp.nmId =  1,
            this.notTemp.aId =  areaId
            // this.addeditnotitempservice.getdataEdit[0] = 1;
            // this.addeditnotitempservice.getdataEdit[1] = '';
            // this.addeditnotitempservice.getdataEdit[2] = action;
            // this.addeditnotitempservice.getdataEdit[3] = eventId;
            // this.addeditnotitempservice.getdataEdit[4] = areaId;
            // this.router.navigate(['/pages/plan/notification-templates/add-edit-notification-template']);
          } else {
            var res = result['data'][0];
            var action = "EDIT"
            this.title='Edit SMS Template'
            this.addAction=false;
            this.notTemp.tempId =  res[0].tId,
            this.notTemp.name =  res[0].tname,
            this.notTemp.desc = res[0].desc,
            this.notTemp.subject =  res[0].subject,
            this.notTemp.content =  res[0].template,
            this.notTemp.nmId = 1;
            this.notTemp.neId = eventId;
            this.notTemp.aId = areaId;
            // this.notTemp.neId = this.getDataForAddEdit[3],
            // this.notTemp.nmId = this.getDataForAddEdit[0],
            // this.notTemp.aId = this.getDataForAddEdit[4]
            // this.addeditnotitempservice.getdataEdit[1] = res[0];
            // this.addeditnotitempservice.getdataEdit[2] = action;
            console.log('Notification Event templates sms:', res[0]);
            // this.router.navigate(['/pages/plan/notification-templates/add-edit-notification-template']);
          }

        } else {
          this.spinner.hide();
          //   var toast : Toast = {
          //     type: 'error',
          //     //title: "Server Error!",
          //     body: "Something went wrong.please try again later.",
          //     showCloseButton: true,
          //     timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }


      }, error => {
        this.spinner.hide();
        //   var toast : Toast = {
        //     type: 'error',
        //     //title: "Server Error!",
        //     body: "Something went wrong.please try again later.",
        //     showCloseButton: true,
        //     timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });

  }

  fetchemaildata(eventId, areaId) {
    this.spinner.show();
    let param = {
      "neId": eventId,
      "nmId": 2,
      "tId": this.tenantId,
      "aId": areaId
    }
    const _urlFetchEventTemplate:string = webApi.domain + webApi.url.fetchnotificationtemplate;
    this.commonFunctionService.httpPostRequest(_urlFetchEventTemplate,param)
    //this.notificationtemplateservice.getNotEventsTemplates(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;

        if (result['type'] == true) {
          if (result['data'][0].length == 0) {
            var action = "ADD"
            this.title='Add Email Template '
            this.addAction=true;
            this.notTemp.tempId = '',
            this.notTemp.name = '',
            this.notTemp.desc = '',
            this.notTemp.subject = '',
            this.notTemp.content = '',
            this.notTemp.neId = eventId,
            this.notTemp.nmId =  2,
            this.notTemp.aId =  areaId
            // this.addeditnotitempservice.getdataEdit[0] = 2;
            // this.addeditnotitempservice.getdataEdit[1] = '';
            // this.addeditnotitempservice.getdataEdit[2] = action;
            // this.addeditnotitempservice.getdataEdit[3] = eventId;
            // this.addeditnotitempservice.getdataEdit[4] = areaId;
            // this.router.navigate(['/pages/plan/notification-templates/add-edit-notification-template']);
          } else {
            var res = result['data'][0];
            var action = "EDIT"
            this.addAction=false;
            this.title='Edit Email Template '
            this.notTemp.tempId =  res[0].tId,
            this.notTemp.name =  res[0].tname,
            this.notTemp.desc = res[0].desc,
            this.notTemp.subject =  res[0].subject,
            this.notTemp.content =  res[0].template,
            this.notTemp.nmId = 2;
            this.notTemp.neId = eventId;
            this.notTemp.aId = areaId;
            // this.addeditnotitempservice.getdataEdit[0] = 2;
            // this.addeditnotitempservice.getdataEdit[1] = res[0];
            // this.addeditnotitempservice.getdataEdit[2] = action;
            // this.addeditnotitempservice.getdataEdit[3] = eventId;
            // this.addeditnotitempservice.getdataEdit[4] = areaId;
            // console.log('Notification Event templates email:', res[0]);
            // this.router.navigate(['/pages/plan/notification-templates/add-edit-notification-template']);
          }

        } else {
          this.spinner.hide();
          //   var toast : Toast = {
          //     type: 'error',
          //     //title: "Server Error!",
          //     body: "Something went wrong.please try again later.",
          //     showCloseButton: true,
          //     timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }


      }, error => {
        this.spinner.hide();
        //   var toast : Toast = {
        //     type: 'error',
        //     //title: "Server Error!",
        //     body: "Something went wrong.please try again later.",
        //     showCloseButton: true,
        //     timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
    // this.router.navigate(['/pages/plan/notification-templates/add-edit-notification-template']);
  }
  helpContent:any
  getHelpContent() {
    return new Promise(resolve => {
      this.http1
        .get('../../../../../assets/help-content/addEditCourseContent.json')
        .subscribe(
          data => {
            this.helpContent = data;
            console.log('Help Array', this.helpContent);
          },
          err => {
            resolve('err');
          }
        );
    });
    // return this.helpContent;
  }
  fetchmobiledata(eventId, areaId) {
    this.spinner.show();
    let param = {
      "neId": eventId,
      "nmId": 3,
      "tId": this.tenantId,
      "aId": areaId
    }
    const _urlFetchEventTemplate:string = webApi.domain + webApi.url.fetchnotificationtemplate;
    this.commonFunctionService.httpPostRequest(_urlFetchEventTemplate,param)
    //this.notificationtemplateservice.getNotEventsTemplates(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;

        if (result['type'] == true) {
          if (result['data'][0].length == 0) {
            var action = "ADD"
            this.title='Add App Notification Template '
            this.addAction=true;
            this.notTemp.tempId = '',
            this.notTemp.name = '',
            this.notTemp.desc = '',
            this.notTemp.subject = '',
            this.notTemp.content = '',
            this.notTemp.neId = eventId,
            this.notTemp.nmId =  3,
            this.notTemp.aId =  areaId
            // this.addeditnotitempservice.getdataEdit[0] = 3;
            // this.addeditnotitempservice.getdataEdit[1] = '';
            // this.addeditnotitempservice.getdataEdit[2] = action;
            // this.addeditnotitempservice.getdataEdit[3] = eventId;
            // this.addeditnotitempservice.getdataEdit[4] = areaId;
            // this.router.navigate(['/pages/plan/notification-templates/add-edit-notification-template']);
          } else {
            var res = result['data'][0];
            var action = "EDIT"
            this.addAction=false;
            this.title='Edit App Notification Template '
            this.notTemp.tempId =  res[0].tId,
            this.notTemp.name =  res[0].tname,
            this.notTemp.desc = res[0].desc,
            this.notTemp.subject =  res[0].subject,
            this.notTemp.content =  res[0].template,
            this.notTemp.nmId = 3;
            this.notTemp.neId = eventId;
            this.notTemp.aId = areaId;
            // this.addeditnotitempservice.getdataEdit[0] = 3;
            // this.addeditnotitempservice.getdataEdit[1] = res[0];
            // this.addeditnotitempservice.getdataEdit[2] = action;
            // this.addeditnotitempservice.getdataEdit[3] = eventId;
            // this.addeditnotitempservice.getdataEdit[4] = areaId;
            console.log('Notification Event templates phone:', res[0]);
            // this.router.navigate(['/pages/plan/notification-templates/add-edit-notification-template']);
          }

        } else {
          this.spinner.hide();
          //   var toast : Toast = {
          //     type: 'error',
          //     //title: "Server Error!",
          //     body: "Something went wrong.please try again later.",
          //     showCloseButton: true,
          //     timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }


      }, error => {
        this.spinner.hide();
        //   var toast : Toast = {
        //     type: 'error',
        //     //title: "Server Error!",
        //     body: "Something went wrong.please try again later.",
        //     showCloseButton: true,
        //     timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
    //this.router.navigate(['/pages/plan/notification-templates/add-edit-notification-template']);
  }

  search1(event){
    const val =event.target.value
    if(val.length>=3 ||val.length==0){
    this.search.eventName = event.target.value
    console.log(event,"event")
    }
    this.noDataVal={
      margin:'mt-5',
      imageSrc: '../../../../../assets/images/no-data-bg.svg',
      title:"Sorry we couldn't find any matches please try again",
      desc:".",
      titleShow:true,
      btnShow:false,
      descShow:false,
      btnText:'Learn More',
      btnLink:''
  }
  }

  configBulkEnrol = {
    newsDetails: {
      show: true,
      tabTitle: "Detail",
      identifier: "addNewsForm",
      disabled: false,
      showSave: true,
    },
    categoryList: {
      show: true,
      tabTitle: "Category",
      identifier: "category",
      disabled: true,
      showSave: false,
    },
    courseList: {
      show: true,
      tabTitle: "Batch",
      identifier: "courseList",
      disabled: true,
      showSave: true,
    },
    resultList: {
      show: true,
      tabTitle: "Result",
      identifier: "resultList",
      disabled: true,
      showSave: false,
    },
  };

  performActionOnBulk(event){
    console.log("Event ==>", event);
    if (event) {
      // console.log('args ==>', event.argument.join(','));
      // console.log('args ==>', [...event.argument]);
      switch (event.action) {
       case 'closeCopy': this.bulkCopyEnableDisable(event["argument"][0],event["argument"][1]);
      }
    }
  }
  showBulkUploadContainer = false;
  selectedBulkActivity = null;
  bulkCopyEnableDisable(activityData,flag){
    this.showBulkUploadContainer = flag;
    this.selectedBulkActivity = activityData;
  }

  getNotificationTemplateForBulk(data, flag){
    this.spinner.show();
    this.resetBasicDetails();
    const param = {
      eventId: data.notEventId,
      aId: data.areaId,
    };
    const _urlGetBulkNotificationTemplate: string =
      webApi.domain + webApi.url.getBulkNotificationTemplate;
    this.commonFunctionService
      .httpPostRequest(_urlGetBulkNotificationTemplate, param)
      // this.newsService.getNotificationsTags(param)
      .then(
        (rescompData) => {

          console.log('rescompData', rescompData);
          if(rescompData['type'] && rescompData['output'] &&  rescompData['output'].length !== 0){
            this.basicDetails['eventId'] = data.notEventId;
            this.basicDetails['aId'] =  data.areaId;
            this.makeNotificationFormDataReady(rescompData['output']);
            this.bulkCopyEnableDisable(data, flag);
            this.spinner.hide();
          }else {
            this.spinner.hide();
            this.toastr.warning('Something went wrong', 'Warning');
          }
        },
        (error) => {
          console.log('error', error);
          this.toastr.warning('Something went wrong', 'Warning');
          this.spinner.hide();
        },
      );
  }

  basicDetails = {
    NotificationTypeName: null,
    // NotificationTypeText: null,
    AppNotificationCheckBox: false,
    AppNotificationSubject: null,
    AppNotificationDescrition: null,
    SMSChecked: false,
    SMSText: null,
    EmailCheckBox: false,
    EmailSubject: null,
    EmailDescrition: null,
    attchRef: null,
    description: "",
    eventId: null,
    aId: null,
    id: 0,
    SMSTemplateId: null,
    emailTemplateId: null,
    appTemplateId: null,
  };
  makeNotificationFormDataReady(data){

    for(let i = 0; i < data.length ; i ++){
      if(data[i]['modeId'] == 1){
        this.basicDetails.SMSText = data[i]['template'];
        this.basicDetails['SMSTemplateId'] = data[i]['tempId'];
      }
      if(data[i]['modeId'] == 2){
        this.basicDetails.EmailSubject = data[i]['subject'];
        this.basicDetails.EmailDescrition = data[i]['template'];
        this.basicDetails['emailTemplateId'] = data[i]['tempId'];
      }
      if(data[i]['modeId'] == 3){
        this.basicDetails.AppNotificationSubject = data[i]['template'];
        this.basicDetails.AppNotificationDescrition = data[i]['desc'];
        this.basicDetails['appTemplateId'] = data[i]['tempId'];
      }
    }
    console.log("basicDetails" , this.basicDetails)
  }

  resetBasicDetails(){
    this.basicDetails = {
      NotificationTypeName: null,
      // NotificationTypeText: null,
      AppNotificationCheckBox: false,
      AppNotificationSubject: null,
      AppNotificationDescrition: null,
      SMSChecked: false,
      SMSText: null,
      EmailCheckBox: false,
      EmailSubject: null,
      EmailDescrition: null,
      attchRef: null,
      description: "",
      eventId: null,
      aId: null,
      SMSTemplateId: null,
      emailTemplateId: null,
      appTemplateId: null,
      id: 0,
    };
  }
}
