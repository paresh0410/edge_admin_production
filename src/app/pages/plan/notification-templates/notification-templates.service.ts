import {Injectable, Inject} from '@angular/core';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppConfig} from '../../../app.module';
import { webApi } from '../../../service/webApi';
import { HttpClient } from "@angular/common/http";

@Injectable()
export class NotificationtemplateServiceService {

  private _urlFetch:string = webApi.domain + webApi.url.fetchnotificationtemplateevents;
  private _urlFetchEventTemplate:string = webApi.domain + webApi.url.fetchnotificationtemplate;
  private _urlFetchNotTempById:string = webApi.domain + webApi.url.fetchnottempbyid
  private _urlAddEditNotificationTemplate:string = webApi.domain + webApi.url.addeditnottemplate
  getdataEdit : any = [];

  constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private _http: Http,private _httpClient:HttpClient){
      //this.busy = this._http.get('...').toPromise();
  }

  getNotEvents(param){
    // let url:any = this._urlFetch;
    // return this._http.post(url,param)
    //     .map((response:Response)=>response.json())
    //     .catch(this._errorHandler);

      return new Promise(resolve => {
        this._httpClient.post(this._urlFetch, param)
        //.map(res => res.json())
        .subscribe(data => {
            resolve(data);
        },
        err => {
            resolve('err');
        });
    });
}

getNotEventsTemplates(param){
  // let url:any = this. _urlFetchEventTemplate;
  // return this._http.post(url,param)
  //     .map((response:Response)=>response.json())
  //     .catch(this._errorHandler);

    return new Promise(resolve => {
      this._httpClient.post(this._urlFetchEventTemplate, param)
      //.map(res => res.json())
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}
_errorHandler(error: Response){
  console.error(error);
  return Observable.throw(error || "Server Error")
}

getNottepById(param){
  // let url:any = this._urlFetchNotTempById;
  // return this._http.post(url,param)
  //     .map((response:Response)=>response.json())
  //     .catch(this._errorHandler);
    return new Promise(resolve => {
      this._httpClient.post(this._urlFetchNotTempById, param)
      //.map(res => res.json())
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}



addEditNotTemplate(param){
  // let url:any = this._urlAddEditNotificationTemplate;
  // return this._http.post(url,param)
  //     .map((response:Response)=>response.json())
  //     .catch(this._errorHandler);

  return new Promise(resolve => {
    this._httpClient.post(this._urlAddEditNotificationTemplate, param)
    //.map(res => res.json())
    .subscribe(data => {
        resolve(data);
    },
    err => {
        resolve('err');
    });
});
}

}
