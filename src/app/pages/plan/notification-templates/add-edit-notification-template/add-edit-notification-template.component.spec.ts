import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditNotificationTemplateComponent } from './add-edit-notification-template.component';

describe('AddEditNotificationTemplateComponent', () => {
  let component: AddEditNotificationTemplateComponent;
  let fixture: ComponentFixture<AddEditNotificationTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditNotificationTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditNotificationTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
