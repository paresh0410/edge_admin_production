
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Routes } from '@angular/router';
import { AddEditNotificationtemplateServiceService } from './add-edit-notification-template.service';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';
import { SuubHeader } from '../../../components/models/subheader.model';

@Component({
  selector: 'ngx-add-edit-notification-template',
  templateUrl: './add-edit-notification-template.component.html',
  styleUrls: ['./add-edit-notification-template.component.scss']
})
export class AddEditNotificationTemplateComponent implements OnInit {

  notTemp: any = {};
  getDataForAddEdit: any = [];
  addAction: boolean = false;
  // htmlEditor:boolean=false;
  title = '';
  emailTemplateName = '';
  emailTemplate = false;
  tenantId: any;
  config = {
    height: '200px',
    uploader: {
      insertImageAsBase64URI: true,
    },
    allowResizeX: false,
    allowResizeY: false,
    placeholder: 'Email Body',
    limitChars: 3000,
  };
  showTemplateFor = '';
  constructor(public router: Router, private addeditnottempservice: AddEditNotificationtemplateServiceService,
    // private toasterService: ToasterService,
    private toastr: ToastrService, private http1: HttpClient,
    private spinner: NgxSpinnerService,
  ) {
    this.getHelpContent();

    if (localStorage.getItem('LoginResData')) {
      var userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('user login data:--', userData);
      this.tenantId = userData.data.data.tenantId;
      console.log("tenant id ", this.tenantId)
    }

    this.getDataForAddEdit = this.addeditnottempservice.getdataEdit;
    console.log('getDataForAddEdit', this.getDataForAddEdit);
    // if (this.getDataForAddEdit[0] == 1) {
    //   this.title = 'SMS'
    // } else if (this.getDataForAddEdit[0] == 2) {
    //   this.title = 'EMAIL';
    //   this.emailTemplate = true;
    // }
    // else if (this.getDataForAddEdit[0] == 3) {
    //   this.title = 'APP'
    // }

    if (this.getDataForAddEdit[0] == 1) {
      this.title = 'SMS';
      this.showTemplateFor = 'sms';
    } else if (this.getDataForAddEdit[0] == 2) {
      this.title = 'EMAIL';
      this.showTemplateFor = 'email';
    }
    else if (this.getDataForAddEdit[0] == 3) {
      this.title = 'APP';
      this.showTemplateFor = 'appNotification';
    }
    // if(this.getDataForAddEdit[0]==2){
    //   this.htmlEditor = true;
    // }else{
    //   this.htmlEditor=false;
    // }

    if (this.getDataForAddEdit[2] == "ADD") {
      this.addAction = true;
      this.notTemp.tempId = '',
        this.notTemp.name = '',
        this.notTemp.desc = '',
        this.notTemp.subject = '',
        this.notTemp.content = '',
        this.notTemp.neId = this.getDataForAddEdit[3],
        this.notTemp.nmId = this.getDataForAddEdit[0],
        this.notTemp.aId = this.getDataForAddEdit[4]

    } else if (this.getDataForAddEdit[2] == "EDIT") {
      this.addAction = false;
      this.notTemp.tempId = this.getDataForAddEdit[1].tId,
        this.notTemp.name = this.getDataForAddEdit[1].tname,
        this.notTemp.desc = this.getDataForAddEdit[1].desc,
        this.notTemp.subject = this.getDataForAddEdit[1].subject,
        this.notTemp.content = this.getDataForAddEdit[1].template,
        this.notTemp.neId = this.getDataForAddEdit[3],
        this.notTemp.nmId = this.getDataForAddEdit[0],
        this.notTemp.aId = this.getDataForAddEdit[4]
    }

    console.log('this.notTemp', this.notTemp);
  }

  header: SuubHeader  = {
    title:   '',
    btnsSearch: true,
    searchBar: false,
    dropdownlabel: ' ',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: 'Save',
    btnName2: '',
    btnName3: '',
    btnAdd: '',
    btnName1show: true,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: false,
    filter: false,
    showBreadcrumb: true,
    breadCrumbList:[]   
  };

  ngOnInit() {
    if (this.getDataForAddEdit.length != 0) {
      this.emailTemplateName = this.getDataForAddEdit[1].eName;
    }
    this.header.title='Edit ' + this.notTemp.name ;
    this.header.breadCrumbList=[
      {   'name': 'Settings',
      'navigationPath': '/pages/plan',
    },
      {
      'name': 'Notificatiion Template',
      'navigationPath': '/pages/plan/notification-templates',
      }]

  }

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }

  back() {
    // this.notTemp = {};
    // this.htmlEditor=false;
    // this.addeditnottempservice.getdataEdit = [];
    this.router.navigate(['/pages/plan/notification-templates']);
  }

  submit(data) {
    this.spinner.show();
    let param = {
      "id": this.addAction ? 0 : this.notTemp.tempId,
      "tname": data.name,
      "tdescription": data.desc,
      "subject": data.subject ? data.subject : null,
      "template": data.content,
      "avPlaceHolders": null,
      "notModeId": this.notTemp.nmId,
      "notEventId": this.notTemp.neId,
      "areaId": this.notTemp.aId,
      "visible": 1,
      "tid": this.tenantId
    }

    console.log('param', param)

    this.addeditnottempservice.addEditNotTemplate(param)
      .then(rescompData => {
        console.log('AddEditNotRes:', rescompData);
        this.spinner.hide();
        var res = rescompData;
        var result = res['data'];
        if (this.getDataForAddEdit[0] == "ADD") {
          if (res['type'] == true) {
            //   var catUpdate : Toast = {
            //     type: 'success',
            //     title: "Notification Template Inserted!",
            //     body: "New template added successfully.",
            //     showCloseButton: true,
            //     timeout: 2000
            // };
            // this.toasterService.pop(catUpdate);
            this.presentToast('success', 'Notification template added');
            window.history.back();
            // this.router.navigate(['/pages/plan/notification-templates']);
          } else {

            //   var catUpdate : Toast = {
            //     type: 'error',
            //     title: "Notification Template Inserted!",
            //     body: "Unable to add template.",
            //     showCloseButton: true,
            //     timeout: 2000
            // };
            // this.toasterService.pop(catUpdate);
            this.presentToast('error', '');
          }
        } else {
          if (res['type'] == true) {

            //   var catUpdate : Toast = {
            //     type: 'success',
            //     title: "Notification Template Updated!",
            //     body: "Template updated successfully.",
            //     showCloseButton: true,
            //     timeout: 2000
            // };
            // this.toasterService.pop(catUpdate);
            this.presentToast('success', 'Notification template updated');
            window.history.back();
            // this.router.navigate(['/pages/plan/notification-templates']);
          } else {
            //   var catUpdate : Toast = {
            //     type: 'error',
            //     title: "Notification Template Updated!",
            //     body: "Unable to update template.",
            //     showCloseButton: true,
            //     timeout: 2000
            // };
            // this.toasterService.pop(catUpdate);
            this.presentToast('error', '');
          }
        }
      }, error => {
        this.spinner.hide();
        console.log('AddEditNotResErr:', error);

        // var toast: Toast = {
        //   type: 'error',
        //   body: "Something went wrong.please try again later.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
  }

  // Help Code Start Here //

  helpContent: any;
  getHelpContent() {
    return new Promise(resolve => {
      this.http1
        .get('../../../../../../assets/help-content/addEditCourseContent.json')
        .subscribe(
          data => {
            this.helpContent = data;
            console.log('Help Array', this.helpContent);
          },
          err => {
            resolve('err');
          }
        );
    });
    // return this.helpContent;
  }

  // Help Code Ends Here //


}

