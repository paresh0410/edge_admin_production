import { TestBed } from '@angular/core/testing';

import { OpenApiServiceService } from './open-api-service.service';

describe('OpenApiServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OpenApiServiceService = TestBed.get(OpenApiServiceService);
    expect(service).toBeTruthy();
  });
});
