import { Injectable } from '@angular/core';
import { webApi } from '../../../service/webApi';
import { HttpClient } from '@angular/common/http';
import { RequestOptions, Headers } from '@angular/http';
import { AuthenticationService } from '../../../service/authentication.service';
import {Http} from '@angular/http';
@Injectable({
  providedIn: 'root'
})
export class OpenApiServiceService {
  private fetch_dropdown = webApi.domain + webApi.url.fetchDropdown_List;
  private fetchGroup_List = webApi.domain + webApi.url.fetchGroupList;
  private fetchGroup_Detail = webApi.domain + webApi.url.fetchGroupDetail;
  private add_editgroup =  webApi.domain + webApi.url.add_editgroup;
  private updateVisibility = webApi.domain + webApi.url.updateVisibility;
  private updateUniqueidentifier = webApi.domain + webApi.url.update_unqiue_system_identifier;
  private add_unique_identifier =  webApi.domain + webApi.url.inset_unqiue_system_identifier;
  private getUniqueidentifier = webApi.domain + webApi.url.get_unique_system_identifier;

  //theme
  private getAllTheme = webApi.domain + webApi.url.getAllThemeData;
  private setTheme = webApi.domain + webApi.url.setThemedata;
  private addEditTheme = webApi.domain + webApi.url.addEditThemeData;
  private changeImage = webApi.domain + webApi.url.changeImage;
  


  


  constructor(private http: HttpClient, private http1 : Http, private authenticationService: AuthenticationService) { }

  fetchDropdown(){
    return new Promise(resolve => {
      this.http.post(this.fetch_dropdown,null)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }

  fetchGroupDetail(id){
    return new Promise(resolve => {
      this.http.post(this.fetchGroup_Detail,id)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }

  getPager(totalItems: number, currentPage: number = 1, pageSize: number) {
		// calculate total pages
		let totalPages = Math.ceil(totalItems / pageSize);

		// ensure current page isn't out of range
		if (currentPage < 1) {
			currentPage = 1;
		} else if (currentPage > totalPages) {
			currentPage = totalPages;
		}

		let startPage: number, endPage: number;
		if (totalPages <= 10) {
			// less than 10 total pages so show all
			startPage = 1;
			endPage = totalPages;
		} else {
			// more than 10 total pages so calculate start and end pages
			if (currentPage <= 6) {
				startPage = 1;
				endPage = 10;
			} else if (currentPage + 4 >= totalPages) {
				startPage = totalPages - 9;
				endPage = totalPages;
			} else {
				startPage = currentPage - 5;
				endPage = currentPage + 4;
			}
    }
  }


  fetchGroupList(){
    return new Promise(resolve => {
      this.http.post(this.fetchGroup_List,null)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }

  addeditgroup(a){
    return new Promise(resolve => {
      this.http.post(this.add_editgroup,a)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }

  updateVisibilityGroup(data){
    return new Promise(resolve => {
      this.http.post(this.updateVisibility,data)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }


  getUniqueidentifiers(params){
    return new Promise(resolve => {
      this.http.post(this.getUniqueidentifier, params)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }

  updateIdentifier(params){
    return new Promise(resolve => {
      this.http.post(this.updateUniqueidentifier,params)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }

  getAllThemeData(){
    return new Promise(resolve => {
      this.http.post(this.getAllTheme,null)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }

  setThemeData(param){
    return new Promise(resolve => {
      this.http.post(this.setTheme,param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }

  addEditThemeData(param){
    return new Promise(resolve => {
      this.http.post(this.addEditTheme,param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }
  changeImageData(param: FormData){

    
    let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    // let headers = new Headers({ 'Authorization': 'Bearer ' + 'myToken' });
    let options : any = new RequestOptions({ headers: headers });
    return new Promise(resolve => {
      this.http1.post(this.changeImage, param, options)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }


  AddUniqueIdentifier(params){
    return new Promise(resolve => {
      this.http.post(this.add_unique_identifier, params)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }

}





