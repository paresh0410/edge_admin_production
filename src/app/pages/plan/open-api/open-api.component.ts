import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap';
import { numberFilterCondition } from 'angular-slickgrid/app/modules/angular-slickgrid/filter-conditions/numberFilterCondition';
import { CourseConsumptionReportModule } from '../../reports/course-consumption-report/course-cosumpiton-report.module';
import { OpenApiServiceService } from './open-api-service.service';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { PassService } from '../../../service/passService';
//import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { ToastrService } from 'ngx-toastr';
import { VALID } from '@angular/forms/src/model';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { webApi } from '../../../service/webApi';
import { SuubHeader } from '../../components/models/subheader.model';
import { noData } from '../../../models/no-data.model';
import { ThemeService } from 'ng2-charts';
// import { visible } from '../../../entity/lovmaster.enum';
@Component({
  selector: 'ngx-open-api',
  templateUrl: './open-api.component.html',
  styleUrls: ['./open-api.component.scss']
})
export class OpenApiComponent implements OnInit {
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No API's available at this time.",
    desc:"Admins will be able to add API's to the organisation.",
    titleShow:true,
    btnShow:true,
    descShow:true,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/open-api-group',
  }
  noDataValue:noData={
    margin:'mt-2',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No user added.",
    desc:"",
    titleShow:true,
    btnShow:true,
    descShow:false,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/open-api-user',
  }
  header: SuubHeader  = {
    title:'Open Api',
    btnsSearch: true,
    placeHolder:'Search',
    searchBar: true,
    dropdownlabel: ' ',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: 'Add',
    btnName3: '',
    btnAdd: 'Add User',
    btnName1show: false,
    btnName2show: true,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: true,
    filter: false,
    showBreadcrumb: true,
    breadCrumbList:[]  
  };
  dummy = [{
    id:1,
    groupName:'G1',
    bands:'Eo1,Eo2',
    courses:'c1,c2',
    workflow:'',
    classRooms:'cr1,cr2'
  }]

  visible = [{id:1,name:'Show'}, {id:0,name:'Hide'}]

  visiblityArray = [{id:1,name:'Show'}, {id:0,name:'Hide'}];

  bands = [];
  btnName:string='Save';
title:string='Add User'
  courses = []
  workFlow = []

  classrooms = []

  selectedName  = [];
  selectedItems =[];
  selectCourses = [];
  selectedWork    =[];
  selectedClass   = [];
  identifier = [];
  band = [];
  course =[];
  work = [];
  class = [];

  settings0 = {
    text: 'System Identifier',
    singleSelection: true,
    classes: 'common-multi',
    primaryKey: 'itemId',
    labelKey: 'itemName',
    noDataLabel: 'Search Users...',
    enableSearchFilter: true,
    //maxHeight:150 ,
    searchBy: ['itemName'],
    lazyLoading: true,
    badgeShowLimit: 3,
    maxHeight:250,
  };
  labels: any = [
		{ labelname: 'System Identifier', bindingProperty: 'username', componentType: 'text' },
    { labelname: 'Bands', bindingProperty: 'bands', componentType: 'text' },
    { labelname: 'Courses', bindingProperty: 'courses', componentType: 'text' },
    { labelname: 'Workflow', bindingProperty: 'workflows', componentType: 'text' },
    { labelname: 'Classrooms', bindingProperty: 'classrooms', componentType: 'text' },
		{ labelname: 'Visibility', bindingProperty: 'btntext', componentType: 'button' },
    { labelname: 'EDIT', bindingProperty: 'tenantId', componentType: 'icon' },
  ]
  labelsUser: any = [
		{ labelname: 'Username', bindingProperty: 'username', componentType: 'text' },
    { labelname: 'Password', bindingProperty: 'password', componentType: 'fulltext' },
		{ labelname: 'Visibility', bindingProperty: 'btntext', componentType: 'button' },
  ]
  settings = {
    text: 'Select Band(s)',
    singleSelection: false,
    classes: 'common-multi',
    primaryKey: 'itemId',
    labelKey: 'itemName',
    noDataLabel: 'Search Users...',
    enableSearchFilter: true,
    //maxHeight:150 ,
    searchBy: ['itemName'],
    lazyLoading: true,
    badgeShowLimit: 3,
    maxHeight:250,
  };

 settings1 = {
  text: 'Select Course(s)',
  singleSelection: false,
  classes: 'common-multi',
  primaryKey: 'itemId',
  labelKey: 'itemName',
  noDataLabel: 'Search Users...',
  enableSearchFilter: true,
  //maxHeight:150 ,
  searchBy: ['itemName'],
  lazyLoading: true,
  badgeShowLimit: 3,
  maxHeight:250,
};

settings2 = {
  text: 'Select Workflow(s)',
  singleSelection: false,
  classes: 'common-multi',
  primaryKey: 'itemId',
  labelKey: 'itemName',
  noDataLabel: 'Search Users...',
  enableSearchFilter: true,
  //maxHeight:150 ,
  searchBy: ['itemName'],
  lazyLoading: true,
  badgeShowLimit: 3,
  maxHeight:250,
};

settings3 = {
  text: 'Select Classroom(s)',
  singleSelection: false,
  classes: 'common-multi',
  primaryKey: 'itemId',
  labelKey: 'itemName',
  noDataLabel: 'Search Users...',
  enableSearchFilter: true,
  //maxHeight:150 ,
  searchBy: ['itemName'],
  lazyLoading: true,
  badgeShowLimit: 3,
  maxHeight:250,
};
  myForm:FormGroup;

  @ViewChild('modal') public modal: ModalDirective;
  isSave: boolean = false;
  formValue: any;
  finalvalue = [];
  id = 0;
  visibility;
  Add: any;
  edit: any;
  editItem: any;
  editIndex: any;
  temData: any;
  pager: any;
  pageSize: number = 10;
  sectionshow: boolean  = false;
  valuevisible: any;
  displayTableData: any;
  tempData: any[];
  popUpTitle: string = '';
  showAddUserModel: boolean = false;
  addUserModel = {
    name: '',
    visible: 1,
  };
  systemIdentifiers = [];
  nodata: boolean=false;

  constructor(private router:Router,private spinner: NgxSpinnerService,
    private commonFunctionService: CommonFunctionsService,
    private toastr: ToastrService, private pagservice: PassService,
    private fb:FormBuilder,private openService:OpenApiServiceService,
    private location: Location) { }

  ngOnInit() {
        this.spinner.show()
    this.header.breadCrumbList=[{
      'name': 'Settings',
      'navigationPath': '/pages/plan',
      }]
    //this.setpage(1)

    this.fetchDropdown()


    //this.fetchGroupDetail()
    this.fetchGroupList()
    //this.visibility = this.visible[0]
    console.log(this.visible,"visible")
    console.log(this.visibility,"this.visibility ")

    console.log(this.finalvalue,"this.finalvalue")

    this.myForm  = this.fb.group({
      // name: new FormControl('', [
      //   Validators.required,
      //   Validators.minLength(2),
      //   Validators.maxLength(50)

      // ]),
      bands: new FormControl('', [
        Validators.required,
        Validators.minLength(1)


      ]),
      courses: new FormControl('', [
        Validators.required,
        Validators.minLength(1)
        ]),
      workFlow: new FormControl('', [
         Validators.required,
         Validators.minLength(1)

      ]),
      classRooms: new FormControl('', [
         Validators.required,
         Validators.minLength(1)

      ]),
      visible: new FormControl('',[
        Validators.required
      ]),
      systemId: new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(50)


      ]),
    });




    // this.myForm.valueChanges.subscribe(value => {
    //   console.log(this.myForm.get('courses').valid, "form course", );
    //   console.log(this.myForm.get('workFlow').valid, "form workFlow", );
    //   console.log(this.myForm.get('classRooms').valid, "form classRooms" ) ;
    //   console.log(value, "form values", )
    //   if((this.myForm.get('courses').valid) || (this.myForm.get('workFlow').valid) || (this.myForm.get('classRooms').valid))
    //     {

    //     // this.myForm.get('workFlow').setErrors(null)
    //     // this.myForm.get('classRooms').setErrors(null)
    //     // this.myForm.get('courses').setErrors(null)
    //     }





        // this.myForm.get('courses').valueChanges.subscribe(val=>{
        //   if(this.myForm.get('courses').valid ) {
        //     this.myForm.get('workFlow').setErrors(null)
        //     this.myForm.get('classRooms').setErrors(null)
        //     this.myForm.get('courses').setErrors(null)

        //   }

        // })

        // this.myForm.get('classRooms').valueChanges.subscribe(val=>{
        //   if(this.myForm.get('classRooms').valid) {
        //     this.myForm.get('workFlow').setErrors(null)
        //     this.myForm.get('classRooms').setErrors(null)
        //     this.myForm.get('courses').setErrors(null)

        //   }

        // })


        // this.myForm.get('workFlow').valueChanges.subscribe(val=>{
        //   if(this.myForm.get('workFlow').valid) {
        //     this.myForm.get('workFlow').setErrors(null)
        //     this.myForm.get('classRooms').setErrors(null)
        //     this.myForm.get('courses').setErrors(null)

        //   }



        // })

        // this.myForm.valueChanges.subscribe(val=>{
        //   if(this.myForm.get('workFlow').value || this.myForm.get('classRoom').value || this.myForm.get('classRoom').value){

        //   }
        // })

        // if((this.myForm.get('courses').value.length!=0) || (this.myForm.get('workFlow').value.length!=0) || (this.myForm.get('classRooms').value.length!=0))
        // {
        // else{
        // this.myForm.get('workFlow').setErrors({required:true})
        // this.myForm.get('classRooms').setErrors({required:true})
        // this.myForm.get('courses').setErrors({required:true})
        // }
     // }
    //})



    // if(this.myForm.valid){
    //   this.isSave = true
    // console.log("success")
    // }
    // else {
    //   this.isSave = false;
    // }


//     this.myForm.valueChanges.subscribe(value => {
//       if((this.myForm.get('name')) && (this.myForm.get('bands')!="") && (this.myForm.get('courses')
//      || this.myForm.get('workflow') || this.myForm.get('classRooms')))
//      this.isSave = true;
//       console.log(value,"subscribe value");

// });

    this.formValue = this.myForm.value



  }


  ngAfterViewInit(){
    // this.myForm.valueChanges.subscribe(value => {
    //   console.log(this.myForm.get('courses').valid, "courses", );
    //   console.log(this.myForm.get('workFlow').valid, "workFlow", );
    //   console.log(this.myForm.get('classRooms').valid, "classRooms" ) ;
    //   console.log(value, "form values", )
    //   if((this.myForm.get('courses').value) )
    //     {
    //      if((this.myForm.get('courses').value.length!=0))
    //      {
    //     this.myForm.get('workFlow').setErrors(null)
    //     this.myForm.get('classRooms').setErrors(null)
    //     this.myForm.get('courses').setErrors(null)
    //     }


    //     else{
    //     this.myForm.get('workFlow').setErrors({required:true})
    //     this.myForm.get('classRooms').setErrors({required:true})
    //     this.myForm.get('courses').setErrors({required:true})
    //     }
    //   }


    //   if (this.myForm.get('workFlow').value){
    //     if((this.myForm.get('workFlow').value.length!=0)){
    //       this.myForm.get('workFlow').setErrors(null)
    //     this.myForm.get('classRooms').setErrors(null)
    //     this.myForm.get('courses').setErrors(null)

    //     }

    //     else{
    //       this.myForm.get('workFlow').setErrors({required:true})
    //       this.myForm.get('classRooms').setErrors({required:true})
    //       this.myForm.get('courses').setErrors({required:true})
    //       }
    //     }


    //     if( (this.myForm.get('classRooms').value)){
    //       if((this.myForm.get('classRooms').value.length!=0)){
    //         this.myForm.get('workFlow').setErrors(null)
    //       this.myForm.get('classRooms').setErrors(null)
    //       this.myForm.get('courses').setErrors(null)

    //       }

    //       else{
    //         this.myForm.get('classRooms').setErrors({required:true})
    //         this.myForm.get('workFlow').setErrors({required:true})
    //         this.myForm.get('courses').setErrors({required:true})
    //         }
    //       }
    // })


      }

  presentToast(type, body) {
		if(type === 'success'){
		  this.toastr.success(body, 'Success', {
			closeButton: false
		   });
		} else if(type === 'error'){
		  this.toastr.error('Please contact site administrator ', 'Error', {
			timeOut: 0,
			closeButton: true
		  });
		} else{
		  this.toastr.warning(body, 'Warning', {
			closeButton: false
			})
		}
	  }


  add(add){
    this.spinner.show();
    this.sectionshow = true;
    //this.spinner.hide()

    //this.modal.show();
    this.popUpTitle= 'Add Group';
    this.Add = add
    this.fetchDropdown()
    //this.setpage(1)
  }

  saveform(){
    const values = this.myForm.value
    console.log(values,"form values")

    if(this.myForm.get('systemId').valid && this.myForm.get('bands').valid){
      this.spinner.show();
      console.log(this.Add,"Add")
      if(this.Add =='add'){
        console.log(this.Add,"add enters")
    const values = this.myForm.value
    console.log(values,"form values")
    var bands = values.bands
    var band = this.extractBand(bands)


    if(values.courses){
     var courses = values.courses
     var course = this.extractCourse(courses)
    }
    else{
       course = values.courses
    }
    if(values.workFlow){
     var works = values.workFlow
     var work = this.extractWork(works)
    }
    else{
      work = values.courses
   }
    if(values.classRooms){
     var classes = values.classRooms
     var clas =   this.extractClass(classes)
  }
  else{
    clas = values.courses
 }

     var a = {}
     a['groupId'] = this.id
     a['groupName'] = values.name
     a['bands'] = band
     a['courses'] = course
     a['workflows'] = work
     a['classrooms'] = clas
     a['visibility'] = values.visible
     a['sId'] = values.systemId[0].itemId
     //a['tenantId'] = 1
     this.finalvalue.push(a)
     console.log(this.finalvalue,"finalvalue")
     const add_editgroup =  webApi.domain + webApi.url.add_editgroup;
      // this.spinner.show()
      this.commonFunctionService.httpPostRequest(add_editgroup,a).then(res=>{
        this.fetchGroupList()
        console.log(res,"addGroupDetail")

        this.presentToast('success', 'Group Added Successfully');
        this.spinner.hide()
       // this.fetchGroupList();
        //this.setpage(1)
      }, err => {
        this.presentToast('error', '');
        this.spinner.hide()
        console.log(err);
      })

      console.log(" enters")
      //this.myForm.markAsTouched();
      // this.form.markAsDirty(); <-- this can be useful

    // if(this.Add =='add'){
    //  this.id = ++this.id;
    //  console.log(this.id,"this.id")
    //  var  value = this.myForm.value
    //  console.log(value,"value")
    //  value.id = this.id

    //  //console.log(a['bands'],"value");
    //  var bands = value.bands
    //  var band = this.extractBand(bands)
    //  console.log(band,"b")


    //  var courses = value.courses
    //  var course = this.extractCourse(courses)

    //  var works = value.workFlow
    //  var work = this.extractWork(works)

    //  var classes = value.classRooms
    //  var clas =   this.extractClass(classes)

    //  var a = {}
    //  a['id'] = this.id
    //  a['name'] = value.name
    //  a['Bands'] = band
    //  a['Courses'] = course
    //  a['WorkFlow'] = work
    //  a['ClassRooms'] = clas
    //  this.finalvalue.push(a)
    //  console.log(this.finalvalue,"finalvalue")
     this.myForm.reset()
     this.sectionshow = false
    }
    else{
      this.editSave(this.editItem,this.editIndex)
    }}
    else{
      Object.values(this.myForm.controls).forEach(control=>{
        console.log(control,"invalid")
        control.markAsDirty();
        control.markAsTouched();

        //console.log(control,"control")
      })
    }


  }
   onItemSelect(item){
     this.band = item.name
    // this.band.push(item.name)
     //console.log(this.band,"band")
  //   console.log(item,"item")

   }
   onSelectAll(event){

   }
   onDeSelectAll(event){

   }


   extractBand(data){

     var b  = '';
     let array = [];
     //const value =this.myForm.value
     data.forEach(element => {

      // b += element.name;
       array.push(element.itemId)


     });
     b = array.join(',')
     array = b.split(',')
     console.log(array)
     return array

   }

   extractCourse(data){
    var b = "";
     let array = [];
     //const value =this.myForm.value
     data.forEach(element => {

      // b += element.name;
       array.push(element.itemId)


     });
      b =array.join(',')
      array = b.split(',')
     console.log(array)
     return array




  }

  extractWork(data){
    var b = "";
     let array = [];
     //const value =this.myForm.value
     data.forEach(element => {

      // b += element.name;
       array.push(element.itemId)


     });
      b =array.join(',')

      array = b.split(',')
      console.log(array)
      return array

    }

  extractClass(data){
    var b = "";
     let array = [];
     //const value =this.myForm.value
     data.forEach(element => {

      // b += element.name;
       array.push(element.itemId)


     });
      b =array.join(',')

      array = b.split(',')
      console.log(array)
      return array


  }

  showTemplate(item){
    console.log(item);
    this.popUpTitle = 'Edit Group';
    var id ={
      'groupId' : item.id
    }
    this.sectionshow = true;
    this.spinner.show()
    //this.modal.show()
  this.fetchGroupDetail(id)

  console.log(item,"item")
  // this.selectedName = item.name;
  // var band = (item.Bands).split(",");
  // var course = (item.Courses).split(",");
  // var work = (item.WorkFlow).split(",");
  // var clas = (item.ClassRooms).split(",");

  // var item4 = this.selectBand(band)
  // //this.selectedItems = item4

  // var item1 = this.selectCourse(course)
  // this.selectCourses = item1

  // var item2 = this.selectwork(work)
  // this.selectWork = item2

  // var item3 = this.selectclass(clas)
  // console.log(item3,"item")
  // this.selectedClass = item3
  // this.add('edit')
  // this.Add = edit
  this.editItem = item
  // this.editIndex = i
  }

  selectBand(a){
    let d = []
    this.bands.forEach((elem1, index) => {elem1;
      a.forEach((elem2, index) => {elem2;
        if(elem1.name=== elem2)
        d.push(elem1)
         //--If elem1 equal elem2

      });
    });
    return d

  }

  selectCourse(a){
    let d = []
    this.courses.forEach((elem1, index) => {elem1;
      a.forEach((elem2, index) => {elem2;
        if(elem1.name=== elem2)
        d.push(elem1)
         //--If elem1 equal elem2

      });
    });
    return d

  }

  selectwork(a){
    let d  = []
    this.workFlow.forEach((elem1, index) => {elem1;
      a.forEach((elem2, index) => {elem2;
        if(elem1.name=== elem2)
        d.push(elem1)
         //--If elem1 equal elem2

      });
    });
    return d

  }

  selectclass(a){
    let d = []
    this.classrooms.forEach((elem1, index) => {elem1;
      a.forEach((elem2, index) => {elem2;
        if(elem1.name===elem2)
        d.push(elem1)
         //--If elem1 equal elem2

      });
    });
    return d

  }

  editSave(item,i){

    console.log(item,"edit Item")
    console.log("edit enters")
   // var d=  this.finalvalue.find(ele=>ele.id == item.id)

   // this.id = ++this.id;
     console.log(this.id,"this.id")
     var  value = this.myForm.value
     console.log(value,"value")
     //value.id = this.id

     //console.log(a['bands'],"value");
     var bands = value.bands
      var band = this.extractBand(bands)
     console.log(band,"b")


     var courses = value.courses
     var course = this.extractCourse(courses)

     var works = value.workFlow
     var work = this.extractWork(works)

     var classes = value.classRooms
     var clas =   this.extractClass(classes)

     var a = {}
     a['groupId'] = item.id
     a['groupName'] = value.name
     a['bands'] = band
     a['courses'] = course
     a['workflows'] = work
     a['classrooms'] = clas
     a['visibility'] = value.visible
     a['sId'] = item.systemIdentifierId
     //a['tenantId'] = 1
     const add_editgroup =  webApi.domain + webApi.url.add_editgroup;
     this.commonFunctionService.httpPostRequest(add_editgroup,a).then(res=>{
       this.fetchGroupList()
       this.spinner.hide()
      this.presentToast('success', 'Group Updated Successfully');
     // this.fetchGroupList();
      //this.setpage(1)
    }, err => {
      this.presentToast('error', '');
      this.spinner.hide()
      console.log(err);
    });


//this.finalvalue.insert(i,a)
     //console.log(this.finalvalue[d],"finalvalue")
     this.myForm.reset()
     this.sectionshow = false


  }


  fetchDropdown(){
    const  fetch_dropdown = webApi.domain + webApi.url.fetchDropdown_List;
    this.commonFunctionService.httpPostRequest(fetch_dropdown,'').then(res=>{
      this.spinner.hide()
      console.log(res,"dropdown")
      if(res['data']['bands']){
      this.bands = res['data']['bands']
      var band =[]
      band.push(this.bands[1])
        // this.selectedItems = band
      //console.log(this.selectedItems,"seelcted Ietms")
      //console.log(this.bands[1],"selected")
      }
      if(res['data']['courses']){
      this.courses = res['data']['courses'];
      var course =[];
      course.push(this.courses[0]);
      }

     // this.selectCourses = course
     if(res['data']['workflows']){
      this.workFlow = res['data']['workflows'];
      var work =[];
      work.push(this.workFlow[1]);
     }
      if(res['data']['classrooms']){
      this.classrooms = res['data']['classrooms'];
      var clas =[];
      clas.push(this.classrooms[1]);

      }

      if(res['data']['systemIdentifiers']){
        this.systemIdentifiers = res['data']['systemIdentifiers'];
        var clas =[];
        clas.push(this.classrooms[1]);

        }

     // this.selectedWork = work
           // this.selectedClass = clas
      this.visibility = this.visible[0].id;



    },
    err => {
      this.presentToast('error', '');
      this.spinner.hide()
      console.log(err);
    });
  }

  back(){
    this.location.back()
  }

  fetchGroupList(){
    this.spinner.show();
    const fetchGroup_List = webApi.domain + webApi.url.fetchGroupList;
    this.commonFunctionService.httpPostRequest(fetchGroup_List,null).then(res=>{
      this.spinner.hide()
      console.log(res,"List")

      this.finalvalue = res['data']['list']
      console.log('this.final value ++++++++++++++++++++++++++++++++++++++++>>',  this.finalvalue );
      
      this.tempData = this.finalvalue
      this.temData = this.finalvalue
     // console.log(this.temData,"length")
      // this.setpage(1)
      for (let i = 0; i < this.finalvalue.length; i++) {
        if(this.finalvalue[i].status == 1) {
          this.finalvalue[i].btntext = 'fa fa-eye';
        } else {
          this.finalvalue[i].btntext = 'fa fa-eye-slash';
        }
      }
      if(this.finalvalue.length==0){
        this.nodata=true
      }else{
        this.nodata=false
      }
    })
  }

  fetchGroupDetail(id){
    const fetchGroup_Detail = webApi.domain + webApi.url.fetchGroupDetail;
    this.commonFunctionService.httpPostRequest(fetchGroup_Detail,id).then(res=>{
      this.spinner.hide()
      console.log(res,"detail")
      //this.selectedName = res['data']['group']['name']
      console.log(this.selectedName,"selectedName")
      this.selectedItems = res['data']['group']['band']

      console.log(this.selectedItems,"selectedItems")
      this.selectCourses = res['data']['group']['course']
      console.log(this.selectCourses,"selectCourses")
      this.selectedWork = res['data']['group']['workflow']
      this.selectedClass = res['data']['group']['classroom']
      this.visibility = res['data']['group']['visibility']
      this.identifier = res['data']['group']['systemIdentifiers']
    },
    err => {
      this.presentToast('error', '');
      this.spinner.hide()
      console.log(err);
    });
  }

  cancel(){
    this.sectionshow = false
    this.myForm.reset()
    //this.modal.hide()
  }

  clear(){
    // this.searchQuestion(event)
    this.nodata=false
    this.finalvalue = this.temData
  }

  searchQuestion(event) {
    var val = event.target.value.toLowerCase();
    var keys = [];
    let data = '';
    // filter our data
    if (this.temData.length > 0) {
      keys = Object.keys(this.temData[0]);
    }
    if(val.length>=3|| val.length==0){
    var temp = this.temData.filter((d) => {
      // keys.forEach(element => {

      // });
      for (const key of keys) {
        if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
          return true;
        }
      }
    });
    if(temp.length==0){
      this.nodata=true;
      this.noDataVal={
        margin:'mt-5',
        imageSrc: '../../../../../assets/images/no-data-bg.svg',
        title:"Sorry we couldn't find any matches please try again",
        desc:".",
        titleShow:true,
        btnShow:false,
        descShow:false,
        btnText:'Learn More',
        btnLink:''
      }
    }
    // update the rows
    //this.setpage(1)
    this.finalvalue = temp;
    //this.setpage(1)
    console.log(this.finalvalue,"displayTableData")
  }
  }

  setpage(page) {
    //this.nodata = false;
    this.pager = this.pagservice.getPager(this.temData.length, page, this.pageSize);
    //console.log(this.pager.pages,"THIS.PAGER");
    this.dispalyData()
  }

  dispalyData() {
    this.displayTableData = this.temData.slice(this.pager.startIndex, this.pager.endIndex + 1);
    console.log(this.displayTableData,"this.displayTable")
    this.finalvalue = this.displayTableData

  }
      // this.displayTableData[index]['Download'] = '';
      visibilityTableGroup(row) {
        let value;
        let status;
    
        if (row.status == 1) {
          row.btntext = 'fa fa-eye-slash';
          value  = 'fa fa-eye-slash';
          row.status = 0
          status = 0;
        } else {
          status = 1;
          value  = 'fa fa-eye';
          row.status = 1;
          row.btntext = 'fa fa-eye';
        }
    
        for(let i =0; i < this.finalvalue.length; i++) {
          if(this.finalvalue[i].id == row.id) {
            this.finalvalue[i].btntext = row.btntext;
            this.finalvalue[i].status = row.status
          }
        }
        // this.spinner.show()
        this.valuevisible = row
        console.log(row,"visible")
    
        console.log(row, "visible")
        var data = {}
        data = {
          groupId: row.id,
          visibility: row.status,
          tenantId: row.tenantId
        }
        const updateVisibility = webApi.domain + webApi.url.updateVisibility;
    
        this.commonFunctionService.httpPostRequest(updateVisibility,data).then(res => {
          this.spinner.hide()
   
          if(row.status==0){
            this.commonFunctionService.presentToast(200,'Group Disabled Successfully','Success');
            }else {
              this.commonFunctionService.presentToast(200, 'Group Enabled Successfully','Success');
            }
          console.log(res, "res")
    
        },
        err => {
          this.presentToast('error', '');
          // this.spinner.hide()
          console.log(err);
        });
    
    
        // this.updated(row);
      }

  // updateVisibilityGroup(item, visible) {

  //   this.spinner.show()
  //   this.valuevisible = visible
  //   console.log(visible,"visible")

  //   console.log(item, "visible")
  //   var data = {}
  //   data = {
  //     groupId: item.id,
  //     visibility: item.status,
  //     tenantId: item.tenantId
  //   }
  //   const updateVisibility = webApi.domain + webApi.url.updateVisibility;

  //   this.commonFunctionService.httpPostRequest(updateVisibility,data).then(res => {
  //     this.spinner.hide()
  //     if(visible == 0){
  //       this.commonFunctionService.presentToast(200,'success', 'Group Disabled Successfully');
  //       }else{
  //         this.commonFunctionService.presentToast(200,'success', 'Group Enabled Successfully');
  //       }
  //     console.log(res, "res")

  //   },
  //   err => {
  //     this.presentToast('error', '');
  //     this.spinner.hide()
  //     console.log(err);
  //   });


  //   this.updated(item)
  // }


  updated(item){
    const fetchGroup_List = webApi.domain + webApi.url.fetchGroupList;
    this.commonFunctionService.httpPostRequest(fetchGroup_List,'').then(ele=>{
      this.finalvalue.forEach(ele1=>{
      if(ele1.id == item.id){
      console.log("enters if")

      ele1.status = this.valuevisible
      this.fetchGroupList();
     console.log(this.finalvalue,"updateVisibility")
      }
      
    })

  })

  }


  OnItemDeSelect(event){

  }

  addUser(event){
    if(event == 'add'){
      this.getUniqueIdentifierList();
      this.showAddUserModel = true;
      this.addUserModel.visible = 1;

    }
  }
  cancelUserModal(form){
    this.showAddUserModel = false;
    form.reset()
    this.addUserModel.visible = 1;
  }
  addUserForm(form) {
    if(form.valid) {
      const param = {
        username: this.addUserModel.name,
        visibility: this.addUserModel.visible,
      };
      this.spinner.show();
      const add_unique_identifier =  webApi.domain + webApi.url.inset_unqiue_system_identifier;
      this.commonFunctionService.httpPostRequest(add_unique_identifier,param).then(res => {
        this.spinner.hide();
        if(res && res['type']) {
          this.presentToast('success',res['message']);
          console.log("Added")

          this.getUniqueIdentifierList();
          form.reset();
          this.addUserModel.visible = 1;
          console.log("entered")
        } else {
          this.toastr.warning(res['message'],'success' );
        }
        console.log(res, "res")

      },
      err => {
        this.presentToast('error', '');
        this.spinner.hide();
        console.log(err);
      });

    }else {
      Object.keys(form.controls).forEach(key => {
        form.controls[key].markAsDirty();
      });
    }
    console.log('Add User Form ===>', form);
    form.reset()
    this.showAddUserModel =false
  }

  uniqueIdenfierList = [];
  noUniqueIdentifierFound = false;
  getUniqueIdentifierList(){
    this.spinner.hide()
    const getUniqueidentifier = webApi.domain + webApi.url.get_unique_system_identifier;
    this.commonFunctionService.httpPostRequest(getUniqueidentifier,{}).then(res => {
      // this.spinner.hide()
     

      if(res && res['data']){
        this.uniqueIdenfierList = res['data'];
        console.log('Data', this.uniqueIdenfierList);
         for (let i = 0; i < this.uniqueIdenfierList.length; i++) {
        if(this.uniqueIdenfierList[i].status == 1) {
          this.uniqueIdenfierList[i].btntext = 'fa fa-eye';
        } else {
          this.uniqueIdenfierList[i].btntext = 'fa fa-eye-slash';
        }
      }
      }
      else{
        this.noUniqueIdentifierFound = true;
      }
    },
    err => {
      // this.presentToast('error', '');
      this.noUniqueIdentifierFound = true;
      this.spinner.hide()
      console.log(err);
    });
  }
  visibilityTableUser(row) {
    let value;
    let status;

    if (row.status == 1) {
      row.btntext = 'fa fa-eye-slash';
      value  = 'fa fa-eye-slash';
      row.status = 0
      status = 0;
    } else {
      status = 1;
      value  = 'fa fa-eye';
      row.status = 1;
      row.btntext = 'fa fa-eye';
    }

    

    const  data = {
      id: row.id,
      visibility: row.status,
    }
    const updateUniqueidentifier = webApi.domain + webApi.url.update_unqiue_system_identifier;
    this.commonFunctionService.httpPostRequest(updateUniqueidentifier,data).then(res => {
      // this.getUniqueIdentifierList();
      
      if(res && res['type'] ){
        this.presentToast('success', res['data']);

      }

      console.log(res, "res");

    },
    err => {
      this.presentToast('error', '');
      this.spinner.hide();
      console.log(err);
    });
  

    console.log('row', row);
  
  }
  // updateVisibilityUser(item, visible){
  //   this.spinner.show();
  //   console.log(visible,"visible");

  //   console.log(item, "visible");

  // const  data = {
  //     id: item.id,
  //     visibility: visible,
  //   }
  //   const updateUniqueidentifier = webApi.domain + webApi.url.update_unqiue_system_identifier;
  //   this.commonFunctionService.httpPostRequest(updateUniqueidentifier,data).then(res => {
  //     this.getUniqueIdentifierList();
      
  //     if(res && res['type'] ){
  //       this.presentToast('success', res['data']);

  //     }

     
  //     console.log(res, "res");

  //   },
  //   err => {
  //     this.presentToast('error', '');
  //     this.spinner.hide();
  //     console.log(err);
  //   });

  // }

}
