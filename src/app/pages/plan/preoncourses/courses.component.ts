import { Component } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
//import { PaginatePipe, PaginationControlsDirective, PaginationService } from 'ng2-pagination';
import { Router, NavigationStart, Routes, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
// import { CoursesService } from './courses.service';
// import {SlimLoadingBarService} from 'ng2-slim-loading-bar';
//import 'easy-pie-chart/dist/jquery.easypiechart.js';
import {BaseChartDirective} from 'ng2-charts/ng2-charts';

@Component({
  selector: 'courses',
  styleUrls: ['./courses.scss'],
  templateUrl: './courses.html'
})
 export class pCourses {
    
  query: string = '';
  public getData;
  errorMsg:any;
  // protected service: CoursesService,
  constructor(private router:Router,private routes:ActivatedRoute) {

  }

  gotocategory(){
     this.router.navigate(['category'],{relativeTo:this.routes});
  }

  gotocontent(){
     this.router.navigate(['content'],{relativeTo:this.routes});
  }

  back(){
    this.router.navigate(['/pages/learning']);
  }
}

