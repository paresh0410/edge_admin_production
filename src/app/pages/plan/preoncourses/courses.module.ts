import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MyDatePickerModule } from 'mydatepicker';

import { pCourses } from './courses.component';
import { CoursesService } from './courses.service';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';


// import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
// import { Addeditcontent } from './addEditContent/addEditContent';
// import { addEditContentService } from './addEditContent/addEditContent.service';

// import { Addeditcontent } from './addEditContent/addEditContent';
// import { addEditContentService } from './addEditContent/addEditContent.service';
// import { HttpConfigInterceptor } from '../../../interceptor/interceptor';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    // TooltipModule,
    ],
  declarations: [
    pCourses,
    // Addeditcontent
  ],
  providers: [
    // { provide : HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi:true},
    CoursesService,
    // addEditContentService
  ]
})

export class pCoursesModule { }
