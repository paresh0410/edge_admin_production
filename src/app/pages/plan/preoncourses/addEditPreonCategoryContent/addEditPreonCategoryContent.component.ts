import { Component, Directive, ViewEncapsulation, forwardRef, Attribute, OnChanges, SimpleChanges, Input, ViewChild, ViewContainerRef, ChangeDetectorRef } from '@angular/core';
import { NG_VALIDATORS, Validator, Validators, AbstractControl, ValidatorFn } from '@angular/forms';
import { Router, NavigationStart, ActivatedRoute } from '@angular/router';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { ToastrService } from 'ngx-toastr';
import { AddEditPreonCategoryContentService } from  './addEditPreonCategoryContent.service';
import { HttpClient } from '@angular/common/http';
import { webAPIService } from '../../../../service/webAPIService'
import { webApi } from '../../../../service/webApi';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonFunctionsService } from '../../../../service/common-functions.service';
import { SuubHeader } from '../../../components/models/subheader.model';
import { PreonContentService } from '../preoncontent/preoncontent.service';
import { AddEditPreonCourseContentService } from '../addEditPreonCourseContent/addEditPreonCourseContent.service';
@Component({
  selector: 'addEditPreonCategoryContent',
  templateUrl: './addEditPreonCategoryContent.html',
  styleUrls: ['./addEditPreonCategoryContent.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class AddEditPreonCategoryContent {
  header: SuubHeader  = {
    title:'',
    btnsSearch: true,
    btnName1: 'Save',
    btnName1show: true,
    btnBackshow: true,
    showBreadcrumb: true,
    breadCrumbList:[
    //   {   'name': 'Learning',
    //   'navigationPath': '/pages/learning',
    // },
      {
        'name': 'Preonboarding Category',
        'navigationPath': '/pages/preonboarding/preoncourses/Preoncategory',
      },
    ]
  };

  query: string = '';
  public getData;

  settingsTagDrop ={};
  tempTags:any =[];
  tagList:any = [];
  selectedTags:any = [];
  title: any;
  formdata: any;
  defaultThumb: any = 'assets/images/category.jpg';
  result: any;
  errorMsg: any;

  visibility: any = []

  catData: any = undefined;

  loader: any = false;
   userData:any;
  tenantId:any;
  courseCount: any = 0;
  constructor(private spinner: NgxSpinnerService,private commonFunctionService: CommonFunctionsService, protected webApiService: webAPIService, protected service: AddEditPreonCategoryContentService, public cdf: ChangeDetectorRef, 
    // private toasterService: ToasterService, 
    protected contentservice: PreonContentService,
    protected courseDataService: AddEditPreonCourseContentService,
    private toastr: ToastrService, private router: Router, private route: ActivatedRoute, private http1: HttpClient) {
    // this.loader = true;
    this.settingsTagDrop = {
      text: 'Select Tags',
      singleSelection: false,
      classes: 'common-multi',
      primaryKey: 'id',
      labelKey: 'name',
      noDataLabel: 'Search Tags...',
      enableSearchFilter: true,
      lazyLoading: true,
      searchBy: ['name'],
      maxHeight:250,
    };
    var category;
    var id;

    if (this.service.data != undefined) {
      category = this.service.data.data;
      id = this.service.data.id;
      this.catData = this.service.data.data;
    } else {
      id = 0;
      // this.back();
      // this.router.navigate(['/pages/dashboard']);
    }
      if(localStorage.getItem('LoginResData')){
      this.userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', this.userData.data);
      // this.userId = this.userData.data.data.id;
      this.tenantId = this.userData.data.data.tenantId;
   }
    console.log("Service Data:", this.service.data);
    console.log('Category Info', category);
    console.log('id', id);

   

    this.getallTagList(id,category);
    this.getDropdownList();
    this.getHelpContent();
  }

  getDropdownList() {
    if(this.service.data)
    {
      // console.log('this.service.data',this.service.data)
    this.visibility = this.service.data.dropdownData;
    }else{
      this.back();
    }
  }
ngOnInit(){
  this.header.title=this.service.data.data?this.service.data.data.categoryName:"Add Preonboarding Category"
  if(this.courseDataService.breadcrumbArray){
    this.header.title = this.courseDataService.breadtitle
    this.header.breadCrumbList = this.courseDataService.breadcrumbArray
  }
}
    
  getallTagList(id,category) {
    this.spinner.show();
    let url = webApi.domain + webApi.url.getAllTagsComan;
    let param = {
      tenantId :this.tenantId
    };
    this.webApiService.getService(url, param)
      .then(rescompData => {
        // this.loader =false;
        this.spinner.hide();
        var temp: any = rescompData;
        this.tagList = temp.data;
        this.tempTags = [... this.tagList];
        this.makeCategoryDataReady(id, category);
        // console.log('Visibility Dropdown',this.visibility);
      },
        resUserError => {
          // this.loader =false;
          this.show =false;
          this.spinner.hide();
          this.errorMsg = resUserError;
        });
  }
  
  show:boolean =false;
  makeCategoryDataReady(id,category){
        if (id == 1) {
          this.title = 'Edit Category';
          this.courseCount = category.coursecount
          this.formdata = {
            categoryId: category.categoryId,
            categoryName: category.categoryName,
            categoryCode: category.categoryCode,
            description: category.description,
            categoryPicRef: category.categoryPicRef ? category.categoryPicRef : this.defaultThumb,
            visible: category.visible,
            tenantId: category.tenantId,
            tags: category.tags == null ? category.tags : category.tags.split(',')
          }
            if(category.tagIds)
            {
              var tagIds =category.tagIds.split(',');
              if(tagIds.length > 0){
                // this.tempTags.forEach((tag) => {
                //   tagIds.forEach((tagId)=>{
                //     if (tag.id == tagId ) {
                //       this.selectedTags.push(tag);
                //     }
                //   });
                // });
                this.selectedTags = this.commonFunctionService.getMatchedArray(this.tempTags, tagIds, 'id');
                }
          }
        } else {
          this.title = 'Add Category';
          this.formdata = {
            categoryId: 0,
            categoryName: this.service.categoryName?this.service.categoryName:'',
            categoryCode: '',
            description: '',
            categoryPicRef: this.defaultThumb,
            visible: 1,
            tenantId: this.tenantId,
            tags: '',
          }
        }
        this.show =true;
      }

  back() {
    // this.router.navigate(['/pages/plan/preoncourses/Preoncategory']);
          // this.courseDataService.breadtitle = data.categoryName
          if (this.courseDataService.breadcrumbArray) {
            var length = this.courseDataService.breadcrumbArray.length - 1
            this.courseDataService.breadtitle = this.courseDataService.breadcrumbArray[length].name
            // this.addassetservice.title = this.title
            this.courseDataService.breadcrumbArray = this.courseDataService.breadcrumbArray.slice(0, length)
            // this.title = this.addassetservice.breadCrumbArray[]
            this.courseDataService.previousBreadCrumb = this.courseDataService.previousBreadCrumb.slice(0, length + 1)
    
          }
    window.history.back()
  }

  categoryImgData: any;
  readCategoryThumb(event: any) {
    var validExts = new Array(".png", ".jpg", ".jpeg");
    var fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
      // var toast: Toast = {
      //   type: 'error',
      //   title: "Invalid file selected!",
      //   body: "Valid files are of " + validExts.toString() + " types.",
      //   showCloseButton: true,
      //   // tapToDismiss: false,
      //   timeout: 2000
      //   // onHideCallback: () => {
      //   //     this.router.navigate(['/pages/plan/users']);
      //   // }
      // };
      // this.toasterService.pop(toast);

      this.toastr.error('Valid file types are  ' + validExts.toString() , 'Error',{
        timeOut: 0,
        closeButton: true
      });

      // this.deleteCourseThumb();
    } else {
      if (event.target.files && event.target.files[0]) {
        this.categoryImgData = event.target.files[0];

        var reader = new FileReader();

        reader.onload = (event: ProgressEvent) => {
          // this.defaultThumb = (<FileReader>event.target).result;
          this.formdata.categoryPicRef = (<FileReader>event.target).result;
        }
        reader.readAsDataURL(event.target.files[0]);
        // this.toastr.error('Error', 'Valid files are of' + validExts.toString() + ' types.', {
        //   timeOut: 0,
        //   closeButton: true
        // });
      }
    }
  }

  deleteCategoryThumb() {
    // this.defaultThumb = 'assets/images/category.jpg';
    this.formdata.categoryPicRef = 'assets/images/category.jpg';
    this.categoryImgData = undefined;
    this.formdata.categoryPicRefs =undefined;
  }

  makeTagDataReady(tagsData) {
    this.formdata.tags  =''
     tagsData.forEach((tag)=>{
      if(this.formdata.tags  == '')
      {
        this.formdata.tags  = tag.id;
      }else
      {
        this.formdata.tags = this.formdata.tags +'|' + tag.id;
      }
      console.log('this.formdata.tags',this.formdata.tags);
     });
    // var tagsData = this.formdata.tags;
    // var tagsString = '';
    // if (tagsData) {
    //   for (let i = 0; i < tagsData.length; i++) {
    //     var tag = tagsData[i];
    //     if (tagsString != "") {
    //       tagsString += "|";
    //     }
    //     if (String(tag) != "" && String(tag) != "null") {
    //       tagsString += tag;
    //     }
    //   }
    //   this.formdata.tags1 = tagsString;
    // }
  }
  // popToaster(){
 
  // }

  fileUploadRes: any;
  categoryAddEditRes: any;

  addUpdateCategory(url, category) {
    this.spinner.show();
    this.webApiService.getService(url, category)
      .then(rescompData => {
        // this.loader =false;
        this.spinner.hide();
        var temp: any = rescompData;
        this.categoryAddEditRes = temp.data;
        if (temp == "err") {
          // var catUpdate: Toast = {
          //   type: 'error',
          //   title: "Category",
          //   body: "Unable to update category.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(catUpdate);

        this.toastr.error('Unable to update category.', 'Error', {
          timeOut: 0,
          closeButton: true
       });
   
        } else if (temp.type == false) {
          // var catUpdate: Toast = {
          //   type: 'error',
          //   title: "Category Name Already Exist",
          //   body: this.categoryAddEditRes.msg,
          //   // body: "Category Name Already Exist",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(catUpdate);

          this.toastr.error( 'Category name already exist', 'Error', {
            timeOut: 0,
            closeButton: true
    });
   
        } else {
          if (this.courseDataService.breadcrumbArray) {
            var length = this.courseDataService.breadcrumbArray.length - 1
            this.courseDataService.breadtitle = this.courseDataService.breadcrumbArray[length].name
            // this.addassetservice.title = this.title
            this.courseDataService.breadcrumbArray = this.courseDataService.breadcrumbArray.slice(0, length)
            // this.title = this.addassetservice.breadCrumbArray[]
            this.courseDataService.previousBreadCrumb = this.courseDataService.previousBreadCrumb.slice(0, length + 1)
    
          }
          this.router.navigate(['/pages/preonboarding/preoncourses/Preoncategory']);
          // var catUpdate: Toast = {
          //   type: 'success',
          //   title: "Category",
          //   body: this.categoryAddEditRes.msg,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(catUpdate);
          if(this.service.data.id == 1){

          this.toastr.success( 'Category updated ', 'Success', {
            closeButton: false
          });
        }else{
          this.toastr.success( 'Category created successfully ', 'Success', {
            closeButton: false
          });
        }
   
        }
        console.log('Category AddEdit Result ', this.categoryAddEditRes)
      },
        resUserError => {
          // this.loader =false;
          this.spinner.hide();
          this.errorMsg = resUserError;
        });
  }

  catCodeDupliRes: any;

  // onSubmit(f){
  //   if(f.valid){
    
  //   } else{
  //     console.log('Please Fill all fields');
  //     Object.keys( f.controls).forEach(key => {
  //       f.controls[key].markAsDirty();
  //      });
  //   }
  // }


  submit(f) {
    // this.loader = true;
    // this.spinner.show();

    if (f.valid) {
      // this.makeTagDataReady();
      // this.checkCategoryValid();
      this.catcheck();
      // var catData = {
      //   categoryId: this.formdata.categoryId,
      //   // categoryCode : this.formdata.categoryCode,
      //   categoryName: this.formdata.categoryName,
      // };
      // let url = webApi.domain + webApi.url.checkCategory;
      // console.log('category Data ', catData);
      // this.webApiService.getService(url, catData)
      // .then(rescompData => {
      //   console.log(rescompData);
      //   // this.loader =false;
      //   this.spinner.hide();
      //   this.catCodeDupliRes = rescompData;
      //   console.log('Category Code duplication result ', this.catCodeDupliRes);
      //   if (this.catCodeDupliRes.catCode.isPresent == "true") {
      //     var codeCheck: Toast = {
      //       type: 'error',
      //       title: 'Category',
      //       body: this.catCodeDupliRes.catCode.msg,
      //       showCloseButton: true,
      //       timeout: 2000
      //     };
      //     this.toasterService.pop(codeCheck);
      //   } else {
      //     this.checkCategoryValid();
      //   }
      //   this.cdf.detectChanges();
      // },
      //   resUserError => {
      //     // this.loader =false;
      //     this.spinner.hide();
      //     this.errorMsg = resUserError
      //   });
    } else{
      console.log('Please Fill all fields');
      // const addEditF: Toast = {
      //   type: 'error',
      //   title: 'Unable to update',
      //   body: 'Please Fill all fields',
      //   showCloseButton: true,
      //   timeout: 2000
      // };
      // this.toasterService.pop(addEditF);

      this.toastr.warning( 'Please fill in the required fields', 'Warning', {
        closeButton: false
      })
      Object.keys( f.controls).forEach(key => {
        f.controls[key].markAsDirty();
       });
    }
  }
  catcheck() {
    var catData = {
      categoryId: this.formdata.categoryId,
      // categoryCode : this.formdata.categoryCode,
      categoryName: this.formdata.categoryName,
    };
    console.log(catData);
    this.service.checkCategory(catData).then(res => {
      if (res['data'][0][0]['isPresent'] === 'true') {
        // var codeCheck: Toast = {
        //   type: 'error',
        //   title: 'Category',
        //   body: res['data'][0][0]['msg'],
        //   showCloseButton: true,
        //   timeout: 2000,
        //   // positionClass: 'toast-top-right'
        // };
        // this.toasterService.pop(codeCheck);

        this.toastr.error( res['data'][0][0]['msg'], 'Error', {
          timeOut: 0,
          closeButton: true
  });
 
      } else {
        this.checkCategoryValid();
      }
      this.cdf.detectChanges();
    }, err => {
      console.log(err);
    });
  }

  checkCategoryValid() {
    // this.loader = true;
    this.spinner.show();
    if (this.selectedTags.length > 0) {
      this.makeTagDataReady(this.selectedTags);
       // this.formdata.tags = this.formattedTags;
     }

    var category = {
      categoryId: this.formdata.categoryId,
      categoryName: this.formdata.categoryName,
      categoryCode: this.formdata.categoryCode,
      description: this.formdata.description,
      // categoryPicRef : this.categoryImgData == undefined ? null : this.formdata.categoryPicRef,
      categoryPicRef: this.formdata.categoryPicRef,
      visible: this.formdata.visible,
      tenantId: this.formdata.tenantId,
      tags: this.formdata.tags,
      parCatId:this.service.parenCatId?this.service.parenCatId:null,
      catTypeId :2
      // tagsList : this.formdata.tags,
    }

    var fd = new FormData();
    fd.append('content', JSON.stringify(category));
    fd.append('file', this.categoryImgData);
    console.log('File Data ', fd);

    console.log('Category Data Img', this.categoryImgData);
    console.log('Category Data ', category);

    let url = webApi.domain + webApi.url.addEditCategory;
    let fileUploadUrl = webApi.domain + webApi.url.fileUpload;
    let param = {
      tId: this.tenantId,
    }

    if (this.categoryImgData != undefined) {
      this.webApiService.getService(fileUploadUrl, fd)
        .then(rescompData => {
          // this.loader =false;
          this.spinner.hide();
          var temp: any = rescompData;
          // this.fileUploadRes = JSON.parse(temp);
          this.fileUploadRes = temp;
          if (temp == "err") {
            // var thumbUpload: Toast = {
            //   type: 'error',
            //   title: "Category Thumbnail",
            //   body: "Unable to upload category thumbnail.",
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(thumbUpload);

            this.toastr.error( 'Unable to upload category image.', 'Error', {
              timeOut: 0,
              closeButton: true
      });
     
          } else if (temp.type == false) {
            // var thumbUpload: Toast = {
            //   type: 'error',
            //   title: "Category Thumbnail",
            //   body: "Unable to upload category thumbnail.",
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(thumbUpload);
            
            this.toastr.error( 'Unable to upload category image.', 'Error', {
              timeOut: 0,
              closeButton: true
            });
          }
          else {
            if (this.fileUploadRes.data != null || this.fileUploadRes.fileError != true) {
              category.categoryPicRef = this.fileUploadRes.data.file_url;
              this.addUpdateCategory(url, category);
            } else {
              // var thumbUpload: Toast = {
              //   type: 'error',
              //   title: "Category Thumbnail",
              //   body: this.fileUploadRes.status,
              //   showCloseButton: true,
              //   timeout: 2000
              // };
              // this.toasterService.pop(thumbUpload);

              
            this.toastr.error( this.fileUploadRes.status , 'Error', {
              timeOut: 0,
              closeButton: true
          }); 
            }
          }
          console.log('File Upload Result', this.fileUploadRes)
        },
          resUserError => {
            // this.loader =false;
            this.spinner.hide();
            this.errorMsg = resUserError;
          });
    } else {
      this.addUpdateCategory(url, category);
    }
  }

  // Help Code Start Here //

  helpContent: any;
  getHelpContent() {
    return new Promise(resolve => {

      this.http1.get('../../../../../../assets/help-content/addEditCourseContent.json').subscribe(
        data => {
          this.helpContent = data;
          console.log('Help Array', this.helpContent);
        },
        err => {
          resolve('err');
        },
      );
    });
    // return this.helpContent;
  }


  // Help Code Ends Here //

  onTagsSelect(item: any) {
    console.log(item);
    console.log(this.selectedTags);
  }
  OnTagDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedTags);
  }
  onTagSearch(evt: any) {
    console.log(evt.target.value);
    const val = evt.target.value;
    this.tagList = [];
    const temp = this.tempTags.filter(function(d) {
      return (
        String(d.name)
          .toLowerCase()
          .indexOf(val) !== -1 ||
        !val
      );
    });
  
    // update the rows
    this.tagList = temp;
    console.log('filtered Tag LIst',this.tagList);
  }

  backToAsset(data){
    this.contentservice.parentCatId = data.categoryId
    this.contentservice.countLevel = data['index'] - 1
    this.courseDataService.breadtitle = data.categoryName
    this.courseDataService.breadcrumbArray = this.courseDataService.breadcrumbArray.slice(0,data.index)
    // this.title = this.addassetservice.breadCrumbArray[]
    this.courseDataService.previousBreadCrumb = this.courseDataService.previousBreadCrumb.slice(0,data.index+1)

    // this.router.navigate(['../'], { relativeTo: this.routes });


  }
}
