import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FilterPipeModule  } from 'ngx-filter-pipe';
import { PreonCategory } from './Preoncategory.component';
import { PreonCategoryService } from  './Preoncategory.service';
import { PreonContentService } from '../preoncontent/preoncontent.service';
import { AddEditPreonCategoryContentService } from '../addEditPreonCategoryContent/addEditPreonCategoryContent.service';
import { PreonNotFoundComponent } from './not-found/not-found.component';
import { ThemeModule } from '../../../../@theme/theme.module';
import { TagInputModule } from 'ngx-chips';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgxContentLoadingModule } from 'ngx-content-loading';
import { ComponentModule } from '../../../../component/component.module';
import { ContextMenuModule } from 'ngx-contextmenu';
// import { MiscellaneousRoutingModule, routedComponents } from './miscellaneous-routing.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FilterPipeModule,
    ThemeModule,
    TagInputModule,
    NgxSkeletonLoaderModule,
    NgxContentLoadingModule,
    ComponentModule,    
    ContextMenuModule.forRoot(),
  ],
  declarations: [
    PreonCategory,
    PreonNotFoundComponent,
    // NotFoundComponent
  ],
  providers: [
    PreonCategoryService,
    AddEditPreonCategoryContentService,
    PreonContentService
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})

export class PreonCategoryModule {}
