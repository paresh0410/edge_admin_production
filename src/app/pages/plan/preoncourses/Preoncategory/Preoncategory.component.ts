import { Component, ViewEncapsulation,ElementRef, ViewChild  } from '@angular/core';
import { Router, NavigationStart, Routes, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PreonCategoryService } from  './Preoncategory.service';
import { AddEditPreonCategoryContentService } from '../addEditPreonCategoryContent/addEditPreonCategoryContent.service';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { PreonContentService } from '../preoncontent/preoncontent.service';
import { webAPIService } from '../../../../service/webAPIService';
import { webApi } from '../../../../service/webApi';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { CommonFunctionsService } from '../../../../service/common-functions.service';
import { Card } from '../../../../models/card.model';
import { SuubHeader } from '../../../components/models/subheader.model';
import { noData } from '../../../../models/no-data.model';
import * as _ from "lodash";
import { ContextMenuComponent } from 'ngx-contextmenu';
import { AddEditPreonCourseContentService } from '../addEditPreonCourseContent/addEditPreonCourseContent.service';
import { Filter } from '../../../../models/filter.modal';
@Component({
  selector: 'Preon-category',
  styleUrls: ['./Preoncategory.scss'],
  templateUrl: './Preoncategory.html',
  encapsulation: ViewEncapsulation.None,
})
export class PreonCategory {
  @ViewChild('ContextMenuComponent' ) public folderMenu: ContextMenuComponent;
  @ViewChild('hover') hover?: ElementRef<HTMLElement>;

  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No Categories at this time.",
    desc:"Categories will appear after they are added by the instructor. Categories are classification for managing the pre-onboarding courses.",
    titleShow:true,
    btnShow:true,
    descShow:true,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/preonboarding-course',
  }
  header: SuubHeader  = {
    title:'Preoncategory Category',
    btnsSearch: true,
    searchBar: true,
    dropdownlabel: ' ',
    drplabelshow: false,
    placeHolder:"Search by keywords",
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: '',
    btnName3: '',
    btnAdd: 'Add Category',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: true,
    filter: false,
    showBreadcrumb: true,
    breadCrumbList:[
      {
        'name': 'Preonboarding',
        'navigationPath': '/pages/preonboarding',
      },
    ]
  };
  cardModify: Card = {
    flag: 'preonBoardingCategory',
    titleProp : 'categoryName',
    discrption: 'description',
    image: 'categoryPicRef',
    courseCount: 'coursecount',
    hoverlable:   true,
    showImage: true,
    showBottomCourseCountr: true,
    option:   true,
    eyeIcon:   true,
    editIcon: true,
    bottomDiv:   true,
    bottomDiscription:   true,
    showBottomList:   true,
    bottomTitle:   true,
    btnLabel: 'Details',
    defaultImage:'assets/images/category.jpg'
  }; 

  cardModifyCourse: Card = {
    flag: 'preonBoardingcourse',
    titleProp : 'fullname',
    image: 'coursePicRef',
    category: 'catName',
    // discrption: 'description',
    courseCompletion: 'courseCompletion',
    compleCount: 'Completioncount',
    totalCount: 'enrollcount',
    manager: 'cereaterName',
    maxpoints: 'maxPoints',
    showCompletedEnroll: true,
    showBottomCategory: true,
    showBottomManager: true,
    showBottomMaximumPoint: true,
    showCourseCompletion: true,
    hoverlable:   true,
    showImage: true,
    option:   true,
    eyeIcon:   true,
    copyIcon: true,
    bottomDiv:   true,
   // bottomDiscription:   true,
    showBottomList:   true,
    bottomTitle:   true,
    btnLabel: 'Edit',
    defaultImage: 'assets/images/courseicon.jpg'
  };

  query: string = '';
  public getData;
  count:number=8
  cat: any;
  category: any = [];
  errorMsg: string;

  content1: any = [];

  search: any;
  loader: any;
   userData:any;
  tenantId:any;
  skeleton=false;
  nodata: any = false;
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  text: any;
  searchText: any;
  title: string;
  dummy: any;
  index: any;
  courseList: any;
  parentCatId: any = null;
  categoryId: any;
  categoryName: any;
  enableDisableCourseData: any;
  enableDisableCourseModal: boolean;
  enableCourse: boolean;
  courseDropdownList: any;
  oldParentCatId: any;
  countLevel: any = 0;
  showOld: boolean = false;
  breadtitle: any;
  breadObj: {};
  breadcrumbArray: any = [
    {
      'name': 'Preonboarding',
      'navigationPath': '/pages/preonboarding',
    }, 
    // {
    //   'name': 'Course Category',
    //   'navigationPath': '/pages/plan/courses/category',
    //   'id': {},
    //   'sameComp': true,
    //   'assetId': null
    // }
  ];

  previousBreadCrumb: any = [
    {
      'name': 'Preonboarding',
      'navigationPath': '/pages/preonboarding',
    }, 
    {
      'name': 'Preoncategory Category',
      'navigationPath': '/pages/preonboarding/preoncourses/Preoncategory',
      'id': {},
      'sameComp': true,
      'assetId': null
    }
  ]
  courseListDummy: any;
  hoverMeasure: boolean = false;
  heigthover: number;
  courseLevel: any;
  creatorList: any;
  workflowData: any;
  filtercon: Filter = {
    ascending: true,
    descending: true,
    showDropdown: true,
    // showDropdown: false,
    dropdownList: [
      { drpName: 'Course Name', val: 1 },
      { drpName: 'Created Date', val: 0 },
    ]
  };
  sortdata = [{
    id: 1,
    title: 'Course name',
    sortingmenu: [
      {
        id: 1,
        value: 'asc',
        name: 'ASC',
      }, {
        id: 2,
        value: 'desc',
        name: 'DESC',
      }
    ],
    type: '',
  },
  {
    id: 2,
    title: 'Time Creation/Time modified',
    sortingmenu: [
      {
        id: 1,
        value: 'asc1',
        name: 'ASC',
      }, {
        id: 2,
        value: 'desc2',
        name: 'DESC',
      }
    ],
    type: '',
  },
  ]
  filters: any = [];
  categoryDummy: any;
  catSearch: any;
  courseSearch: any;
  courseDropdownList1: any;
  filter: boolean = false;
  tagList: any;
  constructor(private spinner: NgxSpinnerService,private toastr: ToastrService, protected webApiService: webAPIService, 
    // private toasterService:ToasterService, 
    protected passService: PreonContentService, protected categoryService: PreonCategoryService,
    protected passService1: AddEditPreonCourseContentService,
    protected addCategoryService: AddEditPreonCategoryContentService,private commonFunctionService: CommonFunctionsService, private router: Router, private http1: HttpClient) {
    this.loader = true;

    this.search = {};
    this.category;
    this.cat = {
      id: '',
    };
      if(localStorage.getItem('LoginResData')){
      this.userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', this.userData.data);
      // this.userId = this.userData.data.data.id;
      this.tenantId = this.userData.data.data.tenantId;
   }
    if(this.showOld == true){
    this.getCategories();
    }
    if(this.showOld == false){

      if (this.passService1.breadcrumbArray) {
        this.breadcrumbArray = this.passService1.breadcrumbArray
        this.breadtitle = this.passService1.breadtitle
        this.previousBreadCrumb = this.passService1.previousBreadCrumb
      }

      if(this.passService.parentCatId){
        this.parentCatId = this.passService.parentCatId
      }
      if(this.passService.countLevel){
        this.countLevel = this.passService.countLevel
      }
      this.getCourseCat(this.parentCatId)
    }
    this.getCourseDropdownList()
    this.getDropdownList();
    this.getHelpContent();
  }

  visibility: any = [];
  getDropdownList() {
    let url = webApi.domain + webApi.url.getVisibiltyDropdown;
    let param = {
      tId:  this.tenantId,
    };
    this.webApiService.getService(url, param)
      .then(rescompData => {
        // this.loader =false;
        var temp: any = rescompData;
        if (temp == "err") {
        } else {
          this.visibility = temp.data;
        }
        this.skeleton=true;
        // console.log('Visibility Dropdown',this.visibility);
      },
        resUserError => {
          // this.loader =false;
          this.spinner.hide();
          this.skeleton=true;
          this.errorMsg = resUserError;
        });
  }

  getCategories() {
    let url = webApi.domain + webApi.url.getCategories;
    let param = {
      tId:  this.tenantId,
    };

    this.webApiService.getService(url, param)
      .then(rescompData => {
        console.log(rescompData);
        // this.loader =false;
        this.nodata = false;
        var temp: any = rescompData;
        if (temp == "err") {
          this.nodata = true;
          this.skeleton=true;
        } else {
          this.category = temp.data;
          this.displayArray=[]
          this.addItems(0,this.sum,'push',this.category);
        }
        this.skeleton=true;
        // console.log('Category Result',rescompData)
      },
        resUserError => {
          // this.loader =false;
          if (resUserError.statusText == "Unauthorized") {
            this.router.navigate(['/login']);
          }
          this.errorMsg = resUserError;
          this.nodata = true;
          this.skeleton=true;
        });
  }

  clear() {
    if(this.searchText.length>=3){
    this.search = {};
    this.nodata=false
    // this.getCategories();
    this.displayArray =[];
    this.newArray = [];
    this.sum = 50;
    this.nodata = false
    this.courseList = this.courseListDummy
    this.addItems(0, this.sum, 'push', this.category);
    }
  else{
    this.search = {};
  }
  }

  back() {
    this.filter = false
    // this.router.navigate(['/pages/preonboarding']);
      if (this.parentCatId == null) {
        this.countLevel = 0
        this.router.navigate(['/pages/preonboarding']);
      }
      else {
        this.countLevel = this.countLevel - 1
        this.parentCatId = this.oldParentCatId
        var index = this.breadcrumbArray.length - 1
        this.breadtitle = this.breadcrumbArray[index].name
        this.breadcrumbArray.pop()
        this.previousBreadCrumb.pop()
        this.getCourseCat(this.oldParentCatId)
      }
    }

  content2(data, id) {
    var passData = {
      id: id,
      data: data
    }
    this.passService.data = passData;
    this.router.navigate(['/pages/preonboarding/preoncourses/preoncontent']);
  }

  gotoCardDetails(data) {
    var passData = {
      id: 0,
      data: data
    }
    this.passService.data = passData;
    this.passService.page = "preonCat"
    this.router.navigate(['/pages/preonboarding/preoncourses/preoncontent']);
  }
  public addeditccategory(data, id) {
    var data1 = {
      data: data,
      id: id,
      dropdownData: this.visibility,
    }

    if (data == undefined) {
    this.addCategoryService.parenCatId = this.parentCatId
    this.passService.parentCatId = this.parentCatId
    this.passService.countLevel = this.countLevel
    // this.addCategoryService.categoryName = this.categoryName
    this.passService1.breadtitle = 'Add Category'
    this.passService1.previousBreadCrumb = this.previousBreadCrumb
    this.passService1.breadcrumbArray = this.previousBreadCrumb
    this.addCategoryService.data = data1;
    this.router.navigate(['/pages/preonboarding/preoncourses/addEditPreonCategoryContent']);
    } else {
    this.addCategoryService.parenCatId = this.parentCatId
    this.passService.parentCatId = this.parentCatId
    this.passService.countLevel = this.countLevel
    this.passService1.breadtitle = 'Add Category'
    this.passService1.previousBreadCrumb = this.previousBreadCrumb
    this.passService1.breadcrumbArray = this.previousBreadCrumb
    this.addCategoryService.data = data1;
    this.router.navigate(['/pages/preonboarding/preoncourses/addEditPreonCategoryContent']);
    }

  }
   
  gotoCardaddedit(data) {
    var data1 = {
      data: data,
      id: 1,
      dropdownData: this.visibility,
    }

    this.breadtitle = data.categoryName
    this.breadObj = {
      'name': data.categoryName,
      // 'navigationPath': '',
      'id': data,
      'navigationPath': '/pages/preonboarding/preoncourses/Preoncategory',
      'sameComp': true,
      'assetId': data.categoryId
    }
  
    this.previousBreadCrumb.push(this.breadObj)
  
    this.breadcrumbArray.push(this.breadObj)
    for (let i = 0; i < this.previousBreadCrumb.length; i++) {
      if (this.breadtitle == this.previousBreadCrumb[i].name) {
        this.breadcrumbArray = this.previousBreadCrumb.slice(0, i)
      }
    }
    this.addCategoryService.parenCatId = this.parentCatId
    // this.addCategoryService.categoryName = data.categoryName
    this.passService.parentCatId = this.parentCatId
    this.passService.countLevel = this.countLevel
    this.passService1.breadtitle = this.breadtitle
    this.passService1.breadcrumbArray = this.breadcrumbArray
    this.passService1.previousBreadCrumb = this.previousBreadCrumb
    this.addCategoryService.data = data1;
    this.router.navigate(['/pages/preonboarding/preoncourses/addEditPreonCategoryContent']);
  }

  deleteCategoryModal: boolean = false;
  deleteCategoryData: any;
  deleteCategory(Category) {
    // console.log('Category content',Category);
    this.deleteCategoryData = Category;
    this.deleteCategoryModal = true;
  }

  deleteCategoryAction(actionType) {
    if (actionType == true) {
      this.closeDeleteCategoryModal();
    } else {
      this.closeDeleteCategoryModal();
    }
  }

  closeDeleteCategoryModal() {
    this.deleteCategoryModal = false;
  }

  disableCat: boolean = false;
  visibiltyRes: any;
  enableDisableCategoryModal: boolean = false;
  enableDisableCategoryData: any;
  enableCat: boolean = false;
  catDisableIndex: any;
  btnName:string='Yes'
  disableCategory(currentIndex, categoryData, status) {
    console.log(categoryData);
    if (categoryData.coursecount > 0) {
      // var catUpdate: Toast = {
      //   type: 'error',
      //   title: "Category",
      //   body: "Sorry Unable to update visibility of category.",
      //   showCloseButton: true,
      //   timeout: 2000,
      // };
      // this.toasterService.pop(catUpdate);

      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.enableDisableCategoryData = categoryData;
      this.enableDisableCategoryModal = true;
    }
    this.catDisableIndex = currentIndex;
    if (categoryData.visible == 0) {
      this.enableCat = false;
    } else {
      this.enableCat = true;
    }
    // if (this.category[currentIndex].visible == 0) {
    //   this.enableCat = false;
    // } else {
    //   this.enableCat = true;
    // }
  }

  clickTodisable(categoryData) {
    if(categoryData.coursecount>0){
      this.presentToast('warning', 'Category have course, not able to hide this category');
    }
    else if (categoryData.visible == 1) {
      this.title="Disable category"
      this.enableCat = true;
      this.enableDisableCategoryData = categoryData;
      this.enableDisableCategoryModal = true;
    } else {
      this.title="Enable category"
      this.enableCat = false;
      this.enableDisableCategoryData = categoryData;
      this.enableDisableCategoryModal = true;
    }
  }

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }

  enableDisableCategory(categoryData) {
    console.log(categoryData);
    this.spinner.show();
    var visibilityData = {
      categoryId: categoryData.categoryId,
      visible: categoryData.visible
    };
    const _urlDisableCategory:string = webApi.domain + webApi.url.disableCategory;
    this.commonFunctionService.httpPostRequest(_urlDisableCategory,visibilityData)
    //this.categoryService.disableCategory(visibilityData)
      .then(rescompData => {
        // this.loader =false;
        this.spinner.hide();
        this.visibiltyRes = rescompData;
        // console.log('Category Visibility Result',this.visibiltyRes);
        if (this.visibiltyRes.type == false) {
          // var catUpdate: Toast = {
          //   type: 'error',
          //   title: "Category",
          //   body: "Unable to update visibility of category.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.closeEnableDisableCategoryModal();
          // this.toasterService.pop(catUpdate);

          this.toastr.error( 'Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        } else {
          // var catUpdate: Toast = {
          //   type: 'success',
          //   title: "Category",
          //   body: this.visibiltyRes.data,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.enableDisableCategoryData.visible == 0;
          this.closeEnableDisableCategoryModal();
          // this.toasterService.pop(catUpdate);

          this.toastr.success(this.visibiltyRes.data, 'Success', {
            closeButton: false
           });
        }
      },
        resUserError => {
          // this.loader =false;
          this.spinner.hide();
          if (resUserError.statusText == "Unauthorized") {
            this.router.navigate(['/login']);
          }
          this.errorMsg = resUserError;
          this.closeEnableDisableCategoryModal();
        });
  }

  enableDisableCategoryAction(actionType) {
    // console.log(this.category[this.catDisableIndex]);
    console.log(this.enableDisableCategoryData);
    if (actionType == true) {
      // if (this.category[this.catDisableIndex].visible == 1) {
      //   this.category[this.catDisableIndex].visible = 0;
      //   var catData = this.category[this.catDisableIndex];
      //   this.enableDisableCategory(catData);
      // } else {
      //   this.category[this.catDisableIndex].visible = 1;
      //   var catData = this.category[this.catDisableIndex];
      //   this.enableDisableCategory(catData);
      // }
      if (this.enableDisableCategoryData.visible == 1) {
        this.enableDisableCategoryData.visible = 0;
        var catData = this.enableDisableCategoryData;
        this.enableDisableCategory(catData);
      } else {
        this.enableDisableCategoryData.visible = 1;
        var catData = this.enableDisableCategoryData;
        this.enableDisableCategory(catData);
      }
    } else {
      this.closeEnableDisableCategoryModal();
    }
  }

  closeEnableDisableCategoryModal() {
    this.enableDisableCategoryModal = false;
  }


  // Help Code Start Here //

  helpContent: any;
  getHelpContent() {
    return new Promise(resolve => {

      this.http1.get('../../../../../../assets/help-content/addEditCourseContent.json').subscribe(
        data => {
          this.helpContent = data;
          console.log('Help Array', this.helpContent);
        },
        err => {
          resolve('err');
        },
      );
    });
    // return this.helpContent;
  }


  // Help Code Ends Here //
  getUrl(string)
  {
    return 'url(' + string + ')';
  }

  onsearch(event){
    this.search.categoryName =  event.target.value
  }
  ngOnDestroy(){
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0){
      e1[0].classList.add('with-scroll');
    }
  }
  ngAfterContentChecked() {
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0)
    {
      e1[0].classList.remove('with-scroll');
    }

   }
   conentHeight = '0px';
   offset: number = 100;
   ngOnInit() {
     this.conentHeight = '0px';
     const e = document.getElementsByTagName('nb-layout-column');
     console.log(e[0].clientHeight);
     // this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
     if (e[0].clientHeight > 1300) {
       this.conentHeight = '0px';
       this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
     } else {
       this.conentHeight = String(e[0].clientHeight) + 'px';
     }
   }
 
   sum = 60;
   addItems(startIndex, endIndex, _method, asset) {
     for (let i = startIndex; i < endIndex; ++i) {
       if (asset[i]) {
         this.displayArray[_method](asset[i]);
       } else {
         break;
       }
 
       // console.log("NIKHIL");
     }
   }
   // sum = 10;
   onScroll(event) {
 
     let element = this.myScrollContainer.nativeElement;
     // element.style.height = '500px';
     // element.style.height = '500px';
     // Math.ceil(element.scrollHeight - element.scrollTop) === element.clientHeight
 
     if (element.scrollHeight - element.scrollTop - element.clientHeight < 5) {
       if (this.newArray.length > 0) {
         if (this.newArray.length >= this.sum) {
           const start = this.sum;
           this.sum += 50;
           this.addItems(start, this.sum, 'push', this.newArray);
         } else if ((this.newArray.length - this.sum) > 0) {
           const start = this.sum;
           this.sum += this.newArray.length - this.sum;
           this.addItems(start, this.sum, 'push', this.newArray);
         }
       } else {
         if (this.category.length >= this.sum) {
           const start = this.sum;
           this.sum += 50;
           this.addItems(start, this.sum, 'push', this.category);
         } else if ((this.category.length - this.sum) > 0) {
           const start = this.sum;
           this.sum += this.category.length - this.sum;
           this.addItems(start, this.sum, 'push', this.category);
         }
       }
 
 
       console.log('Reached End');
     }
   }
   newArray =[];
   displayArray =[];
    // this function is use to dynamicsearch on table
  searchData(event) {
    var temData = this.category;
    var course = this.courseListDummy
    this.text  = event.target.value;
    var val = event.target.value.toLowerCase();
    var keys = [];
    var coursekeys = [];
    this.searchText=val;
    if(event.target.value == '')
    {
      this.displayArray =[];
      this.newArray = [];
      this.sum = 50;
      this.addItems(0, this.sum, 'push', this.category);
    }else{
         // filter our data
    if (temData.length > 0) {
      keys = Object.keys(temData[0]);
    }
    if(val.length>=3||val.length==0){
    var temp = temData.filter((d) => {
      for (const key of keys) {
        if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
          return true;
        }
      }
    });
    if(temp.lenghth==0){
      this.nodata=true
      this.noDataVal={
        margin:'mt-5',
        imageSrc: '../../../../../assets/images/no-data-bg.svg',
        title:"Sorry we couldn't find any matches please try again",
        desc:".",
        titleShow:true,
        btnShow:false,
        descShow:false,
        btnText:'Learn More',
        btnLink:''
      }
    }
    // update the rows
    this.displayArray =[];
    this.newArray = temp;
    this.sum = 50;
  }
    this.addItems(0, this.sum, 'push', this.newArray);

    // new addition
    if (course.length > 0) {
      coursekeys = Object.keys(course[0]);
    }
    if (val.length >= 3 || val.length == 0) {
      var coursetemp = course.filter((d) => {
        for (const key of coursekeys) {
          if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
            return true;
          }
        }
      });
    this.courseList = coursetemp
    
  }
  
  // end 
  }
  if(this.courseList.length == 0 && this.displayArray.length == 0){
    this.nodata = true
  }else{
    this.nodata = false
  }
}

  //new structure code
  contextItem(event,data) {
   
    console.log(event,data);
    var catData = event.item

    if (this.dummy == 'Enable' || this.dummy == 'Disable'){
      this.clickTodisable(catData)
    }
    if(this.dummy == 'Edit'){
      this.gotoCardaddedit(catData)

    }




  }

  View(item,index){
    console.log(item,"cgffgxfgx")
    this.dummy = item;
    this.index = index
    
    }

    getCourseCat(id) {

      if(this.parentCatId == null){
        this.header = {
        title:'Preoncategory Category',
        btnsSearch: true,
        placeHolder:"Search by category name",
        searchBar: true,
        dropdownlabel: ' ',
        drplabelshow: false,
        drpName1: '',
        drpName2: ' ',
        drpName3: '',
        drpName1show: false,
        drpName2show: false,
        drpName3show: false,
        btnName1: '',
        btnName2: '',
        btnName3: '',
        btnAdd: 'Add Category',
        btnName1show: false,
        btnName2show: false,
        btnName3show: false,
        btnBackshow: true,
        btnAddshow: true,
        filter: false,
        showBreadcrumb: true,
        breadCrumbList:this.breadcrumbArray   
      };
      }
      else{
      this.header = {
        title:this.breadtitle,
        btnsSearch: true,
        placeHolder:"Search by category name",
        searchBar: true,
        dropdownlabel: ' ',
        drplabelshow: false,
        drpName1: '',
        drpName2: ' ',
        drpName3: '',
        drpName1show: false,
        drpName2show: false,
        drpName3show: false,
        btnName1: 'Add Course',
        btnName2: '',
        btnName3: '',
        btnAdd: 'Add Category',
        btnName1show: true,
        btnName2show: false,
        btnName3show: false,
        btnBackshow: true,
        btnAddshow: true,
        filter: true,
        showBreadcrumb: true,
        breadCrumbList:this.breadcrumbArray
        }
      }
      if(this.countLevel>3){
        this.header.btnAddshow = false
      }
      this.skeleton = false
      // this.spinner.show();
      let url = webApi.domain + webApi.url.getCourseCat;
      let param = {
        courseTypeId:5,
        catId:id,
        wfId:null
      }
  
      this.webApiService.getService(url, param)
        .then(rescompData => {
          // this.loader =false;
          this.spinner.hide();
          this.nodata = false;
          var temp: any = rescompData;
          if (temp == "err") {
            this.nodata = true;
            this.skeleton=true;
          } else {
            const list = rescompData['list'][1];
            const list1 = rescompData['list'][2];
            this.category = list;
            this.categoryDummy = list
            this.catSearch = list
            this.courseList = list1
            this.courseListDummy = list1
            this.courseSearch = list
            if (rescompData['list'][0].length === 0) {
              this.oldParentCatId = null;
            } else {
              this.oldParentCatId = rescompData['list'][0][0].parentCatId;
            }
            if(list.length == 0 && list1.length == 0){
              this.nodata = true
            }
            else{
              this.nodata = false
            }
            this.displayArray = []
            this.addItems(0,this.sum,'push',this.category);
          }
          this.skeleton=true;
          // console.log('Category Result',rescompData)
        },
          resUserError => {
            // this.loader =false;
            this.spinner.hide();
            this.skeleton=true;
            this.errorMsg = resUserError;
            this.nodata = true;
          });
  
    }


    getAllSubCat(item){
      this.filter = false
      this.countLevel =  this.countLevel+1
      this.parentCatId = item.categoryId
      this.categoryId = this.parentCatId
      this.categoryName = item.categoryName      
      this.passService.parentCatId = this.parentCatId
      this.passService.countLevel = this.countLevel

                  //breadCrumb
                  this.breadtitle = item.categoryName
                  console.log('assetData', item);
                  //// breadCrumb 
                  this.breadObj = {
                    'name': item.categoryName,
                    // 'navigationPath': '',
                    'id': item,
                    'navigationPath': '/pages/preonboarding/preoncourses/Preoncategory',
                    'sameComp': true,
                    'assetId': item.categoryId
                  }
            
                  this.previousBreadCrumb.push(this.breadObj)
            
                  this.breadcrumbArray.push(this.breadObj)
                  for (let i = 0; i < this.previousBreadCrumb.length; i++) {
                    if (this.breadtitle == this.previousBreadCrumb[i].name) {
                      this.breadcrumbArray = this.previousBreadCrumb.slice(0, i)
                    }
                  }
                  //End BreadCrumb
      this.getCourseCat(this.parentCatId)

    }

    clickTodisableBatch(courseData) {
      if (courseData.visible == 1) {
        this.enableCourse = true;
        this.enableDisableCourseData = courseData;
        this.enableDisableCourseModal = true;
      } else {
        this.enableCourse = false;
        this.enableDisableCourseData = courseData;
        this.enableDisableCourseModal = true;
      }
    }
  
    enableDisableCourse(courseData) {
      this.spinner.show();
      var visibilityData = {
        courseId: courseData.courseId,
        visible: courseData.visible
      }
      const _urlDisableCourse:string = webApi.domain + webApi.url.disableCourse;
      this.commonFunctionService.httpPostRequest(_urlDisableCourse,visibilityData)
     // this.contentservice.disableCourse(visibilityData)
        .then(rescompData => {
          // this.loader =false;
          this.spinner.hide();
          this.visibiltyRes = rescompData;
          console.log('Course Visibility Result', this.visibiltyRes);
          if (this.visibiltyRes.type == false) {
            // var courseUpdate: Toast = {
            //   type: 'error',
            //   title: "Course",
            //   body: "Unable to update visibility of course.",
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            this.closeEnableDisableCourseModal();
            // this.toasterService.pop(courseUpdate);
  
            this.toastr.error( 'Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
              timeOut: 0,
              closeButton: true
            });
          } else {
            // var courseUpdate: Toast = {
            //   type: 'success',
            //   title: "Course",
            //   body: this.visibiltyRes.data,
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            this.closeEnableDisableCourseModal();
            // this.toasterService.pop(courseUpdate);
  
  
            this.toastr.success(this.visibiltyRes.data, 'Success', {
              closeButton: false
             });
          }
        },
          resUserError => {
            // this.loader =false;
            this.spinner.hide();
            this.errorMsg = resUserError;
            this.closeEnableDisableCourseModal();
          });
    }
  
    enableDisableCourseAction(actionType) {
      // if(actionType == true){
      //   for(let i = 0 ; i < this.content.length ;i++)
      //   {
      //     if(this.content[i].courseId  ==  this.enableDisableCourseData.courseId)
      //     {
      //       this.courseDisableIndex = i;
      //       if(this.content[this.courseDisableIndex].visible == 1){
      //         this.content[this.courseDisableIndex].visible = 0;
      //         var courseData = this.content[this.courseDisableIndex];
      //         this.enableDisableCourse(courseData);
  
      //       }else{
      //         this.content[this.courseDisableIndex].visible = 1;
      //         var courseData = this.content[this.courseDisableIndex];
      //         this.enableDisableCourse(courseData);
      //       }
      //     }
      //     }
      //   }else{
      //     this.closeEnableDisableCourseModal();
      //   }
  
      if (actionType == true) {
  
        if (this.enableDisableCourseData.visible == 1) {
          this.enableDisableCourseData.visible = 0;
          var courseData = this.enableDisableCourseData;
          this.enableDisableCourse(courseData);
  
        } else {
          this.enableDisableCourseData.visible = 1;
          var courseData = this.enableDisableCourseData;
          this.enableDisableCourse(courseData);
        }
      } else {
        this.closeEnableDisableCourseModal();
      }
    }
  
    closeEnableDisableCourseModal() {
      this.enableDisableCourseModal = false;
    }
  
    formDataCourse: any;
    duplicateContent(courseData) {
      this.formDataCourse = {
        courseId:courseData.courseId,
        fullname: '',
        tenantId: courseData.tenantId,
      }
    }
  
    duplicateCourseModal: boolean = false;
    duplicateCourseData: any;
    duplicateCourseContent(course) {
      console.log('Course content', course);
      this.duplicateCourseData = course;
      this.duplicateCourseModal = true;
      this.duplicateContent(course);
    }
  
    copyCard(course) {
      console.log('Course content', course);
      this.duplicateCourseData = course;
      this.duplicateCourseModal = true;
      this.duplicateContent(course);
    }
  
  
    duplicateCourseAction(actionType) {
      if (actionType == true) {
        this.makeDuplicateCourseDataReady();
        this.closeDuplicateCourseModal();
      } else {
        this.closeDuplicateCourseModal();
      }
    }
  
    closeDuplicateCourseModal() {
      this.duplicateCourseModal = false;
    }

    creditPointsStr: any;
    makeDuplicateCourseDataReady() {
      var courseData = {
        courseId:this.formDataCourse.courseId,
        fullname: this.formDataCourse.fullname,
        userMod: this.userData.data.data.id,
      }
      console.log('Duplicate course data ', courseData);
  
      this.duplicateCourse(courseData);
    }
  
    duplicateCourseRes: any;
    duplicateCourse(course) {
      const _urlDublicateCourse:string = webApi.domain + webApi.url.duplicateCourse;
      this.commonFunctionService.httpPostRequest(_urlDublicateCourse,course)
      //this.contentservice.dublicateCourse(course)
        .then(rescompData => {
          this.loader = false;
          var temp: any = rescompData;
          this.duplicateCourseRes = temp.data;
          if (temp == "err") {
            // this.notFound = true;
            // var courseDuplicate: Toast = {
            //   type: 'error',
            //   title: "Course",
            //   body: "Unable to duplicate course.",
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(courseDuplicate);
  
            this.toastr.error( 'Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
              timeOut: 0,
              closeButton: true
            });
          } else if (temp.type == false) {
            // var courseDuplicate: Toast = {
            //   type: 'error',
            //   title: "Course",
            //   body: this.duplicateCourseRes[0].msg,
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(courseDuplicate);
  
            this.toastr.error( 'Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
              timeOut: 0,
              closeButton: true
            });
          } else {
            // var courseDuplicate: Toast = {
            //   type: 'success',
            //   title: "Course",
            //   body: this.duplicateCourseRes[0].msg,
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(courseDuplicate);
  
            this.toastr.success(this.duplicateCourseRes[0].msg, 'Success', {
              closeButton: false
             });
            // this.getCourses();
            this.getCourseCat(this.parentCatId)
          }
          console.log('Course duplicate Result ', this.duplicateCourseRes);
          // this.cdf.detectChanges();
        },
          resUserError => {
            this.loader = false;
            this.errorMsg = resUserError;
          });
    }
    getCourseDropdownList() {
      const param = {
        tId: this.tenantId,
      }
      this.spinner.show();
      const _urlGetCourseDropdown: string = webApi.domain + webApi.url.getCourseDropdown;
      this.commonFunctionService.httpPostRequest(_urlGetCourseDropdown,param)
      //this.detailsService.getDropdownList(param)
      .then(rescompData => {
        // this.loader =false;
        this.spinner.hide();
        this.courseDropdownList = rescompData['data'];
        console.log('Course Dropdown List ', this.courseDropdownList);
        this.makeCourseDetailsDropdownReady()
      },
        resUserError => {
          // this.loader =false;
          this.spinner.hide();
          this.errorMsg = resUserError
        });
    }

    breadcrumbNavigate(data){
      this.filter = false
      console.log(data,"navigationData")
      this.breadtitle = data.categoryName
      this.parentCatId = data.categoryId;
      this.countLevel = data['index']-1
      this.breadcrumbArray =  this.breadcrumbArray.slice(0,data.index)
      this.previousBreadCrumb = this.previousBreadCrumb.slice(0,data.index+1)
      console.log(this.breadcrumbArray,"breadcrumbArray")
      console.log(this.previousBreadCrumb,"previousBreadCrumb")
      
    
      // this.header.breadCrumbList =  this.breadcrumbArray
      // this.parentCatId = this.oldParentCatId
      this.getCourseCat(this.parentCatId);
    
    }

    public addeditcontent(data, id) {
      var data1 = {
        data: data,
        id: id,
        catId: this.categoryId,
        categoryName:this.categoryName,
        courseDropdowns: this.courseDropdownList
      }
      if (data == undefined) {
        this.passService1.data = data1;
        this.passService.parentCatId = this.parentCatId
        this.passService.countLevel = this.countLevel
        this.passService1.breadtitle = 'Add Preonboarding Course'
        this.passService1.previousBreadCrumb = this.previousBreadCrumb
        this.passService1.breadcrumbArray = this.previousBreadCrumb
        this.passService.page = "preonCat"
        this.router.navigate(['/pages/preonboarding/preoncourses/addEditPreonCourseContent']);
        console.log("Data passed to service" + data1);
        console.log("ID Passed" + data1.id + " " + data1.data);
      } else {
        this.passService1.data = data1;
        this.passService.parentCatId = this.parentCatId
        this.passService.countLevel = this.countLevel
        this.passService.page = "preonCat"
        this.router.navigate(['/pages/preonboarding/preoncourses/addEditPreonCourseContent']);
      }
    }
  
    gotoCardaddeditCourse(data) {
      var data1 = {
        data: data,
        id: 1,
        catId: this.categoryId,
        categoryName:this.categoryName,
        courseDropdowns: this.courseDropdownList
      }
      if (data == undefined) {
        this.passService1.data = data1;
        this.passService.parentCatId = this.parentCatId
        this.passService.countLevel = this.countLevel
        this.passService.page = "preonCat"
        this.router.navigate(['/pages/preonboarding/preoncourses/addEditPreonCourseContent']);
        console.log("Data passed to service" + data1);
        console.log("ID Passed" + data1.id + " " + data1.data);
      } else {
        this.breadtitle = data.fullname
        this.breadObj = {
          'name': data.fullname,
          // 'navigationPath': '',
          'id': data,
          'navigationPath': '/pages/preonboarding/preoncourses/Preoncategory',
          'sameComp': true,
          'assetId': data.categoryId
        }
      
        this.previousBreadCrumb.push(this.breadObj)
      
        this.breadcrumbArray.push(this.breadObj)
        for (let i = 0; i < this.previousBreadCrumb.length; i++) {
          if (this.breadtitle == this.previousBreadCrumb[i].name) {
            this.breadcrumbArray = this.previousBreadCrumb.slice(0, i)
          }
        }
        this.passService1.data = data1;
        this.passService.parentCatId = this.parentCatId
        this.passService.countLevel = this.countLevel
        this.passService1.breadtitle = this.breadtitle
        this.passService1.breadcrumbArray = this.breadcrumbArray
        this.passService1.previousBreadCrumb = this.previousBreadCrumb
        this.passService.page = "preonCat"
        this.router.navigate(['/pages/preonboarding/preoncourses/addEditPreonCourseContent']);
      }
    }

    hoverable() {
      this.heigthover = this.hover.nativeElement.getBoundingClientRect().top;
      var hoversec = this.hover.nativeElement;
      if (this.heigthover > 550 ) {
        hoversec.classList.add('window-top');
      }
    }
  
    mouseup(index, item) {
      if(item.description) {
        for (let k = 0; k < this.category.length; k++) {
          if (k == index) {
            this.category[k].isHoverDesc = 1;
            this.hoverMeasure = true;
          }
        }
      }
    }
  
    mousedown(index) {
      for (let k = 0; k < this.category.length; k++) {
        if (k == index) {
          this.category[k].isHoverDesc = 0;
        }
      }
    }
    ngAfterViewChecked() {
      
      if(this.hoverMeasure == true) {
        this.hoverable();
        this.hoverMeasure = false;
      }
    }

 

makeCourseDetailsDropdownReady() {
  // this.spinner.hide();
  // this.showSpinner =false
  // this.categoryData = this.courseDropdownList.catList;
  // this.courseFormat = this.courseDropdownList.courseType;
  this.courseLevel = this.courseDropdownList.courseLevel;
  // this.visibility = this.courseDropdownList.visibility;
  // this.daysBucket = this.courseDropdownList.leadTime;
  // this.userRoles = this.courseDropdownList.userRoles;
  this.creatorList = this.courseDropdownList.creatorList;
  this.workflowData = this.courseDropdownList.lpList;
  this.tagList =this.courseDropdownList.tagList;
  for (let i = 0; i < this.courseLevel.length; i++) {
    this.courseLevel[i].courseLevelId = this.courseLevel[i].id;
  }
  for (let i = 0; i < this.creatorList.length; i++) {
    this.creatorList[i].creatorId = this.creatorList[i].id;
  }
  // this.bindfilter(this.categoryData);
  this.bindfilter(this.courseLevel);
  this.bindfilter(this.creatorList);
  this.bindfilter(this.workflowData);
  this.bindfilter(this.sortdata);
  this.bindfilter(this.tagList)
}

//filters
bindfilter(obj) {
  let filtername, filterValueName, type, singleSelection;
  if (obj.length > 0) {
    filtername = obj[0]['filterId'];
    filterValueName = obj[0]['filterValue'];
    type = obj[0]['type'];
    singleSelection = obj[0]['singleSelection'];
  }
      const item = {
        count: "",
        value: "",
        tagname: obj.length > 0 ? obj[0]['filterId'] : '',
        isChecked: false,
        list: obj,
        filterValue: filterValueName,
        type: type,
        filterId: filtername,
        singleSelection: singleSelection,
      }
      if (filtername) {
        this.filters.push(item);
      }
  // if (result['data'] && result['data'].length > 0) {
  //   result['data'].forEach( (value, index) => {

  //   })
  // }
}
filteredChanged(event){
  if (event.empty) {
    this.courseList = this.courseListDummy;
    // this.folder = this.tempFolders;
  } else {
    // this.filterFolder(event);
    this.filterFiles(event);
  }
  this.courseSearch = this.courseList;
  this.catSearch = this.category;
}

filterFiles(event) {
  this.courseList = [];
  let indexObj = {};
  let filterId;
  let filterValue;
  const findElement = (element) => String(element[filterId]).toLowerCase() === String(filterValue).toLowerCase();
  // tslint:disable-next-line:forin
  for (const x in event) {
      filterId = x;
      if (event[x] && Array.isArray(event[x])) {
        event[x].forEach((value, key) => {
          filterValue = value[filterId];
          // for(let i = 0; i<this.tempFiles.length; i++) {
          //   // const indexnumber = this.tempFiles.findIndex(findElement);
          //   // indexObj[indexnumber] = true;
          // }
          let indexnumberArray = [];
          if (filterId.startsWith('tag')) {
            // tslint:disable-next-line:max-line-length
            indexnumberArray = this.courseListDummy.map((e, j) => String(e[filterId]).includes(String(filterValue).toLowerCase()) ? j : '').filter(String);
          } else {
          // tslint:disable-next-line:max-line-length
            indexnumberArray = this.courseListDummy.map((e, j) => String(e[filterId]).toLowerCase() === String(filterValue).toLowerCase() ? j : '').filter(String);
// indexObj[indexnumber] = true;
          }
          indexObj = Object.assign(indexObj, indexnumberArray.reduce((a,b)=> (a[b]=true,a),{}));
          // const indexnumber = this.tempFiles.findIndex(findElement);
          // indexObj[indexnumber] = true;
      });
      }
  }
  console.log(JSON.stringify(indexObj));
  // tslint:disable-next-line:forin
  for (const y in indexObj) {
    if (String(y) !== '-1'){
      this.courseList.push(this.courseListDummy[y]);
    }
  }
}
filterFolder(event:any) {
  this.category  = [];
  let indexObj = {};
  let filterValue;
  let filterId;
  // const findElement = (element) => element[filterId] === filterValue;
  // const findElement = map((e, i) => e[filterId] === filterValue ? i : '').filter(String);
  // tslint:disable-next-line:forin
  for (const x in event) {
      filterId = x;
      if (event[x] && Array.isArray(event[x])) {
        event[x].forEach((value, key) => {
            filterValue = value[filterId];
            // for(let i = 0; i<this.tempFolders.length; i++) {
            //   // const indexnumber = this.tempFolders.findIndex(findElement);
            //   // tslint:disable-next-line:max-line-length
            //   const indexnumberArray =
            // this.tempFolders.map((e, j) => e[filterId] === filterValue ? j : '').filter(String);
            //   // indexObj[indexnumber] = true;
            //   indexObj = indexnumberArray.reduce((a,b)=> (a[b]=true,a),{});
            // }
            let indexnumberArray = [];
            if (filterId.startsWith('tag')) {
              // tslint:disable-next-line:max-line-length
              indexnumberArray = this.categoryDummy.map((e, j) => String(e[filterId]).includes(String(filterValue).toLowerCase()) ? j : '').filter(String);
            } else {
              // tslint:disable-next-line:max-line-length
              indexnumberArray = this.categoryDummy.map((e, j) => String(e[filterId]).toLowerCase() === String(filterValue).toLowerCase() ? j : '').filter(String);
            }
              // indexObj[indexnumber] = true;
              indexObj = Object.assign(indexObj, indexnumberArray.reduce((a,b)=> (a[b]=true,a),{}), indexObj);
        });
      }
  }
  console.log(JSON.stringify(indexObj));
  // tslint:disable-next-line:forin
  for (const y in indexObj) {
    if (String(y) !== '-1') {
      this.category.push(this.categoryDummy[y]);
    }
  }
}
SearchFilter(event) {
  const val = event == undefined ? '' : event.target.value.toLowerCase();
  // const files = this.files;
  let keys = [];
  if (val) {
    if (this.courseSearch.length > 0) {
      keys = Object.keys(this.courseSearch[0]);
    }
    if(val.length>=3||val.length==0) {
      const tempFiles = this.courseSearch.filter((d) => {
        for (const key of keys) {
          if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
            return true;
          }
        }
      });
      this.courseList = _.cloneDeep(tempFiles);
      const tempFolder = this.courseSearch.filter((d) => {
        for (const key of keys) {
          if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
            return true;
          }
        }
      });
      this.category = _.cloneDeep(tempFolder);
  } else {
    this.courseList = _.cloneDeep(this.courseSearch);
    this.category = _.cloneDeep(this.catSearch);
  }
} else {
  this.courseList = _.cloneDeep(this.courseSearch);
  this.category = _.cloneDeep(this.catSearch);
}
}

gotoFilter(){
  this.filter = !this.filter
  if(this.filter == false)
  this.courseList = this.courseListDummy
}

getSortData(event){
  console.log(event,"sorting")
  if(event.selectedLevel == 1){
    var property = 'fullname'
  }else{
    var property = 'timecreated'
  }
  var array = this.courseList
  // console.log(this.displayArray,"huhuhuhu")
  var dummyArray = array.sort(this.commonFunctionService.sort(property,event.selectedRadio))
  console.log(dummyArray,"dummyArray")
  this.courseList = array


}

    
  //end new structure code

}



