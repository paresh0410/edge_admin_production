import { Component, ViewEncapsulation, ViewChild, ElementRef} from '@angular/core';
import { Router, NavigationStart, Routes, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BaseChartDirective } from 'ng2-charts/ng2-charts';
import { PreonContentService } from './preoncontent.service';
import { AddEditPreonCourseContentService } from '../addEditPreonCourseContent/addEditPreonCourseContent.service';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { webAPIService } from '../../../../service/webAPIService'
import { webApi } from '../../../../service/webApi'
import { PreondetailsService } from '../addEditPreonCourseContent/Preondetails/Preondetails.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CommonFunctionsService } from '../../../../service/common-functions.service';
import { Card } from '../../../../models/card.model';
import { SuubHeader } from '../../../components/models/subheader.model';
import { noData } from '../../../../models/no-data.model';
@Component({
  selector: 'Preoncontent',
  styleUrls: ['./preoncontent.scss'],
  //template:'content works',
  templateUrl: './preoncontent.html',
  encapsulation: ViewEncapsulation.None,
})
export class PreonContent {
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  header: SuubHeader
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No Courses at this time.",
    desc:"Courses will appear after they are added by the instructor. Courses are a bundle of activities which are designed by the instructors to enable online onboarding of employees or students.",
    titleShow:true,
    btnShow:true,
    descShow:true,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/preonboarding-course',
  }
  query: string = '';
  public getData;
  cat: any;
  category: any = []
  cardModify: Card = {
    flag: 'preonBoardingcourse',
    titleProp : 'fullname',
    image: 'coursePicRef',
    category: 'catName',
    // discrption: 'description',
    courseCompletion: 'courseCompletion',
    compleCount: 'Completioncount',
    totalCount: 'enrollcount',
    manager: 'cereaterName',
    maxpoints: 'maxPoints',
    showCompletedEnroll: true,
    showBottomCategory: true,
    showBottomManager: true,
    showBottomMaximumPoint: true,
    showCourseCompletion: true,
    hoverlable:   true,
    showImage: true,
    option:   true,
    eyeIcon:   true,
    copyIcon: true,
    bottomDiv:   true,
   // bottomDiscription:   true,
    showBottomList:   true,
    bottomTitle:   true,
    btnLabel: 'Edit',
    defaultImage:'assets/images/courseicon.jpg'
  };
  content: any = []
  content1: any = [];
  parentcat: any = []

  search: any;
  errorMsg: string;
  loader: any;
  count:number=8
  catData: any;
  categoryId: any;
  userData: any;
  tenantId: any;

  getCourseUrl = webApi.domain + webApi.url.getCourses;
  getCourseDropdown = webApi.domain + webApi.url.getCourseDropdown;
  getCategoriesurl = webApi.domain + webApi.url.getCategories;

  duplicateCourseUrl = webApi.domain + webApi.url.duplicateCourse;
  nodata: any = false;
  skeleton=false;
  temporary: any;
  searchText: any;
  categoryName: any;

  constructor(private spinner: NgxSpinnerService, private toastr: ToastrService, private detailsService: PreondetailsService,
    //  private toasterService: ToasterService, 
     protected webApiService: webAPIService, private route: ActivatedRoute,
     protected contentservice: PreonContentService, protected passService: AddEditPreonCourseContentService,
     private router: Router,private commonFunctionService: CommonFunctionsService) {
    this.loader = true;
    if (localStorage.getItem('LoginResData')) {
      this.userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', this.userData.data);
      console.log(this.contentservice.page,"page preoncontent")
      // this.userId = this.userData.data.data.id;
      this.tenantId = this.userData.data.data.tenantId;
    }
    this.search = {
      // categoryId: '',
      // courseCode: '',
      // courseCompletion: '',
      // courseId: '',
      // courseLevelId: '',
      // courseOrder: '',
      // coursePicRef: '',
      // courseTypeId: '',
      // craditpoints: '',
      // creatorId: '',
      fullname: '',
      // leadTime: '',
      // shortname: '',
      // summary: '',
      // tags: '',
      // tenantId: '',
      // validFromDate: '',
      // validToDate: '',
      // validationFreq: '',
      // visible: '',
    };

    this.cat = {
      id: ''
    };
    // var passData = this.passService.data;
    var passData = this.contentservice.data;
    console.log(passData);
    if (passData.data != undefined) {
      this.header={
        title:passData.data.categoryName,
        btnsSearch: true,
        searchBar: true,
        catDropdown:true,
        placeHolder:"Search by course name",
        optionValue : 'All Categories',
        dropId:'categoryId',
        dropName:'categoryName',
        dropdownlabel: 'catName',
        drplabelshow: false,
        drpName1: '',
        drpName2: ' ',
        drpName3: '',
        drpName1show: false,
        drpName2show: false,
        drpName3show: false,
        btnName1: '',
        btnName2: '',
        btnName3: '',
        btnAdd: 'Add Course',
        btnName1show: false,
        btnName2show: false,
        btnName3show: false,
        btnBackshow: true,
        btnAddshow: true,
        filter: false,
        showBreadcrumb: true,
        breadCrumbList:[
          {
            'name': 'Preonboarding',
            'navigationPath': '/pages/preonboarding',
          },
          {
            'name': 'Preonboarding Category ',
            'navigationPath': '/pages/preonboarding/preoncourses/Preoncategory',
          },
        ]
      };
      this.categoryId = passData.data.categoryId;
      this.cat.id = this.categoryId;
      this.categoryName=passData.data.categoryName;
      this.tenantId = this.userData.data.data.tenantId;
    } else {
      this.cat.id = "";
      this.header={
        title:'Preoncategory Courses',
        btnsSearch: true,
        searchBar: true,
        catDropdown:true,
        placeHolder:"Search by course name",
        optionValue : 'All Categories',
        dropId:'categoryId',
        dropName:'categoryName',
        dropdownlabel: 'catName',
        drplabelshow: false,
        drpName1: '',
        drpName2: ' ',
        drpName3: '',
        drpName1show: false,
        drpName2show: false,
        drpName3show: false,
        btnName1: '',
        btnName2: '',
        btnName3: '',
        btnAdd: 'Add Course',
        btnName1show: false,
        btnName2show: false,
        btnName3show: false,
        btnBackshow: true,
        btnAddshow: true,
        filter: false,
        showBreadcrumb: true,
        breadCrumbList:[
          {
            'name': 'Preonboarding',
            'navigationPath': '/pages/preonboarding',
          },
        ]
      };
    }
    this.header['id'] =this.cat.id

    this.getCourses();
    this.getCategories();
    this.getCourseDropdownList();
  }
  public param = {
    tId: this.tenantId,
  }
  courseDropdownList: any = [];
  getCourseDropdownList() {
    const param = {
      tId: this.tenantId,
    }
    this.spinner.show();
    const _urlGetCourseDropdown: string = webApi.domain + webApi.url.getCourseDropdown;
    this.commonFunctionService.httpPostRequest(_urlGetCourseDropdown,param)
    //this.detailsService.getDropdownList(param)
    .then(rescompData => {
      // this.loader =false;
      this.spinner.hide();
      this.courseDropdownList = rescompData['data'];
      console.log('Course Dropdown List ', this.courseDropdownList);
    },
      resUserError => {
        // this.loader =false;
        this.spinner.hide();
        this.errorMsg = resUserError
      });
  }

  getCourses() {
    // this.loader = true;
    this.spinner.show();
    let param = {
      cTypeId : 5,
      tId: this.tenantId,
      wfId:null
    };
    // const _urlCourses:string = webApi.domain + webApi.url.getCourses;
    const _urlCourses:string = webApi.domain + webApi.url.get_all_course_optmz;
    this.commonFunctionService.httpPostRequest(_urlCourses,param)
  //  this.contentservice.getCourses(param)
      .then(rescompData => {
        // this.loader =false;
        this.spinner.hide();
        // this.content1 = rescompData['data'];
        this.content1 = rescompData['list'];
        this.temporary =  this.content1
        console.log(this.content1,"content1")
        this.displayArray =[]
        if (this.content1) {
          this.prepareData();
        } else {
          this.nodata = true;
          console.log(this.nodata,"nodata")
        }
        if(this.content1.length==0){
          this.nodata = true;
        }else{
          this.nodata = false;
        }
        console.log('Course Result', this.content1);
        this.loader = false;
      },
        resUserError => {
          console.log(resUserError);
          // if(resUserError.statusText == "Unauthorized"){
          //   this.router.navigate(['/login']);
          // }
          // this.loader = false;
          this.spinner.hide();
          this.errorMsg = resUserError
        });
  }

  getCategories() {
    // this.loader = true;
    let param = {
      tId: this.tenantId,
    };
    this.spinner.show();
    const _urlCategories:string = webApi.domain + webApi.url.getCategories;
    this.commonFunctionService.httpPostRequest(_urlCategories,param)
    //this.contentservice.getCategories(param)
      .then(rescompData => {
        // this.loader =false;
        this.spinner.hide();
        this.parentcat = rescompData['data'];
        console.log('Category Result', this.parentcat);
        // this.loader = false;
        this.skeleton=true;
      },
        resUserError => {
          // this.loader = false;
          this.spinner.hide();
          this.skeleton=true;
          this.errorMsg = resUserError
        });
  }

  prepareData() {
    if (this.categoryId == undefined) {
      this.content = this.content1;
      this.addItems(0,this.sum,'push',this.content1);
      console.log('All Course', this.content);
    } else {
      var tempData =[];
      for (let i = 0; i < this.content1.length; i++) {
        if (this.categoryId == this.content1[i].categoryId) {
          // this.content.push(this.content1[i]);
          tempData.push(this.content1[i]);
        }
      }
      if(this.content1.length == 0){
        this.nodata = true;
        console.log(this.nodata,"nodata")
        this.skeleton=true;
      }
      this.content1 = tempData;
      this.addItems(0,this.sum,'push',this.content1);
      console.log('Filtered Course', this.content1);
    }
    this.loader = false;
    this.skeleton=true;
  }

  doropserch(event) {
    this.content = this.temporary
    this.cat.id = event.id
    console.log(this.cat.id,"cat.id")
    this.displayArray =[];
    if (this.cat.id == undefined || this.cat.id == "") {
       this.content1 = this.content;
      this.sum =50;
      this.addItems(0,this.sum,'push',this.content1)
    } else {
      this.content1 = [];
      var tempData =[]
      for (var i = 0; i < this.content.length; i++) {
        if (this.cat.id == this.content[i].categoryId) {
          tempData.push(this.content[i]);
        }
      }
      this.content1 = tempData;
      this.sum =50;
      this.addItems(0,this.sum,'push',this.content1);

    }
    console.log(this.content,"content")
    if(this.content1.length == 0 ){
      this.nodata = true;
      console.log(this.nodata,"no data")
    }
    else{
      this.nodata = false;
      console.log(this.nodata,"nodata")
    }
  }

  clear() {
    if(this.searchText.length>=3){
      this.nodata=false
    this.search.fullname = '';
    this.displayArray = []
    this.sum = 50;
    this.addItems(0, this.sum, 'push', this.content1);
    }
    else{
    this.search.fullname = '';

    }
  }

  public addeditcontent(data, id) {
    var data1 = {
      data: data,
      id: id,
      catId: this.categoryId,
      categoryName:this.categoryName,
      courseDropdowns: this.courseDropdownList
    }
    if (data == undefined) {
      this.passService.data = data1;
      this.router.navigate(['/pages/preonboarding/preoncourses/addEditPreonCourseContent']);
      console.log("Data passed to service" + data1);
      console.log("ID Passed" + data1.id + " " + data1.data);
    } else {
      this.passService.data = data1;
      this.router.navigate(['/pages/preonboarding/preoncourses/addEditPreonCourseContent']);
    }
  }

  gotoCardaddedit(data) {
    var data1 = {
      data: data,
      id: 1,
      catId: this.categoryId,
      categoryName:this.categoryName,
      courseDropdowns: this.courseDropdownList
    }
    if (data == undefined) {
      this.passService.data = data1;
      this.router.navigate(['/pages/preonboarding/preoncourses/addEditPreonCourseContent']);
      console.log("Data passed to service" + data1);
      console.log("ID Passed" + data1.id + " " + data1.data);
    } else {
      this.passService.data = data1;
      this.router.navigate(['/pages/preonboarding/preoncourses/addEditPreonCourseContent']);
    }
  }

  back() {
    // this.router.navigate(['/pages/plan/courses']);
    // this.router.navigate(['/pages/learning']);
    window.history.back()
    this.contentservice.data.data = undefined;
  }

  deleteCourseModal: boolean = false;
  deleteCourseData: any;
  deleteContent(course) {
    console.log('Course content', course);
    this.deleteCourseData = course;
    this.deleteCourseModal = true;
  }

  deleteContentAction(actionType) {
    if (actionType == true) {
      // this.removeCourse(this.deleteCourseData);
      this.closeDeleteContentModal();
    } else {
      this.closeDeleteContentModal();
    }
  }

  closeDeleteContentModal() {
    // console.log('Course content',course);
    this.deleteCourseModal = false;
  }

  disableCourse: boolean = false;
  disableContent(item, disableStatus) {
    this.disableCourse = !this.disableCourse;
  }

  // enable disable course
  disableCourseVisible: boolean = false;
  visibiltyRes: any;
  enableDisableCourseModal: boolean = false;
  enableDisableCourseData: any;
  enableCourse: boolean = false;
  courseDisableIndex: any;

  disableCourseVisibility(currentIndex, courseData, status) {
    this.enableDisableCourseData = courseData;
    this.enableDisableCourseModal = true;
    this.courseDisableIndex = currentIndex;
    // for(let i = 0 ; i < this.content.length ;i++)
    // {
    //   if(this.content[i].courseId  ==  this.enableDisableCourseData.courseId)
    //   {

    //     if(this.content[i].visible == 0){
    //       this.enableCourse = false;
    //     }else{
    //       this.enableCourse = true;
    //     }
    //   }
    // }
    if (this.enableDisableCourseData.visible == 0) {
      this.enableCourse = false;
    } else {
      this.enableCourse = true;
    }

    // if(this.content[currentIndex].visible == 0){
    //   this.enableCourse = false;
    // }else{
    //   this.enableCourse = true;
    // }
    // for(let i=0;i<this.content.length;i++)
    // {

    // }
  }

  clickTodisable(courseData) {
    if (courseData.visible == 1) {
      this.enableCourse = true;
      this.enableDisableCourseData = courseData;
      this.enableDisableCourseModal = true;
    } else {
      this.enableCourse = false;
      this.enableDisableCourseData = courseData;
      this.enableDisableCourseModal = true;
    }
  }

  enableDisableCourse(courseData) {
    this.spinner.show();
    var visibilityData = {
      courseId: courseData.courseId,
      visible: courseData.visible
    }
    const _urlDisableCourse:string = webApi.domain + webApi.url.disableCourse;
    this.commonFunctionService.httpPostRequest(_urlDisableCourse,visibilityData)
   // this.contentservice.disableCourse(visibilityData)
      .then(rescompData => {
        // this.loader =false;
        this.spinner.hide();
        this.visibiltyRes = rescompData;
        console.log('Course Visibility Result', this.visibiltyRes);
        if (this.visibiltyRes.type == false) {
          // var courseUpdate: Toast = {
          //   type: 'error',
          //   title: "Course",
          //   body: "Unable to update visibility of course.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          this.closeEnableDisableCourseModal();
          // this.toasterService.pop(courseUpdate);

          this.toastr.error( 'Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        } else {
          // var courseUpdate: Toast = {
          //   type: 'success',
          //   title: "Course",
          //   body: this.visibiltyRes.data,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          this.closeEnableDisableCourseModal();
          // this.toasterService.pop(courseUpdate);


          this.toastr.success(this.visibiltyRes.data, 'Success', {
            closeButton: false
           });
        }
      },
        resUserError => {
          // this.loader =false;
          this.spinner.hide();
          this.errorMsg = resUserError;
          this.closeEnableDisableCourseModal();
        });
  }

  enableDisableCourseAction(actionType) {
    // if(actionType == true){
    //   for(let i = 0 ; i < this.content.length ;i++)
    //   {
    //     if(this.content[i].courseId  ==  this.enableDisableCourseData.courseId)
    //     {
    //       this.courseDisableIndex = i;
    //       if(this.content[this.courseDisableIndex].visible == 1){
    //         this.content[this.courseDisableIndex].visible = 0;
    //         var courseData = this.content[this.courseDisableIndex];
    //         this.enableDisableCourse(courseData);

    //       }else{
    //         this.content[this.courseDisableIndex].visible = 1;
    //         var courseData = this.content[this.courseDisableIndex];
    //         this.enableDisableCourse(courseData);
    //       }
    //     }
    //     }
    //   }else{
    //     this.closeEnableDisableCourseModal();
    //   }

    if (actionType == true) {

      if (this.enableDisableCourseData.visible == 1) {
        this.enableDisableCourseData.visible = 0;
        var courseData = this.enableDisableCourseData;
        this.enableDisableCourse(courseData);

      } else {
        this.enableDisableCourseData.visible = 1;
        var courseData = this.enableDisableCourseData;
        this.enableDisableCourse(courseData);
      }
    } else {
      this.closeEnableDisableCourseModal();
    }
  }

  closeEnableDisableCourseModal() {
    this.enableDisableCourseModal = false;
  }

  formDataCourse: any;
  duplicateContent(courseData) {
    this.formDataCourse = {
      courseId:courseData.courseId,
      fullname: '',
      tenantId: courseData.tenantId,
    }
  }

  duplicateCourseModal: boolean = false;
  duplicateCourseData: any;
  duplicateCourseContent(course) {
    console.log('Course content', course);
    this.duplicateCourseData = course;
    this.duplicateCourseModal = true;
    this.duplicateContent(course);
  }

  copyCard(course) {
    console.log('Course content', course);
    this.duplicateCourseData = course;
    this.duplicateCourseModal = true;
    this.duplicateContent(course);
  }


  duplicateCourseAction(actionType) {
    if (actionType == true) {
      this.makeDuplicateCourseDataReady();
      this.closeDuplicateCourseModal();
    } else {
      this.closeDuplicateCourseModal();
    }
  }

  closeDuplicateCourseModal() {
    this.duplicateCourseModal = false;
  }

  formattedDupliTags: any = null;
  makeTagDataReady() {
    var tagsData = this.formDataCourse.tags;
    if (tagsData.length > 0) {
      var tagsString = '';
      for (let i = 0; i < tagsData.length; i++) {
        var tag = tagsData[i];
        if (tagsString != "") {
          tagsString += "|";
        }
        if (tag.value) {
          if (String(tag.value) != "" && String(tag.value) != "null") {
            tagsString += tag.value;
          }
        } else {
          if (String(tag) != "" && String(tag) != "null") {
            tagsString += tag;
          }
        }
      }
      this.formattedDupliTags = tagsString;
    }
  }

  creditPointsStr: any;
  makeDuplicateCourseDataReady() {
    var courseData = {
      courseId:this.formDataCourse.courseId,
      fullname: this.formDataCourse.fullname,
      userMod: this.userData.data.data.id,
    }
    console.log('Duplicate course data ', courseData);

    this.duplicateCourse(courseData);
  }

  duplicateCourseRes: any;
  duplicateCourse(course) {
    const _urlDublicateCourse:string = webApi.domain + webApi.url.duplicateCourse;
    this.commonFunctionService.httpPostRequest(_urlDublicateCourse,course)
    //this.contentservice.dublicateCourse(course)
      .then(rescompData => {
        this.loader = false;
        var temp: any = rescompData;
        this.duplicateCourseRes = temp.data;
        if (temp == "err") {
          // this.notFound = true;
          // var courseDuplicate: Toast = {
          //   type: 'error',
          //   title: "Course",
          //   body: "Unable to duplicate course.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(courseDuplicate);

          this.toastr.error( 'Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        } else if (temp.type == false) {
          // var courseDuplicate: Toast = {
          //   type: 'error',
          //   title: "Course",
          //   body: this.duplicateCourseRes[0].msg,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(courseDuplicate);

          this.toastr.error( 'Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        } else {
          // var courseDuplicate: Toast = {
          //   type: 'success',
          //   title: "Course",
          //   body: this.duplicateCourseRes[0].msg,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(courseDuplicate);

          this.toastr.success(this.duplicateCourseRes[0].msg, 'Success', {
            closeButton: false
           });
          this.getCourses();
        }
        console.log('Course duplicate Result ', this.duplicateCourseRes);
        // this.cdf.detectChanges();
      },
        resUserError => {
          this.loader = false;
          this.errorMsg = resUserError;
        });
  }

  onsearch(event){
    if(event){
      this.search.fullname =  event.target.value;

    }
  }
  ngOnDestroy(){
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0){
      e1[0].classList.add('with-scroll');
    }
  }
  ngAfterContentChecked() {
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0)
    {
      e1[0].classList.remove('with-scroll');
    }

   }
   conentHeight = '0px';
   offset: number = 100;
   ngOnInit() {
     this.conentHeight = '0px';
     const e = document.getElementsByTagName('nb-layout-column');
     console.log(e[0].clientHeight);
     // this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
     if (e[0].clientHeight > 1300) {
       this.conentHeight = '0px';
       this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
     } else {
       this.conentHeight = String(e[0].clientHeight) + 'px';
     }
   }
 
   sum = 40;
   addItems(startIndex, endIndex, _method, asset) {
     for (let i = startIndex; i < endIndex; ++i) {
       if (asset[i]) {
         this.displayArray[_method](asset[i]);
        // console.log(this.displayArray,"this.displayArray")
       } else {
         break;
       }
 
       // console.log("NIKHIL");
     }
   }
   // sum = 10;
   onScroll(event) {
 
     let element = this.myScrollContainer.nativeElement;
     // element.style.height = '500px';
     // element.style.height = '500px';
     // Math.ceil(element.scrollHeight - element.scrollTop) === element.clientHeight
 
     if (element.scrollHeight - element.scrollTop - element.clientHeight < 5) {
       if (this.newArray.length > 0) {
         if (this.newArray.length >= this.sum) {
           const start = this.sum;
           this.sum += 50;
           this.addItems(start, this.sum, 'push', this.newArray);
         } else if ((this.newArray.length - this.sum) > 0) {
           const start = this.sum;
           this.sum += this.newArray.length - this.sum;
           this.addItems(start, this.sum, 'push', this.newArray);
         }
       } else {
         if (this.content1.length >= this.sum) {
           const start = this.sum;
           this.sum += 500;
           this.addItems(start, this.sum, 'push', this.content1);
         } else if ((this.content1.length - this.sum) > 0) {
           const start = this.sum;
           this.sum += this.content1.length - this.sum;
           this.addItems(start, this.sum, 'push', this.content1);
         }
        }
 
 
       console.log('Reached End');
     }
   }

    // this function is use to dynamicsearch on table
    newArray =[];
    displayArray =[];
  searchQuestion(event) {
    var temData = this.content1;
    var val = event.target.value.toLowerCase();
    var keys = [];
    this.searchText=val;
    if(event.target.value == '')
    {
      this.displayArray =[];
      this.newArray = [];
      this.sum = 50;
      this.addItems(0, this.sum, 'push', this.content1);
    }else{
         // filter our data
    if (temData.length > 0) {
      keys = Object.keys(temData[0]);
    }
    if(val.length>=3||val.length==0){
    var temp = temData.filter((d) => {
      for (const key of keys) {
        if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
          return true;
        }
      }
    });
    if(temp.length==0){
      this.nodata=true;
      this.noDataVal={
        margin:'mt-5',
        imageSrc: '../../../../../assets/images/no-data-bg.svg',
        title:"Sorry we couldn't find any matches please try again",
        desc:".",
        titleShow:true,
        btnShow:false,
        descShow:false,
        btnText:'Learn More',
        btnLink:''
      }
    }
    // update the rows
    this.displayArray =[];
    this.newArray = temp;
    this.sum = 50;
  }
    this.addItems(0, this.sum, 'push', this.newArray);
}
}
}

