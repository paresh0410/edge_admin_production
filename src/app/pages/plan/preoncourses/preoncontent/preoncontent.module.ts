import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MyDatePickerModule } from 'mydatepicker';
import { FilterPipeModule  } from 'ngx-filter-pipe';
import { PreonContent } from './preoncontent';
import { PreonContentService } from './preoncontent.service';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { TagInputModule } from 'ngx-chips';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgxContentLoadingModule } from 'ngx-content-loading';
import { ComponentModule } from '../../../../component/component.module';

// import { AddeditccategoryService } from './addEditCategory/addEditCategory.service';

@NgModule({
  imports: [
    AngularMultiSelectModule,
    CommonModule,
    FormsModule,
    FilterPipeModule,
    TagInputModule,
    NgxSkeletonLoaderModule,
    NgxContentLoadingModule, 
    ComponentModule,
  ],
  declarations: [
    PreonContent
  ],
  providers: [
    PreonContentService,
    // AddeditccategoryService
  ]
})

export class PreonContentModule {}
