import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, RequestOptions, Request } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AppConfig } from '../../../../../app.module';

import { webAPIService } from '../../../../../service/webAPIService'
import { webApi } from '../../../../../service/webApi'
import { HttpClient } from "@angular/common/http";
@Injectable()

export class PreonmodulesService {

  public data: any;
  request: Request;
   userData:any;
    tenantId:any;
  // private _urlGetCat:string = "/api/edge/category/getCategory"
  // private _urlAddEditCourse:string = "/api/edge/course/addUpdateCourse"
  // private _urlGetTopics:string = "/api/edge/course/getTopic";
  // private _urlAddEditTopics:string = "/api/edge/course/addUpdateTopic";
  // private _urlGetResource:string = "/api/edge/course/getResource";
  // private _urlAddEditResource:string = "/api/edge/course/addUpdateResource";
  // private _urlGetModules:string = "/api/edge/course/getModules";

  private _urlGetFeedback: string = webApi.domain + webApi.url.searchFeedback;
  private _urlGetQuiz: string = webApi.domain + webApi.url.searchQuiz;
  private _urlAddEditModule: string = webApi.domain + webApi.url.addEditModule;
  private _urlGetCourseModules: string = webApi.domain + webApi.url.getCourseModules;
  private _urlGetCourseModulesActivity: string = webApi.domain + webApi.url.getCourseModulesActivity;
  private _urlGetCourseModulesActivityDrop: string = webApi.domain + webApi.url.getModuleActivityDrop;
  private _urlGetActivityContent: string = webApi.domain + webApi.url.contentSearch;
  private _urlDeleteActivity: string = webApi.domain + webApi.url.deleteActivity;
  private _urlDeleteModule: string = webApi.domain + webApi.url.deleteModule;
  private _urlGetroleDropdownSetting: string = webApi.domain + webApi.url.roleDrop;
  private _urlhandshakeDamUserVerify: string = webApi.domain + webApi.url.handshakeDamUserVerify;
  private _urlhandshakeTokenUpdate: string = webApi.domain + webApi.url.handshakeTokenUpdate;
  private _urlhandshakeDamAsset: string = webApi.domain + webApi.url.handshakeDamAsset;
  private _urlEnableDisableModule: string = webApi.domain + webApi.url.disable_module;
  private _urlEnableDisableActivity: string = webApi.domain + webApi.url.disable_activity;


  constructor(@Inject('APP_CONFIG_TOKEN') private config: AppConfig, private _http: Http, private http1: HttpClient) {
    //this.busy = this._http.get('...').toPromise();


  }

  // new services start //
  addEditMod(moduleData) {
    // return this._http.post(this._urlAddEditModule,moduleData)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);

    return new Promise(resolve => {
      this.http1.post(this._urlAddEditModule, moduleData)
        //.map(res => res.json())

        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  /*****Dam Handshake*****/

  generatingToken(userData) {
    return new Promise(resolve => {
      this.http1.post(this._urlhandshakeDamUserVerify, userData)

        .subscribe(data => {
          resolve(data);
          console.log('Data token response ->', data);
        },
          err => {
            resolve('err');
          });
    });
  }
  updatingToken(userDetail) {
    return new Promise(resolve => {
      this.http1.post(this._urlhandshakeTokenUpdate, userDetail)

        .subscribe(data => {
          resolve(data);
          console.log('Data token response ->', data);
        },
          err => {
            resolve('err');
          });
    });
  }

  ApprovedDamHanshakeData(data) {
    // return this._http.post(this._urlGetActivityContent,data)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);
    return new Promise(resolve => {
      this.http1.post(this._urlhandshakeDamAsset, data)
        //.map(res => res.json())

        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  getallDropdown(param) {
    return new Promise(resolve => {
      this.http1.post(this._urlGetroleDropdownSetting, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  getCourseMod(courseData) {
    // return this._http.post(this._urlGetCourseModules,courseData)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);
    return new Promise(resolve => {
      this.http1.post(this._urlGetCourseModules, courseData)
        //.map(res => res.json())

        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }

  getCourseModActivity(activityData) {
    // return this._http.post(this._urlGetCourseModulesActivity,activityData)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);
    return new Promise(resolve => {
      this.http1.post(this._urlGetCourseModulesActivity, activityData)
        //.map(res => res.json())

        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  getModActivityDrop() {
    // return this._http.post(this._urlGetCourseModulesActivityDrop,this.param)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);
    if(localStorage.getItem('LoginResData')){
      this.userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', this.userData.data);
      // this.userId = this.userData.data.data.id;
      this.tenantId = this.userData.data.data.tenantId;
   }
   let param: any = {
    tId: this.tenantId,
    courseTypeId: 5,
  };
    return new Promise(resolve => {
      this.http1.post(this._urlGetCourseModulesActivityDrop, param)
        //.map(res => res.json())

        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }

  searchActivityContent(data) {
    // return this._http.post(this._urlGetActivityContent,data)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);
    return new Promise(resolve => {
      this.http1.post(this._urlGetActivityContent, data)
        //.map(res => res.json())

        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  searchcoursequizContent(data) {
    // return this._http.post(this._urlGetActivityContent,data)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);
    return new Promise(resolve => {
      this.http1.post(this._urlGetQuiz, data)
        //.map(res => res.json())

        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  searchcoursefeedbackContent(data) {
    // return this._http.post(this._urlGetActivityContent,data)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);
    return new Promise(resolve => {
      this.http1.post(this._urlGetFeedback, data)
        //.map(res => res.json())

        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  deleteActivity(data) {
    // return this._http.post(this._urlDeleteActivity,data)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);
    return new Promise(resolve => {
      this.http1.post(this._urlDeleteActivity, data)
        //.map(res => res.json())

        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }

  deleteModule(data) {
    // return this._http.post(this._urlDeleteModule,data)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);
    return new Promise(resolve => {
      this.http1.post(this._urlDeleteModule, data)
        //.map(res => res.json())

        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }


  moduleEnableDisable(param) {
    return new Promise(resolve => {
      this.http1.post(this._urlEnableDisableModule, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  enableDisableActivity(visibleData) {
    let url: any = this._urlEnableDisableActivity;
    return new Promise(resolve => {
      this.http1.post(url, visibleData)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  // new services end //

  /* getcompletion(user){
     let url:any = `${this.config.FINAL_URL}`+this._urlGetCat;
     // return this._http.post(url,user)
     //   .map((response:Response) => response.json())
     //   .catch(this._errorHandler);
     return new Promise(resolve => {
       this.http1.post(url,user)
       //.map(res => res.json())

       .subscribe(data => {
           resolve(data);
       },
       err => {
           resolve('err');
         });
       });

   }

   getCategories(){
     let url:any = `${this.config.FINAL_URL}`+this._urlGetCat;
     // return this._http.post(url,{})
     //   .map((response:Response) => response.json())
       //   .catch(this._errorHandler);
       return new Promise(resolve => {
         this.http1.post(url,{})
         //.map(res => res.json())

         .subscribe(data => {
             resolve(data);
         },
         err => {
             resolve('err');
           });
         });

   }

   createUpdateCourse(course){
     let url:any = `${this.config.FINAL_URL}`+this._urlAddEditCourse;
     // let headers = new Headers({ 'Content-Type': 'application/json' });
     //  let options = new RequestOptions({ headers: headers });
     // return this._http.post(url,course,options)
     //   .map((response:Response) => response.json())
     //   .catch(this._errorHandler);

     return new Promise(resolve => {
       this.http1.post(url,course)
       //.map(res => res.json())

       .subscribe(data => {
           resolve(data);
       },
       err => {
           resolve('err');
         });
       });
   }

   getTopics(courseId){
     let url:any = `${this.config.FINAL_URL}`+this._urlGetTopics;
     // return this._http.post(url,courseId)
     //   .map((response:Response) => response.json())
     //   .catch(this._errorHandler);
     return new Promise(resolve => {
       this.http1.post(url,courseId)
       //.map(res => res.json())

       .subscribe(data => {
           resolve(data);
       },
       err => {
           resolve('err');
         });
       });

   }

   addEditTopics(topicData){
     let url:any = `${this.config.FINAL_URL}`+this._urlAddEditTopics;
     // return this._http.post(url,topicData)
     //   .map((response:Response) => response.json())
     //   .catch(this._errorHandler);
     return new Promise(resolve => {
       this.http1.post(url,topicData)
       //.map(res => res.json())

       .subscribe(data => {
           resolve(data);
       },
       err => {
           resolve('err');
         });
       });

   }

   getResource(resourceData){
     let url:any = `${this.config.FINAL_URL}`+this._urlGetResource;
     // return this._http.post(url,resourceData)
     //   .map((response:Response) => response.json())
     //   .catch(this._errorHandler);
     return new Promise(resolve => {
       this.http1.post(url,resourceData)
       //.map(res => res.json())

       .subscribe(data => {
           resolve(data);
       },
       err => {
           resolve('err');
         });
       });


   }

   addEditResource(ResourceData){
     let url:any = `${this.config.FINAL_URL}`+this._urlAddEditResource;
     // return this._http.post(url,ResourceData)
     //   .map((response:Response) => response.json())
     //   .catch(this._errorHandler);

     return new Promise(resolve => {
       this.http1.post(url,ResourceData)
       //.map(res => res.json())

       .subscribe(data => {
           resolve(data);
       },
       err => {
           resolve('err');
         });
       });
   }

   getModules(){
     let url:any = `${this.config.FINAL_URL}`+this._urlGetModules;
     // return this._http.post(url,{})
     //   .map((response:Response) => response.json())
     //   .catch(this._errorHandler);
     return new Promise(resolve => {
       this.http1.post(url,{})
       //.map(res => res.json())

       .subscribe(data => {
           resolve(data);
       },
       err => {
           resolve('err');
         });
       });

   }*/


  _errorHandler(error: Response) {
    console.error(error);
    return Observable.throw(error || "Server Error")
  }

}
