import {Injectable, Inject} from '@angular/core';
import {Http, Response, Headers, RequestOptions, Request} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppConfig} from '../../../../../app.module';
import { webApi} from '../../../../../service/webApi';
import { HttpClient } from "@angular/common/http";

@Injectable()

export class PreonGamificationService {

  public data: any;
  request: Request;

  private _url:string = "";
  private _urlInsertladderUrl:string = webApi.domain + webApi.url.insertladdertocourse;
  private _urlGetCourseLadders:string = webApi.domain + webApi.url.getcoursegamification;


  constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private _http: Http,private _httpClient:HttpClient){
      //this.busy = this._http.get('...').toPromise();
  }

  getData() {
     let url:any = `${this.config.FINAL_URL}`+this._url;
     // let url:any = `${this.config.DEV_URL}`+this._urlFilter;
     let headers = new Headers({ 'Content-Type': 'application/json' });
     let options = new RequestOptions({ headers: headers });
     //let body = JSON.stringify(user);
     return this._http.post(url, options ).map((res: Response) => res.json());
  }

  insertladder(param){
  //  let url:any = this._urlInsertladderUrl;
  //  return this._http.post(url,param)
  //      .map((response:Response)=>response.json())
  //      .catch(this._errorHandler);

       return new Promise(resolve => {
        this._httpClient.post(this._urlInsertladderUrl, param)
        //.map(res => res.json())
        .subscribe(data => {
            resolve(data);
        },
        err => {
            resolve('err');
        });
    });
 } 

 getcourseladders(param){
  //  let url:any = this._urlGetCourseLadders;
  //  return this._http.post(url,param)
  //      .map((response:Response)=>response.json())
  //      .catch(this._errorHandler);
       return new Promise(resolve => {
        this._httpClient.post(this._urlGetCourseLadders, param)
        //.map(res => res.json())
        .subscribe(data => {
            resolve(data);
        },
        err => {
            resolve('err');
        });
    });
 } 

  _errorHandler(error: Response){
     console.error(error);
     return Observable.throw(error || "Server Error")
  }


}
