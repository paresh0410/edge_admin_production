import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentModule } from '../../../../component/component.module';
import { NgxEchartsModule } from 'ngx-echarts';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ThemeModule } from '../../../../@theme/theme.module';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { MyDatePickerModule } from 'mydatepicker';
import { FilterPipeModule  } from 'ngx-filter-pipe';
import { TabsModule } from "ngx-tabs";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { TranslateModule } from '@ngx-translate/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NbTabsetModule } from '@nebular/theme';
import { TagInputModule } from 'ngx-chips';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { AddEditPreonCourseContent } from './addEditPreonCourseContent';
import { AddEditPreonCourseContentService } from './addEditPreonCourseContent.service'
import { JoditAngularModule } from 'jodit-angular';
import { PreonrewardsComponent } from './Preonrewards/Preonrewards.component';
import { PreonrewardsService } from './Preonrewards/Preonrewards.service';
import { PreonengageComponent } from './Preonengage/Preonengage.component';
import { PreonengageService } from './Preonengage/Preonengage.service';


import { PreonenrolmentComponent } from './Preonenrolment/Preonenrolment.component';
import { PreonenrolService } from './Preonenrolment/Preonenrolment.service';

import { PreondetailsComponent } from './Preondetails/Preondetails.component';
import { PreondetailsService } from './Preondetails/Preondetails.service';

import { PreonmodulesComponent } from './Preonmodules/Preonmodules.component';
import { PreonmodulesService } from './Preonmodules/Preonmodules.service';

import { PreonGamificationComponent } from './Preongamification/Preongamification.component';
import { PreonGamificationService } from './Preongamification/Preongamification.service';

import { QuillModule } from 'ngx-quill'
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'

import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import {MatSelectModule} from '@angular/material/select';
// import { AddEditActivityComponent } from '../../add-edit-activity/add-edit-activity.component';

@NgModule({
  imports: [
    ThemeModule,
    NgxEchartsModule,
    NgxMyDatePickerModule.forRoot(),
    TabsModule,
    ReactiveFormsModule,
    AngularMultiSelectModule,
    FormsModule,
    TranslateModule.forRoot(),
    NgxDatatableModule,
    Ng2SmartTableModule,
    NbTabsetModule,
    FilterPipeModule,
    MyDatePickerModule,
    QuillModule,
    TagInputModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    BsDatepickerModule.forRoot(),
    DatepickerModule.forRoot(),
    TooltipModule.forRoot(),
    TimepickerModule.forRoot(),
    MatSelectModule,
    JoditAngularModule,
    ComponentModule,
  ],
  declarations: [
    AddEditPreonCourseContent,
    PreonenrolmentComponent,
    PreondetailsComponent,
    PreonmodulesComponent,
    PreonrewardsComponent,
    PreonengageComponent,
    PreonGamificationComponent,
    // AddEditActivityComponent,
  ],
  providers: [
    //slikgridDemoService,
    AddEditPreonCourseContentService,
    PreonenrolService,
    PreondetailsService,
    PreonmodulesService,
    PreonrewardsService,
    PreonengageService,
    PreonGamificationService,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA,
  ]
})
export class AddEditPreonCourseContentModule { }
