
import { Host, ChangeDetectionStrategy, ChangeDetectorRef, Component, ViewEncapsulation, Directive, forwardRef, Attribute, OnChanges, SimpleChanges, Input, ViewChild, ViewContainerRef, OnInit } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AddEditPreonCourseContent } from '../addEditPreonCourseContent';
import { AddEditPreonCourseContentService } from '../addEditPreonCourseContent.service';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { NgbCalendar, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { Router, NavigationStart, ActivatedRoute } from '@angular/router';
import { PreonengageService } from './Preonengage.service';
import { NgxSpinnerService } from 'ngx-spinner';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { arrayObjectToCsvFormatter } from 'angular-slickgrid/app/modules/angular-slickgrid/formatters/arrayObjectToCsvFormatter';
//import { extractStyleParams } from '@angular/animations/browser/src/util';
import { NotificationtemplateServiceService } from '../../../notification-templates/notification-templates.service'
import { ToastrService } from 'ngx-toastr';
import { DataSeparatorService } from '../../../../../service/data-separator.service';
import { noData } from '../../../../../models/no-data.model';
@Component({
	selector: 'Preon-course-engage',
	templateUrl: './Preonengage.html',
	styleUrls: ['./Preonengage.scss'],
	encapsulation: ViewEncapsulation.None
})


export class PreonengageComponent {

	@ViewChild('rulesTable') rulesTable: any;
	@ViewChild(DatatableComponent) tablenoti: DatatableComponent;

	@Input() inpdata: any;
	noDataVal:noData={
		margin:'mt-3',
		imageSrc: '../../../../../assets/images/no-data-bg.svg',
		title:"No notification at this time.",
		desc:'',
		titleShow:true,
		btnShow:false,
		descShow:false,
		btnText:'Learn More',
		btnLink:'',
	  }
	rows: any = [];
	columns: any = [];
	addNot: boolean = false;
	temp: any = [];
	selected = [];
	colorTheme = 'theme-dark-blue';
  tagsArray = [];
  init = false;
	bsConfig: Partial<BsDatepickerConfig>
	config = {
		height: '200px',
		uploader: {
		  insertImageAsBase64URI: true,
		},
		allowResizeX: false,
		allowResizeY: false,
		placeholder: 'Edit Template',
		limitChars: 3000,
	  };


	addEngagePopup: boolean = false;
	showdate: boolean = false;
	showdays: boolean = false;
	selectMethod: String;
	formdataNotifications: any = {};
	eventsDrop: any = [];
	templateDrop: any = [];

  CourseNotification = {
    name:'',
    desc:'',
    event:'',
    lnmsg: false,
    lnmsgtemp:'',
    reviewTextSms: false,
    editSmsTemplateData: '',
    lnnot: false,
    lnnottemp: '',
    reviewTextNot: false,
    editNotTemplateData: '',
    lnemail: false,
    lnemailTemp:'',
    reviewTextEmail:false,
    emailSubjectData:'',
    editEmailTemplateData:'',
    smsSubjectData:'',
    NotSubjectData:'',
  };
	courseId: any;
	optId: any;
	areaId=2;
	userId: any;
	CourseNoti: any;
	warningModal: boolean = false;
	addFlag: boolean = false;
	userData;
	tenantId: any;
	searchText: any;
	searchvalue: any = {
		value: '',
	  };
	  notificationLabel: any = [
		{ labelname: 'Name', bindingProperty: 'rulename', componentType: 'text' },
		{ labelname: 'Description', bindingProperty: 'description', componentType: 'text' },
		{ labelname: 'Modes', bindingProperty: 'cmodes', componentType: 'text' },
		{ labelname: 'Events', bindingProperty: 'notEvent', componentType: 'text' },
		{ labelname: 'EDIT', bindingProperty: 'tenantId', componentType: 'icon' },
  ]
	notiTitle: string;
	noData: boolean;
  constructor(@Host() private parent_Comp: AddEditPreonCourseContent,
  // private toasterService: ToasterService,
  private dataSeparatorService: DataSeparatorService,
		private calendar: NgbCalendar, private engageservice: PreonengageService, private toastr: ToastrService,
		private addEditCourseService: AddEditPreonCourseContentService, private router: Router,
		private route: ActivatedRoute, public cdf: ChangeDetectorRef, private spinner: NgxSpinnerService, private notitempservice: NotificationtemplateServiceService) {
		this.spinner.show();

		if (this.addEditCourseService.data != undefined) {
			console.log('this.addEditCourseService.data for Edit:', this.addEditCourseService.data);
			this.ChooseAddEdit(this.addEditCourseService.data);
		}



		if (localStorage.getItem('LoginResData')) {
			var userData = JSON.parse(localStorage.getItem('LoginResData'));
			console.log('userData', userData.data);
			this.userId = userData.data.data.id;
			console.log('userId', userData.data.data.id);
			this.tenantId = userData.data.data.tenantId;
      console.log('userId', userData.data.data.id);
			this.fetcheventsdropdown();
			this.fetchtemplatedropdown();
		}
		if (localStorage.getItem('Notification')) {
			this.userData = JSON.parse(localStorage.getItem('Notification'));
			console.log('userData', this.userData);
		  }




	}

	ngOnChanges(changes: SimpleChanges): void {
		if(this.inpdata === 'notif') {
		  this.openAddEdit();
		}
	}

	ChooseAddEdit(data) {
		this.optId = data.id;

		if (this.optId == 0) {
			this.courseId = this.addEditCourseService.courseId;
			this.getExistingCourseNotification();
		} else if (this.optId == 1) {
			this.courseId = this.addEditCourseService.data.data.courseId;
			this.tenantId = this.addEditCourseService.data.data.tenantId;
			this.getExistingCourseNotification();

		}
	}


  getExistingCourseNotification() {
    let param = {
      'cId': this.courseId,
	  'areaId':this.areaId,
	  "tid": this.tenantId
	  
    }

    this.engageservice.getCourseNotifications(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        // var CourseNoti = result.data[0];
        this.CourseNoti = result['data'][0] ;
        if (result['type'] == true) {
          console.log('ExistingCourseNotifications:', this.CourseNoti);
		  this.rows = this.CourseNoti;
		  this.rows.length!=0?this.noData=false:this.noData=true;
        //   this.rows = this.CourseNoti.length== 0 ? [] :[...this.rows];

          console.log('this.rows', this.rows);

        //   this.columns = [
        //     { prop: 'rulename', name: 'Name' },
        //     { prop: 'description', name: 'Description' },
        //     // { prop: 'modeIds', name: 'Method'},
        //     { prop: 'cmodes', name: 'Modes' },
        //     { prop: 'notEvent', name: 'Events' }

        //   ];
          this.cdf.detectChanges();
        } else {
          // this.loader = false;
          // var toast: Toast = {
          // 	type: 'error',
          // 	//title: "Server Error!",
          // 	body: "Something went wrong.Please contact site administrator",
          // 	showCloseButton: true,
          // 	timeout: 2000
          // };
          // this.toasterService.pop(toast);

          this.toastr.error('Please contact site administrator and <div (click)="giveFeedback()"> click here <div> to leave your feedback', 'Error', {
            timeOut: 0,
            closeButton: true
          });

        }


      }, error => {
        this.spinner.hide();
        // var toast: Toast = {
        // 	type: 'error',
        // 	//title: "Server Error!",
        // 	body: "Something went wrong.Please contact site administrator",
        // 	showCloseButton: true,
        // 	timeout: 2000
        // };
        // this.toasterService.pop(toast);

        this.toastr.error('Please contact site administrator and <div (click)="giveFeedback()"> click here <div> to leave your feedback', 'Error', {
          timeOut: 0,
          closeButton: true
        });
      });
  }
	// getExistingCourseNotification() {
	// 	let param = {
	// 		"cId": this.courseId,
	// 		"tid": this.tenantId,
	// 	}

	// 	this.engageservice.getCourseNotifications(param)
	// 		.then(rescompData => {
	// 			this.spinner.hide();
	// 			var result = rescompData;
	// 			// var CourseNoti = result.data[0];
	// 			this.CourseNoti = result['data'][0];
	// 			if (result['type'] == true) {
	// 				console.log('ExistingCourseNotifications:', this.CourseNoti);
	// 				this.rows = this.CourseNoti;
	// 				this.rows = [...this.rows];

	// 				console.log('this.rows', this.rows);

	// 				this.columns = [
	// 					{ prop: 'rulename', name: 'Name' },
	// 					{ prop: 'description', name: 'Description' },
	// 					// { prop: 'modeIds', name: 'Method'},
	// 					{ prop: 'cmodes', name: 'Modes' },
	// 					{ prop: 'notEvent', name: 'Events' }

	// 				];
	// 				this.cdf.detectChanges();
	// 			} else {
	// 				// this.loader = false;
	// 				// var toast: Toast = {
	// 				// 	type: 'error',
	// 				// 	//title: "Server Error!",
	// 				// 	body: "Something went wrong.Please contact site administrator",
	// 				// 	showCloseButton: true,
	// 				// 	timeout: 2000
	// 				// };
	// 				// this.toasterService.pop(toast);

	// 				this.toastr.error( 'Please contact site administrator and <div (click)="giveFeedback()"> click here <div> to leave your feedback', 'Error', {
	// 					timeOut: 0,
	// 					closeButton: true
	// 			});

	// 			}


	// 		}, error => {
	// 			this.spinner.hide();
	// 			// var toast: Toast = {
	// 			// 	type: 'error',
	// 			// 	//title: "Server Error!",
	// 			// 	body: "Something went wrong.Please contact site administrator",
	// 			// 	showCloseButton: true,
	// 			// 	timeout: 2000
	// 			// };
	// 			// this.toasterService.pop(toast);

	// 			this.toastr.error( 'Please contact site administrator and <div (click)="giveFeedback()"> click here <div> to leave your feedback', 'Error', {
	// 				timeOut: 0,
	// 				closeButton: true
	// 			});
	// 		});
	// }

	searchnotification(event) {
		const val = event.target.value.toLowerCase();
		this.temp = [...this.CourseNoti];
		this.searchText=val;
		if(val.length>=3||val.length==0){

		const temp = this.temp.filter(function (d) {
			return String(d.rulename).toLowerCase().indexOf(val) !== -1 ||
				d.description.toLowerCase().indexOf(val) !== -1 ||
				d.modes.toLowerCase().indexOf(val) !== -1 ||
				d.notEvent.toLowerCase().indexOf(val) !== -1 ||
				String(d.enrolmode).toLowerCase().indexOf(val) !== -1 ||
				!val
		});

		// update the rows
		this.rows = [...temp];
	}
		// Whenever the filter changes, always go back to the first page
		this.tablenoti.offset = 0;
	}
	clearesearch(){
		if(this.searchText.length>3){
			this.searchText=''
			this.searchvalue={}
			this.getExistingCourseNotification();
		}else{
			this.searchvalue={}
		}
	}
	fetchtemplatedropdown() {
		let param = {
			//"nEventId": '1,2,3,4',
			"tid": this.tenantId,
			"aId": this.areaId
		}
		this.engageservice.getNottemplateDropdown(param)
			.then(rescompData => {

				var result = rescompData;
				var temp = result['data'][0];
				if (result['type'] == true) {
					console.log('TemplateNotDropdown:', rescompData)
					this.templateDrop = temp;
				} else {
					// this.loader = false;
					// var toast: Toast = {
					// 	type: 'error',
					// 	//title: "Server Error!",
					// 	body: "Something went wrong.Please contact site administrator",
					// 	showCloseButton: true,
					// 	timeout: 2000
					// };
					// this.toasterService.pop(toast);

					this.toastr.error( 'Please contact site administrator and <div (click)="giveFeedback()"> click here <div> to leave your feedback', 'Error', {
						timeOut: 0,
						closeButton: true
					});
				}


			}, error => {
				//this.loader = false;
				// var toast: Toast = {
				// 	type: 'error',
				// 	//title: "Server Error!",
				// 	body: "Something went wrong.Please contact site administrator",
				// 	showCloseButton: true,
				// 	timeout: 2000
				// };
				// this.toasterService.pop(toast);

				this.toastr.error( 'Please contact site administrator and <div (click)="giveFeedback()"> click here <div> to leave your feedback', 'Error', {
					timeOut: 0,
					closeButton: true
				});
			});
	}

	fetcheventsdropdown() {
		let param = {
			"tId": this.tenantId,
			"aId": this.areaId
		}
		this.engageservice.getNotEventDropdown(param)
			.then(rescompData => {

				var result = rescompData;
				var temp = result['data'][0];
				if (result['type'] == true) {
					console.log('EventNotDropdown:', rescompData)
					this.eventsDrop = temp;
				} else {
					// this.loader = false;
					// var toast: Toast = {
					// 	type: 'error',
					// 	//title: "Server Error!",
					// 	body: "Something went wrong.Please contact site administrator",
					// 	showCloseButton: true,
					// 	timeout: 2000
					// };
					// this.toasterService.pop(toast);

					this.toastr.error( 'Please contact site administrator and <div (click)="giveFeedback()"> click here <div> to leave your feedback', 'Error', {
						timeOut: 0,
						closeButton: true
					});
				}


			}, error => {
				//this.loader = false;
				// var toast: Toast = {
				// 	type: 'error',
				// 	//title: "Server Error!",
				// 	body: "Something went wrong.Please contact site administrator",
				// 	showCloseButton: true,
				// 	timeout: 2000
				// };
				// this.toasterService.pop(toast);

				this.toastr.error( 'Please contact site administrator and <div (click)="giveFeedback()"> click here <div> to leave your feedback', 'Error', {
					timeOut: 0,
					closeButton: true
				});
			});
	}
btnName='Save'
	openAddEdit() {
    this.CourseNotification = {
      name:'',
      desc:'',
      event:'',
      lnmsg: false,
      lnmsgtemp:'',
      reviewTextSms: false,
      editSmsTemplateData: '',
      lnnot: false,
      lnnottemp: '',
      reviewTextNot: false,
      editNotTemplateData: '',
      lnemail: false,
      lnemailTemp:'',
      reviewTextEmail:false,
      emailSubjectData:'',
      editEmailTemplateData:'',
      smsSubjectData:'',
      NotSubjectData:'',
    };
		this.addEngagePopup = true;
	this.addFlag = true;
	this.notiTitle="Add Notification"
    this.init = false;
	}

	back() {
		this.router.navigate(['/pages/plan/preoncourses/preoncontent']);
	}

	onChange(event) {
		if (event == 'Date') {
			this.showdate = true;
		}
	}
	templatesms: any = [];
	templatenot: any = [];
	templateemail: any = [];
	// onChange2(notEventId) {
  //   // let menuId = null;
  //   // if (localStorage['menuId']) {
  //   //   menuId = localStorage.getItem('menuId');
  //   // }
  //   // const param = {
  //   //   'tId': this.tenantId,
  //   //   // 'menuId': this.addEditCourseService.menuId || menuId,
  //   //   // 'noteventId': notEventId,
  //   //   'noteventId': 31,
  //   //   'menuId': 31,
  //   // };

  //   // this.engageservice.getNotificationsTags(param)
  //   // .then(rescompData => {

  //   //   console.log('Tags Responses ===>', rescompData);
  //   //   if(rescompData && rescompData['data'])
  //   //   {
  //   //     let dataResponse = [];
  //   //     dataResponse = rescompData['data'];
  //   //     if(dataResponse[0].length !=0){
  //   //       this.tagsArray = dataResponse[0];
  //   //       if(this.tagsArray.length !=0){
  //   //         setTimeout(() =>{ this.init = true; }, 400);
  //   //       }


  //   //     }
  //   //   }
  //   //   this.spinner.hide();


  //   // }, error => {
  //   //   this.spinner.hide();
  //   //   //this.loader = false;
  //   //   // var toast: Toast = {
  //   //   // 	type: 'error',
  //   //   // 	//title: "Server Error!",
  //   //   // 	body: "Something went wrong.Please contact site administrator",
  //   //   // 	showCloseButton: true,
  //   //   // 	timeout: 2000
  //   //   // };
  //   //   // this.toasterService.pop(toast);

  //   //   this.toastr.error( 'Please contact site administrator and <div (click)="giveFeedback()"> click here <div> to leave your feedback', 'Error', {
  //   //     timeOut: 0,
  //   //     closeButton: true
  //   //   });
  //   // });
	// 	this.templatesms = [];
	// 	this.templatenot = [];
	// 	this.templateemail = [];

	// 	for (let i = 0; i < this.templateDrop.length; i++) {
	// 		if (this.templateDrop[i].notEventId == notEventId) {
	// 			if (this.templateDrop[i].notModeId == 1) {
	// 				let tempObj = {
	// 					id: this.templateDrop[i].templateId,
	// 					name: this.templateDrop[i].templateName,
	// 					desc: this.templateDrop[i].description,
	// 					template: this.templateDrop[i].template,
	// 					subject: this.templateDrop[i].subject,
	// 				}
	// 				this.templatesms.push(tempObj)
	// 			} else if (this.templateDrop[i].notModeId == 2) {
	// 				let tempObj = {
	// 					id: this.templateDrop[i].templateId,
	// 					name: this.templateDrop[i].templateName,
	// 					desc: this.templateDrop[i].description,
	// 					template: this.templateDrop[i].template,
	// 					subject: this.templateDrop[i].subject,
	// 				}
	// 				this.templateemail.push(tempObj)
	// 			} else if (this.templateDrop[i].notModeId == 3) {
	// 				let tempObj = {
	// 					id: this.templateDrop[i].templateId,
	// 					name: this.templateDrop[i].templateName,
	// 					desc: this.templateDrop[i].description,
	// 					template: this.templateDrop[i].template,
	// 					subject: this.templateDrop[i].subject,
	// 				}
	// 				this.templatenot.push(tempObj)
	// 			}
	// 		}
	// 	}



	// }
  onChange2(notEventId) {
    if (notEventId == '') {
      this.init = false;
    } else {
      this.spinner.show();
      this.templatesms = [];
      this.templatenot = [];
      this.templateemail = [];
      let menuId = null;
      if (localStorage['menuId']) {
        menuId = localStorage.getItem('menuId');
      }
      const param = {
        'tId': this.tenantId,
        'menuId': this.addEditCourseService.menuId || menuId,
        'noteventId': notEventId,
        // 'noteventId': 1,
      };
      this.engageservice.getNotificationsTags(param)
        .then(rescompData => {
          console.log('Tags Responses ===>', rescompData);
          if (rescompData && rescompData['data']) {
            let dataResponse = [];
            dataResponse = rescompData['data'];
            if (dataResponse[0].length != 0) {
              this.tagsArray = dataResponse[0];
              if (this.tagsArray.length != 0) {
                setTimeout(() => { this.init = true; }, 450);
              }


            }
          }
          this.spinner.hide();

        }, error => {
          this.spinner.hide();
          // var toast: Toast = {
          //   type: 'error',
          //   //title: "Server Error!",
          //   body: 'Something went wrong.Please contact site administrator',
          //   showCloseButton: true,
          //   timeout: 2000,
          // };
          // this.toasterService.pop(toast);

          // this.toastr.error('Please contact site administrator and <div (click)="giveFeedback()"> click here <div> to leave your feedback', 'Error', {
          //   timeOut: 0,
          //   closeButton: true
          // });
        });
      for (let i = 0; i < this.templateDrop.length; i++) {
        if (this.templateDrop[i].notEventId == notEventId) {
          if (this.templateDrop[i].notModeId == 1) {
            let tempObj = {
              id: this.templateDrop[i].templateId,
              name: this.templateDrop[i].templateName,
              desc: this.templateDrop[i].description,
              template: this.templateDrop[i].template,
              subject: this.templateDrop[i].subject,
            }
            this.templatesms.push(tempObj);
            console.log(this.templatesms);
            this.CourseNotification.reviewTextSms = false;
            this.CourseNotification.editSmsTemplateData = '';
          } else if (this.templateDrop[i].notModeId == 2) {
            let tempObj = {
              id: this.templateDrop[i].templateId,
              name: this.templateDrop[i].templateName,
              desc: this.templateDrop[i].description,
              template: this.templateDrop[i].template,
              subject: this.templateDrop[i].subject,
            }
            this.templateemail.push(tempObj)
            this.CourseNotification.reviewTextEmail = false;
            this.CourseNotification.editEmailTemplateData = '';
            this.CourseNotification.emailSubjectData = '';
          } else if (this.templateDrop[i].notModeId == 3) {
            let tempObj = {
              id: this.templateDrop[i].templateId,
              name: this.templateDrop[i].templateName,
              desc: this.templateDrop[i].description,
              template: this.templateDrop[i].template,
              subject: this.templateDrop[i].subject,
            }
            this.templatenot.push(tempObj);
            this.CourseNotification.reviewTextNot = false;
            this.CourseNotification.editNotTemplateData = '';
            this.CourseNotification.NotSubjectData = '';
          }
        }
      }
    }





  }
	onChangeDropDownSms(event: Event) {
		let SelectedValue = (<HTMLInputElement>event.target).value;
		console.log(event);
		console.log(SelectedValue);

		this.CourseNotification.reviewTextSms = true;
		for (let i = 0; i < this.templatesms.length; i++) {
			if (SelectedValue == this.templatesms[i].id) {
				this.CourseNotification.editSmsTemplateData = this.templatesms[i].template;
				this.CourseNotification.smsSubjectData = this.templatesms[i].subject;
			}
		}

	}

	onChangeDropDownNot(event: Event) {
		let SelectedValue = (<HTMLInputElement>event.target).value;
		console.log(event);
		console.log(SelectedValue);

		this.CourseNotification.reviewTextNot = true;
		for (let i = 0; i < this.templatenot.length; i++) {
			if (SelectedValue == this.templatenot[i].id) {
				this.CourseNotification.editNotTemplateData = this.templatenot[i].template;
				this.CourseNotification.NotSubjectData = this.templatenot[i].subject;
			}
		}
	}

	onChangeDropDownEmail(event: Event) {
		let SelectedValue = (<HTMLInputElement>event.target).value;
		console.log(event);
		console.log(SelectedValue);

		this.CourseNotification.reviewTextEmail = true;
		for (let i = 0; i < this.templateemail.length; i++) {
			if (SelectedValue == this.templateemail[i].id) {
				this.CourseNotification.editEmailTemplateData = this.templateemail[i].template;
				this.CourseNotification.emailSubjectData = this.templateemail[i].subject;
			}
		}
	}

	onChangeSelect(event) {
		if (event == 'nDays') {
			this.showdays = true;
		}
	}

  closeEngageModal() {
    this.CourseNotification = {
      name:'',
      desc:'',
      event:'',
      lnmsg: false,
      lnmsgtemp:'',
      reviewTextSms: false,
      editSmsTemplateData: '',
      lnnot: false,
      lnnottemp: '',
      reviewTextNot: false,
      editNotTemplateData: '',
      lnemail: false,
      lnemailTemp:'',
      reviewTextEmail:false,
      emailSubjectData:'',
      editEmailTemplateData:'',
      smsSubjectData:'',
      NotSubjectData:'',
    };
    this.init = false;
    this.addEngagePopup = false;
  }
  proceed: boolean = false;
  submitCourseNotification(data, f) {
    if (f.valid) {
      this.spinner.show();
      if (this.addFlag == true) {
        if (this.CourseNoti.length != 0) {
          for (let i = 0; i < this.CourseNoti.length; i++) {
            if (data.event == this.CourseNoti[i].notEventId) {

              this.addEngagePopup = false;
              this.proceed = false;
              this.spinner.hide();
              break;
            } else {
              this.proceed = true;
            }
          }
        } else {
          this.proceed = true;
        }
        if (this.proceed == true) {
          this.submitCourseNotificationFinal(data)
        } else {
          this.warningModal = true;
        }
      } else {
        this.submitCourseNotificationFinal(data);
      }
    } else {
      console.log('Please Fill all fields');
      // const modUpdate: Toast = {
      //   type: 'error',
      //   title: 'Unable to update',
      //   body: 'Please Fill all fields',
      //   showCloseButton: true,
      //   timeout: 2000
      // };
      // this.toasterService.pop(modUpdate);
      Object.keys(f.controls).forEach(key => {
        f.controls[key].markAsDirty();
      });
    }


  }
	// proceed: boolean = false;
	// submitCourseNotification(data, f) {
	// 	if (f.valid) {
	// 		this.spinner.show();
	// 		if (this.addFlag == true) {
	// 			if (this.CourseNoti.length != 0) {
	// 				for (let i = 0; i < this.CourseNoti.length; i++) {
	// 					if (data.event == this.CourseNoti[i].notEventId) {

	// 						this.addEngagePopup = false;
	// 						this.proceed = false;
	// 						this.spinner.hide();
	// 						break;
	// 					} else {
	// 						this.proceed = true;
	// 					}
	// 				}
	// 			} else {
	// 				this.proceed = true;
	// 			}
	// 			if (this.proceed == true) {
	// 				this.submitCourseNotificationFinal(data)
	// 			} else {
	// 				this.warningModal = true;
	// 			}
	// 		} else {
	// 			this.submitCourseNotificationFinal(data);
	// 		}
	// 	} else {
	// 		console.log('Please Fill all fields');
	// 		// const modUpdate: Toast = {
	// 		//   type: 'error',
	// 		//   title: 'Unable to update',
	// 		//   body: 'Please Fill all fields',
	// 		//   showCloseButton: true,
	// 		//   timeout: 2000
	// 		// };
	// 		// this.toasterService.pop(modUpdate);
	// 		Object.keys(f.controls).forEach(key => {
	// 			f.controls[key].markAsDirty();
	// 		});
	// 	}


	// }
	submitCourseNotificationFinal(data) {
		this.getdatareadyforNotifications(data);
		console.log('data', data);
	 if(this.rulename){
		let param = {
		  "areaId": this.areaId,
		  "instanceId": this.courseId,
		  "tid": this.tenantId,
		  "userid": this.userId,
		  "nEventId": data.event,
		  "qOptions": this.rulename,
		}
		this.engageservice.insertUpdateCouresNotifications(param)
		  .then(rescompData => {
			this.spinner.hide();
			var temp = rescompData;
			// console.log('NotificationsSuccess:', rescompData);
			if (this.optId == 0) {
			  if (temp['type'] == true) {
				// var notiInsert: Toast = {
				// 	type: 'success',
				// 	title: "Notifications Inserted!",
				// 	body: "A new notification has been added",
				// 	showCloseButton: true,
				// 	timeout: 2000
				// };
				// this.toasterService.pop(notiInsert);
	
				this.toastr.success('New notification added', 'Success', {
				  closeButton: false
				});
				//this.cdf.detectChanges();
				this.CourseNotification = {
				  name: '',
				  desc: '',
				  event: '',
				  lnmsg: false,
				  lnmsgtemp: '',
				  reviewTextSms: false,
				  editSmsTemplateData: '',
				  lnnot: false,
				  lnnottemp: '',
				  reviewTextNot: false,
				  editNotTemplateData: '',
				  lnemail: false,
				  lnemailTemp: '',
				  reviewTextEmail: false,
				  emailSubjectData: '',
				  editEmailTemplateData: '',
				  smsSubjectData: '',
				  NotSubjectData: '',
				};
				this.getExistingCourseNotification();
			  } else {
				//this.loader = false;
				// var toast: Toast = {
				// 	type: 'error',
				// 	//title: "Server Error!",
				// 	body: "Something went wrong.Please contact site administrator",
				// 	showCloseButton: true,
				// 	timeout: 2000
				// };
				// this.toasterService.pop(toast);
	
				this.toastr.error('Please contact site administrator and <div (click)="giveFeedback()"> click here <div> to leave your feedback', 'Error', {
				  timeOut: 0,
				  closeButton: true
				});
	
				//this.router.navigate(['pages/gamification/ladders']);
			  }
			} else if (this.optId == 1) {
			  if (temp['type'] == true) {
				// var notiInsert: Toast = {
				// 	type: 'success',
				// 	title: "Notifications Updated!",
				// 	body: "Notifications updated .",
				// 	showCloseButton: true,
				// 	timeout: 2000
				// };
				// this.toasterService.pop(notiInsert);
	
				this.toastr.success('Notification updated', 'Success', {
				  closeButton: false
				});
				//this.cdf.detectChanges();
				this.getExistingCourseNotification();
			  } else {
	
				// var toast: Toast = {
				// 	type: 'error',
				// 	//title: "Server Error!",
				// 	body: "Something went wrong.Please contact site administrator",
				// 	showCloseButton: true,
				// 	timeout: 2000
				// };
				// this.toasterService.pop(toast);
	
				this.toastr.error('Please contact site administrator and <div (click)="giveFeedback()"> click here <div> to leave your feedback', 'Error', {
				  timeOut: 0,
				  closeButton: true
				});
				//this.router.navigate(['pages/gamification/ladders']);
			  }
			}
	
	
		  },
			resUserError => {
	
			  this.spinner.hide();
			  // var toast: Toast = {
			  // 	type: 'error',
			  // 	//title: "Server Error!",
			  // 	body: "Something went wrong.Please contact site administrator",
			  // 	showCloseButton: true,
			  // 	timeout: 2000
			  // };
			  // this.toasterService.pop(toast);
	
			  this.toastr.error('Please contact site administrator and <div (click)="giveFeedback()"> click here <div> to leave your feedback', 'Error', {
				timeOut: 0,
				closeButton: true
			  });
			  //this.router.navigate(['pages/gamification/ladders']);
			});
		this.closeEngageModal();
	 }
	 else{
	   this.toastr.warning('Please select any one of the below checkboxes');
	this.spinner.hide();
	 }
	 this.getExistingCourseNotification();
	  }
// 	submitCourseNotificationFinal(data) {
// 		this.getdatareadyforNotifications(data);
// 		console.log('data', data);
//  if(this.rulename){
// 		let param = {
// 			"areaId": 2,
// 			"instanceId": this.courseId,
// 			"tid": this.tenantId,
// 			"userid": this.userId,
// 			"nEventId": data.event,
// 			"qOptions": this.rulename
// 		}
// 		this.engageservice.insertUpdateCouresNotifications(param)
// 			.then(rescompData => {
// 				this.spinner.hide();
// 				var temp = rescompData;
// 				// console.log('NotificationsSuccess:', rescompData);
// 				if (this.optId == 0) {
// 					if (temp['type'] == true) {
// 						// var notiInsert: Toast = {
// 						// 	type: 'success',
// 						// 	title: "Notifications Inserted!",
// 						// 	body: "A new notification has been added",
// 						// 	showCloseButton: true,
// 						// 	timeout: 2000
// 						// };
// 						// this.toasterService.pop(notiInsert);

// 						this.toastr.success('New notification added', 'Success', {
// 							closeButton: false
// 						});
// 						//this.cdf.detectChanges();

//             this.CourseNotification = {
//               name:'',
//               desc:'',
//               event:'',
//               lnmsg: false,
//               lnmsgtemp:'',
//               reviewTextSms: false,
//               editSmsTemplateData: '',
//               lnnot: false,
//               lnnottemp: '',
//               reviewTextNot: false,
//               editNotTemplateData: '',
//               lnemail: false,
//               lnemailTemp:'',
//               reviewTextEmail:false,
//               emailSubjectData:'',
//               editEmailTemplateData:'',
//               smsSubjectData:'',
//               NotSubjectData:'',
//             };
// 						this.getExistingCourseNotification();
// 					} else {
// 						//this.loader = false;
// 						// var toast: Toast = {
// 						// 	type: 'error',
// 						// 	//title: "Server Error!",
// 						// 	body: "Something went wrong.Please contact site administrator",
// 						// 	showCloseButton: true,
// 						// 	timeout: 2000
// 						// };
// 						// this.toasterService.pop(toast);

// 						this.toastr.error( 'Please contact site administrator and <div (click)="giveFeedback()"> click here <div> to leave your feedback', 'Error', {
// 							timeOut: 0,
// 							closeButton: true
// 						});

// 						//this.router.navigate(['pages/gamification/ladders']);
// 					}
// 				} else if (this.optId == 1) {
// 					if (temp['type'] == true) {
// 						// var notiInsert: Toast = {
// 						// 	type: 'success',
// 						// 	title: "Notifications Updated!",
// 						// 	body: "Notifications updated .",
// 						// 	showCloseButton: true,
// 						// 	timeout: 2000
// 						// };
// 						// this.toasterService.pop(notiInsert);

// 						this.toastr.success('Notification updated', 'Success', {
// 							closeButton: false
// 						});
// 						//this.cdf.detectChanges();
// 						this.getExistingCourseNotification();
// 					} else {

// 						// var toast: Toast = {
// 						// 	type: 'error',
// 						// 	//title: "Server Error!",
// 						// 	body: "Something went wrong.Please contact site administrator",
// 						// 	showCloseButton: true,
// 						// 	timeout: 2000
// 						// };
// 						// this.toasterService.pop(toast);

// 						this.toastr.error( 'Please contact site administrator and <div (click)="giveFeedback()"> click here <div> to leave your feedback', 'Error', {
// 							timeOut: 0,
// 							closeButton: true
// 						});
// 						//this.router.navigate(['pages/gamification/ladders']);
// 					}
// 				}


// 			},
// 				resUserError => {

// 					this.spinner.hide();
// 					// var toast: Toast = {
// 					// 	type: 'error',
// 					// 	//title: "Server Error!",
// 					// 	body: "Something went wrong.Please contact site administrator",
// 					// 	showCloseButton: true,
// 					// 	timeout: 2000
// 					// };
// 					// this.toasterService.pop(toast);

// 					this.toastr.error( 'Please contact site administrator and <div (click)="giveFeedback()"> click here <div> to leave your feedback', 'Error', {
// 						timeOut: 0,
// 						closeButton: true
// 					});
// 					//this.router.navigate(['pages/gamification/ladders']);
// 				});
// 		this.closeEngageModal();
		
// 	}
// 	else{
// 		this.toastr.warning('Please select any one of the below checkboxes');
// 	 this.spinner.hide();
// 	  }
// 	  this.getExistingCourseNotification();
// 	   }
	

	rulename: any;
	courseIdnotInsert: any = 1;
	getdatareadyforNotifications(notidetails) {

		if (notidetails.lnmsg == true) {
			var modeidsms = 1;
			var templateidsms = notidetails.lnmsgtemp;
			//var rulenamesms = "course" + "_" + this.courseId + "_" + "SMS";
			var rulenamesms = notidetails.name;
			var ruledescsms = notidetails.desc;
			var noteventid = notidetails.event;
			var templatesms = notidetails.editSmsTemplateData;
			var subjectsms = '';

			// var smsString = rulenamesms + "#" + ruledescsms + "#" + noteventid + "#" + modeidsms + "#" + templateidsms + "#" + templatesms + "#" + 1 + '#' + subjectsms;
      var smsString = rulenamesms + this.dataSeparatorService.Hash + ruledescsms + this.dataSeparatorService.Hash + noteventid + this.dataSeparatorService.Hash + modeidsms + this.dataSeparatorService.Hash + templateidsms + this.dataSeparatorService.Hash + templatesms + this.dataSeparatorService.Hash + 1 + this.dataSeparatorService.Hash + subjectsms;

    } else {
			modeidsms = null;
			templateidsms = null;
		}

		if (notidetails.lnemail == true) {
			var modeidemail = 2;
			var templateidemail = notidetails.lnemailTemp;
			//	var rulenameemail = "course" + "_" + this.courseId + "_" + "EMAIL";
			var rulenameemail = notidetails.name;
			var ruledescemail = notidetails.desc;
			var noteventid = notidetails.event;
			var templateemail = notidetails.editEmailTemplateData;
			var subjectEmail = notidetails.emailSubjectData;

			// var emailString = rulenameemail + "#" + ruledescemail + "#" + noteventid + "#" + modeidemail + "#" + templateidemail + "#" + templateemail + "#" + 1 + '#' + subjectEmail;
      var emailString = rulenameemail + this.dataSeparatorService.Hash + ruledescemail + this.dataSeparatorService.Hash + noteventid + this.dataSeparatorService.Hash + modeidemail + this.dataSeparatorService.Hash + templateidemail + this.dataSeparatorService.Hash + templateemail + this.dataSeparatorService.Hash + 1 + this.dataSeparatorService.Hash + subjectEmail;
    } else {
			modeidemail = null;
			templateidemail = null;
		}

		if (notidetails.lnnot == true) {
			var modeidnot = 3;
			var templateidnot = notidetails.lnnottemp;
			//	var rulenamenotify = "course" + "_" + this.courseId + "_" + "NOTIFY";
			var rulenamenotify = notidetails.name;
			var ruledescnotify = notidetails.desc;
			var noteventid = notidetails.event;
			var templatenot = notidetails.editNotTemplateData;
			var subjectnot = '';

			// var notifyString = rulenamenotify + "#" + ruledescnotify + "#" + noteventid + "#" + modeidnot + "#" + templateidnot + "#" + templatenot + "#" + 1 + '#' + subjectnot;
      var notifyString = rulenamenotify + this.dataSeparatorService.Hash + ruledescnotify + this.dataSeparatorService.Hash + noteventid + this.dataSeparatorService.Hash + modeidnot + this.dataSeparatorService.Hash + templateidnot + this.dataSeparatorService.Hash + templatenot + this.dataSeparatorService.Hash + 1 + this.dataSeparatorService.Hash + subjectnot;
    } else {
			modeidnot = null;
			templateidnot = null;
		}

		// if (modeidsms == null) {
		// 	this.rulename = emailString + "|" + notifyString;
		// } else if (modeidemail == null) {
		// 	this.rulename = smsString + "|" + notifyString;
		// } else if (modeidnot == null) {
		// 	this.rulename = smsString + "|" + emailString;
		// } else if (modeidsms == null && modeidemail == null) {
		// 	this.rulename = notifyString;
		// } else if (modeidemail == null && modeidnot == null) {
		// 	this.rulename = smsString;
		// } else if (modeidnot == null && modeidsms == null) {
		// 	this.rulename = emailString;
		// } else {
		// 	this.rulename = smsString + "|" + emailString + "|" + notifyString;
		// }
		// if (notidetails.lnmsg == true && notidetails.lnemail == true && notidetails.lnnot == true) {
		// 	this.rulename = smsString + "|" + emailString + "|" + notifyString;
		// } else if (notidetails.lnemail == true && notidetails.lnnot == true) {
		// 	this.rulename = emailString + "|" + notifyString;
		// } else if (notidetails.lnmsg == true && notidetails.lnnot == true) {
		// 	this.rulename = smsString + "|" + notifyString;
		// } else if (notidetails.lnmsg == true && notidetails.lnemail == true) {
		// 	this.rulename = smsString + "|" + emailString;
		// } else if (notidetails.lnnot == true) {
		// 	this.rulename = notifyString;
		// } else if (notidetails.lnmsg == true) {
		// 	this.rulename = smsString;
		// } else if (notidetails.lnemail == true) {
		// 	this.rulename = emailString;
		// }
    if (notidetails.lnmsg == true && notidetails.lnemail == true && notidetails.lnnot == true) {
      this.rulename = smsString + this.dataSeparatorService.Pipe + emailString + this.dataSeparatorService.Pipe + notifyString;
    } else if (notidetails.lnemail == true && notidetails.lnnot == true) {
      this.rulename = emailString + this.dataSeparatorService.Pipe + notifyString;
    } else if (notidetails.lnmsg == true && notidetails.lnnot == true) {
      this.rulename = smsString + this.dataSeparatorService.Pipe + notifyString;
    } else if (notidetails.lnmsg == true && notidetails.lnemail == true) {
      this.rulename = smsString + this.dataSeparatorService.Pipe + emailString;
    } else if (notidetails.lnnot == true) {
      this.rulename = notifyString;
    } else if (notidetails.lnmsg == true) {
      this.rulename = smsString;
    } else if (notidetails.lnemail == true) {
      this.rulename = emailString;
    }
		//this.ruledescription = this.rulename;
		console.log('thi.rulename', this.rulename)

	}

	onSelect(data) {
		// if (data.type == "click") {
			this.addFlag = false;
			this.notiTitle="Edit NOtification"
			console.log('rowData', data);
			// var editData = data.row;
			var editData = data;
			this.readyDataforEdit(editData)
			this.addEngagePopup = true;

			this.CourseNotification = {
				name: editData.rulename,
				desc: editData.description,
        event: editData.notEventId,
        lnmsg: false,
        lnmsgtemp:'',
        reviewTextSms: false,
        editSmsTemplateData: '',
        lnnot: false,
        lnnottemp: '',
        reviewTextNot: false,
        editNotTemplateData: '',
        lnemail: false,
        lnemailTemp:'',
        reviewTextEmail:false,
        emailSubjectData:'',
        editEmailTemplateData:'',
        smsSubjectData:'',
        NotSubjectData:'',
			}

			this.selectMethod = "Event";
			this.onChange2(data.notEventId)

			for (let i = 0; i < this.modeArr.length; i++) {
				if (this.modeArr[i] == "1" && this.templateArr[i] != null) {
					this.CourseNotification.lnmsg = true;
					this.CourseNotification.lnmsgtemp = this.templateArr[i];
					this.CourseNotification.reviewTextSms = true;
					this.CourseNotification.editSmsTemplateData = this.templateNameArr[i];
					this.CourseNotification.smsSubjectData = this.templateSubjectArr[i];
				}

				if (this.modeArr[i] == "2" && this.templateArr[i] != null) {
					this.CourseNotification.lnemail = true;
					this.CourseNotification.lnemailTemp = this.templateArr[i];
					this.CourseNotification.reviewTextEmail = true;
					this.CourseNotification.editEmailTemplateData = this.templateNameArr[i];
					this.CourseNotification.emailSubjectData = this.templateSubjectArr[i];
				}

				if (this.modeArr[i] == "3" && this.templateArr[i] != null) {
					this.CourseNotification.lnnot = true;
					this.CourseNotification.lnnottemp = this.templateArr[i];
					this.CourseNotification.reviewTextNot = true;
					this.CourseNotification.editNotTemplateData = this.templateNameArr[i];
					this.CourseNotification.NotSubjectData = this.templateSubjectArr[i];
				}
			}
			console.log('this.CourseNotificationNew', this.CourseNotification);

		// }
	}

	modeArr: any = [];
	templateArr: any = [];
	templateNameArr: any = [];
	templateSubjectArr: any = [];
	readyDataforEdit(editData) {
		if (editData.modeIds) {
			this.modeArr = editData.modeIds.split("@edge@");
		}
		if (editData.templateIds) {
			this.templateArr = editData.templateIds.split("@edge@");
		}
		if (editData.templates) {
			this.templateNameArr = editData.templates.split("@edge@");
		}
		if (editData.subjects) {
			this.templateSubjectArr = editData.subjects.split("@edge@");
		}

		console.log('this.modeArr', this.modeArr);
		console.log('this.templateArr', this.templateArr);
		console.log('this.templateNameArr', this.templateNameArr);
		console.log('this.templateSubjectArr', this.templateSubjectArr);
	}

	closeWarningModal() {
		this.warningModal = false;
	}


	notDetailsModal: boolean = false;
	templateData: any = {};
	getTempData(value, data) {
		//this.addEngagePopup = false;
		this.spinner.show();
		console.log('previewData:', data);
		let param = {
			"tempId": data
		}

		this.notitempservice.getNottepById(param)
			.then(rescompData => {

				if (rescompData != "err") {
					this.spinner.hide();
					var temp = rescompData;
					var data = temp['data'][0];
					var res = data[0];

					if (temp['type'] == true) {
						this.templateData = {
							name: res.tname,
							desc: res.description,
							template: res.template
						}
						console.log('notifytemplateById', this.templateData);
						this.addEngagePopup = false;
						this.notDetailsModal = true;

					} else {
						// var toast: Toast = {
						// 	type: 'error',
						// 	//title: "Server Error!",
						// 	body: "Something went wrong.Please contact site administrator",
						// 	showCloseButton: true,
						// 	timeout: 2000
						// };
						// this.toasterService.pop(toast);

						this.toastr.error( 'Please contact site administrator and <div (click)="giveFeedback()"> click here <div> to leave your feedback', 'Error', {
							timeOut: 0,
							closeButton: true
						});
						//this.addEngagePopup = true;
					}
				} else {
					this.spinner.hide();
					// var toast: Toast = {
					// 	type: 'error',
					// 	//title: "Server Error!",
					// 	body: "Something went wrong.Please contact site administrator",
					// 	showCloseButton: true,
					// 	timeout: 2000
					// };
					// this.toasterService.pop(toast);

					this.toastr.error( 'Please contact site administrator and <div (click)="giveFeedback()"> click here <div> to leave your feedback', 'Error', {
						timeOut: 0,
						closeButton: true
					});
					//	this.addEngagePopup = true;
				}

			})
		// resUserError => {
		//   this.spinner.hide();
		//   var toast: Toast = {
		//     type: 'error',
		//     //title: "Server Error!",
		//     body: "Something went wrong.Please contact site administrator",
		//     showCloseButton: true,
		//     timeout: 2000
		//   };
		//   this.toasterService.pop(toast);
		//   this.addEngagePopup = true;
		// });

	}

	closeNotDetailModal() {
		this.notDetailsModal = false;
		this.addEngagePopup = true;
		this.templateData = {};
	}

}


