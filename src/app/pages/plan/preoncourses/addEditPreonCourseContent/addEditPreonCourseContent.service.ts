import {Injectable, Inject} from '@angular/core';
import {Http, Response, Headers, RequestOptions, Request} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppConfig} from '../../../../app.module';
import { webAPIService } from '../../../../service/webAPIService'
import { webApi } from '../../../../service/webApi'
import { AuthenticationService } from '../../../../service/authentication.service';
import { HttpClient } from '@angular/common/http';
@Injectable()

export class AddEditPreonCourseContentService {

  public courseId:any;

  // public courseId:any;
  public tagList:any =[];
  public data: any= [];
  public showRule: boolean;
  public showRegulatory: boolean;
  public showSelf: boolean;
  public menuId: number;

  private allErnroledusers:string = webApi.domain + webApi.url.getAllEnrolUser;

//   private _urlProfileFields:string = "/api/edge/course/getUserProfileFields";
  private _urlDisableUser:string = webApi.domain + webApi.url.disableuser;
  //private _urlTotalEnroluser:string = "/api/web/getallcourseenrole";

  private _urlProfileFields:string = webApi.domain + webApi.url.profilefield;
  private _urlDisableEnrol: string = webApi.domain + webApi.url.disableEnrol;

  private _urlrempbulk:string= webApi.domain + webApi.url.tempSaveBulkManEnrolPreon;
  private _urlfinalbulk:string= webApi.domain + webApi.url.saveBulkManEnrol;

  private _urlAreaBulEnrol: string = webApi.domain + webApi.url.area_bulk_enrol;
  breadtitle: any;
  breadcrumbArray: any;
  previousBreadCrumb: any;

  constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private _http: Http, private http1 :HttpClient ,private authenticationService: AuthenticationService){
      //this.busy = this._http.get('...').toPromise();
  }


  getUserProfileFields() {
    //  let url:any = `${this.config.FINAL_URL}`+this._urlProfileFields;
     // let url:any = `${this.config.DEV_URL}`+this._urlFilter;
     let headers = new Headers({ 'Content-Type': 'application/json' });
     let options = new RequestOptions({ headers: headers });
     //let body = JSON.stringify(user);
    //  return this._http.post(this._urlProfileFields, options ).map((res: Response) => res.json());
    return new Promise(resolve => {
      this.http1.post(this._urlProfileFields, '')
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  getallenroluser(data){
    // let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    // let options = new RequestOptions({ headers: headers });

  //  return this._http.post(this.allErnroledusers, data, options)
  //  .map((response:Response) => response.json())
  //  .catch(this._errorHandler);


   return new Promise(resolve => {
    this.http1.post(this.allErnroledusers, data)
    //.map(res => res.json())
    .subscribe(data => {
        resolve(data);
    },
    err => {
        resolve('err');
    });
});

 }

 disableUser(visibleData){
  //  let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
  //   let options = new RequestOptions({ headers: headers });
  let url:any = this._urlDisableUser;
  // return this._http.post(url,visibleData, options)
  //     .map((response:Response)=>response.json())
  //     .catch(this._errorHandler);
  return new Promise(resolve => {
    this.http1.post(url, visibleData)
    //.map(res => res.json())
    .subscribe(data => {
        resolve(data);
    },
    err => {
        resolve('err');
    });
});
}
disableEnrol(visibleData) {
  //  let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
  //   let options = new RequestOptions({ headers: headers });
  let url: any = this._urlDisableEnrol;
  // return this._http.post(url,visibleData, options)
  //     .map((response:Response)=>response.json())
  //     .catch(this._errorHandler);
  return new Promise(resolve => {
    this.http1.post(url, visibleData)
      //.map(res => res.json())
      .subscribe(data => {
        resolve(data);
      },
        err => {
          resolve('err');
        });
  });
}
TempManEnrolBulk(item){
    // return this._http.post(this._urlCheckCourseCode,courseData)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);
    // add authorization header with jwt token
        let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
        // let headers = new Headers({ 'Authorization': 'Bearer ' + 'myToken' });
        let options = new RequestOptions({ headers: headers });
    return new Promise(resolve => {
      this._http.post(this._urlrempbulk, item, options)
    .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve('err');
            });
      });

  }

  areaBulkEnrol(param) {
    return new Promise(resolve => {
        this.http1.post(this._urlAreaBulEnrol, param)
            //.map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
                err => {
                    resolve('err');
                });
    });
}

  finalManEnrolBulk(item){
    // return this._http.post(this._urlCheckCourseCode,courseData)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);
       let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
        // let headers = new Headers({ 'Authorization': 'Bearer ' + 'myToken' });
        let options = new RequestOptions({ headers: headers });
    return new Promise(resolve => {
      this._http.post(this._urlfinalbulk, item, options)
    .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve('err');
            });
      });

  }
  // new code end //

  _errorHandler(error: Response){
     console.error(error);
     return Observable.throw(error || "Server Error")
  }


}
