import { Host, ChangeDetectionStrategy, Component, ViewEncapsulation, Directive, forwardRef, Attribute, OnChanges, SimpleChanges, Input, ViewChild, ViewContainerRef, OnInit } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AddEditPreonCourseContent } from '../addEditPreonCourseContent';
import { AddEditPreonCourseContentService } from '../addEditPreonCourseContent.service';
import { BadgesService } from '../../../../gamification/badges/badges.service';
import { PreonrewardsService } from './Preonrewards.service';
import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { CertificateService } from '../../../../gamification/certificate/certificate.service';
import { Router, NavigationStart, ActivatedRoute } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
import { noData } from '../../../../../models/no-data.model';
@Component({
  selector: 'preon-course-rewards',
  templateUrl: './Preonrewards.html',
  styleUrls: ['./Preonrewards.scss'],
  encapsulation: ViewEncapsulation.None
})



export class PreonrewardsComponent {

  @ViewChild('myTable') table: any;
  @ViewChild(DatatableComponent) tableData: DatatableComponent;

	@Input() inpdata: any;

  selected: any = [];
  btnName: string = 'Select';
  badgeTitle:string='Select Badge'
  certTitle:string='Select Template'
  temp = [];

  condataBadge: any;
  condataCert: any;
  badge: boolean = true;
  certificate: boolean = true;

  showEnrolpage: boolean = false;
  badgeshow: boolean = false;
  certshow: boolean = false;

  nodata: boolean;
  noDataVal:noData={
    margin:'mt-5',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"Sorry we couldn't find any matches please try again",
    desc:".",
    titleShow:true,
    btnShow:false,
    descShow:false,
    btnText:'Learn More',
    btnLink:''
}

  rewardsBadge: any = {
    id: '',
    conid: '',
    imgsrc: '',
    name: '',
    descri: ''

  };
  rewardsCert: any = {
    id: '',
    conid: '',
    imgsrc: '',
    name: '',
    descri: ''

  };
  badgeData: any = [];
  certData: any = [];
  search:any={};
  searchcer:any={};


  errorMsg: any;
  userId: any;
  optId: any;
  courseId: any;
    tenantId:any;
  searchText: any;
  response: any;
  tempReg: any;
  responseCert: any;
  constructor(@Host() private parent_Comp: AddEditPreonCourseContent, private rewardservice: PreonrewardsService,
    private toasterService: ToasterService, private toastr: ToastrService, private addEditCourseService: AddEditPreonCourseContentService,
    private badgeservice: BadgesService, private certificateservice: CertificateService,private router: Router,) {

    if (this.addEditCourseService.data != undefined) {
      console.log('this.addEditCourseService.data for Edit:', this.addEditCourseService.data);
      this.ChooseAddEdit(this.addEditCourseService.data);
    }

    if (localStorage.getItem('LoginResData')) {
      var userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', userData.data);
      this.userId = userData.data.data.id;
      console.log('userId', userData.data.data.id);
       this.tenantId = userData.data.data.tenantId;
    }

    this.fetchBadges();
    this.fetchCertificates();
  }

  ngOnChanges(changes: SimpleChanges): void {
		if(this.inpdata === 'rewards') {
		  this.saveRewardsForCoures();
		}
	  }

  // clear()
  // {
  //   this.search={};
  // }
  // clearcer()
  // {
  //   this.searchcer={};
  // }

  ChooseAddEdit(data) {
    this.optId = data.id;

    if (this.optId == 0) {
      this.courseId = this.addEditCourseService.courseId;
    } else if (this.optId == 1) {
      this.courseId = this.addEditCourseService.data.data.courseId;
      this.tenantId = this.addEditCourseService.data.data.tenantId;
      this.getExistingCourseRewards();
    }
  }

  fetchBadges() {
    let param = {
      "tenantId":  this.tenantId,
      "courseId": 0,
      "catId": 0
    }
    this.badgeservice.getBadges(param)
      .then(rescompData => {
        var result = rescompData;
        if (result['type'] == true) {
          this.badgeData = rescompData['data'][0];
          this.response=this.badgeData;
          console.log('this.badgeDataRewards', this.badgeData);
        } else {
          this.errorMsg = rescompData;
        }

      },
        resUserError => {
          // this.loader =false;
          this.errorMsg = resUserError;
        });
  }

  fetchCertificates() {
    let param = {
      "tenantId":  this.tenantId,
      "courseId": 0
    }
    this.certificateservice.getCertificates(param)
      .then(rescompData => {
        var result = rescompData;
        if (result['type'] == true) {
          this.certData = rescompData['data'][0];
          this.responseCert=this.certData;
          console.log(' this.certDataReward', this.certData);
        } else {
          this.errorMsg = rescompData;
        }

      },
        resUserError => {
          // this.loader =false;
          this.errorMsg = resUserError;
        });

  }

  searchBadge(event) {
    const val = event.target.value.toLowerCase();
    // this.allregulatorylist(this.addEditCourseService.data.data);
    this.searchText=val
    this.tempReg = this.response;
    // filter our data
    if(val.length>=3||val.length==0){
    const temp = this.tempReg.filter(function (d) {
      return d.badgeName.toLowerCase().indexOf(val) !== -1 ||
        // String(d.dueDays).toLowerCase() === val ||
        // String(d.reminder).toLowerCase().indexOf(val) !== -1 ||
        !val;
    });
    if(temp.length==0){
      this.nodata=true;
    }
    // update the rows
    this.badgeData = temp;
  }
    // Whenever the filter changes, always go back to the first page
    // this.tableDataReg.offset = 0;
  }
  searchCerti(event) {
    const val = event.target.value.toLowerCase();
    // this.allregulatorylist(this.addEditCourseService.data.data);
    this.searchText=val
    this.tempReg = this.responseCert;
    // filter our data
    if(val.length>=3||val.length==0){
    const temp = this.tempReg.filter(function (d) {
      return d.certName.toLowerCase().indexOf(val) !== -1 ||
        // String(d.dueDays).toLowerCase() === val ||
        // String(d.reminder).toLowerCase().indexOf(val) !== -1 ||
        !val;
    });
    if(temp.length==0){
      this.nodata=true;
    }
    // update the rows
    this.certData = temp;
  }
    // Whenever the filter changes, always go back to the first page
    // this.tableDataReg.offset = 0;
  }
  clear() {
    if(this.searchText.length>=3){
    this.search = {};
    this.searchText='';
    this.badgeData=this.response
    }else{
      this.search={};
    }

  }
  clearcer() {
    if(this.searchText.length>=3){
      this.searchcer = {};
      this.searchText='';
      this.certData=this.responseCert
      }else{
        this.searchcer = {};
      }
  }
  getExistingCourseRewards() {
    let param = {
      "cId": this.courseId,
      "tId":  this.tenantId
    }

    this.rewardservice.getcourserewards(param)
      .then(rescompData => {
        console.log('Existing Course Rewards:', rescompData);
        var result = rescompData;
        var reward = result['data'][0];
        var badges = []
        var certificates = []

        reward.forEach(element => {
          if (element.reward == "badge") {
            badges.push(element)
            this.condataBadge = {
              badgeId: element.rewId,
              badgeName: element.rewName,
              bdescription: element.rewDesc,
              imgsrc: element.imgsrc
            }
            this.rewardsBadge = {
              conid:element.rewId,
              id: element.rewId,
              name: element.rewName,
              descri: element.rewDesc,
              imgsrc: element.imgsrc
            }
          } else {
            certificates.push(element)
            this.rewardsCert = {
              conid:element.rewId,
              id: element.rewId,
              name: element.rewName,
              descri: element.rewDesc,
              imgsrc: element.imgsrc
            }
            this.condataCert = {
              certId: element.rewId,
              certName: element.rewName,
              cdescription: element.rewDesc,
              imgsrc: element.imgsrc
            }
          }
        });

        this.badge = badges.length > 0 ? false : true;
        this.certificate = certificates.length > 0 ? false : true;

      },
        resUserError => {
          this.errorMsg = resUserError
        });

  }





  browse(rid) {
    if (rid == 1) {
      this.badgeshow = true;
    }
    if (rid == 2) {
      this.certshow = true;
    }
  }
  closeModel(rid) {
    if (rid == 1) {
      this.badgeshow = false;
    }
    if (rid == 2) {
      this.certshow = false;
    }
  }

  saveBadge(rid) {
    this.rewardsBadge = {
      id: rid,
      conid: this.condataBadge.badgeId,
      imgsrc: this.condataBadge.badgeIcon,
      name: this.condataBadge.badgeName,
      descri: this.condataBadge.description
    }
    this.badgeshow = false;
    this.badge = false;
  }

  saveCert(rid) {
    this.rewardsCert = {
      id: rid,
      conid: this.condataCert.id,
      imgsrc: this.condataCert.certIcon,
      name: this.condataCert.certName,
      descri: this.condataCert.description
    }
    //this.certificate =true;
    this.certshow = false;
    this.certificate = false;
  }


  activeSelectedBadgeId: any;
  setActiveSelectedBadge(currentIndex, currentBadge) {
    console.log('currentBadgeOld:', currentBadge);

    this.condataBadge = {};
    this.activeSelectedBadgeId = currentBadge.badgeId;
    this.condataBadge = currentBadge;
    console.log('currentBadgeNew:', this.condataBadge);

  }

  activeSelectedCertId: any;
  setActiveSelectedCert(currentIndex, currentCert) {
    console.log('currentCertOld:', currentCert)
    this.condataCert = {};
    this.activeSelectedCertId = currentCert.id;
    this.condataCert = currentCert;
    console.log('currentCertNew:', this.condataCert)
  }

  removeBadge() {
    this.optId=1;
    this.rewardsBadge = {
      id: '',
      conid: '',
      imgsrc: '',
      name: '',
      descri: ''
    };
    this.badge = true;
  }

  removeCert() {
    this.optId=1;
    this.rewardsCert = {
      id: '',
    conid: '',
    imgsrc: '',
    name: '',
    descri: ''
    };
    this.certificate = true;
  }

  formattedrewards: any;
  // saveRewardsForCoures() {
  //   //this.getdatareadyforcourse(this.rewardsCert.conid,this.rewardsBadge.conid);

  //   let param = {
  //     "cId": this.courseId,
  //     "tId":  this.tenantId,
  //     "userId": this.userId,
  //     "badgeId": this.badge ? 0 : this.rewardsBadge.conid,
  //     "certId": this.certificate ? 0 : this.rewardsCert.conid
  //   }

  //   this.rewardservice.insertreward(param)
  //     .then(rescompData => {

  //       var result = rescompData;
  //       console.log('insertrewardresult', rescompData);

  //       if (this.optId == 0) {
  //         if (result['type'] == true) {
  //           // var toast: Toast = {
  //           //   type: 'success',
  //           //   //title: "Server Error!",
  //           //   body: "Reward added to course.",
  //           //   showCloseButton: true,
  //           //   timeout: 2000
  //           // };
  //           // this.toasterService.pop(toast);

  //           this.toastr.success('Reward added ', 'Success', {
  //             closeButton: false
  //            });
  //           // this.router.navigate(["/pages/plan/courses/content"]);
  //           // this.router.navigate(["/pages/plan/preoncourses/preoncontent"]);
  //           window.history.back()
  //         } else {

  //           // var toast: Toast = {
  //           //   type: 'error',
  //           //   body: "Unable to add reward.",
  //           //   showCloseButton: true,
  //           //   timeout: 2000
  //           // };
  //           // this.toasterService.pop(toast);

  //           this.toastr.error( 'Please contact site administrator and <div (click)="giveFeedback()"> click here <div> to leave your feedback', 'Error', {
  //             timeOut: 0,
  //             closeButton: true
  //             });
  //         }
  //       } else if (this.optId == 1) {
  //         if (result['type'] == true) {
  //           // var toast: Toast = {
  //           //   type: 'success',
  //           //   //title: "Server Error!",
  //           //   body: "Reward updated to course.",
  //           //   showCloseButton: true,
  //           //   timeout: 2000
  //           // };
  //           // this.toasterService.pop(toast);

  //           this.toastr.success('Reward updated ', 'Success', {
  //             closeButton: false
  //            });

  //           // this.router.navigate(["/pages/plan/courses/content"]);
  //           // this.router.navigate(["/pages/plan/preoncourses/preoncontent"]);
  //           if (this.addEditCourseService.breadcrumbArray) {
  //             var length = this.addEditCourseService.breadcrumbArray.length - 1
  //             this.addEditCourseService.breadtitle = this.addEditCourseService.breadcrumbArray[length].name
  //             // this.addassetservice.title = this.title
  //             this.addEditCourseService.breadcrumbArray = this.addEditCourseService.breadcrumbArray.slice(0, length)
  //             // this.title = this.addassetservice.breadCrumbArray[]
  //             this.addEditCourseService.previousBreadCrumb = this.addEditCourseService.previousBreadCrumb.slice(0, length + 1)
      
  //           }
  //           window.history.back()
  //         } else {

  //           // var toast: Toast = {
  //           //   type: 'error',
  //           //   body: "Unable to update reward.",
  //           //   showCloseButton: true,
  //           //   timeout: 2000
  //           // };
  //           // this.toasterService.pop(toast);

  //           this.toastr.error( 'Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
  //             timeOut: 0,
  //             closeButton: true
  //            });
  //         }
  //       }


  //     }, error => {
  //       console.log('resulterror', error);

  //       // var toast: Toast = {
  //       //   type: 'error',
  //       //   body: "Something went wrong.please try again later.",
  //       //   showCloseButton: true,
  //       //   timeout: 2000
  //       // };
  //       // this.toasterService.pop(toast);

  //       this.toastr.error( 'Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
  //         timeOut: 0,
  //         closeButton: true
  //       });
  //     });
  // }
  saveRewardsForCoures() {
    //this.getdatareadyforcourse(this.rewardsCert.conid,this.rewardsBadge.conid);
    if(this.rewardsBadge.conid || this.rewardsCert.conid){
    let param = {
      "cId": this.courseId,
      "tId":  this.tenantId,
      "userId": this.userId,
      "badgeId": this.badge ? 0 : this.rewardsBadge.conid,
      "certId": this.certificate ? 0 : this.rewardsCert.conid
    }

    this.rewardservice.insertreward(param)
      .then(rescompData => {

        var result = rescompData;
        console.log('insertrewardresult', rescompData);

        if (this.optId == 0) {
          if (result['type'] == true) {
            // var toast: Toast = {
            //   type: 'success',
            //   //title: "Server Error!",
            //   body: "Reward added to course.",
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(toast);

            this.toastr.success('Reward added ', 'Success', {
              closeButton: false
             });
            // this.router.navigate(["/pages/plan/courses/content"]);
            // this.router.navigate(["/pages/plan/preoncourses/preoncontent"]);
            window.history.back()
          } else {

            // var toast: Toast = {
            //   type: 'error',
            //   body: "Unable to add reward.",
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(toast);

            this.toastr.error( 'Please contact site administrator and <div (click)="giveFeedback()"> click here <div> to leave your feedback', 'Error', {
              timeOut: 0,
              closeButton: true
              });
          }
        } else if (this.optId == 1) {
          if (result['type'] == true) {
            // var toast: Toast = {
            //   type: 'success',
            //   //title: "Server Error!",
            //   body: "Reward updated to course.",
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(toast);

            this.toastr.success('Reward updated ', 'Success', {
              closeButton: false
             });

            // this.router.navigate(["/pages/plan/courses/content"]);
            // this.router.navigate(["/pages/plan/preoncourses/preoncontent"]);
            if (this.addEditCourseService.breadcrumbArray) {
              var length = this.addEditCourseService.breadcrumbArray.length - 1
              this.addEditCourseService.breadtitle = this.addEditCourseService.breadcrumbArray[length].name
              // this.addassetservice.title = this.title
              this.addEditCourseService.breadcrumbArray = this.addEditCourseService.breadcrumbArray.slice(0, length)
              // this.title = this.addassetservice.breadCrumbArray[]
              this.addEditCourseService.previousBreadCrumb = this.addEditCourseService.previousBreadCrumb.slice(0, length + 1)

            }
            // window.history.back()
          } else {

            // var toast: Toast = {
            //   type: 'error',
            //   body: "Unable to update reward.",
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(toast);

            this.toastr.error( 'Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
              timeOut: 0,
              closeButton: true
             });
          }
        }


      }, error => {
        console.log('resulterror', error);

        // var toast: Toast = {
        //   type: 'error',
        //   body: "Something went wrong.please try again later.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);

        this.toastr.error( 'Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
          timeOut: 0,
          closeButton: true
        });
      });
    }else {
      this.toastr.warning('Please select badge or certificate','Warning')
    }
  }
  getdatareadyforcourse(certId, badgeId) {
    if (certId == null || certId == undefined) {
      this.formattedrewards = badgeId + "|" + null;
    } else if (badgeId == null || badgeId == undefined) {
      this.formattedrewards = null + "|" + certId;
    } else {
      this.formattedrewards = badgeId + "|" + certId;
    }
    console.log('this.formattedrewards', this.formattedrewards);
  }
}
