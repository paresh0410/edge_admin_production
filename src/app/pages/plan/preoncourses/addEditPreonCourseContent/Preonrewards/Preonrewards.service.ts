import {Injectable, Inject} from '@angular/core';
import {Http, Response, Headers, RequestOptions, Request} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppConfig} from '../../../../../app.module';
import { webApi} from '../../../../../service/webApi';
import { HttpClient } from "@angular/common/http";

@Injectable()

export class PreonrewardsService {

  public data: any;
  request: Request;

  private _urlProfileFields:string = "/api/edge/course/getUserProfileFields";
  private _urlInsertRewardUrl:string = webApi.domain + webApi.url.insertrewardtocourse;
  private _urlGetCourseReward:string = webApi.domain + webApi.url.getcourserewards;
  

  constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private _http: Http,private _httpClient:HttpClient){
      //this.busy = this._http.get('...').toPromise();
  }


insertreward(param){
  //  let url:any = this._urlInsertRewardUrl;
  //  return this._http.post(url,param)
  //      .map((response:Response)=>response.json())
  //      .catch(this._errorHandler);

       return new Promise(resolve => {
        this._httpClient.post(this._urlInsertRewardUrl, param)
        //.map(res => res.json())
        .subscribe(data => {
            resolve(data);
        },
        err => {
            resolve('err');
        });
    });
 } 

 getcourserewards(param){
  // let url:any = this. _urlGetCourseReward;
  // return this._http.post(url,param)
  //     .map((response:Response)=>response.json())
  //     .catch(this._errorHandler);
  return new Promise(resolve => {
    this._httpClient.post(this._urlGetCourseReward, param)
    //.map(res => res.json())
    .subscribe(data => {
        resolve(data);
    },
    err => {
        resolve('err');
    });
});
 }

 
  _errorHandler(error: Response){
    // console.error(error);
     return Observable.throw(error || "Server Error")
  }


}
