import { Injectable } from '@angular/core';
import { webApi } from '../../../service/webApi';
import { HttpClient } from "@angular/common/http";


@Injectable({
  providedIn: 'root'
})
export class PartnerListServiceService {
  private get_partnerList: string = webApi.domain + webApi.url.getAllPartnerList;
  private update_partnerList = webApi.domain + webApi.url.updatePartnerList;
  constructor(private _http: HttpClient) { }

  getAllPartnerList() {
    return new Promise(resolve => {
      this._http.post(this.get_partnerList, {})
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  updatePartnerList(data) {
    return new Promise(resolve => {
      this._http.post(this.update_partnerList, data)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
}
