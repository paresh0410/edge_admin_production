import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
import { PartnerListServiceService } from './partner-list-service.service';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { webApi } from '../../../service/webApi';
import { SuubHeader } from '../../components/models/subheader.model';
import { PassService } from '../../../service/passService';
import { noData } from '../../../models/no-data.model';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'ngx-partner-list',
  templateUrl: './partner-list.component.html',
  styleUrls: ['./partner-list.component.scss']
})
export class PartnerListComponent implements OnInit {
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:'No Partners at this time ',
    desc:'Partners will appear after they are added by the admin. Partners are external training companies which are mapped to trainers.',
    titleShow:true,
    btnShow:true,
    descShow:true,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/add-partner',
  }
  header: SuubHeader  = {
    title:'Partner',
    btnsSearch: true,
    placeHolder:"Search by Partner Name ",
    searchBar: true,
    dropdownlabel: ' ',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: '',
    btnName3: '',
    btnAdd: 'Add',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: true,
    filter: false,
    showBreadcrumb: true,
    breadCrumbList:[]   
  };
  partnerJson1: any = []
  partnerJson: any = []
  addeditdata: any = [];
  userDetails: any = [];
  param: any = [];
  valueVisible
  editData: any = [];
  msg: any;
  btnName: string = 'Save';
  partnerName:any
  partnerAddress2:any
  partnerAddress1:any
  title: string = '';
  sectionshow: boolean = false;
  searchParticipant: any;
  loader: any;
  noMaster: any;
  // fullAddress:any=[];

  labels: any = [
		// { labelname: 'ID', bindingProperty: 'partnerId', componentType: 'text' },
    { labelname: 'PARTNER NAME', bindingProperty: 'partnerName', componentType: 'text' },
    { labelname: 'ADDRESS', bindingProperty: 'fullAdress', componentType: 'text' },
    // { labelname: 'ADDRESS', bindingProperty: 'partnerAddress2', componentType: 'text' },
		{ labelname: 'ACTION', bindingProperty: 'btntext', componentType: 'button' },
    { labelname: 'EDIT', bindingProperty: 'tenantId', componentType: 'icon' },
  ]
  
  Form = new FormGroup({
    'name': new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(100)
    ]),
    'address': new FormControl('', [
      Validators.required,
      Validators.minLength(5),
      Validators.maxLength(100)

    ]),
    'address1': new FormControl('', [
      Validators.minLength(5),
      Validators.maxLength(100),

    ]),
  })
  pager: any;
  temData: any;
  pageSize: number = 10;
  displayTableData: any;
  searchText: any;
  constructor(private router: Router, private toastr: ToastrService,private commonFunctionService: CommonFunctionsService,
     private partnerData: PartnerListServiceService, private http1: HttpClient,private pagservice: PassService,private spinner: NgxSpinnerService) {
    this.searchParticipant = {}
    this.getHelpContent();
  }
  ngOnInit() {
    this.getAllPartnerList()
    // this.loader = true;
    this.spinner.show();
    this.userDetails = JSON.parse(localStorage.getItem('LoginResData'));
    this.param = {
      tId: this.userDetails.data.data.tenantId,
    };
    this.header.breadCrumbList=[{
      'name': 'Settings',
      'navigationPath': '/pages/plan',
      }]
  }
  getAllPartnerList() {
    // this.loader = true;
    this.spinner.show();
    this.noMaster=false;
    const get_partnerList: string = webApi.domain + webApi.url.getAllPartnerList;
    this.commonFunctionService.httpPostRequest(get_partnerList,{})
    //this.partnerData.getAllPartnerList()
    .then(
      (data) => {
        this.partnerJson1 = data
        this.partnerJson = this.partnerJson1.data
        this.temData = this.partnerJson
        // this.loader = false;
    this.spinner.hide();
        this.noMaster=false;
        // this.setpage(1)
        if(this.partnerJson.length==0){
        this.noMaster=true;
        }
        for (let i = 0; i < this.partnerJson.length; i++) {
            // this.fullAddress=this.partnerAddress1[i]+this.partnerAddress2[i]

          if(this.partnerJson[i].visible == 1) {
            this.partnerJson[i].btntext = 'fa fa-eye';
          } else {
            this.partnerJson[i].btntext = 'fa fa-eye-slash';
          }
          if(this.partnerJson[i].partnerAddress1==null&&this.partnerJson[i].partnerAddress2==null){
            this.partnerJson[i].fullAdress=" ";
          }
          else if(this.partnerJson[i].partnerAddress1==null){
            this.partnerJson[i].fullAdress=this.partnerJson[i].partnerAddress2;
          }
          else if(this.partnerJson[i].partnerAddress2==null){
            this.partnerJson[i].fullAdress=this.partnerJson[i].partnerAddress1;
          }
            else{
          this.partnerJson[i].fullAdress = this.partnerJson[i].partnerAddress1 + ' ' + this.partnerJson[i].partnerAddress2
            }
        }
    // for (let i = 0; i < this.partnerJson.length; i++) {
    //   if(this.partnerAddress1.partnerJson[i]!=undefined&& this.partnerAddress2.partnerJson[i]!=undefined){
    //   this.fullAddress[i]=this.partnerAddress1.partnerJson[i]+this.partnerAddress2.partnerJson[i];
    //   }
    //   else if(this.partnerAddress1.partnerJson[i]!=undefined&&this.partnerAddress2.partnerJson[i]==undefined)
    //   {
    //     this.fullAddress[i]=this.partnerAddress1.partnerJson[i];
    //   }
    //   else if(this.partnerAddress1.partnerJson[i]==undefined&&this.partnerAddress2.partnerJson[i]!=undefined)
    //   {
    //     this.fullAddress[i]=this.partnerAddress2.partnerJson[i];
    //   }
    //   else{
    //     this.fullAddress[i]="NA";
    //   }
    // }

   
        console.log("data=========" ,this.partnerJson);
        // console.log("fullAddress=========" ,this.fullAddress);
      },
      err => {
        this.presentToast('error', '');
        this.loader=false
        console.log(err);
        this.noMaster=true;
      });
      // (error: any) =>( this.loader=false,console.error(error)))
      this.noMaster=false;
  }
  // getFullAdress(){
  //   this.fullAddress="";
  //   for (let i = 0; i < this.partnerJson.length; i++) {
  //     this.fullAddress=this.partnerAddress1[i+1]+this.partnerAddress2[i+1];
  //   }

  // }
  back() {
    this.router.navigate(['../pages/plan']);
  }
  clear() {
    // this.searchParticipant = '';
    if(this.searchParticipant.length>=3){
      this.getAllPartnerList()
    this.searchParticipant = {};
    }
    this.searchParticipant = {};  
  }
  visibilityTableRow(row) {
    let value;
    let status;

    if (row.visible == 1) {
      row.btntext = 'fa fa-eye-slash';
      value  = 'fa fa-eye-slash';
      row.visible = 0
      status = 0;
    } else {
      status = 1;
      value  = 'fa fa-eye';
      row.visible = 1;
      row.btntext = 'fa fa-eye';
    }console.log(row);
    this.valueVisible=status
    this.addeditdata = {
      partnerId: row.partnerId,
      visible: this.valueVisible,
      partnerName: row.partnerName,
      partnerAddress1:row.partnerAddress1,
      partnerAddress2:row.partnerAddress2
    };
    
    if (status === 1) {
      this.msg = 'Partner Enabled Successfully' ;
    } else {
      this.msg = 'Partner Disabled Successfully' ;
      this.sectionshow = false;
    }
    this.updatedata(this.addeditdata, response => {
      console.log('dataMaster', response);
      if (response.type === true) {
        this.sectionshow = false;
        this.presentToast('success', this.msg);
        this.loader = false;
    this.spinner.hide();
      } else {
        this.presentToast('error', '');
        this.loader = false;
    this.spinner.hide();
      }
    });
    this.presentToast('success', this.msg);
  }
  // disableVisibility(index, data, status) {
  //   this.valueVisible=status
  //   this.addeditdata = {
  //     partnerId: data.partnerId,
  //     visible: this.valueVisible,
  //     partnerName: data.partnerName,
  //     partnerAddress1:data.partnerAddress1,
  //     partnerAddress2:data.partnerAddress2
  //   };
    
  //   if (status === 1) {
  //     this.msg = 'Enabled' + ' ' + 'Parntner';
  //   } else {
  //     this.msg = 'Disabled' + ' ' + 'Partner';
  //     this.sectionshow = false;
  //   }
  //   this.result();
  //   this.presentToast('success', this.msg);
  // }
  
  editTableRow(data){
    this.title = 'Update Partner';
    this.partnerName=this.editData.partnerName
    this.partnerAddress1=this.editData.partnerAddress1
    this.partnerAddress2=this.editData.partnerAddress2
  this.sectionshow = true;
  }
  showTemplate(data, value) {
    this.editData = data ? data : [];
    if (value === 0) {
      this.editData.partnerId = 0;
      this.editData.visible = 0;
      this.title = 'Add Partner'
    }
    else {
      this.title = 'Update Partner';
      this.partnerName=this.editData.partnerName
      this.partnerAddress1=this.editData.partnerAddress1
      this.partnerAddress2=this.editData.partnerAddress2
    }
    this.sectionshow = true;
  }
  closesectionModel() {
    this.sectionshow = false;
    this.Form.reset()
  }
  saveTemp(form) {

    if(form && form.valid){
      this.addeditdata = {
        partnerId: this.editData.partnerId,
        visible: this.editData.visible,
        partnerName: this.partnerName,
        partnerAddress1:this.partnerAddress1,
        partnerAddress2:this.partnerAddress2
      };
      if (this.addeditdata.partnerId === 0) {
        this.msg = 'You have successfully added' + ' ' + 'Partner';
        // this.loader = true;
    this.spinner.show();
      } else {
        this.msg = 'You have successfully updated' + ' ' + 'Partner';
        // this.loader = true;
    this.spinner.show();
      }
      this.result();
      this.Form.reset()
    }else {
      Object.keys(this.Form.controls).forEach(key => {
        this.Form.controls[key].markAsDirty();
      });
    }
    
  }
  result() {
    // this.loader=true;
    this.updatedata(this.addeditdata, response => {
      console.log('dataMaster', response);
      if (response.type === true) {
        this.getAllPartnerList()
        this.sectionshow = false;
        this.presentToast('success', this.msg);
        // this.loader = false;
    this.spinner.hide();
      } else {
        this.presentToast('error', '');
        // this.loader = false;
    this.spinner.hide();
      }
    });
  }
  updatedata(addeditdata, cb) {
    const update_partnerList = webApi.domain + webApi.url.updatePartnerList;
    this.commonFunctionService.httpPostRequest(update_partnerList,addeditdata)
    //this.partnerData.updatePartnerList(addeditdata)
    .then(res => {
      cb(res);
    }, err => {
      this.presentToast('error', '');
      console.log(err);
    });
  }
  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }
  get name() { return this.Form.get('name'); }
  get address() { return this.Form.get('address'); }
  get address1() { return this.Form.get('address1'); }
  helpContent: any;
  getHelpContent() {
    return new Promise(resolve => {
      this.http1
        .get('../../../../../../assets/help-content/addEditCourseContent.json')
        .subscribe(
          data => {
            this.helpContent = data;
            console.log('Help Array', this.helpContent);
          },
          err => {
            resolve('err');
          }
        );
    });
  }

  searchData(event){
    this.searchParticipant = event.target.value
    const searchKeyword = this.searchParticipant.toLowerCase();
    this.noMaster=false
    if(searchKeyword.length>=3 ||searchKeyword.length==0){
    const temp = this.temData.filter(function (d) {
      return String(d.partnerName).toLowerCase().indexOf(searchKeyword) !== -1 ||
        !searchKeyword||
        String(d.partnerAddress1).toLowerCase().indexOf(searchKeyword) !== -1
    });
    this.partnerJson = temp;
  
    if(temp.length==0){
      this.noMaster=true
      this.noDataVal={
        margin:'mt-5',
        imageSrc: '../../../../../assets/images/no-data-bg.svg',
        title:"Sorry we couldn't find any matches please try again",
        desc:".",
        titleShow:true,
        btnShow:false,
        descShow:false,
        btnText:'Learn More',
        btnLink:''
      }
    }
      }
}
  search(event){
    this.searchText=event.target.value;
    if(this.searchText.length>=3||this.searchText.length==0){
    this.searchParticipant.partnerName =  event.target.value
  }
  this.noDataVal={
    margin:'mt-5',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"Sorry we couldn't find any matches please try again",
    desc:".",
    titleShow:true,
    btnShow:false,
    descShow:false,
    btnText:'Learn More',
    btnLink:''
}
}

  setpage(page) {
    //this.nodata = false;
    this.pager = this.pagservice.getPager(this.temData.length, page, this.pageSize);
    console.log(this.pager.pages,"THIS.PAGER");
    this.dispalyData()
  }

  dispalyData() {
    this.displayTableData = this.temData.slice(this.pager.startIndex, this.pager.endIndex + 1);
    console.log(this.displayTableData,"this.displayTable")
    this.partnerJson = this.displayTableData

  }
}
