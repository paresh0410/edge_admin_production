import { TestBed } from '@angular/core/testing';

import { PartnerListServiceService } from './partner-list-service.service';

describe('PartnerListServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PartnerListServiceService = TestBed.get(PartnerListServiceService);
    expect(service).toBeTruthy();
  });
});
