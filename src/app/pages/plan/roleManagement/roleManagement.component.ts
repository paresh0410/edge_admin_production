import {Component, OnDestroy} from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators/takeWhile' ;
import { Router,ActivatedRoute} from '@angular/router';

interface CardSettings {
  title: string;
  iconClass: string;
  type: string;
}

@Component({
  selector: 'ngx-roleManagement',
  styleUrls: ['./roleManagement.component.scss'],
  templateUrl: './roleManagement.component.html',
})
export class roleManagement implements OnDestroy {

  private alive = true;

  rows: any[] = [
    {
      id:1,
      name:'Admin'
    },
    {
      id:2,
      name:'Manager'
    },
    {
      id:3,
      name:'Student'
    },
  ];
  selected = [];
  loader:any;
  errorMsg:any;
  userLoginData:any;

  data:any=[];

  constructor(private themeService: NbThemeService,public router:Router,public routes:ActivatedRoute) {

  }

  ngOnDestroy() {
    this.alive = false;
  }

  onSelect({ selected }) {
    console.log('Select Event', selected, this.selected);

    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }

  onActivate(event) {
    if(event.type =='click' && event.cellIndex != 0)
    {
      this.router.navigate(['addeditrole'],{relativeTo:this.routes});
    }
    console.log('Activate Event', event);
  }
  gotoaddrole(){
    this.router.navigate(['addeditrole'],{relativeTo:this.routes});
  }

  back(){
    this.router.navigate(['/pages/plan']);
  }


  displayCheck(row) {
    // return row.name !== 'Ethel Price';
  }

  onPage(event) {
    // clearTimeout(this.timeout);
    // this.timeout = setTimeout(() => {
    //   // console.log('paged!', event);
    // }, 100);
  }

  onDetailToggle(event) {
    console.log('Detail Toggled', event);
  }


  // gotocourses(){
  //   this.router.navigate(['courses'],{relativeTo:this.routes});
  // }

  // gotocoursebundle(){
  //   this.router.navigate(['courseBundle'],{relativeTo:this.routes});
  // }

  
}
