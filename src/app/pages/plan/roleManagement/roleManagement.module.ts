import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { NgxEchartsModule } from 'ngx-echarts';
 import { ThemeModule } from '../../../@theme/theme.module';
import { roleManagement } from './roleManagement.component';
import { ReactiveFormsModule } from '@angular/forms';
import {roleManagementService } from './roleManagement.service';
// import { CourseBundleService } from './courseBundle/courseBundle.service';
// import { CourseBundle } from './courseBundle/courseBundle';
// import { ContentService } from './courses/content/content.service';
import { CommonModule } from '@angular/common';
import { SortablejsModule } from 'angular-sortablejs';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

// import { CourseBundleModule } from './courseBundle/courseBundle.module';


@NgModule({
  imports: [
     ThemeModule,
    NgxEchartsModule,
    CommonModule,
    SortablejsModule,
    ReactiveFormsModule,
    NgxDatatableModule
    // CourseBundleModule
  ],
  declarations: [
    roleManagement,
    // CourseBundle
  ],
  providers: [
    roleManagementService,
    // CourseBundleService,
    // ContentService
  ],
  schemas: [ 
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA 
  ]
})
export class roleManagementModule { }
