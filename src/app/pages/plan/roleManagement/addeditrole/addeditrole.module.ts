import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { NgxEchartsModule } from 'ngx-echarts';
 import { ThemeModule } from '../../../../@theme/theme.module';
import { addeditrole } from './addeditrole.component';
import { ReactiveFormsModule } from '@angular/forms';
import {addeditroleService } from './addeditrole.service';
// import { CourseBundleService } from './courseBundle/courseBundle.service';
// import { CourseBundle } from './courseBundle/courseBundle';
// import { ContentService } from './courses/content/content.service';
import { CommonModule } from '@angular/common';
import { SortablejsModule } from 'angular-sortablejs';

// import { CourseBundleModule } from './courseBundle/courseBundle.module';


@NgModule({
  imports: [
     ThemeModule,
    NgxEchartsModule,
    CommonModule,
    SortablejsModule,
    ReactiveFormsModule,
    // CourseBundleModule
  ],
  declarations: [
    addeditrole,
    // CourseBundle
  ],
  providers: [
    addeditroleService,
    // CourseBundleService,
    // ContentService
  ],
  schemas: [ 
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA 
  ]
})
export class addeditroleModule { }
