import {
  Component,
  EventEmitter,
  Input,
  Output,
  OnInit,
  SimpleChanges,
  ViewChild,
  OnChanges,
  // Renderer2,
  ChangeDetectorRef,
} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import * as $ from 'jquery';
import { PassService } from '../../../service/passService';

@Component({
  selector: 'ngx-add-edit-activity',
  templateUrl: './add-edit-activity.component.html',
  styleUrls: ['./add-edit-activity.component.scss'],
})
export class AddEditActivityComponent implements OnInit, OnChanges {
  @Input() subtypeId: any;
  @Input() formDataActivity: any;
  @Input() pointFormat: any = [];
  @Input() tagList: any = [];
  @Input() roles: any = [];
  @Input() days: any = [];
  @Input() activityCompTrack: any = [];
  @Input() activitieslist: any = [];
  @Input() currentActIndex: any = [];
  @Input() selectedTags: any = [];
  @Input() workflowSteps: any = [];
  @Input() showContentSelected: any;
  @Input() selectedContentData: any;
  @Input() selectedMultiLanguageContent: any = [];
  @Input() allowMultiLanguage: boolean = false;
  @Input() formats: any;
  @Input() activityFileData: any;
  @Input() enableUpload = false;
  @Input() isWorkflow = false;
  @Input() courseTypeId: number;

  @Input() courseTypeDetails = {
    supTypeName: '',
    subTypeName: '',
  };
  @Input() shortUrl: any;
  @Input() linkGenerationInProgress: any;
  @Input() showGenrateButton: any;

  allowedTypes = [];
  activityFormatIcon = "";
  files = [];

  // subtypeId: any;
  // formDataActivity: any;
  // pointFormat: any = [];
  // tagList: any = [];
  // roles: any = [];
  // days: any = [];
  // activityCompTrack: any = [];
  // activitieslist: any = [];
  // currentActIndex: any = [];
  // selectedTags: any = [];
  // contentSelected: any;
  // selectedContent: any;

  // @Input() addEditAcitivityContentData: any;

  @Output() saveActivity = new EventEmitter<any>();
  @Output() browseEvent = new EventEmitter<any>();
  @Output() closeActivityForm = new EventEmitter<any>();
  @Output() generateAcitivitylink = new EventEmitter<any>();
  @Output() removeContentEvent = new EventEmitter<any>();
  @Output() removeMultiContentEvent = new EventEmitter<any>();
  @Output() formatChangeForMultiLanguage = new EventEmitter<any>();
  @ViewChild('fileUpload') fileUpload: any;
  @ViewChild('form') form;
  modules: any = [
    {
      modulename: 'Module name 1',
      activities: [
        { actname: 'Lorem Ipsum is simply dummy' },
        { actname: 'Lorem Ipsum is simply dummy' },
        { actname: 'Lorem Ipsum is simply dummy' },
      ],
    },
    {
      modulename: 'Module name 2',
      activities: [
        { actname: 'Lorem Ipsum is simply dummy' },
        { actname: 'Lorem Ipsum is simply dummy' },
        { actname: 'Lorem Ipsum is simply dummy' },
      ],
    },
  ];

  oldFormatId = null;
  oldSelectedActivityList = [];

  checkedAnyOneCheckBox = true;
  fileValidFlag: boolean= true;
  sessionCheckShow: boolean=false;
  constructor(
    private http1: HttpClient,
    // private render: Renderer2,
    private toastr: ToastrService,
    private passService: PassService,
    public cdf: ChangeDetectorRef
  ) {
    console.log('modules ==>', this.modules);
    this.getHelpContent();
    this.formDataActivity = {
      activityName: '',
      activityDuration: '',
      activityId: '',
      completionCriteria: '',
      completionDate: '',
      completionDays: '',
      contentId: '',
      contentRepId: '',
      courseId: '',
      creditAllocId: '',
      dependentActId: '',
      description: '',
      moduleId: '',
      name: '',
      points: '',
      reference: '',
      startDate: '',
      tags: '',
      tenantId: '',
      craditpoints: [],
      startDays: '',
      byTrainer: '',
      byLearner: '',
      mimeType: '',
      referenceType: '',
      formatId: '',
    };
    if(this.passService.isWorkflow){
      this.sessionCheckShow=true;
    }
    // this.contentSelected
  }
  settingsTagDrop = {
    text: 'Select Tags',
    singleSelection: false,
    classes: 'common-multi',
    primaryKey: 'id',
    labelKey: 'name',
    noDataLabel: 'Search Tags...',
    enableSearchFilter: true,
    lazyLoading: true,
    searchBy: ['name'],
    maxHeight: 250,
  };
  ngOnInit() {
    // console.log('this.contentSelected', this.contentSelected);
    // console.log('this.selectedContent', this.selectedContent);
  }
  // Help Code Start Here //

  //tags data
  onTagsSelect(item: any) {
    console.log(item);
    console.log(this.selectedTags);
  }
  OnTagDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedTags);
  }
  onTagSearch(evt: any) {
    console.log(evt.target.value);
    const val = evt.target.value;
    this.tagList = [];
    // const temp = this.tempTags.filter(function (d) {
    //   return (
    //     String(d.name)
    //       .toLowerCase()
    //       .indexOf(val) !== -1 ||
    //     !val
    //   );
    // });
    // update the rows
    // this.tagList = temp;
    console.log('filtered Tag LIst', this.tagList);
  }

  helpContent: any;
  getHelpContent() {
    return new Promise((resolve) => {
      this.http1
        .get('../../../../../../assets/help-content/addEditCourseContent.json')
        .subscribe(
          (data) => {
            this.helpContent = data;
            this.cdf.detectChanges();
            console.log('Help Array', this.helpContent);
          },
          (err) => {
            resolve('err');
          }
        );
    });
    // return this.helpContent;
  }
  fileName = 'You can drag and drop files here to add them.';

  callSaveActivity(form) {
    console.log('form event ==>', event);
    if (form.valid) {
      if (this.subtypeId === 1) {
        for(let i = 0; i < this.files.length; i++){
          if (this.files[i].validSize && this.files[i].valid && !this.files[i].alreadyUploadedFlag){
            this.activityFileData = this.files[i];
            this.fileValidFlag=this.files[i].valid
            break;
          }
        }
        if (!this.fileValidFlag) {
          this.toastr.warning('Please select valid file.', 'Warning');
          return null;
        }else
        if (
          this.formDataActivity.reference === '' &&
          this.activityFileData == null
        ) {
          this.toastr.warning('Please upload file.', 'Warning');
          return null;
        }
      }else if (Number(this.subtypeId) === 9 || Number(this.subtypeId) === 11){

      }else if(this.subtypeId === 2){
        if(this.allowMultiLanguage){
          if (this.selectedMultiLanguageContent.length === 0) {
            this.toastr.warning('Please select file.', 'Warning');
            return null;
          }
        }else {
          if (this.selectedContentData.name == '') {
            this.toastr.warning('Please select file.', 'Warning');
            return null;
          }
        }
      }else {
        if (this.selectedContentData.name == '') {
          this.toastr.warning('Please browse content first.', 'Warning');
          return null;
        }
      }
        // if(this.formDataActivity.reference === ''  && this.activityFileData == null){
        //   this.toastr.warning('Please Upload asset.', 'Warning');
        //   return null;
        // }

      const event = {
        isValid: form.valid,
        formData: this.formDataActivity,
        formObject: form,
        selectedTags: this.selectedTags,
      };

      if (this.subtypeId === 1) {

        event['activityFileData'] = this.activityFileData;
      } else {
        event['activityFileData'] = null;
      }
      this.saveActivity.emit(event);
    } else {
      Object.keys(form.controls).forEach((key) => {
        form.controls[key].markAsDirty();
      });
      this.toastr.warning('Please fill all required fields', 'warning');
    }
  }

  browse(fileEvent) {
    if (
      (this.formDataActivity.formatId == '' ||
        !this.formDataActivity.formatId ||
        this.formDataActivity.formatId == null) &&
      this.subtypeId === 2
    ) {
      this.toastr.warning('Please Select format first', 'warning');
    } else {
      const event = {
        subtypeId: this.subtypeId,
        fileEvent: fileEvent,
        formatId: this.formDataActivity.formatId,
      };
      this.browseEvent.emit(event);
    }
  }

  closeActivityFormWindow() {
    this.closeActivityForm.emit();
  }
ngOnDestroy(){
  this.closeActivityForm.emit();
}
  ngOnChanges(changes: SimpleChanges) {
    // changes.prop contains the old and the new value...
    // this.intializeDataToVaraible();
    console.log('contentTypeID ==>', this.subtypeId);
    console.log('contentTypeID ==>', this.selectedContentData);
    console.log('contentTypeID ==>', this.formDataActivity.reference);
    if (this.formDataActivity.reference) {
      // this.fileName = this.formDataActivity.reference.substring(
      //   this.formDataActivity.reference.lastIndexOf('/') + 1
      // );
      // this.selectFileTitle = this.fileName;
      // if (this.form) {
      //   console.log('Form Controls ==>', this.form.controls);
      //   // if(this.form.controls){
      //   //   this.form.controls['file'].setValue(this.formDataActivity.reference);
      //   //   this.form.controls['file'].status = 'VALID';
      //   // }

      //   console.log('Form Controls ==>', this.form);
      // }
      this.makeFileComponentDataReady();
    }
    if(this.formDataActivity['formatId'] &&
     this.formDataActivity['formatId'] != "" &&
      this.formDataActivity['formatId'] != null){
        this.formDataActivity['formatIcon'] = this.bindActivityIcon(Number(this.formDataActivity['formatId']));
    }
    // console.log('contentTypeID ==>', this.subtypeId);
  }

  // intializeDataToVaraible(){
  //   if (this.addEditAcitivityContentData) {
  //     this.formDataActivity = this.addEditAcitivityContentData[
  //       'formDataActivity'
  //     ];
  //     this.subtypeId = this.addEditAcitivityContentData['subtypeId'];
  //     this.pointFormat = this.addEditAcitivityContentData['pointFormat'];
  //     this.tagList = this.addEditAcitivityContentData['tagList'];
  //     this.roles = this.addEditAcitivityContentData['roles'];
  //     this.days = this.addEditAcitivityContentData['days'];
  //     this.activityCompTrack = this.addEditAcitivityContentData[
  //       'activityCompTrack'
  //     ];
  //     this.activitieslist = this.addEditAcitivityContentData['activitieslist'];
  //     this.currentActIndex = this.addEditAcitivityContentData[
  //       'currentActIndex'
  //     ];
  //     this.selectedTags = this.addEditAcitivityContentData['selectedTags'];
  //     this.contentSelected = this.addEditAcitivityContentData[
  //       'contentSelected'
  //     ];
  //     this.selectedContent = this.addEditAcitivityContentData[
  //       'selectedContent'
  //     ];
  //   }
  //   console.log('addEditAcitivityContentData', this.addEditAcitivityContentData);
  // }
  // preview data
  assetrefLink = '';
  //  files: any;
  isLocalfile: boolean = false;

  convertFileUrl(fileurl) {
    console.log(fileurl, 'fileUrl');
    if (!fileurl) {
      return fileurl;
    } else if (fileurl.includes('C:\fakepath')) {
      return fileurl.split('C:\fakepath')[1];
    } else {
      return fileurl;
    }
  }
  removeFile() {
    this.activityFileData = null;
    this.fileName = 'You can drag and drop files here to add them.';
    this.selectFileTitle = 'No file chosen';
    this.formDataActivity.assetRef = '';
    this.assetrefLink = '';
    this.isLocalfile = false;
    this.formDataActivity.reference = '';
    // this.nativeElementConfigure();
  }

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false,
      });
    } else if (type === 'error') {
      this.toastr.error(
        'Please contact site administrator and <a > <strong> click here </strong></a> to leave your feedback.',
        'Error',
        {
          timeOut: 0,
          closeButton: true,
        }
      );
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false,
      });
    }
  }
  //  @ViewChild('video_file') videofile: any;
  //  @ViewChild('image_file') imagefile: any;
  //  @ViewChild('audio_file') audiofile: any;
  //  @ViewChild('iframe_file') iframe: any;
  //  @ViewChild('ppt_file') ppt: any;
  //  @ViewChild('word_file') word: any;
  //  @ViewChild('excel_file') excel: any;
  //  @ViewChild('scorm_file') scormfile: any;
  selectFileTitle = '';
  resetFlag = false;
  formatChange(event) {
    this.fileName = 'You can drag and drop files here to add them.';
    this.selectFileTitle = 'No file chosen';
    this.formDataActivity.assetRef = '';
    this.assetrefLink = '';
    if((Number(this.subtypeId) === 2)){
      this.oldFormatId = this.formDataActivity.formatId;
      this.oldSelectedActivityList = this.selectedMultiLanguageContent;
      this.selectedMultiLanguageContent = [];
      const eventToPass = {
        'oldFormatId': this.oldFormatId,
        'oldSelectedActivityList': this.oldSelectedActivityList,
        'selectedMultiLanguageContent': this.selectedMultiLanguageContent,
      }
      this.formatChangeForMultiLanguage.emit(eventToPass);

    };
    this.isLocalfile = false;
    console.log('formDataActivity.formatId', this.formDataActivity.formatId);
    this.formDataActivity['formatIcon'] = this.bindActivityIcon(Number(event.target.value));
    this.passFormatType();
    this.resetFlag = true;
    setTimeout(()=>{
      this.resetFlag = false;
    }, 500);
    this.cdf.detectChanges();
    //  this.nativeElementConfigure();
  }
  removePointsList(i: number) {
    // this.credits.removeAt(i);
    // this.credits.splice(i,1);
    this.formDataActivity.craditpoints.splice(i, 1);
    this.roleSelectedList.splice(i, 1);
    this.selectedRole.splice(i, 1);
    // this.addPointsDisableEnable();
    this.disableSelectedRole();
  }

  disableSelectedRole() {
    this.roles.forEach((data, key) => {
      if (this.selectedRole.indexOf(data.name) >= 0) {
        this.roles[key].isSelected = 1;
      } else {
        this.roles[key].isSelected = 0;
      }
    });
    console.log('Selected Disabled', this.roles);
  }
  //  nativeElementConfigure() {
  //    setTimeout(() => {
  //      if (this.imagefile) {
  //        this.render.listen(this.imagefile.nativeElement, 'change', () => {
  //          this.isLocalfile = true;
  //          var size = 100000000;
  //          var validExts = new Array('image');
  //          this.files = this.imagefile.nativeElement.files;
  //          var fileType = this.files[0].type;
  //          console.log('fileType', fileType);
  //          var fileExt = false;
  //          for (let i = 0; i < validExts.length; i++) {
  //            if (fileType.includes(validExts[i])) {
  //              fileExt = true;
  //              break;
  //            }
  //          }
  //          if (!fileExt) {
  //            this.presentToast('warning', 'Please select a file of type image');
  //            this.removeFile();
  //          } else if (size <= this.files[0].size) {
  //            this.presentToast('warning', 'File size should be less than 100 MB');
  //            this.removeFile();
  //          } else {
  //            try {
  //              console.log('image', this.files[0]);
  //              this.formDataActivity.assetRef = this.files[0].name;
  //              this.assetrefLink = '';
  //              this.fileName = this.files[0].name;
  //              this.activityFileData = this.files[0];
  //              this.selectFileTitle = this.files[0].name;
  //              this.enableUpload = true;
  //              console.log('this.fileName', this.fileName);
  //              console.log('this.activityFileData', this.activityFileData);
  //              console.log('this.islocal',this.isLocalfile)
  //              setTimeout(() => {
  //                this.cdf.detectChanges();
  //                let $source: any = $('#image_here');
  //                $source[0].src = URL.createObjectURL(this.files[0]);
  //                // $source.parent()[0].load();
  //              }, 500);
  //            } catch (e) {
  //              console.log(e);
  //            }
  //          }
  //        });
  //      }

  //      // $(document).on('change','#videofile',function(evt){
  //      if (this.videofile) {
  //        this.render.listen(this.videofile.nativeElement, 'change', () => {
  //          this.isLocalfile = true;
  //          var size = 1000000000;
  //          var validExts = new Array('video');
  //          this.files = this.videofile.nativeElement.files;
  //          var fileType = this.files[0].type;
  //          console.log('fileType', fileType);
  //          var fileExt = false;
  //          for (let i = 0; i < validExts.length; i++) {
  //            if (fileType.includes(validExts[i])) {
  //              fileExt = true;
  //              break;
  //            }
  //          }
  //          if (!fileExt) {
  //            this.presentToast('warning', 'Please select a file of type video');
  //            this.removeFile();
  //          } else if (size <= this.files[0].size) {
  //            this.presentToast('warning', 'File size should be less than 100 MB');
  //            this.removeFile();
  //          } else {
  //            try {
  //              console.log('Video', this.files[0]);
  //              this.formDataActivity.assetRef = this.files[0].name;
  //              this.assetrefLink = '';
  //              this.fileName = this.files[0].name;
  //              this.activityFileData = this.files[0];
  //              this.selectFileTitle = this.files[0].name;
  //              this.enableUpload = true;
  //              console.log('this.fileName', this.fileName);
  //              console.log('this.activityFileData', this.activityFileData);
  //              setTimeout(() => {
  //                this.cdf.detectChanges();
  //                let $source: any = $('#video_here');
  //                $source[0].src = window.URL.createObjectURL(this.files[0]); // URL.createObjectURL(this.files[0]);
  //                // $source.parent()[0].load();
  //              }, 500);
  //            } catch (e) {
  //              console.log(e);
  //            }
  //          }
  //        });
  //      }
  //      ////////////////Code for fetch audio file//////////////

  //      // $(document).on('change','#audiofile',function(evt){
  //      if (this.audiofile) {
  //        this.render.listen(this.audiofile.nativeElement, 'change', () => {
  //          // this.isLocalfile = true;
  //          var size = 1000000000;
  //          this.files = this.audiofile.nativeElement.files;
  //          var validExts = new Array('audio');
  //          var fileType = this.files[0].type;
  //          console.log('fileType', fileType);
  //          var fileExt = false;
  //          for (let i = 0; i < validExts.length; i++) {
  //            if (fileType.includes(validExts[i])) {
  //              fileExt = true;
  //              break;
  //            }
  //          }
  //          if (!fileExt) {
  //            this.presentToast('warning', 'Please select a file type of audio');
  //            this.removeFile();
  //          } else if (size <= this.files[0].size) {
  //            this.presentToast('warning', 'File size should be less than 100 MB');
  //            this.removeFile();
  //          } else {
  //            try {
  //              console.log('audio', this.files[0]);
  //              this.formDataActivity.assetRef = this.files[0].name;
  //              this.assetrefLink = '';
  //              this.fileName = this.files[0].name;
  //              this.activityFileData = this.files[0];
  //              this.selectFileTitle = this.files[0].name;
  //              this.enableUpload = true;
  //              console.log('this.fileName', this.fileName);
  //              console.log('this.activityFileData', this.activityFileData);
  //              setTimeout(() => {
  //                this.cdf.detectChanges();
  //                let $source: any = $('#audio_here');
  //                $source[0].src = URL.createObjectURL(this.files[0]);
  //                // $source.parent()[0].load();
  //              }, 1000);
  //            } catch (e) {
  //              console.log(e);
  //            }
  //          }
  //        });
  //      }
  //      ///////////////////audio file End///////////////////////

  //      // $(document).on('change','#iframefile',function(evt){
  //      if (this.iframe) {
  //        this.render.listen(this.iframe.nativeElement, 'change', () => {
  //          this.isLocalfile = true;
  //          var size = 1000000000;
  //          this.files = this.iframe.nativeElement.files;
  //          var validExts = new Array('pdf');
  //          var fileType = this.files[0].type;
  //          console.log('fileType', fileType);
  //          var fileExt = false;
  //          for (let i = 0; i < validExts.length; i++) {
  //            if (fileType.includes(validExts[i])) {
  //              fileExt = true;
  //              break;
  //            }
  //          }
  //          if (!fileExt) {
  //            this.presentToast('warning', 'Please select a file of type pdf');
  //            this.removeFile();
  //          } else if (size <= this.files[0].size) {
  //            this.presentToast('warning', 'File size should be less than 100 MB');
  //            this.removeFile();
  //          } else {
  //            try {
  //              console.log('pdf', this.files[0]);
  //              this.formDataActivity.assetRef = this.files[0].name;
  //              this.assetrefLink = '';
  //              this.fileName = this.files[0].name;
  //              this.activityFileData = this.files[0];
  //              this.selectFileTitle = this.files[0].name;
  //              this.enableUpload = true;
  //              console.log('this.fileName', this.fileName);
  //              console.log('this.activityFileData', this.activityFileData);
  //              setTimeout(() => {
  //                this.cdf.detectChanges();
  //                let $source: any = $('#iframe');
  //                $source[0].src = URL.createObjectURL(this.files[0]);
  //                // $source.parent()[0].load();
  //              }, 500);
  //            } catch (e) {
  //              console.log(e);
  //            }
  //          }
  //        });
  //      }

  //      if (this.ppt) {
  //        this.render.listen(this.ppt.nativeElement, 'change', () => {
  //          this.isLocalfile = true;
  //          var size = 1000000000;
  //          this.files = this.ppt.nativeElement.files;
  //          console.log('this.files', this.files)
  //          var validExts = new Array('application/vnd.ms-powerpoint', 'application/vnd.openxmlformats-officedocument.presentationml.presentation');
  //          var fileType = this.files[0].type;
  //          console.log('fileType', fileType);
  //          var fileExt = false;
  //          //var fileExt = true;
  //          for (let i = 0; i < validExts.length; i++) {
  //            if (fileType.includes(validExts[i])) {
  //              fileExt = true;
  //              break;
  //            }
  //          }
  //          if (!fileExt) {
  //            this.presentToast('warning', 'Please select a file of type ppt');
  //            this.removeFile();
  //          } else if (size <= this.files[0].size) {
  //            this.presentToast('warning', 'File size should be less than 100 MB');
  //            this.removeFile();
  //          } else {
  //            try {
  //              console.log('ppt', this.files[0]);
  //              this.formDataActivity.assetRef = this.files[0].name;
  //              this.assetrefLink = '';
  //              this.fileName = this.files[0].name;
  //              this.activityFileData = this.files[0];
  //              this.selectFileTitle = this.files[0].name;
  //              this.enableUpload = true;
  //              this.cdf.detectChanges();
  //              console.log('this.fileName', this.fileName);
  //              console.log('this.activityFileData', this.activityFileData);
  //              // setTimeout(() => {
  //              //   let $source: any = $('#iframe');
  //              //   $source[0].src = URL.createObjectURL(this.files[0]);
  //              // $source.parent()[0].load();
  //              // }, 200);
  //            } catch (e) {
  //              console.log(e);
  //            }
  //          }
  //        });
  //      }

  //      if (this.word) {
  //        this.render.listen(this.word.nativeElement, 'change', () => {
  //          this.isLocalfile = true;
  //          var size = 1000000000;
  //          this.files = this.word.nativeElement.files;
  //          console.log('this.files', this.files)
  //          var validExts = new Array('application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document');
  //          var fileType = this.files[0].type;
  //          console.log('fileType', fileType);
  //          var fileExt = false;
  //          //var fileExt = true;
  //          for (let i = 0; i < validExts.length; i++) {
  //            if (fileType.includes(validExts[i])) {
  //              fileExt = true;
  //              break;
  //            }
  //          }
  //          if (!fileExt) {
  //            this.presentToast('warning', 'Please select a file of type word');
  //            this.removeFile();
  //          } else if (size <= this.files[0].size) {
  //            this.presentToast('warning', 'File size should be less than 100 MB');
  //            this.removeFile();
  //          } else {
  //            try {
  //              console.log('ppt', this.files[0]);
  //              this.formDataActivity.assetRef = this.files[0].name;
  //              this.assetrefLink = '';
  //              this.fileName = this.files[0].name;
  //              this.activityFileData = this.files[0];
  //              this.selectFileTitle = this.files[0].name;
  //              this.enableUpload = true;
  //              this.cdf.detectChanges();
  //              console.log('this.fileName', this.fileName);
  //              console.log('this.activityFileData', this.activityFileData);
  //              // setTimeout(() => {
  //              //   let $source: any = $('#iframe');
  //              //   $source[0].src = URL.createObjectURL(this.files[0]);
  //              // $source.parent()[0].load();
  //              // }, 200);
  //            } catch (e) {
  //              console.log(e);
  //            }
  //          }
  //        });
  //      }

  //      if (this.excel) {
  //        this.render.listen(this.excel.nativeElement, 'change', () => {
  //          this.isLocalfile = true;
  //          var size = 1000000000;
  //          this.files = this.excel.nativeElement.files;
  //          console.log('this.files', this.files)
  //          var validExts = new Array('application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  //          var fileType = this.files[0].type;
  //          console.log('fileType', fileType);
  //          var fileExt = false;
  //          //var fileExt = true;
  //          for (let i = 0; i < validExts.length; i++) {
  //            if (fileType.includes(validExts[i])) {
  //              fileExt = true;
  //              break;
  //            }
  //          }
  //          if (!fileExt) {
  //            this.presentToast('warning', 'Please select a file of type word');
  //            this.removeFile();
  //          } else if (size <= this.files[0].size) {
  //            this.presentToast('warning', 'File size should be less than 100 MB');
  //            this.removeFile();
  //          } else {
  //            try {
  //              console.log('ppt', this.files[0]);
  //              this.formDataActivity.assetRef = this.files[0].name;
  //              this.assetrefLink = '';
  //              this.fileName = this.files[0].name;
  //              this.activityFileData = this.files[0];
  //              this.selectFileTitle = this.files[0].name;
  //              this.enableUpload = true;
  //              this.cdf.detectChanges();
  //              console.log('this.fileName', this.fileName);
  //              console.log('this.activityFileData', this.activityFileData);
  //              // setTimeout(() => {
  //              //   let $source: any = $('#iframe');
  //              //   $source[0].src = URL.createObjectURL(this.files[0]);
  //              // $source.parent()[0].load();
  //              // }, 200);
  //            } catch (e) {
  //              console.log(e);
  //            }
  //          }
  //        });
  //      }

  //      if (this.scormfile) {
  //        this.render.listen(this.scormfile.nativeElement, 'change', () => {
  //          this.isLocalfile = true;
  //          var size = 100000000;
  //          this.files = this.scormfile.nativeElement.files;
  //          var validExts = new Array('zip');
  //          var fileType = this.files[0].type;
  //          console.log('fileType', fileType);
  //          var fileExt = false;
  //          for (let i = 0; i < validExts.length; i++) {
  //            if (fileType.includes(validExts[i])) {
  //              fileExt = true;
  //              break;
  //            }
  //          }
  //          if (!fileExt) {
  //            this.presentToast('warning', 'Please select a zip file');
  //            this.removeFile();
  //          } else if (size <= this.files[0].size) {
  //            this.presentToast('warning', 'File size should be less than 100 MB');
  //            this.removeFile();
  //          } else {
  //            try {
  //              console.log('zip', this.files[0]);
  //              this.formDataActivity.assetRef = this.files[0].name;
  //              this.assetrefLink = '';
  //              this.fileName = this.files[0].name;
  //              this.selectFileTitle = this.files[0].name;
  //              this.activityFileData = this.files[0];
  //              this.enableUpload = true;
  //              this.cdf.detectChanges();
  //              console.log('this.fileName', this.fileName);
  //              console.log('this.activityFileData', this.activityFileData);
  //              //this.pdfUpload = true;
  //              // setTimeout( () => {
  //              //   let $source: any = $('#iframe');
  //              //   $source[0].src = URL.createObjectURL(this.files[0]);
  //              //   $source.parent()[0].load();
  //              // }, 200)
  //            } catch (e) {
  //              console.log(e);
  //            }
  //          }
  //        });
  //      }
  //    }, 500);
  //  }

  passFormatType() {
    // if(this.formDataActivity.formatId === 1){
    //   this.allowedTypes = [];
    // }
    switch (Number(this.formDataActivity.formatId)) {
      case 1:
        this.allowedTypes = new Array('video');
        break;
      case 2:
        this.allowedTypes = new Array('audio');
        break;
      case 3:
        this.allowedTypes = new Array('pdf');
        break;
      // case 4: this.allowedTypes =  new Array('k-point');
      //         break;
      case 5:
        this.allowedTypes = new Array('zip');
        break;
      case 7:
        this.allowedTypes = new Array('image');
        break;
      // case 8: this.allowedTypes =  new Array('image');
      //         break;
      case 10:
        this.allowedTypes = new Array(
          'application/vnd.ms-powerpoint',
          'application/vnd.openxmlformats-officedocument.presentationml.presentation'
        );
        break;
      case 11:
        this.allowedTypes = new Array(
          'application/vnd.ms-excel',
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        );
        break;
      case 12:
          this.allowedTypes =  new Array('application/msword',
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document')
          break;

      // this.allowedTypes =
      // new Array(
      //   'application/vnd.ms-powerpoint',
      //   'application/vnd.openxmlformats-officedocument.presentationml.presentation'
      // );
    }
  }

  roleSelectedList = [];
  selectedRole = [];
  disableIfLearner: any = false;
  roleTypeSelected(currentIndex, currentItem) {
    console.log('currentItem ', currentItem);
    console.log('this.pointFormat', this.pointFormat);
    if (currentItem.roleId == '8') {
      this.formDataActivity.craditpoints[currentIndex].pformatid = 1;
      this.disableIfLearner = true;
    } else {
      this.disableIfLearner = false;
    }

    for (let i = 0; i < this.roles.length; i++) {
      var role = this.roles[i];
      if (role.id == Number(currentItem.roleId)) {
        this.roleSelectedList[currentIndex] = role;
        if (this.selectedRole.length > 0) {
          this.selectedRole[currentIndex] = role.name;
        } else {
          this.selectedRole.push(role.name);
        }
      }
    }

    // this.addPointsDisableEnable();
    this.disableSelectedRole();
  }
  copytext(data) {
    console.log('Assets url:>', data);
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = data;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.toastr.success('Link Copied', 'Success');
  }
  generateAcitivtyUrl() {
    this.generateAcitivitylink.emit();
  }
  removeSelectedContent() {
    this.removeContentEvent.emit();
  }
  convertFileUrlEvent(event) {
    console.log('File change event ==>', event);
  }

  toggleCheckbox(event) {
    if (event.target.id === 'byTrainer') {
      this.formDataActivity.byTrainer = true;
      this.formDataActivity.byLearner = false;
      this.checkedAnyOneCheckBox = true;
    } else {
      this.formDataActivity.byLearner = true;
      this.formDataActivity.byTrainer = false;
      this.checkedAnyOneCheckBox = false;
    }
  }

  displayOutput(event) {
    console.log('Output files', event);
    this.files = event;
  }

  makeFileComponentDataReady(){

    const file = {
            displaySize:  null,
            fileType: 'audio',
            indexOfValidIndex: 0,
            invalidMessage: '',
            lastModified: null,
            name:  this.formDataActivity.reference.substring(
                this.formDataActivity.reference.lastIndexOf('/') + 1
              ),
            previewItem: false,
            progress: 100,
            size: null,
            type: '',
            valid: true,
            preViewUrl: this.formDataActivity.reference,
            validSize: true,
            alreadyUploadedFlag: true,
    };
    switch (Number(this.formDataActivity.formatId)) {
      case 1: file['fileType'] = 'video';
              file['fileIcon'] = 'video';
        this.allowedTypes = new Array('video');
        break;
      case 2: file['fileType'] = 'audio';
              file['fileIcon'] = 'audio';
        this.allowedTypes = new Array('audio');
        break;
      case 3: file['fileType'] = 'pdf';
      file['fileIcon'] = 'pdf';
        this.allowedTypes = new Array('pdf');
        break;
      // case 4: this.allowedTypes =  new Array('k-point');
      //         break;
      case 5: file['fileType'] = 'zip';
      file['fileIcon'] = 'scrom';
        this.allowedTypes = new Array('zip');
        break;
      case 7: file['fileType'] = 'image';
              file['fileIcon'] = 'image';
        this.allowedTypes = new Array('image');
        break;
      // case 8: this.allowedTypes =  new Array('image');
      //         break;
      case 10: file['fileType'] = 'powerpoint';
      file['fileIcon'] = 'ppt';
        this.allowedTypes = new Array(
          'application/vnd.ms-powerpoint',
          'application/vnd.openxmlformats-officedocument.presentationml.presentation'
        );
        break;
      case 11: file['fileType'] = 'excel';
      file['fileIcon'] = 'excel';
        this.allowedTypes = new Array(
          'application/vnd.ms-excel',
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        );
        break;
      case 12: file['fileType'] = 'word';
      file['fileIcon'] = 'word';
          this.allowedTypes =  new Array('application/msword',
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document')
          break;

      // this.allowedTypes =
      // new Array(
      //   'application/vnd.ms-powerpoint',
      //   'application/vnd.openxmlformats-officedocument.presentationml.presentation'
      // );
    }
    this.files = [];
    this.files.push(file);
  }

  removeSelectedContentMultiple(index, item, formData){
    console.log('Removed content Index ==>', index);
    console.log('Removed content Item ==>', item);
    // this.selectedMultiLanguageContent.splice(index, 1);
    console.log('content Item  List==>', this.selectedMultiLanguageContent);
    const event = {
      'removedItem': item,
      'selectedMultiLanguageContent': this.selectedMultiLanguageContent,
      'index': index,
      'formData': formData,
    };
    this.removeMultiContentEvent.emit(event);
  }

  bindActivityIcon(formatId) {

    switch (Number(formatId)) {
      case 1: // video
        return "video";
      case 2: // Audio
        return "audio";
      case 3: // PDF
        return "pdf";
      case 4: // K-point
        return "kpoint";
      case 5: // Scrom
        return "scrom";
      case 6: // Youtube
        return "youtube";
      case 7: // Image
        return "image";
      case 8: // External link
        return "url";
      case 9: // Practice file
        return "practice_file";
      case 10: // PPT
        return "ppt";
      case 11: // Excel
        return "excel";
      case 12: // Word
        return "word";
      default:
        return "";
    }
  }
}
