import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxEchartsModule } from 'ngx-echarts';
import { ThemeModule } from '../../@theme/theme.module';
import { PlanComponent } from './plan.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { UsersService } from './users/users.service';
import { EmployeesService } from './employees/employees.service';
import { CourseBundleService } from './courseBundle/courseBundle.service';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
// import { AddEditActivityComponent } from './add-edit-activity/add-edit-activity.component';
// import { CourseBundle } from './courseBundle/courseBundle';
import { ContentService } from './courses/content/content.service';
import { CommonModule } from '@angular/common';
import { SortablejsModule } from 'angular-sortablejs';
// import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
// import { CourseBundleModule } from './courseBundle/courseBundle.module';
import { CoursesModule } from './courses/courses.module';
// import { EmployeesModule } from './employees/employees.module';
// import { UsersModule } from './users/users.module';
// import { HttpConfigInterceptor } from '../../interceptor/interceptor';
import { NotificationTemplatesComponent } from './notification-templates/notification-templates.component';
import { NotificationtemplateServiceService } from './notification-templates/notification-templates.service';
import { AddEditNotificationTemplateComponent } from './notification-templates/add-edit-notification-template/add-edit-notification-template.component';
import { AddEditNotificationtemplateServiceService } from './notification-templates/add-edit-notification-template/add-edit-notification-template.service';
import { QuillModule } from 'ngx-quill';
import 'quill/dist/quill.core.css';
import 'quill/dist/quill.snow.css';
import { TrainerComponent } from './trainer/trainer.component';
import { AddTrainerComponent } from './trainer/add-trainer/add-trainer.component';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { FilterPipeModule  } from 'ngx-filter-pipe';
import { UnEnrolEmployessComponent } from './un-enrol-employess/un-enrol-employess.component';
import { UnEnrolEmployessService } from './un-enrol-employess/un-enrol-employess.service';
import { JsonToXlsxService } from './un-enrol-employess/json-to-xlsx.service';
import { JoditAngularModule } from 'jodit-angular';
import { LocationComponent } from './location/location.component';
import { VenueComponent } from './venue/venue.component';
import { DemoMaterialModule } from '../material-module';
import { AddVenueComponent } from './venue/add-venue/add-venue.component';
import { PartnerListComponent } from './partner-list/partner-list.component';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgxContentLoadingModule } from 'ngx-content-loading';
import { TagsComponent } from './tags/tags.component';
import { OpenApiComponent } from './open-api/open-api.component';
import {MatInputModule,MatOptionModule, MatSelectModule, MatIconModule} from '@angular/material';
import { InstitutesComponent } from './institutes/institutes.component';
import { GbhMappingComponent } from './gbh-mapping/gbh-mapping.component'
import { ComponentModule } from '../../component/component.module';
import { SubheaderComponent } from '../components/subheader/subheader.component';
import {SidebarFormComponent  } from '../../component/sidebar-form/sidebar-form.component';

// import { AddEditModulesComponent } from './add-edit-modules/add-edit-modules.component';
// import { ModuleActivityListComponent } from './module-activity-list/module-activity-list.component';

//import { AddapiComponent } from './open-api/addapi/addapi.component';
// import { DragChipsComponentComponent } from '../../component/drag-chips-component/drag-chips-component.component';

@NgModule({
  imports: [
    FilterPipeModule,
    ThemeModule,
    NgxEchartsModule,
    CommonModule,
    SortablejsModule,
    ReactiveFormsModule,
    AngularMultiSelectModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    DemoMaterialModule,
    MatInputModule,MatOptionModule, MatSelectModule, MatIconModule,
    ComponentModule,
    // CourseBundleModule,
    CoursesModule,
    QuillModule,
    NgxDatatableModule,
    JoditAngularModule,
    NgxSkeletonLoaderModule,
    NgxContentLoadingModule,
    ModalModule.forRoot(),
    ComponentModule.forRoot()
    // EmployeesModule,
    // UsersModule
  ],
  declarations: [
    PlanComponent,
    NotificationTemplatesComponent,
    AddEditNotificationTemplateComponent,
    TrainerComponent,
    AddTrainerComponent,
    UnEnrolEmployessComponent,
    LocationComponent,
    VenueComponent,
    AddVenueComponent,
    PartnerListComponent,
    TagsComponent,
    OpenApiComponent,
    InstitutesComponent,
    GbhMappingComponent,
    // AddEditModulesComponent,
    // ModuleActivityListComponent,
    //ComponentModule.forRoot()
    // AddEditActivityComponent,
    //AddapiComponent,
    // DragChipsComponentComponent,
    // CourseBundle
  ],
  providers: [

    Location, {provide: LocationStrategy, useClass: PathLocationStrategy},

    // { provide : HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi:true},
    UsersService,
    CourseBundleService,
    ContentService,
    ContentService,
    NotificationtemplateServiceService,
    AddEditNotificationtemplateServiceService,
    UnEnrolEmployessService,
    JsonToXlsxService,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA ,
  ]
})
export class PlanModule { }
