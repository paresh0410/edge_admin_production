import { Injectable } from '@angular/core';
import { webApi } from '../../../service/webApi';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class GbhMappingService {
  private get_gbhmapping_list: string = webApi.domain + webApi.url.fetch_gbhmapping_list;
  private get_dropDown = webApi.domain + webApi.url.get_all_gbhmapping_dropdown;
  private add_edit_gbhmapping = webApi.domain + webApi.url.add_edit_gbhmapping;
  private update_visibility = webApi.domain + webApi.url.enable_disable_gbhmapping;
  private get_emp_search_data = webApi.domain + webApi.url.get_emp_search_string;

  constructor(private _http: HttpClient) { }
  getallGBHList() {

    return new Promise(resolve => {
      this._http.post(this.get_gbhmapping_list, {})
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }


  getDropdown() {
    return new Promise(resolve => {
      this._http.post(this.get_dropDown, {})
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  getalluser(data){

    return new Promise(resolve => {
      this._http.post(this.get_emp_search_data, data)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  addedit_GBHMap(data) {

    return new Promise(resolve => {
      this._http.post(this.add_edit_gbhmapping, data)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  enableDisabledGBH(data) {

    return new Promise(resolve => {
      this._http.post(this.update_visibility, data)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

}
