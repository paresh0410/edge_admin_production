import { TestBed } from '@angular/core/testing';

import { GbhMappingService } from './gbh-mapping.service';

describe('GbhMappingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GbhMappingService = TestBed.get(GbhMappingService);
    expect(service).toBeTruthy();
  });
});
