import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { GbhMappingService } from './gbh-mapping.service';
import { PassService } from '../../../service/passService';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { webApi } from '../../../service/webApi';
import { SuubHeader } from '../../components/models/subheader.model';
import { BrandDetailsService } from '../../../service/brand-details.service';
import { noData } from '../../../models/no-data.model';
@Component({
  selector: 'ngx-gbh-mapping',
  templateUrl: './gbh-mapping.component.html',
  styleUrls: ['./gbh-mapping.component.scss']
})
export class GbhMappingComponent implements OnInit {
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No Group Business Heads available at this time.",
    desc:"Admin will be able to add a Group Business Head in the application. Group Business Heads are mapped to employees and are responsible for course approvals .",
    titleShow:true,
    btnShow:true,
    descShow:true,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/add-gbh',
  }
  header: SuubHeader  = {
    title:'Group Business Head Mapping',
    btnsSearch: true,
    placeHolder: 'Search by username, name ',
    searchBar: true,
    dropdownlabel: ' ',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: '',
    btnName3: '',
    btnAdd: 'Add',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: true,
    filter: false,
    showBreadcrumb: true,
    breadCrumbList:[]   
  };
  btnName: string = 'Save';
  searchTags = '';
  list = [];
  gbhList = [];
  pageSize = 10;
  loader: any;
  pager: any = [];
  nodata: boolean = false;
  GBHData: any = [];
  title: any;
  sectionshow: boolean = false;
  GBH: any = [];
  drowDownValue: any = [];
  drowDownVisibleData: any = [];
  subDepartmentDropDown:any = [];
  isPending: boolean = true;
  helpContent: any;
  settings = {
    text: 'Select Sub Department',
    singleSelection: true,
    classes: 'common-multi',
    primaryKey: 'typeid',
    labelKey: 'name',
    noDataLabel: 'Search Sub Department...',
    enableSearchFilter: true,
    searchBy: ['typename', 'name'],
    lazyLoading: true,
    maxHeight: 200,
    showCheckbox: false,
 };
 settings1 = {
  text: 'Select Username',
  singleSelection: true,
  classes: 'common-multi',
  primaryKey: 'id',
  labelKey: 'username',
  noDataLabel: 'Search Username...',
  enableSearchFilter: true,
  searchBy: ['username', 'fullname'],
  lazyLoading: true,
  maxHeight: 200,
  showCheckbox: false,
};
labels: any = [
  // { labelname: 'ID', bindingProperty: 'sr_no', componentType: 'text' },
  { labelname: 'FullName', bindingProperty: 'fullname', componentType: 'text' },
  { labelname: 'Sub-Department', bindingProperty: 'sub Department', componentType: 'text' },
  { labelname: 'ACTION', bindingProperty: 'btntext', componentType: 'button' },
  { labelname: 'EDIT', bindingProperty: 'tenantId', componentType: 'icon' },
]
 userList: any = [];
 selectedEmployee: any = [];
 subDepartmentSelected: any = [];
  currentBrandData: any;
  constructor(public IntiService: GbhMappingService, public http: HttpClient,private commonFunctionService: CommonFunctionsService,
    public brandService: BrandDetailsService, private cdf: ChangeDetectorRef,
    public toastr: ToastrService, private pagservice: PassService, private spinner: NgxSpinnerService ) {
    this.searchTags = '';

     }

  ngOnInit() {
    
    this.header.breadCrumbList=[{
      'name': 'Settings',
      'navigationPath': '/pages/plan',
      }]
    this.getHelpContent();
    this.getGBHList();
    this.currentBrandData = this.brandService.getCurrentBrandData();
  }
  clear() {
    if(this.searchTags.length>=3){
    this.nodata = false
    this.searchTags = '';
    this.getGBHList();
    this.nodata=false
    // this.onsearchBar(event);
  }else{
    this.searchTags='';
  }
}


  getGBHList() {
    // this.loader = true;
    this.spinner.show()
    const get_gbhmapping_list: string = webApi.domain + webApi.url.fetch_gbhmapping_list;
    this.commonFunctionService.httpPostRequest(get_gbhmapping_list,{})
    //this.IntiService.getallGBHList()
    .then(res => {
      // this.loader=false;
      this.spinner.hide();
      console.log(res);
      if (res['type'] == true) {
        this.list = res['data'];
        this.gbhList = this.list;
        if(!this.gbhList.length){
          this.nodata = true
        }else{
          this.nodata = false
        // this.setpage(1);
        for (let index = 0; index < this.gbhList.length; index++) {
          if(this.gbhList[index].visible == 1) {
            this.gbhList[index].btntext = 'fa fa-eye';
          } else {
            this.gbhList[index].btntext = 'fa fa-eye-slash';
          }
          
        }
        
        }
      }else {
        this.nodata = true;
      }
      // this.loader = false;
      this.spinner.hide();
    }, err => {
      this.nodata = true;
      // this.loader = false;
      this.spinner.hide();
      this.presentToast('error', '');
      console.log(err);
    })
  }

  // setpage(page) {
  //   // this.nodata = false;
  //   this.pager = this.pagservice.getPager(this.gbhList.length, page, this.pageSize);
  //   console.log(this.pager);
  //   this.displaydata();
  // }
  displaydata() {
    this.GBH = this.gbhList.slice(this.pager.startIndex, this.pager.endIndex + 1);
    for (let index = 0; index < this.GBH.length; index++) {
      this.GBH[index] = { id: index + 1, ...this.GBH[index] };
      // this.displayTableData[index]['Download'] = '';
      this.GBH[index]['sr_no'] = index + 1;
      this.GBH[index]['Edit'] = '';
    }
    if (!this.GBH.length) {
      this.nodata = true;
    } else {
      this.nodata = false;
    }
    console.log(this.GBH);
    for (let index = 0; index < this.GBH.length; index++) {
      if(this.GBH[index].visible == 1) {
        this.GBH[index].btntext = 'fa fa-eye';
      } else {
        this.GBH[index].btntext = 'fa fa-eye-slash';
      }
    }
  }
  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }
  
  AddGBH(data, status) {
    this.GBHData = [];
    this.getDropDownData();
    if (status == 0) {
      this.GBHData = {
        id: 0,
        visible: '1',
        subDeptId: '',
        GBHId: '',
        username: '',
        fullname: '',
        // instituteName: data.instituteName,
      }
      this.title = "Add GBH";
    } else {
      this.GBHData = {
        id: data.id,
        // subDepartment: [],
        // instituteName: data.instituteName,
        visible: data.visible,
        subDeptId: data.subDeptId,
        GBHId: data.GBHId,
        username: data.username,
        fullname: data.fullname,
        // employee: [],
      }
      this.title = "Edit GBH";
    }
  }
  getDropDownData() {
    const get_dropDown = webApi.domain + webApi.url.get_all_gbhmapping_dropdown;
    //this.commonFunctionService.httpPostRequest(get_dropDown,{})
    this.IntiService.getDropdown()
    .then(
      (data) => {
        this.drowDownValue = data,
        this.drowDownVisibleData = this.drowDownValue.visiblity,
        this.subDepartmentDropDown =  this.drowDownValue.subDepartment,
        this.isPending = false
        this.prepareDataToBind();
        this.sectionshow = true;
      },
      (error: any) => console.error(error))
  }
  prepareDataToBind(){
    for (let index = 0; index < this.subDepartmentDropDown.length; index++) {
        if(this.subDepartmentDropDown[index].id == this.GBHData.subDeptId){
          this.subDepartmentSelected.push(this.subDepartmentDropDown[index]);
        }
    }
    let data = {
      id: this.GBHData.GBHId,
      username: this.GBHData.username,
      fullname: this.GBHData.fullname,
    };
    this.userList.push(data);
    this.selectedEmployee.push(data);
    // for (let index = 0; index < this.subDepartmentDropDown.length; index++) {
    //   if(this.subDepartmentDropDown[index].id == this.GBHData.subDeptId){
    //     this.subDepartmentSelected.push(this.subDepartmentDropDown[index]);
    //   }
  // }
  this.spinner.hide();
  // this.cdf.detectChanges();
  }

  back() {
    window.history.back();
  }
  closesectionModel() {
    this.GBHData = [];
    this.subDepartmentSelected = [];
    this.selectedEmployee = [];
    this.sectionshow = false;
  }
  saveMapping(f) {

    if(f.valid){

      this.spinner.show();
      const param = {
        id: this.GBHData.id,
        subDeptId: this.subDepartmentSelected[0].id,
        empId: this.selectedEmployee[0].id,
        Gvisible: this.GBHData.visible,
      }
      const add_edit_gbhmapping = webApi.domain + webApi.url.add_edit_gbhmapping;
      this.commonFunctionService.httpPostRequest(add_edit_gbhmapping,param)
      //this.IntiService.addedit_GBHMap(param)
      .then(res => {
        console.log(res);
        if (res['type'] == true) {
          var response = res['data'][0];
          if (response.status == 0) {
            this.presentToast('warning', response.msg);
          } else {
            this.presentToast('success', response.msg);
            this.getGBHList();
            this.closesectionModel();
          }
        }
        // else {
        //   this.presentToast('error', '')
        // }
        this.spinner.hide();
      }, err => {
        this.spinner.hide();
        this.presentToast('error', '')
      })
    } else {
      Object.keys(f.controls).forEach(key => {
        f.controls[key].markAsDirty();
      });
    }
  }
  visibilityTableRow(row) {
    let value;
    let status;

    if (row.visible == 1) {
      row.btntext = 'fa fa-eye-slash';
      value  = 'fa fa-eye-slash';
      row.visible = 0
      status = 0;
    } else {
      status = 1;
      value  = 'fa fa-eye';
      row.visible = 1;
      row.btntext = 'fa fa-eye';
    }
    const param = {
      id: row.id,
      visible: status
    }
    // this.spinner.show();
    const update_visibility = webApi.domain + webApi.url.enable_disable_gbhmapping;
    this.commonFunctionService.httpPostRequest(update_visibility,param)
   // this.IntiService.enableDisabledGBH(param)
    .then(res => {
      console.log(res);
      if (res['type'] == true) {
        this.presentToast('success', res['data']);
        row.visible = status;
      }
      this.spinner.hide();
    }, err => {
      console.log(err);
      this.spinner.hide();
      this.presentToast('error', '');
    });

    console.log('row', row);
  }
  disableVisibility(index, data, status) {
    console.log(data);
    const param = {
      id: data.id,
      visible: status
    }
    this.spinner.show();
    const update_visibility = webApi.domain + webApi.url.enable_disable_gbhmapping;
    this.commonFunctionService.httpPostRequest(update_visibility,param)
   // this.IntiService.enableDisabledGBH(param)
    .then(res => {
      console.log(res);
      if (res['type'] == true) {
        this.presentToast('success', res['data']);
        data.visible = status;
      }
      this.spinner.hide();
    }, err => {
      console.log(err);
      this.spinner.hide();
      this.presentToast('error', '');
    });
  }
  getHelpContent() {
    return new Promise(resolve => {

      this.http.get('../../../../../../assets/help-content/addEditCourseContent.json').subscribe(
        data => {
          this.helpContent = data;
          console.log('Help Array', this.helpContent);
        },
        err => {
          resolve('err');
        },
      );
    });
    // return this.helpContent;
  }
  onCreatorSearch(evt: any) {
    this.userList =[];
    console.log(evt.target.value);
    // this.allUNEnrolUser(evt.target.value);
    const val = evt.target.value;
    if (val.length >= 3) {
      this.fetchAllUnEnrolUsersAsync(val, (Data) => {
        if (Data && Data.type === true) {
          if(Data['data'].length != 0){
            this.userList =  [... Data['data']];
          }
          // this.enrolldata = Data['data'];
          // this.tempUsers = Data['data'];
          // this.tempUsers = [...this.tempUsers];
          // console.log('EnrolledUSer ', this.tempUsers);

          // const temp = this.tempUsers.filter(function (d) {
          //   return String(d.ecn).toLowerCase().indexOf(val) !== -1 ||
          //     d.fullname.toLowerCase().indexOf(val) !== -1 || !val;
          // });

          // // update the rows
          // this.usersList = temp;
          // this.cdf.detectChanges();
        } else {
          // console.log('Error getting EnrolledUSer ', Data);
          // this.tempUsers = [];
          // this.tempUsers = [...this.tempUsers];
          // this.usersList = [];
          // this.cdf.detectChanges();
        }
      });
    }
    // else if (val.length === 0) {
    //   this.tempUsers = [];
    //   this.tempUsers = [...this.tempUsers];
    //   this.usersList = [];
    //   // this.cdf.detectChanges();
    // }
    else {
      // this.subscription.unsubscribe();
    }


  }
  fetchAllUnEnrolUsersAsync(params: any, cb) {
    const data = {
      srchStr: params,
    };
    const get_emp_search_data = webApi.domain + webApi.url.get_emp_search_string;
    this.commonFunctionService.httpPostRequest(get_emp_search_data,data)
    //this.IntiService.getalluser(data)
    .then(res => {
      cb(res);
    }, err => {
      console.log(err);
    });
  }
  // onsearchBar(event) {
  //   this.GBH.fullname = event.target.value
  //   // console.log(evt);
  //   const val =  this.GBH.fullname;

  // //   const temp = this.list.filter(function (d) {
  // //     return String(d.fullname).toLowerCase().indexOf(val) !== -1 ||
  // //       !val
  // //     // d.description.toLowerCase().indexOf(val) !== -1 ||
  // //   })
  // //   console.log("this.data",temp);
  // //   this.gbhList = temp;
  // //   this.setpage(1);
  // }
  onsearchBar(event) {
    var temData = this.list;
    var val = event.target.value.toLowerCase();
    var keys = [];
    this.searchTags=val;
         // filter our data
    if (temData.length > 0) {
      keys = Object.keys(temData[0]);
    }
    if(val.length>=3 || val.length==0){
    var temp = temData.filter((d) => {
      for (const key of keys) {
        if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
          return true;
        }
      }
    });
    this.gbhList = temp;
    if(temp.length == 0){
      this.nodata = true
      this.noDataVal={
        margin:'mt-5',
        imageSrc: '../../../../../assets/images/no-data-bg.svg',
        title:"Sorry we couldn't find any matches please try again",
        desc:".",
        titleShow:true,
        btnShow:false,
        descShow:false,
        btnText:'Learn More',
        btnLink:''
      }
    }
    else{
      this.nodata = false
    }
  }
    // this.setpage(1);
}
}
