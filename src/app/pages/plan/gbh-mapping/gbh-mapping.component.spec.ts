import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GbhMappingComponent } from './gbh-mapping.component';

describe('GbhMappingComponent', () => {
  let component: GbhMappingComponent;
  let fixture: ComponentFixture<GbhMappingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GbhMappingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GbhMappingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
