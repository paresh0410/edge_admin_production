import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { TagsService } from './tags.service';
import { HttpClient } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';
import { PassService } from '../../../service/passService';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { webApi } from '../../../service/webApi';
import { SuubHeader } from '../../components/models/subheader.model';
import { noData } from '../../../models/no-data.model';

@Component({
  selector: 'ngx-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.scss']
})
export class TagsComponent implements OnInit {
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:'No Tags at this time.',
    desc:'Admin will be able to add tags in the application. Trainers or Instructors will be able to map multiple tags to a Course or a digital asset.',
    titleShow:true,
    btnShow:true,
    descShow:true,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/add-tags',
  }
  header: SuubHeader  = {
    title:'Tags',
    btnsSearch: true,
    placeHolder:'Search by Tags name',
    searchBar: true,
    dropdownlabel: ' ',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: '',
    btnName3: '',
    btnAdd: 'Add',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: true,
    filter: false,
    showBreadcrumb: true,
    breadCrumbList:[]   
  };
  btnName: string = 'Save';
  searchTags: any;
  taglist: any = [];
  editData: any = [];
  drowDownValue: any = [];
  title: string = '';
  sectionshow: boolean = false;
  isPending: boolean = true;
  drowDownVisibleData: any = [];
  helpContent: any = [];
  searchstr: any;
  // TagsForm = new FormGroup({
  //   'name': new FormControl('', [
  //     Validators.required,
  //     Validators.minLength(3),
  //     Validators.maxLength(100),
  //   ]),
  // });
  // subtagform = new FormGroup({
  //   'subname': new FormControl('', [
  //     Validators.required,
  //     Validators.minLength(3),
  //     Validators.maxLength(100),
  //   ]),
  // });
  // subtaglist: any = [];
  pager: any = [];
  pageSize = 10;
  tagdata: any = [];
  tagsdata: any = [];
  nodata: boolean = false;
  alltags: any = [];
  labels: any = [
    // { labelname: 'ID', bindingProperty: 'id', componentType: 'text' },
		{ labelname: 'NAME', bindingProperty: 'dispName', componentType: 'text' },
    { labelname: 'SUBTAGS', bindingProperty: 'subtags', componentType: 'text' },
		{ labelname: 'ACTION', bindingProperty: 'btntext', componentType: 'button' },
    { labelname: 'EDIT', bindingProperty: 'tenantId', componentType: 'icon' },
  ]
  updateFlag: boolean;
  msg: boolean=false;
  constructor(private toastr: ToastrService, private tagsService: TagsService, private spinner: NgxSpinnerService,
    private http1: HttpClient, private pagservice: PassService, private commonFunctionService: CommonFunctionsService,) {
    // this.searchTags = {};
  }

  ngOnInit() {
    this.spinner.show();
    this.getHelpContent();
    this.getalltags();
    this.header.breadCrumbList=[{
      'name': 'Settings',
      'navigationPath': '/pages/plan',
      }]
  }
 
  back() {
    window.history.back();
  }

  getalltags() {
    // this.spinner.show();
    this.nodata=false;
    const get_taglist = webApi.domain + webApi.url.gettagslist;
    this.commonFunctionService.httpPostRequest(get_taglist,{})
    //this.tagsService.gettagslist()
    .then(res => {
      this.nodata=false;
      console.log(res);
      if (res['type'] == true) {
        this.alltags = res['data'];
      this.spinner.hide();
        this.taglist = this.alltags;
        for (let i = 0; i < this.taglist.length; i++) {
          if(this.taglist[i].isActive == 1) {
            this.taglist[i].btntext = 'fa fa-eye';
          } else {
            this.taglist[i].btntext = 'fa fa-eye-slash';
          }
          if (this.taglist[i].SubDataArr.length > 0) {
            let jdata = this.taglist[i].SubDataArr;
            for (let j = 0; j < jdata.length; j++) {
              if (j == 0) {
                this.taglist[i].subtags = jdata[j].subName;
              } else {
                this.taglist[i].subtags += ',' + jdata[j].subName;
              }
            }
          } else {
            this.taglist[i].subtags = "NA";
          }
        }
      
        console.log(this.taglist);
        this.setpage(1);
        if(this.updateFlag){
        // this.presentToast('success', this.msg);
        this.closesectionModel();
        }
      }
    }, err => {
      this.spinner.hide();
      this.nodata=true;
      this.presentToast('error', '');
    });
  }
  showTemplate(data, value) {
    this.tagdata = [];
    this.getDropDownData();
    console.log(data, value);
    // this.editData = data ? data : [];
    if (value === 0) {
      this.title = 'Add Tags';
      this.tagdata = {
        id: 0,
        name: '',
        visible: '1',
        SubtagsData: [],
      };
    }
    else {
      this.title = 'Edit Tags';
      console.log(data);
      this.tagdata = {
        id: data.tagId,
        name: data.dispName,
        visible: data.isActive,
        SubtagsData: data.SubDataArr,
      };
    }
    this.sectionshow = true;
  }
  openEdit(data){
    this.tagdata = [];
    this.getDropDownData();
    console.log(data);
    this.title = 'Edit Tags';
      console.log(data);
      this.tagdata = {
        id: data.tagId,
        name: data.dispName,
        visible: data.isActive,
        SubtagsData: data.SubDataArr,
      };
  }
  setpage(page) {
    // this.nodata = false;
    this.pager = this.pagservice.getPager(this.taglist.length, page, this.pageSize);
    console.log(this.pager);
    this.displaydata();
  }
  displaydata() {
    this.tagsdata = this.taglist.slice(this.pager.startIndex, this.pager.endIndex + 1);
    for (let index = 0; index < this.tagsdata.length; index++) {
      this.tagsdata[index] = { id: index + 1, ...this.tagsdata[index] };
      // this.displayTableData[index]['Download'] = '';
      this.tagsdata[index]['Edit'] = '';
    }
    if (!this.tagsdata.length) {
      this.nodata = true;
    }else {
      this.nodata = false;
    }
    console.log(this.tagsdata);
  }
  onsearch(evt: any, text) {
    console.log(evt.target.value,"evt");
    //const val = text;
    const val =  evt.target.value;
    this.searchTags=val;
    if(val.length>=3 || val.length ==0){
    const temp = this.alltags.filter(function (d) {
      return String(d.dispName).toLowerCase().indexOf(val) !== -1 ||
        !val
    })
    if(temp.length==0){
      this.nodata=true
      this.noDataVal={
        margin:'mt-5',
        imageSrc: '../../../../../assets/images/no-data-bg.svg',
        title:"Sorry we couldn't find any matches please try again",
        desc:".",
        titleShow:true,
        btnShow:false,
        descShow:false,
        btnText:'Learn More',
        btnLink:''
      }
    }
    console.log(temp);
    this.taglist = temp;
    this.setpage(1);
  }
  }
  clear() {
    if(this.searchTags.length>=3){
    this.searchTags = '';
    this.spinner.show()
    this.getalltags();
    this.nodata=false
    }
    else{
      this.searchTags='';
    }
      // this.onsearch(event, this.searchTags);

  }
  getDropDownData() {
   const  get_dropDown = webApi.domain + webApi.url.getDropDown;
   this.commonFunctionService.httpPostRequest(get_dropDown,{})
    //this.tagsService.getDropdown()
    .then(
      (data) => {
        this.drowDownValue = data
        this.drowDownVisibleData = this.drowDownValue.data.visibleDropData
        this.isPending = false
      },
      (error: any) => console.error(error))
  }

  closesectionModel() {
    this.sectionshow = false;
    this.tagdata = {
      name: '',
      visible: '',
      SubtagsData: []
    };
    this.tagdata.SubtagsData = {}
    // this.getalltags();
    // this.TagsForm.reset()
  }
  getHelpContent() {
    return new Promise(resolve => {
      this.http1
        .get('../../../../../../assets/help-content/addEditCourseContent.json')
        .subscribe(
          data => {
            this.helpContent = data;
            console.log('Help Array', this.helpContent);
          },
          err => {
            resolve('err');
          }
        );
    });
  }
  addsubtag() {
    let obj = {
      subId: '0',
      subName: '',
      subStatus: '1'
    }
    this.tagdata.SubtagsData.push(obj);
  }
  visibilityTableRow(row) {
    let value;
    let status;

    if (row.isActive == 1) {
      row.btntext = 'fa fa-eye-slash';
      value  = 'fa fa-eye-slash';
      row.isActive = 0
      status = 0;
    } else {
      status = 1;
      value  = 'fa fa-eye';
      row.isActive = 1;
      row.btntext = 'fa fa-eye';
    }
    console.log(row);
    const param = {
      tagId: row.tagId,
      visible:row.isActive,
    }
    // this.spinner.show();
    const enableTag = webApi.domain + webApi.url.enableTag;
    this.commonFunctionService.httpPostRequest(enableTag,param)
    //this.tagsService.enabledisable(param)
    .then(res => {
      console.log(res);
      if (res['type'] == true) {
        this.presentToast('success', res['data']);
        row.isActive
      }
      this.spinner.hide();
    }, err => {
      console.log(err);
      this.spinner.hide();
      this.presentToast('error', '');
    });
  }
  // disableVisibility(index, data, status) {
  //   console.log(data);
  //   const param = {
  //     tagId: data.tagId,
  //     visible: status
  //   }
  //   this.spinner.show();
  //   const enableTag = webApi.domain + webApi.url.enableTag;
  //   this.commonFunctionService.httpPostRequest(enableTag,param)
  //   //this.tagsService.enabledisable(param)
  //   .then(res => {
  //     console.log(res);
  //     if (res['type'] == true) {
  //       this.presentToast('success', res['data']);
  //       data.isActive = status;
  //     }
  //     this.spinner.hide();
  //   }, err => {
  //     console.log(err);
  //     this.spinner.hide();
  //     this.presentToast('error', '');
  //   });
  // }

  disableVisibilityofsubtag(index, data, status) {
    console.log(data);
    // this.valueVisible = status
    // if (this.isPending == false) {
    //   this.addeditdata = {
    //     locationId: data.locationId,
    //     visible: this.valueVisible,
    //     locationName: data.locationName,
    //   };
    //   if (status === 1) {
    //     this.msg = 'Enabled' + ' ' + 'Location';
    //   } else {
    //     this.msg = 'Disabled' + ' ' + 'Location';
    //     this.sectionshow = false;
    //   }
    //   this.result();
    //   this.presentToast('success', this.msg);
    // }
    // else {
    //   this.toastr.warning('Data is loading')
    // }
  }

  removesubmenu(data, index) {
    console.log(data, index);
    this.tagdata.SubtagsData.splice(index, 1);
    console.log(this.tagdata.SubtagsData);
  }

  saveTemp(form) {

    if(form && form.valid){
      this.spinner.show();
      console.log(this.tagdata);
      if (this.tagdata.SubtagsData.length > 0) {
        let sdata = this.tagdata.SubtagsData;
  
        for (let i = 0; i < sdata.length; i++) {
          let fdata = sdata[i].subId + '@sub@' + sdata[i].subName + '@sub@' + sdata[i].subStatus;
  
          if (i == 0) {
            this.tagdata.allstr = fdata;
          } else {
            this.tagdata.allstr += '@main@' + fdata;
          }
        }
      }
      console.log(this.tagdata);
      this.update(this.tagdata);
    }else {
      Object.keys(form.controls).forEach(key => {
        form.controls[key].markAsDirty();
      });
    }
   
  }
  update(data) {
    const param = {
      tagId: data.id,
      tagName: data.name,
      visible: data.visible,
      subTagstr: data.allstr,
    }
    const addedit = webApi.domain + webApi.url.addedittag;
    this.commonFunctionService.httpPostRequest(addedit,param)
    //this.tagsService.addedittags(param)
    .then(res => {
      console.log(res);
      if (res['type'] == true) {
        this.updateFlag=true;
        this.msg=res['data'][0]['msg']
        this.presentToast('success', res['data'][0]['msg']);
        // this.closesectionModel();
        this.getalltags();
      }
      // this.spinner.hide();
    }, err => {
      // this.spinner.hide();
      console.log(err);
      this.presentToast('error', '');
    });
  }
  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }
}
