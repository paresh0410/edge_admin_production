import { Injectable } from '@angular/core';
import { webApi } from '../../../service/webApi';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})

export class TagsService {

  private get_dropDown = webApi.domain + webApi.url.getDropDown;
  private get_taglist = webApi.domain + webApi.url.gettagslist;
  private addedit = webApi.domain + webApi.url.addedittag;
  private enableTag = webApi.domain + webApi.url.enableTag;

  constructor(private _http: HttpClient) { }

  gettagslist() {
    return new Promise(resolve => {
      this._http.post(this.get_taglist, {})
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  getDropdown() {
    return new Promise(resolve => {
      this._http.post(this.get_dropDown, {})
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  enabledisable(param) {
    return new Promise(resolve => {
      this._http.post(this.enableTag, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  addedittags(param) {
    return new Promise(resolve => {
      this._http.post(this.addedit, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
}
