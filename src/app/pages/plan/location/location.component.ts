import { Component, OnInit, NgModule } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { LocationService } from './location.service';
import { HttpClient } from '@angular/common/http';
import { webApi } from '../../../service/webApi';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { SuubHeader } from '../../components/models/subheader.model';
import { PassService } from '../../../service/passService';
import { noData } from '../../../models/no-data.model';
import {SidebarFormComponent} from '../../../component/sidebar-form/sidebar-form.component';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'ngx-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss']
})
export class LocationComponent implements OnInit {
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:'No Locations at this time',
    desc:'Locations will appear after they are added by the admin. Locations can be selected in a session for a Blended course.',
    titleShow:true,
    btnShow:true,
    descShow:true,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/add-location',
  }
  header: SuubHeader  = {
    title:'Location',
    btnsSearch: true,
    placeHolder: 'Search by location name',
    searchBar: true,
    dropdownlabel: ' ',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: '',
    btnName3: '',
    btnAdd: 'Add',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: true,
    filter: false,
    showBreadcrumb: true,
    breadCrumbList:[]   
  };
  loader: any;
  noMaster: boolean;
  valueVisible
  locationJson1: any = []
  addeditdata: any = [];
  userDetails: any = [];
  param: any = [];
  editData: any = [];
  drowDownValue: any = []
  drowDownVisibleData: any = []
  selectedVenueId: any='';
  msg: any;
  isPending = true
  title: string = '';
  btnName: string = 'Save';
  visibilityDesc='helpContent?.location_AddEditCategoryContent?.visibility.labelHelpContent'
  locationName1: any
  sectionshow: boolean = false;
  searchParticipant: any;
  locationForm = new FormGroup({
    'name': new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(100)
    ])
  })
  labels: any = [
		// { labelname: 'ID', bindingProperty: 'locationId', componentType: 'text' },
		{ labelname: 'NAME', bindingProperty: 'locationName', componentType: 'text' },
		{ labelname: 'ACTION', bindingProperty: 'btntext', componentType: 'button' },
    { labelname: 'EDIT', bindingProperty: 'tenantId', componentType: 'icon' },
  ]
  pager: any;
  temData: any;
  displayTableData: any;
  pageSize: number = 10;
  searchText: any;
  constructor(private router: Router, private spinner: NgxSpinnerService, private toastr: ToastrService, private commonFunctionService: CommonFunctionsService,private locationData: LocationService,private pagservice: PassService, private http1: HttpClient) {
    this.searchParticipant = {}
    this.getHelpContent();
  }
  ngOnInit() {
    this.getAllLocation()
    this.userDetails = JSON.parse(localStorage.getItem('LoginResData'));
    this.param = {
      tId: this.userDetails.data.data.tenantId,
    };
    this.header.breadCrumbList=[{
      'name': 'Settings',
      'navigationPath': '/pages/plan',
      }]
  }
  locationJson: any = []
  getAllLocation() {
    // this.loader = true;
    this.spinner.show();
    const get_location: string = webApi.domain + webApi.url.getAllLocation;
    this.commonFunctionService.httpPostRequest(get_location,{})
    //this.locationData.getAllLocation()
    .then(
      
      (data) => {
        this.spinner.hide();
        this.locationJson=[]
        this.locationJson1 = data
        this.locationJson = this.locationJson1.data
        this.temData = this.locationJson
        // this.loader = false;
        this.isPending = false
        // this.setpage(1)
        for (let i = 0; i < this.locationJson.length; i++) {

          if(this.locationJson[i].visible == 1) {
            this.locationJson[i].btntext = 'fa fa-eye';
          } else {
            this.locationJson[i].btntext = 'fa fa-eye-slash';
          }
        }
        if(this.locationJson.length==0){
          this.noMaster=true;
        }else{
          this.noMaster=false;
        }
        console.log('locationJson',this.locationJson);
      
      },
      err => {
        this.presentToast('error', '');
        // this.loader=false
        this.spinner.hide()
        console.log(err);
        this.noMaster=true;
      });
      // (error: any) => console.error(error))
  }
  getDropDownData() {
   const  get_dropDown = webApi.domain + webApi.url.getDropDown;
   this.commonFunctionService.httpPostRequest(get_dropDown,{})
   // this.locationData.getDropdown()
    .then(
      (data) => {
        this.drowDownValue = data
        this.drowDownVisibleData = this.drowDownValue.data.visibleDropData
        this.isPending = false
      },
      (error: any) => console.error(error))
  }
  back() {
    this.router.navigate(['../pages/plan']);
  }
  clear() {
    if(this.searchParticipant.length>=3){
      this.getAllLocation()
    this.searchParticipant = {};
    }
    this.searchParticipant = {};
  }
  visibilityTableRow(row) {
    let value;
    let status;

    if (row.visible == 1) {
      row.btntext = 'fa fa-eye-slash';
      value  = 'fa fa-eye-slash';
      row.visible = 0
      status = 0;
    } else {
      status = 1;
      value  = 'fa fa-eye';
      row.visible = 1;
      row.btntext = 'fa fa-eye';
    }

    // for(let i =0; i < this.locationJson.length; i++) {
    //   if(this.locationJson[i].employeeId == row.employeeId) {
    //     this.locationJson[i].btntext = row.btntext;
    //     this.locationJson[i].visible = row.visible
    //   }
    // }
    this.valueVisible = status
    if (this.isPending == false) {
      this.addeditdata = {
        locationId: row.locationId,
        visible: this.valueVisible,
        locationName: row.locationName,
      };
      if (status === 1) {
        this.msg = 'Location Enabled Successfully ';
      } else {
        this.msg = 'Location Disabled Successfully';
        this.sectionshow = false;
      }
      this.result();
      this.presentToast('success', this.msg);
    }
    else {
      this.toastr.warning('Data is loading')
    }

    console.log('row', row);
  }
  // disableVisibility(index, data, status) {
  //   this.valueVisible = status
  //   if (this.isPending == false) {
  //     this.addeditdata = {
  //       locationId: data.locationId,
  //       visible: this.valueVisible,
  //       locationName: data.locationName,
  //     };
  //     if (status === 1) {
  //       this.msg = 'Enabled' + ' ' + 'Location';
  //     } else {
  //       this.msg = 'Disabled' + ' ' + 'Location';
  //       this.sectionshow = false;
  //     }
  //     this.result();
  //     this.presentToast('success', this.msg);
  //   }
  //   else {
  //     this.toastr.warning('Data is loading')
  //   }
  // }
  showTemplate(data, value) {
    this.getDropDownData()
    this.editData = data ? data : [];
    if (value === 0) {
      this.editData.locationId = '';
      this.selectedVenueId = 1;
      this.title = 'Add Location'
    }
    else {
      this.title = 'Edit Location';
      this.locationName1 = this.editData.locationName
      this.selectedVenueId = this.editData.visible;
    }
    this.sectionshow = true;
  }

  editTableRow(data) {
    this.getDropDownData()
    this.editData = data ? data : [];
    this.title = 'Edit Location';
    this.locationName1 = this.editData.locationName;
    this.selectedVenueId = this.editData.visible;
    this.sectionshow = true;
  }
  closesectionModel() {
    this.sectionshow = false;
    this.locationForm.reset()
  }
  // onSubmitSec() {
  //   console.log(this.SecForm);
  // }
  saveTemp(form) {
    if(form && form.valid){
      this.addeditdata = {
        locationId: this.editData.locationId,
        visible: this.selectedVenueId,
        locationName: this.locationName1,
      };
      if (this.addeditdata.locationId === '') {
        // this.loader = true;
        this.msg = 'Location Added successfully';
      } else {
        // this.loader = true;
        this.msg = 'Location Updated successfully';
      }
      this.result();
      this.locationForm.reset()
    }else {
      Object.keys(this.locationForm.controls).forEach(key => {
        this.locationForm.controls[key].markAsDirty();
      });
    }
  
  }
  result() {
    // this.loader = true;
    this.spinner.show()
    this.updatedata(this.addeditdata, response => {
      console.log('dataMaster', response);
      if (response.type === true) {
        this.getAllLocation()
        this.sectionshow = false;
        this.presentToast('success', this.msg);
        // this.loader = false;
        this.spinner.hide()
      } else {
        this.presentToast('error', '');
        // this.loader = false;
        this.spinner.hide()
      }
    });
  }
  updatedata(addeditdata, cb) {
    const update_location = webApi.domain + webApi.url.updateLocation;
    this.commonFunctionService.httpPostRequest(update_location,addeditdata)
    //this.locationData.updateLocation(addeditdata)
    .then(res => {
      cb(res);
    }, err => {
      this.presentToast('error', '');
      console.log(err);
    });
  }
  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }
  helpContent: any;
  getHelpContent() {
    return new Promise(resolve => {
      this.http1
        .get('../../../../../../assets/help-content/addEditCourseContent.json')
        .subscribe(
          data => {
            this.helpContent = data;
            console.log('Help Array', this.helpContent);
          },
          err => {
            resolve('err');
          }
        );
    });
  }

  // setpage(page) {
  //   //this.nodata = false;
  //   this.pager = this.pagservice.getPager(this.temData.length, page, this.pageSize);
  //   console.log(this.pager.pages,"THIS.PAGER");
  //   this.dispalyData()
  // }

  dispalyData() {
    this.displayTableData = this.temData.slice(this.pager.startIndex, this.pager.endIndex + 1);
    console.log(this.displayTableData,"this.displayTable")
    this.locationJson = this.displayTableData
    for (let i = 0; i < this.locationJson.length; i++) {

      if(this.locationJson[i].visible == 1) {
        this.locationJson[i].btntext = 'fa fa-eye';
      } else {
        this.locationJson[i].btntext = 'fa fa-eye-slash';
      }
    }

  }
  searchData(event){
    this.searchParticipant = event.target.value
    const searchKeyword = this.searchParticipant.toLowerCase();
    this.noMaster=false
    if(searchKeyword.length>=3 ||searchKeyword.length==0){
    const temp = this.temData.filter(function (d) {
      return String(d.locationName).toLowerCase().indexOf(searchKeyword) !== -1 ||
        !searchKeyword
        // String(d.venueName).toLowerCase().indexOf(searchKeyword) !== -1
    });
    this.locationJson = temp;
  
    if(temp.length==0){
      this.noMaster=true
      this.noDataVal={
        margin:'mt-5',
        imageSrc: '../../../../../assets/images/no-data-bg.svg',
        title:"Sorry we couldn't find any matches please try again",
        desc:".",
        titleShow:true,
        btnShow:false,
        descShow:false,
        btnText:'Learn More',
        btnLink:''
      }
    }
      }
}

  search(event){
    this.searchText=event.target.value
    if(this.searchText.length>=3 || this.searchText.length==0){
    this.searchParticipant.locationName = event.target.value
  }
  }
  get name() { return this.locationForm.get('name'); }
}
