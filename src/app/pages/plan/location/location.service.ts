import { Injectable } from '@angular/core';
import { webApi } from '../../../service/webApi';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class LocationService {
  private get_location: string = webApi.domain + webApi.url.getAllLocation;
  private update_location = webApi.domain + webApi.url.updateLocation;
  private get_dropDown = webApi.domain + webApi.url.getDropDown;
  constructor(private _http: HttpClient) { }

  getAllLocation() {
    // return this._http.post(this.get_location,{});

    return new Promise(resolve => {
      this._http.post(this.get_location, {})
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  getDropdown() {
    return new Promise(resolve => {
      this._http.post(this.get_dropDown, {})
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  updateLocation(data) {
    return new Promise(resolve => {
      this._http.post(this.update_location, data)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
}
