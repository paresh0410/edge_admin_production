import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { VenueService } from './venue.service';
import { AddEditVenueService } from './add-venue/add-edit-venue.service';
import { webApi } from '../../../service/webApi';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { SuubHeader } from '../../components/models/subheader.model';
import { PassService } from '../../../service/passService';
import { noData } from '../../../models/no-data.model';
import { HttpClient } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'ngx-venue',
  templateUrl: './venue.component.html',
  styleUrls: ['./venue.component.scss']
})
export class VenueComponent implements OnInit {
  dropDownValue: any = []
  locationDropDownData: any
  visibleDropDownData: any
  selectedLocationId: any=null
  selectedVenueId: number = 1;
  title: string = '';
  editData: any = []
  addeditdata: any = []
  value: number
  editVenueForm = new FormGroup({
    'VENUE': new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(100)
    ]),
    'locationName': new FormControl('', [
      Validators.required,
    ]),
    'address': new FormControl('', [
      Validators.required,
      Validators.minLength(5),
      Validators.maxLength(100)

    ]),
    'address1': new FormControl('', [
      Validators.minLength(5),
      Validators.maxLength(100),

    ]),
    'pincode': new FormControl('', [
      Validators.required,
      Validators.pattern(/^[1-9][0-9]{5}$/)
    ]),
    'contactPerson': new FormControl('', [
      Validators.minLength(3),
      Validators.maxLength(100)
    ]),
    'mobile': new FormControl('', [
      Validators.required,
      Validators.pattern(/^[1-9][0-9]{9}$/)
    ])
  });
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:'No Venues at this time',
    desc:'Venues will appear after the admin manually adds a venue. Venues can be mapped to sessions in Blended courses.',
    titleShow:true,
    btnShow:true,
    descShow:true,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/add-venue',
  }
  header: SuubHeader  = {
    title:'Venue',
    btnsSearch: true,
    placeHolder:"Search by venue name / location",
    searchBar: true,
    btnAdd: 'Add',
    btnBackshow: true,
    btnAddshow: true,
    showBreadcrumb: true,
    breadCrumbList:[
      {
      'name': 'Settings',
      'navigationPath': '/pages/plan',
      }
    ]
  };
  venueJson: any = []

  venueJson1: any = [];
  userDetails: any = [];
  param: any = [];
  btnName = 'Save'
  msg: any;
  valueVisible
  index
  show = false;
  sectionshow: boolean = false;
  searchParticipant: any;
  loader: any;
  noMaster: boolean=false;
  tempData: any = [];
  labels: any = [
    // { labelname: 'ID', bindingProperty: 'venueId', componentType: 'text' },
		{ labelname: 'VENUE', bindingProperty: 'venueName', componentType: 'text' },
    { labelname: 'ADDRESS', bindingProperty: 'fullAddress', componentType: 'text' },
    { labelname: 'CONTACT PERSON', bindingProperty: 'contactPerson', componentType: 'text' },
    { labelname: 'Location', bindingProperty: 'locationName', componentType: 'text' },
		{ labelname: 'ACTION', bindingProperty: 'btntext', componentType: 'button' },
    { labelname: 'EDIT', bindingProperty: 'tenantId', componentType: 'icon' },
  ]
  pager: any;
  temData: any;
  pageSize: number = 10;
  displayTableData: any;
  constructor(private router: Router, private toastr: ToastrService,private pagservice: PassService, private http1: HttpClient,
    private commonFunctionService: CommonFunctionsService,private spinner: NgxSpinnerService, private venueData: VenueService, private editDataSend: AddEditVenueService) {
    this.searchParticipant = {}
  }
  @ViewChild('subsectionForm') SecForm: NgForm;
  ngOnInit() {
    this.getDropDownData()
    this.getHelpContent();

    // this.showTemplate(this.editData, this.value)
    this.header['title'] = this.title + ' '+'Venue'
    this.getAllVenue()
    this.loader = true;
    this.userDetails = JSON.parse(localStorage.getItem('LoginResData'));
    this.param = {
      tId: this.userDetails.data.data.tenantId,
    };
  }
  getAllVenue() {
    // this.loader = true;
    this.spinner.show();
    const get_Venue: string = webApi.domain + webApi.url.getAllVenue;
    this.commonFunctionService.httpPostRequest(get_Venue,{})
    //this.venueData.getAllVenue()
    .then(
      (data) => {
        this.venueJson1 = data;
        this.venueJson = this.venueJson1.data;
        this.tempData = this.venueJson;
        this.temData = this.tempData
        this.loader = false;
        this.spinner.hide();
        // this.setpage(1)
        console.log(this.venueJson)
        for (let i = 0; i < this.tempData.length; i++) {

          if(this.tempData[i].visible == 1) {
            this.tempData[i].btntext = 'fa fa-eye';
          } else {
            this.tempData[i].btntext = 'fa fa-eye-slash';
          }
        }
        if(this.temData.length==0){
          this.noMaster=true;
        }else{
          this.noMaster=false;
        }
      },
      err => {
        this.presentToast('error', '');
        // this.loader=false
      this.spinner.hide()
        this.spinner.hide();
        console.log(err);
        this.noMaster=true;
      });
      // (error: any,) =>(this.loader=false ,console.error(error)) )
  }
  back() {
    this.router.navigate(['../pages/plan']);
  }
  clear() {
    this.noMaster=false
    // this.searchParticipant = '';
    this.searchParticipant = {}
    this.tempData = this.venueJson1.data;
  }
  visibilityTableRow(row) {
    let value;
    let status;

    if (row.visible == 1) {
      row.btntext = 'fa fa-eye-slash';
      value  = 'fa fa-eye-slash';
      row.visible = 0
      status = 0;
    } else {
      status = 1;
      value  = 'fa fa-eye';
      row.visible = 1;
      row.btntext = 'fa fa-eye';
    }
    this.valueVisible=status
    this.addeditdata = {
      venueId: row.venueId,
      locationId: row.locationId,
      venueName: row.venueName,
      address1: row.address1,
      address2: row.address2,
      pinCode: row.pinCode,
      contactPerson: row.contactPerson,
      mobile: row.mobile,
      visible: this.valueVisible,
    };
    if (status === 1) {
      this.msg = 'Venue Enabled Successfully';
    } else {
      this.msg = 'Venue Disabled Successfully';
      this.sectionshow = false;
    }
    // this.result();
    this.presentToast('success', this.msg);
    this.updatedata(this.addeditdata, response => {
      console.log('dataMaster', response);
      if (response.type === true) {
        this.getAllVenue()
        this.sectionshow = false;
        this.presentToast('success', this.msg);
        // this.loader=false;
        this.spinner.hide()
      } else {
        this.presentToast('error', '');
      }
    });


    console.log('row', row);
  }

  disableVisibility(index, data, status) {
    this.valueVisible=status
    this.addeditdata = {
      venueId: data.venueId,
      locationId: data.locationId,
      venueName: data.venueName,
      address1: data.address1,
      address2: data.address2,
      pinCode: data.pinCode,
      contactPerson: data.contactPerson,
      mobile: data.mobile,
      visible: this.valueVisible,
    };
    if (status === 1) {
      this.msg = 'Enabled' + ' ' + 'Venue';
    } else {
      this.msg = 'Disabled' + ' ' + 'Venue';
      this.sectionshow = false;
    }
    this.result();
    this.presentToast('success', this.msg);
  }
  result() {
    // this.loader=true;
    this.updatedata(this.addeditdata, response => {
      console.log('dataMaster', response);
      if (response.type === true) {
        this.getAllVenue()
        this.sectionshow = false;
        this.presentToast('success', this.msg);
        // this.loader=false;
        this.spinner.hide()
      } else {
        this.presentToast('error', '');
      }
    });
  }
  updatedata(addeditdata, cb) {
    const update_Venue = webApi.domain + webApi.url.updateVenue;
    this.commonFunctionService.httpPostRequest(update_Venue,addeditdata)
    //this.venueData.updateVenue(addeditdata)
    .then(res => {
      console.log(cb(res),"sresers")
      cb(res);
    }, err => {
      this.presentToast('error', '');
      console.log(err);
    });
  }

  // showTemplate(data, value) {
  //   this.editDataSend.sendData(data, value)
  //   this.router.navigate(['../pages/plan/venue/addVenue']);
  // }
  closesectionModel() {
    this.sectionshow = false;
  }
  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }
  showDiv (j) {
    this.index = j;
    this.show = !this.show;
    this.venueJson[j]['show'] = this.show;
    console.log(this.venueJson);
  }

  searchData(event){
    this.searchParticipant = event.target.value
    const searchKeyword = this.searchParticipant.toLowerCase();
    this.noMaster=false
    if(searchKeyword.length>=3 ||searchKeyword.length==0){
    const temp = this.venueJson.filter(function (d) {
      return String(d.locationName).toLowerCase().indexOf(searchKeyword) !== -1 ||
        !searchKeyword||
        String(d.venueName).toLowerCase().indexOf(searchKeyword) !== -1
    });
    this.tempData = temp;
  
    if(temp.length==0){
      this.noMaster=true
      this.noDataVal={
        margin:'mt-5',
        imageSrc: '../../../../../assets/images/no-data-bg.svg',
        title:"Sorry we couldn't find any matches please try again",
        desc:".",
        titleShow:true,
        btnShow:false,
        descShow:false,
        btnText:'Learn More',
        btnLink:''
      }
    }
      }
}

  setpage(page) {
    //this.nodata = false;
    this.pager = this.pagservice.getPager(this.temData.length, page, this.pageSize);
    console.log(this.pager.pages,"THIS.PAGER");
    this.dispalyData()
  }

  dispalyData() {
    this.displayTableData = this.temData.slice(this.pager.startIndex, this.pager.endIndex + 1);
    console.log(this.displayTableData,"this.displayTable")
    this.tempData = this.displayTableData

  }

  compareValues(key, order = 'asc') {
    return function innerSort(a, b) {
      if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
        // property doesn't exist on either object
        return 0;
      }

      const varA = (typeof a[key] === 'string')
        ? a[key].toUpperCase() : a[key];
      const varB = (typeof b[key] === 'string')
        ? b[key].toUpperCase() : b[key];

      let comparison = 0;
      if (varA > varB) {
        comparison = 1;
      } else if (varA < varB) {
        comparison = -1;
      }
      return (
        (order === 'desc') ? (comparison * -1) : comparison
      );
    };
  }



  getDropDownData() {
    const get_dropDown = webApi.domain + webApi.url.getDropDown;
    this.commonFunctionService.httpPostRequest(get_dropDown,{})
    //this.addEditData.getDropdown()
    .then(
      (data) => {
        this.dropDownValue = data
        this.locationDropDownData = this.dropDownValue.data.locationDropData;
        this.locationDropDownData.sort(this.compareValues('locationName'))
        console.log(' this.locationDropDownData', this.locationDropDownData);
        this.visibleDropDownData = this.dropDownValue.data.visibleDropData;
        this.selectedLocationId=this.editData.locationId;
      },
      (error: any) => console.error(error))
  }
  sidebarForm = false;

  cloeSidebar() {
    this.editData = [];
    this.editVenueForm.reset();
    this.sidebarForm =false;
  }
  showTemplate(data, value) {
    this.sidebarForm =true;
    // this.loader = true;
    this.spinner.show()
    this.editData = data ? data : [];
    if (value === 0) {
      this.getDropDownData()
      this.editData.locationId = null;
      this.editData.venueId = 0;
      // this.editData.visible = 1;
      this.selectedVenueId=this.visibleDropDownData[0].id
      this.title = 'Add Venue'
      // this.loader = false;
      this.spinner.hide()
    }
    else {
      this.getDropDownData()
      this.title = this.editData.venueName
      this.editData = data
      console.log(this.editData,"edit data")
      this.title = 'Edit Venue'
      this.value = value
      this.selectedVenueId = this.editData.visible;
      // this.loader = false;
      this.spinner.hide()
    }
    this.sectionshow = true;
  }
  onSubmit(form) {
    if(this.editVenueForm && this.editVenueForm.valid){
      this.addeditdata = {
        venueId: this.editData.venueId,
        locationId: this.selectedLocationId,
        venueName: this.editData.venueName,
        address1: this.editData.address1,
        address2: this.editData.address2,
        pinCode: this.editData.pinCode,
        contactPerson: this.editData.contactPerson,
        mobile: this.editData.mobile,
        visible: this.selectedVenueId
      };
      console.log(this.addeditdata)
      if (this.addeditdata.venueId === 0) {
        this.msg = 'Venue Added Successfully';
        this.loader = true;
        this.cloeSidebar()
      } else {
        this.msg = 'Venue Updated Successfully';
        this.loader = true;
        this.cloeSidebar()
      }
      this.result();
    }else {
      Object.keys(this.editVenueForm.controls).forEach(key => {
        this.editVenueForm.controls[key].markAsDirty();
      });
    }


  }
  // result() {
  //   this.updatedata(this.addeditdata, response => {
  //     console.log('dataMaster', response);
  //     if (response.type === true) {
  //       this.presentToast('success', this.msg);
  //       this.loader = false;
  //       this.router.navigate(['../pages/plan/venue']);
  //     } else {
  //       this.presentToast('error', '');
  //       this.loader = false;
  //     }
  //   });
  // }





  get VENUE() {
    return this.editVenueForm.get('VENUE')
  }
  get address() {
    return this.editVenueForm.get('address')
  }
  get email() {
    return this.editVenueForm.get('email')
  }
  get contactPerson() {
    return this.editVenueForm.get('contactPerson')
  }
  get mobile() {
    return this.editVenueForm.get('mobile')
  }
  get locationName() {
    return this.editVenueForm.get('locationName')
  }
  get pincode() {
    return this.editVenueForm.get('pincode')
  }
  get address1() {
    return this.editVenueForm.get('address1')
  }
  helpContent: any;
  getHelpContent() {
    return new Promise(resolve => {
      this.http1
        .get('../../../../../../assets/help-content/addEditCourseContent.json')
        .subscribe(
          data => {
            this.helpContent = data;
            console.log('Help Array', this.helpContent);
          },
          err => {
            resolve('err');
          }
        );
    });
  }
}
