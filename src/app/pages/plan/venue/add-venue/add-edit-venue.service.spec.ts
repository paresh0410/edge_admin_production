import { TestBed } from '@angular/core/testing';

import { AddEditVenueService } from './add-edit-venue.service';

describe('AddEditVenueService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddEditVenueService = TestBed.get(AddEditVenueService);
    expect(service).toBeTruthy();
  });
});
