import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AbstractControl, ValidatorFn, FormGroup, FormControl, Validators } from '@angular/forms';
import { AddEditVenueService } from './add-edit-venue.service';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
import { CommonFunctionsService } from '../../../../service/common-functions.service';
import { webApi } from '../../../../service/webApi';
import { SuubHeader } from '../../../components/models/subheader.model';

@Component({
  selector: 'ngx-add-venue',
  templateUrl: './add-venue.component.html',
  styleUrls: ['./add-venue.component.scss']
})
export class AddVenueComponent implements OnInit {
  editData: any = []
  addeditdata: any = []
  value: number
  sectionshow = false
  selectedLocationId: any=null
  selectedVenueId: number = 1;
  title: string = ''
  msg: any;
  loader: any;
  dropDownValue: any = []
  locationDropDownData: any
  visibleDropDownData: any
  editVenueForm = new FormGroup({
    'VENUE': new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(100)
    ]),
    'locationName': new FormControl('', [
      Validators.required,
    ]),
    'address': new FormControl('', [
      Validators.required,
      Validators.minLength(5),
      Validators.maxLength(100)

    ]),
    'address1': new FormControl('', [
      Validators.minLength(5),
      Validators.maxLength(100),

    ]),
    'pincode': new FormControl('', [
      Validators.required,
      Validators.pattern(/^[1-9][0-9]{5}$/)
    ]),
    'contactPerson': new FormControl('', [
      Validators.minLength(3),
      Validators.maxLength(100)
    ]),
    'mobile': new FormControl('', [
      Validators.required,
      Validators.pattern(/^[1-9][0-9]{9}$/)
    ])
  });
  constructor(private router: Router, private toastr: ToastrService,private commonFunctionService: CommonFunctionsService, private addEditData: AddEditVenueService, private http1: HttpClient) { }
  ngOnInit(): void {
    this.getDropDownData()
    this.getHelpContent();
    this.editData = this.addEditData.editData
    console.log(this.editData,"edit data")
    this.value = this.addEditData.value
    this.showTemplate(this.editData, this.value)
    this.header['title'] = this.title + ' '+'Venue'
    
    this.loader = true;
  }
  header: SuubHeader  = {
    title: this.title,
    btnsSearch: true,
    btnName1: 'Save',
    btnName1show: true,
    btnBackshow: true,
    showBreadcrumb:true,
    breadCrumbList:[
      {
        'name': 'Settings',
        'navigationPath': '/pages/plan',
        },
        {
          'name': 'Venue',
          'navigationPath': '/pages/plan/venue',
          }
    ]
  };

  getDropDownData() {
    const get_dropDown = webApi.domain + webApi.url.getDropDown;
    this.commonFunctionService.httpPostRequest(get_dropDown,{})
    //this.addEditData.getDropdown()
    .then(
      (data) => {
        this.dropDownValue = data
        this.locationDropDownData = this.dropDownValue.data.locationDropData;
        this.visibleDropDownData = this.dropDownValue.data.visibleDropData;
        this.selectedLocationId=this.editData.locationId;
      },
      (error: any) => console.error(error))
  }
  showTemplate(data, value) {
    this.loader = true;
    this.editData = data ? data : [];
    if (value === 0) {
      this.editData.locationId = null;
      this.editData.venueId = 0;
      this.editData.visible = this.selectedVenueId;
      this.title = 'Add'
      this.loader = false;
    }
    else {
      this.title = this.editData.venueName
      this.selectedVenueId = this.editData.visible;
      this.loader = false;
    }
    this.sectionshow = true;
  }
  onSubmit() {
    this.addeditdata = {
      venueId: this.editData.venueId,
      locationId: this.selectedLocationId,
      venueName: this.editData.venueName,
      address1: this.editData.address1,
      address2: this.editData.address2,
      pinCode: this.editData.pinCode,
      contactPerson: this.editData.contactPerson,
      mobile: this.editData.mobile,
      visible: this.selectedVenueId
    };
    console.log(this.addeditdata)
    if (this.addeditdata.venueId === 0) {
      this.msg = 'Venue Added Successfully';
      this.loader = true;
    } else {
      this.msg = 'Venue Updated Successfully';
      this.loader = true;
    }
    this.result();
  }
  result() {
    this.updatedata(this.addeditdata, response => {
      console.log('dataMaster', response);
      if (response.type === true) {
        this.presentToast('success', this.msg);
        this.loader = false;
        this.router.navigate(['../pages/plan/venue']);
      } else {
        this.presentToast('error', '');
        this.loader = false;
      }
    });
  }
  updatedata(addeditdata, cb) {
    const update_Venue = webApi.domain + webApi.url.updateVenue;
    this.commonFunctionService.httpPostRequest(update_Venue,addeditdata)
    //this.addEditData.updateVenue(addeditdata)
    .then(res => {
      cb(res);
    }, err => {
      this.presentToast('error', '');
      console.log(err);
    });
  }
  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }
  back() {
    this.router.navigate(['../pages/plan/venue']);
  }


  get VENUE() {
    return this.editVenueForm.get('VENUE')
  }
  get address() {
    return this.editVenueForm.get('address')
  }
  get email() {
    return this.editVenueForm.get('email')
  }
  get contactPerson() {
    return this.editVenueForm.get('contactPerson')
  }
  get mobile() {
    return this.editVenueForm.get('mobile')
  }
  get address1() {
    return this.editVenueForm.get('address1')
  }
  get pincode() {
    return this.editVenueForm.get('pincode')
  }
  helpContent: any;
  getHelpContent() {
    return new Promise(resolve => {
      this.http1
        .get('../../../../../../assets/help-content/addEditCourseContent.json')
        .subscribe(
          data => {
            this.helpContent = data;
            console.log('Help Array', this.helpContent);
          },
          err => {
            resolve('err');
          }
        );
    });
  }
}