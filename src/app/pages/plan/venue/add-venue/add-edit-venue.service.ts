import { Injectable } from '@angular/core';
import { webApi } from '../../../../service/webApi';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class AddEditVenueService {
editData
value
private update_Venue = webApi.domain + webApi.url.updateVenue;
private get_dropDown = webApi.domain + webApi.url.getDropDown;
  constructor(private _http: HttpClient) { }
  updateVenue(data){
    return new Promise(resolve => {
      this._http.post(this.update_Venue, data)
          .subscribe(data => {
              resolve(data);
          },
              err => {
                  resolve('err');
              });
  });
}
getDropdown(){
  return new Promise(resolve => {
    this._http.post(this.get_dropDown, {})
      .subscribe(data => {
        resolve(data);
      },
        err => {
          resolve('err');
        });
  });
}
  sendData(data,value){
    this.editData=data
    this.value=value
  }
}
