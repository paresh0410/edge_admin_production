import { Injectable } from '@angular/core';
import { webApi } from '../../../service/webApi';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class VenueService {
  private get_Venue: string = webApi.domain + webApi.url.getAllVenue;
  private update_Venue = webApi.domain + webApi.url.updateVenue;
  constructor(private _http: HttpClient) { }

  getAllVenue() {
    return new Promise(resolve => {
      this._http.post(this.get_Venue, {})
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  updateVenue(data) {
    return new Promise(resolve => {
      this._http.post(this.update_Venue, data)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
}
