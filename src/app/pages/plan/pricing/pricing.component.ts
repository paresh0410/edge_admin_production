import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { SuubHeader } from '../../components/models/subheader.model';

@Component({
  selector: 'ngx-pricing',
  templateUrl: './pricing.component.html',
  styleUrls: ['./pricing.component.scss']
})
export class PricingComponent implements OnInit {
  sidebarForm: boolean = false;
  titleName: string = "Add Pricing Plan";
  btnName: string =  "Save";
  header: SuubHeader = {
    title: 'Pricing',
    btnsSearch: true,
    placeHolder: 'Search',
    searchBar: true,
    btnAdd: 'Add Pricing Plan',
    btnBackshow: true,
    btnAddshow: true,
    showBreadcrumb: true,
    breadCrumbList: []
  };

  labels: any = [
    { labelname: 'NO.', bindingProperty: 'number', componentType: 'text' },
    { labelname: 'NAME', bindingProperty: 'fullname', componentType: 'text' },
    { labelname: 'NAME', bindingProperty: 'email', componentType: 'text' },
    { labelname: 'Action', bindingProperty: 'edit', componentType: 'icon' },
  ]

  pricingData: any = [
    { 'number': '1', 'fullname': 'Lorem ipsum', 'email': 'abc@gmail.com', 'edit': '' },
    { 'number': '2', 'fullname': 'Lorem ipsum', 'email': 'def@gmail.com', 'edit': '' },
    { 'number': '3', 'fullname': 'Lorem ipsum', 'email': 'ghy@gmail.com', 'edit': '' },
    { 'number': '4', 'fullname': 'Lorem ipsum', 'email': 'jkl@gmail.com', 'edit': '' },
    { 'number': '5', 'fullname': 'Lorem ipsum', 'email': 'mnc@gmail.com', 'edit': '' },
  ];
  valid: boolean = true;

  constructor(private router: Router) { }

  ngOnInit() {
    this.header.breadCrumbList = [{
      'name': 'Settings',
      'navigationPath': '/pages/plan',
    }]
  }

  search(event) {
    console.log(event);
  }

  addBtn() {
    this.sidebarForm = true;
  }

  back() {
    this.router.navigate(['../pages/plan']);
  }

  clear() {

  }

  cloeSidebar() {
    this.sidebarForm = false;
  }

  save() {

  }
}
