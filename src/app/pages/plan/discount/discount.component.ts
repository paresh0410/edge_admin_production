import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { SuubHeader } from '../../components/models/subheader.model';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { webApi } from '../../../service/webApi';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { noData } from '../../../models/no-data.model';
import * as _moment from 'moment';
import {
  DateTimeAdapter,
  OWL_DATE_TIME_FORMATS,
  OWL_DATE_TIME_LOCALE,
} from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';
const moment = (_moment as any).default ? (_moment as any).default : _moment;
import * as _ from 'lodash';
export const MY_CUSTOM_FORMATS = {
  fullPickerInput: 'DD-MM-YYYY HH:mm:ss',
  parseInput: 'DD-MM-YYYY HH:mm:ss',
  datePickerInput: 'DD-MM-YYYY',
  timePickerInput: 'LT',
  monthYearLabel: 'MMM YYYY',
  dateA11yLabel: 'LL',
  monthYearA11yLabel: 'MMMM YYYY',
};

@Component({
  selector: 'ngx-discount',
  templateUrl: './discount.component.html',
  styleUrls: ['./discount.component.scss'],
  providers: [
    {
      provide: DateTimeAdapter,
      useClass: MomentDateTimeAdapter,
      deps: [OWL_DATE_TIME_LOCALE],
    },
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS },
  ],
})
export class DiscountComponent implements OnInit {
  sidebarForm: boolean = false;
  titleName: string = 'Add Discount Plan';
  btnName: string = 'Save';
  header: SuubHeader = {
    title: 'Discount',
    btnsSearch: true,
    placeHolder: 'Search',
    searchBar: true,
    btnAdd: 'Add Discount Plan',
    btnBackshow: true,
    btnAddshow: true,
    showBreadcrumb: true,
    breadCrumbList: [],
  };
  config: noData={
    imageSrc: '../../../assets/images/no-data-bg.svg',
    title: 'Discount Coupons list is empty.',
    desc: '',
    margin: 'mt-5',
    titleShow: true,
    btnShow: false,
    linkShow: false,
    descShow: false,
    link: '',
    linkTxt: '',
    btnText: '',
    btnLink: '',
  }
  labels: any = [
    // { labelname: 'No.', bindingProperty: 'number', componentType: 'text' },
    { labelname: 'Discount Plan', bindingProperty: 'name', componentType: 'text' },
    { labelname: 'NAME', bindingProperty: 'InternalName', componentType: 'text' },
    { labelname: 'Coupon Name', bindingProperty: 'coupanCode', componentType: 'text' },
    { labelname: 'Discount', bindingProperty: 'showValue', componentType: 'htmlStr' },
    { labelname: 'Applicable Date', bindingProperty: 'startDateShow', componentType: 'date' },
    { labelname: 'Expiry Date', bindingProperty: 'expiryDateShow', componentType: 'date' },
    { labelname: 'Action', bindingProperty: 'edit', componentType: 'icon' },
  ];

  discountData: any = [
    // { number: '1', fullname: 'Lorem ipsum', email: 'abc@gmail.com', edit: '' },
    // { number: '2', fullname: 'Lorem ipsum', email: 'def@gmail.com', edit: '' },
    // { number: '3', fullname: 'Lorem ipsum', email: 'ghy@gmail.com', edit: '' },
    // { number: '4', fullname: 'Lorem ipsum', email: 'jkl@gmail.com', edit: '' },
    // { number: '5', fullname: 'Lorem ipsum', email: 'mnc@gmail.com', edit: '' },
  ];
  valid: boolean = true;

  addEditDiscountForm = {
    id: null,
    name: '',
    type: '',
    discountValue: '',
    couponName: '',
    couponCode: '',
    startDate: null,
    expiryDate: null,
    coupanAvailability: '',
  };
  discountTypeDropDown: any = [];

  constructor(
    private router: Router,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private commonFunctionsService: CommonFunctionsService,
  ) {}

  ngOnInit() {
    this.header.breadCrumbList = [
      {
        name: 'Settings',
        navigationPath: '/pages/plan',
      },
    ];
    this.fetchDiscountList();
    this.getDiscountedDropDownList(() => {});
  }

  tempDisplayList: any;
  search(event) {
    console.log(event);
    const val = event.target.value.toLowerCase();
      if (val.length >= 3 || val.length == 0) {
    this.discountedListNotFound=false
        this.tempDisplayList = [...this.discountData];
        const temp = this.tempDisplayList.filter(function (d) {
          return d.name.toLowerCase().indexOf(val) !== -1 ||
            d.InternalName.toLowerCase().indexOf(val) !== -1 ||
            d.coupanCode.toLowerCase().indexOf(val) !== -1 ||
            d.startDateShow.toLowerCase().indexOf(val) !== -1 ||
            d.expiryDateShow.toLowerCase().indexOf(val) !== -1 ||
            // d.mode.toLowerCase() === val || !val;
            !val
        });
        if(temp.length==0){
          this.discountedListNotFound=true
          this.config={
            margin:'mt-5',
            imageSrc: '../../../../../assets/images/no-data-bg.svg',
            title:"Sorry we couldn't find any matches please try again",
            desc:".",
            titleShow:true,
            btnShow:false,
            descShow:false,
            btnText:'Learn More',
            btnLink:''
          }
        }
        this.tempDisplayList = [...temp];
      }
  
    // if(this.tempDisplayList.length === 0){
    //   this.discountedListNotFound = true;
    //   this.config.title = 'No Matching item found';
    // }else {
    //   this.discountedListNotFound = false;
    // }
  }

  // addBtn() {
  //   this.bindValueToAddEditForm('add', null);
  //   // this.sidebarForm = true;
  //   this.showSidebar();


  // }

  showSidebar(){
    this.sidebarForm = true;
  }
  back() {
    this.router.navigate(['../pages/plan']);
  }

  clear() {
    this.tempDisplayList = this.discountData;
  }

  bindValueToAddEditForm(action, data) {
    if(this.discountTypeDropDown.length === 0){
      this.spinner.show();
      this.getDiscountedDropDownList((value) => {
        if(value){
          this.makeFormDataReady(action, data);
          this.showSidebar();
          this.spinner.hide();
        }else {
          this.spinner.hide();
          this.toastr.warning('Unable to fetch discount type dropdown' , 'Warning');
        }
      });
    }else {
      this.makeFormDataReady(action, data);
      this.showSidebar();
    }
  }

  makeFormDataReady(action, data){
    if (action === 'add') {
      this.addEditDiscountForm = {
        id: 0,
        name: '',
        type: '2',
        discountValue: '',
        couponName: '',
        couponCode: '',
        startDate: null,
        expiryDate: null,
        coupanAvailability: '',
      };
    } else {
      this.addEditDiscountForm = {
        id: data.id,
        name: data.name,
        type: data.type,
        discountValue: data.value,
        couponName: data.InternalName,
        couponCode: data.coupanCode,
        startDate: data.startDate ? new Date(data.startDate) : new Date(),
        expiryDate: data.expiryDate ? new Date(data.startDate) : new Date(),
        coupanAvailability: data.coupanAvailability,
      };

    }

  }
  cloeSidebar() {
    this.sidebarForm = false;
  }

  save(form) {
    console.log('Form ===>',form);
    if(form && form.valid){
      this.createUpdateDiscount();
    }else {
      Object.keys(form.controls).forEach(key => {
        form.controls[key].markAsDirty();
      });
    }
  }

  discountedListNotFound = false;
  fetchDiscountList() {
    const getDiscountedList = webApi.domain + webApi.url.getDiscountList;
    const param = {};
    this.spinner.show();
    this.commonFunctionsService
      .httpPostRequest(getDiscountedList, param)
      // this.serveyService.getServey(param)
      .then(
        (rescompData) => {
          this.spinner.hide();
          console.log('getDiscountedList ==>', rescompData);
          if(rescompData && rescompData['type'] && rescompData['data'] && rescompData['data'].length !== 0){
            this.makeTableDataReady(rescompData['data']);
          }else {
            this.discountedListNotFound = true;
          }
        },
        (error) => {
          this.spinner.hide();
          this.discountedListNotFound = true;
          this.toastr.warning('Something went wrong.', 'Warning');
        }
      );
  }

  getDiscountedDropDownList(cb) {
    const getDiscountDropdown = webApi.domain + webApi.url.getDiscountDropdown;
    const param = {};
    // this.spinner.show();
    this.commonFunctionsService
      .httpPostRequest(getDiscountDropdown, param)
      // this.serveyService.getServey(param)
      .then(
        (rescompData) => {
          // this.spinner.hide();
          console.log('getDiscountDropdown ==>', rescompData);
          if(rescompData && rescompData['type'] === true){
            if(rescompData['data'] && rescompData['data'].length !== 0){
                this.discountTypeDropDown = rescompData['data'];
                console.log('this.discountTypeDropDown',this.discountTypeDropDown)
                cb(true);
            }else {
              // this.toastr.warning('Discount Type DropDown is empty', 'Warning');
              cb(false);
            }
          }else {
            // this.toastr.warning('Something went wrong.', 'Warning');
            cb(false);
          }
        },
        (error) => {
          cb(false);

          // this.spinner.hide();
          // this.toastr.warning('Something went wrong.', 'Warning');
        }
      );
  }

  createUpdateDiscount() {
    const addEditDiscountData = webApi.domain + webApi.url.addEditDiscountData;
    this.spinner.show();
    const param = {
      id: this.addEditDiscountForm.id,
      name: this.addEditDiscountForm.name,
      type: this.addEditDiscountForm.type,
      value: this.addEditDiscountForm.discountValue,
      InternalName: this.addEditDiscountForm.couponName,
      coupanCode: this.addEditDiscountForm.couponCode,
      startDate: this.commonFunctionsService.formatSendDate(this.addEditDiscountForm.startDate),
      expiryDate: this.commonFunctionsService.formatSendDate(this.addEditDiscountForm.expiryDate),
      coupanAvailability: this.addEditDiscountForm.coupanAvailability,
    };
    // this.spinner.show();
    this.commonFunctionsService
      .httpPostRequest(addEditDiscountData, param)
      // this.serveyService.getServey(param)
      .then(
        (rescompData) => {
          // this.spinner.hide();

          console.log('addEditDiscountData ==>', rescompData);
          if(rescompData && rescompData['type'] === true){
            if(rescompData && rescompData['data']){
            this.toastr.success(rescompData['data'][0].msg,'Success');
            this.fetchDiscountList();
              this.cloeSidebar();
            }
          }else {
            this.spinner.hide();
            this.toastr.warning('Coupoun Code already exist', 'Warning');
          }
        },
        (error) => {
          this.spinner.hide();
          this.toastr.warning('Something went wrong.', 'Warning');
        }
      );
  }

  makeTableDataReady(data){
    for (let i = 0; i < data.length; i++) {
      // this.rows[i].Date = new Date(this.rows[i].enrolDate);
      // this.rows[i].enrolDate = this.formdate(this.rows[i].Date);
      data[i]['number'] = i + 1;
      data[i]['edit'] = '';
    }
    this.discountData = data;
    this.tempDisplayList = [...this.discountData];
    console.log('discountData', this.discountData);
  }

}
