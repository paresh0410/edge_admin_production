import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddeditusersComponent } from './addeditusers.component';

describe('AddeditusersComponent', () => {
  let component: AddeditusersComponent;
  let fixture: ComponentFixture<AddeditusersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddeditusersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddeditusersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
