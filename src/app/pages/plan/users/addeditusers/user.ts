export class User{
    constructor(
        public userName: string,
        public userPhNo: number,
        public userEmail: string,
        public countryCode: string,
        public location: string
    ){}
}