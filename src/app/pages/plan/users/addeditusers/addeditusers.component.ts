import {
  Component,
  ViewEncapsulation,
  Directive,
  forwardRef,
  Attribute,
  OnChanges,
  SimpleChanges,
  Input,
  ViewChild,
  ViewContainerRef,
  OnInit,
  ChangeDetectorRef,
} from "@angular/core";
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
import { AddEditUserService } from "./addeditusers.service";
import {
  NG_VALIDATORS,
  Validator,
  Validators,
  AbstractControl,
  ValidatorFn,
  FormGroup
} from "@angular/forms";
import { LocalDataSource } from "ng2-smart-table";
import { NgxSpinnerService } from 'ngx-spinner';
//import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import {
  PaginatePipe,
  PaginationControlsDirective,
  PaginationService
} from "ngx-pagination";
import { Router, NavigationStart, ActivatedRoute } from "@angular/router";
import { Users } from "../users.component";
//import { IMyDpOptions ,IMyDateModel, IMyDate} from 'mydatepicker';
import { INgxMyDpOptions, IMyDateModel } from "ngx-mydatepicker";
// import { ToastrService  } from 'ngx-toastr';

//import { ToastsManager } from 'ng2-toastr';

import { FormlyFieldConfig } from "@ngx-formly/core";
// import { ToasterModule, ToasterService, Toast } from "angular2-toaster";
import { SuubHeader } from "../../../components/models/subheader.model";
import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";
// import { calcBindingFlags } from "@angular/core/src/view/util";

@Component({
  selector: "ngx-addeditusers",
  templateUrl: "./addedit_users.component.html",
  styleUrls: ["./addedit_users.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class AddeditusersComponent {
  /**************************** new data ************************ */


  header: SuubHeader  = {
    title: 'User Management',
    btnsSearch: true,
    searchBar: false,
    dropdownlabel: ' ',
    drplabelshow: false,
    drpName1: 'Add User',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: 'Save',
    btnName2: '',
    btnName3: '',
    btnAdd: '',
    btnName1show: true,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: false,
    filter: false,
    showBreadcrumb: true,
    breadCrumbList:[]
  };

  isChecked: any;
  show: boolean;
  adminDrop: boolean = false;
  multipletype: any = [];
  alltypesof: any = [];
  settingsMultiDrop: any;
  settingsDrop: any;
  settingsTrainerDrop: any;
  settings: any;
  settingsManagerDrop: any;
  // type: any = [
  //   { id: 1, name: "Department" },
  //   { id: 2, name: "Sub-Department" },
  //   { id: 3, name: "Business" },
  //   { id: 4, name: "department" },
  //   { id: 5, name: "department" },
  //   { id: 6, name: "department" }
  // ];

  // multipleDeptType: any = [
  //   { id: 1, name: "Skill level" },
  //   { id: 2, name: "Date of joining" },
  //   { id: 3, name: "Otp flag" },
  //   { id: 4, name: "Code of conduct flag" },
  //   { id: 5, name: "Business group" },
  //   { id: 6, name: "Sub department" },
  //   { id: 7, name: "Organization code" },
  //   { id: 8, name: "Aop code" },
  //   { id: 9, name: "Employee classification" },
  //   { id: 10, name: "Aop Name" }
  // ];

  // learners: any = [
  //   { id: 1, name: "Test learner 1" },
  //   { id: 2, name: "Test learner 2" },
  //   { id: 3, name: "Test learner 3" },
  //   { id: 4, name: "Test learner 4" },
  //   { id: 5, name: "Test learner 5" },
  //   { id: 6, name: "Test learner 6" }
  // ];

  // managers: any = [
  //   { id: 1, name: "Test manager 1" },
  //   { id: 2, name: "Test manager 2" },
  //   { id: 3, name: "Test manager 3" },
  //   { id: 4, name: "Test manager 4" },
  //   { id: 5, name: "Test manager 5" },
  //   { id: 6, name: "Test manager 6" }
  // ];

  // trainers: any = [
  //   { id: 1, name: "Test trainer 1 " },
  //   { id: 2, name: "Test trainer 2" },
  //   { id: 3, name: "Test trainer 3" },
  //   { id: 4, name: "Test trainer 4" },
  //   { id: 5, name: "Test trainer 5" },
  //   { id: 6, name: "Test trainer 6" }
  // ];


  /******************** select department type ********************** */

  // allmultipleDeptType: any;
  // prepareTypeDropdown() {
  //   this.multipleDeptType.forEach(element => {
  //     let multidropdown = {};
  //     multidropdown = {
  //       id: element.id,
  //       name: element.name
  //     };
  //     this.allmultipleDeptType.push(multidropdown);
  //   });
  //   console.log("this.allEmployeeEdgeAutoDrop", this.allmultipleDeptType);
  // }

  selectedtype: any;
  isSave: any;
  validation: any = "";
  validationEmail: any = "";
  validationPhone: any = "";
  valid: boolean = true;

  onTypeSelect(item: any) {
    console.log(item);
    console.log(this.selectedtype);
  }

  OnTypeDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedtype);
  }

  // onTypeSearch(evt: any) {
  //   console.log(evt.target.value);
  //   const val = evt.target.value;
  //   this.multipletype = [];
  //   const tuser = this.multipleDeptType.filter(function(d) {
  //     return (
  //       String(d.edgeempId)
  //         .toLowerCase()
  //         .indexOf(val) !== -1 ||
  //       d.edgeempName.toLowerCase().indexOf(val) !== -1 ||
  //       !val
  //     );
  //   });

  //   // update the rows
  //   this.multipletype = tuser;
  // }

  /******************************* learner**************************** */

  allmultipleLearners: any;
  selectedlearner: any;
  multipleLearnertype: any = [];

  // prepareLearnerDropdown() {
  //   this.learners.forEach(element => {
  //     let learnerdropdown = {};
  //     learnerdropdown = {
  //       id: element.id,
  //       name: element.name
  //     };
  //     this.allmultipleLearners.push(learnerdropdown);
  //   });
  //   console.log("this.allEmployeeEdgeAutoDrop", this.allmultipleLearners);
  // }

  onLearnerSelect(item: any) {
    
    console.log(item,"learner");
    this.selectedlearner = item
    console.log(this.selectedlearner);
  }

  OnLearnerDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedlearner);
  }

  onLearnerSearch(evt: any) {
    console.log(evt.target.value);
    const val = evt.target.value;
    if (val.length >= 3) {
      this.multipleLearnertype = [];
      const tlearners = this.employeeList.filter(function (d) {
        return (
          String(d.id)
            .toLowerCase()
            .indexOf(val) !== -1 ||
          d.name.toLowerCase().indexOf(val) !== -1 ||
          d.ecn.toLowerCase().indexOf(val) !== -1 ||
          d.email.toLowerCase().indexOf(val) !== -1 ||
          !val
        );
      });

      // update the rows
      this.multipleLearnertype = tlearners;
      console.log(this.multipleLearnertype,"this.multipleLearnertype")
    }
  }

  /********************************* Manager *************************** */

  allmultipleManager: any;
  multipleManagerType: any;
  selectedManager: any;

  // prepareManagerDropdown() {
  //   this.managers.forEach(element => {
  //     let multiManagerdropdown = {};
  //     multiManagerdropdown = {
  //       id: element.id,
  //       name: element.name
  //     };
  //     this.allmultipleManager.push(multiManagerdropdown);
  //   });
  //   console.log("this.allEmployeeEdgeAutoDrop", this.allmultipleManager);
  // }

  // onManagerSelect(item: any) {
  //   console.log(item);
  //   console.log(this.selectedManager);
  // }

  // OnManagerDeSelect(item: any) {
  //   console.log(item);
  //   console.log(this.selectedManager);
  // }
  // multipleManagerTypeshow:any =[];
  // onManagerSearch(evt: any) {
  //   console.log(evt.target.value);
  //   const val = evt.target.value;
  //   this.multipleManagerType = [];
  //   this.multipleManagerTypeshow =[];
  //   const tmanager = this.multipleManagerType.filter(function(d) {
  //     return (
  //       String(d.id)
  //         .toLowerCase()
  //         .indexOf(val) !== -1 ||
  //       d.name.toLowerCase().indexOf(val) !== -1 ||
  //       !val
  //     );
  //   });

  //   // update the rows
  //   this.multipleManagerTypeshow = tmanager;
  // }

  /******************************* Trainer ********************************** */
  allmultipleTrainer: any;
  selectedTrainer: any;
  multipleTrainertype: any = [];

  // prepareTrainerDropdown() {
  //   this.trainers.forEach(element => {
  //     let multiTrainerdropdown = {};
  //     multiTrainerdropdown = {
  //       id: element.id,
  //       name: element.name
  //     };
  //     this.allmultipleTrainer.push(multiTrainerdropdown);
  //   });
  //   console.log("this.allEmployeeEdgeAutoDrop", this.allmultipleTrainer);
  // }

  onTrainerSelect(item: any) {
    console.log(item);
    console.log(this.selectedTrainer);
  }

  OnTrainerDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedTrainer);
  }

  onTrainerSearch(evt: any) {
    console.log(evt.target.value);
    const val = evt.target.value;
    if (val.length >= 3) {
      this.multipleTrainertype = [];
      const ttrainer = this.trainerList.filter(function (d) {
        return (
          String(d.id)
            .toLowerCase()
            .indexOf(val) !== -1 ||
          d.name.toLowerCase().indexOf(val) !== -1 ||
          !val
        );
      });
      // update the rows
      this.multipleTrainertype = ttrainer;
    }
  }

  // adminTypeArr: any = [
  //   {
  //     departmentType: null,
  //     subType: null
  //   }
  // ];

  // adminType: any;
  // addRow() {
  //   let adminType = {
  //     departmentType: null,
  //     subType: null
  //   };
  //   this.adminTypeArr.push(adminType);
  //   console.log("evalutionSlotsArr", this.adminTypeArr);
  // }


  showSubFeatures: boolean = false;

  // selectMenuItems(event: Event, i, j) {
  //   let data = (<HTMLInputElement>event.target).checked;
  //   console.log(data);
  //   this.features[i].data[j].featureChecked = data;
  //   let temp = this.features[i].data[j];
  //   if (j == 3 && temp.featureChecked === true) {
  //     this.showSubFeatures = true;
  //   } else if (j == 3 && temp.featureChecked === false) {
  //     this.showSubFeatures = false;
  //   }
  // }

  // onclickSubtype(event: Event, i, j, k) {
  //   let subData = (<HTMLInputElement>event.target).checked;
  //   console.log(subData);
  //   let temp = this.features[i].data[j];
  //   if (j == 3 && temp.featureChecked === true) {
  //     temp.subFeatures[k].subfeatureChecked = subData;
  //     // console.log("Blended course Sub category >>",  this.features[j].subFeatures[k]);
  //   }
  //   // else if(j == 3  && temp.featureChecked === false && temp.subFeatures[k].subfeatureChecked === true){
  //   //   temp.subFeatures[k].subfeatureChecked = false;
  //   // }
  // }

  /******************************* new data *************************** */
  public today = new Date();
  //var year=myDate.getFullYear();
  public myDatePickerOptions: INgxMyDpOptions = {
    // other options...
    dateFormat: "yyyy-mm-dd",
    disableSince: {
      year: this.today.getFullYear(),
      month: this.today.getMonth() + 1,
      day: this.today.getDate()
    }
  };
  public myDatePickerOptions1: INgxMyDpOptions = {
    // other options...
    dateFormat: "yyyy-mm-dd"
    //disableSince: {year: 2018, month: 10, day: 10},
  };
  private placeHolder: string = "Select a date";
  rollstring: any;
  bandstring: any;
  deptstring: any;
  loader: boolean = false;
  // dob :IMyDate = {year:0,month:0,day:0};

  // Initialized to specific date (09.10.2018).
  public model1: any = { date: { year: 2018, month: 10, day: 9 } };
  maxDate: Date;
  title: any;
  para: any;
  url: any;
  id: any;
  profileUrl: any = "assets/images/profile1.png";
  dateOfBirth: any = {
    date: {
      year: 2018,
      month: 10,
      day: 9
    }
  };
  dropdownListDept = [];
  selectedItemsDept = [];
  dropdownSettingsDept = {};
  dropdownListRole = [];
  selectedItemsRole = [];
  selectedItemsRolestr = [];
  dropdownSettingsRole = {};
  dropdownListBand = [];
  selectedItemsBand = [];
  dropdownSettingsBand = {};
  // model = new Users('',null,'','','');
  submitted = false;
  onSubmit() {
    this.submitted = true;
  }
  // onSubmit(formdata) {
  //   this.submitted = true;
  //   let user = formdata;
  //       if(this.para.id == 1) {
  //         this.updateUser(user);
  //       } else {
  //         this.createUser(formdata);
  //       }
  // }

  addEdit(formdata) {
    console.log(formdata);
    let user = formdata;
    if (this.para.id == 1) {
      this.updateUser(user);
    } else {
      this.createUser(formdata);
    }
  }
  passwordEye() {
    this.show = !this.show;
  }
  readUrl(event: any) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.onload = (event: ProgressEvent) => {
        this.profileUrl = (<FileReader>event.target).result;
      };

      reader.readAsDataURL(event.target.files[0]);
    }
  }
  ngOnInit(): void {
    this.header.breadCrumbList=[
      {
        'name': 'Settings',
      'navigationPath': '/pages/plan',
      },
      {
      'name': 'Users',
      'navigationPath': '/pages/plan/users',
      }]
      console.log(this.formdataUser,"hjhgjhgghfrdrsr")
  }
  deleteProfile() {
    //   let user = formdata;
    this.profileUrl = "assets/img/app/profile/avatar4.png";
  }

  strArraySta: any = [
    {
      sName: "Active",
      sId: "Y"
    },
    {
      sName: "Inactive",
      sId: "N"
    }
  ];

  strArraySkilllevel: any = [
    {
      sName: "Beginner",
      sId: "Beginner"
    },
    {
      sName: "Intermediate",
      sId: "Intermediate"
    },
    {
      sName: "Expert",
      sId: "Expert"
    }
  ];

  strArrayVer: any = [
    {
      vName: "Verified",
      vId: "Y"
    },
    {
      vName: "Not Verified",
      vId: "N"
    }
  ];

  strArrayCoc: any = [
    {
      vName: "Verified",
      vId: "Y"
    },
    {
      vName: "Not Verified",
      vId: "N"
    }
  ];

  strArrayAttFlag: any = [
    {
      vName: "Off-Role",
      vId: "y"
    },
    {
      vName: "New On-Role",
      vId: "n"
    },
    {
      vName: "Existing On-Role",
      vId: "y"
    }
  ];
  // formdata:{Role:[]};
  strArrayGender: any = [
    {
      gName: "Male",
      gId: "Male"
    },
    {
      gName: "Female",
      gId: "Female"
    }
  ];

  // rolearray:any=[
  //   {

  //   }
  // ]

  usersData: any = [
    {
      username: "",
      firstname: "",
      lastname: "",
      phone1: "",
      email: ""
    }
  ];

  empData: any = [
    {
      username: "1111",
      password: "abc",
      firstname: "abc",
      lastname: "xyz",
      email: "abc@xyz.com",
      phone1: "123456789",
      city: "Mumbai",
      country: "India",
      skilllevel: "Beginner",
      doj: "2018-12-08",
      otp: "Y",
      coc: "Y",
      attFlag: "Y",
      dob: "2018-12-08",
      dept: "IT"
    }
  ];

  formdataUser: any = {
    userId: "",
    ecn: "",
    username: "",
    password: "",
    firstname: "",
    lastname: "",
    email: "",
    phone: "",
  };

  userCheck: any = [];
  errorMsg: string;
  email: string;

  form: FormGroup = new FormGroup({});

  userFields: FormlyFieldConfig[] = [
    {
      className: "row",
      fieldGroup: [
        {
          className: "col-xs-6",
          key: "email",
          type: "input",
          templateOptions: {
            type: "email",
            label: "Email address",
            placeholder: "Enter email"
          },
          validators: {
            validation: Validators.compose([Validators.required])
          }
        },
        {
          className: "col-xs-6",
          key: "password",
          type: "input",
          templateOptions: {
            type: "password",
            label: "Password",
            placeholder: "Password",
            pattern: ""
          },
          validators: {
            validation: Validators.compose([Validators.required])
          }
        }
      ]
    }
  ];

  user = {
    email: "email@gmail.com",
    checked: false
  };

  dojOldDate: any;
  dobOldDate: any;

  roleFeatures = [];
  changpass: boolean = false;
  changpassword: any = 0;
  chang: any = ''
  missmatch: boolean = false;
  constructor(
    protected service: AddEditUserService,
    private route: ActivatedRoute,
    private router: Router,
    private vcr: ViewContainerRef,
    // private toasterService: ToasterService,
    private toastr: ToastrService,
    private cdf: ChangeDetectorRef,
    private http1: HttpClient,
    private spinner: NgxSpinnerService,
  ) {
    this.getHelpContent();
    /*************** new multi drop ************** */
    if (localStorage.getItem("LoginResData")) {
      this.userLoginData = JSON.parse(localStorage.getItem("LoginResData"));
      console.log("user login data:--", this.userLoginData);
    }
    console.log("user Login Data :-", this.userLoginData);
    this.adminDrop = true;
    this.loader=true
    this.settingsMultiDrop = {
      badgeShowLimit: 4,
      text: "Select Sub-type",
      singleSelection: false,
      classes: "common-multi",
      primaryKey: "id",
      labelKey: "name",
      noDataLabel: "Search Sub-type...",
      enableSearchFilter: true,
      searchBy: ["id", "name"],
      maxHeight:250,
      lazyLoading:true,
    };

    this.settingsDrop = {
      badgeShowLimit: 4,
      text: "Select Learner",
      singleSelection: true,
      classes: "common-multi",
      primaryKey: "id",
      labelKey: "name",
      noDataLabel: "Search Learner...",
      enableSearchFilter: true,
      searchBy: ["id", "name", 'ecn','email'],
      // maxHeight:250,
      lazyLoading:true,
    };

    this.settingsTrainerDrop = {
      badgeShowLimit: 4,
      text: "Select Trainer",
      singleSelection: true,
      classes: "common-multi",
      primaryKey: "id",
      labelKey: "name",
      noDataLabel: "Search Trainer...",
      enableSearchFilter: true,
      searchBy: ["id", "name"],
      maxHeight:250,
      lazyLoading:true,
    };
    this.settings = {
      badgeShowLimit: 4,
      text: "Select ",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      classes: "common-multi",
      primaryKey: "id",
      labelKey: "name",
      enableSearchFilter: true,
      maxHeight:250,
      lazyLoading:true,
    };

    this.settingsManagerDrop = {
      badgeShowLimit: 4,
      text: "Select Manager",
      singleSelection: true,
      classes: "common-multi",
      primaryKey: "id",
      labelKey: "name",
      noDataLabel: "Search Manager...",
      enableSearchFilter: true,
      searchBy: ["id", "name"],
      maxHeight:250,
      lazyLoading:true,
    };

    /*************** new multi drop ************** */

    this.service.getUserdropData().then(
      data => {
        this.loader =false;
        if (data['data'] && data['data'].length) {
          this.dropdownListDept = data['data'][2];
          this.dropdownListRole = data['data'][0];
          this.dropdownListBand = data['data'][5];
        }

        console.log("Registered users :- ", data);
      },
      error => {
        // this.loader =false;
        console.error("Reg Error !");
      }
    );

    this.para = this.service.data;
    if (this.para) {
      this.dropdownListDept = this.para.dropdownListDept;
      this.dropdownListRole = this.para.dropdownListRole;
      this.dropdownListBand = this.para.dropdownListBand;
    }

    this.selectedItemsDept = [];
    this.dropdownSettingsDept = {
      singleSelection: false,
      text: "Select Department",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      enableSearchFilter: true,
      // badgeShowLimit: 2,
      classes: "myclass custom-class",
      maxHeight:250,
      lazyLoading:true,
    };
    this.selectedItemsRole = [];
    this.dropdownSettingsRole = {
      singleSelection: false,
      text: "Select Role",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      enableSearchFilter: true,
      // badgeShowLimit: 2,
      classes: "myclass custom-class"
    };

    this.selectedItemsBand = [];
    this.dropdownSettingsBand = {
      singleSelection: false,
      text: "Select Band",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      enableSearchFilter: true,
      // badgeShowLimit: all,
      classes: "myclass custom-class",
      maxHeight:250,
      lazyLoading:true,
    };

    this.today = new Date();
    console.log("Object" + this.today + "year" + this.today.getFullYear);

    if (this.para == undefined) {
      this.para = {
        id: 0,
      };
    }

    this.maxDate = new Date();
    this.maxDate.setDate(this.today.getDate());
    // this.para = 0;
    // this.para.id = this.para == undefined ? 0 : this.para.id;

    if (this.para.id == 1) {
      this.title = "Edit User Master";
      this.isSave = true

      this.formdataUser = {
        userId: this.para.data.id,
        //    ecn: this.para.data.ecn,
        username: this.para.data.username,
        password: this.para.data.password,
        firstname: this.para.data.firstname,
        lastname: this.para.data.lastname,
        email: this.para.data.email,
        phone: this.para.data.phone,
        // gender: this.para.data.gender,
        // Role: this.para.data.Role
        // roles:this.para.data.roles,
        // department:this.para.data.roles.departments,
        // band:this.para.data.roles.bands
      };
    } else {
      this.title = "Add User Master";
      this.isSave = false
      this.formdataUser = {

        userId: 0,
        username: "",
        password: "",
        firstname: "",
        lastname: "",
        email: "",
        phone: "",
        // gender: "",
        // Role: [
        //   {
        //     roleId: "",
        //     role: "",
        //     RoleDe: [],
        //     RoleBan: [],
        //     RoleSDe: [],
        //     RoleSSDe: []
        //   }
        // ]
        // roles:[],
        // department:this.selectedItemsDept,
        // band:this.selectedItemsBand
      };
    }
    const passData = {
      actId: this.para == undefined ? 0 : this.para.id,
      tId: this.userLoginData.data.data.tenantId,
      uId: this.formdataUser.userId,
    };
    this.getUserDropdownData(passData);
    this.getAllRoleData(passData);
    console.log(this.formdataUser.username,"hjjjhh")
  }
  private endDate: INgxMyDpOptions = {
    // other end date options here...
    // date:Date=new Date();
    dateFormat: "yyyy-mm-dd",
    disableSince: { year: 0, month: 0, day: 0 }
  };

  // optional date changed callback
  onDateChanged(event: IMyDateModel): void {
    // date selected
    if (event.jsdate != null) {
      let d: Date = new Date(event.jsdate.getTime());

      // set previous of selected date
      d.setDate(d.getDate() - 1);
      //this.myDate=new Date();
      //let finalDate=this.myDate;

      // Get new copy of options in order the date picker detect change
      let copy: INgxMyDpOptions = this.getCopyOfEndDateOptions();
      copy.disableSince = {
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate()
      };
      // this.startDate = copy;
      // copy.disableUntil={
      //   year:d.getFullYear()+50,
      //   month:d.getMonth()+(50*12);
      //     day:d.getDay()
      // };
      this.endDate = copy;
    }
  }
  private startDate: INgxMyDpOptions = {
    // start date options here...
    dateFormat: "yyyy-mm-dd",
    //selectionTxtFontSize :"12px",
    disableSince: { year: 0, month: 0, day: 0 }
  };
  getCopyOfEndDateOptions(): INgxMyDpOptions {
    return JSON.parse(JSON.stringify(this.endDate));
  }
  getCopyOfStartDateOptions(): INgxMyDpOptions {
    return JSON.parse(JSON.stringify(this.startDate));
  }

  addRoleFeat() {
    this.roleFeatures.push(this.roleFeatures.length);
  }

  submit(user) {
    console.log(user);
  }

  back() {
    // this.router.navigate(["pages/plan/users"]);
    window.history.back();
  }

  onItemSelect(item: any) {
    console.log(item);
    // console.log(this.selectedItems);
  }
  OnItemDeSelect(item: any) {
    console.log(item);
    // console.log(this.selectedItems);
  }
  onSelectAll(items: any) {
    console.log(items);
  }
  onDeSelectAll(items: any) {
    console.log(items);
  }

  onrollSelect(item: any) {
    console.log(item);
    this.rollstring = "";
    /* Array.prototype.map.call this funcyion is used for converting object value into string*/
    this.rollstring = Array.prototype.map
      .call(this.selectedItemsRole, function (item) {
        return item.id;
      })
      .join(",");
    console.log("rollstring", this.rollstring);
  }
  OnrollDeSelect(item: any) {
    this.rollstring = "";
    /* Array.prototype.map.call this funcyion is used for converting object value into string*/
    this.rollstring = Array.prototype.map
      .call(this.selectedItemsRole, function (item) {
        return item.id;
      })
      .join(",");
    console.log("rollstring", this.rollstring);
  }
  onrollSelectAll(items: any) {
    console.log(items);
    this.rollstring = "";
    this.selectedItemsRole = items;
    /* Array.prototype.map.call this funcyion is used for converting object value into string*/
    this.rollstring = Array.prototype.map
      .call(this.selectedItemsRole, function (item) {
        return item.id;
      })
      .join(",");
    console.log("rollstring", this.rollstring);

    // /*this is for converting string to array */
    // var split=this.rollstring.split(",")
    // console.log('split',split);
  }
  onrollDeSelectAll(items: any) {
    console.log(items);
    this.rollstring = "";
    this.selectedItemsRole = items;
    /* Array.prototype.map.call this funcyion is used for converting object value into string*/
    this.rollstring = Array.prototype.map
      .call(this.selectedItemsRole, function (item) {
        return item.id;
      })
      .join(",");
    console.log("rollstring", this.rollstring);
  }
  ondeptSelect(item: any) {
    console.log(item);
    this.deptstring = "";
    /* Array.prototype.map.call this funcyion is used for converting object value into string*/
    this.deptstring = Array.prototype.map
      .call(this.selectedItemsDept, function (item) {
        return item.id;
      })
      .join(",");
    console.log("deptstring", this.deptstring);
  }
  OndeptDeSelect(item: any) {
    this.deptstring = "";
    /* Array.prototype.map.call this funcyion is used for converting object value into string*/
    this.deptstring = Array.prototype.map
      .call(this.selectedItemsDept, function (item) {
        return item.id;
      })
      .join(",");
    console.log("deptstring", this.deptstring);
  }
  ondeptSelectAll(items: any) {
    console.log(items);
    this.deptstring = "";
    this.selectedItemsDept = items;
    /* Array.prototype.map.call this funcyion is used for converting object value into string*/
    this.deptstring = Array.prototype.map
      .call(this.selectedItemsDept, function (item) {
        return item.id;
      })
      .join(",");
    console.log("deptstring", this.deptstring);
  }
  ondeptDeSelectAll(items: any) {
    console.log(items);
    this.deptstring = "";
    this.selectedItemsDept = items;
    /* Array.prototype.map.call this funcyion is used for converting object value into string*/
    this.deptstring = Array.prototype.map
      .call(this.selectedItemsDept, function (item) {
        return item.id;
      })
      .join(",");
    console.log("deptstring", this.deptstring);
  }
  onbandSelect(item: any) {
    console.log(item);
    this.bandstring = "";
    /* Array.prototype.map.call this funcyion is used for converting object value into string*/
    this.bandstring = Array.prototype.map
      .call(this.selectedItemsBand, function (item) {
        return item.id;
      })
      .join(",");
    console.log("bandstring", this.bandstring);
  }
  OnbandDeSelect(item: any) {
    this.bandstring = "";
    /* Array.prototype.map.call this funcyion is used for converting object value into string*/
    this.bandstring = Array.prototype.map
      .call(this.selectedItemsBand, function (item) {
        return item.id;
      })
      .join(",");
    console.log("bandstring", this.bandstring);
  }
  onbandSelectAll(items: any) {
    console.log(items);
    this.bandstring = "";
    this.selectedItemsBand = items;
    /* Array.prototype.map.call this funcyion is used for converting object value into string*/
    this.bandstring = Array.prototype.map
      .call(this.selectedItemsBand, function (item) {
        return item.id;
      })
      .join(",");
    console.log("bandstring", this.bandstring);
  }
  onbandDeSelectAll(items: any) {
    console.log(items);
    this.bandstring = "";
    this.selectedItemsBand = items;
    /* Array.prototype.map.call this funcyion is used for converting object value into string*/
    this.bandstring = Array.prototype.map
      .call(this.selectedItemsBand, function (item) {
        return item.id;
      })
      .join(",");
    console.log("bandstring", this.bandstring);
  }

  // Addrole() {
  //   if (this.formdata.Role == undefined) {
  //     this.formdata.Role = [];
  //   }
  //   var item = {
  //     //temp:this.formdata.Role.length+100,
  //     roleId: "",
  //     role: "",
  //     RoleDe: [],
  //     RoleBan: [],
  //     RoleSDe: [],
  //     RoleSSDe: []
  //   };
  //   this.formdata.Role.push(item);
  // }


  onSelect(role) {
    for (let i = 0; i < this.dropdownListRole.length; i++) {
      if (role.roleId == this.dropdownListRole[i].id) {
        this.dropdownListRole[i].disabled = true;
      }
    }
    // console.log('data',data);
  }
  uploadfinalData: any = {};
  resUpload: any;
  userLoginData: any;
  // rowData:any;
  rowDataRes: any = [];

  createUser(name) {
    console.log("Create user data" + name);
    var role = "";
    var deparment = "";
    var band = "";
    var deptcnt = "";
    var bandcnt = "";

    /*this is for grtting pipe and comaseparated string*/
    for (var i = 0; i < name.Role.length; i++) {
      var parameter = name.Role[i];
      if (role != "") {
        role += "|";
      }
      if (name.Role.length > 1) {
        if (deparment != "") {
          deparment += "|";
        }
        if (band != "") {
          band += "|";
        }
        if (deptcnt != "") {
          deptcnt += "|";
        }
        if (bandcnt != "") {
          bandcnt += "|";
        }

        if (name.Role[0].RoleDe.length == 0 && i == 1 && deptcnt != "") {
          deparment += "|";
        }
        if (name.Role[0].RoleBan.length == 0 && i == 1 && bandcnt != "") {
          band += "|";
        }
      }
      if (
        String(parameter.roleId) != "" &&
        String(parameter.roleId) != "null"
      ) {
        role += parameter.roleId;
        for (var j = 0; j < name.Role[i].RoleDe.length; j++) {
          var parameter = name.Role[i].RoleDe[j];
          if (deparment != "" && j != 0) {
            deparment += ",";
          }
          if (String(parameter.id) != "" && String(parameter.id) != "null") {
            deparment += parameter.id;
          }
        }
        deptcnt += name.Role[i].RoleDe.length;

        for (var k = 0; k < name.Role[i].RoleBan.length; k++) {
          var parameter = name.Role[i].RoleBan[k];
          if (band != "" && k != 0) {
            band += ",";
          }
          if (String(parameter.id) != "" && String(parameter.id) != "null") {
            band += parameter.id;
          }
        }

        bandcnt += name.Role[i].RoleBan.length;
      }
    }
    //   var rollstring=Array.prototype.map.call(name.roles, function(item) { return item.id; }).join(",");
    //   var deptstring=Array.prototype.map.call(name.zdepartment, function(item) { return item.id; }).join(",");
    //   var bandstring=Array.prototype.map.call(name.band, function(item) { return item.id; }).join(",");
    // console.log('bandstring', this.bandstring)
    let user = {
      userId: name.userId,
      username: name.username,
      password: name.password,
      firstname: name.firstname,
      lastname: name.lastname,
      email: name.email,
      phone: name.phone,
      Role: role,
      RoleDe: deparment,
      RoleBan: band,
      rollscnt: name.Role.length,
      deptcnt: deptcnt,
      bandcnt: bandcnt,
      subdep: "",
      subdepcnt: 0,
      subsubdep: "",
      subsubdepcnt: 0
    };
    console.log("add user data ", user);

    this.uploadfinalData = {
      fileData: user,
      username: this.userLoginData.username,
      tId: this.userLoginData.data.data.tenantId
    };
    console.log("upload final Data:-", this.uploadfinalData);

    this.service.addUser(this.uploadfinalData).subscribe(
      resUserData => {
        // this.loader =false;
        this.rowDataRes = resUserData;
        // this.rowData = resUserData.data[0];
        // this.pageSettings = { pageSize: 6 };
        console.log("Res Upload :- ", this.rowDataRes);
        console.log("File Uploaded!");
        if (this.rowDataRes.data.flag == 1) {
          // var createSuccessToast: Toast = {
          //   type: "success",
          //   title: "Success!",
          //   body: "User added! Click to view details.",
          //   showCloseButton: true,
          //   timeout: 2000,
          //   onHideCallback: () => {
          //     this.router.navigate(["/pages/plan/users"]);
          //   }
          // };
          // this.toasterService.pop(createSuccessToast);
          this.presentToast('success', 'User added');
          this.router.navigate(["/pages/plan/users"]);
        }
        if (this.rowDataRes.data.flag == 0) {
          // var createErrorToast: Toast = {
          //   type: "error",
          //   title: "Error!",
          //   body: this.rowDataRes.data.result,
          //   showCloseButton: true,
          //   timeout: 2000
          //   // onHideCallback: () => {
          //   //     this.router.navigate(['/pages/plan/users']);
          //   // }
          // };
          // this.toasterService.pop(createErrorToast);
          this.presentToast('error', '');
          // this.toastr.onClickToast()
          //      .subscribe( toast => {
          //            this.router.navigate(['/pages/users/induction']);
          //    });
          // this.toastrService.error(this.rowDataRes.data.result, 'Error!');
        }
        // this.id = setInterval(()=> {
        //   this.back();
        // },5000);
      },
      resUserError => {
        // this.loader =false;
        this.errorMsg = resUserError;

        //  this.toastr.onClickToast()
        //        .subscribe( toast => {
        //              // this.router.navigate(['/pages/users/induction']);
        //        });
        // this.toastr.error('Error saving User! Click to view details.', 'Error!', {dismiss: 'click',toastLife: 5000});
      }
    );
    //console.log("Res Upload User Data"+this.row)
  }

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }

  getValidation(data,item) {
    var param ={
      tName:data,
      dataType:item
    }
    this.service.getValidation(param).then(
      res => {
        console.log(res);
        if (res["type"] === true) {
          if(item == 1){
         this.validation  = res['data'][0]['msg'];
         
          }
          else if(item == 2){
            this.validationEmail  = res['data'][0]['msg'];
             }
             if(item == 3){
              this.validationPhone  = res['data'][0]['msg'];
               }

        }
      },
      err => {
        console.log(err);
      }
    );
    if(this.validation == "" && this.validationEmail == "" && this.validationPhone == ""){
      this.valid = true
    }else{
      this.valid = false
    }
  }

  updateUser(name) {
    var role = "";
    var deparment = "";
    var band = "";
    var deptcnt = "";
    var bandcnt = "";

    /*this is for grtting pipe and comaseparated string*/
    for (var i = 0; i < name.Role.length; i++) {
      var parameter = name.Role[i];
      if (role != "") {
        role += "|";
      }
      if (name.Role.length > 1) {
        if (deparment != "") {
          deparment += "|";
        }
        if (band != "") {
          band += "|";
        }
        if (deptcnt != "") {
          deptcnt += "|";
        }
        if (bandcnt != "") {
          bandcnt += "|";
        }

        if (name.Role[0].RoleDe.length == 0 && i == 1 && deptcnt != "") {
          deparment += "|";
        }
        if (name.Role[0].RoleBan.length == 0 && i == 1 && bandcnt != "") {
          band += "|";
        }
      }
      if (
        String(parameter.roleId) != "" &&
        String(parameter.roleId) != "null"
      ) {
        role += parameter.roleId;
        for (var j = 0; j < name.Role[i].RoleDe.length; j++) {
          var parameter = name.Role[i].RoleDe[j];
          if (deparment != "" && j != 0) {
            deparment += ",";
          }
          if (String(parameter.id) != "" && String(parameter.id) != "null") {
            deparment += parameter.id;
          }
        }
        deptcnt += name.Role[i].RoleDe.length;

        for (var k = 0; k < name.Role[i].RoleBan.length; k++) {
          var parameter = name.Role[i].RoleBan[k];
          if (band != "" && k != 0) {
            band += ",";
          }
          if (String(parameter.id) != "" && String(parameter.id) != "null") {
            band += parameter.id;
          }
        }

        bandcnt += name.Role[i].RoleBan.length;
      }
    }

    // var rollstring=Array.prototype.map.call(name.rolls, function(item) { return item.id; }).join(",");
    // var deptstring=Array.prototype.map.call(name.department, function(item) { return item.id; }).join(",");
    // var bandstring=Array.prototype.map.call(name.band, function(item) { return item.id; }).join(",");
    let user = {
      userId: name.userId,
      username: name.username,
      password: name.password,
      firstname: name.firstname,
      lastname: name.lastname,
      email: name.email,
      phone: name.phone,
      Role: role,
      RoleDe: deparment,
      RoleBan: band,
      rollscnt: name.Role.length,
      deptcnt: deptcnt,
      bandcnt: bandcnt,
      subdep: "",
      subdepcnt: 0,
      subsubdep: "",
      subsubdepcnt: 0
    };
    console.log("edit user data ", user);

    if (localStorage.getItem("LoginResData")) {
      this.userLoginData = JSON.parse(localStorage.getItem("LoginResData"));
    }
    console.log("user Login Data :-", this.userLoginData);

    this.uploadfinalData = {
      fileData: user,
      username: this.userLoginData.username
    };
    console.log("upload final Data:-", this.uploadfinalData);

    // optional date changed callback

    this.service.updateUser(this.uploadfinalData).subscribe(
      data => {
        this.rowDataRes = data;
        console.log("Res Upload :- ", this.rowDataRes);
        // console.log('File Uploaded!');
        if (this.rowDataRes.data.flag == 1) {
          // var updateSuccessToast: Toast = {
          //   type: "success",
          //   title: "Success!",
          //   body: "User updated! Click to view details.",
          //   showCloseButton: true,
          //   timeout: 2000,
          //   onHideCallback: () => {
          //     this.router.navigate(["/pages/plan/users"]);
          //   }
          // };
          // this.toasterService.pop(updateSuccessToast);
          this.presentToast('success', 'User updated');
          this.router.navigate(["/pages/plan/users"]);
          //  this.toastr.onClickToast()
          //       .subscribe( toast => {
          //             this.router.navigate(['/pages/users/induction']);
          //       });
          //  this.toastr.success('User updated! Click to view details.', 'Success!', {dismiss: 'click',toastLife: 5000});
          // setTimeout(data => {
          //   this.router.navigate(['/pages/users/induction']);
          // },1000);
        }
        if (this.rowDataRes.data.flag == 0) {
          // var updateErrorToast: Toast = {
          //   type: "error",
          //   title: "Error!",
          //   body: this.rowDataRes.data.result,
          //   showCloseButton: true,
          //   timeout: 2000
          //   // onHideCallback: () => {
          //   //     this.router.navigate(['/pages/plan/users']);
          //   // }
          // };
          // this.toasterService.pop(updateErrorToast);
          this.presentToast('error', '');
          // this.toastr.onClickToast()
          //    .subscribe( toast => {
          //          // this.router.navigate(['/pages/users/induction']);
          //    });
          // this.toastr.error(this.rowDataRes.data.result, 'Error!', {dismiss: 'click',toastLife: 5000});
        }
        // this.id = setInterval(()=> {
        //   this.back();
        // },5000);

        // return true;
      },
      error => {
        console.error("Error saving user!");
        //return Observable.throw(error);

        // this.toastr.onClickToast()
        //      .subscribe( toast => {
        //            // this.router.navigate(['/pages/users/induction']);
        //      });
        // this.toastr.error('Error updating User! Click to view details.', 'Error!', {dismiss: 'click',toastLife: 5000});
      }
    );
  }

  /*THiss function is use for get all roles data */
  sectionData: any = [];
  // roleData:any =[];
  getAllRoleData(data) {
    this.service.getAllRoleData(data).then(
      res => {
        console.log(res);
        if (res["type"] === true) {
          this.sectionData = res["data"];
          if (this.para.id == 1 && this.sectionData) {
            this.sectionData.forEach((sec, y) => {
            sec.role.forEach((item, key) => {
              item.roleProfileData.forEach((value, i) => {
                this.dropdownValue(item, value, 1);
              });
              item.roleEmpData.forEach((eData, i) => {
                if (item.roleId == 7 && eData.name && eData.id) {
                  this.multipleTrainertype.push(eData)
                } else if(eData.name && eData.id) {
                  this.multipleLearnertype.push(eData)
                }
              });
            });
            })
          }
        }
      },
      err => {
        console.log(err);
      }
    );
  }

  /*This function is use for select checkbox */
  onRoleCheckBoxClick(ShowDrop, ind: any,pInd:any) {
    console.log(ShowDrop, ind);
  for(let j = 0; j < this.sectionData.length; j++){
  for(let k =0;k < this.sectionData[j].role.length; k++){
    if(this.sectionData[pInd].role[ind].depRoleIds){
    if(this.sectionData[pInd].role[ind].depRoleIds.includes(this.sectionData[j].role[k].roleId))
    {
      this.sectionData[j].role[k].isChecked = 0;
    }
  }
}
    if(pInd == j){
    for (let i = 0; i < this.sectionData[j].role.length; i++) {
      if (ind == i) {
        this.sectionData[j].role[i].isChecked = ShowDrop == 1 ? 0 : 1;
        if (this.sectionData[j].role[i].isChecked == 1) {
          this.sectionData[j].role[i].roleProfileData = [];
        }
      }
      console.log(this.sectionData[j].role[i]);
    }
  }
}
    this.cdf.detectChanges();
  }
  /*get all role dropdowns*/
  trainerList: any = [];
  employeeList: any = [];
  profileFields: any = [];
  getUserDropdownData(data) {
    this.service.getUserDropdownData(data).then(
      res => {
        console.log(res);
        if (res["type"] === true) {
          var dropData = res["data"];
          this.trainerList = dropData.trainerData;
          this.employeeList = dropData.usersData;
          this.profileFields = dropData.profiles;
        }
      },
      err => {
        console.log(err);
      }
    );
  }

  /*get all profiles dropdowns response*/
  newData: any = [];
  getResultProfileData(data, cb) {
    this.service.getResultProfileData(data).then(
      res => {
        console.log(res);
        if (res["type"] === true) {
          var dropData;
          dropData = res["data"][0];
          cb(dropData);
          // return  this.newData =dropData;
        }
      },
      err => {
        console.log(err);
      }
    );
  }

  /*This function is  use for add profile field data*/
  addProfileData(i,z) {
    // this.valid = false
    console.log(i);
    let data = {
      id: "",
      // name:'',
      fieldData: []
    };
    this.sectionData[z].role[i].roleProfileData.push(data);
    console.log(this.sectionData[z].role[i]);
  }
  /*This function is  use for remove profile field data*/
  removeProfileData(i, j,data, item,z) {
    // this.valid = true
    this.sectionData[z].role[i].roleProfileData.splice(j, 1);
    // this.roleData[i].roleProfileData.splice(j,1);
    delete this.profiledDropData[data.roleName][item.id];
  }

  /*This function is for getting value of profile data*/

  profiledDropData: any = {};
  dropdownValue(data, item, j) {
    var passData = {
      fId: item.id,
      tId: this.userLoginData.data.data.tenantId
    };
    /*this call back function use for wait until get server response*/
    this.getResultProfileData(passData, temp => {
      if (this.profiledDropData[data.roleName]) {
        this.profiledDropData[data.roleName][item.id] = temp;
      } else {
        this.profiledDropData[data.roleName] = {};
        this.profiledDropData[data.roleName][item.id] = temp;
      }
    });
  }
  /* this function is for submit the userDetails*/
  submitUser(form) {
    var validSec = true
    console.log('Form ===>',form);
    if(form && form.valid && this.valid == true){
      this.spinner.show();
      this.cdf.detectChanges();
      console.log("sectionData", this.sectionData);
      let demoArray = [];
      // var validSec = true
      for (let j = 0; j < this.sectionData.length; j++) {
      for (let i = 0; i < this.sectionData[j].role.length; i++) {
        if (this.sectionData[j].role[i].isChecked  == 1) {
          demoArray.push(this.sectionData[j].role[i]);
        }
        if(this.sectionData[j].role[i].isChecked == 1 && this.sectionData[j].role[i].roleType == 1 ){
          if(this.sectionData[j].role[i].roleProfileData.length == 0){
             validSec = false
          }
        }
        // else{
        // var validSec = true
        // }
        
      }
    }
    var valid
    if(demoArray.length == 0){
    valid = false
    }
    else{
      valid = true
    }
    if(valid == true && validSec == true ){
      try {
        this.getRoleDataReady(demoArray, finalString => {
          const passData = {
            userId: this.formdataUser.userId,
            username: this.formdataUser.username,
            password: this.formdataUser.password,
            firstname: this.formdataUser.firstname,
            lastname: this.formdataUser.lastname,
            email: this.formdataUser.email,
            phone: this.formdataUser.phone,
            finalString: finalString,
            tenentId: this.userLoginData.data.data.tenantId,
            userCreated: this.userLoginData.data.data.id,
            chnagePassword: this.changpassword,
          };
          console.log(passData);
          this.service.AddedituserData(passData).then(
            res => {
              console.log(res);
              this.spinner.hide();
              this.cdf.detectChanges();
              if (res["type"] === true) {
                const dropData = res["data"];
                const tempData = res["data"][0][0];
                if (dropData == "err") {
                  // this.notFound = true;
                  // var catUpdate: Toast = {
                  //   type: 'error',
                  //   title: "User",
                  //   body: "Unable to update course.",
                  //   showCloseButton: true,
                  //   timeout: 2000
                  // };
                  // this.toasterService.pop(catUpdate);
                  this.presentToast('error', '');
                } else if (dropData.type == false) {
                  // var catUpdate: Toast = {
                  //   type: 'error',
                  //   title: "User",
                  //   body: 'Something went wrong please try again',
                  //   showCloseButton: true,
                  //   timeout: 2000
                  // };
                  // this.toasterService.pop(catUpdate);
                  this.presentToast('error', '');
                } else {
  
                  // var catUpdate: Toast = {
                  //   type: 'success',
                  //   title: "User",
                  //   // body: "Unable to update course.",
                  //   body: tempData.msg,
                  //   showCloseButton: true,
                  //   timeout: 5000,
                  // };
                  // this.toasterService.pop(catUpdate);
                  this.presentToast('success', tempData.msg);
                  this.back();
                  // this.router.navigate(['/pages/plan/courses/content']);
                }
              }
            },
            err => {
              console.log(err);
              this.spinner.hide();
              this.cdf.detectChanges();
            }
          );
        });
      }catch{
        this.spinner.hide();
        this.cdf.detectChanges();
      }
    }else{
      this.spinner.hide()
      this.presentToast('warning','Please select all the fields')
    }
  
    }else {
      Object.keys(form.controls).forEach(key => {
        form.controls[key].markAsDirty();
        this.presentToast('warning','Please select all the fields')
      });
    }
   
  }
  /*This function is use to format the data */
  getRoleDataReady(demoArray, cb) {
    let testDev: string;
    demoArray.forEach(item => {
      if (testDev) {
        testDev += "|"; /*This is role seperator */
      } else {
        testDev = "";
      }
      Object.values(item).forEach((data, key) => {
        if (key != 3 && key != 4) {
          if (testDev && key != 0) {
            testDev +=
              "#" +
              String(
                data
              ); /*The # is denoted rolData seprator eg> roleCode,roleName  */
          } else {
            testDev += String(data);
          }
        } else if (key == 3) {
          Object.values(data).forEach((temp, i) => {
            if (i > 0) {
              testDev +=
                "$"; /*The $ is denoted rolprofile seprator eg> id =2 i.e department and id =3 ie band */
            }
            Object.values(temp).forEach((temp2, j) => {
              if (j == 1) {
                testDev +=
                  "`" /*The ` is denoted rolprofileData seprator eg> id =2 and name=it  */ +
                  Array.prototype.map
                    .call(temp2, function (sot) {
                      //  console.log("item", item);
                      return Object.values(sot).join("~");
                      /*The ~ is denoted key of fieldData seprator eg> id =2 and name=it  */
                    })
                    .join(
                      "^"
                    ); /*The ^ is denoted fieldData seprator eg> id =2 and id =4  */
              } else {
                if (testDev) {
                  if (i == 0) {
                    testDev += "#" + String(temp2);
                  } else {
                    testDev += String(temp2);
                  }
                } else {
                  testDev += String(temp2);
                }
              }
            });
          });
        } else if (key == 4) { // this is for empdata eg  trainer data or learner data
          if (Object.values(data).length > 0) {
            testDev += "#";
          }
          Object.values(data).forEach((tempk, k) => {
            testDev += 0 + "`";
            Object.values(tempk).forEach((tempkp, kp) => {
              if (kp == 0) {
                testDev += String(tempkp);
              } else {
                testDev += "~" + String(tempkp);
              }
            });
          });
        }
      });
    });
    cb(testDev); /*this will return Value */
    console.log("testDev", testDev);
  }


  // Help Code Start Here //

  helpContent: any;
  getHelpContent() {
    return new Promise(resolve => {
      this.http1
        .get('../../../../../../assets/help-content/addEditCourseContent.json')
        .subscribe(
          data => {
            this.helpContent = data;
            console.log('Help Array', this.helpContent);
          },
          err => {
            resolve('err');
          }
        );
    });
    // return this.helpContent;
  }

  // Help Code Ends Here //
  checkValue(event) {
    console.log(event.target.checked);
    if (event.target.checked == true) {
      this.changpass = true;
      // this.chang.password = this.formdataUser.password
      // this.chang.confirmpassword = this.formdataUser.password
      this.title='Change Password'
      this.changpassword = 1;
    } else {
      this.changpass = false;
      this.changpassword = 0;
    }
  }
  closepop() {
    this.chang = '';
    this.changpass = false;
    this.missmatch = false;
  }
  btnName="Save"
  upadatepass(f,form) {
    if (f.valid) {
      if (f.confirmpassword == f.password) {
        this.formdataUser.password = f.password;
        this.submitUser(form);
        this.changpass = false;
        this.missmatch = false;
      } else {
        this.missmatch = true;
      }
    } else {
      Object.keys(f.controls).forEach(key => {
        f.controls[key].markAsDirty();
      });
    }

  }
}
