import { Injectable, Inject } from '@angular/core';
import {
  Http,
  Response,
  Headers,
  RequestOptions,
  Request
} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AppConfig } from '../../../../app.module';
import { HttpClient } from '@angular/common/http';
import { webApi } from '../../../../service/webApi';

@Injectable()
export class AddEditUserService {
  private _url: string = '/api/usermaster/getallDropdowns';
  private _urlAdd: string = '/api/usermaster/SaveUserRole';
  private _urlUpdate: string = '/api/usermaster/UpdateUserRole';
  private _urlUpload: string = '/api/usermaster/insertuser'; // Dev
  private _urlDelRole: string = '/api/usermaster/delate_role_data';

  private _urlgetAllRolesForAddEdit: string =
    webApi.domain + webApi.url.getAllRolesForAddEdit;
  private _urlgetUserDropdownData: string =
    webApi.domain + webApi.url.getUserDropdownData;
  private _urlgetResultProfileData: string =
    webApi.domain + webApi.url.getResultProfileData;
  private _urladdEditUserData: string =
    webApi.domain + webApi.url.addEditUserData;
    private _urlCheckValid: string =
    webApi.domain + webApi.url.getValidationStatus;
  request: Request;

  constructor(
    @Inject('APP_CONFIG_TOKEN') private config: AppConfig,
    private _http: Http,
    private _httpClient: HttpClient
  ) {
    //this.busy = this._http.get('...').toPromise();
  }
  getUserdropData() {
    const url: any = `${this.config.FINAL_URL}` + this._url;
    // return this._http
    //   .post(url, '')
    //   .map((response: Response) => response.json())
    //   .catch(this._errorHandler);
    return new Promise(resolve => {
      this._httpClient.post(url, '')
        .subscribe(data => {
          resolve(data);
        },
        err => {
          resolve('err');
        });
    });
  }

  delRole(userData) {
    let url: any = `${this.config.FINAL_URL}` + this._urlDelRole;
    // let url:any = `${this.config.DEV_URL}`+this._urlFilter;
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    //let body = JSON.stringify(user);
    return this._http
      .post(url, userData, options)
      .map((res: Response) => res.json());
  }

  addUser(user) {
    let url: any = `${this.config.FINAL_URL}` + this._urlAdd;
    // let url:any = `${this.config.DEV_URL}`+this._urlFilter;
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    //let body = JSON.stringify(user);
    return this._http
      .post(url, user, options)
      .map((res: Response) => res.json());
  }

  updateUser(user) {
    let url: any = `${this.config.FINAL_URL}` + this._urlUpdate;
    // let url:any = `${this.config.DEV_URL}`+this._urlFilter;
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    //let body = JSON.stringify(user);
    return this._http
      .post(url, user, options)
      .map((res: Response) => res.json());
  }
  /*This is use for get role data add or edit  */
  getAllRoleData(param) {
    return new Promise(resolve => {
      this._httpClient
        .post(this._urlgetAllRolesForAddEdit, param)
        //.map(res => res.json())
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            resolve('err');
          }
        );
    });
  }

  /*This is use for get all dropdown data */
  getUserDropdownData(param) {
    // return new Promise(resolve => {
    //   this._httpClient
    //     .post(this._urlgetUserDropdownData, param)
    //     //.map(res => res.json())
    //     .subscribe(
    //       data => {
    //         resolve(data);
    //       },
    //       err => {
    //         resolve('err');
    //       }
    //     );
    // });
    return new Promise(resolve => {
      this._httpClient.post(this._urlgetUserDropdownData, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  /*This is use for get profile data value */
  getResultProfileData(param) {
    return new Promise(resolve => {
      this._httpClient
        .post(this._urlgetResultProfileData, param)
        //.map(res => res.json())
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            resolve('err');
          }
        );
    });
  }

  getValidation(param) {
    return new Promise(resolve => {
      this._httpClient
        .post(this._urlCheckValid, param)
        //.map(res => res.json())
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            resolve('err');
          }
        );
    });
  }

  /*This is use for add edit data value */
  AddedituserData(param) {
    return new Promise(resolve => {
      this._httpClient
        .post(this._urladdEditUserData, param)
        //.map(res => res.json())
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            resolve('err');
          }
        );
    });
  }

  _errorHandler(error: Response) {
    console.error(error);
    return Observable.throw(error || 'Server Error');
  }

  public data: any;
}
