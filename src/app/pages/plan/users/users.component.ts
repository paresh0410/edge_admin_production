import { Component, ViewContainerRef, OnInit, ViewChild, Compiler, ViewEncapsulation, ElementRef } from '@angular/core';
import { Router, NavigationStart, Routes, ActivatedRoute } from '@angular/router';
import { UsersService } from './users.service';
import { Column, GridOption, AngularGridInstance, FieldType, Filters, Formatters, GridStateChange, OperatorType, Statistic } from 'angular-slickgrid';
import { NbThemeService } from '@nebular/theme';
import { AddEditUserService } from './addeditusers/addeditusers.service'
import { UploadUsersService } from './uploadusers/uploadusers.service';
import { AppService } from '../../../app.service';
import { ToastrService } from 'ngx-toastr';
// import { ToastsManager } from 'ng2-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { webApi } from '../../../service/webApi';
import { SuubHeader } from '../../components/models/subheader.model';
import { noData } from '../../../models/no-data.model';

@Component({
  selector: 'users',
  templateUrl: './users.html',
  styleUrls: ['./users.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class Users {
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../assets/images/no-data-bg.svg',
    title:'No Users at this time',
    desc:'Users will appear here after they are added to the organisation. Optionally, admins can manually add or bulk import learners and users will be automatically created.',
    descShow:true,
    titleShow:true,
    btnShow:true,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/setting-how-to-add-a-user-manually',
  }

  header: SuubHeader  = {
    title: 'Users',
    btnsSearch: true,
    placeHolder: 'Search',
    searchBar: true,
    dropdownlabel: '',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: '',
    btnName3: '',
    btnAdd: 'Add User',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: true,
    filter: false,
    showBreadcrumb: true,
    breadCrumbList:[]   
  };

  angularGrid: AngularGridInstance;
  angularGrid1: AngularGridInstance;
  unRegGrid: AngularGridInstance;
  regGrid: AngularGridInstance;
  enrolGrid: AngularGridInstance;

  columnDefinitions: Column[];
  columnDefinitions1: Column[];
  unRegcolumnDefinitions: Column[];
  regColumnDefinitions: Column[];
  enrolColumnDefinitions: Column[];

  gridOptions: GridOption;

  gridObj: any;
  gridObj1: any;
  unRegGridObj: any;
  regGridObj: any;
  enrolGridObj: any;
  userData: any[];
  dataset: any[];
  dataset1: any[];
  unRegDataset: any[];
  regDataset: any[];
  enrolDataset: any[];
  unRegCheckData1: boolean = false;
  selectedUnReg: any[];
  selectedReg: any[];
  selectedEnrol: any[];
  selectedTitles: any[];
  selectedTitle: any;

  errorMsg: string;
  loader: any;
  toggleBool: boolean = true;
  selectedAll: any;
  userChecked: any = [];

  tab1: any = false;
  tab2: any = false;
  tab3: any = false;

  selectedRows: any;
  selectedRowsReg: any;
  selectedRowsUnReg: any;

  btnShowSingleReg: any = false;
  btnShowMultipleReg: any = false;

  btnShowSingleUnReg: any = false;
  btnShowMultipleUnReg: any = false;

  unRegRowData: any = [];
  unRegRowDataCount: any = 0;

  regRowData: any = [];
  regRowDataCount: any = 0;

  enrolRowData: any = [];
  enrolRowDataCount: any = 0;

  dropdownListDept = [];
  dropdownListRole = [];
  dropdownListBand = [];
  userTableData: any = [];
  userInductedData: any = [];
  gridShow: boolean;

  changeEvent(event) {
    if (event.target.checked == true) {
      this.toggleBool = false;
    }
    else {
      this.toggleBool = true;
    }
  }
  search: any;
  showdata: any = [];
  addUser: boolean = false;
  editUser: boolean = false;
  unRegUsers: any = [];
  loginUserdata: any;
  currentPage = 1;
  searchstr = '';
  users: any = [];
  labels: any = [
		{ labelname: 'USERNAME', bindingProperty: 'username', componentType: 'text' },
		{ labelname: 'FIRST NAME', bindingProperty: 'firstname', componentType: 'text' },
		{ labelname: 'LAST NAME', bindingProperty: 'lastname', componentType: 'text' },
		{ labelname: 'EMAIL', bindingProperty: 'email', componentType: 'text' },
    { labelname: 'PHONE', bindingProperty: 'phone', componentType: 'text' },
    { labelname: 'EDIT', bindingProperty: 'tenantId', componentType: 'icon' },
	  ];
  totalpages: any;
  selected = [];
  readonly headerHeight = 50;
  readonly rowHeight = 50;
  infiniteworking: boolean = false;
  CourseFilterList: any = [];
  isLoading: boolean = false;
  isworking = false;
  constructor(private themeService: NbThemeService,
    // private toasterService: ToasterService,
    private commonFunctionService: CommonFunctionsService,
    private router: Router, protected service: UsersService, public el: ElementRef,
    vcr: ViewContainerRef, public routes: ActivatedRoute,
    protected uploadService: UploadUsersService, protected passService: AddEditUserService,
    private toastr: ToastrService, private AppService: AppService,private spinner: NgxSpinnerService) {
    // this.dataset = this.prepareData();
    // this.loader = true;
    this.spinner.show();
    this.showdata = this.AppService.getfeatures();
    if (this.showdata) {
      for (let i = 0; i < this.showdata.length; i++) {
        if (this.showdata[i] == 'FAU') {
          this.addUser = true;
        }
        if (this.showdata[i] == 'FEU') {
          // this.editUser =true;
        }
      }
    }
    if (localStorage.getItem('LoginResData')) {
      this.loginUserdata = JSON.parse(localStorage.getItem('LoginResData'));
    }
    this.search = {};


    this.service.getUserdropData()
      .then(data => {
        // this.loader =false;
        console.log("User Drop Data ==>", data)
        if (data['data']) {
          this.dropdownListDept = data['data'][2];
          this.dropdownListRole = data['data'][0];
          this.dropdownListBand = data['data'][5];
        }
        console.log('Registered users :- ', data);
      },
        error => {
          // this.loader =false;
          console.error("Reg Error !");
        });

  }

  ngOnInit(): void {
    this.prepareGrid();
    this.userlist(this.searchstr, this.currentPage);
    // this.loader = false;
    this.header.breadCrumbList=[{
      'name': 'Settings',
      'navigationPath': '/pages/plan',
      }]
  }
  userlist(str, pageNo) {
    this.gridShow=false
    const data = {
      tId: this.loginUserdata.data.data.tenantId,
      lmt: 100,
      pno: pageNo,
      str: str,
    };
    if (pageNo <= 1){
      // this.loader = true;
    this.spinner.show();
    }
    const getuserListUrl: string = webApi.domain + webApi.url.getalluser;
    this.commonFunctionService.httpPostRequest(getuserListUrl,data)
    //this.service.getuserlist(data)
    .then(res => {
      console.log(res);
      if (res['type'] === true) {
        let list = res['data']['list'];
        if (this.users.length > 0 && pageNo > 1) {
          this.users = this.users.concat(list)
        } else {
          this.users = list;
        }
        if(this.users.length==0){
          this.gridShow=true;
         this.noDataVal={ margin:'mt-3',
          imageSrc: '../../../assets/images/no-data-bg.svg',
          title:"Sorry we couldn't find any user please try again",
          desc:'',
          descShow:false,
          titleShow:true,
          btnShow:false,
          btnText:'Learn More',
          btnLink:'',
        }
        }
        this.isworking = false;
        console.log(this.users);
        this.totalpages = res['data'].totalPages;
      }
      // this.loader = false;
    this.spinner.hide();
    // this.gridShow=false

    }, err => {
      // this.loader = false;
    this.spinner.hide();
    this.gridShow=true
      console.log(err);
    })
  }

  onScrollDown(offsetY: number, pageload: boolean) {
    const viewHeight =
      this.el.nativeElement.getBoundingClientRect().height - this.headerHeight;
    console.log(viewHeight,"content");
    if (!this.isworking) {
      this.currentPage++;
      if (this.currentPage <= this.totalpages) {
        this.isworking = true;
        this.userlist(this.searchstr, this.currentPage);
      }
    }
  }
  onSelect(event) {
    console.log(event);
    let unRegCheckData = [];
    this.unRegCheckData1 = false;


    console.log(event.selected);
    unRegCheckData = event.selected;
    if (unRegCheckData.length == 1) {
      this.unRegCheckData1 = true;
      this.unRegRowData = unRegCheckData;
      console.log(this.unRegRowData);
    } else {
      this.unRegCheckData1 = false;
    }



  }
  rowdata : any = {}
   // methos for table componenet edit
  onSelectRow(event) {
    console.log(event);
    let unRegCheckData = [];
    this.unRegCheckData1 = false;
    console.log(event);
    unRegCheckData = event;
    this.rowdata = unRegCheckData
    // if (unRegCheckData.length == 1) {
    //   this.unRegCheckData1 = true;
    //   this.unRegRowData = unRegCheckData;
    //   console.log(this.unRegRowData);
    // } else {
    //   this.unRegCheckData1 = false;
    // }
    this.editRowUsers(1);

  }

  // methos for table componenet edit
  editRowUsers(id) {
    let data = this.rowdata;


    var data1 = {
      data: data,
      id: id,
      dropdownListDept: this.dropdownListDept,
      dropdownListRole: this.dropdownListRole,
      dropdownListBand: this.dropdownListBand
    }
    console.log('E Data', data1);
    this.passService.data = data1;
    this.router.navigate(['add-edit-users'], { relativeTo: this.routes });
  }


  onsearch(evt: any) {
    // if(evt.keyCode!=13){
    console.log(evt,"search");
    this.searchstr = evt.target.value;
    // if (evt.keyCode === 13) {
      const val=evt.target.value;
      if(val.length>=3 || val.length==0) {
      this.currentPage = 1;
      this.userlist(this.searchstr, this.currentPage);
      }
    // }
  }
  clear(){
    // if(this.searchstr){
    console.log("enters")
    if(this.searchstr.length>=3){
    this.search = {};
    this.searchstr = '';
    this.currentPage = 1;
    this.userlist(this.searchstr, this.currentPage);
    this.searchstr='';
    }
    else{
      this.search={};
    }
  }

  // loadUnRegData(){
  //     this.loader = true;
  //     this.service.getUnRegEmp()
  //     .subscribe(resUserData => {
  //       this.loader =false;
  //       this.unRegUsers = resUserData.data[0];
  //       this.unRegDataset = resUserData.data[0];
  //       this.unRegRowData = resUserData.data[0];
  //       this.unRegRowDataCount = this.unRegUsers.length;
  //       this.tabCont1 =true;
  //       this.tabCont2 =false;
  //       this.tabCont3 =false;
  //       console.log('updated Unreg data', this.unRegRowData)
  //       // this.loadInduction();
  //     },
  //     resUserError => {
  //       this.loader =false;
  //       this.errorMsg = resUserError
  //     });
  // }

  // loadInducted(){
  //     this.loader = true;
  //     this.service.getUsers(this.filterDataReg)
  //     .subscribe(resUserData => {
  //       this.loader = false;
  //       // this.dataset1 = resUserData.data[1];
  //       this.enrolDataset = resUserData.data[1];
  //       this.enrolRowData = resUserData.data[1];
  //       this.tabCont1 = false;
  //       this.tabCont2 =false;
  //       this.tabCont3 =true;
  //       this.enrolRowDataCount = this.enrolRowData.length;
  //       console.log('Enrolled Users', this.enrolRowData)
  //       // this.loadInduction();
  //     },
  //     resUserError => {
  //       this.loader =false;
  //       this.errorMsg = resUserError
  //     });
  // }

  // loadInduction(){
  //     this.loader = true;
  //     this.service.getUsers(this.filterDataUnReg)
  //     .subscribe(resUserData => {
  //         this.loader =false;
  //         this.regRowData = resUserData.data[1];
  //         // this.dataset = resUserData.data[1];
  //         this.regDataset = resUserData.data[1];
  //         this.tabCont1 =false;
  //         this.tabCont2 =true;
  //         this.tabCont3 =false;
  //         this.regRowDataCount = this.regRowData.length;
  //         if(this.selectedAll == true){
  //             this.selectedAll = false;
  //         }
  //         if(this.userTableData.length>0){
  //             for(let i=0;i<this.userTableData.length;i++){
  //                 this.userTableData[i].id=i+1;
  //             }
  //         }
  //         this.toggleBool = true;
  //     },
  //     resUserError => {
  //       this.loader =false;
  //       this.errorMsg = resUserError
  //     });
  // }

  prepareGrid() {
    // this.loader = true;
    // this.loadInduction();

    this.columnDefinitions = [
      // {
      //     id: 'ecn',
      //     name: 'EMPLOYEE CODE',
      //     field: 'ecn',
      //     sortable: true,
      //     minWidth: 100,
      //     maxWidth: 200,
      //     type: FieldType.string,
      //     filterable: true,
      //     filter: { model: Filters.compoundInput }
      // },
      {
        id: 'username',
        name: 'USERNAME',
        field: 'username',
        sortable: true,
        type: FieldType.string,
        minWidth: 100,
        maxWidth: 200,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      // {
      //     id: 'password',
      //     name: 'PASSWORD',
      //     field: 'password',
      //     sortable: true,
      //     minWidth: 100,
      //     maxWidth: 200,
      //     type: FieldType.string,
      //     filterable: true,
      //     filter: { model: Filters.compoundInput }
      // },
      {
        id: 'firstname',
        name: 'FIRST NAME',
        field: 'firstname',
        sortable: true,
        minWidth: 100,
        maxWidth: 200,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        id: 'lastname',
        name: 'LAST NAME',
        field: 'lastname',
        sortable: true,
        minWidth: 100,
        maxWidth: 200,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        id: 'email',
        name: 'EMAIL',
        field: 'email',
        minWidth: 100,
        maxWidth: 200,
        type: FieldType.string,
        sortable: true,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        id: 'phone',
        name: 'PHONE',
        field: 'phone',
        minWidth: 100,
        maxWidth: 200,
        type: FieldType.string,
        sortable: true,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      // {
      //     id: 'gender',
      //     name: 'GENDER',
      //     field: 'gender',
      //     minWidth: 100,
      //     maxWidth: 200,
      //     type: FieldType.string,
      //     sortable: true,
      //     filterable: true,
      //     filter: { model: Filters.compoundInput }
      // },
      // {
      //     id: 'role',
      //     name: 'ROLES',
      //     field: 'role',
      //     minWidth: 100,
      //     maxWidth: 200,
      //     type: FieldType.string,
      //     sortable: true,
      //     filterable: true,
      //     filter: { model: Filters.compoundInput }
      // },
      // {
      //     id: 'department',
      //     name: 'DEPARTMENT',
      //     field: 'department',
      //     minWidth: 100,
      //     maxWidth: 200,
      //     type: FieldType.string,
      //     sortable: true,
      //     filterable: true,
      //     filter: { model: Filters.compoundInput }
      // },
      // {
      //     id: 'band',
      //     name: 'BAND',
      //     field: 'band',
      //     minWidth: 100,
      //     maxWidth: 200,
      //     type: FieldType.string,
      //     sortable: true,
      //     filterable: true,
      //     filter: { model: Filters.compoundInput }
      // },
    ];
    this.gridOptions = {
      enableAutoResize: true,       // true by default
      enableCellNavigation: true,
      enableFiltering: true,
      enableColumnReorder: false,
      rowSelectionOptions: {
        // True (Single Selection), False (Multiple Selections)
        selectActiveRow: false
      },
      preselectedRows: [],
      enableCheckboxSelector: true,
      enableRowSelection: true,
      enablePagination: true,
      leaveSpaceForNewRows: true
    };


  }


  GridReady(unRegGrid: any) {
    // this.loader = true;

    // this.unRegcolumnDefinitions=[
    //     {
    //         id: 'code',
    //         name: 'EMPLOYEE CODE',
    //         field: 'code',
    //         sortable: true,
    //         type: FieldType.string,
    //         minWidth: 55,
    //         filterable: true,
    //         filter: { model: Filters.compoundInput }
    //     },
    //     {
    //         id: 'name',
    //         name: 'EMPLOYEE  NAME',
    //         field: 'name',
    //         sortable: true,
    //         minWidth: 55,
    //         type: FieldType.string,
    //         filterable: true,
    //         filter: { model: Filters.compoundInput }
    //     },
    //     {
    //         id: 'band',
    //         name: 'EMPLOYEE BAND',
    //         field: 'band',
    //         type: FieldType.string,
    //         minWidth: 55,
    //         sortable: true,
    //         filterable: true,
    //         filter: { model: Filters.compoundInput }
    //     },
    //     {
    //         id: 'reportingmanagercode',
    //         name:'REPORTING MANAGER CODE',
    //         field: 'reportingmanagercode',
    //         minWidth: 55,
    //         type: FieldType.string,
    //         sortable: true,
    //         filterable: true,
    //         filter: { model: Filters.compoundInput }
    //     },
    //     {
    //         id: 'reportingmanagername',
    //         name:'REPORTING MANAGER NAME',
    //         field: 'reportingmanagername',
    //         minWidth: 55,
    //         type: FieldType.string,
    //         sortable: true,
    //         filterable: true,
    //         filter: { model: Filters.compoundInput }
    //     },
    //     {
    //         id: 'department',
    //         name: 'DEPARTMENT',
    //         field: 'department',
    //         minWidth: 55,
    //         type: FieldType.string,
    //         sortable: true,
    //         filterable: true,
    //         filter: { model: Filters.compoundInput }
    //     },
    //     {
    //         id: 'role',
    //         name: 'ROLE/DESIGNATION',
    //         field: 'role',
    //         minWidth: 55,
    //         type: FieldType.string,
    //         sortable: true,
    //         filterable: true,
    //         filter: { model: Filters.compoundInput }
    //     },
    // ];
    this.unRegGrid = unRegGrid;
    // this.angularGrid1 = angularGrid;

    // var cols = unRegGrid.slickGrid.getColumns();
    // var unRegColoum = this.unRegcolumnDefinitions;
    // console.log("UnReg columns - ", cols);
    // if(cols.length > 0){
    //   this.unRegcolumnDefinitions = [];
    //   this.unRegcolumnDefinitions.push(cols[0]);
    //   unRegColoum.forEach(data=>{
    //     this.unRegcolumnDefinitions.push(data);
    //   })
    // }

    // unRegGrid.slickGrid.setColumns(this.unRegcolumnDefinitions);
    this.unRegGridObj = unRegGrid && unRegGrid.slickGrid || {};

    // this.loader = false;
  }

  unRegHandleSelectedRowsChanged(e, args) {
    // console.log('args.rows',args.rows);
    let unRegCheckData = [];
    this.unRegCheckData1 = false;
    if (Array.isArray(args.rows)) {
      this.selectedUnReg = args.rows.map(idx => {
        const item = this.unRegGridObj.getDataItem(idx);
        unRegCheckData.push(item);
        // return item.title || '';
      });
    }
    console.log('Selected Unreg users ', unRegCheckData);
    // this.unRegCheckData1=unRegCheckData;
    for (var i = 0; i < this.unRegRowData.length; i++) {
      this.unRegRowData[i].check = false;
    }
    for (var i = 0; i < this.unRegRowData.length; i++) {
      for (var j = 0; j < unRegCheckData.length; j++) {
        if (unRegCheckData.length == 1) //this is for actiion button disabled
        {
          this.unRegCheckData1 = true;
        } else {
          this.unRegCheckData1 = false;
        }
        if (this.unRegRowData[i].id == unRegCheckData[j].id) {
          this.unRegRowData[i].check = true;
        }
      }
    }
  }

  unRegGridStateChanged(gridState: GridStateChange) {
    console.log('Client sample, Grid State changed:: ', gridState);
  }

  // regGridReady(regGrid: any) {
  //     this.loader = true;

  //     this.regColumnDefinitions=[
  //         {
  //             id: 'code',
  //             name: 'EMPLOYEE CODE',
  //             field: 'code',
  //             sortable: true,
  //             type: FieldType.string,
  //             minWidth: 55,
  //             filterable: true,
  //             filter: { model: Filters.compoundInput }
  //         },
  //         {
  //             id: 'name',
  //             name: 'EMPLOYEE  NAME',
  //             field: 'name',
  //             sortable: true,
  //             minWidth: 55,
  //             type: FieldType.string,
  //             filterable: true,
  //             filter: { model: Filters.compoundInput }
  //         },
  //         {
  //             id: 'band',
  //             name: 'EMPLOYEE BAND',
  //             field: 'band',
  //             type: FieldType.string,
  //             minWidth: 55,
  //             sortable: true,
  //             filterable: true,
  //             filter: { model: Filters.compoundInput }
  //         },
  //         {
  //             id: 'reportingmanagercode',
  //             name:'REPORTING MANAGER CODE',
  //             field: 'reportingmanagercode',
  //             minWidth: 55,
  //             type: FieldType.string,
  //             sortable: true,
  //             filterable: true,
  //             filter: { model: Filters.compoundInput }
  //         },
  //         {
  //             id: 'reportingmanagername',
  //             name:'REPORTING MANAGER NAME',
  //             field: 'reportingmanagername',
  //             minWidth: 55,
  //             type: FieldType.string,
  //             sortable: true,
  //             filterable: true,
  //             filter: { model: Filters.compoundInput }
  //         },
  //         {
  //             id: 'department',
  //             name: 'DEPARTMENT',
  //             field: 'department',
  //             minWidth: 55,
  //             type: FieldType.string,
  //             sortable: true,
  //             filterable: true,
  //             filter: { model: Filters.compoundInput }
  //         },
  //         {
  //             id: 'role',
  //             name: 'ROLE/DESIGNATION',
  //             field: 'role',
  //             minWidth: 55,
  //             type: FieldType.string,
  //             sortable: true,
  //             filterable: true,
  //             filter: { model: Filters.compoundInput }
  //         },
  //     ];
  //     this.regGrid = regGrid;
  //     // this.angularGrid1 = angularGrid;

  //     var cols = regGrid.slickGrid.getColumns();
  //     var regColoum = this.regColumnDefinitions;
  //     console.log("Reg columns - ", cols);
  //     if(cols.length > 0){
  //       this.regColumnDefinitions = [];
  //       this.regColumnDefinitions.push(cols[0]);
  //       regColoum.forEach(data=>{
  //         this.regColumnDefinitions.push(data);
  //       })
  //     }

  //     regGrid.slickGrid.setColumns(this.regColumnDefinitions);
  //     this.regGridObj = regGrid && regGrid.slickGrid || {};

  //     this.loader = false;
  // }


  // regHandleSelectedRowsChanged(e, args) {
  //     let regCheckData = [];

  //     if (Array.isArray(args.rows)) {
  //         this.selectedReg = args.rows.map(idx => {
  //            const item = this.regGridObj.getDataItem(idx);
  //            regCheckData.push(item);
  //            // return item.title || '';
  //         });
  //     }
  //     console.log('Selected reg users ', regCheckData);

  //     // console.log('args.rows',args.rows);
  //     for(var i=0;i<this.regRowData.length;i++){
  //         this.regRowData[i].check = false;
  //     }

  //     for(var i=0;i<this.regRowData.length;i++){
  //         for(var j=0;j<regCheckData.length;j++){
  //             if(this.regRowData[i].code == regCheckData[j].code){
  //                 this.regRowData[i].check = true;
  //             }
  //         }
  //     }
  // }

  /** Dispatched event of a Grid State Changed event */
  // regGridStateChanged(gridState: GridStateChange) {
  //   console.log('Client sample, Grid State changed:: ', gridState);
  // }

  // enrolGridReady(enrolGrid: any) {
  //     this.loader = true;

  //     this.enrolColumnDefinitions=[
  //         {
  //             id: 'code',
  //             name: 'EMPLOYEE CODE',
  //             field: 'code',
  //             sortable: true,
  //             type: FieldType.string,
  //             minWidth: 55,
  //             filterable: true,
  //             filter: { model: Filters.compoundInput }
  //         },
  //         {
  //             id: 'name',
  //             name: 'EMPLOYEE  NAME',
  //             field: 'name',
  //             sortable: true,
  //             minWidth: 55,
  //             type: FieldType.string,
  //             filterable: true,
  //             filter: { model: Filters.compoundInput }
  //         },
  //         {
  //             id: 'band',
  //             name: 'EMPLOYEE BAND',
  //             field: 'band',
  //             type: FieldType.string,
  //             minWidth: 55,
  //             sortable: true,
  //             filterable: true,
  //             filter: { model: Filters.compoundInput }
  //         },
  //         {
  //             id: 'reportingmanagercode',
  //             name:'REPORTING MANAGER CODE',
  //             field: 'reportingmanagercode',
  //             minWidth: 55,
  //             type: FieldType.string,
  //             sortable: true,
  //             filterable: true,
  //             filter: { model: Filters.compoundInput }
  //         },
  //         {
  //             id: 'reportingmanagername',
  //             name:'REPORTING MANAGER NAME',
  //             field: 'reportingmanagername',
  //             minWidth: 55,
  //             type: FieldType.string,
  //             sortable: true,
  //             filterable: true,
  //             filter: { model: Filters.compoundInput }
  //         },
  //         {
  //             id: 'department',
  //             name: 'DEPARTMENT',
  //             field: 'department',
  //             minWidth: 55,
  //             type: FieldType.string,
  //             sortable: true,
  //             filterable: true,
  //             filter: { model: Filters.compoundInput }
  //         },
  //         {
  //             id: 'role',
  //             name: 'ROLE/DESIGNATION',
  //             field: 'role',
  //             minWidth: 55,
  //             type: FieldType.string,
  //             sortable: true,
  //             filterable: true,
  //             filter: { model: Filters.compoundInput }
  //         },
  //     ];

  //     // this.angularGrid = angularGrid1;

  //     this.enrolGrid = enrolGrid;

  //     var cols = enrolGrid.slickGrid.getColumns();
  //     var enrolColoum = this.enrolColumnDefinitions;
  //     console.log("Enrol columns - ", cols);
  //     if(cols.length > 0){
  //       this.enrolColumnDefinitions = [];
  //       this.enrolColumnDefinitions.push(cols[0]);
  //       enrolColoum.forEach(data=>{
  //         this.enrolColumnDefinitions.push(data);
  //       })
  //     }

  //     enrolGrid.slickGrid.setColumns(this.enrolColumnDefinitions);
  //     this.enrolGridObj = enrolGrid && enrolGrid.slickGrid || {};

  //     this.loader = false;
  // }

  // public ngOnDestroy() {
  // this.angularGrid.destroy();
  // this.angularGrid1.destroy();
  // }

  // enrolHandleSelectedRowsChanged(e, args) {
  //     // console.log('args.rows1',args.rows);

  //     let enrolCheckData = [];

  //     if (Array.isArray(args.rows)) {
  //         this.selectedEnrol = args.rows.map(idx => {
  //            const item = this.enrolGridObj.getDataItem(idx);
  //            enrolCheckData.push(item);
  //            // return item.title || '';
  //         });
  //     }
  //     console.log('Selected enrol users ', enrolCheckData);

  //     for(var i=0;i<this.enrolRowData.length;i++){
  //         this.enrolRowData[i].check = false;
  //     }
  //     for(var i=0;i<this.enrolRowData.length;i++){
  //         for(var j=0;j<enrolCheckData.length;j++){
  //             if(this.enrolRowData[i].code == enrolCheckData[j].code){
  //                 this.enrolRowData[i].check = true;
  //             }
  //         }
  //     }
  // }

  // enrolGridStateChanged(gridState: GridStateChange) {
  //   console.log('Client sample, Grid State changed:: ', gridState);
  // }

  unRegGetDisable() {
    var count = 0;
    for (var i = 0; i < this.unRegRowData.length; i++) {
      if (this.unRegRowData[i].check == true) {
        count++;
      }
    }
    return count;
  }

  // regGetDisable(){
  //     var count = 0 ;
  //     for(var i=0;i<this.regRowData.length;i++){
  //         if(this.regRowData[i].check == true){
  //             count++;
  //         }
  //     }
  //     return count;
  // }

  // enrolGetDisable(){
  //     var count = 0 ;
  //     for(var i=0;i<this.enrolRowData.length;i++){
  //         if(this.enrolRowData[i].check == true){
  //             count++;
  //         }
  //     }
  //     return count;
  // }

  // public uploadUsers() {
  //     this.router.navigate(['upload-users'],{relativeTo:this.routes});
  // }

  AddUsers(id) {
    var data1 = {
      data: '',
      id: id,
      dropdownListDept: this.dropdownListDept,
      dropdownListRole: this.dropdownListRole,
      dropdownListBand: this.dropdownListBand
    }


    console.log('E Data', data1);
    this.passService.data = data1;
    this.router.navigate(['add-edit-users'], { relativeTo: this.routes });
  }

  EditUsers(id, event) {
    let data = this.unRegRowData[0];
    // for (var i = 0; i < this.unRegRowData.length; i++) {
    //   if (this.unRegRowData[i].check == true) {
    //     data = this.unRegRowData[i];
    //     break;
    //   }
    // }

    var data1 = {
      data: data,
      id: id,
      dropdownListDept: this.dropdownListDept,
      dropdownListRole: this.dropdownListRole,
      dropdownListBand: this.dropdownListBand
    }
    console.log('E Data', data1);
    this.passService.data = data1;
    this.router.navigate(['add-edit-users'], { relativeTo: this.routes });
  }
  back(){
    window.history.back();
  }
  // regAddUsers(id){
  //     var data1 ={
  //         data :'',
  //         id: id
  //     }
  //     console.log('E Data',data1);
  //     this.passService.data =data1;
  //     this.router.navigate(['add-edit-users'],{relativeTo:this.routes});
  // }

  // regEditUsers(id){
  //     var data;
  //     for(var i=0;i<this.regRowData.length;i++){
  //         if(this.regRowData[i].check == true){
  //             data = this.regRowData[i];
  //             break;
  //         }
  //     }

  //     var data1 ={
  //         data : data,
  //         id : id
  //     }
  //     console.log('E Data',data1);
  //     this.passService.data =data1;
  //     this.router.navigate(['add-edit-users'],{relativeTo:this.routes});
  // }

  // enrolAddUsers(id){
  //     var data1 ={
  //         data :'',
  //         id: id
  //     }
  //     console.log('E Data',data1);
  //     this.passService.data =data1;
  //     this.router.navigate(['add-edit-users'],{relativeTo:this.routes});
  // }

  // enrolEditUsers(id){
  //     var data;
  //     for(var i=0;i<this.enrolRowData.length;i++){
  //         if(this.enrolRowData[i].check == true){
  //             data = this.enrolRowData[i];
  //             break;
  //         }
  //     }
  //     var data1 ={
  //         data : data,
  //         id : id
  //     }
  //     console.log('E Data',data1);
  //     this.passService.data =data1;
  //     this.router.navigate(['add-edit-users'],{relativeTo:this.routes});
  // }

}
