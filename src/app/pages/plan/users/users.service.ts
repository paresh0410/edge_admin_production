import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, RequestOptions, Request } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AppConfig } from '../../../app.module';
import { HttpClient } from '@angular/common/http';
import { webApi } from '../../../service/webApi'
import { AuthenticationService } from '../../../service/authentication.service';

@Injectable()
export class UsersService {
  private _urlDrop: string = "/api/usermaster/getallDropdowns"
  private _url: string = "/api/usermaster/fetchAlluser"
  private _urlGet: string = "/api/induction/getInductedEmployees"
  private _urlSet: string = "/api/induction/setInductionAttendance"
  private _urlFilter: string = "/api/dashboard/induction_emp_filter" // Dev
  private _urlGetUnRegEmp: string = "/api/induction/getUnRegEmployees"

  private getuserListUrl: string = webApi.domain + webApi.url.getalluser;

  request: Request;

  constructor(@Inject('APP_CONFIG_TOKEN') private config: AppConfig, private _http: Http, private http1: HttpClient,
    private authenticationService: AuthenticationService) {

  }

  getuserlist(data) {
    return new Promise(resolve => {
      this.http1.post(this.getuserListUrl, data)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  getUnRegEmp() {
    let url: any = `${this.config.FINAL_URL}` + this._urlGetUnRegEmp;
    // let url:any = `${this.config.DEV_URL}`+this._urlFilter;
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    //let body = JSON.stringify(user);
    return this._http.post(url, options).map((res: Response) => res.json());
  }

  getUsers(user) {
    let url: any = `${this.config.FINAL_URL}` + this._urlFilter;
    // let url:any = `${this.config.DEV_URL}`+this._urlFilter;
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    //let body = JSON.stringify(user);
    return this._http.post(url, user, options).map((res: Response) => res.json());
  }


  getUserdropData() {
    let url: any = `${this.config.FINAL_URL}` + this._urlDrop;
    // return this._http.post(url,'')
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);
    return new Promise(resolve => {
      this.http1.post(url, '')
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  //@LoadingIndicator()
  getUserTableData() {
    let url: any = `${this.config.FINAL_URL}` + this._url;
    // return this._http.post(url,'')
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);
    return new Promise(resolve => {
      this.http1.post(url, '')
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  updateUser(user) {
    let url: any = `${this.config.FINAL_URL}` + this._urlSet;
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    //let body = JSON.stringify(user);
    return this._http.post(url, user, options).map((res: Response) => res.json());
  }

  getInductedData() {
    let url: any = `${this.config.FINAL_URL}` + this._urlGet;
    return this._http.post(url, '')
      .map((response: Response) => response.json())
      .catch(this._errorHandler);
  }

  _errorHandler(error: Response) {
    console.error(error);
    return Observable.throw(error || "Server Error")
  }

  public data: any;

}
