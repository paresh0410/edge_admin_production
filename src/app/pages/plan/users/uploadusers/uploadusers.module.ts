import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

import { NgxEchartsModule } from 'ngx-echarts';

import { ThemeModule } from '../../../../@theme/theme.module';

import { UploadusersComponent } from './uploadusers.component';
import { UploadUsersService } from './uploadusers.service'
import { TranslateModule } from '@ngx-translate/core';
import { JsonToXlsxService } from './json-to-xlsx-service';
import { XlsxToJsonService } from './xlsx-to-json-service';
import { TabsModule } from 'ngx-tabs';

import { AngularSlickgridModule } from 'angular-slickgrid';

@NgModule({
  imports: [
    ThemeModule,
    NgxEchartsModule,
    TabsModule,
    AngularSlickgridModule.forRoot(),
    TranslateModule.forRoot()
  ],
  declarations: [
    UploadusersComponent,
  ],
  providers: [
    UploadUsersService,
    JsonToXlsxService,
    XlsxToJsonService
  ],
  schemas: [ 
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA 
  ]
})
export class UploadUsersModule { }
