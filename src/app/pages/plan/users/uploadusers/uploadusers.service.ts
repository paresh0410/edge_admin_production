import {Injectable, Inject} from '@angular/core';
import {Http, Response, Headers, RequestOptions, Request} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppConfig} from '../../../../app.module';

@Injectable()
export class UploadUsersService {

	 private _urlUpload:string = "/api/usermaster/insertuser" // Dev	
   private _url:string = "/api/induction/getInductionEmployees";

  constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private _http: Http){

  }

  getUserTableData(){
    let url:any = `${this.config.FINAL_URL}`+this._url;
    return this._http.post(url,'')
      .map((response:Response) => response.json())
      .catch(this._errorHandler);
  }

  uploadUsers(user) {
    let url:any = `${this.config.FINAL_URL}`+this._urlUpload;
    // let url:any = `${this.config.DEV_URL}`+this._urlFilter;
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    //let body = JSON.stringify(user);
    return this._http.post(url, user, options ).map((res: Response) => res.json());
  }

  _errorHandler(error: Response){
    console.error(error);
    return Observable.throw(error || "Server Error")
  }

  public data: any;
  
}
