import { Component, ViewContainerRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { PaginatePipe, PaginationControlsDirective, PaginationService } from 'ngx-pagination';
import { Router, NavigationStart, Routes, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UploadUsersService } from './uploadusers.service';
import {SlimLoadingBarService} from 'ng2-slim-loading-bar';
import {BaseChartDirective} from 'ng2-charts/ng2-charts';
import { FormGroup, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Angular2CsvModule } from 'angular2-csv';
import { PageSettingsModel } from '@syncfusion/ej2-ng-grids';
import { GridComponent, SortService, GroupService, ColumnMenuService, PageService, FilterService } from '@syncfusion/ej2-ng-grids';
import { ContextMenuItemModel, SortSettingsModel, ColumnMenuOpenEventArgs, FilterSettingsModel, ColumnMenuItemModel } from '@syncfusion/ej2-ng-grids';
// import { AgGridNg2 } from 'ag-grid-angular';
import { FileSelectDirective, FileDropDirective, FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { XlsxToJsonService } from './xlsx-to-json-service';
import { JsonToXlsxService } from './json-to-xlsx-service';

import { Column, GridOption, AngularGridInstance, FieldType, Filters, Formatters, GridStateChange, OperatorType, Statistic } from 'angular-slickgrid';
import { ToastrService } from 'ngx-toastr';
// import {ToasterModule, ToasterService, Toast} from 'angular2-toaster';

@Component({
  selector: 'ngx-uploadusers',
  templateUrl: './uploadusers.component.html',
  styleUrls: ['./uploadusers.component.scss'],
  encapsulation: ViewEncapsulation.None
})
 export class UploadusersComponent {

    columnDefinitionsPreview: Column[] = [];
    gridOptionsPreview: GridOption = {};
    datasetPreview: any[] = [];
    angularGridPreview: AngularGridInstance;
    gridObjPreview: any;

    columnDefinitionsResult: Column[] = [];
    gridOptionsResult: GridOption = {};
    datasetResult: any[] = [];
    angularGridResult: AngularGridInstance;
    gridObjResult: any;

    @ViewChild('fileUpload') fileUpload:any; 

    @ViewChild('tabsetUpload') tabSelect:any;

    public result: any;
    private xlsxToJsonService: XlsxToJsonService = new XlsxToJsonService();

    chosenValue:any;
    public data: Object[];

    private gridApi;
    private gridColumnApi;
    private rowData: any[];

    private columnDefs;
    private autoGroupColumnDef;
    private rowSelection;
    private rowGroupPanelShow;
    private pivotPanelShow;
    private paginationPageSize;
    private paginationNumberFormatter;
    private defaultColDef;
    private components;

    private gridApiRes;
    private gridColumnApiRes;
    private rowDataRes: any[];

    private columnDefsRes;
    private autoGroupColumnDefRes;
    private rowSelectionRes;
    private rowGroupPanelShowRes;
    private pivotPanelShowRes;
    private paginationPageSizeRes;
    private paginationNumberFormatterRes;
    private defaultColDefRes;
    private componentsRes;

    public grid: GridComponent;
    public pageSettings: PageSettingsModel;
    public filterSettings: FilterSettingsModel =  {type: 'Menu'};
    public columnMenuOpen (args: ColumnMenuOpenEventArgs) {
      for (let item of args.items) {
        if (item.text === 'Filter' && args.column.field === 'username') {
            (item as ColumnMenuItemModel).hide = true;
        } else {
            (item as ColumnMenuItemModel).hide = false;
        }
      }
    }
    
    // const URL = '/api/';
    uploadURL = 'https://evening-anchorage-3159.herokuapp.com/api/';

    userLoginData:any;

    public uploader:FileUploader = new FileUploader({url: this.uploadURL});
    public hasBaseDropZoneOver:boolean = false;
    public hasAnotherDropZoneOver:boolean = false;
    

    fileUrl:any;
    fileName:any = "You can drag and drop files here to add them.";
    fileIcon:any = "assets/img/app/profile/avatar4.png";

    tab1:any=true;
    tab2:any=false;
    tab3:any=false;

    usersAddedData:any = [];
    usersSkippedData:any = [];
    usersErrorsData:any = [];
    usersAddedCount:any = 0;
    usersSkippedCount:any = 0;
    usersErrorsCount:any = 0;

    enableUpload:any = false;

    userInductedData:any = [];

    finalData:any = [];
    validData:any = [];
    InvalidData:any = [];
    validCount:any;
    invalidCount:any;
    tabs:any;

    // usernamePattern:any = new RegExp("^$|^[A-Za-z0-9]+$");
    usernamePattern:any = new RegExp("^[A-Za-z_-][A-Za-z0-9_-]*$");
    // usernamePatternAlpha:any = new RegExp("/^[a-zA-Z\s]*$/");  // with blankspace
    usernamePatternAlpha:any = new RegExp("[a-zA-Z]{3,30}");  
    // usernamePatternNumbers:any = new RegExp("^[0-9]*$");
    usernamePatternNumbers:any = new RegExp("-?(0|[1-9]\d*)?");
    firstnamePattern:any = new RegExp("[a-zA-Z]{3,30}");
    // firstnamePattern:any = new RegExp("/^[a-zA-Z]*$/"); // without blankspace
    phone1Pattern:any = new RegExp("[6-9]\\d{9}");
    // phone1Pattern:any = new RegExp("[0-9]{0-10}");
    cohort1Pattern:any = new RegExp("^$|^[A-Za-z0-9]+$");
    dojPattern:any = new RegExp("");
    emailPattern:any = new RegExp("[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}");
    // emailPattern1:any = new RegExp("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$");
    // dateSubmitted:any = new RegExp('^(?:(?:10|12|0?[13578])/(?:3[01]|[12][0-9]|0?[1-9])/(?:1[8-9]\\d{2}|[2-9]\\d{3})|(?:11|0?[469])/(?:30|[12][0-9]|0?[1-9])/(?:1[8-9]\\d{2}|[2-9]\\d{3})|0?2/(?:2[0-8]|1[0-9]|0?[1-9])/(?:1[8-9]\\d{2}|[2-9]\\d{3})|0?2/29/[2468][048]00|0?2/29/[3579][26]00|0?2/29/[1][89][0][48]|0?2/29/[2-9][0-9][0][48]|0?2/29/1[89][2468][048]|0?2/29/[2-9][0-9][2468][048]|0?2/29/1[89][13579][26]|0?2/29/[2-9][0-9][13579][26])$')]];
    // dateFormatNew:any = new RegExp("(((0[1-9]|[12]\d|3[01])-(0[13578]|1[02])-((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)-(0[13456789]|1[012])-((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])-02-((19|[2-9]\d)\d{2}))|(29-02-((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))");
    dateFormatNew:any = new RegExp("^[0-9]{4}[\-][0-9]{2}[\-][0-9]{2}$");
    tab2Grid:any = false;
    tab3Grid:any = false;

    eData:any;
    uploadfinalData:any = {};
    resUpload:any;
    errorMsg:any;

    userInductedDataResult:any = [];

    fileReaded:any;
    lines = [];
    uploadedData:any; 
    exceltojson:any;

    resultSheets:any;

    loader :any;

    constructor(private _fb: FormBuilder,vcr: ViewContainerRef,protected service: UploadUsersService,
      protected exportService: JsonToXlsxService, private router:Router, 
      // private toasterService: ToasterService,
      private toastr: ToastrService) {

    }

  exportToExcel(){  
    this.exportService.exportAsExcelFile(this.InvalidData, 'uploadUsersErrorData');
  }    

  prepareGridResult(){
      this.columnDefinitionsResult = [
        {
          id: 'result', 
          name: 'STATUS', 
          field: 'result', 
          sortable: true, 
          minWidth: 170,
          maxWidth: 250,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          id: 'username',
          name: "ECN / USERNAME",
          field: "username",
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },  
        { id: 'password',
          name: "PASSWORD",
          field: "password",
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        // {
        //   name: "sysrole1",
        //   field: "sysrole1",
        //   width: 100,
        //   //hide: true,
        //   filterParams: { newRowsAction: "keep" }
        // },
        {
          id: 'cohort1',
          name: "COHORT1",
          field: "cohort1",
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "FIRST NAME",
          field: "firstname",
          id: 'firstname',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
          
        },
        {
          name: "LAST NAME",
          field: "lastname",
          id: 'lastname',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "EMAIL",
          field: "email",
          id: 'email',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "PHONE",
          field: "phone1",
          id: 'phone1',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "ATTENDANCE",
          field: "attendance",
          id: 'attendance',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "DOJ",
          field: "doj",
          id: 'doj',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "CITY/TOWN",
          field: "city",
          id: 'city',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "INSTITUTION",
          field: "institution",
          id: 'institution',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },

        {
          name: "AGE",
          field: "age",
          id: 'age',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "AOPC",
          field: "aopc",
          id: 'aopc',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "AOPN",
          field: "aopn",
          id: 'aopn',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "AOSP",
          field: "aops",
          id: 'aops',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "BAND",
          field: "band",
          id: 'band',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "BANK",
          field: "bank",
          id: 'bank',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "BANK AC",
          field: "bankac",
          id: 'bankac',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "BLOODGROUP",
          field: "bloodgroup",
          id: 'bloodgroup',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "BUSINESS",
          field: "business",
          id: 'business',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "CCC",
          field: "ccc",
          id: 'ccc',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "CCN",
          field: "ccn",
          id: 'ccn',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "CURRENT TENURE MONTH",
          field: "currenttenuremonth",
          id: 'currenttenuremonth',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "CURRENT TENURE YEAR ",
          field: "currenttenureyear",
          id: 'currenttenureyear',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "DEPARTMENT",
          field: "department",
          id: 'department',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "DOB",
          field: "dob",
          id: 'dob',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "DOL",
          field: "dol",
          id: 'dol',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "DOM",
          field: "dom",
          id: 'dom',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "DOR",
          field: "dor",
          id: 'dor',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "EMG CONTACT LANDLINE",
          field: "emergencycontactlandline",
          id: 'emergencycontactlandline',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "EMG CONTACT MOBILE",
          field: "emergencycontactmobile",
          id: 'emergencycontactmobile',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "EMG CONTACT NAME",
          field: "emergencycontactname",
          id: 'emergencycontactname',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "EMG CONTACT RELATION",
          field: "emergencycontactrelation",
          id: 'emergencycontactrelation',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "EMP CATEGORY",
          field: "empcategory",
          id: 'empcategory',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "EMP CLASSIFICATION",
          field: "empclassification",
          id: 'empclassification',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "EMPLOYEE STATUS",
          field: "employeestatus",
          id: 'employeestatus',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "EPS",
          field: "eps",
          id: 'eps',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "ESIC",
          field: "esic",
          id: 'esic',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "FATHER",
          field: "father",
          id: 'father',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "FULLNAME",
          field: "fullname",
          id: 'fullname',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "GENDER",
          field: "gender",
          id: 'gender',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "HIGHEST QUALIFICATION",
          field: "highestqualification",
          id: 'highestqualification',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "JOB CODE",
          field: "jobcode",
          id: 'jobcode',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "LEVEL",
          field: "level",
          id: 'level',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "LOCATION",
          field: "location",
          id: 'location',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "LOCATION CODE",
          field: "locationcode",
          id: 'locationcode',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "MARTIAL STATUS",
          field: "martialstatus",
          id: 'martialstatus',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "ORG CODE",
          field: "orgcode",
          id: 'orgcode',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "PAN",
          field: "pan",
          id: 'pan',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "PERMANENT ADD",
          field: "permanentadd",
          id: 'permanentadd',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "PERMANENT CITY",
          field: "permanentcity",
          id: 'permanentcity',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "PERMANENT CODE",
          field: "permanentpincode",
          id: 'permanentpincode',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "PERMANENT STATE",
          field: "permanentstate",
          id: 'permanentstate',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "PERSONAL EMAIL",
          field: "personalemail",
          id: 'personalemail',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "PERSONAL MOBILE",
          field: "personalmobile",
          id: 'personalmobile',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "PF NUMBER",
          field: "pfnumber",
          id: 'pfnumber',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "POSITION CODE",
          field: "positioncode",
          id: 'positioncode',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "PRESENT ADDRESS",
          field: "presentaddress",
          id: 'presentaddress',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "PRESENT CITY",
          field: "presentcity",
          id: 'presentcity',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "PRESENT PINCODE",
          field: "presentpincode",
          id: 'presentpincode',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "PRESENT STATE",
          field: "presentstate",
          id: 'presentstate',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "QUALIFICATION YEAR",
          field: "qualificationyear",
          id: 'qualificationyear',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "REPORTING MANAGER CODE",
          field: "reportingmanagercode",
          id: 'reportingmanagercode',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "REPORTING MANAGER NAME",
          field: "reportingmanagername",
          id: 'reportingmanagername',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "ROLE",
          field: "role",
          id: 'role',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "S DEPARTMENT",
          field: "sdepartment",
          id: 'sdepartment',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "SKIP LEVEL M CODE",
          field: "skiplevelmcode",
          id: 'skiplevelmcode',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "SKIP LEVEL M NAME",
          field: "skiplevelmname",
          id: 'skiplevelmname',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "SPOUSE NAME",
          field: "spousename",
          id: 'spousename',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "SS DEPARMENT",
          field: "ssdepartment",
          id: 'ssdepartment',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "STATE",
          field: "state",
          id: 'state',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },    
        {
          name: "TIER",
          field: "tier",
          id: 'tier',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "UAN",
          field: "uan",
          id: 'uan',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "ZONE",
          field: "zone",
          id: 'zone',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "RMEMAIL",
          field: "rmemail",
          id: 'rmemail',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "TRAINING VENUE",
          field: "trainingvenue",
          id: 'trainingvenue',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        }
      ];
      this.gridOptionsResult = {
        enableAutoResize: true,       // true by default
        // enableCellNavigation: true,
        // enableExcelCopyBuffer: true,
        // enableFiltering: true,
        rowSelectionOptions: {
          // True (Single Selection), False (Multiple Selections)
           selectActiveRow: false
        },
        // preselectedRows: [0, 2],
        // enableCheckboxSelector: true,
        enableRowSelection: true,
        // gridMenu: {
        //   // all titles optionally support translation keys, if you wish to use that feature then use the title properties finishing by 'Key'
        //   // example "customTitle" for a plain string OR "customTitleKey" to use a translation key
        //   customTitle: 'Menu',
        //   columnTitle: 'Columns',
        //   //iconCssClass: 'fa fa-ellipsis-v',
        //   hideForceFitButton: true,
        //   hideSyncResizeButton: true,
        //   hideToggleFilterCommand: false, // show/hide internal custom commands
        //   menuWidth:17,
        //   resizeOnShowHeaderRow: true,
        // },
      };
      // this.makeDataReadyPreview();

      // if(this.tab3Grid = true){
      //   this.loader = false;
      // }
      // this.loader = true;

      setTimeout(()=>{
        this.loader = false;
        this.tab3Grid = true;
      },1000);
    }


    makeDataReadyResult(){
      if(localStorage.getItem('LoginResData')){
          this.userLoginData = JSON.parse(localStorage.getItem('LoginResData')); 
        }
        console.log('user Login Data :-',this.userLoginData);

        this.uploadfinalData = {
          fileData:this.rowData,
          username:this.userLoginData.username,
        }  
        console.log('upload final Data:-',this.uploadfinalData);
        //console.log('Res Upload :- ', this.resUpload);
        
        this.loader = true;
        this.service.uploadUsers(this.uploadfinalData)
        .subscribe(resUserData => {

                  // this.loader =false;
          this.resUpload = resUserData;
          // this.datasetResult = resUserData;
          // this.tab3Grid = true;
          // this.source.load(this.userInductedData);
          // this.pageSettings = { pageSize: 6 };
          console.log('Res Upload :- ', this.resUpload);
          console.log('File Uploaded!');

          this.usersErrorsData = [];
          this.usersAddedData = [];
          
          for(let i=0; i<this.resUpload.length; i++){
            if(this.resUpload[i].flag == 0){
              this.usersErrorsData.push(this.resUpload[i]);
            }
            if(this.resUpload[i].flag == 1){
              this.usersAddedData.push(this.resUpload[i]);
            }
          }

          this.usersErrorsCount = this.usersErrorsData.length;
          this.usersAddedCount = this.usersAddedData.length;

          for(let i=0; i<this.usersErrorsData.length; i++){
            this.InvalidData.push(this.usersErrorsData[i]);
          }

          console.log('Final invalid data ',this.InvalidData);

          this.usersErrorsCount = this.InvalidData.length;

          for(let i=0; i<this.resUpload.length; i++){
            this.resUpload[i].id = i;
          }
          // this.datasetResult=[];

          this.datasetResult = this.resUpload;
          console.log('datasetResult', this.datasetResult);
          // this.tab3Grid = true;
        
          this.prepareGridResult();

       },
        resUserError => {
          this.loader =false;
          this.errorMsg = resUserError;

       });
    }

  angularGridReadyResult(angularGrid: any) {
    this.angularGridResult = angularGrid;
    this.gridObjResult = angularGrid && angularGrid.slickGrid || {};
  }

  presentToast(type, body) {
    if(type === 'success'){
      this.toastr.success(body, 'Success', {
        closeButton: false
       });
    } else if(type === 'error'){
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else{
      this.toastr.warning(body, 'Warning', {
        closeButton: false
        })
    }
  }

    readUrl(event:any) {

      var validExts = new Array(".xlsx", ".xls");
      var fileExt = event.target.files[0].name;
      fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
      if (validExts.indexOf(fileExt) < 0) {
        // alert("Invalid file selected, valid files are of " +
        //          validExts.toString() + " types.");
        // var toast : Toast = {
        //     type: 'error',
        //     title: "Invalid file selected!",
        //     body: "Valid files are of " + validExts.toString() + " types.",
        //     showCloseButton: true,
        //     // tapToDismiss: false, 
        //     timeout: 2000
        //     // onHideCallback: () => {
        //     //     this.router.navigate(['/pages/plan/users']);
        //     // }
        // };
        // this.toasterService.pop(toast);
        this.presentToast('warning', 'Valid file types are ' + validExts.toString());
        // setTimeout(data => {
        //   this.router.navigate(['/pages/users']);
        // },1000);
        // return false;
        this.cancel();
      }
      else {
          // return true;
          if (event.target.files && event.target.files[0]) {
          this.fileName = event.target.files[0].name;
          //read file from input
          this.fileReaded = event.target.files[0];

          if(this.fileReaded != '' || this.fileReaded != null || this.fileReaded != undefined){
            this.enableUpload = true;
          }

          let file = event.target.files[0];
          this.xlsxToJsonService.processFileToJson({}, file).subscribe(data => {
            
            this.resultSheets = Object.getOwnPropertyNames(data['sheets']);
            console.log('File Property Names ',this.resultSheets);
            let sheetName = this.resultSheets[0];
            this.result = data['sheets'][sheetName];

            if(this.result.length > 0){
              this.uploadedData = this.result;
            }
            // this.result = JSON.stringify(data['sheets'].Sheet1);
          })
          var reader = new FileReader();
          reader.onload = (event: ProgressEvent) => {
            this.fileUrl = (<FileReader>event.target).result;
            // event.setAttribute("data-title", this.fileName);
            // console.log(this.fileUrl);
          }
          reader.readAsDataURL(event.target.files[0]);
        }
      }  

      // if(event.target.files[0].name.split('.')[event.target.files[0].name.split('.').length-1] === 'xlsx'){
      //     let file = event.target.files[0];
      // } else if(event.target.files[0].name.split('.')[event.target.files[0].name.split('.').length-1] === 'xls'){
      //     let file = event.target.files[0];
      // }else{
      //   alert("Invalid file selected, valid files are of " +
      //          validExts.toString() + " types.");
      // }   
    }

    cancel(){
      // this.fileUpload.nativeElement.files = [];
      console.log(this.fileUpload.nativeElement.files);
      this.fileUpload.nativeElement.value = "";
      console.log(this.fileUpload.nativeElement.files);
      this.fileName = 'You can drag and drop files here to add them.';
      this.enableUpload = false;
    }

    clearPreviewData(){
      this.datasetPreview = [];
    }

    clearResultData(){
      this.datasetResult = [];
    }

    prepareGridPreview(){
      this.columnDefinitionsPreview = [
        {
          id: 'valid', 
          name: 'STATUS', 
          field: 'valid', 
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          id: 'username',
          name: "ECN / USERNAME",
          field: "username",
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },  
        { id: 'password',
          name: "PASSWORD",
          field: "password",
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        // {
        //   name: "sysrole1",
        //   field: "sysrole1",
        //   width: 100,
        //   //hide: true,
        //   filterParams: { newRowsAction: "keep" }
        // },
        {
          id: 'cohort1',
          name: "COHORT1",
          field: "cohort1",
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "FIRST NAME",
          field: "firstname",
          id: 'firstname',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
          
        },
        {
          name: "LAST NAME",
          field: "lastname",
          id: 'lastname',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "EMAIL",
          field: "email",
          id: 'email',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "PHONE",
          field: "phone1",
          id: 'phone1',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "ATTENDANCE",
          field: "attendance",
          id: 'attendance',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "DOJ",
          field: "doj",
          id: 'doj',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "CITY/TOWN",
          field: "city",
          id: 'city',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "INSTITUTION",
          field: "institution",
          id: 'institution',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },

        {
          name: "AGE",
          field: "age",
          id: 'age',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "AOPC",
          field: "aopc",
          id: 'aopc',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "AOPN",
          field: "aopn",
          id: 'aopn',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "AOSP",
          field: "aops",
          id: 'aops',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "BAND",
          field: "band",
          id: 'band',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "BANK",
          field: "bank",
          id: 'bank',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "BANK AC",
          field: "bankac",
          id: 'bankac',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "BLOODGROUP",
          field: "bloodgroup",
          id: 'bloodgroup',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "BUSINESS",
          field: "business",
          id: 'business',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "CCC",
          field: "ccc",
          id: 'ccc',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "CCN",
          field: "ccn",
          id: 'ccn',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "CURRENT TENURE MONTH",
          field: "currenttenuremonth",
          id: 'currenttenuremonth',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "CURRENT TENURE YEAR ",
          field: "currenttenureyear",
          id: 'currenttenureyear',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "DEPARTMENT",
          field: "department",
          id: 'department',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "DOB",
          field: "dob",
          id: 'dob',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "DOL",
          field: "dol",
          id: 'dol',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "DOM",
          field: "dom",
          id: 'dom',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "DOR",
          field: "dor",
          id: 'dor',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "EMG CONTACT LANDLINE",
          field: "emergencycontactlandline",
          id: 'emergencycontactlandline',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "EMG CONTACT MOBILE",
          field: "emergencycontactmobile",
          id: 'emergencycontactmobile',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "EMG CONTACT NAME",
          field: "emergencycontactname",
          id: 'emergencycontactname',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "EMG CONTACT RELATION",
          field: "emergencycontactrelation",
          id: 'emergencycontactrelation',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "EMP CATEGORY",
          field: "empcategory",
          id: 'empcategory',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "EMP CLASSIFICATION",
          field: "empclassification",
          id: 'empclassification',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "EMPLOYEE STATUS",
          field: "employeestatus",
          id: 'employeestatus',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "EPS",
          field: "eps",
          id: 'eps',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "ESIC",
          field: "esic",
          id: 'esic',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "FATHER",
          field: "father",
          id: 'father',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "FULLNAME",
          field: "fullname",
          id: 'fullname',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "GENDER",
          field: "gender",
          id: 'gender',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "HIGHEST QUALIFICATION",
          field: "highestqualification",
          id: 'highestqualification',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "JOB CODE",
          field: "jobcode",
          id: 'jobcode',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "LEVEL",
          field: "level",
          id: 'level',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "LOCATION",
          field: "location",
          id: 'location',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "LOCATION CODE",
          field: "locationcode",
          id: 'locationcode',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "MARTIAL STATUS",
          field: "martialstatus",
          id: 'martialstatus',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "ORG CODE",
          field: "orgcode",
          id: 'orgcode',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "PAN",
          field: "pan",
          id: 'pan',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "PERMANENT ADD",
          field: "permanentadd",
          id: 'permanentadd',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "PERMANENT CITY",
          field: "permanentcity",
          id: 'permanentcity',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "PERMANENT CODE",
          field: "permanentpincode",
          id: 'permanentpincode',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "PERMANENT STATE",
          field: "permanentstate",
          id: 'permanentstate',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "PERSONAL EMAIL",
          field: "personalemail",
          id: 'personalemail',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "PERSONAL MOBILE",
          field: "personalmobile",
          id: 'personalmobile',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "PF NUMBER",
          field: "pfnumber",
          id: 'pfnumber',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "POSITION CODE",
          field: "positioncode",
          id: 'positioncode',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "PRESENT ADDRESS",
          field: "presentaddress",
          id: 'presentaddress',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "PRESENT CITY",
          field: "presentcity",
          id: 'presentcity',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "PRESENT PINCODE",
          field: "presentpincode",
          id: 'presentpincode',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "PRESENT STATE",
          field: "presentstate",
          id: 'presentstate',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "QUALIFICATION YEAR",
          field: "qualificationyear",
          id: 'qualificationyear',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "REPORTING MANAGER CODE",
          field: "reportingmanagercode",
          id: 'reportingmanagercode',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "REPORTING MANAGER NAME",
          field: "reportingmanagername",
          id: 'reportingmanagername',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "ROLE",
          field: "role",
          id: 'role',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "S DEPARTMENT",
          field: "sdepartment",
          id: 'sdepartment',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "SKIP LEVEL M CODE",
          field: "skiplevelmcode",
          id: 'skiplevelmcode',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "SKIP LEVEL M NAME",
          field: "skiplevelmname",
          id: 'skiplevelmname',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "SPOUSE NAME",
          field: "spousename",
          id: 'spousename',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "SS DEPARMENT",
          field: "ssdepartment",
          id: 'ssdepartment',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "STATE",
          field: "state",
          id: 'state',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },    
        {
          name: "TIER",
          field: "tier",
          id: 'tier',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "UAN",
          field: "uan",
          id: 'uan',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "ZONE",
          field: "zone",
          id: 'zone',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "RMEMAIL",
          field: "rmemail",
          id: 'rmemail',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        },
        {
          name: "TRAINING VENUE",
          field: "trainingvenue",
          id: 'trainingvenue',
          sortable: true, 
          minWidth: 85,
          maxWidth: 170,
          type: FieldType.string, 
          filterable: true, 
          filter: { model: Filters.compoundInput }
        }
      ];
      this.gridOptionsPreview = {
        enableAutoResize: true,       // true by default
        // enableCellNavigation: true,
        // enableExcelCopyBuffer: true,
        // enableFiltering: true,
        rowSelectionOptions: {
          // True (Single Selection), False (Multiple Selections)
           selectActiveRow: false
        },
        // preselectedRows: [0, 2],
        // enableCheckboxSelector: true,
        enableRowSelection: true,
        // gridMenu: {
        //   // all titles optionally support translation keys, if you wish to use that feature then use the title properties finishing by 'Key'
        //   // example "customTitle" for a plain string OR "customTitleKey" to use a translation key
        //   customTitle: 'Menu',
        //   columnTitle: 'Columns',
        //   //iconCssClass: 'fa fa-ellipsis-v',
        //   hideForceFitButton: true,
        //   hideSyncResizeButton: true,
        //   hideToggleFilterCommand: false, // show/hide internal custom commands
        //   menuWidth:17,
        //   resizeOnShowHeaderRow: true,
        // },
      };
      // this.makeDataReadyPreview();
      this.loader = true;
      setTimeout(()=>{
        this.loader = false;
        this.tab2Grid = true;
      },1000);
    }

    makeDataReadyPreview(){
      for(let i=0; i<this.uploadedData.length; i++){

          this.uploadedData[i].valid = 'Valid';
          
          if(this.uploadedData[i].username == '' || this.uploadedData[i].username == null  || this.uploadedData[i].username == undefined){
            this.uploadedData[i].valid = 'Invalid';
            // this.uploadedData[i].username = 'Required!';
          }else if(this.usernamePattern.test(this.uploadedData[i].username) || this.usernamePatternAlpha.test(this.uploadedData[i].username) || this.usernamePatternNumbers.test(this.uploadedData[i].username) && this.uploadedData[i].valid == 'Valid'){
            this.uploadedData[i].valid = 'Valid';
          }else{
            this.uploadedData[i].valid = 'Invalid';
          }
          if(this.uploadedData[i].firstname == '' || this.uploadedData[i].firstname == null  || this.uploadedData[i].firstname == undefined){
            this.uploadedData[i].valid = 'Invalid';
            // this.uploadedData[i].firstname = 'Required!';
          }else if(this.firstnamePattern.test(this.uploadedData[i].firstname) && this.uploadedData[i].valid == 'Valid'){
            this.uploadedData[i].valid = 'Valid';
          }else{
            this.uploadedData[i].valid = 'Invalid';
          }
          if(this.uploadedData[i].doj == '' || this.uploadedData[i].doj == null  || this.uploadedData[i].doj == undefined){
            this.uploadedData[i].valid = 'Invalid';
            // this.uploadedData[i].lastname = 'Required!';
          }else if(this.dateFormatNew.test(this.uploadedData[i].doj) && this.uploadedData[i].valid == 'Valid'){
            this.uploadedData[i].valid = 'Valid';
          }else{
            this.uploadedData[i].valid = 'Invalid';
          }
          if(this.uploadedData[i].email == '' || this.uploadedData[i].email == null  || this.uploadedData[i].email == undefined){
            this.uploadedData[i].valid = 'Invalid';
            // this.uploadedData[i].email = 'Required!';
          }else if(this.emailPattern.test(this.uploadedData[i].email) && this.uploadedData[i].valid == 'Valid'){
            this.uploadedData[i].valid = 'Valid';
          }else{
            this.uploadedData[i].valid = 'Invalid';
          }
          if(this.uploadedData[i].phone1 == '' || this.uploadedData[i].phone1 == null  || this.uploadedData[i].phone1 == undefined){
            this.uploadedData[i].valid = 'Invalid';
            // this.uploadedData[i].phone1 = 'Required!';
          }else if(this.phone1Pattern.test(this.uploadedData[i].phone1) && this.uploadedData[i].valid == 'Valid'){
            this.uploadedData[i].valid = 'Valid';
          }else{
            this.uploadedData[i].valid = 'Invalid';
          }
          // if(this.uploadedData[i].sysrole1 == '' || this.uploadedData[i].sysrole1 == null  || this.uploadedData[i].sysrole1 == undefined){
          //   this.uploadedData[i].valid = 'Invalid';
          //   // this.uploadedData[i].sysrole1 = 'Required!';
          // }
          if(this.uploadedData[i].cohort1 == '' || this.uploadedData[i].cohort1 == null  || this.uploadedData[i].cohort1 == undefined){
            this.uploadedData[i].valid = 'Invalid';
            // this.uploadedData[i].cohort1 = 'Required!';
          }else if(this.cohort1Pattern.test(this.uploadedData[i].cohort1) && this.uploadedData[i].valid == 'Valid'){
            this.uploadedData[i].valid = 'Valid';
          }else{
            this.uploadedData[i].valid = 'Invalid';
          }
        }
          this.validData=[];
          this.InvalidData=[];
        for(let i=0; i<this.uploadedData.length; i++){
          if(this.uploadedData[i].valid == 'Valid'){
            this.validData.push(this.uploadedData[i]);
          }else{
            this.InvalidData.push(this.uploadedData[i]);
          }
        }

        for(let i=0; i<this.uploadedData.length; i++){
          this.uploadedData[i].id = i;
        }  

        console.log('Valid row Data',this.validData);
        this.validCount = this.validData.length;
        console.log('Invalid row Data',this.InvalidData);
        this.invalidCount = this.InvalidData.length;

        this.datasetPreview = this.uploadedData;

        this.rowData = this.validData;
        // this.rowData = this.result;
        console.log('Uploaded Data',this.datasetPreview);
        this.prepareGridPreview();
        // this.angularGridReadyPreview();
    }

  angularGridReadyPreview(angularGrid: any) {
    this.angularGridPreview = angularGrid;
    this.gridObjPreview = angularGrid && angularGrid.slickGrid || {};
  }

    excelToJson(data){
      let user1 = data;
        
        // if(user1.name.split('.')[user1.name.split('.').length-1] === 'xlsx'){
        //     // this.exceltojson = xls_json;
        //     this.exceltojson = xlsxtojson;
        // } else {
        //     this.exceltojson = xlstojson;
        //     // this.exceltojson = xlsx_json;
        // }
        this.exceltojson({
          input: user1,
          output: null, //since we don't need output.json
          lowerCaseHeaders:true
        }, function(err, result) {
          if(err) {
            console.error(err);
          }else {
            console.log(result);
          }
        });
    }

    public fileOverBase(e:any):void {
      this.hasBaseDropZoneOver = e;
    }

    

    postfile(){

        this.tabs = this.tabSelect.tabs._results;

        if(this.tabs[0].active){
          this.tabs[0].active = false;
          this.tabs[1].active = true;
          this.tabs[1].disabled = false;
          this.tab1 = false;
          this.tab2 = true;
          this.tab3 = false;
          
          this.makeDataReadyPreview();
          // this.prepareGridPreview();
          // this.makeDataReadyResult();
        }

    }

    clearData(){
      this.rowData = [];
    }

    postfileTab(event){

        this.tabs = this.tabSelect.tabs._results;

        if(event == 0){
          this.tabs[0].active = true;
          this.tabs[1].active = false;
          this.tabs[1].disabled = true;
          this.tabs[2].active = false;
          this.tabs[2].disabled = true;
          this.tab1 = true;
          this.tab2 = false;
          this.tab3 = false;
          this.clearData();
        }
        
        if(event == 1){
          this.tabs[0].active = false;
          this.tabs[1].active = true;
          this.tabs[1].disabled = false;
          this.tabs[2].active = false;
          this.tabs[2].disabled = true;
          this.tab1 = false;
          this.tab2 = true;
          this.tab3 = false;
          // this.clearData();
        }

        if(event == 2){
          this.tabs[0].active = false;
          this.tabs[1].active = false;
          this.tabs[1].disabled = false;
          this.tabs[2].active = true;
          this.tabs[2].disabled = false;
          this.tab1 = false;
          this.tab2 = false;
          this.tab3 = true;
          // this.clearData();
        }
        // if(this.tab1 == true){
        //   this.tab1 = false;
        // }else{
        //   this.tab1 = true;
        // }
        // if(this.tab2 == false){
        //   this.tab2 = true;
        // }else{
        //   this.tab2 = false;
        // }
        this.cancel(); 
    }
    
    getSelectedRows() {
    
    }

    // enableSubmit:any = false;

    getRowDataLength(){
      if(this.rowData.length == 0){
        // var toast : Toast = {
        //     type: 'error',
        //     title: "File cannot be uploaded!",
        //     body: "There is no valid data to upload",
        //     showCloseButton: true,
        //     // tapToDismiss: false, 
        //     timeout: 2000
        //     // onHideCallback: () => {
        //     //     this.router.navigate(['/pages/plan/users']);
        //     // }
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      }else{
        // this.enableSubmit = true;
        this.subfile();
      }
    }

    fileDetails:any={
      filename:'Users Data',
      colSep:''
    }   

    subfile(){
      // this.tab3 = true;
      // this.tab1 = false;
      // this.tab2 = false;
      this.tabs = this.tabSelect.tabs._results;

        if(this.tabs[1].active){
          this.tabs[1].active = false;
          this.tabs[1].disabled = true;
          this.tabs[2].active = true;
          this.tabs[2].disabled = false;
          this.tab1 = false;
          this.tab2 = false;
          this.tab3 = true;

          this.makeDataReadyResult();
        }   
    }

    back(){
    	this.router.navigate(['/pages/plan/users']);
    }

    backTab1(){
      // this.router.navigate(['/pages/users/induction']);
      this.tabs = this.tabSelect.tabs._results;

        if(this.tabs[1].active){
          this.tabs[1].active = false;
          this.tabs[1].disabled = true;
          this.tabs[0].active = true;
          this.tabs[2].active = false;
          this.tabs[2].disabled = true;
          this.tab1 = true;
          this.tab2 = false;
          this.tab3 = false;
        }
      this.cancel();  
    }

    backTab2(){
      // this.router.navigate(['/pages/users/induction']);
      this.tabs = this.tabSelect.tabs._results;

        if(this.tabs[2].active){
          this.tabs[2].active = false;
          this.tabs[2].disabled = true;
          this.tabs[0].active = true;
          this.tabs[1].active = false;
          this.tabs[1].disabled = true;
          this.tab1 = true;
          this.tab2 = false;
          this.tab3 = false;
        }
      this.cancel(); 
      //this.clearPreviewData();
      this.clearResultData(); 
    }
    
  }


