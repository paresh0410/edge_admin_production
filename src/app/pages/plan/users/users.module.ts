import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { NgxEchartsModule } from 'ngx-echarts';
import { ThemeModule } from '../../../@theme/theme.module';
import { TabsModule } from 'ngx-tabs';
//import { slikgridDemo } from './../../..../slikgridDemo.component';
//import { slikgridDemoService } from './slikgridDemo.service';

import { Users } from './users.component';
import { UsersService } from './users.service';

import { TranslateModule } from '@ngx-translate/core';
import { AngularSlickgridModule } from 'angular-slickgrid';
import { UploadusersComponent } from './uploadusers/uploadusers.component';
import { AddeditusersComponent } from './addeditusers/addeditusers.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ComponentModule } from '../../../component/component.module';
//import {AddEditUserService} from './addeditusers/addeditusers.service';
// const PLAN_COMPONENTS = [
//   slikgridDemo
// ];

@NgModule({
  imports: [
    ThemeModule,
    NgxEchartsModule,
    TabsModule,
    NgxPaginationModule,
    NgxDatatableModule,
    // UsersModule,
    AngularSlickgridModule.forRoot(),
    TranslateModule.forRoot(),
    ComponentModule
  ],
  declarations: [
    Users,
    //UploadusersComponent,
    //AddeditusersComponent,
  ],
  providers: [
    UsersService,
    //AddEditUserService
    //slikgridDemoService,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class UsersModule { }
