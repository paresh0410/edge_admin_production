import {Component, OnDestroy, ViewEncapsulation } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators/takeWhile' ;
import { Router,ActivatedRoute} from '@angular/router';
import { AppService } from '../../app.service';
import { SuubHeader } from '../components/models/subheader.model';

interface CardSettings {
  title: string;
  iconClass: string;
  type: string;
}

@Component({
  selector: 'ngx-plan',
  styleUrls: ['./plan.component.scss'],
  templateUrl: './plan.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class PlanComponent implements OnDestroy {
  private alive = true;
  header: SuubHeader  = {
    title: 'Settings',
    showBreadcrumb:true,
    breadCrumbList:[]
  };


  lightCard: CardSettings = {
    title: 'Light',
    iconClass: 'nb-lightbulb',
    type: 'primary',
  };
  rollerShadesCard: CardSettings = {
    title: 'Roller Shades',
    iconClass: 'nb-roller-shades',
    type: 'success',
  };
  wirelessAudioCard: CardSettings = {
    title: 'Wireless Audio',
    iconClass: 'nb-audio',
    type: 'info',
  };
  coffeeMakerCard: CardSettings = {
    title: 'Coffee Maker',
    iconClass: 'nb-coffee-maker',
    type: 'warning',
  };

  statusCards: string;

  commonStatusCardsSet: CardSettings[] = [
    this.lightCard,
    this.rollerShadesCard,
    this.wirelessAudioCard,
    this.coffeeMakerCard,
  ];

  statusCardsByThemes: {
    default: CardSettings[];
    cosmic: CardSettings[];
    corporate: CardSettings[];
  } = {
    default: this.commonStatusCardsSet,
    cosmic: this.commonStatusCardsSet,
    corporate: [
      {
        ...this.lightCard,
        type: 'warning',
      },
      {
        ...this.rollerShadesCard,
        type: 'primary',
      },
      {
        ...this.wirelessAudioCard,
        type: 'danger',
      },
      {
        ...this.coffeeMakerCard,
        type: 'secondary',
      },
    ],
  };
  showdata:any=[];
  employees:boolean=false;
  users:boolean=false;
  courses:boolean=false;
  evaluation:boolean=false;
  competancy:boolean=false;
  courseBundle:boolean=false;
  roleManagement:boolean=false;
  planData:any =[];
  theme: any = {
    menuIcon: "fa fa-themeisle",
    menuName: "Theme",
    menuDescription: "Change theme"
  };
  discount: any = {
    menuIcon: "fas fa-percent",
    menuName: "Discount",
    menuDescription: "Discount"
  };
  pricing: any = {
    menuIcon: "fas fa-rupee-sign",
    menuName: "Pricing",
    menuDescription: "Pricing"
  };

  constructor(private themeService: NbThemeService,public router:Router,public routes:ActivatedRoute,private AppService:AppService) {
    this.themeService.getJsTheme()
      .pipe(takeWhile(() => this.alive))
      .subscribe(theme => {
        this.statusCards = this.statusCardsByThemes[theme.name];
    });
    // this.data ='ion-ios-cloud cloud';
       this.showdata =this.AppService.getmenus();
      // if(this.showdata)
      // {
      //   for(let i=0;i<this.showdata.length;i++)
      //   {
      //     if(this.showdata[i] == 'MEMP')
      //     {
      //       this.employees =true;
      //     }
      //      if(this.showdata[i] == 'MU')
      //     {
      //       this.users =true;
      //     }
      //      if(this.showdata[i] == 'rec')
      //     {
      //       this.courses =true;
      //     }
      //      if(this.showdata[i] == 'training')
      //     {
      //       this.evaluation =true;
      //     }
      //        if(this.showdata[i] == 'rules')
      //     {
      //       this.competancy =true;
      //     }
      //      if(this.showdata[i] == 'MCB')
      //     {
      //       this.courseBundle =true;
      //     }
      //      if(this.showdata[i] == 'training')
      //     {
      //       this.roleManagement =true;
      //     }
      //   }
      // }
      if(this.showdata)
      {
        for (let i = 0; i < this.showdata.length; i++)
        {
          if (Number(this.showdata[i].parentMenuId) === 1)
          {
            this.planData.push(this.showdata[i]);
          }
        }
      }


  }


  ngOnDestroy() {
    this.alive = false;
  }

  gotopages(item){
    console.log('item', item);
    if(item.menuId == 105 || item.menuId == 162){
      this.router.navigate([item.menuRoute, item.menuId], { relativeTo: this.routes });
    }else {
      this.router.navigate([item.menuRoute],{relativeTo: this.routes});
    }

  }

  gotoTrainer(){
    this.router.navigate(['trainer'],{relativeTo:this.routes});
  }

  gotousers(){
    this.router.navigate(['users'],{relativeTo:this.routes});
  }

    gotoemployees(){
    this.router.navigate(['employees'],{relativeTo:this.routes});
  }

  gotocourses(){
    this.router.navigate(['courses'],{relativeTo:this.routes});
  }

  gotocoursebundle(){
    this.router.navigate(['courseBundle'],{relativeTo:this.routes});
  }

  gotoroleManagement(){
    this.router.navigate(['roleManagement'],{relativeTo:this.routes});
  }

  gotonotificationtemplate(){
    this.router.navigate(['notification-templates'],{relativeTo:this.routes});
  }

  gototheme() {
    this.router.navigate(['theme'],{relativeTo:this.routes});
  }

  gotodiscount() {
    this.router.navigate(['discount'],{relativeTo:this.routes});
  }

  gotopricing() {
    this.router.navigate(['pricing'],{relativeTo:this.routes});
  }
}
