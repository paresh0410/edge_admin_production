import { Host, ChangeDetectionStrategy, ChangeDetectorRef, Component, ViewEncapsulation, Directive, forwardRef, Attribute, OnChanges, SimpleChanges, Input, ViewChild, ViewContainerRef, OnInit, AfterViewInit } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { CourseBundle } from '../courseBundle';
import { CourseBundleService } from '../courseBundle.service';
import { enrolService } from '../enrolment/enrolment.service';
import { AddEditCourseContentService } from '../../courses/addEditCourseContent/addEditCourseContent.service';
// import { AddEditCourseContentService } from 'pages/plan/courses/addEditCourseContent/addEditCourseContent.service';
import { FormGroup, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NgbCalendar, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { HttpClient } from '@angular/common/http';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { NgxSpinnerService } from 'ngx-spinner';
import { DatePipe } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import * as _moment from 'moment';
import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';
const moment = (_moment as any).default ? (_moment as any).default : _moment;
import { EnrolmentConfig } from '../../../../models/enrolment.model';
import * as _ from "lodash";

export const MY_CUSTOM_FORMATS = {
  fullPickerInput: 'DD-MM-YYYY HH:mm:ss',
  parseInput: 'DD-MM-YYYY HH:mm:ss',
  datePickerInput: 'DD-MM-YYYY',
  timePickerInput: 'LT',
  monthYearLabel: 'MMM YYYY',
  dateA11yLabel: 'LL',
  monthYearA11yLabel: 'MMMM YYYY'
};
@Component({
  selector: 'course-enrolment',
  templateUrl: './enrolment.html',
  styleUrls: ['./enrolment.scss'],
  providers: [DatePipe,
    { provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE] },
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS }],
  encapsulation: ViewEncapsulation.None
})

export class enrolmentComponent implements AfterViewInit, OnChanges {

  colorTheme = 'theme-dark-blue';
  @Input() data: any;
  bsConfig: Partial<BsDatepickerConfig>
  @ViewChild('myTable') table: any;
  @ViewChild(DatatableComponent) tableData: DatatableComponent;

  @ViewChild('manualTable') manualTable: any;
  @ViewChild(DatatableComponent) tableDataManual: DatatableComponent;

  @ViewChild('rulesTable') rulesTable: any;
  @ViewChild(DatatableComponent) tableDataRules: DatatableComponent;

  @ViewChild('regTable') regTable: any;
  @ViewChild(DatatableComponent) tableDataReg: DatatableComponent;

  @ViewChild('selfTable') selfTable: any;
  @ViewChild(DatatableComponent) tableDataSelf: DatatableComponent;

  usersList: any = [];
  // selectedUsers = [];
  settingsUsersSelDrop = {};
  //  userData:any;
  // tenantId:any;
  dropdownListUsers: any;
  selectedItemsUsers: any;
  dropdownSettingsUsers: any;
  demoData: any = [];
  selectedsettingvalue: any = [];
  selected: any = [];
  rows: any = [];
  temp = [];
  enrollruledata: any = [];
  selectedUsers: any = [];
  rowsUsers: any = [];
  tempUsers = [];

  selectedManual: any = [];
  rowsManual: any = [];
  tempManual = [];

  selectedRules: any = [];
  rowsRules: any = [];
  tempRules = [];
  rowsEnrolRule: any = [];

  selectedReg: any = [];
  rowsReg: any = [];
  tempReg = [];
  msg: any;
  selectedSelf: any = [];
  rowsSelf: any = [];
  tempSelf = [];
  selectedrulevalue: any = [];
  showEnrolpage: boolean = false;
  enableCourse: boolean = false;
  datarule: any;
  menutypeid: any;

  // columns = [
  //  	{ prop: 'name' },
  //  	{ name: 'Company' },
  //  	{ name: 'Gender' }
  // ];
  columnsManual = [
    {
      prop: 'selected',
      name: '',
      sortable: false,
      canAutoResize: false,
      draggable: false,
      resizable: false,
      headerCheckboxable: true,
      checkboxable: true,
      width: 30
    },
    { prop: 'ecn', name: 'EMP CODE' },
    { prop: 'fullname', name: 'FULLNAME' },
    { prop: 'emailId', name: 'EMAIL' },
    { prop: 'phoneNo', name: 'MOBILE' },
    { prop: 'enrolDate', name: 'DOE' },
    { prop: 'enrolmode', name: 'MODE' }
  ];

  columnsRules = [
    {
      prop: 'selected',
      name: '',
      sortable: false,
      canAutoResize: false,
      draggable: false,
      resizable: false,
      headerCheckboxable: true,
      checkboxable: true,
      width: 30
    },
    { prop: 'usersCount', name: 'APPLICABLE USERS' },
    { prop: 'name', name: 'RULE NAME' },
    { prop: 'description', name: 'DESCRIPTION' },
    // { prop: 'dimension', name: 'DIMENSION' },
    // { prop: 'field', name: 'FIELD' },
    // { prop: 'value', name: 'VALUES' }
  ];

  columnsReg = [
    {
      prop: 'selected',
      name: '',
      sortable: false,
      canAutoResize: false,
      draggable: false,
      resizable: false,
      headerCheckboxable: true,
      checkboxable: true,
      width: 30
    },
    { prop: 'enrolDate', name: 'ENROL DATE' },
    { prop: 'dueDays', name: 'DUE DATE (in Days)' },
    { prop: 'reminder', name: 'REMINDER (in Days)' }
  ];

  columnsSelf = [
    {
      prop: 'selected',
      name: '',
      sortable: false,
      canAutoResize: false,
      draggable: false,
      resizable: false,
      headerCheckboxable: true,
      checkboxable: true,
      width: 30
    },
    { prop: 'ecn', name: 'EMP CODE' },
    { prop: 'fullname', name: 'FULLNAME' },
    { prop: 'gender', name: 'GENDER' },
    { prop: 'doj', name: 'DOJ' },
    { prop: 'department', name: 'DEPARTMENT' },
    { prop: 'status', name: 'STATUS' }
  ];

  prospectivType: any = [
    // {
    //   prospId :1,
    //   prospName: 'Immediate'
    // },{
    //   prospId :2,
    //   prospName: 'EOD'
    // }, {
    //   prospId :3,
    //   prospName: 'Custom Date'
    // }
  ]
  // showEnrolpage:boolean = false;

  reviewCheck: any = {
    value1: false,
    value2: false,
    value3: false,
  }

  enrolment: any = {
    manual: true,
    rule: false,
    regulatory: false,
    self: false
  };

  showAddRuleModal: boolean = false;
  showAddRegulatoryModal: boolean = false;
  showAddSelfModal: boolean = false;
  showaddenrolModal: boolean = false;
  // showAddRuleModal:boolean = false;
  //   showAddRegulatoryModal:boolean = false;
  //   showAddSelfModal:boolean = false;

  ruleType: any = [{
    ruleTypeId: 1,
    ruleTypeName: 'Profile Fields'
  }, {
    ruleTypeId: 2,
    ruleTypeName: 'Other'
  }];
  ruledate: any = [];
  ruleApplicType: any = [
    // {
    //   ruleTypeId:1,
    //   ruleTypeName:'Retrospective'
    // },{
    //   ruleTypeId:2,
    //   ruleTypeName:'Prospective'
    // }
  ];
  ruleSubType: any = [{
    ruleTypeId: 1,
    ruleSubTypeId: 1,
    ruleSubTypeName: 'Username',
  }, {
    ruleTypeId: 1,
    ruleSubTypeId: 2,
    ruleSubTypeName: 'Department',
  }, {
    ruleTypeId: 2,
    ruleSubTypeId: 1,
    ruleSubTypeName: 'Doj',
  }, {
    ruleTypeId: 2,
    ruleSubTypeId: 2,
    ruleSubTypeName: 'Custom date',
  }];

  ruleData: any = {
    id: '',
    usersCount: '',
    name: '',
    description: '',
    type: '',
    subType: '',
    value: '',
    prospName: '',
    enroltype: '',
    enroldate: '',
  };

  formdata: any = {
    id: '',
    shortname: '',
    name: '',
    datatype: '',
    selected: ''
  }
  enrolldata: any;
  strArrayType: any = [[]];
  selectedFilterOption = [];
  strArrayPar: any = [];

  public addRulesForm: FormGroup;
  controlList: any = [{ datatype: '' }];
  controlFlag: any = false;

  profileFields: any = [];
  errorMsg: any;
  loader: any;

  private ValueId: number = 0;
  strArrayTypePar: any = [];
  // selectedFilterOption = [];
  selectedRule: any = [];
  // ruleType:any
  selectedRuleType: any = {
    id: ''
  };
  resultdata: any = [];
  profileFieldSelected: boolean = false;

  strArraySkilllevel: any = [{
    sName: 'Beginner',
    sId: 'Beginner'
  },
  {
    sName: 'Intermediate',
    sId: 'Intermediate'
  },
  {
    sName: 'Expert',
    sId: 'Expert'
  }]

  menuType = [];
  datetimeType = [];
  textType = [];
  textareaType = [];

  strArrayTypeSelfFields: any = [[]];
  selectedFilterOptionSelf = [];
  strArrayParSelfFields: any = [];

  public addSelfFieldsForm: FormGroup;
  controlListSelfFields: any = [{ datatype: '' }];
  controlFlagSelfFields: any = false;

  profileFieldsSelf: any = [];

  selfType: any = [{
    typeId: 1,
    typeName: 'Open'
  }, {
    typeId: 2,
    typeName: 'Approval'
  }];

  selfFeildType: any = [{
    selfTypeId: 1,
    selfTypeName: 'Profile Fields'
  }, {
    selfTypeId: 2,
    selfTypeName: 'Other'
  }];

  selfFieldsData: any = {
    id: '',
    name: '',
    description: '',
    type: '',
    subType: '',
    value: '',
    maxCount: ''
  };
  searchvalue: any = {
    value: '',
    value1: '',
    value2:''
  };
  formdataSelf: any = {
    id: '',
    shortname: '',
    name: '',
    datatype: '',
    selected: ''
  }
  userids = "";
  // ruleType:any
  selectedSelfFieldsType: any = {
    id: ''
  };
  content: any = [];
  selfProfileFieldSelected: boolean = false;
  private selfFieldValueId: number = 0;
  strArrayTypeParSelfFields: any = [];
  // selectedFilterOption = [];
  selectedSelfFields: any = [];

  // errorMsg:any;
  //  	profileFields:any = [];

  // tempUsers = [];
  // tempRules = [];
  // tempReg = [];
  // tempSelf = [];

  regData = {
    id: '',
    enrolDate: '',
    dueDays: '',
    reminder: ''
  }
  public regulatoryForm: FormGroup;
  displayenrol: any = false;

  enroluserData: any = {
    rntype: '',
    rnDate: '',
  };

  enroluserDataType: any = [
    // {
    //   TypeId:1,
    //   TypeName:'Date of Joining'
    // },
    {
      TypeId: 2,
      TypeName: 'Enrol Date'
    }
  ];
  labels: any = [
    { labelname: 'ECN', bindingProperty: 'ecn', componentType: 'text' },
    { labelname: 'FULL NAME', bindingProperty: 'fullname', componentType: 'text' },
    { labelname: 'EMAIL', bindingProperty: 'emailId', componentType: 'text' },
    { labelname: 'MOBILE', bindingProperty: 'phoneNo', componentType: 'text' },
    { labelname: 'D.0.E', bindingProperty: 'enroledate', componentType: 'date' },
    { labelname: 'ACTION', bindingProperty: 'btntext', componentType: 'button' },
  ];
  labelsRule: any = [
    { labelname: 'RULE NAME', bindingProperty: 'rulename', componentType: 'text' },
    { labelname: 'DESCRIPTION', bindingProperty: 'description', componentType: 'text' },
    { labelname: 'APPLICABLE USERS', bindingProperty: 'noOfEmp', componentType: 'text' },
    { labelname: 'ACTION', bindingProperty: 'btntext', componentType: 'button' },
    { labelname: 'EDIT', bindingProperty: 'tenantId', componentType: 'icon' },
  ];
  settings = {};
  userData;
  tenantId;
  areaId = 3;
  userId: any;
  itemList = [];
  searchFlag: number;
  searchText: any = '';
  manualEnrol: any;
  ruleEnrol: any;

  // New Changes

  labels4: any = [
    { labelname: "ECN", bindingProperty: "ecn", componentType: "text" },
    {
      labelname: "FULL NAME",
      bindingProperty: "fullname",
      componentType: "text",
    },
    { labelname: "EMAIL", bindingProperty: "emailId", componentType: "text" },
    { labelname: "MOBILE", bindingProperty: "phoneNo", componentType: "text" },
    { labelname: "Date Of Enrolment", bindingProperty: "enrolDate", componentType: "text" },
    { labelname: "MODE", bindingProperty: "enrolmode", componentType: "text" },
    // {
    //   labelname: "ACTION",
    //   bindingProperty: "btntext",
    //   componentType: "button",
    // },
  ];

  config: EnrolmentConfig = {
    manulEnrolmentData: {
      show: true,
      data: [...this.rows],
      labels: this.labels,
      userList: [...this.tempUsers],
      tabTitle: "Manual",
      identifer: "manual",
      helpContent: [],
      selectedUsers: this.selectedUsers,
    },
    ruleBasedEnrolmentData: {
      show: true,
      data: [...this.rowsRules],
      labels: this.labelsRule,
      tabTitle: "Rule Based",
      identifer: "ruleBased",
      ruleData: this.ruleData,
      ruleApplicType: [...this.ruleApplicType],
      prospectivType: [...this.prospectivType],
      profileFields: [...this.profileFields],
      controlList: this.controlList,
      profileFieldSelected: this.profileFieldSelected,
      helpContent: [],
      rowsEnrolRule : this.rowsEnrolRule,
      showEnroleduserPopup : false,
      enrolUserPopupTableLabel: this.labels4,
      showAddRuleModal: this.showAddRuleModal,
      enrolDateOption: true,
      rulesOption: this.ruledate,
    },
    selfEnrolmentData: {
      show: false,
      data: [...this.rowsSelf],
      labels: [],
      tabTitle: "Self",
      identifer: "self",
      helpContent: [],
      showAddSelfModal: this.showAddSelfModal,
      selfFeildType: this.selfFeildType,
      selfFieldsData: this.selfFieldsData,
      selfType: this.selfType,
      isFetchingSettings: false,
      profileFieldsSelf: this.profileFieldsSelf,
      controlList: this.controlList,
    },
    regulatoryEnrolmentData: {
      show: false,
      data: [...this.rowsReg],
      labels: [],
      tabTitle: "Regulatory",
      identifer: "regulatory",
      helpContent: [],
      controlList: this.controlList,
      profileFieldsRegFilter: [],
      showAddRegulatoryFilterModal: false,
      regFilterProfileFieldSelected: false,
      regularData: [],
    },
    priceBasedEnrolmentData: {
      show: false,
      data: [],
      labels: [],
      tabTitle: "Pricing",
      identifer: "pricing",
      helpContent: [],
      courseId: null,
      currencyTypeDropDown: [],
      discountListDropdownList: [],
      addEditPriceForm: [],
      showSidebar: false,
    },
  };
  enrolldatarule: any = [];

  constructor(private calendar: NgbCalendar, private _fb: FormBuilder, private http: HttpClient, private addEditCourseService: AddEditCourseContentService,
    protected service: CourseBundleService, private cdf: ChangeDetectorRef,
    //  private toasterService: ToasterService, 
     private spinner: NgxSpinnerService,
    private enrolService: enrolService, private datePipe: DatePipe, private CB: CourseBundle, private toastr: ToastrService, private http1: HttpClient) {
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
    this.getHelpContent();
    this.addRulesForm = new FormGroup({
      FilterOpt: new FormControl(),
      Value1: new FormControl(),
      Value2: new FormControl(),
    });

    this.addSelfFieldsForm = new FormGroup({
      FilterOpt: new FormControl(),
      Value1: new FormControl(),
      Value2: new FormControl(),
    });
    this.regulatoryForm = new FormGroup({
      enrolDate: new FormControl(),
      dueDays: new FormControl(),
      remDays: new FormControl(),
    });

    // this.demoData = [{ "id": 1, "itemName": "India" },
    //                    { "id": 2, "itemName": "Singapore" },
    //                    { "id": 3, "itemName": "Australia" },
    //                    { "id": 4, "itemName": "Canada" },
    //                    { "id": 5, "itemName": "South Korea" },
    //                    { "id": 6, "itemName": "Brazil" }];
    //  this.dropdownListUsers = this.demoData;
    //  this.selectedItemsUsers = [];
    //  this.dropdownSettingsUsers = {
    //                            singleSelection: false,
    //                            text:"Users",
    //                            selectAllText:'Select All',
    //                            unSelectAllText:'UnSelect All',
    //                            enableSearchFilter: true,
    //                            badgeShowLimit: 2,
    //                            classes:"myclass custom-class"
    //                          };

    this.settingsUsersSelDrop = {
      text: "Select Users",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      classes: "myclass custom-class",
      primaryKey: "ecn",
      labelKey: "fullname",
      noDataLabel: "Search Users...",
      enableSearchFilter: true,
      searchBy: ['ecn', 'fullname'],
      lazyLoading: true,
      badgeShowLimit: 3,
      maxHeight:250,
    };

    this.settings = {
      text: 'Select ',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      classes: 'myclass custom-class',
      primaryKey: 'id',
      labelKey: 'name',
      enableSearchFilter: true,
      lazyLoading: true,
      badgeShowLimit: 3,
      maxHeight:250,
    
    };

    this.fetchManual((data) => {
      // cache our list
      this.tempManual = [...data];
      // this.rowsManual = data;
      this.rows = data;
    });

    this.fetchUnEnrolledUsers((data) => {
      // // cache our list
      // this.tempUsers = [...data];
      // this.rowsUsers = data;
    });

    this.fetchRules((data) => {
      // cache our list
      this.tempRules = [...data];
      this.rowsRules = data;
      // this.rowsRules.push(this.tempRuleData);
    });

    this.fetchReg((data) => {
      // cache our list
      this.tempReg = [...data];
      this.rowsReg = data;
      // this.rowsRules.push(this.tempRuleData);
    });

    this.fetchSelf((data) => {
      // cache our list
      this.tempSelf = [...data];
      this.rowsSelf = data;
      // this.rowsRules.push(this.tempRuleData);
    });
    //       this.fetch((data) => {
    //   // cache our list
    //   // this.temp = [...data];
    //   // this.rows = data;
    // });
    if (localStorage.getItem('LoginResData')) {
      this.userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('Enrolment userData ', this.userData.data);
      this.userId = this.userData.data.data.id;
      this.tenantId = this.userData.data.data.tenantId;
    }
  }

  ngAfterViewInit() {
    
    this.ruledropdownmenu();
    this.getUserProfileFields();
    this.content = this.service.coursebundledata;
    if (this.content) {
      if (this.content.wrkflwStatus == "active") {
        this.displayenrol = true;
      }
      this.allEnrolUser(this.content);
    }

    this.getUserProfileFields();
  }

  ngOnChanges() {
    this.content = this.data;
    console.log('this.content', this.content)
  }
  /*----------------------enrol All---------------*/
  allEnrolUser(content) {
    console.log(content)
    var data = {
      tId: this.tenantId,
      wId: content.workflowid,
    }
    console.log(data);
    this.service.getallenroluser(data).then(enrolData => {
      console.log(enrolData);
      this.enrolldata = enrolData['data'];
      this.rows = enrolData['data'];
      this.rows = [...this.rows];
      this.manualEnrol=this.rows;
      for (let i = 0; i < this.rows.length; i++) {
        // this.rows[i].Date = new Date(this.rows[i].enroledate);
        // this.rows[i].enroledate = this.formdate(this.rows[i].Date);
        if(this.rows[i].visible == 1) {
          this.rows[i].btntext = 'fa fa-eye';
        } else {
          this.rows[i].btntext = 'fa fa-eye-slash';
        }
      }
      console.log("EnrolledUSer", this.rows);
      if(this.enrolldata.visible = 1){
        this.enableCourse = false;
      }else{
        this.enableCourse = true;
      }
      this.passDataToChild();
      this.cdf.detectChanges();
    })
  }
  /*----------------- enrol manual-------------*/
  allmanualenrol(content) {
    this.spinner.show();
    console.log(content)
    var data = {
      tId: this.tenantId,
      wId: content.workflowid,
    }
    console.log(data);
    this.service.getallenroluser(data).then(enrolData => {
      this.spinner.hide();
      console.log(enrolData);
      this.enrolldata = enrolData['data'];
      this.rows = enrolData['data'];
      this.rows = [...this.rows];
      // this.manualEnrol=this.rows;
      console.log("EnrolledUSer", this.rows);
      for (var i = 0; i < this.rows.length; i++) {
      if(this.rows[i].visible == 1) {
        this.rows[i].btntext = 'fa fa-eye';
      } else {
        this.rows[i].btntext = 'fa fa-eye-slash';
      }
    }
      // if(this.enrolldata.visible = 1){
      //   this.enableCourse = false;
      // }else{
      //   this.enableCourse = true;
      // }
      this.cdf.detectChanges();
    })
  }

  /*--------------------all unenrol user --------------*/
  allUNEnrolUser(evt) {
    var data = {
      wId: this.content.workflowid,
      tId: this.content.tenantId,
      searchStr: evt,
      aId: 3,
    }
    console.log(data);
    this.service.searchmanualenrol(data).then(enrolData => {
      console.log(enrolData);
      this.enrolldata = enrolData['data'];
      // this.usersList = enrolData.data;
      this.tempUsers = enrolData['data'];
      this.tempUsers = [...this.tempUsers];
      // if(this.enrolldata.length == 0){
      // 	this.showEnrolpage = !this.showEnrolpage;
      // }
      console.log("EnrolledUSer", this.tempUsers);
    })
    this.cdf.detectChanges();
  }

  TypeSelected($event, i) {
    console.log(this.enroluserData);

  }
  /*------------add enrol date-------------*/
  addenrol() {
    if (this.selectedUsers.length > 0) {
      this.showaddenrolModal = true;
    } else {

      this.toastr.warning('Please select a user.', 'Warning', {
        closeButton: false
      })
    }
  }
  closeenrol() {
    this.showaddenrolModal = false;
    this.enroluserData = [];
  }
  /*-----------------add enroluser-----------------*/

  manEnrolUser() {
    console.log('Selected user ', this.selectedUsers);


    if (this.selectedUsers.length > 0) {
      this.userids = '';
      this.spinner.show();
      for (let i = 0; i < this.selectedUsers.length; i++) {
        var user = this.selectedUsers[i];
        // this.userids = id;
        if (this.userids != "") {
          this.userids += "|";
        }
        if (String(user.id) != "" && String(user.id) != "null") {
          this.userids += user.id;
        }
        console.log("abc", this.userids);

      }
      var selectedDate=null;
      if (this.enroluserData) {
         selectedDate = this.formatDateReady(this.enroluserData.rnDate);
      }
      var data = {
        wId: this.content.workflowid,
        usIds: this.userids,
        rntype: this.enroluserData.rntype,
        rnDate:selectedDate === null ? this.formatDateReady(this.enroluserData.rnDate) : selectedDate,
        tId: this.content.tenantId,
      }     
      
      console.log("Selected data", data);
      this.enrolService.addenroluser(data).then(Response => {
        console.log(Response);
        this.spinner.hide();
        if (Response['type'] == true) {

          this.toastr.success('User(s) enrolled successfully.', 'Success', {
            closeButton: false
          });

          this.allEnrolUser(this.content);
          this.CB.allmanualenrol(this.content);
          this.closeenrol();
          this.clearesearch();

          // this.showaddenrolModal = false;
          // this.selectedUsers.visible = 1;
          // for(let i=0; i< this.selectedUsers.length; i++){
          //   this.rowsManual.push(this.selectedUsers[i]);
          // }
        } else {
          // var enrolUsersToast : Toast = {
          //   type: 'error',
          //   title: "Learning Pathway",
          //   body: "Unable to enrol user(s) at this time, Please try again later.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // // this.closeEnableDisableCourseModal();
          // this.toasterService.pop(enrolUsersToast);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
          this.closeRuleModal();
          this.clearesearch();
        }
      })
    } else {
      // var courseUpdate : Toast = {
      //   type: 'error',
      //   title: "Learning Pathway",
      //   body: "Please select a User.",
      //   showCloseButton: true,
      //   timeout: 2000
      // };
      // // this.closeEnableDisableCourseModal();
      // this.toasterService.pop(courseUpdate);

      this.toastr.warning('Please select a user.', 'Warning', {
        closeButton: false
      });
    }

    for (let j = 0; j < this.tempManual.length; j++) {
      var user = this.tempManual[j];
      for (let i = 0; i < this.selectedUsers.length; i++) {
        if (user.ecn == this.selectedUsers[i].ecn) {
          this.tempManual.splice(j, 1);
        }
      }
    }

    // this.tempUsers = [];
    // this.tempUsers = [...this.tempUsers];
    this.usersList = [];
    this.selectedUsers = [];
    // this.rowsManual = [...this.rowsManual];
    // this.tempManual = this.rowsManual;
    // console.log('Updated enrolled users ', this.rowsManual);
    this.passDataToChild();
  }
  ngOnInit() {

    this.addRulesForm = this._fb.group({
      rules: this._fb.array([
        // this.initRules(),
      ])
    });

    this.addSelfFieldsForm = this._fb.group({
      fields: this._fb.array([
        // this.initRules(),
      ])
    });
    this.regulatoryForm = this._fb.group({
      fields: this._fb.array([
        this.initRegForm(),
        this.initRegForm(),
        this.initRegForm(),
      ])
    });
  }

  initRegForm() {
    return this._fb.group({
      enrolDate: [''],
      dueDays: [''],
      remDays: ['']
    });
  }

  addRegForm() {
    const control = <FormArray>this.regulatoryForm.controls['fields'];
    control.push(this.initRegForm());
  }

  removeRegForm(i: number) {
    const control = <FormArray>this.regulatoryForm.controls['fields'];
    control.removeAt(i);
  }
  visibilityTableRow(row) {
    let value;
    let status;

    if (row.visible == 1) {
      row.btntext = 'fa fa-eye-slash';
      value  = 'fa fa-eye-slash';
      row.visible = 0
      status = 0;
    } else {
      status = 1;
      value  = 'fa fa-eye';
      row.visible = 1;
      row.btntext = 'fa fa-eye';
    }

    for(let i =0; i < this.rows.length; i++) {
      if(this.rows[i].employeeId == row.employeeId) {
        this.rows[i].btntext = row.btntext;
        this.rows[i].visible = row.visible
      }
    }
    var visibilityData = {
      eId: row.id,
      tId: this.tenantId,
      wfId: this.service.coursebundledata.workflowid,
      status: status,
    };
    this.service.enabledisable(visibilityData).then(result => {
      console.log(result);
      this.loader = false;
      this.resultdata = result;
      if (this.resultdata['type'] == true) {
        this.allmanualenrol(this.content);
        this.toastr.success(this.resultdata.data, 'Success', {
          closeButton: false
        });
      }
      this.spinner.hide();
    },
      resUserError => {
        this.spinner.hide();
        this.loader = false;
        this.errorMsg = resUserError;
        this.toastr.success('Server Error', 'Success', {
          closeButton: false
        });
        // this.closeEnableDisableCourseModal();
      });
    console.log('row', row);
    this.passDataToChild();
  }

  // disableCoursebundleVisibility(currentIndex, row, status) {
  //   this.spinner.show();
  //   var visibilityData = {
  //     eId: row.id,
  //     tId: this.tenantId,
  //     wfId: this.service.coursebundledata.workflowid,
  //     status: status,
  //   };
  //   this.service.enabledisable(visibilityData).then(result => {
  //     console.log(result);
  //     this.loader = false;
  //     this.resultdata = result;
  //     if (this.resultdata['type'] == true) {
  //       this.allmanualenrol(this.content);
  //       this.toastr.success(this.resultdata.data, 'Success', {
  //         closeButton: false
  //       });
  //     }
  //     this.spinner.hide();
  //   },
  //     resUserError => {
  //       this.spinner.hide();
  //       this.loader = false;
  //       this.errorMsg = resUserError;
  //       this.toastr.success('Server Error', 'Success', {
  //         closeButton: false
  //       });
  //       // this.closeEnableDisableCourseModal();
  //     });
  // }


  makeRulesReady() {
    for (let i = 0; i < this.profileFields.length; i++) {
      let field = this.profileFields[i];
      if (field.datatype == 'menu') {
        this.menuType.push(field);
      }
      if (field.datatype == 'datetime') {
        this.datetimeType.push(field);
      }
      if (field.datatype == 'text') {
        this.textType.push(field);
      }
      if (field.datatype == 'textarea') {
        this.textareaType.push(field);
      }
    }
  }

  onItemSelect(item: any) {
    console.log(item);
    console.log(this.selectedUsers);
  }
  OnItemDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedUsers);
  }
  onSelectAll(items: any) {
    console.log(items);
  }
  onDeSelectAll(items: any) {
    console.log(items);
  }

  // manEnrolUser(){
  // 	console.log('Selected user ',this.selectedUsers);
  // 	if(this.selectedUsers.length > 0){
  // 		for(let i=0; i< this.selectedUsers.length; i++){
  // 			this.rowsManual.push(this.selectedUsers[i]);
  // 		}
  // 	}

  // 	for(let j=0; j< this.tempManual.length; j++){
  // 		var user = this.tempManual[j];
  // 		for(let i=0; i< this.selectedUsers.length; i++){
  // 			if(user.ecn == this.selectedUsers[i].ecn){
  // 				this.tempManual.splice(j,1);
  // 			}
  // 		}
  // 	}

  // 	this.selectedUsers = [];
  // 	this.usersList = [];
  // 	this.tempManual = this.rowsManual;
  // 	console.log('Updated enrolled users ',this.rowsManual);
  // }

  onSearch(evt: any) {
    console.log(evt.target.value);
    const val = evt.target.value;
      this.fetchAllUnEnrolUsersForLPAsync(val, (enrolData) => {
        if (enrolData.type === true) {
          this.enrolldata = enrolData['data'];
          this.tempUsers = enrolData['data'];
          this.tempUsers = [...this.tempUsers];
          console.log('EnrolledUSer ', this.tempUsers);
          const temp = this.tempUsers.filter(function (d) {
            return String(d.ecn).toLowerCase().indexOf(val) !== -1 ||
              d.fullname.toLowerCase().indexOf(val) !== -1 || !val;
          });

          // update the rows
          this.usersList = temp;
          this.cdf.detectChanges();
    this.passDataToChild();
        
        } else {
          console.log('Error getting EnrolledUSer ', enrolData);
          this.tempUsers = [];
          this.tempUsers = [...this.tempUsers];
          this.usersList = [];
          this.cdf.detectChanges();
        }
      });
    
    // else if (val.length === 0) {
    //   this.tempUsers = [];
    //   this.tempUsers = [...this.tempUsers];
    //   this.usersList = [];
    //   this.cdf.detectChanges();
    // }
    // else {
    //   // this.subscription.unsubscribe();
    // }

    // this.allUNEnrolUser(evt.target.value);
    // const val = evt.target.value;

    // const temp = this.tempUsers.filter(function(d) {
    //   return String(d.ecn).toLowerCase().indexOf(val) !== -1 ||
    //   d.fullname.toLowerCase().indexOf(val) !== -1
    // });

    // // update the rows
    // this.usersList = temp;
  }

  fetchAllUnEnrolUsersForLPAsync(params: any, cb) {
    const data = {
      wId: this.content.workflowid,
      tId: this.content.tenantId,
      // tId : this.tenantId,
      searchStr: params,
      aId: 3,
    }
    this.service.searchmanualenrol(data).then(res => {
      cb(res);
    }, err => {
      console.log(err);
    });
  }

  getUserProfileFields() {
    // 	this.addEditCourseService.getUserProfileFields()
    // 	.subscribe(rescompData => {
    //   	// this.loader =false;
    //   	this.profileFields = rescompData.data[0];
    //   	this.profileFieldsSelf = rescompData.data[0];
    //   	// this.topic = rescompData.data[0];
    //   	console.log('User profile fields',this.profileFields);
    // },
    // resUserError => {
    //   // this.loader =false;
    //   this.errorMsg = resUserError
    // });
    this.enrolService.getUserProfileFields()
      .then(rescompData => {
        // this.loader =false;
        this.profileFields = rescompData['data'][0];
        this.profileFieldsSelf = rescompData['data'][0];
        // this.topic = rescompData.data[0];
        console.log('User profile fields', this.profileFields);
      },
        resUserError => {
          // this.loader =false;
          this.errorMsg = resUserError
        });
  }

  fetchUnEnrolledUsers(cb) {
    const req = new XMLHttpRequest();
    req.open('GET', `assets/data/unEnroledUsers.json`);

    req.onload = () => {
      cb(JSON.parse(req.response));
    };

    req.send();
  }


  fetchRules(cb) {
    // const req = new XMLHttpRequest();
    // req.open('GET', `assets/data/rules.json`);

    // req.onload = () => {
    //   cb(JSON.parse(req.response));
    // };

    // req.send();
  }

  fetchManual(cb) {
    // const req = new XMLHttpRequest();
    // req.open('GET', `assets/data/company.json`);
    // req.open('GET', `assets/data/enroledUsers.json`);
    // req.open('GET', `assets/data/100k.json`);

    // req.onload = () => {
    //   cb(JSON.parse(req.response));
    // };

    // req.send();
  }

  fetchReg(cb) {
    const req = new XMLHttpRequest();
    // req.open('GET', `assets/data/company.json`);
    req.open('GET', `assets/data/regulatory.json`);
    // req.open('GET', `assets/data/100k.json`);

    req.onload = () => {
      cb(JSON.parse(req.response));
    };

    req.send();
  }

  fetchSelf(cb) {
    const req = new XMLHttpRequest();
    // req.open('GET', `assets/data/company.json`);
    req.open('GET', `assets/data/self.json`);
    // req.open('GET', `assets/data/100k.json`);

    req.onload = () => {
      cb(JSON.parse(req.response));
    };

    req.send();
  }
  fetch(cb) {
    const req = new XMLHttpRequest();
    // req.open('GET', `assets/data/company.json`);
    req.open('GET', `assets/data/enroledUsers.json`);
    // req.open('GET', `assets/data/100k.json`);

    req.onload = () => {
      cb(JSON.parse(req.response));
    };

    req.send();
  }


  onSelect({ selected }) {
    console.log('Select Event', selected, this.selected);

    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }

  toggleExpandRow(row) {
    console.log('Toggled Expand Row!', row);
    this.table.rowDetail.toggleExpandRow(row);
  }

  onDetailToggle(event) {
    console.log('Detail Toggled', event);
  }

  onActivate(event) {
    console.log('Activate Event', event);
  }

  selectTab(tabEvent) {
    console.log('tab Selected', tabEvent);
  }

  searchManEnrol(event) {

    const val = event.target.value.toLowerCase();
    this.searchText=val;
    // filter our data
    // const temp = this.temp.filter(function(d) {
    //   return d.name.toLowerCase().indexOf(val) !== -1 ||
    //   d.gender.toLowerCase() === val ||
    //   d.company.toLowerCase().indexOf(val) !== -1 ||
    //   String(d.age).toLowerCase().indexOf(val) !== -1 ||
    //   !val;
    // });
    if (val.length >= 3||val.length==0) {
      this.temp = [...this.enrolldata];
    const temp = this.temp.filter(function (d) {
      return String(d.ecn).toLowerCase().indexOf(val) !== -1 ||
        d.fullname.toLowerCase().indexOf(val) !== -1 ||
        // d.gender.toLowerCase() === val ||
        // d.department.toLowerCase().indexOf(val) !== -1 ||
        // String(d.do).toLowerCase().indexOf(val) !== -1 ||
        // d.mode.toLowerCase() === val ||
        d.emailId.toLowerCase().indexOf(val) !== -1 ||
        d.enroledate.toLowerCase().indexOf(val) !== -1 ||
        d.phoneNo.toLowerCase().indexOf(val) !== -1 ||
        String(d.enrolmode).toLowerCase().indexOf(val) !== -1 ||
        !val;
    });

    // update the rows
    // this.rowsManual = temp;
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    // this.tableDataManual.offset = 0;
    this.passDataToChild();
  }
}

  searchRuleEnrol(event) {
    const val = event.target.value.toLowerCase();
    this.searchText=val;
    // filter our data
    this.temp=this.ruleEnrol;
    if(val.length>=3||val.length==0){
    const temp = this.temp.filter(function (d) {
      // return String(d.usersCount).toLowerCase().indexOf(val) !== -1 ||
      //   String(d.name).toLowerCase().indexOf(val) !== -1 ||
      //   String(d.description).toLowerCase() === val ||
      //   String(d.dimension).toLowerCase().indexOf(val) !== -1 ||
      //   String(d.field).toLowerCase().indexOf(val) !== -1 ||
      //   String(d.value).toLowerCase().indexOf(val) !== -1 ||
      //   !val;
      return String(d.noOfEmp).toLowerCase().indexOf(val) !== -1 ||
        String(d.rulename).toLowerCase().indexOf(val) !== -1 ||
        String(d.description).toLowerCase().indexOf(val) !== -1 ||
        !val;
    });

    // update the rows
    this.rowsRules = temp;
    // Whenever the filter changes, always go back to the first page
    // this.tableDataRules.offset = 0;
    this.passDataToChild();
  }

}

  searchRegEnrol(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.tempReg.filter(function (d) {
      return String(d.enrolDate).toLowerCase().indexOf(val) !== -1 ||
        String(d.dueDays).toLowerCase() === val ||
        String(d.reminder).toLowerCase().indexOf(val) !== -1 ||
        !val;
    });

    // update the rows
    this.rowsReg = temp;
    // Whenever the filter changes, always go back to the first page
    this.tableDataReg.offset = 0;
  }

  searchSelfEnrol(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.tempSelf.filter(function (d) {
      return String(d.ecn).toLowerCase().indexOf(val) !== -1 ||
        d.fullname.toLowerCase().indexOf(val) !== -1 ||
        d.gender.toLowerCase() === val ||
        d.department.toLowerCase().indexOf(val) !== -1 ||
        String(d.doj).toLowerCase().indexOf(val) !== -1 ||
        d.status.toLowerCase() === val || !val;
    });

    // update the rows
    this.rowsSelf = temp;
    // Whenever the filter changes, always go back to the first page
    // this.tableDataSelf.offset = 0;
    this.passDataToChild();
  }

  // enrolUser(){
  //   this.showEnrolpage = !this.showEnrolpage;
  // }

  onCheckBoxClick(event, ruletype) {
    console.log(event, ruletype);
    this.clearRuleType();

    if (event) {
      switch (ruletype) {
        case "manual":
          // code...
          this.enrolment.manual = true;
          this.allmanualenrol(this.content);
          break;
        case "rule":
          // code...
          this.enrolment.rule = true;
          this.allruleList();
          break;
        case "self":
          this.enrolment.self = true;
          // code...
          break;
        case "regulatory":
          this.enrolment.regulatory = true;
          // code...
          break;
        default:
          // code...
          // this.enrolment.manual = true;
          break;
      }
    }

    //  if(event == false){
    //      this.reviewCheck = {};
    //  }else{

    // //  	if(this.enrolment.manual == true){
    // //   	this.enrolment.regulatory = false;
    // // this.enrolment.rule = false;
    // // this.enrolment.self = false;
    // //   }else if(this.enrolment.regulatory == true){
    // //   	this.enrolment.manual = false;
    // // this.enrolment.rule = false;
    // // this.enrolment.self = false;
    // //   }else if(this.enrolment.rule == true){
    // //   	this.enrolment.manual = false;
    // // this.enrolment.regulatory = false;
    // // this.enrolment.self = false;
    // //   }else if(this.enrolment.self == true){
    // //   	this.enrolment.manual = false;
    // // this.enrolment.regulatory = false;
    // // this.enrolment.rule = false;
    // //   }
    //  }
    // console.log('$event',$event);
    //console.log('courseReviewCheck',courseReviewCheck);
  }
  clearRuleType() {
    this.enrolment = {
      manual: false,
      rule: false,
      regulatory: false,
      self: false
    };
  }

  setActiveItem(index, item) {
    console.log("Selected Module", item, index);
  }

  // openRule(){
  // 	// this.parent_Comp.openRuleModal();
  // 	this.showAddRuleModal = this.addEditCourseService.showRule;
  // }

  // openRegulatory(){
  // 	// this.parent_Comp.openRegulatoryModal();
  // 	this.showAddRegulatoryModal = this.addEditCourseService.showRegulatory;
  // }

  // openSelf(){
  // 	// this.parent_Comp.openSelfModal();
  // 	this.showAddSelfModal = this.addEditCourseService.showSelf;
  // }

  // closeRule(){
  // 	this.parent_Comp.closeRuleModal();
  // 	this.showAddRuleModal = this.addEditCourseService.showRule;

  // }



  onSelectManual({ selected }) {
    console.log('Select Manual Event', selected, this.selectedManual);

    this.selectedManual.splice(0, this.selectedManual.length);
    this.selectedManual.push(...selected);

    // if(this.selectedManual.length == 1){
    // 	this.enableShowRuleUsers = true;
    // }else{
    // 	this.enableShowRuleUsers = false;
    // }
  }

  onActivateManual(event) {
    console.log('Activate Event', event);
  }

  rules: any[] = [
    {
      FilterOpt: '1',
      Value1: 'Beginner',
      Value2: ''
    },
    {
      FilterOpt: '8',
      Value1: 'Sales',
      Value2: ''
    }
  ];

  openRuleModal(rowData, id) {

    this.showAddRuleModal = true;

    if (id == 0) {
      this.ruleData = {
        id: 0,
        usersCount: '',
        name: '',
        description: '',
        type: '',
        prospName: '',
        profiles: [],
        visible: '',
        value: '',
        enroltype: '',
        enroldate: '',
      };
      this.selectedrulevalue = [];
      this.msg = "Rule added";
      this.passDataToChild();
    } else {
      this.ruleData = {
        id: rowData.enrolRuleId,
        usersCount: rowData.noOfEmp,
        name: rowData.rulename,
        description: rowData.description,
        type: rowData.ruleAppType,
        prospName: rowData.ruleAppEvent,
        profiles: rowData.profiles,
        visible: rowData.visible,
        value: rowData.ruleAppDate ?  new Date(rowData.ruleAppDate) : '',
        enroltype: rowData.enroltype,
        enroldate: rowData.enroldate ? new Date(rowData.enroldate) : '',
      };
      this.msg = "Rule updated"
      if (this.ruleData.profiles.length > 0) {
        // this.onRuleFieldList(this.ruleData.profiles, 0, (result) => {

        // })
        // this.profileFieldSelected = true;

        for (let i = 0; i < this.ruleData.profiles.length; i++) {
          let rule = this.ruleData.profiles[i];
          console.log(rule);
          console.log(this.profileFields);

          this.callRuleFieldType(rule.field, i, 1);
          console.log(this.datarule);
          if (this.datarule == 'datetime') {
            var array = this.ruleData.profiles[i].fieldValues.split('$');
            this.ruleData.profiles[i].fieldValues = [new Date(array[0]), new Date(array[1])];
          }
          if (this.datarule == 'menu') {
            this.databindrule(this.ruleData.profiles[i], i);
          }
          console.log(rule.fieldValues);
        }
        this.profileFieldSelected = true;
        this.passDataToChild();
        setTimeout(() => {
          this.cdf.detectChanges();
        }, 400);
      }

      console.log('Edit rule data ', this.ruleData);
    }
    this.passDataToChild();
  }

  setUpForm(rules: any[]) {
    return new FormGroup({
      rules: new FormArray(rules.map((rule) => this.createRule(rule)))
    });
  }

  get rulesFormArray() {
    return (this.addRulesForm.get('rules') as FormArray);
  }

  createRule(rule: any) {
    return new FormGroup({
      FilterOpt: new FormControl(rule.FilterOpt || ''),
      Value1: new FormControl(rule.Value1 || ''),
      Value2: new FormControl(rule.Value2 || '')
    })
  }
  prospTypeSelected() {
    console.log(this.ruleData);
    this.ruleData.value = '';
    // if(this.ruleData.)
  }
  enroldatetype() {
    console.log(this.ruleData);
    this.ruleData.enroldate = '';
    // if(this.ruleData.)
  }
  // closeRuleModal(){
  //   this.showAddRuleModal = false;
  //   this.showaddenrolModal = false;
  //   // this.courseDataService.showRule = this.showAddRuleModal;
  //   this.ruleData = {};
  //   this.enroluserData = {}
  //   this.formdata = {};
  //   this.clearRule();
  //   this.profileFieldSelected = false;
  // }

  clearRule() {
    this.addRulesForm.reset({
      FilterOpt: [''],
      Value1: [''],
      Value2: ['']
    })
  }

  initRules() {
    return this._fb.group({
      FilterOpt: [''],
      Value1: [''],
      Value2: ['']
    });
  }

  addRule() {
    if (this.ruleData.type == 1) {
      this.profileFieldSelected = true;
      // this.ruleData.type = 'Profile Fields';
    } else {
      // this.ruleData.type = 'Others';
    }
    const control = <FormArray>this.addRulesForm.controls['rules'];
    control.push(this.initRules());
    console.log(this.addRulesForm.controls['rules']);
    this.controlFlag = true;
    this.controlList.push(0);
    this.strArrayType.push([]);

  }

  removeRule(i: number) {
    const control = <FormArray>this.addRulesForm.controls['rules'];
    control.removeAt(i);
    // this.enableSelect(this.strArrayType[i]);
    this.selectedFilterOption.splice(i, 1);
    this.disableSelect();
    this.controlFlag = true;
    this.controlList.splice(i, 1);
    this.strArrayType.splice(i, 1);
  }

  disableSelect() {
    this.profileFields.forEach((data, key) => {
      if (this.selectedFilterOption.indexOf(data.shortname) >= 0) {
        this.profileFields[key].selected = "true";
      } else {
        this.profileFields[key].selected = "false";
      }
    })
    console.log('Selected Disabled', this.strArrayPar);
  }


  ruleTypeSelected($event, i) {
    console.log(this.ruleData);

  }

  ruledropdownmenu() {
    let param = {
      tId: this.tenantId,
    };
    this.enrolService.dropdown(param).then(res => {
      console.log(res['data']);
      this.ruleApplicType = res['data'][0];
      this.prospectivType = res['data'][1];
      this.ruledate = res['data'][2];
      console.log("dropdown", this.ruleApplicType);
      console.log("dropdown", this.prospectivType);
      this.config.ruleBasedEnrolmentData.ruleApplicType = [
        ...this.ruleApplicType,
      ];
      this.config.ruleBasedEnrolmentData.prospectivType = [
        ...this.prospectivType,
      ];
      this.config.ruleBasedEnrolmentData.rulesOption = [
        ...this.ruledate,
      ];
    })
  }

  callType(id: any, index: any) {
    if (this.strArrayType[index]) {
      this.strArrayType[index] = [];
    }

    if (id.srcElement == undefined || id.target == undefined) {
      this.ValueId = id;
    } else {
      this.ValueId = parseInt((id.srcElement || id.target).value);
    }
    //this.disableSelect();

    // this.controlList[index] = this.ValueId;
    for (let i = 0; i < this.profileFields.length; i++) {
      if (this.profileFields[i].id == this.ValueId) {
        // this.selectedRuleType = this.profileFields[i].datatype;

        this.controlList[index] = this.profileFields[i];

        this.strArrayType[index].push(this.profileFields[i]);
        if (this.selectedFilterOption.length > 0) {
          this.selectedFilterOption[index] = this.profileFields[i].shortname;
        } else {
          this.selectedFilterOption.push(this.profileFields[i].shortname);
        }
      }
    }
    this.disableSelect();
  }

  makeRuleDataready(ruleData) {
    this.spinner.show();
    // var rules = ruleData.rules;
    var rules = ruleData.profiles;
    var dimension = "";
    var field = "";
    var value = "";
    var allstring = "";
    console.log("rules", rules)
    if (rules.length > 0) {
      for (var i = 0; i < rules.length; i++) {
        var rule = rules[i];

        if (this.controlList[i].datatype == 'datetime') {
          var fromdate = this.formatDateReady(rule.fieldValues[0]);
          var todate = this.formatDateReady(rule.fieldValues[1]);
          // rule.fieldValues = this.formatDateReady(rule.fieldValues);
          rule.fieldValues = fromdate + '$' + todate;
          console.log(rule.fieldValues);
        }
        if (this.controlList[i].datatype == 'menu') {
          console.log(this.selectedrulevalue[i]);
          // rules[i].fieldValues = this.createstring(this.selectedrulevalue[i])
          rules[i].fieldValues = this.createstring(rule.fieldValuesArr);
        }
        if (i == 0) {
          allstring = rule.field + "|" + rule.fieldValues + "|1";
        }
        else {
          allstring += "#" + rule.field + "|" + rule.fieldValues + "|1";
        }
        console.log(allstring);
      }
    } else {
      // field = this.ruleData.prospName;
      // value = this.formatDateReady(this.ruleData.value);
      // if(value == "undefined"){
      //   value = "";
      // }
      // allstring = field+ "|" + value + "|" + 1;
    }
    var ruleDimension;
    for (let r = 0; r < this.ruleType.length; r++) {
      if (this.ruleType[r].ruleTypeId == ruleData.type) {
        ruleDimension = this.ruleType[r].ruleTypeName;
        console.log(this.ruleType, " +++ ", ruleDimension)
      }
    }

    // field : field,
    // value : value,
    // usersCount: 0,
    var visibleRule;
    console.log(this.ruleData);
    if (this.ruleData.id == "") {
      visibleRule = 1;
    }
    else {
      visibleRule = this.ruleData.visible;
    }
    console.log("visibleRule", visibleRule);
    // if(this.ruleData.value != ""){
    //    var appData =  this.formatDateReady(this.ruleData.value);
    // }
    var roledata = 0;
    if (ruleData.id != '' && ruleData.id != null && ruleData.id != undefined) {
      roledata = ruleData.id;
    }
    console.log(ruleData.type);
    // if(ruleData.type==2){
    //   allstring=null;
    // }
    if (this.ruleData.value != "" || this.ruleData.value != undefined || this.ruleData.value != null) {
      this.ruleData.value = this.formatDateReady(this.ruleData.value);
    }
    if (this.ruleData.enroldate != "" || this.ruleData.enroldate != undefined || this.ruleData.enroldate != null) {
      this.ruleData.enroldate = this.formatDateReady(this.ruleData.enroldate);
    }
    var newRuleData = {
      rId: roledata,
      rname: ruleData.name,
      rdescription: ruleData.description,
      appType: ruleData.type,
      appevent: this.ruleData.prospName,
      appDate: this.ruleData.value,
      cid: this.content.workflowid,
      tid: this.tenantId,
      userId: this.userId,
      allstr: allstring,
      visible: visibleRule,
      areaId: this.areaId,
      enroltype: this.ruleData.enroltype,
      enroldate: this.ruleData.enroldate,
    }

    console.log('Final rule data', newRuleData);
    this.enrolService.Addruleforcourse(newRuleData).then(res => {
      this.spinner.hide();
      console.log(res);
      this.loader = false;
      this.resultdata = res;
      if (this.resultdata.type == false) {

        this.presentToast('error', '');
      } else {
        this.newruleadd();
        this.allruleList();
        // this.toasterService.pop(courseUpdate);
        this.presentToast('success', this.msg);
      }
    })

    this.closeRuleModal();
  }
  newruleadd() {
    var data = {
      areaId: this.areaId,
      corsId: this.content.workflowid,
      tenId: this.tenantId,
    }
    console.log(data);
    this.enrolService.Addruleforcourse_new_enrolmnet(data).then(res => {
      console.log(res);
      this.passDataToChild();
    });
  }
  createstring(data) {
    var str = '';
    for (var i = 0; i < data.length; i++) {
      if (i == 0) {
        str = data[i].name;
        // str = data[i].id;
      } else {
        str = str + ',' + data[i].name;
        // str = str + ',' + data[i].id;
      }
    }
    return str;
  }
  databindrule(alldata, i) {
    var array = alldata.fieldValues.split(',');
    let newarray = []
    let data = {
      lovtype: this.menutypeid,
      tId: this.tenantId,
    }
    this.enrolService.getprofileFieldDropdown(data).then(res => {
      console.log(res);
      if (res['type'] == true) {
        let strArraySkilllevel = res['data'][0];

        if (strArraySkilllevel && strArraySkilllevel.length > 0) {
          this.make_selected_dropdown_data_ready(alldata, strArraySkilllevel, i);
          this.cdf.detectChanges();
        } else {
          this.enrolService.getprofileFieldDropdown(data).then(res => {
            console.log(res);
            if (res['type'] == true) {
              let strArraySkilllevel = res['data'][0];
              this.profileFields[i].subtype = strArraySkilllevel;
              this.make_selected_dropdown_data_ready(alldata, strArraySkilllevel, i);
            }
            this.cdf.detectChanges();
          });
        }
      }
    });
    this.selectedrulevalue[i] = newarray;
    this.passDataToChild();
  }
  make_selected_dropdown_data_ready(alldata, strArraySkilllevel, i) {
    var array = alldata.fieldValues.split(',');
    let newarray = []
    strArraySkilllevel.forEach(dataitem => {
      array.forEach(element => {
        if (dataitem.name == element)
          newarray.push(dataitem);
      });
    });
    alldata.fieldValuesArr = [];
    if (newarray && newarray.length > 0) {
      alldata.fieldValuesArr = newarray;
      alldata.fieldValues = '';
    }
    // if (i == this.selfFieldsData.profiles.length - 1) {
    //   // this.isFetchingSettings = false;
    // }
    this.cdf.detectChanges();
    this.passDataToChild();
  }
  saveRule(event, f) {
    // this.loader =true;
    // console.log('Events',event);
    if(f.valid){
      let rules: any = this.addRulesForm.value.rules;
      this.ruleData.rules = rules;
      // console.log('Rule data final', this.ruleData);
  
      this.makeRuleDataready(this.ruleData);
      this.passDataToChild();
    } else {
      console.log('Please Fill all fields');
      Object.keys(f.controls).forEach(key => {
        f.controls[key].markAsDirty();
      });
    }
    this.passDataToChild();
  }

  enableShowRuleUsers: boolean = false;
  onSelectRules({ selected }) {
    console.log('Select Rules Event', selected, this.selectedRules);

    this.selectedRules.splice(0, this.selectedRules.length);
    this.selectedRules.push(...selected);

    if (this.selectedRules.length == 1) {
      this.enableShowRuleUsers = true;
    } else {
      this.enableShowRuleUsers = false;
    }
  }

  onActivateRules(event) {
    console.log('Activate Event', event);
  }

  showRuleUsersModal: boolean = false;
  ruleUsersModelTitle: any = '';
  viewRuleUsers(rowData) {
    // this.ruleUsersModelTitle = this.selectedRules[0].name;
    this.ruleUsersModelTitle = rowData.rulename;
    this.showRuleUsersModal = true;
    this.passDataToChild();
    this.allEnrolUserruleList(rowData);
  }

  /*********** enrol user by rules ******************/
  allEnrolUserruleList(content) {
    this.spinner.show();
    // var data = {
    //   areaId: this.areaId,
    //   instanceId: content.enrolRuleId,
    //   tId: this.userdata.tenantId,
    //   mode: 2,
    // }
    var data ={
      areaId: this.areaId,
      instanceId: this.content.workflowid,
      tId: this.tenantId,
      ruleId:content.enrolRuleId,
      mode: 2,
    };
    this.addEditCourseService.getallenroluser(data).then(enrolData => {
      this.spinner.hide();
      this.enrolldatarule = enrolData['data'];
      this.rowsEnrolRule = enrolData['data'];
      this.rowsEnrolRule = [...this.rowsEnrolRule];
      for (let i = 0; i < this.rowsEnrolRule.length; i++) {
        // this.rowsEnrolRule[i].Date = new Date(this.rowsEnrolRule[i].enrolDate);
        // this.rowsEnrolRule[i].enrolDate = this.formdate(this.rowsEnrolRule[i].Date);
      }
      console.log('EnrolledUSer', this.rowsEnrolRule);
      if (this.enrolldata.visible = 1) {
        this.enableCourse = false;
      } else {
        this.enableCourse = true;
      }
      this.cdf.detectChanges();
      this.passDataToChild()
    }, resUserError => {
      // this.loader = false;
      // this.errorMsg = resUserError;
      this.spinner.hide();
      // this.closeEnableDisableCourseModal();
    })

  }
    
  closeRuleUsersModal() {
    this.showRuleUsersModal = false;
    this.ruleUsersModelTitle = '';
    this.passDataToChild();
  }

  openRegulatoryModal() {
    this.showAddRegulatoryModal = true;
    // this.courseDataService.showRegulatory = this.showAddRegulatoryModal;

  }

  onSelectReg({ selected }) {
    console.log('Select Reg Event', selected, this.selectedReg);

    this.selectedReg.splice(0, this.selectedReg.length);
    this.selectedReg.push(...selected);

    // if(this.selectedManual.length == 1){
    // 	this.enableShowRuleUsers = true;
    // }else{
    // 	this.enableShowRuleUsers = false;
    // }
  }

  onActivateReg(event) {
    console.log('Activate Event', event);
  }

  closeRegulatoryModal() {
    this.showAddRegulatoryModal = false;
    // this.courseDataService.showRegulatory = this.showAddRegulatoryModal;
    this.clearRegData();
  }

  makeRegDataReady() {
    if (this.regData.enrolDate != null || this.regData.enrolDate != undefined || this.regData.enrolDate != '') {
      var enrolDt: any = this.regData.enrolDate;
    }

    var regDataDinal = {
      id: this.rowsReg.length + 1,
      enrolDate: enrolDt.formatted,
      dueDays: this.regData.dueDays,
      reminder: this.regData.reminder
    }

    console.log('Regulatory Final data', regDataDinal);

    this.rowsReg.push(regDataDinal);
    this.tempReg = this.rowsReg;

    this.closeRegulatoryModal();
  }

  clearRegData() {
    this.regData = {
      id: '',
      enrolDate: '',
      dueDays: '',
      reminder: ''
    };
  }

  saveReg() {
    // console.log('Regulatory data',this.regData);
    // this.rowsReg.push();

    this.makeRegDataReady();
  }

  openSelfModal() {
    this.showAddSelfModal = true;
    // this.courseDataService.showRule = this.showAddRuleModal;
    this.passDataToChild();
  }

  onSelectSelf({ selected }) {
    console.log('Select Reg Event', selected, this.selectedSelf);

    this.selectedSelf.splice(0, this.selectedSelf.length);
    this.selectedSelf.push(...selected);

    // if(this.selectedManual.length == 1){
    // 	this.enableShowRuleUsers = true;
    // }else{
    // 	this.enableShowRuleUsers = false;
    // }
  }

  onActivateSelf(event) {
    console.log('Activate Event', event);
  }

  closeSelfModal() {
    this.showAddSelfModal = false;
    // this.courseDataService.showRule = this.showAddRuleModal;
    this.selfFieldsData = {};
    this.formdataSelf = {};
    this.clearSelfFields();
    this.selfProfileFieldSelected = false;
  }

  initSelfFields() {
    return this._fb.group({
      FilterOpt: [''],
      Value1: [''],
      Value2: ['']
    });
  }

  addSelfFields() {
    if (this.selfFieldsData.type == 1) {
      this.selfProfileFieldSelected = true;
    }
    const control = <FormArray>this.addSelfFieldsForm.controls['fields'];
    control.push(this.initSelfFields());
    console.log(this.addSelfFieldsForm.controls['fields']);
    this.controlFlagSelfFields = true;
    this.controlListSelfFields.push(0);
    this.strArrayTypeSelfFields.push([]);

  }

  removeSelfFields(i: number) {
    const control = <FormArray>this.addSelfFieldsForm.controls['fields'];
    control.removeAt(i);
    // this.enableSelect(this.strArrayType[i]);
    this.selectedFilterOptionSelf.splice(i, 1);
    this.disableSelectSelfFields();
    this.controlFlagSelfFields = true;
    this.controlListSelfFields.splice(i, 1);
    this.strArrayTypeSelfFields.splice(i, 1);
  }

  disableSelectSelfFields() {
    this.profileFieldsSelf.forEach((data, key) => {
      if (this.selectedFilterOptionSelf.indexOf(data.shortname) >= 0) {
        this.profileFieldsSelf[key].selected = "true";
      } else {
        this.profileFieldsSelf[key].selected = "false";
      }
    })
    console.log('Selected Disabled', this.strArrayPar);
  }

  clearSelfFields() {
    const arr = <FormArray>this.addSelfFieldsForm.controls.fields;
    arr.controls = [];
    this.addSelfFieldsForm.reset({
      FilterOpt: [''],
      Value1: [''],
      Value2: ['']
    })
  }

  selfFieldTypeSelected() {

  }

  callTypeSelfFields(id: any, index: any, selectedField) {
    if (this.strArrayTypeSelfFields[index]) {
      this.strArrayTypeSelfFields[index] = [];
    }

    this.selfFieldValueId = parseInt((id.srcElement || id.target).value);
    //this.disableSelect();

    // this.controlList[index] = this.ValueId;
    for (let i = 0; i < this.profileFieldsSelf.length; i++) {
      if (this.profileFieldsSelf[i].id == this.selfFieldValueId) {
        // this.selectedRuleType = this.profileFields[i].datatype;

        this.controlListSelfFields[index] = this.profileFieldsSelf[i];

        this.strArrayTypeSelfFields[index].push(this.profileFieldsSelf[i]);
        if (this.selectedFilterOptionSelf.length > 0) {
          this.selectedFilterOptionSelf[index] = this.profileFieldsSelf[i].shortname;
        } else {
          this.selectedFilterOptionSelf.push(this.profileFieldsSelf[i].shortname);
        }
      }
    }
    this.disableSelectSelfFields();
  }

  makeSelfDataReady(selfFieldsData) {

    var fields = selfFieldsData.fields;
    var dimension = "";
    var field = "";
    var value = "";

    if (fields.length > 0) {
      for (var i = 0; i < fields.length; i++) {
        var selffield = fields[i];
        // if(dimension != ""){
        //   dimension += "|";
        // }
        // if(String(parameter.type) != "" && String(parameter.type) != "null"){
        //   dimension += parameter.type;
        // }
        if (field != "") {
          field += ",";
        }
        if (String(selffield.FilterOpt) != "" && String(selffield.FilterOpt) != "null") {
          for (let j = 0; j < this.profileFieldsSelf.length; j++) {
            var profileFieldsSelf = this.profileFieldsSelf[j];
            if (selffield.FilterOpt == profileFieldsSelf.id) {
              field += profileFieldsSelf.name;
            }
          }
          // field += rule.FilterOpt;
        }

        if (selffield.Value1.formatted == '' || selffield.Value1.formatted == undefined || selffield.Value1.formatted == null) {
          if (value != "") {
            value += ",";
          }
          if (String(selffield.Value1) != "" && String(selffield.Value1) != "null") {
            value += selffield.Value1;
          }
        } else {
          if (value != "") {
            value += ",";
          }
          if (String(selffield.Value1.formatted) != "" && String(selffield.Value1.formatted) != "null") {
            value += selffield.Value1.formatted;
          }
        }

      }
    }


    var selfType;
    for (let r = 0; r < this.selfType.length; r++) {
      if (this.selfType[r].typeId == selfFieldsData.id) {
        selfType = this.selfType[r].typeName;
      }
    }
    var selfDimension;
    for (let r = 0; r < this.selfFeildType.length; r++) {
      if (this.selfFeildType[r].selfTypeId == selfFieldsData.type) {
        selfDimension = this.selfFeildType[r].selfTypeName;
      }
    }

    var newSelfData = {
      id: this.rowsSelf.length + 1,
      type: selfType,
      maxCount: 0,
      fieldType: selfDimension,
      field: field,
      value: value
    }

    console.log('Final rule data', newSelfData);

    // this.enrolService.ruleData = newRuleData;

    // this.rowsSelf.push(newSelfData);

    // this.tempSelf = this.rowsSelf;

    this.closeSelfModal();
  }

  saveSelfFields() {
    // this.loader =true;
    // console.log(this.addRulesForm.value);
    let selfFields: any = this.addSelfFieldsForm.value.fields;
    this.selfFieldsData.fields = selfFields;
    console.log('Self fields data final', this.selfFieldsData);

    this.makeSelfDataReady(this.selfFieldsData);

    // this.userDashService.updateUser(this.filterData).subscribe(
    //    data => {
    //       this.loader =false;
    //       this.users = data.data[1];
    //       console.log('users Data', this.users)
    //       this.clearFilterData();
    //       // console.log("Data ",data);
    //    },
    //    error => {
    //      this.loader =false;
    //      console.error("Error !");
    //      //return Observable.throw(error);
    //    }
    // );
  }
  clearesearch() {
    if(this.searchText.length>=3){
    this.searchvalue = {};
    // this.allmanualenrol(this.content);
    this.searchText='';

    } else{
    this.searchvalue={}; 
    this.tempUsers = [];
    }
    // this.rows = [...this.enrolldata];
    // this.rowsEnrolRule = [...this.enrolldatarule];
    // this.rowsRules = [...this.enrollruledata];
    this.passDataToChild();
  }
  cleareRule() {
    if(this.searchText.length>=3){
    this.searchvalue = {};
      this.allruleList();
    this.searchText='';
    } else{
    this.searchvalue={};
    }
  }
  enrolUser() {
    this.showEnrolpage = !this.showEnrolpage;
  }
  saveEnrol() {
    this.showEnrolpage = !this.showEnrolpage;

  }

  backToEnrol() {
    this.showEnrolpage = !this.showEnrolpage;
  }
  searchEnrolUser(event) {
    const val = event.target.value.toLowerCase();
    console.log(this.rows);
    this.temp = this.manualEnrol;
    console.log(this.temp);
    this.searchText=val;
    // filter our data
    if(val.length>=3||val.length==0){
    const temp = this.temp.filter(function (d) {
      return String(d.ecn).toLowerCase().indexOf(val) !== -1 ||
        d.fullname.toLowerCase().indexOf(val) !== -1 ||
        d.emailId.toLowerCase().indexOf(val) !== -1 ||
        //d.enrolmode.toLowerCase().indexOf(val) !== -1 ||
        //d.enrolDate.toLowerCase().indexOf(val) !== -1 ||
        d.phoneNo.toLowerCase().indexOf(val) !== -1 ||
        String(d.enrolmode).toLowerCase().indexOf(val) !== -1 ||
        // d.mode.toLowerCase() === val || !val;
        !val
    });

    // update the rows
    this.rows = [...temp];
    // Whenever the filter changes, always go back to the first page
    this.tableData.offset = 0;
  }
  }

  formatDateReady(date) {
    if (date) {
      date = new Date(date);
      var day = date.getDate();
      var monthIndex = ("0" + (date.getMonth() + 1)).slice(-2);
      var year = date.getFullYear();

      return year + '-' + monthIndex + '-' + day;
    }
  }
  formdate(date) {

    if (date) {
      // const months = ["JAN", "FEB", "MAR","APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
      // var day = date.getDate();
      // var monthIndex = months[date.getMonth()];
      // var year = date.getFullYear();

      // return day + '_' + monthIndex + '_' +year;
      var formatted = this.datePipe.transform(date, 'dd-MM-yyyy');
      return formatted;
    }
  }

  /*------------------------Disable rule------------*/
  visibilityTableRowRule(row) {
    let value;
    let status;

    if (row.visible == 1) {
      row.btntext = 'fa fa-eye-slash';
      value  = 'fa fa-eye-slash';
      row.visible = 0
      status = 0;
    } else {
      status = 1;
      value  = 'fa fa-eye';
      row.visible = 1;
      row.btntext = 'fa fa-eye';
    }

    // for(let i =0; i < this.rows.length; i++) {
    //   if(this.rows[i].employeeId == row.employeeId) {
    //     this.rows[i].btntext = row.btntext;
    //     this.rows[i].visible = row.visible
    //   }
    // }
    for(let i =0; i < this.rowsRules.length; i++) {
      if(this.rowsRules[i].employeeId == row.employeeId) {
        this.rowsRules[i].btntext = row.btntext;
        this.rowsRules[i].visible = row.visible
      }
    }
    var visibilityData = {
      enrolRuleId: row.enrolRuleId,
      visible: status
    }
    console.log(visibilityData);
    this.enrolService.disableRule(visibilityData).then(result => {
      this.spinner.hide();
      console.log(result);
      this.loader = false;
      this.resultdata = result;
      if (this.resultdata.type == false) {
        
        this.presentToast('error', '');
      } else {
      
        console.log("after", row.visible)
        this.allruleList();
        // this.toasterService.pop(courseUpdate);
        this.presentToast('success', this.resultdata.data);
      }
      this.cdf.detectChanges();
    },
      resUserError => {
        this.loader = false;
        this.errorMsg = resUserError;
        this.spinner.show();
      
      });

    console.log('row', row);
    this.passDataToChild();
  }
  // disableRuleVisibility(currentIndex, row, status) {
  //   this.spinner.show();
  //   var visibilityData = {
  //     enrolRuleId: row.enrolRuleId,
  //     visible: status
  //   }
  //   console.log(visibilityData);
  //   this.enrolService.disableRule(visibilityData).then(result => {
  //     this.spinner.hide();
  //     console.log(result);
  //     this.loader = false;
  //     this.resultdata = result;
  //     if (this.resultdata.type == false) {
  //       // var courseUpdate : Toast = {
  //       //     type: 'error',
  //       //     title: "Course",
  //       //     body: "Unable to update visibility of Rule.",
  //       //     showCloseButton: true,
  //       //     timeout: 2000
  //       // };
  //       // // this.closeEnableDisableCourseModal();
  //       // this.toasterService.pop(courseUpdate);
  //       this.presentToast('error', '');
  //     } else {
  //       // var courseUpdate : Toast = {
  //       //     type: 'success',
  //       //     title: "Course",
  //       //     body: this.resultdata.data,
  //       //     showCloseButton: true,
  //       //     timeout: 2000
  //       // };
  //       // row.visible = !row.visible;
  //       console.log("after", row.visible)
  //       this.allruleList();
  //       // this.toasterService.pop(courseUpdate);
  //       this.presentToast('success', this.resultdata.data);
  //     }
  //     this.cdf.detectChanges();
  //   },
  //     resUserError => {
  //       this.loader = false;
  //       this.errorMsg = resUserError;
  //       this.spinner.show();
  //       // this.closeEnableDisableCourseModal();
  //     });
  // }

  // /*----------------Rule add ---------*/
  // saveRule(event) {
  //   // this.loader =true;
  //   // console.log('Events',event);
  //   let rules: any = this.addRulesForm.value.rules;
  //   this.ruleData.rules = rules;
  //   console.log('Rule data', this.ruleData);

  //   this.makeRuleDataready(this.ruleData);
  // }

  /*---------------Rule Edit----------*/
  editrule(row) {
    console.log(row);
    this.openRuleModal(row, 1);
    // this.openRulemodel23(row);
  }

  /*------------------Rule list -----------*/
  allruleList() {
    this.spinner.show();
    var data = {
      areaId: this.areaId,
      instanceId: this.content.workflowid,
      tId: this.tenantId,

    }
    console.log(data);
    this.enrolService.getallrule(data).then(enrolData => {
      console.log(enrolData);
      this.spinner.hide();
      this.enrollruledata = enrolData['data'];
      this.rowsRules = enrolData['data'];
      this.rowsRules = [...this.rowsRules];
      this.ruleEnrol=this.rowsRules;
      console.log("RULE", this.rowsRules);
      this.cdf.detectChanges();
      for (var i = 0; i < this.rowsRules.length; i++) {
      if(this.rowsRules[i].visible == 1) {
        this.rowsRules[i].btntext = 'fa fa-eye';
      } else {
        this.rowsRules[i].btntext = 'fa fa-eye-slash';
      }
    }
    this.passDataToChild();
    })
  }

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }

  async onRuleFieldList(list, i, cb) {
    if (list.length === 0 || list.length === i) {
      cb(true);
      this.cdf.detectChanges();
    } else {
      let rule = list[i];
      this.strArrayTypeSelfFields.push([]);
      const responsecalltype = await this.callRuleFieldType(rule.field, i, 1);

      if (this.controlList[i].datatype == 'datetime') {
        var array = list[i].fieldValues.split('$');
        this.ruleData.profiles[i].fieldValues = [new Date(array[0]), new Date(array[1])];
      }
      this.onRuleFieldList(list, i + 1, cb);
    }
  }
  async callRuleFieldType(id: any, index: any, status) {
    this.profileFieldSelected = false;
    this.passDataToChild();
    console.log(id + index);
    console.log(this.ruleData.profiles);
    if (status == 2) {
      this.ruleData.profiles[index].fieldValues = '';
      this.ruleData.profiles[index].fieldValuesArr = [];
    }
    if (this.strArrayType[index]) {
      this.strArrayType[index] = [];
    }

    if (id.srcElement == undefined || id.target == undefined) {
      this.ValueId = id;
    } else {
      this.ValueId = parseInt((id.srcElement || id.target).value);
    }
    for (let i = 0; i < this.profileFields.length; i++) {
      if (this.profileFields[i].id == this.ValueId) {

        this.controlList[index] = this.profileFields[i];
        // this.strArrayType[index].push(this.profileFields[i]);
        // if (this.selectedFilterOption.length > 0) {
        this.selectedFilterOption[index] = this.profileFields[i].shortname;
        this.datarule = this.profileFields[i].datatype;
        this.menutypeid = this.profileFields[i].menuTypeId;
        // this.ruleData.profiles[index].fieldValues = '';
        // this.ruleData.profiles[index].fieldValuesArr = [];
        // console.log(this.menutypeid)
        /*---------profile menu list-----------*/
        if (this.menutypeid != '' && this.menutypeid != null && this.menutypeid != undefined) {
          var data = {
            lovtype: this.menutypeid,
            tId: this.tenantId
          }
          this.enrolService.getprofileFieldDropdown(data).then(res => {
            this.strArraySkilllevel = res['data'][0];
            this.strArraySkilllevel = [...this.strArraySkilllevel];
            this.itemList = this.strArraySkilllevel;
            // this.itemList = this.controlList[i].subtype;
            console.log(this.itemList);
            this.profileFields[i].subtype = this.strArraySkilllevel;
            this.cdf.detectChanges();
            this.passDataToChild();
          })
        }
        console.log(this.profileFields);
        this.profileFieldSelected = true;
        this.passDataToChild();
        //  this.datatype.push(this.ruleData.profiles[i]);
        // } else {
        //   this.selectedFilterOption.push(this.profileFields[i].shortname);
        //   console.log(this.selectedFilterOption);
        // }
        this.cdf.detectChanges();
        this.passDataToChild();
      }
    }
    this.disableSelectedRuleFieldType();
    this.passDataToChild();
  }
  // onItemSelectSelf(currentEvent: any, currentIndex: any, currentItem: any) {
  //   console.log('currentEvent, currentIndex, currentItem', currentEvent, currentIndex, currentItem);
  // }
  // OnItemDeSelectSelf(currentEvent: any, currentIndex: any, currentItem: any) {
  //   console.log('currentEvent, currentIndex, currentItem', currentEvent, currentIndex, currentItem);
  // }
  // onSelectAllSelf(currentEvent: any, currentIndex: any, currentItem: any) {
  //   console.log('currentEvent, currentIndex, currentItem', currentEvent, currentIndex, currentItem);
  // }
  // onDeSelectAllSelf(currentEvent: any, currentIndex: any, currentItem: any) {
  //   console.log('currentEvent, currentIndex, currentItem', currentEvent, currentIndex, currentItem);
  // }
  ruleFieldTypeSelected(currentEvent: any, currentIndex: any) {
    console.log('currentEvent, currentIndex, currentItem', currentEvent, currentIndex);
  }
  // Help Code Start Here //

  helpContent: any;
  getHelpContent() {
    return new Promise(resolve => {

      this.http1.get('../../../../../../assets/help-content/addEditCourseContent.json').subscribe(
        data => {
          this.helpContent = data;
          console.log('Help Array', this.helpContent);
        },
        err => {
          resolve('err');
        },
      );
    });
    // return this.helpContent;
  }


  // Help Code Ends Here //

  addRuleList() {
    if (this.ruleData.profiles.length == 0) {
      this.profileFieldSelected = true;
    }
    let defualtRulesObj = {
      enrolProfileId: 0,
      field: '',
      fieldValues: ''
    }
    this.ruleData.profiles.push(defualtRulesObj);
    this.controlFlag = true;
    this.controlList.push(0);
    this.strArrayType.push([]);
    console.log(this.ruleData.profiles);
    this.disableSelectedRuleFieldType();
    this.passDataToChild();
  }

  removeRuleList(currentIndex) {
    this.profileFieldSelected = false;
    this.ruleData.profiles.splice(currentIndex, 1);
    this.selectedFilterOption.splice(currentIndex, 1);
    this.disableSelectedRuleFieldType();
    this.controlFlag = true;
    this.controlList.splice(currentIndex, 1);
    this.strArrayType.splice(currentIndex, 1);
    this.cdf.detectChanges();
    console.log(this.ruleData.profiles);
    this.profileFieldSelected = true;
    this.passDataToChild();
  }

  updatedProfilesFieldDataAfterRemove(currentItem) {
    // this.profileFieldsSelf = this.profileFieldsSelf.filter(function(value, index, arr){
    //   return value > 5;
    // });
    this.profileFieldsSelf.forEach((data, key) => {
      if (data.id == Number(currentItem.field)) {
        data.subtype = [];
      }
    })
  }
  closeRuleModal() {
    this.showAddRuleModal = false;
    // this.courseDataService.showRule = this.showAddRuleModal;
    this.ruleData = {};
    this.formdata = {};
    // this.clearRule();
    // this.profileFieldSelected = false;
    this.selectedFilterOption = [];
    this.selectedFilterOptionSelf = [];
    this.formdata = {};
    this.controlList = [];
    // this.itemList = [];
    this.selectedUsers = [];
    this.selfProfileFieldSelected = false;
    this.strArrayTypeSelfFields = [];
    for (let i = 0; i < this.profileFields.length; i++) {
      this.profileFields[i].selected = false;
    }
    // this.disableSelectedRuleFieldType();
    this.allruleList();
    this.passDataToChild();
  }
  disableSelectedRuleFieldType() {
    this.profileFields.forEach((data, key) => {
      if (this.selectedFilterOption.indexOf(data.shortname) >= 0) {
        this.profileFields[key].selected = 'true';
      } else {
        this.profileFields[key].selected = 'false';
      }
    })
    console.log('Selected Disabled', this.strArrayPar);
    this.cdf.detectChanges();
  }

  // new enrol ui functions

  performActionOnData(event) {
    console.log("Event ==>", event);
    if (event) {
      // console.log('args ==>', event.argument.join(','));
      // console.log('args ==>', [...event.argument]);
      switch (event.action) {
        // Tab Events
        case "manual":
          // code...
          // this.enrolment.manual = true;
          this.allEnrolUser(this.content);
          break;
        case "ruleBased":
          // code...
          // this.enrolment.rule = true;
          this.allruleList();
          break;
        case "self":
          // this.enrolment.self = true;
          // this.selfenrolledUser(this.content);
          // code...
          break;
        case "regulatory":
          // this.enrolment.regulatory = true;
          // this.allregulatorylist(this.content);
          // code...
          break;
        case "pricing":
            // this.enrolment.regulatory = true;
            // this.getPriceList();
            // code...
            break;

        // Manual Enrol Events
        case "searchManEnrol":
          this.searchManEnrol(event.argument[0]);
          break;
        case "clearesearch":
          this.clearesearch();
          break;
        case "onSearch":
          this.onSearch(event.argument[0]);
          break;
          ////
        case "manEnrolUser": 
          this.selectedUsers = event.argument[0];
          // this.manEnrolUser();
          this.addenrol();
          break;
          ////
        case "visibilityTableRow":
          this.visibilityTableRow(event.argument[0]);
          break;

        // Rule Enrolment
        case "searchRuleEnrol":
          this.searchRuleEnrol(event.argument[0]);
          break;
        case "clearRuleEnrol":
          this.clearRuleEnrol();
          break;
        case "addEditRule":
          this.openRuleModal(event.argument[0], event.argument[1]);
          break;
          ////
        case "visibilityTableRow1":
          this.visibilityTableRowRule(event.argument[0]);
          break;
        case "viewRuleUsers":
          this.viewRuleUsers(event.argument[0]);
          break;
        ////
        case "saveRule":
          // this.selectedUsers = event.argument[0];
          this.ruleData = event.argument[2];
          this.saveRule(event.argument[0], event.argument[1]);
          break;
          ////
        case "ruleTypeSelected":
          this.ruleData = event.argument[1];
          this.ruleTypeSelected(event.argument[0], null);
          break;
          ////
        case "prospTypeSelected":
          this.ruleData = event.argument[1];
          this.prospTypeSelected();
          break;
          ////
        case "removeRuleList":
          // this.selectedUsers = event.argument[0];
          this.ruleData = event.argument[1];
          this.removeRuleList(event.argument[0]);
          break;
          ////
        case "ruleFieldTypeSelected":
          // this.selectedUsers = event.argument[0];
          this.ruleData = event.argument[3];
          this.ruleFieldTypeSelected(event.argument[1], event.argument[2]);
          break;
        case "callRuleFieldType":
          // this.selectedUsers = event.argument[0];
          this.ruleData = event.argument[3];
          this.callRuleFieldType(
            event.argument[0],
            event.argument[1],
            event.argument[2]
          );
          break;
        case "onItemSelectRule":
          // this.selectedUsers = event.argument[0];
          this.ruleData = event.argument[3];
          this.onItemSelectRule(
            event.argument[0],
            event.argument[1],
            event.argument[2]
          );
          break;
        case "onSelectAllRule":
          // this.selectedUsers = event.argument[0];
          this.ruleData = event.argument[3];
          this.onSelectAllRule(
            event.argument[0],
            event.argument[1],
            event.argument[2]
          );
          break;
        case "OnItemDeSelectRule":
          // this.selectedUsers = event.argument[0];
          this.ruleData = event.argument[3];
          this.OnItemDeSelectRule(
            event.argument[0],
            event.argument[1],
            event.argument[2]
          );
          break;
        case "onDeSelectAllRule":
          // this.selectedUsers = event.argument[0];
          this.ruleData = event.argument[3];
          this.onDeSelectAllRule(
            event.argument[0],
            event.argument[1],
            event.argument[2]
          );
          break;
        case "addRuleList":
          // this.selectedUsers = event.argument[0];
          this.addRuleList();
          break;
        case "closePopup":
            // this.selectedUsers = event.argument[0];
            this.closeRuleModal();
            break;
        case "updateFormValuesRules":
              // this.selectedUsers = event.argument[0];
              // this.closeRuleModal();
              this.ruleData = event.argument[0];
              break;
              ////
        case "closeRuleEnrolPopup":
                // this.selectedUsers = event.argument[0];
                // this.closeRuleModal();
               this.closeRuleUsersModal();
                break;

        // Regulatory
        case "searchRuleEnrol":
          this.searchRuleEnrol(event.argument[0]);
          break;
        case "clearRuleEnrol":
          this.clearRuleEnrol();
          break;
        // case "addEditRule":
        //   this.openRuleModal(event.argument[0], event.argument[1]);
        //   break;
        ////
        case "regulatoryVisiblityChange":
          // this.visibilityTableRow2(event.argument[0]);
          break;
          ////
        case "saveReg":
          // this.saveReg(event.argument[0]);
          break;
          ////
        case "saveRegFilter":
            // this.regularData = event.argument[2];
            // this.saveRegFilter(event.argument[0],event.argument[1]);
            break;
          ////
        case "closeRegulatoryFilterModal":
          // this.selectedUsers = event.argument[0];
          // this.closeRegulatoryFilterModal();
          break;
        case "removeRegFilter":
            // this.selectedUsers = event.argument[0];
            // this.removeRegFilter(event.argument[0]);
            break;
        case "regFilterTypeSelected":
          // this.regularData = event.argument[3];
          this.ruleTypeSelected(event.argument[0], null);
          break;
          ////
        case "callTypeRegFilter":
          // this.regularData = event.argument[3];
          // this.callTypeRegFilter(event.argument[0], event.argument[1],event.argument[2]);
          break;
          ////
        case "onItemSelectRegulatory":
            // this.selectedUsers = event.argument[0];
            // this.regularData = event.argument[3];
            this.onItemSelectRule(
              event.argument[0],
              event.argument[1],
              event.argument[2]
            );
            break;
          case "onSelectAllRegulatory":
            // this.selectedUsers = event.argument[0];
            // this.regularData = event.argument[3];
            this.onSelectAllRule(
              event.argument[0],
              event.argument[1],
              event.argument[2]
            );
            break;
          case "OnItemDeSelectRegulatory":
            // this.selectedUsers = event.argument[0];
            // this.regularData = event.argument[3];
            this.OnItemDeSelectRule(
              event.argument[0],
              event.argument[1],
              event.argument[2]
            );
            break;
          case "onDeSelectAllRegulatory":
            // this.selectedUsers = event.argument[0];
            // this.regularData = event.argument[3];
            this.onDeSelectAllRule(
              event.argument[0],
              event.argument[1],
              event.argument[2]
            );
            break;
          case "onSelectAllRegulatory":
              // this.selectedUsers = event.argument[0];
              // this.regularData = event.argument[3];
              this.onSelectAllRule(
                event.argument[0],
                event.argument[1],
                event.argument[2]
              );
              break;
        case "openRegulatoryFilterModal":
                // this.selectedUsers = event.argument[0];
              //  this.openRegulatoryFilterModal();
                break;

        case "addRegulatoryFilter":
          // this.selectedUsers = event.argument[0];
          // this.addRegulatoryFilter();
          break;
       case "updateFormValuesRegulatory":
            // this.selectedUsers = event.argument[0];
            // this.closeRuleModal();
            // this.regularData = event.argument[0];
            break;
        // Self

        case "clearself":
          // this.selectedUsers = event.argument[0];
          this.clearself();
          break;
        case "searchSelfEnrol":
          // this.selectedUsers = event.argument[0];
          this.searchSelfEnrol(event.argument[0]);
          break;
        case "openSelfModal":
          this.openSelfModal();
          break;
          ////
        case "changeVisibilitySelf":
          // this.visibilityTableRow3(event.argument[0]);
          break;
        case "onItemSelectSelf":
          // this.selectedUsers = event.argument[0];
          this.selfFieldsData = event.argument[3];
          this.onItemSelectSelf(
            event.argument[0],
            event.argument[1],
            event.argument[2],
          );
          break;
        case "OnItemDeSelectSelf":
          // this.selectedUsers = event.argument[0];
          this.selfFieldsData = event.argument[3];
          this.OnItemDeSelectSelf(
            event.argument[0],
            event.argument[1],
            event.argument[2]
          );
          break;
       case "onSelectAllSelf":
            // this.selectedUsers = event.argument[0];
            this.selfFieldsData = event.argument[3];
            this.onSelectAllSelf(
              event.argument[0],
              event.argument[1],
              event.argument[2]
            );
            break;
      case "onDeSelectAllSelf":
              // this.selectedUsers = event.argument[0];
              this.selfFieldsData = event.argument[3];
              this.onSelectAllSelf(
                event.argument[0],
                event.argument[1],
                event.argument[2]
              );
              break;
              ////
        case "saveSelfFields":
          // this.selectedUsers = event.argument[0];
          this.selfFieldsData = event.argument[0];
          // this.saveSelfFields(event.argument[0], event.argument[1]);
          break;
          ////
        case "closeSelfModal":
            // this.selectedUsers = event.argument[0];
            // this.closeSelfModal();
            break;
            ////
        case "selfFieldTypeSelected":
              // this.selectedUsers = event.argument[0];
              // this.selfFieldsData = event.argument[3];
              // this.selfFieldTypeSelected(event.argument[0], event.argument[1],event.argument[2]);
              break;
              ////
        case "callTypeSelfFields":
                // this.selectedUsers = event.argument[0];
                this.selfFieldsData = event.argument[3];
                // this.callTypeSelfFields(event.argument[0], event.argument[1],event.argument[2]);
                break;
              ////
        case "addSelfFields":
                  // this.selectedUsers = event.argument[0];
                  // this.addSelfFields();
                  break;
            ////
        case 'removeSelfFields':
                // this.removeSelfFields(event.argument[0], event.argument[1],event.argument[2]);
                break;
        case "updateFormValuesSelf":
                  // this.selectedUsers = event.argument[0];
                  // this.closeRuleModal();
                  this.selfFieldsData = event.argument[0];
                  break;

        // Price
        ////
        case 'createUpdatePrice': 
        // this.addEditPriceForm = event.argument[0];
                                  // this.createUpdatePrice(event.argument[0]);
                                  break;
        ////
        case 'searchBar': 
        // this.searchOnPriceList(event.argument[0]);
                                  break;
        ////
        case 'bindValueToAddEditForm': 
        // this.bindValueToAddEditForm(event.argument[0],event.argument[1]);
                                      break;
        ////
        case 'clearSearch': 
        // this.clearSearch();
                                      break;
        ////
        case 'closeSidebar': 
        // this.closeSidebar();
                                      break;
      }
    }
  }

  passDataToChild() {
    this.config.manulEnrolmentData.userList = [...this.tempUsers];
    this.config.manulEnrolmentData.data = [...this.rows];
    this.config.manulEnrolmentData.selectedUsers = [...this.selectedUsers];

    this.config.ruleBasedEnrolmentData.data = [...this.rowsRules];

    this.config.regulatoryEnrolmentData.data = [...this.rowsReg];

    this.config.ruleBasedEnrolmentData.helpContent = _.clone(this.helpContent);
    this.config.ruleBasedEnrolmentData.profileFieldSelected = _.clone(
      this.profileFieldSelected
    );
    this.config.ruleBasedEnrolmentData.profileFields = _.cloneDeep[this.profileFields]
    this.config.ruleBasedEnrolmentData.ruleData = _.cloneDeep(this.ruleData);
    this.config.ruleBasedEnrolmentData.controlList = _.cloneDeep(this.controlList);
    this.config.ruleBasedEnrolmentData.rowsEnrolRule = [...this.rowsEnrolRule];
    this.config.ruleBasedEnrolmentData.enrolUserPopupTableLabel = [this.labels4];
    this.config.ruleBasedEnrolmentData.showEnroleduserPopup =_.clone(this.showRuleUsersModal);
    this.config.ruleBasedEnrolmentData.showAddRuleModal = _.clone(this.showAddRuleModal);
    this.config.ruleBasedEnrolmentData.profileFields = _.cloneDeep(
      this.profileFields
    );

    // this.config.regulatoryEnrolmentData.showAddRegulatoryFilterModal = _.clone(this.showAddRegulatoryFilterModal)
    // this.config.ruleBasedEnrolmentData.ruleData.profiles = _.clone(this.ruleData.profiles);

    this.config.regulatoryEnrolmentData.helpContent = _.clone(this.helpContent);
    // this.config.regulatoryEnrolmentData.regFilterProfileFieldSelected = _.clone(this.regFilterProfileFieldSelected);
    // this.config.regulatoryEnrolmentData.profileFieldsRegFilter = [...this.profileFieldsRegFilter];
    this.config.regulatoryEnrolmentData.controlList = [...this.controlList];
    // this.config.regulatoryEnrolmentData.regularData =  _.clone(this.regularData);

    this.config.selfEnrolmentData.helpContent = _.clone(this.helpContent);
    this.config.selfEnrolmentData.showAddSelfModal = _.clone(this.showAddSelfModal);
    this.config.selfEnrolmentData.selfFieldsData = _.cloneDeep(this.selfFieldsData);
    this.config.selfEnrolmentData.selfType = _.cloneDeep(this.selfType);
    // this.config.selfEnrolmentData.isFetchingSettings = _.clone(this.isFetchingSettings);
    this.config.selfEnrolmentData.controlList = _.clone(this.controlList);
    this.config.selfEnrolmentData.profileFieldsSelf = _.clone(this.profileFieldsSelf);
    this.config.selfEnrolmentData.selfFeildType = _.clone(this.selfFeildType);
    this.config.selfEnrolmentData.data = _.clone(this.rowsSelf);
    // Pricing

    // this.config.priceBasedEnrolmentData.data = _.clone(this.tempDisplayPriceList);
    // this.config.priceBasedEnrolmentData.currencyTypeDropDown = _.clone(this.currencyTypeDropDown);
    // this.config.priceBasedEnrolmentData.discountListDropdownList = _.clone(this.discountListDropdownList);
    // this.config.priceBasedEnrolmentData.addEditPriceForm = _.clone(this.addEditPriceForm);
    // this.config.priceBasedEnrolmentData.showSidebar = _.clone(this.showPriceSidebar);
    // this.config.priceBasedEnrolmentData.labels =  _.clone(this.labelsPrice);
    // console.log("this.profileFieldSelected", this.profileFieldSelected);
    console.log(
      "this.config",
      this.config
    );
    this.cdf.detectChanges();
  }

  clearRuleEnrol() {
    // if (this.searchText.length >= 3) {
    //   this.searchvalue = {};
    //   this.allruleList(this.content);
    // } else {
    //   this.searchvalue = {};
    // }
    this.allruleList();
    this.passDataToChild();
  }

  onItemSelectSelf(currentEvent: any, currentIndex: any, currentItem: any) {
    console.log(
      "currentEvent, currentIndex, currentItem",
      currentEvent,
      currentIndex,
      currentItem
    );
  }
  OnItemDeSelectSelf(currentEvent: any, currentIndex: any, currentItem: any) {
    console.log(
      "currentEvent, currentIndex, currentItem",
      currentEvent,
      currentIndex,
      currentItem
    );
  }
  onSelectAllSelf(currentEvent: any, currentIndex: any, currentItem: any) {
    console.log(
      "currentEvent, currentIndex, currentItem",
      currentEvent,
      currentIndex,
      currentItem
    );
  }
  onDeSelectAllSelf(currentEvent: any, currentIndex: any, currentItem: any) {
    console.log(
      "currentEvent, currentIndex, currentItem",
      currentEvent,
      currentIndex,
      currentItem
    );
  }
  onItemSelectRule(currentEvent: any, currentIndex: any, currentItem: any) {
    console.log(
      "currentEvent, currentIndex, currentItem",
      currentEvent,
      currentIndex,
      currentItem
    );
  }
  OnItemDeSelectRule(currentEvent: any, currentIndex: any, currentItem: any) {
    console.log(
      "currentEvent, currentIndex, currentItem",
      currentEvent,
      currentIndex,
      currentItem
    );
  }
  onSelectAllRule(currentEvent: any, currentIndex: any, currentItem: any) {
    console.log(
      "currentEvent, currentIndex, currentItem",
      currentEvent,
      currentIndex,
      currentItem
    );
  }
  onDeSelectAllRule(currentEvent: any, currentIndex: any, currentItem: any) {
    console.log(
      "currentEvent, currentIndex, currentItem",
      currentEvent,
      currentIndex,
      currentItem
    );
  }
  clearself() {
    // if (this.searchText.length >= 3) {
    //   this.searchvalue = {};

    // } else {
    //   this.searchvalue = {};
    // }
    // this.selfenrolledUser(this.content);
  }

}
