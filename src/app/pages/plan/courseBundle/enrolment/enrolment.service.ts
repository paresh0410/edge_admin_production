import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, RequestOptions, Request } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from "@angular/common/http";
import { AppConfig } from '../../../../app.module';

import { webApi } from '../../../../service/webApi';

@Injectable()

export class enrolService {

  public data: any;
  request: Request;

  public ruleData: any;

  private _urlProfileFields: string = "/api/edge/course/getUserProfileFields";
  private _urladdenroluser: string = "/api/web/cb/course_bundle_enrole_Manual";

  private _urlprofilefield: string = webApi.domain + webApi.url.profilefield;
  private _urlallRulelist: string = webApi.domain + webApi.url.getallrule;
  private _urlDisableRule = webApi.domain + webApi.url.disablerule;
  private _urlprofilemenudropdown = webApi.domain + webApi.url.profilemenudropdown;
  private _urlgetruledropdown = webApi.domain + webApi.url.getruledropdown;
  private _urlAdd_Rule = webApi.domain + webApi.url.addrule;
  private _urlAdd_Rule_enrolment = webApi.domain + webApi.url.addrulenewother;

  constructor(@Inject('APP_CONFIG_TOKEN') private config: AppConfig, private _http: Http, private _http1: HttpClient) {
    //this.busy = this._http.get('...').toPromise();
  }

  getUserProfileFields() {
    //  let url:any = `${this.config.FINAL_URL}`+this._urlProfileFields;
    //  // let url:any = `${this.config.DEV_URL}`+this._urlFilter;
    //  let headers = new Headers({ 'Content-Type': 'application/json' });
    //  let options = new RequestOptions({ headers: headers });
    //let body = JSON.stringify(user);
    //  return this._http.post(url, options ).map((res: Response) => res.json());
    return new Promise(resolve => {
      this._http1.post(this._urlprofilefield, '')
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  addenroluser(data) {
    let url: any = `${this.config.FINAL_URL}` + this._urladdenroluser;
    return new Promise(resolve => {
      this._http1.post(url, data)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  getallrule(data) {
    //   let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    //   let options = new RequestOptions({ headers: headers });
    //  return this._http.post(this._urlallRulelist, data, options)
    //  .map((response:Response) => response.json())
    //  .catch(this._errorHandler);
    return new Promise(resolve => {
      this._http1.post(this._urlallRulelist, data)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  getprofileFieldDropdown(data) {
    return new Promise(resolve => {
      this._http1.post(this._urlprofilemenudropdown, data)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  disableRule(visibleData) {
    //   let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    //   let options = new RequestOptions({ headers: headers });
    //  return this._http.post(this._urlDisableRule,visibleData, options)
    //      .map((response:Response)=>response.json())
    //      .catch(this._errorHandler);

    return new Promise(resolve => {
      this._http1.post(this._urlDisableRule, visibleData)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  dropdown(param) {
    // let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    // let options = new RequestOptions({ headers: headers });
    // return this._http.post(this._urlgetruledropdown,this.param, options)
    //     .map((response:Response)=>response.json())
    //     .catch(this._errorHandler);
    console.log('Drop Down', param);
    return new Promise(resolve => {
      this._http1.post(this._urlgetruledropdown, param)
        //.map(res => res.json())

        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }

  Addruleforcourse(visibleData) {
    //   let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    //   let options = new RequestOptions({ headers: headers });
    //  return this._http.post(this._urlAdd_Rule,visibleData, options)
    //      .map((response:Response)=>response.json())
    //      .catch(this._errorHandler);

    return new Promise(resolve => {
      this._http1.post(this._urlAdd_Rule, visibleData)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }
  Addruleforcourse_new_enrolmnet(givendata) {
    return new Promise(resolve => {
      this._http1.post(this._urlAdd_Rule_enrolment, givendata)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  _errorHandler(error: Response) {
    console.error(error);
    return Observable.throw(error || "Server Error")
  }


}
