import { Component, OnInit, ViewChild, ChangeDetectorRef, EventEmitter, Output } from '@angular/core';
// import { ToasterService, Toast } from 'angular2-toaster';
import { NgxSpinnerService } from 'ngx-spinner';
import { webAPIService } from '../../../../service/webAPIService';
// import { webApi } from '../../../service/webApi';
import { DatePipe } from '@angular/common';
import { UnEnrolCourseBundleService } from './un-enrol-coursebundle.service';
import { Router, ActivatedRoute } from '@angular/router';
import { XlsxToJsonService } from '../../../plan/users/uploadusers/xlsx-to-json-service';
import { JsonToXlsxService } from './json-to-xlsx.service'
import { NULL_EXPR } from '@angular/compiler/src/output/output_ast';
import { Switch } from '@syncfusion/ej2-buttons';
import { ToastrService } from 'ngx-toastr';
import { CourseBundleService } from '../courseBundle.service';

@Component({
  selector: 'ngx-un-enrol-coursebundle',
  templateUrl: './un-enrol-coursebundle.component.html',
  styleUrls: ['./un-enrol-coursebundle.component.scss']
})
export class UnEnrolCourseBundleComponent implements OnInit {

  @ViewChild('myTable') table: any;
  private xlsxToJsonService: XlsxToJsonService = new XlsxToJsonService();
  rows = [];
  selected = [];
  templateExelLink: any = 'assets/images/Asset_Upload_Template.xlsx';
  /*
    uploadDetailsOne: any = [
      { assetName: "B2B sales", date: '12/04/2019', category: 'Abc', format: '.xls', channel: 'akdf', language: 'english' },
      { assetName: "B2B sales", date: '12/04/2019', category: 'Abc', format: '.xls', channel: 'akdf', language: 'english' },
      { assetName: "B2B sales", date: '12/04/2019', category: 'Abc', format: '.xls', channel: 'akdf', language: 'english' },
      { assetName: "B2B sales", date: '12/04/2019', category: 'Abc', format: '.xls', channel: 'akdf', language: 'english' },
      { assetName: "B2B sales", date: '12/04/2019', category: 'Abc', format: '.xls', channel: 'akdf', language: 'english' },
      { assetName: "B2B sales", date: '12/04/2019', category: 'Abc', format: '.xls', channel: 'akdf', language: 'english' },
      { assetName: "B2B sales", date: '12/04/2019', category: 'Abc', format: '.xls', channel: 'akdf', language: 'english' },
    ]
    */
  userId: any;
  AllEmployess: any = [];
  Allpartners: any = [];
  validDataFinal: any = [];
  statusData: any = [];
  AllAssetDropdown: any = [];
  resultSheets: any = [];
  result: any = [];
  uploadedData: any = [];
  damapproval: any = [];
  lang: any = [];
  fileformatf: any = [];
  filechannel: any = [];
  cat: any = [];
  validdata: any = [];
  invaliddata: any = [];
  tenantId: any = 1;
  content:any=[];
  // dateFormatNew = /^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/g.;
  @Output()unEnrol  = new EventEmitter();

  constructor(
    // private toasterService: ToasterService,
     private spinner: NgxSpinnerService, public router: Router,
    public routes: ActivatedRoute, protected webApiService: webAPIService, private datePipe: DatePipe,
    private bulkUploadService: UnEnrolCourseBundleService, private exportService: JsonToXlsxService,
    private toastr: ToastrService, private cdf: ChangeDetectorRef, private service: CourseBundleService) {
    // this.rows = this.uploadDetailsOne;
    if (localStorage.getItem('LoginResData')) {
      const userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', userData.data);
      this.userId = userData.data.data.id;
      console.log('userId', userData.data.data.id);
      this.tenantId = userData.data.data.tenantId;
    }
    this.content = this.service.coursebundledata;


    // this.getAllEmployees();
    // this.getAllparterns();
    // this.getAllNominatedEmployees();
    // this.getAllAssetDropdown();
  }

  ngOnInit() {
  }

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }

  bulkUploadAssetData: any = null;
  fileReaded: any;
  enableUpload: boolean = false;
  fileUrl: any;


  fileUpload: any;
  fileName: String = 'You can drag and drop files here to add them.';
  fileIcon: any = 'assets/img/app/profile/avatar4.png';
  selectFileTitle: any = 'No file chosen';
  cancelFile() {
    this.bulkUploadAssetData = null;
    if (this.fileUpload) {
      if (this.fileUpload.nativeElement) {
        this.fileUpload.nativeElement.value = null;
      }
    }
    this.fileName = 'You can drag and drop files here to add them.';
    this.selectFileTitle = 'No file chosen';
  }

  passParams: any;
  assetFileData: any;
  fileUploadRes: any;
  fileres: any;
  errorMsg: any;
  uploadedFile: any;


  back() {
    // this.router.navigate(['../'], { relativeTo: this.routes });
    window.history.back()
  }

  readFileUrl(event: any) {
    this.uploadedData = [];
    this.result = [];
    const validExts = new Array('.xlsx', '.xls');
    let fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
      // var toast: Toast = {
      //   type: 'error',
      //   title: 'Invalid file selected!',
      //   body: 'Valid files are of ' + validExts.toString() + 'types.',
      //   showCloseButton: true,
      //   // tapToDismiss: false,
      //   timeout: 2000,
      // };
      // this.toasterService.pop(toast);
      this.presentToast('warning', 'Valid file types are ' + validExts.toString());
      this.cancelFile();
    } else {
      // return true;
      if (event.target.files && event.target.files[0]) {
        this.fileName = event.target.files[0].name;
        // read file from input
        this.fileReaded = event.target.files[0];

        if (this.fileReaded != '' || this.fileReaded != null || this.fileReaded != undefined) {
          this.enableUpload = true;
        }

        let file = event.target.files[0];
        this.bulkUploadAssetData = event.target.files[0];
        console.log('this.bulkUploadAssetDataFileRead', this.bulkUploadAssetData);

        this.xlsxToJsonService.processFileToJson({}, file).subscribe(data => {
          this.resultSheets = Object.getOwnPropertyNames(data['sheets']);
          console.log('File Property Names ', this.resultSheets);
          let sheetName = this.resultSheets[0];
          this.result = data['sheets'][sheetName];
          console.log('dataSheet', data);

          if (this.result.length > 0) {
            this.uploadedData = this.result;
          }
        });
        var reader = new FileReader();
        reader.onload = (event: ProgressEvent) => {
          this.fileUrl = (<FileReader>event.target).result;
          /// console.log(this.fileUrl);
        };
        reader.readAsDataURL(event.target.files[0]);
      }
    }
  }
  showInvalidExcel: boolean = false;
  showTable:boolean =false;
  uploadSheet() {
    this.spinner.show()
    this.validdata = [];
    this.invaliddata = [];
    this.validDataFinal = [];
    this.statusData =[];
    console.log('this.uploadedData', this.uploadedData);
    if (this.uploadedData.length > 0) {
      // this.AllEmployess.some((item,i) => {
      this.uploadedData.forEach((data, j) => {
        if (!data.ecn) {
          this.uploadedData[j].status = 'Invalid';
          this.uploadedData[j].reason = 'ECN is required';
          return;
        } else {
          this.uploadedData[j].status = 'Valid';
          this.uploadedData[j].reason = '';
          return;
        }
      });
      console.log('this.uploadedData', this.uploadedData);
    }
    if (this.uploadedData.length > 0) {
      this.uploadedData.forEach(element => {
        if (element.status === 'Valid') {
          this.validdata.push(element);
        } else {
          this.invaliddata.push(element);
        }
      });
      const param = {
        'option': JSON.stringify(this.validdata),
        'tId': this.tenantId,
        'wfId': this.service.coursebundledata.workflowid,
      };
      this.bulkUploadService.getAllNominatedEmployees(param).then(res => {
        this.spinner.hide()
        this.statusData = res['data'];
        console.log('AllEdgeEmployessDAM:', this.statusData);
        if (this.statusData.length > 0) {
          this.invaliddata.forEach(item => {
            this.statusData.push(item);
          });
          this.invaliddata =[];
          this.statusData.forEach(element => {
            if (element.status === 'Invalid') {
              this.invaliddata.push(element);
            } else {
              this.validDataFinal.push(element);
              this.presentToast('success','Employees unenrolled successfully')
              this.unEnrol.emit();
            }
          });
          if (this.invaliddata.length > 0) {
            this.showInvalidExcel = true;
          }
          this.showTable = true;
          this.cdf.detectChanges();
          this.spinner.hide();
        }
      },resUserError => {
       // this.loader =false;
        this.spinner.hide();
        this.errorMsg = resUserError;
        // this.closeEnableDisableCourseModal();
      });
    }
  }



  exportToExcelInvalid() {
    this.exportService.exportAsExcelFile(this.invaliddata, 'Enrolment Status')
  }
}
