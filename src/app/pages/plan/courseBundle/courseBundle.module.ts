import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { SortablejsModule } from 'angular-sortablejs';
import { FilterPipeModule  } from 'ngx-filter-pipe';
import { CourseBundle } from './courseBundle';
import { CourseBundleService } from './courseBundle.service';
import { NbTabsetModule } from '@nebular/theme';
import { ThemeModule } from '../../../@theme/theme.module';
import { TranslateModule } from '@ngx-translate/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

import { rewardsComponent } from './rewards/rewards.component';
import { rewardsService } from './rewards/rewards.service';

import { GamificationComponent } from './gamification/gamification.component';
import { GamificationService } from './gamification/gamification.service';

import { enrolmentComponent } from './enrolment/enrolment.component';
import { enrolService } from './enrolment/enrolment.service';

import { engageComponent } from './engage/engage.component';
import { engageService } from './engage/engage.service';

import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { TagInputModule } from 'ngx-chips';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { UnEnrolCourseBundleComponent } from './un-enrol-coursebundle/un-enrol-coursebundle.component';
import { UnEnrolCourseBundleService } from './un-enrol-coursebundle/un-enrol-coursebundle.service';
import { JsonToXlsxService } from './un-enrol-coursebundle/json-to-xlsx.service';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgxContentLoadingModule } from 'ngx-content-loading';
import { ComponentModule } from '../../../component/component.module';
import { JoditAngularModule } from 'jodit-angular';



@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    SortablejsModule,
    ReactiveFormsModule,
    FilterPipeModule,
    NbTabsetModule,
    ThemeModule,
    TranslateModule,
    NgxDatatableModule,
    AngularMultiSelectModule,
    BsDatepickerModule.forRoot(),
    DatepickerModule.forRoot(),
    TooltipModule.forRoot(),
    TimepickerModule.forRoot(),
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    TagInputModule,
    NgxSkeletonLoaderModule,
    NgxContentLoadingModule,
    ComponentModule,
    JoditAngularModule
  ],
  declarations: [
    CourseBundle,
    rewardsComponent,
    GamificationComponent,
    enrolmentComponent,
    engageComponent,
    UnEnrolCourseBundleComponent,
    // NotFoundComponent,
  ],
  providers: [
    CourseBundleService,
    rewardsService,
    GamificationService,
    enrolService,
    engageService,
    UnEnrolCourseBundleService,
    JsonToXlsxService,
  ],
  schemas: [ 
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA ,
  ]
})
export class CourseBundleModule { }
