import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, RequestOptions, Request } from '@angular/http';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';
import { AppConfig } from '../../../app.module';
import { webApi } from '../../../service/webApi'
import { AuthenticationService } from '../../../service/authentication.service';

@Injectable()
export class CourseBundleService {


  userData: any;
  tenantId: any;
  coursebundledata: any;

  // public courseId:any;
   public menuId: number;


  // private _urlCourses:string = "/api/edge/course/getCourse";
  // private _urlCoursesWithStructure:string = "/api/edge/course/getCoursesWithStructureId";
  // private _urlCategories:string = "/api/edge/category/getCategory"
  // private _urlGetWorkflow:string = "/api/edge/course/getWorkflows";
  // private _urlAddWorkflow:string = "/api/edge/course/saveWorkflows";
  // private _urlUpdateWorkflow:string = "/api/edge/course/saveWorkflowWithStructure";
  // private _urlRemoveCourse:string = "/api/edge/course/removeCoWorkflStru";

  // private _urlWorkflowStatus:string = "/api/edge/course/updateWorkflowStatus";
  private _urlCourses: string = "/api/web/cb/getCourse";
  private _urlCoursesWithStructure: string = "/api/web/cb/getCoursesWithStructureId";
  private _urlCategories: string = "/api/web/cb/getCategory"
  private _urlGetWorkflow: string = "/api/web/cb/getWorkflows";
  private _urlAddWorkflow: string = "/api/web/cb/saveWorkflows";
  private _urlUpdateWorkflow: string = "/api/web/cb/saveWorkflowWithStructure";
  private _urlWorkflowStatus: string = "/api/web/cb/updateWorkflowStatus";
  private _manualsearchuser: string = "/api/web/cb/listallcoursebundleenrolesearch";
  private _coursebundleEnrol: string = "/api/web/cb/getallcourse_bundle_enrole";

  private _urlRemoveCourse: string = webApi.domain + webApi.url.removeworkflowcourse;
  private _urlrempbulk: string = webApi.domain + webApi.url.tempSaveBulkManEnrolCB;
  private _urlfinalbulk: string = webApi.domain + webApi.url.saveBulkManEnrolCB;
  private _urldeleteCB: string = webApi.domain + webApi.url.deleteCB;

  private add_edit_learning_pathway_url = webApi.domain + webApi.url.add_edit_learning_pathway;
  private workflow_enable_disable_enrol_url = webApi.domain + webApi.url.workflow_enable_disable_enrol;
  private workflow_courses_enable_disable_url = webApi.domain + webApi.url.workflow_courses_enable_disable;
  private _urlAreaBulEnrol: string = webApi.domain + webApi.url.area_bulk_enrol;
  request: Request;
  public data: any;

  constructor(@Inject('APP_CONFIG_TOKEN') private config: AppConfig, private _http: Http, private _http1: HttpClient, private authenticationService: AuthenticationService) {

  }

  /******* To fetch learning pathways list start ********/

  updateWorkflowStatus(workflowStatus) {
    let url = `${this.config.FINAL_URL}` + this._urlWorkflowStatus;
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    //let body = JSON.stringify(user);
    // return this._http.post(url, workflowStatus, options )
    // .map((res: Response) => res.json())
    // .catch(this._errorHandler);
    return new Promise(resolve => {
      this._http1.post(url, workflowStatus)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  getCourses() {
    if (localStorage.getItem('LoginResData')) {
      this.userData = JSON.parse(localStorage.getItem('LoginResData'));
      // console.log('userData', this.userData.data);
      // this.userId = this.userData.data.data.id;
      this.tenantId = this.userData.data.data.tenantId;
    }

    const param = {
      tId: this.tenantId,
    };

    const url = `${this.config.FINAL_URL}` + this._urlCoursesWithStructure;
    // return this._http.post(url,param)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);

    return new Promise(resolve => {
      this._http1.post(url, param)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  getCategories() {
    if (localStorage.getItem('LoginResData')) {
      this.userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', this.userData.data);
      // this.userId = this.userData.data.data.id;
      this.tenantId = this.userData.data.data.tenantId;
    }
    const param = {
      tId: this.tenantId,
    }
    const url = `${this.config.FINAL_URL}` + this._urlCategories;
    // return this._http.post(url, param)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);

    return new Promise(resolve => {
      this._http1.post(url, param)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  deleteCB(item) {
    // return this._http.post(this._urlCheckCourseCode,courseData)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);
    let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    // let headers = new Headers({ 'Authorization': 'Bearer ' + 'myToken' });
    let options = new RequestOptions({ headers: headers });
    // return new Promise(resolve => {
    //   this._http.post(this._urldeleteCB, item, options)
    // .map(res => res.json())
    //         .subscribe(data => {
    //             resolve(data);
    //         },
    //         err => {
    //             resolve('err');
    //         });
    //   });
    return new Promise(resolve => {
      this._http1.post(this._urldeleteCB, item)
        // .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  /******* To fetch learning pathways list end********/

  getWorkflows() {
    let url: any = `${this.config.FINAL_URL}` + this._urlGetWorkflow;
    // let url:any = `${this.config.DEV_URL}`+this._urlFilter;
    // let headers = new Headers({ 'Content-Type': 'application/json' });
    // let options = new RequestOptions({ headers: headers });
    // //let body = JSON.stringify(user);
    // return this._http.post(url, options )
    // .map((res: Response) => res.json())
    // .catch(this._errorHandler);

    if (localStorage.getItem('LoginResData')) {
      this.userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', this.userData.data);
      // this.userId = this.userData.data.data.id;
      this.tenantId = this.userData.data.data.tenantId;
    }
    let param = {
      tId: this.tenantId,
    }

    return new Promise(resolve => {
      this._http1.post(url, param)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  addWorkflow(workflow) {
    let url: any = `${this.config.FINAL_URL}` + this._urlAddWorkflow;
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    //let body = JSON.stringify(user);
    return this._http.post(url, workflow, options)
      .map((res: Response) => res.json())
      .catch(this._errorHandler);
  }

  updateWorkflow(workflow) {
    let url: any = `${this.config.FINAL_URL}` + this._urlUpdateWorkflow;
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    //let body = JSON.stringify(user);
    return this._http.post(url, workflow, options)
      .map((res: Response) => res.json())
      .catch(this._errorHandler);
  }

  removeCourseFromWorkflow(structureID) {
    return new Promise(resolve => {
      this._http1.post(this._urlRemoveCourse, structureID)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });


    // let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    // // let headers = new Headers({ 'Authorization': 'Bearer ' + 'myToken' });
    // let options = new RequestOptions({ headers: headers });
    // return new Promise(resolve => {
    //   this._http.post(this._urlRemoveCourse, structureID, options)
    //     .map(res => res.json())
    //     .subscribe(data => {
    //       resolve(data);
    //     },
    //       err => {
    //         resolve('err');
    //       });
    // });
  }

  searchmanualenrol(data) {
    let url: any = `${this.config.FINAL_URL}` + this._manualsearchuser;
    // let headers = new Headers({ 'Content-Type': 'application/json' });
    // let options = new RequestOptions({ headers: headers });
    // //let body = JSON.stringify(user);
    // return this._http.post(url, data, options )
    // .map((res: Response) => res.json())
    // .catch(this._errorHandler);

    return new Promise(resolve => {
      this._http1.post(url, data)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  getallenroluser(data) {
    let url: any = `${this.config.FINAL_URL}` + this._coursebundleEnrol;
    return new Promise(resolve => {
      this._http1.post(url, data)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }


  TempManEnrolBulk(item) {
    // return this._http.post(this._urlCheckCourseCode,courseData)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);
    // add authorization header with jwt token
    let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    // let headers = new Headers({ 'Authorization': 'Bearer ' + 'myToken' });
    let options = new RequestOptions({ headers: headers });
    return new Promise(resolve => {
      this._http.post(this._urlrempbulk, item, options)
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }

  finalManEnrolBulk(item) {
    // return this._http.post(this._urlCheckCourseCode,courseData)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);
    let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    // let headers = new Headers({ 'Authorization': 'Bearer ' + 'myToken' });
    let options = new RequestOptions({ headers: headers });
    return new Promise(resolve => {
      this._http.post(this._urlfinalbulk, item, options)
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  add_edit_learning_pathway(data) {
    return new Promise(resolve => {
      this._http1.post(this.add_edit_learning_pathway_url, data)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }


  enabledisable(data) {
    return new Promise(resolve => {
      this._http1.post(this.workflow_enable_disable_enrol_url, data)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
}

enabledisableCourse(data) {
  return new Promise(resolve => {
    this._http1.post(this.workflow_courses_enable_disable_url, data)
      .subscribe(data => {
        resolve(data);
      },
        err => {
          resolve('err');
        });
  });
}

areaBulkEnrol(param) {
  return new Promise(resolve => {
      this._http1.post(this._urlAreaBulEnrol, param)
          //.map(res => res.json())
          .subscribe(data => {
              resolve(data);
          },
              err => {
                  resolve('err');
              });
  });
}

_errorHandler(error: Response) {
  console.error(error);
  return Observable.throw(error || 'Server Error')
}

}
