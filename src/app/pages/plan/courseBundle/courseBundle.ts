import { NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators/takeWhile';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { CourseBundleService } from './courseBundle.service';
import { Router, NavigationStart, Routes, ActivatedRoute, Route } from '@angular/router';
import { AppService } from '../../../app.service';
import { HttpClient } from '@angular/common/http';
import { webApi } from '../../../service/webApi';
import { webAPIService } from '../../../service/webAPIService';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { Column, GridOption, AngularGridInstance, FieldType, Filters, Formatters, GridStateChange, OperatorType, Statistic } from 'angular-slickgrid';

import { SortablejsOptions } from 'angular-sortablejs';

import { Component, OnInit, OnDestroy, Directive, ViewEncapsulation, forwardRef, Attribute, OnChanges, SimpleChanges, Input, ViewChild, ViewContainerRef, ElementRef, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { ToastrService } from 'ngx-toastr';

import { LayoutService } from '../../../@core/data/layout.service';
import { NbMenuService, NbSidebarService } from '@nebular/theme';

import { FilterPipe } from 'ngx-filter-pipe';
import { DatePipe } from '@angular/common';
import { Item } from '@syncfusion/ej2-splitbuttons';
import { NgxSpinnerService } from 'ngx-spinner';
import { XlsxToJsonService } from '../../plan/users/uploadusers/xlsx-to-json-service';
import { JsonToXlsxService } from '../../coaching/participants/bulk-upload-coaching/json-to-xlsx.service';
import { Card } from '../../../models/card.model';
import { SuubHeader } from '../../components/models/subheader.model';
import { NgForm } from '@angular/forms';
import { PassService } from '../../../service/passService';
import { BrandDetailsService } from '../../../service/brand-details.service';
import { noData } from '../../../models/no-data.model';
//import { CommonFunctionsService } from '../../../service/common-functions.service';
declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'courseBundle',
  styleUrls: ['./courseBundle.scss'],
  templateUrl: './courseBundle.html',
  encapsulation: ViewEncapsulation.None,
  providers: [DatePipe]
  // template: `<router-outlet></router-outlet>`,
})
export class CourseBundle implements AfterViewInit {
  @ViewChild('wrkflwForm') workflowForm: ElementRef;
  @ViewChild('myTable') table: any;
  @ViewChild('fileUpload') fileUpload: any;
  @ViewChild(DatatableComponent) tableData: DatatableComponent;
  @ViewChild('bundleForm')userFrm: NgForm;
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;


  allow = true;

  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No Learning Pathways at this time.",
    desc:"Learning Pathways will appear after they are added by the instructor. Learning pathways are a bundle of courses aimed at at targetting a skill set for a learner.",
    titleShow:true,
    btnShow:true,
    descShow:true,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/learning-how-to-add-a-learning-pathway-workflow-course-bundle',
  }
  noEnrolData:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No enrollment at this time.",
    desc:"Learning Pathways will appear after they are added by the instructor. Learning pathways are a bundle of courses aimed at at targetting a skill set for a learner.",
    titleShow:true,
    btnShow:true,
    descShow:false,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/learning-how-to-enrol-user-to-a-learning-pathway-workflow-course-bundle',
  }
  cardModify: Card = {
    flag: 'learningpath',
    titleProp : 'wname',
    // discrption: 'description',
    image: 'wImg',
    enroll: 'enrolUsers',
    showBottomEnroll: true,
    hoverlable:   true,
    showImage: true,
    option:   true,
    eyeIcon:   true,
    copyIcon: true,
    bottomDiv:   true,
    showBottomList:   true,
    bottomTitle:   true,
    btnLabel: 'Edit',
    defaultImage:'assets/images/open-book-leaf.jpg'
  };
  header: SuubHeader  = {
    title:'Learning Pathway',
    btnsSearch: true,
    searchBar: true,
    searchtext: '',
    dropdownlabel: ' ',
    placeHolder:'Search by Learning Pathway name',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: '',
    btnName3: '',
    btnAdd: 'Add Learning Pathway',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: true,
    filter: false,
    showBreadcrumb: true,
    breadCrumbList:[
      {
        'name': 'Learning',
        'navigationPath': '/pages/learning',
      },
    ]
  };
  activeIndex: any;
  activeItem: any;
  enrolldata: any;
  count:number=12
  rows: any = [];
  // items1 = [{id:1,name:'a'},{id:2,name:'b'},{id:3,name:'c'},{id:4,name:'d'}];
  // items2 = [{id:5,name:'e'},{id:6,name:'f'},{id:7,name:'g'},{id:8,name:'h'}];
  items1: any = [];
  shortUrl = '';
  //items2: any = [];

  options: SortablejsOptions = {
    group: 'test'
  };
  labels: any = [
		{ labelname: 'ECN', bindingProperty: 'ecn', componentType: 'text' },
		{ labelname: 'FULL NAME', bindingProperty: 'fullname', componentType: 'text' },
		{ labelname: 'EMAIL', bindingProperty: 'emailId', componentType: 'text' },
		{ labelname: 'MOBILE', bindingProperty: 'phoneNo', componentType: 'text' },
		{ labelname: 'D.0.E', bindingProperty: 'enroledate', componentType: 'date' },
		{ labelname: 'ACTION', bindingProperty: 'btntext', componentType: 'button' },
	  ];
    labelsPreview: any = [
      { labelname: 'STATUS', bindingProperty: 'status', componentType: 'text' },
      { labelname: 'ECN', bindingProperty: 'ecn', componentType: 'text' },
      { labelname: 'D.0.E', bindingProperty: 'enrolDate', componentType: 'date' },
      { labelname: 'ACTION', bindingProperty: 'btntext', componentType: 'button' },
      ];
  CourseListOptions: SortablejsOptions = {
    group: {
      name: 'courseList',
      // pull: 'clone',
      put: false,
    },
    draggable: '.draggable'
  };

  BundleListOptions: SortablejsOptions = {
    group: {
      name: 'bundleList',
      put: ['courseList'],
    },
    sort: true,
    // draggable: '.draggable',
    // onEnd: this.onListItemDragEnd.bind(this),
    handle: '.drag-handle',
  };

  // draggableOptions: SortablejsOptions = {
  //   draggable: '.draggable'
  // };
  contentcourseobj: {};
  coursebundle: any = [];
  coursebundle1: any = [];
  content: any = [];
  contentList: any = [];
  content1: any = [];
  parentcat: any = [];
  catData: any;
  categoryId: any;
  errorMsg: any;
  detailsTab: boolean = true;
  modulesTab: boolean = false;
  enrolTab: boolean = false;
  engageTab: boolean = false;
  rewardsTab: boolean = false;
  gamificationTab: boolean = false;
  structures: any;
  skeleton = false;
  nopage=false
  copy: any;
  cat = {
    id: ''
  };

  list2 = [
    '6',
    '7',
    '8',
    '9',
    '10',
  ];

  list3 = [
    '11',
    '12',
  ];

  list2Options: SortablejsOptions = {
    group: {
      name: 'group2',
      put: ['group1', 'group2'],
    },
  };

  list3Options: SortablejsOptions = {
    group: {
      name: 'group2',
      pull: 'clone',
      put: ['group1', 'group2'],
      revertClone: true,
    },
  };

  dropdown: any = [{
    id: 1,
    name: 'Previous course completion'
  },
  {
    id: 2,
    name: 'End in' + ' ' + 'days'
  }

  ]
  temp = [];
  topic: any = [
    {
      tid: 1,
      tname: 'Topic 1',
      tsumm: 'url',
    },
    {
      tid: 2,
      tname: 'Topic 2',
      tsumm: 'url',
    },
    {
      tid: 3,
      tname: 'Topic 3',
      tsumm: 'url',
    },
    {
      tid: 4,
      tname: 'Topic 4',
      tsumm: 'url',
    },
  ]

  Cohortdrop: any = [
    {
      id: 1,
      name: 'B2C SAL PL E1'
    },
    {
      id: 2,
      name: 'B2C SAL PL E2'
    },
    {
      id: 3,
      name: 'B2C SAL PL E3'
    },
    {
      id: 4,
      name: 'B2C SAL PL E4'
    },
  ]

  title: any;
  formdata: any;
  profileUrl: any = 'assets/img/edgeicon.jpg';
  showEnrolpage: boolean = false;
  showBulkpage: boolean = false;
  showBulkUnenrol: boolean = false;
  isActive = false;
  // isActiveRes = false;
  isActiveTop = false;


  formdataRes = {
    rid: '',
    rName: '',
    rSummary: '',
  }

  hidesearchbar: any = true;
  removeRes: any = false;
  resOfTopic: any = [];

  interval: any;
  value: any;
  loader: any;
  workflow: any = [];

  formdatabundle: any = {
    cohortid: '',
    createduserid: '',
    datecreated: '',
    wname: '',
    wImg: '',
    wdescription: '',
    workflowid: '',
    enrolUsers: '',
    wrkflwStatus: ''
  };
  selected: any = [];
  visibility: any = [{
    id: 1,
    name: 'Show'
  },
  {
    id: 2,
    name: 'Hide'
  }];
  editId: any;
  tempWorkData: any = null;
  // topicAll:any = [];
  tempNewWorkData: any = null;
  NewWorkflowData: any;
  addWorkflowResult: any;
  tab1: any = true;
  tab2: any = false;
  workflowData: any;
  workflowResData: any;
  userLoginData: any;
  workflowListData: any;

  search: any = {
    // inputText:'',
    wname: ''
  }
  item: any = {
    fullname: '',
  }
  course: any = {
    id: ''
  }
  dublicate: any;
  dropDisabled: any;
  inputDisabled: any;
  formattedPara: any;
  workflowAddData: any;
  updateResult: any;

  // @ViewChild("listContainer") container:any;
  // @Input() rowHeight:number = 40;
  // @Input() items:any[];
  // itemsInView: any[];
  // startIndex:number = 192;
  // endIndex:number = 0;
  showdata: any = [];
  addCourseBundle: boolean = false;
  editCourseBundle: boolean = false;
  input_search_string: string = '';
  input_search_category_flag: boolean = false;
  tempContentList = [];
  loadSoratbleSection: boolean = false;
  tenantId;
  private xlsxToJsonService: XlsxToJsonService = new XlsxToJsonService();
  backFlag: number = 1;
  searchText: any;
  currentBrandData: any;
  enable: boolean;
  enableDisableModal: boolean;
  constructor(private spinner: NgxSpinnerService, private filterPipe: FilterPipe,
    private sidebarService: NbSidebarService, private layoutService: LayoutService,
     private exportService: JsonToXlsxService ,
    protected service: CourseBundleService,
    // private toasterService: ToasterService,
    private commonFunctionService: CommonFunctionsService,
     protected webApiService: webAPIService,
    public brandService: BrandDetailsService,
    private passService: PassService,
    private themeService: NbThemeService, private router: Router, private AppService: AppService,
    public cdf: ChangeDetectorRef, private datePipe: DatePipe, private toastr: ToastrService,
    //private commonFunctionService: CommonFunctionsService,
    private http1: HttpClient) {
    // this.dataset = this.prepareData();
    this.showdata = this.AppService.getfeatures();
    if (this.showdata) {
      for (let i = 0; i < this.showdata.length; i++) {
        if (this.showdata[i] == 'FACB') {
          this.addCourseBundle = true;
        }
        if (this.showdata[i] == 'FECB') {
          this.editCourseBundle = true;
        }
      }
    }
    // this.loader = true;

    if (localStorage.getItem('LoginResData')) {
      this.userLoginData = JSON.parse(localStorage.getItem('LoginResData'));
      this.tenantId = this.userLoginData.data.data.tenantId;
    }
    console.log('user Login Data :-', this.userLoginData);
    // this.input_search_string = null;
  }

  ngAfterViewInit() {
    this.getCourses();
    this.getCategories();
    this.getWorkflows();
    this.getHelpContent();
  }

  getWorkflows() {
    this.skeleton = false;
    this.service.getWorkflows()
      .then(resUserData => {
        console.log(resUserData);
        this.coursebundle1 = resUserData['data'];
        // this.coursebundle = resUserData['data'];
        this.coursebundle = [];
        this.addItems(0,this.sum,'push',this.coursebundle1);
        console.log('Workflows / Course Bundle', this.coursebundle1);
        if (this.coursebundle1 == undefined){
          this.nopage=true
          this.skeleton = true;
        }
        else if(this.coursebundle1.length) {
          this.workflowListData = resUserData['data'][0].workflowS;
        }
        else{
          this.nopage=true
          this.skeleton = true;
        }
        // console.log('Workflows / Course Bundle', this.coursebundle);
        this.skeleton = true;
      },
        resUserError => {
          this.skeleton = true;
          this.nopage=true
          this.errorMsg = resUserError
        });
  }

  getCourses() {
    this.service.getCourses()
      .then(rescompData => {
        this.loader =false;
        // this.loader =false;
        this.loadSoratbleSection = true;
        this.content1 = rescompData['data'][0];
        this.items1 = rescompData['data'][0];
        this.prepareDraggableData(this.content1);
        // this.prepareListData();
        // console.log('Course Result',this.content1);
      },
        resUserError => {
          this.loader =false;
          this.errorMsg = resUserError;
        });
  }

  getCategories() {
    this.service.getCategories()
      .then(rescompData => {
        console.log('Category Result', rescompData);
        this.loader =false;
        // this.parentcat = rescompData.data[0];
        this.parentcat = rescompData['data'][0];
        // if (this.items2.length > 0) {
        //   for (let i = 0; i < this.items2.length; i++) {
        //     if (this.items2[i].visible === 1) {
        //       this.parentcat.push(this.items2[i])
        //     }
        //   }
        // }
        // console.log('Category Result', this.parentcat);
      },
        resUserError => {
          this.loader =false;
          this.errorMsg = resUserError
        });
  }

  // itemsInView = this.coursebundle1.slice(this.startIndex, this.endIndex);

  // ngOnInit() {
  //     this.input_search_string = null;
  // }
  // refresh() {
  //     let scrollTop = this.container.nativeElement.scrollTop;
  //     let height = this.container.nativeElement.clientHeight;
  //     this.startIndex = Math.floor(scrollTop / this.rowHeight);
  //     this.endIndex = Math.ceil((scrollTop + height) / this.rowHeight);
  //     if (this.items) {
  //         this.coursebundle = this.coursebundle1.slice(this.startIndex, this.endIndex);
  //     }
  // }


  clearSearch() {
    if (this.searchText.length>=3) {
      this.search = {};
      // this.sum = 50;
      // this.coursebundle1=[]
      // this.addItems(0, this.sum, 'push', this.coursebundle1);
      this.getWorkflows();
      this.nopage=false
    }
    else{
      this.search={};
    }
  }

  // serachBundle() {
  //   this.coursebundle = [];
  //   if (this.search.wname == undefined || this.search.wname == '') {
  //     this.coursebundle = this.coursebundle1;
  //   } else {
  //     for (var i = 0; i < this.coursebundle1.length; i++) {
  //       if (this.search.wname.toUpperCase() == this.coursebundle1[i].wname) {
  //         this.coursebundle.push(this.coursebundle1[i]);
  //       }
  //     }
  //   }
  // }

  drop(item) {
    item.enable = true;
    item.value1 = false;
    item.value2 = false;
    item.inputdata = '';
    item.validNumber = true;
    item.minValue = 0;
    item.courseVisible = 1;

    console.log('Content', item);
    console.log(this.contentList);

    setTimeout(() => {
      console.log(this.contentList);
      this.doropserch();
      this.cdf.detectChanges();
      // for(let i=0; i<this.content.length; i++){
      //   if(item.courseid == this.content[i].courseid && item.draggable == true){
      //     this.content[i].draggable = false;
      //   }else{
      //     this.content[i].draggable = true;
      //   }
      // }
      // item.draggable = false;
      // this.content.push(item);

    }, 400);
  }
  identify(index, item) {
    return item.courseid;
  }
  drop2(item) {
    // this.value =
    item.courseVisible = 1;
    console.log('Content List', item);
    console.log(this.contentList);
    setTimeout(() => {
      console.log(this.contentList);
      this.cdf.detectChanges();
    })
    // for(let i=0; i<this.contentList.length; i++){

    // }
  }
  Disbaledata: any;
  disablevaluechnage(i, workflowData) {
    this.Disbaledata = workflowData;
    this.Disbaledata.wrkflwStatus = this.Disbaledata.wrkflwStatus === 'active' ? 'inactive' : 'active';
    //  this.Disbaledata.visible=this.Disbaledata.visible==1?0:1
    // this.coursebundle[i].wrkflwStatus = this.coursebundle[i].wrkflwStatus === 'active' ? 'inactive' : 'active';
  }

  updateWorkflowStatus(workflowData, i) {
    var workflowStatusData = {
      workflowid: workflowData.workflowid,
      wrkflwStatus: workflowData.wrkflwStatus === 'active' ? 'inactive' : 'active'
    }

    this.service.updateWorkflowStatus(workflowStatusData)
      .then(rescompData => {
        this.skeleton = true;
        const updateResult = rescompData;
        console.log('Workflow Update Result', updateResult);
        if (updateResult['type'] == true) {

          this.toastr.success('Workflow updated', 'Success', {
            closeButton: false
          });
          this.disablevaluechnage(i, workflowData)
        } else {

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        }
      },
        resUserError => {
          this.skeleton = true;
          this.errorMsg = resUserError
        });
  }

  clickTodisable(workflowData) {
    this.spinner.show()
    var workflowStatusData = {
      workflowid: workflowData.workflowid,
      wrkflwStatus: workflowData.wrkflwStatus,
    }
    var msg

    if(workflowData.courseVisible == 0) {
      workflowData.courseVisible =1;
      msg ='Workflow Enabled SuccessFully'
      // this.toastr.success('Workflow Enabled Successfully', 'Success', {
      //   closeButton: false
      // });
    } else {
      workflowData.courseVisible =0;
      msg ='Workflow Disabled SuccessFully'
      // this.toastr.success('Workflow Disabled Successfully', 'Success', {
      //   closeButton: false
      // });
    }

    this.service.updateWorkflowStatus(workflowStatusData)
      .then(rescompData => {
        this.skeleton = true;
        const updateResult = rescompData;
        console.log('Workflow Update Result', updateResult);
        if (updateResult['type'] == true) {
          this.spinner.hide()

          this.toastr.success(msg, 'Success', {
            closeButton: false
          });

          // this.getWorkflows();

        } else {
          this.spinner.hide()

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        }
      },
        resUserError => {
          this.skeleton = true;
          this.errorMsg = resUserError
        });
        this.closeEnableDisableModal();
  }
  // showWorklfow(event, workflow) {
  //   workflow.wrkflwStatus = 'active';
  //   // Enable #wrkflwForm
  //   // jQuery( "#wrkflwForm" ).prop( "disabled", false );
  //   // jQuery( "#wrkflwForm" ).attr("disabled", false).off('click');
  //   console.log('Workflow Form ', this.workflowForm);
  //   this.addRemoveClassElement(this.workflowForm, "avoid-clicks");
  //   this.updateWorkflowStatus(workflow);
  // }

  // hideWorklfow(event, workflow) {
  //   workflow.wrkflwStatus = 'inactive';
  //   // Disable #wrkflwForm
  //   // jQuery( "#wrkflwForm" ).prop( "disabled", true );
  //   // jQuery( "#wrkflwForm" ).attr("disabled", true).off('click');
  //   console.log('Workflow Form ', this.workflowForm);
  //   this.addRemoveClassElement(this.workflowForm, "avoid-clicks");
  //   this.updateWorkflowStatus(workflow);
  // }

  // addRemoveClassElement(element: any, classname) {
  //   if (element) {
  //     var elementContent = element.nativeElement;
  //     if (elementContent.classList.length > 0) {
  //       var index = elementContent.className.indexOf(classname);
  //       if (index > -1) { //exist
  //         elementContent.classList.remove(classname);
  //       } else {
  //         elementContent.classList.add(classname);
  //       }
  //     }
  //   }
  // }

  addRemoveClassItem(element: any, classname, workflow) {
    if (element) {
      var elementContent = element.nativeElement;
      if (elementContent.classList.length > 0) {
        //var index = elementContent.className.indexOf(classname);
        if (workflow != null) {
          if (workflow.wrkflwStatus === 'active') { //exist
            elementContent.classList.remove(classname);
          } else {
            elementContent.classList.add(classname);
          }
        }
      }
    }
  }

  // enableeye(item) {
  //   // var value =item.enable;
  //   // console.log('$event',$event);
  //   for (let i = 0; i < this.contentList.length; i++) {
  //     if (item.courseid == this.contentList[i].courseid) {
  //       item.enable = !item.enable;
  //     }
  //   }
  // }

  // get(item) {
  //   for (let i = 0; i < this.contentList.length; i++) {
  //     if (item.id === this.contentList[i].id) {
  //       // item.value = true
  //       this.contentList[i].value = '';
  //     }
  //   }
  //   console.log('Content List', this.contentList);
  // }
  searchcourse(event) {
    const val = event.target.value.toLowerCase();

    // this.allEnrolUser( this.courseDataService.data.data)

    this.temp = [...this.content];
    console.log(this.temp);
    // filter our data
    const temp = this.temp.filter(function (d) {
      return String(d.ecn).toLowerCase().indexOf(val) !== -1 ||
        d.fullname.toLowerCase().indexOf(val) !== -1 ||
        // d.emailId.toLowerCase().indexOf(val) !== -1 ||
        // d.enrolmode.toLowerCase().indexOf(val) !== -1 ||
        // d.enrolDate.toLowerCase().indexOf(val) !== -1 ||
        // d.phoneNo.toLowerCase().indexOf(val) !== -1 ||
        // String(d.enrolmode).toLowerCase().indexOf(val) !== -1 ||
        // // d.mode.toLowerCase() === val || !val;
        !val
    });

    // update the rows
    this.content = [...temp];
    console.log(this.content);
    this.cdf.detectChanges();
    // Whenever the filter changes, always go back to the first page
  }



  doropserch() {
    this.content = [];
    this.contentcourseobj = {};
    if (!this.cat.id) {
      this.input_search_category_flag = false;
      // this.content = this.content1;
      // this.content1[i].draggable = true;
      this.onSearch(this.input_search_string);

      // this.onSearch(this.input_search_string);
      // console.log(this.content);
    } else {
      this.input_search_category_flag = true;
      for (var i = 0; i < this.content1.length; i++) {
        if (this.cat.id == this.content1[i].category) {
          // this.contentcourseobj[this.content1[i].courseid] = true;
          this.content1[i].draggable = true;
          let exisits = false;
          this.contentList.forEach((value, index) => {
            if (this.content1[i].courseid === value.courseid) {
              // value.draggable = true;
              // this.content1[i].draggable = false;
              // this.contentcourseobj[this.content1[i].courseid] = false;
              exisits = true;
            }
          });
          if (!exisits) {
            this.content.push(this.content1[i]);
          }
        }
      }
      this.tempContentList = this.content;
      this.onSearchByCategory(this.input_search_string);
    }
  }
  getContentCourseValue(id) {
    if (!id) { return true; }

    if (this.contentcourseobj && String(this.contentcourseobj[id])) {
      return this.contentcourseobj[id];
    } else {
      return true;
    }
  }
  prepareDraggableData(data1) {
    // this.content1 = this.items1;
    // for (let i = 0; i < this.content1.length; i++) {
    //   if (this.content1[i].draggable || this.content1[i].draggable == 1) {
    //     this.content1[i].draggable = true;
    //   } else {
    //     this.content1[i].draggable = false;
    //   }
    // }
    // console.log(data1)
    for (let i = 0; i < data1.length; i++) {
      if (data1[i].draggable || data1[i].draggable == 1) {
        data1[i].draggable = true;
      } else {
        data1[i].draggable = false;
      }
      this.prepareListData(data1);
    }
  }

  prepareListData(data1) {
    this.content = [];
    if (!this.categoryId) {
      // this.content = this.content1;
      this.content = data1;
      // console.log(this.content);
      // console.log('All Course length ', this.content.length);
    } else {
      // for (let i = 0; i < this.content1.length; i++) {
      //   if (this.categoryId == this.content1[i].category) {
      //     this.content.push(this.content1[i]);
      //   }
      // }
      for (let i = 0; i < data1.length; i++) {
        if (this.categoryId == data1[i].category) {
          this.content.push(data1[i]);
        }
      }
      console.log('Filtered Course length - ', this.content.length);
    }
  }

  // getLength() {
  //   if (this.coursebundle.length > 1) {
  //     this.removeRes = true;
  //   } else {
  //     this.removeRes = false;
  //   }
  //   this.resOfTopic = [];
  // }

  onCheck1Change(event: any, item: any) {
    this.dropDisabled = !this.dropDisabled;
    // console.log('Check 1 event', event);
    if (event == true && item.previousCourseid == 0) {
      for (let i = 0; i < this.contentList.length; i++) {
        if (item.courseid == this.contentList[i].courseid) {
          var itemIndex = i;
          var prevItem = this.contentList[i - 1];
          item.previousCourseid = prevItem.courseid;
        }
      }
    }

    for (let i = 0; i < this.contentList.length; i++) {
      if (item.courseid == this.contentList[i].courseid) {
        item.value1 = event;
      }
    }

    if (item.value1 == true && item.previousCourseid != 0) {
      this.enableSubmit = true;
    } else if (item.value1 == false && item.previousCourseid != 0) {
      this.enableSubmit = true;
      item.previousCourseid = 0;
    } else {
      this.enableSubmit = false;
    }

  }

  onCheck2Change(event: any, item: any) {
    console.log('Check 1 event', event);
    for (let i = 0; i < this.contentList.length; i++) {
      if (item.courseid == this.contentList[i].courseid) {
        item.value2 = event;
      }
    }

    if (event == false && item.cascade_days >= 0) {
      for (let i = 0; i < this.contentList.length; i++) {
        if (item.courseid == this.contentList[i].courseid) {
          item.cascade_days = 0;
          item.validNumber = true;
          this.enableSubmit = true;
        }
      }
    }

    if (event == true && item.minValue >= 0) {
      for (let i = 0; i < this.contentList.length; i++) {
        if (item.courseid == this.contentList[i].courseid) {
          item.cascade_days = item.minValue;
          // item.validNumber = true;
          // this.enableSubmit = true;
        }
      }
    }
  }
  // onSelectEnrolData({ selected }) {
  //   console.log('Select Event', selected);
  //   // console.log('Select Data', this.selected);

  //   this.selected.splice(0, this.selected.length);
  //   this.selected.push(...selected);
  // }
  // activeButtonTop(workflowData, id) {
  //   console.log(workflowData, id);
  //   this.service.coursebundledata = workflowData;
  //   if (this.service.coursebundledata) {
  //     this.allmanualenrol(this.service.coursebundledata);
  //   }
  //   this.hidesearchbar = false;
  //   if (workflowData == undefined) {
  //     var workflowLength = this.coursebundle.length + 1;
  //     // var cohortid = this.coursebundle.cohortid + 1;
  //     // var workflowid = this.coursebundle.workflowid + 1;
  //     // var cid = workflowLength;
  //     // // var cid = cohortid;
  //     // // var wid = workflowid;
  //     // var clname = 'Course Bundle ' + cid;
  //     // var csname = 'CB' + ;
  //     this.formdatabundle = {
  //       cohortid: '',
  //       createduserid: this.userLoginData.data.data.id,
  //       datecreated: '',
  //       wname: '',
  //       wdescription: '',
  //       workflowid: this.coursebundle.workflowid + 1,
  //       wrkflwStatus: '',
  //       enrolUsers: '',
  //       tenantId: this.userLoginData.data.data.tenantId,
  //     }
  //     this.isActiveTop = true;
  //     this.isActive = true;
  //     this.addCourseBundle = false;
  //   }
  //   else {
  //     var workflowdata = workflowData;
  //     var workflowListData = workflowData.workflowS;
  //     this.formdatabundle = {
  //       cohortid: workflowdata.cohortid,
  //       createduserid: workflowdata.createduserid,
  //       datecreated: workflowdata.datecreated,
  //       wname: workflowdata.wname,
  //       wdescription: workflowdata.wdescription,
  //       workflowid: workflowdata.workflowid,
  //       wrkflwStatus: workflowdata.wrkflwStatus,
  //       enrolUsers: workflowdata.enrolUsers
  //     }


  //     console.log(this.formdatabundle);
  //     this.prepareDraggableData();
  //     // this.prepareListData();

  //     for (let i = 0; i < this.content.length; i++) {
  //       if (this.content[i].draggable == false) {
  //         this.content[i].draggable = true;
  //       }
  //       // else{
  //       //   this.content1[i].draggable = false;
  //       // }
  //     }

  //     if (workflowListData != null || workflowListData != undefined) {
  //       this.contentList = workflowListData;
  //       for (let i = 0; i < this.contentList.length; i++) {
  //         this.contentList[i].value1 = this.contentList[i].previousCourseid === 0 ? false : true;
  //         this.contentList[i].value2 = this.contentList[i].cascade_days === 0 ? false : true;
  //         this.contentList[i].validNumber = true;
  //         if (this.contentList[i].value1 == true) {
  //           this.dropDisabled = false;
  //         } else {
  //           this.dropDisabled = true;
  //         }
  //         if (this.contentList[i].value2 == true) {
  //           this.inputDisabled = false;
  //         } else {
  //           this.inputDisabled = true;
  //         }
  //       }

  //       for (let k = 0; k < this.contentList.length; k++) {
  //         for (let j = 0; j < this.content.length; j++) {
  //           if (this.contentList[k].courseid == this.content[j].courseid && this.content[j].draggable == true) {
  //             this.content[j].draggable = false;
  //             // console.log('updated content',this.content[j]);
  //           }
  //           // else if(this.content[j].draggable == false){
  //           //   this.content[j].draggable = true;
  //           //   // console.log('updated content',this.content[j]);
  //           // }
  //         }
  //       }
  //       // this.contentList = this.getDropdownItems();
  //     } else {
  //       this.contentList = [];
  //     }

  //     if (workflowdata != this.tempWorkData) {
  //       if (this.isActive == true) {
  //         // this.isActiveRes = false;
  //         this.isActiveTop = true;
  //         this.isActive = true;
  //       } else if (this.isActiveTop == false) {
  //         // this.isActive = !this.isActive;
  //         this.isActive = true;
  //         this.isActiveTop = true;
  //       }
  //     } else {
  //       workflowdata = null;
  //       // this.isActive = !this.isActive;
  //       this.isActive = false;
  //       this.isActiveTop = false;

  //       // this.sidebarService.toggle(true, 'menu-sidebar');
  //       // this.layoutService.changeLayoutSize();
  //     }
  //     this.tempWorkData = workflowdata;
  //     this.addRemoveClassItem(this.workflowForm, 'avoid-clicks', workflowdata);
  //   }
  // }

  // addtopic() {
  //   var workflowLength = this.coursebundle.length + 1;
  //   var cohortid = this.coursebundle.cohortid + 1;
  //   var workflowid = this.coursebundle.workflowid + 1;
  //   var cid = workflowLength;
  //   // var cid = cohortid;
  //   // var wid = workflowid;
  //   var clname = 'Course Bundle ' + cid;
  //   var csname = 'CB' + cid;
  //   var data = {
  //     cohortid: cid,
  //     createduserid: '',
  //     datecreated: '',
  //     wname: clname,
  //     wdescription: csname,
  //     workflowid: cid,
  //     wrkflwStatus: 'active',
  //     enrolUsers: 0,

  //   };
  //   this.coursebundle.push(data);
  //   this.activeButtonTop(data, 0);
  // }

  // addWorkflow() {
  //   // dataBound, id
  //   this.hidesearchbar = false;
  //   if (localStorage.getItem('LoginResData')) {
  //     this.userLoginData = JSON.parse(localStorage.getItem('LoginResData'));
  //   }
  //   //   if(data == undefined){
  //   //     var workflowLength = this.coursebundle.length + 1;
  //   //     var cohortid = this.coursebundle.cohortid + 1;
  //   //     var workflowid = this.coursebundle.workflowid + 1;
  //   //     var cid = workflowLength;
  //   //     var clname = 'Course Bundle ' + cid;
  //   //     var csname = 'CB' + cid;
  //   //   }
  //   // console.log(this.isActiveTop);
  //   console.log('user Login Data :-', this.userLoginData);

  //   var workflowLength = this.coursebundle.length + 1;
  //   var cohortid = this.coursebundle.cohortid + 1;
  //   var workflowid = this.coursebundle.workflowid + 1;
  //   var cid = workflowLength;
  //   // var cid = cohortid;
  //   // var wid = workflowid;
  //   var clname = 'Course Bundle ' + cid;
  //   var csname = 'CB' + 1;

  //   this.NewWorkflowData = {
  //     cohortid: cid,
  //     createduserid: '',
  //     datecreated: '',
  //     wname: clname,
  //     wdescription: csname,
  //     workflowid: cid,
  //     wrkflwStatus: 'active',
  //     // workfName: this.formdatabundle.longname,
  //     // worksName: this.formdatabundle.longname,
  //     userId: this.userLoginData.data.data.id,
  //     //   actionId : 0
  //     tenantId: this.userLoginData.data.data.tenantId,
  //   }
  //   console.log(this.NewWorkflowData);

  //   this.workflowData = {
  //     cohortid: this.formdatabundle.cohortid,
  //     createduserid: this.formdatabundle.createduserid,
  //     datecreated: this.formdatabundle.datecreated,
  //     wname: this.formdatabundle.wname,
  //     wdescription: this.formdatabundle.wdescription,
  //     workflowid: this.formdatabundle.workflowid,
  //     workfName: this.formdatabundle.wname,
  //     userId: this.userLoginData.data.data.id,
  //     // actionId : 0
  //   };
  //   // this.isActiveTop  = true;
  //   // this.isActive = true;
  //   // this.addCourseBundle = false;
  //   this.addWorkflows(this.NewWorkflowData);
  // }
  // addWorkflow123() {
  //   if (localStorage.getItem('LoginResData')) {
  //     this.userLoginData = JSON.parse(localStorage.getItem('LoginResData'));
  //   }
  //   console.log(this.userLoginData);
  //   this.isActiveTop = true;
  //   this.isActive = true;
  //   this.addCourseBundle = false;
  // }

  lastInsertedId: any;
  // addWorkflows(workflowData) {
  //   console.log(workflowData)
  //   this.loader = true;
  //   this.service.addWorkflow(workflowData)
  //     .subscribe(rescompData => {
  //       this.loader = false;
  //       this.lastInsertedId = rescompData.data[0][0].ID;
  //       this.addWorkflowResult = rescompData.data1;
  //       this.coursebundle = rescompData.data1;
  //       this.workflowListData = rescompData.data1[0].workflowS;
  //       console.log('Add Workflow Result', this.coursebundle);
  //       if (this.lastInsertedId != 0 && this.NewWorkflowData != null) {
  //         //this.getTopics(this.cData.id);
  //         this.newWorkflowAdd();
  //         // var toast: Toast = {
  //         //   type: 'success',
  //         //   title: 'Workflow created!',
  //         //   body: 'New workflow created successfully.',
  //         //   showCloseButton: true,
  //         //   // tapToDismiss: false,
  //         //   // timeout: 0,
  //         //   timeout: 2000
  //         //   // onHideCallback: () => {
  //         //   //     this.router.navigate(['/pages/plan']);
  //         //   // }
  //         // };
  //         // this.toasterService.pop(toast);

  //         this.toastr.success('New workflow created', 'Success', {
  //           closeButton: false
  //         });
  //       } else {
  //         // var toast: Toast = {
  //         //   type: 'error',
  //         //   title: 'Cohort already exists!',
  //         //   body: '',
  //         //   showCloseButton: true,
  //         //   // tapToDismiss: false,
  //         //   // timeout: 0
  //         //   timeout: 2000
  //         //   // onHideCallback: () => {
  //         //   //     this.router.navigate(['/pages/plan/users']);
  //         //   // }
  //         // };
  //         // this.toasterService.pop(toast);

  //         this.toastr.error('Cohort already exists!', 'Error', {
  //           timeOut: 0,
  //           closeButton: true
  //         });
  //         this.closeFormTop();
  //       }
  //     },
  //       resUserError => {
  //         // this.loader =false;
  //         this.errorMsg = resUserError
  //       });
  // }

  // newWorkflowAdd() {
  //   for (let i = 0; i < this.addWorkflowResult.length; i++) {
  //     if (this.addWorkflowResult[i].workflowid == this.lastInsertedId) {
  //       this.formdatabundle = {
  //         cohortid: this.addWorkflowResult[i].cohortid,
  //         createduserid: this.addWorkflowResult[i].createduserid,
  //         datecreated: this.addWorkflowResult[i].datecreated,
  //         wname: this.addWorkflowResult[i].wname,
  //         wdescription: this.addWorkflowResult[i].wdescription,
  //         workflowid: this.addWorkflowResult[i].workflowid,
  //         wrkflwStatus: this.addWorkflowResult[i].wrkflwStatus,
  //         enrolUsers: this.addWorkflowResult[i].enrolUsers,
  //         tenantId: this.userLoginData.data.data.tenantId,
  //       }
  //       this.service.coursebundledata = this.formdatabundle;
  //     }
  //     if (this.service.coursebundledata) {
  //       this.allmanualenrol(this.service.coursebundledata);
  //     }
  //   }

  //   for (let i = 0; i < this.content.length; i++) {
  //     if (this.content[i].draggable == false) {
  //       this.content[i].draggable = true;
  //     }
  //     // else{
  //     //   this.content1[i].draggable = false;
  //     // }
  //   }

  //   this.contentList = [];

  //   var NewWorkData = this.NewWorkflowData;

  //   if (NewWorkData != this.tempNewWorkData) {
  //     if (this.isActive == true) {
  //       // this.isActiveRes = false;
  //       this.isActiveTop = true;
  //       this.isActive = true;
  //     } else if (this.isActiveTop == false) {
  //       // this.isActive = !this.isActive;
  //       this.isActive = true;
  //       this.isActiveTop = true;
  //     }
  //   } else {
  //     NewWorkData = null;
  //     this.isActive = false;
  //     this.isActiveTop = false;
  //   }
  //   this.tempNewWorkData = NewWorkData;
  // }

  updateTopName(formdatabundle) {
    // console.log('formdataTopic',formdataTopic);
    for (let i = 0; i < this.coursebundle.length; i++) {
      if (formdatabundle.workflowid == this.coursebundle[i].workflowid) {
        this.coursebundle[i].wname = formdatabundle.wname;
      }
    }
  }
  ngOnDestroy(){
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0){
      e1[0].classList.add('with-scroll');
    }
  }
  ngAfterContentChecked() {
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0)
    {
      e1[0].classList.remove('with-scroll');
    }

   }
  closeFormTop() {
    this.addCourseBundle = true;
    this.isActiveTop = false;
    this.isActive = false;
    this.tempWorkData = null;
    this.getWorkflows();
    this.hidesearchbar = true;
    // this.sidebarService.toggle(true, 'menu-sidebar');
    // this.layoutService.changeLayoutSize();
    this.cat.id = '';
    // this.categoryId = '';
    this.service.coursebundledata = null;
    this.showGenrateButton = false;
    const dempData = {
      tabTitle: 'Details',
    };
    this.selectedTab(dempData);
  }
  backbread(){
    this.isActiveTop = false;
    const dempData = {
      tabTitle: 'Details',
    };
    this.header = {
      title:'Learning Pathway',
      btnsSearch: true,
      searchBar: true,
      placeHolder:'Search by Learning Pathway name',
      btnAdd: 'Add Learning Pathway',
      btnBackshow: true,
      btnAddshow: true,
      showBreadcrumb: true,
      isSame:true,
      breadCrumbList:[
        {
          'name': 'Learning',
          'navigationPath': '/pages/learning',
        },
      ]
    };
  }
  back() {
    if(this.backFlag==2){
      this.header= {
        title:this.service.coursebundledata?this.service.coursebundledata.wname:"Add learning Pathway",
        btnsSearch: true,
        btnName3: 'Enrol',
        btnName4: 'Bulk Enrol',
        btnName5: 'Bulk Unenrol',
        btnName3show: true,
        btnName4show: true,
        btnName5show: true,
        btnBackshow: true,
        showBreadcrumb: true,
        // isSame:true,
        breadCrumbList:[
          {
            'name': 'Learning',
            'navigationPath': '/pages/learning',
          },
          {
            'isSame':true,
            'name': 'Learning Pathway',
            'navigationPath': '/pages/plan/courseBundle'
          },
        ]
      };
      this.backFlag=1;
      this.showEnrolpage = false;
      this.showBulkpage = false;
      this.showBulkUnenrol = false;
      this.file = [];
      this.preview = false;
      this.fileName = 'Click here to upload an excel file to enrol '+this.currentBrandData.employee.toLowerCase()+ 'to this course';
      this.enableUpload = false;
    }
else{
  console.log(this.isActive)
    if(this.isActiveTop == true && this.backFlag==1) {
    this.getWorkflows();

      this.isActiveTop = false;
      const dempData = {
        tabTitle: 'Details',
      };
      this.header = {
        title:'Learning Pathway',
        btnsSearch: true,
        searchBar: true,
        placeHolder:'Search by Learning Pathway name',
        btnAdd: 'Add Learning Pathway',
        btnBackshow: true,
        btnAddshow: true,
        showBreadcrumb: true,
        isSame:true,
        breadCrumbList:[
          {
            'name': 'Learning',
            'navigationPath': '/pages/learning',
          },
        ]
      };
      // this.getWorkflows();
    }
     else if(this.backFlag==1) {
      this.router.navigate(['/pages/learning']);
    }
  }


    // this.sidebarService.toggle(true, 'menu-sidebar');
    // this.layoutService.changeLayoutSize();
  }

  submit() {
    if (localStorage.getItem('LoginResData')) {
      this.userLoginData = JSON.parse(localStorage.getItem('LoginResData'));
    }
    console.log('user Login Data :-', this.userLoginData);

    this.workflowData = {
      cohortid: this.formdatabundle.cohortid,
      createduserid: this.formdatabundle.createduserid,
      datecreated: this.formdatabundle.datecreated,
      wname: this.formdatabundle.wname,
      wdescription: this.formdatabundle.wdescription,
      workflowid: this.formdatabundle.workflowid,
      workfName: this.formdatabundle.wname,
      enrolUsers: this.formdatabundle.enrolUsers,
      wrkflwStatus: this.formdatabundle.wrkflwStatus,
      userId: this.userLoginData.username
    };

    // this.router.navigate(['/pages/plan']);
    this.loader = true;

    this.service.addWorkflow(this.workflowData)
      .subscribe(resUserData => {
        this.loader = false;
        this.workflowResData = resUserData.data[0];
        console.log('Workflows Res', this.workflowResData);

        this.toastr.success('New workflow created', 'Success', {
          closeButton: false
        });
      },
        resUserError => {
          this.loader = false;
          this.errorMsg = resUserError
        });
  }
  onSearchByCategory(val) {
    const templist = this.tempContentList;
    console.log('Content Length by Category 1 - ', this.content.length);
    this.content = templist.filter(function (d) {
      return String(d.fullname).toLowerCase().indexOf(String(val).toLowerCase()) !== -1 ||
        // d.tags.toLowerCase().indexOf(val) !== -1 ||
        !String(val).toLowerCase();
    });
    console.log('Content Length by Category 2 - ', this.content.length);
    console.log('Content List Length by Category - ', this.contentList.length);
  }

  onSearch(val) {
    this.content = [];
    let courseIds = {};
      for (var i = 0; i < this.contentList.length; i++) {
        courseIds[this.contentList[i].courseid] = true;
      }
      // if (this.contentList.length == 0) {
      //   this.content = this.content1;
      // }
      console.log('Content Length 1 - ', this.content.length);
      let templist = [];
      templist = this.content1.filter(function (d) {
        return String(d.fullname).toLowerCase().indexOf(String(val).toLowerCase()) !== -1 ||
          // d.tags.toLowerCase().indexOf(val) !== -1 ||
          !String(val).toLowerCase();
      });
      templist.forEach((value, index) => {
        if (!courseIds[value.courseid]) {
          // value.draggable = true;
          // this.content1[i].draggable = false;
          // this.contentcourseobj[this.content1[i].courseid] = false;
          this.content.push(value);
        }
      });

    console.log('Content Length 2 - ', this.content.length);
    console.log('Content List Length - ', this.contentList.length);
  }
  onsearch(event){
    console.log(event,"event")
    this.formdatabundle.wname =  event.target.value
    this.search.wname = event.target.value

  }
  private ValueId: number = 0;
  selectedFilterOption = [];
  callTypeSelected(id: any, index: any) {
    this.ValueId = parseInt((id.srcElement || id.target).value);
    console.log('Selected ValueId ', this.ValueId);
    if (this.ValueId == NaN) {
      this.enableSubmit = false;
    } else {
      this.enableSubmit = true;
    }
  }

  deleteResult: any;
  removeCourse(courseData, index) {
    var structureId = courseData.structureid;
    var courseId = courseData.courseid;
    var courseName = courseData.fullname;
    var srtucture = {
      id: structureId,
      tId: this.tenantId,
    }

    // for (let i = 0; i < this.content.length; i++) {
    //   if (courseId == this.content[i].courseid && this.content[i].draggable == false) {
    //     this.content[i].draggable = true;
    //   }
    //   // else{
    //   //   this.content1[i].draggable = false;
    //   // }
    // }

    if (structureId && structureId != 0 && !this.dublicate) {
      this.loader = true;
     const _urlRemoveCourse: string = webApi.domain + webApi.url.removeworkflowcourse;
     this.commonFunctionService.httpPostRequest(_urlRemoveCourse,srtucture)
      //this.service.removeCourseFromWorkflow(srtucture)
        .then(resUserData => {
          this.loader = false;
          this.deleteResult = resUserData['data'][0];
          this.coursebundle = resUserData['data1'];
          // this.workflowListData = resUserData.data1[0].workflowS;
          // this.coursebundle1 = resUserData.data;
          console.log('Workflows delete result', this.deleteResult);
          if(this.deleteResult.status == 1)
          {
            for (let i = 0; i < this.contentList.length; i++) {
              // this.contentList[i].draggable = true;
              if (this.contentList[i].structureid == structureId && this.contentList[i].courseid == courseId) {
                // this.contentList.splice(i, 1);
                // this.contentList[i].draggable = true;
                // this.content.forEach( (value, key) => {
                //   if (value.courseid === courseId) {
                //     // this.content.push(this.contentList[i]);
                //     this.contentList.splice(i, 1);
                //   }
                // });
                // this.content.push(this.contentList[i]);
                this.contentList.splice(i, 1);
                this.doropserch();
                console.log('Removed Content List', this.contentList);

                this.toastr.success(this.deleteResult.msg, 'Success', {
                  closeButton: false
                });

              }
            }
          }else{
            this.toastr.warning(this.deleteResult.msg , 'Warning', {
              closeButton: false
            });
          }

          // this.contentList.splice(index, 1);
          // for (let i = 0; i < this.content.length; i++) {
          //   if (this.content[i].structureid == structureId && this.content[i].courseid == courseId) {
          //     this.content[i].draggable = true;
          //     // this.contentcourseobj[this.content[i].courseid] = true;
          //   }
          // }

        },
          resUserError => {
            this.loader = false;
            this.errorMsg = resUserError;
          });
    } else {
      // this.contentList.splice(index, 1);
      for (let i = 0; i < this.contentList.length; i++) {
        // this.contentList[i].draggable = true;
        if (this.contentList[i].structureid == structureId && this.contentList[i].courseid == courseId) {
          // this.contentList[i].draggable = true;
          //     this.content.forEach( (value, key) => {
          //       if (value.courseid === courseId) {
          //         // this.content.push(this.contentList[i]);
          //         this.contentList.splice(i, 1);
          //       }
          //     });
          //     // if (this.content.length === 0) {
          //     this.content.push(this.contentList[i]);
          this.contentList.splice(i, 1);
          this.doropserch();
        }
      }
      // for (let i = 0; i < this.content.length; i++) {
      //   if (this.content[i].structureid == structureId && this.content[i].courseid == courseId) {
      //     this.content[i].draggable = true;
      //     // this.contentcourseobj[this.content[i].courseid] = true;
      //   }
      // }
      console.log('Removed Content List', this.contentList);
      console.log('Removed Content List 1', this.content);

      this.toastr.success('Course removed', 'Success', {
        closeButton: false
      });
      this.loader = false;
    }

    if (this.contentList.length == 0) {
      this.enableSubmit = false;
    }
  }

  getDropdownItems(index) {
    var previousCourseList = [];
    // console.log('Index ', i);
    for (let i = 0; i < index; i++) {
      previousCourseList.push(this.contentList[i]);
    }
    // console.log('previousCourseList -', previousCourseList);
    return previousCourseList;
  }

  inpuVal: any
  // numberOnly(value) {
  //   this.inpuVal = value;
  //   // var element = document.getElementById(id).value;
  //   // var regex = /[^0-9]/gi;
  //   // element = element.replace(regex, "");
  //   this.value = this.value.replace(/[^0-9.]/g, '');
  //   this.value = this.value.replace(/(\..*)\./g, '$1');
  // }

  // minValue:any = 0;
  setMinVal(event, inputIndex) {
    // var inputElem = document.getElementById('rd3_'+inputIndex);
    // var inputElem = event.target.min;
    // var minValue = 0;
    var inputElem = this.contentList[inputIndex].cascade_days;
    // console.log('Minimum inputvalue',inputElem);
    for (let i = inputIndex; i < this.contentList.length; i++) {

      var control = null;
      if (i == 0) {
        this.contentList[i].minValue = 0;
        control = this.getHtmlControl('rd3_' + i);
        if (control) {
          control.min = 0;
        }
      } else {
        if (i != inputIndex) {
          this.contentList[i].minValue = inputElem;
          // console.log('i value',i);
          var nextControlId = i;
          control = this.getHtmlControl('rd3_' + nextControlId);
          if (control) {
            control.min = inputElem;
          }
          console.log('Minimum inputvalue', this.contentList[i]);
        }
      }

    }
    // var value = parseInt(inputElem);
    // console.log('inputElem',inputElem);
    // for(let i=0; i<this.contentList[i]; i++){
    //   if(this.contentList[i] == inputIndex-1){
    //       return this.contentList[i].cascade_days;
    //   }
    // }
    // return  inputElem.value;
  }

  getHtmlControl(id) {
    return document.getElementById(id);
  }

  enableSubmit: boolean = true;
  onInput(event, i, item) {
    let currentNumber: any = event.target.value;
    let currentIndex: number = i;
    let currentItem: any = item;
    var validCourse = [];
    if (i != 0) {
      // for(let j=0; j<this.contentList.length; j++)
      if (parseInt(currentNumber) < this.contentList[currentIndex].minValue || currentNumber == '') {
        this.enableSubmit = false;
        currentItem.validNumber = false;
      } else {
        this.enableSubmit = true;
        currentItem.validNumber = true;
        this.setMinVal(event, i);
      }
    } else {
      if (this.contentList.length > 1) {
        this.setMinVal(event, i);
        let newIndex = currentIndex + 1;
        for (let j = newIndex; j < this.contentList.length; j++) {
          var listItem = this.contentList[j];
          if (listItem.value2 == true) {
            if (parseInt(currentNumber) > listItem.cascade_days || currentNumber == '') {
              this.enableSubmit = false;
              listItem.validNumber = false;
              listItem.minValue = currentNumber;
            } else {
              listItem.validNumber = true;
              this.enableSubmit = true;
            }
          }
        }
      }
    }

    for (let i = 0; i < this.contentList.length; i++) {
      var validItem = this.contentList[i];
      if (validItem.validNumber == true) {
        validCourse.push(listItem);
      }
    }
    let validLen = validCourse.length;
    let totalLen = this.contentList.length;
    if (validLen == totalLen) {
      this.enableSubmit = true;
    } else {
      this.enableSubmit = false;
    }
  }

  // validateNumber(evt, id) {
  //   var control = null;
  //   control = this.getHtmlControl('rd3_' + id);
  //   console.log('Element ', control.min);
  // }

  // onSearchChange(inputVal) {
  //   console.log('Search text ', inputVal);
  //   this.serachBundle();
  // }

  activeItemId: any;
  setActiveItem(currentIndex, currentItem) {
    this.activeIndex = currentIndex;
    this.activeItem = currentItem;
    this.activeItemId = currentItem.workflowid;
  }

  clearCurrIndex(input) {
    // this.activeIndex = null;
    // this.addRemoveClassItem(this.workflowForm, "avoid-clicks",workflowdata);
  }



  allmanualenrol(content) {
    this.spinner.show();
    console.log(content)
    var data = {
      tId: this.userLoginData.data.data.tenantId,
      wId: content.workflowid,
    }
    console.log(data);
    this.service.getallenroluser(data).then(enrolData => {
      this.spinner.hide();
      console.log(enrolData);
      this.enrolldata = enrolData['data'];
      this.rows = enrolData['data'];
      this.rows = [...this.rows];
      for (let i = 0; i < this.rows.length; i++) {
        // this.rows[i].Date = new Date(this.rows[i].enroledate);
        // this.rows[i].enroledate = this.formdate(this.rows[i].Date);
        if(this.rows[i].visible == 1) {
					this.rows[i].btntext = 'fa fa-eye';
				  } else {
					this.rows[i].btntext = 'fa fa-eye-slash';
				  }
      }
      console.log('EnrolledUSer', this.rows);
      // if(this.enrolldata.visible = 1){
      //   this.enableCourse = false;
      // }else{
      //   this.enableCourse = true;
      // }
      this.cdf.detectChanges();
    })
  }
  searchEnrolUser(event) {
    const val = event.target.value.toLowerCase();
    this.temp = [...this.enrolldata];
    console.log(this.temp);
    // filter our data
    this.searchText=val;
    if(val.length>=3||val.length==0){
    const temp = this.temp.filter(function (d) {
      return String(d.ecn).toLowerCase().indexOf(val) !== -1 ||
        d.fullname.toLowerCase().indexOf(val) !== -1 ||
        d.emailId.toLowerCase().indexOf(val) !== -1 ||
        //d.enrolmode.toLowerCase().indexOf(val) !== -1 ||
        //d.enrolDate.toLowerCase().indexOf(val) !== -1 ||
        d.phoneNo.toLowerCase().indexOf(val) !== -1 ||
        String(d.enrolmode).toLowerCase().indexOf(val) !== -1 ||
        // d.mode.toLowerCase() === val || !val;
        !val
    });

    // update the rows
    this.rows = [...temp];
    // Whenever the filter changes, always go back to the first page
    // this.tableData.offset = 0;
  }

  }
  clearesearch() {
    if (this.searchText.length>=3) {
      this.search = {};
      // this.allmanualenrol(this.service.coursebundledata);
      this.rows = this.temp
    }
    else{
      this.search={};
    }
  }
  // saveEnrol(){
  //   this.showEnrolpage = !this.showEnrolpage;
  // }
  formdate(date) {

    if (date) {
      // const months = ["JAN", "FEB", "MAR","APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
      // var day = date.getDate();
      // var monthIndex = months[date.getMonth()];
      // var year = date.getFullYear();

      // return day + '_' + monthIndex + '_' +year;
      var formatted = this.datePipe.transform(date, 'dd-MM-yyyy');
      return formatted;
    }
  }
  /*BULk ENROL*/
  saveEnrol() {
    // this.detailsTab = false;
    // this.modulesTab = false;
    // this.enrolTab = false;
    // this.engageTab = true;
    // this.rewardsTab = false;

    // this.courseEnrol = false;
    // this.courseEngage = true;

    this.header= {
      title:this.service.coursebundledata?this.service.coursebundledata.wname:"Add learning Pathway",
      btnsSearch: true,
      btnName4: 'Bulk Enrol',
      btnName5: 'Bulk Unenrol',
      btnName4show: true,
      btnName5show: true,
      btnBackshow: true,
      showBreadcrumb: true,
      // isSame:true,
      breadCrumbList:[

          {
            'name': 'Learning',
            'navigationPath': '/pages/learning',
          },
          {
            'isSame':true,
            'name': 'Learning Pathway',
            'navigationPath': '/pages/plan/courseBundle'
          },
      ]
    };
    this.backFlag=2;
    this.showEnrolpage = true;
    this.showBulkpage = false;
    this.showBulkUnenrol = false;
  }

  bulkEnrol() {
    this.header= {
      title:this.service.coursebundledata?this.service.coursebundledata.wname:"Add learning Pathway",
      btnsSearch: true,
      btnName3: 'Enrol',
      btnName5: 'Bulk Unenrol',
      btnName2show: false,
      btnName3show: true,
      btnName5show: true,
      btnBackshow: true,
      showBreadcrumb: true,
      // isSame:true,
      breadCrumbList:[
        {
          'name': 'Learning',
          'navigationPath': '/pages/learning',
        },
        {
          'isSame':true,
          'name': 'Learning Pathway',
          'navigationPath': '/pages/plan/courseBundle'
        },
      ]
    };
    this.backFlag=2;
    this.showEnrolpage = false;
    this.showBulkpage = true;
    this.showBulkUnenrol = false;
  }
  cancelBulk() {
    this.showEnrolpage = true;
    this.showBulkpage = false;
    this.showBulkUnenrol = false;
  }

  bulkUnEnrol() {
    this.header= {
      title:this.service.coursebundledata?this.service.coursebundledata.wname:"Add learning Pathway",
      btnsSearch: true,
      btnName3: 'Enrol',
      btnName4: 'Bulk Enrol',
      btnName3show: true,
      btnName4show: true,
      btnBackshow: true,
      showBreadcrumb: true,
      // isSame:true,
      breadCrumbList:[
        {
          'name': 'Learning',
          'navigationPath': '/pages/learning',
        },
        {
          'isSame':true,
          'name': 'Learning Pathway',
          'navigationPath': '/pages/plan/courseBundle'
        },
      ]
    };
    this.backFlag=2;
    this.showEnrolpage = false;
    this.showBulkpage = false;
    this.showBulkUnenrol = true;
  }
  fileName: any ;
  fileReaded: any;
  enableUpload: any;
  file: any = [];
  datasetPreview: any;
  preview: boolean = false;

  bulkUploadData: any = [];
  readUrl(event: any) {
    this.showInvalidExcel = false;
    this.invaliddata = [];
    this.resultdata = [];
    var validExts = new Array('.xlsx', '.xls');
    var fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
      // alert("Invalid file selected, valid files are of " +
      //          validExts.toString() + " types.");
      // var toast: Toast = {
      //   type: 'error',
      //   title: 'Invalid file selected!',
      //   body: 'Valid files are of ' + validExts.toString() + ' types.',
      //   showCloseButton: true,
      //   // tapToDismiss: false,
      //   timeout: 2000
      //   // onHideCallback: () => {
      //   //     this.router.navigate(['/pages/plan/users']);
      //   // }
      // };
      // this.toasterService.pop(toast);

      this.toastr.warning('Valid file types are ' + validExts.toString(), 'Warning', {
        closeButton: false
      });
      // setTimeout(data => {
      //   this.router.navigate(['/pages/users']);
      // },1000);
      // return false;
      this.cancel();
    }
    else {
      // return true;
      if (event.target.files && event.target.files[0]) {
        this.fileName = event.target.files[0].name;
        //read file from input
        this.fileReaded = event.target.files[0];

        if (this.fileReaded != '' || this.fileReaded != null || this.fileReaded != undefined) {
          this.enableUpload = true;
        }

        this.file = event.target.files[0];
        var reader = new FileReader();

        this.bulkUploadData = event.target.files[0];
        console.log('this.bulkUploadData', this.bulkUploadData);

        this.xlsxToJsonService.processFileToJson({}, this.file).subscribe(data => {
         const resultSheets = Object.getOwnPropertyNames(data['sheets']);
          console.log('File Property Names ', resultSheets);
          let sheetName = resultSheets[0];
          const result = data['sheets'][sheetName];
          console.log('dataSheet', data);
          console.log('this.result', result);

          if (result.length > 0) {
            this.uploadedData = result;
          }
          console.log('this.uploadedData', this.uploadedData);
        });

        reader.onload = (event: ProgressEvent) => {
          let fileUrl = (<FileReader>event.target).result;
          // event.setAttribute("data-title", this.fileName);
          // console.log(this.fileUrl);
        }
        reader.readAsDataURL(event.target.files[0]);
      }
    }

    // if(event.target.files[0].name.split('.')[event.target.files[0].name.split('.').length-1] === 'xlsx'){
    //     let file = event.target.files[0];
    // } else if(event.target.files[0].name.split('.')[event.target.files[0].name.split('.').length-1] === 'xls'){
    //     let file = event.target.files[0];
    // }else{
    //   alert("Invalid file selected, valid files are of " +
    //          validExts.toString() + " types.");
    // }
  }

  cancel() {
    // this.fileUpload.nativeElement.files = [];
    console.log(this.fileUpload.nativeElement.files);
    this.fileUpload.nativeElement.value = '';
    console.log(this.fileUpload.nativeElement.files);
    this.fileName = 'Click here to upload an excel file to enrol ' +this.currentBrandData.employee.toLowerCase()+ ' to this course';
    this.enableUpload = false;
    this.file = [];
    this.preview = false;
  }
  resultdata: any = [];

  uploadfile() {
    this.spinner.show();
    this.service.coursebundledata.createduserid = this.userLoginData.data.data.id;
    var fd = new FormData();
    fd.append('content', JSON.stringify(this.service.coursebundledata));
    fd.append('file', this.file);

    this.service.TempManEnrolBulk(fd).then(result => {
      console.log(result);
      this.loader =false;

      var res = result;
      try {
        if (res['data1'][0]) {
          this.resultdata = res['data1'][0];
          for (let i = 0; i < this.resultdata.length; i++) {
            this.resultdata[i].enrolDate = this.formatDate(this.resultdata[i].enrolDate);
          }

          this.datasetPreview = this.resultdata;
          this.preview = true;
          this.cdf.detectChanges();
          // this.spinner.hide();
        }
      } catch (e) {

        // var courseUpdate: Toast = {
        //   type: 'error',
        //   title: 'Enrol',
        //   body: 'Something went wrong.please try again later.',
        //   showCloseButton: true,
        //   timeout: 2000
        // };

        // // this.closeEnableDisableCourseModal();
        // this.toasterService.pop(courseUpdate);

        this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
          timeOut: 0,
          closeButton: true
        });
      }
      this.spinner.hide();

    },
      resUserError => {
        this.loader =false;
        this.spinner.hide();
        this.errorMsg = resUserError;
        // this.closeEnableDisableCourseModal();
      });
  }

  formatDate(date) {
    var d = new Date(date)
    var formatted = this.datePipe.transform(d, 'dd-MMM-yyyy');
    return formatted;
  }

  exportToExcelInvalid() {
    this.exportService.exportAsExcelFile(this.invaliddata, 'Enrolment Status')
    //this.showInvalidExcel = false;
  }


  addEditModRes: any;
  fileUploadRes: any;
  savebukupload1() {
    var data = {
      userId: this.service.coursebundledata.createduserid,
      workflowid: this.service.coursebundledata.workflowid,
      areaId: 3,
      tId: this.tenantId,
    }
    this.loader = true;
    this.spinner.show();
    this.service.finalManEnrolBulk(data)
      .then(rescompData => {
        this.loader =false;
        this.spinner.hide();
        var temp: any = rescompData;
        this.addEditModRes = temp.data[0];
        if (temp == 'err') {
          // var modUpdate: Toast = {
          //   type: 'error',
          //   title: 'Enrol',
          //   body: 'Something went wrong',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(modUpdate);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        } else if (temp.type == false) {
          // var modUpdate: Toast = {
          //   type: 'error',
          //   title: 'Enrol',
          //   body: this.addEditModRes[0].msg,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(modUpdate);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        } else {
          // var modUpdate: Toast = {
          //   type: 'success',
          //   title: 'Enrol',
          //   body: this.addEditModRes[0].msg,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(modUpdate);

          this.toastr.success(this.addEditModRes[0].msg, 'Success', {
            closeButton: false
          });
          this.backToEnrol()
          this.allmanualenrol(data);
          this.cdf.detectChanges();
          this.preview = false;

        }
        console.log('Enrol bulk Result ', this.addEditModRes);
        this.cdf.detectChanges();
      },
        resUserError => {
          this.loader = false;
          this.errorMsg = resUserError;
        });
  }
  backToEnrol() {
    this.showEnrolpage = false;
    this.showBulkpage = false;
    this.showBulkUnenrol = false;
    this.file = [];
    this.preview = false;
    this.fileName = 'Click here to upload an excel file to enrol ' +this.currentBrandData.employee.toLowerCase()+ ' to this course';
    this.enableUpload = false;

  }
  clearPreviewData() {
    this.datasetPreview = [];
    this.preview = false;

  }

  RemoveCb(bundle, i) {
    var data = {
      workflowid: bundle.workflowid,
      tenantId: this.userLoginData.data.data.tenantId,
    }
    this.loader = true;
    this.spinner.show();
    this.service.deleteCB(data)
      .then(rescompData => {
        this.loader =false;
        this.spinner.hide();
        var temp: any = rescompData;
        this.addEditModRes = temp.data[0];
        if (temp == 'err') {
          // var modUpdate: Toast = {
          //   type: 'error',
          //   title: 'Learning pathway',
          //   body: 'Something went wrong',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(modUpdate);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        } else if (temp.type == false) {
          // var modUpdate: Toast = {
          //   type: 'error',
          //   title: 'Learning pathway',
          //   body: this.addEditModRes.msg,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(modUpdate);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        } else {
          if (this.addEditModRes.flag === 2 || this.addEditModRes.flag === 3) {
            // var cbDelete: Toast = {
            //   type: 'warning',
            //   title: 'Learning pathway',
            //   body: this.addEditModRes.msg,
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(cbDelete);

            this.toastr.warning(this.addEditModRes.msg, 'Warning', {
              closeButton: false
            });
          } else {
            // var cbDelete: Toast = {
            //   type: 'success',
            //   title: 'Learning pathway',
            //   body: this.addEditModRes.msg,
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(cbDelete);

            this.toastr.success(this.addEditModRes.msg, 'Success', {
              closeButton: false
            });
            // this.backToEnrol()
            // this.allmanualenrol(data);
            // this.cdf.detectChanges();
            // this.preview = false;
            this.getWorkflows();
          }
        }
        console.log('Enrol bulk Result ', this.addEditModRes);
        // this.cdf.detectChanges();
      },
        resUserError => {
          this.loader = false;
          this.errorMsg = resUserError;
        });
  }
  // Help Code Start Here //

  helpContent: any;
  getHelpContent() {
    return new Promise(resolve => {

      this.http1.get('../../../../../../assets/help-content/addEditCourseContent.json').subscribe(
        data => {
          this.helpContent = data;
          console.log('Help Array', this.helpContent);
        },
        err => {
          resolve('err');
        },
      );
    });
    // return this.helpContent;
  }


  // Help Code Ends Here //

  /*************** Add learning pathway start ***************/

  // add_edit_learning_pathway(lpData, actionId) {
  //   this.hidesearchbar = false;
  //   if (actionId === 0) {
  //     this.formdatabundle = {
  //       cohortid: '',
  //       createduserid: '',
  //       datecreated: '',
  //       wname: '',
  //       wdescription: '',
  //       workflowid: '',
  //       wrkflwStatus: '',
  //       enrolUsers: '',
  //       tenantId: '',
  //     };
  //     this.makeAddWorkflowDataReady();
  //   } else {
  //     this.formdatabundle = {
  //       cohortid: lpData.cohortid,
  //       createduserid: lpData.createduserid,
  //       datecreated: lpData.datecreated,
  //       wname: lpData.wname,
  //       wdescription: lpData.wdescription,
  //       workflowid: lpData.workflowid,
  //       wrkflwStatus: lpData.wrkflwStatus,
  //       enrolUsers: lpData.enrolUsers,
  //       tenantId: lpData.tenantId,
  //     };
  //   }
  // }

  add_edit_learning_pathway(workflowData, id, dublicate) {
    const dempData = {
      tabTitle: 'Details'
    }
    this.selectedTab(dempData);
    // this.isActiveTop=true;
    this.dublicate = dublicate;
    this.copy = id;
    // this.getCourses();
    // this.getCategories();
    this.enrolldata = [];
    this.shortUrl = '';
    this.rows = [];
    this.rows = [...this.rows];
    this.service.coursebundledata = workflowData;
    if (id != 0) {
      if (this.service.coursebundledata) {
        this.allmanualenrol(this.service.coursebundledata);
        this.checkWorkflowUrl(this.service.coursebundledata);
      }
    }
    this.hidesearchbar = false;
    if (!workflowData) {
      if (id == 0) {
        this.formdatabundle = {
          actionId: id,
          cohortid: '',
          createduserid: this.userLoginData.data.data.id,
          datecreated: '',
          wname: '',
          wImg: '',
          wdescription: '',
          workflowid: '',
          wrkflwStatus: '',
          enrolUsers: '',
          tenantId: this.userLoginData.data.data.tenantId,
        }
    //      const dempData = {
    //   tabTitle: 'Details'
    // }
    // this.selectedTab(dempData);
    // this.isActiveTop=true;
        this.makeAddEditLearningPathwayDataReady(workflowData, null);
        // this.addRemoveClassItem(this.workflowForm, 'avoid-clicks', workflowData);
        this.isActiveTop = true;
        this.isActive = true;
        this.addCourseBundle = false;
      }

    } else {
      const workflowdata = workflowData;
      const workflowListData = workflowData.workflowS;
      if (id == 0) {
        this.formdatabundle = {
          actionId: id,
          cohortid: workflowdata.cohortid,
          createduserid: workflowdata.createduserid,
          datecreated: workflowdata.datecreated,
          wname: '',
          wdescription: '',
          // wname: workflowdata.wname,
          // wdescription: workflowdata.wdescription,
          wImg: workflowdata.wImg,
          workflowid: workflowdata.workflowid,
          wrkflwStatus: workflowdata.wrkflwStatus,
          enrolUsers: workflowdata.enrolUsers,
        };
        // const dempData = {
        //   tabTitle: 'Details'
        // }
        // this.selectedTab(dempData);
        // this.isActiveTop=true;
      } else {
        this.formdatabundle = {
          actionId: id,
          cohortid: workflowdata.cohortid,
          createduserid: workflowdata.createduserid,
          datecreated: workflowdata.datecreated,
          wname: workflowdata.wname,
          wImg: workflowdata.wImg,
          wdescription: workflowdata.wdescription,
          workflowid: workflowdata.workflowid,
          wrkflwStatus: workflowdata.wrkflwStatus,
          enrolUsers: workflowdata.enrolUsers,
        };
      }
    //          const dempData = {
    //   tabTitle: 'Details'
    // }
    // this.selectedTab(dempData);
    // this.isActiveTop=true;
      this.prepareDraggableData(this.content1);
      this.makeAddEditLearningPathwayDataReady(workflowdata, workflowListData);
      this.addRemoveClassItem(this.workflowForm, 'avoid-clicks', workflowdata);
    }
    this.input_search_string = '';
    this.cat.id = '';
    this.doropserch();
    this.isActiveTop=true;
    console.log('formatted courses data this.content - ', this.content);

  }

  copyCard(workflowData, id, dublicate) {
    this.dublicate = dublicate;
    this.copy = id;

    this.enrolldata = [];
    this.shortUrl = '';
    this.rows = [];
    this.rows = [...this.rows];
    this.service.coursebundledata = workflowData;
    if (id != 0) {
      if (this.service.coursebundledata) {
        this.allmanualenrol(this.service.coursebundledata);
        this.checkWorkflowUrl(this.service.coursebundledata);
      }
    }
    this.hidesearchbar = false;
    if (!workflowData) {
      if (id == 0) {
        this.formdatabundle = {
          actionId: id,
          cohortid: '',
          createduserid: this.userLoginData.data.data.id,
          datecreated: '',
          wname: '',
          wImg: '',
          wdescription: '',
          workflowid: '',
          wrkflwStatus: '',
          enrolUsers: '',
          tenantId: this.userLoginData.data.data.tenantId,
        }
        this.makeAddEditLearningPathwayDataReady(workflowData, null);

        this.isActiveTop = true;
        this.isActive = true;
        this.addCourseBundle = false;
      }

    } else {
      const workflowdata = workflowData;
      const workflowListData = workflowData.workflowS;
      if (id == 0) {
        this.formdatabundle = {
          actionId: id,
          cohortid: workflowdata.cohortid,
          createduserid: workflowdata.createduserid,
          datecreated: workflowdata.datecreated,
          wname: '',
          wdescription: '',
          wImg: workflowdata.wImg,
          workflowid: workflowdata.workflowid,
          wrkflwStatus: workflowdata.wrkflwStatus,
          enrolUsers: workflowdata.enrolUsers,
        };
      } else {
        this.formdatabundle = {
          actionId: id,
          cohortid: workflowdata.cohortid,
          createduserid: workflowdata.createduserid,
          datecreated: workflowdata.datecreated,
          wname: workflowdata.wname,
          wImg: workflowdata.wImg,
          wdescription: workflowdata.wdescription,
          workflowid: workflowdata.workflowid,
          wrkflwStatus: workflowdata.wrkflwStatus,
          enrolUsers: workflowdata.enrolUsers,
        };
      }
      this.prepareDraggableData(this.content1);
      this.makeAddEditLearningPathwayDataReady(workflowdata, workflowListData);
      this.addRemoveClassItem(this.workflowForm, 'avoid-clicks', workflowdata);
    }
    this.input_search_string = '';
    this.cat.id = '';
    this.doropserch();
    console.log('formatted courses data this.content - ', this.content);
   this.cdf.detectChanges();
  }


  makeAddEditLearningPathwayDataReady(workflowdata, workflowListData) {
    for (let i = 0; i < this.content.length; i++) {
      if (this.content[i].draggable == false) {
        this.content[i].draggable = true;
      }
      // else{
      //   this.content1[i].draggable = false;
      // }
    }

    // for (let i = 0; i < this.content1.length; i++) {
    //   if (this.content1[i].draggable == false) {
    //     this.content1[i].draggable = true;
    //   }
    // }

    // console.log('formatted courses data this.content1 1 - ', this.content1);

    if (workflowListData) {
      this.contentList = workflowListData;
      for (let i = 0; i < this.contentList.length; i++) {
        this.contentList[i].value1 = this.contentList[i].previousCourseid === 0 ? false : true;
        this.contentList[i].value2 = this.contentList[i].cascade_days === 0 ? false : true;
        this.contentList[i].validNumber = true;
        if (this.contentList[i].value1 == true) {
          this.dropDisabled = false;
        } else {
          this.dropDisabled = true;
        }
        if (this.contentList[i].value2 == true) {
          this.inputDisabled = false;
        } else {
          this.inputDisabled = true;
        }
      }

      for (let k = 0; k < this.contentList.length; k++) {
        for (let j = 0; j < this.content.length; j++) {
          if (this.content[j].courseid === this.contentList[k].courseid && this.content[j].draggable == true) {
            this.content[j].draggable = false;
            // console.log('updated content',this.content[j]);
          } else {
            this.content[j].draggable = true;
          }
          // else if(this.content[j].draggable == false){
          //   this.content[j].draggable = true;
          //   // console.log('updated content',this.content[j]);
          // }
        }
      }
      // this.contentList = this.getDropdownItems();
    } else {
      this.contentList = [];
    }

    if (workflowdata != this.tempWorkData) {
      if (this.isActive == true) {
        // this.isActiveRes = false;
        this.isActiveTop = true;
        this.isActive = true;
      } else if (this.isActiveTop == false) {
        // this.isActive = !this.isActive;
        this.isActive = true;
        this.isActiveTop = true;
      }
    } else {
      workflowdata = null;
      // this.isActive = !this.isActive;
      this.isActive = false;
      this.isActiveTop = false;
    }
    this.tempWorkData = workflowdata;

    // console.log('formatted courses data this.content 2 - ', this.content);

  }

  formattedString() {
    // this.loader =true;

    // var finalContentList = [];
    for (let i = 0; i < this.contentList.length; i++) {
      this.contentList[i].sortOrder = i;
    }

    var para = this.contentList;
    var string = '';
    var structureIdS = '';
    var courseIdS = '';
    var cascadeDays = '';
    var prevCourseId = '';
    var sortOrderList = '';
    var compStatus ='';
    if (para.length > 0) {
      for (var i = 0; i < para.length; i++) {
        var parameter = para[i];
        if (structureIdS != '') {
          structureIdS += '|';
        }
        if (String(parameter.structureid) != '' && String(parameter.structureid) != 'null') {
          structureIdS += parameter.structureid;
        }

        if (courseIdS != '') {
          courseIdS += '|';
        }
        if (String(parameter.courseid) != '' && String(parameter.courseid) != 'null') {
          courseIdS += parameter.courseid;
        }
        if (sortOrderList != '') {
          sortOrderList += '|';
        }
        if (String(parameter.sortOrder) != '' && String(parameter.sortOrder) != 'null') {
          sortOrderList += parameter.sortOrder;
        }
        if (cascadeDays != '') {
          cascadeDays += '|';
        }
        if (String(parameter.cascade_days) != '' && String(parameter.cascade_days) != 'null') {
          cascadeDays += parameter.cascade_days;
        }
        if (prevCourseId != '') {
          prevCourseId += '|';
        }
        if (String(parameter.previousCourseid) != '' && String(parameter.previousCourseid) != 'null') {
          prevCourseId += parameter.previousCourseid;
        }
        if (compStatus != '') {
          compStatus += '|';
        }
        if (String(parameter.courseVisible) != '' && String(parameter.courseVisible) != 'null') {
          compStatus += parameter.courseVisible;
        }
      }
    }

    this.workflowAddData = {
      actionId: this.formdatabundle.actionId,
      workflowId: this.formdatabundle.workflowid,
      cohortId: this.formdatabundle.cohortid,
      workfName: this.formdatabundle.wname,
      wImg: this.formdatabundle.wImg,
      worksName: this.formdatabundle.wdescription,
      wrkflwStatus: this.formdatabundle.wrkflwStatus,
      structureIdS: structureIdS,
      courseIdS: courseIdS,
      sortOrder: sortOrderList,
      cascadeDays: cascadeDays,
      prevCourseId: prevCourseId,
      len: para.length,
      userId: this.userLoginData.data.data.id,
      compStatus :compStatus,
      tenantId: this.userLoginData.data.data.tenantId,
    }
    if (this.service.coursebundledata && this.copy == 0) {
      // if(!this.dublicate){
      if (this.dublicate) {
        this.workflowAddData.structureIdS = 0;
      } else {
        this.workflowAddData.structureIdS = this.structures;
      }

    }
    console.log('Final Data ', this.workflowAddData);
    this.updateimage(this.workflowAddData);
  }

  updateimage(data) {
    let workflowId;
    let workflowMsg = 'Workflow added/updated';
    console.log("final Data=>", this.workflowAddData);
    if (this.workflowAddData) {
      if (this.courseImgData != undefined) {
        var fd = new FormData();
        fd.append("content", JSON.stringify(data));
        fd.append("file", this.courseImgData);
        let fileUploadUrl = webApi.domain + webApi.url.multifileUpload;
        this.webApiService.getService(fileUploadUrl, fd).then(
          rescompData => {
            this.spinner.hide();
            var temp: any = rescompData;
            this.fileUploadRes = rescompData;
            if (temp == "err") {
              this.toastr.error('Unable to upload workflow Thumbnail', 'error', {
                closeButton: false
              });
              this.spinner.hide();
              // this.presentToast('error', '');
            } else if (temp.type == false) {
              this.toastr.error('Unable to upload workflow Thumbnail', 'error', {
                closeButton: false
              });
              this.spinner.hide();
            } else {
              if (this.fileUploadRes.length > 0) {
                data.wImg = this.fileUploadRes[0];
                // quiz.picRef = this.fileUploadRes[0];
                this.add_update_learning_pathway(data);
              } else {
                this.toastr.error('Unable to upload workflow Thumbnail', 'error', {
                  closeButton: false
                });
                this.spinner.hide();
              }
            }
            console.log("File Upload Result", this.fileUploadRes);
          },
          resUserError => {
            this.spinner.hide();
            this.errorMsg = resUserError;
          }
        );
      } else {
        this.add_update_learning_pathway(data);
      }

    }
    // this.service.add_edit_learning_pathway(this.workflowAddData)
    //   .then(resUserData => {
    //     this.loader = false;
    //     this.updateResult = resUserData['data'][0][0];
    //     console.log('Workflows add/update result', this.updateResult);
    //     if (resUserData['type']) {
    //       if (resUserData['data'][1]) {
    //         workflowId = resUserData['data'][1][0].workfId;
    //         workflowMsg = resUserData['data'][0][0].SQLResult;
    //         console.log('Learning pathway added id ', workflowId);
    //         console.log('Learning pathway added msg ', workflowMsg);

    //       }

    //       if (workflowId) {
    //         const contentTemp = {
    //           workflowid: workflowId,
    //           wrkflwStatus: "active",
    //           tenantId: this.tenantId
    //         }
    //         this.service.coursebundledata = contentTemp;
    //         this.cdf.detectChanges();
    //       }




    //       this.toastr.success(workflowMsg, 'Success', {
    //         closeButton: false
    //       });
    //       //this.closeFormTop();
    //       const dempData = {
    //         tabTitle: 'Enrol'
    //       }
    //       this.selectedTab(dempData);
    //     } else {

    //       this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
    //         timeOut: 0,
    //         closeButton: true
    //       });
    //     }
    //   },
    //     resUserError => {
    //       this.loader = false;
    //       this.errorMsg = resUserError;
    //     });
  }

  add_update_learning_pathway(data) {
    let workflowId;
    let workflowMsg = 'Workflow added/updated';
    const add_edit_learning_pathway_url = webApi.domain + webApi.url.add_edit_learning_pathway;
    this.commonFunctionService.httpPostRequest(add_edit_learning_pathway_url,this.workflowAddData)
    //this.service.add_edit_learning_pathway(this.workflowAddData)
      .then(resUserData => {
        this.loader = false;
        this.spinner.hide();
        this.updateResult = resUserData['data'][0][0];
        console.log('Workflows add/update result', this.updateResult);
        if (resUserData['type']) {
          if (resUserData['data'][1]) {
            workflowId = resUserData['data'][1][0].workfId;
            workflowMsg = resUserData['data'][0][0].SQLResult;
            this.structures = resUserData['data'][2][0].structureIdS;
            this.dublicate = false;
            console.log('Learning pathway added id ', workflowId);
            console.log('Learning pathway added msg ', workflowMsg);

          }

          if (workflowId) {
            this.formdatabundle.workflowid = workflowId;
            this.formdatabundle.actionId = 1;
            const contentTemp = {
              workflowid: workflowId,
              wrkflwStatus: "active",
              tenantId: this.tenantId
            }
            this.service.coursebundledata = contentTemp;
            this.cdf.detectChanges();
          }

          this.toastr.success(workflowMsg, 'Success', {
            closeButton: false
          });
          //this.closeFormTop();
          const dempData = {
            tabTitle: 'Notification'
          }
          this.selectedTab(dempData);
          // this.getWorkflows();
        } else {
          this.spinner.hide();
          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        }
      },
        resUserError => {
          this.spinner.hide();
          this.loader = false;
          this.errorMsg = resUserError;
        });
  }

  validateData() {
    this.spinner.show();
    var invalidCount = 0;
    if (this.contentList.length === 0) {
      // const invalidToast: Toast = {
      //   type: 'error',
      //   title: 'Unable to update',
      //   body: 'Please select a course first',
      //   showCloseButton: true,
      //   // tapToDismiss: false,
      //   timeout: 10000,
      //   // timeout: 0
      //   // onHideCallback: () => {
      //   //     this.router.navigate(['/pages/plan']);
      //   // }
      // };
      // this.toasterService.pop(invalidToast);
      this.spinner.hide();
      this.toastr.warning('Please select a course', 'Warning', {
        closeButton: false
      });
      return;
    } else {
      for (let i = 0; i < this.contentList.length; i++) {
        if (i === 0) {
          if (this.contentList[i].value1 == true && this.contentList[i].previousCourseid > 0) {
            this.contentList[i].previousCourseid = 0;
            this.contentList[i].value1 = false;
          }
          var firstVal = this.contentList[i];
        } else {
          if (this.contentList[i].value2 == true && this.contentList[i].cascade_days < firstVal.cascade_days) {
            this.contentList[i].validNumber = false;
            invalidCount++;
          }
          if (this.contentList[i].value1 == true && this.contentList[i].previousCourseid < firstVal.cascade_days) {
            this.contentList[i].validNumber = false;
            invalidCount++;
          }
        }
      }
      if (invalidCount > 0) {
        // const invalidToast: Toast = {
        //   type: 'error',
        //   title: 'Unable to update Workflow!',
        //   body: 'Make sure the input data is correct!',
        //   showCloseButton: true,
        //   // tapToDismiss: false,
        //   timeout: 2000,
        //   // timeout: 0
        //   // onHideCallback: () => {
        //   //     this.router.navigate(['/pages/plan']);
        //   // }
        // };
        // this.toasterService.pop(invalidToast);

        this.toastr.warning('Make sure the input data is correct!', 'Warning', {
          closeButton: false
        });
      } else {
        this.formattedString();

        // this.add_update_learning_pathway();
        // this.service.updateWorkflow(this.workflowAddData)
        //   .subscribe(resUserData => {
        //     this.loader = false;
        //     this.updateResult = resUserData.data[0][0];
        //     console.log('Workflows update result', this.updateResult);
        //     if (resUserData.type) {
        //       var toast: Toast = {
        //         type: 'success',
        //         title: 'Workflow updated!',
        //         body: 'Workflow updated successfully.',
        //         showCloseButton: true,
        //       };
        //       this.toasterService.pop(toast);
        //       this.closeFormTop();
        //     } else {
        //       var toast: Toast = {
        //         type: 'error',
        //         title: 'Workflow not updated!',
        //         body: 'Unable to update Workflow at this time.',
        //         showCloseButton: true,
        //         timeout: 2000
        //       };
        //       this.toasterService.pop(toast);
        //     }
        //   },
        //     resUserError => {
        //       this.loader = false;
        //       this.errorMsg = resUserError;
        //     });
      }
    }

  }



  updateWorkflow(f): void {
    console.log(this.userFrm);

    // if (f.valid) {
      if (this.userFrm.valid) {
      this.validateData();
    } else {
      console.log('Please Fill all fields');
      // const addEditF: Toast = {
      //   type: 'error',
      //   title: 'Unable to update',
      //   body: 'Please Fill all fields',
      //   showCloseButton: true,
      //   timeout: 2000
      // };
      // this.toasterService.pop(addEditF);

      this.toastr.warning('Please fill in the required details', 'Warning', {
        closeButton: false
      });
      Object.keys(this.userFrm.controls).forEach(key => {
        this.userFrm.controls[key].markAsDirty();
      });
    }
    // this.formattedString();
  }

  /*************** Add learning pathway end ***************/

  /** tab switch*/
  // selectedTabTest(tabEvent){
  //     console.log('tab Selected', tabEvent);
  //     if (tabEvent.tabTitle == 'Details') {

  //       this.detailsTab = true;
  //       this.enrolTab = false;
  //     }

  //     else if (tabEvent.tabTitle == 'Enrol') {
  //       this.detailsTab = false;
  //       this.enrolTab = true;
  //       this.showEnrolpage = false;
  //       // this.showBulkpage = false;
  //       // this.showBulkUnenrol = false;
  //     }
  //    else if (tabEvent.tabTitle == 'Notification') {
  //       this.engageTab =true
  //       this.detailsTab = false;
  //       this.enrolTab = false;
  //       this.showEnrolpage = false;

  //     }

  //     this.cdf.detectChanges();


  // }
  selectedTab(tabEvent) {
    console.log('tab Selected', tabEvent);

    // if(tabEvent.tabTitle == 'Home') {
    // }

    if (tabEvent.tabTitle == 'Details') {
      if(this.service.coursebundledata){}
      this.header= {
        title:this.service.coursebundledata?this.service.coursebundledata.wname:"Add learning Pathway",
        btnsSearch: true,
        btnName1: 'save',
        btnName1show: true,
        btnBackshow: true,
        showBreadcrumb: true,
        // isSame:true,
        breadCrumbList:[
          {
            'name': 'Learning',
            'navigationPath': '/pages/learning',
          },
          {
            'isSame':true,
            'name': 'Learning Pathway',
            'navigationPath': '/pages/plan/courseBundle'
          },
        ]
      };
      this.backFlag=1;
      this.spinner.show();
      setTimeout(() => {
        this.spinner.hide()
      }, 2000);
      this.detailsTab = true;
      this.enrolTab = false;
      this.engageTab =false
    }

    else if (tabEvent.tabTitle == 'Enrol') {
      this.header= {
        title:this.service.coursebundledata?this.service.coursebundledata.wname:"Add learning Pathway",
        btnsSearch: true,
        btnName3: 'Enrol',
        btnName4: 'Bulk Enrol',
        btnName5: 'Bulk Unenrol',
        btnName3show: true,
        btnName4show: true,
        btnName5show: true,
        btnBackshow: true,
        showBreadcrumb: true,
        // isSame:true,
        breadCrumbList:[
          {
            'name': 'Learning',
            'navigationPath': '/pages/learning',
          },
          {
            'isSame':true,
            'name': 'Learning Pathway',
            'navigationPath': '/pages/plan/courseBundle'
          },
        ]
      };
      this.backFlag=1;
      this.spinner.show();
      setTimeout(() => {
        this.spinner.hide()
      }, 2000);
      this.engageTab =false
      this.detailsTab = false;
      this.enrolTab = true;
      this.showEnrolpage = false;
      this.showBulkpage = false;
      this.showBulkUnenrol = false;
    }
    else if (tabEvent.tabTitle == 'Notification') {
      this.header= {

        title:this.service.coursebundledata?this.service.coursebundledata.wname:"Add learning Pathway",
         btnsSearch: true,
        btnName2: 'Add Notification',
        btnName2show: true,
        btnName3show: false,
        btnBackshow: true,
        btnAddshow: false,
        filter: false,
        showBreadcrumb: true,
        // isSame:true,
        breadCrumbList:[
          {
            'name': 'Learning',
            'navigationPath': '/pages/learning',
          },
          {
            'isSame':true,
            'name': 'Learning Pathway',
            'navigationPath': '/pages/plan/courseBundle'
          },
        ]
      };
      this.backFlag=1;
      this.spinner.show();
      setTimeout(() => {
        this.spinner.hide()
      }, 2000);
      this.detailsTab = false;
      this.enrolTab = false;
      this.showEnrolpage = false;
      this.engageTab =true
      // this.showBulkpage = false;
      // this.showBulkUnenrol = false;
    }

    this.cdf.detectChanges();

  }
  courseImgData: any;
  readCourseThumb(event: any) {
    var size = 1000000;
    var validExts = new Array('.png', '.jpg', '.jpeg');
    var fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {

      this.toastr.warning('Valid file types are ' + validExts.toString(), 'Warning', {
        closeButton: false
      });

      // this.deleteCourseThumb();
    } else if (size <= event.target.files[0].size) {

      this.toastr.warning('File size should be less than 100 KB', 'Warning', {
        timeOut: 5000,
        closeButton: false
      })
      // this.deleteCourseThumb();
    } else {
      if (event.target.files && event.target.files[0]) {
        this.courseImgData = event.target.files[0];

        var reader = new FileReader();

        reader.onload = (event: ProgressEvent) => {

          this.formdatabundle.wImg = (<FileReader>event.target).result;
          this.cdf.detectChanges();
        };
        reader.readAsDataURL(event.target.files[0]);
      }
    }
  }

  openNotification() {
    this.passService.crossCommunication.emit();
  }

  deleteCourseThumb() {
    // this.defaultThumb = 'assets/images/category.jpg';
    this.formdatabundle.wImg = 'assets/images/courseicon.jpg';
    this.courseImgData = undefined;
    this.formdata.categoryPicRefs = undefined;
  }
  bindImage(data) {
    if (data) {
      return 'url(' + data + ')';
    } else {
      let defaullt = 'assets/images/open-book-leaf.jpg';
      return 'url(' + defaullt + ')';
    }
  }

  visibilityTableRow(row) {
		let value;
		let status;

		if (row.visible == 1) {
		  row.btntext = 'fa fa-eye-slash';
		  value  = 'fa fa-eye-slash';
		  row.visible = 0
		  status = 0;
		} else {
		  status = 1;
		  value  = 'fa fa-eye';
		  row.visible = 1;
		  row.btntext = 'fa fa-eye';
		}

		for(let i =0; i < this.rows.length; i++) {
		  if(this.rows[i].id == row.id) {
			this.rows[i].btntext = row.btntext;
			this.rows[i].visible = row.visible;
		  }
		}
    var visibilityData = {
      eId: row.id,
      tId: this.userLoginData.data.data.tenantId,
      wfId: this.service.coursebundledata.workflowid,
      status: status,
    };
    if(visibilityData.status==0){
      this.toastr.success('User Disabled Successfully', 'Success', {
            closeButton: false
          });
    }else{
      this.toastr.success('User Enabled Successfully', 'Success', {
        closeButton: false
      });
    }
    const workflow_enable_disable_enrol_url = webApi.domain + webApi.url.workflow_enable_disable_enrol;
    this.commonFunctionService.httpPostRequest(workflow_enable_disable_enrol_url,visibilityData)
    //this.service.enabledisable(visibilityData)
    .then(res => {
      console.log(res);
      // if (res['type'] == true) {
      //   this.allmanualenrol(this.service.coursebundledata);
      //   this.toastr.success(res['data'], 'Success', {
      //     closeButton: false
      //   });
      // }
      this.allmanualenrol(this.service.coursebundledata);

      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      this.toastr.warning('Server Error', 'warning', {
        closeButton: false
      });
    })
		console.log('row', row);
    }
    getAllEnrollment(){
      this.allmanualenrol(this.service.coursebundledata);
    }
  disableCoursebundleVisibility(currentIndex, row, status) {
    console.log(currentIndex, row, status)
    this.spinner.show();
    var visibilityData = {
      eId: row.id,
      tId: this.userLoginData.data.data.tenantId,
      wfId: this.service.coursebundledata.workflowid,
      status: status,
    };
    const workflow_enable_disable_enrol_url = webApi.domain + webApi.url.workflow_enable_disable_enrol;
    this.commonFunctionService.httpPostRequest(workflow_enable_disable_enrol_url,visibilityData)
    //this.service.enabledisable(visibilityData)
    .then(res => {
      console.log(res);
      if (res['type'] == true) {
        this.allmanualenrol(this.service.coursebundledata);
        this.toastr.success(res['data'], 'Success', {
          closeButton: false
        });
      }
      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      this.toastr.error('Server Error', 'Success', {
        closeButton: false
      });
    })
  }

  enableDisableWorkflowCOurses(item , wfId , i )
  {
    console.log('hideshowData=>',item,wfId,i);

    var params ={
      courseId: item.courseid,
      wfId: wfId,
      status: item.courseVisible == 0 ? 1 : 0,
    }

    if(item.structureid)
    {
    const workflow_courses_enable_disable_url = webApi.domain + webApi.url.workflow_courses_enable_disable;
    this.commonFunctionService.httpPostRequest(workflow_courses_enable_disable_url,params)
    //this.service.enabledisableCourse(params)
    .then(res => {
      console.log(res);
      if (res['type'] == true) {
        // this.allmanualenrol(this.service.coursebundledata);
        this.toastr.success(res['data'], 'Success', {
          closeButton: false
        });
        this.contentList[i].courseVisible = params.status;
      }
      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      this.toastr.error('Server Error', 'Success', {
        closeButton: false
      });
    })
  }else{
    this.contentList[i].courseVisible = params.status;
  }

  }
  uploadedData: any = [];
  invaliddata: any = [];
  showInvalidExcel: boolean = false;
  savebukupload() {
    // this.loader = true;
    if (this.uploadedData.length > 0 && this.uploadedData.length <= 2000) {
      this.spinner.show();

      // var option: string = Array.prototype.map
      //   .call(this.uploadedData, function (item) {
      //     console.log("item", item);
      //     return Object.values(item).join("#");
      //   })
      //   .join("|");

      // New Function as per requirement.

      var option: string = Array.prototype.map
        .call(this.uploadedData, (item) => {
          const array = Object.keys(item);
          let string = '';
          array.forEach(element => {
            if (element === 'EnrolDate') {
              string += this.commonFunctionService.formatDateTimeForBulkUpload(item[element]);
            } else {
              string = item[element] + '#';
            }
          });
          // console.log("item", item);
          // return Object.values(item).join("#");
          return string;
        })
        .join("|");


      //  console.log('this.courseDataService.workflowId', this.courseDataService.workflowId);

      const data = {
        wfId: null,
        aId: 3,
        iId: this.service.coursebundledata.workflowid,
        // userId: this.content.userId,
        upString: option
      }
      console.log('data', data);
      const _urlAreaBulEnrol: string = webApi.domain + webApi.url.area_bulk_enrol;
      this.commonFunctionService.httpPostRequest(_urlAreaBulEnrol, data)
        //this.service.areaBulkEnrol(data)
        .then(rescompData => {
          // this.loader =false;
          this.spinner.hide();
          var temp: any = rescompData;
          this.addEditModRes = temp.data;
          if (temp == 'err') {
            this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
              timeOut: 0,
              closeButton: true
            });
          } else if (temp.type == false) {
            this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
              timeOut: 0,
              closeButton: true
            });
          } else {
            this.enableUpload = false;
            this.fileName = 'Click here to upload an excel file to enrol '+this.currentBrandData.employee.toLowerCase()+'to this course';
            if (temp['data'].length > 0) {
              if (temp['data'].length === temp['invalidCnt'].invalidCount) {
                this.toastr.warning('No valid '+this.currentBrandData.employee.toLowerCase()+'s found to enrol.', 'Warning', {
                  closeButton: false
                });
              } else {
                this.toastr.success('Enrol successfully.', 'Success', {
                  closeButton: false

                });
                this.allmanualenrol(this.service.coursebundledata);
              }
              this.invaliddata = temp['data'];
              this.showInvalidExcel = true;

              // this.invaliddata = temp['data'];
              // if (this.uploadedData.length == this.invaliddata.length) {
              //   this.toastr.warning('No valid employees found to enrol.', 'Warning', {
              //     closeButton: false
              //     });
              // } else {
              //   this.toastr.success('Enrol successfully.', 'Success', {
              //     closeButton: false
              //     });
              // }
            } else {
              this.invaliddata = [];
              this.showInvalidExcel = false;
            }




            // this.backToEnrol()
            this.cdf.detectChanges();
            this.preview = false;
            this.showInvalidExcel = true;

          }
          console.log('Enrol bulk Result ', this.addEditModRes);
          this.cdf.detectChanges();
        },
          resUserError => {
            //this.loader = false;
            this.errorMsg = resUserError;
          });
    } else {
      if (this.uploadedData.length == 0) {
        this.toastr.warning('No data found.', 'warning', {
          closeButton: true
        });
      } else {
        this.toastr.warning('File data can not exceed more than 2000.', 'warning', {
          closeButton: true
        });
      }

    }

  }

  copytext(data) {
    console.log("Assets url:>", data);
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = data;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.toastr.success('Link Copied', 'Success');
  }
  showGenrateButton = false;
  linkGenerationInProgress = false;
  checkWorkflowUrl(content){
    if(content['redirectionLink'] && content['redirectionLink'] !== null){
      this.shortUrl = content['redirectionLink'];
      this.showGenrateButton = true;
    } else {
      this.showGenrateButton = false;
    }
  }

  generateWorkflowUrl(){
    this. linkGenerationInProgress = true;
    this.cdf.detectChanges();
    const _generateURL: string = webApi.domain + webApi.url.genearteFirebaseUrl;
    const tenant_Info = JSON.parse(localStorage.getItem('tenant_Info'));
    let portalUrl = '';
    if(tenant_Info){
      let domainURL = window.location.hostname;
      // let domainURL = 'devadmin.edgelearning.co.in';
      if(domainURL === 'localhost'){
        portalUrl = '';
      }else {
        if(domainURL){
          domainURL =  domainURL.substring(domainURL.indexOf('.')+1);
        }
        portalUrl = tenant_Info.tenantCode + '.' + domainURL;
      }
    }
    const params = {
      'instanceId': this.formdatabundle['workflowid'],
      'areaId': 3,
      'indentifier': 'workflow',
      'portalUrl': portalUrl,
      'platform': 3,
    };
    params['link'] = 'pages/dynamic-link-learn/' + '3' + '/' + this.formdatabundle['workflowid'] + '/'  + 'workflow';
    this.commonFunctionService.httpPostRequest(_generateURL, params).then((res) =>{
      console.log('Response' ,res);
      if(res['statusCode'] === 200) {
        this.shortUrl = res['data']['shortLink'];
        this.showGenrateButton = true;
        this.cdf.detectChanges();
        this.linkGenerationInProgress = false;
      }else{
        this.toastr.error('Unable to generate link at this time', 'Error', {
          closeButton: false,
        });
        this.linkGenerationInProgress = false;
      }
    }).catch(function (err) {
      // next(new Error('user not found'));
      this.toastr.error('Unable to generate link at this time', 'Error', {
        closeButton: false,
      });
      this.linkGenerationInProgress = false;
    });
  }

  // ngOnDestroy(){
  //   let e1 = document.getElementsByTagName('nb-layout');
  //   if(e1 && e1.length !=0){
  //     e1[0].classList.add('with-scroll');
  //   }
  // }
  // ngAfterContentChecked() {
  //   let e1 = document.getElementsByTagName('nb-layout');
  //   if(e1 && e1.length !=0)
  //   {
  //     e1[0].classList.remove('with-scroll');
  //   }

  //  }
    conentHeight = '0px';
   offset: number = 100;
   ngOnInit() {
     this.conentHeight = '0px';
     const e = document.getElementsByTagName('nb-layout-column');
     console.log(e[0].clientHeight);
     // this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
     if (e[0].clientHeight > 1300) {
       this.conentHeight = '0px';
       this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
     } else {
       this.conentHeight = String(e[0].clientHeight) + 'px';
     }
    this.currentBrandData = this.brandService.getCurrentBrandData();
    this.fileName = 'Click here to upload an excel file to enrol ' +this.currentBrandData.employee.toLowerCase()+ ' to this course';
   }

   sum = 40;
   addItems(startIndex, endIndex, _method, asset) {
     for (let i = startIndex; i < endIndex; ++i) {
       if (asset[i]) {
         this.coursebundle[_method](asset[i]);
       } else {
         break;
       }

       // console.log("NIKHIL");
     }
   }
   // sum = 10;
   onScroll(event) {

     let element = this.myScrollContainer.nativeElement;
     // element.style.height = '500px';
     // element.style.height = '500px';
     // Math.ceil(element.scrollHeight - element.scrollTop) === element.clientHeight

     if (element.scrollHeight - element.scrollTop - element.clientHeight < 5) {
       if (this.newArray.length > 0) {
         if (this.newArray.length >= this.sum) {
           const start = this.sum;
           this.sum += 50;
           this.addItems(start, this.sum, 'push', this.newArray);
         } else if ((this.newArray.length - this.sum) > 0) {
           const start = this.sum;
           this.sum += this.newArray.length - this.sum;
           this.addItems(start, this.sum, 'push', this.newArray);
         }
       } else {
         if (this.coursebundle1.length >= this.sum) {
           const start = this.sum;
           this.sum += 50;
           this.addItems(start, this.sum, 'push', this.coursebundle1);
         } else if ((this.coursebundle1.length - this.sum) > 0) {
           const start = this.sum;
           this.sum += this.coursebundle1.length - this.sum;
           this.addItems(start, this.sum, 'push', this.coursebundle1);
         }
       }


       console.log('Reached End');
     }
   }
   newArray =[];
    // this function is use to dynamicsearch on table
  searchQuestion(event) {
    var temData = this.coursebundle1;
    this.searchText = event.target.value
    var val = event.target.value.toLowerCase();
    var keys = [];
    if(event.target.value == '')
    {
      this.coursebundle =[];
      this.newArray = [];
      this.sum = 50;
      this.addItems(0, this.sum, 'push', this.coursebundle1);
    }else{
         // filter our data
    if (temData.length > 0) {
      keys = Object.keys(temData[0]);
    }
    if(val.length>=3||val.length==0){
    var temp = temData.filter((d) => {
      for (const key of keys) {
        if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
          return true;
        }
      }
    });
    if(temp.length==0){
      this.nopage=true
      this.noDataVal={
        margin:'mt-5',
        imageSrc: '../../../../../assets/images/no-data-bg.svg',
        title:"Sorry we couldn't find any matches please try again",
        desc:".",
        titleShow:true,
        btnShow:false,
        descShow:false,
        btnText:'Learn More',
        btnLink:''
      }
    }
    // update the rows
    this.coursebundle =[];
    this.newArray = temp;
    this.sum = 50;
    this.addItems(0, this.sum, 'push', this.newArray);
  }
}
}
btnName:string='Yes'
clickTodisableWorkflow(courseData, i) {

  if(this.coursebundle[i].wrkflwStatus == 'inactive') {
    this.coursebundle[i].wrkflwStatus = 'active'
  } else {
    this.coursebundle[i].wrkflwStatus = 'inactive'
  }

  this.clickTodisable(courseData);


    // if (courseData.wrkflwStatus == 'inactive' ) {
    //   this.enable = false;
    //   this.title="Enable Pathway"
    //   this.workflowData = courseData;
    //   this.enableDisableModal = true;
    // } else {
    //   this.enable = true;
    //   this.title="Disable Pathway"
    //   this.workflowData = courseData;
    //   this.enableDisableModal = true;

    // }
}
enableDisableWorkflowAction(actionType) {
  if (actionType == true) {

    if (this.workflowData.wrkflwStatus == 'inactive' ) {
      // this.workflowData.visible = 0;
      var courseData = this.workflowData;
      this.clickTodisable(courseData);
    } else {
      // this.workflowData.visible = 1;
      var courseData = this.workflowData;
      this.clickTodisable(courseData);
    }
  } else {
    this.closeEnableDisableModal();
  }
}
closeEnableDisableModal() {
  this.enableDisableModal = false;
}
}


