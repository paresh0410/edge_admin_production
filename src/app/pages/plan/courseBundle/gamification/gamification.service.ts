import {Injectable, Inject} from '@angular/core';
import {Http, Response, Headers, RequestOptions, Request} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppConfig} from '../../../../app.module';
@Injectable()

export class GamificationService {

  public data: any;
  request: Request;

  private _url:string = "";

  constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private _http: Http){
      //this.busy = this._http.get('...').toPromise();
  }

  getData() {
     let url:any = `${this.config.FINAL_URL}`+this._url;
     // let url:any = `${this.config.DEV_URL}`+this._urlFilter;
     let headers = new Headers({ 'Content-Type': 'application/json' });
     let options = new RequestOptions({ headers: headers });
     //let body = JSON.stringify(user);
     return this._http.post(url, options ).map((res: Response) => res.json());
  }

  _errorHandler(error: Response){
     console.error(error);
     return Observable.throw(error || "Server Error")
  }


}
