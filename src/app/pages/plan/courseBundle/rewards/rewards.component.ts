import { Host, ChangeDetectionStrategy, Component, ViewEncapsulation, Directive,forwardRef,Attribute,OnChanges,SimpleChanges,Input, ViewChild, ViewContainerRef,OnInit } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { CourseBundle } from '../courseBundle';
import { CourseBundleService } from '../courseBundle.service';

@Component ({
   selector: 'course-rewards',
   templateUrl: './rewards.html',
   styleUrls: ['./rewards.scss'],
   encapsulation: ViewEncapsulation.None   
})

export class rewardsComponent {
  
	@ViewChild('myTable') table: any;
  	@ViewChild(DatatableComponent) tableData: DatatableComponent;

  	dropdownListUsers:any;
  	selectedItemsUsers:any;
  	dropdownSettingsUsers:any;
  	demoData:any=[];

  	selected:any = [];
  	rows:any=[];
  	temp = [];

  	columns = [
    	{ prop: 'name' },
    	{ name: 'Company' },
    	{ name: 'Gender' }
  	];	
     condata:any;
    badge:any;
    certificate:any;

  	showEnrolpage:boolean = false;
    badgeshow:boolean=false;
    certshow:boolean=false;

	  reviewCheck:any = {
	    value1 : false,
	    value2 : false,
	    value3 : false,
	  }

	  enrolment:any = {
	    manual:'',
	    rule:'',
	    regulatory:'',
	    self:''
	  };
      rewards:any = {
      id:'',
      conid:'',
      imgsrc:'',
      name:'',
      descri:''

    };
      badgeData:any =[
      {
        bid:1,
        bimgsrc:'assets/images/course1.jpg',
        bname:'Badge 1',
        bdescri:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book'
      },
          {
        bid:2,
        bimgsrc:'assets/images/course1.jpg',
        bname:'Badge 2',
        bdescri:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book'
      },
          {
        bid:3,
        bimgsrc:'assets/images/course1.jpg',
        bname:'Badge 3',
        bdescri:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book'
      },
          {
        bid:4,
        bimgsrc:'assets/images/course1.jpg',
        bname:'Badge 4',
        bdescri:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book'
      },
          {
        bid:5,
        bimgsrc:'assets/images/course1.jpg',
        bname:'Badge 5',
        bdescri:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book'
      },
          {
        bid:6,
        bimgsrc:'assets/images/course1.jpg',
        bname:'Badge 6',
        bdescri:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book'
      },
      ]

       certData:any =[
      {
        cid:1,
        cimgsrc:'assets/images/course1.jpg',
        cname:'Template 1',
        cdescri:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book'
      },
          {
        cid:2,
        cimgsrc:'assets/images/course1.jpg',
        cname:'Template 2',
        cdescri:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book'
      },
          {
        cid:3,
        cimgsrc:'assets/images/course1.jpg',
        cname:'Template 3',
        cdescri:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book'
      },
          {
        cid:4,
        cimgsrc:'assets/images/course1.jpg',
        cname:'Template 4',
        cdescri:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book'
      },
          {
        cid:5,
        cimgsrc:'assets/images/course1.jpg',
        cname:'Template 5',
        cdescri:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book'
      },
          {
        cid:6,
        cimgsrc:'assets/images/course1.jpg',
        cname:'Template 6',
        cdescri:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book'
      },
      ]

	showAddRuleModal:boolean = false;  
	showAddRegulatoryModal:boolean = false;  
	showAddSelfModal:boolean = false;

	errorMsg:any;  
  	profileFields:any = [];


  	constructor() {
  		
  		this.demoData = [{ "id": 1, "itemName": "India" },
                              { "id": 2, "itemName": "Singapore" },
                              { "id": 3, "itemName": "Australia" },
                              { "id": 4, "itemName": "Canada" },
                              { "id": 5, "itemName": "South Korea" },
                              { "id": 6, "itemName": "Brazil" }];
	    this.dropdownListUsers = this.demoData;
	    this.selectedItemsUsers = [];
	    this.dropdownSettingsUsers = { 
	                              singleSelection: false, 
	                              text:"Users",
	                              selectAllText:'Select All',
	                              unSelectAllText:'UnSelect All',
	                              enableSearchFilter: true,
	                              badgeShowLimit: 2,
	                              classes:"myclass custom-class"
	                            };

	    this.fetch((data) => {
	      // cache our list
	      this.temp = [...data];
	      this.rows = data;
	    });      

	    // this.getUserProfileFields();
  	}

  // 	getUserProfileFields(){
  //   	this.addEditCourseService.getUserProfileFields()
  //   	.subscribe(rescompData => { 
  //     	// this.loader =false;
  //     	this.profileFields = rescompData.data[0];
  //     	// this.topic = rescompData.data[0];
  //     	console.log('User profile fields',this.profileFields); 
  //   },
  //   resUserError => {
  //     // this.loader =false;
  //     this.errorMsg = resUserError
  //   });
  // }

  	fetch(cb) {
    const req = new XMLHttpRequest();
    req.open('GET', `assets/data/company.json`);
    // req.open('GET', `assets/data/100k.json`);

    req.onload = () => {
      cb(JSON.parse(req.response));
    };

    req.send();
  }  

  onSelect({ selected }) {
    console.log('Select Event', selected, this.selected);

    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }  

  toggleExpandRow(row) {
    console.log('Toggled Expand Row!', row);
    this.table.rowDetail.toggleExpandRow(row);
  }

  onDetailToggle(event) {
    console.log('Detail Toggled', event);
  }

  onActivate(event) {
    console.log('Activate Event', event);
  }

  selectTab(tabEvent){
    console.log('tab Selected', tabEvent);
  }

  searchManEnrol(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function(d) {
      return d.name.toLowerCase().indexOf(val) !== -1 || 
      d.gender.toLowerCase() === val || 
      d.company.toLowerCase().indexOf(val) !== -1 || 
      String(d.age).toLowerCase().indexOf(val) !== -1 || 
      !val;
    });

    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    this.tableData.offset = 0;
  }

  searchRuleEnrol(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function(d) {
      return d.name.toLowerCase().indexOf(val) !== -1 || 
      d.gender.toLowerCase() === val || 
      d.company.toLowerCase().indexOf(val) !== -1 || 
      String(d.age).toLowerCase().indexOf(val) !== -1 || 
      !val;
    });

    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    this.tableData.offset = 0;
  }

  searchRegEnrol(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function(d) {
      return d.name.toLowerCase().indexOf(val) !== -1 || 
      d.gender.toLowerCase() === val || 
      d.company.toLowerCase().indexOf(val) !== -1 || 
      String(d.age).toLowerCase().indexOf(val) !== -1 || 
      !val;
    });

    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    this.tableData.offset = 0;
  }

  searchSelfEnrol(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function(d) {
      return d.name.toLowerCase().indexOf(val) !== -1 || 
      d.gender.toLowerCase() === val || 
      d.company.toLowerCase().indexOf(val) !== -1 || 
      String(d.age).toLowerCase().indexOf(val) !== -1 || 
      !val;
    });

    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    this.tableData.offset = 0;
  }

  enrolUser(){
    this.showEnrolpage = !this.showEnrolpage;
  }

  onCheckBoxClick(rid){
    // if(event == false){
    //   this.reviewCheck = {};
    // }
        // if(rid==1)
        // {
        //   this.certificate =true;
        // }else if(rid==2)
        // {
        //   this.badge=true;
        // }

    // console.log('$event',event);
    // console.log('courseReviewCheck',courseReviewCheck);

  }

  setActiveItem(index,item){
    console.log("Selected Module",item,index);
  }

  // openRule(){
  // 	this.parent_Comp.openRuleModal();
  // 	this.showAddRuleModal = this.addEditCourseService.showRule;
  // }

  // openRegulatory(){
  // 	this.parent_Comp.openRegulatoryModal();
  // 	this.showAddRegulatoryModal = this.addEditCourseService.showRegulatory;
  // }

  // openSelf(){
  // 	this.parent_Comp.openSelfModal();
  // 	this.showAddSelfModal = this.addEditCourseService.showSelf;
  // }

  browse(rid)
  {
    if(rid==1)
    {
      this.badgeshow =true;
    }
    if(rid==2)
    {
      this.certshow=true;
    }
  }
  closeModel(rid)
  {
     if(rid==1)
    {
      this.badgeshow =false;
    }
    if(rid==2)
    {
      this.certshow=false;
    }
  }

  save(rid)
  {
    if(rid ==1)
    {
      this.rewards = {
      id:rid,
      conid:this.condata.bid,
      imgsrc:this.condata.bimgsrc,
      name:this.condata.bname,
      descri:this.condata.bdescri
    };
    this.badgeshow =false;
     this.badge=true;
    this.certificate =true;
  }
     if(rid ==2)
    {
      
      this.rewards = {
      id:rid,
      conid:this.condata.cid,
      imgsrc:this.condata.cimgsrc,
      name:this.condata.cname,
      descri:this.condata.cdescri
    };
     this.badge=true;
      this.certificate =true;
     this.certshow =false; 
    // console.log('item',item)

  }
}

  onCheckBoxselecClick(item)
  {
    this.condata={};
   
   this.condata=item;
  }
  remove(rid)
  {
    
      if(rid==1)
    {
      this.rewards ={};
    }
    if(rid==2)
    {
     this.rewards ={};
    }
     this.certificate =false;
     this.badge=false;
  }
  
  saveRewards(){
    // this.rewardsTab = false;
    
  }

  // closeRule(){
  // 	this.parent_Comp.closeRuleModal();
  // 	this.showAddRuleModal = this.addEditCourseService.showRule;

  // }


}