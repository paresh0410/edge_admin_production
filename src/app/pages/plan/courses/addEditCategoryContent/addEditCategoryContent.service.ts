import {Injectable, Inject} from '@angular/core';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppConfig} from '../../../../app.module';

import { webAPIService } from '../../../../service/webAPIService'
import { webApi } from '../../../../service/webApi'
import { HttpClient } from "@angular/common/http";
@Injectable()
export class  AddEditCategoryContentService {

  public data:any;
  private _url:string = "/api/edge/category/addUpdateCategory" //DevTest
  private _urlCheckCatCode:string = webApi.domain + webApi.url.checkCategory;

  request: Request;
  
  constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private _http: Http, private http1 :HttpClient){
      //this.busy = this._http.get('...').toPromise();
  }

  // new code start //
  checkCategory(catData){
    
      return new Promise(resolve => {
        this.http1.post(this._urlCheckCatCode, catData)
          //.map(res => res.json())
          .subscribe(data => {
            resolve(data);
          },
            err => {
              resolve('err');
            });
      });
  }
  // new code end //

  // createUpdateCategory(category){
  //   let url:any = `${this.config.FINAL_URL}`+this._url;
  //   return this._http.post(url,category)
  //     .map((response:Response) => response.json())
  //     .catch(this._errorHandler);
  // }

  _errorHandler(error: Response){
    console.error(error);
    return Observable.throw(error || "Server Error")
  }

}
