import { ElementRef, Host, ChangeDetectionStrategy, Component, ChangeDetectorRef, ViewEncapsulation, Directive, forwardRef, Attribute, OnChanges, SimpleChanges, Input, ViewChild, ViewContainerRef, OnInit } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router, NavigationStart, ActivatedRoute } from '@angular/router';
import { NgbCalendar, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { enrolService } from './enrolment/enrolment.service';
import { AddEditCourseContentService } from './addEditCourseContent.service';
import { FormGroup, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { ToastrService } from 'ngx-toastr';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { Content } from '../content/content';
import { contentReady } from '@syncfusion/ej2-grids';
import { DatePipe } from '@angular/common';
import { FileSelectDirective, FileDropDirective, FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { NgxSpinnerService } from 'ngx-spinner';
import { XlsxToJsonService } from '../../../plan/users/uploadusers/xlsx-to-json-service';
import { JsonToXlsxService } from '../../../coaching/participants/bulk-upload-coaching/json-to-xlsx.service';
import { CommonFunctionsService } from '../../../../service/common-functions.service';
import { webApi } from '../../../../service/webApi';
import { SuubHeader } from '../../../components/models/subheader.model';
import { PassService } from '../../../../service/passService';
import { EventEmitterService } from '../../../../service/event-emitter.service';
import { Subject } from 'rxjs';
import { BrandDetailsService } from '../../../../service/brand-details.service';
import { noData } from '../../../../models/no-data.model';
import { ContentService } from '../content/content.service';
// import { detailsComponent } from './details/details.component';

@Component({
  selector: 'addEditCourseContent',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './addEditCourseContent.html',
  styleUrls: ['./addEditCourseContent.scss', './myDatePickerComp.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [DatePipe]
  // providers:[detailsComponent]
  // declarations:[detailsComponent]
  // directives: [detailsComponent]
})

export class AddEditCourseContent {
  @ViewChild('fileUpload') fileUpload: any;
  colorTheme = 'theme-dark-blue';

  bsConfig: Partial<BsDatepickerConfig>
  private xlsxToJsonService: XlsxToJsonService = new XlsxToJsonService();
  addEngagePopup: boolean;
  selectMethod: String;

  model: NgbDateStruct;
  today = this.calendar.getToday();
  enrolldata: any;
  // @Input() detailsComp : detailsComponent;
  // @ViewChild(detailsComponent ) detailsComp: detailsComponent ;


  @ViewChild('myTable') table: any;
  @ViewChild(DatatableComponent) tableData: DatatableComponent;
  dropdownListUsers: any;
  selectedItemsUsers: any;
  dropdownSettingsUsers: any;
  demoData: any = [];
  resultdata: any = [];
  selected: any = [];
  rows: any = [];
  temp = [];

  noDataVal:noData={
    margin:'mt-3 mb-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No enrollment .",
    desc:"Admins will be able to add API's to the organisation.",
    titleShow:true,
    btnShow:true,
    descShow:false,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/learning-how-to-enrol-users-to-an-online-course',
  }
  showdate: boolean = false;
  showdays: boolean = false;

  // columns = [
  //   { prop: 'name' },
  //   { name: 'Company' },
  //   { name: 'Gender' }
  // ];
  labels: any = [
    { labelname: 'ECN', bindingProperty: 'ecn', componentType: 'text' },
    { labelname: 'FULL NAME', bindingProperty: 'fullname', componentType: 'text' },
    { labelname: 'EMAIL', bindingProperty: 'emailId', componentType: 'text' },
    { labelname: 'MOBILE', bindingProperty: 'phoneNo', componentType: 'text' },
    { labelname: 'D.0.E', bindingProperty: 'enrolDate', componentType: 'text' },
    { labelname: 'MODE', bindingProperty: 'enrolmode', componentType: 'text' },
    { labelname: 'ACTION', bindingProperty: 'btntext', componentType: 'button' },
    // { labelname: 'Action', bindingProperty: 'action', componentType: 'select' },
    // { labelname: 'Action', bindingProperty: 'action', componentType: 'checkbox' },

  ];

  columns = [
    { prop: 'ecn', name: 'EMP CODE' },
    { prop: 'fullname', name: 'FULLNAME' },
    { prop: 'gender', name: 'GENDER' },
    { prop: 'doj', name: 'DOJ' },
    { prop: 'department', name: 'DEPARTMENT' },
    { prop: 'mode', name: 'MODE' }
  ];

  showEnrolpage: boolean = false;
  showBulkpage: boolean = false;

  reviewCheck: any = {
    value1: false,
    value2: false,
    value3: false,
  }

  enrolment: any = {
    manual: '',
    rule: '',
    regulatory: '',
    self: ''
  };

  searchvalue: any = {
    value: ''
  };

  courseDetails: boolean = true;
  courseModules: boolean = false;
  courseEnrol: boolean = false;
  courseEngage: boolean = false;
  courseRewards: boolean = false;
  courseGamification = false;

  detailsTab: boolean = true;

  detailsDone =false;
  
  modulesTab: boolean = false;
  enrolTab: boolean = false;
  engageTab: boolean = false;
  rewardsTab: boolean = false;
  gamificationTab: boolean = false;
  enableCourse: boolean = false;
  enablefield: boolean = false;
  showAddRuleModal: boolean = false;
  showAddRegulatoryModal: boolean = false;
  showAddSelfModal: boolean = false;

  enableDisableCourseModal: boolean = false;
  enableDisableCourseData: any;
  courseDisableIndex: any;
  backflag = 1;
  ruleType: any = [{
    ruleTypeId: 1,
    ruleTypeName: 'Profile Fields'
  }, {
    ruleTypeId: 2,
    ruleTypeName: 'Other'
  }];

  ruleSubType: any = [{
    ruleTypeId: 1,
    ruleSubTypeId: 1,
    ruleSubTypeName: 'Username',
  }, {
    ruleTypeId: 1,
    ruleSubTypeId: 2,
    ruleSubTypeName: 'Department',
  }, {
    ruleTypeId: 2,
    ruleSubTypeId: 1,
    ruleSubTypeName: 'Doj',
  }, {
    ruleTypeId: 2,
    ruleSubTypeId: 2,
    ruleSubTypeName: 'Custom date',
  }];

  ruleData: any = {
    id: '',
    name: '',
    description: '',
    type: '',
    subType: '',
    value: ''
  };

  formdata: any = {
    id: '',
    shortname: '',
    name: '',
    datatype: '',
    selected: ''
  }

  strArrayType: any = [[]];
  selectedFilterOption = [];
  strArrayPar: any = [];

  public addRulesForm: FormGroup;
  controlList: any = [{ datatype: '' }];
  controlFlag: any = false;

  profileFields: any = [];
  errorMsg: any;
  loader: any;

  private ValueId: number = 0;
  strArrayTypePar: any = [];
  // selectedFilterOption = [];
  selectedRule: any = [];
  // ruleType:any
  selectedRuleType: any = {
    id: ''
  };

  profileFieldSelected: boolean = false;

  strArraySkilllevel: any = [{
    sName: 'Beginner',
    sId: 'Beginner'
  },
  {
    sName: 'Intermediate',
    sId: 'Intermediate'
  },
  {
    sName: 'Expert',
    sId: 'Expert'
  }]

  menuType = [];
  datetimeType = [];
  textType = [];
  textareaType = [];

  strArrayTypeSelfFields: any = [[]];
  selectedFilterOptionSelf = [];
  strArrayParSelfFields: any = [];

  public addSelfFieldsForm: FormGroup;
  controlListSelfFields: any = [{ datatype: '' }];
  controlFlagSelfFields: any = false;

  profileFieldsSelf: any = [];
  enabledata: any = [];
  enableuser: any = [];
  selfType: any = [{
    typeId: 1,
    typeName: 'Open'
  }, {
    typeId: 2,
    typeName: 'Approval'
  }];

  selfFeildType: any = [{
    selfTypeId: 1,
    selfTypeName: 'Profile Fields'
  }, {
    selfTypeId: 2,
    selfTypeName: 'Other'
  }];

  header: SuubHeader;
  selfFieldsData: any = {
    id: '',
    name: '',
    description: '',
    type: '',
    subType: '',
    value: '',
    maxCount: ''
  };

  formdataSelf: any = {
    id: '',
    shortname: '',
    name: '',
    datatype: '',
    selected: ''
  }

  // ruleType:any
  selectedSelfFieldsType: any = {
    id: ''
  };

  selfProfileFieldSelected: boolean = false;
  private selfFieldValueId: number = 0;
  strArrayTypeParSelfFields: any = [];
  // selectedFilterOption = [];
  selectedSelfFields: any = [];

  // @Host() private details_Comp: detailsComponent,

  @ViewChild('addEditCourseTabs') activityForm: ElementRef;
  selectedActInd: any = 0;

  content: any;
  userLoginData: any = [];
  userdata: any;
  courseIdEn: any;

  tenantId: any;
  rowsTest: any;
  uId: any;
  enrol: boolean = false;
  btnFlag: number;
  isdata: any;
  eventsSubject: Subject<void> = new Subject<void>();
  searchText: any;
  currentBrandData: any;
  nodata: boolean = true;
  tabTitle: any;


  constructor(private spinner: NgxSpinnerService, private calendar: NgbCalendar, private _fb: FormBuilder,
    protected courseDataService: AddEditCourseContentService, protected enrolService: enrolService,
    public brandService: BrandDetailsService,
    vcr: ViewContainerRef, private router: Router, private route: ActivatedRoute, private exportService: JsonToXlsxService,
    private commonFunctionService: CommonFunctionsService,
    private passService: PassService,
    protected contentService: ContentService,
    private eventEmitterService: EventEmitterService,
    
    private toasterService: ToasterService, private toastr: ToastrService, public cdf: ChangeDetectorRef, private datePipe: DatePipe) {
    var userData = JSON.parse(localStorage.getItem('LoginResData'));
    console.log('userData', userData.data);
    // this.userId = userData.data.data.id;
    this.uId = userData.data.data.id;
    this.tenantId = userData.data.data.tenantId;
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });

    this.commonFunctionService.formatDateTimeForBulkUpload('1-12-2020');
    this.demoData = [];
    this.dropdownListUsers = this.demoData;
    this.selectedItemsUsers = [];
    this.dropdownSettingsUsers = {
      singleSelection: false,
      text: 'Users',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      badgeShowLimit: 2,
      classes: 'myclass custom-class'
    };

    this.fetch((data) => {
      // cache our list
      // this.temp = [...data];
      // for(let i= 0; i < data.length; i ++) {
      //   if(data[i].visible = 1) {
      //     data[i].btntext = 'enable';
      //   } else {
      //     data[i].btntext = 'disabled';
      //   }
      // }

      // this.rowsTest = data;
      // console.log('this.rows+++++++++++++++++++++++++++++++++++++++++++++++++++++++>',this.rowsTest);

    });

    var content;
    var optId;
    var catId;


    // if(this.service.data != undefined){
    //   content = this.service.data.data;
    //   optId = this.service.data.id;
    //   catId = this.service.data.catId;
    //   this.conData = this.service.data.data;
    // }else{
    //   optId = 0;
    // }

    // this.getUserProfileFields();
    if (localStorage.getItem('LoginResData')) {
      this.userLoginData = JSON.parse(localStorage.getItem('LoginResData'));
      this.userdata = this.userLoginData.data.data;
      console.log('login data', this.userdata);
    }

    try {
      if (this.courseDataService.data) {
        this.content = this.courseDataService.data.data;
        this.content.userId = this.userdata.id;
        this.courseIdEn = this.content.courseId;
        console.log('content', this.content);

      }
    } catch
    {
      this.content = {};
      this.courseIdEn = undefined
    }
  }

  addRemoveClassElement(element: any, classname) {
    if (element) {
      var elementContent = element.nativeElement;
      if (elementContent.classList.length > 0) {
        var index = elementContent.className.indexOf(classname);
        if (index > -1) { //exist
          elementContent.classList.remove(classname);
        } else {
          elementContent.classList.add(classname);
        }
      }
    }
  }
  clear() {
    if (this.searchText.length >= 3) {
      this.searchvalue = {};
      this.allEnrolUser(this.content);
    }
    else {
      this.searchvalue = {};
      this.searchText = ''
    }
  }
  fetch(cb) {
    // const req = new XMLHttpRequest();
    // // req.open('GET', `assets/data/company.json`);
    // req.open('GET', `assets/data/enroledUsers.json`);
    // // req.open('GET', `assets/data/100k.json`);

    // req.onload = () => {
    //   cb(JSON.parse(req.response));
    // };

    // req.send();
  }

  onSelectEnrolData({ selected }) {
    console.log('Select Event', selected);
    // console.log('Select Data', this.selected);

    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }

  searchEnrolUser(event) {
    const val = event.target.value.toLowerCase();

    // this.allEnrolUser( this.courseDataService.data.data)
    this.searchText = val;
    this.temp = [...this.enrolldata];
    console.log(this.temp);
    // filter our data
    if (val.length >= 3 || val.length == 0) {
      const temp = this.temp.filter(function (d) {
        return String(d.ecn).toLowerCase().indexOf(val) !== -1 ||
          d.fullname.toLowerCase().indexOf(val) !== -1 ||
          d.emailId.toLowerCase().indexOf(val) !== -1 ||
          d.enrolmode.toLowerCase().indexOf(val) !== -1 ||
          d.enrolDate.toLowerCase().indexOf(val) !== -1 ||
          d.phoneNo.toLowerCase().indexOf(val) !== -1 ||
          String(d.enrolmode).toLowerCase().indexOf(val) !== -1 ||
          // d.mode.toLowerCase() === val || !val;
          !val
      });

      // update the rows
      this.rows = [...temp];
      this.rowsTest = [...temp]
      // this.tableData.offset = 0;
      if(this.rows.length == 0){
        this.nodata = true
      }else{
        this.nodata = false
      }
      console.log(this.rows, "this.rows")
    }

    // Whenever the filter changes, always go back to the first page
  }

  onEnrolDataClickEvent(event) {
    // if (event.type == 'click' && event.cellIndex != 0) {
    //     // Do something when you click on row cell other than checkbox.
    //     console.log('Activate Event', event);
    // }
    // const checkboxCellIndex = 1;
    if (event.type === 'checkbox') {
      // Stop event propagation and let onSelect() work
      console.log('Checkbox Selected', event);
      event.event.stopPropagation();
    } else if (event.type === 'click' && event.cellIndex != 0) {
      // Do somethings when you click on row cell other than checkbox
      console.log('Row Clicked', event.row); /// <--- object is in the event row variable
    }
  }

  deleteEnrolData(selectedRow) {
    console.log('Current User', selectedRow);
    for (let i = 0; i < this.rows.length; i++) {
      var row = this.rows[i];
      if (selectedRow.ecn == row.ecn) {
        this.rows.splice(i, 1);
        this.rows = [...this.rows];
      }
    }
    this.temp = this.rows;
    // this.tableData.offset = 0;
  }

  timeout: any;
  onPageEnrolData(event) {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      console.log('paged!', event);
    }, 100);
  }

  enrolUser() {
    this.showEnrolpage = !this.showEnrolpage;
  }

  toggleExpandRow(row) {
    // console.log('Toggled Expand Row!', row);
    this.table.rowDetail.toggleExpandRow(row);
  }

  onDetailToggle(event) {
    // console.log('Detail Toggled', event);
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function (d) {
      return d.name.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    this.tableData.offset = 0;
  }

  onCheckBoxClick(event, courseReviewCheck) {
    if (event == false) {
      this.reviewCheck = {};
    }
    // console.log('$event',$event);
    console.log('courseReviewCheck', courseReviewCheck);

  }

  setActiveItem(index, item) {
    console.log('Selected Module', item, index);
  }

  selectedTab(tabEvent) {
    console.log('tab Selected', tabEvent);
    this.eventEmitterService.subsVar = undefined;

   

    if (tabEvent.tabTitle == 'Details') {

      this.courseDetails = true;
      this.courseModules = false;
      this.courseEnrol = false;
      this.courseEngage = false;
      this.courseRewards = false;
      this.courseGamification = false;
      this.modulesTab = false;
      this.detailsTab = true;
      this.enrolTab = false;
      this.engageTab = false;
      this.rewardsTab = false;

      this.passService.detailsDone =false;
    } 

    if(this.passService.detailsDone == false) {
      return;
    }
    
    
    if (tabEvent.tabTitle == 'Modules') {
      this.courseDetails = false;
      this.courseModules = true;
      this.courseEnrol = false;
      this.courseEngage = false;
      this.courseRewards = false;
      this.courseGamification = false;
      this.modulesTab = true;
      this.detailsTab = false;
      this.enrolTab = false;
      this.engageTab = false;
      this.rewardsTab = false;

      this.passService.detailsDone =true;
    } else if (tabEvent.tabTitle == 'Enrol') {
      this.spinner.show();
      this.courseDetails = false;
      this.courseModules = false;
      this.courseEnrol = true;
      this.courseEngage = false;
      this.courseRewards = false;
      this.courseGamification = false;
      this.modulesTab = false;
      this.detailsTab = false;
      this.enrolTab = true;
      this.allEnrolUser(this.content)
      this.engageTab = false;
      this.rewardsTab = false;

      this.passService.detailsDone =true;
    } else if (tabEvent.tabTitle == 'Notification') {
      this.courseDetails = false;
      this.courseModules = false;
      this.courseEnrol = false;
      this.courseEngage = true;
      this.courseRewards = false;
      this.courseGamification = false;
      this.modulesTab = false;
      this.detailsTab = false;
      this.enrolTab = false;
      this.engageTab = true;
      this.rewardsTab = false;

      this.passService.detailsDone =true;
    } else if (tabEvent.tabTitle == 'Rewards') {
      this.courseDetails = false;
      this.courseModules = false;
      this.courseEnrol = false;
      this.courseEngage = false;
      this.courseRewards = true;
      this.courseGamification = false;
      this.modulesTab = false;
      this.detailsTab = false;
      this.enrolTab = false;
      this.engageTab = false;
      this.rewardsTab = true;

      this.passService.detailsDone =true;
    } else if (tabEvent.tabTitle == 'Gamification') {
      this.courseDetails = false;
      this.courseModules = false;
      this.courseEnrol = false;
      this.courseEngage = false;
      this.courseRewards = false;
      this.courseGamification = true;
      this.modulesTab = false;
      this.detailsTab = false;
      this.enrolTab = false;
      this.engageTab = false;
      this.rewardsTab = false;

      this.passService.detailsDone =true;
    }

    this.cdf.detectChanges();

  }

  selectedTabTest(tabEvent) {
    console.log('tab Selected', tabEvent);
    this.tabTitle = tabEvent.tabTitle

    if (tabEvent.tabTitle == 'Details') {
    this.passService.detailsDone = false
    this.header = {
    title: this.content?this.content.fullname:'Add Course',
    btnsSearch: true,
    btnName8: 'Save & Next',
    btnName8show: true,
    btnBackshow: true,
    showBreadcrumb: true,
    }
    this.header.breadCrumbList=this.courseDataService.data.catId ?
    [
     {
       'name': 'Learning',
       'navigationPath': '/pages/learning',
     },
     {
       'name': 'Course Category',
       'navigationPath': '/pages/plan/courses/category',
     },
     {
       'name': this.courseDataService.data.categoryName,
       'navigationPath': '/pages/plan/courses/content',
      //  'navigationPath': '/pages/plan/courses/category',
      
     }
   ]:[
       {
         'name': 'Learning',
         'navigationPath': '/pages/learning',
       },
       {
         'name': 'Online Course',
         'navigationPath': '/pages/plan/courses/content',
       },
   ]

   if(this.courseDataService.breadcrumbArray){
     this.header.title = this.courseDataService.breadtitle
     this.header.breadCrumbList = this.courseDataService.breadcrumbArray
   }
      this.btnFlag = 1;
    } 

    // if(this.content == undefined) {
      if(this.passService.detailsDone == false) {
        this.btnFlag = 1;
        this.courseDetails = true;
        this.courseModules = false;
        this.courseEnrol = false;
        this.courseEngage = false;
        this.courseRewards = false;
        this.courseGamification = false;
        this.modulesTab = false;
        this.detailsTab = true;
        this.enrolTab = false;
        this.engageTab = false;
        this.rewardsTab = false;
       
        if (tabEvent.tabTitle !== 'Details') {
          //new
          if(this.content == undefined ){
          //end new
          this.toastr.warning('Please save details before proceeding to other tabs', 'Warning', {
            timeOut: 0,
            closeButton: true
          });
          return false;
        }
      }
        
        // return false;
      }
    // }
    
  

    
    if (tabEvent.tabTitle == 'Modules') {
        this.header = {
          title: this.content?this.content.fullname:'Add Course',
          btnsSearch: true,
          btnName5: 'Add Module',
          btnName9: 'Save',
          btnName10: 'Cancel',
          btnName5show: true,
          btnBackshow: true,
          showBreadcrumb: true,
        };
          this.header.breadCrumbList=this.courseDataService.data.catId?
          [
           {
             'name': 'Learning',
             'navigationPath': '/pages/learning',
           },
           {
             'name': 'Course Category',
             'navigationPath': '/pages/plan/courses/category',
           },
           {
            'name': this.courseDataService.data.categoryName,
            'navigationPath': '/pages/plan/courses/content',
            // 'navigationPath': '/pages/plan/courses/category',
           }
         ]:[
             {
               'name': 'Learning',
               'navigationPath': '/pages/learning',
             },
             {
               'name': 'Online Course',
               'navigationPath': '/pages/plan/courses/content',
             },
         ]
         if(this.courseDataService.breadcrumbArray){
          this.header.title = this.courseDataService.breadtitle
          this.header.breadCrumbList = this.courseDataService.breadcrumbArray
        }
      this.btnFlag = 1;
      // this.spinner.show();
      // setTimeout(() => {
      //   this.spinner.hide()
      // }, 2000);
      this.courseDetails = false;
      this.courseModules = true;
      this.courseEnrol = false;
      this.courseEngage = false;
      this.courseRewards = false;
      this.courseGamification = false;
      this.modulesTab = true;
      this.detailsTab = false;
      this.enrolTab = false;
      this.engageTab = false;
      this.rewardsTab = false;
    } else if (tabEvent.tabTitle == 'Enrol') {
      this.header = {
        title: this.content?this.content.fullname:'Add Course',
        btnsSearch: true,
        btnName2: 'Enrol',
        btnName3: 'Bulk Enrol',
        btnName2show: true,
        btnName3show: true,
        btnBackshow: true,
        showBreadcrumb: true,
      };
        this.header.breadCrumbList=this.courseDataService.data.catId?
        [
         {
           'name': 'Learning',
           'navigationPath': '/pages/learning',
         },
         {
           'name': 'Course Category',
           'navigationPath': '/pages/plan/courses/category',
         },
         {
          'name': this.courseDataService.data.categoryName,
          'navigationPath': '/pages/plan/courses/content',
         }
       ]:[
           {
             'name': 'Learning',
             'navigationPath': '/pages/learning',
           },
           {
             'name': 'Online Course',
             'navigationPath': '/pages/plan/courses/content',
           },
       ]
       if(this.courseDataService.breadcrumbArray){
        this.header.title = this.courseDataService.breadtitle
        this.header.breadCrumbList = this.courseDataService.breadcrumbArray
      }

      this.btnFlag = 1;
      // this.spinner.show();
      // setTimeout(() => {
      //   this.spinner.hide()
      // }, 2000);
      this.courseDetails = false;
      this.courseModules = false;
      this.courseEnrol = true;
      this.courseEngage = false;
      this.courseRewards = false;
      this.courseGamification = false;
      this.modulesTab = false;
      this.detailsTab = false;
      this.enrolTab = true;
      this.engageTab = false;
      this.rewardsTab = false;
      this.showEnrolpage = false;
      this.showBulkpage = false;
    } else if (tabEvent.tabTitle == 'Notification') {
      this.header = {
        title: this.content?this.content.fullname:'Add Course',
        btnsSearch: true,
        btnName6: 'Add Notification',
        btnName5show: false,
        btnName6show: true,
        btnBackshow: true,
        btnAddshow: false,
        filter: false,
        showBreadcrumb: true,

      };
        this.header.breadCrumbList=this.courseDataService.data.catId?
        [
         {
           'name': 'Learning',
           'navigationPath': '/pages/learning',
         },
         {
           'name': 'Course Category',
           'navigationPath': '/pages/plan/courses/category',
         },
         {
          'name': this.courseDataService.data.categoryName,
           'navigationPath': '/pages/plan/courses/content',
         }
       ]:[
           {
             'name': 'Learning',
             'navigationPath': '/pages/learning',
           },
           {
             'name': 'Online Course',
             'navigationPath': '/pages/plan/courses/content',
           },
       ]
       if(this.courseDataService.breadcrumbArray){
        this.header.title = this.courseDataService.breadtitle
        this.header.breadCrumbList = this.courseDataService.breadcrumbArray
      }

      this.btnFlag = 1;
      // this.spinner.show();
      // setTimeout(() => {
      //   this.spinner.hide()
      // }, 2000);
      this.courseDetails = false;
      this.courseModules = false;
      this.courseEnrol = false;
      this.courseEngage = true;
      this.courseRewards = false;
      this.courseGamification = false;
      this.modulesTab = false;
      this.detailsTab = false;
      this.enrolTab = false;
      this.engageTab = true;
      this.rewardsTab = false;
    } else if (tabEvent.tabTitle == 'Rewards') {
      this.header = {
        title: this.content?this.content.fullname:'Add Course',
        btnsSearch: true,
        btnName7: 'Submit',
        btnName7show: true,
        btnBackshow: true,
        showBreadcrumb: true,
      };
        this.header.breadCrumbList=this.courseDataService.data.catId?
        [
         {
           'name': 'Learning',
           'navigationPath': '/pages/learning',
         },
         {
           'name': 'Course Category',
           'navigationPath': '/pages/plan/courses/category',
         },
         {
          'name': this.courseDataService.data.categoryName,
           'navigationPath': '/pages/plan/courses/content',
         }
       ]:[
           {
             'name': 'Learning',
             'navigationPath': '/pages/learning',
           },
           {
             'name': 'Online Course',
             'navigationPath': '/pages/plan/courses/content',
           },
       ]
       if(this.courseDataService.breadcrumbArray){
        this.header.title = this.courseDataService.breadtitle
        this.header.breadCrumbList = this.courseDataService.breadcrumbArray
      }
      this.backflag=1;
      // this.spinner.show();
      // setTimeout(() => {
      //   this.spinner.hide()
      // }, 2000);
      this.courseDetails = false;
      this.courseModules = false;
      this.courseEnrol = false;
      this.courseEngage = false;
      this.courseRewards = true;
      this.courseGamification = false;
      this.modulesTab = false;
      this.detailsTab = false;
      this.enrolTab = false;
      this.engageTab = false;
      this.rewardsTab = true;
    } else if (tabEvent.tabTitle == 'Gamification') {
      this.courseDetails = false;
      this.courseModules = false;
      this.courseEnrol = false;
      this.courseEngage = false;
      this.courseRewards = false;
      this.courseGamification = true;
      this.modulesTab = false;
      this.detailsTab = false;
      this.enrolTab = false;
      this.engageTab = false;
      this.rewardsTab = false;
    }

    this.cdf.detectChanges();

  }

  saveDetails() {
    // this.detailsTab = false;
    // this.modulesTab = true;
    // this.enrolTab = false;
    // this.engageTab = false;
    // this.rewardsTab = false;

    // this.courseDetails = false;
    // this.courseModules = true;

    // this.detailsComp.saveCourse();
  }

  saveModules() {
    // this.detailsTab = false;
    // this.modulesTab = false;
    // this.enrolTab = true;
    // this.engageTab = false;
    // this.rewardsTab = false;

    // this.courseModules = false;
    // this.courseEnrol = true;
  }

  saveEnrol() {
    // this.detailsTab = false;
    // this.modulesTab = false;
    // this.enrolTab = false;
    // this.engageTab = true;
    // this.rewardsTab = false;

    // this.courseEnrol = false;
    // this.courseEngage = true;
      this.header = {
        title: this.content?this.content.fullname:'Add Course',
        btnsSearch: true,
        btnName3: 'Bulk Enrol',
        btnName3show: true,
        btnBackshow: true,
        showBreadcrumb: true,

      };
       this.header.breadCrumbList=this.courseDataService.data.catId?
        [
         {
           'name': 'Learning',
           'navigationPath': '/pages/learning',
         },
         {
           'name': 'Course Category',
           'navigationPath': '/pages/plan/courses/category',
         },
         {
          'name': this.courseDataService.data.categoryName,
           'navigationPath': '/pages/plan/courses/content',
         }
       ]:[
           {
             'name': 'Learning',
             'navigationPath': '/pages/learning',
           },
           {
             'name': 'Online Course',
             'navigationPath': '/pages/plan/courses/content',
           },
       ]
       if(this.courseDataService.breadcrumbArray){
        this.header.title = this.courseDataService.breadtitle
        this.header.breadCrumbList = this.courseDataService.breadcrumbArray
      }
    this.btnFlag = 2;
    this.showEnrolpage = true;
    this.showBulkpage = false;
  }

  bulkEnrol() {
      this.header = {
        title: this.content?this.content.fullname:'Add Course',
        btnsSearch: true,
        btnName2: 'Enrol',
        btnName2show: true,
        btnBackshow: true,
        showBreadcrumb: true,
      };
        this.header.breadCrumbList=this.courseDataService.data.catId?
        [
         {
           'name': 'Learning',
           'navigationPath': '/pages/learning',
         },
         {
           'name': 'Course Category',
           'navigationPath': '/pages/plan/courses/category',
         },
         {
          'name': this.courseDataService.data.categoryName,
           'navigationPath': '/pages/plan/courses/content',
         }
       ]:[
           {
             'name': 'Learning',
             'navigationPath': '/pages/learning',
           },
           {
             'name': 'Online Course',
             'navigationPath': '/pages/plan/courses/content',
           },
       ]
       if(this.courseDataService.breadcrumbArray){
        this.header.title = this.courseDataService.breadtitle
        this.header.breadCrumbList = this.courseDataService.breadcrumbArray
      }

    this.btnFlag = 2;
    this.showEnrolpage = false;
    this.showBulkpage = true;
  }

  backToEnrol() {
    this.showEnrolpage = false;
    this.showBulkpage = false;
    this.file = [];
    this.preview = false;
    this.fileName = 'Click here to upload an excel file to enrol ' + this.currentBrandData.employee.toLowerCase() + ' to this course'
    this.enableUpload = false;
    this.allEnrolUser(this.content);

  }

  saveEngage() {
    // this.detailsTab = false;
    // this.modulesTab = false;
    // this.enrolTab = false;
    // this.engageTab = false;
    // this.rewardsTab = true;

    // this.courseEngage = false;
    // this.courseRewards = true;
    this.addEngagePopup = true;
  }

  closeEngageModal() {
    this.addEngagePopup = false;
  }

  saveRewards() {
    // this.rewardsTab = false;

  }

  back() {
    // this.router.navigate(['/pages/plan/courses/content']);
    if (this.btnFlag == 2) {
        this.header = {
          title: this.content?this.content.fullname:'Add Course',
          btnsSearch: true,
          btnName2: 'Enrol',
          btnName3: 'Bulk Enrol',
          btnName2show: true,
          btnName3show: true,
          btnBackshow: true,
          showBreadcrumb: true,
          breadCrumbList:this.courseDataService.data.catId?
          [
      {
        'name': 'Learning',
        'navigationPath': '/pages/learning',
      },  {
        'name': 'Course Category',
        'navigationPath': '/pages/plan/courses/category',
      },
      {
        'name': this.courseDataService.data.categoryName,
        'navigationPath': '/pages/plan/courses/content',
      },
    ]:[
        {
          'name': 'Learning',
          'navigationPath': '/pages/learning',
        },
        {
          'name': 'Online Course',
          'navigationPath': '/pages/plan/courses/content',
        },
    ]
        };
      this.btnFlag = 1;
      this.showEnrolpage = false;
      this.showBulkpage = false;
      this.file = [];
      this.preview = false;
      this.fileName = 'Click here to upload an excel file to enrol ' + this.currentBrandData.employee.toLowerCase() + ' to this course'
      this.enableUpload = false;
      this.allEnrolUser(this.content);
    }
    else {
    // this.courseDataService.breadtitle = data.categoryName
    if(this.courseDataService.breadcrumbArray){
      var length = this.courseDataService.breadcrumbArray.length-1
      this.courseDataService.breadtitle = this.courseDataService.breadcrumbArray[length].name
      // this.addassetservice.title = this.title
      this.courseDataService.breadcrumbArray = this.courseDataService.breadcrumbArray.slice(0,length)
      // this.title = this.addassetservice.breadCrumbArray[]
      this.courseDataService.previousBreadCrumb = this.courseDataService.previousBreadCrumb.slice(0,length+1)

    }
    window.history.back()

  }
  }

  ngOnInit() {
    this.spinner.show();
    if(this.content){}
    this.passService.detailsDone = false;
    console.log("gcfgcfgc", localStorage.getItem('blended-category'))
    this.addEngagePopup = false;
    this.makeCourseDataReady();
    this.currentBrandData = this.brandService.getCurrentBrandData();
    this.fileName = 'Click here to upload an excel file to enrol ' + this.currentBrandData.employee.toLowerCase() + ' to this course'
    if(this.currentBrandData){
      this.labels[0].labelname = this.currentBrandData.ecn;
    }
  }
  makeCourseDataReady() {
    this.content;
    if (this.courseDataService.data) {
      this.content = this.courseDataService.data.data;
      console.log('content', this.content);
      if (this.courseIdEn) {

        this.allEnrolUser(this.content);

      }
    } else {
      this.showEnrolpage = !this.showEnrolpage;
    }
  }
  //--------------- enrolled User (all)----------------------//
  allEnrolUser(content) {
    this.spinner.show();
    var data = {
      areaId: 2,
      instanceId: content.courseId,
      tId: this.tenantId,
      mode: 0,
    }
    console.log(data);



    const allErnroledusers: string = webApi.domain + webApi.url.getAllEnrolUser;
    this.commonFunctionService.httpPostRequest(allErnroledusers, data)
      //this.courseDataService.getallenroluser(data)
      .then(enrolData => {
        this.spinner.hide();
        this.enrol = true;
        this.enrolldata = enrolData['data'];
        this.rows = enrolData['data'];
        this.rows = [...this.rows];
        for (let i = 0; i < this.rows.length; i++) {
          // this.rows[i].Date = new Date(this.rows[i].enrolDate);
          // this.rows[i].enrolDate = this.formdate(this.rows[i].Date);
          if (this.rows[i].visible == 1) {
            this.rows[i].btntext = 'fa fa-eye';
          } else {
            this.rows[i].btntext = 'fa fa-eye-slash';
          }
        }
        this.rowsTest = this.rows;
        console.log('this.rows', this.rowsTest);
        if(this.rowsTest.length==0){
          this.nodata=true;
        }else{
          this.nodata=false;
        }

        if (this.enrolldata.visible = 1) {
          this.enableCourse = false;
        } else {
          this.enableCourse = true;
        }
        // this.backToEnrol();
        this.preview = false;
        //this.showEnrolpage =true;
        this.cdf.detectChanges();


      })

  }

  disableCourseVisibility(currentIndex, row, status) {
    var visibilityData = {
      employeeId: row.employeeId,
      visible: status,
      courseId: this.courseIdEn,
      tId: this.tenantId,
      aId: 2,
    }
    const _urlDisableEnrol: string = webApi.domain + webApi.url.disableEnrol;
    this.commonFunctionService.httpPostRequest(_urlDisableEnrol, visibilityData)
      //this.courseDataService.disableEnrol(visibilityData)
      .then(result => {
        console.log(result);
        this.loader = false;
        this.resultdata = result;
        if (this.resultdata.type == false) {
          // var courseUpdate: Toast = {
          //   type: 'error',
          //   title: 'Course',
          //   body: 'Unable to update visibility of User.',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // // this.closeEnableDisableCourseModal();
          // this.toasterService.pop(courseUpdate);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        } else {
          // var courseUpdate: Toast = {
          //   type: 'success',
          //   title: 'Course',
          //   body: this.resultdata.data,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // row.visible = !row.visible;
          console.log('after', row.visible)
          this.allEnrolUser(this.courseDataService.data.data);
          // this.toasterService.pop(courseUpdate);

          this.toastr.success(this.resultdata.data, 'Success', {
            closeButton: false
          });
        }
      },
        resUserError => {
          this.loader = false;
          this.errorMsg = resUserError;
          this.closeEnableDisableCourseModal();
        });
  }

  visibilityTableRow(row) {

    // if (row && row['wfId'] && row['wfId'] !== '' && row['wfId'] != null) {
    //   const msg = row['msg'] ? row['msg'] : 'Unable to hide user as it is enrolled in workflow.';
    //   this.toastr.warning(msg, 'Warning');
    //   return null;
    // }
    let value;
    let status;

    if (row.visible == 1) {
      row.btntext = 'fa fa-eye-slash';
      value = 'fa fa-eye-slash';
      row.visible = 0
      status = 0;
    } else {
      status = 1;
      value = 'fa fa-eye';
      row.visible = 1;
      row.btntext = 'fa fa-eye';
    }

    for (let i = 0; i < this.rowsTest.length; i++) {
      if (this.rowsTest[i].employeeId == row.employeeId) {
        this.rowsTest[i].btntext = row.btntext;
        this.rowsTest[i].visible = row.visible
      }
    }
    var visibilityData = {
      employeeId: row.employeeId,
      visible: status,
      courseId: this.courseIdEn,
      tId: this.tenantId,
      aId: 2,
    }
    const _urlDisableEnrol: string = webApi.domain + webApi.url.disableEnrol;
    this.commonFunctionService.httpPostRequest(_urlDisableEnrol, visibilityData)
      .then(result => {
        console.log(result);
        this.loader = false;
        this.resultdata = result;
        if (this.resultdata.type == false) {

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        } else {
          console.log('after', row.visible)


          this.allEnrolUser(this.courseDataService.data.data);

          this.toastr.success(this.resultdata.data, 'Success', {
            closeButton: false
          });
        }
      },
        resUserError => {
          this.loader = false;
          this.errorMsg = resUserError;
          this.closeEnableDisableCourseModal();
        });
    console.log('row', row);
  }

  enableDisableCourseAction(actionType) {
    if (actionType == true) {
      if (this.enabledata == 1) {
        this.enabledata = 0;
        // var courseData = this.content[this.courseDisableIndex];
        this.enableDisableCourse(this.enableuser);
      } else {
        this.enabledata = 1;
        // var courseData = this.content[this.courseDisableIndex];
        this.enableDisableCourse(this.enableuser);
      }
    } else {
      this.closeEnableDisableCourseModal();
    }
  }
  enableDisableCourse(enableuser) {
    console.log(enableuser);
    var visibilityData = {
      userId: enableuser.ecn,
      visible: enableuser.visible
    }
    const _urlDisableUser: string = webApi.domain + webApi.url.disableuser;
    this.commonFunctionService.httpPostRequest(_urlDisableUser, visibilityData)
      //this.courseDataService.disableUser(visibilityData)
      .then(result => {
        console.log(result);
        this.loader = false;
        this.resultdata = result;
        if (this.resultdata.type == false) {
          // var courseUpdate: Toast = {
          //   type: 'error',
          //   title: 'Course',
          //   body: 'Unable to update visibility of User.',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          this.closeEnableDisableCourseModal();
          // this.toasterService.pop(courseUpdate);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        } else {
          // var courseUpdate: Toast = {
          //   type: 'success',
          //   title: 'Course',
          //   body: this.resultdata.data,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          this.closeEnableDisableCourseModal();
          // this.toasterService.pop(courseUpdate);

          this.toastr.success(this.resultdata.data, 'Success', {
            closeButton: false
          });
        }
      },
        resUserError => {
          this.loader = false;
          this.errorMsg = resUserError;
          this.closeEnableDisableCourseModal();
        });
  }
  closeEnableDisableCourseModal() {
    this.enableDisableCourseModal = false;
  }

  onChange(event) {
    if (event == 'Date') {
      this.showdate = true;
    }
  }

  onChangeSelect(event) {
    if (event == 'nDays') {
      this.showdays = true;
    }
  }
  formdate(date) {

    if (date) {
      // const months = ['JAN', 'FEB', 'MAR','APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
      // var day = date.getDate();
      // var monthIndex = months[date.getMonth()];
      // var year = date.getFullYear();

      // return day + '_' + monthIndex + '_' +year;
      var formatted = this.datePipe.transform(date, 'dd-MM-yyyy');
      return formatted;
    }
  }
  /*BULk ENROL*/
  fileName: any;
  fileReaded: any;
  enableUpload: any;
  file: any = [];
  datasetPreview: any;
  preview: boolean = false;
  bulkUploadData: any = [];
  uploadedData: any = [];
  showInvalidExcel: boolean = false;
  readUrl(event: any) {
    this.showInvalidExcel = false;
    var validExts = new Array('.xlsx', '.xls');
    var fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
      // alert('Invalid file selected, valid files are of ' +
      //          validExts.toString() + ' types.');
      // var toast: Toast = {
      //   type: 'error',
      //   title: 'Invalid file selected!',
      //   body: 'Valid files are of ' + validExts.toString() + ' types.',
      //   showCloseButton: true,
      //   // tapToDismiss: false,
      //   timeout: 2000
      //   // onHideCallback: () => {
      //   //     this.router.navigate(['/pages/plan/users']);
      //   // }
      // };
      // this.toasterService.pop(toast);

      this.toastr.warning('Valid file types are ' + validExts.toString(), 'Warning', {
        closeButton: false
      });
      // setTimeout(data => {
      //   this.router.navigate(['/pages/users']);
      // },1000);
      // return false;
      this.cancel();
    }
    else {
      // return true;
      if (event.target.files && event.target.files[0]) {
        this.fileName = event.target.files[0].name;
        //read file from input
        this.fileReaded = event.target.files[0];

        if (this.fileReaded != '' || this.fileReaded != null || this.fileReaded != undefined) {
          this.enableUpload = true;
          this.showInvalidExcel = false;
        }

        this.file = event.target.files[0];

        this.bulkUploadData = event.target.files[0];
        console.log('this.bulkUploadData', this.bulkUploadData);

        this.xlsxToJsonService.processFileToJson({}, this.file).subscribe(data => {
          var resultSheets = Object.getOwnPropertyNames(data['sheets']);
          console.log('File Property Names ', resultSheets);
          let sheetName = resultSheets[0];
          const result = data['sheets'][sheetName];
          console.log('dataSheet', data);
          console.log('this.result', result);

          if (result.length > 0) {
            this.uploadedData = result;
          }
          console.log('this.uploadedData', this.uploadedData);
        });

        var reader = new FileReader();
        reader.onload = (event: ProgressEvent) => {
          let fileUrl = (<FileReader>event.target).result;
          // event.setAttribute('data-title', this.fileName);
          // console.log(this.fileUrl);
        }
        reader.readAsDataURL(event.target.files[0]);
      }
    }

    // if(event.target.files[0].name.split('.')[event.target.files[0].name.split('.').length-1] === 'xlsx'){
    //     let file = event.target.files[0];
    // } else if(event.target.files[0].name.split('.')[event.target.files[0].name.split('.').length-1] === 'xls'){
    //     let file = event.target.files[0];
    // }else{
    //   alert('Invalid file selected, valid files are of ' +
    //          validExts.toString() + ' types.');
    // }
  }
  invaliddata: any = [];
  savebukupload() {
    // this.loader = true;
    if (this.uploadedData.length > 0 && this.uploadedData.length <= 2000) {
      this.spinner.show();

      // var option: string = Array.prototype.map
      //   .call(this.uploadedData, function (item) {
      //     console.log("item", item);
      //     return Object.values(item).join("#");
      //   })
      //   .join("|");

      // New Function as per requirement.

      var option: string = Array.prototype.map
        .call(this.uploadedData, (item) => {
          const array = Object.keys(item);
          let string = '';
          array.forEach(element => {
            if (element === 'EnrolDate') {
              string += this.commonFunctionService.formatDateTimeForBulkUpload(item[element]);
            } else {
              string = item[element] + '#';
            }
          });
          // console.log("item", item);
          // return Object.values(item).join("#");
          return string;
        })
        .join("|");


      //  console.log('this.courseDataService.workflowId', this.courseDataService.workflowId);

      const data = {
        wfId: null,
        aId: 2,
        iId: this.courseIdEn,
        // userId: this.content.userId,
        upString: option,
      }
      console.log('data', data);
      const _urlAreaBulEnrol: string = webApi.domain + webApi.url.area_bulk_enrol;
      this.commonFunctionService.httpPostRequest(_urlAreaBulEnrol, data)
        //this.courseDataService.areaBulkEnrol(data)
        .then(rescompData => {
          // this.loader =false;
          this.spinner.hide();
          var temp: any = rescompData;
          this.addEditModRes = temp.data;
          if (temp == 'err') {
            this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
              timeOut: 0,
              closeButton: true
            });
          } else if (temp.type == false) {
            this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
              timeOut: 0,
              closeButton: true
            });
          } else {
            this.enableUpload = false;
            this.fileName = 'Click here to upload an excel file to enrol ' + this.currentBrandData.employee.toLowerCase() + ' to this course'
            if (temp['data'].length > 0) {
              if (temp['data'].length === temp['invalidCnt'].invalidCount) {
                this.toastr.warning('No valid ' + this.currentBrandData.employee.toLowerCase() + 's found to enrol.', 'Warning', {
                  closeButton: false
                });
              } else {
                this.toastr.success('Enrol successfully.', 'Success', {
                  closeButton: false
                });
                this.allEnrolUser(this.content)
              }
              this.invaliddata = temp['data'];
              this.showInvalidExcel = true;

              // this.invaliddata = temp['data'];
              // if (this.uploadedData.length == this.invaliddata.length) {
              //   this.toastr.warning('No valid employees found to enrol.', 'Warning', {
              //     closeButton: false
              //     });
              // } else {
              //   this.toastr.success('Enrol successfully.', 'Success', {
              //     closeButton: false
              //     });
              // }
            } else {
              this.invaliddata = [];
              this.showInvalidExcel = false;
            }




            // this.backToEnrol()
            this.cdf.detectChanges();
            this.preview = false;
            this.showInvalidExcel = true;

          }
          console.log('Enrol bulk Result ', this.addEditModRes);
          this.cdf.detectChanges();
        },
          resUserError => {
            //this.loader = false;
            this.errorMsg = resUserError;
          });
    } else {

      if (this.uploadedData.length > 2000) {
        this.toastr.warning('File Data cannot exceed more than 2000', 'warning', {
          closeButton: true
        });
      }
      else {
        this.toastr.warning('No Data Found', 'warning', {
          closeButton: true
        });
      }
    }

  }

  exportToExcelInvalid() {
    this.exportService.exportAsExcelFile(this.invaliddata, 'Enrolment Status')
  }

  cancel() {
    // this.fileUpload.nativeElement.files = [];
    console.log(this.fileUpload.nativeElement.files);
    this.fileUpload.nativeElement.value = '';
    console.log(this.fileUpload.nativeElement.files);
    this.fileName = 'Click here to upload an excel file to enrol ' + this.currentBrandData.employee.toLowerCase() + ' to this course'
    this.enableUpload = false;
    this.file = [];
    this.preview = false;
  }

  uploadfile() {
    this.spinner.show();
    this.content.areaId = 2;
    this.content.userId = this.uId;
    var fd = new FormData();
    fd.append('content', JSON.stringify(this.content));
    fd.append('file', this.file);

    this.courseDataService.TempManEnrolBulk(fd).then(result => {
      console.log(result);
      // this.loader =false;

      var res = result;
      try {
        if (res['data1'][0]) {
          this.resultdata = res['data1'][0];
          for (let i = 0; i < this.resultdata.length; i++) {
            this.resultdata[i].enrolDate = this.formatDate(this.resultdata[i].enrolDate);
          }

          this.datasetPreview = this.resultdata;
          this.preview = true;
          this.cdf.detectChanges();
          // this.spinner.hide();
        }
      } catch (e) {

        // var courseUpdate: Toast = {
        //   type: 'error',
        //   title: 'Enrol',
        //   body: 'Something went wrong.please try again later.',
        //   showCloseButton: true,
        //   timeout: 2000
        // };

        // // this.closeEnableDisableCourseModal();
        // this.toasterService.pop(courseUpdate);

        this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
          timeOut: 0,
          closeButton: true
        });
      }
      this.spinner.hide();

    },
      resUserError => {
        // this.loader =false;
        this.spinner.hide();
        this.errorMsg = resUserError;
        // this.closeEnableDisableCourseModal();
      });
  }

  formatDate(date) {
    var d = new Date(date)
    var formatted = this.datePipe.transform(d, 'dd-MMM-yyyy');
    return formatted;
  }

  clearPreviewData() {
    this.datasetPreview = [];
    this.cancel();

  }

  addEditModRes: any;
  fileUploadRes: any;
  savebukupload1() {
    var data = {
      userId: this.content.userId,
      coursId: this.courseIdEn,
      areaId: 2,
      tId: this.tenantId,
    }
    // this.loader = true;
    this.spinner.show();
    this.courseDataService.finalManEnrolBulk(data)
      .then(rescompData => {
        // this.loader =false;
        this.spinner.hide();
        var temp: any = rescompData;
        this.addEditModRes = temp.data[0];
        if (temp == 'err') {
          // var modUpdate: Toast = {
          //   type: 'error',
          //   title: 'Enrol',
          //   body: 'Something went wrong',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(modUpdate);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        } else if (temp.type == false) {
          // var modUpdate: Toast = {
          //   type: 'error',
          //   title: 'Enrol',
          //   body: this.addEditModRes[0].msg,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(modUpdate);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        } else {
          // var modUpdate: Toast = {
          //   type: 'success',
          //   title: 'Enrol',
          //   body: this.addEditModRes[0].msg,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(modUpdate);

          this.toastr.success(this.addEditModRes[0].msg, 'Success', {
            closeButton: false
          });
          this.fileName = 'Click here to upload an excel file to enrol ' + this.currentBrandData.employee.toLowerCase() + ' to this course'
          this.showEnrolpage = false;
          this.showBulkpage = false;
          this.preview = false;
          this.enableUpload = false;
          this.allEnrolUser(this.content);
        }
        console.log('Enrol bulk Result ', this.addEditModRes);
        this.cdf.detectChanges();
      },
        resUserError => {
          this.loader = false;
          this.errorMsg = resUserError;
        });
  }

  passEvent(value) {
    this.eventEmitterService.varib = value;
    this.eventEmitterService.onComponentClick(value);
    // this.passService.crossCommunication.emit();
  }
  setHeader(event) {
  this.header = {
    title: event,
    btnsSearch: true,
    btnName9: 'Save',
    btnName10: 'Cancel',
    btnName9show: true,
    btnName10show: true,
    btnBackshow: true,
    showBreadcrumb: true,
    breadCrumbList:this.courseDataService.data.catId?
    [
     {
       'name': 'Learning',
       'navigationPath': '/pages/learning',
     },  {
       'name': 'Course Category',
       'navigationPath': '/pages/plan/courses/category',
     },
     {
      'name': this.courseDataService.data.categoryName,
      'navigationPath': '/pages/plan/courses/content',
     },
     {
      'isSame':true,
      'name': this.content?this.content.fullname:'Add Course',
      'navigationPath': '/pages/plan/courses/addEditCourseContent',
     },
   ]:[
       {
         'name': 'Learning',
         'navigationPath': '/pages/learning',
       },
       {
         'name': 'Online Course',
         'navigationPath': '/pages/plan/courses/content',
       },
            {
              'isSame':true,
      'name':  this.content?this.content.fullname:'Add Course',
      'navigationPath': '/pages/plan/courses/addEditCourseContent',
     },
   ]
  };


  }
  onActevent(data) {
    this.isdata=data
  this.header = {
  title: data.type,
  btnsSearch: true,
  // btnName9: 'Save',
  // btnName10: 'Cancel',
  // btnName9show: true,
  // btnName10show: true,
  btnBackshow: true,
  showBreadcrumb: true,
 }
  this.header.breadCrumbList=this.courseDataService.data.catId?
  [
   {
     'name': 'Learning',
     'navigationPath': '/pages/learning',
   },
   {
     'name': 'Course Category',
     'navigationPath': '/pages/plan/courses/category',
   },
   {
    'name': this.courseDataService.data.categoryName,
    'navigationPath': '/pages/plan/courses/content',
   },
   {
    'isSame':true,
    'name':this.content?this.content.fullname:'Add Course',
    'navigationPath': '/pages/plan/courses/addEditCourseContent',
   },
   {
     'isSame':true,
      'name':data.name,
    'navigationPath': '/pages/plan/courses/addEditCourseContent',
   },
 ]:[
     {
       'name': 'Learning',
       'navigationPath': '/pages/learning',
     },
     {
       'name': 'Online Course',
       'navigationPath': '/pages/plan/courses/content',
     },
     {
     'isSame':true,
      'name': this.content?this.content.fullname:'Add Course',
      'navigationPath': '/pages/plan/courses/addEditCourseContent',
     },
     {
     'isSame':true,
      'name':data.name,
      'navigationPath': '/pages/plan/courses/addEditCourseContent',
     },
 ]
 if(this.courseDataService.breadcrumbArray){
  this.header.title = this.courseDataService.breadtitle
  this.header.breadCrumbList = this.courseDataService.breadcrumbArray
}

  }
  oncancelAct() {
    if(this.tabTitle == 'Modules'){
      this.header = {
        title: this.content?this.content.fullname:'Add Course',
        btnsSearch: true,
        btnName5: 'Add Module',
        btnName5show: true,
        btnBackshow: true,
        showBreadcrumb: true,
      };
        this.header.breadCrumbList=this.courseDataService.data.catId?
        [
         {
           'name': 'Learning',
           'navigationPath': '/pages/learning',
         },
         {
           'name': 'Course Category',
           'navigationPath': '/pages/plan/courses/category',
         },
         {
          'name': this.courseDataService.data.categoryName,
           'navigationPath': '/pages/plan/courses/content',
         }
       ]:[
           {
             'name': 'Learning',
             'navigationPath': '/pages/learning',
           },
           {
             'name': 'Online Course',
             'navigationPath': '/pages/plan/courses/content',
           },
       ]
       if(this.courseDataService.breadcrumbArray){
        this.header.title = this.courseDataService.breadtitle
        this.header.breadCrumbList = this.courseDataService.breadcrumbArray
      }
    }else{
      
    }
  }
  passData(val) {
    this.isdata = val;
    if (val === 'modules') {
        this.header = {
          title: this.content?this.content.fullname:'Add Course',
          btnsSearch: true,
          btnName9: 'Save',
          btnName10: 'Cancel',
          showBreadcrumb: true,
        };
          this.header.breadCrumbList=this.courseDataService.data.catId?
          [
           {
             'name': 'Learning',
             'navigationPath': '/pages/learning',
           },
           {
             'name': 'Course Category',
             'navigationPath': '/pages/plan/courses/category',
           },
           {
            'name': this.courseDataService.data.categoryName,
            'navigationPath': '/pages/plan/courses/content',
           }
         ]:[
             {
               'name': 'Learning',
               'navigationPath': '/pages/learning',
             },
             {
               'name': 'Online Course',
               'navigationPath': '/pages/plan/courses/content',
             },
         ]
         if(this.courseDataService.breadcrumbArray){
          this.header.title = this.courseDataService.breadtitle
          this.header.breadCrumbList = this.courseDataService.breadcrumbArray
        }

    } else if (val === 'cancel') {
        this.header = {
          title: this.content?this.content.fullname:'Add Course',
          btnsSearch: true,
          btnName5: 'Add Module',
          btnName9: 'Save',
          btnName10: 'Cancel',
          btnName5show: true,
          btnBackshow: true,
          showBreadcrumb: true,
        };
          this.header.breadCrumbList=this.courseDataService.data.catId ?
          [
           {
             'name': 'Learning',
             'navigationPath': '/pages/learning',
           },
           {
             'name': 'Course Category',
             'navigationPath': '/pages/plan/courses/category',
           },
           {
            'name': this.courseDataService.data.categoryName,
            'navigationPath': '/pages/plan/courses/content',
           }
         ]:[
             {
               'name': 'Learning',
               'navigationPath': '/pages/learning',
             },
             {
               'name': 'Online Course',
               'navigationPath': '/pages/plan/courses/content',
             },
         ]
         if(this.courseDataService.breadcrumbArray){
          this.header.title = this.courseDataService.breadtitle
          this.header.breadCrumbList = this.courseDataService.breadcrumbArray
        }
    }

    // this.eventsSubject.next();
    setTimeout(() => {
      this.isdata = '';
    }, 2000);
  }

  backToAsset(data){
    this.contentService.parentCatId = data.categoryId
    this.contentService.countLevel = data['index'] - 1
    this.courseDataService.breadtitle = data.categoryName
    this.courseDataService.breadcrumbArray = this.courseDataService.breadcrumbArray.slice(0,data.index)
    // this.title = this.addassetservice.breadCrumbArray[]
    this.courseDataService.previousBreadCrumb = this.courseDataService.previousBreadCrumb.slice(0,data.index+1)

    // this.router.navigate(['../'], { relativeTo: this.routes });


  }
}

