import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

import { NgxEchartsModule } from 'ngx-echarts';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ThemeModule } from '../../../../@theme/theme.module';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { MyDatePickerModule } from 'mydatepicker';
import { FilterPipeModule  } from 'ngx-filter-pipe';
import { TabsModule } from "ngx-tabs";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { TranslateModule } from '@ngx-translate/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NbTabsetModule } from '@nebular/theme';
import { TagInputModule } from 'ngx-chips';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { AddEditCourseContent } from './addEditCourseContent';
import { AddEditCourseContentService } from './addEditCourseContent.service'
import { JoditAngularModule } from 'jodit-angular';
import { rewardsComponent } from './rewards/rewards.component';
import { rewardsService } from './rewards/rewards.service';
import { engageComponent } from './engage/engage.component';
import { engageService } from './engage/engage.service';
// import { AddEditActivityComponent } from '../../add-edit-activity/add-edit-activity.component';

// import { DragChipsComponentComponent } from '../../../../component/drag-chips-component/drag-chips-component.component';
import { enrolmentComponent } from './enrolment/enrolment.component';
import { enrolService } from './enrolment/enrolment.service';
import { ComponentModule } from '../../../../component/component.module';
import { detailsComponent } from './details/details.component';
import { detailsService } from './details/details.service';

import { modulesComponent } from './modules/modules.component';
import { modulesService } from './modules/modules.service';

import { GamificationComponent } from './gamification/gamification.component';
import { GamificationService } from './gamification/gamification.service';

import { QuillModule } from 'ngx-quill'
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'

import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import {MatSelectModule} from '@angular/material/select';

@NgModule({
  imports: [
    ThemeModule,
    NgxEchartsModule,
    NgxMyDatePickerModule.forRoot(),
    TabsModule,
    ReactiveFormsModule,
    AngularMultiSelectModule,
    FormsModule,
    TranslateModule.forRoot(),
    NgxDatatableModule,
    Ng2SmartTableModule,
    NbTabsetModule,
    FilterPipeModule,
    MyDatePickerModule,
    QuillModule,
    TagInputModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    BsDatepickerModule.forRoot(),
    DatepickerModule.forRoot(),
    TooltipModule.forRoot(),
    TimepickerModule.forRoot(),
    MatSelectModule,
    JoditAngularModule,
    ComponentModule,
  ],
  declarations: [
    AddEditCourseContent,
    enrolmentComponent,
    detailsComponent,
    modulesComponent,
    rewardsComponent,
    engageComponent,
    GamificationComponent,
    // AddEditActivityComponent,
    // DragChipsComponentComponent,

  ],
  providers: [
    //slikgridDemoService,
    AddEditCourseContentService,
    enrolService,
    detailsService,
    modulesService,
    rewardsService,
    engageService,
    GamificationService

  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA,
  ]
})
export class AddEditCourseContentModule { }
