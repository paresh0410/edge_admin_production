import {
  Host,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ViewEncapsulation,
  Directive,
  forwardRef,
  Attribute,
  OnChanges,
  SimpleChanges,
  Input,
  ViewChild,
  ViewContainerRef,
  OnInit,
} from "@angular/core";
// import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AddEditCourseContent } from "../addEditCourseContent";
import { AddEditCourseContentService } from "../addEditCourseContent.service";
import {
  FormGroup,
  FormArray,
  FormBuilder,
  Validators,
  FormControl,
} from "@angular/forms";
import { NgbCalendar, NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { HttpClient } from "@angular/common/http";
import { enrolService } from "./enrolment.service";
import { BsDatepickerConfig } from "ngx-bootstrap/datepicker";
import { FocusKeyManager } from "@angular/cdk/a11y";
import { FilesProp } from "@syncfusion/ej2-inputs";
import { dataBound } from "@syncfusion/ej2-grids";
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from "@angular/router";
import { DatePipe } from "@angular/common";
import { ToastrService } from "ngx-toastr";

import { Subscription } from "rxjs/Rx";

// import * as _moment from "moment";
// import {
//   DateTimeAdapter,
//   OWL_DATE_TIME_FORMATS,
//   OWL_DATE_TIME_LOCALE,
// } from "ng-pick-datetime";
// import { MomentDateTimeAdapter } from "ng-pick-datetime-moment";
// const moment = (_moment as any).default ? (_moment as any).default : _moment;
import * as _ from "lodash";

import { EnrolmentConfig } from '../../../../../models/enrolment.model';

import { webApi } from '../../../../../service/webApi';
import { BrandDetailsService } from '../../../../../service/brand-details.service';
import { CommonFunctionsService } from '../../../../../service/common-functions.service';
import { constant } from "lodash";
import { feature } from '../../../../../../environments/feature-environment';
// export const MY_CUSTOM_FORMATS = {
//   fullPickerInput: "DD-MM-YYYY HH:mm:ss",
//   parseInput: "DD-MM-YYYY HH:mm:ss",
//   datePickerInput: "DD-MM-YYYY",
//   timePickerInput: "LT",
//   monthYearLabel: "MMM YYYY",
//   dateA11yLabel: "LL",
//   monthYearA11yLabel: "MMMM YYYY",
// };
@Component({
  selector: "course-enrolment",
  templateUrl: "./enrolment.html",
  styleUrls: ["./enrolment.scss"],
  providers: [DatePipe],
  // providers: [
  //   DatePipe,
  //   {
  //     provide: DateTimeAdapter,
  //     useClass: MomentDateTimeAdapter,
  //     deps: [OWL_DATE_TIME_LOCALE],
  //   },
  //   { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS },
  // ],
  encapsulation: ViewEncapsulation.None,
})
export class enrolmentComponent {
  private subscription: Subscription;

  colorTheme = "theme-dark-blue";

  // bsConfig: Partial<BsDatepickerConfig>;

  // @ViewChild('myTable') table: any;
  // @ViewChild(DatatableComponent) tableData: DatatableComponent;

  // @ViewChild('manualTable') manualTable: any;
  // @ViewChild(DatatableComponent) tableDataManual: DatatableComponent;

  // @ViewChild('rulesTable') rulesTable: any;
  // @ViewChild(DatatableComponent) tableDataRules: DatatableComponent;

  // @ViewChild('regTable') regTable: any;
  // @ViewChild(DatatableComponent) tableDataReg: DatatableComponent;

  // @ViewChild('selfTable') selfTable: any;
  // @ViewChild(DatatableComponent) tableDataSelf: DatatableComponent;

  usersList: any = [];
  // selectedUsers = [];
  settingsUsersSelDrop = {};
  settingsprofileSelDrop = {};
  content: any = [];
  dropdownListUsers: any;
  selectedItemsUsers: any;
  dropdownSettingsUsers: any;
  demoData: any = [];

  selected: any = [];
  rows: any = [];
  temp = [];
  minDate = new Date();
  nextDay: any;
  selectedUsers: any = [];
  rowsUsers: any = [];
  tempUsers = [];

  selectedManual: any = [];
  rowsManual: any = [];
  tempManual = [];
  rowsEnrolRule: any = [];
  selectedRules: any = [];
  rowsRules: any = [];
  tempRules = [];

  selectedReg: any = [];
  rowsReg: any = [];
  tempReg = [];
  enrolldata: any = [];
  enrollruledata: any = [];
  enrollselfdata: any = [];
  enrollregdata: any = [];
  selectedSelf: any = [];
  rowsSelf: any = [];
  tempSelf = [];
  labels: any = [
    { labelname: "ECN", bindingProperty: "ecn", componentType: "text" },
    {
      labelname: "FULL NAME",
      bindingProperty: "fullname",
      componentType: "text",
    },
    { labelname: "EMAIL", bindingProperty: "emailId", componentType: "text" },
    { labelname: "MOBILE", bindingProperty: "phoneNo", componentType: "text" },
    { labelname: "Date Of Enrolment", bindingProperty: "enrolDate", componentType: "text" },
    { labelname: "MODE", bindingProperty: "enrolmode", componentType: "text" },
    {
      labelname: "ACTION",
      bindingProperty: "btntext",
      componentType: "button",
    },
  ];
  labelsRule: any = [
    {
      labelname: "RULE NAME",
      bindingProperty: "rulename",
      componentType: "text",
    },
    {
      labelname: "DESCRIPTION",
      bindingProperty: "description",
      componentType: "text",
    },
    {
      labelname: "APPLICABLE USERS",
      bindingProperty: "noOfEmp",
      componentType: "text",
    },
    {
      labelname: "ACTION",
      bindingProperty: "btntext",
      componentType: "button",
    },
    { labelname: "EDIT", bindingProperty: "tenantId", componentType: "icon" },
  ];
  labelsReg: any = [
    {
      labelname: "ENROL DATE",
      bindingProperty: "enrolDate",
      componentType: "text",
    },
    {
      labelname: "ACTION",
      bindingProperty: "btntext",
      componentType: "button",
    },
  ];
  labelsSelf: any = [
    { labelname: "ECN", bindingProperty: "ecn", componentType: "text" },
    {
      labelname: "FULL NAME",
      bindingProperty: "fullname",
      componentType: "text",
    },
    { labelname: "EMAIL", bindingProperty: "emailId", componentType: "text" },
    { labelname: "MOBILE", bindingProperty: "phoneNo", componentType: "text" },
    { labelname: "Date Of Enrolment", bindingProperty: "enrolDate", componentType: "text" },
    { labelname: "MODE", bindingProperty: "enrolmode", componentType: "text" },
    {
      labelname: "ACTION",
      bindingProperty: "btntext",
      componentType: "button",
    },
  ];
  labels4: any = [
    { labelname: "ECN", bindingProperty: "ecn", componentType: "text" },
    {
      labelname: "FULL NAME",
      bindingProperty: "fullname",
      componentType: "text",
    },
    { labelname: "EMAIL", bindingProperty: "emailId", componentType: "text" },
    { labelname: "MOBILE", bindingProperty: "phoneNo", componentType: "text" },
    { labelname: "Date Of Enrolment", bindingProperty: "enrolDate", componentType: "text" },
    { labelname: "MODE", bindingProperty: "enrolmode", componentType: "text" },
    // {
    //   labelname: "ACTION",
    //   bindingProperty: "btntext",
    //   componentType: "button",
    // },
  ];
  // columnsManual = [
  //   {
  //     prop: "selected",
  //     name: "",
  //     sortable: false,
  //     canAutoResize: false,
  //     draggable: false,
  //     resizable: false,
  //     headerCheckboxable: true,
  //     checkboxable: true,
  //     width: 30,
  //   },
  //   { prop: "ecn", name: "EMP CODE" },
  //   { prop: "fullname", name: "FULLNAME" },
  //   { prop: "gender", name: "GENDER" },
  //   { prop: "doj", name: "DOJ" },
  //   { prop: "department", name: "DEPARTMENT" },
  //   { prop: "mode", name: "MODE" },
  // ];

  // columnsRules = [
  //   {
  //     prop: "selected",
  //     name: "",
  //     sortable: false,
  //     canAutoResize: false,
  //     draggable: false,
  //     resizable: false,
  //     headerCheckboxable: true,
  //     checkboxable: true,
  //     width: 30,
  //   },
  //   { prop: "usersCount", name: "APPLICABLE USERS" },
  //   { prop: "name", name: "RULE NAME" },
  //   { prop: "description", name: "DESCRIPTION" },
  //   // { prop: 'dimension', name: 'DIMENSION' },
  //   // { prop: 'field', name: 'FIELD' },
  //   // { prop: 'value', name: 'VALUES' }
  // ];

  // columnsReg = [
  //   {
  //     prop: "selected",
  //     name: "",
  //     sortable: false,
  //     canAutoResize: false,
  //     draggable: false,
  //     resizable: false,
  //     headerCheckboxable: true,
  //     checkboxable: true,
  //     width: 30,
  //   },
  //   { prop: "enrolDate", name: "ENROL DATE" },
  //   { prop: "dueDays", name: "DUE DATE (in Days)" },
  //   { prop: "reminder", name: "REMINDER (in Days)" },
  // ];

  // columnsSelf = [
  //   {
  //     prop: "selected",
  //     name: "",
  //     sortable: false,
  //     canAutoResize: false,
  //     draggable: false,
  //     resizable: false,
  //     headerCheckboxable: true,
  //     checkboxable: true,
  //     width: 30,
  //   },
  //   { prop: "ecn", name: "EMP CODE" },
  //   { prop: "fullname", name: "FULLNAME" },
  //   { prop: "gender", name: "GENDER" },
  //   { prop: "doj", name: "DOJ" },
  //   { prop: "department", name: "DEPARTMENT" },
  //   { prop: "status", name: "STATUS" },
  // ];

  showEnrolpage: boolean = false;
  enableCourse: boolean = false;

  reviewCheck: any = {
    value1: false,
    value2: false,
    value3: false,
  };

  enrolment: any = {
    manual: true,
    rule: false,
    regulatory: false,
    self: false,
  };
  resultdata: any = [];
  showAddRuleModal: boolean = false;
  showAddRegulatoryModal: boolean = false;
  showAddSelfModal: boolean = false;
  enableDisableCourseModal: boolean = false;
  featureConfig = feature;
  // showAddRuleModal:boolean = false;
  //   showAddRegulatoryModal:boolean = false;
  //   showAddSelfModal:boolean = false;

  ruleType: any = [
    {
      ruleTypeId: 1,
      ruleTypeName: "Profile Fields",
    },
  ];

  ruleApplicType: any = [];
  ruledate: any = [];
  prospectivType: any = [];

  ruleSubType: any = [
    {
      ruleTypeId: 1,
      ruleSubTypeId: 1,
      ruleSubTypeName: "Username",
    },
    {
      ruleTypeId: 1,
      ruleSubTypeId: 2,
      ruleSubTypeName: "Department",
    },
    {
      ruleTypeId: 2,
      ruleSubTypeId: 1,
      ruleSubTypeName: "Doj",
    },
    {
      ruleTypeId: 2,
      ruleSubTypeId: 2,
      ruleSubTypeName: "Custom date",
    },
  ];

  prospectiveData: any = {
    id: "",
    usersCount: "",
    name: "",
    description: "",
    type: "",
    subType: "",
    value: "",
    prospname: "",
  };
  selfFieldsData: any = {
    enrolSelfId: "",
    sid: "",
    maxCount: "",
    cid: "",
    tid: "",
    userId: "",
    profiles: [],
    type:"",
  };

  searchvalue: any = {
    value: "",
    value1: "",
  };

  ruleData: any = {
    id: 0,
    usersCount: "",
    name: "",
    description: "",
    type: "",
    subType: "",
    value: "",
    prospName: "",
    enroltype: "",
    enroldate: "",
    profiles: [],
  };

  formdata: any = {
    id: "",
    shortname: "",
    name: "",
    datatype: "",
    selected: "",
  };

  strArrayType: any = [[]];
  selectedFilterOption = [];
  strArrayPar: any = [];
  datarule: any;
  menutypeid: any;
  public addRulesForm: FormGroup;
  controlList: any = [{ datatype: "" }];
  controlFlag: any = false;

  profileFields: any = [];
  errorMsg: any;
  loader: any;

  enabledata: any = [];
  enableuser: any = [];

  private ValueId: number = 0;
  strArrayTypePar: any = [];
  // selectedFilterOption = [];
  selectedRule: any = [];
  // ruleType:any
  selectedRuleType: any = {
    id: "",
  };

  profileFieldSelected: boolean = false;

  prospectivemodeul: boolean = false;
  itemList = [];
  selectedItems = [];
  settings = {};
  strArraySkilllevel: any = [];
  openfilter: any;
  menuType = [];
  datetimeType = [];
  textType = [];
  textareaType = [];
  msg: any;
  msg1: any;
  msg2: any;
  regiD: any;
  regenId: any = "";
  strArrayTypeSelfFields: any = [[]];
  selectedFilterOptionSelf = [];
  strArrayParSelfFields: any = [];

  public addSelfFieldsForm: FormGroup;
  controlListSelfFields: any = [{ datatype: "" }];
  controlFlagSelfFields: any = false;

  profileFieldsSelf: any = [];

  selfType: any = [];

  selfFeildType: any = [
    {
      selfTypeId: 1,
      selfTypeName: "Profile Fields",
    },
  ];
  // selfFieldsData:any;
  regularprofiles: any;
  formdataSelf: any = {
    id: "",
    shortname: "",
    name: "",
    datatype: "",
    selected: "",
  };

  // ruleType:any
  selectedSelfFieldsType: any = {
    id: "",
  };

  selfProfileFieldSelected: boolean = false;
  private selfFieldValueId: number = 0;
  strArrayTypeParSelfFields: any = [];
  // selectedFilterOption = [];
  selectedSelfFields: any = [];

  regData: any = {
    id: "",
    enrolDate: "",
    dueDays: "",
    reminder: "",
  };

  public regulatoryForm: FormGroup;

  strArrayTypeRegFilter: any = [[]];
  selectedFilterOptionRegFilter = [];
  strArrayParRegFilter: any = [];
  public addRegFilterForm: FormGroup;
  controlListRegFilter: any = [{ datatype: "" }];
  controlFlagRegFilter: any = false;
  profileFieldsRegFilter: any = [];
  regFilterProfileFieldSelected: boolean = true;
  private regFilterValueId: number = 0;
  strArrayTypeParRegFilter: any = [];
  // selectedFilterOption = [];
  selectedRegFilter: any = [];

  dueDaysArr: any = [
    { id: 1, name: 1 },
    { id: 2, name: 2 },
    { id: 3, name: 3 },
    { id: 4, name: 4 },
    { id: 5, name: 5 },
    { id: 6, naAme: 6 },
    { id: 7, name: 7 },
    { id: 8, name: 8 },
    { id: 9, name: 9 },
    { id: 10, name: 10 },
    { id: 11, name: 11 },
    { id: 12, name: 12 },
    { id: 13, name: 13 },
    { id: 14, name: 14 },
    { id: 15, name: 15 },
    { id: 16, name: 16 },
    { id: 17, name: 17 },
    { id: 18, name: 18 },
    { id: 19, name: 19 },
    { id: 20, name: 20 },
    { id: 21, name: 21 },
    { id: 22, name: 22 },
    { id: 23, name: 23 },
    { id: 24, name: 24 },
    { id: 25, name: 25 },
    { id: 26, name: 26 },
    { id: 27, name: 27 },
    { id: 28, name: 28 },
    { id: 29, name: 29 },
    { id: 30, name: 30 },
  ];
  showSearch: boolean = false;
  labelsPrice: any = [
    { labelname: 'NO', bindingProperty: 'number', componentType: 'text' },
    { labelname: 'Name', bindingProperty: 'name', componentType: 'text' },
    { labelname: 'Amount', bindingProperty: 'amount', componentType: 'price' },
      { labelname: 'Currency', bindingProperty: 'currencyName', componentType: 'text' },
    { labelname: 'Valid From', bindingProperty: 'validFrom_show', componentType: 'text' },
    { labelname: 'Valid To', bindingProperty: 'validTo_show', componentType: 'text' },
    { labelname: "EDIT", bindingProperty: "tenantId", componentType: "icon" },
  ];
  selectedfiltervalue: any = [];
  selectedsettingvalue: any = [];
  selectedrulevalue: any = [];
  regularData: any;
  remDaysArr: any = this.dueDaysArr;
  userLoginData: any;
  toppings = new FormControl();
  toppingList: string[] = [
    "Extra cheese",
    "Mushroom",
    "Onion",
    "Pepperoni",
    "Sausage",
    "Tomato",
  ];
  userids = "";
  userdata: any;
  selftypefield: any = [];
  enrolldatarule: any = [];
  areaId = 2;
  searchText: any;
  searchFlag: number;

  // Price Based
  pricingList;
  tempDisplayPriceList;
  currencyTypeDropDown = [];
  discountListDropdownList = [];
  showPriceSidebar = false;
  addEditPriceForm = {
    id: null,
    name: '',
    currency: '',
    amount: '',
    validFrom: null,
    currentDate: null,
    validTo: null,
    discount: [],
    isFree: 1,
  };

  // New Changes
  config: EnrolmentConfig = {
    manulEnrolmentData: {
      show: true,
      data: [...this.rowsManual],
      labels: this.labels,
      userList: [...this.tempUsers],
      tabTitle: "Manual",
      identifer: "manual",
      helpContent: [],
      selectedUsers: this.selectedUsers,
    },
    ruleBasedEnrolmentData: {
      show: true,
      data: [...this.rowsRules],
      labels: this.labelsRule,
      tabTitle: "Rule Based",
      identifer: "ruleBased",
      ruleData: this.ruleData,
      ruleApplicType: [...this.ruleApplicType],
      prospectivType: [...this.prospectivType],
      profileFields: [...this.profileFields],
      controlList: this.controlList,
      profileFieldSelected: this.profileFieldSelected,
      helpContent: [],
      rowsEnrolRule : this.rowsEnrolRule,
      showEnroleduserPopup : false,
      enrolUserPopupTableLabel: this.labels4,
      showAddRuleModal: this.showAddRuleModal,
    },
    selfEnrolmentData: {
      show: true,
      data: [...this.rowsSelf],
      labels: this.labelsSelf,
      tabTitle: "Self",
      identifer: "self",
      helpContent: [],
      showAddSelfModal: this.showAddSelfModal,
      selfFeildType: this.selfFeildType,
      selfFieldsData: this.selfFieldsData,
      selfType: this.selfType,
      isFetchingSettings: false,
      profileFieldsSelf: this.profileFieldsSelf,
      controlList: this.controlList,
    },
    regulatoryEnrolmentData: {
      show: true,
      data: [...this.rowsReg],
      labels: [...this.labelsReg],
      tabTitle: "Regulatory",
      identifer: "regulatory",
      helpContent: [],
      controlList: this.controlList,
      profileFieldsRegFilter: this.profileFieldsRegFilter,
      showAddRegulatoryFilterModal: false,
      regFilterProfileFieldSelected: this.regFilterProfileFieldSelected,
      regularData: this.regularData,
    },
    priceBasedEnrolmentData: {
      show: this.featureConfig.pricingTab,
      data: [],
      labels: this.labelsPrice,
      tabTitle: "Pricing",
      identifer: "pricing",
      helpContent: [],
      courseId: null,
      currencyTypeDropDown: [],
      discountListDropdownList: [],
      addEditPriceForm: this.addEditPriceForm,
      showSidebar: false,
    },
  };
  currentBrandData:any;
  ruleId: any;

  constructor(
    private calendar: NgbCalendar,
    private _fb: FormBuilder,
    private http: HttpClient,
    @Host() private parent_Comp: AddEditCourseContent,
    private addEditCourseService: AddEditCourseContentService,
    // private toasterService: ToasterService,
    private toastr: ToastrService,
    private enrolService: enrolService,
    public cdf: ChangeDetectorRef,
    private spinner: NgxSpinnerService,
    private router: Router,
    private datePipe: DatePipe,
    private http1: HttpClient,
    private commonFunctionsService: CommonFunctionsService,
    public brandService: BrandDetailsService,
  ) {
    this.getHelpContent();
    this.currentBrandData = this.brandService.getCurrentBrandData();
    this.settings = {
      text: "Select ",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      classes: "myclass custom-class",
      primaryKey: "id",
      enableSearchFilter: true,
      labelKey: "name",
      lazyLoading: true,
      badgeShowLimit: 2,
      maxHeight: 150,
    };
    var day = new Date();
    this.nextDay = new Date(day);
    this.nextDay.setDate(day.getDate() + 1);

    if (localStorage.getItem("LoginResData")) {
      this.userLoginData = JSON.parse(localStorage.getItem("LoginResData"));
      this.userdata = this.userLoginData.data.data;
      console.log("login data", this.userdata);
    }
    if(this.currentBrandData){
      this.labels[0].labelname = this.currentBrandData.ecn;
      // this.labelsRule[0].labelname = this.currentBrandData.ecn;
      this.labelsSelf[0].labelname = this.currentBrandData.ecn;
      this.labels4[0].labelname = this.currentBrandData.ecn;
    }
    // this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });

    this.addRulesForm = new FormGroup({
      FilterOpt: new FormControl(),
      Value1: new FormControl(),
      Value2: new FormControl(),
    });

    this.addSelfFieldsForm = new FormGroup({
      FilterOpt: new FormControl(),
      Value1: new FormControl(),
      Value2: new FormControl(),
    });

    this.addRegFilterForm = new FormGroup({
      FilterOpt: new FormControl(),
      Value1: new FormControl(),
      Value2: new FormControl(),
    });

    this.regulatoryForm = new FormGroup({
      enrolDate: new FormControl(),
      dueDays: new FormControl(),
      remDays: new FormControl(),
    });

    this.settingsUsersSelDrop = {
      text: "Select Users",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      classes: "myclass custom-class",
      primaryKey: "ecn",
      labelKey: "fullname",
      noDataLabel: "Search Users...",
      enableSearchFilter: true,
      searchBy: ["ecn", "fullname"],
      lazyLoading: true,
      badgeShowLimit: 2,
      maxHeight: 200,
    };

    this.fetchManual((data) => {
      // cache our list
      this.tempManual = [...data];
      this.rowsManual = data;
    });

    this.fetchUnEnrolledUsers((data) => {
      // cache our list
      this.tempUsers = [...data];
      // this.rowsUsers = data;
    });

    this.fetchRules((data) => {
      // cache our list
      this.tempRules = [...data];
      this.rowsRules = data;
      console.log(
        "this.rowsRules==============================",
        this.rowsRules
      );
      // this.rowsRules.push(this.tempRuleData);
    });

    this.fetchReg((data) => {
      // cache our list
      this.tempReg = [...data];
      this.rowsReg = data;
      // this.rowsRules.push(this.tempRuleData);
    });

    this.fetchSelf((data) => {
      // cache our list
      this.tempSelf = [...data];
      this.rowsSelf = data;
      // this.rowsRules.push(this.tempRuleData);
    });

    this.getUserProfileFields();
  }

  ngOnInit() {
    this.spinner.show();
    this.ruledropdownmenu();
    this.getUserProfileFields();
    this.addRulesForm = this._fb.group({
      rules: this._fb.array([
        // this.initRules(),
      ]),
    });

    this.addSelfFieldsForm = this._fb.group({
      fields: this._fb.array([
        // this.initRules(),
      ]),
    });

    this.addRegFilterForm = this._fb.group({
      filters: this._fb.array([
        // this.initRegFilter(),
      ]),
    });

    this.regulatoryForm = this._fb.group({
      fields: this._fb.array([
        this.initRegForm(),
        this.initRegForm(),
        this.initRegForm(),
      ]),
    });

    this.makeCourseDataReady();
    // this.onCheckBoxClick(true, 'manual');
  }

  selfdatafield() {
    let data = {
      lovtype: 15,
      tId: this.userdata.tenantId,
    };
    this.enrolService.getselfdropdownlist(data).then((res) => {
      console.log(res);
      this.selftypefield = res;
      this.selfType = this.selftypefield.data[0];
      console.log(this.selfType);
      this.passDataToChild();
    });
  }
  ruledropdownmenu() {
    this.enrolService.dropdown().then((res) => {
      console.log(res["data"]);
      this.ruleApplicType = res["data"][0];
      this.prospectivType = res["data"][1];
      this.ruledate = res["data"][2];
      this.config.ruleBasedEnrolmentData.ruleApplicType = [
        ...this.ruleApplicType,
      ];
      this.config.ruleBasedEnrolmentData.prospectivType = [
        ...this.prospectivType,
      ];
      // this.config.ruleBasedEnrolmentData.ruleData = _.cloneDeep(this.ruledate);
      console.log("dropdown", this.ruleApplicType);
      console.log("dropdown", this.prospectivType);
      // console.log(" this.config.ruleBasedEnrolmentData.ruleData",  this.config.ruleBasedEnrolmentData.ruleData);
    });
  }

  makeCourseDataReady() {
    this.content;
    if (this.addEditCourseService.data) {
      this.content = this.addEditCourseService.data.data;
      if (this.content == undefined) {
        this.content = {
          courseId: this.addEditCourseService.data.courseId,
        };
        console.log("content", this.content);
      }
      console.log("content", this.content);
      this.allEnrolUser(this.content);
      this.selfdatafield();
      this.config.priceBasedEnrolmentData.courseId =  this.content.courseId;
      console.log(" this.config.priceBasedEnrolmentData",  this.config.priceBasedEnrolmentData);
    } else {
      this.showEnrolpage = !this.showEnrolpage;
    }
    // this.cdf.detectChanges();
  }

  /*------------------Rule list -----------*/
  allruleList(content) {
    this.spinner.show();
    var data = {
      areaId: this.areaId,
      instanceId: content.courseId,
      tId: this.userdata.tenantId,
    };
    console.log(data);
    this.enrolService.getallrule(data).then(
      (enrolData) => {
        console.log(enrolData);
        this.spinner.hide();
        if (enrolData["type"] === true) {
          this.enrollruledata = enrolData["data"];
          this.rowsRules = enrolData["data"];
          this.rowsRules = [...this.rowsRules];
          console.log("RULE", this.rowsRules);
          for (var i = 0; i < this.rowsRules.length; i++) {
            if (this.rowsRules[i].visible == 1) {
              this.rowsRules[i].btntext = "fa fa-eye";
            } else {
              this.rowsRules[i].btntext = "fa fa-eye-slash";
            }
          }
        }
        this.passDataToChild();
        this.cdf.detectChanges();
      },
      (err) => {
        console.log(err);
      }
    );
  }

  /*------------------------Disable rule------------*/
  visibilityTableRow1(row) {
    let value;
    let status;

    if (row.visible == 1) {
      row.btntext = "fa fa-eye-slash";
      value = "fa fa-eye-slash";
      row.visible = 0;
      status = 0;
    } else {
      status = 1;
      value = "fa fa-eye";
      row.visible = 1;
      row.btntext = "fa fa-eye";
    }

    for (let i = 0; i < this.rowsRules.length; i++) {
      if (this.rowsRules[i].employeeId == row.employeeId) {
        this.rowsRules[i].btntext = row.btntext;
        this.rowsRules[i].visible = row.visible;
      }
    }
    var visibilityData = {
      enrolRuleId: row.enrolRuleId,
      visible: status,
    };
    console.log(visibilityData);
    this.enrolService.disableRule(visibilityData).then(
      (result) => {
        this.spinner.hide();
        console.log(result);
        this.loader = false;
        this.resultdata = result;
        if (this.resultdata.type == false) {
          this.toastr.error(
            'Please contact site administrator and <div (click)="giveFeedback()"> click here <div> to leave your feedback',
            "Error",
            {
              timeOut: 0,
              closeButton: true,
            }
          );
        } else {
          console.log("after", row.visible);
          this.allruleList(this.addEditCourseService.data.data);
          this.toastr.success(this.resultdata.data, "Success", {
            closeButton: false,
          });
        }
      },
      (resUserError) => {
        this.loader = false;
        this.errorMsg = resUserError;
        this.spinner.show();
      }
    );

    console.log("row", row);
  }
  // disableRuleVisibility(currentIndex, row, status) {
  //   this.spinner.show();
  //   var visibilityData = {
  //     enrolRuleId: row.enrolRuleId,
  //     visible: status
  //   };
  //   console.log(visibilityData);
  //   this.enrolService.disableRule(visibilityData).then(result => {
  //     this.spinner.hide();
  //     console.log(result);
  //     this.loader = false;
  //     this.resultdata = result;
  //     if (this.resultdata.type == false) {
  //       // var courseUpdate: Toast = {
  //       //   type: 'error',
  //       //   title: 'Course',
  //       //   body: 'Unable to update visibility of Rule.',
  //       //   showCloseButton: true,
  //       //   timeout: 2000
  //       // };
  //       // // this.closeEnableDisableCourseModal();
  //       // this.toasterService.pop(courseUpdate);

  //       this.toastr.error('Please contact site administrator and <div (click)="giveFeedback()"> click here <div> to leave your feedback', 'Error', {
  //         timeOut: 0,
  //         closeButton: true
  //       });

  //     } else {
  //       // var courseUpdate: Toast = {
  //       //   type: 'success',
  //       //   title: 'Course',
  //       //   body: this.resultdata.data,
  //       //   showCloseButton: true,
  //       //   timeout: 2000
  //       // };
  //       // row.visible = !row.visible;
  //       console.log('after', row.visible)
  //       this.allruleList(this.addEditCourseService.data.data);
  //       // this.toasterService.pop(courseUpdate);

  //       this.toastr.success(this.resultdata.data, 'Success', {
  //         closeButton: false
  //       });

  //     }
  //     // this.cdf.detectChanges();
  //   },
  //     resUserError => {
  //       this.loader = false;
  //       this.errorMsg = resUserError;
  //       this.spinner.show();
  //       // this.closeEnableDisableCourseModal();
  //     });
  // }

  /*----------------Rule add ---------*/
  saveRule(event, f) {
    // this.loader =true;
    // console.log('Events',event);
    if (f.valid) {
      let rules: any = this.addRulesForm.value.rules;
      this.ruleData.rules = rules;
      console.log("Rule data", this.ruleData);

      this.makeRuleDataready(this.ruleData);
    } else {
      console.log("Please Fill all fields");
      Object.keys(f.controls).forEach((key) => {
        f.controls[key].markAsDirty();
      });
    }
  }

  /*---------------Rule Edit----------*/
  editrule(row) {
    console.log(row);
    this.openRuleModal(row, 1);
    // this.openRulemodel23(row);
  }

  /*--------------- enrolled User (manual)----------------------*/
  allEnrolUser(content) {
    this.spinner.show();
    var data = {
      areaId: this.areaId,
      instanceId: content.courseId,
      tId: this.userdata.tenantId,
      mode: 1,
    };
    console.log(data);
    this.addEditCourseService.getallenroluser(data).then((enrolData) => {
      this.enrolldata = enrolData["data"];
      this.rowsManual = enrolData["data"];
      this.rowsManual = [...this.rowsManual];

      for (let i = 0; i < this.rowsManual.length; i++) {
        // this.rowsManual[i].Date = new Date(this.rowsManual[i].enrolDate);
        // this.rowsManual[i].enrolDate = this.formdate(this.rowsManual[i].Date);
        if (this.rowsManual[i].visible == 1) {
          this.rowsManual[i].btntext = "fa fa-eye";
        } else {
          this.rowsManual[i].btntext = "fa fa-eye-slash";
        }

      }

      this.spinner.hide();
      console.log("EnrolledUSer", this.rowsManual);
      if ((this.enrolldata.visible = 1)) {
        this.enableCourse = false;
      } else {
        this.enableCourse = true;
      }
      this.passDataToChild();
      // this.cdf.detectChanges();
    });
  }
  /*--------------- Unenrolled User (manual)----------------------*/

  allUNEnrolUser(evt) {
    const data = {
      courseId: this.content.courseId,
      tId: this.userdata.tenantId,
      searchStr: evt,
      aId: 2,
    };
    // console.log(data);
    if (evt.length > 3) {
      this.enrolService.getallunenroluser(data).then((enrolData) => {
        this.spinner.show()
        console.log(enrolData);
        this.enrolldata = enrolData["data"];
        // this.usersList = enrolData.data;
        this.tempUsers = enrolData["data"];
        this.tempUsers = [...this.tempUsers];
        this.passDataToChild();
        this.spinner.hide()
        // if(this.enrolldata.length == 0){
        // 	this.showEnrolpage = !this.showEnrolpage;
        // }
        console.log("EnrolledUSer ", this.tempUsers);
      });
      // this.cdf.detectChanges();
    }
  }

  fetchAllUnEnrolUsersAsync(params: any, cb) {
    this.spinner.show()
    const data = {
      courseId: this.content.courseId,
      tId: this.userdata.tenantId,
      searchStr: params,
      aId: 2,
    };
    this.enrolService.getallunenroluser(data).then(
      (res) => {
      this.spinner.hide()
        cb(res);
      },
      (err) => {
        console.log(err);
      }
    );
  }

  /*-----------------disable manual user-------------*/
  visibilityTableRow(row) {
    // if (row && row["wfId"] && row["wfId"] !== "" && row["wfId"] != null) {
    //   const msg = row["msg"]
    //     ? row["msg"]
    //     : "Unable to hide user as it is enrolled in workflow.";
    //   this.toastr.warning(msg, "Warning");
    //   return null;
    // }
    let value;
    let status;

    if (row.visible == 1) {
      row.btntext = "fa fa-eye-slash";
      value = "fa fa-eye-slash";
      row.visible = 0;
      status = 0;
    } else {
      status = 1;
      value = "fa fa-eye";
      row.visible = 1;
      row.btntext = "fa fa-eye";
    }

    for (let i = 0; i < this.rowsManual.length; i++) {
      if (this.rowsManual[i].employeeId == row.employeeId) {
        this.rowsManual[i].btntext = row.btntext;
        this.rowsManual[i].visible = row.visible;
      }
    }
    var visibilityData = {
      employeeId: row.employeeId,
      visible: status,
      courseId: this.content.courseId,
      tId: this.userdata.tenantId,
      aId: 2,
    };
    this.addEditCourseService.disableEnrol(visibilityData).then(
      (result) => {
        console.log(result);
        this.spinner.hide();
        this.loader = false;
        this.resultdata = result;
        if (this.resultdata.type == false) {
          this.toastr.error(
            'Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.',
            "Error",
            {
              timeOut: 0,
              closeButton: true,
            }
          );
        } else {
          console.log("after", row.visible);
          this.allEnrolUser(this.addEditCourseService.data.data);
          this.toastr.success(this.resultdata.data, "Success", {
            closeButton: false,
          });
        }
      },
      (resUserError) => {
        this.loader = false;
        this.errorMsg = resUserError;
        this.spinner.show();
        // this.closeEnableDisableCourseModal();
      }
    );

    console.log("row", row);
  }

  // disableCourseVisibility(currentIndex, row, status) {
  //   this.spinner.show();
  //   var visibilityData = {
  //     employeeId: row.employeeId,
  //     visible: status,
  //     courseId: this.content.courseId,
  //     tId: this.userdata.tenantId,
  //     aId: 2,
  //   }
  //   this.addEditCourseService.disableEnrol(visibilityData).then(result => {
  //     console.log(result);
  //     this.spinner.hide();
  //     this.loader = false;
  //     this.resultdata = result;
  //     if (this.resultdata.type == false) {
  //       // var courseUpdate: Toast = {
  //       //   type: 'error',
  //       //   title: 'Course',
  //       //   body: 'Unable to update visibility of User.',
  //       //   showCloseButton: true,
  //       //   timeout: 2000
  //       // };
  //       // // this.closeEnableDisableCourseModal();
  //       // this.toasterService.pop(courseUpdate);

  //       this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
  //         timeOut: 0,
  //         closeButton: true
  //       });
  //     } else {
  //       // var courseUpdate: Toast = {
  //       //   type: 'success',
  //       //   title: 'Course',
  //       //   body: this.resultdata.data,
  //       //   showCloseButton: true,
  //       //   timeout: 2000
  //       // };
  //       // row.visible = !row.visible;
  //       console.log('after', row.visible)
  //       this.allEnrolUser(this.addEditCourseService.data.data);
  //       // this.toasterService.pop(courseUpdate);

  //       this.toastr.success(this.resultdata.data, 'Success', {
  //         closeButton: false
  //       });
  //     }
  //   },
  //     resUserError => {
  //       this.loader = false;
  //       this.errorMsg = resUserError;
  //       this.spinner.show();
  //       // this.closeEnableDisableCourseModal();
  //     });
  // }

  enableDisableCourseAction(actionType) {
    // if(actionType == true){
    //   if(this.enabledata == 1){
    //     this.enabledata = 0;
    //     // var courseData = this.content[this.courseDisableIndex];
    //     this.enableDisableCourse(this.enableuser);
    //   }else{
    //     this.enabledata = 1;
    //     // var courseData = this.content[this.courseDisableIndex];
    //     this.enableDisableCourse(this.enableuser);
    //   }
    // }else{
    //   this.closeEnableDisableCourseModal();
    // }
  }

  /*-----------------add enroluser-----------------*/

  manEnrolUser() {
    console.log("Selected user ", this.selectedUsers);

    if (this.selectedUsers.length > 0) {
      this.userids = "";
      this.spinner.show();
      for (let i = 0; i < this.selectedUsers.length; i++) {
        var user = this.selectedUsers[i];
        // this.userids = id;
        if (this.userids != "") {
          this.userids += "|";
        }
        if (String(user.id) != "" && String(user.id) != "null") {
          this.userids += user.id;
        }
        console.log("abc", this.userids);
      }
      var data = {
        empIds: this.userids,
        tId: this.userdata.tenantId,
        courseId: this.content.courseId,
        modeId: 1,
        visible: 1,
        areaId: this.areaId,
        uId: this.userdata.id,
      };
      console.log("Selected data", data);
      this.enrolService.addenroluser(data).then((Response) => {
        console.log(Response);
        this.spinner.hide();
        if (Response["type"] == true) {
          // var enrolUsersToast: Toast = {
          //   type: 'success',
          //   title: 'Course',
          //   body: 'User(s) enrolled .',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(enrolUsersToast);

          this.toastr.success("User(s) enrolled .", "Success", {
            closeButton: false,
          });

          this.allEnrolUser(this.content);
          // this.selectedUsers.visible = 1;
          // for(let i=0; i< this.selectedUsers.length; i++){
          //   this.rowsManual.push(this.selectedUsers[i]);
          // }
        } else {
          // var enrolUsersToast: Toast = {
          //   type: 'error',
          //   title: 'Course',
          //   body: 'Please contact site administrator and <div (click)="giveFeedback()"> click here <div> to leave your feedback',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(enrolUsersToast);

          this.toastr.error(
            'Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.',
            "Error",
            {
              timeOut: 0,
              closeButton: true,
            }
          );
        }
      });
    } else {
      // var courseUpdate: Toast = {
      //   type: 'error',
      //   title: 'Course',
      //   body: 'Please select a User.',
      //   showCloseButton: true,
      //   timeout: 2000
      // };
      // // this.closeEnableDisableCourseModal();
      // this.toasterService.pop(courseUpdate);

      this.toastr.warning("Please select a user.", "Warning", {
        closeButton: false,
      });
    }

    for (let j = 0; j < this.tempManual.length; j++) {
      var user = this.tempManual[j];
      for (let i = 0; i < this.selectedUsers.length; i++) {
        if (user.ecn == this.selectedUsers[i].ecn) {
          this.tempManual.splice(j, 1);
        }
      }
    }
    this.passDataToChild();
    this.selectedUsers = [];
    this.tempUsers = [];
    this.tempUsers = [...this.tempUsers];
    this.passDataToChild();
    this.usersList = [];
    this.rowsManual = [...this.rowsManual];
    this.tempManual = this.rowsManual;
    console.log("Updated enrolled users ", this.rowsManual);
  }

  initRegFilter() {
    return this._fb.group({
      FilterOpt: [""],
      Value1: [""],
      Value2: [""],
    });
  }

  prepareRegFilter() {
    this.selectedFilterOptionRegFilter = [];
    this.controlListRegFilter = [{ datatype: "" }];
    this.clearRegFilter();
    this.disableSelectRegFilter();
    this.addRegFilterForm = this._fb.group({
      filters: this._fb.array([this.initRegFilter()]),
    });
  }

  addRegulatoryFilter() {
    this.regFilterProfileFieldSelected = false;
    let defualtRegsObj = {
      enrolProfileId: 0,
      field: "",
      fieldValues: "",
    };
    this.regularData.profiles.push(defualtRegsObj);
    this.controlFlag = true;
    this.controlList.push(0);
    this.passDataToChild();
    this.strArrayType.push([]);
    this.regFilterProfileFieldSelected = true;
    this.passDataToChild();
    this.cdf.detectChanges();
  }

  removeRegFilter(currentIndex) {
    this.regFilterProfileFieldSelected = false;
    this.regularData.profiles.splice(currentIndex, 1);
    this.selectedFilterOption.splice(currentIndex, 1);
    this.disableSelectRegFilter();
    this.controlFlag = true;
    this.controlList.splice(currentIndex, 1);
    this.strArrayType.splice(currentIndex, 1);
    console.log(this.regularData.profiles);
    this.regFilterProfileFieldSelected = true;
    this.passDataToChild();
    this.cdf.detectChanges();
  }

  clearRegFilter() {
    const arr = <FormArray>this.addRegFilterForm.controls.filters;
    arr.controls = [];
    this.addRegFilterForm.reset({
      FilterOpt: [""],
      Value1: [""],
      Value2: [""],
    });
  }

  regFilterTypeSelected() {}

  callTypeRegFilter(id: any, index: any, status) {
    console.log(id + index);
    if (status == 2) {
      this.regularData.profiles[index].fieldValues = "";
      this.regularData.profiles[index].fieldValuesArr = [];
    }
    this.passDataToChild();
    if (this.strArrayType[index]) {
      this.strArrayType[index] = [];
    }

    if (id.srcElement == undefined || id.target == undefined) {
      this.ValueId = id;
    } else {
      this.ValueId = parseInt((id.srcElement || id.target).value);
    }
    for (let i = 0; i < this.profileFieldsRegFilter.length; i++) {
      if (this.profileFieldsRegFilter[i].id == this.ValueId) {
        this.controlList[index] = this.profileFieldsRegFilter[i];
        // this.strArrayType[index].push(this.profileFields[i]);
        //  if(this.selectedFilterOption.length > 0){
        this.selectedFilterOption[index] = this.profileFieldsRegFilter[
          i
        ].shortname;
        this.datarule = this.profileFieldsRegFilter[i].datatype;
        this.menutypeid = this.profileFieldsRegFilter[i].menuTypeId;
        console.log(this.menutypeid);
        /*---------profile menu list-----------*/
        if (
          this.menutypeid != "" &&
          this.menutypeid != null &&
          this.menutypeid != undefined
        ) {
          var data = {
            lovtype: this.menutypeid,
            tId: this.userdata.tenantId,
          };
          this.enrolService.getprofileFieldDropdown(data).then((res) => {
            this.strArraySkilllevel = res["data"][0];
            this.strArraySkilllevel = [...this.strArraySkilllevel];
            this.itemList = this.strArraySkilllevel;
            // this.itemList = this.controlList[i].subtype;
            console.log(this.itemList);
            this.profileFieldsRegFilter[i].subtype = this.strArraySkilllevel;
            this.passDataToChild();
            // this.cdf.detectChanges();
          });
        }
        console.log(this.profileFieldsRegFilter);
        //  this.datatype.push(this.ruleData.profiles[i]);
        // }else{
        // this.selectedFilterOption.push(this.profileFields[i].shortname);
        // }
      }
    }
    this.passDataToChild();
    // this.disableSelectedRuleFieldType();
    this.disableSelectRegFilter();
  }

  disableSelectRegFilter() {
    this.profileFieldsRegFilter.forEach((data, key) => {
      if (this.selectedFilterOption.indexOf(data.shortname) >= 0) {
        this.profileFieldsRegFilter[key].selected = "true";
      } else {
        this.profileFieldsRegFilter[key].selected = "false";
      }
    });
    this.passDataToChild();
    console.log("Selected Disabled", this.strArrayPar);
    this.cdf.detectChanges();
  }

  showAddRegulatoryFilterModal: boolean = false;

  openRegulatoryFilterModal() {
    // this.config.regulatoryEnrolmentData.showAddRegulatoryFilterModal = _.clone(this.showAddRegulatoryFilterModal)

    this.filterdata();
    this.showAddRegulatoryFilterModal = true;
    this.passDataToChild();
  }
  filterdata() {
    var data = {
      courseId: this.content.courseId,
      TId: this.userdata.tenantId,
      eModeId: 3,
      eSetId: this.regiD,
    };
    if (data.eSetId == "" || data.eSetId == null || data.eSetId == undefined) {
      data.eSetId = 0;
    }
    console.log(data);

    this.enrolService.getfilter(data).then((res) => {
      this.regularprofiles = res["data"][0];
      console.log(this.regularprofiles);
      this.regulatorydata(this.regularprofiles);
      this.cdf.detectChanges();
    });
  }
  regulatorydata(data) {
    console.log(data);
    var allstring = "";
    if (data == "" || data == undefined || data == null) {
      this.regularData = {
        modeId: "",
        aId: "",
        cid: "",
        profiles: [],
        tid: "",
        createrId: "",
      };
      this.addRegulatoryFilter();
      this.msg1 = "Filter added ";
    } else {
      this.regularData = {
        modeId: "",
        aId: "",
        cid: "",
        profiles: this.regularprofiles,
        tid: "",
        createrId: "",
      };
      this.msg1 = "Filter updated ";
      if (this.regularData.profiles.length > 0) {
        for (let i = 0; i < this.regularData.profiles.length; i++) {
          let rule = this.regularData.profiles[i];
          this.callTypeRegFilter(rule.field, i, 1);
          console.log(this.datarule);
          if (this.datarule == "datetime") {
            var array = this.regularData.profiles[i].fieldValues.split("$");
            this.regularData.profiles[i].fieldValues = [
              new Date(array[0]),
              new Date(array[1]),
            ];
            // this.regularData.profiles[i].fieldValues = new Date(this.regularData.profiles[i].fieldValues);
          }
          if (this.datarule == "menu") {
            this.databind(this.regularData.profiles[i], i);
          }
        }
        this.profileFieldSelected = true;
        this.passDataToChild();
        setTimeout(() => {
          this.cdf.detectChanges();
        }, 400);
      }

      console.log("Edit regulatory data ", this.regularData);
    }
    this.showAddRegulatoryFilterModal = true;
    this.passDataToChild();
  }
  databind(alldata, i) {
    var array = alldata.fieldValues.split(",");
    let newarray = [];
    let data = {
      lovtype: this.menutypeid,
      tId: this.userdata.tenantId,
    };
    this.enrolService.getprofileFieldDropdown(data).then((res) => {
      console.log(res);
      if (res["type"] == true) {
        let strArraySkilllevel = res["data"][0];

        if (strArraySkilllevel && strArraySkilllevel.length > 0) {
          this.make_selected_dropdown_data_ready_Reg(
            alldata,
            strArraySkilllevel,
            i
          );
        } else {
          this.enrolService.getprofileFieldDropdown(data).then((res) => {
            console.log(res);
            if (res["type"] == true) {
              let strArraySkilllevel = res["data"][0];
              this.profileFields[i].subtype = strArraySkilllevel;
              this.make_selected_dropdown_data_ready_Reg(
                alldata,
                strArraySkilllevel,
                i
              );
            }
          });
        }
      }
      this.cdf.detectChanges();
    });
    this.selectedrulevalue[i] = newarray;
  }
  closeRegulatoryFilterModal() {
    this.showAddRegulatoryFilterModal = false;
    this.passDataToChild();
    this.regularData = {
      modeId: "",
        aId: "",
        cid: "",
        profiles: [],
        tid: "",
        createrId: "",
    };
    this.selectedFilterOption = [];
    for (let i = 0; i < this.profileFieldsRegFilter.length; i++) {
      this.profileFieldsRegFilter[i].selected = false;
    }
    // this.disableSelectRegFilter();
    // this.
    // this.prepareRegFilter();
  }

  selectedDueDate: any;
  callTypeRegDueDays(id, curIndex, field) {
    // this.remDaysArr = [];
    // this.selectedDueDate = parseInt((id.srcElement || id.target).value);
    // for(let i=1; i<=this.selectedDueDate; i++){
    // 	var remDays = {
    // 		id : i,
    // 		name : i
    // 	}
    // 	this.remDaysArr.push(remDays);
    // }
    // console.log('final rem days ',this.remDaysArr);
    console.log("final rem days ", id, curIndex, field);
  }

  initRegForm() {
    return this._fb.group({
      enrolDate: [""],
      // dueDays: [''],
      // remDays: ['']
    });
  }

  addRegForm() {
    const control = <FormArray>this.regulatoryForm.controls["fields"];
    control.push(this.initRegForm());
  }

  removeRegForm(i: number) {
    const control = <FormArray>this.regulatoryForm.controls["fields"];
    control.removeAt(i);
  }

  makeRulesReady() {
    for (let i = 0; i < this.profileFields.length; i++) {
      let field = this.profileFields[i];
      if (field.datatype == "menu") {
        this.menuType.push(field);
      }
      if (field.datatype == "datetime") {
        this.datetimeType.push(field);
      }
      if (field.datatype == "text") {
        this.textType.push(field);
      }
      if (field.datatype == "textarea") {
        this.textareaType.push(field);
      }
    }
  }

  onItemSelect(item: any) {
    console.log(item);
    console.log(this.selectedUsers);
  }
  OnItemDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedUsers);
  }
  onSelectAll(items: any) {
    console.log(items);
  }
  onDeSelectAll(items: any) {
    console.log(items);
  }

  /*---------------- list of regulatory----------*/
  allregulatorylist(content) {
    this.spinner.show();
    var data = {
      courseId: content.courseId,
      tId: this.userdata.tenantId,
    };
    console.log(data);
    this.enrolService.getallregulatory(data).then((res) => {
      console.log(res);
      this.spinner.hide();
      this.enrollregdata = res["data"];
      this.rowsReg = res["data"];

      for (var i = 0; i < this.rowsReg.length; i++) {
        // formdate
        // this.rowsReg[i].enrolDate = this.formatDateReady(this.rowsReg[i].enrolDate);
        // this.rowsReg[i].enrolDate = this.formdate(this.rowsReg[i].enrolDate);
        this.regiD = this.rowsReg[0].enrolRegId;
        if (this.rowsReg[i].visible == 1) {
          this.rowsReg[i].btntext = "fa fa-eye";
        } else {
          this.rowsReg[i].btntext = "fa fa-eye-slash";
        }
        // this.regenId = this.rowsReg[0].enrolRegDtId;
      }
      this.rowsReg = [...this.rowsReg];
      console.log(this.rowsReg);
      this.passDataToChild();
      this.cdf.detectChanges();
    });
    this.rowsReg;
  }

  /*-------------- Disable Regulatory------------*/

  visibilityTableRow2(row) {
    let value;
    let status;

    if (row.visible == 1) {
      row.btntext = "fa fa-eye-slash";
      value = "fa fa-eye-slash";
      row.visible = 0;
      status = 0;
    } else {
      status = 1;
      value = "fa fa-eye";
      row.visible = 1;
      row.btntext = "fa fa-eye";
    }

    for (let i = 0; i < this.rowsReg.length; i++) {
      if (this.rowsReg[i].employeeId == row.employeeId) {
        this.rowsReg[i].btntext = row.btntext;
        this.rowsReg[i].visible = row.visible;
      }
    }
    // this.passDataToChild();
    var visibilityData = {
      enrolRegDtId: row.enrolRegDtId,
      visible: status,
    };
    console.log(visibilityData);
    this.enrolService.disableregulatory(visibilityData).then(
      (result) => {
        console.log(result);
        this.spinner.hide();
        this.loader = false;
        this.resultdata = result;
        if (this.resultdata.type == false) {
          this.toastr.error(
            'Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.',
            "Error",
            {
              timeOut: 0,
              closeButton: true,
            }
          );
        } else {
          console.log("after", row.visible);
          this.allregulatorylist(this.addEditCourseService.data.data);

          this.toastr.success(this.resultdata.data, "Success", {
            closeButton: false,
          });
        }
      },
      (resUserError) => {
        this.loader = false;
        this.errorMsg = resUserError;
        this.spinner.hide();
        // this.closeEnableDisableCourseModal();
      }
    );

    console.log("row", row);
  }
  // disableregulatoryVisibility(currentIndex, row, status) {
  //   this.spinner.show();
  //   var visibilityData = {
  //     enrolRegDtId: row.enrolRegDtId,
  //     visible: status
  //   }
  //   console.log(visibilityData);
  //   this.enrolService.disableregulatory(visibilityData).then(result => {
  //     console.log(result);
  //     this.spinner.hide();
  //     this.loader = false;
  //     this.resultdata = result;
  //     if (this.resultdata.type == false) {
  //       // var courseUpdate: Toast = {
  //       //   type: 'error',
  //       //   title: 'Course',
  //       //   body: 'Unable to update visibility of Rule.',
  //       //   showCloseButton: true,
  //       //   timeout: 2000
  //       // };
  //       // // this.closeEnableDisableCourseModal();
  //       // this.toasterService.pop(courseUpdate);

  //       this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
  //         timeOut: 0,
  //         closeButton: true
  //       });

  //     } else {
  //       // var courseUpdate: Toast = {
  //       //   type: 'success',
  //       //   title: 'Course',
  //       //   body: this.resultdata.data,
  //       //   showCloseButton: true,
  //       //   timeout: 2000
  //       // };
  //       // row.visible = !row.visible;
  //       console.log('after', row.visible)
  //       this.allregulatorylist(this.addEditCourseService.data.data);
  //       // this.toasterService.pop(courseUpdate);

  //       this.toastr.success(this.resultdata.data, 'Success', {
  //         closeButton: false
  //       });
  //     }

  //   },
  //     resUserError => {
  //       this.loader = false;
  //       this.errorMsg = resUserError;
  //       this.spinner.hide();
  //       // this.closeEnableDisableCourseModal();
  //     });
  //   // this.cdf.detectChanges();
  // }
  /* --------------Add regulatory Date------------ */
  saveReg(data) {
    console.log(data);
    if (data.enrolDate != "") {
      this.makeRegDataReady(data);
    } else {
      this.toastr.warning("Please select a Date", "Warning", {
        closeButton: false,
      });
    }
  }
  clearesearch() {
    if (this.searchText.length >= 3) {
      this.searchvalue = {};
      this.tempUsers = [];
      this.passDataToChild();
      this.searchText = "";
      this.allEnrolUser(this.content);
    } else {
      this.searchvalue = {};
    }
  }
  clearself() {
    // if (this.searchText.length >= 3) {
    //   this.searchvalue = {};

    // } else {
    //   this.searchvalue = {};
    // }
    this.selfenrolledUser(this.content);
  }
  clearRuleEnrol() {
    // if (this.searchText.length >= 3) {
    //   this.searchvalue = {};
    //   this.allruleList(this.content);
    // } else {
    //   this.searchvalue = {};
    // }
    this.allruleList(this.content);
  }
  onSearch(evt: any) {
    console.log(evt.target.value);
    // this.allUNEnrolUser(evt.target.value);
    const val = evt.target.value;
    this.searchText = val;
    this.fetchAllUnEnrolUsersAsync(val, (enrolData) => {
      if (enrolData.type === true) {
        this.enrolldata = enrolData["data"];
        this.tempUsers = enrolData["data"];
        this.tempUsers = [...this.tempUsers];

        console.log("EnrolledUSer ", this.tempUsers);
        //  if (val.length >= 3||val.length==0) {
        const temp = this.tempUsers.filter(function (d) {
          return (
            String(d.ecn).toLowerCase().indexOf(val) !== -1 ||
            d.fullname.toLowerCase().indexOf(val) !== -1 ||
            !val
          );
        });

        // update the rows
        this.usersList = temp;
        this.passDataToChild();
        // }
        // this.cdf.detectChanges();
      } else {
        console.log("Error getting EnrolledUSer ", enrolData);
        this.tempUsers = [];
        this.tempUsers = [...this.tempUsers];
        this.usersList = [];
        // this.cdf.detectChanges();
      }
    });

    // else if (val.length === 0) {
    //   this.tempUsers = [];
    //   this.tempUsers = [...this.tempUsers];
    //   this.usersList = [];
    //   // this.cdf.detectChanges();
    // }
    // else {
    //   // this.subscription.unsubscribe();
    // }

    // this.tempUsers = [];
    // this.http.get('https://restcountries.eu/rest/v2/name/'+evt.target.value+'?fulltext=true')
    //     .subscribe(res => {
    //         console.log(res);
    //         this.usersList = res;
    //     }, error => {

    //     });

    // const temp = this.tempUsers.filter(function (d) {
    //   return String(d.ecn).toLowerCase().indexOf(val) !== -1 ||
    //     d.fullname.toLowerCase().indexOf(val) !== -1
    //   // d.gender.toLowerCase() === val ||
    //   // d.department.toLowerCase().indexOf(val) !== -1 ||
    //   // String(d.doe).toLowerCase().indexOf(val) !== -1 || !val;
    // });

    // // update the rows
    // this.usersList = temp;
  }

  getUserProfileFields() {
    this.enrolService.getProfileFields().then(
      (rescompData) => {
        console.log("profile", rescompData);
        // this.loader =false;
        this.profileFields = rescompData["data"][0];
        this.profileFieldsSelf = rescompData["data"][0];
        this.profileFieldsRegFilter = rescompData["data"][0];
        for (let i = 0; i < this.profileFields.length; i++) {
          this.profileFields[i].subtype = [];
          this.profileFieldsSelf[i].subtype = [];
          this.profileFieldsRegFilter[i].subtype = [];
        }
        this.passDataToChild();
        // this.topic = rescompData.data[0];
        console.log("User profile fields", this.profileFields);
      },
      (resUserError) => {
        // this.loader =false;
        this.errorMsg = resUserError;
      }
    );
  }

  fetchUnEnrolledUsers(cb) {
    // const req = new XMLHttpRequest();
    // req.open('GET', `assets/data/unEnroledUsers.json`);
    // req.onload = () => {
    //   cb(JSON.parse(req.response));
    // };
    // req.send();
  }

  fetchRules(cb) {
    // const req = new XMLHttpRequest();
    // req.open('GET', `assets/data/rules.json`);
    // req.onload = () => {
    //   cb(JSON.parse(req.response));
    // };
    // req.send();
  }

  fetchManual(cb) {
    // const req = new XMLHttpRequest();
    // // req.open('GET', `assets/data/company.json`);
    // req.open('GET', `assets/data/enroledUsers.json`);
    // // req.open('GET', `assets/data/100k.json`);
    // req.onload = () => {
    //   cb(JSON.parse(req.response));
    // };
    // req.send();
  }

  fetchReg(cb) {
    // const req = new XMLHttpRequest();
    // // req.open('GET', `assets/data/company.json`);
    // req.open('GET', `assets/data/regulatory.json`);
    // // req.open('GET', `assets/data/100k.json`);
    // req.onload = () => {
    //   cb(JSON.parse(req.response));
    // };
    // req.send();
  }

  fetchSelf(cb) {
    // const req = new XMLHttpRequest();
    // // req.open('GET', `assets/data/company.json`);
    // req.open('GET', `assets/data/self.json`);
    // // req.open('GET', `assets/data/100k.json`);
    // req.onload = () => {
    //   cb(JSON.parse(req.response));
    // };
    // req.send();
  }

  onSelect({ selected }) {
    console.log("Select Event", selected, this.selected);

    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }

  // toggleExpandRow(row) {
  //   console.log('Toggled Expand Row!', row);
  //   this.table.rowDetail.toggleExpandRow(row);
  // }

  onDetailToggle(event) {
    console.log("Detail Toggled", event);
  }

  onActivate(event) {
    // console.log('Activate Event', event);
    if (event.type === "checkbox") {
      // Stop event propagation and let onSelect() work
      console.log("Checkbox Selected", event);
      event.event.stopPropagation();
    } else if (event.type === "click" && event.cellIndex != 0) {
      // Do somethings when you click on row cell other than checkbox
      console.log("Row Clicked", event.row); /// <--- object is in the event row variable
    }
  }

  selectTab(tabEvent) {
    console.log("tab Selected", tabEvent);
  }

  searchManEnrol(event) {
    const val = event.target.value.toLowerCase();
    // this.allEnrolUser(this.addEditCourseService.data.data);
    // // this.cdf.detectChanges();
    this.searchText = val;
    this.temp = [...this.enrolldata];
    if (val.length >= 3 || val.length == 0) {
      const temp = this.temp.filter(function (d) {
        return (
          String(d.ecn).toLowerCase().indexOf(val) !== -1 ||
          d.fullname.toLowerCase().indexOf(val) !== -1 ||
          d.emailId.toLowerCase().indexOf(val) !== -1 ||
          d.enrolmode.toLowerCase().indexOf(val) !== -1 ||
          d.enrolDate.toLowerCase().indexOf(val) !== -1 ||
          d.phoneNo.toLowerCase().indexOf(val) !== -1 ||
          String(d.enrolmode).toLowerCase().indexOf(val) !== -1 ||
          !val
        );
      });

      // update the rows
      this.rowsManual = [...temp];
      this.passDataToChild();
    }
    // Whenever the filter changes, always go back to the first page
    // this.tableDataManual.offset = 0;
  }
  searchEnrolRule(event) {
    const val = event.target.value.toLowerCase();
    // this.allEnrolUser(this.addEditCourseService.data.data);
    // // this.cdf.detectChanges();
    this.searchText = val;
    this.temp = [...this.enrolldatarule];
    if (val.length >= 3 || val.length == 0) {
      const temp = this.temp.filter(function (d) {
        return (
          String(d.ecn).toLowerCase().indexOf(val) !== -1 ||
          d.fullname.toLowerCase().indexOf(val) !== -1 ||
          d.emailId.toLowerCase().indexOf(val) !== -1 ||
          d.enrolmode.toLowerCase().indexOf(val) !== -1 ||
          d.enrolDate.toLowerCase().indexOf(val) !== -1 ||
          d.phoneNo.toLowerCase().indexOf(val) !== -1 ||
          String(d.enrolmode).toLowerCase().indexOf(val) !== -1 ||
          !val
        );
      });

      // update the rows
      this.rowsEnrolRule = [...temp];
      this.passDataToChild();
    }
    // Whenever the filter changes, always go back to the first page
    // this.tableDataManual.offset = 0;
  }
  searchRuleEnrol(event) {
    const val = event.target.value.toLowerCase();
    // this.allruleList(this.addEditCourseService.data.data);
    this.searchText = val;
    this.tempRules = [...this.enrollruledata];

    if (val.length >= 3 || val.length == 0) {
      const temp = this.tempRules.filter(function (d) {
        return (
          String(d.noOfEmp).toLowerCase().indexOf(val) !== -1 ||
          String(d.rulename).toLowerCase().indexOf(val) !== -1 ||
          String(d.description).toLowerCase().indexOf(val) !== -1 ||
          // String(d.dimension).toLowerCase().indexOf(val) !== -1 ||
          // String(d.field).toLowerCase().indexOf(val) !== -1 ||
          // String(d.value).toLowerCase().indexOf(val) !== -1 ||
          !val
        );
      });

      // update the rows
      this.rowsRules = [...temp];
      this.passDataToChild();
      // Whenever the filter changes, always go back to the first page
      // this.tableDataRules.offset = 0;
    }
  }

  searchRegEnrol(event) {
    const val = event.target.value.toLowerCase();
    // this.allregulatorylist(this.addEditCourseService.data.data);
    this.searchText = val;
    this.tempReg = [...this.enrollregdata];
    // filter our data
    if (val.length >= 3 || val.length == 0) {
      const temp = this.tempReg.filter(function (d) {
        return (
          d.enrolDate.toLowerCase().indexOf(val) !== -1 ||
          // String(d.dueDays).toLowerCase() === val ||
          // String(d.reminder).toLowerCase().indexOf(val) !== -1 ||
          !val
        );
      });

      // update the rows
      this.rowsReg = [...temp];
    }
    // Whenever the filter changes, always go back to the first page
    // this.tableDataReg.offset = 0;
  }

  searchSelfEnrol(event) {
    const val = event.target.value.toLowerCase();
    // this.selfenrolledUser(this.addEditCourseService.data.data);

    this.tempSelf = [...this.enrollselfdata];
    // filter our data
    if (val.length >= 3 || val.length == 0) {
      const temp = this.tempSelf.filter(function (d) {
        return (
          String(d.ecn).toLowerCase().indexOf(val) !== -1 ||
          d.fullname.toLowerCase().indexOf(val) !== -1 ||
          d.emailId.toLowerCase().indexOf(val) !== -1 ||
          d.enrolmode.toLowerCase().indexOf(val) !== -1 ||
          d.enrolDate.toLowerCase().indexOf(val) !== -1 ||
          d.phoneNo.toLowerCase().indexOf(val) !== -1 ||
          // d.department.toLowerCase().indexOf(val) !== -1 ||
          // d.status.toLowerCase().indexOf(val) !== -1 ||
          !val
        );
      });

      // update the rows
      this.rowsSelf = [...temp];
      this.passDataToChild();
      this.searchFlag = 3;
    }

    // Whenever the filter changes, always go back to the first page
    // this.tableDataSelf.offset = 0;
  }

  enrolUser() {
    this.showEnrolpage = !this.showEnrolpage;
  }

  onCheckBoxClick(event, ruletype) {
    console.log("Ruletype", ruletype);
    this.clearRuleType();

    if (event) {
      switch (ruletype) {
        case "manual":
          // code...
          this.enrolment.manual = true;
          this.allEnrolUser(this.content);
          break;
        case "rule":
          // code...
          this.enrolment.rule = true;
          this.allruleList(this.content);
          break;
        case "self":
          this.enrolment.self = true;
          this.selfenrolledUser(this.content);
          // code...
          break;
        case "regulatory":
          this.enrolment.regulatory = true;
          this.allregulatorylist(this.content);
          // code...
          break;
        default:
          // code...
          this.enrolment.manual = true;
          this.allEnrolUser(this.content);
          break;
      }
    }

    //  if(event == false){
    //      this.reviewCheck = {};
    //  }else{

    // //  	if(this.enrolment.manual == true){
    // //   	this.enrolment.regulatory = false;
    // // this.enrolment.rule = false;
    // // this.enrolment.self = false;
    // //   }else if(this.enrolment.regulatory == true){
    // //   	this.enrolment.manual = false;
    // // this.enrolment.rule = false;
    // // this.enrolment.self = false;
    // //   }else if(this.enrolment.rule == true){
    // //   	this.enrolment.manual = false;
    // // this.enrolment.regulatory = false;
    // // this.enrolment.self = false;
    // //   }else if(this.enrolment.self == true){
    // //   	this.enrolment.manual = false;
    // // this.enrolment.regulatory = false;
    // // this.enrolment.rule = false;
    // //   }
    //  }
    // console.log('$event',$event);
    //console.log('courseReviewCheck',courseReviewCheck);
  }
  clearRuleType() {
    this.enrolment = {
      manual: false,
      rule: false,
      regulatory: false,
      self: false,
    };
  }

  setActiveItem(index, item) {
    console.log("Selected Module", item, index);
  }

  openRule() {
    // this.parent_Comp.openRuleModal();
    this.showAddRuleModal = this.addEditCourseService.showRule;
  }

  openRegulatory() {
    // this.parent_Comp.openRegulatoryModal();
    this.showAddRegulatoryModal = this.addEditCourseService.showRegulatory;
  }
  /*-------------Open Self----------*/
  openSelf() {
    // this.parent_Comp.openSelfModal();
    this.showAddSelfModal = this.addEditCourseService.showSelf;
    this.passDataToChild();
    this.self();
  }
  self() {
    this.spinner.show();
    var data = {
      courseId: this.content.courseId,
      tId: this.userdata.tenantId,
    };
    console.log(data);
    this.enrolService.getfechsetting(data).then((res) => {
      this.spinner.hide();
      console.log(res);
    });
  }
  // closeRule(){
  // 	this.parent_Comp.closeRuleModal();
  // 	this.showAddRuleModal = this.addEditCourseService.showRule;

  // }

  onSelectManual({ selected }) {
    console.log("Select Manual Event", selected, this.selectedManual);

    this.selectedManual.splice(0, this.selectedManual.length);
    this.selectedManual.push(...selected);

    // if(this.selectedManual.length == 1){
    // 	this.enableShowRuleUsers = true;
    // }else{
    // 	this.enableShowRuleUsers = false;
    // }
  }

  onActivateManual(event) {
    // console.log('Activate Event', event);
    if (event.type === "checkbox") {
      // Stop event propagation and let onSelect() work
      console.log("Checkbox Selected", event);
      event.event.stopPropagation();
    } else if (event.type === "click" && event.cellIndex != 0) {
      // Do somethings when you click on row cell other than checkbox
      console.log("Row Clicked", event.row); /// <--- object is in the event row variable
    }
  }

  deleteManual(selectedRow) {
    console.log("Manual Current", selectedRow);
    for (let i = 0; i < this.rowsManual.length; i++) {
      var row = this.rowsManual[i];
      if (selectedRow.ecn == row.ecn) {
        this.rowsManual.splice(i, 1);
        this.rowsManual = [...this.rowsManual];
      }
    }
    this.tempManual = this.rowsManual;
    // this.tableData.offset = 0;
  }

  rules: any[];
  selectedRuleToEdit: any = {
    profiles: [],
  };
  openRuleModal(rowData, id) {
    // this.selectedrulevalue = '';

    // this.courseDataService.showRule = this.showAddRuleModal;
    if (id == 0) {
      // this.addRulesForm.reset();
      this.ruleData = {
        id: 0,
        usersCount: "",
        name: "",
        description: "",
        type: "",
        prospName: "",
        profiles: [],
        visible: "",
        value: "",
        enroltype: "",
        enroldate: "",
      };
      this.selectedrulevalue = [];
      this.msg = "Rule added ";
      this.passDataToChild();
    } else {
      this.ruleData = {
        id: rowData.enrolRuleId,
        usersCount: rowData.noOfEmp,
        name: rowData.rulename,
        description: rowData.description,
        type: rowData.ruleAppType,
        prospName: rowData.ruleAppEvent,
        profiles: rowData.profiles,
        visible: rowData.visible,
        value: new Date(rowData.ruleAppDate),
        enroltype: rowData.enroltype,
        enroldate: new Date(rowData.enroldate),
      };
      this.msg = "Rule updated ";
      if (this.ruleData.profiles.length > 0) {
        for (let i = 0; i < this.ruleData.profiles.length; i++) {
          let rule = this.ruleData.profiles[i];
          console.log(rule);
          console.log(this.profileFields);

          this.callRuleFieldType(rule.field, i, 1);
          console.log(this.datarule);
          if (this.datarule == "datetime") {
            let array = [];
            array = this.ruleData.profiles[i].fieldValues.split("$");
            this.ruleData.profiles[i].fieldValues = [
              new Date(array[0]),
              new Date(array[1]),
            ];
          }
          if (this.datarule == "menu") {
            this.databindrule(this.ruleData.profiles[i], i);
          }
          console.log(rule.fieldValues);
        }
        this.profileFieldSelected = true;
        this.passDataToChild();
        setTimeout(() => {
          this.cdf.detectChanges();
        }, 400);
        // this.onRuleFieldList(this.ruleData.profiles, 0, (result) => {
        //   this.cdf.detectChanges();
        // })
      }

      console.log("Edit rule data ", this.ruleData);
    }
    // this.config.ruleBasedEnrolmentData.ruleData = _.cloneDeep(this.ruleData);
    this.showAddRuleModal = true;
    this.passDataToChild();
  }
  async onRuleFieldList(list, i, cb) {
    if (list.length === 0 || list.length === i) {
      cb(true);
      this.cdf.detectChanges();
    } else {
      let rule = list[i];
      this.strArrayTypeSelfFields.push([]);
      const responsecalltype = await this.callRuleFieldType(rule.field, i, 1);

      if (this.controlList[i].datatype == "datetime") {
        var array = list[i].fieldValues.split("$");
        this.ruleData.profiles[i].fieldValues = [
          new Date(array[0]),
          new Date(array[1]),
        ];
      }
      // if (this.controlList[i].datatype == 'menu') {
      //   this.databindself(this.selfFieldsData.profiles[i], i, i);
      //   console.log(this.selectedsettingvalue[i]);
      // }
      this.passDataToChild();
      this.onRuleFieldList(list, i + 1, cb);
    }
  }
  databindrule(alldata, i) {
    var array = alldata.fieldValues.split(",");
    let newarray = [];
    let data = {
      lovtype: this.menutypeid,
      tId: this.userdata.tenantId,
    };
    this.enrolService.getprofileFieldDropdown(data).then((res) => {
      console.log(res);
      if (res["type"] == true) {
        let strArraySkilllevel = res["data"][0];

        if (strArraySkilllevel && strArraySkilllevel.length > 0) {
          this.make_selected_dropdown_data_ready(
            alldata,
            strArraySkilllevel,
            i
          );
          this.cdf.detectChanges();
        } else {
          this.enrolService.getprofileFieldDropdown(data).then((res) => {
            console.log(res);
            if (res["type"] == true) {
              let strArraySkilllevel = res["data"][0];
              this.profileFields[i].subtype = strArraySkilllevel;
              this.make_selected_dropdown_data_ready(
                alldata,
                strArraySkilllevel,
                i
              );

            }
            this.cdf.detectChanges();
          });
        }
      }
    });
    this.selectedrulevalue[i] = newarray;

  }

  /* --------------Add regulatory Filter------------ */

  saveRegFilter(item, f) {
    if (f.valid) {
      console.log(item);
      // console.log(this.addRegFilterForm);
      //var filter = this.addRegFilterForm.value.filters;
      this.makefilter(item);
    } else {
      console.log("Please Fill all fields");
      Object.keys(f.controls).forEach((key) => {
        f.controls[key].markAsDirty();
      });
    }
  }
  createstring(data) {
    var str = "";
    for (var i = 0; i < data.length; i++) {
      if (i == 0) {
        str = data[i].name;
        // str = data[i].id;
      } else {
        str = str + "," + data[i].name;
        // str = str + ',' + data[i].id;
      }
    }
    return str;
  }
  makefilter(data) {
    var allstring = "";
    console.log(data.profiles);
    var filter = data.profiles;
    if (filter.length > 0) {
      for (var i = 0; i < filter.length; i++) {
        console.log(this.controlList);
        if (this.controlList[i].datatype == "datetime") {
          var fromdate = this.formatDateReady(filter[i].fieldValues[0]);
          var todate = this.formatDateReady(filter[i].fieldValues[1]);
          // rule.fieldValues = this.formatDateReady(rule.fieldValues);
          filter[i].fieldValues = fromdate + "$" + todate;
          console.log(filter[i].fieldValues);
        }
        if (this.controlList[i].datatype == "menu") {
          console.log(filter[i].fieldValuesArr);
          filter[i].fieldValues = this.createstring(filter[i].fieldValuesArr);
        }
        if (i == 0) {
          allstring = filter[i].field + "|" + filter[i].fieldValues + "|1";
        } else {
          allstring +=
            "#" + filter[i].field + "|" + filter[i].fieldValues + "|1";
        }
        console.log(allstring);
      }
      if (this.regiD == "" || this.regiD == null || this.regiD == undefined) {
        this.regiD = 0;
      }
      var newRegulatoryData = {
        modeId: 3,
        aId: this.regiD,
        cid: this.content.courseId,
        allstr: allstring,
        tid: this.userdata.tenantId,
        createrId: this.userdata.id,
        // createrId: this.userLoginData.username
      };

      console.log("Final rule data", newRegulatoryData);
      this.enrolService
        .regulatory_filterforcourse(newRegulatoryData)
        .then((res) => {
          console.log(res);
          if ((res["type"] = true)) {
            // var courseUpdate: Toast = {
            //   type: 'success',
            //   title: 'Course',
            //   body: this.msg1,
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // // this.addregulatorynew();
            // this.toasterService.pop(courseUpdate);

            this.toastr.success(this.msg1, "Success", {
              closeButton: false,
            });
          } else {
            // var courseUpdate: Toast = {
            //   type: 'error',
            //   title: 'Course',
            //   body: 'Unable to add a filter',
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(courseUpdate);

            this.toastr.error(
              'Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.',
              "Error",
              {
                timeOut: 0,
                closeButton: true,
              }
            );
          }
          // this.cdf.detectChanges();
        });
    }
    this.closeRegulatoryFilterModal();
  }

  disable(field) {
    for (let i = 0; i < this.selectedFilterOption.length; i++) {
      console.log(this.selectedFilterOption[i] + "," + field.shortname);
      if (this.selectedFilterOption[i] == field.shortname) {
        console.log(this.selectedFilterOption[i]);
        this.selectedFilterOption.splice(i, 1);
      }
    }
  }
  /******************* add edit rule new start*/
  ruleFieldTypeSelected(i, item) {
    console.log(item);
  }
  // tempprofilefieldslist: any = {};
  async callRuleFieldType(id: any, index: any, status) {
    this.profileFieldSelected = false;
    this.passDataToChild();
    console.log(id + index);
    console.log(this.ruleData.profiles);
    if (status == 2) {
      this.ruleData.profiles[index].fieldValues = "";
      this.ruleData.profiles[index].fieldValuesArr = [];
    }
    if (this.strArrayType[index]) {
      this.strArrayType[index] = [];
    }

    if (id.srcElement == undefined || id.target == undefined) {
      this.ValueId = id;
    } else {
      this.ValueId = parseInt((id.srcElement || id.target).value);
    }
    for (let i = 0; i < this.profileFields.length; i++) {
      if (this.profileFields[i].id == this.ValueId) {
        this.controlList[index] = this.profileFields[i];
        // this.strArrayType[index].push(this.profileFields[i]);
        // if (this.selectedFilterOption.length > 0) {
        this.selectedFilterOption[index] = this.profileFields[i].shortname;
        this.datarule = this.profileFields[i].datatype;
        this.menutypeid = this.profileFields[i].menuTypeId;
        // this.ruleData.profiles[index].fieldValues = '';
        // this.ruleData.profiles[index].fieldValuesArr = [];
        // console.log(this.menutypeid)
        /*---------profile menu list-----------*/
        if (
          this.menutypeid != "" &&
          this.menutypeid != null &&
          this.menutypeid != undefined
        ) {
          var data = {
            lovtype: this.menutypeid,
            tId: this.userdata.tenantId,
          };

          this.enrolService.getprofileFieldDropdown(data).then((res) => {
            this.strArraySkilllevel = res["data"][0];
            this.strArraySkilllevel = [...this.strArraySkilllevel];
            this.itemList = this.strArraySkilllevel;
            // this.itemList = this.controlList[i].subtype;
            console.log(this.itemList);
            this.profileFields[i].subtype = this.strArraySkilllevel;
            this.passDataToChild();
            this.cdf.detectChanges();
          });
        }
        console.log(this.profileFields);
        this.profileFieldSelected = true;
        this.passDataToChild();
        //  this.datatype.push(this.ruleData.profiles[i]);
        // } else {
        //   this.selectedFilterOption.push(this.profileFields[i].shortname);
        //   console.log(this.selectedFilterOption);
        // }
        this.cdf.detectChanges();
      }
    }
    this.passDataToChild();
    this.disableSelectedRuleFieldType();
  }

  disableSelectedRuleFieldType() {
    this.profileFields.forEach((data, key) => {
      if (this.selectedFilterOption.indexOf(data.shortname) >= 0) {
        this.profileFields[key].selected = "true";
      } else {
        this.profileFields[key].selected = "false";
      }
    });
    console.log("Selected Disabled", this.strArrayPar);
    this.cdf.detectChanges();
    this.passDataToChild();
    this.allregulatorylist(this.content);
  }

  addRuleList() {
    if (this.ruleData.profiles.length == 0) {
      this.profileFieldSelected = true;
    }
    let defualtRulesObj = {
      enrolProfileId: 0,
      field: "",
      fieldValues: "",
    };
    this.ruleData.profiles.push(defualtRulesObj);
   this.controlFlag = true;
    this.controlList.push(0);
    this.strArrayType.push([]);
    console.log(this.ruleData.profiles);

    this.disableSelectedRuleFieldType();
    this.passDataToChild();
  }

  removeRuleList(currentIndex) {
    this.profileFieldSelected = false;
    this.ruleData.profiles.splice(currentIndex, 1);
    this.selectedFilterOption.splice(currentIndex, 1);
    this.disableSelectedRuleFieldType();
    this.controlFlag = true;
    this.controlList.splice(currentIndex, 1);
    this.strArrayType.splice(currentIndex, 1);
    this.cdf.detectChanges();
    console.log(this.ruleData.profiles);
    this.profileFieldSelected = true;
  }
  /******************* add edit rule new end*/

  setUpForm(rules: any[]) {
    return new FormGroup({
      rules: new FormArray(rules.map((rule) => this.createRule(rule))),
    });
  }

  get rulesFormArray() {
    return this.addRulesForm.get("rules") as FormArray;
  }

  createRule(rule: any) {
    return new FormGroup({
      FilterOpt: new FormControl(rule.FilterOpt || ""),
      Value1: new FormControl(rule.Value1 || ""),
      Value2: new FormControl(rule.Value2 || ""),
    });
  }

  closeRuleModal() {
    this.showAddRuleModal = false;
    // this.courseDataService.showRule = this.showAddRuleModal;
    this.ruleData = {
      id: 0,
    usersCount: "",
    name: "",
    description: "",
    type: "",
    subType: "",
    value: "",
    prospName: "",
    enroltype: "",
    enroldate: "",
    profiles: [],
    };
    this.ruleData.profiles = [];
    this.selectedFilterOption = [];
    this.formdata = {};
    this.clearRule();
    this.profileFieldSelected = false;
    for (let i = 0; i < this.profileFields.length; i++) {
      this.profileFields[i].selected = false;
    }
    this.passDataToChild();
    this.allruleList(this.addEditCourseService.data.data);
    // this.disableSelectedRuleFieldType();
  }

  clearRule() {
    this.addRulesForm.reset({
      FilterOpt: [""],
      Value1: [""],
      Value2: [""],
    });
  }

  initRules() {
    return this._fb.group({
      FilterOpt: [""],
      Value1: [""],
      Value2: [""],
    });
  }

  addRule() {
    if (this.ruleData.type == 1) {
      this.profileFieldSelected = true;
      // this.ruleData.type = 'Profile Fields';
    } else if (this.ruleData.type == 2) {
      this.profileFieldSelected = false;
      // this.ruleData.type = 'Others';
    }
    const control = <FormArray>this.addRulesForm.controls["rules"];
    control.push(this.initRules());
    console.log(this.addRulesForm.controls["rules"]);
    this.controlFlag = true;
    this.controlList.push(0);
    this.strArrayType.push([]);
  }

  removeRule(i: number) {
    const control = <FormArray>this.addRulesForm.controls["rules"];
    control.removeAt(i);
    // this.enableSelect(this.strArrayType[i]);
    this.selectedFilterOption.splice(i, 1);
    this.disableSelect();
    this.controlFlag = true;
    this.controlList.splice(i, 1);
    this.strArrayType.splice(i, 1);
  }

  disableSelect() {
    this.profileFields.forEach((data, key) => {
      if (this.selectedFilterOption.indexOf(data.shortname) >= 0) {
        this.profileFields[key].selected = "true";
      } else {
        this.profileFields[key].selected = "false";
      }
    });
    console.log("Selected Disabled", this.strArrayPar);
  }

  ruleTypeSelected($event, i) {
    console.log(this.ruleData);
  }
  prospTypeSelected() {
    console.log(this.ruleData);
    // if(this.ruleData.)
  }
  enroldatetype() {
    console.log(this.ruleData);
    // if(this.ruleData.)
  }
  callType(id: any, index: any) {
    if (this.strArrayType[index]) {
      this.strArrayType[index] = [];
    }

    if (id.srcElement == undefined || id.target == undefined) {
      this.ValueId = id;
    } else {
      this.ValueId = parseInt((id.srcElement || id.target).value);
    }
    // this.disableSelect();

    // this.controlList[index] = this.ValueId;
    for (let i = 0; i < this.profileFields.length; i++) {
      if (this.profileFields[i].id == this.ValueId) {
        // this.selectedRuleType = this.profileFields[i].datatype;

        this.controlList[index] = this.profileFields[i];

        this.strArrayType[index].push(this.profileFields[i]);
        if (this.selectedFilterOption.length > 0) {
          this.selectedFilterOption[index] = this.profileFields[i].shortname;
        } else {
          this.selectedFilterOption.push(this.profileFields[i].shortname);
        }
      }
    }
    this.disableSelect();
  }

  makeRuleDataready(ruleData) {
    this.spinner.show();
    // var rules = ruleData.rules;
    var rules = ruleData.profiles;
    var dimension = "";
    var field = "";
    var value = "";
    var allstring = "";
    console.log("rules", rules);
    if (rules.length > 0) {
      for (var i = 0; i < rules.length; i++) {
        var rule = rules[i];

        if (this.controlList[i].datatype == "datetime") {
          var fromdate = this.formatDateReady(rule.fieldValues[0]);
          var todate = this.formatDateReady(rule.fieldValues[1]);
          // rule.fieldValues = this.formatDateReady(rule.fieldValues);
          rule.fieldValues = fromdate + "$" + todate;
          console.log(rule.fieldValues);
        }
        if (this.controlList[i].datatype == "menu") {
          // console.log(this.selectedrulevalue[i]);
          console.log(rule.fieldValuesArr);
          rules[i].fieldValues = this.createstring(rule.fieldValuesArr);
        }

        if (i == 0) {
          allstring = rule.field + "|" + rule.fieldValues + "|1";
        } else {
          allstring += "#" + rule.field + "|" + rule.fieldValues + "|1";
        }
        console.log(allstring);
      }
    } else {
    }
    var ruleDimension;
    for (let r = 0; r < this.ruleType.length; r++) {
      if (this.ruleType[r].ruleTypeId == ruleData.type) {
        ruleDimension = this.ruleType[r].ruleTypeName;
        console.log(this.ruleType, " +++ ", ruleDimension);
      }
    }

    // field : field,
    // value : value,
    // usersCount: 0,
    var visibleRule;
    console.log(this.ruleData);
    if (this.ruleData.id == "") {
      visibleRule = 1;
    } else {
      visibleRule = this.ruleData.visible;
    }
    console.log("visibleRule", visibleRule);
    // if(this.ruleData.value != ''){
    //    var appData =  this.formatDateReady(this.ruleData.value);
    // }
    var roledata = 0;
    if (ruleData.id != "" && ruleData.id != null && ruleData.id != undefined) {
      roledata = ruleData.id;
    }
    console.log(ruleData.type);
    // if(ruleData.type==2){
    //   allstring=null;
    // }
    if (
      this.ruleData.value != "" ||
      this.ruleData.value != undefined ||
      this.ruleData.value != null
    ) {
      this.ruleData.value = this.formatDateReady(this.ruleData.value);
    }
    if (
      this.ruleData.enroldate != "" ||
      this.ruleData.enroldate != undefined ||
      this.ruleData.enroldate != null
    ) {
      this.ruleData.enroldate = this.formatDateReady(this.ruleData.enroldate);
    }
    var newRuleData = {
      rId: roledata,
      rname: ruleData.name,
      rdescription: ruleData.description,
      appType: ruleData.type,
      appevent: this.ruleData.prospName,
      appDate: this.ruleData.value,
      cid: this.content.courseId,
      tid: this.userdata.tenantId,
      userId: this.userdata.id,
      allstr: allstring,
      visible: visibleRule,
      areaId: this.areaId,
      enroltype: this.ruleData.enroltype,
      enroldate: this.ruleData.enroldate,
    };
    console.log("Final rule data", newRuleData);
    this.enrolService.Addruleforcourse(newRuleData).then((res) => {
      this.spinner.hide();
      console.log(res);
      this.loader = false;
      this.resultdata = res;
      if (this.resultdata.type == false) {
        this.toastr.error(
          'Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.',
          "Error",
          {
            timeOut: 0,
            closeButton: true,
          }
        );
      } else {
        this.newruleadd();
        this.allruleList(this.addEditCourseService.data.data);
        // this.toasterService.pop(courseUpdate);

        this.toastr.success(this.msg, "Success", {
          closeButton: false,
        });
      }
    });
    this.closeRuleModal();
  }

  enableShowRuleUsers: boolean = false;
  onSelectRules({ selected }) {
    console.log("Select Rules Event", selected, this.selectedRules);

    this.selectedRules.splice(0, this.selectedRules.length);
    this.selectedRules.push(...selected);

    if (this.selectedRules.length == 1) {
      this.enableShowRuleUsers = true;
    } else {
      this.enableShowRuleUsers = false;
    }
  }

  onActivateRules(event) {
    // console.log('Activate Event', event);
    if (event.type === "checkbox") {
      // Stop event propagation and let onSelect() work
      console.log("Checkbox Selected", event);
      event.event.stopPropagation();
    } else if (event.type === "click" && event.cellIndex != 0) {
      // Do somethings when you click on row cell other than checkbox
      console.log("Row Clicked", event.row); /// <--- object is in the event row variable
    }
  }

  deleteRules(selectedRow) {
    console.log("Rules Current", selectedRow);
    for (let i = 0; i < this.rowsRules.length; i++) {
      var row = this.rowsRules[i];
      if (selectedRow.id == row.id) {
        this.rowsRules.splice(i, 1);
        this.rowsRules = [...this.rowsRules];
      }
    }
    this.tempRules = this.rowsRules;
    // this.tableData.offset = 0;
  }

  showRuleUsersModal: boolean = false;
  ruleUsersModelTitle: any = "";

  viewRuleUsers(rowData) {
    console.log(rowData);
    // this.ruleUsersModelTitle = this.selectedRules[0].name;
    this.ruleUsersModelTitle = rowData.name;
    this.showRuleUsersModal = true;
    // this.config.ruleBasedEnrolmentData.showEnroleduserPopup = true;
    // this.config.ruleBasedEnrolmentData.enrolUserPopupTableLabel = [...this.labels4];
    this.passDataToChild();
    this.allEnrolUserruleList(this.content,rowData);
  }

  /*********** enrol user by rules ******************/
  allEnrolUserruleList(content,rowData) {
    this.spinner.show();
    var data = {
      areaId: this.areaId,
      instanceId: content.courseId,
      tId: this.userdata.tenantId,
      ruleId:rowData.enrolRuleId,
      mode: 2,
    };
    this.addEditCourseService.getallenroluser(data).then((enrolData) => {
      this.spinner.hide();
      this.enrolldatarule = enrolData["data"];
      this.rowsEnrolRule = enrolData["data"];
      this.rowsEnrolRule = [...this.rowsEnrolRule];
      // for (let i = 0; i < this.rowsEnrolRule.length; i++) {
      //   this.rowsEnrolRule[i].Date = new Date(this.rowsEnrolRule[i].enrolDate);
      //   this.rowsEnrolRule[i].enrolDate = this.formdate(this.rowsEnrolRule[i].Date);
      // }
      console.log("EnrolledUSer", this.rowsEnrolRule);
      if ((this.enrolldata.visible = 1)) {
        this.enableCourse = false;
      } else {
        this.enableCourse = true;
      }

      this.passDataToChild();
      // this.cdf.detectChanges();
    });
  }
  closeRuleUsersModal() {
    this.showRuleUsersModal = false;
    this.ruleUsersModelTitle = "";
  }

  openRegulatoryModal() {
    this.showAddRegulatoryModal = true;
    // this.courseDataService.showRegulatory = this.showAddRegulatoryModal;
  }

  onSelectReg({ selected }) {
    console.log("Select Reg Event", selected, this.selectedReg);

    this.selectedReg.splice(0, this.selectedReg.length);
    this.selectedReg.push(...selected);

    // if(this.selectedManual.length == 1){
    // 	this.enableShowRuleUsers = true;
    // }else{
    // 	this.enableShowRuleUsers = false;
    // }
  }

  onActivateReg(event) {
    // console.log('Activate Event', event);
    if (event.type === "checkbox") {
      // Stop event propagation and let onSelect() work
      console.log("Checkbox Selected", event);
      event.event.stopPropagation();
    } else if (event.type === "click" && event.cellIndex != 0) {
      // Do somethings when you click on row cell other than checkbox
      console.log("Row Clicked", event.row); /// <--- object is in the event row variable
    }
  }

  deleteReg(selectedRow) {
    console.log("Rules Current", selectedRow);
    for (let i = 0; i < this.rowsReg.length; i++) {
      var row = this.rowsReg[i];
      if (selectedRow.id == row.id) {
        this.rowsReg.splice(i, 1);
        this.rowsReg = [...this.rowsReg];
      }
    }
    this.tempReg = this.rowsReg;
    this.passDataToChild();
    // this.tableData.offset = 0;
  }

  closeRegulatoryModal() {
    this.showAddRegulatoryModal = false;
    // this.courseDataService.showRegulatory = this.showAddRegulatoryModal;
    this.clearRegData();
  }

  makeRegDataReady(data) {
    this.spinner.show();
    console.log(data);
    var enrolDt: any = this.formatDateReady(data.enrolDate);
    var roledata = 0;
    console.log(this.regiD);
    if (this.regiD != "" && this.regiD != null && this.regiD != undefined) {
      roledata = this.regiD;
    } else {
      roledata = 0;
    }

    console.log(this.regenId);

    var regDataDinal = {
      rId: roledata,
      rDtId: 0,
      enDate: enrolDt,
      cid: this.content.courseId,
      tid: this.userdata.tenantId,
      userId: this.userdata.id,
      visible: 1,
    };

    console.log("Regulatory Final data", regDataDinal);
    this.enrolService.Addregulatoryforcourse(regDataDinal).then((result) => {
      console.log(result);
      this.spinner.hide();
      if (result["type"] == true) {
        this.regData = {};
        this.addregulatorynew();
        this.toastr.success("Enrol date added ", "Success", {
          closeButton: false,
        });
      } else {
        this.toastr.error(
          'Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.',
          "Error",
          {
            timeOut: 0,
            closeButton: true,
          }
        );
      }
    });
    this.allregulatorylist(this.content);
  }

  clearRegData() {
    this.regData = {
      id: "",
      enrolDate: "",
      dueDays: "",
      reminder: "",
    };
  }

  /*--------------- enrolled User (self)----------------------*/
  selfenrolledUser(content) {
    this.spinner.show();
    var data = {
      areaId: this.areaId,
      instanceId: content.courseId,
      tId: this.userdata.tenantId,
      mode: 4,
    };
    console.log(data);
    this.addEditCourseService.getallenroluser(data).then(
      (enrolData) => {
        this.spinner.hide();
        console.log(enrolData);
        // this.enrollselfdata = enrolData["data"];
        // this.rowsSelf = enrolData["data"];
        // this.rowsSelf = [...this.rowsSelf];
        // this.makeSelfTableDataReady( enrolData["data"]);

        if(enrolData['type'] && enrolData["data"] && enrolData["data"].length != 0){
          for (let i = 0; i < enrolData["data"].length; i++) {
            // this.rowsManual[i].Date = new Date(this.rowsManual[i].enrolDate);
            // this.rowsManual[i].enrolDate = this.formdate(this.rowsManual[i].Date);
            if (enrolData["data"][i].visible == 1) {
              enrolData["data"][i].btntext = "fa fa-eye";
            } else {
              enrolData["data"][i].btntext = "fa fa-eye-slash";
            }

          }
          this.rowsSelf = enrolData["data"];
          this.rowsSelf = [...this.rowsSelf];
          this.enrollselfdata = enrolData["data"];
        }

        if (this.enrolldata.length == 0) {
          this.showEnrolpage = !this.showEnrolpage;
        }
        console.log("SELF USER", this.rowsSelf);
        this.passDataToChild();
        // this.cdf.detectChanges();
      },
      (resUserError) => {
        console.log(resUserError);
        if (resUserError.statusText == "Unauthorized") {
          this.router.navigate(["/login"]);
        }
        // this.loader = false;
        this.spinner.hide();
        this.errorMsg = resUserError;
      }
    );
  }

  /*--------------disable self user--------------*/
  visibilityTableRow3(row) {
    if (row && row["wfId"] && row["wfId"] !== "" && row["wfId"] != null) {
      const msg = row["msg"]
        ? row["msg"]
        : "Unable to hide user as it is enrolled in workflow.";
      this.toastr.warning(msg, "Warning");
      return null;
    }
    let value;
    let status;

    if (row.visible == 1) {
      row.btntext = "fa fa-eye-slash";
      value = "fa fa-eye-slash";
      row.visible = 0;
      status = 0;
    } else {
      status = 1;
      value = "fa fa-eye";
      row.visible = 1;
      row.btntext = "fa fa-eye";
    }

    for (let i = 0; i < this.rowsSelf.length; i++) {
      if (this.rowsSelf[i].employeeId == row.employeeId) {
        this.rowsSelf[i].btntext = row.btntext;
        this.rowsSelf[i].visible = row.visible;
      }
    }
    this.passDataToChild();
    var visibilityData = {
      employeeId: row.employeeId,
      visible: status,
      courseId: this.content.courseId,
      tId: this.userdata.tenantId,
      aId: 2,
    };
    this.addEditCourseService.disableEnrol(visibilityData).then(
      (result) => {
        console.log(result);
        this.spinner.hide();
        this.loader = false;
        this.resultdata = result;
        if (this.resultdata.type == false) {
          this.toastr.error(
            'Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.',
            "Error",
            {
              timeOut: 0,
              closeButton: true,
            }
          );
        } else {
          console.log("after", row.visible);
          this.allEnrolUser(this.addEditCourseService.data.data);
          // this.toasterService.pop(courseUpdate);

          this.toastr.success(this.resultdata.data, "Success", {
            closeButton: false,
          });
        }
      },
      (resUserError) => {
        this.loader = false;
        this.errorMsg = resUserError;
        // this.closeEnableDisableCourseModal();
      }
    );

    console.log("row", row);
  }
  // disableselfVisibility(currentIndex, row, status) {
  //   this.spinner.show();
  //   var visibilityData = {
  //     employeeId: row.employeeId,
  //     visible: status,
  //     courseId: this.content.courseId,
  //     tId: this.userdata.tenantId,
  //     aId: 2,
  //   }
  //   this.addEditCourseService.disableEnrol(visibilityData).then(result => {
  //     console.log(result);
  //     this.spinner.hide();
  //     this.loader = false;
  //     this.resultdata = result;
  //     if (this.resultdata.type == false) {

  //       this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
  //         timeOut: 0,
  //         closeButton: true
  //       });
  //     } else {

  //       console.log('after', row.visible)
  //       this.allEnrolUser(this.addEditCourseService.data.data);
  //       // this.toasterService.pop(courseUpdate);

  //       this.toastr.success(this.resultdata.data, 'Success', {
  //         closeButton: false
  //       });
  //     }
  //   },
  //     resUserError => {
  //       this.loader = false;
  //       this.errorMsg = resUserError;
  //       // this.closeEnableDisableCourseModal();
  //     });
  // }

  /*--------open setting for self ------*/
  openSelfModal() {
    this.showAddSelfModal = true;
    this.fetchsetting();
    this.passDataToChild();
    // this.courseDataService.showRule = this.showAddRuleModal;
  }

  onSelectSelf({ selected }) {
    console.log("Select Reg Event", selected, this.selectedSelf);

    this.selectedSelf.splice(0, this.selectedSelf.length);
    this.selectedSelf.push(...selected);
  }

  onActivateSelf(event) {
    // console.log('Activate Event', event);
    if (event.type === "checkbox") {
      // Stop event propagation and let onSelect() work
      console.log("Checkbox Selected", event);
      event.event.stopPropagation();
    } else if (event.type === "click" && event.cellIndex != 0) {
      // Do somethings when you click on row cell other than checkbox
      console.log("Row Clicked", event.row); /// <--- object is in the event row variable
    }
  }

  deleteSelf(selectedRow) {
    console.log("Self Current", selectedRow);
    for (let i = 0; i < this.rowsSelf.length; i++) {
      var row = this.rowsSelf[i];
      if (selectedRow.ecn == row.ecn) {
        this.rowsSelf.splice(i, 1);
        this.rowsSelf = [...this.rowsSelf];
      }
    }
    this.tempSelf = this.rowsSelf;
    this.passDataToChild();
    // this.tableData.offset = 0;
  }

  initSelfFields() {
    return this._fb.group({
      FilterOpt: [""],
      Value1: [""],
      Value2: [""],
    });
  }

  /*************** New code Start **************/

  isFetchingSettings: boolean = false;

  fetchsetting() {
    this.spinner.show();
    var data = {
      courseId: this.content.courseId,
      tId: this.userdata.tenantId,
    };
    this.enrolService.getfechsetting(data).then(async (res) => {
      console.log("fetch setting", res);
      this.spinner.hide();
      const selfFields = res["data"][0];
      this.selfFieldsData = res["data"][0];
      if (selfFields) {
        // this.isFetchingSettings = true;
        this.onSelfFieldList(this.selfFieldsData.profiles, 0, (result) => {
          this.passDataToChild();
        });
        this.selfFieldsData = {
          id: this.selfFieldsData.enrolSelfId,
          sid: this.selfFieldsData.selfType,
          maxCount: this.selfFieldsData.maxEnrolments,
          type: 1,
          cid: this.selfFieldsData.cid,
          tid: this.userdata.tid,
          userId: this.selfFieldsData.userId,
          profiles: this.selfFieldsData.profiles,
        };
        this.msg2 = "Setting updated ";
        /**
         * List of selected fields for self enrolled setting
         */

        // this.passDataToChild();
        console.log("Edit self data ", this.selfFieldsData);
      } else {
        this.selfFieldsData = {
          id: 0,
          sid: "",
          maxCount: "",
          cid: "",
          tid: "",
          userId: "",
          profiles: [],
          type:"",
        };
        this.msg2 = "Setting added ";
        this.passDataToChild();
        console.log(this.selfFieldsData);
      }
      console.log(this.selfFieldsData.enrolSelfId);
    });
  }
  async onSelfFieldList(list, i, cb) {
    if (list.length === 0 || list.length === i) {
      cb(true);
      this.cdf.detectChanges();
    } else {
      let rule = list[i];
      this.strArrayTypeSelfFields.push([]);
      const responsecalltype = await this.callTypeSelfFields(rule.field, i, 1);

      if (this.controlList[i].datatype == "datetime") {
        var array = list[i].fieldValues.split("$");
        this.selfFieldsData.profiles[i].fieldValues = [
          new Date(array[0]),
          new Date(array[1]),
        ];

      }
      // this.passDataToChild();
      // if (this.controlList[i].datatype == 'menu') {
      //   this.databindself(this.selfFieldsData.profiles[i], i, i);
      //   console.log(this.selectedsettingvalue[i]);
      // }
      this.onSelfFieldList(list, i + 1, cb);
    }
  }
  closeSelfModal() {
    this.showAddSelfModal = false;
    this.passDataToChild();
    this.selfFieldsData = {
      enrolSelfId: "",
      sid: "",
      maxCount: "",
      cid: "",
      tid: "",
      userId: "",
      profiles: [],
    };
    this.selectedFilterOptionSelf = [];
    this.formdataSelf = {};
    this.formdata = {};
    this.controlList = [];
    this.itemList = [];
    this.selectedUsers = [];
    this.selfProfileFieldSelected = false;
    this.strArrayTypeSelfFields = [];
    for (let i = 0; i < this.profileFieldsSelf.length; i++) {
      this.profileFieldsSelf[i].selected = false;
    }
    this.isFetchingSettings = false;
    this.passDataToChild();
    // this.isFetchingSettings = false;
    // this.disableSelectSelfFields();
  }

  selectedFieldValue: any;

  onDropdownFieldSelected() {
    this.selectedFieldValue = this.profileFieldsSelf.find(
      (selectedField) => selectedField.field === this.selectedFieldValue
    );
    this.passDataToChild();
  }

  convert_int_to_string(inputString) {
    if (!inputString) return null;
    return String(inputString);
  }

  convert_string_to_int(inputInt) {
    if (!inputInt) return null;
    return Number(inputInt);
  }

  async databindself(alldata, i, profFieldIndex) {
    let data = {
      lovtype: this.menutypeid,
      tId: this.userdata.tenantId,
    };
    let strArraySkilllevel = this.profileFieldsSelf[profFieldIndex].subtype;
    if (this.menutypeid) {
      if (strArraySkilllevel && strArraySkilllevel.length > 0) {
        this.make_selected_dropdown_data_ready(alldata, strArraySkilllevel, i);
      } else {
        await this.enrolService.getprofileFieldDropdown(data).then((res) => {
          console.log(res);
          if (res["type"] == true) {
            let strArraySkilllevel = res["data"][0];
            this.profileFieldsSelf[profFieldIndex].subtype = strArraySkilllevel;
            this.make_selected_dropdown_data_ready(
              alldata,
              strArraySkilllevel,
              i
            );

          }
        });
      }
    }
  }

  make_selected_dropdown_data_ready(alldata, strArraySkilllevel, i) {
    var array = alldata.fieldValues.split(",");
    let newarray = [];
    strArraySkilllevel.forEach((dataitem) => {
      array.forEach((element) => {
        if (dataitem.name == element) newarray.push(dataitem);
      });
    });
    alldata.fieldValuesArr = [];
    if (newarray && newarray.length > 0) {
      alldata.fieldValuesArr = newarray;
      alldata.fieldValues = "";
    }
    if (i == this.selfFieldsData.profiles.length - 1) {
      this.isFetchingSettings = false;
      this.passDataToChild();
    }

    this.passDataToChild();
    this.cdf.detectChanges();
  }

  make_selected_dropdown_data_ready_Reg(alldata, strArraySkilllevel, i) {
    var array = alldata.fieldValues.split(",");
    let newarray = [];
    strArraySkilllevel.forEach((dataitem) => {
      array.forEach((element) => {
        if (dataitem.name == element) newarray.push(dataitem);
      });
    });
    alldata.fieldValuesArr = [];
    if (newarray && newarray.length > 0) {
      alldata.fieldValuesArr = newarray;
      alldata.fieldValues = "";
    }
    if (i == this.regularData.profiles.length - 1) {
      // this.isFetchingSettings = false;
    }
    this.cdf.detectChanges();
  }
  onItemSelectSelf(currentEvent: any, currentIndex: any, currentItem: any) {
    console.log(
      "currentEvent, currentIndex, currentItem",
      currentEvent,
      currentIndex,
      currentItem
    );
  }
  OnItemDeSelectSelf(currentEvent: any, currentIndex: any, currentItem: any) {
    console.log(
      "currentEvent, currentIndex, currentItem",
      currentEvent,
      currentIndex,
      currentItem
    );
  }
  onSelectAllSelf(currentEvent: any, currentIndex: any, currentItem: any) {
    console.log(
      "currentEvent, currentIndex, currentItem",
      currentEvent,
      currentIndex,
      currentItem
    );
  }
  onDeSelectAllSelf(currentEvent: any, currentIndex: any, currentItem: any) {
    console.log(
      "currentEvent, currentIndex, currentItem",
      currentEvent,
      currentIndex,
      currentItem
    );
  }
  onItemSelectRule(currentEvent: any, currentIndex: any, currentItem: any) {
    console.log(
      "currentEvent, currentIndex, currentItem",
      currentEvent,
      currentIndex,
      currentItem
    );
  }
  OnItemDeSelectRule(currentEvent: any, currentIndex: any, currentItem: any) {
    console.log(
      "currentEvent, currentIndex, currentItem",
      currentEvent,
      currentIndex,
      currentItem
    );
  }
  onSelectAllRule(currentEvent: any, currentIndex: any, currentItem: any) {
    console.log(
      "currentEvent, currentIndex, currentItem",
      currentEvent,
      currentIndex,
      currentItem
    );
  }
  onDeSelectAllRule(currentEvent: any, currentIndex: any, currentItem: any) {
    console.log(
      "currentEvent, currentIndex, currentItem",
      currentEvent,
      currentIndex,
      currentItem
    );
  }
  addSelfFields() {
    this.isFetchingSettings = false;
    this.passDataToChild();
    if (this.selfFieldsData.profiles.length == 0) {
      this.selfProfileFieldSelected = true;
    }

    let defualselfObj = {
      enrolProfileId: 0,
      field: "",
      fieldValues: "",
      fieldValuesArr: [],
    };
    this.selfFieldsData.profiles.push(defualselfObj);
    // this.controlFlag = true;
    this.controlList.push(0);
    console.log(this.controlList);
    // this.strArrayType.push([]);
    this.strArrayTypeSelfFields.push([]);
    console.log(this.strArrayTypeSelfFields);
    this.isFetchingSettings = true;
    this.passDataToChild();
  }

  removeSelfFields(currentIndex, id, currentItem) {
    this.isFetchingSettings = false;
    this.passDataToChild();
    console.log("Existing data : ", this.selfFieldsData);
    // this.profileFieldsSelf[currentIndex].subtype = [];
    // this.updatedProfilesFieldDataAfterRemove(currentItem);
    this.selfFieldsData.profiles.splice(currentIndex, 1);
    // this.selfFieldsData.profiles = [...this.selfFieldsData.profiles];
    console.log("Updated data : ", this.selfFieldsData);
    this.selectedFilterOptionSelf.splice(currentIndex, 1);
    this.selectedsettingvalue.splice(currentIndex, 1);
    this.disableSelectSelfFields();
    this.controlFlag = true;
    this.controlList.splice(currentIndex, 1);
    // this.controlList = [...this.controlList];
    this.strArrayTypeSelfFields.splice(currentIndex, 1);

    this.cdf.detectChanges();
    this.isFetchingSettings = true;
    this.passDataToChild();
  }

  updatedProfilesFieldDataAfterRemove(currentItem) {
    // this.profileFieldsSelf = this.profileFieldsSelf.filter(function(value, index, arr){
    //   return value > 5;
    // });
    this.profileFieldsSelf.forEach((data, key) => {
      if (data.id == Number(currentItem.field)) {
        data.subtype = [];
      }
    });
    this.passDataToChild();
  }

  disableSelectSelfFields() {
    this.profileFieldsSelf.forEach((data, key) => {
      if (this.selectedFilterOptionSelf.indexOf(data.shortname) >= 0) {
        this.profileFieldsSelf[key].selected = "true";
      } else {
        this.profileFieldsSelf[key].selected = "false";
        // this.clear_selected_fields_data(this.profileFieldsSelf[key]);
      }
    });
    console.log("Self Selected Disabled", this.strArrayTypeSelfFields);
    this.passDataToChild();
    // this.cdf.detectChanges();
  }

  clear_selected_fields_data(selectedProfileFieldData) {
    this.selfFieldsData.profiles.forEach((data, key) => {
      if (selectedProfileFieldData.id == data.field) {
        // data.fieldValues = '';
        // data.fieldValuesArr = [];
      }
    });
    this.passDataToChild();
  }

  selfFieldTypeSelected(
    currentEvent: any,
    currentIndex: any,
    currentItem: any
  ) {
    console.log(
      "currentEvent, currentIndex, currentItem",
      currentEvent,
      currentIndex,
      currentItem
    );
  }

  // async callTypeSelfFields(id: any, index: any, status) {
  //   console.log(this.selectedsettingvalue[index]);
  //   this.selectedsettingvalue[index] = [];
  //   if (status == 2) {
  //     this.selfFieldsData.profiles[index].fieldValuesArr = [];
  //     this.selfFieldsData.profiles[index].fieldvalues = '';
  //   }
  //   if (this.strArrayTypeSelfFields[index]) {
  //     this.strArrayTypeSelfFields[index] = [];
  //   }

  //   if (id.srcElement == undefined || id.target == undefined) {
  //     this.ValueId = id;
  //   } else {
  //     this.ValueId = parseInt((id.srcElement || id.target).value);
  //   }

  //   for (let i = 0; i < this.profileFieldsSelf.length; i++) {
  //     if (this.profileFieldsSelf[i].id == this.ValueId) {
  //       this.controlList[index] = this.profileFieldsSelf[i];
  //       this.profileFieldsSelf[i].subtype = [];
  //       this.strArrayTypeSelfFields[index].push(this.profileFieldsSelf[i]);
  //       this.selectedFilterOptionSelf[index] = this.profileFieldsSelf[i].shortname;
  //       this.datarule = this.profileFieldsSelf[i].datatype;
  //       this.menutypeid = this.profileFieldsSelf[i].menuTypeId;
  //       console.log('menutypeid for :', i, 'is :', this.menutypeid);

  //       if (this.menutypeid != '' && this.menutypeid != null && this.menutypeid != undefined) {
  //         var data = {
  //           lovtype: this.menutypeid,
  //           tId: this.userdata.tenantId
  //         }
  //         const response = await this.enrolService.getprofileFieldDropdown(data);
  //         const subTypeFields = response['data'][0];
  //         console.log('subTypeFields ', subTypeFields);
  //         this.profileFields[i].subtype = subTypeFields;

  //         // if (this.isFetchingSettings) {
  //           if (this.controlList[index].datatype == 'menu') {
  //             this.databindself(this.selfFieldsData.profiles[index], index, i);
  //             console.log(this.selectedsettingvalue[index]);
  //           }
  //         // fields[i].fieldValues = this.createstring(this.selectedsettingvalue[i])
  //         console.log(selffield.fieldValuesArr);
  //         fields[i].fieldValues = this.createstring(selffield.fieldValuesArr);
  //       }
  //       if (i == 0) {
  //         allstring = selffield.field + '|' + selffield.fieldValues + '|1';
  //       }
  //       else {
  //         allstring += '#' + selffield.field + '|' + selffield.fieldValues + '|1';
  //       }
  //       console.log(allstring);
  //     }
  //   }

  //   var selfType;
  //   for (let r = 0; r < this.selfType.length; r++) {
  //     if (this.selfType[r].idd == selfFieldsData.id) {
  //       selfType = this.selfType[r].typeName;
  //     }
  //   }
  //   var selfDimension;
  //   for (let r = 0; r < this.selfFeildType.length; r++) {
  //     if (this.selfFeildType[r].selfTypeId == selfFieldsData.type) {
  //       selfDimension = this.selfFeildType[r].selfTypeName;
  //     }
  //   }
  //   var selfdataid = 0;
  //   if (selfFieldsData.id != '' && selfFieldsData.id != null && selfFieldsData.id != undefined) {
  //     selfdataid = selfFieldsData.id;
  //   }
  //   var newSelfData = {
  //     rId: selfdataid,
  //     sType: selfFieldsData.sid,
  //     maxEnrolments: selfFieldsData.maxCount,
  //     cid: this.content.courseId,
  //     tid: this.userdata.tenantId,
  //     userId: this.userdata.id,
  //     allstr: allstring,
  //     areaId: this.areaId,
  //   }

  //   console.log('Final self data', newSelfData);

  //   this.enrolService.addselfsetting(newSelfData).then(res => {
  //     console.log(res);
  //     if (res['type'] == false) {

  //       this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
  //         timeOut: 0,
  //         closeButton: true
  //       });
  //     } else {
  //       this.newsetting(res['data'][0])

  //       this.toastr.success(this.msg2, 'Success', {
  //         closeButton: false
  //       });
  //     }
  //   })

  //   this.closeSelfModal();
  // }
  async callTypeSelfFields(id: any, index: any, status) {
    // this.isFetchingSettings = false;
    console.log(this.selectedsettingvalue[index]);
    this.selectedsettingvalue[index] = [];
    console.log("new", this.selfFieldsData.profiles[index]);
    if (status == 2) {
      this.selfFieldsData.profiles[index].fieldValuesArr = [];
      this.selfFieldsData.profiles[index].fieldValues = "";
    }
    if (this.strArrayTypeSelfFields[index]) {
      this.strArrayTypeSelfFields[index] = [];
    }

    if (id.srcElement == undefined || id.target == undefined) {
      this.ValueId = id;
    } else {
      this.ValueId = parseInt((id.srcElement || id.target).value);
    }

    for (let i = 0; i < this.profileFieldsSelf.length; i++) {
      if (this.profileFieldsSelf[i].id == this.ValueId) {
        this.controlList[index] = this.profileFieldsSelf[i];
        this.profileFieldsSelf[i].subtype = [];
        this.strArrayTypeSelfFields[index].push(this.profileFieldsSelf[i]);
        this.selectedFilterOptionSelf[index] = this.profileFieldsSelf[
          i
        ].shortname;
        this.datarule = this.profileFieldsSelf[i].datatype;
        this.menutypeid = this.profileFieldsSelf[i].menuTypeId;
        console.log(
          "menutypeid for :",
          i,
          "is :",
          this.selectedFilterOptionSelf
        );
        console.log("menutypeid for :", i, "is :", this.strArrayTypeSelfFields);

        if (
          this.menutypeid != "" &&
          this.menutypeid != null &&
          this.menutypeid != undefined
        ) {
          var data = {
            lovtype: this.menutypeid,
            tId: this.userdata.tenantId,
          };
          const response = await this.enrolService.getprofileFieldDropdown(
            data
          );
          const subTypeFields = response["data"][0];
          console.log("subTypeFields ", subTypeFields);
          this.profileFieldsSelf[i].subtype = subTypeFields;

          if (status == 1) {
            if (this.controlList[index].datatype == "menu") {
              this.databindself(this.selfFieldsData.profiles[index], index, i);
              console.log(this.selectedsettingvalue[index]);
            }
          }

        }
      }
    }
    this.isFetchingSettings = true;
    this.passDataToChild();
    this.disableSelectSelfFields();
  }
  newsetting(res) {
    console.log(res);
    var data = {
      inst: this.content.courseId,
      setId: res.setId,
      tenId: this.userdata.tenantId,
      mgrId: this.content.creatorId,
    };
    console.log(data);
    this.enrolService.addselfsetting_new(data).then((res) => {
      console.log(res);
    });
  }
  newruleadd() {
    var data = {
      areaId: this.areaId,
      corsId: this.content.courseId,
      tenId: this.userdata.tenantId,
    };
    console.log(data);
    this.enrolService.Addruleforcourse_new_enrolmnet(data).then((res) => {
      console.log(res);
    });
  }
  addregulatorynew() {
    var data = {
      corsId: this.content.courseId,
      tenId: this.content.tenantId,
    };
    console.log(data);
    this.enrolService.Addregulatoryforcourse_new_enrol(data).then((res) => {
      console.log(res);
    });
    this.allregulatorylist(this.content);
  }
  saveSelfFields(item, f) {
    if (f.valid) {
      console.log(item);
      // this.loader =true;
      // console.log(this.addRulesForm.value);
      let selfFields: any = item;
      this.selfFieldsData.fields = selfFields;
      console.log("Self fields data final", selfFields);

      this.makeSelfDataReady(selfFields);
    } else {
      console.log("Please Fill all fields");
      Object.keys(f.controls).forEach((key) => {
        f.controls[key].markAsDirty();
      });
    }
  }
  formatDateReady(date) {
    if (date) {
      date = new Date(date);
      var day = date.getDate();
      var monthIndex = ("0" + (date.getMonth() + 1)).slice(-2);
      var year = date.getFullYear();

      return year + "-" + monthIndex + "-" + day;
    }
  }

  formdate(date) {
    if (date) {
      // const months = ['JAN', 'FEB', 'MAR','APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
      // var day = date.getDate();
      // var monthIndex = months[date.getMonth()];
      // var year = date.getFullYear();
      // return day + '_' + monthIndex + '_' +year;
      var formatted = this.datePipe.transform(date, "dd-MM-yyyy");
      return formatted;
    }
  }

  ngOnDestroy() {
    if (this.addEditCourseService.data.data) {
      this.parent_Comp.allEnrolUser(this.addEditCourseService.data.data);
    } else {
      this.parent_Comp.allEnrolUser(this.addEditCourseService.data);
    }
    // this.contentservice.data =demo;
    console.log("Service destroy");
  }

  // Help Code Start Here //

  helpContent: any;
  getHelpContent() {
    return new Promise((resolve) => {
      this.http1
        .get("../../../../../../assets/help-content/addEditCourseContent.json")
        .subscribe(
          (data) => {
            this.helpContent = data;
            this.passDataToChild();
            console.log("Help Array", this.helpContent);
          },
          (err) => {
            resolve("err");
          }
        );
    });
    // return this.helpContent;
  }

  // Help Code Ends Here //

  makeSelfDataReady(selfFieldsData) {
    var fields = selfFieldsData.profiles;
    var dimension = "";
    var field = "";
    var value = "";
    var allstring = "";
    if (fields.length > 0) {
      for (var i = 0; i < fields.length; i++) {
        var selffield = fields[i];

        if (this.controlList[i].datatype == "datetime") {
          var fromdate = this.formatDateReady(selffield.fieldValues[0]);
          var todate = this.formatDateReady(selffield.fieldValues[1]); // rule.fieldValues = this.formatDateReady(rule.fieldValues);
          selffield.fieldValues = fromdate + "$" + todate;
          console.log(selffield.fieldValues); // selffield.fieldValues = this.formatDateReady(selffield.fieldValues);
        }
        if (this.controlList[i].datatype == "menu") {
          // console.log(this.selectedsettingvalue[i]);
          // fields[i].fieldValues = this.createstring(this.selectedsettingvalue[i])
          console.log(selffield.fieldValuesArr);
          fields[i].fieldValues = this.createstring(selffield.fieldValuesArr);
        }
        if (i == 0) {
          allstring = selffield.field + "|" + selffield.fieldValues + "|1";
        } else {
          allstring +=
            "#" + selffield.field + "|" + selffield.fieldValues + "|1";
        }
        console.log(allstring);
      }
    }

    var selfType;
    for (let r = 0; r < this.selfType.length; r++) {
      if (this.selfType[r].idd == selfFieldsData.id) {
        selfType = this.selfType[r].typeName;
      }
    }
    var selfDimension;
    for (let r = 0; r < this.selfFeildType.length; r++) {
      if (this.selfFeildType[r].selfTypeId == selfFieldsData.type) {
        selfDimension = this.selfFeildType[r].selfTypeName;
      }
    }
    var selfdataid = 0;
    if (
      selfFieldsData.id != "" &&
      selfFieldsData.id != null &&
      selfFieldsData.id != undefined
    ) {
      selfdataid = selfFieldsData.id;
    }
    if(allstring != ""){
    var newSelfData = {
      rId: selfdataid,
      sType: selfFieldsData.sid,
      maxEnrolments: selfFieldsData.maxCount,
      cid: this.content.courseId,
      tid: this.userdata.tenantId,
      userId: this.userdata.id,
      allstr: allstring,
      areaId: this.areaId,
    };
    this.passDataToChild();
    console.log("Final self data", newSelfData);

    this.enrolService.addselfsetting(newSelfData).then((res) => {
      console.log(res);
      if (res["type"] == false) {
        this.toastr.error(
          'Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.',
          "Error",
          {
            timeOut: 0,
            closeButton: true,
          }
        );
      } else {
        this.newsetting(res["data"][0]);
        this.passDataToChild();
        this.toastr.success(this.msg2, "Success", {
          closeButton: false,
        });
      }
    });

    this.closeSelfModal();
  }else{
    this.toastr.warning('Please select conditions of enrollment','warning')
  }
  }

  passDataToChild() {
    this.config.manulEnrolmentData.userList = [...this.tempUsers];
    this.config.manulEnrolmentData.data = [...this.rowsManual];
    this.config.manulEnrolmentData.selectedUsers = [...this.selectedUsers];

    this.config.ruleBasedEnrolmentData.data = [...this.rowsRules];

    this.config.regulatoryEnrolmentData.data = [...this.rowsReg];

    this.config.ruleBasedEnrolmentData.helpContent = _.clone(this.helpContent);
    this.config.ruleBasedEnrolmentData.profileFieldSelected = _.clone(
      this.profileFieldSelected
    );
    this.config.ruleBasedEnrolmentData.profileFields = _.cloneDeep[this.profileFields]
    this.config.ruleBasedEnrolmentData.ruleData = _.cloneDeep(this.ruleData);
    this.config.ruleBasedEnrolmentData.controlList = _.cloneDeep(this.controlList);
    this.config.ruleBasedEnrolmentData.rowsEnrolRule = [...this.rowsEnrolRule];
    this.config.ruleBasedEnrolmentData.enrolUserPopupTableLabel = [this.labels4];
    this.config.ruleBasedEnrolmentData.showEnroleduserPopup =_.clone(this.showRuleUsersModal);
    this.config.ruleBasedEnrolmentData.showAddRuleModal = _.clone(this.showAddRuleModal);
    this.config.ruleBasedEnrolmentData.profileFields = _.cloneDeep(
      this.profileFields
    );

    this.config.regulatoryEnrolmentData.showAddRegulatoryFilterModal = _.clone(this.showAddRegulatoryFilterModal)
    // this.config.ruleBasedEnrolmentData.ruleData.profiles = _.clone(this.ruleData.profiles);

    this.config.regulatoryEnrolmentData.helpContent = _.clone(this.helpContent);
    this.config.regulatoryEnrolmentData.regFilterProfileFieldSelected = _.clone(this.regFilterProfileFieldSelected);
    this.config.regulatoryEnrolmentData.profileFieldsRegFilter = [...this.profileFieldsRegFilter];
    this.config.regulatoryEnrolmentData.controlList = [...this.controlList];
    this.config.regulatoryEnrolmentData.regularData =  _.clone(this.regularData);

    this.config.selfEnrolmentData.helpContent = _.clone(this.helpContent);
    this.config.selfEnrolmentData.showAddSelfModal = _.clone(this.showAddSelfModal);
    this.config.selfEnrolmentData.selfFieldsData = _.cloneDeep(this.selfFieldsData);
    this.config.selfEnrolmentData.selfType = _.cloneDeep(this.selfType);
    this.config.selfEnrolmentData.isFetchingSettings = _.clone(this.isFetchingSettings);
    this.config.selfEnrolmentData.controlList = _.clone(this.controlList);
    this.config.selfEnrolmentData.profileFieldsSelf = _.clone(this.profileFieldsSelf);
    this.config.selfEnrolmentData.selfFeildType = _.clone(this.selfFeildType);
    this.config.selfEnrolmentData.data = _.clone(this.rowsSelf);
    // Pricing

    this.config.priceBasedEnrolmentData.data = _.clone(this.tempDisplayPriceList);
    this.config.priceBasedEnrolmentData.currencyTypeDropDown = _.clone(this.currencyTypeDropDown);
    this.config.priceBasedEnrolmentData.discountListDropdownList = _.clone(this.discountListDropdownList);
    this.config.priceBasedEnrolmentData.addEditPriceForm = _.clone(this.addEditPriceForm);
    this.config.priceBasedEnrolmentData.showSidebar = _.clone(this.showPriceSidebar);
    this.config.priceBasedEnrolmentData.labels =  _.clone(this.labelsPrice);
    console.log("this.profileFieldSelected", this.profileFieldSelected);
    console.log(
      "this.config",
      this.config
    );
    this.cdf.detectChanges();
  }

  performActionOnData(event) {
    console.log("Event ==>", event);
    if (event) {
      // console.log('args ==>', event.argument.join(','));
      // console.log('args ==>', [...event.argument]);
      switch (event.action) {
        // Tab Events
        case "manual":
          // code...
          // this.enrolment.manual = true;
          this.allEnrolUser(this.content);
          break;
        case "ruleBased":
          // code...
          // this.enrolment.rule = true;
          this.allruleList(this.content);
          break;
        case "self":
          // this.enrolment.self = true;
          this.selfenrolledUser(this.content);
          // code...
          break;
        case "regulatory":
          // this.enrolment.regulatory = true;
          this.allregulatorylist(this.content);
          // code...
          break;
        case "pricing":
            // this.enrolment.regulatory = true;
            this.getPriceList();
            // code...
            break;

        // Manual Enrol Events
        case "searchManEnrol":
          this.searchManEnrol(event.argument[0]);
          break;
        case "clearesearch":
          this.clearesearch();
          break;
        case "onSearch":
          this.onSearch(event.argument[0]);
          break;
        case "manEnrolUser":
          this.selectedUsers = event.argument[0];
          this.manEnrolUser();
          break;
        case "visibilityTableRow":
          this.visibilityTableRow(event.argument[0]);
          break;

        // Rule Enrolment
        case "searchRuleEnrol":
          this.searchRuleEnrol(event.argument[0]);
          break;
        case "clearRuleEnrol":
          this.clearRuleEnrol();
          break;
        case "addEditRule":
          this.openRuleModal(event.argument[0], event.argument[1]);
          break;
        case "visibilityTableRow1":
          this.visibilityTableRow1(event.argument[0]);
          break;
        case "viewRuleUsers":
          this.viewRuleUsers(event.argument[0]);
          break;
        case "saveRule":
          // this.selectedUsers = event.argument[0];
          this.ruleData = event.argument[2];
          this.saveRule(event.argument[0], event.argument[1]);
          break;
        case "ruleTypeSelected":
          this.ruleData = event.argument[1];
          this.ruleTypeSelected(event.argument[0], null);
          break;
        case "prospTypeSelected":
          this.ruleData = event.argument[1];
          this.prospTypeSelected();
          break;
        case "removeRuleList":
          // this.selectedUsers = event.argument[0];
          this.ruleData = event.argument[1];
          this.removeRuleList(event.argument[0]);
          break;
        case "ruleFieldTypeSelected":
          // this.selectedUsers = event.argument[0];
          this.ruleData = event.argument[3];
          this.ruleFieldTypeSelected(event.argument[1], event.argument[2]);
          break;
        case "callRuleFieldType":
          // this.selectedUsers = event.argument[0];
          this.ruleData = event.argument[3];
          this.callRuleFieldType(
            event.argument[0],
            event.argument[1],
            event.argument[2]
          );
          break;
        case "onItemSelectRule":
          // this.selectedUsers = event.argument[0];
          this.ruleData = event.argument[3];
          this.onItemSelectRule(
            event.argument[0],
            event.argument[1],
            event.argument[2]
          );
          break;
        case "onSelectAllRule":
          // this.selectedUsers = event.argument[0];
          this.ruleData = event.argument[3];
          this.onSelectAllRule(
            event.argument[0],
            event.argument[1],
            event.argument[2]
          );
          break;
        case "OnItemDeSelectRule":
          // this.selectedUsers = event.argument[0];
          this.ruleData = event.argument[3];
          this.OnItemDeSelectRule(
            event.argument[0],
            event.argument[1],
            event.argument[2]
          );
          break;
        case "onDeSelectAllRule":
          // this.selectedUsers = event.argument[0];
          this.ruleData = event.argument[3];
          this.onDeSelectAllRule(
            event.argument[0],
            event.argument[1],
            event.argument[2]
          );
          break;
        case "addRuleList":
          // this.selectedUsers = event.argument[0];
          this.addRuleList();
          break;
        case "closePopup":
            // this.selectedUsers = event.argument[0];
            this.closeRuleModal();
            break;
        case "updateFormValuesRules":
              // this.selectedUsers = event.argument[0];
              // this.closeRuleModal();
              this.ruleData = event.argument[0];
              break;
        case "closeRuleEnrolPopup":
                // this.selectedUsers = event.argument[0];
                // this.closeRuleModal();
               this.closeRuleUsersModal();
                break;

        // Regulatory
        case "searchRuleEnrol":
          this.searchRuleEnrol(event.argument[0]);
          break;
        case "clearRuleEnrol":
          this.clearRuleEnrol();
          break;
        // case "addEditRule":
        //   this.openRuleModal(event.argument[0], event.argument[1]);
        //   break;
        case "regulatoryVisiblityChange":
          this.visibilityTableRow2(event.argument[0]);
          break;
        case "saveReg":
          this.saveReg(event.argument[0]);
          break;
        case "saveRegFilter":
            this.regularData = event.argument[2];
            this.saveRegFilter(event.argument[0],event.argument[1]);
            break;
        case "closeRegulatoryFilterModal":
          // this.selectedUsers = event.argument[0];
          this.closeRegulatoryFilterModal();
          break;
        case "removeRegFilter":
            // this.selectedUsers = event.argument[0];
            this.removeRegFilter(event.argument[0]);
            break;
        case "regFilterTypeSelected":
          this.regularData = event.argument[3];
          this.ruleTypeSelected(event.argument[0], null);
          break;
        case "callTypeRegFilter":
          this.regularData = event.argument[3];
          this.callTypeRegFilter(event.argument[0], event.argument[1],event.argument[2]);
          break;
        case "onItemSelectRegulatory":
            // this.selectedUsers = event.argument[0];
            this.regularData = event.argument[3];
            this.onItemSelectRule(
              event.argument[0],
              event.argument[1],
              event.argument[2]
            );
            break;
          case "onSelectAllRegulatory":
            // this.selectedUsers = event.argument[0];
            this.regularData = event.argument[3];
            this.onSelectAllRule(
              event.argument[0],
              event.argument[1],
              event.argument[2]
            );
            break;
          case "OnItemDeSelectRegulatory":
            // this.selectedUsers = event.argument[0];
            this.regularData = event.argument[3];
            this.OnItemDeSelectRule(
              event.argument[0],
              event.argument[1],
              event.argument[2]
            );
            break;
          case "onDeSelectAllRegulatory":
            // this.selectedUsers = event.argument[0];
            this.regularData = event.argument[3];
            this.onDeSelectAllRule(
              event.argument[0],
              event.argument[1],
              event.argument[2]
            );
            break;
          case "onSelectAllRegulatory":
              // this.selectedUsers = event.argument[0];
              this.regularData = event.argument[3];
              this.onSelectAllRule(
                event.argument[0],
                event.argument[1],
                event.argument[2]
              );
              break;
        case "openRegulatoryFilterModal":
                // this.selectedUsers = event.argument[0];
               this.openRegulatoryFilterModal();
                break;

        case "addRegulatoryFilter":
          // this.selectedUsers = event.argument[0];
          this.addRegulatoryFilter();
          break;
       case "updateFormValuesRegulatory":
            // this.selectedUsers = event.argument[0];
            // this.closeRuleModal();
            this.regularData = event.argument[0];
            break;
        // Self

        case "clearself":
          // this.selectedUsers = event.argument[0];
          this.clearself();
          break;
        case "searchSelfEnrol":
          // this.selectedUsers = event.argument[0];
          this.searchSelfEnrol(event.argument[0]);
          break;
        case "openSelfModal":
          this.openSelfModal();
          break;
        case "changeVisibilitySelf":
          this.visibilityTableRow3(event.argument[0]);
          break;
        case "onItemSelectSelf":
          // this.selectedUsers = event.argument[0];
          this.selfFieldsData = event.argument[3];
          this.onItemSelectSelf(
            event.argument[0],
            event.argument[1],
            event.argument[2],
          );
          break;
        case "OnItemDeSelectSelf":
          // this.selectedUsers = event.argument[0];
          this.selfFieldsData = event.argument[3];
          this.OnItemDeSelectSelf(
            event.argument[0],
            event.argument[1],
            event.argument[2]
          );
          break;
       case "onSelectAllSelf":
            // this.selectedUsers = event.argument[0];
            this.selfFieldsData = event.argument[3];
            this.onSelectAllSelf(
              event.argument[0],
              event.argument[1],
              event.argument[2]
            );
            break;
      case "onDeSelectAllSelf":
              // this.selectedUsers = event.argument[0];
              this.selfFieldsData = event.argument[3];
              this.onSelectAllSelf(
                event.argument[0],
                event.argument[1],
                event.argument[2]
              );
              break;
        case "saveSelfFields":
          // this.selectedUsers = event.argument[0];
          this.selfFieldsData = event.argument[0];
          this.saveSelfFields(event.argument[0], event.argument[1]);
          break;
        case "closeSelfModal":
            // this.selectedUsers = event.argument[0];
            this.closeSelfModal();
            break;
        case "selfFieldTypeSelected":
              // this.selectedUsers = event.argument[0];
              // this.selfFieldsData = event.argument[3];
              this.selfFieldTypeSelected(event.argument[0], event.argument[1],event.argument[2]);
              break;
        case "callTypeSelfFields":
                // this.selectedUsers = event.argument[0];
                this.selfFieldsData = event.argument[3];
                this.callTypeSelfFields(event.argument[0], event.argument[1],event.argument[2]);
                break;
        case "addSelfFields":
                  // this.selectedUsers = event.argument[0];
                  this.addSelfFields();
                  break;
        case 'removeSelfFields':
                this.removeSelfFields(event.argument[0], event.argument[1],event.argument[2]);
                break;
        case "updateFormValuesSelf":
                  // this.selectedUsers = event.argument[0];
                  // this.closeRuleModal();
                  this.selfFieldsData = event.argument[0];
                  break;

        // Price
        case 'createUpdatePrice': this.addEditPriceForm = event.argument[0];
                                  this.createUpdatePrice(event.argument[0]);
                                  break;
        case 'searchBar': this.searchOnPriceList(event.argument[0]);
                                  break;
        case 'bindValueToAddEditForm': this.bindValueToAddEditForm(event.argument[0],event.argument[1]);
                                      break;
        case 'clearSearch': this.clearSearch();
                                      break;
        case 'closeSidebar': this.closeSidebar();
                                      break;
      }
    }
  }


  getPriceList() {
    const getDiscountedList = webApi.domain + webApi.url.getPriceDetails;
    const param = {
      courseId : this.content.courseId,
    };
    this.spinner.show();
    this.commonFunctionsService
      .httpPostRequest(getDiscountedList, param)
      // this.serveyService.getServey(param)
      .then(
        (rescompData) => {
          this.spinner.hide();
          console.log('getDiscountedList ==>', rescompData);
          if(rescompData && rescompData['type'] && rescompData['data'] && rescompData['data'].length !== 0){
            this.makeTableDataReady(rescompData['data']);
          }
        },
        (error) => {
          this.spinner.hide();
          // this.listNotFound = true;
          this.toastr.warning('Something went wrong.', 'Warning');
        }
      );
  }

  makeTableDataReady(data){
    for (let i = 0; i < data.length; i++) {
      // this.rows[i].Date = new Date(this.rows[i].enrolDate);
      // this.rows[i].enrolDate = this.formdate(this.rows[i].Date);
      data[i]['number'] = i + 1;
      data[i]['edit'] = '';
    }
    this.pricingList = data;

    this.tempDisplayPriceList = [...this.pricingList];
    console.log('pricingList', this.pricingList);
    this.passDataToChild();
  }

  makeSelfTableDataReady(data){
    for (let i = 0; i < data.length; i++) {
      // this.rows[i].Date = new Date(this.rows[i].enrolDate);
      // this.rows[i].enrolDate = this.formdate(this.rows[i].Date);
      data[i]['number'] = i + 1;
      data[i]['edit'] = '';
    }
        this.enrollselfdata = data;
        this.rowsSelf = [...this.enrollselfdata];
    this.passDataToChild();
  }

  makeFormDataReady(action, data){
    if (action === 'add') {
      this.addEditPriceForm = {
        id: 0,
        name: '',
        currency: '',
        amount: '',
        validFrom: null,
        currentDate: new Date(),
        validTo: null,
        discount: [],
        isFree: 1,
      };
    } else {
      this.addEditPriceForm = {
        id: data.id,
        name: data.name,
        currency: data.currency,
        amount: data.amount,
        validFrom: data.startDate ? new Date(data.validFrom) : new Date(),
        currentDate: data.startDate ? new Date(data.currentDate) : new Date(),
        validTo: data.startDate ? new Date(data.validTo) : new Date(),
        discount: [],
        isFree: data.isFree,
      };
      for (let index = 0; index < this.discountListDropdownList.length; index++) {
        const element = this.discountListDropdownList[index];
        if(element.id === Number(data.discountIds)){
          this.addEditPriceForm.discount.push(element);
        }
      }
    }
    this.showPriceSidebar = true;
    this.passDataToChild();
  }
  createUpdatePrice(formData) {
    const addEditDiscountData = webApi.domain + webApi.url.addEditPriceData;
    this.spinner.show();
    let param = {};

    if(Number(this.addEditPriceForm.isFree) !== 1){
       param = {
        id: this.addEditPriceForm.id,
        courseId: this.content.courseId,
        name: this.addEditPriceForm.name,
        currency: this.addEditPriceForm.currency,
        amount: this.addEditPriceForm.amount,
        validFrom: this.commonFunctionsService.formatSendDate(this.addEditPriceForm.validFrom),
        currentDate: this.commonFunctionsService.formatSendDate(this.addEditPriceForm.currentDate),
        validTo: this.commonFunctionsService.formatSendDate(this.addEditPriceForm.validTo),
        // distStr: this.addEditPriceForm.discount[0].id,
        isFree: this.addEditPriceForm.isFree,
      };
      if(this.addEditPriceForm.discount.length != 0){
        param['distStr'] = this.addEditPriceForm.discount[0].id;
      }else {
        param['distStr'] = '';
      }
    }else {
       param = {
        id: this.addEditPriceForm.id,
        courseId: this.content.courseId,
        name: this.addEditPriceForm.name,
        currency: null,
        amount: null,
        validFrom: this.commonFunctionsService.formatSendDate(this.addEditPriceForm.validFrom),
        currentDate: this.commonFunctionsService.formatSendDate(this.addEditPriceForm.currentDate),
        validTo: this.commonFunctionsService.formatSendDate(this.addEditPriceForm.validTo),
        distStr: null,
        isFree: this.addEditPriceForm.isFree,
      };
    }

    // this.addEditPriceForm.discount.forEach()
    // this.spinner.show();
    this.commonFunctionsService
      .httpPostRequest(addEditDiscountData, param)
      // this.serveyService.getServey(param)
      .then(
        (rescompData) => {
          this.spinner.hide();

          console.log('addEditDiscountData ==>', rescompData);
          if(rescompData && rescompData['type'] === true){
            if(rescompData && rescompData['data']){
              this.showPriceSidebar = false;
              this.passDataToChild();
              this.toastr.success(rescompData['data'][0].msg , 'Success');
              this.getPriceList();
              // this.closeSidebar();
            }
          }else {
            this.spinner.hide();
            this.toastr.warning('Something went wrong.', 'Warning');
          }
        },
        (error) => {
          this.spinner.hide();
          this.toastr.warning('Something went wrong.', 'Warning');
        }
      );
  }

  bindValueToAddEditForm(action, data) {
    if(this.currencyTypeDropDown.length === 0){
      this.spinner.show();
      this.getDropDownList((value) => {
        if(value){
          this.makeFormDataReady(action, data);
         this.showPriceSidebar = true;
         this.passDataToChild();
          this.spinner.hide();
        }else {
          this.spinner.hide();
          this.toastr.warning('Unable to fetch discount type dropdown' , 'Warning');
        }
      });
    }else {
      this.makeFormDataReady(action, data);
      this.showPriceSidebar = true;
    }
  }
  getDropDownList(cb) {
    const getPriceDropdown = webApi.domain + webApi.url.getPriceDropdown;
    const param = {};
    // this.spinner.show();
    this.commonFunctionsService
      .httpPostRequest(getPriceDropdown, param)
      // this.serveyService.getServey(param)
      .then(
        (rescompData) => {
          // this.spinner.hide();
          console.log('getDiscountDropdown ==>', rescompData);
          if(rescompData && rescompData['type'] === true){
            this.fetchDiscountList();

            if(rescompData['data'] && rescompData['data'].length !== 0){
                this.currencyTypeDropDown = rescompData['data'];
                this.passDataToChild();
            }else {
              // this.toastr.warning('Discount Type DropDown is empty', 'Warning');
              cb(false);
            }
            cb(true);
          }else {
            // this.toastr.warning('Something went wrong.', 'Warning');
            cb(false);
          }
        },
        (error) => {
          cb(false);

          // this.spinner.hide();
          // this.toastr.warning('Something went wrong.', 'Warning');
        }
      );
  }
  fetchDiscountList() {
    const getDiscountedList = webApi.domain + webApi.url.getDiscountList;
    const param = {};
    // this.spinner.show();
    this.commonFunctionsService
      .httpPostRequest(getDiscountedList, param)
      // this.serveyService.getServey(param)
      .then(
        (rescompData) => {
          // this.spinner.hide();
          console.log('getDiscountedList ==>', rescompData);
          if(rescompData && rescompData['type'] && rescompData['data'] && rescompData['data'].length !== 0){
            // this.makeTableDataReady(rescompData['data']);
            this.discountListDropdownList = rescompData['data'];

          }else {
            // this.discountedListNotFound = true;
          }
          this.passDataToChild();
        },
        (error) => {
          this.spinner.hide();
          // this.discountedListNotFound = true;
          this.toastr.warning('Something went wrong.', 'Warning');
        }
      );
  }
  // searchOnPriceList(event){
  //   console.log(event);
  //   const val = event.target.value.toLowerCase();
  //     if (val.length >= 3 || val.length == 0) {
  //       this.tempDisplayPriceList = [...this.pricingList];
  //       const temp = this.tempDisplayPriceList.filter(function (d) {
  //         return d.currency.toLowerCase().indexOf(val) !== -1 ||
  //           d.name.toLowerCase().indexOf(val) !== -1 ||
  //           d.amount.toLowerCase().indexOf(val) !== -1 ||
  //           !val;
  //       });
  //       this.tempDisplayPriceList = [...temp];
  //     }
  // }

  searchOnPriceList(event) {
    const val = event.target.value.toLowerCase();
    this.tempDisplayPriceList = [];
    let keys = [];
    // filter our data
    if (this.pricingList.length > 0) {
      keys = Object.keys(this.pricingList[0]);
    }
    if(val.length>=3||val.length==0){
    const temp = this.pricingList.filter((d) => {
      // keys.forEach(element => {

      // });
      for (const key of keys) {
        if (String(d[key]) && String(d[key]).toLowerCase().indexOf(val) !== -1) {
          return true;
        }
      }
    });

    // update the rows
    // this.data = temp;
    this.tempDisplayPriceList = [...temp];
    this.passDataToChild();
  }

  }


  clearSearch(){
    this.tempDisplayPriceList =  [...this.pricingList];
    this.passDataToChild();
  }

  closeSidebar(){
    this.showPriceSidebar = false;
  }
}
