import { Host, ChangeDetectorRef, Component, ViewEncapsulation, Directive, forwardRef, Attribute, OnChanges, SimpleChanges, Input, ViewChild, ViewContainerRef, OnInit } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AddEditCourseContent } from '../addEditCourseContent';
import { AddEditCourseContentService } from '../addEditCourseContent.service';
import { BadgesService } from '../../../../gamification/badges/badges.service';
import { rewardsService } from './rewards.service';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { CertificateService } from '../../../../gamification/certificate/certificate.service';
import { Router, NavigationStart, ActivatedRoute } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { PassService } from '../../../../../service/passService';
import { EventEmitterService } from '../../../../../service/event-emitter.service';
import { noData } from '../../../../../models/no-data.model';
import { isDefaultChangeDetectionStrategy } from '@angular/core/src/change_detection/constants';
@Component({
  selector: 'course-rewards',
  templateUrl: './rewards.html',
  styleUrls: ['./rewards.scss'],
  encapsulation: ViewEncapsulation.None
})

export class rewardsComponent  implements OnInit {

  @ViewChild('myTable') table: any;
  @ViewChild(DatatableComponent) tableData: DatatableComponent;
  @Input() inpdata: any;

  selected: any = [];

  temp = [];
  btnName: string = 'Select';
  badgeTitle:string='Select Badge'
  certTitle:string='Select Template'
  condataBadge: any;
  condataCert: any;
  badge: boolean = true;
  certificate: boolean = true;

  showEnrolpage: boolean = false;
  badgeshow: boolean = false;
  certshow: boolean = false;

  rewardsBadge: any = {
    id: '',
    conid: '',
    imgsrc: '',
    name: '',
    descri: ''

  };
  rewardsCert: any = {
    id: '',
    conid: '',
    imgsrc: '',
    name: '',
    descri: ''

  };
  badgeData: any = [];
  certData: any = [];
  search: any = {};
  searchcer: any = {};


  errorMsg: any;
  userId: any;
  optId: any;
  courseId: any;
  tenantId: any;
  first: any = {
    badge: false,
    certificate: false
  }
  badgeenable: boolean = false;
  certenable: boolean = false;
  searchText: any=String;
  tempReg: any[];
  response: any;
  responseCert: any=[];
  nodata: boolean;
  noDataVal:noData={
    margin:'mt-5',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"Sorry we couldn't find any matches please try again",
    desc:".",
    titleShow:true,
    btnShow:false,
    descShow:false,
    btnText:'Learn More',
    btnLink:''
}
  enableBtn: boolean=false;
  constructor(@Host() private parent_Comp: AddEditCourseContent, private rewardservice: rewardsService,
  protected courseDataService: AddEditCourseContentService,
    private spinner: NgxSpinnerService,
    // private toasterService: ToasterService,
     private toastr: ToastrService,
    private addEditCourseService: AddEditCourseContentService,
    private passService :PassService,
    private eventEmitterService: EventEmitterService, public cdf: ChangeDetectorRef,
    private badgeservice: BadgesService, private certificateservice: CertificateService, private router: Router) {

    if (this.addEditCourseService.data != undefined) {
      console.log('this.addEditCourseService.data for Edit:', this.addEditCourseService.data);
      this.ChooseAddEdit(this.addEditCourseService.data);
    }


    if (localStorage.getItem('LoginResData')) {
      var userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', userData.data);
      this.userId = userData.data.data.id;
      console.log('userId', userData.data.data.id);
      this.tenantId = userData.data.data.tenantId;
    }

    this.fetchBadges();
    this.fetchCertificates();
  }

  ngOnInit() {
    this.spinner.show();
    // this.passService.crossCommunication.subscribe(
    //   () => {
    //     this.saveRewardsForCoures();
    //   });
    // if(this.eventEmitterService.varib === 'rewards') {
      // if (this.eventEmitterService.subsVar === undefined) {
      //   this.eventEmitterService.subsVar = this.eventEmitterService.invokeComponentFunction.subscribe((res: any) => {
      //     if(res === 'rewards') {
      //     this.saveRewardsForCoures();
      //     } else {
      //       this.eventEmitterService.onComponentClick(res);
      //       }
      //   });
      // }
    // }
  }



  ngAfterViewChecked() {

  }

  ngOnChanges(changes: SimpleChanges): void {
    if(this.inpdata === 'rewards') {
      this.saveRewardsForCoures();
    }
  }

  clear() {
    if(this.searchText.length>=3){
    this.search = {};
    this.badgeData=this.response;
    this.searchText='';
    this.nodata=false
    }else{
      this.search={};
    }

  }
  clearcer() {
    if(this.searchText.length>=3){
      this.searchcer = {};
      this.certData=this.responseCert;
      this.searchText='';
      this.nodata=false
      }else{
        this.searchcer = {};
      }
  }

  ChooseAddEdit(data) {
    this.optId = data.id;

    if (this.optId == 0) {
      this.courseId = this.addEditCourseService.courseId;
    } else if (this.optId == 1) {
      this.courseId = this.addEditCourseService.data.data.courseId;
      this.tenantId = this.addEditCourseService.data.data.tenantId;
      this.getExistingCourseRewards();
    }
  }
  fetchBadges() {
    this.spinner.show();
    this.response=[];
    let param = {
      "tenantId": this.tenantId,
      "courseId": 0,
      "catId": 0
    }
    this.badgeservice.getBadges(param)
      .then(rescompData => {
        var result = rescompData;
        if (result['type'] == true) {
          this.badgeData = rescompData['data'][0];
          this.response=this.badgeData;
    this.spinner.hide();
          console.log('this.badgeDataRewards', this.badgeData);
          if (this.first.certificate) {
            this.cdf.detectChanges();
            this.first = {
              badge: false,
              certificate: false
            }
          } else {
            this.first.badge = true;
          }

        } else {
          this.errorMsg = rescompData;
        }

      },
        resUserError => {
          // this.loader =false;
          this.errorMsg = resUserError;
        });
  }

  fetchCertificates() {
    this.spinner.show();
    this.responseCert=[]
    let param = {
      "tenantId": this.tenantId,
      "courseId": 0
    }
    this.certificateservice.getCertificates(param)
      .then(rescompData => {
        var result = rescompData;
        if (result['type'] == true) {
          this.certData = rescompData['data'][0];
    this.spinner.hide();
          this.responseCert=this.certData;
          console.log(' this.certDataReward', this.certData);
          if (this.first.badge) {
            this.cdf.detectChanges();
            this.first = {
              badge: false,
              certificate: false
            }
          } else {
            this.first.certificate = true;
          }
        } else {
          this.errorMsg = rescompData;
        }

      },
        resUserError => {
          // this.loader =false;
          this.errorMsg = resUserError;
        });

  }

  searchBadge(event) {
    const val = event.target.value.toLowerCase();
    // this.allregulatorylist(this.addEditCourseService.data.data);
    this.searchText=val
    // filter our data
    if(val.length>=3||val.length==0){
    this.nodata=false;
    const temp = this.response.filter(function (d) {
      return d.badgeName.toLowerCase().indexOf(val) !== -1 ||
        // String(d.dueDays).toLowerCase() === val ||
        // String(d.reminder).toLowerCase().indexOf(val) !== -1 ||
        !val;
    });
    if(temp.length==0){
      this.nodata=true;
    }
    // update the rows
    this.badgeData = temp;
  }
    // Whenever the filter changes, always go back to the first page
    // this.tableDataReg.offset = 0;
  }
  searchCerti(event) {
    const val = event.target.value.toLowerCase();
    // this.allregulatorylist(this.addEditCourseService.data.data);
    this.searchText=val
    // filter our data
    if(val.length>=3||val.length==0){
    this.nodata=false;
    const temp = this.responseCert.filter(function (d) {
      return d.certName.toLowerCase().indexOf(val) !== -1 ||
        // String(d.dueDays).toLowerCase() === val ||
        // String(d.reminder).toLowerCase().indexOf(val) !== -1 ||
        !val;
    });

  if(temp.length==0){
    this.nodata=true;
  }
    // update the rows
    this.certData = temp;
  }
    // Whenever the filter changes, always go back to the first page
    // this.tableDataReg.offset = 0;
  }
  getExistingCourseRewards() {
    let param = {
      "cId": this.courseId,
      "tId": this.tenantId
    }

    this.rewardservice.getcourserewards(param)
      .then(rescompData => {
        console.log('Existing Course Rewards:', rescompData);
        var result = rescompData;
        var reward = result['data'][0];
        var badges = []
        var certificates = []

        reward.forEach(element => {
          if (element.reward == "badge") {
            badges.push(element)
            this.condataBadge = {
              badgeId: element.rewId,
              badgeName: element.rewName,
              bdescription: element.rewDesc,
              imgsrc: element.imgsrc
            }
            this.rewardsBadge = {
              conid: element.rewId,
              id: element.rewId,
              name: element.rewName,
              descri: element.rewDesc,
              imgsrc: element.imgsrc
            }
            this.badgeenable = true

          } else {
            certificates.push(element)
            this.rewardsCert = {
              conid: element.rewId,
              id: element.rewId,
              name: element.rewName,
              descri: element.rewDesc,
              imgsrc: element.imgsrc
            }
            this.condataCert = {
              certId: element.rewId,
              certName: element.rewName,
              cdescription: element.rewDesc,
              imgsrc: element.imgsrc
            }
            this.certenable = true
          }
        });

        this.badge = badges.length > 0 ? false : true;
        this.certificate = certificates.length > 0 ? false : true;

      },
        resUserError => {
          this.errorMsg = resUserError
        });

  }





  browse(rid) {
    if (rid == 1) {
      this.badgeshow = true;
    }
    if (rid == 2) {
      this.certshow = true;
    }
  }
  closeModel(rid) {
    if (rid == 1) {
      this.badgeshow = false;
      this.enableBtn =  false;
      this.condataBadge = {};
    }
    if (rid == 2) {
      this.certshow = false;
      this.enableBtn =  false;
      this.condataCert = {};
    }
  }

  saveBadge(rid) {
    this.badgeenable = true
    this.optId=0;
    this.rewardsBadge = {
      id: rid,
      conid: this.condataBadge.badgeId,
      imgsrc: this.condataBadge.badgeIcon,
      name: this.condataBadge.badgeName,
      descri: this.condataBadge.description
    }
    this.badgeshow = false;
    this.badge = false;
  }

  saveCert(rid) {
    this.certenable = true
    this.optId=0;
    this.rewardsCert = {
      id: rid,
      conid: this.condataCert.id,
      imgsrc: this.condataCert.certIcon,
      name: this.condataCert.certName,
      descri: this.condataCert.description
    }
    //this.certificate =true;
    this.certshow = false;
    this.certificate = false;
  }


  activeSelectedBadgeId: any;
  setActiveSelectedBadge(currentIndex, currentBadge) {
    this.optId=0;
    console.log('currentBadgeOld:', currentBadge);

    this.condataBadge = {};
    this.activeSelectedBadgeId = currentBadge.badgeId;
    this.condataBadge = currentBadge;
    console.log('currentBadgeNew:', this.condataBadge);
    if(this.condataBadge){
      this.enableBtn=true
      }
  }

  activeSelectedCertId: any;
  setActiveSelectedCert(currentIndex, currentCert) {
    this.optId=0;
    console.log('currentCertOld:', currentCert)
    this.condataCert = {};
    this.activeSelectedCertId = currentCert.id;
    this.condataCert = currentCert;
    if(this.condataCert){
      this.enableBtn=true
      }
    console.log('currentCertNew:', this.condataCert)
  }

  removeBadge() {
    this.badgeenable = false
    this.optId=1;
    this.rewardsBadge = {
      id: '',
      conid: '',
      imgsrc: '',
      name: '',
      descri: ''
    };
    this.badge = true;
  }

  removeCert() {
    this.certenable = false
    this.optId=1;
    this.rewardsCert = {
      id: '',
    conid: '',
    imgsrc: '',
    name: '',
    descri: ''
    };
    this.certificate = true;
  }

  formattedrewards: any;
  // old function
//   saveRewardsForCoures() {
//     if(!this.badgeenable && !this.certenable){
//       this.toastr.warning('Please select badge or certificate','Warning')

//     }
//     else if( this.rewardsBadge.id == '' && this.rewardsCert.id == ''){
//       this.toastr.warning('Please select badge or certificate','Warning')
//     }
//       else{

//     //this.getdatareadyforcourse(this.rewardsCert.conid,this.rewardsBadge.conid);
//     this.spinner.show();
//     let param = {
//       "cId": this.courseId,
//       "tId": this.tenantId,
//       "userId": this.userId,
//       "badgeId": this.badge ? 0 : this.rewardsBadge.conid,
//       "certId": this.certificate ? 0 : this.rewardsCert.conid
//     }
//     this.rewardservice.insertreward(param)
//       .then(rescompData => {

//         var result = rescompData;
//         console.log('insertrewardresult', rescompData);
//         this.spinner.hide();
//         if (this.optId == 0) {
//           if (result['type'] == true) {
//             // var toast: Toast = {
//             //   type: 'success',
//             //   //title: "Server Error!",
//             //   body: "Reward added to course.",
//             //   showCloseButton: true,
//             //   timeout: 2000
//             // };
//             // this.toasterService.pop(toast);

//             this.toastr.success('Reward Added Successfully', 'Success', {
//               closeButton: false
//             });
//             this.optId=1;
//             //new change
//             if(this.courseDataService.breadcrumbArray){
//               var length = this.courseDataService.breadcrumbArray.length-1
//               this.courseDataService.breadtitle = this.courseDataService.breadcrumbArray[length].name
//               // this.addassetservice.title = this.title
//               this.courseDataService.breadcrumbArray = this.courseDataService.breadcrumbArray.slice(0,length)
//               // this.title = this.addassetservice.breadCrumbArray[]
//               this.courseDataService.previousBreadCrumb = this.courseDataService.previousBreadCrumb.slice(0,length+1)

//             }
//             //end new change
//             this.router.navigate(["/pages/plan/courses/category"]);
//           } else {

//             // var toast: Toast = {
//             //   type: 'error',
//             //   body: "Unable to add reward.",
//             //   showCloseButton: true,
//             //   timeout: 2000
//             // };
//             // this.toasterService.pop(toast);

//             this.toastr.error('Please contact site administrator and <div (click)="giveFeedback()"> click here <div> to leave your feedback', 'Error', {
//               timeOut: 0,
//               closeButton: true
//             });
//           }
//         } else if (this.optId == 1) {
//           if (result['type'] == true) {
//             // var toast: Toast = {
//             //   type: 'success',
//             //   //title: "Server Error!",
//             //   body: "Reward updated to course.",
//             //   showCloseButton: true,
//             //   timeout: 2000
//             // };
//             // this.toasterService.pop(toast);

//             this.toastr.success('Reward Updated Successfully ', 'Success', {
//               closeButton: false
//             });
//             //new change
//             if(this.courseDataService.breadcrumbArray){
//               var length = this.courseDataService.breadcrumbArray.length-1
//               this.courseDataService.breadtitle = this.courseDataService.breadcrumbArray[length].name
//               // this.addassetservice.title = this.title
//               this.courseDataService.breadcrumbArray = this.courseDataService.breadcrumbArray.slice(0,length)
//               // this.title = this.addassetservice.breadCrumbArray[]
//               this.courseDataService.previousBreadCrumb = this.courseDataService.previousBreadCrumb.slice(0,length+1)

//             }
//             //end new change

//             this.router.navigate(["/pages/plan/courses/category"]);
//           } else {

//             // var toast: Toast = {
//             //   type: 'error',
//             //   body: "Unable to update reward.",
//             //   showCloseButton: true,
//             //   timeout: 2000
//             // };
//             // this.toasterService.pop(toast);
//             this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
//               timeOut: 0,
//               closeButton: true
//             });
//           }
//         }


//       }, error => {
//         this.spinner.hide();
//         console.log('resulterror', error);

//         // var toast: Toast = {
//         //   type: 'error',
//         //   body: "Something went wrong.please try again later.",
//         //   showCloseButton: true,
//         //   timeout: 2000
//         // };
//         // this.toasterService.pop(toast);

//         this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
//           timeOut: 0,
//           closeButton: true
//         });
//       });
//     // this.passService.crossCommunication.unsubscribe();
//     this.eventEmitterService.subsVar = undefined;
//     // this.eventEmitterService.invokeComponentFunction.unsubscribe();
//   }
// }
saveRewardsForCoures() {

  // Old Code

  // if(!this.badgeenable && !this.certenable){
  //   this.toastr.warning('Please select badge or certificate','Warning')

  // }
  // else if( this.rewardsBadge.id == '' && this.rewardsCert.id == ''){
  //   this.toastr.warning('Please select badge or certificate','Warning')
  // }
  //   else{

  //this.getdatareadyforcourse(this.rewardsCert.conid,this.rewardsBadge.conid);

  // Validation changes
  if(this.rewardsBadge.conid || this.rewardsCert.conid){
  this.spinner.show();
  let param = {
    "cId": this.courseId,
    "tId": this.tenantId,
    "userId": this.userId,
    "badgeId": this.badge ? 0 : this.rewardsBadge.conid,
    "certId": this.certificate ? 0 : this.rewardsCert.conid
  }
  this.rewardservice.insertreward(param)
    .then(rescompData => {

      var result = rescompData;
      console.log('insertrewardresult', rescompData);
      this.spinner.hide();
      if (this.optId == 0) {
        if (result['type'] == true) {
          // var toast: Toast = {
          //   type: 'success',
          //   //title: "Server Error!",
          //   body: "Reward added to course.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);

          this.toastr.success('Reward Added Successfully', 'Success', {
            closeButton: false
          });
          this.optId=1;
          //new change
          if(this.courseDataService.breadcrumbArray){
            var length = this.courseDataService.breadcrumbArray.length-1
            this.courseDataService.breadtitle = this.courseDataService.breadcrumbArray[length].name
            // this.addassetservice.title = this.title
            this.courseDataService.breadcrumbArray = this.courseDataService.breadcrumbArray.slice(0,length)
            // this.title = this.addassetservice.breadCrumbArray[]
            this.courseDataService.previousBreadCrumb = this.courseDataService.previousBreadCrumb.slice(0,length+1)

          }
          //end new change
          // this.router.navigate(["/pages/plan/courses/category"]);
        } else {

          // var toast: Toast = {
          //   type: 'error',
          //   body: "Unable to add reward.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);

          this.toastr.error('Please contact site administrator and <div (click)="giveFeedback()"> click here <div> to leave your feedback', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        }
      } else if (this.optId == 1) {
        if (result['type'] == true) {
          // var toast: Toast = {
          //   type: 'success',
          //   //title: "Server Error!",
          //   body: "Reward updated to course.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);

          this.toastr.success('Reward Updated Successfully ', 'Success', {
            closeButton: false
          });
          //new change
          if(this.courseDataService.breadcrumbArray){
            var length = this.courseDataService.breadcrumbArray.length-1
            this.courseDataService.breadtitle = this.courseDataService.breadcrumbArray[length].name
            // this.addassetservice.title = this.title
            this.courseDataService.breadcrumbArray = this.courseDataService.breadcrumbArray.slice(0,length)
            // this.title = this.addassetservice.breadCrumbArray[]
            this.courseDataService.previousBreadCrumb = this.courseDataService.previousBreadCrumb.slice(0,length+1)

          }
          //end new change

          this.router.navigate(["/pages/plan/courses/category"]);
        } else {

          // var toast: Toast = {
          //   type: 'error',
          //   body: "Unable to update reward.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        }
      }


    }, error => {
      this.spinner.hide();
      console.log('resulterror', error);

      // var toast: Toast = {
      //   type: 'error',
      //   body: "Something went wrong.please try again later.",
      //   showCloseButton: true,
      //   timeout: 2000
      // };
      // this.toasterService.pop(toast);

      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    });
  // this.passService.crossCommunication.unsubscribe();
  this.eventEmitterService.subsVar = undefined;
  // this.eventEmitterService.invokeComponentFunction.unsubscribe();
}else {
  this.toastr.warning('Please select badge or certificate','Warning')
}
}
  getdatareadyforcourse(certId, badgeId) {
    if (certId == null || certId == undefined) {
      this.formattedrewards = badgeId + "|" + null;
    } else if (badgeId == null || badgeId == undefined) {
      this.formattedrewards = null + "|" + certId;
    } else {
      this.formattedrewards = badgeId + "|" + certId;
    }
    console.log('this.formattedrewards', this.formattedrewards);
  }
}
