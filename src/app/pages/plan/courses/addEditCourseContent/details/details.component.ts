import {
  Injectable,
  HostListener,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation,
  Directive,
  forwardRef,
  Attribute,
  OnChanges,
  SimpleChanges,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ViewContainerRef,
  ElementRef,
  OnInit
} from '@angular/core';
import {
  FormGroup,
  FormArray,
  FormBuilder,
  FormControl,
} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { DatePipe } from '@angular/common';
import { ToasterService } from 'angular2-toaster';
import { ToastrService } from 'ngx-toastr';
import { detailsService } from './details.service';
import { AddEditCourseContentService } from '../addEditCourseContent.service';
import { AddEditCourseContent } from '../addEditCourseContent';
import { BsDatepickerDirective } from 'ngx-bootstrap/datepicker';
import { webAPIService } from '../../../../../service/webAPIService';
import { webApi } from '../../../../../service/webApi';
import { NgxSpinnerService } from 'ngx-spinner';
import { enrolService } from '../enrolment/enrolment.service';
import { HttpClient } from '@angular/common/http';
// import { AppService } from '../../../../../app.service';
import { CommonFunctionsService } from '../../../../../service/common-functions.service';
import { PassService } from '../../../../../service/passService';
import { EventEmitterService } from '../../../../../service/event-emitter.service';
import { ModuleAccessService } from '../../../../../service/module-access.service';
import { BrandDetailsService } from '../../../../../service/brand-details.service';
import { SuubHeader } from '../../../../components/models/subheader.model';
// import { HttpErrorResponse } from '@angular/common/https';

@Component({
  selector: 'course-details',
  templateUrl: './details.html',
  styleUrls: ['./details.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [DatePipe]
})
//@Injectable()
export class detailsComponent implements OnInit {
  @ViewChild(BsDatepickerDirective) datepicker: BsDatepickerDirective;

  @ViewChild('enrolledUsersTable') table: any;
  @ViewChild('fileUpload') fileUpload: any;
  @ViewChild('inputtext') inputtext: any;

  @ViewChild('courseForm') coursefrm: any;
  @ViewChild('PForm') pfrm: any;

  @Input() inpdata: any;

  @Output() savennext = new EventEmitter<any>();


  tenantId: any;

  showEnrolpage: boolean = false;

  settings = {
    columns: {
      id: {
        title: 'ID'
      },
      name: {
        title: 'Full Name'
      },
      username: {
        title: 'User Name'
      },
      email: {
        title: 'Email'
      }
    }
  };

  enrolData: any[] = [];
  expanded: any = {};
  timeout: any;
  query: string = '';
  public getData;
  chosenValue: any;
  public data: Object[];
  public today = new Date();
  public selectedStartDate = new Date();
  private placeHolder: string = 'Select a date';

  public myDatePickerOptions: INgxMyDpOptions = {
    // other options...
    dateFormat: 'yyyy-mm-dd',
    disableUntil: {
      year: this.today.getFullYear(),
      month: this.today.getMonth() + 1,
      day: this.today.getDate()
    }
  };

  public expectedDateOptions: INgxMyDpOptions = {
    dateFormat: 'yyyy-mm-dd',
    disableUntil: {
      year: this.today.getFullYear(),
      month: this.today.getMonth() + 1,
      day: this.today.getDate()
    }
  };

  public myDatePickerOptions1: INgxMyDpOptions;

  // Initialized to specific date (09.10.2018).
  public model: any = { date: { year: 2018, month: 4, day: 9 } };
  private startDatePlaceHolder: string = 'Start Date';
  private endDatePlaceHolder: string = 'End date';

  private endDate: INgxMyDpOptions = {
    // other end date options here...
    //date:Date=new Date();
    dateFormat: 'yyyy-mm-dd',
    disableSince: { year: 0, month: 0, day: 0 }
  };

  // optional date changed callback
  private startDate: INgxMyDpOptions = {
    // start date options here...
    dateFormat: 'yyyy-mm-dd',
    //selectionTxtFontSize :'12px',
    disableSince: { year: 0, month: 0, day: 0 }
  };
  date = new Date();
  errorMsg: string;
  loader: any;

  strArrayPar: any = [
    {
      pId: 1,
      pName: 'Activity Completion'
    },
    {
      pId: 2,
      pName: 'User Profile Fields'
    },
    {
      pId: 3,
      pName: 'Grades'
    },
    {
      pId: 2,
      pName: 'Date From'
    },
    {
      pId: 3,
      pName: 'Date Until'
    }
  ];

  strArrayTypePar: any = [];

  strArrayType: any = [[]];

  formdataRules: any = {
    parId: '',
    parName: '',
    pDBName: '',
    toDt: '',
    fromDt: '',
    Value1: '',
    Value2: '',
    ptypeId: '',
    ptypeName: ''
  };
  pageload: boolean;
  qtd: any[] = [];
  controlList: any = [];
  controlFlag: any = false;
  public myForm: FormGroup;

  users: any = [];
  filters: any = [];
  report_id: any = {
    id: 1
  };

  topic: any = [];

  topicRes1: any = [
    {
      id: '',
      course: '',
      section: '',
      name: '',
      summary: '',
      summaryformat: '',
      sequence: '',
      visible: '',
      availability: ''
    }
  ];

  resourse: any = [];

  title: any;

  profileUrl: any = 'assets/images/edgeicon.jpg';

  isActive = false;
  isActiveRes = false;
  isActiveTop = false;

  removeRes: any = false;
  removeTop: any = false;
  resOfTopic: any = [];

  interval: any;

  fileUrl: any;
  fileName: any ;
  fileIcon: any = 'assets/img/app/profile/avatar4.png';
  enableUpload: any = false;
  startdateSelected: boolean = false;
  showResource: any = false;
  formdataTopic = {
    tid: '',
    tName: '',
    tTags: '',
    tSummary: ''
  };

  formdataRes = {
    tid: '',
    rid: '',
    rType: '',
    rName: '',
    rTags: '',
    rSummary: ''
  };

  parentcat: any = [
    {
      id: '',
      name: ''
    }
  ];

  resType: any = [
    {
      rTypeId: 1,
      rTypeName: 'File'
    },
    {
      rTypeId: 2,
      rTypeName: 'Other'
    }
  ];

  compTrackType: any = [
    {
      cTypeId: 1,
      cTypeName: 'Do not indicate activity completion'
    },
    {
      cTypeId: 2,
      cTypeName: 'Students can manually mark the activity as completed'
    },
    {
      cTypeId: 3,
      cTypeName: 'Show activity as complete when conditions are met'
    }
  ];

  enableCompTrack: any = [
    {
      ecTypeId: 1,
      ecTypeName: 'Yes'
    },
    {
      ecTypeId: 2,
      ecTypeName: 'No'
    }
  ];

  courseLayout: any = [
    {
      clTypeId: 1,
      clTypeName: 'Show all sections on one page'
    },
    {
      clTypeId: 2,
      clTypeName: 'Show one section per page'
    }
  ];

  hiddenSection: any = [
    {
      hTypeId: 1,
      hTypeName: 'Hidden sections are always shows in collapsed form'
    },
    {
      hTypeId: 2,
      hTypeName: 'Hidden sections are completely invisible'
    }
  ];
  header: SuubHeader  = {
    title:'',
    btnsSearch: true,
    searchBar: false,
    dropdownlabel: ' ',
    placeHolder:'',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: '',
    btnName3: '',
    btnAdd: '',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: true,
    filter: false,
    showBreadcrumb: true,
    breadCrumbList:[]
  };
  courseReviewCheck: any = {
    value: false
  };

  reviewCheck: any = {
    value1: false,
    value2: false,
    value3: false
  };

  categoryData: any;
  ModulesListData: any;
  resourceData: any;
  secResources: any;
  formattedStartDate: any;
  formattedEndDate: any;
  sha1hash: any;
  sha1hashHex: any;
  uploadFileData: any;
  getCourseId: any;
  cData: any;

  result: any;
  topicResult: any;
  addTopicResult: any;
  NewTopicData: any = null;
  tempNewTopData: any = null;

  formdataTopic1: any = {
    id: '',
    course: '',
    section: '',
    name: '',
    summary: '',
    summaryformat: '',
    sequence: '',
    visible: '',
    availability: ''
  };

  conData: any;

  dropdownListUsers: any;
  selectedItemsUsers: any;
  dropdownSettingsUsers: any;
  demoData: any = [];
  saveCourseID: any;
  // sorting
  key: string; //set default
  reverse: boolean;
  private ValueId: number = 0;
  selectedFilterOption = [];
  search2: any = {};
  formattedPara: any;
  filterData: any = {
    userProfFields: null,
    gradeGreatEqual: null,
    gradeLess: null,
    compTrack: null,
    dateUntil: null,
    dateFrom: null,
    remarks: null
  };
  fileReaded: any;
  lines = [];
  uploadedData: any;
  exceltojson: any;
  resultSheets: any;
  newValueRes: any;
  oldValueRes: any;
  elementRefRes: any;
  showRestrict: any = false;
  showTags: any = false;
  showRestrictTop: any = false;
  showTagsTop: any = false;
  tab1: any = true;
  tab2: any = false;
  tab3: any = false;
  tab4: any = false;
  tab5: any = false;
  topicRes: any = {};
  tempNewResData: any = null;
  tempTopData: any = null;
  tempResData: any = null;
  newValueTop: any;
  oldValueTop: any;
  elementRefTop: any;
  tabLoadTimes: Date[] = [];
  enrolment: any = {
    manual: '',
    rule: '',
    regulatory: '',
    self: ''
  };

  months: any = [
    { id: 1, name: 1 },
    { id: 2, name: 2 },
    { id: 3, name: 3 },
    { id: 4, name: 4 },
    { id: 5, name: 5 },
    { id: 6, name: 6 },
    { id: 7, name: 7 },
    { id: 8, name: 8 },
    { id: 9, name: 9 },
    { id: 10, name: 10 },
    { id: 11, name: 11 },
    { id: 12, name: 12 }
  ];

  days: any = [
    { id: 1, name: 1 },
    { id: 2, name: 2 },
    { id: 3, name: 3 },
    { id: 4, name: 4 },
    { id: 5, name: 5 },
    { id: 6, name: 6 },
    { id: 7, name: 7 },
    { id: 8, name: 8 },
    { id: 9, name: 9 },
    { id: 10, name: 10 },
    { id: 11, name: 11 },
    { id: 12, name: 12 },
    { id: 13, name: 13 },
    { id: 14, name: 14 },
    { id: 15, name: 15 },
    { id: 16, name: 16 },
    { id: 17, name: 17 },
    { id: 18, name: 18 },
    { id: 19, name: 19 },
    { id: 20, name: 20 },
    { id: 21, name: 21 },
    { id: 22, name: 22 },
    { id: 23, name: 23 },
    { id: 24, name: 24 },
    { id: 25, name: 25 },
    { id: 26, name: 26 },
    { id: 27, name: 27 },
    { id: 28, name: 28 },
    { id: 29, name: 29 },
    { id: 30, name: 30 }
  ];

  daysBucket: any = [];
  visibility: any = [];
  courseFormat: any = [];
  courseLevel: any = [];
  creatorList: any = [];
  userRoles: any = [];

  usersList: any = [];
  tempUsers: any = [];
  selectedCreator = [];
  settingsCreatorDrop = {};
  settingsTagDrop = {};
  tempTags: any = [];
  selectedTags: any = [];
  tagList: any = [];

  items = ['Javascript', 'Typescript'];
  // tags = ['@item'];
  viewbindflag: boolean = false;
  // courseIconUrl : any = 'assets/images/course1.jpg';
  courseIconUrl: any = 'assets/images/courseicon.jpg';
  credits: any = [
    {
      creditAllocId: 0,
      roleId: 8,
      creditTypeId: 1,
      creditTypeValueac: '',
      creditTypeValuebc: ''
    }
  ];

  marksType: any = [];

  formdata: any = {
    categoryId: '',
    courseCode: '',
    courseCompletion: '',
    courseId: '',
    courseLevelId: '1',
    courseOrder: '',
    coursePicRef: this.courseIconUrl,
    courseTypeId: '',
    creatorId: '',
    validToDate: '',
    fullname: '',
    leadTime: '',
    shortname: '',
    validFromDate: '',
    summary: '',
    tags: '',
    tenantId: this.tenantId,
    validationFreq: '',
    visible: '',
    craditpoints: this.credits
    // managerCreditPoints: '',
    // managerCreditPointsId: '',
    // learnerCreditPoints: '',
    // learnerCreditPointsId: ''
  };
  credit: any;
  tags: any;
  contentDetails:any
  valid: boolean = false;
  page: string;
  show: boolean = false;
  currentBrandData: any;

  @HostListener('window:scroll')
  onScrollEvent() {
    this.datepicker.hide();
  }

  public pointsForm: FormGroup;
  userId: any;
  selftypefield: any = [];
  shortUrl: any;
  showurl: boolean = false;
  dataReady: boolean = true;
  constructor(
    private spinner: NgxSpinnerService,
    protected webApiService: webAPIService,
    private service: AddEditCourseContentService,
    private toasterService: ToasterService,
    private toastr: ToastrService,
    private datePipe: DatePipe,
    private _fb: FormBuilder,
    vcr: ViewContainerRef,
    protected detailsService: detailsService,
    private router: Router,
    private route: ActivatedRoute,
    private modalService: NgbModal,
    public cdf: ChangeDetectorRef,
    private enrolService: enrolService,
    private http1: HttpClient,
    private AddEditCourseContent: AddEditCourseContent,
    private commonFunctionService: CommonFunctionsService,
    private passService :PassService,
    private eventEmitterService: EventEmitterService,
    private  moduleAccessService: ModuleAccessService,
    public brandService: BrandDetailsService,
  ) {
    this.getHelpContent();
    this.spinner.show();
    this.pageload = true;
    if (localStorage.getItem('LoginResData')) {
      var userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', userData.data);
      this.userId = userData.data.data.id;
      this.tenantId = userData.data.data.tenantId;
    }
    this.page = localStorage.getItem('cat')
    if(this.page == 'category'){
      this.show = true;
    }
    this.qtd;
    this.myForm = new FormGroup({
      FilterOpt: new FormControl(),
      Value1: new FormControl(),
      Value2: new FormControl()
    });

    // this.clearFormData();

    // this.makeMultiDropDataReady();
    // this.getCourseCat();

    this.settingsCreatorDrop = {
      text: 'Select User',
      singleSelection: true,
      classes: 'common-multi',
      primaryKey: 'ecn',
      labelKey: 'fullname',
      noDataLabel: 'Search user...',
      enableSearchFilter: true,
      searchBy: ['ecn', 'fullname'],
      lazyLoading: true,
    };

    this.settingsTagDrop = {
      badgeShowLimit: 4,
      text: 'Select Tags',
      singleSelection: false,
      classes: 'common-multi',
      primaryKey: 'id',
      labelKey: 'name',
      noDataLabel: 'Search Tags...',
      enableSearchFilter: true,
      lazyloading: true,
      searchBy: ['name'],
    };

    // this.fetchUnEnrolledUsers((data) => {
    //     // cache our list
    //     this.tempUsers = [...data];
    //     // this.rowsUsers = data;
    //   });

    this.getCourseDropdownList();
    // this.makeCourseDataReady();
    if (!this.validfrmDate) {
      this.validfrmDate = new Date();
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(this.inpdata === 'savennext') {
      this.saveCourseDetails(this.coursefrm, this.pfrm);
    }
  }

  clearFormData() {
    this.formdata = {
      categoryId: '',
      courseCode: '',
      courseCompletion: '',
      courseId: 0,
      courseLevelId: '1',
      courseOrder: '',
      coursePicRef: this.courseIconUrl,
      courseTypeId: '',
      creatorId: '',
      validToDate: '',
      fullname: '',
      leadTime: '',
      shortname: '',
      validFromDate: '',
      summary: '',
      tags: '',
      tenantId: this.tenantId,
      validationFreq: '',
      visible: '',
      craditpoints: this.credits,
      userModified: this.userId
      // managerCreditPoints: '',
      // managerCreditPointsId: 0,
      // learnerCreditPoints: '',
      // learnerCreditPointsId: 0
    };
  }

  addPointsList() {
    // let defualtCreditsObjCopy = Object.assign({}, this.defualtCreditsObj);
    let defualtCreditsObj = {
      creditAllocId: 0,
      roleId: '',
      creditTypeId: '',
      creditTypeValueac: '',
      creditTypeValuebc: ''
    };
    // console.log(defualtCreditsObj);
    // this.credits.push(defualtCreditsObj);
    this.formdata.craditpoints.push(defualtCreditsObj);
  }

  removePointsList(i: number) {
    // this.credits.removeAt(i);
    // this.credits.splice(i,1);
    this.formdata.craditpoints.splice(i, 1);
    this.roleSelectedList.splice(i, 1);
    this.selectedRole.splice(i, 1);
    this.addPointsDisableEnable();
    this.disableSelectedRole();
  }

  disableAddCredit: boolean = false;
  addPointsDisableEnable() {
    if (this.userRoles.length == this.roleSelectedList.length) {
      this.disableAddCredit = true;
    } else {
      this.disableAddCredit = false;
    }
  }

  roleSelectedList = [];
  selectedRole = [];
  roleTypeSelected(currentIndex, currentItem) {
    console.log('currentItem ', currentItem);
    // this.userRoles.forEach(role => {
    //   if(role.isSelected == 1){
    //     this.roleSelectedList.push(role);
    //   }
    // });

    for (let i = 0; i < this.userRoles.length; i++) {
      var role = this.userRoles[i];
      if (role.id == Number(currentItem.roleId)) {
        this.roleSelectedList[currentIndex] = role;
        if (this.selectedRole.length > 0) {
          this.selectedRole[currentIndex] = role.name;
        } else {
          this.selectedRole.push(role.name);
        }
      }
    }

    this.addPointsDisableEnable();
    this.disableSelectedRole();
  }

  disableSelectedRole() {
    this.userRoles.forEach((data, key) => {
      if (this.selectedRole.indexOf(data.name) >= 0) {
        this.userRoles[key].isSelected = 1;
      } else {
        this.userRoles[key].isSelected = 0;
      }
    });
    console.log('Selected Disabled', this.userRoles);
  }

  marksTypeSelected(currentEvent, currentIndex, currentItem) {
    console.log('currentItem ', currentItem);
  }
  moduleDataAccess: any;
  disableVisiblityStatusForRole: any;

  ngOnInit() {
    // this.spinner.show();
    this.currentBrandData = this.brandService.getCurrentBrandData();
    this.fileName='Click here to upload an excel file to enrol '+ this.currentBrandData.employee.toLowerCase() +' to this course'
    this.selfdatafield();
    this.moduleDataAccess = this.moduleAccessService.getRoleAccessData('onlineCourse');
    console.log('Module Data Access ==>', this.moduleDataAccess);
    if(this.moduleDataAccess){
      this.disableVisiblityStatusForRole = !this.moduleDataAccess.allowCourseVisibilityModification;
    }
    const param = {
      cId: this.service.data.data ? this.service.data.data.courseId : 0,
      tId: this.tenantId,
      };

      const get_all_course_credit_tags_optmz: string = webApi.domain + webApi.url.get_all_course_credit_tags_optmz;
      this.commonFunctionService.httpPostRequest(get_all_course_credit_tags_optmz,param).then(res=>{
        this.credit = res['credits']
        this.tags = res['tags']['tagids']
        this.contentDetails = res['details']
        this.makeCourseDataReady();
      })

    // this.makeCourseDataReady();
    // this.getHelpContent();


    // this.passService.crossCommunication.subscribe(
    //   () => {
    //     this.saveCourseDetails(this.coursefrm,this.pfrm);
    //   });

    // if (this.eventEmitterService.subsVar === undefined) {
    //   this.eventEmitterService.subsVar = this.eventEmitterService.invokeComponentFunction.subscribe(() => {
    //     this.saveCourseDetails(this.coursefrm,this.pfrm);
    //   });
    // }
    this.header.breadCrumbList = [
      {
        'name': 'Learning',
        'navigationPath': '/pages/learning',
      },
      {
       'name': 'Online Courses',
       'navigationPath': '/plan/courses/content',
       }
     ];


  }

  courseDropdownList: any;
  getCourseDropdownList() {
    this.spinner.show();
    let param: any = {
      tId: this.tenantId,
      typeId: 1
    };
    this.detailsService.getDropdownList(param).then(
      rescompData => {
        // this.loader =false;
        this.spinner.hide();
        this.courseDropdownList = rescompData['data'];
        console.log('Course Dropdown List ', this.courseDropdownList);
        this.makeCourseDetailsDropdownReady();

        // this.dataReady = true;
        this.cdf.detectChanges();
      },
      resUserError => {
        // this.loader =false;
        this.spinner.hide();
        this.errorMsg = resUserError;
        this.dataReady = true;
      }
    );
  }

  makeCourseDetailsDropdownReady() {
    this.visibility = [];
    this.tagList = [];
    this.courseFormat = [];
    this.courseLevel = [];
    this.daysBucket = [];
    this.categoryData = [];
    this.userRoles = [];
    this.creatorList = [];
    // for(let i=0; i<this.courseDropdownList.length; i++){
    //   var item = this.courseDropdownList[i];
    //   if(item.length > 0){
    //     for(let j=0; j<item.length; j++){
    //       if(item[j].typeid == 0){
    //         this.categoryData.push(item[j]);
    //       }else if(item[j].typeid == 1){
    //         this.visibility.push(item[j]);
    //       }else if(item[j].typeid == 2){
    //         this.courseFormat.push(item[j]);
    //       }else if(item[j].typeid == 3){
    //         this.courseLevel.push(item[j]);
    //       }else if(item[j].typeid == 4){
    //         this.daysBucket.push(item[j]);
    //       }else{
    //         // console.log('no matched found!');
    //         this.userRoles.push(item[j]);
    //       }
    //     }
    //   }
    // }

    this.categoryData = this.courseDropdownList.catList;
    this.courseFormat = this.courseDropdownList.courseType;
    this.courseLevel = this.courseDropdownList.courseLevel;
    this.visibility = this.courseDropdownList.visibility;
    this.daysBucket = this.courseDropdownList.leadTime;
    this.userRoles = this.courseDropdownList.userRoles;
    this.creatorList = this.courseDropdownList.creatorList;
    this.tagList = this.courseDropdownList.tagList;

    console.log('categoryData', this.categoryData);
    console.log('courseFormat', this.courseFormat);
    console.log('courseLevel', this.courseLevel);
    console.log('visibility', this.visibility);
    console.log('daysBucket', this.daysBucket);
    console.log('User Roles', this.userRoles);
    console.log('creator List', this.creatorList);
    console.log('this.tagList', this.tagList);

    this.service.tagList = this.tagList;
    this.tempUsers = [...this.creatorList];
    if(this.conData.creatorId){
      this.selectedCreator = this.commonFunctionService.getMatchedArray(this.tempUsers, [this.conData.creatorId], 'id');
    }
    this.tempTags = [... this.tagList];
    if (this.tags) {
      var tagIds = this.tags.split(',');
      // this.tempTags.forEach((tag) => {
      //   tagIds.forEach((tagId) => {
      //     if (tag.id == tagId) {
      //       this.selectedTags.push(tag);
      //     }
      //   });
      // });

      this.selectedTags = this.commonFunctionService.getMatchedArray(this.tempTags, tagIds, 'id');
    }
    this.cdf.detectChanges();
    // this.makeCourseDataReady();
  }

  courseImgData: any;
  readCourseThumb(event: any) {
    var size = 1000000;
    var validExts = new Array('.png', '.jpg', '.jpeg');
    var fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
      // var toast: Toast = {
      //   type: 'error',
      //   title: 'Invalid file selected!',
      //   body: 'Valid files are of ' + validExts.toString() + ' types.',
      //   showCloseButton: true,
      //   // tapToDismiss: false,
      //   timeout: 2000
      //   // onHideCallback: () => {
      //   //     this.router.navigate(['/pages/plan/users']);
      //   // }
      // };
      // this.toasterService.pop(toast);

      this.toastr.warning('Valid file types are ' + validExts.toString(), 'Warning', {
        closeButton: false
      });

      // this.deleteCourseThumb();
    } else if (size <= event.target.files[0].size) {
      // var toast: Toast = {
      //   type: 'error',
      //   title: 'file size exceeded!',
      //   body: 'file size should be less than 100 KB)',
      //   showCloseButton: true,
      //   // tapToDismiss: false,
      //   timeout: 2000
      //   // onHideCallback: () => {
      //   //     this.router.navigate(['/pages/plan/users']);
      //   // }
      // };
      // this.toasterService.pop(toast);

      this.toastr.warning('File size should be less than 100 KB', 'Warning', {
        timeOut: 5000,
        closeButton: false
      })
      // this.deleteCourseThumb();
    } else {
      if (event.target.files && event.target.files[0]) {
        this.courseImgData = event.target.files[0];

        var reader = new FileReader();

        reader.onload = (event: ProgressEvent) => {
          // this.defaultThumb = (<FileReader>event.target).result;
          //this.formdata.coursePicRef = null;
          this.formdata.coursePicRef = (<FileReader>event.target).result;
          this.cdf.detectChanges();
        };
        reader.readAsDataURL(event.target.files[0]);
      }
    }
  }

  deleteCourseThumb() {
    // this.defaultThumb = 'assets/images/category.jpg';
    this.formdata.coursePicRef = 'assets/images/courseicon.jpg';
    this.courseImgData = undefined;
    this.formdata.categoryPicRefs = undefined;
  }

  makeCourseDataReady() {
    var content

    var todaysDate = new Date();


    var optId;
    var catId;

    //console.log(this.service.data,"form data")


    if (this.service.data) {
      console.log(this.service.data,"this.service.data")

      // content = this.service.data.data;
      content = this.contentDetails
      // content.push(this.credit)
      // content.push(this.tags)
      console.log(content,"content")
      optId = this.service.data.id;
      catId = this.service.data.catId;
      this.conData = this.service.data.data;
      // this.conData = this.contentDetails
      if (content) {
        this.checkCourseUrl(content);
        if (content.validationFreq) {
          this.courseReviewCheck.value = true;
        } else {
          this.courseReviewCheck.value = false;
        }
      }
    } else {
      this.router.navigate(['pages/dashboard']);
      // optId = 0;
    }
    console.log('Content to Edit', content);
    console.log('OPTid', optId);
    console.log('CATid', catId);

    if (optId == 1) {
      this.title = 'Edit Content';
      this.date =new Date(content.validFromDate);
      this.formdata = {
        categoryId: content.categoryId,
        courseCode: content.courseCode,
        // courseCompletion: content.courseCompletion,
        courseCompletion: this.conData.courseCompletion,
        courseId: this.conData.courseId,
        courseLevelId: content.courseLevelId,
        courseOrder: content.courseOrder,
        // coursePicRef: content.coursePicRef
        //   ? content.coursePicRef
        //   : this.courseIconUrl,
        coursePicRef: this.conData.coursePicRef
        ? this.conData.coursePicRef
        : this.courseIconUrl,
        courseTypeId: 1,
        // creatorId: this.selectedCreator,
        creatorId:this.conData.creatorId,
        validToDate: new Date(content.validToDate),
        // fullname: content.fullname,
        fullname: this.conData.fullname,
        leadTime: content.leadTime,
        shortname: content.shortname,
        validFromDate: new Date(content.validFromDate),
        summary: content.summary,
        tags: this.tags == null ? this.tags : this.tags.split(','),
        //tags: content.tags == null ? content.tags : content.tags.split(','),
        tenantId: this.conData.tenantId,
        validationFreq: content.validationFreq,
        // visible: content.visible,
        visible: this.conData.visible,
        //craditpoints: content.craditpoints,
        craditpoints: this.credit,
        userModified: this.userId,
        totalEnrol:content.totalEnrol,
        // managerCreditPoints: content.managerCreditPoints,
        // managerCreditPointsId: content.managerCreditPointsId,
        // learnerCreditPoints: content.learnerCreditPoints,
        // learnerCreditPointsId: content.learnerCreditPointsId
      };

      // this.tempUsers.forEach((user, key) => {
      //   if (user.id == content.creatorId) {
      //     this.selectedCreator.push(user);
      //   }
      // });
      console.log(this.formdata.craditpoints,"formdata.craditpoints")

      // this.selectedCreator = this.commonFunctionService.getMatchedArray(this.tempUsers, [content.creatorId], 'id');
      if(this.tempUsers.length>0){
      this.selectedCreator = this.commonFunctionService.getMatchedArray(this.tempUsers, [this.conData.creatorId], 'id');
      console.log(this.selectedCreator,"selectedCreator")
      }

      if (this.tags) {
        var tagIds = this.tags.split(',');
        // this.tempTags.forEach((tag) => {
        //   tagIds.forEach((tagId) => {
        //     if (tag.id == tagId) {
        //       this.selectedTags.push(tag);
        //     }
        //   });
        // });

        this.selectedTags = this.commonFunctionService.getMatchedArray(this.tempTags, tagIds, 'id');
      }

      if (this.formdata.craditpoints && this.formdata.craditpoints.length > 0) {
        for (let i = 0; i < this.formdata.craditpoints.length; i++) {
          var credit = this.formdata.craditpoints[i];
          this.roleTypeSelected(i, credit);
        }
      }

      if (this.formdata.validationFreq) {
        this.courseValidChecked = true;
      } else {
        this.courseValidChecked = false;
      }
    } else {
      this.title = 'Add Content';
      this.formdata = {
        categoryId: catId ? catId : '',
        courseCode: '',
        courseCompletion: 1,
        courseId: 0,
        courseLevelId: '1',
        courseOrder: 0,
        coursePicRef: this.courseIconUrl,
        courseTypeId: '',
        creatorId: '',
        validToDate: '',
        fullname: '',
        leadTime: '',
        shortname: '',
        validFromDate: todaysDate,
        summary: '',
        tags: '',
        tenantId: this.tenantId,
        validationFreq: '',
        visible: 1,
        craditpoints: this.credits,
        userModified: this.userId,
        totalEnrol: 0,
        // managerCreditPoints: 0,
        // managerCreditPointsId: 0,
        // learnerCreditPoints: 0,
        // learnerCreditPointsId: 0
      };
      this.roleTypeSelected(0, this.credits[0]);
    }

    this.cData = {
      id: content == undefined ? 0 : content.courseId
    };
    this.viewbindflag = true;
    // if(this.service.data!= undefined){
    //   this.courseDropdownList = this.service.data.courseDropdowns;
    //   this.makeCourseDetailsDropdownReady();
    // }
  }


  formatDateReady(date) {
    if (date) {
      date = new Date(date);
      var day = date.getDate();
      var monthIndex = ('0' + (date.getMonth() + 1)).slice(-2);
      var year = date.getFullYear();

      return year + '-' + monthIndex + '-' + day;
    }

    // var formattedDate = year + '-' + monthIndex + '-' +day;
    // console.log('formattedDate',formattedDate);
  }

  makeTagDataReady(tagsData) {
    this.formdata.tags = ''
    tagsData.forEach((tag) => {
      if (this.formdata.tags == '') {
        this.formdata.tags = tag.id;
      } else {
        this.formdata.tags = this.formdata.tags + '|' + tag.id;
      }
      console.log('this.formdata.tags', this.formdata.tags);
    });

    // var tagsData = this.formdata.tags;
    // if (tagsData.length > 0) {
    //   var tagsString = '';
    //   for (let i = 0; i < tagsData.length; i++) {
    //     var tag = tagsData[i];
    //     if (tagsString != '') {
    //       tagsString += '|';
    //     }
    //     if (tag.value) {
    //       if (String(tag.value) != '' && String(tag.value) != 'null') {
    //         tagsString += tag.value;
    //       }
    //     } else {
    //       if (String(tag) != '' && String(tag) != 'null') {
    //         tagsString += tag;
    //       }
    //     }
    //   }
    //   // return tagsString;
    //   this.formattedTags = tagsString;
    // }

    // this.formdata.tags1 = tagsString;
  }

  formattedTags: any = null;
  validFromDate: any;
  validToDate: any;
  creatorId: any;
  creditPointsStr = '';
  makeCourseDetailsReady() {
    this.validFromDate = this.formatDateReady(this.formdata.validFromDate);
    this.creatorId =
      this.selectedCreator.length > 0 ? this.selectedCreator[0].id : '';
    console.log('selected creator', this.selectedCreator);
    // this.formattedTags = this.formdata.tags;
    if (this.selectedTags.length > 0) {
      this.makeTagDataReady(this.selectedTags);
      // this.formdata.tags = this.formattedTags;
    }
    // this.formdata.tags = this.makeTagDataReady(this.formattedTags);
    if (this.courseReviewCheck.value != true) {
      this.validToDate = this.formatDateReady(this.formdata.validToDate);
    } else {
      // this.validToDate = this.calculateValidToDate(this.formdata.validFromDate,this.formdata.validationFreq);
    }

    if (this.formdata.craditpoints && this.formdata.craditpoints.length > 0) {
      this.creditPointsStr = '';
      for (let i = 0; i < this.formdata.craditpoints.length; i++) {
        var creditPoints = this.formdata.craditpoints[i];
        if (this.creditPointsStr != '') {
          this.creditPointsStr += '#';
        }
        if (
          String(creditPoints.creditAllocId) != '' &&
          String(creditPoints.creditAllocId) != 'null'
        ) {
          this.creditPointsStr += creditPoints.creditAllocId;
        }
        if (
          String(creditPoints.roleId) != '' &&
          String(creditPoints.roleId) != 'null'
        ) {
          this.creditPointsStr += '|' + creditPoints.roleId;
        }
        if (
          String(creditPoints.creditTypeId) != '' &&
          String(creditPoints.creditTypeId) != 'null'
        ) {
          this.creditPointsStr += '|' + creditPoints.creditTypeId;
        }
        if (
          String(creditPoints.creditTypeValuebc) != '' &&
          String(creditPoints.creditTypeValuebc) != 'null'
        ) {
          this.creditPointsStr += '|' + creditPoints.creditTypeValuebc;
        }
        if (
          String(creditPoints.creditTypeValueac) != '' &&
          String(creditPoints.creditTypeValueac) != 'null'
        ) {
          this.creditPointsStr += '|' + creditPoints.creditTypeValueac;
        }
      }
      console.log('creditPoints string', this.creditPointsStr);
      // var myVar2 = this.formdata.craditpoints.join('#');
      // console.log('Array  string',myVar2);
    }
  }
  validfrmDate: any;

  onValidFromDateChanged(dateChangedEvent, fromDate) {
    if (this.courseReviewCheck.value == true) {
      this.validToDate = this.calculateValidToDate(
        dateChangedEvent,
        this.formdata.validationFreq
      );
      this.formdata.validToDate = new Date(this.validToDate);
    } else {
      this.validfrmDate = new Date(fromDate);
      this.formdata.validToDate = '';
      // if(this.pageload ===false)
      // {

      // }
      // else
      // {
      //   this.pageload =false;
      // }
    }
  }

  calculateValidToDate(fromDate, Freq) {
    var date = new Date(fromDate);
    var frequency = Number(Freq) + 1;
    var day = date.getDate();
    var monthIndex = ('0' + (date.getMonth() + frequency)).slice(-2);
    var year = date.getFullYear();

    if (Number(monthIndex) > 12) {
      var diffCount = Number(monthIndex) - 12;
      // var monthIndexCal = ('0' + (date.getMonth() + diffCount)).slice(-2);
      var monthIndexCal = '0' + String(diffCount);
      var yearCal = date.getFullYear() + 1;
      return yearCal + '-' + monthIndexCal + '-' + day;
    } else {
      return year + '-' + monthIndex + '-' + day;
    }
  }

  courseCodeDupliRes: any;
  // checkCourseCodeDupli(){
  //   var courseData = {
  //     courseId : this.formdata.courseId,
  //     courseCode : this.formdata.courseCode
  //   }
  //   this.detailsService.checkCourse(courseData).subscribe(rescompData => {
  //     // this.loader =false;
  //     this.courseCodeDupliRes = rescompData.data;
  //     console.log('Course Code duplication result ',this.courseCodeDupliRes);
  //   },
  //   resUserError => {
  //     // this.loader =false;
  //     this.errorMsg = resUserError
  //   });
  // }

  saveCourseDetails(f,p) {
    this.valid = true;

    // && this.selectedTags.length
    if (f.valid && p.valid) {
      this.makeCourseDetailsReady();
      // this.loader = true;
      // this.spinner.hide();
      this.checkCourseCode();
      // var courseData = {
      //   courseId : this.formdata.courseId,
      //   courseCode : this.formdata.courseCode,
      //   shortname : this.formdata.shortname
      // }
      // console.log('courseData',courseData);
      // this.detailsService.checkCourse(courseData).then(rescompData => {
      //   // this.loader =false;
      //   this.spinner.hide();
      //   this.courseCodeDupliRes = rescompData;
      //   console.log('Course Code duplication result ',this.courseCodeDupliRes);
      //   if(this.courseCodeDupliRes.courseCode.isPresent == 'true'){
      //     var codeCheck : Toast = {
      //       type: 'error',
      //       title: 'Course',
      //       body: this.courseCodeDupliRes.courseCode.msg,
      //       // body: temp.msg,
      //       showCloseButton: true,
      //       timeout: 2000
      //   };
      //   this.toasterService.pop(codeCheck);
      //   }else if(this.courseCodeDupliRes.shortname.isPresent == 'true'){
      //     var codeCheck : Toast = {
      //       type: 'error',
      //       title: 'Course',
      //       body: this.courseCodeDupliRes.shortname.msg,
      //       // body: temp.msg,
      //       showCloseButton: true,
      //       timeout: 2000
      //   };
      //   this.toasterService.pop(codeCheck);
      //   }else{
      //     this.checkCourseCode();
      //   }
      //   this.cdf.detectChanges();
      // },
      // resUserError => {
      //   // this.loader =false;
      //   this.spinner.hide();
      //   this.errorMsg = resUserError
      // });
    } else {
      console.log('Please Fill all fields');
      // const addEditF: Toast = {
      //   type: 'error',
      //   title: 'Unable to update',
      //   body: 'Please Fill all fields',
      //   showCloseButton: true,
      //   timeout: 2000
      // };
      // this.toasterService.pop(addEditF);

      this.toastr.warning('Please fill in the required fields', 'Warning', {
        closeButton: false
      });

      Object.keys(f.controls).forEach(key => {
        f.controls[key].markAsDirty();
      });
      Object.keys(p.controls).forEach(key => {
        p.controls[key].markAsDirty();
      });
    }
  }

  checkCourseCode() {
    this.spinner.show();

    var course = {
      categoryId: this.formdata.categoryId,
      courseCode: this.formdata.courseCode,
      courseCompletion: this.formdata.courseCompletion,
      courseId: this.formdata.courseId,
      courseLevelId: this.formdata.courseLevelId,
      courseOrder: this.formdata.courseOrder,
      coursePicRef: this.formdata.coursePicRef,
      courseTypeId: 1,
      creatorId: this.creatorId,
      validToDate: this.validToDate,
      fullname: this.formdata.fullname,
      leadTime: this.formdata.leadTime,
      shortname: this.formdata.shortname,
      validFromDate: this.validFromDate,
      summary: this.formdata.summary,
      tags: this.formdata.tags,
      tenantId: this.formdata.tenantId,
      validationFreq: this.formdata.validationFreq,
      visible: this.formdata.visible,
      craditpoints: this.creditPointsStr,
      userModified: this.userId
      // managerCreditPoints: this.formdata.managerCreditPoints,
      // managerCreditPointsId: this.formdata.managerCreditPointsId,
      // learnerCreditPoints: this.formdata.learnerCreditPoints,
      // learnerCreditPointsId: this.formdata.learnerCreditPointsId
    };
    console.log('Course Details ', course);

    var fd = new FormData();
    fd.append('content', JSON.stringify(course));
    fd.append('file', this.courseImgData);
    console.log('File Data ', fd);

    console.log('Course Data Img', this.courseImgData);
    console.log('Course Data ', course);

    let url = webApi.domain + webApi.url.addEditCourse;
    let fileUploadUrl = webApi.domain + webApi.url.fileUpload;
    let param = {
      tId: this.tenantId
    };

    if (this.courseImgData != undefined) {
      this.webApiService.getService(fileUploadUrl, fd).then(
        rescompData => {
          // this.loader =false;
          this.spinner.hide();
          var temp: any = rescompData;
          this.cdf.detectChanges();
          this.fileUploadRes = temp;
          // this.fileUploadRes = JSON.parse(temp);
          if (temp == 'err') {
            // this.notFound = true;
            // var thumbUpload: Toast = {
            //   type: 'error',
            //   title: 'Course Thumbnail',
            //   body: 'Unable to upload course thumbnail.',
            //   // body: temp.msg,
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(thumbUpload);

            this.toastr.error('Please contact site administrator and <div (click)="giveFeedback()"> click here <div> to leave your feedback', 'Error', {
              timeOut: 0,
              closeButton: true
            });
          } else if (temp.type == false) {
            // var thumbUpload: Toast = {
            //   type: 'error',
            //   title: 'Course Thumbnail',
            //   body: 'Unable to upload course thumbnail.',
            //   // body: temp.msg,
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(thumbUpload);

            this.toastr.error('Please contact site administrator and <div (click)="giveFeedback()"> click here <div> to leave your feedback', 'Error', {
              timeOut: 0,
              closeButton: true
            });
          } else {
            if (
              this.fileUploadRes.data != null ||
              this.fileUploadRes.fileError != true
            ) {
              course.coursePicRef = this.fileUploadRes.data.file_url;
              this.addUpdateCourse(url, course);
            } else {
              // var thumbUpload: Toast = {
              //   type: 'error',
              //   title: 'Course Thumbnail',
              //   // body: 'Unable to upload course thumbnail.',
              //   body: this.fileUploadRes.status,
              //   showCloseButton: true,
              //   timeout: 2000
              // };
              // this.toasterService.pop(thumbUpload);

              this.toastr.error('Please contact site administrator and <div (click)="giveFeedback()"> click here <div> to leave your feedback', 'Error', {
                timeOut: 0,
                closeButton: true
              });
            }
          }
          console.log('File Upload Result', this.fileUploadRes);
          this.cdf.detectChanges();
        },
        resUserError => {
          // this.loader =false;
          this.spinner.hide();
          this.errorMsg = resUserError;
        }
      );
    } else {
      this.addUpdateCourse(url, course);
    }
  }

  giveFeedback() {

  }

  fileUploadRes: any;
  courseAddEditRes: any;

  addUpdateCourse(url, course) {
    this.spinner.show();
    this.webApiService.getService(url, course).then(
      rescompData => {
        // this.loader =false;
        this.spinner.hide();
        var temp: any = rescompData;
        this.courseAddEditRes = temp.data;
        this.cdf.detectChanges();
        if (temp == 'err') {
          // this.notFound = true;
          // var catUpdate: Toast = {
          //   type: 'error',
          //   title: 'Course',
          //   body: 'Unable to update course.',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(catUpdate);

          this.toastr.error('Please contact site administrator and <div (click)="giveFeedback()"> click here <div> to leave your feedback', 'Error', {
            timeOut: 0,
            closeButton: true
          });

        } else if (temp.type == false) {
          // var catUpdate: Toast = {
          //   type: 'error',
          //   title: 'Course',
          //   body: this.courseAddEditRes[0].msg,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(catUpdate);

          this.toastr.error('Please contact site administrator and <div (click)="giveFeedback()"> click here <div> to leave your feedback', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        } else {
          // var catUpdate: Toast = {
          //   type: 'success',
          //   title: 'Course',
          //   // body: 'Unable to update course.',
          //   body: this.courseAddEditRes[0].msg,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(catUpdate);

          this.toastr.success(this.courseAddEditRes[0].msg, 'Success', {
            closeButton: false
          });

          this.passService.detailsDone = true;

          const DataTab = {
            tabTitle: 'Modules',
          }
          try {
            this.service.data.courseId = this.courseAddEditRes[0].courseId ?
              this.courseAddEditRes[0].courseId : this.formdata.courseId;
            this.service.data.courseCode = this.courseAddEditRes[0].courseCode;
            this.formdata.courseCode = this.courseAddEditRes[0].courseCode;
            this.formdata.courseId = this.courseAddEditRes[0].courseId;
            // this.service.data.data;
            // console.log(this.service.data.data.courseId);
            // this.coursecontent.emit(this.courseAddEditRes[0].courseId);
            // this.AddEditCourseContent.cidpassed(this.courseAddEditRes[0]);
            this.AddEditCourseContent.selectedTab(DataTab);
            this.savennext.emit();
            // if(this.checkCourseUrl(this.service.data)){

            // }
            this.checkCourseUrl(this.service.data);
            // if (!this.showurl) {
            //   this.createurl(this.service.data.courseCode);
            // }
          } catch (e) {
            console.log('course result is wrong', this.courseAddEditRes);
            //this.service.courseId =undefined;
          }
          //  this.AddEditCourseContent.selectedTab(DataTab)
          // this.router.navigate(['/pages/plan/courses/content']);
        }
        console.log('Course AddEdit Result ', this.courseAddEditRes);
        //  this.service.courseId = this.courseAddEditRes[0].courseId;
        this.cdf.detectChanges();
      },
      resUserError => {
        // this.loader =false;
        this.spinner.hide();
        this.errorMsg = resUserError;
      }
    );
  }

  saveCourse() {
    // this.tab1 = false;
    // this.tab2 = true;
    // console.log('Form Input Data: '+this.formdata.fullname+'Full Data'+this.formdata);

    var course = {
      cacherev: this.formdata.cacherev,
      calendartype: this.formdata.calendartype,
      category: this.formdata.category,
      completionnotify: this.formdata.completionnotify,
      defaultgroupingid: this.formdata.defaultgroupingid,
      enablecompletion: this.formdata.enablecompletion,
      // enddate: this.formdata.enddate
      enddate:
        this.formdata.enddate.epoc == undefined
          ? this.formattedEndDate
          : this.formdata.enddate.epoc,
      format: this.formdata.format,
      fullname: this.formdata.fullname,
      groupmode: this.formdata.groupmode,
      groupmodeforce: this.formdata.groupmodeforce,
      id: this.formdata.id,
      idnumber: this.formdata.idnumber,
      lang: this.formdata.lang,
      legacyfiles: this.formdata.legacyfiles,
      marker: this.formdata.marker,
      maxbytes: this.formdata.maxbytes,
      newsitems: this.formdata.newsitems,
      requested: this.formdata.requested,
      shortname: this.formdata.shortname,
      showgrades: this.formdata.showgrades,
      showreports: this.formdata.showreports,
      sortorder: this.formdata.sortorder,
      //startdate: this.formdata.startdate,
      startdate:
        this.formdata.startdate.epoc == undefined
          ? this.formattedStartDate
          : this.formdata.startdate.epoc,
      summary: this.formdata.summary,
      summaryformat: this.formdata.summaryformat,
      theme: this.formdata.theme,
      timecreated: this.formdata.timecreated,
      timemodified: this.formdata.timemodified,
      visible: this.formdata.visible,
      visibleold: this.formdata.visibleold
    };

    // this.saveCourseID = course.id;
    var saveCourseID = course.id;

    var fileData = {
      id: 0,
      // contenthash : this.fileReadedArr,
      contenthash: this.fileUrl,
      pathnamehash: '',
      contextid: this.formdata.contextId,
      component: 'user',
      filearea: 'draft',
      itemid: 0,
      filepath: '/',
      filename:
        this.uploadFileData == undefined ? '' : this.uploadFileData.name,
      userid: 2,
      filesize:
        this.uploadFileData == undefined ? '' : this.uploadFileData.size,
      mimetype:
        this.uploadFileData == undefined ? '' : this.uploadFileData.type,
      status: '',
      source: '',
      author: 'admin User',
      license: 'allrightsreserved',
      timecreated: '',
      timemodified: '',
      sortorder: '',
      referencefileid: '',
      contextlevel: 'course',
      instanceid: course.id
    };

    console.log('Start Date', this.formdata.startdate);
    console.log('End date', this.formdata.enddate);
    const formData: any = new FormData();
    // const files: Array<File> = this.fileReadedArr;
    console.log('Readed File Data' + this.fileReaded);
    // for(let i =0; i < files.length; i++){
    formData.append('file', this.fileReaded, this.fileReaded.name);
    // }

    var courseData = {
      course: course,
      file: fileData,
      fileRead: formData
    };

    console.log('Form Data', this.formdata);
    console.log('Course Data', courseData);
    // this.createUpdateCourse(courseData,saveCourseID);
  }

  // createUpdateCourse(courseData,saveCourseID){
  //   this.detailsService.createUpdateCourse(courseData)
  //   .subscribe(rescompData => {
  //     // this.loader =false;
  //     this.result = rescompData;
  //     console.log('Course Result',this.result)
  //     if(this.result.type == true){
  //       // this.router.navigate(['/pages/ccategory/ccategorydata']);
  //       // this.tab1 = false;
  //       // this.tab2 = true;
  //       // this.getTopics(saveCourseID);
  //     }
  //     // this.getTopics(course.id);
  //   },
  //   resUserError => {
  //     // this.loader =false;
  //     this.errorMsg = resUserError
  //   });
  // }

  onExpectDateChange(event: IMyDateModel): void {
    if (event.jsdate != null) {
      let d: Date = new Date(event.jsdate.getTime());

      // set previous of selected date
      d.setDate(d.getDate() - 1);
      //this.myDate=new Date();
      //let finalDate=this.myDate;

      // Get new copy of options in order the date picker detect change
      let copy: INgxMyDpOptions = this.getCopyOfEndDateOptions();
      copy.disableSince = {
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate()
      };
      //this.myDatePickerOptions1.disableSince=copy.disableSince;
      // this.startDate = copy;
      // copy.disableUntil={
      //   year:d.getFullYear()+50,
      //   month:d.getMonth()+(50*12);
      //     day:d.getDay()
      // };
      this.selectedStartDate = d;
      this.myDatePickerOptions1 = {
        dateFormat: 'yyyy-mm-dd',
        disableUntil: {
          year: d.getFullYear(),
          month: d.getMonth() + 1,
          day: d.getDate() + 1
        }
      };
      this.endDate = copy;
    }
  }

  onStartDateChanged(event: IMyDateModel): void {
    // date selected
    if (event.jsdate != null) {
      let d: Date = new Date(event.jsdate.getTime());

      // set previous of selected date
      d.setDate(d.getDate() - 1);
      //this.myDate=new Date();
      //let finalDate=this.myDate;

      // Get new copy of options in order the date picker detect change
      let copy: INgxMyDpOptions = this.getCopyOfEndDateOptions();
      copy.disableSince = {
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate()
      };
      //this.myDatePickerOptions1.disableSince=copy.disableSince;
      // this.startDate = copy;
      // copy.disableUntil={
      //   year:d.getFullYear()+50,
      //   month:d.getMonth()+(50*12);
      //     day:d.getDay()
      // };
      this.selectedStartDate = d;
      this.myDatePickerOptions1 = {
        dateFormat: 'yyyy-mm-dd',
        disableUntil: {
          year: d.getFullYear(),
          month: d.getMonth() + 1,
          day: d.getDate() + 1
        }
      };
      this.endDate = copy;
      this.startdateSelected = true;
    }
  }

  onEndDateChanged(event: IMyDateModel) {
    if (event.jsdate != null) {
      let d: Date = new Date(event.jsdate.getTime());

      // set previous of selected date
      d.setDate(d.getDate() + 1);

      // Get new copy of options in order the date picker detect change
      let copy: INgxMyDpOptions = this.getCopyOfStartDateOptions();
      copy.disableSince = {
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate()
      };
      this.startDate = copy;
      // end date changed...
    } else {
      let copy: INgxMyDpOptions = this.getCopyOfStartDateOptions();
      copy.disableSince = { year: 0, month: 0, day: 0 };
      this.startDate = copy;
    }
  }

  getCopyOfEndDateOptions(): INgxMyDpOptions {
    return JSON.parse(JSON.stringify(this.endDate));
  }

  getCopyOfStartDateOptions(): INgxMyDpOptions {
    return JSON.parse(JSON.stringify(this.startDate));
  }

  initRules() {
    return this._fb.group({
      FilterOpt: [''],
      Value1: [''],
      Value2: ['']
    });
  }

  addFilter() {
    const control = <FormArray>this.myForm.controls['parameters'];
    control.push(this.initRules());
    console.log(this.myForm.controls['parameters']);
    this.controlFlag = true;
    this.controlList.push(0);
    this.strArrayType.push([]);
  }

  removeFilter(i: number) {
    const control = <FormArray>this.myForm.controls['parameters'];
    control.removeAt(i);
    // this.enableSelect(this.strArrayType[i]);
    this.selectedFilterOption.splice(i, 1);
    this.disableSelect();
    this.controlFlag = true;
    this.controlList.splice(i, 1);
    this.strArrayType.splice(i, 1);
  }

  clearFilter() {
    this.myForm.reset({
      FilterOpt: [''],
      Value1: [''],
      Value2: ['']
    });
  }

  clearResults() {
    this.users = [];
    // this.rowData=[];
  }

  save(model) {
    console.log(model);
  }

  sort(key) {
    this.key = key;
    this.reverse = !this.reverse;
  }

  disableSelect() {
    this.strArrayPar.forEach((data, key) => {
      if (this.selectedFilterOption.indexOf(data.pDBName) >= 0) {
        this.strArrayPar[key].pSelected = 'true';
      } else {
        this.strArrayPar[key].pSelected = 'false';
      }
    });
    console.log('Selected Disabled', this.strArrayPar);
  }

  callType(id: any, index: any) {
    if (this.strArrayType[index]) {
      this.strArrayType[index] = [];
    }

    this.ValueId = parseInt((id.srcElement || id.target).value);
    //this.disableSelect();

    this.controlList[index] = this.ValueId;
    for (let i = 0; i < this.strArrayTypePar.length; i++) {
      if (this.strArrayTypePar[i].pId == this.ValueId) {
        this.strArrayType[index].push(this.strArrayTypePar[i]);
        if (this.selectedFilterOption.length > 0) {
          this.selectedFilterOption[index] = this.strArrayTypePar[i].pDBName;
        } else {
          this.selectedFilterOption.push(this.strArrayTypePar[i].pDBName);
        }
      }
    }
    this.disableSelect();
  }

  format(date) {
    var DT = new Date(date);
    var month = DT.getMonth() + 1;
    var day = DT.getDate();
    var year = DT.getFullYear();
    return { date: { year: year, month: month, day: day } };
  }

  clear() {
    this.search2 = {};
  }

  formattedString() {
    var para = this.formattedPara;
    var string = '';
    if (para.length > 0) {
      for (var i = 0; i < para.length; i++) {
        var parameter = para[i];
        if (string != '') {
          string += ',';
        }
        if (parameter.pDBName != '' && parameter.Value1 != '') {
          string += parameter.pDBName + '|' + parameter.Value1;
        }
        if (parameter.Value2 != '') {
          string += '|' + parameter.Value2;
        }
      }
    }
    console.log(string);
  }

  clearFilterData() {
    this.filterData = {
      userProfFields: null,
      gradeGreatEqual: null,
      gradeLess: null,
      compTrack: null,
      dateUntil: null,
      dateFrom: null,
      remarks: null
    };
  }

  // onSubmit(f){
  //   if(f.valid){

  //   } else{
  //     console.log('Please Fill all fields');
  //     Object.keys( f.controls).forEach(key => {
  //       f.controls[key].markAsDirty();
  //      });
  //   }
  // }

  submitForm() {
    this.loader = true;
    console.log(this.myForm.value);
    let parameters: any = this.myForm.value.parameters;

    for (let d = 0; d < parameters.length; d++) {
      if (parameters[d].Value1 != null) {
        if (parameters[d].Value1.epoc != undefined) {
          parameters[d].Value1 = parameters[d].Value1.epoc;
        }
      }
      if (parameters[d].Value1 != null) {
        if (parameters[d].Value2.epoc != undefined) {
          parameters[d].Value2 = parameters[d].Value2.epoc;
        }
      }
    }
    console.log('Filter Date', parameters);

    this.formattedPara = parameters;

    for (let i = 0; i < this.formattedPara.length; i++) {
      for (let j = 0; j < this.strArrayPar.length; j++) {
        if (
          parseInt(this.formattedPara[i].FilterOpt) === this.strArrayPar[j].pId
        ) {
          // parameters.push(this.strArrayPar[i].pDBName);
          this.formattedPara[i].pDBName = this.strArrayPar[j].pDBName;
          this.formattedPara[i].pIsMultiple = this.strArrayPar[j].pIsMultiple;
        }
      }
    }
    console.log('Filter Data', this.formattedPara);

    for (let j = 0; j < this.formattedPara.length; j++) {
      if (this.formattedPara[j].pIsMultiple == 'false') {
        this.filterData[this.formattedPara[j].pDBName] = this.formattedPara[
          j
        ].Value1;
      } else {
        this.filterData[this.formattedPara[j].pDBName].to = this.formattedPara[
          j
        ].Value1;
        this.filterData[
          this.formattedPara[j].pDBName
        ].from = this.formattedPara[j].Value2;
      }
    }
    console.log('Filter Final Data', this.filterData);

  }

  submit() {
    this.router.navigate(['/pages/plan/courses/content']);
  }

  back() {
    this.router.navigate(['/pages/plan/courses/content']);
  }

  readUrl(event: any) {
    var validExts = new Array('.xlsx', '.xls');
    var fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
      //this.toastr.onClickToast()
      //.subscribe( toast => {
      // this.router.navigate(['/pages/users/induction']);
      ///});
      //this.toastr.error('valid files are of ' + validExts.toString() + ' types.', 'Invalid file selected!', {dismiss: 'click',toastLife: 5000});
      //this.cancel();
    } else {
      // return true;
      if (event.target.files && event.target.files[0]) {
        this.fileName = event.target.files[0].name;
        //read file from input

        this.fileReaded = event.target.files[0];

        if (
          this.fileReaded != '' ||
          this.fileReaded != null ||
          this.fileReaded != undefined
        ) {
          this.enableUpload = true;
        }

        let file = event.target.files[0];
        var reader = new FileReader();
        reader.onload = (event: ProgressEvent) => {
          this.fileUrl = (<FileReader>event.target).result;
        };
        reader.readAsDataURL(event.target.files[0]);
      }
    }
  }

  public editableChangeCallbackRes(
    newValue: string,
    oldValue: string,
    elementRef: ElementRef
  ) {
    //  handle new value
    this.newValueRes = newValue;
    this.oldValueRes = oldValue;
    this.elementRefRes = elementRef;
    console.log('oldValueRes', this.oldValueRes);
    console.log('newValueRes', this.newValueRes);
    console.log('elementRefRes', this.elementRefRes);
  }

  cancel() {
    // this.fileUpload.nativeElement.files = [];
    console.log(this.fileUpload.nativeElement.files);
    this.fileUpload.nativeElement.value = '';
    console.log(this.fileUpload.nativeElement.files);
    this.fileName='Click here to upload an excel file to enrol '+ this.currentBrandData.employee.toLowerCase() +' to this course'
    this.enableUpload = false;
  }

  public editableChangeCallbackTop(
    newValue: string,
    oldValue: string,
    elementRef: ElementRef
  ) {
    //  handle new value
    this.newValueTop = newValue;
    this.oldValueTop = oldValue;
    this.elementRefTop = elementRef;
    console.log('oldValueTop', this.oldValueTop);
    console.log('newValueTop', this.newValueTop);
    console.log('elementRefTop', this.elementRefTop);
  }

  courseValidChecked: boolean = false;
  onCourseValidCheckBoxClick(event, courseReviewCheck) {
    if (event == false) {
      this.reviewCheck = {};
      // this.courseReviewCheck = false;
      this.courseValidChecked = false;
      this.formdata.validationFreq = '';
      this.formdata.leadTime = '';
    } else {
      // this.courseReviewCheck = true;
      this.courseValidChecked = true;
    }
    // console.log('$event',$event);
    console.log('courseReviewCheck', courseReviewCheck);
  }

  onValidFreqMonthSelected(currentEvent) {
    // console.log('currentEvent ',currentEvent);
    console.log('monthFreq ', currentEvent);
    if (this.courseReviewCheck.value == true) {
      this.validToDate = this.calculateValidToDate(
        this.formdata.validFromDate,
        currentEvent
      );
      this.formdata.validToDate = new Date(this.validToDate);
    }
    this.daysBucket.forEach(element => {
      console.log('element', element);
      if (Number(currentEvent) * 30 >= Number(element.name)) {
        element.status = true;
      } else {
        element.status = false;
      }
    });
    this.formdata.leadTime = ''

  }

  onCreatorSelect(item: any) {
    console.log(item);
    console.log(this.selectedCreator);
  }
  OnCreatorDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedCreator);
  }

  // fetchUnEnrolledUsers(cb) {
  //     const req = new XMLHttpRequest();
  //     req.open('GET', `assets/data/unEnroledUsers.json`);

  //     req.onload = () => {
  //       cb(JSON.parse(req.response));
  //     };

  //     req.send();
  // }

  // onCreatorSearch(evt: any) {
  //   console.log(evt.target.value);
  //   const val = evt.target.value;
  //   this.usersList = [];
  //   const temp = this.tempUsers.filter(function (d) {
  //     return (
  //       String(d.ecn)
  //         .toLowerCase()
  //         .indexOf(val) !== -1 ||
  //       d.fullname.toLowerCase().indexOf(val) !== -1 ||
  //       d.email.toLowerCase().indexOf(val) !== -1 ||
  //       d.ecn.toLowerCase().indexOf(val) !== -1 ||
  //       !val
  //     );
  //   });

  //   // update the rows
  //   this.usersList = temp;
  //   console.log(this.usersList);
  // }

  onCreatorSearch(evt: any) {
    console.log(evt.target.value);
    const val = evt.target.value;
    // this.usersList = [];
    if(val && val.length>2){
    this.getAllCreator(val)
    }
  }
  getAllCreator(str) {
    // if (str.length > 3) {
      this.spinner.show();
      const param = {
        searchStr: str,
        // tId: this.tenantId
      };
     const _urlGetAllEmployee: string = webApi.domain + webApi.url.getAllCreator;
     this.commonFunctionService.httpPostRequest(_urlGetAllEmployee,param)
      // this.addassetservice.getAllEdgeEmployee(param)
      .then(
        rescompData => {
          this.spinner.hide();
          var result = rescompData;
          console.log('getAllEdgeEmployessDAM:', rescompData);
          if (result['type'] == true) {
            if (result['data'][0].length == 0) {
              // this.noEdgeEmployees = true;
            } else {
              this.usersList = result['data'];
              console.log(this.usersList,"hhvvgg")
              // this.tempUsers = result['data'][0];

            }
          } else {
            // var toast: Toast = {
            //   type: 'error',
            //   //title: 'Server Error!',
            //   body: 'Something went wrong.please try again later.',
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(toast);
          // this.presentToast('error', '');
          }
        },
        error => {
          this.spinner.hide();
          // var toast: Toast = {
          //   type: 'error',
          //   //title: 'Server Error!',
          //   body: 'Something went wrong.please try again later.',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          // this.presentToast('error', '');
        }
      );
    // }
  }
  formdate(date) {
    if (date) {
      // const months = ['JAN', 'FEB', 'MAR','APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
      // var day = date.getDate();
      // var monthIndex = months[date.getMonth()];
      // var year = date.getFullYear();

      // return day + '_' + monthIndex + '_' +year;
      var formatted = this.datePipe.transform(date, 'dd-MM-yyyy');
      return formatted;
    }
  }

  selfdatafield() {
    let data = {
      lovtype: 37,
      tId: this.tenantId
    };
    console.log(data);
    this.enrolService.getselfdropdownlist(data).then(res => {
      console.log(res);
      this.selftypefield = res;
      this.marksType = this.selftypefield.data[0];
      console.log('marktype', this.marksType);
      this.cdf.detectChanges();
    });
  }

  // Tag cganges

  onTagsSelect(item: any) {
    console.log(item);
    console.log(this.selectedTags);
  }
  OnTagDeSelect(item: any) {
    if(this.selectedTags.length == 0){
      this.valid = true
    }
    console.log(item);
    console.log(this.selectedTags);
  }
  onTagSearch(evt: any) {
    console.log(evt.target.value);
    const val = evt.target.value;
    this.tagList = [];
    const temp = this.tempTags.filter(function (d) {
      return (
        String(d.name)
          .toLowerCase()
          .indexOf(val) !== -1 ||
        !val
      );
    });

    // update the rows
    this.tagList = temp;
    console.log('filtered Tag LIst', this.tagList);
  }
  // Help Code Start Here //

  helpContent: any;
  getHelpContent() {
    return new Promise(resolve => {
      this.http1
        .get('../../../../../../assets/help-content/addEditCourseContent.json')
        .subscribe(
          data => {
            this.helpContent = data;
            console.log('Help Array', this.helpContent);
          },
          err => {
            resolve('err');
          }
        );
    });
    // return this.helpContent;
  }

  // Help Code Ends Here //

  // createurl(code) {
  //   let curl = this.helpContent.Course_URL.url;
  //   this.porturl = curl + '/' + code;
  //   this.showurl = true;
  //   console.log(this.porturl);
  // }
  copytext(data) {
    console.log("Assets url:>", data);
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = data;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.toastr.success('Link Copied', 'Success');
  }
  showGenrateButton = false;
  linkGenerationInProgress = false;
  checkCourseUrl(content){
    if(content['redirectionLink'] && content['redirectionLink'] !== null){
      this.shortUrl = content['redirectionLink'];
      this.showGenrateButton = true;
    } else {
      this.showGenrateButton = false;
    }
  }

  generateCourseUrl(){
    const _generateURL: string = webApi.domain + webApi.url.genearteFirebaseUrl;
    this. linkGenerationInProgress = true;
    this.cdf.detectChanges();
    const tenant_Info = JSON.parse(localStorage.getItem('tenant_Info'));
    let portalUrl = '';
    if(tenant_Info){
      let domainURL = window.location.hostname;
      // let domainURL = 'devadmin.edgelearning.co.in';
      if(domainURL === 'localhost'){
        portalUrl = '';
      }else {
        if(domainURL){
          domainURL =  domainURL.substring(domainURL.indexOf('.')+1);
        }
        portalUrl = tenant_Info.tenantCode + '.' + domainURL;
      }
    }
    const params = {
      'instanceId':  this.formdata.courseId,
      'areaId': 2,
      'indentifier': 'course',
      'portalUrl': portalUrl,
      'platform': 3,
    };
    params['link'] = 'pages/dynamic-link-learn/' + '2' + '/' +  this.formdata.courseId + '/' + 'course';
    this.commonFunctionService.httpPostRequest(_generateURL, params).then((res) =>{
      console.log('Response' ,res);
      if(res['statusCode'] === 200) {
        this.shortUrl = res['data']['shortLink'];
        this.showGenrateButton = true;
        this.cdf.detectChanges();
        this.linkGenerationInProgress = false;
      }else{
        this.linkGenerationInProgress = false;
        this.toastr.error('Unable to generate link at this time', 'Error', {
          closeButton: false,
        });

      }
    }).catch(function (err) {
      // next(new Error('user not found'));
      this.linkGenerationInProgress = false;
      this.toastr.error('Unable to generate link at this time', 'Error', {
        closeButton: false,
      });
      // this.linkGenerationInProgress = false;
    });
  }
}
