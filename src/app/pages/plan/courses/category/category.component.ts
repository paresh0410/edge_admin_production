import { Component, ViewEncapsulation, AfterContentChecked, ViewChild, ElementRef, OnDestroy,ChangeDetectorRef } from '@angular/core';
import { Router, NavigationStart, Routes, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CategoryService } from './category.service';
import { AddEditCategoryContentService } from '../addEditCategoryContent/addEditCategoryContent.service';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { ContentService } from '../content/content.service';
import { webAPIService } from '../../../../service/webAPIService';
import { webApi } from '../../../../service/webApi';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { CommonFunctionsService } from '../../../../service/common-functions.service';
import { Card } from '../../../../models/card.model';
import { noData } from '../../../../models/no-data.model';
import { SuubHeader } from '../../../components/models/subheader.model';
import { ContextMenuComponent } from 'ngx-contextmenu';
import { AddEditCourseContentService } from '../addEditCourseContent/addEditCourseContent.service';
import * as _ from "lodash";
import { Filter } from '../../../../models/filter.modal';
@Component({
  selector: 'category',
  styleUrls: ['./category.scss'],
  templateUrl: './category.html',
  encapsulation: ViewEncapsulation.None,
})
export class Category implements AfterContentChecked, OnDestroy {

  notFound: boolean = false;
  breadcrumbArray: any = [
    {
      'name': 'Learning',
      'navigationPath': '/pages/learning',
    },
    // {
    //   'name': 'Course Category',
    //   'navigationPath': '/pages/plan/courses/category',
    //   'id': {},
    //   'sameComp': true,
    //   'assetId': null
    // }
  ];

  previousBreadCrumb: any = [
    {
      'name': 'Learning',
      'navigationPath': '/pages/learning',
    },
    {
      'name': 'Course Category',
      'navigationPath': '/pages/plan/courses/category',
      'id': {},
      'sameComp': true,
      'assetId': null
    }
  ]
  @ViewChild('ContextMenuComponent' ) public folderMenu: ContextMenuComponent;
  @ViewChild('hover') hover?: ElementRef<HTMLElement>;
  header: SuubHeader  = {
    title:'Course Category',
    btnsSearch: true,
    placeHolder:"Search by category name",
    searchBar: true,
    dropdownlabel: ' ',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: '',
    btnName3: '',
    btnAdd: 'Add Category',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: true,
    filter: false,
    showBreadcrumb: true,
    breadCrumbList:[]
  };
  count:number=12
  countLevel = 0
  query: string = '';
  public getData;
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:'No Categories at this time.',
    desc:'Categories will appear after they are added by the instructor. Categories are classification for managing the courses in the application.',
    titleShow:true,
    btnShow:true,
    descShow:true,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/setting-how-to-add-an-online-category',
  }
  cardModify: Card = {
    flag: 'courseCategory',
    titleProp : 'categoryName',
    discrption: 'description',
    question: 'qcnt',
    image: 'categoryPicRef',
    courseCount: 'coursecount',
    showBottomCourseCountr: true,
    hoverlable:   true,
    showImage: true,
    option:   true,
    eyeIcon:   true,
    editIcon: true,
    bottomDiv:   true,
    bottomDiscription:   true,
    showBottomList:   true,
    bottomTitle:   true,
    btnLabel: 'Details',
    defaultImage:'assets/images/category.jpg'
  };
  cardModifyCourse: Card = {
    flag: 'courseContent',
    titleProp : 'fullname',
    // discrption: 'description',
    courseCompletion: 'courseCompletion',
    compleCount: 'Completioncount',
    totalCount: 'enrollcount',
    manager: 'cereaterName',
    lpName:'lpName',
    maxpoints:'maxPoints',
    image: 'coursePicRef',
    showCompletedEnroll: true,
    showCourseCompletion: true,
    showBottomMaximumPoint: true,
    showBottomManager: true,
    showlpName:true,
    hoverlable:true,
    showImage: true,
    option:   true,
    eyeIcon:   true,
    copyIcon: true,

    bottomDiv:   true,
   // bottomDiscription:   true,
    showBottomList:   true,
    bottomTitle:   true,
    btnLabel: 'Edit',
    defaultImage: 'assets/images/courseicon.jpg'
  };

  cat: any;
  category: any = [];
  errorMsg: string;
  totalPages: any;
  content1: any = [];
  pagelmt = 100;
  search: any;
  loader: any;
   userData:any;
  tenantId:any;
  // conentHeight: string;
  isworking = false;
  offset: number = 100;
  str: any = '';
  currentPage = 1;
  skeleton = false;
  title: string;
  formdata: any;
  defaultThumb: any = 'assets/images/category.jpg';
  result: any;

  visibility: any = []

  catData: any = undefined;


  settingsTagDrop ={};
  tempTags:any =[];
  tagList:any = [];
  selectedTags:any = [];
  categoryId: any;
  data: any;
  id: any;
  dummy: any;
  index: any;
  showOld:boolean = false
  parentCatId: any = null;
  courseList: any;
  duplicateCourseData: any;
  duplicateCourseModal: boolean;
  categoryName: any;
  oldParentCatId: any;
  breadObj:{}
  breadtitle: any = 'Course Category';
  courseListDummy: any;
  categoryDummy: any;
  categoryData: any;
  courseFormat: any;
  courseLevel: any;
  daysBucket: any;
  userRoles: any;
  creatorList: any;
  workflowData: any;
  filtercon: Filter = {
    ascending: true,
    descending: true,
    showDropdown: true,
    // showDropdown: false,
    dropdownList: [
      { drpName: 'Course Name', val: 1 },
      { drpName: 'Created Date', val: 0 },
    ]
  };
  sortdata = [{
    id: 1,
    title: 'Course name',
    sortingmenu: [
      {
        id: 1,
        value: 'asc',
        name: 'ASC',
      }, {
        id: 2,
        value: 'desc',
        name: 'DESC',
      }
    ],
    type: '',
  },
  {
    id: 2,
    title: 'Time Creation/Time modified',
    sortingmenu: [
      {
        id: 1,
        value: 'asc1',
        name: 'ASC',
      }, {
        id: 2,
        value: 'desc2',
        name: 'DESC',
      }
    ],
    type: '',
  },
  ]
  filters: any =[];
  catSearch: any;
  courseSearch: any;
  hoverMeasure: boolean = false;
  heigthover: number;

  filter: boolean = false;
  courseCount: any = 0
  
  constructor(private spinner: NgxSpinnerService,private toastr: ToastrService,
   public cdf: ChangeDetectorRef,
   protected webApiService: webAPIService,
    //  private toasterService:ToasterService,
     protected passService: ContentService, protected categoryService: CategoryService,protected passService1: AddEditCourseContentService,
    protected addCategoryService: AddEditCategoryContentService, private router: Router, private http1: HttpClient,private commonFunctionService: CommonFunctionsService,) {
    this.loader = true;
    // this.conentHeight = '0px';
    this.search = {};
    this.category;
    this.cat = {
      id: '',
    };
    this.settingsTagDrop = {
      badgeShowLimit: 6,
      text: 'Select Tags',
      singleSelection: false,
      classes: 'common-multi',
      primaryKey: 'id',
      labelKey: 'name',
      noDataLabel: 'Search Tags...',
      enableSearchFilter: true,
      lazyloading: true,
      searchBy: ['name'],
    }

      if(localStorage.getItem('LoginResData')){
      this.userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', this.userData.data);
      // this.userId = this.userData.data.data.id;
      this.tenantId = this.userData.data.data.tenantId;

   }
   localStorage.setItem('cat','category')
    this.getDropdownList();
    this.getCourseDropdownList();
    this.getHelpContent();
    if(this.showOld == true){
    this.getCategories(this.str, this.currentPage);
    }
    if(this.showOld == false){
      if(this.passService1.breadcrumbArray){
        this.breadcrumbArray = this.passService1.breadcrumbArray
        this.breadtitle = this.passService1.breadtitle
        this.previousBreadCrumb = this.passService1.previousBreadCrumb
      }
      if(this.passService.parentCatId){
        this.parentCatId = this.passService.parentCatId
      }
      if(this.passService.countLevel){
        this.countLevel = this.passService.countLevel
      }

      // if(this.passService.categoryName){
      //   this.categoryName = this.passService.categoryName
      // }
      // this.spinner.show();
      this.getCourseCat(this.parentCatId,this.currentPage)
      var id
      var data
      this.getallTagList(id,data);
    }

  }

  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  getDropdownList() {
    // this.spinner.show();
    let url = webApi.domain + webApi.url.getVisibiltyDropdown;
    let param = {
      tId:  this.tenantId,
    };
    this.webApiService.getService(url, param)
      .then(rescompData => {
        // this.loader =false;
        this.spinner.hide();
        var temp: any = rescompData;
        if (temp == "err") {
        } else {
          this.visibility = temp.data;
        }
        // console.log('Visibility Dropdown',this.visibility);
      },
        resUserError => {
          // this.loader =false;
          this.spinner.hide();
          this.errorMsg = resUserError;
        });
  }
// ngOnChanges(){
//   if(this.catShow){
//     this.getallTagList(this.id,this.data);
//   }
// }
  getallTagList(id,category) {
    this.tempTags=[]
    this.tagList=[]
    // this.spinner.show();
    // this.cdf.detectChanges();
    let url = webApi.domain + webApi.url.getAllTagsComan;
    let param = {
      tenantId :this.tenantId
    };
    this.webApiService.getService(url, param)
      .then(rescompData => {
        // this.loader =false;

        var temp: any = rescompData;
        this.tagList = temp.data;
        this.tempTags = [... this.tagList];
        this.bindfilter(this.tagList)
        // if (category) {
          // this.makeCategoryDataReady(id, category);
        // }
        // console.log('Visibility Dropdown',this.visibility);
      },
        resUserError => {
          // this.loader =false;
          this.show =false;
          this.spinner.hide();
          this.errorMsg = resUserError;
        });
  }

  show:boolean =false;
  makeCategoryDataReady(id,category){
    this.selectedTags=[];
        if (id == 1) {
          this.formdata = {
            categoryId: category.categoryId,
            categoryName: category.categoryName,
            categoryCode: category.categoryCode,
            description: category.description,
            categoryPicRef: category.categoryPicRef ? category.categoryPicRef : this.defaultThumb,
            visible: category.visible,
            tenantId: category.tenantId,
            tags: category.tags == null ? category.tags : category.tags.split(',')

          }
          this.categoryId=this.formdata.categoryId;
            if(category.tagIds)
            {
              var tagIds =category.tagIds.split(',');
              if(tagIds.length > 0){
                this.tempTags.forEach((tag) => {
                  tagIds.forEach((tagId)=>{
                    if (tag.id == tagId ) {
                      this.selectedTags.push(tag);
                    }
                  });
                });
                }
          }
        } else {
          this.formdata = {
            categoryId: 0,
            categoryName: '',
            categoryCode: '',
            description: '',
            categoryPicRef: this.defaultThumb,
            visible: 1,
            tenantId: this.tenantId,
            tags: '',
          }
          this.categoryId=this.formdata.categoryId;
        }
        this.spinner.hide();
        // this.cdf.detectChanges();
        this.show =true;
      }

  categoryImgData: any;
  readCategoryThumb(event: any) {
    var validExts = new Array(".png", ".jpg", ".jpeg");
    var fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
      // var toast: Toast = {
      //   type: 'error',
      //   title: "Invalid file selected!",
      //   body: "Valid files are of " + validExts.toString() + " types.",
      //   showCloseButton: true,
      //   // tapToDismiss: false,
      //   timeout: 2000
      //   // onHideCallback: () => {
      //   //     this.router.navigate(['/pages/plan/users']);
      //   // }
      // };
      // this.toasterService.pop(toast);

      this.toastr.error('Valid file types are  ' + validExts.toString() , 'Error',{
        timeOut: 0,
        closeButton: true
      });

      // this.deleteCourseThumb();
    } else {
      if (event.target.files && event.target.files[0]) {
        this.categoryImgData = event.target.files[0];

        var reader = new FileReader();

        reader.onload = (event: ProgressEvent) => {
          // this.defaultThumb = (<FileReader>event.target).result;
          this.formdata.categoryPicRef = (<FileReader>event.target).result;
        }
        reader.readAsDataURL(event.target.files[0]);
        // this.toastr.error('Error', 'Valid files are of' + validExts.toString() + ' types.', {
        //   timeOut: 0,
        //   closeButton: true
        // });
      }
    }
  }

  deleteCategoryThumb() {
    // this.defaultThumb = 'assets/images/category.jpg';
    this.formdata.categoryPicRef = 'assets/images/category.jpg';
    this.categoryImgData = undefined;
    this.formdata.categoryPicRefs =undefined;
  }

  makeTagDataReady(tagsData) {
    this.formdata.tags  =''
     tagsData.forEach((tag)=>{
      if(this.formdata.tags  == '')
      {
        this.formdata.tags  = tag.id;
      }else
      {
        this.formdata.tags = this.formdata.tags +'|' + tag.id;
      }
      console.log('this.formdata.tags',this.formdata.tags);
     });
    // var tagsData = this.formdata.tags;
    // var tagsString = '';
    // if (tagsData) {
    //   for (let i = 0; i < tagsData.length; i++) {
    //     var tag = tagsData[i];
    //     if (tagsString != "") {
    //       tagsString += "|";
    //     }
    //     if (String(tag) != "" && String(tag) != "null") {
    //       tagsString += tag;
    //     }
    //   }
    //   this.formdata.tags1 = tagsString;
    // }
  }

  // popToaster(){

  // }

  catShow=false
  closesectionModel(){
    this.catShow=false
    // this.tempTags=[]
    // this.tagList=[]
    this.categoryImgData=undefined
  }

  fileUploadRes: any;
  categoryAddEditRes: any;
  addUpdateCategory(url, category) {
    this.spinner.show();
    this.webApiService.getService(url, category)
      .then(rescompData => {
        // this.loader =false;
        this.spinner.hide();
        var temp: any = rescompData;
        this.categoryAddEditRes = temp.data;
        if (temp == "err") {
          // var catUpdate: Toast = {
          //   type: 'error',
          //   title: "Category",
          //   body: "Unable to update category.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(catUpdate);

        this.toastr.error('Unable to update category.', 'Error', {
          timeOut: 0,
          closeButton: true
       });
       this.catShow=false

        } else if (temp.type == false) {
          // var catUpdate: Toast = {
          //   type: 'error',
          //   title: "Category Name Already Exist",
          //   body: this.categoryAddEditRes.msg,
          //   // body: "Category Name Already Exist",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(catUpdate);

          this.toastr.error( 'Category name already exist', 'Error', {
            timeOut: 0,
            closeButton: true
    });
    // this.catShow=false

        } else {
          // this.router.navigate(['/pages/plan/courses/category']);
    this.catShow=false
    if(this.showOld == true){
    this.getCategories(this.str, this.currentPage);
    }else{
    this.getCourseCat(this.parentCatId,this.currentPage)
    }

          // var catUpdate: Toast = {
          //   type: 'success',
          //   title: "Category",
          //   body: this.categoryAddEditRes.msg,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(catUpdate);
          if(this.categoryId==0){
    this.catShow=false
    this.categoryImgData=undefined
          this.toastr.success( 'Category Created Sucessfully ', 'Success', {
            closeButton: false
          });
        }
        else{
    this.catShow=false
    this.categoryImgData=undefined
          this.toastr.success( 'Category Updated Sucessfully ', 'Success', {
            closeButton: false
          });
        }

        }
        console.log('Category AddEdit Result ', this.categoryAddEditRes)
      },
        resUserError => {
          // this.loader =false;
          this.spinner.hide();
          this.errorMsg = resUserError;
        });
  }

  catCodeDupliRes: any;

  // onSubmit(f){
  //   if(f.valid){

  //   } else{
  //     console.log('Please Fill all fields');
  //     Object.keys( f.controls).forEach(key => {
  //       f.controls[key].markAsDirty();
  //      });
  //   }
  // }


  submit(f) {
    // this.loader = true;
    // this.spinner.show();
    if (f.valid) {
      // this.checkCategoryValid();
      this.catcheck();
      // var catData = {
      //   categoryId: this.formdata.categoryId,
      //   // categoryCode : this.formdata.categoryCode,
      //   categoryName: this.formdata.categoryName,
      // };
      // let url = webApi.domain + webApi.url.checkCategory;
      // console.log('category Data ', catData);
      // this.webApiService.getService(url, catData)
      // .then(rescompData => {
      //   console.log(rescompData);
      //   // this.loader =false;
      //   this.spinner.hide();
      //   this.catCodeDupliRes = rescompData;
      //   console.log('Category Code duplication result ', this.catCodeDupliRes);
      //   if (this.catCodeDupliRes.catCode.isPresent == "true") {
      //     var codeCheck: Toast = {
      //       type: 'error',
      //       title: 'Category',
      //       body: this.catCodeDupliRes.catCode.msg,
      //       showCloseButton: true,
      //       timeout: 2000
      //     };
      //     this.toasterService.pop(codeCheck);
      //   } else {
      //     this.checkCategoryValid();
      //   }
      //   this.cdf.detectChanges();
      // },
      //   resUserError => {
      //     // this.loader =false;
      //     this.spinner.hide();
      //     this.errorMsg = resUserError
      //   });
    } else{
      console.log('Please Fill all fields');
      // const addEditF: Toast = {
      //   type: 'error',
      //   title: 'Unable to update',
      //   body: 'Please Fill all fields',
      //   showCloseButton: true,
      //   timeout: 2000
      // };
      // this.toasterService.pop(addEditF);

      this.toastr.warning( 'Please fill in the required fields', 'Warning', {
        closeButton: false
      })
      Object.keys( f.controls).forEach(key => {
        f.controls[key].markAsDirty();
       });
    }
  }
  catcheck() {
    var catData = {
      categoryId: this.formdata.categoryId,
      // categoryCode : this.formdata.categoryCode,
      categoryName: this.formdata.categoryName,
    };
    console.log(catData);
    const _urlCheckCatCode:string = webApi.domain + webApi.url.checkCategory;
    this.commonFunctionService.httpPostRequest(_urlCheckCatCode,catData)
    //this.service.checkCategory(catData)
    .then(res => {
      if (res['data'][0][0]['isPresent'] === 'true') {
        // var codeCheck: Toast = {
        //   type: 'error',
        //   title: 'Category',
        //   body: res['data'][0][0]['msg'],
        //   showCloseButton: true,
        //   timeout: 2000,
        //   // positionClass: 'toast-top-right'
        // };
        // this.toasterService.pop(codeCheck);

        this.toastr.error( res['data'][0][0]['msg'], 'Error', {
          timeOut: 0,
          closeButton: true
  });

      } else {
        this.checkCategoryValid();
      }
      this.cdf.detectChanges();
    }, err => {
      console.log(err);
    });
  }

  checkCategoryValid() {
    // this.loader = true;
    this.spinner.show();
    if (this.selectedTags.length > 0) {
      this.makeTagDataReady(this.selectedTags);
       // this.formdata.tags = this.formattedTags;
     }
    var category = {
      categoryId: this.formdata.categoryId,
      categoryName: this.formdata.categoryName,
      categoryCode: this.formdata.categoryCode,
      description: this.formdata.description,
      // categoryPicRef : this.categoryImgData == undefined ? null : this.formdata.categoryPicRef,
      categoryPicRef: this.formdata.categoryPicRef,
      visible: this.formdata.visible,
      tenantId: this.formdata.tenantId,
      tags:this.formdata.tags,
      parCatId:this.parentCatId,
      catTypeId:1
      // tagsList : this.formdata.tags,
    }

    var fd = new FormData();
    fd.append('content', JSON.stringify(category));
    fd.append('file', this.categoryImgData);
    console.log('File Data ', fd);

    console.log('Category Data Img', this.categoryImgData);
    console.log('Category Data ', category);

    let url = webApi.domain + webApi.url.addEditCategory;
    let fileUploadUrl = webApi.domain + webApi.url.fileUpload;
    let param = {
      tId: this.tenantId,
    }

    if (this.categoryImgData != undefined) {
      this.webApiService.getService(fileUploadUrl, fd)
        .then(rescompData => {
          // this.loader =false;
          this.spinner.hide();
          var temp: any = rescompData;
          // this.fileUploadRes = JSON.parse(temp);
          this.fileUploadRes = temp;
          if (temp == "err") {
            // var thumbUpload: Toast = {
            //   type: 'error',
            //   title: "Category Thumbnail",
            //   body: "Unable to upload category thumbnail.",
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(thumbUpload);

            this.toastr.error( 'Unable to upload category image.', 'Error', {
              timeOut: 0,
              closeButton: true
      });

          } else if (temp.type == false) {
            // var thumbUpload: Toast = {
            //   type: 'error',
            //   title: "Category Thumbnail",
            //   body: "Unable to upload category thumbnail.",
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(thumbUpload);

            this.toastr.error( 'Unable to upload category image.', 'Error', {
              timeOut: 0,
              closeButton: true
            });
          }
          else {
            if (this.fileUploadRes.data != null || this.fileUploadRes.fileError != true) {
              category.categoryPicRef = this.fileUploadRes.data.file_url;
              this.addUpdateCategory(url, category);
            } else {
              // var thumbUpload: Toast = {
              //   type: 'error',
              //   title: "Category Thumbnail",
              //   body: this.fileUploadRes.status,
              //   showCloseButton: true,
              //   timeout: 2000
              // };
              // this.toasterService.pop(thumbUpload);


            this.toastr.error( this.fileUploadRes.status , 'Error', {
              timeOut: 0,
              closeButton: true
          });
            }
          }
          console.log('File Upload Result', this.fileUploadRes)
        },
          resUserError => {
            // this.loader =false;
            this.spinner.hide();
            this.errorMsg = resUserError;
          });
    } else {
      this.addUpdateCategory(url, category);
    }
  }

  // Help Code Start Here //

  helpContent: any;


  // Tag cganges

onTagsSelect(item: any) {
  console.log(item);
  console.log(this.selectedTags);
}
OnTagDeSelect(item: any) {
  console.log(item);
  console.log(this.selectedTags);
}
onTagSearch(evt: any) {
  console.log(evt.target.value);
  const val = evt.target.value;
  if(val.length>=3||val.length==0){
  this.tagList = [];
  const temp = this.tempTags.filter(function(d) {
    return (
      String(d.name)
        .toLowerCase()
        .indexOf(val) !== -1 ||
      !val
    );

  });

  // update the rows
  this.tagList = temp;
  console.log('filtered Tag LIst',this.tagList);
}
}


  // getCategories() {
  //   this.spinner.show();
  //   // let url = webApi.domain + webApi.url.getCategories;
  //
  //   let param = {
  //     tId:  this.tenantId,
  //   };

  //   this.webApiService.getService(url, param)
  //     .then(rescompData => {
  //       console.log(rescompData);
  //       // this.loader =false;
  //       this.spinner.hide();
  //       this.notFound = false;
  //       var temp: any = rescompData;
  //       if (temp == "err") {
  //         this.notFound = true;
  //       } else {
  //         this.category = temp.data;
  //       }
  //       // console.log('Category Result',rescompData)
  //     },
  //       resUserError => {
  //         // this.loader =false;
  //         this.spinner.hide();
  //         if (resUserError.statusText == "Unauthorized") {
  //           this.router.navigate(['/login']);
  //         }
  //         this.errorMsg = resUserError;
  //         this.notFound = true;
  //       });
  // }

  getCategories(str, pageNo) {
    // this.spinner.show();
    // let url = webApi.domain + webApi.url.getCategories;
    this.skeleton = false;
    this.notFound=false;
    var dataf =
    {
      tId: this.tenantId,
      lmt: this.pagelmt,
      pno: pageNo,
      str: str,
    };
    const urlGetCategory = webApi.domain + webApi.url.getCategoriesNew;
    this.commonFunctionService.httpPostRequest(urlGetCategory,dataf)
    //this.categoryService.getCategoriesPagination(dataf)
      .then(rescompData => {
        // console.log(rescompData);
        // // this.loader =false;
        // this.spinner.hide();
        // this.notFound = false;
        // var temp: any = rescompData;
        // if (temp == "err") {
        //   this.notFound = true;
        // } else {
        //   this.category = temp.data;
        // }
        // console.log('Category Result',rescompData)
          // this.spinner.hide();

          //  this.notFound = true;
          console.log('Category Result:', rescompData);
          // if (rescompData['data'].length > 0) {
            // this.skeleton = false;
          if (rescompData['type'] === true) {
            this.totalPages = rescompData['data']['totalPages'];
            const list = rescompData['data']['list'];
            if (this.category.length > 0 && pageNo > 1) {
              this.category = this.category.concat(list);
            }else {
              this.category = list;
            }
            if(this.category.length==0){
              this.notFound=true
              this.noDataVal={
                margin:'mt-5',
                imageSrc: '../../../../../assets/images/no-data-bg.svg',
                title:"Sorry we couldn't find any matches please try again",
                desc:".",
                titleShow:true,
                btnShow:false,
                descShow:false,
                btnText:'Learn More',
                btnLink:''
              }
            }
            this.isworking = false;
            // console.log(this.feedback);
            // feedback=this.
            // if (this.feedback) {
            //   this.feedback = this.getUnique(this.feedback, 'fid')
            //   for (let j = 0; j < this.feedback.length; j++) {
            //     this.feedback[j].points = [];
            //     // for (let i = 0; i < feedback.length; i++) {
            //     if (this.feedback[j].fid == this.feedback[j].fid) {
            //       if (this.feedback[j].roleId) {
            //         var data =
            //         {
            //           crid: 0,
            //           roleId: this.feedback[j].roleId,
            //           pformatid: this.feedback[j].dimension,
            //           bcpoints: this.feedback[j].bcPoints,
            //           acpoints: this.feedback[j].acPoints,
            //         }
            //         this.feedback[j].points.push(data);
            //       }

            //     }
            //     // }
            //   }
            //   this.showContent();
            // }

            // console.log('Category Result', this.feedback);
          }
          else{
            this.notFound=true
          }
          this.spinner.hide();
          this.skeleton = true;
      },
        resUserError => {
          // this.loader =false;
          this.spinner.hide();
          if (resUserError.statusText == "Unauthorized") {
            this.router.navigate(['/login']);
          }
          this.errorMsg = resUserError;
          this.notFound = true;
          this.skeleton = true;
        });
  }

  ngAfterContentChecked() {
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0)
    {
      e1[0].classList.remove('with-scroll');
    }

    // const e = document.getElementsByTagName('nb-layout-column');
    // // console.log(e[0].clientHeight);
    // // this.conentHeight = String(e[0].clientHeight) + 'px';
    // if (e[0].clientHeight > 700) {
    //   this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
    // } else {
    //   this.conentHeight = String(e[0].clientHeight) + 'px';
    // }

  }

  // getFeedback(str, pageNo) {
  //   var dataf =
  //   {
  //     tId: this.loginUserdata.data.data.tenantId,
  //     lmt: this.pagelmt,
  //     pno: pageNo,
  //     str: str,
  //   }
  //   this.spinner.show();
  //   this.categoryService.getFeedbacks(dataf)
  //     .then(rescompData => {
  //       this.spinner.hide();
  //       //  this.notFound = true;
  //       console.log('All Feedback:', rescompData);
  //       // if (rescompData['data'].length > 0) {
  //       if (rescompData['type'] === true) {
  //         this.totalPages = rescompData['data']['totalPages'];
  //         const list = rescompData['data']['list'];
  //         if (this.feedback.length > 0 && pageNo > 1) {
  //           this.feedback = this.feedback.concat(list);
  //         } else {
  //           this.feedback = list;
  //         }
  //         this.isworking = false;
  //         // console.log(this.feedback);
  //         // feedback=this.
  //         if (this.feedback) {
  //           this.feedback = this.getUnique(this.feedback, 'fid')
  //           for (let j = 0; j < this.feedback.length; j++) {
  //             this.feedback[j].points = [];
  //             // for (let i = 0; i < feedback.length; i++) {
  //             if (this.feedback[j].fid == this.feedback[j].fid) {
  //               if (this.feedback[j].roleId) {
  //                 var data =
  //                 {
  //                   crid: 0,
  //                   roleId: this.feedback[j].roleId,
  //                   pformatid: this.feedback[j].dimension,
  //                   bcpoints: this.feedback[j].bcPoints,
  //                   acpoints: this.feedback[j].acPoints,
  //                 }
  //                 this.feedback[j].points.push(data);
  //               }

  //             }
  //             // }
  //           }
  //           this.showContent();
  //         }

  //         console.log('Category Result', this.feedback);
  //       }

  //       // this.notFound = true;
  //     },
  //       resUserError => {
  //         this.spinner.hide();
  //         this.loader = false;
  //         this.errorMsg = resUserError;
  //         this.notFound = false;
  //         // notFound
  //         // this.router.navigate(['**']);
  //       });
  // }
  clear() {
    if(this.str.length>=3){
      this.notFound=false
    this.search = {};
    this.currentPage=1;
    if(this.showOld == true){
    this.getCategories('',this.currentPage)
    }else{
      this.notFound = false
      this.category = this.categoryDummy
      this.courseList = this.courseListDummy
    }

    }
    else{
      this.search={};
    }
  }

  back() {
    this.filter = false
    if(this.parentCatId == null){
      this.countLevel = 0
      this.router.navigate(['/pages/learning']);
    }
    else{
      this.countLevel = this.countLevel-1
      this.parentCatId = this.oldParentCatId
      var index = this.breadcrumbArray.length - 1
      this.breadtitle = this.breadcrumbArray[index].name
      this.breadcrumbArray.pop()
      this.previousBreadCrumb.pop()
      this.getCourseCat(this.oldParentCatId,this.currentPage)
    }
  }

  content2(data, id) {
    var passData = {
      id: id,
      data: data
    }
    this.passService.data = passData;
    this.router.navigate(['/pages/plan/courses/content']);
  }


  gotoCarddetails(data){
    var passData = {
      id: 0,
      data: data
    }
    this.passService.data = passData;
    this.router.navigate(['/pages/plan/courses/content']);
  }
  btnName='Save';
  public addeditccategory(data, id) {
    // var data1 = {
    //   data: data,
    //   id: id,
    //   dropdownData: this.visibility,
    // }

    if (data == undefined) {
      // this.addCategoryService.data = data1;
      this.title = 'Add Category';
    // this.getallTagList(id,data);
    this.makeCategoryDataReady(id, data);
    // this.getDropdownList();
    // this.getHelpContent();
      this.catShow=true;
      // this.router.navigate(['/pages/plan/courses/addEditCategoryContent']);
    } else {
    // this.getallTagList(id,data);
    // this.getHelpContent();
    this.makeCategoryDataReady(id, data);
    this.courseCount = data.coursecount
    this.title = 'Edit Category';
    // this.getDropdownList();
      this.catShow=true;
      // this.addCategoryService.data = data1;
      // this.router.navigate(['/pages/plan/courses/addEditCategoryContent']);
    }

  }

  // gotoCardaddedit(id,data) {
    // var data1 = {
    //   data: data,
    //   id: 1,
    //   dropdownData: this.visibility,
    // }

    // this.data=data
    // this.id=1;
    // this.title = 'Edit Category';
    // this.catShow=true
    // this.getallTagList(id,data);
    // this.getDropdownList();
    // this.getHelpContent();
      // this.catShow=true;
    // this.addCategoryService.data = data1;
    // this.router.navigate(['/pages/plan/courses/addEditCategoryContent']);

  // }

  deleteCategoryModal: boolean = false;
  deleteCategoryData: any;
  deleteCategory(Category) {
    // console.log('Category content',Category);
    this.deleteCategoryData = Category;
    this.deleteCategoryModal = true;
  }

  deleteCategoryAction(actionType) {
    if (actionType == true) {
      this.closeDeleteCategoryModal();
    } else {
      this.closeDeleteCategoryModal();
    }
  }

  closeDeleteCategoryModal() {
    this.deleteCategoryModal = false;
  }

  disableCat: boolean = false;
  visibiltyRes: any;
  enableDisableCategoryModal: boolean = false;
  enableDisableCategoryData: any;
  enableCat: boolean = false;
  catDisableIndex: any;
// btnName:string='/Yes;'
  disableCategory(currentIndex, categoryData, status) {
    console.log(categoryData);
    if (categoryData.coursecount > 0) {
      // var catUpdate: Toast = {
      //   type: 'error',
      //   title: "Category",
      //   body: "Sorry Unable to update visibility of category.",
      //   showCloseButton: true,
      //   timeout: 2000,
      // };
      // this.toasterService.pop(catUpdate);

      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.enableDisableCategoryData = categoryData;
      this.enableDisableCategoryModal = true;
    }
    this.catDisableIndex = currentIndex;
    if (categoryData.visible == 0) {
      this.enableCat = false;
    } else {
      this.enableCat = true;
    }
    // if (this.category[currentIndex].visible == 0) {
    //   this.enableCat = false;
    // } else {
    //   this.enableCat = true;
    // }
  }

  clickTodisable(categoryData) {
    if(categoryData.coursecount>0){
      this.presentToast('warning', 'Category have courses, not able to hide this category');
    }
    else if (categoryData.visible == 1) {
      this.title="Disable category"
      this.enableCat = true;
      this.enableDisableCategoryData = categoryData;
      this.enableDisableCategoryModal = true;
    } else {
      this.title="Enable category"
      this.enableCat = false;
      this.enableDisableCategoryData = categoryData;
      this.enableDisableCategoryModal = true;
    }
  }

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }

  enableDisableCategory(i,categoryData) {
    if(categoryData.coursecount>0){
      this.presentToast('warning', 'Category have courses, not able to hide this category');
    }else{
    console.log(categoryData);
    // this.spinner.show();
    var visibilityData = {
      categoryId: categoryData.categoryId,
      visible: categoryData.visible==1?0:1
    };
if(this.category[i].visible==0){
this.category[i].visible=1;
// this.category[i].categoryId=1;

}else{
  this.category[i].visible=0;
  // this.category[i].categoryId=0;
}
    const _urlDisableCategory:string = webApi.domain + webApi.url.disableCategory;
    this.commonFunctionService.httpPostRequest(_urlDisableCategory,visibilityData)

    //this.categoryService.disableCategory(visibilityData)
      .then(rescompData => {
        // this.loader =false;
        // this.spinner.hide();
        this.visibiltyRes = rescompData;
        // console.log('Category Visibility Result',this.visibiltyRes);
        if (this.visibiltyRes.type == false) {
          // var catUpdate: Toast = {
          //   type: 'error',
          //   title: "Category",
          //   body: "Unable to update visibility of category.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.closeEnableDisableCategoryModal();
          // this.toasterService.pop(catUpdate);

          this.toastr.error( 'Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        } else {
          // var catUpdate: Toast = {
          //   type: 'success',
          //   title: "Category",
          //   body: this.visibiltyRes.data,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.enableDisableCategoryData.visible == 0;
          // this.closeEnableDisableCategoryModal();
          // this.toasterService.pop(catUpdate);

          this.toastr.success(this.visibiltyRes.data, 'Success', {
            closeButton: false
           });
        }
      },
        resUserError => {
          // this.loader =false;
          // this.spinner.hide();
          if (resUserError.statusText == "Unauthorized") {
            this.router.navigate(['/login']);
          }
          this.errorMsg = resUserError;
          this.closeEnableDisableCategoryModal();
        });
      }
  }

  enableDisableCategoryAction(actionType) {
    // console.log(this.category[this.catDisableIndex]);
    console.log(this.enableDisableCategoryData);
    if (actionType == true) {
      // if (this.category[this.catDisableIndex].visible == 1) {
      //   this.category[this.catDisableIndex].visible = 0;
      //   var catData = this.category[this.catDisableIndex];
      //   this.enableDisableCategory(catData);
      // } else {
      //   this.category[this.catDisableIndex].visible = 1;
      //   var catData = this.category[this.catDisableIndex];
      //   this.enableDisableCategory(catData);
      // }
      if (this.enableDisableCategoryData.visible == 1) {
        this.enableDisableCategoryData.visible = 0;
        var catData = this.enableDisableCategoryData;
        // this.enableDisableCategory(catData);
      } else {
        this.enableDisableCategoryData.visible = 1;
        var catData = this.enableDisableCategoryData;
        // this.enableDisableCategory(catData);
      }
    } else {
      this.closeEnableDisableCategoryModal();
    }
  }

  closeEnableDisableCategoryModal() {
    this.enableDisableCategoryModal = false;
  }


  onScroll(event) {
    let element = this.myScrollContainer.nativeElement;
    // element.style.height = '500px';
    // element.style.height = '500px';
    // Math.ceil(element.scrollHeight - element.scrollTop) === element.clientHeight
    if (element.scrollHeight - element.scrollTop - element.clientHeight < 20) {
      if (!this.isworking) {
        this.currentPage++;
        if (this.currentPage <= this.totalPages) {
          this.isworking = true;
          this.skeleton = false;
          this.getCategories(this.str, this.currentPage);
        }
      }
    }
  }

  // Help Code Start Here //
  getHelpContent() {
    return new Promise(resolve => {

      this.http1.get('../../../../../../assets/help-content/addEditCourseContent.json').subscribe(
        data => {
          this.helpContent = data;
          console.log('Help Array', this.helpContent);
        },
        err => {
          resolve('err');
        },
      );
    });
    // return this.helpContent;
  }

  onsearch(evt: any) {
    console.log(evt);
    // this.search = evt.target.value;
    this.str = evt.target.value;
    const val=this.str;

    if(val.length>=3 || val.length==0) {
      this.currentPage = 1;
      if(this.showOld == true){
      this.getCategories(this.str, this.currentPage);
      }else{
        var temData = this.categoryDummy;
        var course = this.courseListDummy
        var keys = []
        var coursekeys = []
        //category

        if (temData.length > 0) {
          keys = Object.keys(temData[0]);
        }
        if (val.length >= 3 || val.length == 0) {
          var temp = temData.filter((d) => {
            for (const key of keys) {
              if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
                return true;
              }
            }
          });
        this.category = temp
      }

      //course
        if (course.length > 0) {
          coursekeys = Object.keys(course[0]);
        }
        if (val.length >= 3 || val.length == 0) {
          var coursetemp = course.filter((d) => {
            for (const key of coursekeys) {
              if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
                return true;
              }
            }
          });
        this.courseList = coursetemp
      }
      if(this.category.length == 0 && this.courseList.length == 0){
        this.notFound = true
      }else{
        this.notFound = false
      }
      // end
      }

    }
  }
  // Help Code Ends Here //
  getUrl(string)
  {
    return 'url(' + string + ')';
  }


  //new structure code
  contextItem(event,data) {

    console.log(event,data);
    var catData = event.item

    if (this.dummy == 'Enable' || this.dummy == 'Disable'){
      this.enableDisableCategory(this.index,catData)
    }
    if(this.dummy == 'Edit'){
      this.addeditccategory(catData,1)

    }
    if(this.dummy == 'Duplicate'){
      this.copyCard(catData)

    }




  }

  View(item,index){
    console.log(item,"cgffgxfgx")
    this.dummy = item;
    this.index = index

    }

    getCourseCat(id, pageNo) {
      pageNo = 1
      // this.spinner.show();
      // this.cdf.detectChanges();
      if(this.parentCatId == null){
        this.header = {
        title:'Course Category',
        btnsSearch: true,
        placeHolder:"Search by category name",
        searchBar: true,
        dropdownlabel: ' ',
        drplabelshow: false,
        drpName1: '',
        drpName2: ' ',
        drpName3: '',
        drpName1show: false,
        drpName2show: false,
        drpName3show: false,
        btnName1: '',
        btnName2: '',
        btnName3: '',
        btnAdd: 'Add Category',
        btnName1show: false,
        btnName2show: false,
        btnName3show: false,
        btnBackshow: true,
        btnAddshow: true,
        filter: false,
        showBreadcrumb: true,
        breadCrumbList:this.breadcrumbArray
      };
      }
      else{
        this.header = {
        title:this.breadtitle,
        btnsSearch: true,
        placeHolder:"Search by category name",
        searchBar: true,
        dropdownlabel: ' ',
        drplabelshow: false,
        drpName1: '',
        drpName2: ' ',
        drpName3: '',
        drpName1show: false,
        drpName2show: false,
        drpName3show: false,
        btnName1: 'Add Course',
        btnName2: '',
        btnName3: '',
        btnAdd: 'Add Category',
        btnName1show: true,
        btnName2show: false,
        btnName3show: false,
        btnBackshow: true,
        btnAddshow: true,
        filter: true,
        showBreadcrumb: true,
        breadCrumbList:this.breadcrumbArray
        }
      }
      if(this.countLevel>3){
        this.header.btnAddshow = false
        // this.header.btnName1show = false
      }
      // this.spinner.show();
      // let url = webApi.domain + webApi.url.getCategories;
      this.skeleton = false;
      this.notFound=false;
      var dataf =
      {
        courseTypeId:1,
        catId:id,
        wfId:null
      };
      const urlGetCategory = webApi.domain + webApi.url.getCourseCat;
      this.commonFunctionService.httpPostRequest(urlGetCategory,dataf)
      //this.categoryService.getCategoriesPagination(dataf)
        .then(rescompData => {
          // console.log(rescompData);
          // // this.loader =false;
          // this.spinner.hide();
          // this.notFound = false;
          // var temp: any = rescompData;
          // if (temp == "err") {
          //   this.notFound = true;
          // } else {
          //   this.category = temp.data;
          // }
          // console.log('Category Result',rescompData)
            // this.spinner.hide();

            //  this.notFound = true;
            console.log('Category Result:', rescompData);
            // if (rescompData['data'].length > 0) {
              // this.skeleton = false;
            if (rescompData['type'] === true) {
              this.spinner.hide();
              // this.totalPages = rescompData['data']['totalPages'];
              const list = rescompData['list'][1];
              const list1 = rescompData['list'][2];

              if (rescompData['list'][0].length === 0) {
                this.oldParentCatId = null;
              } else {
                this.oldParentCatId = rescompData['list'][0][0].parentCatId;
              }
              if (this.category.length > 0 && pageNo > 1) {
                this.category = this.category.concat(list);
              }else {
                this.category = list;
                this.categoryDummy = list
                this.catSearch = list
                this.courseList = list1
                this.courseListDummy = list1
                this.courseSearch = list1

              }
              if(this.category.length==0 && this.courseList.length == 0){
                this.notFound=true
                this.noDataVal={
                  margin:'mt-3',
                  imageSrc: '../../../assets/images/no-data-bg.svg',
                  title:'No Courses at this time',
                  desc:'Courses will appear after they are added by the instructor. Courses are a bundle of learning activities which are designed by the instructors to enable self-paced online learning',
                  titleShow:true,
                  btnShow:true,
                  descShow:true,
                  btnText:'Learn More',
                  btnLink:'https://faq.edgelearning.co.in/kb/setting-how-to-create-an-online-course',
                }
              }
              this.isworking = false;
              // console.log(this.feedback);
              // feedback=this.
              // if (this.feedback) {
              //   this.feedback = this.getUnique(this.feedback, 'fid')
              //   for (let j = 0; j < this.feedback.length; j++) {
              //     this.feedback[j].points = [];
              //     // for (let i = 0; i < feedback.length; i++) {
              //     if (this.feedback[j].fid == this.feedback[j].fid) {
              //       if (this.feedback[j].roleId) {
              //         var data =
              //         {
              //           crid: 0,
              //           roleId: this.feedback[j].roleId,
              //           pformatid: this.feedback[j].dimension,
              //           bcpoints: this.feedback[j].bcPoints,
              //           acpoints: this.feedback[j].acPoints,
              //         }
              //         this.feedback[j].points.push(data);
              //       }

              //     }
              //     // }
              //   }
              //   this.showContent();
              // }

              // console.log('Category Result', this.feedback);
            }
            else{
              this.notFound=true
            }
            // this.spinner.hide();
            this.skeleton = true;
        },
          resUserError => {
            // this.loader =false;
            this.spinner.hide();
            if (resUserError.statusText == "Unauthorized") {
              this.router.navigate(['/login']);
            }
            this.errorMsg = resUserError;
            this.notFound = true;
            this.skeleton = true;
          });
    }


    getAllSubCat(item){
      this.filter = false
      this.parentCatId = item.categoryId
      this.categoryId = this.parentCatId
      this.categoryName = item.categoryName
      this.countLevel = this.countLevel+1
      this.skeleton = false

      //breadCrumb
      this.breadtitle = item.categoryName
      console.log('assetData', item);
      //// breadCrumb
      this.breadObj = {
        'name': item.categoryName,
        // 'navigationPath': '',
        'id': item,
        'navigationPath': '/pages/plan/courses/category',
        'sameComp': true,
        'assetId': item.categoryId
      }

      this.previousBreadCrumb.push(this.breadObj)

      this.breadcrumbArray.push(this.breadObj)
      for (let i = 0; i < this.previousBreadCrumb.length; i++) {
        if (this.breadtitle == this.previousBreadCrumb[i].name) {
          this.breadcrumbArray = this.previousBreadCrumb.slice(0, i)
        }
      }
      //End BreadCrumb
      this.getCourseCat(this.parentCatId,this.currentPage)

    }

    enableDisableCourse(courseData){
      var visibilityData = {
        courseId: courseData.courseId,
        visible: courseData.visible==1?0:1,
      }

      const _urlDisableCourse: string = webApi.domain + webApi.url.disableCourse;
      this.commonFunctionService.httpPostRequest(_urlDisableCourse,visibilityData)
      .then(rescompData => {
        // this.loader =false;
        this.visibiltyRes = rescompData;
        // this.spinner.hide();
        //this.showSpinner =false
        console.log('Course Visibility Result', this.visibiltyRes);
        if (this.visibiltyRes.type == false) {
          // var courseUpdate: Toast = {
          //   type: 'error',
          //   title: "Course",
          //   body: "Unable to update visibility of course.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.closeEnableDisableCourseModal();
          // this.toasterService.pop(courseUpdate);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        } else {
          // var courseUpdate: Toast = {
          //   type: 'success',
          //   title: "Course",
          //   body: this.visibiltyRes.data,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.spinner.hide();
          //this.showSpinner = false
          // this.closeEnableDisableCourseModal();
          // this.toasterService.pop(courseUpdate);


          this.toastr.success(this.visibiltyRes.data, 'Success', {
            closeButton: false
          });
        this.getCourseCat(this.parentCatId,this.currentPage)

        }
      },
        resUserError => {
          // this.loader =false;
          this.errorMsg = resUserError;
          this.skeleton=true;
          // this.skeletonF=true;
          // this.spinner.hide();
          //this.showSpinner = false
          // this.closeEnableDisableCourseModal();
        });

}
copyCard(course) {
  //this.spinner.show()
  console.log('Course content', course);
  this.duplicateCourseData = course;
  this.duplicateCourseModal = true;
  this.title="Duplicate course "+this.duplicateCourseData.fullname
  this.btnName="Duplicate"
  this.duplicateContent(course);

}
formDataCourse: any;
duplicateContent(courseData) {
  this.formDataCourse = {
    courseId: courseData.courseId,
    fullname: '',
    tenantId: courseData.tenantId,
  }
  // this.spinner.hide()
  //this.showSpinner = false
}

duplicateCourseAction(actionType) {
  // this.spinner.show()
  //this.showSpinner = false
  if (actionType == true) {
    this.makeDuplicateCourseDataReady();
    this.closeDuplicateCourseModal();
  } else {
    // this.spinner.show()
    this.closeDuplicateCourseModal();
  }
  // this.spinner.hide()
 // this.showSpinner = false
}

closeDuplicateCourseModal() {
  this.duplicateCourseModal = false;
}

creditPointsStr: any;
makeDuplicateCourseDataReady() {
  var courseData = {
    courseId: this.formDataCourse.courseId,
    fullname: this.formDataCourse.fullname,
    userMod: this.userData.data.data.id,
  }
  console.log('Duplicate course data ', courseData);

  this.duplicateCourse(courseData);
}

duplicateCourseRes: any;
duplicateCourse(course) {
  // this.showSpinner = true
  this.spinner.show();
  const _urlDublicateCourse: string = webApi.domain + webApi.url.duplicateCourse;
  this.commonFunctionService.httpPostRequest(_urlDublicateCourse,course)
  //this.contentservice.dublicateCourse(course)
    .then(rescompData => {
      this.loader = false;
      var temp: any = rescompData;
      this.duplicateCourseRes = temp.data;
      if (temp == "err") {
        // this.showSpinner = false
        // this.spinner.hide()
        //this.showSpinner =false
        // this.notFound = true;
        // var courseDuplicate: Toast = {
        //   type: 'error',
        //   title: "Course",
        //   body: "Unable to duplicate course.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(courseDuplicate);

        this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
          timeOut: 0,
          closeButton: true
        });
      } else if (temp.type == false) {
        // this.spinner.hide()
        // this.showSpinner = false
        // var courseDuplicate: Toast = {
        //   type: 'error',
        //   title: "Course",
        //   body: this.duplicateCourseRes[0].msg,
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(courseDuplicate);

        this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
          timeOut: 0,
          closeButton: true
        });
      } else {
        // this.spinner.hide()
        // this.showSpinner = false
        // var courseDuplicate: Toast = {
        //   type: 'success',
        //   title: "Course",
        //   body: this.duplicateCourseRes[0].msg,
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(courseDuplicate);

        this.toastr.success(this.duplicateCourseRes[0].msg, 'Success', {
          closeButton: false
        });
        // this.getCourses(this.coursedata);
        this.getCourseCat(this.parentCatId,this.currentPage)
      }
      console.log('Course duplicate Result ', this.duplicateCourseRes);
      // this.cdf.detectChanges();
    },
      resUserError => {
        this.loader = false;
        this.errorMsg = resUserError;
        this.skeleton=true;
        // this.skeletonF=true;
        this.spinner.hide()
        // this.showSpinner = false
      });
}

courseDropdownList: any = [];
getCourseDropdownList() {
  const param = {
    tId: this.tenantId,
  }
  const _urlGetCourseDropdown: string = webApi.domain + webApi.url.getCourseDropdown;
  this.commonFunctionService.httpPostRequest(_urlGetCourseDropdown,param)
  // this.spinner.show();
  //this.detailsService.getDropdownList(param)
  .then(rescompData => {
    // this.loader =false;
    this.courseDropdownList = rescompData['data'];
    console.log('Course Dropdown List ', this.courseDropdownList);
    this.makeCourseDetailsDropdownReady();
  },
    resUserError => {
      // this.loader =false;
      this.errorMsg = resUserError;
      // this.skeleton=true;
      // this.skeletonF=true;
    });
}

makeCourseDetailsDropdownReady() {
  // this.spinner.hide();
  // this.showSpinner =false
  this.categoryData = this.courseDropdownList.catList;
  this.courseFormat = this.courseDropdownList.courseType;
  this.courseLevel = this.courseDropdownList.courseLevel;
  // this.visibility = this.courseDropdownList.visibility;
  this.daysBucket = this.courseDropdownList.leadTime;
  this.userRoles = this.courseDropdownList.userRoles;
  this.creatorList = this.courseDropdownList.creatorList;
  this.workflowData = this.courseDropdownList.lpList;
  for (let i = 0; i < this.courseLevel.length; i++) {
    this.courseLevel[i].courseLevelId = this.courseLevel[i].id;
  }
  for (let i = 0; i < this.creatorList.length; i++) {
    this.creatorList[i].creatorId = this.creatorList[i].id;
  }
  // this.bindfilter(this.categoryData);
  this.bindfilter(this.courseLevel);
  this.bindfilter(this.creatorList);
  this.bindfilter(this.workflowData);
  this.bindfilter(this.sortdata);
}

 addeditcontent(data, id) {
  if(!this.categoryName){
    this.categoryName=this.passService1.previousBreadCrumb[this.passService1.previousBreadCrumb.length-1].id.categoryName
    this.categoryId=this.passService1.previousBreadCrumb[this.passService1.previousBreadCrumb.length-1].id.categoryId
  }
  var data1 = {
    data: data,
    id: id,
    catId: this.categoryId,
    categoryName:this.categoryName,
    courseDropdowns: this.courseDropdownList,
  };
  if (data == undefined) {
    this.passService1.data = data1;
    this.passService.parentCatId = this.parentCatId
    // this.passService.categoryName = this.categoryName
    this.passService.countLevel = this.countLevel
    this.passService1.breadtitle = 'Add Course'
    this.passService1.previousBreadCrumb = this.previousBreadCrumb
    this.passService1.breadcrumbArray = this.previousBreadCrumb
    this.router.navigate(['/pages/plan/courses/addEditCourseContent']);
    console.log("Data passed to service" + data1);
    console.log("ID Passed" + data1.id + " " + data1.data);
  } else {
    this.passService1.data = data1;
    this.passService.parentCatId = this.parentCatId
    this.passService.countLevel = this.countLevel
  this.router.navigate(['/pages/plan/courses/addEditCourseContent']);
}

}

gotoCardaddedit(data) {
  var data1 = {
    data: data,
    id: 1,
    catId: this.categoryId,
    categoryName:this.categoryName,
    courseDropdowns: this.courseDropdownList,
  };
  this.breadtitle = data.fullname
  this.breadObj = {
    'name': data.fullname,
    // 'navigationPath': '',
    'id': data,
    'navigationPath': '/pages/plan/courses/category',
    'sameComp': true,
    'assetId': data.categoryId
  }

  this.previousBreadCrumb.push(this.breadObj)

  this.breadcrumbArray.push(this.breadObj)
  for (let i = 0; i < this.previousBreadCrumb.length; i++) {
    if (this.breadtitle == this.previousBreadCrumb[i].name) {
      this.breadcrumbArray = this.previousBreadCrumb.slice(0, i)
    }
  }
  this.passService.parentCatId = this.parentCatId
  this.passService.countLevel = this.countLevel
  this.passService1.data = data1;
  this.passService1.breadtitle = this.breadtitle
  this.passService1.breadcrumbArray = this.breadcrumbArray
  this.passService1.previousBreadCrumb = this.previousBreadCrumb
  this.router.navigate(['/pages/plan/courses/addEditCourseContent']);
}

breadcrumbNavigate(data){
  this.filter = false
  console.log(data,"navigationData")
  this.breadtitle = data.categoryName
  this.parentCatId = data.categoryId;
  this.countLevel = data['index']-1
  this.breadcrumbArray =  this.breadcrumbArray.slice(0,data.index)
  this.previousBreadCrumb = this.previousBreadCrumb.slice(0,data.index+1)
  console.log(this.breadcrumbArray,"breadcrumbArray")
  console.log(this.previousBreadCrumb,"previousBreadCrumb")


  // this.header.breadCrumbList =  this.breadcrumbArray
  // this.parentCatId = this.oldParentCatId
  this.getCourseCat(this.parentCatId,this.currentPage);

}


//filters
bindfilter(obj) {
  let filtername, filterValueName, type, singleSelection;
  if (obj.length > 0) {
    filtername = obj[0]['filterId'];
    filterValueName = obj[0]['filterValue'];
    type = obj[0]['type'];
    singleSelection = obj[0]['singleSelection'];
  }
      const item = {
        count: "",
        value: "",
        tagname: obj.length > 0 ? obj[0]['filterId'] : '',
        isChecked: false,
        list: obj,
        filterValue: filterValueName,
        type: type,
        filterId: filtername,
        singleSelection: singleSelection,
      }
      if (filtername) {
        this.filters.push(item);
      }
  // if (result['data'] && result['data'].length > 0) {
  //   result['data'].forEach( (value, index) => {

  //   })
  // }
}
filteredChanged(event){
  if (event.empty) {
    this.courseList = this.courseListDummy;
    // this.folder = this.tempFolders;
  } else {
    // this.filterFolder(event);
    this.filterFiles(event);
  }
  this.courseSearch = this.courseList;
  this.catSearch = this.category;
}

filterFiles(event) {
  this.courseList = [];
  let indexObj = {};
  let filterId;
  let filterValue;
  const findElement = (element) => String(element[filterId]).toLowerCase() === String(filterValue).toLowerCase();
  // tslint:disable-next-line:forin
  for (const x in event) {
      filterId = x;
      if (event[x] && Array.isArray(event[x])) {
        event[x].forEach((value, key) => {
          filterValue = value[filterId];
          // for(let i = 0; i<this.tempFiles.length; i++) {
          //   // const indexnumber = this.tempFiles.findIndex(findElement);
          //   // indexObj[indexnumber] = true;
          // }
          let indexnumberArray = [];
          if (filterId.startsWith('tag')) {
            // tslint:disable-next-line:max-line-length
            indexnumberArray = this.courseListDummy.map((e, j) => String(e[filterId]).includes(String(filterValue).toLowerCase()) ? j : '').filter(String);
          } else {
          // tslint:disable-next-line:max-line-length
            indexnumberArray = this.courseListDummy.map((e, j) => String(e[filterId]).toLowerCase() === String(filterValue).toLowerCase() ? j : '').filter(String);
// indexObj[indexnumber] = true;
          }
          indexObj = Object.assign(indexObj, indexnumberArray.reduce((a,b)=> (a[b]=true,a),{}));
          // const indexnumber = this.tempFiles.findIndex(findElement);
          // indexObj[indexnumber] = true;
      });
      }
  }
  console.log(JSON.stringify(indexObj));
  // tslint:disable-next-line:forin
  for (const y in indexObj) {
    if (String(y) !== '-1'){
      this.courseList.push(this.courseListDummy[y]);
    }
  }
}
filterFolder(event:any) {
  this.category  = [];
  let indexObj = {};
  let filterValue;
  let filterId;
  // const findElement = (element) => element[filterId] === filterValue;
  // const findElement = map((e, i) => e[filterId] === filterValue ? i : '').filter(String);
  // tslint:disable-next-line:forin
  for (const x in event) {
      filterId = x;
      if (event[x] && Array.isArray(event[x])) {
        event[x].forEach((value, key) => {
            filterValue = value[filterId];
            // for(let i = 0; i<this.tempFolders.length; i++) {
            //   // const indexnumber = this.tempFolders.findIndex(findElement);
            //   // tslint:disable-next-line:max-line-length
            //   const indexnumberArray =
            // this.tempFolders.map((e, j) => e[filterId] === filterValue ? j : '').filter(String);
            //   // indexObj[indexnumber] = true;
            //   indexObj = indexnumberArray.reduce((a,b)=> (a[b]=true,a),{});
            // }
            let indexnumberArray = [];
            if (filterId.startsWith('tag')) {
              // tslint:disable-next-line:max-line-length
              indexnumberArray = this.categoryDummy.map((e, j) => String(e[filterId]).includes(String(filterValue).toLowerCase()) ? j : '').filter(String);
            } else {
              // tslint:disable-next-line:max-line-length
              indexnumberArray = this.categoryDummy.map((e, j) => String(e[filterId]).toLowerCase() === String(filterValue).toLowerCase() ? j : '').filter(String);
            }
              // indexObj[indexnumber] = true;
              indexObj = Object.assign(indexObj, indexnumberArray.reduce((a,b)=> (a[b]=true,a),{}), indexObj);
        });
      }
  }
  console.log(JSON.stringify(indexObj));
  // tslint:disable-next-line:forin
  for (const y in indexObj) {
    if (String(y) !== '-1') {
      this.category.push(this.categoryDummy[y]);
    }
  }
}
SearchFilter(event) {
  const val = event == undefined ? '' : event.target.value.toLowerCase();
  // const files = this.files;
  let keys = [];
  if (val) {
    if (this.courseSearch.length > 0) {
      keys = Object.keys(this.courseSearch[0]);
    }
    if(val.length>=3||val.length==0) {
      const tempFiles = this.courseSearch.filter((d) => {
        for (const key of keys) {
          if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
            return true;
          }
        }
      });
      this.courseList = _.cloneDeep(tempFiles);
      const tempFolder = this.courseSearch.filter((d) => {
        for (const key of keys) {
          if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
            return true;
          }
        }
      });
      this.category = _.cloneDeep(tempFolder);
  } else {
    this.courseList = _.cloneDeep(this.courseSearch);
    this.category = _.cloneDeep(this.catSearch);
  }
} else {
  this.courseList = _.cloneDeep(this.courseSearch);
  this.category = _.cloneDeep(this.catSearch);
}
}

gotoFilter(){
  this.filter = !this.filter
  if(this.filter == false)
  this.courseList = this.courseSearch
}

getSortData(event){
  console.log(event,"sorting")
  if(event.selectedLevel == 1){
    var property = 'fullname'
  }else{
    var property = 'timecreated'
  }
  var array = this.courseList
  // console.log(this.displayArray,"huhuhuhu")
  var dummyArray = array.sort(this.commonFunctionService.sort(property,event.selectedRadio))
  console.log(dummyArray,"dummyArray")
  this.courseList = array


}

hoverable() {
  this.heigthover = this.hover.nativeElement.getBoundingClientRect().top;
  var hoversec = this.hover.nativeElement;
  if (this.heigthover > 550 ) {
    hoversec.classList.add('window-top');
  }
}

mouseup(index, item) {
  if(item.description) {
    for (let k = 0; k < this.category.length; k++) {
      if (k == index) {
        this.category[k].isHoverDesc = 1;
        this.hoverMeasure = true;
      }
    }
  }
}

mousedown(index) {
  for (let k = 0; k < this.category.length; k++) {
    if (k == index) {
      this.category[k].isHoverDesc = 0;
    }
  }
}
ngAfterViewChecked() {

  if(this.hoverMeasure == true) {
    this.hoverable();
    this.hoverMeasure = false;
  }
}



  //end new structure code


  ngOnDestroy(){
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0){
      e1[0].classList.add('with-scroll');
    }

  }
  ngOnInit(): void {
    // this.header.breadCrumbList=[{
    //   'name': 'Learning',
    //   'navigationPath': '/pages/learning',
    //   }]
  }
}



