import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FilterPipeModule  } from 'ngx-filter-pipe';
import { Category } from './category.component';
import { CategoryService } from './category.service';
import { ContentService } from '../content/content.service';
import { AddEditCategoryContentService } from '../addEditCategoryContent/addEditCategoryContent.service';
// import { NotFoundComponent } from './not-found/not-found.component';
import { ThemeModule } from '../../../../@theme/theme.module';
import { TagInputModule } from 'ngx-chips';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgxContentLoadingModule } from 'ngx-content-loading';
import { ComponentModule } from '../../../../component/component.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { ContextMenuModule } from 'ngx-contextmenu';

// import { MiscellaneousRoutingModule, routedComponents } from './miscellaneous-routing.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FilterPipeModule,
    ThemeModule,
    TagInputModule,
    NgxContentLoadingModule,
    AngularMultiSelectModule,
    NgxSkeletonLoaderModule,
    ComponentModule.forRoot(),
    ContextMenuModule.forRoot(),

  ],
  declarations: [
    Category,
    // NotFoundComponent
  ],
  providers: [
    CategoryService,
    AddEditCategoryContentService,
    ContentService
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})

export class CategoryModule {}
