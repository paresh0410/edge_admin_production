import { Component, ViewEncapsulation,
  ElementRef, ViewChild, AfterViewInit
} from '@angular/core';
import { Router,
  // NavigationStart, Routes,
  ActivatedRoute } from '@angular/router';
// import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
// import { BaseChartDirective } from 'ng2-charts/ng2-charts';
import { ContentService } from './content.service';
import { AddEditCourseContentService } from '../addEditCourseContent/addEditCourseContent.service';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { webAPIService } from '../../../../service/webAPIService';
import { webApi } from '../../../../service/webApi'
import { detailsService } from '../addEditCourseContent/details/details.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { isString } from 'util';
import { AssetService } from '../../../dam/asset/asset.service';
import { ToastrService } from 'ngx-toastr';
import { NbSidebarService } from '@nebular/theme';
import { LayoutService } from '../../../../@core/data/layout.service';
import { CommonFunctionsService } from '../../../../service/common-functions.service';
import { Card } from '../../../../models/card.model';
import { noData } from '../../../../models/no-data.model';
import { SuubHeader } from '../../../components/models/subheader.model';
// import { ScrollEvent } from 'ngx-scroll-event';
import { ModuleAccessService } from '../../../../service/module-access.service';
import { Filter } from '../../../../models/filter.modal';
declare var window: any;

@Component({
  selector: 'content',
  styleUrls: ['./content.scss'],
  //template:'content works',
  templateUrl: './content.html',
  encapsulation: ViewEncapsulation.None,
})
export class Content  {

  header: SuubHeader;
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../assets/images/no-data-bg.svg',
    title:'No Courses at this time',
    desc:'Courses will appear after they are added by the instructor. Courses are a bundle of learning activities which are designed by the instructors to enable self-paced online learning',
    titleShow:true,
    btnShow:true,
    descShow:true,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/setting-how-to-create-an-online-course',
  }
  // @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  myInnerHeight: any = window.innerHeight - 120;
  sortdata = [{
    id: 1,
    title: 'Course name',
    sortingmenu: [
      {
        id: 1,
        value: 'asc',
        name: 'ASC',
      }, {
        id: 2,
        value: 'desc',
        name: 'DESC',
      }
    ],
    type: '',
  },
  {
    id: 2,
    title: 'Time Creation/Time modified',
    sortingmenu: [
      {
        id: 1,
        value: 'asc1',
        name: 'ASC',
      }, {
        id: 2,
        value: 'desc2',
        name: 'DESC',
      }
    ],
    type: '',
  },
  ]

  sortingmenu: any = [
    {
      id: 1,
      value: 'asc',
      name: 'ASC',
    }, {
      id: 2,
      value: 'desc',
      name: 'DESC',
    }
  ];
  query: string = '';
  public getData;

  cat: any;
  category: any = []

  content: any = []
  content1: any = [];
  parentcat: any = []

  search: any;
  errorMsg: string;
  loader: any;
  catData: any;
  categoryId: any;
  userData: any;
  tenantId: any;
  searchTags: any = [];
  searchcategory: any = [];
  getCourseUrl = webApi.domain + webApi.url.getCourses;
  getCourseDropdown = webApi.domain + webApi.url.getCourseDropdown;
  getCategoriesurl = webApi.domain + webApi.url.getCategories;

  duplicateCourseUrl = webApi.domain + webApi.url.duplicateCourse;
  nodata: any = false;
  displayArray: any = [];
  visibility: any = [];
  courseFormat: any = [];
  courseLevel: any = [];
  daysBucket: any = [];
  categoryData: any = [];
  userRoles: any = [];
  creatorList: any = [];
  workflowData:any =[];
  searchcourselevel: any;
  searchmanager: any;
  searchWorkflow:any;
  Tags: any = [];
  order = '';
  isworking = false;
  popupMsg:string='';
  isPending =true;
  currentPage = 1;
  // totalPages: any;
  data: any;
  // conentHeight: string;
  // offset: number = 600;
  coursedata: any = [];
  skeleton=false;
  skeletonF=false;
  nodataFound=false;
  cardModify: Card = {
    flag: 'courseContent',
    titleProp : 'fullname',
    // discrption: 'description',
    courseCompletion: 'courseCompletion',
    compleCount: 'Completioncount',
    totalCount: 'enrollcount',
    manager: 'cereaterName',
    lpName:'lpName',
    maxpoints:'maxPoints',
    image: 'coursePicRef',
    showCompletedEnroll: true,
    showCourseCompletion: true,
    showBottomMaximumPoint: true,
    showBottomManager: true,
    showlpName:true,
    hoverlable:true,
    showImage: true,
    option:   true,
    eyeIcon:   true,
    copyIcon: true,

    bottomDiv:   true,
   // bottomDiscription:   true,
    showBottomList:   true,
    bottomTitle:   true,
    btnLabel: 'Edit',
    defaultImage: 'assets/images/courseicon.jpg',
  };
  moduleDataAccess: any;
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  showSpinner: boolean = false;
  categoryName: any;
  title: string;
  // filter
  filters: any = [];
  filter: boolean = false;
  filtersInner: any = [];
  filtercon: Filter = {
    ascending: true,
    descending: true,
    showDropdown: true,
    dropdownList: [
      { drpName: 'Course Name', val: 1 },
      { drpName: 'Creation Date', val: 0 },
    ]
  };
  searchText: any;
  count:number=8
  constructor(private spinner: NgxSpinnerService, private detailsService: detailsService,
    //  private toasterService: ToasterService,
    protected webApiService: webAPIService, private route: ActivatedRoute,
    protected contentservice: ContentService,
    private toastr: ToastrService, private sidebarService: NbSidebarService,
    private layoutService: LayoutService,
    protected passService: AddEditCourseContentService, private router: Router,
    public assetservice: AssetService,
    private  moduleAccessService: ModuleAccessService,
    public commonFunctionService: CommonFunctionsService) {
    // this.toggleSidebar();
    // this.loader = true;
    // this.conentHeight = '0px';
    if (localStorage.getItem('LoginResData')) {
      this.userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', this.userData.data);
      // this.userId = this.userData.data.data.id;
      this.tenantId = this.userData.data.data.tenantId;
    }
    
    this.searchTags = {
      // tags
    };
    this.searchcategory = {
      // categoryName
    };
    this.searchcourselevel = {
      // name
    }
    this.searchmanager = {
      // fullname
    }
    this.searchWorkflow = {
      //lpName
    }
    this.search = {
      categoryId: '',
      courseCode: '',
      courseCompletion: '',
      courseId: '',
      courseLevelId: '',
      lpId: '',
      courseOrder: '',
      coursePicRef: '',
      courseTypeId: '',
      craditpoints: '',
      creatorId: '',
      fullname: '',
      leadTime: '',
      shortname: '',
      summary: '',
      tags: '',
      tenantId: '',
      validFromDate: '',
      validToDate: '',
      validationFreq: '',
      visible: '',
    };

    this.cat = {
      id: ''
    };
    // var passData = this.passService.data;
    var passData = this.contentservice.data;
    console.log(passData);
    if (passData.data != undefined) {
      this.header = {
        title:passData.data.categoryName,
        btnsSearch: true,
        searchBar: true,
        dropdownlabel: ' ',
        placeHolder:'Search by keywords',
        drplabelshow: false,
        drpName1: '',
        drpName2: ' ',
        drpName3: '',
        drpName1show: false,
        drpName2show: false,
        drpName3show: false,
        btnName1: '',
        btnName2: '',
        btnName3: '',
        btnAdd: 'Add Course',
        btnName1show: false,
        btnName2show: false,
        btnName3show: false,
        btnBackshow: true,
        btnAddshow: true,
        filter: true,
        showBreadcrumb: true,
        breadCrumbList:[
          {   'name': 'Learning',
          'navigationPath': '/pages/learning',
        },
          {
            'name': 'Course Category',
            'navigationPath': '/pages/plan/courses/category',
          },
        ]
      };
      this.categoryId = passData.data.categoryId;
      this.categoryName=passData.data.categoryName;
      this.cat.id = this.categoryId;
      this.tenantId = this.userData.data.data.tenantId;
    } else {
      this.header = {
        title:'Online Course',
        btnsSearch: true,
        searchBar: true,
        dropdownlabel: ' ',
        placeHolder:'Search by keywords',
        drplabelshow: false,
        drpName1: '',
        drpName2: ' ',
        drpName3: '',
        drpName1show: false,
        drpName2show: false,
        drpName3show: false,
        btnName1: '',
        btnName2: '',
        btnName3: '',
        btnAdd: 'Add Course',
        btnName1show: false,
        btnName2show: false,
        btnName3show: false,
        btnBackshow: true,
        btnAddshow: true,
        filter: true,
        showBreadcrumb: true,
        breadCrumbList:[
          {
            'name': 'Learning',
            'navigationPath': '/pages/learning',
          },
        ]
      };
      this.cat.id = "";
    }

    this.coursedata = {
      ord: '',
      lmt: 12,
      pno: this.currentPage,
      catIds: this.cat.id,
      manIds: '',
      crsLevIds: '',
      cNameStr: '',
      lpIds:''
    };
    this.isPending=false;
    this.getCourses(this.coursedata);
    this.getCategories();
    this.getCourseDropdownList();
    this.getalltags();
  }

  public param = {
    tId: this.tenantId,
  };
  // ngAfterViewInit() {
  //   this.conentHeight = '0px';
  //   const e = document.getElementsByTagName('nb-layout-column');
  //   console.log(e[0].clientHeight,"client Height");
  //   // this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
  //   if (e[0].clientHeight > 700) {
  //     this.conentHeight = '0px';
  //     this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
  //     console.log(e[0].clientHeight,"client Height If");
  //   } else {
  //     this.conentHeight = String(e[0].clientHeight) + 'px';
  //     console.log(e[0].clientHeight,"client Height else");
  //   }
  // }
  courseDropdownList: any = [];
  getCourseDropdownList() {
    const param = {
      tId: this.tenantId,
    }
    const _urlGetCourseDropdown: string = webApi.domain + webApi.url.getCourseDropdown;
    this.commonFunctionService.httpPostRequest(_urlGetCourseDropdown,param)
    // this.spinner.show();
    //this.detailsService.getDropdownList(param)
    .then(rescompData => {
      // this.loader =false;
      this.courseDropdownList = rescompData['data'];
      console.log('Course Dropdown List ', this.courseDropdownList);
      this.makeCourseDetailsDropdownReady();
    },
      resUserError => {
        // this.loader =false;
        this.errorMsg = resUserError;
        // this.skeleton=true;
        // this.skeletonF=true;
      });
  }
  getalltags() {
    let param = {
      'aId': 2,
      'tId': this.tenantId,
      'cId': 1,
    }
    const _urlGetAllTags: string = webApi.domain + webApi.url.getAllTags;
    this.commonFunctionService.httpPostRequest(_urlGetAllTags,param)
    //this.assetservice.getAllTags(param)
      .then(rescompData => {
        //  this.skeleton=true;
        //     this.skeletonF=true;
        //  this.loader = true;
        var result = rescompData;
        console.log('getTagsResponse:', rescompData);
        if (result['type'] == true) {
          if (result['data'][0].length == 0) {
            this.nodataFound = true;

          } else {
            const tagArr = result['data'][0];
            for (let i = 0; i < tagArr.length; i++) {
              tagArr[i].status = false;
            }
            this.Tags = tagArr;
            this.bindfilter(this.Tags);
            /*bhavesh told me to comment it*/
            console.log('this.Tags', this.Tags);
            //  this.loader = true;
          }
          // this.skeletonF=true;
          // // this.skeleton=true;
        } else {
          // this.loader = true;
          // this.noTags = true;
          // var toast: Toast = {
          //   type: 'error',
          //   //title: "Server Error!",
          //   body: 'Something went wrong.please try again later.',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          // this.skeleton=true;
          // this.skeletonF=true;
        }
        // this.skeleton=true;
        // this.skeletonF=true;
      }, error => {
        //  this.loader = true;
        // this.noTags = true;
        // var toast: Toast = {
        //   type: 'error',
        //   //title: "Server Error!",
        //   body: 'Something went wrong.please try again later.',
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        this.toastr.warning('Something went wrong.please try again later.')
      });
  }
  bindfilter(obj) {
    const filtername = obj.length > 0 ? obj[0]['filterId'] : '';
    const filterValueName = obj.length > 0 ? obj[0]['filterValue'] : '';    
    const item = {
          count: "",
          value: "",
          tagname: obj.length > 0 ? obj[0]['filterId'] : '',
          isChecked: false,
          list: obj,
          filterValue: filterValueName
        }
        if (filtername) {
          this.filters.push(item);
        }
    // if (result['data'] && result['data'].length > 0) {
    //   result['data'].forEach( (value, index) => {
        
    //   })
    // }
  }
  filteredChanged1(event) {
    // this.showSpinner = true
    this.spinner.show();
    console.log('Filtered Event - ', event);
    const obj = event;
    this.filterData = {};
    this.SelectedCategoryLength = {};
    this.objectSetter = {};
    let allempty = true;
    if (obj) {
      for (const key in obj) {
        const list = obj[key];
        if (Array.isArray(list)) {
          list.forEach((value, index) => {
            allempty = false;
            console.log(key, ' - ', value);
            this.SubjectName_CheckBoxedStatus = true;
            this.subjectName(value, key);
          });
        }
      }
      if (!allempty) {
        this.fieldValueCreatorFunction();
      } else {
        this.displayArray = [];
        this.newArray = [];
        this.sum = 50;
        this.fieldValueCreatorFunction();
        // this.addItems(0, this.sum, 'push', this.content);
         this.nodataFound = false;
      }
    } else {
      this.displayArray = [];
      this.newArray = [];
      this.sum = 50;
      this.fieldValueCreatorFunction();
      // this.addItems(0, this.sum, 'push', this.content);
      this.nodataFound = false;
    }
    // this.spinner.hide();

  }

  getSortData(event){
    console.log(event,"sorting")
    if(event.selectedLevel == 1){
      var property = 'fullname'
    }else{
      var property = 'timecreated'
    }
    var array = this.displayArray
    // console.log(this.displayArray,"huhuhuhu")
    var dummyArray = array.sort(this.commonFunctionService.sort(property,event.selectedRadio))
    console.log(dummyArray,"dummyArray")
    this.displayArray = array
  
  
  }
  makeCourseDetailsDropdownReady() {
    // this.spinner.hide();
    // this.showSpinner =false
    this.categoryData = this.courseDropdownList.catList;
    this.courseFormat = this.courseDropdownList.courseType;
    this.courseLevel = this.courseDropdownList.courseLevel;
    this.visibility = this.courseDropdownList.visibility;
    this.daysBucket = this.courseDropdownList.leadTime;
    this.userRoles = this.courseDropdownList.userRoles;
    this.creatorList = this.courseDropdownList.creatorList;
    this.workflowData = this.courseDropdownList.lpList;
    for (let i = 0; i < this.courseLevel.length; i++) {
      this.courseLevel[i].courseLevelId = this.courseLevel[i].id;
    }
    for (let i = 0; i < this.creatorList.length; i++) {
      this.creatorList[i].creatorId = this.creatorList[i].id;
    }
    // this.bindfilter(this.categoryData);
    this.bindfilter(this.courseLevel);
    this.bindfilter(this.creatorList);
    this.bindfilter(this.workflowData);
    this.bindfilter(this.sortdata);
  }
  getCourses(data) {
    // this.showSpinner = true
    // this.loader = true;
    //  this.spinner.show();
   // this.showSpinner = true
  this.skeleton=false;
   this.skeletonF=false;
  // this.showSpinner = false
    this.displayArray =[];
    this.sum =50;
    let param = {
      cTypeId: 1,
      tId: this.tenantId,
      ord: this.order,
      lmt: 50,
      pno: data.pno,
      catIds: data.catIds,
      manIds: data.manIds,
      crsLevIds: data.crsLevIds,
      cNameStr: data.cNameStr,
      lpIds:data.lpIds,
      wfId:null
    };
    if(this.isPending==false){
      this.isPending=true;
      const get_course_opt = webApi.domain + webApi.url.get_all_course_optmz;
      //const _urlCourses_demo: string = webApi.domain + webApi.url.getCourses_demo;
      this.commonFunctionService.httpPostRequest(get_course_opt,param)
      //this.contentservice.getCourses(param)
      .then(rescompData => {
    // this.showSpinner = false
        this.spinner.hide()
        this.isPending= false;
        // this.loader =false;
        this.skeletonF=true;
        this.skeleton=true;
        if (rescompData['type'] === true) {
          this.spinner.hide();
          // const list = rescompData['data']['list'];
          // this.totalPages = rescompData['data'].totalPages;

          // const list = rescompData['list'];
          // this.totalPages = 1;
          // this.displayArray = rescompData['list'];
          // this.courseData = rescompData['list'];
          this.content1 = rescompData['list'];
          // this.addItems(0,this.sum,'push',this.content1);
          // this.spinner.hide()
          // this.showSpinner = false
          // console.log('this.displayArray', this.displayArray);
          if( this.content1.length == 0) {
            this.nodataFound = true;
            this.skeleton=true;
            this.skeletonF=true;
            this.loader = false;
            // this.spinner.hide()
            // this.showSpinner = false
            // this.isPending= false;
          }
        else {
          this.skeleton=false;
          this.skeletonF=true;
          this.loader = false;
          this.nodataFound = false;
          // this.spinner.hide()
        //  this.showSpinner = false
          // this.isPending= false;
        }


        if (this.content1) {
          this.prepareData();
        } else {
          this.nodata = 'true';
        }
        }
      },
        resUserError => {
          console.log(resUserError);
          this.errorMsg = resUserError;
          this.loader = false;
          this.isPending= false;
          this.spinner.hide()
          // this.showSpinner = false
        });
  }
}
  onsearch(evt: any) {
    console.log(evt);
   // this.spinner.show()
  //  this.skeletonF = false;
    // this.searchstr = evt.target.value;
    let cNameStr = evt.target.value;
    if (evt.keyCode === 13) {
      // this.showSpinner = true
      //this.showSpinner = true
      this.currentPage = 1;
      this.coursedata.pno = 1;
      this.coursedata.cNameStr = cNameStr;
      this.displayArray = [];
      this.getCourses(this.coursedata);
    }
  }
  getCategories() {
    //this.loader = true;
    // this.skeleton=false;
    // this.skeletonF=false;
    let param = {
      tId: this.tenantId,
    };
    // this.spinner.show();
    const _urlCategories: string = webApi.domain + webApi.url.getCategories;
    this.commonFunctionService.httpPostRequest(_urlCategories,param)
    //this.contentservice.getCategories(param)
      .then(rescompData => {
    // this.showSpinner = false
        // this.loader =false;
        // this.skeleton=true;
        // this.skeletonF=true;
        this.spinner.hide();
        if(rescompData['type'] === true) {
        this.parentcat = rescompData['data'];
        console.log('Category Result', this.parentcat);
        this.bindfilter(this.parentcat);
        this.loader = false;
      }
      else{
        this.nodataFound=true
        //this.showSpinner = false
      }
      // this.skeleton = true;
    },
        resUserError => {
          this.nodataFound=true
          //this.showSpinner = false
          // this.loader = false;
          this.errorMsg = resUserError;
          // this.skeleton=true;
          // this.skeletonF=true;
        });
  }

  prepareData() {
    // this.skeleton=false;
    // this.skeletonF=false;
    if (this.categoryId == undefined) {
      // this.asset = this.content;
      this.content = this.content1;
      this.addItems(0,this.sum,'push',this.content1);
      // this.displayArray = this.content;

      console.log('All Course', this.content);
    } else {
      var tempData =[];
      for (let i = 0; i < this.content1.length; i++) {
        if (this.categoryId == this.content1[i].categoryId) {
          tempData.push(this.content1[i]);
        }
      }
      this.content1 = tempData;
      this.addItems(0,this.sum,'push',this.content1);
      console.log('Filtered Course', this.content);
    }
    this.loader = false;
    // this.skeletonF=true;
    this.skeleton=true;
  }

  doropserch() {
    this.content = [];
    if (this.cat.id == undefined || this.cat.id == "") {
      this.content = this.content1;
    } else {
      for (var i = 0; i < this.content1.length; i++) {
        if (this.cat.id == this.content1[i].categoryId) {
          this.content.push(this.content1[i]);
        }
      }
    }
  }

  clear() {
    this.search = {};
    this.coursedata.pno = 1;
    if(this.searchText.length>=3){
      this.coursedata.cNameStr = '';
      this.displayArray = [];
      this.getCourses(this.coursedata);
    }
  }

  public addeditcontent(data, id) {
    var data1 = {
      data: data,
      id: id,
      catId: this.categoryId,
      categoryName:this.categoryName,
      courseDropdowns: this.courseDropdownList,
    };
    if (data == undefined) {
      this.passService.data = data1;
      this.router.navigate(['/pages/plan/courses/addEditCourseContent']);
      console.log("Data passed to service" + data1);
      console.log("ID Passed" + data1.id + " " + data1.data);
    } else {
      this.passService.data = data1;
    this.router.navigate(['/pages/plan/courses/addEditCourseContent']);
  }
  }

  gotoCardaddedit(data) {
    var data1 = {
      data: data,
      id: 1,
      catId: this.categoryId,
      categoryName:this.categoryName,
      courseDropdowns: this.courseDropdownList,
    };

    this.passService.data = data1;
    this.router.navigate(['/pages/plan/courses/addEditCourseContent']);
  }
  back() {
    // if(!this.contentservice.data){
    //   this.router.navigate(['/pages/learning']);
    // }else{
    //   this.router.navigate(['/pages/plan/courses/category']);
    // }

    this.router.navigate(['/pages/learning']);
   
  }

  deleteCourseModal: boolean = false;
  deleteCourseData: any;
  deleteContent(course) {
    console.log('Course content', course);
    this.deleteCourseData = course;
    this.deleteCourseModal = true;
  }

  deleteContentAction(actionType) {
    if (actionType == true) {
      // this.removeCourse(this.deleteCourseData);
      this.closeDeleteContentModal();
    } else {
      this.closeDeleteContentModal();
    }
  }

  closeDeleteContentModal() {
    // console.log('Course content',course);
    this.deleteCourseModal = false;
  }

  disableCourse: boolean = false;
  disableContent(item, disableStatus) {
    this.disableCourse = !this.disableCourse;
  }

  // enable disable course
  disableCourseVisible: boolean = false;
  visibiltyRes: any;
  enableDisableCourseModal: boolean = false;
  enableDisableCourseData: any;
  enableCourse: boolean = false;
  courseDisableIndex: any;
  btnName:string='Yes'

  disableCourseVisibility(currentIndex, courseData, status) {
    this.enableDisableCourseData = courseData;
    this.enableDisableCourseModal = true;
    this.courseDisableIndex = currentIndex;
    // for(let i = 0 ; i < this.content.length ;i++)
    // {
    //   if(this.content[i].courseId  ==  this.enableDisableCourseData.courseId)
    //   {

    //     if(this.content[i].visible == 0){
    //       this.enableCourse = false;
    //     }else{
    //       this.enableCourse = true;
    //     }
    //   }
    // }
    if (this.enableDisableCourseData.visible == 0) {
      this.enableCourse = false;
    } else {
      this.enableCourse = true;
    }

    // if(this.content[currentIndex].visible == 0){
    //   this.enableCourse = false;
    // }else{
    //   this.enableCourse = true;
    // }
    // for(let i=0;i<this.content.length;i++)
    // {

    // }
  }
  /* function to check wheater the course id in workflow or not */
  visibleStatus(courseData) {
    var visibilityData = {
      courseId: courseData.courseId,
      visible: courseData.visible==1 ? 0 : 1,
    }
    const _urlDisableCourse: string = webApi.domain + webApi.url.visibleCheck;
    this.commonFunctionService.httpPostRequest(_urlDisableCourse,visibilityData)
      .then(rescompData => {
        this.visibiltyRes = rescompData;
        console.log('Course Visibility Result', this.visibiltyRes);
        if (this.visibiltyRes.type == false) {
          this.closeEnableDisableCourseModal();

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        } else {

          const res=this.visibiltyRes.data[0]
          if (res.flag==2){
             this.popupMsg=res.msg;
            this.enableDisableCourseModal = true;
          } else{
            courseData.visible=visibilityData.visible;
            this.toastr.success(res.msg, 'Success', {
              closeButton: false
            });
          }
        }
      },
        resUserError => {
          // this.loader =false;
          this.errorMsg = resUserError;
          this.skeleton=true;
          this.skeletonF=true;
          // this.spinner.hide();
          //this.showSpinner = false
          this.closeEnableDisableCourseModal();
        });
  }
  clickTodisable(courseData) {

    if(this.moduleDataAccess.allowCourseVisibilityModification){
      if (courseData.visible == 1 ) {
        if(courseData.lpId!='NA'){
          var defaultString="This Course is a part of "+courseData.lpName+' Workflow(s)/Learning Pathway(s)';
          this.popupMsg=courseData['lpMsg'] ? courseData['lpMsg'] : defaultString;

        }
      this.title="Disable course"
        this.enableCourse = true;
        this.enableDisableCourseData = courseData;
        this.enableDisableCourseModal = true;
      } else {
      this.title="Enable course"
        this.enableCourse = false;
        this.enableDisableCourseData = courseData;
        this.enableDisableCourseModal = true;

      }
    }else {
      this.toastr.info('Your role does not have permission to change visibility for courses', 'Info');
    }

    // this.visibleStatus(courseData);
  }

  enableDisableCourse(i,courseData) {
    // this.spinner.show();
   // this.showSpinner = true
   if(this.moduleDataAccess.allowCourseVisibilityModification){
      if(courseData.lpId!='NA'){
        var defaultString="This Course is a part of "+courseData.lpName+' Workflow(s)/Learning Pathway(s)';
        this.popupMsg=courseData['lpMsg'] ? courseData['lpMsg'] : defaultString;
  }
    var visibilityData = {
      courseId: courseData.courseId,
      visible: courseData.visible==1?0:1,
    }
    if(this.displayArray[i].visible==0){
this.displayArray[i].visible=1;
}else{
  this.displayArray[i].visible=0;
}
    const _urlDisableCourse: string = webApi.domain + webApi.url.disableCourse;
    this.commonFunctionService.httpPostRequest(_urlDisableCourse,visibilityData)
    //this.contentservice.disableCourse(visibilityData)
      .then(rescompData => {
        // this.loader =false;
        this.visibiltyRes = rescompData;
        // this.spinner.hide();
        //this.showSpinner =false
        console.log('Course Visibility Result', this.visibiltyRes);
        if (this.visibiltyRes.type == false) {
          // var courseUpdate: Toast = {
          //   type: 'error',
          //   title: "Course",
          //   body: "Unable to update visibility of course.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.closeEnableDisableCourseModal();
          // this.toasterService.pop(courseUpdate);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        } else {
          // var courseUpdate: Toast = {
          //   type: 'success',
          //   title: "Course",
          //   body: this.visibiltyRes.data,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.spinner.hide();
          //this.showSpinner = false
          // this.closeEnableDisableCourseModal();
          // this.toasterService.pop(courseUpdate);


          this.toastr.success(this.visibiltyRes.data, 'Success', {
            closeButton: false
          });
        }
      },
        resUserError => {
          // this.loader =false;
          this.errorMsg = resUserError;
          this.skeleton=true;
          this.skeletonF=true;
          // this.spinner.hide();
          //this.showSpinner = false
          // this.closeEnableDisableCourseModal();
        });
        
}else {
  this.toastr.info('Your role does not have permission to change visibility for courses', 'Info');
}
  }

  enableDisableCourseAction(actionType) {
    // if(actionType == true){
    //   for(let i = 0 ; i < this.content.length ;i++)
    //   {
    //     if(this.content[i].courseId  ==  this.enableDisableCourseData.courseId)
    //     {
    //       this.courseDisableIndex = i;
    //       if(this.content[this.courseDisableIndex].visible == 1){
    //         this.content[this.courseDisableIndex].visible = 0;
    //         var courseData = this.content[this.courseDisableIndex];
    //         this.enableDisableCourse(courseData);

    //       }else{
    //         this.content[this.courseDisableIndex].visible = 1;
    //         var courseData = this.content[this.courseDisableIndex];
    //         this.enableDisableCourse(courseData);
    //       }
    //     }
    //     }
    //   }else{
    //     this.closeEnableDisableCourseModal();
    //   }

    if (actionType == true) {

      if (this.enableDisableCourseData.visible == 1) {
        this.enableDisableCourseData.visible = 0;
        var courseData = this.enableDisableCourseData;
        // this.enableDisableCourse(courseData);

      } else {
        this.enableDisableCourseData.visible = 1;
        var courseData = this.enableDisableCourseData;
        // this.enableDisableCourse(courseData);
      }
    } else {
      this.closeEnableDisableCourseModal();
    }
  }

  closeEnableDisableCourseModal() {
    this.enableDisableCourseModal = false;
    this.popupMsg='';
  }

  formDataCourse: any;
  duplicateContent(courseData) {
    this.formDataCourse = {
      courseId: courseData.courseId,
      fullname: '',
      tenantId: courseData.tenantId,
    }
    // this.spinner.hide()
    //this.showSpinner = false
  }

  duplicateCourseModal: boolean = false;
  duplicateCourseData: any;
  duplicateCourseContent(course) {
    console.log('Course content', course);
    this.duplicateCourseData = course;
    this.duplicateCourseModal = true;
    this.duplicateContent(course);
  }


  copyCard(course) {
    //this.spinner.show()
    console.log('Course content', course);
    this.duplicateCourseData = course;
    this.duplicateCourseModal = true;
    this.title="Duplicate course "+this.duplicateCourseData.fullname
    this.btnName="Duplicate"
    this.duplicateContent(course);

  }
  duplicateCourseAction(actionType) {
    // this.spinner.show()
    //this.showSpinner = false
    if (actionType == true) {
      this.makeDuplicateCourseDataReady();
      this.closeDuplicateCourseModal();
    } else {
      // this.spinner.show()
      this.closeDuplicateCourseModal();
    }
    // this.spinner.hide()
   // this.showSpinner = false
  }

  closeDuplicateCourseModal() {
    this.duplicateCourseModal = false;
  }

  formattedDupliTags: any = null;
  makeTagDataReady() {
    var tagsData = this.formDataCourse.tags;
    if (tagsData.length > 0) {
      var tagsString = '';
      for (let i = 0; i < tagsData.length; i++) {
        var tag = tagsData[i];
        if (tagsString != "") {
          tagsString += "|";
        }
        if (tag.value) {
          if (String(tag.value) != "" && String(tag.value) != "null") {
            tagsString += tag.value;
          }
        } else {
          if (String(tag) != "" && String(tag) != "null") {
            tagsString += tag;
          }
        }
      }
      this.formattedDupliTags = tagsString;
    }
  }

  creditPointsStr: any;
  makeDuplicateCourseDataReady() {
    var courseData = {
      courseId: this.formDataCourse.courseId,
      fullname: this.formDataCourse.fullname,
      userMod: this.userData.data.data.id,
    }
    console.log('Duplicate course data ', courseData);

    this.duplicateCourse(courseData);
  }

  duplicateCourseRes: any;
  duplicateCourse(course) {
    // this.showSpinner = true
    this.spinner.show();
    const _urlDublicateCourse: string = webApi.domain + webApi.url.duplicateCourse;
    this.commonFunctionService.httpPostRequest(_urlDublicateCourse,course)
    //this.contentservice.dublicateCourse(course)
      .then(rescompData => {
        this.loader = false;
        var temp: any = rescompData;
        this.duplicateCourseRes = temp.data;
        if (temp == "err") {
          // this.showSpinner = false
          // this.spinner.hide()
          //this.showSpinner =false
          // this.notFound = true;
          // var courseDuplicate: Toast = {
          //   type: 'error',
          //   title: "Course",
          //   body: "Unable to duplicate course.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(courseDuplicate);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        } else if (temp.type == false) {
          // this.spinner.hide()
          // this.showSpinner = false
          // var courseDuplicate: Toast = {
          //   type: 'error',
          //   title: "Course",
          //   body: this.duplicateCourseRes[0].msg,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(courseDuplicate);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        } else {
          // this.spinner.hide()
          // this.showSpinner = false
          // var courseDuplicate: Toast = {
          //   type: 'success',
          //   title: "Course",
          //   body: this.duplicateCourseRes[0].msg,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(courseDuplicate);

          this.toastr.success(this.duplicateCourseRes[0].msg, 'Success', {
            closeButton: false
          });
          this.getCourses(this.coursedata);
        }
        console.log('Course duplicate Result ', this.duplicateCourseRes);
        // this.cdf.detectChanges();
      },
        resUserError => {
          this.loader = false;
          this.errorMsg = resUserError;
          this.skeleton=true;
          this.skeletonF=true;
          this.spinner.hide()
          // this.showSpinner = false
        });
  }


  /*8888************************* filter************************/



  fieldValue = [];
  ObjectSetterValueArray = [];

  filterData = {};

  // subject filteration starts here
  // main array
  newArray = [];

  // array for display on html page
  // Initially without filter it will hold data of asset
  // nofilterAsset variable for validation
  noFilterAsset: boolean = false;

  // new array for tags
  tagArray = [];
  filterTag: any = [];
  // number display to n
  SelectedCategoryLength: any = {};
  SubjectFilterArray = [];
  SubjectName_CheckBoxedStatus: boolean;
  filterParam: any = [];
  // subj: any;
  /*

  After loading the screen
  ***********************************
  when we select the checkbox SubjectName function will be called.
  After execution of statement takes places
  */
  // tslint:disable-next-line: max-line-length
  filterDataObjectArrayCreation(filterData_CategoryNameForKey: any, FiltersubjectNameForArray: any, checkboxStatus: boolean) {

    let dataValue = null;
    let flag = false;
    for (const key in this.filterData) {
      if (filterData_CategoryNameForKey === key) {
        flag = true;
        dataValue = key;
        break;
      } else {
        flag = false;
      }
    }
    if (checkboxStatus) {
      if (!flag) {
        Object.assign(this.filterData, { [filterData_CategoryNameForKey]: [] });
        (this.filterData[filterData_CategoryNameForKey]).push(FiltersubjectNameForArray[filterData_CategoryNameForKey]);
        // console.log('Filter data after insertion object with array checkbox', this.filterData);
      } else {
        (this.filterData[dataValue]).push(FiltersubjectNameForArray[filterData_CategoryNameForKey]);
        // console.log('Filter data after insertion object with array checkbox', this.filterData[dataValue]);
        // console.log('true');
      }
      // console.log('Filter data after insertion object with array checkbox', this.filterData);
      // console.log(this.filterData);
    } else {
      for (let i = 0; i < this.filterData[dataValue].length; i++) {
        if (this.filterData[dataValue][i] === FiltersubjectNameForArray[dataValue]) {
          this.filterData[dataValue].splice(i, 1);
          if (this.filterData[dataValue].length === 0) {
            delete this.filterData[dataValue];
            this.SelectedCategoryLength[dataValue] = 0;
            delete this.objectSetter[dataValue];
            break;
          }
          // console.log('Filter data after Splicing object with array checkbox', this.filterData);
          // console.log(this.filterData[dataValue]);
        }
      }
    }
  }


  objectSetter = {};

  fieldValueCreatorFunction() {
    // this.spinner.show()
    // this.showSpinner = true
    this.filterParam = [];
    this.coursedata = {
      catIds: '',
      crsLevIds: '',
      manIds: '',
      lpIds: '',
    }
    // const keys = Object.keys(this.filterData);
    this.fieldValue = Object.keys(this.filterData);
    // console.log('FieldValue Object Data', this.fieldValue);
    for (const ObjectSetterData of this.fieldValue) {
      Object.assign(this.objectSetter, { [ObjectSetterData]: false });
    }

    for (const filterDataCate of this.fieldValue) {
      Object.assign(this.SelectedCategoryLength, { [filterDataCate]: this.filterData[filterDataCate].length });
    }

    if (this.filterData) {
      // let filter: any;
      // tslint:disable-next-line: forin
      for (const filter in this.filterData) {
        this.filterParam[filter] = this.filterData[filter].join(',');
      }
      if (this.filterParam.categoryId) {
        this.coursedata.catIds = this.filterParam.categoryId;
      }
      if (this.filterParam.courseLevelId) {
        this.coursedata.crsLevIds = this.filterParam.courseLevelId;
      }
      if (this.filterParam.creatorId) {
        this.coursedata.manIds = this.filterParam.creatorId;
      }
      if (this.filterParam.lpId) {
        this.coursedata.lpIds = this.filterParam.lpId;
      }
      this.coursedata.pno = 1;
      console.log(this.coursedata);
      this.displayArray = [];
      this.getCourses(this.coursedata);
    }
    // this.spinner.hide()
    // this.showSpinner = false
    this.skeletonF  = true
  }

  tagCreatorFunction(CategoryObject: any, StatusToPushOrPop: any) {
    this.filterData = [];
    if (StatusToPushOrPop) {
      this.tagArray.push(CategoryObject);
      for (const data of this.tagArray) {
        for (const key in data) {
          if (isString(data[key])) {
            // console.log('key type',isString(data[key]));
            // console.log(key);
            this.filterTag.push(key);
          }
        }
      }
    }
    console.log('Filter TagArray', this.filterTag);
    console.log('Tag Array', this.tagArray);
  }

  subjectName(subj: any, cat: any) {
    this.skeletonF = false
    // this.spinner.show()
    console.log(cat,"cat")
    console.log(subj,"vghvhgvghccgcgc" + cat);
    // console.log(subj.name);
    // console.log('Category name', cat);
    // this.SubjectFilterArray.push(subj.name);

    if (cat === "tags") {
      for (const tagArray of this.Tags) {
        if (subj === tagArray) {
          console.log('tag matches');
          tagArray.status = this.SubjectName_CheckBoxedStatus;
          break;
        }
      }
    }

    this.filterDataObjectArrayCreation(cat, subj, this.SubjectName_CheckBoxedStatus);
    // this.fieldValueCreatorFunction();
    //this.showSpinner = false;
    // this.onFilterSubjectPush(cat);
    // this.tagCreatorFunction(subj, this.SubjectName_CheckBoxedStatus);
  }

  onFilterSubjectPush(filterData_CategoryNameForKey: any) {
    this.newArray = [];
    for (const data of this.content) {

      for (const ObjectSetterData of this.fieldValue) {
        Object.assign(this.objectSetter, { [ObjectSetterData]: false });
      }

      for (const value of this.fieldValue) {
        // this.tagArray = [];
        // console.log('Value', value);
        // console.log('FilterData of particular value', this.filterData[value]);
        // console.log('Length of FilterData of particular value', this.filterData[value].length);
        // tslint:disable-next-line: prefer-for-of
        // if (value === filterData_CategoryNameForKey) {
        if (value == 'tags') {
          console.log(value);

          for (const tagData of data[value]) {
            for (let i = 0; i < this.filterData[value].length; i++) {

              // Pushing data into tag array
              // this.tagArray.push({[value]:this.filterData[value][i]});

              if (tagData == undefined || tagData != this.filterData[value][i]) {
                // console.log('ifTrue');
                // let checkData = data[value];
              } else {
                // console.log('ifFalse');
                this.objectSetter[value] = true;
                // let checkData = data[value];
                break;
              }
            }
          }

        }
        else {
          console.log('else part executed');

          for (let i = 0; i < this.filterData[value].length; i++) {

            // Pushing data into tag array
            // this.tagArray.push({[value]:this.filterData[value][i]});

            // if (data[value] === undefined || data[value] !== this.filterData[value][i]) {
            if (data[value] == undefined || data[value] != this.filterData[value][i]) {
              // console.log('ifTrue');
              // let checkData = data[value];
            } else {
              // console.log('ifFalse');
              this.objectSetter[value] = true;
              // let checkData = data[value];
              break;
            }
          }

        }
      }
      // }
      // let ObjectDataValue = null;
      let Objectflag = true;
      this.ObjectSetterValueArray = Object.values(this.objectSetter);

      for (const objectSetterValueArrayData of this.ObjectSetterValueArray) {
        if (Objectflag && objectSetterValueArrayData) {
          Objectflag = true;
        } else {
          Objectflag = false;
        }
      }

      if (Objectflag) {
        this.newArray.push(data);
      }
    }

    // console.log(this.tagArray);
    console.log(this.newArray);


    if ((this.fieldValue.length !== 0)) {
      if ((this.newArray.length !== 0)) {

       // this.displayArray = this.newArray;
        // this.content = this.newArray;
        this.displayArray = [];
        this.sum = 50;
        this.addItems(0, this.sum, 'push', this.newArray);
        this.noFilterAsset = false;
        this.nodataFound = false;

      } else {
        this.displayArray = [];
        this.nodataFound = true;
        // this.noFilterAsset = true;
      }
    } else {
      this.noFilterAsset = false;
      this.nodataFound = false;
      //this.displayArray = this.content;
      this.displayArray = [];
      this.sum = 50;
      this.addItems(0, this.sum, 'push', this.content1);
    }
  }

  // subject filteration ends here


  onUpdateSubjectName(event: Event) {
    // this.showSpinner = true
    this.spinner.show()
    console.log(event);
    this.SubjectName_CheckBoxedStatus = (<HTMLInputElement>event.target).checked;
    // this.showSpinner = false

    // console.log('Subject Name', this.SubjectName_CheckBoxedStatus);
  }
  // ngOnDestroy() {
  //   this.toggleSidebar();
  // }
  // toggleSidebar(): boolean {
  //   this.sidebarService.toggle(true, 'menu-sidebar');
  //   this.layoutService.changeLayoutSize();
  //   return false;
  // }
  sorting(event, i, type, sort) {
    // this.showSpinner = true
    console.log('event, i, type, sort', event, i, type, sort);
    var temp: any = this.makeSortingDataReady();
    console.log(temp);
    if (temp) {
      this.order = temp;
      if (this.order) {
        this.coursedata.pno = 1;
        this.coursedata.ord = this.order;
        this.displayArray = [];
        this.getCourses(this.coursedata);
      }
    }
  }

  makeSortingDataReady() {
    var sortStr = '';
    for (let i = 0; i < this.sortdata.length; i++) {
      // if (sortStr != '') {
      //   sortStr += ',';
      // }
      if (this.sortdata[i].id == 1) {
        if (this.sortdata[i].type) {
          if (sortStr != '') {
            sortStr += ',';
          }
          sortStr += 'fullname' + ' ' + this.sortdata[i].type;
        }
      }
      if (this.sortdata[i].id == 2) {
        if (this.sortdata[i].type) {
          if (sortStr != '') {
            sortStr += ',';
          }
          sortStr += 'timemodified' + ' ' + this.sortdata[i].type;
        }
      }
    }
    console.log('Sort straing', sortStr);
    return sortStr;
  }


  // onScroll(event) {
  //   let element = this.myScrollContainer.nativeElement;
  //   // element.style.height = '500px';
  //   // element.style.height = '500px';
  //   // Math.ceil(element.scrollHeight - element.scrollTop) === element.clientHeight
  //   if (element.scrollHeight - element.scrollTop - element.clientHeight < 20) {
  //     if (!this.isworking) {
  //       this.currentPage++;
  //       if (this.currentPage <= this.totalPages) {
  //         this.isworking = true;
  //         // this.skeleton=false;
  //         this.coursedata.pno = this.currentPage;
  //         // this.loader = true;
  //         this.getCourses(this.coursedata);
  //       }
  //     }
  //   }
  // }
  gotoFilter() {
    this.filter = !this.filter;
  }
  ngOnDestroy(){
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0){
      e1[0].classList.add('with-scroll');
    }
  }
  ngAfterContentChecked() {
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0)
    {
      e1[0].classList.remove('with-scroll');
    }

   }
   conentHeight = '0px';
   offset: number = 100;
   ngOnInit() {
    this.moduleDataAccess = this.moduleAccessService.getRoleAccessData('onlineCourse');
    console.log('Module Data Access ==>', this.moduleDataAccess);
    if(this.moduleDataAccess){
      this.cardModify.eyeIcon = this.moduleDataAccess.allowCourseVisibilityModification;
    }
     this.conentHeight = '0px';
     const e = document.getElementsByTagName('nb-layout-column');
     console.log(e[0].clientHeight);
     // this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
     if (e[0].clientHeight > 1300) {
       this.conentHeight = '0px';
       this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
     } else {
       this.conentHeight = String(e[0].clientHeight) + 'px';
     }
   }

   sum = 40;
   addItems(startIndex, endIndex, _method, asset) {
     for (let i = startIndex; i < endIndex; ++i) {
       if (asset[i]) {
         this.displayArray[_method](asset[i]);
       } else {
         break;
       }

       // console.log("NIKHIL");
     }
   }
   // sum = 10;
   onScroll(event) {

     let element = this.myScrollContainer.nativeElement;
     // element.style.height = '500px';
     // element.style.height = '500px';
     // Math.ceil(element.scrollHeight - element.scrollTop) === element.clientHeight

     if (element.scrollHeight - element.scrollTop - element.clientHeight < 5) {
       if (this.newArray.length > 0) {
         if (this.newArray.length >= this.sum) {
           const start = this.sum;
           this.sum += 50;
           this.addItems(start, this.sum, 'push', this.newArray);
         } else if ((this.newArray.length - this.sum) > 0) {
           const start = this.sum;
           this.sum += this.newArray.length - this.sum;
           this.addItems(start, this.sum, 'push', this.newArray);
         }
       } else {
         if (this.content1.length >= this.sum) {
           const start = this.sum;
           this.sum += 50;
           this.addItems(start, this.sum, 'push', this.content1);
         } else if ((this.content1.length - this.sum) > 0) {
           const start = this.sum;
           this.sum += this.content1.length - this.sum;
           this.addItems(start, this.sum, 'push', this.content1);
         }
       }


       console.log('Reached End');
     }
   }

    // this function is use to dynamicsearch on table
  searchQuestion(event) {
    var temData = this.content1;
    this.searchText=event.target.value
    var val = event == undefined ? '' : event.target.value.toLowerCase();
    var keys = [];
    if(val == '')
    {
      this.displayArray =[];
      this.newArray = [];
      this.sum = 50;
      this.addItems(0, this.sum, 'push', this.content1);
      if(this.displayArray.length>0){
        this.nodataFound = false
      }else{
        this.nodataFound = true
      }
    }else{
         // filter our data
    if (temData.length > 0) {
      keys = Object.keys(temData[0]);
    }
    if(val.length>=3||val.lenght==0){
    this.nodataFound=false
    var temp = temData.filter((d) => {
      for (const key of keys) {
        if(key == 'fullname'){
        if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
          return true;
        }
       }
      }
    });
    if(temp.length==0){
      this.nodataFound=true
       this.noDataVal={
        margin:'mt-5',
        imageSrc: '../../../../../assets/images/no-data-bg.svg',
        title:"Sorry we couldn't find any matches please try again",
        desc:".",
        titleShow:true,
        btnShow:false,
        descShow:false,
        btnText:'Learn More',
        btnLink:''
      }
    }
    // update the rows
    this.displayArray =[];
    this.newArray = temp;
    this.sum = 50;
    this.addItems(0, this.sum, 'push', this.newArray);
    this.skeleton = true
  }
}
}


}

