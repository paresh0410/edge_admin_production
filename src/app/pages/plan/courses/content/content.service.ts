import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Injectable, Inject, ModuleWithProviders } from '@angular/core';
import { AppConfig } from '../../../../app.module';
import { HttpClient } from "@angular/common/http";
import { webAPIService } from '../../../../service/webAPIService';
import { webApi } from '../../../../service/webApi';

import { AuthenticationService } from '../../../../service/authentication.service';

@Injectable()
export class ContentService {

  private _urlCourses: string = webApi.domain + webApi.url.getCourses;
  private _urlCategories: string = webApi.domain + webApi.url.getCategories;
  private _urlDisableCourse: string = webApi.domain + webApi.url.disableCourse;
  private _urlDublicateCourse: string = webApi.domain + webApi.url.duplicateCourse;
  private _urlCourses_demo: string = webApi.domain + webApi.url.getCourses_demo;

  public data: any = {};


  // public param:any = {
  //   tId: 1
  // };

  private headers;
  private options;
  parentCatId: any;
  countLevel: number;
  categoryName: any;

  constructor(@Inject('APP_CONFIG_TOKEN') private config: AppConfig, private _http: HttpClient, private authenticationService: AuthenticationService) {
    // add authorization header with jwt token
    // this.headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    // this.options = new RequestOptions({ headers: this.headers });
  }

  disableCourse(visibleData) {
    let url: any = this._urlDisableCourse;
    // let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    // let options = new RequestOptions({ headers: headers });
    // return this._http.post(url,visibleData)
    //     .map((response:Response)=>response.json())
    //     .catch(this._errorHandler);

    return new Promise(resolve => {
      this._http.post(url, visibleData)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  getCourses(data) {
    // let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    // let options = new RequestOptions({ headers: headers });
    // return this._http.post(this._urlCourses,this.param)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);


    return new Promise(resolve => {
      // this._http.post(this._urlCourses, data)
      this._http.post(this._urlCourses_demo, data)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  getCategories(data) {
    // let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    // let options = new RequestOptions({ headers: headers });
    // return this._http.post(this._urlCategories,this.param)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);
    return new Promise(resolve => {
      this._http.post(this._urlCategories, data)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  dublicateCourse(visibleData) {
    let url: any = this._urlDublicateCourse;
    return new Promise(resolve => {
      this._http.post(url, visibleData)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  _errorHandler(error: Response) {
    console.error(error);
    return Observable.throw(error || "Server Error")
  }
}
