import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MyDatePickerModule } from 'mydatepicker';
import { FilterPipeModule  } from 'ngx-filter-pipe';
import { Content } from './content';
import { ContentService } from './content.service';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { TagInputModule } from 'ngx-chips';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgxContentLoadingModule } from 'ngx-content-loading';
import { ComponentModule } from '../../../../component/component.module';

// import { SidebarComponent } from '../../../../component/sidebar/sidebar.component';



// import { AddeditccategoryService } from './addEditCategory/addEditCategory.service';

@NgModule({
  imports: [
    AngularMultiSelectModule,
    CommonModule,
    FormsModule,
    FilterPipeModule,
    TagInputModule,
    NgxSkeletonLoaderModule,
    NgxContentLoadingModule,
    ComponentModule,
  ],
  declarations: [
    Content,
    // SidebarComponent,
  ],
  providers: [
    ContentService,
    // AddeditccategoryService
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
  ]
})

export class ContentModule {}
