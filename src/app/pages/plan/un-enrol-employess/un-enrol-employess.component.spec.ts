import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnEnrolEmployessComponent } from './un-enrol-employess.component';

describe('UnEnrolEmployessComponent', () => {
  let component: UnEnrolEmployessComponent;
  let fixture: ComponentFixture<UnEnrolEmployessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnEnrolEmployessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnEnrolEmployessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
