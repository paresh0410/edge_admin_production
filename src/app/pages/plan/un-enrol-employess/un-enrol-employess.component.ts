import { Component, OnInit, ViewChild, ChangeDetectorRef } from "@angular/core";
// import { ToasterService, Toast } from 'angular2-toaster';
import { NgxSpinnerService } from "ngx-spinner";
import { webAPIService } from "../../../service/webAPIService";
// import { webApi } from '../../../service/webApi';
import { DatePipe } from "@angular/common";
import { UnEnrolEmployessService } from "./un-enrol-employess.service";
import { Router, ActivatedRoute } from "@angular/router";
import { XlsxToJsonService } from "../../plan/users/uploadusers/xlsx-to-json-service";
import { JsonToXlsxService } from "./json-to-xlsx.service";
import { NULL_EXPR } from "@angular/compiler/src/output/output_ast";
import { Switch } from "@syncfusion/ej2-buttons";
import { ToastrService } from "ngx-toastr";
import { CommonFunctionsService } from "../../../service/common-functions.service";
import { webApi } from "../../../service/webApi";
import { SuubHeader } from "../../components/models/subheader.model";
import { DataSeparatorService } from '../../../service/data-separator.service';

@Component({
  selector: "ngx-un-enrol-employess",
  templateUrl: "./un-enrol-employess.component.html",
  styleUrls: ["./un-enrol-employess.component.scss"],
})
export class UnEnrolEmployessComponent implements OnInit {
  allowUpload = false;
  header: SuubHeader = {
    // title:'Bulk Unenrol',
    btnsSearch: true,
    btnBackshow: true,
    showBreadcrumb: true,
    btnName1show: true,
    btnName1: "Upload",
    btnNameupload: true,
    // uploadsheetLink: "assets/images/Demo_sheet_for_unenrol_Courses.xlsx",
    breadCrumbList: [],
  };
  @ViewChild("myTable") table: any;
  private xlsxToJsonService: XlsxToJsonService = new XlsxToJsonService();
  rows = [];
  selected = [];
  templateExelLink: any = "assets/images/Asset_Upload_Template.xlsx";
  /*
    uploadDetailsOne: any = [
      { assetName: "B2B sales", date: '12/04/2019', category: 'Abc', format: '.xls', channel: 'akdf', language: 'english' },
      { assetName: "B2B sales", date: '12/04/2019', category: 'Abc', format: '.xls', channel: 'akdf', language: 'english' },
      { assetName: "B2B sales", date: '12/04/2019', category: 'Abc', format: '.xls', channel: 'akdf', language: 'english' },
      { assetName: "B2B sales", date: '12/04/2019', category: 'Abc', format: '.xls', channel: 'akdf', language: 'english' },
      { assetName: "B2B sales", date: '12/04/2019', category: 'Abc', format: '.xls', channel: 'akdf', language: 'english' },
      { assetName: "B2B sales", date: '12/04/2019', category: 'Abc', format: '.xls', channel: 'akdf', language: 'english' },
      { assetName: "B2B sales", date: '12/04/2019', category: 'Abc', format: '.xls', channel: 'akdf', language: 'english' },
    ]
    */
  userId: any;
  AllEmployess: any = [];
  Allpartners: any = [];
  validDataFinal: any = [];
  statusData: any = [];
  AllAssetDropdown: any = [];
  resultSheets: any = [];
  result: any = [];
  uploadedData: any = [];
  damapproval: any = [];
  lang: any = [];
  fileformatf: any = [];
  filechannel: any = [];
  cat: any = [];
  validdata: any = [];
  invaliddata: any = [];
  tenantId: any = 1;
  menuId = null;
  // dateFormatNew = /^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/g.;
  labels: any = [
    { labelname: "STATUS", bindingProperty: "status", componentType: "text" },
    { labelname: "REASON", bindingProperty: "reason", componentType: "text" },
    { labelname: "ECN", bindingProperty: "ecn", componentType: "text" },
    { labelname: "CODE", bindingProperty: "code", componentType: "text" },
  ];

  constructor(
    // private toasterService: ToasterService,
    private spinner: NgxSpinnerService,
    public router: Router,
    public routes: ActivatedRoute,
    protected webApiService: webAPIService,
    private datePipe: DatePipe,
    private bulkUploadService: UnEnrolEmployessService,
    private exportService: JsonToXlsxService,
    private commonFunctionService: CommonFunctionsService,
    private toastr: ToastrService,
    private cdf: ChangeDetectorRef,
    private dataSeparatorService: DataSeparatorService,
  ) {
    // this.rows = this.uploadDetailsOne;
    if (localStorage.getItem("LoginResData")) {
      const userData = JSON.parse(localStorage.getItem("LoginResData"));
      console.log("userData", userData.data);
      this.userId = userData.data.data.id;
      console.log("userId", userData.data.data.id);
      this.tenantId = userData.data.data.tenantId;
    }
    this.menuId =
      this.routes.snapshot.params["menuId"] == undefined
        ? 0
        : Number(this.routes.snapshot.params["menuId"]);
    console.log('menuId', this.menuId);
    if(this.menuId == 105){
      this.header.title = 'Bulk Unenrol';
      this.header.uploadsheetLink = 'assets/images/Demo_sheet_for_unenrol_Courses.xlsx';
    }else{
      this.header.title = 'Bulk Enrol';
      this.header.uploadsheetLink = 'assets/images/Demo_sheet_for_enrol_Courses.xlsx';
    }
    // this.getAllEmployees();
    // this.getAllparterns();
    // this.getAllNominatedEmployees();
    // this.getAllAssetDropdown();
  }

  ngOnInit() {
    this.header.breadCrumbList = [
      {
        name: "Settings",
        navigationPath: "/pages/plan",
      },
    ];
  }

  presentToast(type, body) {
    if (type === "success") {
      this.toastr.success(body, "Success", {
        closeButton: false,
      });
    } else if (type === "error") {
      this.toastr.error(
        'Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.',
        "Error",
        {
          timeOut: 0,
          closeButton: true,
        }
      );
    } else {
      this.toastr.warning(body, "Warning", {
        closeButton: false,
      });
    }
  }

  bulkUploadAssetData: any = null;
  fileReaded: any;
  enableUpload: boolean = false;
  fileUrl: any;

  fileUpload: any;
  fileName: String = "You can drag and drop files here to add them.";
  fileIcon: any = "assets/img/app/profile/avatar4.png";
  selectFileTitle: any = "No file chosen";
  cancelFile() {
    this.bulkUploadAssetData = null;
    if (this.fileUpload) {
      if (this.fileUpload.nativeElement) {
        this.fileUpload.nativeElement.value = null;
      }
    }
    this.allowUpload = false;
    this.fileName = "You can drag and drop files here to add them.";
    this.selectFileTitle = "No file chosen";
  }

  passParams: any;
  assetFileData: any;
  fileUploadRes: any;
  fileres: any;
  errorMsg: any;
  uploadedFile: any;
  submit() {
    this.spinner.show();
    var option: string = Array.prototype.map
      .call(this.validdata, function (item) {
        console.log("item", item);
        return Object.values(item).join("#");
      })
      .join("|");
    const param = {
      tId: this.tenantId,
      userId: this.userId,
      option: option,
    };
    console.log("params", param);
    const _urlinsertBulkUploadParticipent: string =
      webApi.domain + webApi.url.insertBulkUploadParticipent;
    this.commonFunctionService
      .httpPostRequest(_urlinsertBulkUploadParticipent, param)
      //this.bulkUploadService.insertAssetBulkUpload(param)
      .then(
        (rescompData) => {
          this.spinner.hide();
          const result = rescompData;
          console.log("Bulk Upload assets:", rescompData);
          if (result["type"] == true) {
            // const toast: Toast = {
            //   type: 'success',
            //   title: 'Bulk Upload.',
            //   body: 'Uploaded successfully.',
            //   showCloseButton: true,
            //   timeout: 2000,
            // };
            this.spinner.hide();
            // this.toasterService.pop(toast);
            this.presentToast("success", "File uploaded");
            this.back();
          } else {
            // var toast: Toast = {
            //   type: 'error',
            //   title: 'Bulk Upload.',
            //   body: 'Unable to upload.',
            //   showCloseButton: true,
            //   timeout: 2000,
            // };
            this.spinner.hide();
            // this.toasterService.pop(toast);
            this.presentToast("error", "");
          }
        },
        (error) => {
          this.spinner.hide();
          // var toast: Toast = {
          //   type: 'error',
          //   //title: "Server Error!",
          //   body: 'Something went wrong.please try again later.',
          //   showCloseButton: true,
          //   timeout: 2000,
          // };
          // this.toasterService.pop(toast);
          this.presentToast("error", "");
        }
      );
  }

  back() {
    this.router.navigate(["../../"], { relativeTo: this.routes });
  }

  // uploadData(){
  //   if(this.menuId == 105){
  //     this.uploadSheet();
  //   }else {
  //     this.uploadEnrolSheet();
  //   }
  // }

  // empobj:any={}
  // getAllEmployees() {
  //   const param = {
  //     'srcStr': null,
  //     'tenantId': this.tenantId,
  //   };
  //   this.bulkUploadService.getAllEmpListUP(param)
  //     .then(rescompData => {
  //       this.AllEmployess = rescompData['data'][0];
  //       if(this.AllEmployess.length > 0)
  //       {
  //         this.AllEmployess.forEach((element,i) => {
  //           this.empobj[element.code] = i + 1;
  //         });
  //         console.log('empobj:', this.empobj);
  //       }
  //       console.log('AllEmployess:', this.AllEmployess);
  //     });
  // }
  // partObj:any={};
  // getAllparterns() {
  //   const param = {
  //     'tId': this.tenantId,
  //   };
  //   this.bulkUploadService.getAllpartnersListUP(param).then(res => {
  //     this.Allpartners = res['data'][0];
  //     if(this.Allpartners.length > 0)
  //       {
  //         this.Allpartners.forEach((element,i) => {
  //           this.partObj[element.partnerName] = i + 1;
  //         });
  //         console.log('partObj:', this.partObj);
  //       }
  //     console.log('AllEdgeEmployessDAM:', this.Allpartners);
  //   });
  // }
  // nomiObj:any={};
  // getAllNominatedEmployees() {

  // }

  readFileUrl(event: any) {
    if(event.target.files && event.target.files.length == 0){
      return null;
    }
    this.uploadedData = [];
    this.result = [];
    const validExts = new Array(".xlsx", ".xls");
    let fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf("."));
    if (validExts.indexOf(fileExt) < 0) {
      // var toast: Toast = {
      //   type: 'error',
      //   title: 'Invalid file selected!',
      //   body: 'Valid files are of ' + validExts.toString() + 'types.',
      //   showCloseButton: true,
      //   // tapToDismiss: false,
      //   timeout: 2000,
      // };
      // this.toasterService.pop(toast);
      this.presentToast(
        "warning",
        "Valid file types are " + validExts.toString()
      );
      this.allowUpload = false;
      this.cancelFile();
    } else {
      // return true;
      if (event.target.files && event.target.files[0]) {
        this.fileName = event.target.files[0].name;
        // read file from input
        this.fileReaded = event.target.files[0];

        if (
          this.fileReaded != "" ||
          this.fileReaded != null ||
          this.fileReaded != undefined
        ) {
          this.enableUpload = true;
        }

        let file = event.target.files[0];
        this.bulkUploadAssetData = event.target.files[0];
        console.log(
          "this.bulkUploadAssetDataFileRead",
          this.bulkUploadAssetData
        );

        this.xlsxToJsonService.processFileToJson({}, file).subscribe((data) => {
          this.resultSheets = Object.getOwnPropertyNames(data["sheets"]);
          console.log("File Property Names ", this.resultSheets);
          let sheetName = this.resultSheets[0];
          this.result = data["sheets"][sheetName];
          console.log("dataSheet", data);

          if (this.result.length > 0) {
            this.uploadedData = this.result;
          }
        });
        var reader = new FileReader();
        reader.onload = (event: ProgressEvent) => {
          this.fileUrl = (<FileReader>event.target).result;
          /// console.log(this.fileUrl);
        };
        reader.readAsDataURL(event.target.files[0]);
        this.allowUpload = true;
      }
    }
  }
  showInvalidExcel: boolean = false;
  showTable: boolean = false;
  uploadSheet() {
    this.spinner.show();
    // this.spinner.show()
    this.validdata = [];
    this.invaliddata = [];
    this.validDataFinal = [];
    this.statusData = [];
    console.log("this.uploadedData", this.uploadedData);
    if (this.uploadedData.length > 0) {
      // this.AllEmployess.some((item,i) => {
      this.uploadedData.forEach((data, j) => {
        if (!data.ecn) {
          this.uploadedData[j].status = "Invalid";
          this.uploadedData[j].reason = "ECN is required";
          // this.spinner.hide()
          return;
        } else if (!data.code) {
          this.uploadedData[j].status = "Invalid";
          this.uploadedData[j].reason = "Code is required";
          // this.spinner.hide()
          return;
        } else {
          this.uploadedData[j].status = "Valid";
          this.uploadedData[j].reason = "";
          // this.spinner.hide()
          return;
        }
      });
      console.log("this.uploadedData", this.uploadedData);
    }
    if (this.uploadedData.length > 0) {
      this.uploadedData.forEach((element) => {
        if (element.status === "Valid") {
          this.validdata.push(element);
        } else {
          this.invaliddata.push(element);
        }
      });
      const param = {
        option: JSON.stringify(this.validdata),
        tId: this.tenantId,
        userId: this.userId,
      };
      const _urlFetchAllNominatedEmployee: string =
        webApi.domain + webApi.url.unenrolValidation;
      this.commonFunctionService
        .httpPostRequest(_urlFetchAllNominatedEmployee, param)
        //this.bulkUploadService.getAllNominatedEmployees(param)
        .then(
          (res) => {
            this.spinner.hide();
            this.statusData = res["data"];
            console.log("AllEdgeEmployessDAM:", this.statusData);
            if (this.statusData.length > 0) {
              this.invaliddata.forEach((item) => {
                this.statusData.push(item);
              });
              this.invaliddata = [];
              this.statusData.forEach((element) => {
                if (element.status === "Invalid") {
                  this.invaliddata.push(element);
                } else {
                  this.validDataFinal.push(element);
                }
              });
              if (this.invaliddata.length > 0) {
                this.showInvalidExcel = true;
              }
              this.presentToast("success", "Participants uploaded");
              this.showTable = true;
              this.cdf.detectChanges();
              this.spinner.hide();
            }
          },
          (resUserError) => {
            this.spinner.hide();
            // this.skeleton = true;
            // this.nopage=true
            this.errorMsg = resUserError;
          }
        );
    }
  }

  uploadEnrolSheet(){
    this.validdata = [];
    this.invaliddata = [];
    this.statusData = [];
    this.showTable = false;
    if (this.uploadedData.length > 0) {
      const finalString = this.prepareDataToUpload(this.uploadedData);
      this.spinner.show();
      this.cdf.detectChanges();
      if(finalString !=""){
        const param = {
          // userId: this.userId,
          finalString: finalString,
          aId: '2',
        };
        let url = "";
        console.log("param", param);
        if(this.menuId == 105){
          url =  webApi.domain + webApi.url.unenrolEmployeesAreaWise;
        }else {
          url =  webApi.domain + webApi.url.enrolEmployeesAreaWise;
        }
        console.log("param", param);
        console.log("url", url);
      this.commonFunctionService
        .httpPostRequest(url, param)
        //this.bulkUploadService.getAllNominatedEmployees(param)
        .then(
          (res) => {
            if(res["type"] && res["output"] && res["output"].length !=0  && Array.isArray(res["output"])){
              this.statusData =  res["output"];
              this.showTable = true;
              for (let index = 0; index <  this.statusData.length; index++) {
                const element =  this.statusData[index];
                if(Number(element['valid']) === 1){
                  element['status'] = 'Valid';
                  this.validdata.push(element);
                }else {
                  delete element['valid'];
                  element['status'] = 'Invalid';
                  this.invaliddata.push(element);
                }
              }
              if(this.invaliddata && this.invaliddata.length !== 0){
                this.showInvalidExcel = true;
              }else {
                this.showInvalidExcel = false;
              }
            }else {
              this.presentToast('Warning', 'Something Went Wrong.');
            }
            console.log("res:", res);
            this.spinner.hide();
            // this.statusData = res["data"];
          },
          (resUserError) => {
            this.spinner.hide();
            this.presentToast('Warning', 'Something Went Wrong.');
            // this.errorMsg = resUserError;
          }
        );
    }else {
        console.log('No data found');
      }
    }else {
      this.presentToast("warning", "Please Upload data to process");
    }
    // this.spinner.show();

  }

  exportToExcelInvalid() {
    this.exportService.exportAsExcelFile(this.invaliddata, "Invalid Data");
  }

  prepareDataToUpload(data){
    let string = "";
    if (!data || !Array.isArray(data)){
      return string;
    }else {
      for (let index = 0; index < data.length; index++) {
        const element = data[index];
        if(string != ""){
          string = string + this.dataSeparatorService.Pipe;
        }
        string = string + String(element['ecn']).trim() + this.dataSeparatorService.Hash + String(element['code']).trim();
      }
    }
    return string;
  }
}
