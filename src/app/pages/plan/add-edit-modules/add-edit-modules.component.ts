import {
  Component,
  OnInit,
  Input,
  ChangeDetectorRef,
  Output,
  EventEmitter,
  OnChanges,
  ViewChild,
  SimpleChanges,
} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  DateTimeAdapter,
  OWL_DATE_TIME_FORMATS,
  OWL_DATE_TIME_LOCALE,
} from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';

export const MY_CUSTOM_FORMATS = {
  fullPickerInput: 'DD-MM-YYYY h:mm:ss A',
  parseInput: 'DD-MM-YYYY h:mm:ss A',
  datePickerInput: 'DD-MM-YYYY',
  timePickerInput: 'LT',
  monthYearLabel: 'MMM YYYY',
  dateA11yLabel: 'LL',
  monthYearA11yLabel: 'MMMM YYYY',
};

@Component({
  selector: 'ngx-add-edit-modules',
  templateUrl: './add-edit-modules.component.html',
  styleUrls: ['./add-edit-modules.component.scss'],
  providers: [
    {
      provide: DateTimeAdapter,
      useClass: MomentDateTimeAdapter,
      deps: [OWL_DATE_TIME_LOCALE],
    },
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS },
  ],
})
export class AddEditModulesComponent implements OnInit, OnChanges {
  @Input() formDataModule: any = {
    moduleId: '',
    courseId: '',
    moduleName: '',
    summary: '',
    visible: '',
    modulePic: 'assets/images/course1.jpg',
    courseOrder: '',
    tenantId: '',
    learnerCreditPoints: '',
    learnerRoleId: 8,
    creditPoints: '',
    creatorId: '',
    moduleCode:'',
    startDate: '',
    endDate: '',
    standardId: '',
    trainers: [
      {
        standardId: '',
        trainerId: '',
        train: [],
        visible: '',
        traineresId: 0,
        isCheck:0
      },
    ],
  };
  @Input() visibility: any = [];
  @Input() tempUsers: any = [];
  @Input() trainerVisible: any = [];
  @Input() courseTypeId: number = null;
  @Input() getEventData: string = '';

  @Output() performAction = new EventEmitter<any>();

  @ViewChild('modf') formval: any;
  @ViewChild('userForm1') trainerForm: any;


  trainer: boolean;
  constructor(private http: HttpClient, public cdf: ChangeDetectorRef) {
    this.getHelpContent();
  }

  ngOnInit() {
    console.log('CourseTypeId =>', this.courseTypeId);
    console.log(this.formDataModule,"dtaattttt")
  }
  settingsCreatorDrop = {
    text: 'Select Trainer',
    singleSelection: true,
    classes: 'common-multi',
    primaryKey: 'trainerName',
    labelKey: 'trainerName',
    // noDataLabel: 'Search Trainer...',
    enableSearchFilter: true,
    searchBy: ['trainerName'],
    maxHeight: 250,
    lazyLoading: true,
  };
  helpContent: any;
  getHelpContent() {
    return new Promise((resolve) => {
      this.http
        .get('../../../../../../assets/help-content/addEditCourseContent.json')
        .subscribe(
          (data) => {
            this.helpContent = data;
            this.cdf.detectChanges();
            console.log('Help Array', this.helpContent);
          },
          (err) => {
            resolve('err');
          }
        );
    });
    // return this.helpContent;
  }

  /*Trainer Multi select Dropdown*/
  // search: any = [];
  // demoUsers: any = [];
  // usersList: any = [];
  // selectedCreator = [];
  // onCreatorSearch(evt: any) {
  //   console.log(evt.target.value);
  //   const val = evt.target.value;
  //   this.usersList = [];
  //   const temp = this.demoUsers.filter(function (d) {
  //     return (
  //       String(d.trainerName)
  //         .toLowerCase()
  //         .indexOf(val) !== -1 ||
  //       d.agencyName.toLowerCase().indexOf(val) !== -1 ||
  //       !val
  //     );
  //   });

  //   // update the rows
  //   this.usersList = temp;
  //   var nomi = {
  //     id: 0,
  //     trainerName: 'Request for Nomination',
  //     standardId: '',
  //     agencyName: ''
  //   };
  //   this.usersList.push(nomi);
  //   console.log(this.usersList, 'userList');
  // }

  // onCreatorSelect(item: any, k) {
  //   this.formDataModule.trainers.forEach(res => {
  //     if (res.train == '') {
  //       this.trainer = false;
  //     }
  //     else {
  //       this.trainer = true;
  //     }

  //   })

  //   console.log(item);
  //   console.log(this.selectedCreator);
  //   for (let j = 0; j < this.formDataModule.trainers.length; j++) {
  //     if (k == j) {
  //       this.formDataModule.trainers[j].trainerId = item.id;
  //     }
  //   }
  //   // for(let i=0;i<this.demoUsers.length;i++)
  //   // {
  //   //   if(this.demoUsers[i].id == item.id)
  //   //   {
  //   //     this.demoUsers.splice(1,i)
  //   //   }
  //   // }
  //   // this.search ='';
  // }
  // OnCreatorDeSelect(item: any, k) {
  //   this.trainer = false;
  //   console.log(item);
  //   for (let j = 0; j < this.formDataModule.trainers.length; j++) {
  //     if (k == j) {
  //       this.formDataModule.trainers[j].trainerId = '';
  //       this.formDataModule.trainers[j].train = [];
  //     }
  //   }
  //   //  for(let i=0;i<this.tempUsers.length;i++)
  //   // {
  //   //   if(this.demoUsers[i].id == item.id)
  //   //   {
  //   //     this.demoUsers.push(item)
  //   //   }
  //   // }
  //   // this.search ='';
  //   console.log(this.selectedCreator);
  // }

  date = new Date();
  setMinDate(startDate, date) {
    if (
      String(Date.parse(startDate)) === 'NaN' ||
      String(Date.parse(date)) === 'NaN'
    ) {
      return new Date();
    } else if (startDate > new Date() || date > new Date()) {
      return new Date();
    } else {
      return startDate;
    }
  }

  passDataToParent(action, ...args) {
    console.log('action to be performed ==>', action, args);
    const event = {
      action: action,
      argument: args,
    };
    console.log('action to be performed ==>', event);
    this.performAction.emit(event);
  }
  // deleteModuleThumb() {
  //   // this.defaultThumb = 'assets/images/category.jpg';
  //   this.formDataModule.modulePic = 'assets/images/category.jpg';
  //   this.moduleImgData = null;
  //   this.formDataModule.categoryPicRefs = undefined;
  // }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.getEventData === 'cancel') {
      this.passDataToParent('closeModuleForm', '');
    } else if (this.getEventData === 'save') {
      // var param ={
      //   formval:this.formval,
      //   trainer:this.trainerForm
      // }
      this.passDataToParent('saveModuleDetails', this.formval,this.trainerForm);
    }
  }
}
