import { Component, ViewContainerRef, OnInit, ViewChild, Compiler, ViewEncapsulation, ElementRef } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { AppService } from '../../../app.service';
import { PaginatePipe, PaginationControlsDirective, PaginationService } from 'ngx-pagination';
import { Router, NavigationStart, Routes, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EmployeesService } from './employees.service';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { FormGroup, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Column, GridOption, AngularGridInstance, FieldType, Filters, Formatters, GridStateChange, OperatorType, Statistic } from 'angular-slickgrid';
import { NbThemeService } from '@nebular/theme';
import { AddEditEmployeesService } from './addeditemployees/addeditemployees.service'
import { UploadEmployeesService } from './uploademployees/uploademployees.service';
// import { ToastsManager } from 'ng2-toastr';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { DatePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { webApi } from '../../../service/webApi';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { OWL_DATE_TIME_FORMATS, DateTimeAdapter, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';
import { SuubHeader } from '../../components/models/subheader.model';
import { BrandDetailsService } from '../../../service/brand-details.service';
import { noData } from '../../../models/no-data.model';

// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
// import * as _moment from 'moment';
// import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
// import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';

// const moment = (_moment as any).default ? (_moment as any).default : _moment;
export const MY_CUSTOM_FORMATS = {
  fullPickerInput: 'DD-MM-YYYY HH:mm:ss',
  parseInput: 'DD-MM-YYYY HH:mm:ss',
  datePickerInput: 'DD-MM-YYYY',
  timePickerInput: 'LT',
  monthYearLabel: 'MMM YYYY',
  dateA11yLabel: 'LL',
  monthYearA11yLabel: 'MMMM YYYY'
};
@Component({
  selector: 'employees',
  templateUrl: './employees.html',
  styleUrls: ['./employees.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    { provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE] },
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS }]
})

export class Employees {
  // @ViewChild('grid')
  // @ViewChild('agGrid') agGrid: AgGridNg2;
  // @ViewChild('agGridReg') agGridReg: AgGridNg2;
  // @ViewChild('agGridUnReg') agGridUnReg: AgGridNg2;
  // showSpinner: boolean = false;
  // this.spinner.show();
  header: SuubHeader = {
    title: '',
    btnsSearch: true,
    drplabelshow: true,
    drpName1show: true,
    btnBackshow: true,
    showBreadcrumb: true,
    breadCrumbList: [
      {
        'name': 'Settings',
        'navigationPath': '/pages/plan',
      }
    ]
  };
  showSpinner = false;
  angularGrid: AngularGridInstance;
  angularGrid1: AngularGridInstance;
  unRegGrid: AngularGridInstance;
  regGrid: AngularGridInstance;
  enrolGrid: AngularGridInstance;

  columnDefinitions: Column[];
  columnDefinitions1: Column[];
  unRegcolumnDefinitions: Column[];
  regColumnDefinitions: Column[];
  enrolColumnDefinitions: Column[];

  gridOptions: GridOption;

  gridObj: any;
  gridObj1: any;
  unRegGridObj: any;
  regGridObj: any;
  enrolGridObj: any;

  dataset: any[];
  dataset1: any[];
  unRegDataset: any[];
  regDataset: any[];
  enrolDataset: any[];

  selectedUnReg: any[];
  selectedReg: any[];
  selectedEnrol: any[];
  selectedTitles: any[];
  selectedTitle: any;

  errorMsg: string;
  loader: any;
  toggleBool: boolean = true;
  selectedAll: any;
  userChecked: any = [];

  tab1: any = false;
  tab2: any = false;
  tab3: any = false;

  selectedRows: any;
  selectedRowsReg: any;
  selectedRowsUnReg: any;

  btnShowSingleReg: any = false;
  btnShowMultipleReg: any = false;

  btnShowSingleUnReg: any = false;
  btnShowMultipleUnReg: any = false;

  unRegRowData: any = [];
  unRegRowDataCount: any = 0;

  regRowData: any = [];
  regRowDataCount: any = 0;

  enrolRowData: any = [];
  enrolRowDataCount: any = 0;

  userTableData: any = [];
  userInductedData: any = [];
  contdata: any;

  tabCont1: boolean = false;
  tabCont2: boolean = false;
  tabCont3: boolean = false;

  dateOfJoining = new Date();

  employeeStatus: any = [
    {
      id: 0,
      status: 'All',
    },
    {
      id: 1,
      status: 'Active',
    },
    {
      id: 2,
      status: 'Inactive',
    },
  ];

  @ViewChild('myTable') table: any;
  rows = [];
  selected = [];

  employeeList: any = []
  labels: any = [
    { labelname: 'Unique Code', bindingProperty: 'ecn', componentType: 'text' },
    { labelname: 'FULL NAME', bindingProperty: 'fullname', componentType: 'text' },
    { labelname: 'EMAIL', bindingProperty: 'email', componentType: 'text' },
    { labelname: 'MOBILE', bindingProperty: 'phone', componentType: 'text' },
    { labelname: 'DATE OF JOINING', bindingProperty: 'doj', componentType: 'text' },
    { labelname: 'USER STATUS', bindingProperty: 'isActive', componentType: 'text' },
    // { labelname: 'Re - Certification', bindingProperty: 'isRecert', componentType: 'text' },
    // { labelname: 'ACTION', bindingProperty: 'btntext', componentType: 'button' },
  ]

  formData: any = {
    doj: new Date(),
    status: 0,
    str: '',
  }
  userLoginData: any = [];
  currentPage = 1;
  totalPages: any;
  str = '';
  isworking = false;
  readonly headerHeight = 50;
  readonly rowHeight = 50;
  currentBrandData: any;
  noData: boolean = false;
  noDataVal: noData = {
    margin: 'mt-5',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title: "Sorry we couldn't find any matches",
    desc: ".",
    titleShow: true,
    btnShow: false,
    descShow: false,
    btnText: 'Learn More',
    btnLink: ''
  }
  constructor(private themeService: NbThemeService, private router: Router,
    protected service: EmployeesService, vcr: ViewContainerRef, public routes: ActivatedRoute, public el: ElementRef,
    protected uploadService: UploadEmployeesService, protected passService: AddEditEmployeesService,
    private AppService: AppService, private toastr: ToastrService, private spinner: NgxSpinnerService,
    private datepipe: DatePipe, private http1: HttpClient,
    public brandService: BrandDetailsService,
    private commonFunctionService: CommonFunctionsService,
  ) {
    // this.getHelpContent();
    if (localStorage.getItem('LoginResData')) {
      this.userLoginData = JSON.parse(localStorage.getItem('LoginResData'));
    }
    this.contdata = this.AppService.getuserdata();

  }

  ngOnInit(): void {
    console.log(this.formData.doj, 'doj');
    const currentDate = new Date();
    let lastmonthDate = new Date();
    lastmonthDate = new Date(lastmonthDate.setMonth(lastmonthDate.getMonth() - 1));
    this.formData.doj = [lastmonthDate, currentDate];
    this.currentBrandData = this.brandService.getCurrentBrandData();
    this.header.title = this.currentBrandData.employee;
    this.header.dropdownlabel = 'New ' + this.currentBrandData.employee + 's';
    this.header.drpName1 = 'Upload ' + this.currentBrandData.employee + 's';
    this.noDataVal = {
      margin: 'mt-5',
      imageSrc: '../../../../../assets/images/no-data-bg.svg',
      title: "Sorry we couldn't find any " + this.currentBrandData.employee + 's please try again',
      desc: ".",
      titleShow: true,
      btnShow: false,
      descShow: false,
      btnText: 'Learn More',
      btnLink: ''
    }
    if (this.currentBrandData.employee === 'Employee') {
      this.getHelpContent();
    } else {
      this.getContent();
    }
    this.getEmpData(this.str, this.currentPage);
  }

  // new changes

  selectedEmployees: any = [];
  onSelect(event: any) {
    console.log('on select event', event);
    this.selectedEmployees = event.selected;
  }
  public uploadUsers() {
    this.router.navigate(['upload-employees'], { relativeTo: this.routes });
  }
  clearSearch() {
    this.formData.str = '';
  }
  // presentToast(type, body) {
  //   if (type === 'success') {
  //     this.toastr.success(body, 'Success', {
  //       closeButton: false
  //     });
  //   } else if (type === 'error') {
  // tslint:disable-next-line:max-line-length
  //     this.toastr.error('Please contact site administrator and <a (click)='giveFeedback()'> <strong> click here </strong></a> to leave your feedback.', 'Error', {
  //       timeOut: 0,
  //       closeButton: true
  //     });
  //   } else {
  //     this.toastr.warning(body, 'Warning', {
  //       closeButton: false
  //     })
  //   }
  // }

  back() {
    console.log(event, 'event')
    this.router.navigate(['pages/plan']);
  }
  formatDate(date) {
    // if (date) {
    //   const d = new Date(date);
    //   const formatted = this.datepipe.transform(d, 'yyyy-MM-dd');
    //   return formatted;
    // }
    const d = new Date(date);
    // const formatted = this.datepipe.transform(d, 'yyyy-MM-dd');
    const formattedTime = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate() + ' ' + '00:00:00';
    return formattedTime;
  }
  searchEmpData() {
    this.str = '',
      this.currentPage = 1,
      console.log(this.formData);
    this.getEmpData(this.str, this.currentPage);
  }
  /*This function use  for getting all inducted employees */
  getEmpData(str, pageNo) {
    // this.showSpinner = true;
    this.spinner.show();
    const params = {
      sDate: this.formatDate(this.formData.doj[0]),
      eDate: this.formatDate(this.formData.doj[1]),
      sStatus: Number(this.formData.status) ? (Number(this.formData.status) === 1 ? 'Active' : 'Inactive') : 'all',
      tId: this.userLoginData.data.data.tenantId,
      lmt: 100,
      pno: pageNo,
      str: this.formData.str,
    }
    if (pageNo > 1) {
      this.spinner.show();
    }
    const _urlgetallemployee: string = webApi.domain + webApi.url.getallemployee;
    this.commonFunctionService.httpPostRequest(_urlgetallemployee, params).then(rescompData => {
      // this.loader =false;
      // this.spinner.hide();
      if (rescompData['type'] === true) {
        this.totalPages = rescompData['data']['totalPages'];
        const list = rescompData['data']['list'];
        if (this.employeeList.length > 0 && pageNo > 1) {
          this.employeeList = this.employeeList.concat(list);
        } else {
          this.employeeList = list;
        }
        if (this.employeeList.length == 0) {
          this.noData = true;
        } else {
          this.noData = false;
        }
        this.isworking = false;
        console.log('employeeList ', this.employeeList);
      }
      // this.showSpinner = false;
      this.spinner.hide();
    },
      resUserError => {
        this.noData = true;
        // this.loader =false;
        // this.spinner.hide();
        // this.showSpinner = false;
      this.spinner.hide();
        this.errorMsg = resUserError;
      });
    // this.service.getAllEMPloyees(params).then(rescompData => {
    //   // this.loader =false;

    //   this.spinner.hide();
    //   if (rescompData['type'] === true) {
    //     this.totalPages = rescompData['data']['totalPages'];
    //     const list = rescompData['data']['list'];
    //     if (this.employeeList.length > 0 && pageNo > 1) {
    //       this.employeeList = this.employeeList.concat(list);
    //     } else {
    //       this.employeeList = list;
    //     }
    //     this.isworking = false;
    //     console.log('employeeList ', this.employeeList);
    //   }
    // },
    //   resUserError => {
    //     // this.loader =false;
    //     this.spinner.hide();
    //     this.errorMsg = resUserError;
    //   });
  }

  onScrollDown(offsetY: number, pageload: boolean) {
    const viewHeight =
      this.el.nativeElement.getBoundingClientRect().height - this.headerHeight;
    console.log(viewHeight);
    if (!this.isworking) {
      this.currentPage++;
      if (this.currentPage <= this.totalPages) {
        this.isworking = true;
        this.getEmpData(this.str, this.currentPage);
      }
    }
  }
  updatAttendanceStatus(status) {
    if (this.selectedEmployees.length > 0) {
      let temArray = [];
      this.selectedEmployees.forEach(element => {
        temArray.push(element.userid)
      });

      const strDemo: string = temArray.join();
      const params = {
        strEmp: strDemo,
        attenStatus: status,
        tId: this.userLoginData.data.data.tenantId,

      };
      const _urlUpdateEmployeeStatus = webApi.domain + webApi.url.updateEMPAttendanceStatus;
      this.commonFunctionService.httpPostRequest(_urlUpdateEmployeeStatus, params).then(data => {
        // this.loader =false;

        this.spinner.hide();
        if (data['type'] == true) {
          if (status === 'Active') {
            this.commonFunctionService.presentToast(200, 'Attendance Marked ', 'Information');
          } else if (status === 'Inactive') {
            // this.presentToast('success', 'Attendance Unmarked');
            this.commonFunctionService.presentToast(200, 'Attendance Unmarked ', 'Information');
          }

          this.selectedEmployees = [];
          this.getEmpData(this.str, this.currentPage);
        } else {
          this.loader = false;
          this.commonFunctionService.presentToast(500, '', '');
          // this.presentToast('error', '');
        }
      },
        resUserError => {
          // this.loader =false;
          this.spinner.hide();
          this.loader = false;
          this.commonFunctionService.presentToast(500, '', '');
          //     console.error('Error enrolling user!');
        });
      // console.log('Checked users id ',userid);
      // this.service.UPdateAttendanceStatus(user).then(
      //   data => {
      //     if (data['type'] == true) {
      //       if (status === 'Y') {
      //         this.presentToast('success', 'Attendance Marked ');
      //       } else if (status === 'N') {
      //         this.presentToast('success', 'Attendance Unmarked');
      //       }

      //       this.selectedEmployees = [];
      //       this.getEmpData(this.str, this.currentPage);
      //     } else {
      //       this.loader = false;
      //       this.presentToast('error', '');
      //     }
      //   },
      //   error => {
      //     this.loader = false;
      //     console.error('Error enrolling user!');

      //   }
      // );
    }
  }

  // Help Code Start Here //

  helpContent: any;
  getHelpContent() {
    // return new Promise(resolve => {
    //   this.http1
    //     .get('../../../../../../assets/help-content/addEditCourseContent.json')
    //     .subscribe(
    //       data => {
    //         this.helpContent = data;
    //         console.log('Help Array', this.helpContent);
    //       },
    //       err => {
    //         resolve('err');
    //       }
    //     );
    // });
    // return this.helpContent;
    this.commonFunctionService.getHelpContent().then(
      data => {
        this.helpContent = data;
        console.log('Help Array', this.helpContent);
      },
      err => {
        console.log('err');
      }
    );
  }
  getContent() {
    this.commonFunctionService.getLabel().then(
      data => {
        this.helpContent = data;
        console.log('Help Array', this.helpContent);
      },
      err => {
        console.log('err');
      }
    );
  }

  // Help Code Ends Here //

  // showToaster(type){
  //   switch(type){
  //     case 'Succeess': this.toastr.success('Message', 'Helloooo', {timeOut: 0, extendedTimeOut: 0, closeButton: true});
  //                     break;
  //     case 'Warning': this.toastr.warning('Message', 'Helloooo', {timeOut: 0, extendedTimeOut: 0,  closeButton: true});
  //                     break;
  //     case 'Info': this.toastr.info('Message', 'Helloooo', {timeOut: 0, extendedTimeOut: 0,  closeButton: true});
  //                     break;
  //     case 'Error': this.toastr.error('Message', 'Helloooo', {timeOut: 0, extendedTimeOut: 0,  closeButton: true});
  //                     break;
  //   }
  // }
}
