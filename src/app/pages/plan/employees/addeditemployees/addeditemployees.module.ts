import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

import { NgxEchartsModule } from 'ngx-echarts';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ThemeModule } from '../../../../@theme/theme.module';
//import { slikgridDemo } from './../../..../slikgridDemo.component';
//import { slikgridDemoService } from './slikgridDemo.service';

import { AddeditemployeesComponent } from './addeditemployees.component';
import { AddEditEmployeesService } from './addeditemployees.service'
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import {TabsModule} from "ngx-tabs";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { TranslateModule } from '@ngx-translate/core';
//import { AngularSlickgridModule } from 'angular-slickgrid';

// const PLAN_COMPONENTS = [
//   slikgridDemo
// ];

@NgModule({
  imports: [
    ThemeModule,
    NgxEchartsModule,
    NgxMyDatePickerModule.forRoot(),
    TabsModule,
    ReactiveFormsModule,
    FormsModule,
    // UsersModule,
    //AngularSlickgridModule.forRoot(),
    TranslateModule.forRoot(),
    AngularMultiSelectModule
  ],
  declarations: [
    AddeditemployeesComponent,
  ],
  providers: [
    //slikgridDemoService,
    AddEditEmployeesService
  ],
  schemas: [ 
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA 
  ]
})
export class AddeditemployeesModule { }
