import { Component, ViewEncapsulation, Directive,forwardRef,Attribute,OnChanges,SimpleChanges,Input, ViewChild, ViewContainerRef,OnInit } from '@angular/core';
import { AddEditEmployeesService } from './addeditemployees.service';
import { NG_VALIDATORS, Validator, Validators, AbstractControl, ValidatorFn, FormGroup } from '@angular/forms';
import { LocalDataSource } from 'ng2-smart-table';
//import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import { PaginatePipe, PaginationControlsDirective, PaginationService } from 'ngx-pagination';
import { Router, NavigationStart, ActivatedRoute } from '@angular/router';
import { Employees }    from '../employees.component';
//import { IMyDpOptions ,IMyDateModel, IMyDate} from 'mydatepicker';
import {INgxMyDpOptions, IMyDateModel} from 'ngx-mydatepicker';
// import { ToastrService  } from 'ngx-toastr';

//import { ToastsManager } from 'ng2-toastr';
import { ToastrService } from 'ngx-toastr';
import { FormlyFieldConfig } from '@ngx-formly/core';
// import {ToasterModule, ToasterService, Toast} from 'angular2-toaster';

@Component({
  selector: 'ngx-addeditemployees',
  templateUrl: './addeditemployees.component.html',
  styleUrls: ['./addeditemployees.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AddeditemployeesComponent {
  public  today=new Date();
  //var year=myDate.getFullYear();
  public myDatePickerOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'yyyy-mm-dd',
        disableSince: {year: this.today.getFullYear(), month: this.today.getMonth()+1, day: this.today.getDate()},

    };
    public myDatePickerOptions1: INgxMyDpOptions = {
      // other options...
      dateFormat: 'yyyy-mm-dd',
      //disableSince: {year: 2018, month: 10, day: 10},

  };
  public placeHolder: string = 'Select a date';

  // dob :IMyDate = {year:0,month:0,day:0};

  // Initialized to specific date (09.10.2018).
  public model1: any = { date: { year: 2018, month: 10, day: 9 } };
  maxDate:Date;
  title:any;
  para:any;
  url:any;
  id:any;
  profileUrl : any = 'assets/images/profile1.png';
  dateOfBirth:any={
    date:{
      year:2018,
      month:10,
      day:9
    }
  };
  dropdownListDept = [];
  selectedItemsDept = [];
  dropdownSettingsDept = {};
  dropdownListRole = [];
  selectedItemsRole = [];
  dropdownSettingsRole = {};
  dropdownListBand = [];
  selectedItemsBand = [];
  dropdownSettingsBand = {};
 // model = new Users('',null,'','','');
  submitted = false;
  tenantId: any;
  onSubmit() { this.submitted = true; }
  // onSubmit(formdata) {
  //   this.submitted = true;    
  //   let user = formdata;
  //       if(this.para.id == 1) {
  //         this.updateUser(user);
  //       } else {
  //         this.createUser(formdata);
  //       }
  // }
  
  addEdit(formdata){
    console.log(formdata);  
    let user = formdata;
        if(this.para.id == 1) {
          this.updateUser(user);
        } else {
          this.createUser(formdata);
        }
  }
  readUrl(event:any) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.onload = (event: ProgressEvent) => {
        this.profileUrl = (<FileReader>event.target).result;
      }

      reader.readAsDataURL(event.target.files[0]);
    }
  }

  deleteProfile() {
  //   let user = formdata;
      this.profileUrl = 'assets/img/app/profile/avatar4.png';
  }

	  strArraySta :any =[
      {
          sName: 'Active',
          sId : 'Y'
        },
        {
          sName: 'Inactive',
          sId : 'N'
        }]

    strArraySkilllevel :any =[{
          sName: 'Beginner',
          sId : 'Beginner' 
        },
        {
          sName: 'Intermediate',
          sId : 'Intermediate' 
        },
        {
          sName: 'Expert',
          sId : 'Expert' 
        }]

    strArrayVer :any =[{
          vName: 'Verified',
          vId : 'Y' 
        },
        {
          vName: 'Not Verified',
          vId : 'N' 
        }]

    strArrayCoc :any =[{
          vName: 'Verified',
          vId : 'Y' 
        },
        {
          vName: 'Not Verified',
          vId : 'N' 
        }] 

    strArrayAttFlag :any =[
        {
          vName: 'Off-Role',
          vId : 'y' 
        },
        {
          vName: 'New On-Role',
          vId : 'n' 
        },
        {
          vName: 'Existing On-Role',
          vId : 'y' 
        }]      

    strArrayGender :any =[{
          gName: 'Male',
          gId : 'Male' 
        },
        {
          gName: 'Female',
          gId : 'Female' 
        }] 
        
    // rolearray:any=[
    //   {

    //   }
    // ]

    usersData:any =[{
                    username:'',
                    firstname:'',
                    lastname:'',
                    phone1:'',
                    email:'',
                  }]    

    empData:any =[{
                  username:'1111',
                  password:'abc',
                  firstname:'abc',
                  lastname:'xyz',
                  email:'abc@xyz.com',
                  phone1:'123456789',
                  city:'Mumbai',
                  country:'India',
                  skilllevel:'Beginner',
                  doj:'2018-12-08',
                  otp:'Y',
                  coc:'Y',
                  attFlag:'Y',
                  dob:'2018-12-08',
                  dept:'IT',
                  }]              

    formdata:any ={
                    cohort:'',
                    cohortidnumber:'',
                    code:'',
                    name:'',
                    institution:'',
                    business:'',
                    department:'',
                    sdepartment:'',
                    ssdepartment:'',
                    orgcode:'',
                    zone:'',
                    state:'',
                    city:'',
                    inductionLocation:'',
                    role:'',
                    band:'',
                    level:'',
                    empclassification:'',
                    gender:'',
                    presentaddress:'',
                    presentcity:'',
                    presentstate:'',
                    presentpincode:'',
                    permanentadd:'',
                    permanentcity:'',
                    permanentstate:'',
                    permanentpincode:'',
                    contactDetails:'',
                    email:'',
                    personalmobile:'',
                    personalemail:'',
                    dob:'',
                    doj:'',
                    employeestatus:'',
                    reportingmanagercode:'',
                    reportingmanagername:'',
                    skiplevelmcode:'',
                    skiplevelmname:'',
                    firstname:'',
                    lastname:'',
                    empcategory:'',
                    rmemail:'',
                    trainingvenue:'',
                    attinfodata:''
                  }

    userCheck:any = [];
    errorMsg: string;
    email: string;

    form: FormGroup = new FormGroup({});

    userFields : FormlyFieldConfig[] = [{
      className: 'row',
      fieldGroup: [{
          className: 'col-xs-6',
          key: 'email',
          type: 'input',
          templateOptions: {
              type: 'email',
              label: 'Email address',
              placeholder: 'Enter email'
          },
          validators: {
            validation: Validators.compose([Validators.required])
          }
      }, {
          className: 'col-xs-6',
          key: 'password',
          type: 'input',
          templateOptions: {
              type: 'password',
              label: 'Password',
              placeholder: 'Password',
              pattern: ''
          },
          validators: {
            validation: Validators.compose([Validators.required])
          }
      }]
    }];
   
    user = {
      email: 'email@gmail.com',
      checked: false
    };  

    dojOldDate:any;
    dobOldDate:any;

 
    roleFeatures = [];

    constructor(protected service: AddEditEmployeesService,private route: ActivatedRoute,private router:Router,
     vcr: ViewContainerRef, 
    //  private toasterService: ToasterService, 
     private toastr: ToastrService) {
     
      // this.tenantId = this.userData.data.data.tenantId;
      // this.toasterService.pop('success', 'Args Title', 'Args Body');
     var dept = [
        {
          id:1,
          itemName:'Collections'
        },
        {
          id:2,
          itemName:'Business Loans'
        },
        {
          id:3,
          itemName:'SE LAP'
        },
        {
          id:4,
          itemName:'Professional Loans'
        },
        {
          id:5,
          itemName:'Insurance'
        },   {
          id:6,
          itemName:'LAS'
        },
        {
          id:7,
          itemName:'Group Learning Academy'
        },
      ];
      this.dropdownListDept =dept
      this.selectedItemsDept = [];
      this.dropdownSettingsDept = {
        singleSelection: false, 
        text:"Select Department",
        selectAllText:'Select All',
        unSelectAllText:'UnSelect All',
        enableSearchFilter: true,
        // badgeShowLimit: 2,
        classes:"myclass custom-class"
      };
      this.dropdownListRole = [
        {
          id:1,
          itemName:'Admin'
        },
        {
          id:2,
          itemName:'Manager'
        },
        {
          id:3,
          itemName:'Student'
        }
      ];
      this.selectedItemsRole = [];
      this.dropdownSettingsRole = {
        singleSelection: false, 
        text:"Select Role",
        selectAllText:'Select All',
        unSelectAllText:'UnSelect All',
        enableSearchFilter: true,
        // badgeShowLimit: 2,
        classes:"myclass custom-class"
      };
      this.dropdownListBand = [
        {
          id:1,
          itemName:'Band 1'
        },
        {
          id:2,
          itemName:'Band 2'
        },
        {
          id:3,
          itemName:'Band 3'
        },
        {
          id:4,
          itemName:'Band 4'
        }
      ];
      this.selectedItemsBand = [];
      this.dropdownSettingsBand = {
        singleSelection: false, 
        text:"Select Band",
        selectAllText:'Select All',
        unSelectAllText:'UnSelect All',
        enableSearchFilter: true,
        // badgeShowLimit: all,
        classes:"myclass custom-class"
      };

      
      this.today=new Date();
      console.log("Object"+this.today+"year"+this.today.getFullYear);
      this.para = this.service.data;

      if(this.para == undefined){
        this.para = {
          id : 0
        }
      }

      this.maxDate=new Date();
      this.maxDate.setDate(this.today.getDate());
      // this.para = 0;
      // this.para.id = this.para == undefined ? 0 : this.para.id;
    	if(this.para.id == 1){
    		this.title = 'Edit User Master';
        this.dojOldDate = this.para.data.doj;
        this.dobOldDate = this.para.data.dob;
        var dojDate:any = this.dojOldDate == 'NA' ? '' : new Date(this.dojOldDate); 
        var dobDate:any = this.dobOldDate == 'NA' ? '' : new Date(this.dobOldDate);
        console.log(this.para.data);
    		this.formdata = {
			    cohort:this.para.data.cohort,
          cohortidnumber:this.para.data.cohortidnumber,
          code:this.para.data.code,           
          name:this.para.data.name,
          userId:this.para.data.userId ,
          institution:this.para.data.institution,
          business:this.para.data.business,
          department:this.para.data.department,
          sdepartment:this.para.data.sdepartment,
          ssdepartment:this.para.data.ssdepartment,
          orgcode:this.para.data.orgcode,
          zone:this.para.data.zone,
          state:this.para.data.state,
          city:this.para.data.city,
          inductionLocation:this.para.data.inductionLocation,
          role:this.para.data.role,
          band:this.para.data.band,
          level:this.para.data.level,
          empclassification:this.para.data.empclassification,
          gender:this.para.data.gender,
          password:this.para.data.password,
          presentaddress:this.para.data.presentaddress,
          presentcity:this.para.data.presentcity,
          presentstate:this.para.data.presentstate,
          presentpincode:this.para.data.presentpincode,
          permanentadd:this.para.data.permanentadd,
          permanentcity:this.para.data.permanentcity,
          permanentstate:this.para.data.permanentstate,
          permanentpincode:this.para.data.permanentpincode,
          contactDetails:this.para.data.contactDetails,
          email:this.para.data.email,
          personalmobile:this.para.data.personalmobile,
          personalemail:this.para.data.personalemail,
          dob:this.para.data.dob == 'NA' ? [] : { date: { year: dobDate.getFullYear(), month: dobDate.getMonth() + 1, day: dobDate.getDate() } },
          doj:this.para.data.doj == 'NA' ? [] : { date: { year: dojDate.getFullYear(), month: dojDate.getMonth() + 1, day: dojDate.getDate() } },
          employeestatus:this.para.data.employeestatus,
          reportingmanagercode:this.para.data.reportingmanagercode,
          reportingmanagername:this.para.data.reportingmanagername,
          skiplevelmcode:this.para.data.skiplevelmcode,
          skiplevelmname:this.para.data.skiplevelmname,
          firstname:this.para.data.firstname,
          lastname:this.para.data.lastname,
          empcategory:this.para.data.empcategory,
          rmemail:this.para.data.rmemail,
          trainingvenue:this.para.data.trainingvenue,
          attinfodata:this.para.data.attinfodata
      		}
      	}else{
      		this.title = 'Add User Master';
      		this.formdata = {
	          cohort:'',
            cohortidnumber:'',
            code:'',           
            name:'',
            institution:'',
            business:'',
            department:'',
            sdepartment:'',
            ssdepartment:'',
            orgcode:'',
            zone:'',
            state:'',
            city:'',
            inductionLocation:'',
            role:'',
            band:'',
            level:'',
            empclassification:'',
            gender:'',
            password:'',
            presentaddress:'',
            presentcity:'',
            presentstate:'',
            presentpincode:'',
            permanentadd:'',
            permanentcity:'',
            permanentstate:'',
            permanentpincode:'',
            contactDetails:'',
            email:'',
            personalmobile:'',
            personalemail:'',
            dob:'',
            doj:'',
            employeestatus:'',
            reportingmanagercode:'',
            reportingmanagername:'',
            skiplevelmcode:'',
            skiplevelmname:'',
            firstname:'',
            lastname:'',
            empcategory:'',
            rmemail:'',
            trainingvenue:'',
            attinfodata:'n',
            // strArrayAttFlag:'n'
              //verifiedName:'',
              //isActiveName :''
              // strArraySta :'',
              // strArrayOwnerShip : '',
              // strArrayVer : ''
      		}
      	}
    }

    presentToast(type, body) {
      if(type === 'success'){
        this.toastr.success(body, 'Success', {
          closeButton: false
         });
      } else if(type === 'error'){
        this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
          timeOut: 0,
          closeButton: true
        });
      } else{
        this.toastr.warning(body, 'Warning', {
          closeButton: false
          })
      }
    }

    private endDate: INgxMyDpOptions = {
      // other end date options here...
      //date:Date=new Date();
      dateFormat: 'yyyy-mm-dd',
      disableSince: {year: 0, month: 0, day:0},
  }

     // optional date changed callback
     onDateChanged(event: IMyDateModel): void {
      // date selected
      if(event.jsdate != null)
      {
      let d: Date = new Date(event.jsdate.getTime());

      // set previous of selected date
      d.setDate(d.getDate()-1);
      //this.myDate=new Date();
      //let finalDate=this.myDate;

      // Get new copy of options in order the date picker detect change
      let copy: INgxMyDpOptions = this.getCopyOfEndDateOptions();
      copy.disableSince = {year: d.getFullYear(), 
                           month: d.getMonth() + 1, 
                           day: d.getDate()};                    
      // this.startDate = copy;
      // copy.disableUntil={
      //   year:d.getFullYear()+50,
      //   month:d.getMonth()+(50*12);
      //     day:d.getDay()
      // }; 
      this.endDate=copy;
    }
      }
      private startDate: INgxMyDpOptions = {
        // start date options here...
        dateFormat: 'yyyy-mm-dd',
       //selectionTxtFontSize :"12px",
       disableSince: {year: 0, month: 0, day: 0},
      }
      getCopyOfEndDateOptions(): INgxMyDpOptions {
        return JSON.parse(JSON.stringify(this.endDate));
    }
    getCopyOfStartDateOptions(): INgxMyDpOptions {
      return JSON.parse(JSON.stringify(this.startDate));
  }


  addRoleFeat() {
    this.roleFeatures.push(this.roleFeatures.length);
  }

  submit(user) {
    console.log(user);
  }

  back(){
    this.router.navigate(['pages/plan/employees']);
  }

  onItemSelect(item:any){
    console.log(item);
    // console.log(this.selectedItems);
}
OnItemDeSelect(item:any){
    console.log(item);
    // console.log(this.selectedItems);
}
onSelectAll(items: any){
    console.log(items);
}
onDeSelectAll(items: any){
    console.log(items);
}

    uploadfinalData:any = {};
    resUpload:any;
    userLoginData:any;
    // rowData:any;
    rowDataRes:any = [];

    rowData:any = {
      age: '',
      aopc: '',
      aopn: '', 
      aops: '',
      band: this.formdata.band,
      bank: '',
      bankac: '',
      bloodgroup: '',
      business: this.formdata.business,
      ccc: '',
      ccn: '',
      city: this.formdata.city,
      cohort1: this.formdata.cohortidnumber,
      currenttenuremonth: '',
      currenttenureyear: '',
      department: this.formdata.department,
      dob: this.formdata.dob,
      doj: this.formdata.doj,
      dol: '',
      dom: '',
      dor: '',
      email: this.formdata.email,
      emergencycontactlandline: '',
      emergencycontactmobile: '',
      emergencycontactname: '',
      emergencycontactrelation: '',
      empcategory: this.formdata.empcategory,
      empclassification: this.formdata.empclassification,
      employeestatus: this.formdata.employeestatus,
      eps: '',
      esic: '',
      father: '',
      firstname: this.formdata.firstname,
      fullname: this.formdata.name,
      gender: this.formdata.gender,
      highestqualification: '',
      institution: this.formdata.institution,
      jobcode: '',
      lastname: this.formdata.lastname,
      level: this.formdata.level,
      location: this.formdata.inductionLocation,
      locationcode: '',
      martialstatus: '',
      orgcode: this.formdata.orgcode,
      pan: '',
      password: '',
      permanentadd: this.formdata.permanentadd,
      permanentcity: this.formdata.permanentcity,
      permanentpincode: this.formdata.permanentpincode,
      permanentstate: this.formdata.permanentstate,
      personalemail: this.formdata.personalemail,
      personalmobile: this.formdata.personalmobile,
      pfnumber: '',
      phone1: this.formdata.contactDetails,
      positioncode: '',
      presentaddress: this.formdata.presentaddress,
      presentcity: this.formdata.presentcity,
      presentpincode: this.formdata.presentpincode,
      presentstate: this.formdata.presentstate,
      qualificationyear: '',
      reportingmanagercode: this.formdata.reportingmanagercode,
      reportingmanagername: this.formdata.reportingmanagername,
      rmemail: this.formdata.rmemail,
      role: this.formdata.role,
      sdepartment: this.formdata.sdepartment,
      skiplevelmcode: this.formdata.skiplevelmcode,
      skiplevelmname: this.formdata.skiplevelmname,
      spousename: '',
      ssdepartment: this.formdata.ssdepartment,
      state: this.formdata.state,
      tier: '',
      trainingvenue: this.formdata.trainingvenue,
      uan: '',
      username: this.formdata.code,
      valid: '',
      zone: this.formdata.zone,
    }

    createUser(name) {
      console.log("Create user data"+name);
      let user = {
          age: '',
          aopc: '',
          aopn: '', 
          aops: '',
          attendance: name.attinfodata,
          band: name.band,
          bank: '',
          bankac: '',
          bloodgroup: '',
          business: name.business,
          ccc: '',
          ccn: '',
          city: name.city,
          cohort1: name.cohortidnumber,
          currenttenuremonth: '',
          currenttenureyear: '',
          department: name.department,
          dob: name.dob.formatted,
          doj: name.doj.formatted,
          dol: '',
          dom: '',
          dor: '',
          email: name.email,
          emergencycontactlandline: '',
          emergencycontactmobile: '',
          emergencycontactname: '',
          emergencycontactrelation: '',
          empcategory: name.empcategory,
          empclassification: name.empclassification,
          employeestatus: name.employeestatus,
          eps: '',
          esic: '',
          father: '',
          firstname: name.firstname,
          fullname: name.name,
          gender: name.gender,
          highestqualification: '',
          institution: name.institution,
          jobcode: '',
          lastname: name.lastname,
          level: name.level,
          location: name.inductionLocation,
          locationcode: '',
          martialstatus: '',
          orgcode: name.orgcode,
          pan: '',
          password: '',
          permanentadd: name.permanentadd,
          permanentcity: name.permanentcity,
          permanentpincode: name.permanentpincode,
          permanentstate: name.permanentstate,
          personalemail: name.personalemail,
          personalmobile: name.personalmobile,
          pfnumber: '',
          phone1: name.contactDetails,
          positioncode: '',
          presentaddress: name.presentaddress,
          presentcity: name.presentcity,
          presentpincode: name.presentpincode,
          presentstate: name.presentstate,
          qualificationyear: '',
          reportingmanagercode: name.reportingmanagercode,
          reportingmanagername: name.reportingmanagername,
          rmemail: name.rmemail,
          role: name.role,
          sdepartment: name.sdepartment,
          skiplevelmcode: name.skiplevelmcode,
          skiplevelmname: name.skiplevelmname,
          spousename: '',
          ssdepartment: name.ssdepartment,
          state: name.state,
          tier: '',
          trainingvenue: name.trainingvenue,
          uan: '',
          username: name.code,
          valid: '',
          zone: name.zone,  
          tenantId: this.tenantId
        };
      console.log('add user data ',user);
      if(localStorage.getItem('LoginResData')){
          this.userLoginData = JSON.parse(localStorage.getItem('LoginResData')); 
          console.log('user login data:--',this.userLoginData);
          this.tenantId = this.userLoginData.data.data.tenantId;
          console.log("tenant id ", this.tenantId)
        }

        console.log('user Login Data :-',this.userLoginData);

        this.uploadfinalData = {
          fileData : user,
          username : this.userLoginData.username,
        }  
        console.log('upload final Data:-',this.uploadfinalData);

        this.service.addUser(this.uploadfinalData)
        .then(resUserData => { 
         //  this.loader =false;
          this.rowDataRes = resUserData;
          // this.rowData = resUserData.data[0];
          // this.pageSettings = { pageSize: 6 };
          console.log('Res Upload :- ', this.rowDataRes);
          console.log('File Uploaded!');
          if(this.rowDataRes.data.flag == 1){

            // var createSuccessToast : Toast = {
            //     type: 'success',
            //     title: 'Success!',
            //     body: 'Employee added! Click to view details.',
            //     showCloseButton: true,
            //     timeout: 2000,
            //     onHideCallback: () => {
            //         this.router.navigate(['/pages/plan/employees']);
            //     }
            // };
            // this.toasterService.pop(createSuccessToast);
            this.presentToast('success', 'Employee added');
            this.router.navigate(['/pages/plan/employees']);

             // this.toastr.onClickToast()
             //       .subscribe( toast => {              
             //             this.router.navigate(['/pages/users/induction']);
             //       });
              // this.toastrService.success('User added! Click to view details.', 'Success!');
             // setTimeout(data => {
             //   this.router.navigate(['/pages/users/induction']);
             // },1000);
          }
          if(this.rowDataRes.data.flag == 0){
            // var createErrorToast : Toast = {
            //     type: 'error',
            //     title: 'Error!',
            //     body: this.rowDataRes.data.result,
            //     showCloseButton: true,
            //     timeout: 2000,
            //     // onHideCallback: () => {
            //     //     this.router.navigate(['/pages/plan/users']);
            //     // }
            // };
            // this.toasterService.pop(createErrorToast);
            this.presentToast('error', '');
            // this.toastr.onClickToast()
            //      .subscribe( toast => {              
            //            this.router.navigate(['/pages/users/induction']);
            //    });
           // this.toastrService.error(this.rowDataRes.data.result, 'Error!');
          }
           // this.id = setInterval(()=> {
           //   this.back(); 
           // },5000);

        },
        resUserError => {
          // this.loader =false;
          this.errorMsg = resUserError;

          //  this.toastr.onClickToast()
          //        .subscribe( toast => {              
          //              // this.router.navigate(['/pages/users/induction']);
          //        });
           // this.toastr.error('Error saving User! Click to view details.', 'Error!', {dismiss: 'click',toastLife: 5000});
        });
        //console.log("Res Upload User Data"+this.row)
    }
   
    updateUser(name) {
      let user = {
        age: '',
        aopc: '',
        aopn: '', 
        aops: '',
        attendance: name.attinfodata,
        band: name.band,
        bank: '',
        bankac: '',
        bloodgroup: '',
        business: name.business,
        ccc: '',
        ccn: '',
        city: name.city,
        cohort1: name.cohortidnumber,
        currenttenuremonth: '',
        currenttenureyear: '',
        department: name.department,
        // dob: name.dob.epoc,
        // doj: name.doj.epoc,
        dob: name.dob.formatted == undefined ? this.dobOldDate : name.dob.formatted, 
        doj: name.doj.formatted == undefined ? this.dojOldDate : name.doj.formatted,
        dol: '',
        dom: '',
        dor: '',
        email: name.email,
        emergencycontactlandline: '',
        emergencycontactmobile: '',
        emergencycontactname: '',
        emergencycontactrelation: '',
        empcategory: name.empcategory,
        empclassification: name.empclassification,
        employeestatus: name.employeestatus,
        eps: '',
        esic: '',
        father: '',
        firstname: name.firstname,
        fullname: name.name,
        gender: name.gender,
        highestqualification: '',
        institution: name.institution,
        jobcode: '',
        lastname: name.lastname,
        level: name.level,
        location: name.inductionLocation,
        locationcode: '',
        martialstatus: '',
        orgcode: name.orgcode,
        pan: '',
        password: '',
        permanentadd: name.permanentadd,
        permanentcity: name.permanentcity,
        permanentpincode: name.permanentpincode,
        permanentstate: name.permanentstate,
        personalemail: name.personalemail,
        personalmobile: name.personalmobile,
        pfnumber: '',
        phone1: name.contactDetails,
        positioncode: '',
        presentaddress: name.presentaddress,
        presentcity: name.presentcity,
        presentpincode: name.presentpincode,
        presentstate: name.presentstate,
        qualificationyear: '',
        reportingmanagercode: name.reportingmanagercode,
        reportingmanagername: name.reportingmanagername,
        rmemail: name.rmemail,
        role: name.role,
        sdepartment: name.sdepartment,
        skiplevelmcode: name.skiplevelmcode,
        skiplevelmname: name.skiplevelmname,
        spousename: '',
        ssdepartment: name.ssdepartment,
        state: name.state,
        tier: '',
        trainingvenue: name.trainingvenue,
        uan: '',
        username: name.code,
        valid: '',
        zone: name.zone,  
        userId: name.userId == null ? this.para.data.id :name.userId,
        tenantId: this.tenantId
      };
      console.log('edit user data ',user);
      
      if(localStorage.getItem('LoginResData')){
          this.userLoginData = JSON.parse(localStorage.getItem('LoginResData')); 
        }
        console.log('user Login Data :-',this.userLoginData);

        this.uploadfinalData = {
          fileData:user,
          username:this.userLoginData.username,
        }  
        console.log('upload final Data:-',this.uploadfinalData);
      
        // optional date changed callback
    
      this.service.updateUser(this.uploadfinalData).then(
         data => {
           this.rowDataRes = data;
           console.log('Res Upload :- ', this.rowDataRes);
           // console.log('File Uploaded!');
           if(this.rowDataRes.data.flag == 1){
                // var updateSuccessToast : Toast = {
                //     type: 'success',
                //     title: 'Success!',
                //     body: 'Employee updated! Click to view details.',
                //     showCloseButton: true,
                //     timeout: 2000,
                //     onHideCallback: () => {
                //         this.router.navigate(['/pages/plan/employees']);
                //     }
                // };
                // this.toasterService.pop(updateSuccessToast);
            this.presentToast('success', 'Employee updated');
            this.router.navigate(['/pages/plan/employees']);
                //  this.toastr.onClickToast()
                //       .subscribe( toast => {              
                //             this.router.navigate(['/pages/users/induction']);
                //       });
                //  this.toastr.success('User updated! Click to view details.', 'Success!', {dismiss: 'click',toastLife: 5000});
                // setTimeout(data => {
                //   this.router.navigate(['/pages/users/induction']);
                // },1000);
           }
           if(this.rowDataRes.data.flag == 0){
                // var updateErrorToast : Toast = {
                //     type: 'error',
                //     title: 'Error!',
                //     body: this.rowDataRes.data.result,
                //     showCloseButton: true,
                //     timeout: 2000,
                //     // onHideCallback: () => {
                //     //     this.router.navigate(['/pages/plan/users']);
                //     // }
                // };
                // this.toasterService.pop(updateErrorToast);
            this.presentToast('error', '');
             // this.toastr.onClickToast()
             //    .subscribe( toast => {              
             //          // this.router.navigate(['/pages/users/induction']);
             //    });
             // this.toastr.error(this.rowDataRes.data.result, 'Error!', {dismiss: 'click',toastLife: 5000});
           }  
           // this.id = setInterval(()=> {
           //   this.back(); 
           // },5000);

           // return true;
         },
         error => {
           console.error("Error saving user!");
           //return Observable.throw(error);
           
           // this.toastr.onClickToast()
           //      .subscribe( toast => {              
           //            // this.router.navigate(['/pages/users/induction']);
           //      });
           // this.toastr.error('Error updating User! Click to view details.', 'Error!', {dismiss: 'click',toastLife: 5000});
         }
      );
    }

    

    selRole : any = {
      id : ''
    }

    roles : any = [{
      id : 1,
      name : 'Admin'
    },{
      id : 2,
      name : 'L&D'
    },{
      id : 3,
      name : 'CTO'
    },]

    menus : any = [{
      menuid : 1,
      name : 'users',
      features : [{
        featureid : 1,
        name : 'Add user',
        menuid : 1,
        selected : false,
      },{
        featureid : 2,
        name : 'Upload user',
        menuid : 1,
        selected : false,
      },{
        featureid : 3,
        name : 'Edit user',
        menuid : 1,
        selected : false,
      }],
      selected : false,
      parent : 'plan'
    },
    {
      menuid : 2,
      name : 'course',
      features : [{
        featureid : 1,
        name : 'Add course',
        menuid : 2,
        selected : false,
      },{
        featureid : 2,
        name : 'Edit course',
        menuid : 2,
        selected : false,
      }],
      selected : false,
      parent : 'plan'
    },
    {
      menuid : 3,
      name : 'course bundle',
      features : [{
        featureid : 1,
        name : 'Add course bundle',
        menuid : 3,
        selected : false,
      },{
        featureid : 2,
        name : 'Edit course bundle',
        menuid : 3,
        selected : false,
      }],
      selected : false,
      parent : 'plan'
    }];

  

  onMenuClicked(menu:any, event: any){
    console.log('Menu data',menu);
    console.log('Menu checked',event.target.checked);
    var checkedVal = event.target.checked;
    for(let i=0; i<this.menus.length; i++){
      var menuItem = this.menus[i].features;
      for(let j=0; j<menuItem.length; j++){  
        if(menu.menuid == menuItem[j].menuid){
          if(checkedVal == true){
            menuItem[j].selected = true;
          }else{
            menuItem[j].selected = false;
          }
        }
      }  
    }
  }

  onFeatureClicked(feature:any, event: any){
    console.log('Feature data',feature);
    console.log('Feature checked',event.target.checked);
    var checkedVal = event.target.checked;
    var checkedFeat = [];

    for(let i=0; i<this.menus.length; i++){
      if(feature.menuid == this.menus[i].menuid){
        var menuItems = this.menus[i].features;
        for(let j=0; j<menuItems.length; j++){
          if(menuItems[j].selected == true){
            checkedFeat.push(menuItems[j]);
          }
        }
      }
    }

    if(checkedFeat.length == menuItems.length){
      for(let i=0; i<this.menus.length; i++){
        if(feature.menuid == this.menus[i].menuid){
          // if(checkedVal == true){
            this.menus[i].selected = true;
          // }else{
            // this.menus[i].selected = false;
          // }
        }
      }
    }else{
      for(let i=0; i<this.menus.length; i++){
        if(feature.menuid == this.menus[i].menuid){
          // if(checkedVal == true){
            // this.menus[i].selected = true;
          // }else{
            this.menus[i].selected = false;
          // }
        }
      }
    }

  }  

}

