import {Injectable, Inject} from '@angular/core';
import {Http, Response, Headers, RequestOptions, Request} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppConfig} from '../../../../app.module';
import { HttpClient } from "@angular/common/http";
@Injectable()

export class AddEditEmployeesService {

	private _urlAdd:string = "/api/usermaster/adduser"
	private _urlUpdate:string = "/api/usermaster/updateuser"
	private _urlUpload:string = "/api/usermaster/insertuser" // Dev	

	request: Request;

	constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private _http: Http, private http1 :HttpClient){
	    //this.busy = this._http.get('...').toPromise();
	}

	addUser(user) {
	   let url:any = `${this.config.FINAL_URL}`+this._urlAdd;
	   // let url:any = `${this.config.DEV_URL}`+this._urlFilter;
	//    let headers = new Headers({ 'Content-Type': 'application/json' });
	//    let options = new RequestOptions({ headers: headers });
	//    //let body = JSON.stringify(user);
	//    return this._http.post(url, user, options ).map((res: Response) => res.json());

	return new Promise(resolve => {
		this.http1.post(url,user)
		//.map(res => res.json())
	   
		.subscribe(data => {
			resolve(data);
		},
		err => {
			resolve('err');
		  });
		});
	}

	updateUser(user) {
	   let url:any = `${this.config.FINAL_URL}`+this._urlUpdate;
	   // let url:any = `${this.config.DEV_URL}`+this._urlFilter;
	//    let headers = new Headers({ 'Content-Type': 'application/json' });
	//    let options = new RequestOptions({ headers: headers });
	//    //let body = JSON.stringify(user);
	//    return this._http.post(url, user, options ).map((res: Response) => res.json());
	return new Promise(resolve => {
		this.http1.post(url,user)
		//.map(res => res.json())
	   
		.subscribe(data => {
			resolve(data);
		},
		err => {
			resolve('err');
		  });
		});

	}

	_errorHandler(error: Response){
	   console.error(error);
	   return Observable.throw(error || "Server Error")
	}

	public data: any;

}
