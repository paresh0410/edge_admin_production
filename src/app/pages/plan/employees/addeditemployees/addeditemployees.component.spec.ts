import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddeditemployeesComponent } from './addeditemployees.component';

describe('AddeditemployeesComponent', () => {
  let component: AddeditemployeesComponent;
  let fixture: ComponentFixture<AddeditemployeesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddeditemployeesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddeditemployeesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
