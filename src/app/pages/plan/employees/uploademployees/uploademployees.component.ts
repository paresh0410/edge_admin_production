import { Component, ViewContainerRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router} from '@angular/router';
import { UploadEmployeesService } from './uploademployees.service';
import { FormBuilder } from '@angular/forms';
import { PageSettingsModel } from '@syncfusion/ej2-ng-grids';
import { GridComponent } from '@syncfusion/ej2-ng-grids';
import { ColumnMenuOpenEventArgs, FilterSettingsModel, ColumnMenuItemModel } from '@syncfusion/ej2-ng-grids';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { XlsxToJsonService } from './xlsx-to-json-service';
import { JsonToXlsxService } from './json-to-xlsx-service';
import { ToastrService } from 'ngx-toastr';
import { Column, GridOption, AngularGridInstance, FieldType, Filters } from 'angular-slickgrid';
import { CommonFunctionsService } from '../../../../service/common-functions.service';
// import { ToasterService } from 'angular2-toaster';
import { SuubHeader } from '../../../components/models/subheader.model';
import { BrandDetailsService } from '../../../../service/brand-details.service';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'ngx-uploademployees',
  templateUrl: './uploademployees.component.html',
  styleUrls: ['./uploademployees.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class UploademployeesComponent {

  columnDefinitionsPreview: Column[] = [];
  gridOptionsPreview: GridOption = {};
  datasetPreview: any[] = [];
  angularGridPreview: AngularGridInstance;
  gridObjPreview: any;

  columnDefinitionsResult: Column[] = [];
  gridOptionsResult: GridOption = {};
  datasetResult: any[] = [];
  angularGridResult: AngularGridInstance;
  gridObjResult: any;

  @ViewChild('fileUpload') fileUpload: any;

  @ViewChild('tabsetUpload') tabSelect: any;

  public result: any;
  private xlsxToJsonService: XlsxToJsonService = new XlsxToJsonService();

  chosenValue: any;
  public data: Object[];

  private gridApi;
  private gridColumnApi;
  private rowData: any[];

  private columnDefs;
  private autoGroupColumnDef;
  private rowSelection;
  private rowGroupPanelShow;
  private pivotPanelShow;
  private paginationPageSize;
  private paginationNumberFormatter;
  private defaultColDef;
  private components;

  private gridApiRes;
  private gridColumnApiRes;
  private rowDataRes: any[];

  private columnDefsRes;
  private autoGroupColumnDefRes;
  private rowSelectionRes;
  private rowGroupPanelShowRes;
  private pivotPanelShowRes;
  private paginationPageSizeRes;
  private paginationNumberFormatterRes;
  private defaultColDefRes;
  private componentsRes;

  public grid: GridComponent;
  public pageSettings: PageSettingsModel;
  public filterSettings: FilterSettingsModel = { type: 'Menu' };
  currentBrandData: any;
  Upload: boolean;
  Preview: boolean;
  Results: boolean;
  public columnMenuOpen(args: ColumnMenuOpenEventArgs) {
    for (let item of args.items) {
      if (item.text === 'Filter' && args.column.field === 'username') {
        (item as ColumnMenuItemModel).hide = true;
      } else {
        (item as ColumnMenuItemModel).hide = false;
      }
    }
  }

  // const URL = '/api/';
  uploadURL = 'https://evening-anchorage-3159.herokuapp.com/api/';

  userLoginData: any;

  public uploader: FileUploader = new FileUploader({ url: this.uploadURL });
  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;


  fileUrl: any;
  fileName: any
  fileIcon: any = "assets/img/app/profile/avatar4.png";

  tab1: any = true;
  tab2: any = false;
  tab3: any = false;

  usersAddedData: any = [];
  usersSkippedData: any = [];
  usersErrorsData: any = [];
  usersAddedCount: any = 0;
  usersSkippedCount: any = 0;
  usersErrorsCount: any = 0;

  enableUpload: any = false;

  userInductedData: any = [];

  finalData: any = [];
  validData: any = [];
  InvalidData: any = [];
  validCount: any;
  invalidCount: any;
  tabs: any;

   usernamePattern:any = new RegExp("^[a-zA-Z0-9_-]{6,15}$");
  // usernamePattern: any = new RegExp("^[A-Za-z_-][A-Za-z0-9_-]*$");
  // usernamePatternAlpha:any = new RegExp("/^[a-zA-Z\s]*$/");  // with blankspace
  usernamePatternAlpha: any = new RegExp("[a-zA-Z]{3,30}");
  // usernamePatternNumbers:any = new RegExp("^[0-9]*$");
  usernamePatternNumbers: any = new RegExp("-?(0|[1-9]\d*)?");
  firstnamePattern: any = new RegExp("[a-zA-Z]{3,30}");
  // firstnamePattern:any = new RegExp("/^[a-zA-Z]*$/"); // without blankspace
  phone1Pattern: any = new RegExp("[6-9]\\d{9}");
  // phone1Pattern:any = new RegExp("[0-9]{0-10}");
  cohort1Pattern: any = new RegExp("^$|^[A-Za-z0-9]+$");
  dojPattern: any = new RegExp("");
   emailPattern: any = new RegExp("[a-zA-Z0-9.-_]{1,}@[a-zA-Z0-9.-]{2,}[.]{1}[a-zA-Z]{2,}");
  // emailPattern: any = new RegExp("[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}");
  // emailPattern1:any = new RegExp("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$");
  // dateSubmitted:any = new RegExp('^(?:(?:10|12|0?[13578])/(?:3[01]|[12][0-9]|0?[1-9])/(?:1[8-9]\\d{2}|[2-9]\\d{3})|(?:11|0?[469])/(?:30|[12][0-9]|0?[1-9])/(?:1[8-9]\\d{2}|[2-9]\\d{3})|0?2/(?:2[0-8]|1[0-9]|0?[1-9])/(?:1[8-9]\\d{2}|[2-9]\\d{3})|0?2/29/[2468][048]00|0?2/29/[3579][26]00|0?2/29/[1][89][0][48]|0?2/29/[2-9][0-9][0][48]|0?2/29/1[89][2468][048]|0?2/29/[2-9][0-9][2468][048]|0?2/29/1[89][13579][26]|0?2/29/[2-9][0-9][13579][26])$')]];
  // dateFormatNew:any = new RegExp("(((0[1-9]|[12]\d|3[01])-(0[13578]|1[02])-((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)-(0[13456789]|1[012])-((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])-02-((19|[2-9]\d)\d{2}))|(29-02-((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))");
  dateFormatNew: any = new RegExp("^[0-9]{4}[\-][0-9]{2}[\-][0-9]{2}$");
  tab2Grid: any = false;
  tab3Grid: any = false;

  eData: any;
  uploadfinalData: any = {};
  resUpload: any;
  errorMsg: any;

  userInductedDataResult: any = [];

  fileReaded: any;
  lines = [];
  uploadedData: any;
  exceltojson: any;

  resultSheets: any;

  loader: any;
  headertab1:SuubHeader
  headertab2:SuubHeader
  header: SuubHeader={
      title: '',
      btnsSearch: true,
      btnBackshow:true,
      btnName1:'Upload',
      btnName1show:true,
      showBreadcrumb:true,
      btnNameupload: true,
      uploadsheetLink: "assets/images/Template_employee_upload.xlsx",
       breadCrumbList:[]
      };

    labels: any = [
    { labelname: 'STATUS', bindingProperty: 'valid', componentType: 'text' },
    { labelname: 'REASON', bindingProperty: 'msg', componentType: 'text' },
    { labelname: 'ECN / USERNAME', bindingProperty: 'username', componentType: 'text' },
    { labelname: 'COHORT1', bindingProperty: 'cohort1', componentType: 'text' },
    { labelname: 'FIRST NAME', bindingProperty: 'firstname', componentType: 'text' },
    { labelname: 'LAST NAME', bindingProperty: 'lastname', componentType: 'text' },
    { labelname: 'EMAIL', bindingProperty: 'email', componentType: 'text' },
    { labelname: 'PHONE', bindingProperty: 'phone1', componentType: 'text' },
    { labelname: 'ATTENDANCE', bindingProperty: 'attendance', componentType: 'text' },
    { labelname: '    DOJ     ', bindingProperty: 'dojDisplay', componentType: 'date' },
    { labelname: 'CITY/TOWN', bindingProperty: 'city', componentType: 'text' },
    { labelname: 'INSTITUTION', bindingProperty: 'institution', componentType: 'text' },
    { labelname: 'AGE', bindingProperty: 'age', componentType: 'text' },
    { labelname: 'AOPC', bindingProperty: 'aopc', componentType: 'text' },
    { labelname: 'AOPN', bindingProperty: 'aopn', componentType: 'text' },
    { labelname: 'AOSP', bindingProperty: 'aops', componentType: 'text' },
    { labelname: 'BAND', bindingProperty: 'band', componentType: 'text' },
    { labelname: 'BANK', bindingProperty: 'bank', componentType: 'text' },
    { labelname: 'BANK AC', bindingProperty: 'bankac', componentType: 'text' },
    { labelname: 'BLOODGROUP', bindingProperty: 'bloodgroup', componentType: 'text' },
    { labelname: 'BUSINESS', bindingProperty: 'business', componentType: 'text' },
    { labelname: 'CCC', bindingProperty: 'ccc', componentType: 'text' },
    { labelname: 'CCN', bindingProperty: 'ccn', componentType: 'text' },
    { labelname: 'CURRENT TENURE MONTH', bindingProperty: 'currenttenuremonth', componentType: 'text' },
    { labelname: 'CURRENT TENURE YEAR', bindingProperty: 'currenttenureyear', componentType: 'text' },
    { labelname: 'DEPARTMENT', bindingProperty: 'department', componentType: 'text' },
    { labelname: 'DOB', bindingProperty: 'dob', componentType: 'text' },
    { labelname: 'DOL', bindingProperty: 'dol', componentType: 'text' },
    { labelname: 'DOM', bindingProperty: 'dom', componentType: 'text' },
    { labelname: 'DOR', bindingProperty: 'dor', componentType: 'text' },
    { labelname: 'EMG CONTACT LANDLINE', bindingProperty: 'emergencycontactlandline', componentType: 'text' },
    { labelname: 'EMG CONTACT MOBILE', bindingProperty: 'emergencycontactmobile', componentType: 'text' },
    { labelname: 'EMG CONTACT NAME', bindingProperty: 'emergencycontactname', componentType: 'text' },
    { labelname: 'EMG CONTACT RELATION', bindingProperty: 'emergencycontactrelation', componentType: 'text' },
    { labelname: 'EMP CATEGORY', bindingProperty: 'empcategory', componentType: 'text' },
    { labelname: 'EMP CLASSIFICATION', bindingProperty: 'empclassification', componentType: 'text' },
    { labelname: 'EMP STATUS', bindingProperty: 'employeestatus', componentType: 'text' },
    { labelname: 'EPS', bindingProperty: 'eps', componentType: 'text' },

    { labelname: 'ESIC', bindingProperty: 'esic', componentType: 'text' },
    { labelname: 'FATHER', bindingProperty: 'father', componentType: 'text' },
    { labelname: 'FULLNAME', bindingProperty: 'fullname', componentType: 'text' },
    { labelname: 'GENDER', bindingProperty: 'gender', componentType: 'text' },
    { labelname: 'HIGHEST QUALIFICATION', bindingProperty: 'highestqualification', componentType: 'text' },
    { labelname: 'JOB CODE', bindingProperty: 'jobcode', componentType: 'text' },
    { labelname: 'LEVEL', bindingProperty: 'level', componentType: 'text' },
    { labelname: 'LOCATION', bindingProperty: 'location', componentType: 'text' },
    { labelname: 'LOCATION CODE', bindingProperty: 'locationcode', componentType: 'text' },
    { labelname: 'MARTIAL STATUS', bindingProperty: 'martialstatus', componentType: 'text' },
      { labelname: 'ORG CODE', bindingProperty: 'orgcode', componentType: 'text' },
    { labelname: 'PAN', bindingProperty: 'pan', componentType: 'text' },
    { labelname: 'PERMANENT ADD', bindingProperty: 'permanentadd', componentType: 'text' },
    { labelname: 'PERMANENT CITY', bindingProperty: 'permanentcity', componentType: 'text' },
    { labelname: 'PERMANENT CODE', bindingProperty: 'permanentpincode', componentType: 'text' },


    { labelname: 'PERMANENT STATE', bindingProperty: 'permanentstate', componentType: 'text' },
    { labelname: 'PERSONAL EMAIL', bindingProperty: 'personalemail', componentType: 'text' },
    { labelname: 'PERSONAL MOBILE', bindingProperty: 'personalmobile', componentType: 'text' },
    { labelname: 'PF NUMBER', bindingProperty: 'pfnumber', componentType: 'text' },
    { labelname: 'POSITION CODE', bindingProperty: 'positioncode', componentType: 'text' },
    { labelname: 'PRESENT ADDRESS', bindingProperty: 'presentaddress', componentType: 'text' },
    { labelname: 'PRESENT CITY', bindingProperty: 'presentcity', componentType: 'text' },
    { labelname: 'PRESENT PINCODE', bindingProperty: 'presentpincode', componentType: 'text' },
    { labelname: 'PRESENT STATE', bindingProperty: 'presentstate', componentType: 'text' },
    { labelname: 'QUALIFICATION YEAR', bindingProperty: 'qualificationyear', componentType: 'text' },

    { labelname: 'REPORTING MANAGER CODE', bindingProperty: 'reportingmanagercode', componentType: 'text' },
    { labelname: 'REPORTING MANAGER NAME', bindingProperty: 'reportingmanagername', componentType: 'text' },
    { labelname: 'ROLE', bindingProperty: 'role', componentType: 'text' },
    { labelname: 'S DEPARTMENT', bindingProperty: 'sdepartment', componentType: 'text' },
    { labelname: 'SKIP LEVEL M CODE', bindingProperty: 'skiplevelmcode', componentType: 'text' },
    { labelname: 'SKIP LEVEL M NAME', bindingProperty: 'skiplevelmname', componentType: 'text' },
    { labelname: 'SPOUSE NAME', bindingProperty: 'spousename', componentType: 'text' },
    { labelname: 'SS DEPARMENT', bindingProperty: 'ssdepartment', componentType: 'text' },
    { labelname: 'STATE', bindingProperty: 'state', componentType: 'text' },

    { labelname: 'TIER', bindingProperty: 'tier', componentType: 'text' },
    { labelname: 'UAN', bindingProperty: 'uan', componentType: 'text' },
    { labelname: 'ZONE', bindingProperty: 'zone', componentType: 'text' },
    { labelname: 'RMEMAIL', bindingProperty: 'rmemail', componentType: 'text' },
    { labelname: 'TRAINING VENUE', bindingProperty: 'trainingvenue', componentType: 'text' },

    { labelname: 'Source', bindingProperty: 'source', componentType: 'text' },
  ];
  labelsResult: any = [
    { labelname: 'STATUS', bindingProperty: 'result', componentType: 'text' },
    { labelname: 'ECN / USERNAME', bindingProperty: 'username', componentType: 'text' },
    { labelname: 'COHORT1', bindingProperty: 'cohort1', componentType: 'text' },
    { labelname: 'FIRST NAME', bindingProperty: 'firstname', componentType: 'text' },
    { labelname: 'LAST NAME', bindingProperty: 'lastname', componentType: 'text' },
    { labelname: 'EMAIL', bindingProperty: 'email', componentType: 'text' },
    { labelname: 'PHONE', bindingProperty: 'phone1', componentType: 'text' },
    { labelname: 'ATTENDANCE', bindingProperty: 'attendance', componentType: 'text' },
    { labelname: '    DOJ     ', bindingProperty: 'dojDisplay', componentType: 'date' },
    { labelname: 'CITY/TOWN', bindingProperty: 'city', componentType: 'text' },
    { labelname: 'INSTITUTION', bindingProperty: 'institution', componentType: 'text' },
    { labelname: 'AGE', bindingProperty: 'age', componentType: 'text' },
    { labelname: 'AOPC', bindingProperty: 'aopc', componentType: 'text' },
    { labelname: 'AOPN', bindingProperty: 'aopn', componentType: 'text' },
    { labelname: 'AOSP', bindingProperty: 'aops', componentType: 'text' },
    { labelname: 'BAND', bindingProperty: 'band', componentType: 'text' },
    { labelname: 'BANK', bindingProperty: 'bank', componentType: 'text' },
    { labelname: 'BANK AC', bindingProperty: 'bankac', componentType: 'text' },
    { labelname: 'BLOODGROUP', bindingProperty: 'bloodgroup', componentType: 'text' },
    { labelname: 'BUSINESS', bindingProperty: 'business', componentType: 'text' },
    { labelname: 'CCC', bindingProperty: 'ccc', componentType: 'text' },
    { labelname: 'CCN', bindingProperty: 'ccn', componentType: 'text' },
    { labelname: 'CURRENT TENURE MONTH', bindingProperty: 'currenttenuremonth', componentType: 'text' },
    { labelname: 'CURRENT TENURE YEAR', bindingProperty: 'currenttenureyear', componentType: 'text' },
    { labelname: 'DEPARTMENT', bindingProperty: 'department', componentType: 'text' },
    { labelname: 'DOB', bindingProperty: 'dob', componentType: 'text' },
    { labelname: 'DOL', bindingProperty: 'dol', componentType: 'text' },
    { labelname: 'DOM', bindingProperty: 'dom', componentType: 'text' },
    { labelname: 'DOR', bindingProperty: 'dor', componentType: 'text' },
    { labelname: 'EMG CONTACT LANDLINE', bindingProperty: 'emergencycontactlandline', componentType: 'text' },
    { labelname: 'EMG CONTACT MOBILE', bindingProperty: 'emergencycontactmobile', componentType: 'text' },
    { labelname: 'EMG CONTACT NAME', bindingProperty: 'emergencycontactname', componentType: 'text' },
    { labelname: 'EMG CONTACT RELATION', bindingProperty: 'emergencycontactrelation', componentType: 'text' },
    { labelname: 'EMP CATEGORY', bindingProperty: 'empcategory', componentType: 'text' },
    { labelname: 'EMP CLASSIFICATION', bindingProperty: 'empclassification', componentType: 'text' },
    { labelname: "EMP STATUS", bindingProperty: 'employeestatus', componentType: 'text' },
    { labelname: 'EPS', bindingProperty: 'eps', componentType: 'text' },

    { labelname: 'ESIC', bindingProperty: 'esic', componentType: 'text' },
    { labelname: 'FATHER', bindingProperty: 'father', componentType: 'text' },
    { labelname: 'FULLNAME', bindingProperty: 'fullname', componentType: 'text' },
    { labelname: 'GENDER', bindingProperty: 'gender', componentType: 'text' },
    { labelname: 'HIGHEST QUALIFICATION', bindingProperty: 'highestqualification', componentType: 'text' },
    { labelname: 'JOB CODE', bindingProperty: 'jobcode', componentType: 'text' },
    { labelname: 'LEVEL', bindingProperty: 'level', componentType: 'text' },
    { labelname: 'LOCATION', bindingProperty: 'location', componentType: 'text' },
    { labelname: 'LOCATION CODE', bindingProperty: 'locationcode', componentType: 'text' },
    { labelname: 'MARTIAL STATUS', bindingProperty: 'martialstatus', componentType: 'text' },
      { labelname: 'ORG CODE', bindingProperty: 'orgcode', componentType: 'text' },
    { labelname: 'PAN', bindingProperty: 'pan', componentType: 'text' },
    { labelname: 'PERMANENT ADD', bindingProperty: 'permanentadd', componentType: 'text' },
    { labelname: 'PERMANENT CITY', bindingProperty: 'permanentcity', componentType: 'text' },
    { labelname: 'PERMANENT CODE', bindingProperty: 'permanentpincode', componentType: 'text' },


    { labelname: 'PERMANENT STATE', bindingProperty: 'permanentstate', componentType: 'text' },
    { labelname: 'PERSONAL EMAIL', bindingProperty: 'personalemail', componentType: 'text' },
    { labelname: 'PERSONAL MOBILE', bindingProperty: 'personalmobile', componentType: 'text' },
    { labelname: 'PF NUMBER', bindingProperty: 'pfnumber', componentType: 'text' },
    { labelname: 'POSITION CODE', bindingProperty: 'positioncode', componentType: 'text' },
    { labelname: 'PRESENT ADDRESS', bindingProperty: 'presentaddress', componentType: 'text' },
    { labelname: 'PRESENT CITY', bindingProperty: 'presentcity', componentType: 'text' },
    { labelname: 'PRESENT PINCODE', bindingProperty: 'presentpincode', componentType: 'text' },
    { labelname: 'PRESENT STATE', bindingProperty: 'presentstate', componentType: 'text' },
    { labelname: 'QUALIFICATION YEAR', bindingProperty: 'qualificationyear', componentType: 'text' },

    { labelname: 'REPORTING MANAGER CODE', bindingProperty: 'reportingmanagercode', componentType: 'text' },
    { labelname: 'REPORTING MANAGER NAME', bindingProperty: 'reportingmanagername', componentType: 'text' },
    { labelname: 'ROLE', bindingProperty: 'role', componentType: 'text' },
    { labelname: 'S DEPARTMENT', bindingProperty: 'sdepartment', componentType: 'text' },
    { labelname: 'SKIP LEVEL M CODE', bindingProperty: 'skiplevelmcode', componentType: 'text' },
    { labelname: 'SKIP LEVEL M NAME', bindingProperty: 'skiplevelmname', componentType: 'text' },
    { labelname: 'SPOUSE NAME', bindingProperty: 'spousename', componentType: 'text' },
    { labelname: 'SS DEPARMENT', bindingProperty: 'ssdepartment', componentType: 'text' },
    { labelname: 'STATE', bindingProperty: 'state', componentType: 'text' },

    { labelname: 'TIER', bindingProperty: 'tier', componentType: 'text' },
    { labelname: 'UAN', bindingProperty: 'uan', componentType: 'text' },
    { labelname: 'ZONE', bindingProperty: 'zone', componentType: 'text' },
    { labelname: 'RMEMAIL', bindingProperty: 'rmemail', componentType: 'text' },
    { labelname: 'TRAINING VENUE', bindingProperty: 'trainingvenue', componentType: 'text' },

    { labelname: 'Source', bindingProperty: 'source', componentType: 'text' },
  ];

    constructor(private _fb: FormBuilder, vcr: ViewContainerRef, protected service: UploadEmployeesService,
    public brandService: BrandDetailsService,private spinner: NgxSpinnerService,
    protected exportService: JsonToXlsxService, private router: Router,
    // private toasterService: ToasterService,
    private toastr: ToastrService, private commonservice: CommonFunctionsService) {
    if (localStorage.getItem('LoginResData')) {
      this.userLoginData = JSON.parse(localStorage.getItem('LoginResData'));
    }
    console.log('user Login Data :-', this.userLoginData);
    this.getUploadData( () =>{

    });
  }

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }

  exportToExcel() {
    this.exportService.exportAsExcelFile(this.InvalidData, 'uploadUsersErrorData');
  }


  prepareGridResult() {
    this.columnDefinitionsResult = [
      {
        id: 'result',
        name: 'STATUS',
        field: 'result',
        sortable: true,
        minWidth: 250,
        // maxWidth: 700,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        id: 'username',
        name: "ECN / USERNAME",
        field: "username",
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      // {
      //   id: 'password',
      //   name: "PASSWORD",
      //   field: "password",
      //   sortable: true,
      //   minWidth: 85,
      //   // maxWidth: 170,
      //   type: FieldType.string,
      //   filterable: true,
      //   filter: { model: Filters.compoundInput }
      // },
      // {
      //   name: "sysrole1",
      //   field: "sysrole1",
      //   width: 100,
      //   //hide: true,
      //   filterParams: { newRowsAction: "keep" }
      // },
      {
        id: 'cohort1',
        name: "COHORT1",
        field: "cohort1",
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "FIRST NAME",
        field: "firstname",
        id: 'firstname',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }

      },
      {
        name: "LAST NAME",
        field: "lastname",
        id: 'lastname',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "EMAIL",
        field: "email",
        id: 'email',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "PHONE",
        field: "phone1",
        id: 'phone1',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "ATTENDANCE",
        field: "attendance",
        id: 'attendance',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "DOJ",
        field: "doj",
        id: 'doj',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "CITY/TOWN",
        field: "city",
        id: 'city',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "INSTITUTION",
        field: "institution",
        id: 'institution',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },

      {
        name: "AGE",
        field: "age",
        id: 'age',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "AOPC",
        field: "aopc",
        id: 'aopc',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "AOPN",
        field: "aopn",
        id: 'aopn',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "AOSP",
        field: "aops",
        id: 'aops',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "BAND",
        field: "band",
        id: 'band',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "BANK",
        field: "bank",
        id: 'bank',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "BANK AC",
        field: "bankac",
        id: 'bankac',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "BLOODGROUP",
        field: "bloodgroup",
        id: 'bloodgroup',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "BUSINESS",
        field: "business",
        id: 'business',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "CCC",
        field: "ccc",
        id: 'ccc',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "CCN",
        field: "ccn",
        id: 'ccn',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "CURRENT TENURE MONTH",
        field: "currenttenuremonth",
        id: 'currenttenuremonth',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "CURRENT TENURE YEAR ",
        field: "currenttenureyear",
        id: 'currenttenureyear',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "DEPARTMENT",
        field: "department",
        id: 'department',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "DOB",
        field: "dob",
        id: 'dob',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "DOL",
        field: "dol",
        id: 'dol',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "DOM",
        field: "dom",
        id: 'dom',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "DOR",
        field: "dor",
        id: 'dor',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "EMG CONTACT LANDLINE",
        field: "emergencycontactlandline",
        id: 'emergencycontactlandline',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "EMG CONTACT MOBILE",
        field: "emergencycontactmobile",
        id: 'emergencycontactmobile',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "EMG CONTACT NAME",
        field: "emergencycontactname",
        id: 'emergencycontactname',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "EMG CONTACT RELATION",
        field: "emergencycontactrelation",
        id: 'emergencycontactrelation',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "EMP CATEGORY",
        field: "empcategory",
        id: 'empcategory',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "EMP CLASSIFICATION",
        field: "empclassification",
        id: 'empclassification',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: this.currentBrandData.employee +" STATUS",
        field: "employeestatus",
        id: 'employeestatus',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "EPS",
        field: "eps",
        id: 'eps',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "ESIC",
        field: "esic",
        id: 'esic',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "FATHER",
        field: "father",
        id: 'father',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "FULLNAME",
        field: "fullname",
        id: 'fullname',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "GENDER",
        field: "gender",
        id: 'gender',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "HIGHEST QUALIFICATION",
        field: "highestqualification",
        id: 'highestqualification',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "JOB CODE",
        field: "jobcode",
        id: 'jobcode',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "LEVEL",
        field: "level",
        id: 'level',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "LOCATION",
        field: "location",
        id: 'location',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "LOCATION CODE",
        field: "locationcode",
        id: 'locationcode',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "MARTIAL STATUS",
        field: "martialstatus",
        id: 'martialstatus',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "ORG CODE",
        field: "orgcode",
        id: 'orgcode',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "PAN",
        field: "pan",
        id: 'pan',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "PERMANENT ADD",
        field: "permanentadd",
        id: 'permanentadd',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "PERMANENT CITY",
        field: "permanentcity",
        id: 'permanentcity',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "PERMANENT CODE",
        field: "permanentpincode",
        id: 'permanentpincode',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "PERMANENT STATE",
        field: "permanentstate",
        id: 'permanentstate',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "PERSONAL EMAIL",
        field: "personalemail",
        id: 'personalemail',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "PERSONAL MOBILE",
        field: "personalmobile",
        id: 'personalmobile',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "PF NUMBER",
        field: "pfnumber",
        id: 'pfnumber',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "POSITION CODE",
        field: "positioncode",
        id: 'positioncode',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "PRESENT ADDRESS",
        field: "presentaddress",
        id: 'presentaddress',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "PRESENT CITY",
        field: "presentcity",
        id: 'presentcity',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "PRESENT PINCODE",
        field: "presentpincode",
        id: 'presentpincode',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "PRESENT STATE",
        field: "presentstate",
        id: 'presentstate',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "QUALIFICATION YEAR",
        field: "qualificationyear",
        id: 'qualificationyear',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "REPORTING MANAGER CODE",
        field: "reportingmanagercode",
        id: 'reportingmanagercode',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "REPORTING MANAGER NAME",
        field: "reportingmanagername",
        id: 'reportingmanagername',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "ROLE",
        field: "role",
        id: 'role',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "S DEPARTMENT",
        field: "sdepartment",
        id: 'sdepartment',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "SKIP LEVEL M CODE",
        field: "skiplevelmcode",
        id: 'skiplevelmcode',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "SKIP LEVEL M NAME",
        field: "skiplevelmname",
        id: 'skiplevelmname',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "SPOUSE NAME",
        field: "spousename",
        id: 'spousename',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "SS DEPARMENT",
        field: "ssdepartment",
        id: 'ssdepartment',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "STATE",
        field: "state",
        id: 'state',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "TIER",
        field: "tier",
        id: 'tier',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "UAN",
        field: "uan",
        id: 'uan',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "ZONE",
        field: "zone",
        id: 'zone',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "RMEMAIL",
        field: "rmemail",
        id: 'rmemail',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "TRAINING VENUE",
        field: "trainingvenue",
        id: 'trainingvenue',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      }
    ];

    this.gridOptionsResult = {
      // autoResize: {
      //   containerId: 'container'
      // },
      enableAutoResize: true,
      // enableAutoResize: true,       // true by default
      // enableCellNavigation: true,
      // enableExcelCopyBuffer: true,
      // enableFiltering: true,
      rowSelectionOptions: {
        // True (Single Selection), False (Multiple Selections)
        selectActiveRow: false
      },
      // preselectedRows: [0, 2],
      // enableCheckboxSelector: true,
      enableRowSelection: true,
      forceFitColumns:true,
      enableCellNavigation: true,
      enableColumnReorder: false,
      // gridMenu: {
      //   // all titles optionally support translation keys, if you wish to use that feature then use the title properties finishing by 'Key'
      //   // example "customTitle" for a plain string OR "customTitleKey" to use a translation key
      //   customTitle: 'Menu',
      //   columnTitle: 'Columns',
      //   //iconCssClass: 'fa fa-ellipsis-v',
      //   hideForceFitButton: true,
      //   hideSyncResizeButton: true,
      //   hideToggleFilterCommand: false, // show/hide internal custom commands
      //   menuWidth:17,
      //   resizeOnShowHeaderRow: true,
      // },
    };
    // this.makeDataReadyPreview();

    // if(this.tab3Grid = true){
    //   this.loader = false;
    // }
    // this.loader = true;

    setTimeout(() => {
      // this.loader = false;
      this.spinner.hide()
      this.tab3Grid = true;
    }, 1000);
  }

  ngOnInit(): void {

    this.currentBrandData = this.brandService.getCurrentBrandData();
    this.header.breadCrumbList=[
      {
        'name': 'Settings',
        'navigationPath': '/pages/plan',
        },
        {
          'name':this.currentBrandData.employee,
          'navigationPath': '/pages/plan/employees',
          }
    ]
    // console.log("this.currentBrandData",this.currentBrandData)
    this.header.title='Upload '+this.currentBrandData.employee;
    this.fileName="Click here to upload an excel file to add/upload "+this.currentBrandData.employee +'. Click on the Template button to download the excel template';
  }

  makeDataReadyResult() {


    this.uploadfinalData = {
      fileData: this.rowData,
      username: this.userLoginData.username,
    }
    console.log('upload final Data:-', this.uploadfinalData);
    //console.log('Res Upload :- ', this.resUpload);

    this.loader = true;
    this.spinner.show();
    this.service.uploadUsers(this.uploadfinalData)
      .then(resUserData => {

        // this.loader =false;
        this.resUpload = resUserData;
        // this.datasetResult = resUserData;
        // this.tab3Grid = true;
        // this.source.load(this.userInductedData);
        // this.pageSettings = { pageSize: 6 };
        console.log('Res Upload :- ', this.resUpload);
        console.log('File Uploaded!');

        this.usersErrorsData = [];
        this.usersAddedData = [];

        for (let i = 0; i < this.resUpload.length; i++) {
          if (this.resUpload[i].flag == 0) {
            this.usersErrorsData.push(this.resUpload[i]);
          }
          if (this.resUpload[i].flag == 1) {
            this.usersAddedData.push(this.resUpload[i]);
          }
        }

        this.usersErrorsCount = this.usersErrorsData.length;
        this.usersAddedCount = this.usersAddedData.length;

        for (let i = 0; i < this.usersErrorsData.length; i++) {
          this.InvalidData.push(this.usersErrorsData[i]);
        }

        console.log('Final invalid data ', this.InvalidData);

        this.usersErrorsCount = this.InvalidData.length;

        for (let i = 0; i < this.resUpload.length; i++) {
          this.resUpload[i].id = i;
        }
        // this.datasetResult=[];

        this.datasetResult = this.resUpload;
        console.log('datasetResult', this.datasetResult);
        // this.tab3Grid = true;

        this.prepareGridResult();

      },
        resUserError => {
          // this.loader = false;
          this.spinner.hide();
          this.errorMsg = resUserError;

        });
  }

  angularGridReadyResult(angularGrid: any) {
    this.angularGridResult = angularGrid;
    this.gridObjResult = angularGrid && angularGrid.slickGrid || {};
  }




  readUrl(event: any) {

    var validExts = new Array(".xlsx", ".xls");
    var fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
      // alert("Invalid file selected, valid files are of " +
      //          validExts.toString() + " types.");
      // var toast : Toast = {
      //     type: 'error',
      //     title: "Invalid file selected!",
      //     body: "Valid files are of " + validExts.toString() + " types.",
      //     showCloseButton: true,
      //     // tapToDismiss: false,
      //     timeout: 2000
      //     // onHideCallback: () => {
      //     //     this.router.navigate(['/pages/plan/users']);
      //     // }
      // };
      // this.toasterService.pop(toast);
      this.presentToast('warning', 'Valid file types are ' + validExts.toString());
      // setTimeout(data => {
      //   this.router.navigate(['/pages/users']);
      // },1000);
      // return false;
      this.cancel();
    }
    else {
      // return true;
      if (event.target.files && event.target.files[0]) {
        this.fileName = event.target.files[0].name;
        //read file from input
        this.fileReaded = event.target.files[0];

        if (this.fileReaded != '' || this.fileReaded != null || this.fileReaded != undefined) {
          this.enableUpload = true;
        }

        let file = event.target.files[0];
        this.xlsxToJsonService.processFileToJson({}, file).subscribe(data => {
        // this.xlsxToJsonService.convertXlsxToJson(file).subscribe(data => {
          this.resultSheets = Object.getOwnPropertyNames(data['sheets']);
          console.log('File Property Names ', this.resultSheets);
          let sheetName = this.resultSheets[0];
          this.result = data['sheets'][sheetName];

          if (this.result.length > 0) {
            this.uploadedData = this.result;
          }
          // this.result = JSON.stringify(data['sheets'].Sheet1);
        })
        var reader = new FileReader();
        reader.onload = (event: ProgressEvent) => {
          this.fileUrl = (<FileReader>event.target).result;
          // event.setAttribute("data-title", this.fileName);
          // console.log(this.fileUrl);
        }
        reader.readAsDataURL(event.target.files[0]);
      }
    }

    // if(event.target.files[0].name.split('.')[event.target.files[0].name.split('.').length-1] === 'xlsx'){
    //     let file = event.target.files[0];
    // } else if(event.target.files[0].name.split('.')[event.target.files[0].name.split('.').length-1] === 'xls'){
    //     let file = event.target.files[0];
    // }else{
    //   alert("Invalid file selected, valid files are of " +
    //          validExts.toString() + " types.");
    // }
  }

  cancel() {
    // this.fileUpload.nativeElement.files = [];
    console.log(this.fileUpload.nativeElement.files);
    this.fileUpload.nativeElement.value = "";
    console.log(this.fileUpload.nativeElement.files);
    this.fileName = 'Click here to upload an excel file to add/upload '+this.currentBrandData.employee +'s . Click on the Template button to download the excel template';
    this.enableUpload = false;
  }

  clearPreviewData() {
    this.datasetPreview = [];
  }

  clearResultData() {
    this.datasetResult = [];
  }

  prepareGridPreview() {
    this.columnDefinitionsPreview = [
      {
        id: 'valid',
        name: 'STATUS',
        field: 'valid',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        id: 'msg',
        name: 'REASON',
        field: 'msg',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        id: 'username',
        name: "ECN / USERNAME",
        field: "username",
        sortable: true,
        minWidth: 85,
        // // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      // {
      //   id: 'password',
      //   name: "PASSWORD",
      //   field: "password",
      //   sortable: true,
      //   minWidth: 85,
      //   // maxWidth: 170,
      //   type: FieldType.string,
      //   filterable: true,
      //   filter: { model: Filters.compoundInput }
      // },
      // {
      //   name: "sysrole1",
      //   field: "sysrole1",
      //   width: 100,
      //   //hide: true,
      //   filterParams: { newRowsAction: "keep" }
      // },
      {
        id: 'cohort1',
        name: "COHORT1",
        field: "cohort1",
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "FIRST NAME",
        field: "firstname",
        id: 'firstname',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }

      },
      {
        name: "LAST NAME",
        field: "lastname",
        id: 'lastname',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "EMAIL",
        field: "email",
        id: 'email',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "PHONE",
        field: "phone1",
        id: 'phone1',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "ATTENDANCE",
        field: "attendance",
        id: 'attendance',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "DOJ",
        field: "doj",
        id: 'doj',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "CITY/TOWN",
        field: "city",
        id: 'city',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "INSTITUTION",
        field: "institution",
        id: 'institution',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },

      {
        name: "AGE",
        field: "age",
        id: 'age',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "AOPC",
        field: "aopc",
        id: 'aopc',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "AOPN",
        field: "aopn",
        id: 'aopn',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "AOSP",
        field: "aops",
        id: 'aops',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "BAND",
        field: "band",
        id: 'band',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "BANK",
        field: "bank",
        id: 'bank',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "BANK AC",
        field: "bankac",
        id: 'bankac',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "BLOODGROUP",
        field: "bloodgroup",
        id: 'bloodgroup',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "BUSINESS",
        field: "business",
        id: 'business',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "CCC",
        field: "ccc",
        id: 'ccc',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "CCN",
        field: "ccn",
        id: 'ccn',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "CURRENT TENURE MONTH",
        field: "currenttenuremonth",
        id: 'currenttenuremonth',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "CURRENT TENURE YEAR ",
        field: "currenttenureyear",
        id: 'currenttenureyear',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "DEPARTMENT",
        field: "department",
        id: 'department',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "DOB",
        field: "dob",
        id: 'dob',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "DOL",
        field: "dol",
        id: 'dol',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "DOM",
        field: "dom",
        id: 'dom',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "DOR",
        field: "dor",
        id: 'dor',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "EMG CONTACT LANDLINE",
        field: "emergencycontactlandline",
        id: 'emergencycontactlandline',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "EMG CONTACT MOBILE",
        field: "emergencycontactmobile",
        id: 'emergencycontactmobile',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "EMG CONTACT NAME",
        field: "emergencycontactname",
        id: 'emergencycontactname',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "EMG CONTACT RELATION",
        field: "emergencycontactrelation",
        id: 'emergencycontactrelation',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "EMP CATEGORY",
        field: "empcategory",
        id: 'empcategory',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "EMP CLASSIFICATION",
        field: "empclassification",
        id: 'empclassification',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: this.currentBrandData.employee +" STATUS",
        field: "employeestatus",
        id: 'employeestatus',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "EPS",
        field: "eps",
        id: 'eps',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "ESIC",
        field: "esic",
        id: 'esic',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "FATHER",
        field: "father",
        id: 'father',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "FULLNAME",
        field: "fullname",
        id: 'fullname',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "GENDER",
        field: "gender",
        id: 'gender',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "HIGHEST QUALIFICATION",
        field: "highestqualification",
        id: 'highestqualification',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "JOB CODE",
        field: "jobcode",
        id: 'jobcode',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "LEVEL",
        field: "level",
        id: 'level',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "LOCATION",
        field: "location",
        id: 'location',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "LOCATION CODE",
        field: "locationcode",
        id: 'locationcode',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "MARTIAL STATUS",
        field: "martialstatus",
        id: 'martialstatus',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "ORG CODE",
        field: "orgcode",
        id: 'orgcode',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "PAN",
        field: "pan",
        id: 'pan',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "PERMANENT ADD",
        field: "permanentadd",
        id: 'permanentadd',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "PERMANENT CITY",
        field: "permanentcity",
        id: 'permanentcity',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "PERMANENT CODE",
        field: "permanentpincode",
        id: 'permanentpincode',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "PERMANENT STATE",
        field: "permanentstate",
        id: 'permanentstate',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "PERSONAL EMAIL",
        field: "personalemail",
        id: 'personalemail',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "PERSONAL MOBILE",
        field: "personalmobile",
        id: 'personalmobile',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "PF NUMBER",
        field: "pfnumber",
        id: 'pfnumber',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "POSITION CODE",
        field: "positioncode",
        id: 'positioncode',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "PRESENT ADDRESS",
        field: "presentaddress",
        id: 'presentaddress',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "PRESENT CITY",
        field: "presentcity",
        id: 'presentcity',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "PRESENT PINCODE",
        field: "presentpincode",
        id: 'presentpincode',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "PRESENT STATE",
        field: "presentstate",
        id: 'presentstate',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "QUALIFICATION YEAR",
        field: "qualificationyear",
        id: 'qualificationyear',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "REPORTING MANAGER CODE",
        field: "reportingmanagercode",
        id: 'reportingmanagercode',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "REPORTING MANAGER NAME",
        field: "reportingmanagername",
        id: 'reportingmanagername',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "ROLE",
        field: "role",
        id: 'role',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "S DEPARTMENT",
        field: "sdepartment",
        id: 'sdepartment',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "SKIP LEVEL M CODE",
        field: "skiplevelmcode",
        id: 'skiplevelmcode',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "SKIP LEVEL M NAME",
        field: "skiplevelmname",
        id: 'skiplevelmname',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "SPOUSE NAME",
        field: "spousename",
        id: 'spousename',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "SS DEPARMENT",
        field: "ssdepartment",
        id: 'ssdepartment',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "STATE",
        field: "state",
        id: 'state',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "TIER",
        field: "tier",
        id: 'tier',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "UAN",
        field: "uan",
        id: 'uan',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "ZONE",
        field: "zone",
        id: 'zone',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "RMEMAIL",
        field: "rmemail",
        id: 'rmemail',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      },
      {
        name: "TRAINING VENUE",
        field: "trainingvenue",
        id: 'trainingvenue',
        sortable: true,
        minWidth: 85,
        // maxWidth: 170,
        type: FieldType.string,
        filterable: true,
        filter: { model: Filters.compoundInput }
      }
    ];
    this.gridOptionsPreview = {
      enableAutoResize: true,       // true by default
      // enableCellNavigation: true,
      // enableExcelCopyBuffer: true,
      // enableFiltering: true,
      rowSelectionOptions: {
        // True (Single Selection), False (Multiple Selections)
        selectActiveRow: false
      },
      // preselectedRows: [0, 2],
      // enableCheckboxSelector: true,
      enableRowSelection: true,
      enableColumnReorder: false,
      // gridMenu: {
      //   // all titles optionally support translation keys, if you wish to use that feature then use the title properties finishing by 'Key'
      //   // example "customTitle" for a plain string OR "customTitleKey" to use a translation key
      //   customTitle: 'Menu',
      //   columnTitle: 'Columns',
      //   //iconCssClass: 'fa fa-ellipsis-v',
      //   hideForceFitButton: true,
      //   hideSyncResizeButton: true,
      //   hideToggleFilterCommand: false, // show/hide internal custom commands
      //   menuWidth:17,
      //   resizeOnShowHeaderRow: true,
      // },
    };
    // this.makeDataReadyPreview();
    // this.loader = true;
    this.spinner.show()
    setTimeout(() => {
      // this.loader = false;
      this.spinner.hide()
      this.tab2Grid = true;
    }, 1000);
  }

  makeDataReadyPreview() {
    for (let i = 0; i < this.uploadedData.length; i++) {
      this.uploadedData[i].msg ='';
      this.uploadedData[i].source = this.uploadedData[i].source ? this.uploadedData[i].source.toUpperCase() : this.uploadedData[i].source;

      this.uploadedData[i].valid = 'Valid';

      if (this.uploadedData[i].username == '' || this.uploadedData[i].username == null || this.uploadedData[i].username == undefined) {
        this.uploadedData[i].valid = 'Invalid';
        this.uploadedData[i].msg ='Username is required'
        continue;
        // this.uploadedData[i].username = 'Required!';
      } else if (this.usernamePattern.test(this.uploadedData[i].username) && this.uploadedData[i].valid == 'Valid') {
        this.uploadedData[i].valid = 'Valid';
      } else {
        this.uploadedData[i].valid = 'Invalid';
        this.uploadedData[i].msg ='Username is invalid,it should contain minimun 6 and maximum 15 characters';
        continue;
      }
      if (this.uploadedData[i].firstname == '' || this.uploadedData[i].firstname == null || this.uploadedData[i].firstname == undefined) {
        this.uploadedData[i].valid = 'Invalid';
        // this.uploadedData[i].firstname = 'Required!';
        this.uploadedData[i].msg ='Firstname is required';
        continue;
      } else if (this.firstnamePattern.test(this.uploadedData[i].firstname) && this.uploadedData[i].valid == 'Valid') {
        this.uploadedData[i].valid = 'Valid';
      } else {
        this.uploadedData[i].valid = 'Invalid';
        // this.uploadedData[i].msg ='Firstname is invalid';
        this.uploadedData[i].msg ='Firstname should have more than 3 characters';

        continue;
      }
      if (this.uploadedData[i].doj == '' || this.uploadedData[i].doj == null || this.uploadedData[i].doj == undefined) {
        this.uploadedData[i].valid = 'Invalid';
        this.uploadedData[i].msg ='Date of joining is required';
        continue;
        // this.uploadedData[i].lastname = 'Required!';
      } else {
        // this.uploadedData[i].dojDisplay = this.uploadedData[i].doj;
        let doj = String(this.uploadedData[i].doj);
        if(doj.indexOf('/') != -1){
          // doj.replace(/gi, "red");
          doj = doj.split("/").join("-");
        }
        this.uploadedData[i].dojDisplay = doj;
        // this.uploadedData[i].dojDisplay =
          const dojDate = this.convertDate(doj);
          this.uploadedData[i].doj = dojDate; // this.convertDate(this.uploadedData[i].doj);
          if (this.dateFormatNew.test(dojDate) && this.uploadedData[i].valid == 'Valid') {
            this.uploadedData[i].valid = 'Valid';
          } else {
            this.uploadedData[i].valid = 'Invalid';
            // this.uploadedData[i].msg ='Date of joining is invalid';
            this.uploadedData[i].msg = 'DOJ - Invalid date format';

            continue;
          }
      }
      if (this.uploadedData[i].dor) {
        let dor = String(this.uploadedData[i].doj);
        if(dor.indexOf('/') != -1){
          // doj.replace(/gi, "red");
          dor = dor.split("/").join("-")
        }
        this.uploadedData[i].dor = this.convertDate(dor);
      }
      if (this.uploadedData[i].dol) {
        let dol = String(this.uploadedData[i].doj);
        if(dol.indexOf('/') != -1){
          // doj.replace(/gi, "red");
          dol = dol.split("/").join("-")
        }
        this.uploadedData[i].dol = this.convertDate(dol);
      }
      if (this.uploadedData[i].dob) {
        let dob = String(this.uploadedData[i].dob);
        if(dob.indexOf('/') != -1){
          // doj.replace(/gi, "red");
          dob = dob.split("/").join("-")
        }
        this.uploadedData[i].dob = this.convertDate(dob);
      }
      if (this.uploadedData[i].dom) {
        let dom = String(this.uploadedData[i].dom);
        if(dom.indexOf('/') != -1){
          // doj.replace(/gi, "red");
          dom = dom.split("/").join("-")
        }
        this.uploadedData[i].dom = this.convertDate(dom);
      }
      if (this.uploadedData[i].email == '' || this.uploadedData[i].email == null || this.uploadedData[i].email == undefined) {
        this.uploadedData[i].valid = 'Invalid';
        this.uploadedData[i].msg ='Email is required';
        continue;
        // this.uploadedData[i].email = 'Required!';
      } else if (this.emailPattern.test(this.uploadedData[i].email) && this.uploadedData[i].valid == 'Valid') {
        this.uploadedData[i].valid = 'Valid';
      } else {
        this.uploadedData[i].valid = 'Invalid';
        this.uploadedData[i].msg ='Email Invalid';
        continue;
      }
      if (this.uploadedData[i].employeestatus == '' || this.uploadedData[i].employeestatus == null || this.uploadedData[i].employeestatus == undefined) {
        this.uploadedData[i].valid = 'Invalid';
        this.uploadedData[i].msg ='User_status is required';
        continue;
      }
      else if(this.uploadedData[i].employeestatus && (this.uploadedData[i].employeestatus.toUpperCase() == 'ACTIVE' || this.uploadedData[i].employeestatus.toUpperCase() == 'INACTIVE')){
        this.uploadedData[i].valid = 'Valid';
      }else{
        this.uploadedData[i].valid = 'Invalid';
        this.uploadedData[i].msg ='User_status is invalid';
        continue;
      }
      if (this.uploadedData[i].isUser == '' || this.uploadedData[i].isUser == null || this.uploadedData[i].isUser == undefined) {
        this.uploadedData[i].isUser = null;
      }
      if (this.uploadedData[i].source == '' || this.uploadedData[i].source == null || this.uploadedData[i].source == undefined) {
        this.uploadedData[i].valid = 'Invalid';
        this.uploadedData[i].msg ='Source is required';
        continue;
        // this.uploadedData[i].phone1 = 'Required!';
      } else if (this.tempObjSrc[String(this.uploadedData[i].source).toUpperCase()] && this.uploadedData[i].valid == 'Valid') {
        this.uploadedData[i].valid = 'Valid';
      } else {
        this.uploadedData[i].valid = 'Invalid';
        this.uploadedData[i].msg ='Source is invalid';
        continue;
      }
      if (this.uploadedData[i].phone1 == '' || this.uploadedData[i].phone1 == null || this.uploadedData[i].phone1 == undefined) {
        this.uploadedData[i].valid = 'Invalid';
        this.uploadedData[i].msg ='Phone No is required';
        continue;
        // this.uploadedData[i].email = 'Required!';
      }else if(String(this.uploadedData[i].phone1).trim().length != 10){
        this.uploadedData[i].valid = 'Invalid';
        this.uploadedData[i].msg ='Phone No length should be 10 digits';
        continue;
      } else if (this.phone1Pattern.test(this.uploadedData[i].phone1) && this.uploadedData[i].valid == 'Valid') {
        this.uploadedData[i].valid = 'Valid';
      } else {
        this.uploadedData[i].valid = 'Invalid';
        this.uploadedData[i].msg ='Invalid Phone no.';
        continue;
      }
      if (this.uploadedData[i].fullname == '' || this.uploadedData[i].fullname == null || this.uploadedData[i].fullname == undefined) {
        this.uploadedData[i].valid = 'Invalid';
        // this.uploadedData[i].firstname = 'Required!';
        this.uploadedData[i].msg ='Full name is required';
        continue;
      } else if (this.firstnamePattern.test(this.uploadedData[i].fullname) && this.uploadedData[i].valid == 'Valid') {
        this.uploadedData[i].valid = 'Valid';
      } else {
        this.uploadedData[i].valid = 'Invalid';
        // this.uploadedData[i].msg ='Firstname is invalid';
        this.uploadedData[i].msg ='Full name should have more than 3 characters';

        continue;
      }
      if (this.uploadedData[i].lastname == '' || this.uploadedData[i].lastname == null || this.uploadedData[i].lastname == undefined) {
        this.uploadedData[i].valid = 'Invalid';
        // this.uploadedData[i].firstname = 'Required!';
        this.uploadedData[i].msg ='last name is required';
        continue;
      } else if (this.firstnamePattern.test(this.uploadedData[i].lastname) && this.uploadedData[i].valid == 'Valid') {
        this.uploadedData[i].valid = 'Valid';
      } else {
        this.uploadedData[i].valid = 'Invalid';
        // this.uploadedData[i].msg ='Firstname is invalid';
        this.uploadedData[i].msg ='last name should have more than 3 characters';

        continue;
      }
    }
    this.validData = [];
    this.InvalidData = [];
    for (let i = 0; i < this.uploadedData.length; i++) {
      if (this.uploadedData[i].valid == 'Valid') {
        this.validData.push(this.uploadedData[i]);
      } else {
        this.InvalidData.push(this.uploadedData[i]);
      }
    }

    for (let i = 0; i < this.uploadedData.length; i++) {
      this.uploadedData[i].id = i;
    }

    console.log('Valid row Data', this.validData);
    this.validCount = this.validData.length;
    console.log('Invalid row Data', this.InvalidData);
    this.invalidCount = this.InvalidData.length;

    this.datasetPreview = this.uploadedData;

    this.rowData = this.validData;
    // this.rowData = this.result;
    console.log('Uploaded Data', this.datasetPreview);
    this.prepareGridPreview();
    // this.angularGridReadyPreview();
  }

  angularGridReadyPreview(angularGrid: any) {
    this.angularGridPreview = angularGrid;
    this.gridObjPreview = angularGrid && angularGrid.slickGrid || {};
  }

  excelToJson(data) {
    let user1 = data;

    // if(user1.name.split('.')[user1.name.split('.').length-1] === 'xlsx'){
    //     // this.exceltojson = xls_json;
    //     this.exceltojson = xlsxtojson;
    // } else {
    //     this.exceltojson = xlstojson;
    //     // this.exceltojson = xlsx_json;
    // }
    this.exceltojson({
      input: user1,
      output: null, //since we don't need output.json
      lowerCaseHeaders: true
    }, function (err, result) {
      if (err) {
        console.error(err);
      } else {
        console.log(result);
      }
    });
  }

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }



  postfile() {

    // this.tabs = this.tabSelect.tabs._results;
    if(this.tempObjSrc != {} && this.tempObjSrc){
      this.doNextProcessPostFile();
    }else {
      this.getUploadData((data) =>{
        if(data){
          this.doNextProcessPostFile();
        }else {
          this.toastr.warning('Unable to get sources list', 'Warning');
        }

      });
    }


  }

  doNextProcessPostFile(){
    if (this.tabs='Upload') {
      this.header={
      title: 'Upload '+this.currentBrandData.employee,
      btnsSearch: true,
      btnBackshow:true,
      btnName1:'Save',
      btnName1show:true,

      showBreadcrumb:true,
       breadCrumbList:[
        {
          'name': 'Settings',
          'navigationPath': '/pages/plan',
          },
          {
            'name':this.currentBrandData.employee,
            'navigationPath': '/pages/plan/employees',
            }
       ]
    }
      this.Upload = false;
      this.Preview = true;
      this.Results=false
      // this.tabs[0].active = false;
      // this.tabs[1].active = true;
      // this.tabs[1].disabled = false;
      this.tab1 = false;
      this.tab2 = true;
      this.tab3 = false;

      this.makeDataReadyPreview();
      // this.prepareGridPreview();
      // this.makeDataReadyResult();
    }

  }
  clearData() {
    this.rowData = [];
  }

  postfileTab(tabEvent) {

    this.tabs = tabEvent.tabTitle;

    if (tabEvent.tabTitle == 'Upload') {
      // this.tabs[0].active = true;
      // this.tabs[1].active = false;
      // this.tabs[1].disabled = true;
      // this.tabs[2].active = false;
      // this.tabs[2].disabled = true;
      this.tab1 = true;
      this.tab2 = false;
      this.tab3 = false;
      this.header={
        title: 'Upload '+this.currentBrandData.employee,
        btnsSearch: true,
        btnBackshow:true,
        btnName1:'Upload',
        btnName1show:true,
        btnNameupload: true,
        uploadsheetLink: "assets/images/Template_employee_upload.xlsx",
        showBreadcrumb:true,
         breadCrumbList:[
          {
            'name': 'Settings',
            'navigationPath': '/pages/plan',
            },
            {
              'name':this.currentBrandData.employee,
              'navigationPath': '/pages/plan/employees',
              }
         ]
      }
      this.clearData();
    }

    else if (tabEvent.tabTitle == 'Preview') {
this.header={
  title: 'Upload '+this.currentBrandData.employee,
  btnsSearch: true,
  btnBackshow:true,
  btnName1:'Save',
  btnName1show:true,
  showBreadcrumb:true,
   breadCrumbList:[
    {
      'name': 'Settings',
      'navigationPath': '/pages/plan',
      },
      {
        'name':this.currentBrandData.employee,
        'navigationPath': '/pages/plan/employees',
        }
   ]
}
      // this.tabs[0].active = false;
      // this.tabs[1].active = true;
      // this.tabs[1].disabled = false;
      // this.tabs[2].active = false;
      // this.tabs[2].disabled = true;
      this.tab1 = false;
      this.tab2 = true;
      this.tab3 = false;
      // this.clearData();
    }

    else if (tabEvent.tabTitle == 'Results') {
      this.tab1 = false;
      this.tab2 = false;
      this.tab3 = true;
      this.header={
        title: 'Upload '+this.currentBrandData.employee,
        btnsSearch: true,
        btnBackshow:true,
        showBreadcrumb:true,
         breadCrumbList:[
          {
            'name': 'Settings',
            'navigationPath': '/pages/plan',
            },
            {
              'name':this.currentBrandData.employee,
              'navigationPath': '/pages/plan/employees',
              }
         ]
      }
      // this.tabs[0].active = false;
      // this.tabs[1].active = false;
      // this.tabs[1].disabled = false;
      // this.tabs[2].active = true;
      // this.tabs.disabled = false;

      // this.clearData();
    }
    // if(this.tab1 == true){
    //   this.tab1 = false;
    // }else{
    //   this.tab1 = true;
    // }
    // if(this.tab2 == false){
    //   this.tab2 = true;
    // }else{
    //   this.tab2 = false;
    // }
    this.cancel();
  }

  getSelectedRows() {

  }

  // enableSubmit:any = false;

  getRowDataLength() {
    if (this.rowData.length == 0) {
      // var toast : Toast = {
      //     type: 'error',
      //     title: "File cannot be uploaded!",
      //     body: "There is no valid data to upload",
      //     showCloseButton: true,
      //     // tapToDismiss: false,
      //     timeout: 2000
      //     // onHideCallback: () => {
      //     //     this.router.navigate(['/pages/plan/users']);
      //     // }
      // };
      // this.toasterService.pop(toast);
      this.presentToast('error', '');
    } else {
      // this.enableSubmit = true;
      this.header={
        title: 'Upload '+this.currentBrandData.employee,
        btnsSearch: true,
        btnBackshow:true,
        showBreadcrumb:true,
         breadCrumbList:[
          {
            'name': 'Settings',
            'navigationPath': '/pages/plan',
            },
            {
              'name':this.currentBrandData.employee,
              'navigationPath': '/pages/plan/employees',
              }
         ]
      }
      this.subfile();
    }
  }

  fileDetails: any = {
    filename: 'Users Data',
    colSep: ''
  }

  subfile() {
    if (this.tab2) {
      // this.tabs[1].active = false;
      // this.tabs[1].disabled = true;
      // this.tabs[2].active = true;
      // this.tabs[2].disabled = false;
      this.tab1 = false;
      this.tab2 = false;
      this.tab3 = true;

      this.makeDataReadyResult();
    }
  }
  back() {
      this.router.navigate(['/pages/plan/employees']);
  }

  backTab1() {
    // this.router.navigate(['/pages/users/induction']);
    // this.tabs = this.tabSelect.tabs._results;

    if (this.tab2) {
      this.header={
        title: 'Upload '+this.currentBrandData.employee,
        btnsSearch: true,
        btnBackshow:true,
        btnName1:'Upload',
        btnName1show:true,
        btnNameupload: true,
        uploadsheetLink: "assets/images/Template_employee_upload.xlsx",
        showBreadcrumb:true,
         breadCrumbList:[
          {
            'name': 'Settings',
            'navigationPath': '/pages/plan',
            },
            {
              'name':this.currentBrandData.employee,
              'navigationPath': '/pages/plan/employees',
              }
         ]
      }
      // this.tabs[1].active = false;
      // this.tabs[1].disabled = true;
      // this.tabs[0].active = true;
      // this.tabs[2].active = false;
      // this.tabs[2].disabled = true;

      this.tab1 = true;
      this.tab2 = false;
      this.tab3 = false;
    }
    this.cancel();
  }

  backTab2() {
    // this.router.navigate(['/pages/users/induction']);
    // this.tabs = this.tabSelect.tabs._results;

    if (this.tab3) {
      this.header={
        title: 'Upload '+this.currentBrandData.employee,
        btnsSearch: true,
        btnBackshow:true,
        btnName1:'Save',
        btnName1show:true,
        showBreadcrumb:true,
         breadCrumbList:[
          {
            'name': 'Settings',
            'navigationPath': '/pages/plan',
            },
            {
              'name':this.currentBrandData.employee,
              'navigationPath': '/pages/plan/employees',
              }
         ]
      }
      // this.tabs[2].active = false;
      // this.tabs[2].disabled = true;
      // this.tabs[0].active = true;
      // this.tabs[1].active = false;
      // this.tabs[1].disabled = true;
      this.tab1 = false;
      this.tab2 = true;
      this.tab3 = false;
    }
    this.cancel();
    //this.clearPreviewData();
    this.clearResultData();
  }
  upDateDropLIst: any = [];
  tempObjSrc: any = null;
  getUploadData(cb) {
    const item = {
      tId: this.userLoginData.data.data.tenantId,
    }
    // this.spinner.show();
    this.loader =true;
    this.service.getalllovData(item).then(rescompData => {
      this.loader =false;
      this.upDateDropLIst = rescompData['data'];
      if (this.upDateDropLIst && this.upDateDropLIst.length > 0) {
        this.tempObjSrc = {};
        this.upDateDropLIst.forEach((element, i) => {
          this.tempObjSrc[element.name] = i + 1;
        });
        cb(true);
      }else {
        cb(null);
      }
      console.log('Course Dropdown List ', this.upDateDropLIst);
    },
      resUserError => {
        this.loader =false;
        // this.spinner.hide();
        this.errorMsg = resUserError;
        cb(null);
      });
  }
  convertDate(date) {
    console.log(this.commonservice.formatDigitDate(date),"date")
    console.log(this.commonservice.formatDateTimeForBulkUpload(date),"bulkdate");
    // return this.commonservice.formatDigitDate(date);
    return this.commonservice.formatDateTimeForBulkUpload(date)
  }
}


