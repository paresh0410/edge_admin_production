import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

import { NgxEchartsModule } from 'ngx-echarts';

import { ThemeModule } from '../../../../@theme/theme.module';

import { UploademployeesComponent } from './uploademployees.component';
import { UploadEmployeesService } from './uploademployees.service'
import { TranslateModule } from '@ngx-translate/core';
import { JsonToXlsxService } from './json-to-xlsx-service';
import { XlsxToJsonService } from './xlsx-to-json-service';
import { TabsModule } from 'ngx-tabs';

import { AngularSlickgridModule } from 'angular-slickgrid';
import { ComponentModule } from '../../../../component/component.module';
@NgModule({
  imports: [
    ThemeModule,
    NgxEchartsModule,
    TabsModule,
    AngularSlickgridModule.forRoot(),
    TranslateModule.forRoot(),
    ComponentModule,
  ],
  declarations: [
    UploademployeesComponent,
  ],
  providers: [
    UploadEmployeesService,
    JsonToXlsxService,
    XlsxToJsonService
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class UploadEmployeesModule { }
