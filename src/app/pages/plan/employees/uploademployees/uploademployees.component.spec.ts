import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploademployeesComponent } from './uploademployees.component';

describe('UploadusersComponent', () => {
  let component: UploademployeesComponent;
  let fixture: ComponentFixture<UploademployeesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploademployeesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploademployeesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
