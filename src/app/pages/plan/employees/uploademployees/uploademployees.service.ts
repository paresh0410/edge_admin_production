import {Injectable, Inject} from '@angular/core';
import {Http, Response, Headers, RequestOptions, Request} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppConfig} from '../../../../app.module';
import{ webApi} from '../../../../service/webApi';
import { HttpClient } from "@angular/common/http";
@Injectable()
export class UploadEmployeesService {

	 private _urlUpload:string =webApi.domain + webApi.url.uploadEmployess;
   private _url:string = webApi.domain + webApi.url.getLOVuploadData;

  constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private _http: Http, private http1 :HttpClient){

  }

  getalllovData(ddata){
    // let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    // let options = new RequestOptions({ headers: headers });

  //  return this._http.post(this.allErnroledusers, data, options)
  //  .map((response:Response) => response.json())
  //  .catch(this._errorHandler);


   return new Promise(resolve => {
    this.http1.post(this._url, ddata)
    //.map(res => res.json())
    .subscribe(data => {
        resolve(data);
    },
    err => {
        resolve('err');
    });
});

 }

  uploadUsers(user) {
    return new Promise(resolve => {
      this.http1.post(this._urlUpload, user)
      //.map(res => res.json())
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });

  }

  _errorHandler(error: Response){
    console.error(error);
    return Observable.throw(error || "Server Error")
  }

  public data: any;
  
}
