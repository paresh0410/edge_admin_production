import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { NgxEchartsModule } from 'ngx-echarts';
import { ThemeModule } from '../../../@theme/theme.module';
import { TabsModule } from 'ngx-tabs';
//import { slikgridDemo } from './../../..../slikgridDemo.component';
//import { slikgridDemoService } from './slikgridDemo.service';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import {  Employees } from './employees.component';
import {  EmployeesService } from './employees.service';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { TranslateModule } from '@ngx-translate/core';
import { AngularSlickgridModule } from 'angular-slickgrid';
import { UploademployeesComponent } from './uploademployees/uploademployees.component';
import { AddeditemployeesComponent } from './addeditemployees/addeditemployees.component';
import { ComponentModule } from '../../../component/component.module';

//import {AddEditUserService} from './addeditusers/addeditusers.service';
// const PLAN_COMPONENTS = [
//   slikgridDemo
// ];

@NgModule({
  imports: [
    ThemeModule,
    NgxEchartsModule,
    TabsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    NgxDatatableModule,
    // UsersModule,
    AngularSlickgridModule.forRoot(),
    TranslateModule.forRoot(),
    ComponentModule
  ],
  declarations: [
     Employees,
    // UploademployeesComponent,
    // AddeditemployeesComponent,
  ],
  providers: [
     EmployeesService,
    //AddEditUserService
    //slikgridDemoService,
  ],
  schemas: [ 
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA 
  ]
})
export class EmployeesModule { }
