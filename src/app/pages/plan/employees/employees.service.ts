import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, RequestOptions, Request } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AppConfig } from '../../../app.module';
import { webApi } from '../../../service/webApi';
import { HttpClient } from "@angular/common/http";
@Injectable()
export class EmployeesService {

  private _url: string = "/api/induction/getInductionEmployees"
  private _urlGet: string = "/api/induction/getInductedEmployees"
  private _urlSet: string = "/api/induction/setInductionAttendance"
  private _urlFilter: string = "/api/dashboard/induction_emp_filter" // Dev
  private _urlGetUnRegEmp: string = "/api/induction/getUnRegEmployees";
  private _urlGetEMpData: string = webApi.domain + webApi.url.getAllEMPMasterData;
  private _urlUpdateEMpStatus: string = webApi.domain + webApi.url.updateEMPAttendanceStatus;
  private _urlgetallemployee: string = webApi.domain + webApi.url.getallemployee;


  request: Request;
  public data: any;

  constructor(
    @Inject('APP_CONFIG_TOKEN') private config: AppConfig,
     private _http: Http, private http1: HttpClient) {

  }

  /****************** To get unregistered employees start **************/
  getUnRegEmp(user) {
    let url: any = `${this.config.FINAL_URL}` + this._urlGetUnRegEmp;
    return new Promise(resolve => {
      this.http1.post(url, user)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  /****************** To get unregistered employees end **************/

  /****************** To get induction and inducted employees start **************/
  getUsers(user) {
    let url: any = `${this.config.FINAL_URL}` + this._urlFilter;
    return new Promise(resolve => {
      this.http1.post(url, user)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  /****************** To get induction and inducted employees end **************/

  /****************** To mark attendance start **************/
  updateUser(user) {
    let url: any = `${this.config.FINAL_URL}` + this._urlSet;
    return new Promise(resolve => {
      this.http1.post(url, user)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  /****************** To mark attendance end **************/

  /****************** Not Used start **************/
  //@LoadingIndicator()
  getUserTableData() {
    let url: any = `${this.config.FINAL_URL}` + this._url;
    return new Promise(resolve => {
      this.http1.post(url, '')
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  getInductedData() {
    let url: any = `${this.config.FINAL_URL}` + this._urlGet;
    return new Promise(resolve => {
      this.http1.post(url, '')
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  /**THis is for gettinf emploeyee list */
  getAllEMPloyees(user) {
    return new Promise(resolve => {
      this.http1.post(this._urlgetallemployee, user)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  /*THis is for updating the employee */
  UPdateAttendanceStatus(user) {
    return new Promise(resolve => {
      this.http1.post(this._urlUpdateEMpStatus, user)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  /****************** Not Used end **************/

  _errorHandler(error: Response) {
    console.error(error);
    return Observable.throw(error || "Server Error")
  }

}
