import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Injectable, Inject, ModuleWithProviders } from '@angular/core';
import { webAPIService } from '../../../service/webAPIService';
import { webApi } from '../../../service/webApi';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from '../../../service/authentication.service'
import { AppConfig } from '../../../app.module';

// import { webAPIService } from '../../../service/webAPIService';
// import { webApi } from '../../../service/webApi';

// import { AuthenticationService } from '../../../service/authentication.service'

@Injectable()
export class BlendedService {

  private _urlCourses: string = webApi.domain + webApi.url.getBatches;
  private _urlCategories: string = webApi.domain + webApi.url.getCategories;
  private _urlDisableCourse: string = webApi.domain + webApi.url.disableBatch;
  private _nominationInstanceList: string = webApi.domain + webApi.url.getnomination;
  private _urlDisableInstance: string = webApi.domain + webApi.url.disableNominationInstances;
  private _urlgetallfcodeofconductce: string = webApi.domain + webApi.url.getallfcodeofconduct;
  private _urladdeditcodeofconduct: string = webApi.domain + webApi.url.addeditcodeofconduct;
  private addEditNominationInstance: string = webApi.domain + webApi.url.addEditInstance;
  private nominationDropdown: string = webApi.domain + webApi.url.nominationInstanceDropdown;
  private workflowevaluationUrl: string = webApi.domain + webApi.url.workflow_evaluation;
  private trainerlisturl: string = webApi.domain + webApi.url.get_All_Trainers;
  private _EEPInstanceList: string = webApi.domain + webApi.url.getEEP;
  private _EEPurlDisableInstance: string = webApi.domain + webApi.url.disableEEPInstances;
  private _EEPurlgetallfcodeofconductce: string = webApi.domain + webApi.url.getallEEPcodeofconduct;
  private _EEPurladdeditcodeofconduct: string = webApi.domain + webApi.url.addeditEEPcodeofconduct;
  private EPPaddEditNominationInstance: string = webApi.domain + webApi.url.addEditEEPInstance;
  private EEPnominationDropdown: string = webApi.domain + webApi.url.EEPInstanceDropdown;
  private _EEPurlgetallfBuisnessBreak: string = webApi.domain + webApi.url.getallEEPbuisnessbreak;
  private _EEPurladdeditfBuisnessBreak: string = webApi.domain + webApi.url.addeditEEPbuisnessbreak;
  private _urlDublicateCourse: string = webApi.domain + webApi.url.duplicateCourse;

  public data: any = [];
  // public program: any;
  public date: any = [];
  userData;
  tenantId;
  private headers;
  private options;
  crsId: any;
  wfId: any = null;
  public program : any = {
    catName: null,
    catId: null,
    pName: null,
    pId: '',
    instName: '',
    nDesc: '',
    sDate: '',
    eDate: '',
    eCsrId: null,
    visible: '',
    instituteName: '',
    instituteId: '',
    partnerName: '',
    distributionPartnerId: '',
    budget: '',
    seatCost: '',
  };

  // From TTT Flags
  // public showWorkFlowBatche: boolean = false;
  public workflowId: number = null;
  parenCatId: any;
  countLevel: any;
  parentCatId: any;

  constructor(@Inject('APP_CONFIG_TOKEN') private config: AppConfig, private _http: HttpClient,
    private authenticationService: AuthenticationService) {
    // add authorization header with jwt token
    // this.headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    // this.options = new RequestOptions({ headers: this.headers });
    if (localStorage.getItem('LoginResData')) {
      this.userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', this.userData.data);
      // this.userId = this.userData.data.data.id;
      this.tenantId = this.userData.data.data.tenantId;
    }

  }

  public param: any = {
    tId: this.tenantId,
  };
  disableCourse(visibleData) {
    let url: any = this._urlDisableCourse;
    // let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    // let options = new RequestOptions({ headers: headers });
    // return this._http.post(url,visibleData)
    //     .map((response:Response)=>response.json())
    //     .catch(this._errorHandler);

    return new Promise(resolve => {
      this._http.post(url, visibleData)
        // .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  dublicateCourse(visibleData) {
    let url: any = this._urlDublicateCourse;
    return new Promise(resolve => {
      this._http.post(url, visibleData)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  getCourses(data) {
    // let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    // let options = new RequestOptions({ headers: headers });
    // return this._http.post(this._urlCourses,this.param)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);


    return new Promise(resolve => {
      this._http.post(this._urlCourses, data)
        // .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  getCategories(data) {
    // let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    // let options = new RequestOptions({ headers: headers });
    // return this._http.post(this._urlCategories,this.param)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);
    return new Promise(resolve => {
      this._http.post(this._urlCategories, data)
        // .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  getallinstancecodeofcondunce(data) {
    return new Promise(resolve => {
      this._http.post(this._urlgetallfcodeofconductce, data)
        // .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  _errorHandler(error: Response) {
    console.error(error);
    return Observable.throw(error || "Server Error")
  }


  getnomination(nominationData) {
    let url: any = this._nominationInstanceList;

    return new Promise(resolve => {
      this._http.post(url, nominationData)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  addeditcodeofcontent(Data) {
    return new Promise(resolve => {
      this._http.post(this._urladdeditcodeofconduct, Data)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  disableInstance(data) {
    // let url:any = this._urlDisableInstance;
    return new Promise(resolve => {
      this._http.post(this._urlDisableInstance, data)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  getAddEditInstance(data) {
    // let url:any = this._urlDisableInstance;
    return new Promise(resolve => {
      this._http.post(this.addEditNominationInstance, data)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  getnominationDropdown(data) {
    // let url:any = this._urlDisableInstance;
    return new Promise(resolve => {
      this._http.post(this.nominationDropdown, data)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  workflowevaluation(data) {
    // let url:any = this._urlDisableInstance;
    return new Promise(resolve => {
      this._http.post(this.workflowevaluationUrl, data)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  trainerList(data) {
    // let url:any = this._urlDisableInstance;
    return new Promise(resolve => {
      this._http.post(this.trainerlisturl, data)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  EEPgetnomination(nominationData) {
    let url: any = this._EEPInstanceList;

    return new Promise(resolve => {
      this._http.post(url, nominationData)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }


  EEPdisableInstance(data) {
    // let url:any = this._urlDisableInstance;
    return new Promise(resolve => {
      this._http.post(this._EEPurlDisableInstance, data)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  EEPgetallinstancecodeofcondunce(data) {
    return new Promise(resolve => {
      this._http.post(this._EEPurlgetallfcodeofconductce, data)
        // .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  EEPaddeditcodeofcontent(Data) {
    return new Promise(resolve => {
      this._http.post(this._EEPurladdeditcodeofconduct, Data)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  EEPgetAddEditInstance(data) {
    // let url:any = this._urlDisableInstance;
    return new Promise(resolve => {
      this._http.post(this.EPPaddEditNominationInstance, data)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  EEPgetnominationDropdown(data) {
    // let url:any = this._urlDisableInstance;
    return new Promise(resolve => {
      this._http.post(this.EEPnominationDropdown, data)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  getallEEPbuisnessbreak(nominationData) {
    let url: any = this._EEPurlgetallfBuisnessBreak;

    return new Promise(resolve => {
      this._http.post(url, nominationData)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  addeditEEPbuisnessbreak(Data) {
    return new Promise(resolve => {
      this._http.post(this._EEPurladdeditfBuisnessBreak, Data)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
}
