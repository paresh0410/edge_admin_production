import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkBatchComponent } from './bulk-batch.component';

describe('BulkBatchComponent', () => {
  let component: BulkBatchComponent;
  let fixture: ComponentFixture<BulkBatchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BulkBatchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkBatchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
