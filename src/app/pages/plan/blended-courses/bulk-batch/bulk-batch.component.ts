import { Component, OnInit } from '@angular/core';
import { SuubHeader } from '../../../components/models/subheader.model';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { webApi } from '../../../../service/webApi';
import { CommonFunctionsService } from '../../../../service/common-functions.service';
import { JsonToXlsxService } from '../../un-enrol-employess/json-to-xlsx.service';
import { noData } from '../../../../models/no-data.model';
import { NgxSpinnerService } from 'ngx-spinner';
import { isThisMinute } from 'date-fns';


@Component({
  selector: 'ngx-bulk-batch',
  templateUrl: './bulk-batch.component.html',
  styleUrls: ['./bulk-batch.component.scss']
})
export class BulkBatchComponent implements OnInit {
  header: SuubHeader  = {
    title: 'Bulk-Batch',
    btnsSearch: true,
    btnBackshow: true,
    showBreadcrumb: true,
    btnAddshow: true,
    btnAdd: 'Upload',
    breadCrumbList:[
      {
        'name': 'Blended Learning',
        'navigationPath': '/pages/blended-home',
      },
    ]
  };
  sidebarForm: boolean = false;
  fileName : any = 'Click here to upload an excel file to set notification in batches'
  labels1:any =[
  { labelname: 'STATUS', bindingProperty: 'valid', componentType: 'text' },
  { labelname: 'REASON', bindingProperty: 'msg', componentType: 'text' },
  { labelname: 'ECN / USERNAME', bindingProperty: 'username', componentType: 'text' },
  { labelname: 'COHORT1', bindingProperty: 'cohort1', componentType: 'text' },
  { labelname: 'FIRST NAME', bindingProperty: 'firstname', componentType: 'text' },
  { labelname: 'LAST NAME', bindingProperty: 'lastname', componentType: 'text' },
  { labelname: 'EMAIL', bindingProperty: 'email', componentType: 'text' },
  { labelname: 'PHONE', bindingProperty: 'phone1', componentType: 'text' },
  { labelname: 'ATTENDANCE', bindingProperty: 'attendance', componentType: 'text' },
  { labelname: '    DOJ     ', bindingProperty: 'dojDisplay', componentType: 'date' },
]

labels:any =[
  { labelname: 'user Name', bindingProperty: 'username', componentType: 'text' },
  { labelname: 'FullName', bindingProperty: 'fullname', componentType: 'text' },
  { labelname: 'Batch Count', bindingProperty: 'courseCount', componentType: 'text' },
  { labelname: 'Created Date', bindingProperty: 'createdDate', componentType: 'date' },
  { labelname: 'Valid Till', bindingProperty: 'validTill', componentType: 'date' },
  { labelname: 'Download', bindingProperty: '', componentType: 'icondown'},

  
]

labelsSession:any =[
  { labelname: 'user Name', bindingProperty: 'username', componentType: 'text' },
  { labelname: 'FullName', bindingProperty: 'fullname', componentType: 'text' },
  { labelname: 'Session Count', bindingProperty: 'sessionCount', componentType: 'text' },
  { labelname: 'Created Date', bindingProperty: 'createdDate', componentType: 'date' },
  { labelname: 'Valid Till', bindingProperty: 'validTill', componentType: 'date' },
  { labelname: 'Download', bindingProperty: '', componentType: 'icondown'},

]
  uploadedFile: boolean = false;
  titleName : any = 'Upload File'
  file: any;
  tabEvent: any;
  batchList: any;
  dupBatchList: any;
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No Data Found.",
    desc:'To create batches in bulk click on upload button or to know more about bulk batch click on "Learn More"',
    titleShow:true,
    btnShow:true,
    descShow:true,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/learning-how-to-add-a-batch',
  };
  noData: boolean;
  sessionList: any;
  dupSessionList: any;
  search: number;
  param: { tabId: number; event: any; };

  constructor(private toastr: ToastrService,private router: Router,private commonservice: CommonFunctionsService,
    private spinner: NgxSpinnerService,
    protected exportService: JsonToXlsxService,) { 

    // this.getBatchList()
    // this.getSessionList()
  }

  ngOnInit() {
    this.header  = {
      title: 'Bulk-Batch',
      btnsSearch: true,
      btnBackshow: true,
      showBreadcrumb: true,
      btnAddshow: true,
      btnAdd: 'Upload',
      btnNameupload: false,
      uploadsheetLink: "assets/images/Template_employee_upload.xlsx",
      breadCrumbList:[
        {
          'name': 'Blended Learning',
          'navigationPath': '/pages/blended-home',
        },
      ]
    };
  }

  back(){
    if(this.sidebarForm == false){
    this.router.navigate(['pages/blended-home'])
    }
    else{
      this.getBatchList()
      this.getSessionList()
      this.sidebarForm = false
      // if(this.tabEvent.tabTitle == 'Batch'){
      //   this.getBatchList()
      // }
      this.header  = {
        title: 'Bulk-Batch',
        btnsSearch: true,
        searchBar:true,
        placeHolder:'Search by keyword',
        btnBackshow: true,
        showBreadcrumb: true,
        btnAddshow: true,
        btnAdd: 'Upload',
        btnNameupload: false,
        uploadsheetLink: "assets/images/Template_employee_upload.xlsx",
        breadCrumbList:[
          {
            'name': 'Blended Learning',
            'navigationPath': '/pages/blended-home',
          },
        ]
      };
    }
  }

  getBatchList(){
    this.spinner.show()
    const _urlgetBatch:string = webApi.domain + webApi.url.getBatchBulkList;
    this.commonservice.httpPostRequest(_urlgetBatch,{}).then(res=>{
      this.spinner.hide()
      console.log("res",res)
      if(res['type'] == true){
        console.log(res)
        this.batchList = res['output']
        this.dupBatchList = res['output']
        if(this.batchList.length == 0){
          this.noData = true
        }else{
          this.noData = false
        }
      }else{
        this.spinner.hide()
        this.presentToast('warning','Something went wrong')
      }

    })
  }

  getSessionList(){
    this.spinner.show()
    const _urlgetBatch:string = webApi.domain + webApi.url.getSessionBulkList;
    this.commonservice.httpPostRequest(_urlgetBatch,{}).then(res=>{
      this.spinner.hide()
      console.log("res",res)
      if(res['type'] == true){
        console.log(res)
        this.sessionList = res['output']
        this.dupSessionList = res['output']
        if(this.sessionList.length == 0){
          this.noData = true
        }else{
          this.noData = false
        }
      }else{
        this.spinner.hide()
        this.presentToast('warning','Something went wrong')
      }

    })
  }
  

  upload(){
    this.sidebarForm = true
    this.noData = false
    var event = 0
    this.onSelectTabTest(event)
    // this.router.navigate(['blended-home/bulk-batch/upload']);
    // this.router.navigate(['/pages/blended-home/blended-category/bulk-batch/upload']);


  }
  download(event){
    console.log(event)
    var param = {
      uniqueId:event.uniqueId
    }
    const _urlgetUploadedData:string = webApi.domain + webApi.url.getUploadedData;
    this.commonservice.httpPostRequest(_urlgetUploadedData,param).then(res=>{
      console.log("res",res)
      if(res['type'] == true){
        console.log(res)
        this.exportService.exportAsExcelFile(res['output'], 'uploadedData');

        // this.batchList = res['output']
      }else{
        this.presentToast('warning','Something went wrong')
      }

    })
  }


  downloadSession(event){
    console.log(event)
    var param = {
      uniqueId:event.uniqueId
    }
    const _urlgetUploadedData:string = webApi.domain + webApi.url.getSessionUploadedData;
    this.commonservice.httpPostRequest(_urlgetUploadedData,param).then(res=>{
      console.log("res",res)
      if(res['type'] == true){
        console.log(res)
        this.exportService.exportAsExcelFile(res['output'], 'uploadedData');

        // this.batchList = res['output']
      }else{
        this.presentToast('warning','Something went wrong')
      }

    })
  }
  changeTab(tabEvent){
    this.tabEvent = tabEvent
    this.sidebarForm = false
    if (tabEvent.tabTitle == 'Batch') {
      this.header  = {
        title: 'Bulk-Batch',
        btnsSearch: true,
        searchBar:true,
        placeHolder:'Search by keyword',
        btnBackshow: true,
        showBreadcrumb: true,
        btnAddshow: true,
        btnAdd: 'Upload',
        btnNameupload: false,
        uploadsheetLink: "assets/images/Template_employee_upload.xlsx",
        breadCrumbList:[
          {
            'name': 'Blended Learning',
            'navigationPath': '/pages/blended-home',
          },
        ]
      };
      this.getBatchList()
      // var event = 0
      // this.onSelectTabTest(event)
    }
    else{
      this.header  = {
        title: 'Bulk-Batch',
        btnsSearch: true,
        searchBar:true,
        placeHolder:'Search by keyword',
        btnBackshow: true,
        showBreadcrumb: true,
        btnAddshow: true,
        btnAdd: 'Upload',
        btnNameupload: false,
        uploadsheetLink: "assets/images/Template_employee_upload.xlsx",
        breadCrumbList:[
          {
            'name': 'Blended Learning',
            'navigationPath': '/pages/blended-home',
          },
        ]
      };
      this.getSessionList()

      // var event = 0
      // this.onSelectTabTest(event)
    }

    }


  
  onSelectTab(event){
    // if (tabEvent.tabTitle == 'Add File') {
    

  }

  onSelectTabTest(tabEvent){
    console.log('tab Selected', tabEvent);

    // if (tabEvent.tabTitle == 'Add File') {
    if (tabEvent == 0) {
      this.search = 0
      this.header = {
        title: 'Bulk Batches',
        btnsSearch: true,
        searchBar: false,
        placeHolder:'Search by keyword',
        btnBackshow: true,
        showBreadcrumb: true,
        btnAddshow: false,
        btnAdd: 'Upload',
        btnName1show : false,
        btnName1: 'Save',
        btnNameupload: false,
        uploadsheetLink: "assets/images/Template_employee_upload.xlsx",
        breadCrumbList:[
          {
            'name': 'Blended Learning',
            'navigationPath': '/pages/blended-home',
          },
        ]
      };
      // this.uploadedFile = false
      this.header.btnAddshow = false
      // this.tab1 = true
      // this.tab2 = false
      // this.tab3 = false
    }
    // else if(tabEvent.tabTitle == 'Preview')
    else if(tabEvent == 1)
    
    {
      this.search = 1
      this.header ={
        title: 'Bulk Batches',
        btnsSearch: true,
        searchBar:true,
        placeHolder:'Search by keyword',
        btnBackshow: true,
        showBreadcrumb: true,
        btnAddshow: false,
        btnAdd: 'Save',
        breadCrumbList:[
          {
            'name': 'Blended Learning',
            'navigationPath': '/pages/blended-home',
          },
        ]
      };
      // this.uploadedFile = true
      // this.tab2 = true
      // this.tab1 = false
      // this.tab3 = false


    }
    else{
      this.search = 2
      this.header ={
        title: 'Bulk Batches',
        btnsSearch: true,
        btnBackshow: true,
        searchBar: true,
        placeHolder:'Search by keyword',
        showBreadcrumb: true,
        btnAddshow: false,
        btnAdd: 'Save',
        breadCrumbList:[
          {
            'name': 'Blended Learning',
            'navigationPath': '/pages/blended-home',
          },
        ]
      };
      this.header.btnAddshow = false
      // this.uploadedFile = true
      // this.tab3 = true
      // this.tab1 = false
      // this.tab2 = false

    }


  }
  uploadFile(){
    this.uploadedFile = true

  }
  cloeSidebar(){
    this.sidebarForm = false
    this.uploadedFile = false
  }

  searchData(event) {
    if(this.search && this.sidebarForm == true){
       this.param = {
        tabId :this.search,
        event :event
      }
    }else{
    if(this.tabEvent.tabTitle == 'Session'){
      this.searchDataSession(event)
    }else{
    var searchtext
    this.header.searchtext = event.target.value
    console.log(searchtext);
    searchtext= event.target.value;
    const val = searchtext.toLowerCase();
    if(val.length>=3 || val.length == 0){
    const tempcc = this.dupBatchList.filter(function (d) {
      return String(d.username).toLowerCase().indexOf(val) !== -1 ||
        d.fullname.toLowerCase().indexOf(val) !== -1 ||
        d.createdDate.toLowerCase().indexOf(val) !== -1 ||
        d.validTill.toLowerCase().indexOf(val) !== -1 ||
        !val
    });
    console.log(tempcc);
    this.batchList = tempcc;
    if(tempcc.length == 0){
      this.noData = true
      this.noDataVal={
        margin:'mt-3',
        imageSrc: '../../../../../assets/images/no-data-bg.svg',
        title:"Sorry we couldn't find any matches please try again.",
        desc:"",
        titleShow:true,
        btnShow:false,
        descShow:false,
        btnText:'Learn More',
        btnLink:'',
      }
    }else{
      this.noData = false
    }


    
  }
}
}
  }
  clear() {
    if(this.header.searchtext){
    this.header.searchtext = '';
    this.batchList = this.dupBatchList
    this.sessionList = this.dupSessionList
    }
    else{
    }
  }

  searchDataSession(event){
    var searchtext
    this.header.searchtext = event.target.value
    console.log(searchtext);
    searchtext= event.target.value;
    const val = searchtext.toLowerCase();
    if(val.length>=3  || val.length == 0){
    const tempcc = this.dupSessionList.filter(function (d) {
      return String(d.username).toLowerCase().indexOf(val) !== -1 ||
        d.fullname.toLowerCase().indexOf(val) !== -1 ||
        d.createdDate.toLowerCase().indexOf(val) !== -1 ||
        d.validTill.toLowerCase().indexOf(val) !== -1 ||
        !val
    });
    console.log(tempcc);
    this.sessionList = tempcc;
    if(tempcc.length == 0){
      this.noDataVal={
        margin:'mt-3',
        imageSrc: '../../../../../assets/images/no-data-bg.svg',
        title:"Sorry we couldn't find any matches please try again.",
        desc:"",
        titleShow:true,
        btnShow:false,
        descShow:false,
        btnText:'Learn More',
        btnLink:'',
      };
      this.noData = true
    }else{
      this.noData = false
    }


    
  }

  }

  onSelectFile(event) {
    var validExts
    
      validExts = new Array(
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/vnd.ms-excel",
      )
    
    var fileType = event.target.files[0].type;
    var fileExt = false;
    for(let i=0; i<validExts.length; i++){
      if(fileType.includes(validExts[i])){
        // return true;
        fileExt = true;
        break;
      }
    }
    // if(validExts.indexOf(fileType) < 0) {
    if(!fileExt) {

    
      this.presentToast('warning', 'Please select a valid file type');
    
    }else{
    this.file = event.target.files[0]
    this.fileName = event.target.files[0].name
    // this.file =file
    for (let i = 0; i < event.target.files.length; i++) {
        
      // upload file using path
      var reader = new FileReader();
      reader.onload = (event:any) => {
        // console.log(event.target.result);

      }
      reader.readAsDataURL(event.target.files[i]);
    }
    // console.log(path,"path")
  }
  }
  presentToast(type, body) {
    if(type === 'success'){
    this.toastr.success(body, 'Success', {
    closeButton: false
    });
    } else if(type === 'error'){
    this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
    timeOut: 0,
    closeButton: true
    });
    }else if(type === 'info'){
      this.toastr.info(body, 'Success', {
      closeButton: false
      });
    }else{
    this.toastr.warning(body, 'Warning', {
    closeButton: false
    })
    }
    }

}
