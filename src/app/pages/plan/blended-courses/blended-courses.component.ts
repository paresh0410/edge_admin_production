import { Component, ViewEncapsulation ,ElementRef, ViewChild,ChangeDetectorRef} from '@angular/core';
import { Router, NavigationStart, Routes, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BaseChartDirective } from 'ng2-charts/ng2-charts';
import { AddEditBlendCourseContentService } from './addEditCourseContent/addEditCourseContent.service';
import { BlendedService } from './blended.service';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { ToastrService } from 'ngx-toastr';
import { webAPIService } from '../../../service/webAPIService';
import { webApi } from '../../../service/webApi';
import { detailsBlendService } from './addEditCourseContent/details/details.service';
import { costingService } from './addEditCourseContent/costing/costing.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { isString } from 'util';
import { AssetService } from '../../dam/asset/asset.service';
import { NbSidebarService } from '@nebular/theme';
import { LayoutService } from '../../../@core/data/layout.service';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { Card } from '../../../models/card.model';
import { SuubHeader } from '../../components/models/subheader.model';
import { noData } from '../../../models/no-data.model';
import { Filter } from '../../../models/filter.modal';
declare var window: any;

@Component({
  selector: 'ngx-blended-courses',
  templateUrl: './blended-courses.component.html',
  styleUrls: ['./blended-courses.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class BlendedCoursesComponent {
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  header: SuubHeader
  myInnerHeight: any = window.innerHeight - 120;
  skeleton=false;
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No Batches at this time.",
    desc:"Batches will appear after they are added by the instructor. Batches are a bundle of learning activities which are designed by the instructors to enable blended learning",
    btnShow:true,
    descShow:true,
    titleShow:true,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/learning-how-to-add-a-batch',
  }
  cardModify: Card = {
    flag: 'blendedCourse',
    titleProp : 'fullname',
    image: 'coursePicRef',
    category: 'catName',
    // discrption: 'description',
    courseCompletion: 'Completioncount',
    compleCount: 'Completioncount',
    totalCount: 'enrollcount',
    manager: 'cereaterName',
    maxpoints: 'maxPoints',
    showCompletedEnroll: true,
    showBottomCategory: true,
    showBottomManager: true,
    showBottomMaximumPoint: true,
    showCourseCompletion: true,
    hoverlable:   true,
    showImage: true,
    option:   true,
    eyeIcon:   true,
    copyIcon: true,
    bottomDiv:   true,
   // bottomDiscription:   true,
    showBottomList:   true,
    bottomTitle:   true,
    btnLabel: 'Edit',
    defaultImage: 'assets/images/courseicon.jpg'
  };
  sortdata = [{
    id: 1,
    title: 'Batch name',
    sortingmenu: [
      {
        id: 1,
        value: 'asc',
        name: 'ASC',
      }, {
        id: 2,
        value: 'desc',
        name: 'DESC',
      }
    ],
    type: ''
  },
  {
    id: 2,
    title: 'Time Creation/Time modified',
    sortingmenu: [
      {
        id: 1,
        value: 'asc1',
        name: 'ASC',
      }, {
        id: 2,
        value: 'desc2',
        name: 'DESC',
      }
    ],
    type: ''
  }
  ]

  sortingmenu: any = [
    {
      id: 1,
      value: 'asc',
      name: 'ASC',
    }, {
      id: 2,
      value: 'desc',
      name: 'DESC',
    }
  ];
  query: string = '';
  public getData;

  cat: any;
  category: any = []
  count:number=8
  content: any = []
  content1: any = [];
  parentcat: any = [];
  // noContent;
  search: any;
  errorMsg: string;
  loader: any;
  showSpinner:boolean = false;

  catData: any;
  categoryId: any;
  userData: any;
  tenantId: any;
  param;
  getCourseUrl = webApi.domain + webApi.url.getBatches;
  getCourseDropdown = webApi.domain + webApi.url.getBatchDropdown;
  getCategoriesurl = webApi.domain + webApi.url.getCategories;

  duplicateCourseUrl = webApi.domain + webApi.url.duplicateBatch;
  nodata: any = false;
  searchTags: any;
  searchcategory: any;
  searchcourselevel: any;
  searchmanager: any;
  displayArray: any = [];
  courseFormat: any = [];
  visibility: any = [];
  courseLevel: any = [];
  daysBucket: any = [];
  categoryData: any = [];
  userRoles: any = [];
  creatorList: any = [];
  programs: any = [];
  venueData: any = [];
  locationData: any = [];
  workflow: any = [];
  searchprogram: any;
  searchlocation: any;
  searchvenue: any;
  Tags: any = [];
  order = '';
  sortData: any;
  coloumn: any;
  categoryName: any;
  // filter
  filters: any = [];
  filter: boolean = false;
  filtersInner: any = [];
  filtercon: Filter = {
    ascending: true,
    descending: true,
    showDropdown: true,
    dropdownList: [
      { drpName: 'Batch Name', val: 1 },
      { drpName: 'Created Date', val: 2 },
    ]
  };
  constructor(private spinner: NgxSpinnerService, private detailsService: detailsBlendService,
    private cdf: ChangeDetectorRef,
    // private toasterService: ToasterService,
    protected webApiService: webAPIService, private route: ActivatedRoute,
    protected contentservice: BlendedService, private toastr: ToastrService, private sidebarService: NbSidebarService,
    private layoutService: LayoutService, protected passService: AddEditBlendCourseContentService,
    private commonFunctionService: CommonFunctionsService,
    private router: Router, public assetservice: AssetService) {

    // this.loader = true;

    this.search = {
      // categoryId: '',
      // courseCode: '',
      // courseCompletion: '',
      // courseId: '',
      // courseLevelId: '',
      // courseOrder: '',
      // coursePicRef: '',
      // courseTypeId: '',
      // craditpoints: '',
      // creatorId: '',
      fullname: '',
      // leadTime: '',
      // shortname: '',
      // summary: '',
      // tags: '',
      // tenantId: '',
      // validFromDate: '',
      // validToDate: '',
      // validationFreq: '',
      // visible: '',
    };
    this.searchTags = {
      // tags
    };
    this.searchcategory = {
      // categoryName
    };
    this.searchcourselevel = {
      // name
    }
    this.searchmanager = {
      // fullname
    }
    this.searchprogram = {
      // name
    }
    this.searchlocation = {
      // name
    }
    this.searchvenue = {
      // name
    }

    this.cat = {
      id: ''
    };
    if (localStorage.getItem('LoginResData')) {
      this.userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', this.userData.data);
      // this.userId = this.userData.data.data.id;
      this.tenantId = this.userData.data.data.tenantId;
    }
    this.param = {
      tId: this.tenantId,
    }
    // var passData = this.passService.data;
    var passData = this.contentservice.data;
    console.log(passData);
    if (passData.data !== undefined) {
      this.header={
        title:passData.data.categoryName,
        btnsSearch: true,
        searchBar: true,
        placeHolder:'Serach By keywords',
        dropdownlabel: ' ',
        drplabelshow: false,
        drpName1: '',
        drpName2: ' ',
        drpName3: '',
        drpName1show: false,
        drpName2show: false,
        drpName3show: false,
        btnName1: '',
        btnName2: '',
        btnName3: '',
        btnAdd: 'Add Batch',
        btnName1show: false,
        btnName2show: false,
        btnName3show: false,
        btnBackshow: true,
        btnAddshow: true,
        filter: true,
        showBreadcrumb: true,
        breadCrumbList:[
          //  {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          {
            'name': 'Blended Learning',
            'navigationPath': '/pages/blended-home',
          },
          {
            'name': 'Blended Category',
            'navigationPath': '/pages/blended-home/blended-category',
          },
        ]
      };
      this.categoryId = passData.data.categoryId;
      this.categoryName=passData.data.categoryName;
      this.cat.id = this.categoryId;
    } else {
      this.cat.id = '';
      this.header={
        title:'Batches',
        btnsSearch: true,
        searchBar: true,
        placeHolder:'Serach By keywords',
        dropdownlabel: ' ',
        drplabelshow: false,
        drpName1: '',
        drpName2: ' ',
        drpName3: '',
        drpName1show: false,
        drpName2show: false,
        drpName3show: false,
        btnName1: '',
        btnName2: '',
        btnName3: '',
        btnAdd: 'Add Batch',
        btnName1show: false,
        btnName2show: false,
        btnName3show: false,
        btnBackshow: true,
        btnAddshow: true,
        filter: true,
        showBreadcrumb: true,
        breadCrumbList:[
          //  {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          {
            'name': 'Blended Learning',
            'navigationPath': '/pages/blended-home',
          },
        ]
      };
    }
    // this.toggleSidebar();
    this.getCourses();
    this.getCategories();
    this.getCourseDropdownList();
    this.getalltags();
  }

  courseDropdownList: any = [];
  getCourseDropdownList() {
    // this.spinner.show();
    let data = {
      tId: this.tenantId,
      typeId:4
    }
    const _urlGetCourseDropdown: string = webApi.domain + webApi.url.getBatchDropdown;
    //this.commonFunctionService.httpPostRequest(_urlGetCourseDropdown,data)
    this.detailsService.getDropdownList(data)
    .then(rescompData => {
      // this.loader =false;
      this.spinner.hide();
      this.courseDropdownList = rescompData['data'];
      // console.log('Course Dropdown List ', this.courseDropdownList);
      try {
        for (let i = 0; i < this.courseDropdownList.programData.length; i++) {
          this.courseDropdownList.programData[i].programId = this.courseDropdownList.programData[i].id;
        }
        for (let i = 0; i < this.courseDropdownList.courseLevel.length; i++) {
          this.courseDropdownList.courseLevel[i].courseLevelId = this.courseDropdownList.courseLevel[i].id;
        }
        for (let i = 0; i < this.courseDropdownList.creatorList.length, i++;) {
          this.courseDropdownList.creatorList[i].creatorId = this.courseDropdownList.creatorList[i].id;
        }
        for (let i = 0; i < this.courseDropdownList.locationData.length; i++) {
          this.courseDropdownList.locationData[i].locationId = this.courseDropdownList.locationData[i].id;
        }
        for (let i = 0; i < this.courseDropdownList.venueData.length; i++) {
          this.courseDropdownList.venueData[i].venueId = this.courseDropdownList.venueData[i].id;
        }


        this.courseFormat = this.courseDropdownList.courseType;
        this.courseLevel = this.courseDropdownList.courseLevel;
        this.visibility = this.courseDropdownList.visibility;
        this.daysBucket = this.courseDropdownList.leadTime;
        this.userRoles = this.courseDropdownList.userRoles;
        this.creatorList = this.courseDropdownList.creatorList;
        this.programs = this.courseDropdownList.programData;
        this.locationData = this.courseDropdownList.locationData;
        this.venueData = this.courseDropdownList.venueData;
        this.workflow = this.courseDropdownList.workflow;

        this.bindfilter(this.programs);
        this.bindfilter(this.courseLevel);
        this.bindfilter(this.locationData);
        if (this.creatorList && this.creatorList.length > 0) {
          this.creatorList.forEach(element => {
            element.filterId = 'Batch Creator';
          })
        }
        this.bindfilter(this.creatorList);
        this.bindfilter(this.venueData);

      } catch (err) {
        console.log(err);
      }
      // this.noContent = false;

    },
      resUserError => {
        // this.loader =false;
        this.spinner.hide();
        this.errorMsg = resUserError
      });
  }
  getalltags() {
    let param = {
      // 'aId': 2,
      'tenantId': this.tenantId,
      // 'cId': 4,
    }
    const _urlGetAllTags: string = webApi.domain + webApi.url.getAllTags;
    // this.commonFunctionService.httpPostRequest(_urlGetAllTags,param)
    this.assetservice.getAllTags(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('getTagsResponse:', rescompData);
        if (result['type'] == true) {

          // if (result['data'][0].length == 0) {
          if (result['data'].length == 0) {

            // this.noTags = true;
          } else {
            // const tagArr = result['data'][0];
            const tagArr = result['data'];
            for (let i = 0; i < tagArr.length; i++) {
              tagArr[i].status = false;
            }
            this.Tags = tagArr;
            console.log('this.Tags', this.Tags);
            this.bindfilter(this.Tags);
          }

        } else {
          this.spinner.hide();
          // this.noTags = true;
          // var toast: Toast = {
          //   type: 'error',
          //   //title: "Server Error!",
          //   body: 'Something went wrong.please try again later.',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        }


      }, error => {
        this.spinner.hide();
        // this.noTags = true;
        // var toast: Toast = {
        //   type: 'error',
        //   //title: "Server Error!",
        //   body: 'Something went wrong.please try again later.',
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
          timeOut: 0,
          closeButton: true
        });
      });
  }
  getCourses() {
    // this.showSpinner = true

    // this.loader = true;
    this.content1 = [];
    this.displayArray = [];
    this.content = [];
    // if (this.contentservice.showWorkFlowBatche){
    //   const data = {
    //     cTypeId: 4,
    //     tId: this.tenantId,
    //     ord: '',
    //     wfId: this.contentservice.workflowId,
    //   };
    // } else {
    //   const data = {
    //     cTypeId: 4,
    //     tId: this.tenantId,
    //     ord: this.order,
    //     wfId: null,
    //   };
    // }
    const data = {
          cTypeId: 4,
          tId: this.tenantId,
          ord: '',
          // wfId: this.contentservice.workflowId,
          wfId:this.contentservice.workflowId ? this.contentservice.workflowId:null
        };
    // this.spinner.show();
    // const _urlCourses: string = webApi.domain + webApi.url.getBatches;
    const get_course_opt = webApi.domain + webApi.url.get_all_course_optmz;
    this.commonFunctionService.httpPostRequest(get_course_opt,data)
    //this.contentservice.getCourses(data)
      .then(rescompData => {
        // this.loader =false;
        // this.spinner.hide();


        // this.content1 = rescompData['data'];
        this.content1 = rescompData['list'];
        this.showSpinner = false
        console.log(this.content1,"content 1 blennded")

        if (this.content1) {
          this.skeleton=true;
          this.prepareData();
        } else {
          this.nodata = true;
          this.skeleton=true;
        }
        console.log('Course Result', this.content1);
        this.loader = false;
      },
        resUserError => {
          console.log(resUserError);
          // if(resUserError.statusText == "Unauthorized"){
          //   this.router.navigate(['/login']);
          // }
          // this.loader = false;
          this.spinner.hide();
          this.skeleton=true;
          this.errorMsg = resUserError;
          this.showSpinner = false
          // this.noContent = false;
        });
  }

  getCategories() {
    const data = {
      tId: this.tenantId
    }
    // this.loader = true;
    // this.spinner.show();
    const _urlCategories: string = webApi.domain + webApi.url.getCategories;
    //this.commonFunctionService.httpPostRequest(_urlCategories,data)
    this.contentservice.getCategories(data)
      .then(rescompData => {
        // this.loader =false;
        this.spinner.hide();
        this.parentcat = rescompData['data'];
        console.log('Category Result', this.parentcat);
        this.bindfilter(this.parentcat);
        this.loader = false;
      },
        resUserError => {
          // this.loader = false;
          this.spinner.hide();
          this.errorMsg = resUserError
        });
  }

  prepareData() {
    if (this.categoryId == undefined) {
      // this.asset = this.content;
      this.content = this.content1;
      this.addItems(0,this.sum,'push',this.content1);
      // this.displayArray = this.content;

      console.log('All Course', this.content);
    } else {
      var tempData =[];
      for (let i = 0; i < this.content1.length; i++) {
        if (this.categoryId == this.content1[i].categoryId) {
          tempData.push(this.content1[i]);
        }
      }
      this.content1 = tempData;
      this.addItems(0,this.sum,'push',this.content1);
      console.log('Filtered Course', this.content);

      if(this.content1.length == 0){
        this.nodata = true;
        this.skeleton=true;
      }
      console.log('Filtered Course', this.content);
    }
    this.loader = false;
  }

  doropserch() {
    this.content = [];
    if (this.cat.id == undefined || this.cat.id == "") {
      this.content = this.content1;
    } else {
      for (var i = 0; i < this.content1.length; i++) {
        if (this.cat.id == this.content1[i].categoryId) {
          this.content.push(this.content1[i]);
        }
      }
    }
  }

  clear() {
    this.search.fullname = '';
  }

  public addeditcontent(data, id) {
    var data1 = {
      data: data,
      id: id,
      catId: this.categoryId,
      categoryName:this.categoryName,
      courseDropdowns: this.courseDropdownList,
    };
    if(this.contentservice.workflowId != null){
      this.passService.workflowId = this.contentservice.workflowId;
      this.passService.isWorkflow = true;
    } else {
      this.passService.workflowId = null;
      this.passService.isWorkflow = false;
    }
    if (data == undefined) {
      this.passService.data = data1;
      // this.router.navigate(['/pages/plan/blended-home/blended-courses/addEditCourseContent']);
      this.router.navigate(['addEditCourseContent'], { relativeTo: this.route });
      console.log('Data passed to service' + data1);
      console.log('ID Passed' + data1.id + ' ' + data1.data);
    } else {
      this.passService.data = data1;
      // this.router.navigate(['/pages/plan/blended-home/blended-courses/addEditCourseContent']);
      this.router.navigate(['addEditCourseContent'], { relativeTo: this.route });
    }
  }

  gotoCardaddedit(data) {
    var data1 = {
      data: data,
      id: 1,
      catId: this.categoryId,
      categoryName:this.categoryName,
      courseDropdowns: this.courseDropdownList,
    };

    // if(this.contentservice.workflowId != null){
    //   this.passService.workflowId = this.contentservice.workflowId;
    //   this.passService.isWorkflow = true;
    // } else {
    //   this.passService.workflowId = null;
    //   this.passService.isWorkflow = false;
    // }

    if(this.contentservice.workflowId != null){
      this.passService.workflowId = this.contentservice.workflowId;
      this.passService.isWorkflow = true;
    } else {
      this.passService.workflowId = null;
      this.passService.isWorkflow = false;
    }


    this.passService.data = data1;
      // this.router.navigate(['/pages/plan/blended-home/blended-courses/addEditCourseContent']);
      this.router.navigate(['addEditCourseContent'], { relativeTo: this.route });

  }

  back() {
    // this.router.navigate(['/pages/plan/courses']);
    // this.router.navigate(['/pages/learning/blended-home']);
    window.history.back()

    // this.router.navigate(['blended-home'],{relativeTo:this.route});
    this.contentservice.data.data = undefined;
  }

  deleteCourseModal: boolean = false;
  deleteCourseData: any;
  deleteContent(course) {
    console.log('Course content', course);
    this.deleteCourseData = course;
    this.deleteCourseModal = true;
  }

  deleteContentAction(actionType) {
    if (actionType == true) {
      // this.removeCourse(this.deleteCourseData);
      this.closeDeleteContentModal();
    } else {
      this.closeDeleteContentModal();
    }
  }

  closeDeleteContentModal() {
    // console.log('Course content',course);
    this.deleteCourseModal = false;
  }

  disableCourse: boolean = false;
  disableContent(item, disableStatus) {
    this.disableCourse = !this.disableCourse;
  }

  // enable disable course
  disableCourseVisible: boolean = false;
  visibiltyRes: any;
  enableDisableCourseModal: boolean = false;
  enableDisableCourseData: any;
  enableCourse: boolean = false;
  courseDisableIndex: any;

  disableCourseVisibility(currentIndex, courseData, status) {
    this.enableDisableCourseData = courseData;
    this.enableDisableCourseModal = true;
    this.courseDisableIndex = currentIndex;

    // if (this.content[currentIndex].visible == 0) {
    //   this.enableCourse = false;
    // } else {
    //   this.enableCourse = true;
    // }
    if (this.enableDisableCourseData.visible == 0) {
      this.enableCourse = false;
    } else {
      this.enableCourse = true;
    }
  }

  clickTodisable(courseData, i) {
    if(this.displayArray[i].visible == 1) {
      this.displayArray[i].visible = 0
    } else {
      this.displayArray[i].visible = 1
    }

    this.enableDisableCourse(courseData)

    // if (courseData.visible == 1) {
    //   this.enableCourse = true;
    //   this.enableDisableCourseData = courseData;
    //   this.enableDisableCourseModal = true;
    // } else {
    //   this.enableCourse = false;
    //   this.enableDisableCourseData = courseData;
    //   this.enableDisableCourseModal = true;
    // }
  }

  enableDisableCourse(courseData) {
    // this.spinner.show();
    var visibilityData = {
      courseId: courseData.courseId,
      visible: courseData.visible
    }
    const _urlDisableCourse: string = webApi.domain + webApi.url.disableBatch;
    //this.commonFunctionService.httpPostRequest(_urlDisableCourse,visibilityData)

   this.contentservice.disableCourse(visibilityData)
      .then(rescompData => {
        // this.loader =false;
        // this.spinner.hide();
        this.visibiltyRes = rescompData;
        console.log('Course Visibility Result', this.visibiltyRes);
        if (this.visibiltyRes.type == false) {
          // var courseUpdate: Toast = {
          //   type: 'error',
          //   title: "Course",
          //   body: "Unable to update visibility of course.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          this.closeEnableDisableCourseModal();
          // this.toasterService.pop(courseUpdate);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        } else {
          // var courseUpdate: Toast = {
          //   type: 'success',
          //   title: "Course",
          //   body: this.visibiltyRes.data,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          this.closeEnableDisableCourseModal();
          // this.toasterService.pop(courseUpdate);

          this.toastr.success(this.visibiltyRes.data, 'Success', {
            closeButton: false
          });
        }
      },
        resUserError => {
          // this.loader =false;
          this.spinner.hide();
          this.errorMsg = resUserError;
          this.closeEnableDisableCourseModal();
        });
  }

  enableDisableCourseAction(actionType) {
    // if (actionType == true) {
    //   if (this.content[this.courseDisableIndex].visible == 1) {
    //     this.content[this.courseDisableIndex].visible = 0;
    //     var courseData = this.content[this.courseDisableIndex];
    //     this.enableDisableCourse(courseData);
    //   } else {
    //     this.content[this.courseDisableIndex].visible = 1;
    //     var courseData = this.content[this.courseDisableIndex];
    //     this.enableDisableCourse(courseData);
    //   }
    // } else {
    //   this.closeEnableDisableCourseModal();
    // }
    if (actionType == true) {
      let courseData;
      if (this.enableDisableCourseData.visible == 1) {
        this.enableDisableCourseData.visible = 0;
        courseData = this.enableDisableCourseData;
        this.enableDisableCourse(courseData);

      } else {
        this.enableDisableCourseData.visible = 1;
        courseData = this.enableDisableCourseData;
        this.enableDisableCourse(courseData);
      }
    } else {
      this.closeEnableDisableCourseModal();
    }
  }

  closeEnableDisableCourseModal() {
    this.enableDisableCourseModal = false;
  }

  formDataCourse: any;
  duplicateContent(courseData) {
    this.formDataCourse = {
      courseId: courseData.courseId,
      fullname: '',
      tenantId: courseData.tenantId,
    }
  }


  duplicateCourseModal: boolean = false;
  duplicateCourseData: any;
  duplicateCourseContent(course) {
    console.log('Course content', course);
    this.duplicateCourseData = course;
    this.duplicateCourseModal = true;
    this.duplicateContent(course);
  }
  copyCard(course) {
    console.log('Course content', course);
    this.duplicateCourseData = course;
    this.duplicateCourseModal = true;
    this.duplicateContent(course);
  }

  duplicateCourseAction(actionType) {
    if (actionType == true) {
      this.makeDuplicateCourseDataReady();
      this.closeDuplicateCourseModal();
    } else {
      this.closeDuplicateCourseModal();
    }
  }

  closeDuplicateCourseModal() {
    this.duplicateCourseModal = false;
  }

  formattedDupliTags: any = null;
  makeTagDataReady() {
    var tagsData = this.formDataCourse.tags;
    if (tagsData.length > 0) {
      var tagsString = '';
      for (let i = 0; i < tagsData.length; i++) {
        var tag = tagsData[i];
        if (tagsString != "") {
          tagsString += "|";
        }
        if (tag.value) {
          if (String(tag.value) != "" && String(tag.value) != "null") {
            tagsString += tag.value;
          }
        } else {
          if (String(tag) != "" && String(tag) != "null") {
            tagsString += tag;
          }
        }
      }
      this.formattedDupliTags = tagsString;
    }
  }

  creditPointsStr: any;
  // makeDuplicateCourseDataReady() {
  //   // if(this.formDataCourse.tags){
  //   //   this.makeTagDataReady();
  //   // }

  //   if (this.formDataCourse.craditpoints.length > 0) {
  //     this.creditPointsStr = '';
  //     for (let i = 0; i < this.formDataCourse.craditpoints.length; i++) {
  //       var creditPoints = this.formDataCourse.craditpoints[i];
  //       if (this.creditPointsStr != "") {
  //         this.creditPointsStr += "#";
  //       }
  //       if (String(creditPoints.creditAllocId) != "" && String(creditPoints.creditAllocId) != "null") {
  //         this.creditPointsStr += creditPoints.creditAllocId;
  //       }
  //       if (String(creditPoints.roleId) != "" && String(creditPoints.roleId) != "null") {
  //         this.creditPointsStr += '|' + creditPoints.roleId;
  //       }
  //       if (String(creditPoints.creditTypeId) != "" && String(creditPoints.creditTypeId) != "null") {
  //         this.creditPointsStr += '|' + creditPoints.creditTypeId;
  //       }
  //       if (String(creditPoints.creditTypeValue) != "" && String(creditPoints.creditTypeValue) != "null") {
  //         this.creditPointsStr += '|' + creditPoints.creditTypeValue;
  //       }
  //     }
  //     console.log('creditPoints string', this.creditPointsStr);
  //   }

  //   var courseData = {
  //     categoryId: this.formDataCourse.categoryId,
  //     courseCode: this.formDataCourse.courseCode,
  //     courseCompletion: this.formDataCourse.courseCompletion,
  //     courseId: this.formDataCourse.courseId,
  //     courseLevelId: this.formDataCourse.courseLevelId,
  //     courseOrder: this.formDataCourse.courseOrder,
  //     coursePicRef: this.formDataCourse.coursePicRef,
  //     courseTypeId: this.formDataCourse.courseTypeId,
  //     craditpoints: this.creditPointsStr,
  //     creatorId: this.formDataCourse.creatorId,
  //     fullname: this.formDataCourse.fullname,
  //     leadTime: this.formDataCourse.leadTime,
  //     shortname: this.formDataCourse.shortname,
  //     summary: this.formDataCourse.summary,
  //     // tags: this.formattedDupliTags,
  //     tags: this.formDataCourse.tags,
  //     tenantId: this.formDataCourse.tenantId,
  //     validFromDate: this.formDataCourse.validFromDate,
  //     validToDate: this.formDataCourse.validToDate,
  //     validationFreq: this.formDataCourse.validationFreq,
  //     visible: this.formDataCourse.visible,
  //   }
  //   console.log('Duplicate course data ', courseData);

  //   this.duplicateCourse(this.duplicateCourseUrl, courseData);
  // }

  // duplicateCourseRes: any;
  // duplicateCourse(url, course) {
  //   this.webApiService.getService(url, course)
  //     .then(rescompData => {
  //       this.loader = false;
  //       var temp: any = rescompData;
  //       this.duplicateCourseRes = temp.data;
  //       if (temp == "err") {
  //         // this.notFound = true;
  //         var courseDuplicate: Toast = {
  //           type: 'error',
  //           title: "Course",
  //           body: "Unable to duplicate course.",
  //           showCloseButton: true,
  //           timeout: 2000
  //         };
  //         this.toasterService.pop(courseDuplicate);
  //       } else if (temp.type == false) {
  //         var courseDuplicate: Toast = {
  //           type: 'error',
  //           title: "Course",
  //           body: this.duplicateCourseRes[0].msg,
  //           showCloseButton: true,
  //           timeout: 2000
  //         };
  //         this.toasterService.pop(courseDuplicate);
  //       } else {
  //         var courseDuplicate: Toast = {
  //           type: 'success',
  //           title: "Course",
  //           body: this.duplicateCourseRes[0].msg,
  //           showCloseButton: true,
  //           timeout: 2000
  //         };
  //         this.toasterService.pop(courseDuplicate);
  //       }
  //       console.log('Course duplicate Result ', this.duplicateCourseRes);
  //       // this.cdf.detectChanges();
  //     },
  //       resUserError => {
  //         this.loader = false;
  //         this.errorMsg = resUserError;
  //       });
  // }

  makeDuplicateCourseDataReady() {
    var courseData = {
      courseId: this.formDataCourse.courseId,
      fullname: this.formDataCourse.fullname,
      userMod: this.userData.data.data.id,
    }
    console.log('Duplicate course data ', courseData);

    this.duplicateCourse(courseData);
  }

  duplicateCourseRes: any;
  duplicateCourse(course) {
    this.showSpinner = true

    const _urlDublicateCourse: string = webApi.domain + webApi.url.duplicateCourse;
    //this.commonFunctionService.httpPostRequest(_urlDublicateCourse,course)
    this.contentservice.dublicateCourse(course)
      .then(rescompData => {
        this.loader = false;
        var temp: any = rescompData;
        this.duplicateCourseRes = temp.data;
        if (temp == "err") {
          this.showSpinner = false
          // this.notFound = true;
          // var courseDuplicate: Toast = {
          //   type: 'error',
          //   title: "Course",
          //   body: "Unable to duplicate course.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(courseDuplicate);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        } else if (temp.type == false) {
          this.showSpinner = false
          // var courseDuplicate: Toast = {
          //   type: 'error',
          //   title: "Course",
          //   body: this.duplicateCourseRes[0].msg,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(courseDuplicate);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        } else {
          this.showSpinner = false;
          // var courseDuplicate: Toast = {
          //   type: 'success',
          //   title: "Course",
          //   body: this.duplicateCourseRes[0].msg,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(courseDuplicate);

          this.toastr.success(this.duplicateCourseRes[0].msg, 'Success', {
            closeButton: false
          });
        }
        console.log('Course duplicate Result ', this.duplicateCourseRes);
        this.skeleton = false
        // this.cdf.detectChanges();
        this.getCourses();
      },
        resUserError => {
          this.loader = false;
          this.errorMsg = resUserError;
        });
  }


  // ngOnDestroy() {
  //   var demo = {
  //     data: undefined,
  //     id: 0,
  //   }
  //   // this.contentservice.data = demo;
  //   console.log('Service destroy');
  //   // this.toggleSidebar();
  // }



  /*8888************************* filter************************/



  fieldValue = [];
  ObjectSetterValueArray = [];

  filterData = {};

  // searchTags:any;



  // subject filteration starts here
  // main array
  newArray = [];

  // array for display on html page
  // Initially without filter it will hold data of asset
  // displayArray = this.asset;

  // nofilterAsset variable for validation
  noFilterAsset: boolean = false;


  // new array for tags
  tagArray = [];
  //
  filterTag: any = [];
  // number display to n
  SelectedCategoryLength: any = {};

  SubjectFilterArray = [];
  SubjectName_CheckBoxedStatus: boolean;
  // subj: any;



  /*

  After loading the screen
  ***********************************
  when we select the checkbox SubjectName function will be called.

  After execution of statement takes places

  */

  // tslint:disable-next-line: max-line-length
  filterDataObjectArrayCreation(filterData_CategoryNameForKey: any, FiltersubjectNameForArray: any, checkboxStatus: boolean) {


    let dataValue = null;
    let flag = false;
    for (const key in this.filterData) {
      if (filterData_CategoryNameForKey === key) {
        flag = true;
        dataValue = key;
        break;
      } else {
        flag = false;
      }
    }
    if (checkboxStatus) {
      if (!flag) {
        Object.assign(this.filterData, { [filterData_CategoryNameForKey]: [] });
        (this.filterData[filterData_CategoryNameForKey]).push(FiltersubjectNameForArray[filterData_CategoryNameForKey]);
        // console.log('Filter data after insertion object with array checkbox', this.filterData);
      } else {
        (this.filterData[dataValue]).push(FiltersubjectNameForArray[filterData_CategoryNameForKey]);
        // console.log('Filter data after insertion object with array checkbox', this.filterData[dataValue]);
        // console.log('true');
      }
      // console.log('Filter data after insertion object with array checkbox', this.filterData);
      // console.log(this.filterData);
    } else {
      for (let i = 0; i < this.filterData[dataValue].length; i++) {
        if (this.filterData[dataValue][i] === FiltersubjectNameForArray[dataValue]) {
          this.filterData[dataValue].splice(i, 1);
          if (this.filterData[dataValue].length === 0) {
            delete this.filterData[dataValue];
            this.SelectedCategoryLength[dataValue] = 0;
            delete this.objectSetter[dataValue];
            break;
          }
          // console.log('Filter data after Splicing object with array checkbox', this.filterData);
          // console.log(this.filterData[dataValue]);
        }
      }
    }
  }


  objectSetter = {};

  fieldValueCreatorFunction() {
    this.spinner.show()
    // const keys = Object.keys(this.filterData);
    this.fieldValue = Object.keys(this.filterData);
    // console.log('FieldValue Object Data', this.fieldValue);
    for (const ObjectSetterData of this.fieldValue) {
      Object.assign(this.objectSetter, { [ObjectSetterData]: false });
    }

    for (const filterDataCate of this.fieldValue) {
      Object.assign(this.SelectedCategoryLength, { [filterDataCate]: this.filterData[filterDataCate].length });
    }
    // console.log('object length', this.SelectedCategoryLength);
  }

  tagCreatorFunction(CategoryObject: any, StatusToPushOrPop: any) {
    this.filterData = [];
    if (StatusToPushOrPop) {
      this.tagArray.push(CategoryObject);
      for (const data of this.tagArray) {
        for (const key in data) {
          if (isString(data[key])) {
            // console.log('key type',isString(data[key]));
            // console.log(key);
            this.filterTag.push(key);
          }
        }
      }
    }
    console.log('Filter TagArray', this.filterTag);
    console.log('Tag Array', this.tagArray);
  }


  subjectName(subj: any, cat: any) {
    this.spinner.show()
    console.log(subj, cat);
    // console.log(subj.name);
    // console.log('Category name', cat);
    // this.SubjectFilterArray.push(subj.name);

    if (cat === "tags") {
      for (const tagArray of this.Tags) {
        if (subj === tagArray) {
          console.log('tag matches');
          tagArray.status = this.SubjectName_CheckBoxedStatus;
          break;
        }
      }
    }

    this.filterDataObjectArrayCreation(cat, subj, this.SubjectName_CheckBoxedStatus);
    this.fieldValueCreatorFunction();
    this.onFilterSubjectPush(cat);
    // this.tagCreatorFunction(subj, this.SubjectName_CheckBoxedStatus);
    this.spinner.hide()


  }

  onFilterSubjectPush(filterData_CategoryNameForKey: any) {
    this.spinner.show()
    this.newArray = [];
    for (const data of this.content) {

      for (const ObjectSetterData of this.fieldValue) {
        Object.assign(this.objectSetter, { [ObjectSetterData]: false });
      }

      for (const value of this.fieldValue) {
        // this.tagArray = [];
        // console.log('Value', value);
        // console.log('FilterData of particular value', this.filterData[value]);
        // console.log('Length of FilterData of particular value', this.filterData[value].length);
        // tslint:disable-next-line: prefer-for-of
        // if (value === filterData_CategoryNameForKey) {
        if (value == 'tags') {
          console.log(value);

          for (const tagData of data[value]) {
            for (let i = 0; i < this.filterData[value].length; i++) {

              // Pushing data into tag array
              // this.tagArray.push({[value]:this.filterData[value][i]});

              if (tagData == undefined || tagData != this.filterData[value][i]) {
                // console.log('ifTrue');
                // let checkData = data[value];
              } else {
                // console.log('ifFalse');
                this.objectSetter[value] = true;
                // let checkData = data[value];
                break;
              }
            }
          }

        }
        else {
          console.log('else part executed');

          for (let i = 0; i < this.filterData[value].length; i++) {

            // Pushing data into tag array
            // this.tagArray.push({[value]:this.filterData[value][i]});

            // if (data[value] === undefined || data[value] !== this.filterData[value][i]) {
            if (data[value] == undefined || data[value] != this.filterData[value][i]) {
              // console.log('ifTrue');
              // let checkData = data[value];
            } else {
              // console.log('ifFalse');
              this.objectSetter[value] = true;
              // let checkData = data[value];
              break;
            }
          }

        }
      }
      // }



      // let ObjectDataValue = null;
      let Objectflag = true;
      this.ObjectSetterValueArray = Object.values(this.objectSetter);

      for (const objectSetterValueArrayData of this.ObjectSetterValueArray) {
        if (Objectflag && objectSetterValueArrayData) {
          Objectflag = true;
        } else {
          Objectflag = false;
        }
      }

      if (Objectflag) {
        this.newArray.push(data);
      }
    }

    // console.log(this.tagArray);
    console.log(this.newArray);


    if ((this.fieldValue.length !== 0)) {
      if ((this.newArray.length !== 0)) {
        // this.displayArray = this.newArray;
        // this.content = this.newArray;
        this.displayArray = [];
        this.sum = 50;
        this.addItems(0, this.sum, 'push', this.newArray);
        this.noFilterAsset = false;
        this.nodata = false;
      } else {
        this.displayArray = [];
        this.nodata = true;
        // this.noFilterAsset = true;
      }
    } else {
      this.noFilterAsset = false;
      // this.displayArray = this.content;
      this.displayArray = [];
      this.sum = 50;
      this.addItems(0, this.sum, 'push', this.content1);
      this.nodata = false;
    }
    if (this.displayArray.length == 0) {
      this.nodata = true;
    }
    this.spinner.hide()
  }

  // subject filteration ends here


  onUpdateSubjectName(event: Event) {
    this.showSpinner = true
    console.log(event);
    this.SubjectName_CheckBoxedStatus = (<HTMLInputElement>event.target).checked;
    // console.log('Subject Name', this.SubjectName_CheckBoxedStatus);
  }
  // ngOnDestroy() {
  //   this.toggleSidebar();
  // }
  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    this.layoutService.changeLayoutSize();
    return false;
  }

  sorting(event, i, type, sort) {
    this.showSpinner = true
    console.log('event, i, type, sort', event, i, type, sort);
    var temp: any = this.makeSortingDataReady();
    console.log(temp);
    if (temp) {
      this.order = temp;
      if (this.order) {
        this.getCourses();
      }
    }
  }

  makeSortingDataReady() {
    var sortStr = '';
    for (let i = 0; i < this.sortdata.length; i++) {
      if (sortStr != '') {
        sortStr += ',';
      }
      if (this.sortdata[i].id == 1) {
        if (this.sortdata[i].type) {
          sortStr += 'fullname' + ' ' + this.sortdata[i].type;
        }
      }
      if (this.sortdata[i].id == 2) {
        if (this.sortdata[i].type) {
          sortStr += 'timemodified' + ' ' + this.sortdata[i].type;
        }
      }
    }
    console.log('Sort straing', sortStr);
    return sortStr;
  }

  ngOnDestroy(){
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0){
      e1[0].classList.add('with-scroll');
    }
  }
  ngAfterContentChecked() {
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0)
    {
      e1[0].classList.remove('with-scroll');
    }

   }
   conentHeight = '0px';
   offset: number = 100;
   ngOnInit() {
     this.conentHeight = '0px';
     const e = document.getElementsByTagName('nb-layout-column');
     console.log(e[0].clientHeight);
     // this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
     if (e[0].clientHeight > 1300) {
       this.conentHeight = '0px';
       this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
     } else {
       this.conentHeight = String(e[0].clientHeight) + 'px';
     }
   }

   sum = 40;
   addItems(startIndex, endIndex, _method, asset) {
     for (let i = startIndex; i < endIndex; ++i) {
       if (asset[i]) {
         this.displayArray[_method](asset[i]);
       } else {
         break;
       }

       // console.log("NIKHIL");
     }
     this.displayArray.length==0?this.nodata=true:this.nodata=false
   }
   // sum = 10;
   onScroll(event) {

     let element = this.myScrollContainer.nativeElement;
     // element.style.height = '500px';
     // element.style.height = '500px';
     // Math.ceil(element.scrollHeight - element.scrollTop) === element.clientHeight

     if (element.scrollHeight - element.scrollTop - element.clientHeight < 5) {
       if (this.newArray.length > 0) {
         if (this.newArray.length >= this.sum) {
           const start = this.sum;
           this.sum += 100;
           this.addItems(start, this.sum, 'push', this.newArray);
         } else if ((this.newArray.length - this.sum) > 0) {
           const start = this.sum;
           this.sum += this.newArray.length - this.sum;
           this.addItems(start, this.sum, 'push', this.newArray);
         }
       } else {
         if (this.content1.length >= this.sum) {
           const start = this.sum;
           this.sum += 500;
           this.addItems(start, this.sum, 'push', this.content1);
         } else if ((this.content1.length - this.sum) > 0) {
           const start = this.sum;
           this.sum += this.content1.length - this.sum;
           this.addItems(start, this.sum, 'push', this.content1);
         }
       }


       console.log('Reached End');
     }
   }

    // this function is use to dynamicsearch on table
  searchQuestion(event) {
    var temData = this.content1;
    var val = event == undefined ? '' : event.target.value.toLowerCase();
    var keys = [];
    if(val === '')
    {
      this.displayArray =[];
      this.newArray = [];
      this.sum = 50;
      this.addItems(0, this.sum, 'push', this.content1);
    }else{
         // filter our data
    if (temData.length > 0) {
      keys = Object.keys(temData[0]);
    }
    if(val.length>=3||val.length==0){
    var temp = temData.filter((d) => {
      for (const key of keys) {
        if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
          return true;
        }
      }
    });

    // update the rows
    this.displayArray =[];
    this.newArray = temp;
    this.sum = 50;
    this.addItems(0, this.sum, 'push', this.newArray);
  }
}
}

bindfilter(obj) {
  const filtername = obj.length > 0 ? obj[0]['filterId'] : '';
  const filterValueName = obj.length > 0 ? obj[0]['filterValue'] : '';
      const item = {
        count: "",
        value: "",
        tagname: obj.length > 0 ? obj[0]['filterId'] : '',
        isChecked: false,
        list: obj,
        filterValue: filterValueName
      }
      if (filtername) {
        this.filters.push(item);
      }
  // if (result['data'] && result['data'].length > 0) {
  //   result['data'].forEach( (value, index) => {

  //   })
  // }
}
filteredChanged1(event) {
  this.showSpinner =true;
  this.cdf.detectChanges();
  console.log('Filtered Event - ', event);
  const obj = event;
  this.filterData = {};
  this.SelectedCategoryLength = {};
  this.objectSetter = {};
  let allempty = true;
  if (obj) {
    for(const key in obj){
      const list = obj[key];
      if (Array.isArray(list)) {
      list.forEach( (value, index) => {
        allempty = false;
        console.log(key, ' - ', value);
        this.SubjectName_CheckBoxedStatus = true;
        this.subjectName(value, key);
      });
    }
    }
    if (allempty) {
      this.displayArray =[];
      this.newArray = [];
      this.sum = 50;
      this.addItems(0, this.sum, 'push', this.content1);
    }
  } else {
    this.displayArray =[];
      this.newArray = [];
      this.sum = 50;
      this.addItems(0, this.sum, 'push', this.content1);
  }
  setTimeout(() =>{
    this.showSpinner =false;
    this.cdf.detectChanges();
  }, 500);

}

getSortData(event){
  console.log(event,"sorting")
  if(event.selectedLevel == 1){
    var property = 'fullname'
  }else{
    var property = 'timecreated'
  }
  var array = this.displayArray
  // console.log(this.displayArray,"huhuhuhu")
  var dummyArray = array.sort(this.commonFunctionService.sort(property,event.selectedRadio))
  console.log(dummyArray,"dummyArray")
  this.displayArray = array


}
gotoFilter() {
  this.filter = !this.filter;
}
}

