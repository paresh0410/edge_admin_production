import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EepWorkflowComponent } from './eep-workflow.component';

describe('EepWorkfolwComponent', () => {
  let component: EepWorkflowComponent;
  let fixture: ComponentFixture<EepWorkflowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EepWorkflowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EepWorkflowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
