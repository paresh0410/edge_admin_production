import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { ContentService } from './../content.service';
import { DatePipe } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { ToastrService } from 'ngx-toastr';
import { BlendedService } from '../../../blended.service';
//import { AddEditNominationComponent } from '../add-edit-nomination.component';
import { AddEditEepWorkflowComponent } from '../add-edit-eep-workflow.component';
import { noData } from '../../../../../../models/no-data.model';


@Component({
  selector: 'eep-nominees',
  templateUrl: './nominees.component.html',
  styleUrls: ['./nominees.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NomineesComponent implements OnInit {
  @ViewChild('myTable') table: any;
  rows = [];

  steps: [
    {
      id: 1,
      name: "",
    },
    {
      id: 2,
      name: "",
    },
    {
      id: 3,
      name: "",
    },
    {
      id: 4,
      name: "",
    },
    {
      id: 5,
      name: "",
    },
    {
      id: 6,
      name: "",
    },
    {
      id: 7,
      name: "",
    },
    {
      id: 8,
      name: "",
    },
    {
      id: 9,
      name: "",
    },
    {
      id: 10,
      name: "",
    }
  ]
  nominees: any = [
    {
      ecn: 111,
      name: "Shirish Sharma",
      phone: 382975080,
      email: "shirish@gmail.com",
      workflow: this.steps
    },
    {
      ecn: 112,
      name: "Padmaja Pednekar",
      phone: 3297492020,
      email: "padmajaP12@gmail.com",
      workflow: this.steps
    },
    {
      ecn: 113,
      name: "Shivraj Singh",
      phone: 382975080,
      email: "shivraj@gmail.com",
      workflow: this.steps
    },
    {
      ecn: 114,
      name: "Anirudhsingh Rathod",
      phone: 382975080,
      email: "anirudh@gmail.com",
      workflow: this.steps
    },
    {
      ecn: 115,
      name: "Pushpadeep Shetty",
      phone: 382975080,
      email: "pushpadeep@gmail.com",
      workflow: this.steps
    },
  ];

  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No nominees at this time.",
    desc:"",
    titleShow:true,
    btnShow:true,
    descShow:false,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/eep-nominees',
  }

  nominee_Detail: any = [
    {
      ecn: 111,
      full_name: "Shirish Sharma",
      phone: 382975080,
      email: "shirish@gmail.com",
      band: 2134,
      business: "NA",
      subDept: "Human Resource",
      reporting_Manager: "Abc",
      alternate_phone: 444448929,
      trainer_before: false,
      profile_pic: "assets/images/alan.png"
    },
    {
      ecn: 112,
      full_name: "Padmaja Pednekar",
      phone: 3297492020,
      email: "padmajaP12@gmail.com",
      band: 4235,
      business: "NA",
      subDept: "Risk",
      reporting_Manager: "Abc",
      alternate_phone: 444448929,
      trainer_before: true,
      profile_pic: "assets/images/lee.png"
    },
    {
      ecn: 113,
      full_name: "Shivraj Singh",
      phone: 382975080,
      email: "shivraj@gmail.com",
      band: 2134,
      business: "NA",
      subDept: "Collection",
      reporting_Manager: "Abc",
      alternate_phone: 444448929,
      trainer_before: false,
      profile_pic: "assets/images/jack.png"
    },
    {
      ecn: 114,
      full_name: "Anirudhsingh Rathod",
      phone: 382975080,
      email: "anirudh@gmail.com",
      band: 2134,
      business: "NA",
      subDept: "Insurance",
      reporting_Manager: "Abc",
      alternate_phone: 444448929,
      trainer_before: true,
      profile_pic: "assets/images/nick.png"
    },
    {
      ecn: 115,
      full_name: "Pushpadeep Shetty",
      phone: 382975080,
      email: "pushpadeep@gmail.com",
      band: 2134,
      business: "NA",
      subDept: "Human Resource",
      reporting_Manager: "Abc",
      alternate_phone: 444448929,
      trainer_before: false,
      profile_pic: "assets/images/alan.png"
    },
  ];

  todayDate = new Date();
  approvalStatus: boolean;

  BHapproval: any = [
    { ecn: 111, BHName: "Bhakti Joshi", status: 1, onDate: this.todayDate },
    { ecn: 112, BHName: "Bhakti Joshi", status: 0, onDate: this.todayDate },
    { ecn: 113, BHName: "Bhakti Joshi", status: 0, onDate: this.todayDate },
    { ecn: 114, BHName: "Bhakti Joshi", status: 1, onDate: this.todayDate },
    { ecn: 115, BHName: "Bhakti Joshi", status: 1, onDate: this.todayDate },
  ];

  assessment: any = [
    { id: 1, question: "gbjkakvlknlak ", answer: "Option A" },
    { id: 2, question: "gbjkakvlknlak", answer: "Option A" },
    { id: 3, question: "gbjkakvlknlak ", answer: "Option A" },
    { id: 4, question: "gbjkakvlknlak ", answer: "Option A" },
    { id: 5, question: "gbjkakvlknlak ", answer: "Option A" },
    { id: 6, question: "gbjkakvlknlak ", answer: "Option A" },
    { id: 7, question: "gbjkakvlknlak ", answer: "Option A" },
    { id: 8, question: "gbjkakvlknlak ", answer: "Option A" },
    { id: 9, question: "gbjkakvlknlak ", answer: "Option A" },
    { id: 10, question: "gbjkakvlknlak ", answer: "Option A" }
  ];


  FGassessment: any = [
    { id: 1, question: "gbjkakvlknlak ", answer: "Option A" },
    { id: 2, question: "gbjkakvlknlak", answer: "Option A" },
    { id: 3, question: "gbjkakvlknlak ", answer: "Option A" },
    { id: 4, question: "gbjkakvlknlak ", answer: "Option A" },
    { id: 5, question: "gbjkakvlknlak ", answer: "Option A" },
    { id: 6, question: "gbjkakvlknlak ", answer: "Option A" },
    { id: 7, question: "gbjkakvlknlak ", answer: "Option A" },
    { id: 8, question: "gbjkakvlknlak ", answer: "Option A" },
    { id: 9, question: "gbjkakvlknlak ", answer: "Option A" },
    { id: 10, question: "gbjkakvlknlak ", answer: "Option A" }
  ];

  feedback: any = [
    { id: 1, question: "gbjkakvlknlak ", score: 5 },
    { id: 2, question: "gbjkakvlknlak", score: 3 },
    { id: 3, question: "gbjkakvlknlak ", score: 4 },
    { id: 4, question: "gbjkakvlknlak ", score: 5 },
    { id: 5, question: "gbjkakvlknlak ", score: 4 },
    { id: 6, question: "gbjkakvlknlak ", score: 2 },
    { id: 7, question: "gbjkakvlknlak ", score: 4 },
    { id: 8, question: "gbjkakvlknlak ", score: 3 },
    { id: 9, question: "gbjkakvlknlak ", score: 5 },
    { id: 10, question: "gbjkakvlknlak ", score: 2 }
  ];

  evaluationCallDetails: any =
    {
      trainerName: "Padmaja Pednekar",
      date: this.todayDate,
      fromTime: this.todayDate,
      toTime: this.todayDate
    };

  courseDetail: any =
    {
      courseName: "Padmaja Pednekar",
      venue: "Pune",
      date: this.todayDate,
      time: this.todayDate
    };
  webinar: any = [];

  loginUserdata: any;
  serviceData: any;
  nodata: boolean;
  constructor(private contentService: ContentService, private datepipe: DatePipe,
    private spinner: NgxSpinnerService,
    // private toasterService: ToasterService, 
    private toastr: ToastrService,
    public router: Router, public routes: ActivatedRoute,
    private blendedService: BlendedService,
    public AddEditNominationComponent: AddEditEepWorkflowComponent,
  ) {
    if (localStorage.getItem('LoginResData')) {
      this.loginUserdata = JSON.parse(localStorage.getItem('LoginResData'));
    }

    if (this.blendedService.program) {
      this.serviceData = this.blendedService.program;
    }

    this.rows = this.nominees;
    if(this.rows.length!=0){
      this.nodata=false;
    }else{
      this.nodata=true;
    }
    this.getAllNominees();
  }

  getAllNominees() {
    const param = {
      'wfId': this.serviceData.nId,
      'tId': this.loginUserdata.data.data.tenantId,
    };
    this.contentService.getAllNominees(param)
      .then(res => {
        this.spinner.hide();
        const result = res;
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          console.log('nominees:', result['data']);
          //this.steps = result['data'];
        }
      },
        error => {
          this.spinner.hide();
          this.presentToast('error', '');
        });
  }

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }

  certificate: any = {
    certificatePath: "assets/images/noImage2.png",
    status: true,
    date: this.todayDate,
    approver: "Approver Name"
  }

  ngOnInit() {
  }

  stepSelected: number;



  employee_Detail: any = {};
  BHApproved: any = {};

  toggleExpandRow(value, row) {
    console.log('value', value);

    if (value == 1) {
      this.stepSelected = value;
      console.log('Toggled Expand Row!', row);
      this.table.rowDetail.toggleExpandRow(row);
      for (var i = 0; i <= this.nominee_Detail.length; i++) {
        if (row.ecn == this.nominee_Detail[i].ecn) {
          this.employee_Detail = {
            ecn: this.nominee_Detail[i].ecn,
            name: this.nominee_Detail[i].full_name,
            phone: this.nominee_Detail[i].phone,
            email: this.nominee_Detail[i].email,
            band: this.nominee_Detail[i].band,
            business: this.nominee_Detail[i].business,
            subDept: this.nominee_Detail[i].subDept,
            reporting_Manager: this.nominee_Detail[i].reporting_Manager,
            alternate_phone: this.nominee_Detail[i].alternate_phone,
            trainer_before: this.nominee_Detail[i].trainer_before,
            profile_pic: this.nominee_Detail[i].profile_pic,
          }
        }
      }

      // this.closeRowDetail(row, value);
    }


    else if (value == 2) {
      this.stepSelected = value;
      console.log('Toggled Expand Row!', row);
      this.table.rowDetail.toggleExpandRow(row);
      for (var i = 0; i < this.BHapproval.length; i++) {
        if (row.ecn == this.BHapproval[i].ecn) {
          this.BHApproved = {
            ecn: this.BHapproval[i].ecn,
            BHName: this.BHapproval[i].BHName,
            status: this.BHapproval[i].status,
            onDate: this.BHapproval[i].onDate
          }
          // this.approvalStatus = this.BHApproved[i].status;
        }
        console.log(this.BHApproved);
      }
    }
    else if (value == 3) {
      this.stepSelected = value;
      console.log('Toggled Expand Row!', row);
      this.table.rowDetail.toggleExpandRow(row);
    }
    else if (value == 4) {
      this.stepSelected = value;
      console.log('Toggled Expand Row!', row);
      this.table.rowDetail.toggleExpandRow(row);
    }
    else if (value == 5) {
      this.stepSelected = value;
      console.log('Toggled Expand Row!', row);
      this.table.rowDetail.toggleExpandRow(row);
    }
    else if (value == 6) {
      this.stepSelected = value;
      console.log('Toggled Expand Row!', row);
      this.table.rowDetail.toggleExpandRow(row);
    }
    else if (value == 7) {
      this.stepSelected = value;
      console.log('Toggled Expand Row!', row);
      this.table.rowDetail.toggleExpandRow(row);
    }
    else if (value == 8) {
      this.stepSelected = value;
      console.log('Toggled Expand Row!', row);
      this.table.rowDetail.toggleExpandRow(row);
    }
    else if (value == 9) {
      this.stepSelected = value;
      console.log('Toggled Expand Row!', row);
      this.table.rowDetail.toggleExpandRow(row);
    }
    else if (value == 10) {
      this.stepSelected = value;
      console.log('Toggled Expand Row!', row);
      this.table.rowDetail.toggleExpandRow(row);
    }
    this.table.rowDetail.toggleExpandRow(row);

  }

  closeRowDetail(selectedRow, step) {
    if (step == this.stepSelected) {
      this.table.rowDetail.toggleExpandRow(selectedRow);
    }
  }
  webinarSubmit(data) {
    console.log(data);
  }


}
