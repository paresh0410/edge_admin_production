import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditEepWorkflowComponent } from './add-edit-eep-workflow.component';

describe('AddEditEepWorkflowComponent', () => {
  let component: AddEditEepWorkflowComponent;
  let fixture: ComponentFixture<AddEditEepWorkflowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditEepWorkflowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditEepWorkflowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
