import { Component, OnInit, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { BlendedService } from '../../../blended.service';
import { ContentService } from '../../add-edit-eep-workflow/content.service';
import { DatePipe } from '@angular/common';
// import { ToasterService, Toast } from 'angular2-toaster';
import { NgxSpinnerService } from 'ngx-spinner';
import { webApi } from '../../../../../../service/webApi';
import { DomSanitizer } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import * as _moment from 'moment';
import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';
import { DataSeparatorService } from '../../../../../../service/data-separator.service';
import { BrandDetailsService } from '../../../../../../service/brand-details.service';
import { noData } from '../../../../../../models/no-data.model';

const moment = (_moment as any).default ? (_moment as any).default : _moment;

export const MY_CUSTOM_FORMATS = {
  fullPickerInput: 'DD-MM-YYYY HH:mm:ss',
    parseInput: 'DD-MM-YYYY HH:mm:ss',
    datePickerInput: 'DD-MM-YYYY',
    timePickerInput: 'LT',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY'
};
@Component({
  selector: 'ngx-step-wise-data',
  templateUrl: './step-wise-data.component.html',
  styleUrls: ['./step-wise-data.component.scss'],
  providers: [
    {provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE]},
    {provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS}],
  encapsulation: ViewEncapsulation.None,
})
export class StepWiseDataComponent implements OnInit {
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No Workflow at this time.",
    desc:'',
    titleShow:true,
    btnShow:true,
    descShow:false,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/eep-nominees',
  }
  stepsData: any;
  selectedstep: any;
  data: any;
  allData: any;
  tabsArray: any = [];
  dataLoaded: boolean = false;
  BHApproved: any = [];
  loginUserdata: any = [];
  wfId: any;
  noParticipants = false;
  empId: any;
  dataPopUp: any;
  finaldata: any = [];
  approveparam: any = [];
  webinar: any = [];
  isapprove = 0;
  showPopup = false;
  marked = false;
  theCheckbox = false;
  modal_showPopup: boolean = false;
  showDetail: boolean = false;
  acceptBox = false;
  pubValue: any;
  trainers: any = [];
  step: any = [];
  employee_Detail: any = [];
  evaluationCallDetails: any = [];
  feedback: any = [];
  feedbackShow: boolean = false;
  callStartDateException: boolean = false;
  addWebinarEvent: boolean = false;
  webEventConfirm: boolean = false;
  webEventConfirmMsg: any;
  fgAssessment: any = [];
  noQuizAttempted: boolean = false;
  quizData: any = [];
  assessment: any = [];
  feedbackShowKT: boolean = false;
  vivaFeedBackObject: any = [];
  feedbackShowVD: boolean = false;
  batchDetails: any = [];
  batchSelected: boolean = false;
  mockFeedBackObject: any = [];
  mockshowVD: boolean = false;
  assesmentshowVD: boolean = false;
  assesmentFeedBackObject: any = [];
  CerPresent: boolean = false;
  CertificateObject: any = [];
  evalfbobject: any = {};
  fgAssessmentobj: any = {};
  Certmsg: any;
  disableSubmit = false;
  comment: any;
  selectedIndex = 0;
  errflag: Boolean = false;
  rejRes: any = '';
  errflag1: Boolean = false;
  ignoreColArray: any = [];
  currentTabLabel: string= '';
  currentBrandData: any;
  constructor(public blendedService: BlendedService, public contentservice: ContentService,
    private cdf: ChangeDetectorRef, private datepipe: DatePipe,
    public brandService: BrandDetailsService,
    private dataSeparatorService: DataSeparatorService,
    // public toasterService: ToasterService,
    private spinner: NgxSpinnerService, private sanitizer: DomSanitizer, private toastr: ToastrService) {
  }

  AfterViewInIt() {
  }

  closeModal(event) {
    this.modal_showPopup = false;
  }
  ngOnInit() {
    this.spinner.show();
    setTimeout(() => {
      this.spinner.hide()
    }, 2000);
    this.loginUserdata = JSON.parse(localStorage.getItem('LoginResData'));
    this.wfId = this.blendedService.wfId;
    this.steps();
    this.trainerlist();
    this.currentBrandData = this.brandService.getCurrentBrandData();
  }

  trainerlist() {
    const data = {
      tId: this.loginUserdata.data.data.tenantId,
    };
    this.blendedService.trainerList(data).then(res => {
      if (res['type'] === true) {
        try {
          this.trainers = res['data'];
          console.log('Trainers List', this.trainers);
        } catch (e) {
          console.log(e);
        }
      }
    }, err => {
      console.log(err);
    });
  }
  noStepsFound: boolean = false;
  steps() {
    const param = {
      wfId: this.blendedService.wfId,
      tId: this.loginUserdata.data.data.tenantId,
    };
    console.log(param);
    this.contentservice.stepwisedata(param).then(res => {
      console.log(res);
      if (res['type'] === true) {
        try {
          this.stepsData = res['data'];
          if (this.stepsData.length > 0) {
            try {
              this.step = this.stepsData[0];
              this.selectedstep = this.stepsData[0].id;
              this.stepdetail(this.step);
              this.noStepsFound = false;
            } catch (e) {
              console.log(e);
            }
          } else {
            this.noStepsFound = true;
          }
        } catch (e) {
          console.log(e);
        }
      }
    }, err => {
      console.log(err);
    });
  }

  stepdetail(step) {
    this.data = [];
    this.step = step;
    this.selectedstep = step.id;
    this.dataLoaded = false;
    this.tabsArray = [];
    var param = {
      wfId: this.wfId,
      sId: this.selectedstep,
      tId: this.loginUserdata.data.data.tenantId,
    };
    console.log(param);
    this.contentservice.stepwiseuserdata(param).then(res => {
      console.log();
      if (res['type'] === true) {
        try {
          this.allData = res['data'];
            console.log('Data ===>', this.allData);
            if(this.allData) {
              this.tabsArray = Object.keys(this.allData);
              console.log('Tabs Array ==>', this.tabsArray);
              this.isapprove = 0;
              this.theCheckbox = false;
              if(this.tabsArray.length !== 0){
                this.selectedIndex = 0;
              }
              // this.tabsLoaded = true;
            }
            this.data = this.allData[this.tabsArray[0]];
            if(this.data.length !== 0){
              this.dataLoaded = false;
            }else{
              this.dataLoaded = true;
            }
          // if(this.selectedstep == 2 || this.selectedstep == 3 || this.selectedstep == 4) {
          //   this.allData = res['data'];
          //   console.log('Data ===>', this.allData);
          //   if(this.allData) {
          //     this.tabsArray = Object.keys(this.allData);
          //     console.log('Tabs Array ==>', this.tabsArray);
          //   }
          //   this.data = this.allData[this.tabsArray[0]];
          // }else{
          //   this.data = res['data'];
          //   console.log('Data ===>', this.data);
          //   if (this.data) {
          //     for (let i = 0; i < this.data.length; i++) {
          //       if (this.data[i].isSelected) {
          //         this.data[i].isSelected = this.data[i].isSelected === 'true' ? true : false;
          //       }
          //     }
          //
          //   }
          // }
          this.noParticipants = true;
        } catch (e) {
          console.log(e);
        }
      }
    }, err => {
      console.log(err);
    });
  }

  tabChanged(data){
    console.log('Tab Changed ==>', data);
    // console.log('Tab Changed ==>', tabcdas);
    // if(data.tab)
    if(data.index !== -1) {
      this.currentTabLabel = data.tab.textLabel;
      this.data = this.allData[this.tabsArray[data.index]];
    }
  }
  repeatCssFunction(col) {
    return 'repeat(' + col + ', 1fr)';
  }
  displayDataFromChild(data) {
    console.log('Child Data ====>', data);
    this.finaldata = data;
  }
  updata(value) {
    // this.finaldata.appStat = value;
    if (this.comment) {
      if (this.finaldata.length == 0){
        this.toastr.info('Info', 'Please Select  '+ this.currentBrandData.employee.toLowerCase() +'s first');
      }else {
      this.empbind(this.finaldata, response => {
        if (response) {
          this.finaldata.appStat = value;
          console.log(this.finaldata);
          const data = {
            appStat: this.finaldata.appStat,
            userId: this.loginUserdata.data.data.id,
            wfId: this.wfId,
            allstr: this.empId,
            tId: this.loginUserdata.data.data.tenantId,
            comment: this.comment
          };
          this.contentservice.stepwiseBhapproval(data).then(res => {
            console.log(res);
            if (res['type'] === true) {
              this.presentToast('success', 'Business Head approval successful');
              this.stepdetail(this.step);
              this.comment = '';
              // this.getBhapprovalStatus(this.dataPopUp.empId, this.step);
            }
          }, err => {
            this.presentToast('error', '');
            console.log(err);
          });
        } else {
          this.presentToast('error', '');
          console.log('something went wrong');
        }
      });
    }
    } else {
      this.errflag = true;
    }
  }

  updatAdminAprroval(value) {
    // this.finaldata.appStat = value;
    if (this.comment) {
      if (this.finaldata.length == 0){
        this.toastr.info('Info', 'Please Select  '+ this.currentBrandData.employee.toLowerCase() +'s first');
      }else {
      this.empbind(this.finaldata, response => {
        if (response) {
          this.finaldata.appStat = value;
          console.log(this.finaldata);
          const data = {
            appStat: this.finaldata.appStat,
            userId: this.loginUserdata.data.data.id,
            wfId: this.wfId,
            allstr: this.empId,
            tId: this.loginUserdata.data.data.tenantId,
            comment: this.comment
          };
          this.contentservice.stepwiseadminapproval(data).then(res => {
            console.log(res);
            if (res['type'] === true) {
              this.presentToast('success', 'Admin approval successful');
              this.stepdetail(this.step);
              this.comment = '';
              // this.getBhapprovalStatus(this.dataPopUp.empId, this.step);
            }
          }, err => {
            this.presentToast('error', '');
            console.log(err);
          });
        } else {
          this.presentToast('error', '');
          console.log('something went wrong');
        }
      });
    }
    } else {
      this.errflag = true;
    }
  }


  empbind(data: any, cb) {
    this.empId = '';
    for (let i = 0; i < data.length; i++) {
      if (data[i].isSelected === true) {
        if (this.empId !== '') {
          this.empId += '|';
        }
        this.empId += data[i].empId;
      }
    }
    cb(this.empId);
  }

  validateEndDate(data) {
    // this.minDateto = data;
  }
  radioValue(data) {
    console.log("Data of Radio ===>", data);
    this.isapprove = data;
    if (data === 1 || data === 0) {
      if(data === 1){
        this.rejRes = '';
      }
      this.disableSubmit = false;
    } else {
      this.disableSubmit = true;
    }
  }

  onCheckValue(data, checkValue){
      if(checkValue == 'approve'){
        this.rejRes = null;
        this.disableSubmit = false;
        if(data.target.checked){
          this.isapprove = 1;
        }

      }
      else if(checkValue == 'reject'){
        this.isapprove = data.target.checked ? 0 : 1;
          if(data == 0){
            this.disableSubmit = true;

          }else{
            this.disableSubmit = false;

          }
      }
  }
  submit() {
    if (this.comment) {
      this.errflag = false;
    } else {
      this.errflag = true;
    }
  }
  submit1() {
    console.log(this.rejRes);
    if (this.rejRes) {
      this.errflag1 = false;
    } else {
      this.errflag1 = true;
    }
  }

  // webinarSubmit(webinar) {
  //   console.log(webinar);
  //   if(this.finaldata.length == 0){
  //     this.toastr.info('Info', 'Please Select employees first');
  //   }else {
  //     this.empbind(this.finaldata, response => {
  //       if (response) {
  //         const data = {
  //           userId: this.loginUserdata.data.data.id,
  //           wfId: this.wfId,
  //           allstr: this.empId,
  //           tId: this.loginUserdata.data.data.tenantId,
  //           trainerId: webinar.trainer,
  //           eventDate: this.formatDate(webinar.startdate),
  //           eventStartTime: this.formatTime(webinar.fromtime),
  //           eventEndTime: this.formatTime(webinar.totime),
  //           stepId: this.step.id,
  //         };
  //         console.log(data);
  //         this.contentservice.stepwiseenerolewebinar(data).then(res => {
  //           console.log(res);
  //           if (res['type'] === true) {
  //             this.presentToast('success', 'Webinar upadated');
  //             this.stepdetail(this.step);
  //           }
  //         }, err => {
  //           this.presentToast('error', '');
  //           console.log(err);
  //         });
  //       } else {
  //         this.presentToast('error', '');
  //       }
  //     });
  //   }

  // }

  bindPopUpDate(data) {
    console.log('Bind Pop Data====>', data);
    this.dataPopUp = data;
    this.stepFunctionToCallServices(data.empId, this.step);
  }

  stepFunctionToCallServices(empId, step) {
    switch (parseInt(step.id)) {
      case 1: this.getNomineeDetails(empId, step);
        break;
      case 2: this.getFgAssessmentDetails(empId, step);
        break;
      case 3: this.getBhapprovalStatus(empId, step);
        break;
      case 4: this.getAdminStatus(empId, step);
        break;
      default: return;
    }
  }

  getNomineeDetails(empId, step) {
    this.spinner.show();
    const param = {
      'wfId': this.blendedService.wfId,
      'empId': empId,
      'tId': this.loginUserdata.data.data.tenantId,
    };
    console.log('nomineeDetailsParams:', param, empId, step);
    this.contentservice.getNomineeDetails(param)
      .then(res => {
        this.spinner.hide();
        const result = res;
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          console.log('nominee Details:', result['data']);
          const nomDetails = result['data'][0];
          this.employee_Detail = nomDetails;
          this.showDetail = true;
          this.selectedstep = step.id;
          console.log('nomDetails', nomDetails);

          this.timewoutfun(this.employee_Detail, this.selectedstep);
        }
      },
        error => {
          this.spinner.hide();
          this.presentToast('error', '');
        });
  }
  getBhapprovalStatus(empId, step) {
    this.spinner.show();
    const param = {
      'wfId': this.blendedService.wfId,
      'empId': empId,
      'tId': this.loginUserdata.data.data.tenantId,
    };
    console.log('bhApprovalParams:', param, empId, step);
    this.contentservice.getBhApprovalStatus(param)
      .then(res => {
        this.spinner.hide();
        const result = res;
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          console.log('BH Approval:', result['data']);
          if (result['data'].length > 0) {
            var bhstatus = result['data'][0];
          }
          if (bhstatus) {
            if (bhstatus.approveStatus) {
              this.BHApproved.status = true;
              this.BHApproved.BHName = bhstatus.BHName;
              this.BHApproved.onDate = this.formatDate(bhstatus.approveDate);
              this.BHApproved.onTime = this.formatTime(bhstatus.approveDate);
              this.BHApproved.approveStatus = bhstatus.approveStatus;
            } else {
              this.BHApproved.BHId = bhstatus.BHId;
              this.BHApproved.status = false;
              this.BHApproved.BHName = bhstatus.BHName;
            }
          } else {
            this.BHApproved = [];
          }
          this.showDetail = true;
          // this.selectedstep = step.stepId;
          this.timewoutfun(this.BHApproved, this.selectedstep);
          console.log('bhstatus', bhstatus);
        }
      },
        error => {
          this.spinner.hide();
          this.presentToast('error', '');
        });
  }
  adminaprove: any = {};
  getAdminStatus(empId, step) {
    this.spinner.show();
    const param = {
      'wfId': this.blendedService.wfId,
      'empId': empId,
      'userId': this.loginUserdata.data.data.id,
      'tId': this.loginUserdata.data.data.tenantId,
    };
    console.log('adminApprovalParams:', param, empId, step);
    this.contentservice.getadminApprovalStatus(param)
      .then(res => {
        this.spinner.hide();
        const result = res;
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          console.log('Admin Approval:', result['data']);
          if (result['data'].length > 0) {
            var adminstatus = result['data'][0];
          }

          if (adminstatus) {
            if (adminstatus.approveStatus || adminstatus.approveStatus == 0) {
              this.adminaprove.status = true;
              this.adminaprove.username = adminstatus.username;
              this.adminaprove.onDate = this.formatDate(adminstatus.approveDate);
              this.adminaprove.onTime = this.formatTime(adminstatus.approveDate);
              this.adminaprove.approveStatus = adminstatus.approveStatus;
            } else {
              this.adminaprove.appId = adminstatus.appId;
              this.adminaprove.status = false;
              this.adminaprove.username = adminstatus.username;
            }
          }
          this.showDetail = true;
          this.timewoutfun(this.adminaprove, this.selectedstep);
         // this.stepSelected = step.stepOrder;
          console.log('bhstatus', adminstatus);
        }
      },
        error => {
          this.spinner.hide();
          this.presentToast('error', '');
        });
  }
  getFgAssessmentDetails(empId, step) {
    const param = {
      'tId': this.loginUserdata.data.data.tenantId,
      'empId': empId,
      'aId': step.activityId,
    };
    console.log('param', param);
    this.contentservice.getFgAssessmentDetails(param).then(
      res => {
        this.spinner.hide();
        const result = res;
        console.log('getFgAssessmentDetails', result);
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          let correctCount: any = 0;
          let wrongCount: any = 0;
          let pendingCount: any = 0;
          this.fgAssessment = result['data'][0];
          console.log('Fg Assessment:', this.fgAssessment);
          if (this.fgAssessment.length == 0) {
            this.noQuizAttempted = true;
          } else {
            this.noQuizAttempted = false;
            for (let i = 0; i < this.fgAssessment.length; i++) {
              this.quizData.totalQue = this.fgAssessment.length;
              if(this.fgAssessment[i].questionFormatId != 4) {
              if (this.fgAssessment[i].answerCorrect == 2) {
                correctCount++;
              } else if (this.fgAssessment[i].answerCorrect == 1) {
                // pendingCount++;
                wrongCount++;
              } else if (this.fgAssessment[i].answerCorrect == 0) {
                wrongCount++;
              }
            }
              if (this.fgAssessment[i].questionFormatId == 1) {
                let str = new String(this.fgAssessment[i].answers);
                const qt1 = str.replace(/%hash%/g, '|');
                // const dataReplace = '/' + this.dataSeparatorService.Hash + '/g';
                // const qt1 = str.replace(dataReplace, '|');
                this.fgAssessment[i].formattedAns = qt1;
              }
              if (this.fgAssessment[i].questionFormatId == 2 || this.fgAssessment[i].questionFormatId == 3) {
                const str = new String(this.fgAssessment[i].answers);
                const qt2 = str.replace(/%hash%/g, '-');
                // const dataReplace = '/"' + this.dataSeparatorService.Hash + '"/g';
                // const qt2 = str.replace(dataReplace, '-');
                const str1 = new String(qt2);
                const qt21 = str1.replace(/%tild%/g, '-');
                // const qt21 = str1.replace(/'|'/g, ',');
                // const dataReplace2 = '/' + this.dataSeparatorService.Pipe + '/g';
                // const qt21 = str1.replace(dataReplace2, ',');
                this.fgAssessment[i].formattedAns = qt21;
              }
              if (this.fgAssessment[i].questionFormatId == 4) {
                this.fgAssessment[i].formattedAns = this.fgAssessment[i].answers;
              }
            }

            this.quizData.correctCount = correctCount;
            this.quizData.wrongCount = wrongCount;
            this.quizData.pendingCount = pendingCount;
            this.fgAssessmentobj = {
              fgAssessment: this.fgAssessment,
              quizData: this.quizData,
            }
            this.timewoutfun(this.fgAssessmentobj, this.selectedstep);
          }
        }
      },
      error => {
        this.spinner.hide();
        this.presentToast('error', '');
      },
    );
  }



  toggleVisibility(e) {
    this.marked = e.target.checked;
    console.log(this.marked);
  }

  toggleVisibility1(value) {
    this.isapprove = value;
  }

  formatDate(date) {
    if (date) {
      const d = new Date(date);
      const formatted = this.datepipe.transform(d, 'yyyy-MM-dd');
      return formatted;
    }
  }

  formatTime(time) {
    const t = new Date(time);
    const formattedTime = this.datepipe.transform(t, 'h:mm:ss');
    return formattedTime;
  }
  timewoutfun(data, stepId) {
    setTimeout(() => {
      this.dataPopUp = data;
      this.selectedstep = stepId;
      this.showPopup = true;
      this.showDetail = true;
      this.modal_showPopup = true;
      this.cdf.detectChanges();
    }, 100);
  }
  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }
}
