import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepWiseDataComponent } from './step-wise-data.component';

describe('StepWiseDataComponent', () => {
  let component: StepWiseDataComponent;
  let fixture: ComponentFixture<StepWiseDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepWiseDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepWiseDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
