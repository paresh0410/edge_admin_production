import { Component, ViewChild, OnInit, ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material';
import { XlsxToJsonService } from '../../../../users/uploadusers/xlsx-to-json-service';
import { JsonToXlsxService } from '../../../../users/uploadusers/json-to-xlsx-service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ContentService } from '../content.service';
import { BlendedService } from '../../../blended.service';
import { BrandDetailsService } from '../../../../../../service/brand-details.service';
@Component({
  selector: 'ngx-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss']
})
export class PopupComponent implements OnInit {
  @ViewChild('fileUpload') fileUpload: any;
  IsmodelShow: boolean;
  dialogRef: any;
  uploadedData: any = [];
  result: any = [];
  showInvalidExcel: boolean = false;
  bulkUploadData: any = [];
  resultSheets: any = [];
  fileUrl: any;
  contentData: any = [];
  invaliddata: any = [];
  addEditModRes: any;
  errorMsg: any;
  private xlsxToJsonService: XlsxToJsonService = new XlsxToJsonService();
  currentBrandData: any;
    constructor(private toastr: ToastrService, public dialog: MatDialog, private exportService: JsonToXlsxService,
    public brandService: BrandDetailsService,
    private spinner: NgxSpinnerService, public contentService: ContentService, private blendedService: BlendedService, public cdf: ChangeDetectorRef) { }
  openDialog() {
    const dialogRef = this.dialog.open(PopupComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
  ngOnInit() {
    this.contentData = this.blendedService.program;
    this.currentBrandData = this.brandService.getCurrentBrandData();
    this.fileName='Click here to upload an excel file to enrol '+ this.currentBrandData.employee.toLowerCase() +' to this course'
  }
  fileName: any ;
  fileReaded: any;
  enableUpload: any;
  file: any = [];
  preview: boolean = false;

  cancel() {
    // this.fileUpload.nativeElement.files = [];
    console.log(this.fileUpload.nativeElement.files);
    this.fileUpload.nativeElement.value = '';
    console.log(this.fileUpload.nativeElement.files);
    this.fileName='Click here to upload an excel file to enrol '+ this.currentBrandData.employee.toLowerCase() +' to this course'
    this.enableUpload = false;
    this.file = [];
    this.preview = false;
  }

  close() {
    this.dialogRef.close();
  }
  readUrl(event: any) {
    this.uploadedData = [];
    this.result = [];
    const validExts = new Array('.xlsx', '.xls');
    let fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
      this.toastr.warning('Valid file types are ' + validExts.toString(), 'Warning', {
        closeButton: false
      })
      // setTimeout(data => {
      //   this.router.navigate(['/pages/users']);
      // },1000);
      // return false;
      this.cancel();
    } else {
      // return true;
      if (event.target.files && event.target.files[0]) {
        this.fileName = event.target.files[0].name;
        // read file from input
        this.fileReaded = event.target.files[0];

        if (this.fileReaded != '' || this.fileReaded != null || this.fileReaded != undefined) {
          this.enableUpload = true;
          this.showInvalidExcel = false;
        }

        let file = event.target.files[0];
        this.bulkUploadData = event.target.files[0];
        console.log('this.bulkUploadData', this.bulkUploadData);

        this.xlsxToJsonService.processFileToJson({}, file).subscribe(data => {
          this.resultSheets = Object.getOwnPropertyNames(data['sheets']);
          console.log('File Property Names ', this.resultSheets);
          let sheetName = this.resultSheets[0];
          this.result = data['sheets'][sheetName];
          console.log('dataSheet', data);
          console.log('this.result', this.result);

          if (this.result.length > 0) {
            this.uploadedData = this.result;
          }
          console.log('this.uploadedData', this.uploadedData);
        });
        var reader = new FileReader();
        reader.onload = (event: ProgressEvent) => {
          this.fileUrl = (<FileReader>event.target).result;
          /// console.log(this.fileUrl);
        };
        reader.readAsDataURL(event.target.files[0]);
      }
    }
  }

  saveaproval() {
    // this.loader = true;
    if (this.uploadedData.length > 0 && this.uploadedData.length <= 2000) {
      this.spinner.show();

      var option: string = Array.prototype.map
        .call(this.uploadedData, function (item) {
          console.log("item", item);
          var temp = {
            ecn :item.ecn,
            pmr: item.pmr,
            dac: item.dac
          }
          return Object.values(temp).join("#");
        })
        .join("|");

      //  console.log('this.courseDataService.workflowId', this.courseDataService.workflowId);

      const data = {
        aId: 31,
        iId: this.contentData.nId,
        upString: option
      }
      console.log('data', data);
      this.contentService.eepPMRDACaccepatance(data)
        .then(rescompData => {
          // this.loader =false;
          this.spinner.hide();
          var temp: any = rescompData;
          this.addEditModRes = temp.data;
          if (temp == 'err') {
            this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
              timeOut: 0,
              closeButton: true
            });
          } else if (temp.type == false) {
            this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
              timeOut: 0,
              closeButton: true
            });
              } else {
                this.toastr.success('Enrol successfully.', 'Success', {
                  closeButton: false
                });
              }
            // this.backToEnrol()
            this.cdf.detectChanges();
            this.preview = false;
            this.showInvalidExcel = true;
            this.cancel();
            this.dialog.closeAll();
           // this.close();
          console.log('Enrol bulk Result ', this.addEditModRes);
          this.cdf.detectChanges();
        },
          resUserError => {
            //this.loader = false;
            this.errorMsg = resUserError;
          });
    } else {

      if (this.uploadedData.length > 2000) {
        this.toastr.warning('File Data cannot exceed more than 2000', 'warning', {
          closeButton: true
        });
      }
      else {
        this.toastr.warning('No Data Found', 'warning', {
          closeButton: true
        });
      }
    }

  }

}
