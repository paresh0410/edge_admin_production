import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, RequestOptions, Request } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AppConfig } from '../../../../../../app.module';
import { webAPIService } from '../../../../../../service/webAPIService';
import { webApi } from '../../../../../../service/webApi';
import { AuthenticationService } from '../../../../../../service/authentication.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class EnrolNomineeService {
  public data: any;
  request: Request;
  public param: any = {
    tId: 1
  };
  public ruleData: any;

  //   private _urlProfileFields:string = "/api/edge/course/getUserProfileFields";
  private _urlprofilefield: string = webApi.domain + webApi.url.profilefield;
  private _urlprofilemenudropdown = webApi.domain + webApi.url.profilemenudropdown;
  private _urlEnrolUSermanual: string = "/api/edge/";
  private allUNErnroledusers: string = webApi.domain + webApi.url.EEPenrolserch;
  private _urlDisableUser: string = webApi.domain + webApi.url.disableCourse;
  private _urlAddEnrolUser: string = webApi.domain + webApi.url.addeepmanualEnrol;
  private _urlallRulelist: string = webApi.domain + webApi.url.getallrule;
  private _urlDisableRule = webApi.domain + webApi.url.disablerule;
  private _urlAdd_Rule = webApi.domain + webApi.url.addrule;
  private _urlAdd_Rule_enrolment = webApi.domain + webApi.url.addrulenewother;
  private _urlallRegulatorylist = webApi.domain + webApi.url.getallregulatorylist;
  private _urlDisableRegulatory = webApi.domain + webApi.url.disableregulatory;
  private _urlAdd_Regulatory = webApi.domain + webApi.url.addregulatory;
  private _urlAdd_Regulatory_new = webApi.domain + webApi.url.addregulatorynew;
  private _urlAdd_Regu_filter = webApi.domain + webApi.url.addregulatoryfilter;
  private _urlfetchsetting = webApi.domain + webApi.url.getEEPselfsetting;
  private _urlgetProfile = webApi.domain + webApi.url.getcourseprofile;
  private _urladdselfsetting = webApi.domain + webApi.url.EEPaddselfsetting;
  private addselfsetting_newUrl = webApi.domain + webApi.url.addselfsetting_eep;

  private _urlgetruledropdown = webApi.domain + webApi.url.getruledropdown;

  private _urlgetselfdropdown = webApi.domain + webApi.url.selfdrop;

  constructor(@Inject('APP_CONFIG_TOKEN') private config: AppConfig, private _http: Http, private http1: HttpClient, private authenticationService: AuthenticationService) {
    //this.busy = this._http.get('...').toPromise();
  }

  getProfileFields() {
    //  let url:any = `${this.config.FINAL_URL}`+this._urlProfileFields;
    //  let url:any = `${this.config.FINAL_URL}`+this._urlprofilefield;
    // let url:any = `${this.config.DEV_URL}`+this._urlFilter;
    //  let headers = new Headers({ 'Content-Type': 'application/json' });
    let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });
    //let body = JSON.stringify(user);
    // return this._http.post(this._urlprofilefield, options).map((res: Response) => res.json());
    return new Promise(resolve => {
      this.http1.post(this._urlprofilefield, '')
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  _errorHandler(error: Response) {
    console.error(error);
    return Observable.throw(error || "Server Error")
  }

  getprofileFieldDropdown(data) {
    return new Promise(resolve => {
      this.http1.post(this._urlprofilemenudropdown, data)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  getenrolmanualUser(data) {
    //   let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    //   let options = new RequestOptions({ headers: headers });
    //  return this._http.get(this._urlEnrolUSermanual, options)
    //     .map((response:Response) => response.json())
    //     .catch(this._errorHandler);
    return new Promise(resolve => {
      this.http1.post(this._urlEnrolUSermanual, data)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  disableUser(visibleData) {
    //   let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    //   let options = new RequestOptions({ headers: headers });
    //  let url:any = this._urlDisableUser;
    //  return this._http.post(url,visibleData, options)
    //      .map((response:Response)=>response.json())
    //      .catch(this._errorHandler);
    return new Promise(resolve => {
      this.http1.post(this._urlDisableUser, visibleData)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }


  getallunenroluser(data) {
    //   let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    //   let options = new RequestOptions({ headers: headers });
    //  return this._http.post(this.allUNErnroledusers, data, options)
    //  .map((response:Response) => response.json())
    //  .catch(this._errorHandler);
    return new Promise(resolve => {
      this.http1.post(this.allUNErnroledusers, data)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  addenroluser(data) {
    //   let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    //   let options = new RequestOptions({ headers: headers });
    //  return this._http.post(this._urlAddEnrolUser,data, options)
    //  .map((response:Response) => response.json())
    //  .catch(this._errorHandler);
    return new Promise(resolve => {
      this.http1.post(this._urlAddEnrolUser, data)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  getallrule(data) {
    //   let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    //   let options = new RequestOptions({ headers: headers });
    //  return this._http.post(this._urlallRulelist, data, options)
    //  .map((response:Response) => response.json())
    //  .catch(this._errorHandler);
    return new Promise(resolve => {
      this.http1.post(this._urlallRulelist, data)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  disableRule(visibleData) {
    //   let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    //   let options = new RequestOptions({ headers: headers });
    //  return this._http.post(this._urlDisableRule,visibleData, options)
    //      .map((response:Response)=>response.json())
    //      .catch(this._errorHandler);

    return new Promise(resolve => {
      this.http1.post(this._urlDisableRule, visibleData)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  Addruleforcourse(visibleData) {
    //   let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    //   let options = new RequestOptions({ headers: headers });
    //  return this._http.post(this._urlAdd_Rule,visibleData, options)
    //      .map((response:Response)=>response.json())
    //      .catch(this._errorHandler);

    return new Promise(resolve => {
      this.http1.post(this._urlAdd_Rule, visibleData)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }

  Addruleforcourse_new_enrolmnet(givendata) {
    return new Promise(resolve => {
      this.http1.post(this._urlAdd_Rule_enrolment, givendata)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  getallregulatory(data) {
    // let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    // let options = new RequestOptions({ headers: headers });
    // return this._http.post(this._urlallRegulatorylist, data, options)
    // .map((response:Response) => response.json())
    // .catch(this._errorHandler);
    return new Promise(resolve => {
      this.http1.post(this._urlallRegulatorylist, data)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  disableregulatory(visibleData) {
    // let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    // let options = new RequestOptions({ headers: headers });
    // return this._http.post(this._urlDisableRegulatory,visibleData, options)
    //     .map((response:Response)=>response.json())
    //     .catch(this._errorHandler);
    return new Promise(resolve => {
      this.http1.post(this._urlDisableRegulatory, visibleData)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }

  Addregulatoryforcourse(data) {
    // let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    // let options = new RequestOptions({ headers: headers });
    // return this._http.post(this._urlAdd_Regulatory,data, options)
    //     .map((response:Response)=>response.json())
    //     .catch(this._errorHandler);
    return new Promise(resolve => {
      this.http1.post(this._urlAdd_Regulatory, data)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }
  Addregulatoryforcourse_new_enrol(givendata) {
    return new Promise(resolve => {
      this.http1.post(this._urlAdd_Regulatory_new, givendata)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  regulatory_filterforcourse(data) {
    // let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    // let options = new RequestOptions({ headers: headers });
    // return this._http.post(this._urlAdd_Regu_filter,data, options)
    //     .map((response:Response)=>response.json())
    //     .catch(this._errorHandler);
    return new Promise(resolve => {
      this.http1.post(this._urlAdd_Regu_filter, data)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }
  getfilter(data) {
    // let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    // let options = new RequestOptions({ headers: headers });
    // return this._http.post(this._urlgetProfile, data, options)
    // .map((response:Response) => response.json())
    // .catch(this._errorHandler);

    return new Promise(resolve => {
      this.http1.post(this._urlgetProfile, data)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  getfechsetting(data) {
    // let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    // let options = new RequestOptions({ headers: headers });
    // return this._http.post(this._urlfetchsetting, data, options)
    // .map((response:Response) => response.json())
    // .catch(this._errorHandler);
    return new Promise(resolve => {
      this.http1.post(this._urlfetchsetting, data)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }

  addselfsetting(data) {
    // let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    // let options = new RequestOptions({ headers: headers });
    // return this._http.post(this._urladdselfsetting,data, options)
    //     .map((response:Response)=>response.json())
    //     .catch(this._errorHandler);
    return new Promise(resolve => {
      this.http1.post(this._urladdselfsetting, data)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }
  addselfsetting_new(data) {
    return new Promise(resolve => {
      this.http1.post(this.addselfsetting_newUrl, data)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }
  dropdown() {
    // let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    // let options = new RequestOptions({ headers: headers });
    // return this._http.post(this._urlgetruledropdown,this.param, options)
    //     .map((response:Response)=>response.json())
    //     .catch(this._errorHandler);
    return new Promise(resolve => {
      this.http1.post(this._urlgetruledropdown, this.param)
        //.map(res => res.json())

        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }

  getselfdropdownlist(data) {
    return new Promise(resolve => {
      this.http1.post(this._urlgetselfdropdown, data)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

}
