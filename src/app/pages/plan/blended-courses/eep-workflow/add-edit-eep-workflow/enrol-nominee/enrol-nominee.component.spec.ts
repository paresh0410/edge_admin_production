import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnrolNomineeComponent } from './enrol-nominee.component';

describe('EnrolNomineeComponent', () => {
  let component: EnrolNomineeComponent;
  let fixture: ComponentFixture<EnrolNomineeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnrolNomineeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnrolNomineeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
