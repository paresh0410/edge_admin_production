import { Component, OnInit, AfterViewInit, ViewChild, ViewEncapsulation, Input, OnChanges } from '@angular/core';
import { ContentService } from './../content.service';
import { DatePipe } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { ToastrService } from 'ngx-toastr';
import { BlendedService } from '../../../blended.service';
import { AddEditEepWorkflowComponent } from '../add-edit-eep-workflow.component';
//import { AddEditNominationComponent } from '../add-edit-nomination.component';

import { DomSanitizer } from '@angular/platform-browser';
//import { Domain } from 'domain';
// import { webAPIService } from '../../../../../../service/webAPIService';
import { webApi } from '../../../../../../service/webApi';
import { DataSeparatorService } from '../../../../../../service/data-separator.service';
import { noData } from '../../../../../../models/no-data.model';

@Component({
  selector: 'eep-nominee-details',
  templateUrl: './nominee-details.component.html',
  styleUrls: ['./nominee-details.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NomineeDetailsComponent implements OnInit, OnChanges, AfterViewInit {
  @Input('chnage') chnage;
  @Input() servicedata: any;
  steps: [
    {
      id: 1,
      name: "Nomination Details",
    },
    {
      id: 2,
      name: "BH Approval",
    },
    {
      id: 3,
      name: "Evaluation Call slot selection",
    },
    {
      id: 4,
      name: "Feedback",
    },
    {
      id: 5,
      name: "Assessment Details",
    },
    {
      id: 6,
      name: "Webinar Call",
    },
    {
      id: 7,
      name: "FG Assessment",
    },
    {
      id: 8,
      name: "Course Assigned",
    },
    {
      id: 9,
      name: "Certificate",
    }
  ]
  nominees: any = [];

  noDataVal:noData={
    margin:'mt-3 mb-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No Nominees at this time.",
    desc:"",
    titleShow:true,
    btnShow:true,
    descShow:false,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/eep-nominees',
  }

  nominee_Detail: any = [
    {
      ecn: 111,
      full_name: "Shirish Sharma",
      phone: 382975080,
      email: "shirish@gmail.com",
      band: 2134,
      business: "NA",
      subDept: "Human Resource",
      reporting_Manager: "Abc",
      alternate_phone: 444448929,
      trainer_before: false,
      profile_pic: "assets/images/alan.png"
    },
    {
      ecn: 112,
      full_name: "Padmaja Pednekar",
      phone: 3297492020,
      email: "padmajaP12@gmail.com",
      band: 4235,
      business: "NA",
      subDept: "Risk",
      reporting_Manager: "Abc",
      alternate_phone: 444448929,
      trainer_before: true,
      profile_pic: "assets/images/lee.png"
    },
    {
      ecn: 113,
      full_name: "Shivraj Singh",
      phone: 382975080,
      email: "shivraj@gmail.com",
      band: 2134,
      business: "NA",
      subDept: "Collection",
      reporting_Manager: "Abc",
      alternate_phone: 444448929,
      trainer_before: false,
      profile_pic: "assets/images/jack.png"
    },
    {
      ecn: 114,
      full_name: "Anirudhsingh Rathod",
      phone: 382975080,
      email: "anirudh@gmail.com",
      band: 2134,
      business: "NA",
      subDept: "Insurance",
      reporting_Manager: "Abc",
      alternate_phone: 444448929,
      trainer_before: true,
      profile_pic: "assets/images/nick.png"
    },
    {
      ecn: 115,
      full_name: "Pushpadeep Shetty",
      phone: 382975080,
      email: "pushpadeep@gmail.com",
      band: 2134,
      business: "NA",
      subDept: "Human Resource",
      reporting_Manager: "Abc",
      alternate_phone: 444448929,
      trainer_before: false,
      profile_pic: "assets/images/alan.png"
    },
  ];

  todayDate = new Date();
  approvalStatus: boolean;

  BHapproval: any = [
    { ecn: 111, BHName: "Bhakti Joshi", status: 1, onDate: this.todayDate, onTime: this.todayDate, comments: "dcalnasnkdnasncln" },
    { ecn: 112, BHName: "Bhakti Joshi", status: 0, onDate: null, onTime: null, comments: "" },
    { ecn: 113, BHName: "Bhakti Joshi", status: 0, onDate: null, onTime: null, comments: "" },
    { ecn: 114, BHName: "Bhakti Joshi", status: 1, onDate: this.todayDate, onTime: this.todayDate, comments: "asdfcdhabkjfb" },
    { ecn: 115, BHName: "Bhakti Joshi", status: 1, onDate: this.todayDate, onTime: this.todayDate, comments: "abdkjcfhkjdb" },
  ];

  assessment: any = [];


  FGassessment: any = [
    // { id: 1, question: "gbjkakvlknlak ", answer: "Option A" },
    // { id: 2, question: "gbjkakvlknlak", answer: "Option A" },
    // { id: 3, question: "gbjkakvlknlak ", answer: "Option A" },
    // { id: 4, question: "gbjkakvlknlak ", answer: "Option A" },
    // { id: 5, question: "gbjkakvlknlak ", answer: "Option A" },
    // { id: 6, question: "gbjkakvlknlak ", answer: "Option A" },
    // { id: 7, question: "gbjkakvlknlak ", answer: "Option A" },
    // { id: 8, question: "gbjkakvlknlak ", answer: "Option A" },
    // { id: 9, question: "gbjkakvlknlak ", answer: "Option A" },
    // { id: 10, question: "gbjkakvlknlak ", answer: "Option A" }
  ];

  feedback: any = [];
  feedbackShow: boolean;

  evaluationCallDetails: any =
    {
      // trainerName: "Padmaja Pednekar",
      // date: this.todayDate,
      // fromTime: this.todayDate,
      // toTime: this.todayDate
    };

  courseDetail: any =
    {
      courseName: "Padmaja Pednekar",
      venue: "Pune",
      date: this.todayDate,
      time: this.todayDate
    };
  webinar: any = {
    trainer: '0',
  };


  loginUserdata: any;
  serviceData: any;
  wfId: any;
  noEmpEnrol: boolean = false;
  trainers: any = [];
  nomineeslist: any = [];
  searchtext: any;
  searchText: any;
  constructor(private contentService: ContentService, private datepipe: DatePipe,
    private dataSeparatorService: DataSeparatorService,
    private spinner: NgxSpinnerService,
    // private toasterService: ToasterService,
    private toastr: ToastrService,
    public router: Router, public routes: ActivatedRoute,
    private blendedService: BlendedService, private sanitizer: DomSanitizer) {
    // this.showReason = true;
    if (localStorage.getItem('LoginResData')) {
      this.loginUserdata = JSON.parse(localStorage.getItem('LoginResData'));
    }

    if (this.blendedService.program) {
      this.serviceData = this.blendedService.program;
      this.getAllNominees();
      this.trainerlist();
      console.log(this.serviceData);
    }
    if (this.blendedService.wfId) {
      this.wfId = this.blendedService.wfId;
    }

  }

  ngOnInit() {
    if (this.blendedService.program) {
      this.serviceData = this.blendedService.program;
      console.log(this.serviceData);
    }
    if (this.blendedService.wfId) {
      this.wfId = this.blendedService.wfId;
      this.getAllNominees();
    }
    this.trainerlist();
  }
  ngOnChanges() {
    console.log(this.servicedata);
    if (this.blendedService.program) {
      this.getAllNominees();
      this.trainerlist();
    }
  }
  ngAfterViewInit() {
    // console.log(this.servicedata);
    // if (this.servicedata.nId) {
    //   this.getAllNominees();
    //   this.trainerlist();
    // }
  }
  trainerlist() {
    const data = {
      tId: this.loginUserdata.data.data.tenantId,
    };
    this.blendedService.trainerList(data).then(res => {
      if (res['type'] === true) {
        try {
          this.trainers = res['data'];
        } catch (e) {
          console.log(e);
        }
      }
      console.log('this.trainers', this.trainers);
    }, err => {
      console.log(err);
    });
  }

  minDateto: Date;

  validateEndDate(data) {
    this.minDateto = data;
  }
  notificationLabel: any = [
		{ labelname: 'ECN No.', bindingProperty: 'ecn', componentType: 'text' },
		{ labelname: 'Name', bindingProperty: 'name', componentType: 'text' },
		{ labelname: 'Contact', bindingProperty: 'phone', componentType: 'text' },
        { labelname: 'Email', bindingProperty: 'email', componentType: 'text' },
  ]
  getAllNominees() {
    this.spinner.show();
    const param = {
      'wfId': this.blendedService.wfId,
      'tId': this.loginUserdata.data.data.tenantId,
    };
    // const param = {
    //   'wfId': this.servicedata.nId,
    //   'tId': this.loginUserdata.data.data.tenantId,
    // }
    this.contentService.getAllNominees(param)
      .then(res => {

        const result = res;
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          console.log('nominees:', result['data']);
          const nomArr = result['data'];
          if (nomArr.length === 0) {
                this.spinner.hide();
            this.noEmpEnrol = true;
            if (this.chnage == true) {
              this.presentToast('warnig', 'Workflow doesnt have any enrollment');
            }
            console.log('noEmpEnrol', this.noEmpEnrol);
          } else {
            this.noEmpEnrol = false;
            nomArr.forEach(element => {
              if (element.workflow) {
                element.workflow = element.workflow.split(',');
              }

              // element.activityIds = element.activityIds.split(',');
            });
            console.log('nomArr', nomArr);
            this.nomineeslist = nomArr;
            this.nominees = this.nomineeslist;
            this.insertingClickablePropertyInObject(nomArr);
            console.log(this.nominees);
            this.spinner.hide();
          }

          //this.steps = result['data'];
        }
      },
        error => {
          this.spinner.hide();
          this.presentToast('error', '');
        });
  }

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }

  certificate: any = {
    certificatePath: "assets/images/noImage2.png",
    status: true,
    date: this.todayDate,
    approver: "Approver Name"
  }

  stepSelected: any;
  selectedRow: any
  showDetail: boolean = false;

  activityId: any;
  previousRowIndex: any;
  showRowDetail(rowIndex, stepIndex, rowData, step) {
    this.BHApproved = {};
    console.log("row index >>", rowIndex);
    console.log("step index >>", stepIndex);
    console.log("row Data >>", rowData);
    console.log("step  >>", step);

    if (this.showDetail && this.stepSelected != step.stepOrder) {
      this.stepFunctionToCallServices(rowData.empId, step);
      this.showDetail = true;
      this.stepSelected = step.stepOrder;
      this.selectedRow = rowIndex;
    } else if (this.showDetail && this.selectedRow == rowIndex) {
      this.showDetail = false;
    } else {
      this.stepFunctionToCallServices(rowData.empId, step);
      // if(step.stepOrder){

      //   this.showDetail = true;
      // }
      this.stepSelected = step.stepOrder;
      this.selectedRow = rowIndex;
    }

  }

  stepFunctionToCallServices(empId, step) {
    switch (parseInt(step.stepOrder)) {
      case 1: this.getNomineeDetails(empId, step);
        break;
      case 2: this.getFgAssessmentDetails(empId, step);
        break;
      case 3: this.getBhapprovalStatus(empId, step);
        break;
      case 4: this.getAdminStatus(empId, step);
        break;
      // case 5: this.getWebinarEventByEmpId(empId, step);
      //   break;
      // case 6: this.getObserverDetail(empId, step);
      //   break;
      // case 7: this.getFgAssessmentDetails(empId, step);
      //   break;
      // case 8: this.getKnowledgeTestDetails(empId, step);
      //   break;
      // case 9: this.getVivaDetails(empId, step);
      //   break;
      // case 10: this.getBatchDetail(empId);
      //   break;
      // case 11: this.getMockDetails(empId, step);
      //   break;
      // case 12: this.getAssesmentDetails(empId, step);;
      //   break;
      // case 13: this.getCertificateDetails(empId);
      //   break;
      default: return;
    }
  }

  getNomineeDetails(empId, step) {
    this.spinner.show();
    const param = {
      'wfId': this.blendedService.wfId,
      'empId': empId,
      'tId': this.loginUserdata.data.data.tenantId,
    };
    console.log('nomineeDetailsParams:', param, empId, step);
    this.contentService.getNomineeDetails(param)
      .then(res => {
        this.spinner.hide();
        const result = res;
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          console.log('nominee Details:', result['data']);
          const nomDetails = result['data'][0];
          this.employee_Detail = nomDetails;
          this.showDetail = true;
          this.stepSelected = step.stepOrder;
          console.log('nomDetails', nomDetails);
        }
      },
        error => {
          this.spinner.hide();
          this.presentToast('error', '');
        });
  }
  /////// step 3
  getBhapprovalStatus(empId, step) {
    this.spinner.show();
    const param = {
      'wfId': this.blendedService.wfId,
      'empId': empId,
      'tId': this.loginUserdata.data.data.tenantId,
    };
    console.log('bhApprovalParams:', param, empId, step);
    this.contentService.getBhApprovalStatus(param)
      .then(res => {
        this.spinner.hide();
        const result = res;
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          console.log('BH Approval:', result['data']);
          if (result['data'].length > 0) {
            var bhstatus = result['data'][0];
          }

          if (bhstatus) {
            if (bhstatus.approveStatus || bhstatus.approveStatus == 0) {
              this.BHApproved.status = true;
              this.BHApproved.BHName = bhstatus.BHName;
              // this.BHApproved.onDate = bhstatus.approveDate;
              this.BHApproved.onDate = this.formatDate(bhstatus.approveDate);
              this.BHApproved.onTime = this.formatTime(bhstatus.approveDate);
              this.BHApproved.approveStatus = bhstatus.approveStatus;
            } else {
              this.BHApproved.BHId = bhstatus.BHId;
              this.BHApproved.status = false;
              this.BHApproved.BHName = bhstatus.BHName;
            }
          }
          this.showDetail = true;
          this.stepSelected = step.stepOrder;
          console.log('bhstatus', bhstatus);
        }
      },
        error => {
          this.spinner.hide();
          this.presentToast('error', '');
        });
  }


  employee_Detail: any = {};
  BHApproved: any = {};



  onapprove(rowData, data, status) {
    this.spinner.show();
    console.log('rowData', rowData);
    //console.log('step', step);
    console.log('data', data);
    console.log('status', status);
    const param = {
      'appId': data.BHId ? data.BHId : null,
      'appStat': status,
      'appCmt': data.comments,
      'actId': this.activityId,
      'userId': this.loginUserdata.data.data.id,
      'wfId': this.blendedService.wfId,
      'empId': rowData.empId,
      'tId': this.loginUserdata.data.data.tenantId,
    };
    console.log('param', param);
    this.contentService.updateBhApprovalStatus(param).then(
      res => {
        this.spinner.hide();
        const result = res;
        console.log('result', result);
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          this.presentToast('success', 'GBH Approval Successfull.');
          console.log('BH Approval on approve:', result['data']);
          this.BHApproved = {};
          this.showDetail = false;
          this.getAllNominees();
        }
      },
      error => {
        this.spinner.hide();
        this.presentToast('error', '');
      },
    );
  }
  /*******************************fg assessment / step 2**********************************************/
  adminaprove: any = {};
  getAdminStatus(empId, step) {
    this.spinner.show();
    const param = {
      'wfId': this.blendedService.wfId,
      'empId': empId,
      'userId': this.loginUserdata.data.data.id,
      'tId': this.loginUserdata.data.data.tenantId,
    };
    console.log('adminApprovalParams:', param, empId, step);
    this.contentService.getadminApprovalStatus(param)
      .then(res => {
        this.spinner.hide();
        const result = res;
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          console.log('Admin Approval:', result['data']);
          if (result['data'].length > 0) {
            var adminstatus = result['data'][0];
          }

          if (adminstatus) {
            if (adminstatus.approveStatus || adminstatus.approveStatus == 0) {
              this.adminaprove.status = true;
              this.adminaprove.username = adminstatus.username;
              this.adminaprove.onDate = this.formatDate(adminstatus.approveDate);
              this.adminaprove.onTime = this.formatTime(adminstatus.approveDate);
              this.adminaprove.approveStatus = adminstatus.approveStatus;
            } else {
              this.adminaprove.appId = adminstatus.appId;
              this.adminaprove.status = false;
              this.adminaprove.username = adminstatus.username;
            }
          }
          this.showDetail = true;
          this.stepSelected = step.stepOrder;
          console.log('bhstatus', adminstatus);
        }
      },
        error => {
          this.spinner.hide();
          this.presentToast('error', '');
        });
  }


  employee_Detail1: any = {};
  adminApproved: any = {};



  onadminapprove(rowData, data, status) {
    this.spinner.show();
    console.log('rowData', rowData);
    //console.log('step', step);
    console.log('data', data);
    console.log('status', status);
    const param = {
      'appId': this.loginUserdata.data.data.id,
      'appStat': status,
      'appCmt': data.comments,
      //'actId': this.activityId,
      'userId': this.loginUserdata.data.data.id,
      'wfId': this.blendedService.wfId,
      'empId': rowData.empId,
      'tId': this.loginUserdata.data.data.tenantId,
    };
    console.log('param', param);
    this.contentService.updateadminApprovalStatus(param).then(
      res => {
        this.spinner.hide();
        const result = res;
        console.log('result', result);
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          this.presentToast('success', 'Admin approval successfully.');
          console.log('Admin Approval on approve:', result['data']);
          this.adminaprove = {};
          this.showDetail = false;
          this.getAllNominees();
        }
      },
      error => {
        this.spinner.hide();
        this.presentToast('error', '');
      },
    );
  }
  /*******************************fg assessment / step 2**********************************************/
  fgAssessment: any;
  noQuizAttempted: boolean = false;
  quizData: any = {};
  getFgAssessmentDetails(empId, step) {
    const param = {
      'tId': this.loginUserdata.data.data.tenantId,
      'empId': empId,
      'aId': step.activityId,
    };
    // const param = {
    //   'tId': 1,
    //   'empId': 4,
    //   'aId': 565,
    // };
    console.log('param', param);
    this.contentService.getFgAssessmentDetails(param).then(
      res => {
        this.spinner.hide();
        const result = res;
        console.log('getFgAssessmentDetails', result);
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          let correctCount: any = 0;
          let wrongCount: any = 0;
          let pendingCount: any = 0;
          let scoreCount:any =0;
          this.fgAssessment = result['data'][0];
          console.log('Fg Assessment:', this.fgAssessment);
          if (this.fgAssessment.length == 0) {
            this.noQuizAttempted = true;
          } else {
            this.noQuizAttempted = false;
            for (let i = 0; i < this.fgAssessment.length; i++) {
              this.quizData.totalQue = this.fgAssessment.length;
              if(this.fgAssessment[i].questionFormatId != 4) {
              if (this.fgAssessment[i].answerCorrect == 2) {
                correctCount++;
              } else if (this.fgAssessment[i].answerCorrect == 1) {
                // pendingCount++;
                wrongCount++;
              } else if (this.fgAssessment[i].answerCorrect == 0) {
                wrongCount++;
              }
            }
              if (this.fgAssessment[i].questionFormatId == 1) {
                let str = new String(this.fgAssessment[i].answers);
                // const dataReplace = '/' + this.dataSeparatorService.Hash + '/g';
                // const qt1 = str.replace(dataReplace, '|');
                const qt1 = str.replace(/%hash%/g, '|');
                // const qt1 = qt1.replace(/%tild%/g, ',');
                this.fgAssessment[i].formattedAns = qt1;
              }
              if (this.fgAssessment[i].questionFormatId == 2 || this.fgAssessment[i].questionFormatId == 3) {
                const str = new String(this.fgAssessment[i].answers);
                // const dataReplace = '/' + this.dataSeparatorService.Hash + '/g';
                const qt2 = str.replace(/%hash%/g, '-');
                const str1 = new String(qt2);
                // const dataReplace2 = '/' + this.dataSeparatorService.Pipe + '/g';
                const qt21 = str1.replace(/%tild%/g, ',');
                this.fgAssessment[i].formattedAns = qt21;
              }
              if (this.fgAssessment[i].questionFormatId == 4) {
                this.fgAssessment[i].formattedAns = this.fgAssessment[i].answers;
              }
              scoreCount = scoreCount + this.fgAssessment[i].score;
            }
            this.quizData.correctCount = correctCount;
            this.quizData.wrongCount = wrongCount;
            this.quizData.pendingCount = pendingCount;
            this.quizData.scoreCount = scoreCount;
            this.showDetail = true;
            this.stepSelected = step.stepOrder;
          }
        }
      },
      error => {
        this.spinner.hide();
        this.presentToast('error', '');
      },
    );
  }


  getObserverDetail(empId, step) {
    console.log(empId, step);
  }
  numberOfCol() {

    for (let i = 0; i < this.nominees.length; i++) {
      this.nominees[i].finalWflength = this.nominees[i].finalWf.length;
      var totalCols = this.nominees[i].finalWflength;
      console.log("Final workflow steps", this.nominees[i].finalWf);
    }
    return 'repeat(1fr ,' + totalCols + ' )';
    // console.log("Nomineee Array", this.nominees);
  }

  formatDate(date) {
    // const d = new Date(date);
    console.log(date);
    date = date.split(" ");
    console.log(date);
    const formatted = date[0];
    // const formatted = this.datepipe.transform(d, 'yyyy-MM-dd');
    return String(formatted);
  }

  formatDateMonth(date) {
    const d = new Date(date);
    const formatted = this.datepipe.transform(d, 'dd-MMM-yyyy');
    return formatted;
  }

  formatTime(date) {
    // const d = new Date(date);
    console.log(date);
    date = date.split(" ");
    console.log(date);
    const formattedTime = date[1];
    console.log(formattedTime);

    const currentTime = formattedTime.split(":");

    var hours = currentTime[0];
    var minutes = currentTime[1];

    if (minutes < 10)
      minutes = "0" + minutes;

    var suffix = "AM";
    if (hours >= 12) {
      suffix = "PM";
      hours = hours - 12;
    }
    if (hours == 0) {
      hours = 12;
    }
    var current_time = hours + ":" + minutes + " " + suffix;
    console.log("date currenttime", current_time);
    // const formattedTime = this.datepipe.transform(t, 'h:mm:ss');
    return String(current_time);
  }

  insertingClickablePropertyInObject(nomArr) {
    for (const nomineedata of nomArr) {
      let count = 0;
      const arrayWF = nomineedata.finalWf;
      for (let i = 0; i < arrayWF.length; i++) {
        if (arrayWF[i].status != 1) {
          // if ((arrayWF[i].stepOrder >= 3) && (arrayWF[3].clickableType == true)) {
          //   arrayWF[i].clickableType = true;
          // }
          // else {
          arrayWF[i].clickableType = false;
          // }
        }
        else {
          count++;
          arrayWF[i].clickableType = true;
        }
        if (arrayWF.length > count) {
          arrayWF[count].clickableType = true;
        }
      }
    }
  }

  // /******************************* GET CERTIFICATE / step 12**********************************************/
  // CerPresent: boolean = false;
  // CertificateObject: any;
  // Certmsg: any;
  // getCertificateDetails(empId) {
  //     const url = webApi.domain + webApi.url.getCertificateLink + 'empId=' +
  //          empId + '&wfId=' + this.wfId + '&tId=' + this.loginUserdata.data.data.tenantId + '&type=app';
  //   console.log('url', url);
  //   this.contentService.getCertifiacteUrl(url).then(
  //     res => {
  //       this.spinner.hide();
  //       const result = res;
  //       console.log('getCertificateDetails', result);
  //       if (result['type'] === false) {
  //         this.presentToast('error', 'Certificate Details', 'Something went wrong.please try again later.');
  //       } else {
  //         if(result['data'].filePath)
  //         {
  //           this.CertificateObject = result['data'];
  //           this.CertificateObject.demopth = webApi.domain +  this.CertificateObject.filePath;
  //           if(this.CertificateObject.demopth){
  //             this.CertificateObject.finalPath = this.transform(this.getDocumentUrl(this.CertificateObject.demopth));
  //           }
  //           this.CerPresent = true;
  //         }else{
  //           this.Certmsg = result['data'];
  //           this.CerPresent = false;
  //         }

  //         this.showDetail = true;
  //         console.log('Certificate Details:', this.CertificateObject);
  //       }
  //     },
  //     error => {
  //       this.spinner.hide();
  //       this.presentToast('error', 'Certificate Details', 'Something went wrong.please try again later.');
  //     },
  //   );
  // }

  // transform(url) {
  //   return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  // }
  // getDocumentUrl(url) {
  //   return ('https://docs.google.com/gview?url=' + encodeURI(url) + '&embedded=true');
  // }
  clear() {
    if(this.searchText.length>=3){
    this.searchtext = "";
    this.getAllNominees();
    this.searchText=''
    }else{
    this.searchtext = "";
    }
  }
  SarchFilter($event, searchtext) {
    console.log(searchtext);
    const val = searchtext.toLowerCase();
    this.searchText=val;
    if(val.length>=3||val.length==0){
    const tempcc = this.nomineeslist.filter(function (d) {
      return String(d.name).toLowerCase().indexOf(val) !== -1 ||
        d.email.toLowerCase().indexOf(val) !== -1 ||
        d.phone.toLowerCase().indexOf(val) !== -1 ||
        d.ecn.toLowerCase().indexOf(val) !== -1 ||
        !val
    })
    console.log(tempcc);
    this.nominees = tempcc;
  }
  }
}

