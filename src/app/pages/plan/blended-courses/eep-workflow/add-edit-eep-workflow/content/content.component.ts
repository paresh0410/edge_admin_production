import { Component, OnInit, AfterViewInit, OnChanges, ChangeDetectorRef, Input, Output, EventEmitter } from "@angular/core";
// import { AddEditProgramBlendedService } from './../addEditProgramBlended.service';
import { ContentService } from "../content.service";
import { NgxSpinnerService } from "ngx-spinner";
// import { ToasterModule, ToasterService, Toast } from "angular2-toaster";
import { ToastrService } from 'ngx-toastr';
import {
  Router,
  NavigationStart,
  Routes,
  ActivatedRoute
} from "@angular/router";
import { BlendedService } from "../../../blended.service";
import { AddEditEepWorkflowComponent } from '../add-edit-eep-workflow.component';
import { noData } from "../../../../../../models/no-data.model";
//import { AddEditNominationComponent } from '../add-edit-nomination.component';

@Component({
  selector: "eep-content",
  templateUrl: "./content.component.html",
  styleUrls: ["./content.component.scss"]
})
export class ContentComponent implements OnInit, OnChanges, AfterViewInit {
  @Input('chnage') chnage;
  @Input('servicedata') servicedata;
  @Output() public passcrsId = new EventEmitter();
  modal: boolean;
  courses: any;
  name: string;
  id: number;
  badgeTitle:string='Select Course'
  btnName:string='Select'
  contentDataArray: any = [];
  contentArray: any = [];
  callArray: any = [];

  allCourse: any = [];

  tenantId: any;
  courseId: any;
  wfId: any = 0;
  userId: any;
  condataCourse: any;
  couresshow: boolean = false;
  courseSelected: boolean = false;
  nominationdata: any = [];
  searchtext: any;
  coursesCC: any = [];
  temp: any = [];
  nodata: boolean=false;
  noDataVal:noData={
    margin:'mt-5',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"Sorry we couldn't find any matches please try again",
    desc:".",
    titleShow:true,
    btnShow:false,
    descShow:false,
    btnText:'Learn More',
    btnLink:''
}
  constructor(
    private contentService: ContentService,
    private spinner: NgxSpinnerService,
    private router: Router,
    public routes: ActivatedRoute,
    // private toasterService: ToasterService,
    private toastr: ToastrService,
    private BlendedService: BlendedService,
    public AddEditEepWorkflowComponent: AddEditEepWorkflowComponent,
    public cdf: ChangeDetectorRef,
  ) {
    if (this.BlendedService.program) {
      this.nominationdata = this.BlendedService.program;
      // this.tenantId = this.nominationdata.tenantId;
      this.courseId = this.nominationdata.eCsrId;
      this.wfId = this.BlendedService.wfId;
    }

    if (localStorage.getItem("LoginResData")) {
      var userData = JSON.parse(localStorage.getItem("LoginResData"));
      console.log("userData", userData.data);
      this.userId = userData.data.data.id;
      this.tenantId = userData.data.data.tenantId;
    }
    // this.getAllCoursesCC();
    // this.getAllEnrolCourseModules();
    //this.ActiveTab = this.contentArray[0].id;
    //this.contentDataArray = this.contentArray[0].list;
  }

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }

  ngOnChanges() {
    console.log(this.servicedata);
    console.log("changes", this.chnage);
    if (this.BlendedService.program) {
      this.nominationdata = this.BlendedService.program;
      this.tenantId = this.tenantId;
      this.courseId = this.nominationdata.eCsrId;
      this.wfId = this.BlendedService.wfId;
    }
    console.log(this.courseId);
    this.getAllCoursesCC();
  }
  getAllCoursesCC() {
    this.spinner.show();
    const param = {
      tId: this.tenantId,
    };
    this.contentService.getAllCoursesCC(param).then(
      rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log("getAllEmployessCC:", rescompData);
        this.spinner.hide();
        if (result["type"] == true) {
          if (result["data"][0].length == 0) {
            // this.noEmployees = true;
      this.nodata=true
          } else {
            this.temp = result["data"][0]
            this.coursesCC = this.temp;
      this.nodata=false
            console.log("this.coursesCC", this.coursesCC);
            // this.participantsservice.allEmployess = this.allEmployees;
            // console.log('this.participantsservice.getAllEmployees',this.participantsservice.getAllEmployees);
          }
        } else {
          // var toast: Toast = {
          //   type: "error",
          //   //title: "Server Error!",
          //   body: "Something went wrong.please try again later.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }
      },
      error => {
        this.spinner.hide();
        // var toast: Toast = {
        //   type: "error",
        //   //title: "Server Error!",
        //   body: "Something went wrong.please try again later.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      }
    );
  }

  ngOnInit() {
    if (this.BlendedService.program) {
      this.nominationdata = this.BlendedService.program;
      this.tenantId = this.tenantId;
      this.courseId = this.nominationdata.eCsrId;
      this.wfId = this.BlendedService.wfId;
    }
    if (this.nominationdata.eCsrId) {
      this.courseId = this.nominationdata.eCsrId;
    } else {
      this.courseId = this.BlendedService.crsId;
    }
    this.getAllCoursesCC();
    this.getAllEnrolCourseModules();
    // this.modal = false;
  }
  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    this.BlendedService.program
    this.getAllCoursesCC();
  }
  openModal() {
    this.couresshow = true;
  }

  closeModal() {
    this.couresshow = false;
  }

  selectItem(item) {
    if (!item.isSelected) {
      item.isSelected = true;
    } else {
      item.isSelected = false;
    }
  }

  // addSelected(){
  //   // this.allCourse.forEach(obj => {

  //   // });
  // }

  activeSelectedCourseId: any;
  setActiveSelectedCourse(currentIndex, currentCourse) {
    console.log("currentCourseOld:", currentCourse);

    this.condataCourse = {};
    this.activeSelectedCourseId = currentCourse.courseId;
    this.condataCourse = currentCourse;
    console.log("currentCourseNew:", this.condataCourse);
  }
  courseCCObj: any = {};
  saveCourse() {
    this.spinner.show();
    this.courseCCObj = {
      courseId: this.condataCourse.courseId,
      coursePicRef: this.condataCourse.coursePicRef,
      fullname: this.condataCourse.fullname,
      summary: this.condataCourse.summary
    };
    console.log("Selected Course:", this.condataCourse);
    // this.badge = false;

    let param = {
      courseId: this.condataCourse.courseId,
      wfId: this.BlendedService.wfId,
      //    this.BlendedService.program ?  this.BlendedService.program.nId : 0,
      tId: this.tenantId,
    };
    console.log("param:", param);
    this.contentService.enrollCourseForCCContent(param).then(
      rescompData => {
        this.spinner.hide();
        var res = rescompData;
        console.log("Selected Course:", res);
        if (res["type"] == true) {
          this.presentToast('success', 'Course enrolled to course.');
          this.courseSelected = true;
          this.getAllEnrolCourseModules();
          this.couresshow = false;
          this.passcrsId.emit(param.courseId);
          this.BlendedService.program.eCsrId = param.courseId;
          // const DataTab={
          //   tabTitle:'Steps',
          // }
          // try{
          //   this.AddEditEepWorkflowComponent.selectedTab(DataTab);
          // }catch(e)
          // {
          //   // console.log('course result is wrong',this.submitRes)
          //   // this.blendedService.wfId =undefined;
          // }
          // this.router.navigate(['../'], { relativeTo: this.routes });
          this.cdf.detectChanges();
        } else {
          // var toast: Toast = {
          //   type: "error",
          //   body: "Something went wrong.please try again later.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', 'Please try again');
        }
      },
      resUserError => {
        this.spinner.hide();
        //this.errorMsg = resUserError;
        // var toast: Toast = {
        //   type: "error",
        //   body: "Something went wrong.please try again later.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', 'Please try again');
      }
    );
  }

  courseAvailable: boolean = false;
  enrolId: any;
  courseData: any = {};
  getAllEnrolCourseModules() {
    const param = {
      courseId: this.condataCourse ? this.condataCourse.courseId : this.courseId,
      wfId: this.BlendedService.wfId,
      //   this.BlendedService.program ?  this.BlendedService.program.nId : 0,
      tId: this.tenantId,
    };

    this.contentService.getAllEnrolCourseModule(param).then(
      rescompData => {
        this.spinner.hide();

        const res = rescompData;
        console.log("ModulesCC:", res);
        if (res["type"] == true) {
          const Finres: any = res;
          if (Finres.data.length == 0) {
            this.courseAvailable = false;
          } else {
            this.courseAvailable = true;
            this.contentArray = Finres.data;
            // for (var i = 0; i < this.contentArray.length; i++) {
            //   var tempModule: any = i + 1;
            //   this.contentArray[i].id = i;
            //   this.contentArray[i].moduleName = 'Module' + ' ' + tempModule;
            // }
            this.ActiveTab = this.contentArray[0].id;
            this.contentDataArray = this.contentArray[0].list;
            this.enrolId = this.contentDataArray[0].enrolId;
            console.log("enrolId", this.enrolId);
            console.log("this.contentDataArray", this.contentDataArray);
            for (var i = 0; i < this.contentDataArray.length; i++) {
              const tempActivity: any = i + 1;
              this.contentDataArray[i].img = "assets/images/open-book-leaf.jpg";
              this.contentDataArray[i].activityName = 'Activity' + ' ' + tempActivity;
            }
            console.log("CD===>", this.contentArray);
            this.courseData = this.contentArray[0].list[0];
            console.log('this.courseData', this.courseData);
            // var enrolId = this.contentArray.find(function (element){
            // 	return element.enrolId
            // });
            // console.log('enrolId',enrolId);
            this.spinner.hide();
            console.log("ModulesCC:", res);
            console.log("currentCourseNew:", this.condataCourse);
          }
        } else {
          // var toast: Toast = {
          //   type: "error",
          //   body: "Something went wrong.please try again later.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }
      },
      resUserError => {
        this.spinner.hide();
        //this.errorMsg = resUserError;
        // var toast: Toast = {
        //   type: "error",
        //   body: "Something went wrong.please try again later.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      }
    );
  }
  ActiveTab: any;

  // TabSwitcher(){
  //   this.AddEditNominationComponent.tabEnabler.codeOfConduct = false;
  //   this.AddEditNominationComponent.tabEnabler.content = false;
  //   this.AddEditNominationComponent.tabEnabler.enrolTab = false;
  //   this.AddEditNominationComponent.tabEnabler.nomineeTab = false;
  //   this.AddEditNominationComponent.tabEnabler.detailsTab = false;
  //   this.AddEditNominationComponent.tabEnabler.steps = true;
  //   console.log(this.AddEditNominationComponent);
  // }

  tabChanged(data) {
    this.spinner.show();
    console.log("DATA--->", data);
    this.ActiveTab = data.id;
    this.contentDataArray = this.contentArray[data.id].list;
    for (var i = 0; i < this.contentDataArray.length; i++) {
      var temp: any = i + 1;
      this.contentDataArray[i].img = "assets/images/open-book-leaf.jpg";
      this.contentDataArray[i].activityName = "Activity" + " " + temp;
    }
    this.spinner.hide();
  }

  romoveCourseModules(data) {
    console.log("data", data);
    let param = {
      tId: this.tenantId,
      wfId: this.BlendedService.wfId,
      //   this.BlendedService.program ?  this.BlendedService.program.nId : 0,
      // "aId":20,
      // "calId":this.callId,
      // "empId":this.empId
    };
    this.contentService.removeEnrolCourse(param).then(
      rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log("Remove Course Result", rescompData);
        if (result["type"] == true) {
          // var toast: Toast = {
          //   type: "success",
          //   //title: "Server Error!",
          //   body: "Course removed successfully.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('success', 'Course removed.');
          this.enrolId = null;
          this.courseAvailable = false;
          this.passcrsId.emit('');
          this.BlendedService.program.eCsrId = null;
        } else {
          // var toast: Toast = {
          //   type: "error",
          //   //title: "Server Error!",
          //   body: "Something went wrong.please try again later.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }
      },
      error => {
        this.spinner.hide();
        // var toast: Toast = {
        //   type: "error",
        //   //title: "Server Error!",
        //   body: "Something went wrong.please try again later.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      }
    );
  }

  goToCourseDetails(data) {
    this.router.navigate(["content/course-details"], {
      relativeTo: this.routes
    });
  }

  clear() {
    if(this.searchtext.length>=3){
      this.searchtext = "";
      this.getAllCoursesCC();
      this.nodata=false
    }
    else{
      this.searchtext = "";
    }

  }
  SarchFilter($event, searchtext) {
    console.log(searchtext);
    const val = searchtext.toLowerCase();
    this.searchtext=val;
    if(val.length>=3||val.length==0){
      this.nodata=false
    const tempcc = this.temp.filter(function (d) {
      return String(d.fullname).toLowerCase().indexOf(val) !== -1 ||
        // d['Valid Till'].toLowerCase().indexOf(val) !== -1 ||
        // d.Category.toLowerCase().indexOf(val) !== -1 ||
        // d.Mode.toLowerCase().indexOf(val) !== -1 ||
        !val
    })
    console.log(tempcc);
    this.coursesCC = tempcc;
    if(!tempcc.length){
      this.nodata=true
    }
  }
  }
  // ngOnDestroy()
  // {
  //   this.courseId ='';
  // }
}
