import { Component, OnInit, AfterViewInit, ChangeDetectorRef, Input, OnChanges, SimpleChanges } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ContentService } from './../content.service';
import { DatePipe } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { ToastrService } from 'ngx-toastr';
import { BlendedService } from '../../../blended.service';
import { AddEditEepWorkflowComponent } from '../add-edit-eep-workflow.component';
import * as _moment from 'moment';
import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';

const moment = (_moment as any).default ? (_moment as any).default : _moment;
export const MY_CUSTOM_FORMATS = {
  fullPickerInput: 'DD-MM-YYYY HH:mm:ss',
  parseInput: 'DD-MM-YYYY HH:mm:ss',
  datePickerInput: 'DD-MM-YYYY',
  timePickerInput: 'LT',
  monthYearLabel: 'MMM YYYY',
  dateA11yLabel: 'LL',
  monthYearA11yLabel: 'MMMM YYYY'
};
@Component({
  selector: 'eep-steps',
  templateUrl: './steps.component.html',
  styleUrls: ['./steps.component.scss'],
  providers: [DatePipe,
    { provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE] },
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS }]
})
export class StepsComponent implements OnInit, AfterViewInit {

  @Input('chnage') chnage;
  @Input() servicedata: any;
  @Input() inpdata: any;
  loginUserdata: any;
  serviceData: any = [];
  wfId: any;
  eCsrId: any;
  todatDate = new Date();
  str: string;
  act: any;
  constructor(private contentService: ContentService, private datepipe: DatePipe, private spinner: NgxSpinnerService,
    // private toasterService: ToasterService, 
    private toastr: ToastrService,
    public router: Router, public routes: ActivatedRoute,
    private blendedService: BlendedService,
    public AddEditEepWorkflowComponent: AddEditEepWorkflowComponent,
    public cdf: ChangeDetectorRef,
  ) {
    

    if (localStorage.getItem('LoginResData')) {
      this.loginUserdata = JSON.parse(localStorage.getItem('LoginResData'));
    }

    if (this.blendedService.program) {
      this.serviceData = this.blendedService.program;
    }
    // console.log('this.serviceData', this.serviceData);
    if (this.blendedService.wfId) {
      this.wfId = this.blendedService.wfId;
      console.log('coc wfId', this.wfId);
    }
    // if(this.serviceData) {
    //   this.getCourseActivities();
    //   this.getWorkflowSteps();
    // }
    console.log('lastEndDate', this.lastEndDate);
  }

  ngOnInit() {
    if (this.blendedService.program) {
      this.serviceData = this.blendedService.program;
    }
    if (this.blendedService.date) {
      const vdate = this.blendedService.date;
      this.lastEndDate = new Date(vdate.sDate);
      this.maxdateto = new Date(vdate.eDate);
    } else {
      this.lastEndDate = new Date(this.serviceData.sDateF);
      this.maxdateto = new Date(this.serviceData.eDateF);
    }
    this.todatDate.setDate(this.lastEndDate.getDate() - 1);
    console.log(this.blendedService.date != '' || this.blendedService.date != null);
    if (this.serviceData.eCsrId) {
      this.eCsrId = this.serviceData.eCsrId;
    } else {
      this.eCsrId = this.blendedService.crsId;
    }
    // console.log('this.serviceData', this.serviceData);
    if (this.blendedService.wfId) {
      this.wfId = this.blendedService.wfId;
      console.log('coc wfId', this.wfId);
    }
    // this.getCourseActivities();
    this.getWorkflowSteps();
  }
  ngOnChanges(changes: SimpleChanges): void {
    console.log("changes", this.chnage);
    // this.getCourseActivities();
    if(this.inpdata === 'steps') {
		  this.onSubmit();
		}
    this.getWorkflowSteps();
    // this.getCourseActivities();
    
  }

  ngAfterViewInit() {
    // if (this.serviceData.eCsrId) {
    //   this.getCourseActivities();
    //   this.getWorkflowSteps();
    // } else {
    //   this.presentToast('warning', 'Please select a content');
    // }
    console.log(this.serviceData);
    if (this.serviceData.eCsrId) {
      this.eCsrId = this.serviceData.eCsrId;
    } else {
      this.eCsrId = this.blendedService.crsId;
    }
    this.getCourseActivities();
    // this.getWorkflowSteps();
  }

  getCourseActivities() {
    this.spinner.show();
    const param = {
      'courseId': this.eCsrId,
      'tId': this.loginUserdata.data.data.tenantId,
    };
    console.log('getCourseActivitiesParam', param);
    this.contentService.getCourseActivities(param)
      .then(res => {
        this.spinner.hide();
        const result = res;
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          console.log('courseActivities:', result['data'][0]);
          this.activities = result['data'][0];
          if (this.chnage == true) {
            if (!this.activities.length) {
              this.cnt=0
              this.presentToast('warning', 'Please select a content');
               this.formInvalid = true;
            }
          }
        }
      },
        error => {
          this.spinner.hide();
          this.presentToast('error', '');
        });
  }

  getWorkflowSteps() {
    this.spinner.show();
    const param = {
      'wfId': this.blendedService.wfId,
      'tId': this.loginUserdata.data.data.tenantId,
    };

    this.contentService.getAllWorkflowSteps(param)
      .then(res => {
        this.spinner.hide();
        const result = res;
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          console.log('steps:', result['data']);
          this.steps = result['data'];
        }
      },
        error => {
          this.spinner.hide();
          this.presentToast('error', '');
        });
  }

  stepForm: NgForm;

  // activityDropDown = [ "Assessment", "adbfkja", "dmfkjandslkjn", "ajkdjsnfkjcndsa"]
  activities: any = [];

  steps: any = [];

  onChecked(event: Event, i) {
    let data = (<HTMLInputElement>event.target).checked;
    console.log(data);
    this.steps[i]["endDate"] ='';
    this.steps[i]['weightage'] ='';
    this.steps[i]['startDate'] ='';
    this.steps[i]['activityId'] ='';

    // if (this.SelectValue.length === 0  || !data) {
    //   this.reviewFormData.ReviewText = false;
    // } else {
    //   this.reviewFormData.ReviewText = true;

    // }
  }

  minDateto: Date;
  lastEndDate: Date = new Date();
  maxdateto: Date = new Date();
  onSearchChange(data) {
    this.minDateto = data;
  }

  greaterThanHundred: boolean = false;
  inputValue: number;
  getWeightage(event: any, stepForm: NgForm) {
    if (event.target.value < 0) {
      event.target.value = 0;
    }
    if (event.target.value > 100) {
      event.target.value = 100;
    }
  }

  formInvalid: boolean = false;
  cnt: number = 0;
  weightNotMatch: boolean = false;
  checkedSteps: any = [];
  onSubmit() {
    this.checkedSteps=[];
    this.formInvalid = false;
    this.spinner.show();
    console.log('this.cntOnSubmit', this.cnt);
    console.log(this.steps);
    this.steps.forEach(element => {
      if (element.checked) {
        element.startDate = this.formatDate(element.startDate);
        element.endDate = this.formatDate(element.endDate);
        this.checkedSteps.push(element);
      }
    });
    console.log("this.checksteps",this.checkedSteps)
    this.checkedSteps.forEach(element=> {
      this.cnt = this.cnt + element.weightage;
      // if(element.isActivityEnable==1&&(element.activityId==0||element.activityId==null)){
      //   this.formInvalid = true;
      // }
      if (!element.startDate || !element.endDate||(element.isActivityEnable==1&&!element.activityId) ){
        this.formInvalid = true;
    }
  
    });
    console.log('cnt', this.cnt);   
        const stepsStr = this.prepareData(this.checkedSteps);
        console.log('checkedSteps', this.checkedSteps);
        if(!this.formInvalid){
      if (this.cnt == 100) {
        const param = {
          'wfId': this.blendedService.wfId,
          'steps': stepsStr,
          'tId': this.loginUserdata.data.data.tenantId,
          'ucrId': this.loginUserdata.data.data.id,
        };
        console.log('stepsParam', param);
        this.contentService.insertWorkflowSteps(param)
          .then(res => {
            this.spinner.hide();
            const result = res;
            if (result['type'] === false) {
              this.presentToast('error', '');
            } else {
              var msg =res['data'][0].msg;
              if (res['data'][0].status == 0)
              {
                this.presentToast('warning', msg);
              }else{
                this.presentToast('success', msg);
                // this.router.navigate(['pages/learning/blended-home/nomination-instance']);
                // this.TabSwitcher();
                const DataTab = {
                  tabTitle: 'Enrol',
                }
                try {
                  this.AddEditEepWorkflowComponent.selectedTab(DataTab);
                } catch (e) {
                  // console.log('course result is wrong',this.submitRes)
                  // this.blendedService.wfId =undefined; 
                }
              }
            
              this.cdf.detectChanges();
            }
          },
            error => {
              this.spinner.hide();
              this.presentToast('error', '');
            });

      } else {
        this.spinner.hide();
        this.weightNotMatch = true;
      }
    }
  // }
  else {
      this.spinner.hide();
      this.cnt=0;
      this.toastr.warning('Please enter mandetory Data', 'Warning', {
        closeButton: false
      });
    }
  }

  // TabSwitcher(){
  //   this.AddEditNominationComponent.tabEnabler.steps = false;
  //   this.AddEditNominationComponent.tabEnabler.enrolTab = true;
  //   this.AddEditNominationComponent.tabEnabler.codeOfConduct = false;
  //   this.AddEditNominationComponent.tabEnabler.content = false;
  //   this.AddEditNominationComponent.tabEnabler.nomineeTab = false;
  //   this.AddEditNominationComponent.tabEnabler.detailsTab = false;
  //   console.log(this.AddEditNominationComponent);
  // }

  prepareData(steps) {
    let finalStr: any;
    this.str='';
    console.log('prepareDatacheckedSteps', this.checkedSteps);
    for (let i = 0; i < this.checkedSteps.length; i++) {
      this.act = this.checkedSteps[i].activityId == '' || this.checkedSteps[i].activityId == null ?
        0 : this.checkedSteps[i].activityId;
       this.str = this.checkedSteps[i].id + '#' + this.checkedSteps[i].weightage + '#' + this.checkedSteps[i].startDate
        + '#' + this.checkedSteps[i].endDate + '#' + this.act + '#' + (this.checkedSteps[i].checked ? 1 : 0);

      if (i === 0) {
        finalStr = this.str;
      } else {
        finalStr = finalStr + '|' + this.str;
      }
    }
    console.log('finalStr', finalStr);
    return finalStr;
  }

  formatDate(date) {
    if (date) {
      const d = new Date(date);
      const formatted = this.datepipe.transform(d, 'yyyy-MM-dd');
      return formatted;
    }
    else {
      return date;
    }
  }

  closeModal() {
    this.weightNotMatch = false;
    this.cnt = 0;
    console.log('cntClose', this.cnt);
    this.checkedSteps = [];
  }

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }
}
