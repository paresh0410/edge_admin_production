import {
  Component, OnInit, AfterViewInit, ViewChild, ViewEncapsulation, ChangeDetectorRef, Input,
  Output, EventEmitter, OnChanges, SimpleChanges
} from '@angular/core';
import { BsDatepickerDirective } from 'ngx-bootstrap/datepicker';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { DatePipe } from '@angular/common';
import { BlendedService } from '../../../blended.service';
import { ToastrService } from 'ngx-toastr';
//import { EepWorkflowComponent } from '../add-edit-eep-workflow.component';
import {
  NG_VALIDATORS,
  Validator,
  Validators,
  AbstractControl,
  ValidatorFn
} from '@angular/forms';
import { NgForm } from '@angular/forms';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { AddEditEepWorkflowComponent } from '../add-edit-eep-workflow.component';
import { dataReady } from '@syncfusion/ej2-grids';
//import { AddEditNominationComponent } from '../add-edit-nomination.component';
import * as _moment from 'moment';
import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';
import { NgxSpinnerService } from 'ngx-spinner';

const moment = (_moment as any).default ? (_moment as any).default : _moment;
export const MY_CUSTOM_FORMATS = {
  fullPickerInput: 'DD-MM-YYYY HH:mm:ss',
  parseInput: 'DD-MM-YYYY HH:mm:ss',
  datePickerInput: 'DD-MM-YYYY',
  timePickerInput: 'LT',
  monthYearLabel: 'MMM YYYY',
  dateA11yLabel: 'LL',
  monthYearA11yLabel: 'MMMM YYYY'
};
@Component({
  selector: 'eep-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
  encapsulation: ViewEncapsulation.None,
  // providers: [DatePipe]
  providers: [
    { provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE] },
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS }]
})
export class DetailsComponent implements OnInit, AfterViewInit {
  @ViewChild(NgForm) nominationForm: NgForm;
  @Input() servicedata: any;
  @Output() public passWfId = new EventEmitter();
  @Input() inpdata: any;
  @ViewChild("nominationForm") formval: any;

  nominationData: any = {
    instName: '',
    pId: '',
    visible: '',
    sDate: '',
    eDate: '',
    nDesc: '',
    budget: '',
    instituteId: '',
    distributionPartnerId: '',
    seatCost: '',
  };
  minDateto: Date;
  programs: any = [];

  visibilityOptions: any = [];
  currentUser: any = [];
  serviceData: any ;
  wfId: any;
  distribution_partner: any = [];
  institute_name: any = [];
  mdate = new Date();
  submitRes: any;
  errorMsg: string;
  Id: string;
  constructor(
    private blendedService: BlendedService,
    private toasterService: ToastrService,
    private toastr: ToastrService,
    public router: Router,
    public routes: ActivatedRoute,
    private http1: HttpClient,
    public AddEditEepWorkflowComponent: AddEditEepWorkflowComponent,
    public cdf: ChangeDetectorRef,
    private spinner: NgxSpinnerService,
  ) {
    // console.log("current user" ,this.currentUser)
  }

  ngOnInit() {
    // this.mdate.setDate(this.mdate.getDate()-1);
    // if(this.mdate < this.nominationData.sdate){
    //   this.mdate.setDate(this.mdate.getDate()-1)
    // }


    console.log(this.mdate);
    this.currentUser = JSON.parse(localStorage.getItem('LoginResData'));
    this.serviceData = this.blendedService.program ? this.blendedService.program : {};
    console.log(this.serviceData,"service Data")
    if (this.serviceData === undefined || this.serviceData === '') {
      this.nominationData = {
        instName: '',
        pId: '',
        visible: '',
        sDate: '',
        eDate: '',
        nDesc: '',
        budget: '',
        instituteId: '',
        distributionPartnerId: '',
        seatCost: '',
      };
      // this.mdate.setDate(this.mdate.getDate()-1);
    } else {
      this.nominationData = {
        instName: this.serviceData.instName,
        pId: this.serviceData.pId,
        visible: this.serviceData.visible,
        sDate: this.serviceData.sDateF,
        eDate: this.serviceData.eDateF,
        nDesc: this.serviceData.nDesc,
        budget: this.serviceData.budget,
        instituteId: this.serviceData.instituteId,
        distributionPartnerId: this.serviceData.distributionPartnerId,
        seatCost: this.serviceData.seatCost,

      };
      // this.mdate.setDate(this.nominationData.sDate);
      // if(this.mdate < this.nominationData.sDate){
      //   // this.mdate.setDate(new Date (this.nominationData.sDate));
      //   this.mdate.setDate(this.nominationData.sDate);
      // }
      this.blendedService.date ={
        sDate: this.formatSendDateTime(this.nominationData.sDate),
        eDate: this.formatSendDateTime(this.nominationData.eDate),
      }
      // let currentDate = new Date();
      // currentDate.setDate(currentDate.getDate() - 1);
      if( this.formatSendDateTime(this.mdate) >  this.formatSendDateTime(this.nominationData.sDate)){
        // this.mdate.setDate(new Date (this.nominationData.sDate));
        this.mdate.setDate(this.nominationData.sDate - 1);
      } else {
        this.mdate.setDate(this.mdate.getDate()-1);
      }

    }

    console.log(this.nominationData);
    this.getDropdownData();
    this.getHelpContent();
    this.getbisnesData();
  }

  ngOnChanges(changes: SimpleChanges): void {
		if(this.inpdata === 'details') {
		  this.onSubmit(this.nominationData);
		}
	}

  ngAfterViewInit() {
  }
  getDropdownData() {
    const data = {
      // wfId: nId,
      tId: this.currentUser.data.data.tenantId,
    };
    console.log(data);
    // this.spinner.show();
    this.blendedService.EEPgetnominationDropdown(data).then(
      rescompData => {
        console.log('dropdown data', rescompData);
        // this.spinner.hide();
        this.programs = rescompData['data'][1];
        this.visibilityOptions = rescompData['data'][0];
        this.institute_name = rescompData['data'][2];
        this.distribution_partner = rescompData['data'][3];
        console.log('Program data', this.programs);
        // this.loader = false;
      },
      resUserError => {
        // this.spinner.hide();
        this.errorMsg = resUserError;
      }
    );
  }

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }

  disableSubmit: boolean = false;

  onSubmit(data) {
    var Id
    console.log('form data >> ', data);
    if(!this.formval.form.valid) {
      Object.keys(this.formval.controls).forEach(key => {
        console.log('this.formval.controls[key]', this.formval.controls[key]);
        this.formval.controls[key].markAsDirty();
      });
      setTimeout(() => {
        this.toasterService.warning('Please fill required details',"Warning")      
      },0)
      return;
    } else {
    if (this.serviceData) {
      var nId = this.serviceData.nId;

    }
    else if(this.Id != ''){
      nId = this.Id

    } else {
      nId = null;
    }
    const details = {
      nId: nId,
      pId: data.pId,
      instName: data.instName,
      nDesc: data.nDesc,
      sDate: this.formatSendDateTime(data.sDate),
      eDate: this.formatSendDateTime(data.eDate),
      visible: data.visible,
      budgetData: data.budget,
      instiId: data.instituteId,
      partnerId: data.distributionPartnerId,
      seatCostData: data.seatCost,
      tId: this.currentUser.data.data.tenantId,
      ucrId: this.currentUser.data.data.id,
    };
    this.disableSubmit = true;
    this.spinner.show();
    this.cdf.detectChanges();
    // if (!details.nId) {
    //   details.nId = '';
    // }
    // this.nominationForm.resetForm();

    console.log(details);
    this.blendedService.EEPgetAddEditInstance(details).then(
      rescompData => {
        console.log(rescompData);

        // this.spinner.hide();
        this.disableSubmit = false;
        this.spinner.hide();
        this.cdf.detectChanges();
        if(rescompData && rescompData['data'] && rescompData['data'].length !=0){
          this.submitRes = rescompData;
          this.serviceData['nId']= rescompData['data'][0].instanceId;
          this.Id = this.submitRes.data[0].instanceId;
        }
        console.log(this.serviceData.nId,"nId")
        if (this.submitRes.type === false) {
          // const catUpdate: Toast = {
          //   type: 'error',
          //   title: 'EEP Workflow',
          //   body: this.submitRes.data[0].msg,
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
          console.log();

        } else {
          // const catUpdate: Toast = {
          //   type: 'success',
          //   title: 'EEP Workflow',
          //   body: this.submitRes.data[0].msg,
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('success', this.submitRes.data[0].msg);
          // this.nominationData = {
          //   instName: '',
          //   pId: '',
          //   visible: '',
          //   sDate: '',
          //   eDate: '',
          //   nDesc: '',
          //   budget: '',
          //   instituteId: '',
          //   distributionPartnerId: '',
          //   seatCost: '',
          // };
          // this.blendedService.wfId = this.submitRes.data[0].instanceId ? this.submitRes.data[0].instanceId : this.blendedService.wfId;

          const DataTab = {
            tabTitle: 'Program Structure',
          }
          try {
            this.blendedService.wfId = this.submitRes.data[0].instanceId ?
              this.submitRes.data[0].instanceId : this.serviceData.nId;
              this.serviceData['nId'] =this.blendedService.wfId;
            this.onBuisnessSubmit(this.blendedService.wfId);
            this.AddEditEepWorkflowComponent.selectedTab(DataTab);
            const data1 = {
              wfId: this.blendedService.wfId,
              sDate: this.formatSendDateTime(data.sDate),
              eDate: this.formatSendDateTime(data.eDate),
            }
            this.passWfId.emit(data1);
            this.blendedService.date = {
              sDate: data.sDate,
              eDate: data.eDate
            }
          } catch (e) {
            console.log('course result is wrong', this.submitRes)
            this.blendedService.wfId = undefined;
          }
          this.cdf.detectChanges();
        }
      },
      resUserError => {
        this.disableSubmit = false;
        this.spinner.hide();
        this.cdf.detectChanges();
        // this.spinner.hide();
        if (resUserError.statusText === 'Unauthorized') {
          // this.router.navigate(['/login']);
        }
        this.errorMsg = resUserError;
      }
    );
    }
  }
  buisnessData: any = [];
  getbisnesData() {
    const data = {
      wfId: this.serviceData ? this.serviceData.nId : 0,
      tId: this.currentUser.data.data.tenantId,
    };
    console.log(data);
    // this.spinner.show();
    this.blendedService.getallEEPbuisnessbreak(data).then(
      rescompData => {
        console.log('Buisness data', rescompData);
        // this.spinner.hide();
        //   this.programs = rescompData['data'][1];
        try {
          this.buisnessData = rescompData['data'];
          console.log('this.buisnessData', this.buisnessData);
        } catch (e) {
          this.buisnessData = [];
        }


        // this.loader = false;
      },
      resUserError => {
        // this.spinner.hide();
        this.errorMsg = resUserError;
      }
    );
  }


  onBuisnessSubmit(nid) {
    let tempData = [];
    for (let i = 0; i < this.buisnessData.length; i++) {
      const item = {
        buisnessId: this.buisnessData[i].LOVValue,
        praposedSeats: this.buisnessData[i].praposedSeats,
        adjustSeats: this.buisnessData[i].adjustSeats,
      }
      tempData.push(item)
    }
    const option: string = Array.prototype.map.call(tempData, function (item) {
      console.log('item', item)
      return Object.values(item).join('#');
    }).join('|');
    const details = {
      wfId: nid,
      sections: option,
      tId: this.currentUser.data.data.tenantId,
      ucrId: this.currentUser.data.data.id,
    };
    // if (!details.nId) {
    //   details.nId = '';
    // }
    // this.nominationForm.resetForm();

    console.log(details);
    this.blendedService.addeditEEPbuisnessbreak(details).then(
      rescompData => {
        console.log(rescompData);
        // this.spinner.hide();
        this.submitRes = rescompData;
        if (this.submitRes.type === false) {
          // const catUpdate: Toast = {
          //   type: 'error',
          //   title: 'EEP Details',
          //   body: this.submitRes.data[0].msg,
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          // this.toasterService.pop(catUpdate);
          // this.presentToast('error', '');
          console.log();

        } else {
          // const catUpdate: Toast = {
          //   type: 'success',
          //   title: 'EEP Details',
          //   body: this.submitRes.data[0].msg,
          //   showCloseButton: true,
          //   timeout: 4000,
          // };
          // this.toasterService.pop(catUpdate);
          // this.presentToast('success', this.submitRes.data[0].msg);
        }
      },
      resUserError => {
        // this.spinner.hide();
        if (resUserError.statusText === 'Unauthorized') {
          // this.router.navigate(['/login']);
        }
        this.errorMsg = resUserError;
      }
    );
  }

  onSearchChange(data) {
    this.minDateto = new Date(data);
    this.minDateto.setDate(this.minDateto.getDate() + 1);
    console.log(this.minDateto);
    if (this.nominationData.eDate <= this.nominationData.sDate) {
      this.nominationData.eDate = '';
    }
  }

  formatSendDateTime(Dtime) {
    const t = new Date(Dtime);
    // const formattedDateTime = this.datepipe.transform(t, 'yyyy-MM-dd h:mm');
    const year = t.getFullYear() + '-' + (t.getMonth() + 1) + '-' + t.getDate() + ' ' + '00:00:00';
    // const time = ' ' + t.getHours() + ':' + t.getMinutes();
    const formattedDateTime = year;
    // + time;
    return formattedDateTime;
  }

  // Help Code Start Here //

  helpContent: any;
  getHelpContent() {
    return new Promise(resolve => {
      this.http1
        .get(
          '../../../../../../../assets/help-content/addEditCourseContent.json'
        )
        .subscribe(
          data => {
            this.helpContent = data;
            console.log('Help Array', this.helpContent);
          },
          err => {
            resolve('err');
          }
        );
    });
    // return this.helpContent;
  }

  // Help Code Ends Here //
}
