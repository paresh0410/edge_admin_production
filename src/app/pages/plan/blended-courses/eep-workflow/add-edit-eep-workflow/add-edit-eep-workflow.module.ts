import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CalendarModule, DateAdapter } from 'angular-calendar';

import { StarRatingModule } from 'angular-star-rating';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { NgxEchartsModule } from 'ngx-echarts';
import { ThemeModule } from '../../../../../@theme/theme.module';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { MyDatePickerModule } from 'mydatepicker';
import { FilterPipeModule  } from 'ngx-filter-pipe';
import { TabsModule } from 'ngx-tabs';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { TranslateModule } from '@ngx-translate/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NbTabsetModule } from '@nebular/theme';
import { TagInputModule } from 'ngx-chips';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { AddEditEepWorkflowComponent } from './add-edit-eep-workflow.component';
import { DetailsComponent } from './details/details.component';
// import { NomineesComponent } from './nominees/nominees.component';
// import { CalendarComponent } from './calendar/calendar.component';
import { ContentComponent } from './content/content.component';
import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NomineeDetailsComponent } from './nominee-details/nominee-details.component';
import { NomineesComponent } from './nominees/nominees.component';
import { CodeOfConductComponent } from './code-of-conduct/code-of-conduct.component';
import { StepsComponent } from './steps/steps.component';
import { EnrolNomineeComponent } from './enrol-nominee/enrol-nominee.component';
import { SortablejsModule } from 'angular-sortablejs';
import { ContentService } from './content.service';
import { StepWiseDataComponent } from './step-wise-data/step-wise-data.component';
import {ComponentModule} from '../../../../../component/component.module';
import { DemoMaterialModule } from '../../../../material-module';
import { PopupComponent } from './popup/popup.component';
import { JoditAngularModule } from 'jodit-angular';
@NgModule({
  imports: [
    CommonModule,
    ThemeModule,
    NgxEchartsModule,
    DatepickerModule,
    BsDatepickerModule,
    NgxMyDatePickerModule.forRoot(),
    TabsModule,
    ReactiveFormsModule,
    AngularMultiSelectModule,
    FormsModule,
    TranslateModule.forRoot(),
    NgxDatatableModule,
    Ng2SmartTableModule,
    NbTabsetModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    FilterPipeModule,
    MyDatePickerModule,
    TagInputModule,
    TooltipModule.forRoot(),
    FilterPipeModule,
    TagInputModule,
    AngularMultiSelectModule,
    StarRatingModule.forRoot() ,
    TooltipModule.forRoot(),
    SortablejsModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
    DemoMaterialModule,
    ComponentModule,
    JoditAngularModule,
  ],
  declarations: [
    AddEditEepWorkflowComponent,
    DetailsComponent,
    PopupComponent,
    // NomineesComponent,
    // CalendarComponent,
    ContentComponent,
    NomineeDetailsComponent,
    CodeOfConductComponent,
    StepsComponent,
    EnrolNomineeComponent,
    NomineesComponent,
    StepWiseDataComponent,
    // PopupComponent,
    // TableComponent ,
    // TableModalComponent,
  ],
  entryComponents: [
     PopupComponent,
  ],
  providers: [
    ContentService,
   ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA ,
  ]
})

export class AddEditEepWorkflowModule {}
