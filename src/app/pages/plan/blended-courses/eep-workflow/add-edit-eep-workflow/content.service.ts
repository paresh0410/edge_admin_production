import { Injectable, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AppConfig } from '../../../../../app.module';
import { webApi } from '../../../../../service/webApi';
import { HttpClient } from "@angular/common/http";

@Injectable()
export class ContentService {



    private _urlGetAllCourses: string = webApi.domain + webApi.url.getallEEPcoursesinstance;
    private _urlEnrolCourseForCCContent: string = webApi.domain + webApi.url.enrolcourseforEEPinstance;
    private _urlGetAllEnrollCourseModule: string = webApi.domain + webApi.url.getallEEPenrolcoursemoduleinstance;
    private _urlRemoveEnrolCourse: string = webApi.domain + webApi.url.removeEEPenrolcourseinstance;
    private _urlInsertWorklowSteps: string = webApi.domain + webApi.url.insertEEPSteps;
    private _urlGetWorklowSteps: string = webApi.domain + webApi.url.getallEEPsteps;
    private _urlGetAllTttNominees: string = webApi.domain + webApi.url.getAllEEPNominnes;
    private _urlGetNomineeDetails: string = webApi.domain + webApi.url.getEEPNomineeDetails;
    private _urlGetBhApprovalStatus: string = webApi.domain + webApi.url.getEEPGBhapprovalStatus;
    private _urlUpdateBhapprovalStatus: string = webApi.domain + webApi.url.updateEEPGBhApprovalStatus;
    private _getActivityDropDown: string = webApi.domain + webApi.url.getEEPistanceactivitydropdown;
    private _getFgAssessmentDetails: string = webApi.domain + webApi.url.updateEEPAptitudeTestDetails;
    private _urlGetadminApprovalStatus: string = webApi.domain + webApi.url.getEEPAdminapprovalStatus;
    private _urlUpdateadminapprovalStatus: string = webApi.domain + webApi.url.updateEEPAdminApprovalStatus;
    private _getstepwisedataurl: string = webApi.domain + webApi.url.getstepwisedataEEP;
    private _getstepwiseuserdata: string = webApi.domain + webApi.url.getstepwiseuserdataEEP;
    private _getstepwisebhapproval: string = webApi.domain + webApi.url.stepwisebhapprovalEEP;
    private _getstepwiseadminapproval: string = webApi.domain + webApi.url.stpwiseadminapprovalEEP;
    private _getallEEPenroluser: string = webApi.domain + webApi.url.getallEEPenroluser;
    private _getSerchEEPenroluser: string = webApi.domain + webApi.url.getSerchEEPenroluser;
    private _enabledisableuser: string = webApi.domain + webApi.url.enableDisableEEPuser;
    private _urlEEPBulEnrol:string =webApi.domain + webApi.url.urlEEPBulEnrol;
    private _urlEEPPMRDAC:string =webApi.domain + webApi.url.urlEEPPMRDAC;
    private _urlemployeeCriteria:string =webApi.domain + webApi.url.employeeCriteria;

    public courseId: any;
    public programId: any;
    public tenantId: any;
    constructor(@Inject('APP_CONFIG_TOKEN') private config: AppConfig, private _http: Http, private _httpClient: HttpClient) {
        //this.busy = this._http.get('...').toPromise();
    }

    getAllCoursesCC(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlGetAllCourses, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    enrollCourseForCCContent(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlEnrolCourseForCCContent, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }


    removeEnrolCourse(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlRemoveEnrolCourse, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    getAllEnrolCourseModule(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlGetAllEnrollCourseModule, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    insertWorkflowSteps(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlInsertWorklowSteps, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    getAllWorkflowSteps(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlGetWorklowSteps, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }


    getAllNominees(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlGetAllTttNominees, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }


    getNomineeDetails(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlGetNomineeDetails, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    getBhApprovalStatus(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlGetBhApprovalStatus, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    updateBhApprovalStatus(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlUpdateBhapprovalStatus, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    getCourseActivities(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._getActivityDropDown, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    getFgAssessmentDetails(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._getFgAssessmentDetails, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }
    getCertifiacteUrl(Cerurl) {
        return new Promise(resolve => {
            this._httpClient.get(Cerurl)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }
    getadminApprovalStatus(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlGetadminApprovalStatus, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    updateadminApprovalStatus(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlUpdateadminapprovalStatus, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    stepwisedata(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._getstepwisedataurl, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    stepwiseuserdata(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._getstepwiseuserdata, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    stepwiseBhapproval(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._getstepwisebhapproval, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    stepwiseadminapproval(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._getstepwiseadminapproval, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    getallenroluser(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._getallEEPenroluser, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    getserchenroluser(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._getSerchEEPenroluser, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    enableDisableEEP(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._enabledisableuser, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    eepBulkEnrol(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlEEPBulEnrol, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }
    eepPMRDACaccepatance(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlEEPPMRDAC, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }
    eepemployeeCriteria(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlemployeeCriteria, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    
    _errorHandler(error: Response) {
        console.error(error);
        return Observable.throw(error || 'Server Error')
    }


}
