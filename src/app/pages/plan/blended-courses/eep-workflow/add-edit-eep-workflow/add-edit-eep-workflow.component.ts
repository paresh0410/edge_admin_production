import { Component, ViewChild, OnInit, OnDestroy, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { Router, NavigationStart, Routes, ActivatedRoute } from '@angular/router';
import { BlendedService } from '../../blended.service';
import { AddEditCourseContentService } from '../../../courses/addEditCourseContent/addEditCourseContent.service';
import { DatePipe } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { XlsxToJsonService } from '../../../users/uploadusers/xlsx-to-json-service';
import { JsonToXlsxService } from '../../../users/uploadusers/json-to-xlsx-service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ContentService } from './content.service';
// import { truncate } from 'fs';
import { DatatableComponent } from '@swimlane/ngx-datatable';
// import { Inject} from '@angular/core';
import { PopupComponent } from './popup/popup.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CommonFunctionsService } from '../../../../../service/common-functions.service';
import { webApi } from '../../../../../service/webApi';
import { SuubHeader } from '../../../../components/models/subheader.model';
import { BrandDetailsService } from '../../../../../service/brand-details.service';
import { noData } from '../../../../../models/no-data.model';
@Component({
  selector: 'ngx-add-edit-eep-workflow',
  templateUrl: './add-edit-eep-workflow.component.html',
  styleUrls: ['./add-edit-eep-workflow.component.scss'],
  encapsulation: ViewEncapsulation.None,

})
export class AddEditEepWorkflowComponent implements OnInit, OnDestroy {
  @ViewChild('myTable') table: any;

  @ViewChild('fileUpload') fileUpload: any;
  @ViewChild(DatatableComponent) tableData: DatatableComponent;
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No Enrollment at this time.",
    desc:'',
    titleShow:true,
    btnShow:true,
    descShow:false,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/eep-pmr-dac',
  }
  employeeCriteriaPopup: boolean = false;
  backflag = 1;
  detailsTab: boolean = true;
  nomineeTab: boolean = false;
  steps: boolean = false;
  steps_data: boolean = false;
  content: boolean = false;
  enrolTab: boolean = false;
  codeOfConduct: boolean = false;
  courseID: any;
  servicedata: any;
  showEnrolpage: boolean = false;
  showBulkpage: boolean = false;
  preview: boolean = false;
  tenantId: any;
  searchvalue: any = {
    value: ''
  };
  selectedall: any = [];
  userId: number;
  temp: any = [];
  rows: any = [];
  enrolldata: any;
  resultdata: any = [];
  errorMsg;
  fileName: any;
  fileReaded: any;
  enableUpload: any;
  file: any = [];
  datasetPreview: any;
  resultSheets: any = [];
  result: any = [];
  uploadedData: any = [];
  bulkUploadData: any = [];
  fileUrl: any;
  validdata: any = [];
  invaliddata: any = [];
  contentData: any = [];
  addEditModRes: any;
  timeout: any;
  isShown: boolean = false;
  columns: any = [];
  areaId = 31;
  header: SuubHeader;
  labels: any = [
    { labelname: 'EMP CODE', bindingProperty: 'ecn', componentType: 'text' },
    { labelname: 'FULL NAME', bindingProperty: 'fullname', componentType: 'text' },
    { labelname: 'EMAIL', bindingProperty: 'emailId', componentType: 'text' },
    { labelname: 'MOBILE', bindingProperty: 'phoneNo', componentType: 'text' },
    { labelname: 'D.0.E', bindingProperty: 'enrolDate', componentType: 'text' },
    { labelname: 'CRITERIA STATUS', bindingProperty: 'result', componentType: 'text' },
    { labelname: 'ACTION', bindingProperty: 'btntext', componentType: 'button' },
  ];
  // rows: any = [
  // { 'empcode': 'ecn', 'Fullname': 'fullname', 'email':'emailId','number':'phoneNo','date':'enrolDate','mode':'enrolmode','button' :'btntext' },
  // ];
  private xlsxToJsonService: XlsxToJsonService = new XlsxToJsonService();
  rowsTest: any;
  isdata: any;
  searchText: any;
  currentBrandData: any;

  constructor(public routes: ActivatedRoute, private router: Router, private blendedService: BlendedService,
    private commonFunctionService: CommonFunctionsService,
    public addEditCourseService: AddEditCourseContentService, private datePipe: DatePipe,
    public brandService: BrandDetailsService,
    public cdf: ChangeDetectorRef, private toastr: ToastrService, private exportService: JsonToXlsxService,
    private spinner: NgxSpinnerService, public dialog: MatDialog, public contentService: ContentService) {

    this.columns = [
      {
        prop: 'selected',
        name: '',
        sortable: false,
        canAutoResize: false,
        draggable: false,
        resizable: false,
        headerCheckboxable: true,
        checkboxable: true,
        width: 30
      },
      { prop: 'ecn', name: 'EMP CODE' },
      { prop: 'fullname', name: 'FULLNAME' },
      { prop: 'gender', name: 'GENDER' },
      { prop: 'doj', name: 'DOJ' },
      { prop: 'department', name: 'DEPARTMENT' },
      { prop: 'mode', name: 'MODE' }
    ];
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(PopupComponent, {
      width: '78vw',
      panelClass: 'gopal-modalbox'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.allEnrolUser();
    });
  }


  ngOnInit() {
    var userData = JSON.parse(localStorage.getItem('LoginResData'));
    this.currentBrandData = this.brandService.getCurrentBrandData();
    this.fileName = 'Click here to upload an excel file to enrol ' + this.currentBrandData.employee.toLowerCase() + ' to this course'
    console.log('userData', userData.data);
    this.userId = userData.data.data.id;
    this.tenantId = userData.data.data.tenantId;
    if (this.blendedService.program) {
      this.servicedata = this.blendedService.program;
      this.contentData = this.blendedService.program;
    } else {
      this.servicedata = {
        nId: '',
        catName: '',
        catId: '',
        pName: '',
        pId: '',
        instName: '',
        nDesc: '',
        sDateF: '',
        sDate: '',
        eDateF: '',
        eDate: '',
        eCsrId: '',
        visible: 1,
        instituteName: '',
        instituteId: '',
        partnerName: '',
        distributionPartnerId: '',
        budget: '',
        seatCost: '',
        tenantId: '',
        timemodified: '',
        ucrId: '',
        enrollcount: '',
      }
    }

    console.log(this.servicedata);
    if (this.servicedata) {
      this.allEnrolUser();
    }
    this.allEnrolUser();
  }
  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    this.allEnrolUser();
  }
  ngOnChanges() {

    this.allEnrolUser();
  }
  ngOnDestroy() {
    this.blendedService.wfId = '';
    this.blendedService.crsId = '';
  }
  selectedTab(tabEvent) {
    console.log('tab Selected', tabEvent);

    if (tabEvent.tabTitle == "Details") {
      this.header = {
        title: this.servicedata.instName!=""?this.servicedata.instName:'Add EEP',
        btnsSearch: true,
        btnName4: 'Save',
        btnName4show: true,
        btnBackshow: true,
        showBreadcrumb: true,
        breadCrumbList: [
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          // {
          //   'name': 'Blended Courses',
          //   'navigationPath': '/pages/learning/blended-home',
          // },
          {
            'name': 'EEP Workflow',
            'navigationPath': '/pages/eep-workflow',
          },
        ]
      };
      this.backflag = 1;
      this.detailsTab = true;
      this.codeOfConduct = false;
      this.steps = false;
      this.content = false;
      this.enrolTab = false;
      this.nomineeTab = false;
      this.showEnrolpage = false;
      this.showBulkpage = false;
      this.steps_data = false;
    } else if (tabEvent.tabTitle == "Program Structure") {
      this.header = {
        title: this.servicedata.instName!=""?this.servicedata.instName:'Add EEP',
        btnsSearch: true,
        btnName5: 'Add Section',
        btnName6: 'Save',
        btnName5show: true,
        btnName6show: true,
        btnBackshow: true,
        showBreadcrumb: true,
        breadCrumbList: [
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          // {
          //   'name': 'Blended Courses',
          //   'navigationPath': '/pages/learning/blended-home',
          // },
          {
            'name': 'EEP Workflow',
            'navigationPath': '/pages/eep-workflow',
          },
        ]
      };
      this.spinner.show();
    setTimeout(() => {
      this.spinner.hide()
    }, 2000);
      this.backflag = 1;
      this.detailsTab = false;
      this.codeOfConduct = true;
      this.steps = false;
      this.content = false;
      this.enrolTab = false;
      this.nomineeTab = false;
      this.showEnrolpage = false;
      this.showBulkpage = false;
      this.steps_data = false;
    } else if (tabEvent.tabTitle == "Content") {
      this.header = {
        title: this.servicedata.instName!=""?this.servicedata.instName:'Add EEP',
        btnsSearch: true,
        btnBackshow: true,
        showBreadcrumb: true,
        breadCrumbList: [
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          // {
          //   'name': 'Blended Courses',
          //   'navigationPath': '/pages/learning/blended-home',
          // },
          {
            'name': 'EEP Workflow',
            'navigationPath': '/pages/eep-workflow',
          },
        ]
      };
      this.backflag = 1;
      this.detailsTab = false;
      this.codeOfConduct = false;
      this.steps = false;
      this.content = true;
      this.enrolTab = false;
      this.nomineeTab = false;
      this.showEnrolpage = false;
      this.showBulkpage = false;
      this.steps_data = false;
    }
    else if (tabEvent.tabTitle == "Steps") {
      this.header = {
        title: this.servicedata.instName!=""?this.servicedata.instName:'Add EEP',
        btnsSearch: true,
        btnName7: 'Save',
        btnName7show: true,
        btnBackshow: true,
        showBreadcrumb: true,
        breadCrumbList: [
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          // {
          //   'name': 'Blended Courses',
          //   'navigationPath': '/pages/learning/blended-home',
          // },
          {
            'name': 'EEP Workflow',
            'navigationPath': '/pages/eep-workflow',
          },
        ]
      };
      this.backflag = 1;
      this.spinner.show();
    setTimeout(() => {
      this.spinner.hide()
    }, 2000);
      this.detailsTab = false;
      this.codeOfConduct = false;
      this.steps = true;
      this.content = false;
      this.showEnrolpage = false;
      this.showBulkpage = false;
      this.enrolTab = false;
      this.nomineeTab = false;
      this.steps_data = false;
    } else if (tabEvent.tabTitle == "Enrol") {
      this.header = {
        title: this.servicedata.instName!=""?this.servicedata.instName:'Add EEP',
        btnsSearch: true,
        searchBar: false,
        btnName2: 'Enrol',
        btnName3: 'Bulk Enrol',
        btnName8: 'Download',
        btnName9: 'Upload',
        btnName2show: true,
        btnName3show: true,
        btnName8show: true,
        btnName9show: true,
        btnBackshow: true,
        showBreadcrumb: true,
        breadCrumbList: [
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          // {
          //   'name': 'Blended Courses',
          //   'navigationPath': '/pages/learning/blended-home',
          // },
          {
            'name': 'EEP Workflow',
            'navigationPath': '/pages/eep-workflow',
          },
        ]
      };
      this.spinner.show();
    setTimeout(() => {
      this.spinner.hide()
    }, 2000);
      this.backflag = 1;
      this.detailsTab = false;
      this.codeOfConduct = false;
      this.steps = false;
      this.content = false;
      this.enrolTab = true;
      this.nomineeTab = false;
      this.showEnrolpage = false;
      this.showBulkpage = false;
      this.steps_data = false;
    } else if (tabEvent.tabTitle == "Nominees") {
      this.header = {
        title: this.servicedata.instName!=""?this.servicedata.instName:'Add EEP',
        btnsSearch: true,
        btnBackshow: true,
        showBreadcrumb: true,
        breadCrumbList: [
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          // {
          //   'name': 'Blended Courses',
          //   'navigationPath': '/pages/learning/blended-home',
          // },
          {
            'name': 'EEP Workflow',
            'navigationPath': '/pages/eep-workflow',
          },
        ]
      };
      this.spinner.show();
    setTimeout(() => {
      this.spinner.hide()
    }, 2000);
      this.backflag = 1;
      this.detailsTab = false;
      this.codeOfConduct = false;
      this.steps = false;
      this.content = false;
      this.showEnrolpage = false;
      this.showBulkpage = false;
      this.enrolTab = false;
      this.nomineeTab = true;
      this.steps_data = false;
    } else if (tabEvent.tabTitle === 'Steps Data') {
      this.header = {
        title: this.servicedata.instName!=""?this.servicedata.instName:'Add EEP',
        btnsSearch: true,
        btnBackshow: true,
        showBreadcrumb: true,
        breadCrumbList: [
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          // {
          //   'name': 'Blended Courses',
          //   'navigationPath': '/pages/learning/blended-home',
          // },
          {
            'name': 'EEP Workflow',
            'navigationPath': '/pages/eep-workflow',
          },
        ]
      };
      this.backflag = 1;
      this.detailsTab = false;
      this.codeOfConduct = false;
      this.steps = false;
      this.content = false;
      this.enrolTab = false;
      this.showEnrolpage = false;
      this.showBulkpage = false;
      this.nomineeTab = false;
      this.steps_data = true;
    }
  }

  getId(event) {
    console.log(event);
    this.blendedService.wfId = event.wfId;
  }
  getcrsId(event) {
    console.log(event);
    this.blendedService.crsId = event;
    console.log(this.servicedata);
    console.log(this.blendedService.crsId);
  }
  back() {

    if (this.backflag == 2) {
      this.showEnrolpage = false;
      this.showBulkpage = false;
      this.preview = false;
      this.header = {
        title: this.servicedata.instName!=""?this.servicedata.instName:'Add EEP',
        btnsSearch: true,
        btnName2: 'Enrol',
        btnName3: 'Bulk Enrol',
        btnName8: 'Download',
        btnName9: 'Upload',
        btnName2show: true,
        btnName3show: true,
        btnName8show: true,
        btnName9show: true,
        btnBackshow: true,
        showBreadcrumb: true,
        breadCrumbList: [
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          // {
          //   'name': 'Blended Courses',
          //   'navigationPath': '/pages/learning/blended-home',
          // },
          {
            'name': 'EEP Workflow',
            'navigationPath': '/pages/eep-workflow',
          },
        ]
      };
      this.backflag = 1;
      this.showEnrolpage = false;
      this.showBulkpage = false;
      this.preview = false;
    } else {
      window.history.back();
    }

    // this.router.navigate(['../'], { relativeTo: this.routes });
  }
  saveEnrol() {
    this.header = {
      title: this.servicedata.instName!=""?this.servicedata.instName:'Add EEP',
      btnsSearch: true,
      btnName3: 'Bulk Enrol',
      btnName3show: true,
      btnBackshow: true,
      showBreadcrumb: true,
      breadCrumbList: [
        // {
        //   'name': 'Learning',
        //   'navigationPath': '/pages/learning',
        // },
        // {
        //   'name': 'Blended Courses',
        //   'navigationPath': '/pages/learning/blended-home',
        // },
        {
          'name': 'EEP Workflow',
          'navigationPath': '/pages/eep-workflow'
        },
      ]
    };
    this.backflag = 2;
    this.showEnrolpage = true;
    this.showBulkpage = false;
    this.cdf.detectChanges();
  }
  bulkEnrol(data) {
    this.header = {
      title: this.servicedata.instName!=""?this.servicedata.instName:'Add EEP',
      btnsSearch: true,
      btnName2: 'Enrol',
      btnName2show: true,
      btnBackshow: true,
      showBreadcrumb: true,
      breadCrumbList: [
        // {
        //   'name': 'Learning',
        //   'navigationPath': '/pages/learning',
        // },
        // {
        //   'name': 'Blended Courses',
        //   'navigationPath': '/pages/learning/blended-home',
        // },
        {
          'name': 'EEP Workflow',
          'navigationPath': '/pages/eep-workflow'
        },
      ]
    };
    this.backflag = 2;
    this.showEnrolpage = false;
    this.showBulkpage = true;
  }
  backToEnrol() {
    this.showEnrolpage = false;
    this.showBulkpage = false;
    this.preview = false;
  }
  clearesearch() {
    if (this.searchText.length >= 3) {
      this.searchvalue.value = '';
      this.allEnrolUser();
      this.searchText = ''
    }
    else {
      this.searchvalue.value = '';
    }
  }
  allEnrolUser() {
    this.isShown = false;
    this.spinner.show();
    var data = {
      areaId: 31,
      instanceId: this.servicedata.nId,
      tId: this.tenantId,
      mode: 0,
    };
    console.log(data);
    const _getallEEPenroluser: string = webApi.domain + webApi.url.getallEEPenroluser;
    this.commonFunctionService.httpPostRequest(_getallEEPenroluser, data)
      //this.contentService.getallenroluser(data)
      .then(enrolData => {
        this.spinner.hide();
        this.enrolldata = enrolData['data'];
        this.rows = enrolData['data'];
        this.rows = [...this.enrolldata];
        //  for (let i = 0; i < this.rows.length; i++) {
        //     this.rows[i].Date = new Date(this.rows[i].enrolDate);
        //     this.rows[i].enrolDate = this.formdate(this.rows[i].Date);

        //   }
        for (let i = 0; i < this.rows.length; i++) {
          if (this.rows[i].visible == 1) {
            this.rows[i].btntext = 'fa fa-eye';
          } else {
            this.rows[i].btntext = 'fa fa-eye-slash';
          }
        }

        console.log("this.rows", this.rows);

        //this.spinner.hide();
        this.isShown = true;
        this.cdf.detectChanges();
      });
  }
  formdate(date) {

    if (date) {
      var formatted = this.datePipe.transform(date, 'dd-MM-yyyy');
      return formatted;
    }
  }
  searchEnrolUser(event) {
    const val = event.target.value.toLowerCase();

    // this.allEnrolUser( this.courseDataService.data.data)

    this.temp = [...this.enrolldata];
    console.log(this.temp);
    // filter our data
    this.searchText = val;
    if (val.length >= 3 || val.length == 0) {
      const temp = this.temp.filter(function (d) {
        return String(d.ecn).toLowerCase().indexOf(val) !== -1 ||
          d.fullname.toLowerCase().indexOf(val) !== -1 ||
          d.emailId.toLowerCase().indexOf(val) !== -1 ||
          d.enrolmode.toLowerCase().indexOf(val) !== -1 ||
          d.enrolDate.toLowerCase().indexOf(val) !== -1 ||
          d.phoneNo.toLowerCase().indexOf(val) !== -1 ||
          d.result.toLowerCase().indexOf(val) !== -1 ||
          String(d.enrolmode).toLowerCase().indexOf(val) !== -1 ||
          // d.mode.toLowerCase() === val || !val;
          !val
      });

      // update the rows
      this.rows = [...temp];
    }
    // Whenever the filter changes, always go back to the first page
    this.tableData.offset = 0;
  }
  onEnrolDataClickEvent(event) {
    // if (event.type == 'click' && event.cellIndex != 0) {
    //     // Do something when you click on row cell other than checkbox.
    //     console.log('Activate Event', event);
    // }
    // const checkboxCellIndex = 1;
    // if (event.type === 'checkbox') {
    //   // Stop event propagation and let onSelect() work
    //   console.log('Checkbox Selected', event);
    //   event.event.stopPropagation();
    // } else
    if (event.type === 'click' && event.cellIndex != 0) {


      this.getEmployeeCriteria(event.row);
      // Do somethings when you click on row cell other than checkbox
      console.log('Row Clicked', event.row); /// <--- object is in the event row variable
    }
  }
  onSelectEnrolData({ selected }) {
    console.log('Select Event', selected);
    // console.log('Select Data', this.selected);

    //this.selected.splice(0, this.selected.length);
    //this.selected.push(...selected);
  }

  visibilityTableRow(row) {
    let value;
    let status;

    if (row.visible == 1) {
      row.btntext = 'fa fa-eye-slash';
      value = 'fa fa-eye-slash';
      row.visible = 0
      status = 0;
    } else {
      status = 1;
      value = 'fa fa-eye';
      row.visible = 1;
      row.btntext = 'fa fa-eye';
    }

    for (let i = 0; i < this.rows.length; i++) {
      if (this.rows[i].employeeId == row.employeeId) {
        this.rows[i].btntext = row.btntext;
        this.rows[i].visible = row.visible
      }
    }
    var visibilityData = {
      employeeId: row.employeeId,
      visible: status,
      courseId: this.blendedService.wfId,
      tId: this.tenantId,
      aId: 31,
    };
    const _enabledisableuser: string = webApi.domain + webApi.url.enableDisableEEPuser;
    this.commonFunctionService.httpPostRequest(_enabledisableuser, visibilityData)
      //this.contentService.enableDisableEEP(visibilityData)
      .then(result => {
        console.log(result);
        this.spinner.hide();
        // this.loader = false;
        this.resultdata = result;
        if (this.resultdata.type == true) {
          this.presentToast('success', this.resultdata.data);
          this.allEnrolUser();
          // this.closeEnableDisableCourseModal();
          // this.toasterService.pop(courseUpdate);
        }
      },
        resUserError => {
          // this.loader = false;
          this.errorMsg = resUserError;
          this.spinner.show();
          this.presentToast('error', 'Something went wrong please try again');
          // this.closeEnableDisableCourseModal();
        });

    console.log('row', row);
  }
  // disableCourseVisibility(currentIndex, row, status) {
  //   this.spinner.show();
  //   var visibilityData = {
  //     employeeId: row.employeeId,
  //     visible: status,
  //     courseId: this.blendedService.wfId,
  //     tId: this.tenantId,
  //     aId: 31,
  //   };
  //   const _enabledisableuser: string = webApi.domain + webApi.url.enableDisableEEPuser;
  //   this.commonFunctionService.httpPostRequest(_enabledisableuser,visibilityData)
  //   //this.contentService.enableDisableEEP(visibilityData)
  //   .then(result => {
  //     console.log(result);
  //     this.spinner.hide();
  //     // this.loader = false;
  //     this.resultdata = result;
  //     if (this.resultdata.type == true) {
  //       this.presentToast('success', this.resultdata.data);
  //       this.allEnrolUser();
  //       // this.closeEnableDisableCourseModal();
  //       // this.toasterService.pop(courseUpdate);
  //     }
  //   },
  //     resUserError => {
  //       // this.loader = false;
  //       this.errorMsg = resUserError;
  //       this.spinner.show();
  //       this.presentToast('error', 'Something went wrong please try again');
  //       // this.closeEnableDisableCourseModal();
  //     });
  // }
  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }
  readUrl(event: any) {

    var validExts = new Array('.xlsx', '.xls');
    var fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
      // alert('Invalid file selected, valid files are of ' +
      //          validExts.toString() + ' types.');
      // var toast: Toast = {
      //   type: 'error',
      //   title: 'Invalid file selected!',
      //   body: 'Valid files are of ' + validExts.toString() + ' types.',
      //   showCloseButton: true,
      //   // tapToDismiss: false,
      //   timeout: 2000
      //   // onHideCallback: () => {
      //   //     this.router.navigate(['/pages/plan/users']);
      //   // }
      // };
      // this.toasterService.pop(toast);

      this.toastr.warning('Valid file types are ' + validExts.toString(), 'Warning', {
        closeButton: false
      })
      // setTimeout(data => {
      //   this.router.navigate(['/pages/users']);
      // },1000);
      // return false;
      this.cancel();
    } else {
      // return true;
      if (event.target.files && event.target.files[0]) {
        this.fileName = event.target.files[0].name;
        //read file from input
        this.fileReaded = event.target.files[0];

        if (this.fileReaded != '' || this.fileReaded != null || this.fileReaded != undefined) {
          this.enableUpload = true;
        }

        this.file = event.target.files[0];
        var reader = new FileReader();
        reader.onload = (event: ProgressEvent) => {
          let fileUrl = (<FileReader>event.target).result;
          // event.setAttribute('data-title', this.fileName);
          // console.log(this.fileUrl);
        }
        reader.readAsDataURL(event.target.files[0]);
      }
    }
  }
  // cancel() {
  //   // this.fileUpload.nativeElement.files = [];
  //   console.log(this.fileUpload.nativeElement.files);
  //   this.fileUpload.nativeElement.value = '';
  //   console.log(this.fileUpload.nativeElement.files);
  //   this.fileName = 'Click here to upload an excel file to enrol employee to this course';
  //   this.enableUpload = false;
  //   this.file = [];
  //   this.preview = false;
  // }
  uploadfile() {
    this.spinner.show();


    var fd = new FormData();
    this.contentData.areaId = 31;
    this.contentData.userId = this.userId;
    this.contentData.courseId = this.contentData.nId;
    fd.append('content', JSON.stringify(this.contentData));
    fd.append('file', this.file);
    console.log(fd);
    this.addEditCourseService.TempManEnrolBulk(fd).then(result => {
      console.log(result);
      // this.loader =false;

      var res = result;
      try {
        if (res['data1'][0]) {
          this.resultdata = res['data1'][0];
          for (let i = 0; i < this.resultdata.length; i++) {
            this.resultdata[i].enrolDate = this.formdate(this.resultdata[i].enrolDate);
          }
          console.log(this.resultdata)
          this.datasetPreview = this.resultdata;
          this.preview = true;
          this.cdf.detectChanges();
          this.spinner.hide();
        }
      } catch (e) {

        // var courseUpdate: Toast = {
        //   type: 'error',
        //   title: 'Enrol',
        //   body: 'Something went wrong.please try again later.',
        //   showCloseButton: true,
        //   timeout: 2000
        // };

        // this.closeEnableDisableCourseModal();
        // this.toasterService.pop(courseUpdate);

        this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
          timeOut: 0,
          closeButton: true
        });
      }
      this.spinner.hide();

    },
      resUserError => {
        // this.loader =false;
        this.spinner.hide();
        this.errorMsg = resUserError;
        // this.closeEnableDisableCourseModal();
      });
  }
  clearPreviewData() {
    this.datasetPreview = [];
    this.cancel();
  }
  onPageEnrolData(event) {
    // clearTimeout(this.timeout);
    // this.timeout = setTimeout(() => {
    //   console.log('paged!', event);
    // }, 100);
  }
  savebukupload1() {
    var data = {
      userId: this.userId,
      coursId: this.contentData.nId,
      areaId: 31,
      tId: this.tenantId,
    }
    // this.loader = true;
    this.spinner.show();
    this.addEditCourseService.finalManEnrolBulk(data).then(rescompData => {
      // this.loader =false;
      this.spinner.hide();
      var temp: any = rescompData;
      this.addEditModRes = temp.data[0];
      if (temp == 'err') {


        this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
          timeOut: 0,
          closeButton: true
        });
      } else if (temp.type == false) {


        this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
          timeOut: 0,
          closeButton: true
        });
      } else {

        this.toastr.success(this.addEditModRes[0].msg, 'Success', {
          closeButton: false
        });
        this.fileName = 'Click here to upload an excel file to enrol ' + this.currentBrandData.employee.toLowerCase() + ' to this course'
        this.showEnrolpage = false;
        this.showBulkpage = false;
        this.preview = false;
        this.enableUpload = false;
        this.allEnrolUser();
      }

      this.cdf.detectChanges();
    },
      resUserError => {
        // this.loader = false;
        this.errorMsg = resUserError;
      });
  }
  showInvalidExcel: boolean = false;
  savebukupload() {
    // this.loader = true;
    if (this.uploadedData.length > 0 && this.uploadedData.length <= 2000) {
      this.spinner.show();

      // var option: string = Array.prototype.map
      //   .call(this.uploadedData, function (item) {
      //     console.log("item", item);
      //     return Object.values(item).join("#");
      //   })
      //   .join("|");

      // New Function as per requirement.

      var option: string = Array.prototype.map
        .call(this.uploadedData, (item) => {
          const array = Object.keys(item);
          let string = '';
          array.forEach(element => {
            if (element === 'EnrolDate') {
              string += this.commonFunctionService.formatDateTimeForBulkUpload(item[element]);
            } else {
              string = item[element] + '#';
            }
          });
          // console.log("item", item);
          // return Object.values(item).join("#");
          return string;
        })
        .join("|");

      //  console.log('this.courseDataService.workflowId', this.courseDataService.workflowId);

      const data = {
        wfId: null,
        aId: 31,
        iId: this.contentData.nId,
        // userId: this.content.userId,
        upString: option
      }
      console.log('data', data);
      this.contentService.eepBulkEnrol(data)
        .then(rescompData => {
          // this.loader =false;
          this.spinner.hide();
          var temp: any = rescompData;
          this.addEditModRes = temp.data;
          if (temp == 'err') {
            this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
              timeOut: 0,
              closeButton: true
            });
          } else if (temp.type == false) {
            this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
              timeOut: 0,
              closeButton: true
            });
          } else {
            this.enableUpload = false;
            this.fileName = 'Click here to upload an excel file to enrol ' + this.currentBrandData.employee.toLowerCase() + ' to this course'
            if (temp['data'].length > 0) {
              if (temp['data'].length === temp['invalidCnt'].invalidCount) {
                this.toastr.warning('No valid ' + this.currentBrandData.employee.toLowerCase() + 's found to enrol.', 'Warning', {
                  closeButton: false
                });
              } else {
                this.toastr.success('Enrol successfully.', 'Success', {
                  closeButton: false
                });
              }
              this.invaliddata = temp['data'];
              this.showInvalidExcel = true;

              // this.invaliddata = temp['data'];
              // if (this.uploadedData.length == this.invaliddata.length) {
              //   this.toastr.warning('No valid employees found to enrol.', 'Warning', {
              //     closeButton: false
              //     });
              // } else {
              //   this.toastr.success('Enrol successfully.', 'Success', {
              //     closeButton: false
              //     });
              // }
            } else {
              this.invaliddata = [];
              this.showInvalidExcel = false;
            }




            // this.backToEnrol()
            this.cdf.detectChanges();
            this.allEnrolUser();
            this.preview = false;
            this.showInvalidExcel = true;
            this.cancel();

          }
          console.log('Enrol bulk Result ', this.addEditModRes);
          this.cdf.detectChanges();
        },
          resUserError => {
            //this.loader = false;
            this.errorMsg = resUserError;
          });
    } else {

      if (this.uploadedData.length > 2000) {
        this.toastr.warning('File Data cannot exceed more than 2000', 'warning', {
          closeButton: true
        });
      }
      else {
        this.toastr.warning('No Data Found', 'warning', {
          closeButton: true
        });
      }
    }

  }

  exportToExcelInvalid() {
    this.exportService.exportAsExcelFile(this.invaliddata, 'Enrolment Status')
  }

  cancel() {
    // this.fileUpload.nativeElement.files = [];
    console.log(this.fileUpload.nativeElement.files);
    this.fileUpload.nativeElement.value = '';
    console.log(this.fileUpload.nativeElement.files);
    this.fileName = 'Click here to upload an excel file to enrol ' + this.currentBrandData.employee.toLowerCase() + ' to this course'
    this.enableUpload = false;
    this.file = [];
    this.preview = false;
  }
  readFileUrl(event: any) {
    this.uploadedData = [];
    this.result = [];
    const validExts = new Array('.xlsx', '.xls');
    let fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
      this.toastr.warning('Valid file types are ' + validExts.toString(), 'Warning', {
        closeButton: false
      })
      // setTimeout(data => {
      //   this.router.navigate(['/pages/users']);
      // },1000);
      // return false;
      this.cancel();
    } else {
      // return true;
      if (event.target.files && event.target.files[0]) {
        this.fileName = event.target.files[0].name;
        // read file from input
        this.fileReaded = event.target.files[0];

        if (this.fileReaded != '' || this.fileReaded != null || this.fileReaded != undefined) {
          this.enableUpload = true;
          this.showInvalidExcel = false;
        }

        let file = event.target.files[0];
        this.bulkUploadData = event.target.files[0];
        console.log('this.bulkUploadData', this.bulkUploadData);

        this.xlsxToJsonService.processFileToJson({}, file).subscribe(data => {
          this.resultSheets = Object.getOwnPropertyNames(data['sheets']);
          console.log('File Property Names ', this.resultSheets);
          let sheetName = this.resultSheets[0];
          this.result = data['sheets'][sheetName];
          console.log('dataSheet', data);
          console.log('this.result', this.result);

          if (this.result.length > 0) {
            this.uploadedData = this.result;
          }
          console.log('this.uploadedData', this.uploadedData);
        });
        var reader = new FileReader();
        reader.onload = (event: ProgressEvent) => {
          this.fileUrl = (<FileReader>event.target).result;
          /// console.log(this.fileUrl);
        };
        reader.readAsDataURL(event.target.files[0]);
      }
    }
  }
  tempData = [];
  downloadTableData() {
    for (let i = 0; i < this.rows.length; i++) {
      var tempObj = {
        ecn: this.rows[i].ecn,
        fullname: this.rows[i].fullname,
        pmr: this.rows[i].pmr,
        dac: this.rows[i].dac,
      }
      this.tempData.push(tempObj);
    }
    this.exportService.exportAsExcelFile(this.tempData, 'Approved Data')
  }
  // employeeCriteriaPopup:boolean=false;
  criteriaRows: any = [];
  getEmployeeCriteria(param) {
    this.criteriaRows = [];
    var employeDetails = {
      areaId: this.areaId,
      instanceId: param.workflowId,
      employeeId: param.employeeId
    }
    this.contentService.eepemployeeCriteria(employeDetails)
      .then(result => {
        console.log(result);
        this.spinner.hide();
        // this.loader = false;
        this.resultdata = result;
        this.criteriaRows = result['data'];
        console.log('this.criteriaRows', this.criteriaRows);
        this.employeeCriteriaPopup = true;
      },
        resUserError => {
          // this.loader = false;
          this.errorMsg = resUserError;
          this.spinner.show();
          this.presentToast('error', 'Something went wrong please try again');
          // this.closeEnableDisableCourseModal();
        });

    console.log('EmployeeData', employeDetails);
  }
  closeEmployeeCriteriaModal() {
    this.employeeCriteriaPopup = false;
  }

  passData(val) {
    this.isdata = val;
    // this.eventsSubject.next();
    setTimeout(() => {
      this.isdata = '';
    }, 2000);
  }
}
