import { Component, OnInit, ViewEncapsulation ,ElementRef, ViewChild } from '@angular/core';
import { Router, NavigationStart, Routes, ActivatedRoute } from '@angular/router';
import { BlendedService } from '../blended.service';
import { NgxSpinnerService } from 'ngx-spinner';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { ToastrService } from 'ngx-toastr';
import { CommonFunctionsService } from '../../../../service/common-functions.service';
import { webApi } from '../../../../service/webApi';
import { Card } from '../../../../models/card.model';
import { SuubHeader } from '../../../components/models/subheader.model';
import { noData } from '../../../../models/no-data.model';

@Component({
  selector: 'ngx-eep-workflow-instance',
  templateUrl: './eep-workflow.component.html',
  styleUrls: ['./eep-workflow.component.scss'],
  encapsulation: ViewEncapsulation.None,

})
export class EepWorkflowComponent implements OnInit {
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No EEP cohort at this time.",
    titleShow:true,
    btnShow:true,
    descShow:false,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/eep-cohort',
  }
  header: SuubHeader  = {
    title:'EEP Workflow',
    btnsSearch: true,
    searchBar: true,
    dropdownlabel: ' ',
    placeHolder:'Search by instance name',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: '',
    btnName3: '',
    btnAdd: 'Add Cohort',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: false,
    btnAddshow: true,
    filter: false,
    showBreadcrumb: true,
    breadCrumbList:[
      // {
      //   'name': 'Learning',
      //   'navigationPath': '/pages/learning',
      // },
      // {
      //   'name': 'Blended Courses',
      //   'navigationPath': '/pages/learning/blended-home',
      // },
    ]
  };
  skeletonCount=8
  currentUser: any = [];
  skeleton = false;
  cardModify: Card = {
    flag: 'eepWorkflow',
    titleProp : 'instName',
   
    image: 'instancePicRef',
    
    hoverlable:   true,
    showImage: true,
    option:   true,
    eyeIcon:   true,
   
   
    bottomDiv:   true,
   
    bottomTitle:   true,
    btnLabel: 'Edit',
    defaultImage:'assets/images/courseicon.jpg'
  };
  // tId =  this.currentUser.data.data.tenantId;
  nominationInstances: any = [
    // {
    //   categoryId: 1,
    //   instanceName: "Test 1",
    //   instanceCode: "test",
    //   instanceId: 12,
    //   programId: 1,
    //   startDate: new Date(),
    //   endDate: new Date(),
    //   role: "",
    //   pointsBefore: 50,
    //   pointsAfter: 80,
    //   managerCredits: 50,
    //   batchLocation:"",
    //   batchLocationId:1,
    //   batchVenueId: 1,
    //   batchVenue: "",
    //   batchLevelId: 1,
    //   batchOrder: 0,
    //   instancePicRef: "https://bhaveshedgetest.s3.ap-south-1.amazonaws.com/splash.png",
    //   tenantId: 1,
    //   visible: 1,
    //   bcId:1,

    // },
    // {
    //   categoryId: 2,
    //   instanceName: "Test 2",
    //   instanceCode: "demo",
    //   instanceId: 2,
    //   programId: 5,
    //   startDate: new Date(),
    //   endDate: new Date(),
    //   role: "",
    //   pointsBefore: 50,
    //   pointsAfter: 80,
    //   managerCredits: 50,
    //   batchLocation:"",
    //   batchLocationId:1,
    //   batchVenueId: 1,
    //   batchVenue: "",
    //   batchLevelId: 1,
    //   batchOrder: 0,
    //   instancePicRef: "https://bhaveshedgetest.s3.ap-south-1.amazonaws.com/splash.png",
    //   tenantId: 1,
    //   visible: 1,
    //   bcId:1,
    // },
    // {
    //   categoryId: 3,
    //   instanceName: "Test 2",
    //   instanceCode: "test3",
    //   instanceId: 10,
    //   programId: 3,
    //   startDate: new Date(),
    //   endDate: new Date(),
    //   role: "",
    //   pointsBefore: 50,
    //   pointsAfter: 80,
    //   managerCredits: 50,
    //   batchLocation:"",
    //    batchLocationId:1,
    //   batchVenueId: 1,
    //   batchVenue: "",
    //   batchLevelId: 1,
    //   batchOrder: 0,
    //   instancePicRef: "https://bhaveshedgetest.s3.ap-south-1.amazonaws.com/splash.png",
    //   tenantId: 1,
    //   visible: 1,
    //   bcId:1,
    // }
  ];
  searchinst: any = [];
  searchText: any;
  enable: boolean;
  title: string;
  enableDisableModal: boolean;
  eepData: any;
  constructor(private spinner: NgxSpinnerService, public routes: ActivatedRoute,
    // private toasterService: ToasterService, 
    private router: Router,
    private toastr: ToastrService,
    private blendedService: BlendedService,
    private commonFunctionService: CommonFunctionsService,) {

  }

  // ngOnInit() {
   
  // }
  clear() {
    if(this.searchText.length>=3){
    this.searchinst = {};
    this.displayArray =[];
    this.newArray = [];
    this.sum = 50;
    this.addItems(0, this.sum, 'push', this.nominationInstances);
    this.noNominationInstance=false
    this.searchText='';
    }
    else{
    this.searchinst = {};
    }
  };
  errorMsg: string;
  nodata: any = false;
  visibiltyRes: any;
  noNominationInstance: boolean = false;
  displayInstances() {
    // this.spinner.show();
    var data1 = {
      tId: this.currentUser.data.data.tenantId,
    };
    const _EEPInstanceList: string = webApi.domain + webApi.url.getEEP;
    this.commonFunctionService.httpPostRequest(_EEPInstanceList,data1)
    //this.blendedService.EEPgetnomination(data1)
      .then(rescompData => {
        // this.spinner.hide();
        console.log(rescompData);
        if (rescompData['type'] === true) {
          if (rescompData['data'].length == 0) {
            this.skeleton = true;
            this.noNominationInstance = true;
          } else {
            this.skeleton = true;
            this.nominationInstances = rescompData['data'];
            this.addItems(0,this.sum,'push',this.nominationInstances);
          }

          console.log('Nomination Instances >>', this.nominationInstances);
        } else {
          this.nodata = true;
          this.noNominationInstance = true;
          this.skeleton = true;
        }
      });
  }

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }

  disableInstance(i, item) {
    var visibilityData = {
      nid: item.nId,
      visible: item.visible == 1 ? 0 : 1,
    }
    // let url = webApi.domain + webApi.url.disablequiz;
    const _EEPurlDisableInstance: string = webApi.domain + webApi.url.disableEEPInstances;
    this.commonFunctionService.httpPostRequest(_EEPurlDisableInstance,visibilityData)
    //this.blendedService.EEPdisableInstance(visibilityData)
      .then(rescompData => {
        this.spinner.hide();
        this.visibiltyRes = rescompData;
        if (this.visibiltyRes.type == false) {
          // var catUpdate: Toast = {
          //   type: 'error',
          //   title: "Nomination Visibility",
          //   body: "Unable to update visibility of Instance",
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          // this.toasterService.pop(catUpdate);

          this.presentToast('error', '');
        } else {
          // var catUpdate: Toast = {
          //   type: 'success',
          //   title: "Nomination Visibility",
          //   body: this.visibiltyRes.data,
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('success', this.visibiltyRes.data);

          this.disablevaluechnage(i, item)
        }
      },
        resUserError => {
          this.spinner.hide();
          if (resUserError.statusText == "Unauthorized") {
            this.router.navigate(['/login']);
          }
          this.errorMsg = resUserError;
        });
  }

  clickTodisable(item) {
    var visibilityData = {
      nid: item.nId,
      visible: item.visible,
    }


    // if(item.visible == 1) {
    //   item.visible = 0;
    // } else {
    //   item.visible = 1;
    // }
    // let url = webApi.domain + webApi.url.disablequiz;
    const _EEPurlDisableInstance: string = webApi.domain + webApi.url.disableEEPInstances;
    this.commonFunctionService.httpPostRequest(_EEPurlDisableInstance,visibilityData)
    //this.blendedService.EEPdisableInstance(visibilityData)
      .then(rescompData => {
        // this.spinner.hide();
        this.visibiltyRes = rescompData;
        if (this.visibiltyRes.type == false) {
         

          this.presentToast('error', '');
        } else {
          
          // this.presentToast('success', this.visibiltyRes.data);

         
        }
      },
        resUserError => {
          this.spinner.hide();
          if (resUserError.statusText == "Unauthorized") {
            this.router.navigate(['/login']);
          }
          this.errorMsg = resUserError;
        });
        this.closeEnableDisableModal();
  }

  disablevaluechnage(i, item) {
    this.nominationInstances[i].visible = this.nominationInstances[i].visible == 1 ? 0 : 1;
  }

  public addeditcontent(data, id) {
    this.blendedService.program = data ? data : '';
    if (data) {
      this.blendedService.wfId = data.nId;
    } else {
      this.blendedService.wfId = 0;
    }
    console.log("data", data);

    this.router.navigate(['add-edit-eep-workflow'], { relativeTo: this.routes });
  }

  gotoCardEdit(data) {
    this.blendedService.program = data ? data : '';
    if (data) {
      this.blendedService.wfId = data.nId;
    } else {
      this.blendedService.wfId = 0;
    }
    console.log("data", data);

    this.router.navigate(['add-edit-eep-workflow'], { relativeTo: this.routes });
  }

  back() {
    window.history.back();
    // this.router.navigate(['../'], { relativeTo: this.routes });
  }

  onsearch(event){
    console.log(event,"event")
    
    this.searchinst.instName =  event.target.value
    
  }
  
  ngOnDestroy(){
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0){
      e1[0].classList.add('with-scroll');
    }
  }
  ngAfterContentChecked() {
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0)
    {
      e1[0].classList.remove('with-scroll');
    }

   }
   conentHeight = '0px';
   offset: number = 100;
   ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('LoginResData'));
    this.displayInstances();
     this.conentHeight = '0px';
     const e = document.getElementsByTagName('nb-layout-column');
     console.log(e[0].clientHeight);
     // this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
     if (e[0].clientHeight > 1300) {
       this.conentHeight = '0px';
       this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
     } else {
       this.conentHeight = String(e[0].clientHeight) + 'px';
     }
   }
 
   sum = 40;
   addItems(startIndex, endIndex, _method, asset) {
     for (let i = startIndex; i < endIndex; ++i) {
       if (asset[i]) {
         this.displayArray[_method](asset[i]);
       } else {
         break;
       }
 
       // console.log("NIKHIL");
     }
   }
   // sum = 10;
   onScroll(event) {
 
     let element = this.myScrollContainer.nativeElement;
     // element.style.height = '500px';
     // element.style.height = '500px';
     // Math.ceil(element.scrollHeight - element.scrollTop) === element.clientHeight
 
     if (element.scrollHeight - element.scrollTop - element.clientHeight < 5) {
       if (this.newArray.length > 0) {
         if (this.newArray.length >= this.sum) {
           const start = this.sum;
           this.sum += 50;
           this.addItems(start, this.sum, 'push', this.newArray);
         } else if ((this.newArray.length - this.sum) > 0) {
           const start = this.sum;
           this.sum += this.newArray.length - this.sum;
           this.addItems(start, this.sum, 'push', this.newArray);
         }
       } else {
         if (this.nominationInstances.length >= this.sum) {
           const start = this.sum;
           this.sum += 50;
           this.addItems(start, this.sum, 'push', this.nominationInstances);
         } else if ((this.nominationInstances.length - this.sum) > 0) {
           const start = this.sum;
           this.sum += this.nominationInstances.length - this.sum;
           this.addItems(start, this.sum, 'push', this.nominationInstances);
         }
       }
 
 
       console.log('Reached End');
     }
   }
   newArray =[];
   displayArray =[];
    // this function is use to dynamicsearch on table
  searchData(event) {
    var temData = this.nominationInstances;
    var val = event.target.value.toLowerCase();
    var keys = [];
    this.searchText=val;
    if(event.target.value == '')
    {
      this.displayArray =[];
      this.newArray = [];
      this.sum = 50;
      this.addItems(0, this.sum, 'push', this.nominationInstances);
    }else{
         // filter our data
    if (temData.length > 0) {
      keys = Object.keys(temData[0]);
    }
    if(val.length>=3||val.length==0){
    this.noNominationInstance=false;
    var temp = temData.filter((d) => {
      for (const key of keys) {
        if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
          return true;
        }
      }
    });

    // update the rows
    this.displayArray =[];
    this.newArray = temp;
    this.sum = 50;
    this.addItems(0, this.sum, 'push', this.newArray);
  }
  if(temp.length==0){
    this.noNominationInstance=true;
    this.noDataVal={
      margin:'mt-5',
      imageSrc: '../../../../../assets/images/no-data-bg.svg',
      title:"Sorry we couldn't find any matches please try again",
      desc:".",
      titleShow:true,
      btnShow:false,
      descShow:false,
      btnText:'Learn More',
      btnLink:''
    }
  }
  }
}
btnName:string='Yes'
clickTodisableEep(item,i) {

    if(this.displayArray[i].visible == 1) {
      this.displayArray[i].visible = 0
    } else {
      this.displayArray[i].visible = 1
    }
    this.eepData = item;
    this.clickTodisable(this.eepData);



    // if (item.visible == 1 ) {
    // this.title="Disable Workflow"
    //   this.enable = true;
    //   this.eepData = item;
    //   // this.enableDisableModal = true;
    //   this.enableDisableEepAction(this.enable)
    // } else {
    // this.title="Enable Workflow"
    //   this.enable = false;
    //   this.eepData = item;
    //   // this.enableDisableModal = true;
    //   this.enableDisableEepAction(this.enable)

    // }
}
enableDisableEepAction(actionType) {
  var courseData = this.eepData;
  this.clickTodisable(courseData);


  // if (actionType == true) {

  //   if (this.eepData.visible == 1) {
  //     var courseData = this.eepData;
  //     this.clickTodisable(courseData);
  //   } else {
  //     var courseData = this.eepData;
  //     this.clickTodisable(courseData);
  //   }
  // } else {
  //   this.closeEnableDisableModal();
  // }
}


closeEnableDisableModal() {
  this.enableDisableModal = false;
}
}
