import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
//import io from '../../assets/js/socket';
import io from '../../../../../../assets/js/socket';
// import {ENUM} from '../service/enum';
import { webAPIService } from '../../../../../service/webAPIService';
import {webApi} from '../../../../../service/webApi';
@Injectable()
export class UserChatService{
    socket:any;
    constructor(public http:Http){
        //this.http = http;
        this.socket =  null;
    }
    httpCall(httpData){
        if (httpData.url === undefined || httpData.url === null || httpData.url === ''){
            alert(`Invalid HTTP call`);
        }
        const HTTP = this.http;
        const params = httpData.params;
        // return new Promise( (resolve, reject) => {
        //     HTTP.post(httpData.url, params).then( (response) => {
        //         resolve(response.data);
        //     }).catch( (response, status, header, config) => {
        //         reject(response.data);
        //     });
        // });
        return new Promise(resolve => {
            this.http.post(httpData.url, params)
            .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve(err);
                if(err){
                    //this.toastr.error('Please check server connection','Server Error!');
                    resolve(err);
                }
            });
        });
    }
    connectSocketServer(userId){
        //const socket = io.connect('http://localhost:9845', { query: `userId=${userId}` });
        const socket = io.connect(webApi.domain, { query: `userId=${userId}` });
        this.socket = socket;
    }

    socketEmit(eventName, params){
        this.socket.emit(eventName, params);
    }

    socketOn(eventName, callback) {
        this.socket.on(eventName, (response) => {
            if (callback) {
                callback(response);
            }
        });
    }
    
    getMessages(userId, friendId) {
        return new Promise((resolve, reject) => {
            this.httpCall({
                url: webApi.domain + webApi.url.getChatMessage,//'http://localhost:9845/api/web/getMessages',
                params: {
                    'userId': userId,
                    'toUserId': friendId
                }
            }).then((response) => {
                resolve(response);
            }).catch((error) => {
                reject(error);
            });
        });
    }
    searchUser(name) {
        return new Promise((resolve, reject) => {
            this.httpCall({
                url: webApi.domain + webApi.url.getChatUserList,//'http://localhost:9845/api/web/getChatUserlist',
                params: {
                    'username':name
                }
            }).then((response) => {
                resolve(response);
            }).catch((error) => {
                reject(error);
            });
        });
    }
    loginUser(username, password){
        return new Promise((resolve, reject) => {
        this.httpCall({
            url: webApi.domain + webApi.url.getChatLogin,//'http://localhost:9845/api/web/chatLogin',
            params: {
                'username': username,
                'password': password
            }
        })
        .then((response) => {
            resolve(response);
            //$location.path(`/home/${response.userId}`);
            //$scope.$apply();
        })
        .catch((error) => {
            alert(error.message);
        });
    })
    }

    scrollToBottom(){
        const messageThread = document.querySelector('.message-thread');
        setTimeout(() => {
            messageThread.scrollTop = messageThread.scrollHeight + 500;
        }, 10);        
    }
    
}