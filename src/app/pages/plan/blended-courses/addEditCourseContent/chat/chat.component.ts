import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
// import { throwIfAlreadyLoaded } from '../../@core/module-import-guard';
import { Window } from 'selenium-webdriver';
declare var window: any;
import {FormBuilder, FormGroup, FormControl} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx'; 
declare let userdetail;
import { UserChatService } from './chat.service';
import { ToastrService } from 'ngx-toastr';
// import { ENUM } from '../../service/enum';
import { webAPIService } from '../../../../../service/webAPIService';
import {webApi} from '../../../../../service/webApi';
@Component({
  selector: 'batch-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})

export class ChatComponent implements OnInit {
  //window:any = Window;
  
  myInnerHeight: any = window.innerHeight - 100;
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;

  profilePic: string;
  name: string;
  messagePeview: string;
  lastMsgTime: string;
  pendingMsgs: string;

  activateChat: boolean;


  message: any;

  sendMessage(value) {
    let chat:any = {
      userid: 2,
      userProfilePic: "./assets/images/eva.png",
      message: value,
      contentType: "text",
      datetime: new Date().toISOString(),
      pendingMessage: "",
      unreadMessageCount: []
    }
    //let msg=(<HTMLInputElement>event.target).value;
    //this.chatbox.push(chat);
    this.message = '';
    this.scrollToBottom();
  }


  // chat(chatItem){
  //   for(var i=0; this.chatList.length; i++){
  //     if(chatItem.id==this.chatList[i].id)
  //       {
  //         this.chatbox[i].=this.chatList[i]
  //       }
  //   }
  // }




  prevIndex: number = 0;
  userdetail:any = {};
  searchCtrl = new FormControl();
  filteredUsers: Observable<any[]>;

  users: Observable<any[]>
  isLoading = false;
  myForm: FormGroup;
  chatUser:any = {};
  clientHeight:any =0; 
  spinner:any = false;
  chatInit:any = false;
  constructor(public chatService: UserChatService, public cdf:ChangeDetectorRef,private fb: FormBuilder,private toastr: ToastrService) { 

    
    this.createForm();
    this.filteredUsers = this.searchCtrl.valueChanges
      .startWith(null)
      .debounceTime(200)
      .distinctUntilChanged()
      .switchMap(val => {
        return this.filter(val || '')
      })  

      this.clientHeight = this.myInnerHeight - 130;
  }
  filter(val: string) {
    if(!val){
      return [];
    }else{
      return this.chatService.httpCall({
        url: webApi.domain+webApi.url.getChatList,//'http://localhost:9845/api/web/getChatUserlist',
        params: {
          'username': val
        }
      }).then((response: any) => {
        return response.userlist;
      })
    }
    
  }
  createForm() {
    this.myForm = this.fb.group({
      keyword: ''
       });
  }
  
  activeUser(currentindex, previousindex) {
    if (previousindex || previousindex == 0) {
      this.data.chatlist[previousindex].active = false;
    }
    if (currentindex || currentindex == 0) {
      this.data.chatlist[currentindex].active = true;
    }
    this.prevIndex = currentindex;
  }

  scrollToBottom(): void {
    try {
      setTimeout(() => {
        this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;// + this.myScrollContainer.nativeElement.scrollTop;
      }, 10)

    } catch (err) {
      console.log(err);
    }
  }

  /*Chat */
  ngOnInit() {
    this.activateChat = false;
    this.prevIndex = 0;
    this.data = {
      username: '',
      chatlist: [],
      selectedFriendId: null,
      selectedFriendName: null,
      messages: [],
      invite:false,
      inviteuserid : 0
    };
    if(localStorage.getItem("userdetail")){
      this.userdetail = JSON.parse(localStorage.getItem("userdetail"));
    }
    this.UserId = this.userdetail.userId;
    this.connectSocket(this.UserId);
    this.getChatList(this.UserId);
    this.incommingMessage();
    this.usercomming();
    this.invitationcomming();
    this.userapprovalcomming();
    //this.userSessionCheck(this.UserId);
    //this.initiateCheckUserName();
  }
  UserId:any = "";//$routeParams.userId;
  friendData:any;
  public data:any= {
    username: '',
    chatlist: [],
    selectedFriendId: null,
    selectedFriendName: null,
    messages: [],
    invite:false,
    inviteuserid : 0
  };
  UserList = [
    {
      id: 1,
      name: 'Usa'
    },
    {
      id: 2,
      name: 'England'
    }
 ];
  connectSocket(UserId) {
    this.chatService.connectSocketServer(UserId);
  }

  selectEvent(item) {
    // do something with selected item
  }
 
  onChangeSearch(val: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
    this.chatService.searchUser(val).then((result:any)=>{
      this.UserList = result.userlist;
    }).catch((error)=>{
      this.UserList = [];
    })
  }
  
  onFocused(e){
    // do something when input is focused
  }
  getChatList(UserId){
    this.chatService.httpCall({
      url: webApi.domain+webApi.url.getActiveChatList,//'http://localhost:9845/api/web/getActiveChatList',
      params: {
        'userId': UserId
      }
    }).then((response: any) => {
      this.data.chatlist = response.chatlist;
    })
  }

  userSessionCheck(UserId) {

    this.chatService.httpCall({
      //url: 'http://localhost:9845/api/web/chatUserSessionCheck',
      url: webApi.domain+webApi.url.getActiveChatList,//'http://localhost:9845/api/web/getActiveChatList',
      params: {
        'userId': UserId
      }
    }).then((response: any) => {
      this.data.username = response.username;
      this.chatService.socketEmit(`chat-list`, UserId);
      this.chatService.socketOn('chat-list-response', (response) => {
        //this.$apply( () =>{
          if(localStorage.getItem("userdetail")){
            this.userdetail = JSON.parse(localStorage.getItem("userdetail"));
          }
          var UserId = this.userdetail.userId;
          
        if (!response.error) {
          if (response.singleUser) {
            /* 
            * Removing duplicate user from chat list array
            */
            if (this.data.chatlist.length > 0 && response.chatList.length > 0) {
              this.data.chatlist = this.data.chatlist.filter(function (obj) {
                return obj.userid !== response.chatList[0].userid;// && response.chatlist[0].userid != this.UserId;
              });
            }
            /* 
            * Adding new online user into chat list array
            */
           if(response.chatList){
            // response.chatList.forEach(element => {
            //   this.data.chatlist.push(element);  
            // });
              //this.data.chatlist.push(response.chatList[0]);
              this.data.chatlist = response.chatList.filter(function (obj) {
                return obj.userid !== UserId;
              });
           }
          } else if (response.userDisconnected) {
            /* 
            * Removing a user from chat list, if user goes offline
            */
            this.data.chatlist = this.data.chatlist.filter(function (obj) {
              return obj.socketid !== response.socketId;
            });
          } else {
            /* 
            * Updating entire chatlist if user logs in
            */
           this.data.chatlist = response.chatList.filter(function (obj) {
              return obj.userid !== UserId;
            });
            //this.data.chatlist = response.chatList || [];
          }
          //this.cdf.detectChanges();
        } else {
          alert(`Faild to load Chat list`);
        }
        //});
        console.log("data",this.data);
      });
    })
  }

  incommingMessage() {

    /*
    * This eventt will display the new incmoing message
    */
    this.chatService.socketOn('add-message-response', (response) => {
      //this.$apply(() => {
        if (response && response.fromUserId == this.data.selectedFriendId) {
          response.friendProfilePic = "./assets/images/jack.png";
          this.data.messages.push(response);
          //this.chatService.scrollToBottom();
          this.scrollToBottom();
        }else{
          this.data.chatlist.forEach((user,key)=>{
            if(user.userid === response.fromUserId){
              user.socketid = response.toSocketId;
              user.messagePeview = response.message;
              user.lastMsgTime = new Date();
              if(user.pendingMsgs){
                user.pendingMsgs = user.pendingMsgs + 1;
              }else{
                user.pendingMsgs = 1;
              }
              this.toastr.success( user.username + 'send you a message ');
            }
          })
        }
        try{
          this.cdf.detectChanges();
        }catch(e){
          console.log(e);
        }
      });
    //});
  //})
// .catch((error) => {
//   console.log(error.message);
//   this.$apply(() => {
//     $location.path(`/`);
//   });
// });

  }


selectFriendToChat(friendId) {
  /*
  * Highlighting the selected user from the chat list
  */
 this.chatInit = true;
 this.spinner = true;
 this.data.messages = [];
  const friendData = this.data.chatlist.filter((obj) => {
    return obj.userid === friendId;
  });
  this.data.selectedFriendName = friendData[0]['username'];
  this.data.selectedFriendId = friendId;
  this.chatUser = friendData[0];
  this.data.chatlist[this.prevIndex].pendingMsgs = 0;
  /**
  * This HTTP call will fetch chat between two users
  */
if(this.chatUser.online === undefined){
  this.data.invite = true;
  this.data.inviteuserid = friendId;
  this.data.messages = [];
  this.cdf.detectChanges();
}else{
  if((this.chatUser.accepted === 0 && this.chatUser.rejected === 0 && this.chatUser.invited === 0) || (this.chatUser.accepted === 0 && this.chatUser.rejected === 0 && this.chatUser.invited === 1)){
    this.data.invite = true;
    this.data.inviteuserid = 0;
  }else{
    this.data.invite = false;
  this.data.inviteuserid = 0;
  }
  //userdetail = this.data;
  this.chatService.getMessages(this.UserId, friendId).then((response:any) => {
    //this.$apply(() => {
      if(response.messages.length > 0){
        this.data.messages = response.messages[0];
      }
      this.scrollToBottom();
      this.spinner = false;
      this.cdf.detectChanges();
    //});
  }).catch((error) => {
    console.log(error);
    this.spinner = false;
    alert('Unexpected Error, Contact your Site Admin.');
  });
}
  
}

sendChatMessage(event, value) {

  //if (event.keyCode === 13) {

    let toUserId = null;
    let toSocketId = null;

    /* Fetching the selected User from the chat list starts */
    let selectedFriendId = this.data.selectedFriendId;
    if (selectedFriendId === null) {
      return null;
    }
    this.friendData = this.data.chatlist.filter((obj) => {
      return obj.userid === selectedFriendId;
    });
    /* Fetching the selected User from the chat list ends */

    /* Emmiting socket event to server with Message, starts */
    if (this.friendData.length > 0) {

      toUserId = this.friendData[0]['userid'];
      toSocketId = this.friendData[0]['socketid'];

      let messagePacket = {
        message: value,//document.querySelector('#message').value,
        fromUserId: this.UserId,
        toUserId: toUserId,
        toSocketId: toSocketId,
        userProfilePic:"./assets/images/eva.png",
        friendId : selectedFriendId
      };
      this.data.messages.push(messagePacket);
      // this.chatService.socketEmit(`add-message`, messagePacket);
      this.chatService.socketEmit(`add-message`, messagePacket);
      this.message = '';
      this.scrollToBottom();
      //document.querySelector('#message').value = '';
      //this.chatService.scrollToBottom();
    } else {
      alert('Unexpected Error Occured,Please contact Admin');
    }
    /* Emmiting socket event to server with Message, ends */
  //}
}

alignMessage(fromUserId){
  return fromUserId == this.UserId ? true : false;
}

logout() {
  this.chatService.socketEmit(`logout`,this.UserId);
  //$location.path(`/`);
}

/* usernamme check variables starts*/
TypeTimer:any;
TypingInterval:any = 800;
/* usernamme check variables ends*/


initiateCheckUserName () {
    //$scope.data.usernameAvailable = false;
    //$timeout.cancel(TypeTimer);
    setTimeout(()=>{
      if(this.userdetail.username){
        this.chatService.httpCall({
          url: webApi.domain+webApi.url.checkChatUserName,//'http://localhost:9845/api/web/chatUsernameCheck',
          params: {
              'username': this.userdetail.username
          }
      })
      .then((response) => {
          //$scope.$apply( () =>{
              //$scope.data.usernameAvailable = response.error ? true : false;
          //});
          console.log(response);
      })
      .catch((error) => {
          // $scope.$apply(() => {
          //     $scope.data.usernameAvailable = true;
          // });
          console.log(error);
      });
      }
    }, this.TypingInterval)
}

clearCheckUserName(){
    //$timeout.cancel(TypeTimer);
}
addUserInChatList(user){

  var flag = false;
  this.data.chatlist.forEach((data,kety)=>{
      if(user.userid == data.userid){
        flag = true;
      }
  })
  if(!flag){
    user.actie = false;
    user.username = user.name;
    user.lastMsgTime = new Date();
    this.data.chatlist.push(user);
  }
}
inviteuser(userid, friendid){
  if(userid && friendid){
    this.chatService.httpCall({
      url: webApi.domain + webApi.url.inviteUserForChat,//'http://localhost:9845/api/web/inviteUserForChat',
      params: {
          'userid': userid,
          'frienduserid':friendid
      }
  })
  .then((response:any) => {
      //$scope.$apply( () =>{
          //$scope.data.usernameAvailable = response.error ? true : false;
      //});
      console.log(response);
      if(response.userlist.length > 0){
        this.data.chatlist = response.userlist[0];
      }
      this.activeUser(this.data.chatlist.length - 1,this.prevIndex);
      this.selectFriendToChat(friendid);
      this.sendInvitation();
      this.cdf.detectChanges();
  })
  .catch((error) => {
      // $scope.$apply(() => {
      //     $scope.data.usernameAvailable = true;
      // });
      console.log(error);
  });
  }
}
sendInvitation(){
  if(this.data.chatlist){
    this.data.chatlist.forEach((user, key)=>{
      if(user.userid === this.chatUser.userid){
        //this.chatService.socketEmit(`send-invitation`, {toSocketId: user.socketid, userId:this.chatUser.userid});
        this.chatService.socketEmit(`send-invitation`, {toSocketId: user.socketid, userId:this.UserId});
      }
    })
  }
}
sendapproval(){
  this.chatService.socketEmit(`send-approval`, {toSocketId: this.chatUser.socketid, userId:this.UserId});
}
inviteresponse(userid,accepted,rejected){
  if(userid){
    this.chatService.httpCall({
      url: webApi.domain + webApi.url.inviteChatResponse,//'http://localhost:9845/api/web/inviteResponse',
      params: {
          'userid': userid,
          'frienduserid':this.UserId,
          'rejected':rejected,
          'accepted':accepted
      }
  })
  .then((response:any) => {
      //$scope.$apply( () =>{
          //$scope.data.usernameAvailable = response.error ? true : false;
      //});
      console.log(response);
      if(response.chatlist.length > 0){
        this.data.chatlist = response.chatlist;
      }
      this.activeUser(this.data.chatlist.length - 1,this.prevIndex);
      this.sendapproval();
      this.selectFriendToChat(userid);
      this.cdf.detectChanges();
      
  })
  .catch((error) => {
      // $scope.$apply(() => {
      //     $scope.data.usernameAvailable = true;
      // });
      console.log(error);
  });
  }
}
  /*End Chat*/

  usercomming(){
    this.chatService.socketOn('user-comming-chat-list-response', (response) => {
        
        if(this.data.chatlist){
          this.data.chatlist.forEach((user,key)=>{
            if(response.userinfo.length > 0){
              if(user.userid == response.userinfo[0].userid){
                this.data.chatlist[key].socketid = response.userinfo[0].socketid;
                this.toastr.success( response.userinfo[0].username + ' ' + 'is online');
              }
            }
          })
        }
    })
  }
  userapprovalcomming(){
    this.chatService.socketOn('user-approval-comming-response', (response) => {
        
        if(this.data.chatlist){
          this.data.chatlist.forEach((user,key)=>{
            if(response.userinfo.length > 0){
              if(user.userid == response.userinfo[0].userid){
                this.data.chatlist[key] = response.userinfo[0];
                this.toastr.success( response.userinfo[0].username + ' ' + ' accepted your request');
              }
            }
          })
        }
    })
  }
  invitationcomming(){
    this.chatService.socketOn('invitation-from-user-response', (response) => {
      if(response.userinfo.length > 0){
            this.data.chatlist.push(response.userinfo[0]);
            this.toastr.success( response.userinfo[0].username + ' ' + 'has invited you.');
      }
    })
  }
  
}

