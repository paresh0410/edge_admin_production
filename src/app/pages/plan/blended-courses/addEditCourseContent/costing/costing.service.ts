import {Injectable, Inject} from '@angular/core';
import {Http, Response, Headers, RequestOptions, Request} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppConfig} from '../../../../../app.module';

import { webAPIService } from '../../../../../service/webAPIService'
import { webApi } from '../../../../../service/webApi'
import { HttpClient } from "@angular/common/http";

@Injectable()

export class costingService {

public data: any;
  request: Request;
  userData;
  tenantId;
  private _urlfor_getcost:string = webApi.domain + webApi.url.get_batch_cost;
  private _save_batch_cost: string = webApi.domain + webApi.url.save_batch_cost;
//   private _urlGetCourseDropdown:string = webApi.domain + webApi.url.getBatchDropdown;
//   private _urlCheckCourseCode:string = webApi.domain + webApi.url.checkBatch;


  constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private _http: Http, private http1 :HttpClient ){
      //this.busy = this._http.get('...').toPromise();
        if(localStorage.getItem('LoginResData')){
      this.userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', this.userData.data);
      // this.userId = this.userData.data.data.id;
      this.tenantId = this.userData.data.data.tenantId;
   }
   
  }
  public param:any = {
        tId: this.tenantId,
    };
  // new code start //
  getbatchcost(data){
    return new Promise(resolve => {
      this.http1.post(this._urlfor_getcost, data)
      //.map(res => res.json())
     
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
        });
      });
    }
    savebatchcost(data){
        return new Promise(resolve => {
          this.http1.post(this._save_batch_cost, data)
          //.map(res => res.json())
         
          .subscribe(data => {
              resolve(data);
          },
          err => {
              resolve('err');
            });
          });
        }
  // new code end //

  _errorHandler(error: Response){
     console.error(error);
     return Observable.throw(error || "Server Error")
  }


}
