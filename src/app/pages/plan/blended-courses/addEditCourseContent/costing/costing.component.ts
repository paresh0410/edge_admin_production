import { Component, OnInit, ViewEncapsulation, ChangeDetectorRef, ViewChild, OnChanges,	SimpleChanges, Input } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { XlsxToJsonService } from './xlsx-to-json-service';
import { JsonToXlsxService } from './json-to-xlsx-service';
import { costingService } from '../../addEditCourseContent/costing/costing.service'
import { AddEditCourseContentService } from '../../../courses/addEditCourseContent/addEditCourseContent.service';
import { AddEditBlendCourseContentService } from '../addEditCourseContent.service';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { ToastrService } from 'ngx-toastr';
import { Router, NavigationStart, ActivatedRoute } from "@angular/router";

@Component({
  selector: 'batch-costing',
  templateUrl: './costing.component.html',
  styleUrls: ['./costing.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CostingComponent implements OnInit {
  @ViewChild('myTable') table: any;
  @ViewChild('selfForm') formval: any;
  // @ViewChild(DatatableComponent) costing: DatatableComponent;
	@Input() inpdata: any;

  rows: any = [];
  columns: any = [];
  itemDate = new Date();


  fileData: any = [];
  // { id: 1, date: this.itemDate, itemName: "ABC", cost: 12355 },
  // { id: 2, date: this.itemDate, itemName: "PQR", cost: 12355 },
  // { id: 3, date: this.itemDate, itemName: "XYZ", cost: 12355 },
  // { id: 4, date: this.itemDate, itemName: "LMO", cost: 12355 },
  // { id: 5, date: this.itemDate, itemName: "SKD", cost: 12355 },
  // { id: 6, date: this.itemDate, itemName: "REW", cost: 12355 },

  data: any = [];
  fetchdata: any = [];
  userId: any;
  tenantId: any;
  numVal: any = new RegExp('[0-9]*');

  private xlsxToJsonService: XlsxToJsonService = new XlsxToJsonService();
  constructor(public cost: costingService, private router: Router, private addEditCourseService: AddEditBlendCourseContentService, public cdf: ChangeDetectorRef,
    // private toasterService: ToasterService, 
    private toastr: ToastrService) {
    // this.rows = this.fileData;
    var userData = JSON.parse(localStorage.getItem('LoginResData'));
    console.log('userData', userData.data);
    this.userId = userData.data.data.id;
    this.tenantId = userData.data.data.tenantId;
    this.data = this.addEditCourseService.data.data;
    console.log(this.data);
    // if(this.data)
    // {
    this.getcsting();
    // }
  }

  ngOnInit() {
    // this.getcsting();
  }

  ngOnChanges(changes: SimpleChanges): void {
		if(this.inpdata === 'slot') {
		  this.save();
		}
	  }

  getcsting() {
    this.fetchdata = {
      courseId: this.data ? this.data.courseId : null,
      tId: this.tenantId,
    };
    this.cost.getbatchcost(this.fetchdata).then(res => {
      console.log(res);
      if (res['type'] == true) {
        this.fileData = res['data'][0];
        console.log(this.fileData);
      };
      this.cdf.detectChanges();
    }, err => {
      console.log(err);
    })
  }
  save() {
    if (this.data) {
      console.log(this.fileData);
      console.log('formval', this.formval);
      if(!this.formval.valid) {
        this.toastr.warning('Please fill valid numbers', 'Warning', {
          closeButton: false
        });
        return;
      } else {
      for (let i = 0; i < this.fileData.length; i++) {
        delete this.fileData[i].lineItem;
      }
      var option: string = Array.prototype.map.call(this.fileData, function (item) {
        console.log('item', item)
        return Object.values(item).join('#')
      }).join("|");
      console.log(option);
      var data = {
        courseId: this.data.courseId,
        costData: option,
        tId: this.tenantId,
        userId: this.userId
      }

      console.log(data);
      this.cost.savebatchcost(data).then(res => {
        console.log(res);
        if (res['type'] = true) {
          // var catUpdate: Toast = {
          //   type: 'success',
          //   title: "Cost",
          //   body: res['data'][0][0].msg,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(catUpdate);
          if (this.addEditCourseService.breadcrumbArray) {
            var length = this.addEditCourseService.breadcrumbArray.length - 1
            this.addEditCourseService.breadtitle = this.addEditCourseService.breadcrumbArray[length].name
            // this.addassetservice.title = this.title
            this.addEditCourseService.breadcrumbArray = this.addEditCourseService.breadcrumbArray.slice(0, length)
            // this.title = this.addassetservice.breadCrumbArray[]
            this.addEditCourseService.previousBreadCrumb = this.addEditCourseService.previousBreadCrumb.slice(0, length + 1)
    
          }
          window.history.back()

          this.toastr.success(res['data'][0][0].msg, 'Success', {
            closeButton: false
          });
          this.getcsting();
          // this.router.navigate(["/pages/learning/blended-home/blended-courses"]);
        }
      });
     }
    } else {
      this.toastr.warning('kindly create course', 'Warning', {
        closeButton: false
      });
   }
  }


  //   readmatchimg(event:any,ind) {
  //     var size = 100000;
  //     var validExts = new Array("image");
  //     var fileExt = event.target.files[0].type;
  //     fileExt = fileExt.substring(0,5);
  //     if(size <= event.target.files[0].size){
  //         var toast : Toast = {
  //             type: 'error',
  //             title: "file size exceeded!",
  //             body: "files size should be less than 100KB",
  //             showCloseButton: true,
  //             timeout: 2000

  //         };
  //         // this.toasterService.pop(toast);
  //          event.target.value = "";
  //     }else{
  //       if(validExts.indexOf(fileExt) < 0){
  //         var toast : Toast = {
  //             type: 'error',
  //             title: "Invalid file selected!",
  //             body: "Valid files are of " + validExts.toString() + " types.",
  //             showCloseButton: true,
  //             timeout: 2000
  //         };
  //         // this.toasterService.pop(toast);

  //       }
  //       else
  //       if (event.target.files && event.target.files[0]) {
  //               for(let i =0;i<this.matching.moption.length;i++)
  //               {
  //                 if(i == ind)
  //                 {
  //                    this.matching.moption[i].qfileName=event.target.files[0].name?event.target.files[0].name:'No file Chosen';
  //                    this.matching.moption[i].optionRef=event.target.files[0];
  //                 }

  //               }

  //       }
  //     }
  //   }

  //   cancelmatch(ind){
  //     for(let i =0;i<this.matching.moption.length;i++)
  //               {
  //                 if(i == ind)
  //                 {
  //                     this.matching.moption[i].qfileName="No File Chosen"
  //                     this.matching.moption[i].optionRef=""
  //                 }

  //               }
  //   }

}


