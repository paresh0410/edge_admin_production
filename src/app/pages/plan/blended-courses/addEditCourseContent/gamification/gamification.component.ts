import { Host, ChangeDetectionStrategy, Component, ViewEncapsulation, Directive,forwardRef,Attribute,OnChanges,SimpleChanges,Input, ViewChild, ViewContainerRef,OnInit } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AddEditBlendCourseContent } from '../addEditCourseContent';
import { AddEditBlendCourseContentService } from '../addEditCourseContent.service';
import { laddersService } from '../../../../gamification/ladders/ladders.service';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import {GamificationBlendService  } from './gamification.service';
import { NgxSpinnerService } from 'ngx-spinner';



@Component ({
   selector: 'batch-gamification',
   templateUrl: './gamification.component.html',
   styleUrls: ['./gamification.component.scss'],
   encapsulation: ViewEncapsulation.None
})

export class GamificationBlendComponent {
ladder=[];

ladderIds:any =[];
userId:any;
optId:any;
courseId:any;
errorMsg:any;
tenantId:any;
    constructor(@Host() private parent_Comp: AddEditBlendCourseContent,
    // private toasterService: ToasterService,
    private gamificationservice:GamificationBlendService, private addEditCourseService: AddEditBlendCourseContentService,
    private ladderservice: laddersService,private spinner: NgxSpinnerService) {
        this.fetchLadders();
        if(this.addEditCourseService.data !=undefined){
            console.log('this.addEditCourseService.data for Edit:',this.addEditCourseService.data);
            this.ChooseAddEdit(this.addEditCourseService.data);
          }

          if(localStorage.getItem('LoginResData')){
            var userData = JSON.parse(localStorage.getItem('LoginResData'));
          console.log('userData',userData.data);
          this.userId = userData.data.data.id;
          console.log('userId',userData.data.data.id);
          this.tenantId = userData.data.data.tenantId;
          }

    }

    ChooseAddEdit(data){
        this.optId = data.id;

        if(this.optId == 0){
          this.courseId = this.addEditCourseService.courseId;
        }else if(this.optId == 1){
          this.courseId = this.addEditCourseService.data.data.courseId;
          this.getExistingCourseNotification();
        }
      }

      getExistingCourseNotification(){
        let param ={
            "cId":this.courseId,
            "tId":  this.tenantId ,
          }

          this.gamificationservice.getcourseladders(param)
          .then(rescompData => {
            console.log('rescompData',rescompData);
            var result = rescompData;
            var ladders = result['data'][0];
            console.log('ladders',ladders);
            if(result['type'] == true){
              console.log('ladders',ladders);
              for(let i=0;i<this.ladder.length;i++){
                  for(let j=0;j<ladders.length;j++){
                      if(this.ladder[i].ladderId == ladders[j].ladderId){
                          this.ladder[i].activate = true;
                          let ladderObj = {
                            ladderid:ladders[j].ladderId,
                        }
                          this.ladderIds.push(ladderObj);
                      }
                  }
              }
              console.log('this.ladderIdsOld',this.ladderIds)
              console.log('this.ladderNew',this.ladder)
            }else{
              this.errorMsg = rescompData;
            }
          },
          resUserError => {
            // this.loader =false;
            this.errorMsg = resUserError
          });
      }


    fetchLadders(){
        let param = {
            "tenantId":  this.tenantId ,
            "courseId":1
        }

        this.ladderservice.getLadders(param)
        .then(rescompData => {

            var result = rescompData;
            console.log('getladderResponse:',rescompData);
            if(result['type'] == true){
              this.ladder = result['data'][0]
              console.log('this.ladder',this.ladder);
              //this.loader = false;
            }else{
                 // this.loader = false;
              //   var toast : Toast = {
              //     type: 'error',
              //     //title: "Server Error!",
              //     body: "Something went wrong.please try again later.",
              //     showCloseButton: true,
              //     timeout: 2000
              // };
              // this.toasterService.pop(toast);
            }


          },error=>{
              //this.loader = false;
            //   var toast : Toast = {
            //     type: 'error',
            //     //title: "Server Error!",
            //     body: "Something went wrong.please try again later.",
            //     showCloseButton: true,
            //     timeout: 2000
            // };
            // this.toasterService.pop(toast);
          });
    }

    // onClickCheckbox(ladderId){
    //     console.log(ladderId);
    //     let ladderObj = {
    //         ladderid:ladderId,
    //     }
    //     this.ladderIds.push(ladderObj);
    // }

    saveLaddersForCoures(){
       var formattedsLadders =  this.getReadydataforsavecourse(this.ladderIds)
        console.log(this.ladderIds);
        console.log('this.addEditCourseService.data',this.addEditCourseService.data);
        let param = {
            "cId":this.addEditCourseService.data.data.courseId,
            "tId":  this.tenantId ,
            "userId":this.userId,
            "dispname":formattedsLadders
        }

        this.gamificationservice.insertladder(param)
        .then(rescompData => {

            if(this.optId == 0){
                var result = rescompData;
                console.log('result', rescompData);
                console.log('insertLadderResponse:', rescompData);
                if (result['type'] == true) {
                // var toast: Toast = {
                //     type: 'success',
                //     //title: "Server Error!",
                //     body: "Ladders added to course.",
                //     showCloseButton: true,
                //     timeout: 2000
                // };
                // this.toasterService.pop(toast);

                // this.loader = false;
                } else {
                // this.loader = false;
                // var toast: Toast = {
                //     type: 'error',
                //     //title: "Server Error!",
                //     body: "Unable to add ladders.",
                //     showCloseButton: true,
                //     timeout: 2000
                // };
                // this.toasterService.pop(toast);
                }
            }else if(this.optId == 1){
                var result = rescompData;
                console.log('result', rescompData);
                console.log('insertLadderResponse:', rescompData);
                if (result['type'] == true) {
                //   var toast: Toast = {
                //     type: 'success',
                //     //title: "Server Error!",
                //     body: "Ladders updated to course.",
                //     showCloseButton: true,
                //     timeout: 2000
                //   };
                //  this.toasterService.pop(toast);

                 // this.loader = false;
                } else {
                 // this.loader = false;
                //   var toast: Toast = {
                //     type: 'error',
                //     //title: "Server Error!",
                //     body: "Unable to update ladders.",
                //     showCloseButton: true,
                //     timeout: 2000
                //   };
                //  this.toasterService.pop(toast);
                }
            }



          }, error => {
           // this.loader = false;
           console.log('resulterror', error);

            // var toast: Toast = {
            //   type: 'error',
            //   //title: "Server Error!",
            //   body: "Something went wrong.please try again later.",
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(toast);
          });
    }

    getReadydataforsavecourse(ladderIdsArr){
        for(let i=0;i<ladderIdsArr.length;i++){
            var str = ladderIdsArr[i].ladderid;
            if(i==0){
                var strArr = str;
            }else{
                strArr += "|"+str;
            }
        }
        return strArr;
    }


    activeSelectedLadders:any;
    setActiveSelectedLadders(currentIndex,currentLadder){
        if(this.ladder[currentIndex].activate == true){
            this.ladder[currentIndex].activate = false;
            for(let i=0;i<this.ladderIds.length;i++){
                if(this.ladderIds[i].ladderid == currentLadder.ladderId){
                    this.ladderIds.splice(i,1);
                }
            }
            console.log('this.ladderIds',this.ladderIds)
        }else{
            for(let i=0;i<this.ladder.length;i++){
                if(this.ladder[i].ladderId == currentLadder.ladderId){
                    this.ladder[i].activate = true;
                    let ladderObj = {
                        ladderid:currentLadder.ladderId,
                    }
                    this.ladderIds.push(ladderObj);
                }
            }
            console.log('this.ladderIds',this.ladderIds)
        }

    }


}
