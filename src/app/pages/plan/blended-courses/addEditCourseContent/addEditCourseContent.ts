import { ElementRef, Host, ChangeDetectionStrategy, Component, ChangeDetectorRef, ViewEncapsulation, Directive, forwardRef, Attribute, OnChanges, SimpleChanges, Input, ViewChild, ViewContainerRef, OnInit } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router, NavigationStart, ActivatedRoute } from '@angular/router';
import { NgbCalendar, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { enrolBlendService } from './enrolment/enrolment.service';
import { AddEditBlendCourseContentService } from './addEditCourseContent.service';
import { FormGroup, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { ToastrService } from 'ngx-toastr';

import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { contentReady } from '@syncfusion/ej2-grids';
import { BlendedCoursesComponent } from '../blended-courses.component';
import { AddEditCourseContentService } from '../../courses/addEditCourseContent/addEditCourseContent.service';
import { DatePipe } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';
import { XlsxToJsonService } from '../../../plan/users/uploadusers/xlsx-to-json-service';
import { JsonToXlsxService } from '../../../coaching/participants/bulk-upload-coaching/json-to-xlsx.service';
import { CommonFunctionsService } from '../../../../service/common-functions.service';
import { webApi } from '../../../../service/webApi';
import { SuubHeader } from '../../../components/models/subheader.model';
import { BrandDetailsService } from '../../../../service/brand-details.service';
import { noData } from '../../../../models/no-data.model';
import { BlendedService } from '../blended.service';
@Component({
  selector: 'addEditCourseContent',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './addEditCourseContent.html',
  styleUrls: ['./addEditCourseContent.scss', './myDatePickerComp.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [DatePipe],
})




export class AddEditBlendCourseContent {

  colorTheme = 'theme-dark-blue';

  header: SuubHeader;

  bsConfig: Partial<BsDatepickerConfig>

  addEngagePopup: boolean;
  selectMethod: String;

  model: NgbDateStruct;
  today = this.calendar.getToday();
  enrolldata: any;
  // @Input() detailsComp : detailsComponent;
  // @ViewChild(detailsComponent ) detailsComp: detailsComponent ;

  @ViewChild('fileUpload') fileUpload: any;
  @ViewChild('myTable') table: any;
  @ViewChild(DatatableComponent) tableData: DatatableComponent;
  dropdownListUsers: any;
  selectedItemsUsers: any;
  dropdownSettingsUsers: any;
  demoData: any = [];
  resultdata: any = [];
  selected: any = [];
  rows: any = [];
  temp = [];

  showdate: boolean = false;
  showdays: boolean = false;

  // columns = [
  //   { prop: 'name' },
  //   { name: 'Company' },
  //   { name: 'Gender' }
  // ];
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No enrollment at this time.",
    desc:"",
    titleShow:true,
    btnShow:true,
    descShow:false,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/learning-how-to-enrol-user-to-a-batch',
  }
  labels: any = [
    { labelname: 'ECN', bindingProperty: 'ecn', componentType: 'text' },
    { labelname: 'FULL NAME', bindingProperty: 'fullname', componentType: 'text' },
    { labelname: 'EMAIL', bindingProperty: 'emailId', componentType: 'text' },
    { labelname: 'MOBILE', bindingProperty: 'phoneNo', componentType: 'text' },
    { labelname: 'D.0.E', bindingProperty: 'enrolDate', componentType: 'text' },
    { labelname: 'MODE', bindingProperty: 'enrolmode', componentType: 'text' },
    { labelname: 'ACTION', bindingProperty: 'btntext', componentType: 'button' },
  ];
  // rows: any = [
  //   { 'empcode': 'ecn', 'Fullname': 'fullname', 'email':'emailId','number':'phoneNo','date':'enrolDate','mode':'enrolmode','button' :'btntext' },
  // ];
  columns = [
    { prop: 'ecn', name: 'EMP CODE' },
    { prop: 'fullname', name: 'FULLNAME' },
    { prop: 'gender', name: 'GENDER' },
    { prop: 'doj', name: 'DOJ' },
    { prop: 'department', name: 'DEPARTMENT' },
    { prop: 'mode', name: 'MODE' }
  ];

  showEnrolpage: boolean = false;
  showBulkpage: boolean = false;

  reviewCheck: any = {
    value1: false,
    value2: false,
    value3: false,
  }

  enrolment: any = {
    manual: '',
    rule: '',
    regulatory: '',
    self: ''
  };

  searchvalue: any = {
    value: ''
  };

  courseDetails: boolean = true;
  courseModules: boolean = false;
  courseEnrol: boolean = false;
  courseEngage: boolean = false;
  courseRewards: boolean = false;
  courseGamification = false;

  detailsTab: boolean = true;
  modulesTab: boolean = false;
  enrolTab: boolean = false;
  engageTab: boolean = false;
  rewardsTab: boolean = false;
  gamificationTab: boolean = false;
  enableCourse: boolean = false;
  enablefield: boolean = false;
  showAddRuleModal: boolean = false;
  showAddRegulatoryModal: boolean = false;
  showAddSelfModal: boolean = false;

  enableDisableCourseModal: boolean = false;
  enableDisableCourseData: any;
  courseDisableIndex: any;

  ruleType: any = [{
    ruleTypeId: 1,
    ruleTypeName: 'Profile Fields'
  }, {
    ruleTypeId: 2,
    ruleTypeName: 'Other'
  }];

  ruleSubType: any = [{
    ruleTypeId: 1,
    ruleSubTypeId: 1,
    ruleSubTypeName: 'Username',
  }, {
    ruleTypeId: 1,
    ruleSubTypeId: 2,
    ruleSubTypeName: 'Department',
  }, {
    ruleTypeId: 2,
    ruleSubTypeId: 1,
    ruleSubTypeName: 'Doj',
  }, {
    ruleTypeId: 2,
    ruleSubTypeId: 2,
    ruleSubTypeName: 'Custom date',
  }];

  ruleData: any = {
    id: '',
    name: '',
    description: '',
    type: '',
    subType: '',
    value: ''
  };

  formdata: any = {
    id: '',
    shortname: '',
    name: '',
    datatype: '',
    selected: ''
  }
  content: any = [];
  strArrayType: any = [[]];
  selectedFilterOption = [];
  strArrayPar: any = [];

  public addRulesForm: FormGroup;
  controlList: any = [{ datatype: '' }];
  controlFlag: any = false;

  profileFields: any = [];
  errorMsg: any;
  loader: any;

  private ValueId: number = 0;
  strArrayTypePar: any = [];
  // selectedFilterOption = [];
  selectedRule: any = [];
  // ruleType:any
  selectedRuleType: any = {
    id: ''
  };

  profileFieldSelected: boolean = false;

  strArraySkilllevel: any = [{
    sName: 'Beginner',
    sId: 'Beginner'
  },
  {
    sName: 'Intermediate',
    sId: 'Intermediate'
  },
  {
    sName: 'Expert',
    sId: 'Expert'
  }]

  menuType = [];
  datetimeType = [];
  textType = [];
  textareaType = [];

  strArrayTypeSelfFields: any = [[]];
  selectedFilterOptionSelf = [];
  strArrayParSelfFields: any = [];

  public addSelfFieldsForm: FormGroup;
  controlListSelfFields: any = [{ datatype: '' }];
  controlFlagSelfFields: any = false;

  profileFieldsSelf: any = [];
  enabledata: any = [];
  enableuser: any = [];
  selfType: any = [{
    typeId: 1,
    typeName: 'Open'
  }, {
    typeId: 2,
    typeName: 'Approval'
  }];

  selfFeildType: any = [{
    selfTypeId: 1,
    selfTypeName: 'Profile Fields'
  }, {
    selfTypeId: 2,
    selfTypeName: 'Other'
  }];

  selfFieldsData: any = {
    id: '',
    name: '',
    description: '',
    type: '',
    subType: '',
    value: '',
    maxCount: ''
  };

  formdataSelf: any = {
    id: '',
    shortname: '',
    name: '',
    datatype: '',
    selected: ''
  }

  // ruleType:any
  selectedSelfFieldsType: any = {
    id: ''
  };

  selfProfileFieldSelected: boolean = false;
  private selfFieldValueId: number = 0;
  strArrayTypeParSelfFields: any = [];
  // selectedFilterOption = [];
  selectedSelfFields: any = [];

  // @Host() private details_Comp: detailsComponent,
  private xlsxToJsonService: XlsxToJsonService = new XlsxToJsonService();
  @ViewChild('addEditCourseTabs') activityForm: ElementRef;
  selectedActInd: any = 0;

  batchDetails: boolean;
  batchSession: boolean;
  batchEnrol: boolean;
  batchNotification: boolean;
  batchRewards: boolean;
  barchChat: boolean;
  batchWorkflow: boolean;
  batchCosting: boolean;
  /*BULk ENROL*/
  fileName: any;
  fileReaded: any;
  enableUpload: any;
  file: any = [];
  datasetPreview: any;
  preview: boolean = false;
  resultSheets: any = [];
  result: any = [];
  uploadedData: any = [];
  bulkUploadData: any = [];
  fileUrl: any;
  validdata: any = [];
  invaliddata: any = [];
  AllEmployess: any = [];
  empobj: any;
  nomiObj: any;
  showInvalidExcel: boolean = false;
  addEditModRes: any = [];
  userId: any;
  tenantId: any;
  backFlag: number;
  showPagination = true;

  isdata: any;
  searchText: any;
  currentBrandData: any;
  nodata: boolean = true;
  tabTitle: any;

  constructor(private calendar: NgbCalendar, private _fb: FormBuilder,
    private addEditCourseContentService: AddEditCourseContentService, private toastr: ToastrService,
    protected courseDataService: AddEditBlendCourseContentService, vcr: ViewContainerRef,
    protected contentservice: BlendedService,
    private router: Router, private route: ActivatedRoute,
    public brandService: BrandDetailsService,
    // private toasterService: ToasterService,
    public cdf: ChangeDetectorRef, private datePipe: DatePipe, private spinner: NgxSpinnerService,
    private commonFunctionService: CommonFunctionsService,
    private exportService: JsonToXlsxService) {
    var userData = JSON.parse(localStorage.getItem('LoginResData'));
    console.log('userData', userData.data);
    this.userId = userData.data.data.id;
    this.tenantId = userData.data.data.tenantId;
    // this.courseDataService.data = {data:{}};
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });

    this.demoData = [{ "id": 1, "itemName": "India" },
    { "id": 2, "itemName": "Singapore" },
    { "id": 3, "itemName": "Australia" },
    { "id": 4, "itemName": "Canada" },
    { "id": 5, "itemName": "South Korea" },
    { "id": 6, "itemName": "Brazil" }];
    this.dropdownListUsers = this.demoData;
    this.selectedItemsUsers = [];
    this.dropdownSettingsUsers = {
      singleSelection: false,
      text: "Users",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      badgeShowLimit: 2,
      classes: "myclass custom-class"
    };

    this.fetch((data) => {
      // cache our list
      this.temp = [...data];
      this.rows = data;

    });

    var content;
    var optId;
    var catId;
  }

  addRemoveClassElement(element: any, classname) {
    if (element) {
      var elementContent = element.nativeElement;
      if (elementContent.classList.length > 0) {
        var index = elementContent.className.indexOf(classname);
        if (index > -1) { //exist
          elementContent.classList.remove(classname);
        } else {
          elementContent.classList.add(classname);
        }
      }
    }
  }
  clearesearch() {
    if (this.searchText.length >= 3) {
      this.searchText = '';
      this.searchvalue = {};
      this.allEnrolUser(this.content);
    } else {
      this.searchvalue = {};

    }
  }
  fetch(cb) {
    // const req = new XMLHttpRequest();
    // // req.open('GET', `assets/data/company.json`);
    // req.open('GET', `assets/data/enroledUsers.json`);
    // // req.open('GET', `assets/data/100k.json`);

    // req.onload = () => {
    //   cb(JSON.parse(req.response));
    // };

    // req.send();
  }

  onSelectEnrolData({ selected }) {
    console.log('Select Event', selected);
    // console.log('Select Data', this.selected);

    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }

  searchEnrolUser(event) {
    const val = event.target.value.toLowerCase();

    // this.allEnrolUser( this.courseDataService.data.data)

    this.temp = [...this.enrolldata];
    console.log(this.temp);
    // filter our data
    this.searchText = val;
    if (val.length >= 3 || val.length == 0) {
      const temp = this.temp.filter(function (d) {
        return String(d.ecn).toLowerCase().indexOf(val) !== -1 ||
          d.fullname.toLowerCase().indexOf(val) !== -1 ||
          d.emailId.toLowerCase().indexOf(val) !== -1 ||
          d.enrolmode.toLowerCase().indexOf(val) !== -1 ||
          d.enrolDate.toLowerCase().indexOf(val) !== -1 ||
          d.phoneNo.toLowerCase().indexOf(val) !== -1 ||
          String(d.enrolmode).toLowerCase().indexOf(val) !== -1 ||
          // d.mode.toLowerCase() === val || !val;
          !val
      });

      // update the rows
      this.rows = [...temp];
      if(this.rows.length == 0){
        this.nodata = true
      }
      else{
        this.nodata = false
      }
    }
    // Whenever the filter changes, always go back to the first page
    this.tableData.offset = 0;
  }

  onEnrolDataClickEvent(event) {
    // if (event.type == 'click' && event.cellIndex != 0) {
    //     // Do something when you click on row cell other than checkbox.
    //     console.log('Activate Event', event);
    // }
    // const checkboxCellIndex = 1;
    if (event.type === 'checkbox') {
      // Stop event propagation and let onSelect() work
      console.log('Checkbox Selected', event);
      event.event.stopPropagation();
    } else if (event.type === 'click' && event.cellIndex != 0) {
      // Do somethings when you click on row cell other than checkbox
      console.log('Row Clicked', event.row); /// <--- object is in the event row variable
    }
  }

  deleteEnrolData(selectedRow) {
    console.log('Current User', selectedRow);
    for (let i = 0; i < this.rows.length; i++) {
      var row = this.rows[i];
      if (selectedRow.ecn == row.ecn) {
        this.rows.splice(i, 1);
        this.rows = [...this.rows];
      }
    }
    this.temp = this.rows;
    // this.tableData.offset = 0;
  }

  timeout: any;
  onPageEnrolData(event) {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      console.log('paged!', event);
    }, 100);
  }

  enrolUser() {
    this.showEnrolpage = !this.showEnrolpage;
  }

  toggleExpandRow(row) {
    // console.log('Toggled Expand Row!', row);
    this.table.rowDetail.toggleExpandRow(row);
  }

  onDetailToggle(event) {
    // console.log('Detail Toggled', event);
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function (d) {
      return d.name.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    this.tableData.offset = 0;
  }

  onCheckBoxClick(event, courseReviewCheck) {
    if (event == false) {
      this.reviewCheck = {};
    }
    // console.log('$event',$event);
    console.log('courseReviewCheck', courseReviewCheck);

  }

  setActiveItem(index, item) {
    console.log("Selected Module", item, index);
  }




  selectedTab(tabEvent) {
    this.tabTitle = tabEvent.tabTitle
    console.log('tab Selected', tabEvent);

    if (tabEvent.tabTitle == "Details") {

      this.header = {
        title: this.content?this.content.fullname:'Add Batch',
        btnsSearch: true,
        btnName4: 'Save & Next',
        btnName4show: true,
        btnBackshow: true,
        showBreadcrumb: true,
      }
         this.header.breadCrumbList=this.courseDataService.data.catId ?
          [
            // {
            //   'name': 'Learning',
            //   'navigationPath': '/pages/learning',
            // },
            {
              'name': 'Blended Learning',
              'navigationPath': '/pages/blended-home',
            },
            {
              'name': 'Blended Category',
              'navigationPath': '/pages/blended-home/blended-category',
            },
            {
              'name': this.courseDataService.data.categoryName,
              'navigationPath': '/pages/blended-home/blended-courses',
            },
          ]:
          [
            // {
            //   'name': 'Learning',
            //   'navigationPath': '/pages/learning',
            // },
            {
              'name': 'Blended Learning',
              'navigationPath': '/pages/blended-home',
            },
            {
              'name': 'Batches',
              'navigationPath': '/pages/blended-home/blended-courses',
            },
          ]
          if(this.courseDataService.breadcrumbArray){
            this.header.title = this.courseDataService.breadtitle
            this.header.breadCrumbList = this.courseDataService.breadcrumbArray
          }
          // this.spinner.show();
          // setTimeout(() => {
          //   this.spinner.hide()
          // }, 2000);
      this.backFlag = 1;
      this.batchDetails = true;
      this.batchSession = false;
      this.batchEnrol = false;
      this.batchRewards = false;
      this.batchNotification = false;
      this.barchChat = false;
      this.batchWorkflow = false;
      this.batchCosting = false;
    } else if (tabEvent.tabTitle == "Sessions") {
      this.header = {
        title: this.content?this.content.fullname:'Add Batch',
        btnsSearch: true,
        btnBackshow: true,
        showBreadcrumb: true,
        btnName5:'Add Session',
        btnName5show:true,
      };
        this.header.breadCrumbList=this.courseDataService.data.catId ?
        [
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          {
            'name': 'Blended Learning',
            'navigationPath': '/pages/blended-home',
          },
          {
            'name': 'Blended Category',
            'navigationPath': '/pages/blended-home/blended-category',
          },
          {
            'name': this.courseDataService.data.categoryName,
            'navigationPath': '/pages/blended-home/blended-courses',
          },
        ]:
        [
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          {
            'name': 'Blended Learning',
            'navigationPath': '/pages/blended-home',
          },
          {
            'name': 'Batches',
            'navigationPath': '/pages/blended-home/blended-courses',
          },
        ]
        if(this.courseDataService.breadcrumbArray){
          this.header.title = this.courseDataService.breadtitle
          this.header.breadCrumbList = this.courseDataService.breadcrumbArray
        }
        // this.spinner.show();
        // setTimeout(() => {
        //   this.spinner.hide()
        // }, 2000);
      this.backFlag = 1;
      this.batchDetails = false;
      this.batchSession = true;
      this.batchEnrol = false;
      this.batchRewards = false;
      this.batchNotification = false;
      this.barchChat = false;
      this.batchWorkflow = false;
      this.batchCosting = false;
    } else if (tabEvent.tabTitle == "Enrol") {
      this.header = {
        title: this.content?this.content.fullname:'Add Batch',
        btnsSearch: true,
        btnName2: 'Enrol',
        btnName3: 'Bulk Enrol',
        btnName2show: true,
        btnName3show: true,
        btnBackshow: true,
        showBreadcrumb: true,
      };
        this.header.breadCrumbList=this.courseDataService.data.catId ?
        [
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          {
            'name': 'Blended Learning',
            'navigationPath': '/pages/blended-home',
          },
          {
            'name': 'Blended Category',
            'navigationPath': '/pages/blended-home/blended-category',
          },
          {
            'name': this.courseDataService.data.categoryName,
            'navigationPath': '/pages/blended-home/blended-courses',
          },
        ]:
        [
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          {
            'name': 'Blended Learning',
            'navigationPath': '/pages/blended-home',
          },
          {
            'name': 'Batches',
            'navigationPath': '/pages/blended-home/blended-courses',
          },
        ]
        if(this.courseDataService.breadcrumbArray){
          this.header.title = this.courseDataService.breadtitle
          this.header.breadCrumbList = this.courseDataService.breadcrumbArray
        }
        // this.spinner.show();
        // setTimeout(() => {
        //   this.spinner.hide()
        // }, 2000);
      this.backFlag = 2;
      this.batchDetails = false;
      this.batchSession = false;
      this.batchEnrol = true;
      this.allEnrolUser(this.content);
      this.batchRewards = false;
      this.batchNotification = false;
      this.barchChat = false;
      this.batchWorkflow = false;
      this.batchCosting = false;
    } else if (tabEvent.tabTitle == "Notification") {
      this.header = {
        title: this.content?this.content.fullname:'Add Batch',
        btnsSearch: true,
        btnBackshow: true,
        btnName8:'Add Notification',
        btnName8show:true,
        showBreadcrumb: true,
      };
        this.header.breadCrumbList=this.courseDataService.data.catId ?
        [
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          {
            'name': 'Blended Learning',
            'navigationPath': '/pages/blended-home',
          },
          {
            'name': 'Blended Category',
            'navigationPath': '/pages/blended-home/blended-category',
          },
          {
            'name': this.courseDataService.data.categoryName,
            'navigationPath': '/pages/blended-home/blended-courses',
          },
        ]:
        [
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          {
            'name': 'Blended Learning',
            'navigationPath': '/pages/blended-home',
          },
          {
            'name': 'Batches',
            'navigationPath': '/pages/blended-home/blended-courses',
          },
        ]
        if(this.courseDataService.breadcrumbArray){
          this.header.title = this.courseDataService.breadtitle
          this.header.breadCrumbList = this.courseDataService.breadcrumbArray
        }
      this.backFlag = 1;
      // this.spinner.show();
      // setTimeout(() => {
      //   this.spinner.hide()
      // }, 2000);
      this.batchDetails = false;
      this.batchSession = false;
      this.batchEnrol = false;
      this.batchRewards = false;
      this.batchNotification = true;
      this.barchChat = false;
      this.batchWorkflow = false;
      this.batchCosting = false;
    } else if (tabEvent.tabTitle == "Rewards") {
      this.header = {
        title: this.content?this.content.fullname:'Add Batch',
        btnsSearch: true,
        btnName9:'Save',
        btnName9show:true,
        btnBackshow: true,
        showBreadcrumb: true,
      };
        this.header.breadCrumbList=this.courseDataService.data.catId ?
        [
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          {
            'name': 'Blended Learning',
            'navigationPath': '/pages/blended-home',
          },
          {
            'name': 'Blended Category',
            'navigationPath': '/pages/blended-home/blended-category',
          },
          {
            'name': this.courseDataService.data.categoryName,
            'navigationPath': '/pages/blended-home/blended-courses',
          },
        ]:
        [
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          {
            'name': 'Blended Learning',
            'navigationPath': '/pages/blended-home',
          },
          {
            'name': 'Batches',
            'navigationPath': '/pages/blended-home/blended-courses',
          },
        ]
        if(this.courseDataService.breadcrumbArray){
          this.header.title = this.courseDataService.breadtitle
          this.header.breadCrumbList = this.courseDataService.breadcrumbArray
        }
        // this.spinner.show();
        // setTimeout(() => {
        //   this.spinner.hide()
        // }, 2000);
      this.backFlag = 1;
      this.batchDetails = false;
      this.batchSession = false;
      this.batchEnrol = false;
      this.batchRewards = true;
      this.batchNotification = false;
      this.barchChat = false;
      this.batchWorkflow = false;
      this.batchCosting = false;
    } else if (tabEvent.tabTitle == "Chat") {
      this.header = {
        title: this.content?this.content.fullname:'Add Batch',
        btnsSearch: true,
        btnBackshow: true,
        showBreadcrumb: true,
      };
        this.header.breadCrumbList=this.courseDataService.data.catId ?
        [
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          {
            'name': 'Blended Learning',
            'navigationPath': '/pages/blended-home',
          },
          {
            'name': 'Blended Category',
            'navigationPath': '/pages/blended-home/blended-category',
          },
          {
            'name': this.courseDataService.data.categoryName,
            'navigationPath': '/pages/blended-home/blended-courses',
          },
        ]:
        [
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          {
            'name': 'Blended Learning',
            'navigationPath': '/pages/blended-home',
          },
          {
            'name': 'Batches',
            'navigationPath': '/pages/blended-home/blended-courses',
          },
        ]
        if(this.courseDataService.breadcrumbArray){
          this.header.title = this.courseDataService.breadtitle
          this.header.breadCrumbList = this.courseDataService.breadcrumbArray
        }
        // this.spinner.show();
        // setTimeout(() => {
        //   this.spinner.hide()
        // }, 2000);
      this.backFlag = 1;
      this.batchDetails = false;
      this.batchSession = false;
      this.batchEnrol = false;
      this.batchRewards = false;
      this.batchNotification = false;
      this.barchChat = true;
      this.batchWorkflow = false;
      this.batchCosting = false;
    } else if (tabEvent.tabTitle == "Workflow") {
      this.header = {
        title: this.content?this.content.fullname:'Add Batch',
        btnsSearch: true,
        btnBackshow: true,
        showBreadcrumb: true,
      };
      if(this.courseDataService.data.catId){
        this.header.breadCrumbList=this.courseDataService.data.catId?
        [
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          {
            'name': 'Blended Learning',
            'navigationPath': '/pages/blended-home',
          },
          {
            'name': 'Blended Category',
            'navigationPath': '/pages/blended-home/blended-category',
          },
          {
            'name': this.courseDataService.data.categoryName,
            'navigationPath': '/pages/blended-home/blended-courses',
          },
        ]:
        [
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          {
            'name': 'Blended Learning',
            'navigationPath': '/pages/blended-home',
          },
          {
            'name': 'Batches',
            'navigationPath': '/pages/blended-home/blended-courses',
          },
        ]
        if(this.courseDataService.breadcrumbArray){
          this.header.title = this.courseDataService.breadtitle
          this.header.breadCrumbList = this.courseDataService.breadcrumbArray
        }}else{
          this.header.breadCrumbList= [
            // {
            //   'name': 'Learning',
            //   'navigationPath': '/pages/learning',
            // },
            
            {
              'name': 'Blended Learning',
              'navigationPath': '/pages/blended-home',
            },
            {
              'name': 'Batches',
              'navigationPath': '/pages/blended-home/blended-courses',
            },
          ]
          if(this.courseDataService.breadcrumbArray){
            this.header.title = this.courseDataService.breadtitle
            this.header.breadCrumbList = this.courseDataService.breadcrumbArray
          }
        }
        // this.spinner.show();
        // setTimeout(() => {
        //   this.spinner.hide()
        // }, 2000);
      this.backFlag = 1;
      this.batchDetails = false;
      this.batchSession = false;
      this.batchEnrol = false;
      this.batchRewards = false;
      this.batchNotification = false;
      this.barchChat = false;
      this.batchWorkflow = true;
      this.batchCosting = false;
    } else if (tabEvent.tabTitle == "Costing") {
      this.header = {
        title: this.content?this.content.fullname:'Add Batch',
        btnsSearch: true,
        btnName10:'Save',
        btnName10show:true,
        btnBackshow: true,
        showBreadcrumb: true,
      };
        this.header.breadCrumbList=this.courseDataService.data.catId?
        [
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          {
            'name': 'Blended Learning',
            'navigationPath': '/pages/blended-home',
          },
          {
            'name': 'Blended Category',
            'navigationPath': '/pages/blended-home/blended-category',
          },
          {
            'name': this.courseDataService.data.categoryName,
            'navigationPath': '/pages/blended-home/blended-courses',
          },
        ]:
        [
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          {
            'name': 'Blended Learning',
            'navigationPath': '/pages/blended-home',
          },
          {
            'name': 'Batches',
            'navigationPath': '/pages/blended-home/blended-courses',
          },
        ]
        if(this.courseDataService.breadcrumbArray){
          this.header.title = this.courseDataService.breadtitle
          this.header.breadCrumbList = this.courseDataService.breadcrumbArray
        }
        // this.spinner.show();
        // setTimeout(() => {
        //   this.spinner.hide()
        // }, 2000);
      this.backFlag = 1;
      this.batchDetails = false;
      this.batchSession = false;
      this.batchEnrol = false;
      this.batchRewards = false;
      this.batchNotification = false;
      this.barchChat = false;
      this.batchWorkflow = false;
      this.batchCosting = true;
    }
    this.cdf.detectChanges();
  }
  saveDetails() {
    // this.detailsTab = false;
    // this.modulesTab = true;
    // this.enrolTab = false;
    // this.engageTab = false;
    // this.rewardsTab = false;

    // this.courseDetails = false;
    // this.courseModules = true;

    // this.detailsComp.saveCourse();
  }

  saveModules() {
    // this.detailsTab = false;
    // this.modulesTab = false;
    // this.enrolTab = true;
    // this.engageTab = false;
    // this.rewardsTab = false;

    // this.courseModules = false;
    // this.courseEnrol = true;
  }

  saveEnrol() {
    this.header = {
      title: this.content?this.content.fullname:'Add Batch',
      btnsSearch: true,
      btnName3: 'Bulk Enrol',
      btnName3show: true,
      btnBackshow: true,
      showBreadcrumb: true,
    };
      this.header.breadCrumbList=this.courseDataService.data.catId ?
      [
        // {
        //   'name': 'Learning',
        //   'navigationPath': '/pages/learning',
        // },
        {
          'name': 'Blended Learning',
          'navigationPath': '/pages/blended-home',
        },
        {
          'name': 'Blended Category',
          'navigationPath': '/pages/blended-home/blended-category',
        },
        {
          'name': this.courseDataService.data.categoryName,
          'navigationPath': '/pages/blended-home/blended-courses',
        },
      ]:
      [
        // {
        //   'name': 'Learning',
        //   'navigationPath': '/pages/learning',
        // },
        {
          'name': 'Blended Learning',
          'navigationPath': '/pages/blended-home',
        },
        {
          'name': 'Batches',
          'navigationPath': '/pages/blended-home/blended-courses',
        },
      ]
      if(this.courseDataService.breadcrumbArray){
        this.header.title = this.courseDataService.breadtitle
        this.header.breadCrumbList = this.courseDataService.breadcrumbArray
      }
    this.backFlag = 2;
    // this.detailsTab = false;
    // this.modulesTab = false;
    // this.enrolTab = false;
    // this.engageTab = true;
    // this.rewardsTab = false;

    // this.courseEnrol = false;
    // this.courseEngage = true;
    this.showEnrolpage = true;
    this.showBulkpage = false;
  }
  bulkEnrol() {
    this.header = {
      title: this.content?this.content.fullname:'Add Batch',
      btnsSearch: true,
      btnName2: 'Enrol',
      btnName2show: true,
      btnBackshow: true,
      showBreadcrumb: true,
    }
      this.header.breadCrumbList=this.courseDataService.data.catId ?
      [
        // {
        //   'name': 'Learning',
        //   'navigationPath': '/pages/learning',
        // },
        {
          'name': 'Blended Learning',
          'navigationPath': '/pages/blended-home',
        },
        {
          'name': 'Blended Category',
          'navigationPath': '/pages/blended-home/blended-category',
        },
        {
          'name': this.courseDataService.data.categoryName,
          'navigationPath': '/pages/blended-home/blended-courses',
        },
      ]:
      [
        // {
        //   'name': 'Learning',
        //   'navigationPath': '/pages/learning',
        // },
        {
          'name': 'Blended Learning',
          'navigationPath': '/pages/blended-home',
        },
        {
          'name': 'Batches',
          'navigationPath': '/pages/blended-home/blended-courses',
        },
      ]
      if(this.courseDataService.breadcrumbArray){
        this.header.title = this.courseDataService.breadtitle
        this.header.breadCrumbList = this.courseDataService.breadcrumbArray
      }
    this.backFlag = 2;
    this.showEnrolpage = false;
    this.showBulkpage = true;
  }
  backToEnrol() {
    this.showEnrolpage = false;
    this.showBulkpage = false;
    this.file = [];
    this.preview = false;
    this.fileName = 'Click here to upload an excel file to enrol ' + this.currentBrandData.employee.toLowerCase() + ' to this batch'
    this.enableUpload = false;
    this.allEnrolUser(this.content);
  }
  saveEngage() {
    // this.detailsTab = false;
    // this.modulesTab = false;
    // this.enrolTab = false;
    // this.engageTab = false;
    // this.rewardsTab = true;

    // this.courseEngage = false;
    // this.courseRewards = true;
    this.addEngagePopup = true;
  }

  closeEngageModal() {
    this.addEngagePopup = false;
  }

  saveRewards() {
    // this.rewardsTab = false;

  }

  back() {
    if (this.backFlag == 2) {
      this.header = {
        title: this.content?this.content.fullname:'Add Batch',
        btnsSearch: true,
        btnName2: 'Enrol',
        btnName3: 'Bulk Enrol',
        btnName2show: true,
        btnName3show: true,
        btnBackshow: true,
        showBreadcrumb: true,
      };
        this.header.breadCrumbList=this.courseDataService.data.catId ?
        [
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          {
            'name': 'Blended Learning',
            'navigationPath': '/pages/blended-home',
          },
          {
            'name': 'Blended Category',
            'navigationPath': '/pages/blended-home/blended-category',
          },
          {
            'name': this.courseDataService.data.categoryName,
            'navigationPath': '/pages/blended-home/blended-courses',
          },
        ]:
        [
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          {
            'name': 'Blended Learning',
            'navigationPath': '/pages/blended-home',
          },
          {
            'name': 'Batches',
            'navigationPath': '/pages/blended-home/blended-courses',
          },
        ]
        if(this.courseDataService.breadcrumbArray){
          this.header.title = this.courseDataService.breadtitle
          this.header.breadCrumbList = this.courseDataService.breadcrumbArray
        }
      this.backFlag = 1;
      this.showEnrolpage = false;
      this.showBulkpage = false;
      this.file = [];
      this.preview = false;
      this.fileName = 'Click here to upload an excel file to enrol ' + this.currentBrandData.employee.toLowerCase() + ' to this batch'
      this.enableUpload = false;
      this.allEnrolUser(this.content);
    }
    // this.router.navigate(['/pages/learning/blended-home/blended-courses']);
    else {
        // this.courseDataService.breadtitle = data.categoryName
        if(this.courseDataService.breadcrumbArray){
          var length = this.courseDataService.breadcrumbArray.length-1
          this.courseDataService.breadtitle = this.courseDataService.breadcrumbArray[length].name
          // this.addassetservice.title = this.title
          this.courseDataService.breadcrumbArray = this.courseDataService.breadcrumbArray.slice(0,length)
          // this.title = this.addassetservice.breadCrumbArray[]
          this.courseDataService.previousBreadCrumb = this.courseDataService.previousBreadCrumb.slice(0,length+1)
    
        }
      
      window.history.back();

    }
  }

  ngOnInit() {
    this.addEngagePopup = false;
    this.makeCourseDataReady();
    this.currentBrandData = this.brandService.getCurrentBrandData();
    this.fileName = 'Click here to upload an excel file to enrol ' + this.currentBrandData.employee.toLowerCase() + ' to this batch'
  }
  courseIdEn: any;
  makeCourseDataReady() {
    var content;
    if (this.courseDataService.data) {
      this.content = this.courseDataService.data.data;
      // this.addEditCourseContentService = this.courseDataService.data;
      console.log("content", this.content);
      if (this.content) {
        this.courseIdEn = this.content.courseId;
        if (this.batchEnrol) {
          this.allEnrolUser(this.content);
        }
        this.getAllEmployees(this.content);
      }
    } else {
      this.showEnrolpage = !this.showEnrolpage;
    }
  }
  //--------------- enrolled User (all)----------------------//
  allEnrolUser(content) {
    if (content) {
      var data = {
        areaId: 2,
        instanceId: content.courseId,
        // tId: content.tenantId,
        tId :this.tenantId,
        mode: 0
      }
    }
    console.log(data);
    const allErnroledusers: string = webApi.domain + webApi.url.getAllEnrolUser;
    this.commonFunctionService.httpPostRequest(allErnroledusers, data)
      //this.courseDataService.getallenroluser(data)
      .then(enrolData => {
        //new
        if(enrolData['type'] == true){
        this.enrolldata = enrolData['data'];
        this.rows = enrolData['data'];
        this.rows = [...this.rows];
        if(this.rows.length!=0){
          this.nodata=false;
        }else{
          this.nodata=true;
        }
        for (let i = 0; i < this.rows.length; i++) {
          // this.rows[i].Date = new Date(this.rows[i].enrolDate);
          // this.rows[i].enrolDate = this.formdate(this.rows[i].Date);
          if (this.rows[i].visible == 1) {
            this.rows[i].btntext = 'fa fa-eye';
          } else {
            this.rows[i].btntext = 'fa fa-eye-slash';
          }
        }

        console.log("EnrolledUSer", this.rows);
        if (this.enrolldata.visible = 1) {
          this.enableCourse = false;
        } else {
          this.enableCourse = true;
        }
        this.cdf.detectChanges();
      }else{
        
      }


      })
  }
  getAllEmployees(content) {
    this.empobj = {};
    const param = {
      'srcStr': null,
      'tenantId': content.tenantId,
    };
    const _urlGetAllEmpList = webApi.domain + webApi.url.getAllEmpListUP;
    this.commonFunctionService.httpPostRequest(_urlGetAllEmpList, param)
      //this.courseDataService.getAllEmpListUP(param)
      .then(rescompData => {
        this.AllEmployess = rescompData['data'][0];
        if (this.AllEmployess.length > 0) {
          this.AllEmployess.forEach((element, i) => {
            this.empobj[element.code] = i + 1;
          });
          console.log('empobj:', this.empobj);
        }
        console.log('AllEmployess:', this.AllEmployess);
      });
  }
  visibilityTableRow(row) {
    let value;
    let status;

    if (row.visible == 1) {
      row.btntext = 'fa fa-eye-slash';
      value = 'fa fa-eye-slash';
      row.visible = 0
      status = 0;
    } else {
      status = 1;
      value = 'fa fa-eye';
      row.visible = 1;
      row.btntext = 'fa fa-eye';
    }

    for (let i = 0; i < this.rows.length; i++) {
      if (this.rows[i].employeeId == row.employeeId) {
        this.rows[i].btntext = row.btntext;
        this.rows[i].visible = row.visible
      }
    }
    var visibilityData = {
      employeeId: row.employeeId,
      visible: status,
      courseId: this.content.courseId,
      tId: this.tenantId,
      aId: 2,
    }
    const _urlDisableEnrol: string = webApi.domain + webApi.url.disableEnrol;
    this.commonFunctionService.httpPostRequest(_urlDisableEnrol, visibilityData)
      //this.courseDatabulkService.disableEnrol(visibilityData)
      .then(result => {
        console.log(result);
        this.loader = false;
        this.resultdata = result;
        if (this.resultdata.type == false) {
          // var courseUpdate: Toast = {
          //   type: 'error',
          //   title: "Course",
          //   body: "Unable to update visibility of User.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // // this.closeEnableDisableCourseModal();
          // this.toasterService.pop(courseUpdate);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        } else {
          // var courseUpdate: Toast = {
          //   type: 'success',
          //   title: "Course",
          //   body: this.resultdata.data,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // row.visible = !row.visible;
          console.log("after", row.visible)
          this.allEnrolUser(this.courseDataService.data.data);
          // this.toasterService.pop(courseUpdate);

          if (this.resultdata.status == 'warning') {
            this.toastr.warning(this.resultdata.data, this.resultdata.status, {
              closeButton: false,
            });
          } else {
            this.toastr.success(this.resultdata.data, this.resultdata.status, {
              closeButton: false,
            });
          }
        }
      },
        resUserError => {
          this.loader = false;
          this.errorMsg = resUserError;
          this.closeEnableDisableCourseModal();
        });

    console.log('row', row);
  }

  // disableCourseVisibility(currentIndex, row, status) {
  //   var visibilityData = {
  //     employeeId: row.employeeId,
  //     visible: status,
  //     courseId: this.content.courseId,
  //     tId: this.tenantId,
  //     aId: 2,
  //   }
  //   const _urlDisableEnrol: string = webApi.domain + webApi.url.disableEnrol;
  //   this.commonFunctionService.httpPostRequest(_urlDisableEnrol,visibilityData)
  //   //this.courseDatabulkService.disableEnrol(visibilityData)
  //   .then(result => {
  //     console.log(result);
  //     this.loader = false;
  //     this.resultdata = result;
  //     if (this.resultdata.type == false) {
  //       // var courseUpdate: Toast = {
  //       //   type: 'error',
  //       //   title: "Course",
  //       //   body: "Unable to update visibility of User.",
  //       //   showCloseButton: true,
  //       //   timeout: 2000
  //       // };
  //       // // this.closeEnableDisableCourseModal();
  //       // this.toasterService.pop(courseUpdate);

  //       this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
  //         timeOut: 0,
  //         closeButton: true
  //       });
  //     } else {
  //       // var courseUpdate: Toast = {
  //       //   type: 'success',
  //       //   title: "Course",
  //       //   body: this.resultdata.data,
  //       //   showCloseButton: true,
  //       //   timeout: 2000
  //       // };
  //       // row.visible = !row.visible;
  //       console.log("after", row.visible)
  //       this.allEnrolUser(this.courseDataService.data.data);
  //       // this.toasterService.pop(courseUpdate);

  //       if (this.resultdata.status == 'warning') {
  //         this.toastr.warning(this.resultdata.data, this.resultdata.status, {
  //           closeButton: false,
  //         });
  //       } else {
  //         this.toastr.success(this.resultdata.data, this.resultdata.status, {
  //           closeButton: false,
  //         });
  //       }
  //     }
  //   },
  //     resUserError => {
  //       this.loader = false;
  //       this.errorMsg = resUserError;
  //       this.closeEnableDisableCourseModal();
  //     });


  // }

  enableDisableCourseAction(actionType) {
    if (actionType == true) {
      if (this.enabledata == 1) {
        this.enabledata = 0;
        // var courseData = this.content[this.courseDisableIndex];
        this.enableDisableCourse(this.enableuser);
      } else {
        this.enabledata = 1;
        // var courseData = this.content[this.courseDisableIndex];
        this.enableDisableCourse(this.enableuser);
      }
    } else {
      this.closeEnableDisableCourseModal();
    }
  }
  enableDisableCourse(enableuser) {
    console.log(enableuser);
    var visibilityData = {
      userId: enableuser.ecn,
      visible: enableuser.visible
    }
    const _urlDisableUser: string = webApi.domain + webApi.url.disableuser;
    this.commonFunctionService.httpPostRequest(_urlDisableUser, visibilityData)
      //this.courseDataService.disableUser(visibilityData)
      .then(result => {
        console.log(result);
        this.loader = false;
        this.resultdata = result;
        if (this.resultdata.type == false) {
          // var courseUpdate: Toast = {
          //   type: 'error',
          //   title: "Course",
          //   body: "Unable to update visibility of User.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          this.closeEnableDisableCourseModal();
          // this.toasterService.pop(courseUpdate);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        } else {
          // var courseUpdate: Toast = {
          //   type: 'success',
          //   title: "Course",
          //   body: this.resultdata.data,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          this.closeEnableDisableCourseModal();
          // this.toasterService.pop(courseUpdate);

          this.toastr.success(this.resultdata.data, 'Success', {
            closeButton: false
          });
        }
      },
        resUserError => {
          this.loader = false;
          this.errorMsg = resUserError;
          this.closeEnableDisableCourseModal();
        });
  }
  closeEnableDisableCourseModal() {
    this.enableDisableCourseModal = false;
  }

  onChange(event) {
    if (event == 'Date') {
      this.showdate = true;
    }
  }

  onChangeSelect(event) {
    if (event == 'nDays') {
      this.showdays = true;
    }
  }
  // bulkUploadData : any = [];
  readFileUrl(event: any) {
    this.uploadedData = [];
    this.invaliddata = [];
    this.result = [];
    this.showInvalidExcel = false;
    const validExts = new Array('.xlsx', '.xls');
    let fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
      this.toastr.warning('Valid file types are ' + validExts.toString(), 'Warning', {
        closeButton: false
      })
      // setTimeout(data => {
      //   this.router.navigate(['/pages/users']);
      // },1000);
      // return false;
      this.cancel();
    } else {
      // return true;
      if (event.target.files && event.target.files[0]) {
        this.fileName = event.target.files[0].name;
        // read file from input
        this.fileReaded = event.target.files[0];

        if (this.fileReaded != '' || this.fileReaded != null || this.fileReaded != undefined) {
          this.enableUpload = true;
          this.showInvalidExcel = false;
        }

        let file = event.target.files[0];
        this.bulkUploadData = event.target.files[0];
        console.log('this.bulkUploadData', this.bulkUploadData);

        this.xlsxToJsonService.processFileToJson({}, file).subscribe(data => {
          this.resultSheets = Object.getOwnPropertyNames(data['sheets']);
          console.log('File Property Names ', this.resultSheets);
          let sheetName = this.resultSheets[0];
          this.result = data['sheets'][sheetName];
          console.log('dataSheet', data);
          console.log('this.result', this.result);

          if (this.result.length > 0) {
            this.uploadedData = this.result;
          }
          console.log('this.uploadedData', this.uploadedData);
        });
        var reader = new FileReader();
        reader.onload = (event: ProgressEvent) => {
          this.fileUrl = (<FileReader>event.target).result;
          /// console.log(this.fileUrl);
        };
        reader.readAsDataURL(event.target.files[0]);
      }
    }
  }

  // readUrl(event: any) {

  //   var validExts = new Array('.xlsx', '.xls');
  //   var fileExt = event.target.files[0].name;
  //   fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
  //   if (validExts.indexOf(fileExt) < 0) {
  //     // alert('Invalid file selected, valid files are of ' +
  //     //          validExts.toString() + ' types.');
  //     // var toast: Toast = {
  //     //   type: 'error',
  //     //   title: 'Invalid file selected!',
  //     //   body: 'Valid files are of ' + validExts.toString() + ' types.',
  //     //   showCloseButton: true,
  //     //   // tapToDismiss: false,
  //     //   timeout: 2000
  //     //   // onHideCallback: () => {
  //     //   //     this.router.navigate(['/pages/plan/users']);
  //     //   // }
  //     // };
  //     // this.toasterService.pop(toast);

  //     this.toastr.warning('Valid file types are ' + validExts.toString(), 'Warning', {
  //       closeButton: false
  //     })
  //     // setTimeout(data => {
  //     //   this.router.navigate(['/pages/users']);
  //     // },1000);
  //     // return false;
  //     this.cancel();
  //   } else {
  //     // return true;
  //     if (event.target.files && event.target.files[0]) {
  //       this.fileName = event.target.files[0].name;
  //       //read file from input
  //       this.fileReaded = event.target.files[0];

  //       if (this.fileReaded != '' || this.fileReaded != null || this.fileReaded != undefined) {
  //         this.enableUpload = true;
  //         this.showInvalidExcel =  false;
  //       }

  //       this.file = event.target.files[0];
  //       var reader = new FileReader();
  //       reader.onload = (event: ProgressEvent) => {
  //         let fileUrl = (<FileReader>event.target).result;
  //         // event.setAttribute('data-title', this.fileName);
  //         // console.log(this.fileUrl);
  //       }
  //       reader.readAsDataURL(event.target.files[0]);
  //     }
  //   }
  // }

  cancel() {
    this.bulkUploadData = [];
    // this.fileUpload.nativeElement.files = [];
    console.log(this.fileUpload.nativeElement.files);
    this.fileUpload.nativeElement.value = '';
    console.log(this.fileUpload.nativeElement.files);
    this.fileName = 'Click here to upload an excel file to enrol employee to this batch';
    this.enableUpload = false;
    this.file = [];
    this.preview = false;
  }
  uploadfile() {
    this.spinner.show();


    var fd = new FormData();
    this.content.areaId = 2;
    this.content.userId = this.userId;
    fd.append('content', JSON.stringify(this.content));
    fd.append('file', this.file);
    console.log(fd);
    this.courseDataService.TempManEnrolBulk(fd).then(result => {
      console.log(result);
      // this.loader =false;

      var res = result;
      try {
        if (res['data1'][0]) {
          this.resultdata = res['data1'][0];
          // for (let i = 0; i < this.resultdata.length; i++) {
          //   this.resultdata[i].enrolDate = this.formdate(this.resultdata[i].enrolDate);
          // }

          this.datasetPreview = this.resultdata;
          this.preview = true;
          this.cdf.detectChanges();
          // this.spinner.hide();
        }
      } catch (e) {

        // var courseUpdate: Toast = {
        //   type: 'error',
        //   title: 'Enrol',
        //   body: 'Something went wrong.please try again later.',
        //   showCloseButton: true,
        //   timeout: 2000
        // };

        // this.closeEnableDisableCourseModal();
        // this.toasterService.pop(courseUpdate);

        this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
          timeOut: 0,
          closeButton: true
        });
      }
      this.spinner.hide();

    },
      resUserError => {
        // this.loader =false;
        this.spinner.hide();
        this.errorMsg = resUserError;
        // this.closeEnableDisableCourseModal();
      });
  }
  uploadfile1() {
    this.validdata = [];
    this.invaliddata = [];
    console.log('this.AllEmployess', this.AllEmployess);
    console.log('this.uploadedData', this.uploadedData);
    if (this.AllEmployess.length > 1) {
      // this.AllEmployess.some((item,i) => {
      this.uploadedData.forEach((data, j) => {
        if (!data.UserEcn) {
          this.uploadedData[j].status = 'Invalid';
          this.uploadedData[j].reason = 'ECN is required';
          return;
        } else if (!this.empobj[data.UserEcn]) {
          this.uploadedData[j].status = 'Invalid';
          this.uploadedData[j].reason = 'Invalid ECN';
          return;
        } else if (this.nomiObj[data.UserEcn]) {
          this.uploadedData[j].status = 'Invalid';
          this.uploadedData[j].reason = 'User already nominated';
          return;
        } else if (!data.EnrolDate) {
          this.uploadedData[j].status = 'Invalid';
          this.uploadedData[j].reason = 'Date is required ';
          return;
        } else if (!(/^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/g.test(data.date))) {
          this.uploadedData[j].status = 'Invalid';
          this.uploadedData[j].reason = 'Invalid date format.Use DD/MM/YYYY date format';
          return;
        } else {
          this.uploadedData[j].status = 'Valid';
          this.uploadedData[j].reason = '';
          return;
        }
      });

      console.log('this.uploadedData', this.uploadedData);
    }
    if (this.uploadedData.length > 0) {
      this.uploadedData.forEach(element => {
        if (element.status === 'Valid') {
          delete element.status;
          delete element.reason;
          element.date = element.date.split("/").reverse().join("-")
          this.validdata.push(element);
        } else {
          this.invaliddata.push(element);
        }
      });
      this.datasetPreview = this.resultdata;
      this.preview = true;
      this.cdf.detectChanges();
    }
    this.spinner.hide();
    if (this.invaliddata.length > 0) {
      this.showInvalidExcel = true;
    }
  }
  exportToExcelInvalid() {
    this.exportService.exportAsExcelFile(this.invaliddata, 'Enrolment Status');
  }
  formdate(date) {

    if (date) {
      // const months = ["JAN", "FEB", "MAR","APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
      // var day = date.getDate();
      // var monthIndex = months[date.getMonth()];
      // var year = date.getFullYear();

      // return day + '_' + monthIndex + '_' +year;
      var formatted = this.datePipe.transform(date, 'dd-MM-yyyy');
      return formatted;
    }
  }
  savebukupload() {
    // this.loader = true;
    if (this.uploadedData.length > 0 && this.uploadedData.length <= 2000) {
      this.spinner.show();

      // var option: string = Array.prototype.map
      //   .call(this.uploadedData, function (item) {
      //     console.log("item", item);
      //     return Object.values(item).join("#");
      //   })
      //   .join("|");


      // New Function as per requirement.

      var option: string = Array.prototype.map
        .call(this.uploadedData, (item) => {
          const array = Object.keys(item);
          let string = '';
          array.forEach(element => {
            if (element === 'EnrolDate') {
              string += this.commonFunctionService.formatDateTimeForBulkUpload(item[element]);
            } else {
              string = item[element] + '#';
            }
          });
          // console.log("item", item);
          // return Object.values(item).join("#");
          return string;
        })
        .join("|");
      // console.log('this.courseDataService.workflowId', this.courseDataService.workflowId);

      const data = {
        wfId: this.courseDataService.workflowId ? this.courseDataService.workflowId : null,
        aId: 2,
        iId: this.content.courseId,
        // userId: this.content.userId,
        upString: option
      }
      console.log('data', data);
      this.courseDataService.areaBulkEnrol(data)
        .then(rescompData => {
          // this.loader =false;
          this.spinner.hide();
          var temp: any = rescompData;
          this.addEditModRes = temp.data;
          if (temp == 'err') {
            this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
              timeOut: 0,
              closeButton: true
            });
          } else if (temp.type == false) {
            this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
              timeOut: 0,
              closeButton: true
            });
          } else {
            this.enableUpload = false;
            this.fileName = 'Click here to upload an excel file to enrol ' + this.currentBrandData.employee.toLowerCase() + ' to this batch'
            if (temp['data'].length > 0) {
              if (temp['data'].length === temp['invalidCnt'].invalidCount) {
                this.toastr.warning('No valid employees found to enrol.', 'Warning', {
                  closeButton: false
                });
              } else {
                this.toastr.success('Enrol successfully.', 'Success', {
                  closeButton: false
                });
                this.allEnrolUser(this.content)
              }
              this.invaliddata = temp['data'];
              this.showInvalidExcel = true;

              // this.invaliddata = temp['data'];
              // if (this.uploadedData.length == this.invaliddata.length) {
              //   this.toastr.warning('No valid employees found to enrol.', 'Warning', {
              //     closeButton: false
              //     });
              // } else {
              //   this.toastr.success('Enrol successfully.', 'Success', {
              //     closeButton: false
              //     });
              // }
            } else {
              this.invaliddata = [];
              this.showInvalidExcel = false;
            }




            // this.backToEnrol()
            this.cdf.detectChanges();
            this.preview = false;
            this.showInvalidExcel = true;

          }
          console.log('Enrol bulk Result ', this.addEditModRes);
          this.cdf.detectChanges();
        },
          resUserError => {
            this.loader = false;
            this.errorMsg = resUserError;
          });
    } else {
      if (this.uploadedData.length > 2000) {
        this.toastr.warning('File Data cannot exceed more than 2000', 'warning', {
          closeButton: true
        });
      }
      else {
        this.toastr.warning('No Data Found', 'warning', {
          closeButton: true
        });
      }

    }

  }
  onActevent(data) {
    this.header = {
    title: data.type,
    btnsSearch: true,
    btnBackshow: true,
    showBreadcrumb: true,
   }
    this.header.breadCrumbList=this.courseDataService.data.catId?
    [
    //  {
    //    'name': 'Learning',
    //    'navigationPath': '/pages/learning',
    //  }, 
    {
      'name': 'Blended Learning',
      'navigationPath': '/pages/blended-home',
    },
    {
      'name': 'Blended Category',
      'navigationPath': '/pages/blended-home/blended-category',
    },
    {
      'name': this.courseDataService.data.categoryName,
      'navigationPath': '/pages/blended-home/blended-courses',
    },
     {
      'isSame':true,
      'name':this.content?this.content.fullname:'Add Course',
      'navigationPath': '/pages/blended-home/blended-courses/addEditCourseContent',  
       },
     {
       'isSame':true,
        'name':data.name,
        'navigationPath': '/pages/blended-home/blended-courses/addEditCourseContent',  
      },
   ]:[
      //  {
      //    'name': 'Learning',
      //    'navigationPath': '/pages/learning',
      //  },
      {
        'name': 'Blended Learning',
        'navigationPath': '/pages/blended-home',
      },
      {
        'name': 'Batches',
        'navigationPath': '/pages/blended-home/blended-courses',
      },
       {
       'isSame':true,
        'name': this.content?this.content.fullname:'Add Course',
        'navigationPath': '/pages/blended-home/blended-courses/addEditCourseContent',  
      },
       {
       'isSame':true,
        'name':data.name,
        'navigationPath': '/pages/blended-home/blended-courses/addEditCourseContent',  
       },
   ]
   if(this.courseDataService.breadcrumbArray){
    this.header.title = this.courseDataService.breadtitle
    this.header.breadCrumbList = this.courseDataService.breadcrumbArray
  }
  
    }
    setHeader(event) {
      this.header = {
        title: event,
        btnName5: 'Add Session',
        btnName6: 'Save',
        btnName7: 'Cancel',
        btnName5show: false,
        btnName6show: true,
        btnName7show: true,
        btnsSearch: true,
        btnBackshow: true,
        showBreadcrumb: true,
        breadCrumbList:this.courseDataService.data.catId?
        [
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // }, 
          {
            'name': 'Blended Learning',
            'navigationPath': '/pages/blended-home',
          },
         {
           'name': 'Blended Category',
           'navigationPath': '/pages/blended-home/blended-category',
         },
         {
          'name': this.courseDataService.data.categoryName,
          'navigationPath': '/pages/blended-home/blended-courses',
        },
         {
          'isSame':true,
          'name':this.content?this.content.fullname:'Add Course',
          'navigationPath': '/pages/blended-home/blended-courses/addEditCourseContent',  
           },
       ]:[
        // {
        //   'name': 'Learning',
        //   'navigationPath': '/pages/learning',
        // }, 
        {
          'name': 'Blended Learning',
          'navigationPath': '/pages/blended-home',
        },
       {
        'name': 'Batches',
        'navigationPath': '/pages/blended-home/blended-courses',
      },
       {
        'isSame':true,
        'name':this.content?this.content.fullname:'Add Course',
        'navigationPath': '/pages/blended-home/blended-courses/addEditCourseContent',  
         },
       ]
      };
    }
  passData(val) {
    this.isdata = val;

    if(val === 'modules') {
      this.header = {
        title: this.content?this.content.fullname:'Add Batch',
        btnsSearch: true,
        btnBackshow: true,
        btnName5: 'Add Session',
        btnName6: 'Save',
        btnName7: 'Cancel',
        btnName5show: false,
        btnName6show: true,
        btnName7show: true,
        showBreadcrumb: true,
      };
      this.header.breadCrumbList=this.courseDataService.data.catId ?
      [
        // {
        //   'name': 'Learning',
        //   'navigationPath': '/pages/learning',
        // },
        {
          'name': 'Blended Learning',
          'navigationPath': '/pages/blended-home',
        },
        {
          'name': 'Blended Category',
          'navigationPath': '/pages/blended-home/blended-category',
        },
        {
          'name': this.courseDataService.data.categoryName,
          'navigationPath': '/pages/blended-home/blended-courses',
        },
      ]:
      [
        // {
        //   'name': 'Learning',
        //   'navigationPath': '/pages/learning',
        // },
        {
          'name': 'Blended Learning',
          'navigationPath': '/pages/blended-home',
        },
        {
          'name': 'Batches',
          'navigationPath': '/pages/blended-home/blended-courses',
        },
      ]
      if(this.courseDataService.breadcrumbArray){
        this.header.title = this.courseDataService.breadtitle
        this.header.breadCrumbList = this.courseDataService.breadcrumbArray
      }
    } else if (val === 'cancel') {
      this.header = {
        title: this.content?this.content.fullname:'Add Batch',
        btnsSearch: true,
        btnBackshow: true,
        btnName5: 'Add Session',
        btnName6: 'Save',
        btnName7: 'Cancel',
        btnName5show: true,
        btnName6show: false,
        btnName7show: false,
        showBreadcrumb: true,
      };
      this.header.breadCrumbList=this.courseDataService.data.catId ?
      [
        // {
        //   'name': 'Learning',
        //   'navigationPath': '/pages/learning',
        // },
        {
          'name': 'Blended Learning',
          'navigationPath': '/pages/blended-home',
        },
        {
          'name': 'Blended Category',
          'navigationPath': '/pages/blended-home/blended-category',
        },
        {
          'name': this.courseDataService.data.categoryName,
          'navigationPath': '/pages/blended-home/blended-courses',
        },
      ]:
      [
        // {
        //   'name': 'Learning',
        //   'navigationPath': '/pages/learning',
        // },
        {
          'name': 'Blended Learning',
          'navigationPath': '/pages/blended-home',
        },
        {
          'name': 'Batches',
          'navigationPath': '/pages/blended-home/blended-courses',
        },
      ]
      if(this.courseDataService.breadcrumbArray){
        this.header.title = this.courseDataService.breadtitle
        this.header.breadCrumbList = this.courseDataService.breadcrumbArray
      }
    }
    // this.eventsSubject.next();
    setTimeout(() => {
    this.isdata = '';
    },2000);
  }
  oncancelAct(){
    if(this.tabTitle == "Sessions"){
    this.header = {
      title: this.content?this.content.fullname:'Add Batch',
      btnsSearch: true,
      btnBackshow: true,
      btnName5: 'Add Session',
      btnName6: 'Save',
      btnName7: 'Cancel',
      btnName5show: true,
      btnName6show: false,
      btnName7show: false,
      showBreadcrumb: true,
    };
    this.header.breadCrumbList=this.courseDataService.data.catId ?
    [
      // {
      //   'name': 'Learning',
      //   'navigationPath': '/pages/learning',
      // },
      {
        'name': 'Blended Learning',
        'navigationPath': '/pages/blended-home',
      },
      {
        'name': 'Blended Category',
        'navigationPath': '/pages/blended-home/blended-category',
      },
      {
        'name': this.courseDataService.data.categoryName,
        'navigationPath': '/pages/blended-home/blended-courses',
      },
    ]:
    [
      // {
      //   'name': 'Learning',
      //   'navigationPath': '/pages/learning',
      // },
      {
        'name': 'Blended Learning',
        'navigationPath': '/pages/blended-home',
      },
      {
        'name': 'Batches',
        'navigationPath': '/pages/blended-home/blended-courses',
      },
    ]
    if(this.courseDataService.breadcrumbArray){
      this.header.title = this.courseDataService.breadtitle
      this.header.breadCrumbList = this.courseDataService.breadcrumbArray
    }
  }else{
    
  }
  }
  passEData() {
    this.header  = {
      title:'Course Details',
      btnsSearch: true,
      // btnName6: 'Save',
      // btnName7: 'Cancel',
      // btnName6show: true,
      // btnName7show: true,
      btnBackshow: true,
      showBreadcrumb:true,
      breadCrumbList:this.courseDataService.data.catId ?
      [
        // {
        //   'name': 'Learning',
        //   'navigationPath': '/pages/learning',
        // },
        {
          'name': 'Blended Learning',
          'navigationPath': '/pages/blended-home',
        },
        {
          'name': 'Blended Category',
          'navigationPath': '/pages/blended-home/blended-category',
        },
        {
          'name': this.courseDataService.data.categoryName,
          'navigationPath': '/pages/blended-home/blended-courses',
        },
      ]:
      [
        // {
        //   'name': 'Learning',
        //   'navigationPath': '/pages/learning',
        // },
        {
          'name': 'Blended Learning',
          'navigationPath': '/pages/blended-home',
        },
        {
          'name': 'Batches',
          'navigationPath': '/pages/blended-home/blended-courses',
        },
      ]
    };
  }
  backToAsset(data){
    this.contentservice.parentCatId = data.categoryId
    this.contentservice.countLevel = data['index'] - 1
    this.courseDataService.breadtitle = data.categoryName
    this.courseDataService.breadcrumbArray = this.courseDataService.breadcrumbArray.slice(0,data.index)
    // this.title = this.addassetservice.breadCrumbArray[]
    this.courseDataService.previousBreadCrumb = this.courseDataService.previousBreadCrumb.slice(0,data.index+1)

    // this.router.navigate(['../'], { relativeTo: this.routes });


  }

}

