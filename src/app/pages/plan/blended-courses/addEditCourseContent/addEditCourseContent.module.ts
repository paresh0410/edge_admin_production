import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

import { NgxEchartsModule } from 'ngx-echarts';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ThemeModule } from '../../../../@theme/theme.module';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { MyDatePickerModule } from 'mydatepicker';
import { FilterPipeModule  } from 'ngx-filter-pipe';
import { TabsModule } from "ngx-tabs";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { TranslateModule } from '@ngx-translate/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NbTabsetModule } from '@nebular/theme';
import { TagInputModule } from 'ngx-chips';
import { JoditAngularModule } from 'jodit-angular';
import { AddEditBlendCourseContent } from './addEditCourseContent';
import { AddEditBlendCourseContentService } from './addEditCourseContent.service'

import { rewardsBlendComponent } from './rewards/rewards.component';
import { rewardsBlendService } from './rewards/rewards.service';
import { engageBlendComponent } from './engage/engage.component';
import { engageBlendService } from './engage/engage.service';


import { enrolmentBlendComponent } from './enrolment/enrolment.component';
import { enrolBlendService } from './enrolment/enrolment.service';
// import { DragChipsComponentComponent } from '../../../../component/drag-chips-component/drag-chips-component.component';
import { ComponentModule } from '../../../../component/component.module';
// import { ChatComponent } from './chat/chat.component';
// import { UserChatService } from './chat/chat.service';

import { detailsBlendComponent } from './details/details.component';
import { detailsBlendService } from './details/details.service';

import { modulesBlendComponent } from './modules/modules.component';
import { modulesBlendService } from './modules/modules.service';

import { GamificationBlendComponent } from './gamification/gamification.component';
import { GamificationBlendService } from './gamification/gamification.service';

import { QuillModule } from 'ngx-quill'
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'

import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

import {MatSelectModule} from '@angular/material/select';
import { WorkflowComponent } from './workflow/workflow.component';
import { CostingComponent } from './costing/costing.component';
import { SortablejsModule } from 'angular-sortablejs';

 import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { costingService } from './costing/costing.service';
/*PRod */
import { ChatComponent } from './chat/chat.component';

@NgModule({
  imports: [
    ThemeModule,
    SortablejsModule,
    NgxEchartsModule,
    NgxMyDatePickerModule.forRoot(),
    TabsModule,
    ReactiveFormsModule,
    AngularMultiSelectModule,
    FormsModule,
    TranslateModule.forRoot(),
    NgxDatatableModule,
    Ng2SmartTableModule,
    NbTabsetModule,
    FilterPipeModule,
    MyDatePickerModule,
    QuillModule,
    TagInputModule,
    BsDatepickerModule.forRoot(),
    DatepickerModule.forRoot(),
    TooltipModule.forRoot(),
    TimepickerModule.forRoot(),
    MatSelectModule ,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    JoditAngularModule,
    ComponentModule,
  ],
  declarations: [
    AddEditBlendCourseContent,
    enrolmentBlendComponent,
    detailsBlendComponent,
    modulesBlendComponent,
    rewardsBlendComponent,
    engageBlendComponent,
    GamificationBlendComponent,
    WorkflowComponent,
    CostingComponent,
    ChatComponent,
    // DragChipsComponentComponent,
    // ChatComponent

  ],
  providers: [
    //slikgridDemoService,
    AddEditBlendCourseContentService,
    enrolBlendService,
    detailsBlendService,
    modulesBlendService,
    rewardsBlendService,
    engageBlendService,
    costingService,
    GamificationBlendService,
    // UserChatService

  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class AddEditBlendCourseContentModule { }
