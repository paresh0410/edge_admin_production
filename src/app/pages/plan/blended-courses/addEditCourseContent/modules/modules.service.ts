import {Injectable, Inject} from '@angular/core';
import {Http, Response, Headers, RequestOptions, Request} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppConfig} from '../../../../../app.module';

import { webAPIService } from '../../../../../service/webAPIService'
import { webApi } from '../../../../../service/webApi'
import { HttpClient } from "@angular/common/http";
@Injectable()

export class modulesBlendService {

  public data: any;
  request: Request;
   userData:any;
  tenantId:any;
  // private _urlGetCat:string = "/api/edge/category/getCategory"
  // private _urlAddEditCourse:string = "/api/edge/course/addUpdateCourse"
  // private _urlGetTopics:string = "/api/edge/course/getTopic";
  // private _urlAddEditTopics:string = "/api/edge/course/addUpdateTopic";
  // private _urlGetResource:string = "/api/edge/course/getResource";
  // private _urlAddEditResource:string = "/api/edge/course/addUpdateResource";
  // private _urlGetModules:string = "/api/edge/course/getModules";

  private _urlGetFeedback:string = webApi.domain + webApi.url.searchFeedback;
  private _urlGetQuiz:string = webApi.domain + webApi.url.searchQuiz;
  private _urlAddEditModule:string = webApi.domain + webApi.url.addEditModuleBatch;
  private _urlGetCourseModules:string = webApi.domain + webApi.url.getBatchModules;
  private _urlGetTrainers:string = webApi.domain + webApi.url.get_Trainers
  private _urlGetCourseModulesActivity:string = webApi.domain + webApi.url.getBatchModulesActivity;
  private _urlGetCourseModulesActivityDrop:string = webApi.domain + webApi.url.getModuleActivityDropBatch;
  private _urlGetActivityContent:string = webApi.domain + webApi.url.contentSearch;
  private _urlDeleteActivity:string = webApi.domain + webApi.url.deleteActivityBatch;
  private _urlDeleteModule:string = webApi.domain + webApi.url.deleteModuleBatch;
  private _urlGetroleDropdownSetting: string = webApi.domain + webApi.url.roleDrop;
  /* dam handshake */
  private _urlhandshakeDamUserVerify: string = webApi.domain + webApi.url.handshakeDamUserVerify;
  private _urlhandshakeDamAsset: string = webApi.domain + webApi.url.handshakeDamAsset;
  private _urlremoveTrainerEnrolment: string = webApi.domain + webApi.url.removeTrainerEnrolment;
  private _urlEnableDisableModule: string = webApi.domain + webApi.url.disable_module;
  private _urlEnableDisableActivity: string = webApi.domain + webApi.url.disable_activity;

  constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private _http: Http, private http1 :HttpClient){
      //this.busy = this._http.get('...').toPromise();

  }


  // new services start //
  addEditMod(moduleData){
    // return this._http.post(this._urlAddEditModule,moduleData)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);

    return new Promise(resolve => {
      this.http1.post(this._urlAddEditModule,moduleData)
      //.map(res => res.json())

      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
        });
      });
  }

  getCourseMod(courseData){
    // return this._http.post(this._urlGetCourseModules,courseData)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);
    return new Promise(resolve => {
      this.http1.post(this._urlGetCourseModules,courseData)
      //.map(res => res.json())

      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
        });
      });

  }

  getTrainers(param){
    return new Promise(resolve => {
      this.http1.post(this._urlGetTrainers,param)
      //.map(res => res.json())

      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
        });
      });
  }

  
  getallDropdown(param){
    return new Promise(resolve => {
      this.http1.post(this._urlGetroleDropdownSetting, param)
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}

  getCourseModActivity(activityData){
    // return this._http.post(this._urlGetCourseModulesActivity,activityData)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);
    return new Promise(resolve => {
      this.http1.post(this._urlGetCourseModulesActivity,activityData)
      //.map(res => res.json())

      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
        });
      });
  }

  getModActivityDrop(){
    // return this._http.post(this._urlGetCourseModulesActivityDrop,this.param)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);
    if(localStorage.getItem('LoginResData')){
      this.userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', this.userData.data);
      // this.userId = this.userData.data.data.id;
      this.tenantId = this.userData.data.data.tenantId;
   }
    const param: any = {
      tId: this.tenantId,
      courseTypeId: 4,
    };
    return new Promise(resolve => {
      this.http1.post(this._urlGetCourseModulesActivityDrop, param)
      //.map(res => res.json())

      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
        });
      });

  }

  searchActivityContent(data){
    // return this._http.post(this._urlGetActivityContent,data)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);
    return new Promise(resolve => {
      this.http1.post(this._urlGetActivityContent,data)
      //.map(res => res.json())

      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
        });
      });
  }

  searchcoursequizContent(data){
    // return this._http.post(this._urlGetActivityContent,data)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);
    return new Promise(resolve => {
      this.http1.post(this._urlGetQuiz,data)
      //.map(res => res.json())

      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
        });
      });
  }
  searchcoursefeedbackContent(data){
    // return this._http.post(this._urlGetActivityContent,data)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);
    return new Promise(resolve => {
      this.http1.post(this._urlGetFeedback,data)
      //.map(res => res.json())

      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
        });
      });
  }

  deleteActivity(data){
    // return this._http.post(this._urlDeleteActivity,data)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);
    return new Promise(resolve => {
      this.http1.post(this._urlDeleteActivity,data)
      //.map(res => res.json())

      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
        });
      });

  }

  deleteModule(data){
    // return this._http.post(this._urlDeleteModule,data)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);
    return new Promise(resolve => {
      this.http1.post(this._urlDeleteModule,data)
      //.map(res => res.json())

      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
        });
      });
  }

    /*****Dam Handshake*****/

    generatingToken(userData) {
      return new Promise(resolve => {
        this.http1.post(this._urlhandshakeDamUserVerify, userData)

          .subscribe(data => {
            resolve(data);
            console.log('Data token response ->', data);
          },
            err => {
              resolve('err');
            });
      });
    }

    ApprovedDamHanshakeData(data) {
      // return this._http.post(this._urlGetActivityContent,data)
      //   .map((response:Response) => response.json())
      //   .catch(this._errorHandler);
      return new Promise(resolve => {
        this.http1.post(this._urlhandshakeDamAsset, data)
          //.map(res => res.json())

          .subscribe(data => {
            resolve(data);
          },
            err => {
              resolve('err');
            });
      });
    }

  _errorHandler(error: Response){
     console.error(error);
     return Observable.throw(error || "Server Error")
  }

  removeTrainerEnrolment(param){
    // return this._http.post(this._urlAddEditModule,moduleData)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);

    return new Promise(resolve => {
      this.http1.post(this._urlremoveTrainerEnrolment,param)
      //.map(res => res.json())

      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
        });
      });
  }

  moduleEnableDisable(param) {
    return new Promise(resolve => {
      this.http1.post(this._urlEnableDisableModule, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  enableDisableActivity(visibleData) {
    let url: any = this._urlEnableDisableActivity;
    return new Promise(resolve => {
      this.http1.post(url, visibleData)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
}
