import {
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation,
  Directive,
  forwardRef,
  Attribute,
  OnChanges,
  SimpleChanges,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ViewContainerRef,
  ElementRef,
  OnInit,
  Renderer2,
} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from '../../../../../service/authentication.service'
import { DatatableComponent } from '@swimlane/ngx-datatable';
import {
  NG_VALIDATORS,
  Validator,
  Validators,
  AbstractControl,
  ValidatorFn,
  FormGroup,
  FormArray,
  FormBuilder,
  FormControl
} from '@angular/forms';
import { LocalDataSource } from 'ng2-smart-table';
import { Router, NavigationStart, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { DatePipe } from '@angular/common';
import { OwlDateTimeModule } from 'ng-pick-datetime';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { modulesBlendService } from './modules.service';
import { AddEditBlendCourseContent } from '../addEditCourseContent';
import { AddEditBlendCourseContentService } from '../addEditCourseContent.service';
import { id } from '@swimlane/ngx-charts/release/utils';
import { webAPIService } from '../../../../../service/webAPIService';
import { webApi } from '../../../../../service/webApi';
import { LOGTYPE, ACTIVITYLABELS } from '../../../../../entity/log.enum';
import { LogServices } from '../../../../../service/log.service';
import { transform, isEqual, isObject } from 'lodash';
import * as _moment from 'moment';
import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';
import { f2 } from '@angular/core/src/render3';
const moment = (_moment as any).default ? (_moment as any).default : _moment;
import { CommonFunctionsService } from '../../../../../service/common-functions.service';
import * as $ from 'jquery';
import { noData } from '../../../../../models/no-data.model';
import { DataSeparatorService } from '../../../../../service/data-separator.service';
import * as _ from "lodash";

export const MY_CUSTOM_FORMATS = {
  fullPickerInput: 'DD-MM-YYYY h:mm:ss A',
  parseInput: 'DD-MM-YYYY h:mm:ss A',
  datePickerInput: 'DD-MM-YYYY',
  timePickerInput: 'LT',
  monthYearLabel: 'MMM YYYY',
  dateA11yLabel: 'LL',
  monthYearA11yLabel: 'MMMM YYYY'
};
@Component({
  selector: 'batch-modules',
  templateUrl: './modules.html',
  styleUrls: ['./modules.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [DatePipe,
    { provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE] },
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS }],
  changeDetection: ChangeDetectionStrategy.OnPush

})
export class modulesBlendComponent implements OnInit {
  myDateValue: Date;
  mytime: Date = new Date();
  activityLogId = LOGTYPE.activity;
  trainer: boolean;
  trainerVisible: boolean = false;
  moduleTrainers: any = [];
  isTrain: boolean = false;
  valid: boolean = true;
  spinnerHide: boolean = false;
  onDateChange(newDate: Date) {
    console.log(newDate);
  }
  @ViewChild('train') train: any;

  @ViewChild('enrolledUsersTable') table: any;
  @ViewChild('fileUpload') fileUpload: any;
  @ViewChild('inputtext') inputtext: any;

  @Input() inpdata: any;
  @Output() cancelm = new EventEmitter<any>();
  @Output() editev = new EventEmitter<any>();
  @Output() addAct = new EventEmitter<any>();
  @Output() editm = new EventEmitter<any>();
  @Output() closeAct = new EventEmitter<any>();
  @ViewChild("modf") formval: any;
  // @ViewChild('video_file') videofile: any;
  // @ViewChild('image_file') imagefile: any;
  // @ViewChild('audio_file') audiofile: any;
  // @ViewChild('iframe_file') iframe: any;
  // @ViewChild('ppt_file') ppt: any;
  // @ViewChild('word_file') word: any;
  // @ViewChild('excel_file') excel: any;
  // @ViewChild('scorm_file') scormfile: any;
  tagList: any = [];
  tempTags: any = [];
  selectedTags: any = [];
  settingsTagDrop = {};
  formDataActivityCopy: any = {};
  //activityLogId =LOGTYPE.activity;
  currentRoleId: any;
  formDataActivity: any = {
    activityDuration: '',
    activityId: 0,
    completionCriteria: 1,
    completionDate: '',
    completionDays: 0,
    contentId: 0,
    contentRepId: '',
    courseId: 0,
    creditAllocId: 0,
    dependentActId: 0,
    description: '',
    moduleId: 0,
    name: '',
    points: '',
    reference: '',
    startDate: '',
    tags: null,
    //tenantId: 1,
    workflowStepId: '',
    craditpoints: [
      {
        crid: 0,
        roleId: '',
        pformatid: '',
        bcpoints: '',
        acpoints: ''
      }
    ],
    sessionDuration: ''
  };
  noDataVal:noData={
		margin:'mt-3',
		imageSrc: '../../../../../assets/images/no-data-bg.svg',
		title:"No session at this time.",
		desc:"",
		titleShow:true,
		btnShow:true,
		descShow:false,
		btnText:'Learn More',
		btnLink:'https://faq.edgelearning.co.in/kb/learning-how-to-add-a-session-to-a-batch',
	  }
  usersList: any = [];
  tempUsers: any = [];
  selectedCreator: any = [];
  settingsCreatorDrop = {};
  days: any = [
    { id: 1, name: 1 },
    { id: 2, name: 2 },
    { id: 3, name: 3 },
    { id: 4, name: 4 },
    { id: 5, name: 5 },
    { id: 6, name: 6 },
    { id: 7, name: 7 },
    { id: 8, name: 8 },
    { id: 9, name: 9 },
    { id: 10, name: 10 },
    { id: 11, name: 11 },
    { id: 12, name: 12 },
    { id: 13, name: 13 },
    { id: 14, name: 14 },
    { id: 15, name: 15 },
    { id: 16, name: 16 },
    { id: 17, name: 17 },
    { id: 18, name: 18 },
    { id: 19, name: 19 },
    { id: 20, name: 20 },
    { id: 21, name: 21 },
    { id: 22, name: 22 },
    { id: 23, name: 23 },
    { id: 24, name: 24 },
    { id: 25, name: 25 },
    { id: 26, name: 26 },
    { id: 27, name: 27 },
    { id: 28, name: 28 },
    { id: 29, name: 29 },
    { id: 30, name: 30 }
  ];

  showEnrolpage: boolean = false;

  settings = {
    columns: {
      id: {
        title: 'ID'
      },
      name: {
        title: 'Full Name'
      },
      username: {
        title: 'User Name'
      },
      email: {
        title: 'Email'
      }
    }
  };

  workflowSteps = [];

  // activityTypeList:any = [{
  //   id : 1,
  //   name : 'FILE',
  //   icon : 'ion ion-document'
  // },{
  //   id : 2,
  //   name : 'CONTENT',
  //   icon : 'ion ion-ios-box'
  // },{
  //   id : 3,
  //   name : 'SCORM',
  //   icon : 'ion ion-cube'
  // },{
  //   id : 4,
  //   name : 'EXTERNAL',
  //   icon : 'ion ion-filing'
  // }];

  selectedActivityType: any = {
    id: '',
    name: ''
  };

  enrolData: any[] = [];
  expanded: any = {};
  timeout: any;
  date = new Date();
  query: string = '';
  public getData;

  chosenValue: any;
  public data: Object[];
  public today = new Date();
  public selectedStartDate = new Date();
  private placeHolder: string = 'Select a date';

  public myDatePickerOptions: INgxMyDpOptions = {
    // other options...
    dateFormat: 'yyyy-mm-dd',
    disableUntil: {
      year: this.today.getFullYear(),
      month: this.today.getMonth() + 1,
      day: this.today.getDate()
    }
  };
  public expectedDateOptions: INgxMyDpOptions = {
    dateFormat: 'yyyy-mm-dd',
    disableUntil: {
      year: this.today.getFullYear(),
      month: this.today.getMonth() + 1,
      day: this.today.getDate()
    }
  };
  public myDatePickerOptions1: INgxMyDpOptions;

  myOptions: INgxMyDpOptions = {
    // other options...
    dateFormat: 'dd.mm.yyyy'
  };

  // Initialized to specific date (09.10.2018).
  public model: any = { date: { year: 2018, month: 4, day: 9 } };
  private startDatePlaceHolder: string = 'Start Date';
  private endDatePlaceHolder: string = 'End date';

  private endDate: INgxMyDpOptions = {
    // other end date options here...
    //date:Date=new Date();
    dateFormat: 'yyyy-mm-dd',
    disableSince: { year: 0, month: 0, day: 0 }
  };

  // optional date changed callback
  private startDate: INgxMyDpOptions = {
    // start date options here...
    dateFormat: 'yyyy-mm-dd',
    //selectionTxtFontSize :'12px',
    disableSince: { year: 0, month: 0, day: 0 }
  };

  errorMsg: string;
  loader: any;

  strArrayPar: any = [
    {
      pId: 1,
      pName: 'Activity Completion'
    },
    {
      pId: 2,
      pName: 'User Profile Fields'
    },
    {
      pId: 3,
      pName: 'Grades'
    },
    {
      pId: 2,
      pName: 'Date From'
    },
    {
      pId: 3,
      pName: 'Date Until'
    }
  ];

  strArrayTypePar: any = [];

  strArrayType: any = [[]];

  formdataRules: any = {
    parId: '',
    parName: '',
    pDBName: '',
    toDt: '',
    fromDt: '',
    Value1: '',
    Value2: '',
    ptypeId: '',
    ptypeName: ''
  };

  qtd: any[] = [];
  controlList: any = [];
  controlFlag: any = false;
  public myForm: FormGroup;

  users: any = [];
  filters: any = [];
  report_id: any = {
    id: 1
  };

  topic: any = [];

  topicRes1: any = [
    {
      id: '',
      course: '',
      section: '',
      name: '',
      summary: '',
      summaryformat: '',
      sequence: '',
      visible: '',
      availability: ''
    }
  ];

  resourse: any = [];

  title: any;
  formdata: any;
  profileUrl: any = 'assets/images/edgeicon.jpg';

  isActive = false;
  isActiveRes = false;
  isActiveTop = false;
  // isActiveTop = true;

  removeRes: any = false;
  removeTop: any = false;
  resOfTopic: any = [];

  interval: any;

  fileUrl: any;
  fileName: any = 'You can drag and drop files here to add them.';
  fileIcon: any = 'assets/img/app/profile/avatar4.png';
  enableUpload: any = false;
  startdateSelected: boolean = false;
  showResource: any = false;
  formdataTopic = {
    tid: '',
    tName: '',
    tTags: '',
    tSummary: ''
  };
  formdataRes = {
    tid: '',
    rid: '',
    rType: '',
    rName: '',
    rTags: '',
    rSummary: ''
  };

  parentcat: any = [
    {
      id: '',
      name: ''
    }
  ];

  resType: any = [
    {
      rTypeId: 1,
      rTypeName: 'File'
    },
    {
      rTypeId: 2,
      rTypeName: 'Other'
    }
  ];

  compTrackType: any = [
    {
      cTypeId: 1,
      cTypeName: 'Do not indicate activity completion'
    },
    {
      cTypeId: 2,
      cTypeName: 'Students can manually mark the activity as completed'
    },
    {
      cTypeId: 3,
      cTypeName: 'Show activity as complete when conditions are met'
    }
  ];

  enableCompTrack: any = [
    {
      ecTypeId: 1,
      ecTypeName: 'Yes'
    },
    {
      ecTypeId: 2,
      ecTypeName: 'No'
    }
  ];

  courseFormat: any = [
    {
      cfTypeId: 'topics',
      cfTypeName: 'Topics Format'
    },
    {
      cfTypeId: 'site',
      cfTypeName: 'Site'
    }
  ];

  courseLayout: any = [
    {
      clTypeId: 1,
      clTypeName: 'Show all sections on one page'
    },
    {
      clTypeId: 2,
      clTypeName: 'Show one section per page'
    }
  ];

  hiddenSection: any = [
    {
      hTypeId: 1,
      hTypeName: 'Hidden sections are always shows in collapsed form'
    },
    {
      hTypeId: 2,
      hTypeName: 'Hidden sections are completely invisible'
    }
  ];

  courseReviewCheck: any = {
    value: false
  };

  reviewCheck: any = {
    value1: false,
    value2: false,
    value3: false
  };

  categoryData: any;
  ModulesListData: any;
  resourceData: any;
  secResources: any;
  formattedStartDate: any;
  formattedEndDate: any;
  sha1hash: any;
  sha1hashHex: any;
  uploadFileData: any;
  getCourseId: any;
  cData: any;

  result: any;
  topicResult: any;
  addTopicResult: any;
  NewTopicData: any = null;
  tempNewTopData: any = null;

  formdataTopic1: any = {
    id: '',
    course: '',
    section: '',
    name: '',
    summary: '',
    summaryformat: '',
    sequence: '',
    visible: '',
    availability: ''
  };

  conData: any;

  dropdownListUsers: any;
  selectedItemsUsers: any;
  dropdownSettingsUsers: any;
  demoData: any = [];
  saveCourseID: any;
  // sorting
  key: string; //set default
  reverse: boolean;
  private ValueId: number = 0;
  selectedFilterOption = [];
  search2: any = {};
  formattedPara: any;
  filterData: any = {
    userProfFields: null,
    gradeGreatEqual: null,
    gradeLess: null,
    compTrack: null,
    dateUntil: null,
    dateFrom: null,
    remarks: null
  };
  fileReaded: any;
  lines = [];
  uploadedData: any;
  exceltojson: any;
  resultSheets: any;
  newValueRes: any;
  oldValueRes: any;
  elementRefRes: any;
  showRestrict: any = false;
  showTags: any = false;
  showRestrictTop: any = false;
  showTagsTop: any = false;
  tab1: any = true;
  tab2: any = false;
  tab3: any = false;
  tab4: any = false;
  tab5: any = false;
  topicRes: any = {};
  tempNewResData: any = null;
  tempTopData: any = null;
  tempResData: any = null;
  newValueTop: any;
  oldValueTop: any;
  elementRefTop: any;
  tabLoadTimes: Date[] = [];
  enrolment: any = {
    manual: '',
    rule: '',
    regulatory: '',
    self: ''
  };

  items = ['Javascript', 'Typescript'];
  tags = ['@item'];

  moduleIconUrl: any = 'assets/images/course1.jpg';

  formDataModule: any = {
    moduleId: '',
    courseId: '',
    moduleName: '',
    summary: '',
    visible: '',
    modulePic: this.moduleIconUrl,
    courseOrder: '',
    tenantId: '',
    learnerCreditPoints: '',
    learnerRoleId: 8,
    creditPoints: '',
    creatorId: '',
    startDate: '',
    endDate: '',
    standardId: '',
    moduleCode:'',
    trainers: [
      {
        standardId: '',
        trainerId: '',
        train: [],
        visible: '',
        traineresId: 0
      }
    ]
  };

  visibility: any = [];
  tenantId: any;
  userLoginData: any;
  currentUId: any;
  moduledisplaylist = false;
  // currentRoleId :any;
  courseNav: any = {
    currentModule: '',
    CurrentActivity: ''
  };
  isWorkflow: boolean = false;
  constructor(
    private spinner: NgxSpinnerService,
    protected webApiService: webAPIService,
    private datePipe: DatePipe,
    // private toasterService: ToasterService,
    private toastr: ToastrService,
    private _fb: FormBuilder,
    vcr: ViewContainerRef,
    protected service: AddEditBlendCourseContentService,
    protected ModuleService: modulesBlendService,
    private router: Router,
    private route: ActivatedRoute,
    private modalService: NgbModal,
    public cdf: ChangeDetectorRef,
    private http1: HttpClient,
    private authservice: AuthenticationService,
    private LogServices: LogServices,
    public commonFunctionService: CommonFunctionsService,
    private render: Renderer2,
    private dataSeparatorService: DataSeparatorService,
  ) {
    //   if(this.train){
    //   this.train.valueChanges.subscribe(
    //     result =>{
    //       console.log(result,"result");
    //       if(this.train.valid){
    //         this.trainer = true;
    //       }
    //       else{
    //         this.trainer = false;
    //       }
    //   }

    // );}
    this.spinner.show()
    this.getHelpContent();
    this.getlogMsg();

    if (localStorage.getItem('LoginResData')) {
      this.userLoginData = JSON.parse(localStorage.getItem('LoginResData'));
      this.tenantId = this.userLoginData.data.data.tenantId;
      this.currentRoleId = this.userLoginData.data.otherdata[0][0].roleId;
    }
    console.log('user Login Data :-', this.userLoginData);

    if (this.userLoginData.data.data) {
      this.currentUId = this.userLoginData.data.data.id;
    }
    this.tagList = this.service.tagList;
    this.tempTags = [... this.tagList];
    this.settingsTagDrop = {
      text: 'Select Tags',
      singleSelection: false,
      classes: 'myclass custom-class',
      primaryKey: 'id',
      labelKey: 'name',
      noDataLabel: 'Search Tags...',
      enableSearchFilter: true,
      lazyLoading: true,
      searchBy: ['name'],
      maxHeight: 250,
    };

    if (this.service.isWorkflow) {
      this.isWorkflow = true;
    } else {
      this.isWorkflow = false;
    }

    this.settingsCreatorDrop = {
      text: 'Select Trainer',
      singleSelection: true,
      // classes: 'myclass custom-class',
      primaryKey: 'trainerName',
      labelKey: 'trainerName',
      // noDataLabel: 'Search Trainer...',
      enableSearchFilter: true,
      searchBy: ['trainerName', 'agencyName'],
      maxHeight: 250,
      lazyLoading: true,
    };

    this.myDateValue = new Date();

    this.qtd;
    this.myForm = new FormGroup({
      FilterOpt: new FormControl(),
      Value1: new FormControl(),
      Value2: new FormControl()
    });

    this.getEnrolledData(data => {
      this.enrolData = data;
    });

    this.interval = setInterval(() => {
      this.getLength();
    }, 1000);

    //  this.getCourseCat();
    this.makeModuleDataReady();
    // this.getCourseModules();
    this.makeCourseDataReady();

    this.getActDrop();
    this.getallDrop();
    this.getTokenAndUserData();

  }

  ngOnInit(){
    this.spinner.show();
    setTimeout(() => {
      this.spinner.hide()
    }, 7000);
  }


  ngOnChanges(changes: SimpleChanges): void {
    if (this.inpdata === 'modules') {
      this.addEditCourseModule('', 0);
    } else if (this.inpdata === 'cancel') {
      this.closeModuleForm()
      this.closeActivityForm();
    } else if (this.inpdata === 'save') {
      this.saveModuleDetails(this.formval, this.train);
    }else if (this.inpdata === 'closeMod') {
      // this.saveModuleDetails(this.formval);
      this.closeModuleForm();
      this.closeActivityForm();
    }
  }

  GlobalDamToken = null;
  dataDetail: string = null;

  getTokenAndUserData() {
    // this.spinner.show();
    // const userCredentials = {
    //   Username: 'admin',
    //   password: '123456',
    //   tId: 1
    // }
    const token = this.authservice.getToken();
    let result = null;
    if (token && this.GlobalDamToken == null) {
      this.ModuleService.generatingToken(null).then(
        rescompData => {
          this.loader = false;
          result = rescompData;
          this.GlobalDamToken = result['token'];
          console.log(this.GlobalDamToken);
          console.log('Response token Data with activity Result', result);
          this.cdf.detectChanges();
          if (this.GlobalDamToken != null) {
            this.dataDetail = 'Search content...';
            this.noDataFoundActivitySearch = {
              errorKey: "searchContent",
              message: "Search content...",
            };
          } else {
            this.dataDetail = 'You Have no access to the Dam Asset';
            this.noDataFoundActivitySearch = {
              errorKey: "token_key",
              message: "You have no access to the Dam Asset",
            };
          }
        },
        resUserError => {
          this.loader = false;
          this.errorMsg = resUserError;
        }
      );
    }
  }

  // getCourseCat(){
  //   this.ModuleService.getCategories().then(rescompData => {
  //     this.loader =false;
  //     this.categoryData = rescompData['data'][0];
  //     console.log('Category Result',this.categoryData);
  //     this.cdf.detectChanges();
  //   },
  //   resUserError => {
  //     this.loader =false;
  //     this.errorMsg = resUserError
  //   });
  // }

  //new code start//
  moduleImgData: any = null;
  readModuleThumb(event: any) {
    var validExts = new Array('.png', '.jpg', '.jpeg');
    var fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
      // var toast: Toast = {
      //   type: 'error',
      //   title: 'Invalid file selected!',
      //   body: 'Valid files are of ' + validExts.toString() + ' types.',
      //   showCloseButton: true,
      //   // tapToDismiss: false,
      //   timeout: 2000
      //   // onHideCallback: () => {
      //   //     this.router.navigate(['/pages/plan/users']);
      //   // }
      // };
      // this.toasterService.pop(toast);

      this.toastr.warning('Valid file types are ' + validExts.toString(), 'Warning', {
        closeButton: false
      })
      // this.deleteCourseThumb();
    } else {
      if (event.target.files && event.target.files[0]) {
        this.moduleImgData = event.target.files[0];

        var reader = new FileReader();

        reader.onload = (event: ProgressEvent) => {
          // this.defaultThumb = (<FileReader>event.target).result;
          this.formDataModule.modulePic = (<FileReader>event.target).result;
          this.cdf.detectChanges();
        };
        reader.readAsDataURL(event.target.files[0]);
        this.cdf.detectChanges();
      }
    }
    // this.cdf.detectChanges();
  }

  deleteModuleThumb() {
    // this.defaultThumb = 'assets/images/category.jpg';
    this.formDataModule.modulePic = 'assets/images/category.jpg';
    this.moduleImgData = null;
    this.formDataModule.categoryPicRefs = undefined;
  }

  courseModules: any;
  courseId: any;
  getCourseModules() {
    this.spinner.show();
    var courseId = {
      courseId: this.courseId,
      tId: this.tenantId,
    };
    this.ModuleService.getCourseMod(courseId).then(
      rescompData => {
        this.loader = false;
        this.spinner.hide();
        this.courseModules = rescompData['data'];
        this.activitieslist = rescompData["actDrop"]
        if (this.courseModules.length == 0) {
          this.moduledisplaylist = true;
        } else {
          this.moduledisplaylist = false;
          this.bindActivityIconsToActivity(this.courseModules);
        }
        console.log('Course modules with activity Result', this.courseModules);
        console.log("activity list for actdropdata", this.activitieslist);
        this.cdf.detectChanges();
      },
      resUserError => {
        this.loader = false;
        this.spinner.hide();
        this.errorMsg = resUserError;
      }
    );
  }
  aid: any;
  makeModuleDataReady() {
    this.spinner.show();

    var content;
    var optId;
    var catId;
    var newCourseId;

    if (this.service.data != undefined) {
      content = this.service.data.data;
      if (content == undefined) {
        content = {
          courseId: this.service.data.courseId,
        }
      }
      optId = this.service.data.id;
      catId = this.service.data.catId;
      this.aid = optId;
      this.conData = this.service.data.data;
      this.courseId = content ? content.courseId : this.service.data.courseId;
      newCourseId = this.service.courseId;
    } else {
      optId = 0;
      this.aid = optId;
    }

    // this.getCourseModules();

    console.log('Course data in module', content);
    console.log('OPTid', optId);
    console.log('CATid', catId);

    var courseId = {
      courseId: this.courseId,
      tId: this.tenantId,
    };

    if (this.courseId) {
      this.ModuleService.getCourseMod(courseId).then(
        rescompData => {
          this.loader = false;
          // this.spinner.hide();
          this.courseModules = rescompData['data'];
          this.activitieslist = rescompData["actDrop"];
          if (this.courseModules.length == 0) {
            this.moduledisplaylist = true;
          } else {
            this.bindActivityIconsToActivity(this.courseModules);
            this.moduledisplaylist = false;
          }
          console.log(
            'Course modules with activity Result',
            this.courseModules
          );
          console.log("activity list for actdropdata", this.activitieslist);
          this.cdf.detectChanges();
        },
        resUserError => {
          this.loader = false;
          this.spinner.hide();
          this.errorMsg = resUserError;
        }
      );
    }
  }

  getTrainers(moduleId){
    this.spinner.show();
    var id = {
      iId: moduleId,
    };
    this.ModuleService.getTrainers(id).then(
      rescompData => {
        this.loader = false;
        this.spinner.hide();
        this.moduleTrainers = rescompData['data']
        this.formDataModule.trainers =  this.moduleTrainers
        if (this.moduleTrainers) {
          for (let i = 0; i < this.formDataModule.trainers.length; i++) {
            this.formDataModule.trainers[i].train = [];
            for (let j = 0; j < this.tempUsers.length; j++) {
              if (
                this.formDataModule.trainers[i].trainerId == this.tempUsers[j].id
              ) {
                this.formDataModule.trainers[i].train.push(this.tempUsers[j]);
              }
              if (this.formDataModule.trainers[i].trainerId == 0) {
                this.formDataModule.trainers[i].traineresId = true;
              }
            }
          }
        }
        this.isTrain = true
        // this.courseModules = rescompData['data'];
        // this.activitieslist = rescompData["actDrop"]

        console.log('Course modules with activity Result', this.moduleTrainers);
        console.log("activity list for actdropdata", this.activitieslist);
        this.cdf.detectChanges();
      },
      resUserError => {
        this.loader = false;
        this.spinner.hide();
        this.errorMsg = resUserError;
      }
    );
  }

  tempModData: any;
  addEditCourseModule(moduleData, optId) {
    this.aid = optId
    console.log(moduleData,"moduleData")
    if(moduleData){
    this.getTrainers(moduleData.moduleId)
    }

    // var moduleData:any = this.courseModules;
    // var optId = 0;
    if (optId == 1) {
    // if(this.isTrain == true){
      this.editev.emit();
      this.title = 'Edit Session';
      this.formDataModule = {
        moduleId: moduleData.moduleId,
        courseId: moduleData.courseId,
        moduleName: moduleData.modulename,
        summary: moduleData.summary,
        visible: moduleData.visible,
        moduleCode:moduleData.moduleCode,
        modulePic: moduleData.modulePic
          ? moduleData.modulePic
          : this.moduleIconUrl,
        courseOrder: moduleData.courseOrder,
        tenantId: moduleData.tenantId,
        learnerCreditPoints: moduleData.points,
        learnerRoleId: 8,
        creditPoints: '',
        creatorId: moduleData.creatorId,
        creditAllocId: moduleData.creditAllocId,
        startDate: new Date(moduleData.startDate),
        endDate: new Date(moduleData.endDate),
        standardId: moduleData.standardId,
        trainerId: moduleData.trainerId,
        // trainers: moduleData.trainers ? moduleData.trainers : [],
        trainers: this.moduleTrainers ? this.moduleTrainers : [],

      };
      this.editm.emit(this.formDataModule.moduleName);
      console.log("start date:", new Date(moduleData.startDate).toLocaleString("en-US", { timeZone: "Asia/Kolkata" }));
      console.log("start date:", new Date(moduleData.endDate).toLocaleString("en-US", { timeZone: "Asia/Kolkata" }));
      //   var nomi =
      //   var nomi =
      // {
      //   id:0,
      //   trainerName:'Request for Nomination',
      //   standardId:'',
      //   agencyName:''
      // }
      // this.tempUsers.push(nomi);
      // if (moduleData.trainers) {
      if (this.moduleTrainers) {
        for (let i = 0; i < this.formDataModule.trainers.length; i++) {
          this.formDataModule.trainers[i].train = [];
          for (let j = 0; j < this.tempUsers.length; j++) {
            if (
              this.formDataModule.trainers[i].trainerId == this.tempUsers[j].id
            ) {
              this.formDataModule.trainers[i].train.push(this.tempUsers[j]);
            }
            if (this.formDataModule.trainers[i].trainerId == 0) {
              this.formDataModule.trainers[i].traineresId = true;
            }
          }
        }
      }

      //  this.tempUsers.forEach((user,key) => {
      //   if(user.id == moduleData.trainerId){
      //     this.selectedCreator.push(user);
      //   }
      // });
      console.log(this.formDataModule.trainers)
      this.courseNav.currentModule = moduleData;
    // }
  }
    else {
      this.title = 'Add Session';
      this.editm.emit(this.title);
      this.trainerVisible = true
      this.formDataModule = {
        moduleId: 0,
        courseId: this.courseId,
        moduleName: '',
        summary: '',
        visible: 1,
        modulePic: this.moduleIconUrl,
        courseOrder: 0,
        tenantId: this.tenantId,
        learnerCreditPoints: '',
        learnerRoleId: 8,
        creditPoints: '',
        creatorId: this.currentUId,
        creditAllocId: 0,
        moduleCode:'',
        startDate: '',
        endDate: '',
        standardId: '',
        trainerId: '',
        trainers: [
          // {
          //   standardId: '',
          //   trainerId: '',
          //   train: [],
          //   visible: '',
          //   traineresId: 0
          // }
        ]
      };
    }

    if (moduleData != this.tempModData) {
      if (this.isActive == true && this.isActiveRes == true) {
        this.isActiveRes = false;
        this.tempNewResData = {};
        this.tempResData = null;
        this.isActiveTop = true;
        this.isActive = true;
      } else if (this.isActiveTop == false) {
        // this.isActive = !this.isActive;
        this.isActive = true;
        this.isActiveTop = true;
      }
      // else{
      //   this.isActive = !this.isActive;
      //   this.isActiveTop  = false;
      // }
    } else {
      moduleData = null;
      // this.isActive = !this.isActive;
      this.isActive = false;
      this.isActiveTop = false;
    }
    this.tempModData = moduleData;
  // }
  }

  addEditModRes: any;
  fileUploadRes: any;
  addEditModule(url, moduleData) {
    // this.loader = true;
    this.spinner.show();
    this.webApiService.getService(url, moduleData).then(
      rescompData => {
        // this.loader =false;
        this.spinner.hide();
        var temp: any = rescompData;
        this.addEditModRes = temp.data;
        if (temp == 'err') {
          // var modUpdate: Toast = {
          //   type: 'error',
          //   title: 'Session',
          //   body: 'Unable to update Session.',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(modUpdate);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });

        } else if (temp.type == false) {
          // var modUpdate: Toast = {
          //   type: 'error',
          //   title: 'Session',
          //   body: this.addEditModRes[0].msg,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(modUpdate);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });

        } else {
          if (this.aid == 0) {
            this.toastr.success('Session created successfully', 'Success', {
              closeButton: false
            });
            // var modUpdate: Toast = {
            //   type: 'success',
            //   title: 'Session',
            //   body: 'New session Created Successfully',
            //   showCloseButton: true,
            //   timeout: 2000
            // };
          }
          else {
            this.toastr.success('Session Updated successfully', 'Success', {
              closeButton: false
            });
            //   // var modUpdate: Toast = {
            //   //   type: 'success',
            //   //   title: 'Session',
            //   //   body: 'Session Updated Successfully',
            //   //   showCloseButton: true,
            //   //   timeout: 2000
            //   // };
          }

          // this.toasterService.pop(modUpdate);
          this.getTrainers(moduleData.moduleId)
          this.getCourseModules();
          this.closeModuleForm();
          this.cdf.detectChanges();
        }
        console.log('Module AddEdit Result ', this.addEditModRes);
        this.cdf.detectChanges();
      },
      resUserError => {
        this.loader = false;
        this.errorMsg = resUserError;
      }
    );
  }
  formatDatetime(data) {
    if (data) {

      data = new Date(data);
      var day = data.getDate();
      var monthIndex = ('0' + (data.getMonth() + 1)).slice(-2);
      var year = data.getFullYear();
      var hours = ('0' + data.getHours()).slice(-2);
      var minutes = ('0' + data.getMinutes()).slice(-2);
      var seconds = ('0' + data.getSeconds()).slice(-2);

      return year + '-' + monthIndex + '-' + day + ' ' + hours + ':' + minutes + ':' + seconds;

    }
  }

  saveModuleDetails(f, f2) {
    this.valid  = true
    console.log(f, "f");
    console.log(f2, "f2")
    if(this.formDataModule && this.formDataModule.trainers.length>0){
      this.validation()
    }
    if (f.valid && this.valid == true) {
      // this.loader = true;
      this.spinner.show();
      console.log('this.formDataModule', this.formDataModule);
      var credidPointsStr = '';
      if (!this.formDataModule.creditPoints) {
        credidPointsStr +=
          '0' +
          '|' +
          '8' +
          '|' +
          '1' +
          '|' +
          this.formDataModule.learnerCreditPoints;
      } else {
        credidPointsStr = this.formDataModule.creditPoints;
      }
      if (this.checkDuplicateInObject('trainerId', this.formDataModule.trainers)) {
        this.toastr.warning('You cannot add same trainer in list', 'Warning');
        this.spinner.hide();
        return null;
      }
      if (this.formDataModule.trainers) {
        for (let i = 0; i < this.formDataModule.trainers.length; i++) {
          delete this.formDataModule.trainers[i].train;
          delete this.formDataModule.trainers[i].isCheck
          if (this.formDataModule.trainers[i].traineresId) {
            this.formDataModule.trainers[i].trainerId = 0;
            delete this.formDataModule.trainers[i].traineresId;
          } else {
            delete this.formDataModule.trainers[i].traineresId;
          }
        }

        console.log(this.formDataModule.trainers, "trainers")

        var option: string = Array.prototype.map
          .call(this.formDataModule.trainers, function (item) {
            console.log('item', item);
            return Object.values(item).join('#');
          })
          .join('|');
      }
      if (option == "") {
        option = null;
      }
      console.log('credidPointsStr', credidPointsStr);
      console.log(this.formDataModule);
      console.log(this.selectedCreator);
      var moduleData = {
        moduleId: this.formDataModule.moduleId,
        courseId: this.formDataModule.courseId,
        moduleName: this.formDataModule.moduleName,
        summary: this.formDataModule.summary,
        visible: this.formDataModule.visible,
        modulePic: this.formDataModule.modulePic,
        courseOrder: this.formDataModule.courseOrder,
        tenantId: this.formDataModule.tenantId,
        learnerCreditPoints: this.formDataModule.learnerCreditPoints,
        learnerRoleId: this.formDataModule.learnerRoleId,
        creditPoints: credidPointsStr,
        creatorId: this.currentUId,
        creditAllocId: this.formDataModule.creditAllocId,
        startDate: this.formatDatetime(this.formDataModule.startDate),
        endDate: this.formatDatetime(this.formDataModule.endDate),
        standardId: this.formDataModule.standardId,
        trainerId:
          this.formDataModule.trainers.length > 0 ? this.formDataModule.trainers[0].trainerId : null,
        trainers: option
      };
      console.log('Session data ', moduleData);

      var fd = new FormData();
      fd.append('content', JSON.stringify(moduleData));
      fd.append('file', this.moduleImgData);
      console.log('File Data ', fd);

      console.log('Course Data Img', this.moduleImgData);
      console.log('Course Data ', moduleData);

      let url = webApi.domain + webApi.url.addEditModuleBatch;
      let fileUploadUrl = webApi.domain + webApi.url.fileUpload;
      let param = {
        tId: this.tenantId
      };

      if (this.moduleImgData) {
        this.webApiService.getService(fileUploadUrl, fd).then(
          rescompData => {
            this.loader = false;
            var temp: any = rescompData;
            // this.fileUploadRes = JSON.parse(temp);
            this.fileUploadRes = temp;
            this.spinner.hide();
            if (temp == 'err') {
              // this.notFound = true;
              // var thumbUpload: Toast = {
              //   type: 'error',
              //   title: 'Session Thumbnail',
              //   body: 'Unable to upload Session thumbnail.',
              //   // body: temp.msg,
              //   showCloseButton: true,
              //   timeout: 2000
              // };
              // this.toasterService.pop(thumbUpload);

              this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
                timeOut: 0,
                closeButton: true
              });
            } else if (temp.type == false) {
              // var thumbUpload: Toast = {
              //   type: 'error',
              //   title: 'Session Thumbnail',
              //   body: 'Unable to upload Session thumbnail.',
              //   // body: temp.msg,
              //   showCloseButton: true,
              //   timeout: 2000
              // };
              // this.toasterService.pop(thumbUpload);

              this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
                timeOut: 0,
                closeButton: true
              });
            } else {
              if (
                this.fileUploadRes.data != null ||
                this.fileUploadRes.fileError != true
              ) {
                moduleData.modulePic = this.fileUploadRes.data.file_url;
                this.addEditModule(url, moduleData);
                this.cdf.detectChanges();
              } else {
                // var thumbUpload: Toast = {
                //   type: 'error',
                //   title: 'Session Thumbnail',
                //   // body: 'Unable to upload course thumbnail.',
                //   body: this.fileUploadRes.status,
                //   showCloseButton: true,
                //   timeout: 2000
                // };
                // this.toasterService.pop(thumbUpload);

                this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
                  timeOut: 0,
                  closeButton: true
                });
              }
            }
            console.log('File Upload Result', this.fileUploadRes);
          },
          resUserError => {
            // this.loader =false;
            this.spinner.hide();
            this.errorMsg = resUserError;
          }
        );
      } else {
        this.addEditModule(url, moduleData);
        this.cdf.detectChanges();
      }
    } else {
      console.log('Please Fill all fields');
      // const modUpdate: Toast = {
      //   type: 'error',
      //   title: 'Unable to update',
      //   body: 'Please Fill all fields',
      //   showCloseButton: true,
      //   timeout: 2000
      // };
      // this.toasterService.pop(modUpdate);

      this.toastr.warning('Please fill in the required fields', 'Warning', {
        closeButton: false
      });
      Object.keys(f.controls).forEach(key => {
        f.controls[key].markAsDirty();
      });
    }

  }


  checkDuplicateInObject(propertyName, inputArray) {
    console.log(inputArray, "inputArray")
    let seenDuplicate = false,
      testObject = {};
    inputArray.map(function (item) {
      console.log(item, "item")
      if (item[propertyName] != 0) {
        let itemPropertyName = item[propertyName];
        console.log(itemPropertyName, "itemPropertyName")
        if (itemPropertyName in testObject) {
          console.log(testObject, "testObject")
          console.log(testObject[itemPropertyName].duplicate, "jjkbjbjh")
          item.duplicate = true;
          seenDuplicate = true;
        }
        else {
          testObject[itemPropertyName] = item;
          delete item.duplicate;
        }
      }
    });

    return seenDuplicate;
  }

  validation(){
    for(let i= 0;i<this.formDataModule.trainers.length;i++){
      if(this.formDataModule.trainers[i]['train'].length == 0 && this.trainer == false )
      {
        this.valid = false
      }
      else{
        this.valid = true
      }
    }
  }

  makeCreditPointsReady() {
    var credidPointsStr = '0' + '|' + '8' + '|' + '1' + '|' + '0';
  }

  activityDataRes: any;
  currentActivityData: any;
  getActivityData(activityData, optId) {
    this.spinner.show();

    var actData = {
      supertypeId: activityData.supertypeId,
      subtypeId: activityData.activityTypeId,
      // instanceId : activityData.activityInstanceId,
      instanceId: activityData.activityId,
    };
    this.currentActivityData = activityData;
    if (activityData.supertypeId) {
      this.selectedActivitySupType = Number(activityData.supertypeId);
    } else {
      this.selectedActivitySupType = '';
    }

    if (activityData.activityTypeId) {
      this.selectedActivitySubType = Number(activityData.activityTypeId);
    } else {
      this.selectedActivitySubType = '';
    }
    this.findActivityNameBySubType();
    this.ModuleService.getCourseModActivity(actData).then(
      rescompData => {
        this.loader = false;
        this.spinner.hide();
        this.activityDataRes = rescompData['data'][0];
        console.log('Activity Result', this.activityDataRes);
        if (rescompData['type'] == true) {
          if (rescompData['data'].length > 0) {
            this.makeActivityDatReady(this.activityDataRes, optId);
            this.checkActivityUrl(this.activityDataRes);
            this.cdf.detectChanges();
          } else {
            // var activityFetch: Toast = {
            //   type: 'error',
            //   title: 'Activity',
            //   body: 'No data found.',
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(activityFetch);

            this.toastr.error('No data found', 'Error', {
              timeOut: 0,
              closeButton: true
            });
          }
        } else {
          // var activityFetch: Toast = {
          //   type: 'error',
          //   title: 'Activity',
          //   body: 'Unable to access data at this time.',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(activityFetch);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        }
      },
      resUserError => {
        this.loader = false;
        this.spinner.hide();
        this.errorMsg = resUserError;
      }
    );
  }

  // makeActivityFormDataReady(actData){

  // }

  selectFileTitle: any = 'No file chosen';
  makeActivityDatReady(actData, optId) {
    // this.makeActivityFormDataReady(actData);
    if (actData.activityDuration) {
      var resStr = actData.activityDuration.split(':');
      var formattedDuration = new Date();
      formattedDuration.setHours(resStr[0], resStr[1], resStr[2]);
    } else {
    }
    // var formattedDuration = new Date(resStr);
    // var finalStr = resStr.join();

    this.formDataActivity = {
      activityName: actData.activityName,
      activityDuration: formattedDuration,
      activityId: actData.activityId,
      completionCriteria: actData.completionCriteria,
      completionDate: actData.completionDate,
      completionDays: actData.completionDays,
      contentId: actData.contentId,
      contentRepId: actData.contentRepId,
      courseId: actData.courseId,
      creditAllocId: actData.creditAllocId,
      dependentActId: Number(actData.dependentActId),
      description: actData.description,
      moduleId: actData.moduleId,
      name: actData.name,
      points: actData.points,
      reference: actData.reference,
      startDate: actData.startDate,
      tags: actData.tagIds == null ? actData.tagIds : actData.tagIds.split(","),
      tenantId: actData.tenantId,
      craditpoints: actData.craditpoints ? actData.craditpoints : [],
      sessionDuration: String(actData.sessionDuration),
      startDays: actData.startDays,
      byTrainer: actData.byTrainer == 1 ? true : false,
      byLearner: actData.byLearner == 1 ? true : false,
      workflowStepId: this.isWorkflow ? actData.workflowStepId : null,
      formatId: actData.formatId,
    };
    this.showContentSelected = true;
    this.showContentSelectedData.name = this.formDataActivity.name;
    this.showContentSelectedData.refImage = this.formDataActivity.reference;
    if (this.formDataActivity.tags) {
      this.tempTags.forEach((tag) => {
        this.formDataActivity.tags.forEach((tagId) => {
          if (tag.id == tagId) {
            this.selectedTags.push(tag);
          }
        });
      });

    }
    // we use this for fetching the labels of form
    this.formDataActivityCopy = { ... this.formDataActivity };
    // this.formDataActivityCopy.craditpoints =[];
    this.formDataActivityCopy.craditpointsStr = Array.prototype.map
      .call(this.formDataActivity.craditpoints, function (item) {
        console.log("item", item);
        return Object.values(item).join("|");
      })
      .join("#");
    console.log('this.formDataActivityCopy', this.formDataActivityCopy);
    console.log('this.formDataActivity', this.formDataActivity);
    this.getallDrop();
    this.existingActivityData = this.formDataActivity;
    if (Number(this.selectedActivitySubType) == 2) {
      this.makeMultiActivitycontentDataReady(actData);
    }
    if (this.formDataActivity.dependentActId) {
      this.actStartDate = '1';
      // this.startDateRadio = false;
    } else if (this.formDataActivity.startDate) {
      this.actStartDate = '2';
      // this.startDateRadio = true;
    }

    if (this.formDataActivity.completionDays) {
      this.actCompDate = '1';
      // this.endDateRadio = false;
    } else if (this.formDataActivity.completionDate) {
      this.actCompDate = '2';
      // this.endDateRadio = true;
    }

    if (this.formDataActivity.reference) {
      this.fileName = this.formDataActivity.reference.substring(
        this.formDataActivity.reference.lastIndexOf('/') + 1
      );
      this.selectFileTitle = this.fileName;
      this.enableUpload = true;
    }

    if (this.currentActivityData) {
      if (
        this.currentActivityData.activityTypeId == 2 ||
        this.currentActivityData.activityTypeId == 5 ||
        this.currentActivityData.activityTypeId == 6 ||
        this.currentActivityData.activityTypeId == 12
      ) {
        this.activityContentSelected = true;
      } else {
        this.activityContentSelected = false;
      }
    }

    this.openActForm(this.formDataActivity);
  }

  // formDataActivity:any;
  compDateSelected: boolean = false;
  compDaySelected: boolean = false;
  actCompDate: any = '1';
  actStartDate: any = '1';
  activitieslist: any = [];
  currentModuleData: any;
  exisitingSelectedActivityData: any;
  currentActIndex: any = null;
  currentModIndex: any = 0;
  currentActivityName: any = '';
  actFormType: any;
  addEditActivity(activityData, moduleData, optId, currentActivityIndex, currentModIndex) {
    this.selectedTags = [];
    this.formDataActivity = {};
    this.currentModuleData = moduleData;
    this.exisitingSelectedActivityData = activityData;
    // this.activitieslist = moduleData.activity;
    this.currentActivityName = activityData.activityName
    this.currentModIndex = currentModIndex;
    this.courseNav.currentModule = moduleData;
    this.shortUrl = '';

    if(this.selectedActivitySubType == 2){
      this.getTokenAndUserData();
    }
    this.actFormType = optId;

    if (currentActivityIndex == null) {
      if (this.activitieslist.length > 0) {
        this.currentActIndex = this.activitieslist.length + 1;
      } else {
        this.currentActIndex = 0;
      }
    } else {
      this.currentActIndex = currentActivityIndex == 0 && currentModIndex > 0 ? currentModIndex : currentActivityIndex;
    }

    if (optId == 1) {
      let object={
        'name':this.courseNav.currentModule.modulename,
        'type':'Edit '+activityData.activityName
      }
     this.addAct.emit(object);
      this.title = 'Edit Activity';
      if (activityData) {
        // this.onActivityCheckBoxClick(true, activityData.supertypeId);
        this.selectedActivitySupType = activityData.supertypeId;
      }
      this.getActivityData(activityData, optId);
    } else {
      this.title = "Add "+ this.selectedType.subTypeName ;
      let object={
        'name':this.courseNav.currentModule.modulename,
        'type':this.title
      }
     this.addAct.emit(object);
      // this.title = 'Add Activity';
      this.formDataActivity = {
        activityName: '',
        activityDuration: '',
        activityId: 0,
        completionCriteria: 1,
        completionDate: '',
        completionDays: 0,
        contentId: 0,
        contentRepId: '',
        courseId: Number(moduleData.courseId),
        creditAllocId: 0,
        dependentActId: 0,
        description: '',
        moduleId: Number(moduleData.moduleId),
        name: '',
        points: '',
        reference: '',
        startDate: '',
        tags: null,
        tenantId: this.tenantId,
        craditpoints: [
          {
            crid: 0,
            roleId: '',
            pformatid: '',
            bcpoints: '',
            acpoints: ''
          }
        ],
        sessionDuration: '',
        startDays: '',
        byTrainer: '',
        byLearner: '',
        formatId: '',
      };
      this.showGenrateButton = false;
      // this.defaultActivityData = this.formDataActivity;
      this.showContentSelected = true;
      this.showContentSelectedData.name = this.formDataActivity.name;
      this.showContentSelectedData.refImage = this.formDataActivity.reference;

      if (this.currentActivityData) {
        this.currentActivityData = {};
      }

      // if(this.currentActivityData){
      this.activityContentSelected = false;
      // this.selectedActivitySubType = 2;
      //}

      this.fileName = 'You can drag and drop files here to add them.';
      this.selectFileTitle = 'No file chosen';

      this.activityFileData = null;

      this.actStartDate = '1';
      this.actCompDate = '1';
      this.selectedMultiLanguageContent = [];
      // this.onActivityCheckBoxClick(false, 0);
      this.setDefaultActivityData(moduleData);
      this.openActForm(this.formDataActivity);
      this.getallDrop();
    }
  }

  defaultActivityData: any;
  existingActivityData: any;
  setDefaultActivityData(moduleData) {
    var activityData = this.exisitingSelectedActivityData;
    // if(activityData){
    //   this.onActivityCheckBoxClick(activityData.supertypeId);
    // }
    this.selectedTags = [];
    this.formDataActivity = {
      activityName: '',
      activityDuration: '',
      activityId: 0,
      completionCriteria: 1,
      completionDate: '',
      completionDays: 0,
      contentId: 0,
      contentRepId: '',
      courseId: Number(moduleData.courseId),
      creditAllocId: 0,
      dependentActId: 0,
      description: '',
      moduleId: Number(moduleData.moduleId),
      name: '',
      points: '',
      reference: '',
      startDate: '',
      tags: null,
      tenantId: this.tenantId,
      craditpoints: [
        {
          crid: 0,
          roleId: '',
          pformatid: '',
          bcpoints: '',
          acpoints: ''
        }
      ],
      sessionDuration: '',
      startDays: '',
      byTrainer: '',
      byLearner: '',
      formatId: '',
    };

    if (this.selectedActivitySupType == 4 && this.selectedActivitySubType == 9) {
      this.formDataActivity.byTrainer = true;
      this.formDataActivity.byLearner = false;
    }

    this.roleSelectedList = [];
    this.selectedRole = [];
    this.getallDrop();
    this.activityContentSelected = false;
    // this.cancelFile();

    if (this.formDataActivity.dependentActId) {
      this.actStartDate = '1';
      this.startDateRadio = false;
    } else if (this.formDataActivity.startDate) {
      this.actStartDate = '2';
      this.startDateRadio = true;
    } else {
      this.actStartDate = '1';
      this.startDateRadio = false;
    }

    if (this.formDataActivity.completionDays) {
      this.actCompDate = '1';
      this.endDateRadio = false;
    } else if (this.formDataActivity.completionDate) {
      this.actCompDate = '2';
      this.endDateRadio = true;
    } else {
      this.actCompDate = '1';
      this.endDateRadio = false;
    }
    this.showContentSelected = false;
    // this.selectedContentData.
  }

  setExistingActivityData() {
    this.formDataActivity = this.existingActivityData;

    if (this.formDataActivity.dependentActId) {
      this.actStartDate = '1';
      this.startDateRadio = false;
    } else if (this.formDataActivity.startDate) {
      this.actStartDate = '2';
      this.startDateRadio = true;
    }

    if (this.formDataActivity.completionDays) {
      this.actCompDate = '1';
      this.endDateRadio = false;
    } else if (this.formDataActivity.completionDate) {
      this.actCompDate = '2';
      this.endDateRadio = true;
    }

    if (this.formDataActivity.reference) {
      this.fileName = this.formDataActivity.reference.substring(
        this.formDataActivity.reference.lastIndexOf('/') + 1
      );
      this.selectFileTitle = this.fileName;
    }
  }

  openActForm(actData) {
    var NewResData = actData;

    if (NewResData != this.tempNewResData) {
      if (this.isActive == true && this.isActiveTop == true) {
        this.isActiveTop = false;
        this.tempModData = {};
        this.isActiveRes = true;
        this.isActive = true;
      } else if (this.isActiveRes == false) {
        // this.isActive = !this.isActive;
        this.isActive = true;
        this.isActiveRes = true;
      }
      // else{
      //   this.isActive = !this.isActive;
      //   this.isActiveRes  = false;
      // }
    } else {
      NewResData = null;
      // this.isActive = !this.isActive;
      this.isActive = false;
      this.isActiveRes = false;
    }
    this.tempNewResData = NewResData;
  }

  activityDropRes: any;
  activityTypeList: any = [];
  activitySubTypeList: any = [];
  activityCompTrack: any = [];
  trainerStandard: any = [];
  trainerList: any = [];
  formats:any =[];
  getActDrop() {
    this.loader = true;
    this.spinner.hide();
    this.ModuleService.getModActivityDrop().then(
      rescompData => {
        this.loader = false;
        // this.spinner.hide();
        this.activityDropRes = rescompData['data'];
        console.log('Activity dropdown Result', this.activityDropRes);
        if (rescompData['type'] == true) {
          this.activityTypeList = this.activityDropRes.activityTypeList;
          this.activitySubTypeList = this.activityDropRes.activitySubTypeList;
          console.log(this.activitySubTypeList);
          this.spinnerHide = true
          this.activityCompTrack = this.activityDropRes.activityCompTrack;
          this.trainerList = this.activityDropRes.trainers;
          this.visibility = this.activityDropRes.visibility;
          this.formats = this.activityDropRes.formats;
          console.log(this.visibility, "visibility")
          this.trainerStandard = this.activityDropRes.trainerStandard;
          this.workflowSteps = this.activityDropRes.workflowSteps;
          this.languageList = this.activityDropRes.language;
          this.tempUsers = [...this.trainerList];
          console.log(this.tempUsers, "this.tempUsers")
          this.cdf.detectChanges();
        } else {
          this.spinnerHide = true
          // var activityDropFetch: Toast = {
          //   type: 'error',
          //   title: 'Activity',
          //   body: 'Unable to access data at this time.',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(activityDropFetch);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        }
      },
      resUserError => {
        this.loader = false;
        this.spinnerHide = true
        this.spinner.hide();
        this.errorMsg = resUserError;
      }
    );
  }

  selectedActivitySupType: any;
  // onActivityCheckBoxClick(currenrEvent, actType) {
  //   this.clearActType();

  //   if (currenrEvent) {
  //     switch (actType) {
  //       // case 'content':
  //       // case 'content':
  //       case 1:
  //         this.activityType.content = true;
  //         this.selectedActivitySupType = 1;
  //         this.selectedActivitySubType = 2;
  //         break;
  //       // case 'assesment':
  //       case 2:
  //         this.activityType.assesment = true;
  //         this.selectedActivitySupType = 2;
  //         this.selectedActivitySubType = 5;
  //         break;
  //       // case 'Assigement':
  //       case 3:
  //         this.activityType.assignment = true;
  //         this.selectedActivitySupType = 5;
  //         break;
  //       // case 'management':
  //       case 4:
  //         this.activityType.management = true;
  //         this.selectedActivitySupType = 4;
  //         this.selectedActivitySubType = 9;
  //         break;

  //       // case 'communication':
  //       case 5:
  //         this.activityType.communication = true;
  //         this.selectedActivitySupType = 4;
  //         break;
  //       // case 'Evaluation':
  //       case 6:
  //         this.activityType.evaluation = true;
  //         this.selectedActivitySupType = 6;
  //         this.selectedActivitySubType = 12;
  //         break;
  //       default:
  //         this.activityType.content = true;
  //         this.selectedActivitySupType = 1;
  //         // this.activityType.assignment = true;
  //         break;
  //     }

  //     this.setDefaultActivityData(this.currentModuleData);
  //   }
  // }

  // clearActType() {
  //   this.activityType = {
  //     content: false,
  //     assesment: false,
  //     management: false,
  //     communication: false,
  //     assignment: false,
  //     evaluation: false
  //   };
  //   this.selectedActivitySupType = '';
  // }

  setDefActType() {
    this.activityType = {
      content: false,
      assesment: false,
      management: false,
      communication: false,
      assignment: false,
      evaluation: false
    };
    this.selectedActivitySupType = 1;
  }

  startDateRadio: boolean = false;
  startDateRadioClicked(curEvent) {
    console.log('Start date Current Event', curEvent);
    if (curEvent == 2) {
      this.formDataActivity.dependentActId = '';
      this.startDateRadio = true;
    } else {
      this.formDataActivity.startDate = '';
      this.startDateRadio = false;
    }
  }

  endDateRadio: boolean = true;
  // compDateRadioClicked(curEvent){
  //   console.log('Comp date Current Event', curEvent);
  //   if(curEvent == 2){
  //     this.formDataActivity.completionDays = '';
  //     this.endDateRadio = true;
  //   }else{
  //     this.formDataActivity.completionDate = '';
  //     this.endDateRadio = false;
  //   }
  // }


  formatDateReady(data, type) {
    if (data) {
      if (type == 1) {
        data = new Date(data);
        var day = data.getDate();
        var monthIndex = ('0' + (data.getMonth() + 1)).slice(-2);
        var year = data.getFullYear();

        return year + '-' + monthIndex + '-' + day;
      } else {
        data = new Date(data);
        var hours = ('0' + data.getHours()).slice(-2);
        var minutes = ('0' + data.getMinutes()).slice(-2);
        var seconds = ('0' + data.getSeconds()).slice(-2);

        return hours + ':' + minutes + ':' + seconds;
      }
    }
  }

  validTime: any;
  validStartDate: any;
  validCompDate: any;
  // checkedAnyOneCheckBox = true;

  makeActivityDataReady() {
    this.validTime = this.formatDateReady(
      this.formDataActivity.activityDuration,
      2
    );
    if (this.formDataActivity.startDate) {
      this.validStartDate = this.formatDateReady(
        this.formDataActivity.startDate,
        1
      );
    }
    if (this.formDataActivity.completionDate) {
      this.validCompDate = this.formatDateReady(
        this.formDataActivity.completionDate,
        1
      );
    }
    if (this.selectedTags.length > 0) {
      this.makeTagDataReady(this.selectedTags);
      // this.formdata.tags = this.formattedTags;
    }
  }

  saveActivityDetails(event) {
    // console.log('f', f);
    // console.log('point form ', pointform);
    let f = event.formObject;
    this.formDataActivity = event.formData;
    this.selectedTags = event.selectedTags;
    this.activityFileData = event.activityFileData;
    if (f.valid) {
      this.loader = true;
      this.spinner.show();
      this.makeActivityDataReady();

      var credidPointsStr = "";
      if (!this.formDataActivity.creditAllocId) {
        credidPointsStr +=
          "0" + "|" + "8" + "|" + "1" + "|" + this.formDataActivity.points;
      } else {
        credidPointsStr =
          this.formDataActivity.creditAllocId +
          "|" +
          "8" +
          "|" +
          "1" +
          "|" +
          this.formDataActivity.points;
      }

      console.log("credidPointsStr", credidPointsStr);
      var mimetype = null;
      var reftype = null;
      if (this.activeSelectedContent) {
        if (this.activeSelectedContent.mime_type) {
          mimetype = this.activeSelectedContent.mime_type;
        }
        if (this.activeSelectedContent.file_type) {
          reftype = this.activeSelectedContent.file_type;
        }
      }
      var option: string = Array.prototype.map
        .call(this.formDataActivity.craditpoints, function (item) {
          console.log("item", item);
          return Object.values(item).join("|");
        })
        .join("#");
      this.formDataActivity.craditpointsStr = option;
      // onnthis function we find the diffrence beetween 2 objects
      var modifiedJsone = this.createLogJson(this.formDataActivity, this.formDataActivityCopy);
      // on this function we have to create statements for store change fields
      this.createStatmentsforActivityLog(modifiedJsone);
      console.log('modifiedJsone', modifiedJsone);
      if (this.formDataActivity.formatId) {
        var formatId = this.formDataActivity.formatId;
      }
      else if (this.activityDataRes) {
        formatId = this.activityDataRes.formatId;
      }
      else {
        formatId = null;
      }
      var activityData = {
        activityName: this.formDataActivity.activityName,
        activityDuration: this.validTime,
        activityId: Number(this.formDataActivity.activityId),
        completionCriteria: this.formDataActivity.completionCriteria,
        completionDate: this.validCompDate,
        completionDays: Number(this.formDataActivity.completionDays),
        contentId: Number(this.formDataActivity.contentId),
        contentRepId: this.formDataActivity.contentRepId,
        courseId: Number(this.formDataActivity.courseId),
        // creditAllocId: this.formDataActivity.creditAllocId,
        dependentActId: Number(this.formDataActivity.dependentActId),
        description: this.formDataActivity.description,
        moduleId: Number(this.formDataActivity.moduleId),
        cname: this.formDataActivity.name,
        points: credidPointsStr,
        reference: this.formDataActivity.reference,
        startDate: this.validStartDate,
        tags: this.formDataActivity.tagIds,
        tenantId: Number(this.formDataActivity.tenantId),
        // credidPoints: credidPointsStr
        // supertypeId: this.currentActivityData.supertypeId,
        supertypeId: Number(this.selectedActivitySupType),
        // activityTypeId: this.currentActivityData.activityTypeId,
        activityTypeId: Number(this.selectedActivitySubType),
        usermodified: Number(this.currentUId),
        mimeType: mimetype,
        referenceType: reftype,
        craditpoints: option,
        sessionDuration: this.formDataActivity.sessionDuration,
        startDays: this.formDataActivity.startDays,
        byTrainer: this.formDataActivity.byTrainer == true ? 1 : 0,
        byLearner: this.formDataActivity.byLearner == true ? 1 : 0,
        formatId: formatId,
        //formatId:  this.formDataActivity.formatId ? this.formDataActivity.formatId: this.activityDataRes.formatId,

        workflowStepId: this.isWorkflow
          ? this.formDataActivity.workflowStepId
          : null,
          languageId:
          this.selectedMultiLanguageContent.length != 0
            ? this.selectedMultiLanguageContent[0]["langId"]
            : this.languageList ? this.languageList[0]['languageId'] : 1,
      };
      activityData['Allstring'] = this.makeMulticontentDataReady();
      console.log("Activity Data ", activityData);

      var fd = new FormData();
      fd.append("content", JSON.stringify(activityData));
      fd.append("file", this.activityFileData);
      console.log("File Data ", fd);

      console.log("Course Data Img", this.activityFileData);
      console.log("Course Data ", activityData);

      let url = webApi.domain + webApi.url.addEditActivityBatch;
      let fileUploadUrl = webApi.domain + webApi.url.fileUpload;
      let param = {
        tId: this.tenantId
      };

      if (this.activityFileData) {
        this.webApiService.getService(fileUploadUrl, fd).then(
          rescompData => {
            this.loader = false;
            this.spinner.hide();
            var temp: any = rescompData;
            this.fileUploadRes = temp;
            // this.fileUploadRes = JSON.parse(temp);
            this.cdf.detectChanges();
            if (temp == "err") {
              // this.notFound = true;
              // var fileUpload: Toast = {
              //   type: 'error',
              //   title: 'Activity',
              //   body: 'Unable to upload Activity content.',
              //   // body: temp.msg,
              //   showCloseButton: true,
              //   timeout: 2000
              // };
              // this.toasterService.pop(fileUpload);

              this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
                timeOut: 0,
                closeButton: true
              });
            } else if (temp.type == true) {
              if (this.fileUploadRes.fileExists == true) {
                this.existingActivity(this.fileUploadRes, activityData);
              } else {
                if (
                  this.fileUploadRes.data != null ||
                  this.fileUploadRes.fileError != true
                ) {
                  activityData.reference = this.fileUploadRes.data.file_url;
                  activityData.contentRepId = this.fileUploadRes.data.id;
                  activityData.referenceType = null;
                  if (this.fileUploadRes.data.mime_type) {
                    activityData.mimeType = this.fileUploadRes.data.mime_type;
                  } else {
                    activityData.mimeType = null;
                  }

                  if (this.fileUploadRes.data.file_type) {
                    activityData.referenceType = this.fileUploadRes.data.file_type;
                  } else {
                    activityData.referenceType = null;
                  }

                  console.log("activityData after file upload:", activityData);
                  this.addEditActivityData(url, activityData);
                  // this.cdf.detectChanges();
                } else {
                  // var fileUpload: Toast = {
                  //   type: "error",
                  //   title: "Activity",
                  //   // body: "Unable to upload course thumbnail.",
                  //   body: this.fileUploadRes.status,
                  //   showCloseButton: true,
                  //   timeout: 2000
                  // };
                  // this.toasterService.pop(fileUpload);

                  this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
                    timeOut: 0,
                    closeButton: true
                  });
                }
                // var fileUpload: Toast = {
                //   type: "error",
                //   title: "Activity",
                //   body: "Unable to upload Activity content.",
                //   // body: temp.msg,
                //   showCloseButton: true,
                //   timeout: 2000
                // };
                // this.toasterService.pop(fileUpload);

                // this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
                //   timeOut: 0,
                //   closeButton: true
                // });
              }
            }else {
              //  this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
              //     timeOut: 0,
              //     closeButton: true
              //   });
              this.toastr.warning('Unexpected Error', 'Warning');
            }
            console.log("File Upload Result", this.fileUploadRes);
          },
          resUserError => {
            this.loader = false;
            this.errorMsg = resUserError;
          }
        );
      } else {
        this.addEditActivityData(url, activityData);
        this.cdf.detectChanges();
      }
    } else {
      console.log('Please Fill all fields');
      // const addEditF: Toast = {
      //   type: 'error',
      //   title: 'Unable to update',
      //   body: 'Please Fill all fields',
      //   showCloseButton: true,
      //   timeout: 2000
      // };
      // this.toasterService.pop(addEditF);

      // this.toastr.warning('Please fill in the required fields', 'Warning', {
      //   closeButton: false
      // });
      // Object.keys(f.controls).forEach(key => {
      //   f.controls[key].markAsDirty();
      // });
    }
  }

  addEditActivityRes: any;
  addEditActivityData(url, activityData) {
    this.spinner.show();
    this.webApiService.getService(url, activityData).then(
      rescompData => {
        this.loader = false;
        this.spinner.hide();
        var temp: any = rescompData;
        this.addEditActivityRes = temp.data;
        this.cdf.detectChanges();
        if (temp == 'err') {
          // var actUpdate: Toast = {
          //   type: 'error',
          //   title: 'Activity',
          //   body: 'Unable to update Activity.',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(actUpdate);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        } else if (temp.type == false) {
          // var actUpdate: Toast = {
          //   type: 'error',
          //   title: 'Activity',
          //   body: this.addEditActivityRes[0].msg,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(actUpdate);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        } else {
          // var actUpdate: Toast = {
          //   type: 'success',
          //   title: 'Activity',
          //   body: this.addEditActivityRes[0].msg,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(actUpdate);

          this.toastr.success(this.addEditActivityRes[0].msg, 'Success', {
            closeButton: false
          });

          console.log('this.addEditActivityRes[0].actId', this.addEditActivityRes[0])
          var param = {
            courseId: activityData.courseId,
            moduleId: activityData.moduleId,
            activityId: activityData.activityId ? activityData.activityId : this.addEditActivityRes[0].actId,
            activityName: activityData.activityName,
            userId: activityData.usermodified,
            actionType: activityData.activityId ? 'EDIT ACTIVITY' : 'ADD ACTIVITY',
            activityData: JSON.stringify(activityData),
            otherData: this.addEditActivityRes[0].msg,
            role: this.currentRoleId,
            origin: 'ADMIN',
            url: window.location.href,
            ip: '',
            crud: activityData.activityId ? 'U' : 'C',
            tenantId: this.tenantId,
            logType: this.activityLogId,
            changeString: activityData.activityId ? this.changeString : this.logMsg.addActivity,
          }
          this.LogServices.saveLogged(param);
          this.getCourseModules();
          this.closeActivityForm();
        }
        console.log('Activity AddEdit Result ', this.addEditActivityRes);
        this.cdf.detectChanges();
      },
      resUserError => {
        this.loader = false;
        this.errorMsg = resUserError;
      }
    );
  }

  existingActivityModal: boolean = false;
  existingActivityFileData: any;
  existingFileData: any;
  existingActivity(fileData, activity) {
    console.log('File content', fileData);
    this.existingFileData = fileData;
    this.existingActivityFileData = activity;
    this.existingActivityModal = true;
    this.cdf.detectChanges();
  }

  existingActivityAction(actionType) {
    if (actionType == true) {
      this.proceedExistingFile(this.existingActivityFileData);
    } else {
      this.closeExistingActivityModal();
    }
  }

  closeExistingActivityModal() {
    this.existingActivityModal = false;
  }

  proceedExistingFile(activityData) {
    let url = webApi.domain + webApi.url.addEditActivityBatch;
    if(this.existingFileData.data['docs'] && this.existingFileData.data['docs'].length != 0){
      activityData.reference = this.existingFileData.data.docs[0].file_url;
      activityData.contentRepId = this.existingFileData.data.docs[0].id;
      activityData.mimeType = this.existingFileData.data.docs[0].mime_type;
      this.addEditActivityData(url, activityData);
     this.closeExistingActivityModal();
    }else {
      this.toastr.warning('file data error', 'Warning');
    }


  }

  activityFileData: any = null;
  // readFileUrl(event: any, indx) {
  //   var size = 100000000;
  //   if (indx == 1) {
  //     var validExts = new Array('image', 'video', 'audio', 'pdf');
  //   } else if (indx == 3) {
  //     var validExts = new Array('rar', 'zip', '7z');
  //   }
  //   if (indx == 10) {
  //     var validExts = new Array('image');
  //   }
  //   var fileType = event.target.files[0].type;
  //   // fileType = fileType.substring(0,5);

  //   // var validExts = new Array('.png', '.jpg', '.jpeg',);
  //   // var fileExt = event.target.files[0].name;
  //   // fileExt = fileExt.substring(fileExt.lastIndexOf('.'));

  //   var fileExt = false;
  //   for (let i = 0; i < validExts.length; i++) {
  //     if (fileType.includes(validExts[i])) {
  //       // return true;
  //       fileExt = true;
  //       break;
  //     }
  //   }

  //   // if(validExts.indexOf(fileType) < 0) {
  //   if (!fileExt) {
  //     // var toast: Toast = {
  //     //   type: 'error',
  //     //   title: 'Invalid file selected!',
  //     //   body: 'Valid files are of ' + validExts.toString() + ' types.',
  //     //   showCloseButton: true,
  //     //   timeout: 2000
  //     // };
  //     // this.toasterService.pop(toast);

  //     this.toastr.warning('Valid file types are ' + validExts.toString(), 'Warning', {
  //       closeButton: false
  //     });
  //     this.cancelFile();
  //   } else {
  //     if (size <= event.target.files[0].size) {
  //       // var toast: Toast = {
  //       //   type: 'error',
  //       //   title: 'File size exceeded!',
  //       //   body: 'File size should be less than 100MB',
  //       //   showCloseButton: true,
  //       //   timeout: 2000
  //       // };
  //       // this.toasterService.pop(toast);

  //       this.toastr.warning('File size should be less than 100MB', 'Warning', {
  //         closeButton: false
  //       });
  //       this.cancelFile();
  //     } else {
  //       if (event.target.files && event.target.files[0]) {
  //         this.fileName = event.target.files[0].name;
  //         //read file from input
  //         this.fileReaded = event.target.files[0];
  //         this.activityFileData = event.target.files[0];

  //         if (
  //           this.fileReaded != '' ||
  //           this.fileReaded != null ||
  //           this.fileReaded != undefined
  //         ) {
  //           this.enableUpload = true;
  //         }
  //         let file = event.target.files[0];
  //         var reader = new FileReader();
  //         reader.onload = (event: ProgressEvent) => {
  //           this.fileUrl = (<FileReader>event.target).result;
  //         };
  //         reader.readAsDataURL(event.target.files[0]);
  //       }
  //     }
  //   }
  // }

  // cancelFile() {
  //   // this.fileUpload.nativeElement.files = [];
  //   this.activityFileData = null;
  //   // console.log(this.fileUpload.nativeElement.files);
  //   if (this.fileUpload) {
  //     if (this.fileUpload.nativeElement) {
  //       this.fileUpload.nativeElement.value = '';
  //     }
  //   }

  //   // console.log(this.fileUpload.nativeElement.files);
  //   this.fileName = 'You can drag and drop files here to add them.';
  //   this.selectFileTitle = 'No file chosen';
  //   this.enableUpload = false;
  // }

  selectedActivitySubType: any = null;
  // activateActivityTab(event, activity, currentIndex) {

  //   console.log("Activity name", activity);
  //   // workflow.wrkflwStatus = 'active';
  //   // console.log('Activity Form ',this.workflowForm);
  //   // this.addRemoveClassElement(this.activityForm, 'active');
  //   this.selectedActInd = currentIndex;
  //   this.selectedActivitySubType = activity.id;

  //   if (this.currentActivityData) {
  //     if (activity.id == this.currentActivityData.activityTypeId) {
  //       // this.formDataActivity = this.existingActivityData;
  //       this.setExistingActivityData();
  //     } else {
  //       // this.formDataActivity = this.defaultActivityData;
  //       this.setDefaultActivityData(this.currentModuleData);
  //     }
  //   } else {
  //     // this.formDataActivity = this.defaultActivityData;
  //     this.setDefaultActivityData(this.currentModuleData);
  //   }
  // }

  srchContentResult: any = [];
  contentSearchQry: string = '';
  // searchActivityContent(currentEvent, srchStr) {
  //   var srchQuery = currentEvent;
  //   console.log('contentSearchQry ', srchStr);
  //   var param = {
  //     searchString: srchStr,
  //     tId: this.tenantId,
  //     userId: this.currentUId,
  //     token: this.GlobalDamToken,
  //     assetType: this.selectedFormatId,
  //   };
  //   if (srchStr.length > 3 && this.GlobalDamToken != null) {
  //     this.spinner.show();
  //     this.ModuleService.ApprovedDamHanshakeData(param).then(
  //       rescompData => {
  //         this.spinner.hide();
  //         if (rescompData) {
  //           this.srchContentResult = rescompData['AssetData'];
  //           if (this.srchContentResult.length == 0 && rescompData['type'] == false) {
  //             this.dataDetail = 'You Have no access to the Dam Asset';
  //           } else {
  //             this.dataDetail = 'No Asset Available';
  //           }

  //         } else {
  //           this.srchContentResult = [];
  //           this.dataDetail = 'No Asset Available';

  //         }

  //         console.log('Search Content Result ', rescompData);
  //         this.cdf.detectChanges();
  //       },
  //       resUserError => {
  //         // this.loader =false;
  //         this.spinner.hide();
  //         this.errorMsg = resUserError;
  //       }
  //     );
  //   }
  // }

  selectedActivityContent(currentIndex, content) {
    console.log('Selected content ', content);
  }

  activeSelectedContentIndex: any;
  activeSelectedContent: any;
  activeSelectedContentId: any;
  activeSelectedContentName: any;

  setActiveSelectedContent(currentIndex, currentContent) {
    this.activeSelectedContentIndex = currentIndex;
    this.activeSelectedContent = currentContent;
    this.activeSelectedContentId = currentContent.id;
    this.activeSelectedContentName = currentContent.filename;
  }

  activityContentShow: boolean = false;
  activityContentSelected: boolean = false;
  selectedFormatId = null;
  browseActivityContent(event) {
    this.activityContentShow = true;
    this.contentSearchQry = '';
    this.selectedFormatId = event.formatId;
  }

  closeActivityContentModel() {
    this.activityContentShow = false;
    this.srchContentResult = [];
    this.activeSelectedContentId = null;
  }

  showContentSelected = false;
  showContentSelectedData = {
    name : '',
    refImage: '',
  }
  saveSelectedActivityContent() {
    console.log('Selected activity content ', this.activeSelectedContent);
    // this.formDataActivity.reference = this.activeSelectedContent.file_url;
    // this.formDataActivity.reference = this.activeSelectedContent.assetRef;
    // this.formDataActivity.contentRepId = this.activeSelectedContent.id;
    // this.formDataActivity.name = this.activeSelectedContent.assetName;
    // this.formDataActivity.description = this.activeSelectedContent.description;
    // this.activityContentSelected = true;
    // this.formDataActivity.formatId = this.activeSelectedContent.formatId

    this.formDataActivity.reference = this.activeSelectedContent.assetRef;
    this.formDataActivity.contentRepId = this.activeSelectedContent.id;
    this.formDataActivity.name = this.activeSelectedContent.assetName;
    // this.formDataActivity.description = this.activeSelectedContent.description;
    this.activityContentSelected = true;
    this.formDataActivity.formatId = this.activeSelectedContent.formatId;
    this.formDataActivity.mimeType =
      this.activeSelectedContent.mimeType ? this.activeSelectedContent.mimeType : this.activeSelectedContent.mime_type;
    this.formDataActivity.referenceType =
      this.activeSelectedContent.referenceType ?
        this.activeSelectedContent.referenceType : this.activeSelectedContent.file_type;

    this.showContentSelected = true;
    this.showContentSelectedData.name = this.formDataActivity.name;
    this.showContentSelectedData.refImage =  this.formDataActivity.reference;

    this.closeActivityContentModel();

    this.enableUpload = true;
  }

  removeSelectedActivityContent() {
    this.formDataActivity.reference = '';
    this.formDataActivity.contentRepId = '';
    this.formDataActivity.name = '';
    this.activeSelectedContentId = '';
    this.formDataActivity.description = '';

    this.activityContentSelected = false;

    this.enableUpload = false;

    this.showContentSelected = false;
    this.showContentSelectedData.name = '';
    this.showContentSelectedData.refImage = '';
  }
  /*-----------------------COURSE QUIZ------------------------*/

  quizContentShow: boolean = false;
  quizContentSelected: boolean = false;
  browsequizContent() {
    this.quizContentShow = true;
    this.quizSearchQry = '';
  }

  closequizContentModel() {
    this.quizContentShow = false;
    this.srchquizResult = '';
  }

  saveSelectedQuizContent() {
    console.log('Selected activity content ', this.activeSelectedQuiz);
    this.formDataActivity.reference = this.activeSelectedQuiz.picRef;
    this.formDataActivity.contentRepId = this.activeSelectedQuiz.qid;
    this.formDataActivity.name = this.activeSelectedQuiz.qName;
    // this.formDataActivity.description = this.activeSelectedQuiz.qdesc;
    this.activityContentSelected = true;

    this.showContentSelected = true;
    this.showContentSelectedData.name = this.activeSelectedQuiz.qName;
    this.showContentSelectedData.refImage = this.activeSelectedQuiz.picRef;
    this.closequizContentModel();

    this.enableUpload = true;
  }

  removeSelectedQuizContent() {
    this.formDataActivity.reference = '';
    this.formDataActivity.contentRepId = '';
    this.formDataActivity.name = '';
    this.activeSelectedQuizId = '';
    this.formDataActivity.description = '';

    this.activityContentSelected = false;

    this.enableUpload = false;
    this.showContentSelected = false;
    this.showContentSelectedData.name = '';
    this.showContentSelectedData.refImage = '';
  }

  activeSelectedQuizIndex: any;
  activeSelectedQuiz: any;
  activeSelectedQuizId: any;
  // activeSelectedContentName:any;

  setActiveSelectedQuiz(currentIndex, currentContent) {
    this.activeSelectedQuizIndex = currentIndex;
    this.activeSelectedQuiz = currentContent;
    this.activeSelectedQuizId = currentContent.qid;
    // this.activeSelectedContentName = currentContent.filename;
  }

  srchquizResult: any = [];
  quizSearchQry: string = '';
  searchQuizContent(currentEvent, srchStr) {
    var srchQuery = currentEvent;
    console.log('quizSearchQry ', srchStr);
    var param = {
      tId: this.tenantId,
      searchStr: srchStr
    };
    if (srchStr.length > 3) {
      this.spinner.show();
      this.ModuleService.searchcoursequizContent(param).then(
        rescompData => {
          // this.loader =false;
          this.spinner.hide();
          this.srchquizResult = rescompData['data'];
          console.log('Search Content Result ', rescompData);
          this.cdf.detectChanges();
        },
        resUserError => {
          // this.loader =false;
          this.spinner.hide();
          this.errorMsg = resUserError;
        }
      );
    }
  }
  /*------------------------COURSE FEEDBACK-------------------------------------------*/
  feedbackContentShow: boolean = false;
  feedbackContentSelected: boolean = false;
  browsefeedbackContent() {
    this.feedbackContentShow = true;
    this.feedbackSearchQry = '';
  }

  closefeedbackContentModel() {
    this.feedbackContentShow = false;
    this.srchfeedbackResult = '';
  }

  saveSelectedFeedbackContent() {
    console.log('Selected activity content ', this.activeSelectedFeedback);
    this.formDataActivity.reference = this.activeSelectedFeedback.picRef;
    this.formDataActivity.contentRepId = this.activeSelectedFeedback.fid;
    this.formDataActivity.name = this.activeSelectedFeedback.fname;
    // this.formDataActivity.description = this.activeSelectedFeedback.fdesc;
    this.activityContentSelected = true;

    this.showContentSelected = true;
    this.showContentSelectedData.name = this.activeSelectedFeedback.fname;
    this.showContentSelectedData.refImage = this.activeSelectedFeedback.picRef;
    this.closefeedbackContentModel();

    this.enableUpload = true;
  }

  removeSelectedFeedbackContent() {
    this.formDataActivity.reference = '';
    this.formDataActivity.contentRepId = '';
    this.formDataActivity.name = '';
    this.activeSelectedFeedbackId = '';
    this.formDataActivity.description = '';

    this.activityContentSelected = false;

    this.enableUpload = false;

    this.showContentSelected = false;
    this.showContentSelectedData.name = '';
    this.showContentSelectedData.refImage = '';
  }

  activeSelectedFeedbackIndex: any;
  activeSelectedFeedback: any;
  activeSelectedFeedbackId: any;
  // activeSelectedContentName:any;

  setActiveSelectedFeedback(currentIndex, currentContent) {
    this.activeSelectedFeedbackIndex = currentIndex;
    this.activeSelectedFeedback = currentContent;
    this.activeSelectedFeedbackId = currentContent.fid;
    // this.activeSelectedContentName = currentContent.filename;
  }

  srchfeedbackResult: any = [];
  feedbackSearchQry: string = '';
  searchFeedbackContent(currentEvent, srchStr) {
    var srchQuery = currentEvent;
    console.log('quizSearchQry ', srchStr);
    var param = {
      tId: this.tenantId,
      searchStr: srchStr
    };
    if (srchStr.length > 3) {
      this.spinner.show();
      this.ModuleService.searchcoursefeedbackContent(param).then(
        rescompData => {
          // this.loader =false;
          this.spinner.hide();
          this.srchfeedbackResult = rescompData['data'];
          console.log('Search Content Result ', rescompData);
          this.cdf.detectChanges();
        },
        resUserError => {
          // this.loader =false;
          this.spinner.hide();
          this.errorMsg = resUserError;
        }
      );
    }
  }

  /*-------------------------------------------------------------------------------------*/
  async closeModuleForm() {
    this.isActiveTop = false;
    this.isActive = false;
    // this.tempTopData = null;
    this.tempModData = null;
    this.inpdata = '';
    this.cdf.detectChanges();
    await this.passEc();
  }

  passEc() {
    this.cancelm.emit();
    this.isActiveTop = false;
    this.isActive = false;
    // this.tempTopData = null;
    this.tempModData = null;
    this.inpdata = '';
  }

  closeActivityForm() {
    this.closeAct.emit();
    this.isActiveRes = false;
    this.isActive = false;
    // this.tempResData = null;
    this.tempNewResData = null;
  }

  deleteModuleModal: boolean = false;
  deleteModuleData: any;
  deleteModule(Module) {
    console.log('Module content', Module);
    this.deleteModuleData = Module;
    this.deleteModuleModal = true;
  }

  deleteModuleAction(actionType) {
    if (actionType == true) {
      this.removeModule(this.deleteModuleData);
      this.closeDeleteModuleModal();
    } else {
      this.closeDeleteModuleModal();
    }
  }

  closeDeleteModuleModal() {
    // console.log('Course content',course);
    this.deleteModuleModal = false;
  }

  deleteModuleRes: any;
  removeModule(moduleData) {
    this.spinner.show();
    var data = {
      moduleId: moduleData.moduleId
    };
    this.ModuleService.deleteModule(data).then(
      rescompData => {
        // this.loader =false;
        this.spinner.hide();
        this.deleteModuleRes = rescompData['data'];
        console.log('Delete module result ', rescompData);
        if (rescompData['type'] == true) {
          // var toast: Toast = {
          //   type: 'success',
          //   title: 'Session',
          //   body: 'Session delated sucessfully',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);

          this.toastr.success('Session deleted', 'Success', {
            closeButton: false
          });
          this.getCourseModules();
          this.cdf.detectChanges();
        } else {
          // var toast: Toast = {
          //   type: 'error',
          //   title: 'Session',
          //   body: 'Unable to delete Session',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        }
      },
      resUserError => {
        // this.loader =false;
        this.errorMsg = resUserError;
        // var toast: Toast = {
        //   type: 'error',
        //   title: 'Session',
        //   body: 'Unable to delete Session',
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);

        this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
          timeOut: 0,
          closeButton: true
        });
      }
    );
  }

  deleteActivityModal: boolean = false;
  deleteActivityData: any;
  deleteActivity(Activity) {
    console.log('Activity content', Activity);
    this.deleteActivityData = Activity;
    this.deleteActivityModal = true;
  }

  deleteActivityAction(actionType) {
    if (actionType == true) {
      this.removeActivity(this.deleteActivityData);
      this.closeDeleteActivityModal();
    } else {
      this.closeDeleteActivityModal();
    }
  }

  closeDeleteActivityModal() {
    // console.log('Course content',course);
    this.deleteActivityModal = false;
  }

  deleteActivityRes: any;
  removeActivity(activityData) {
    this.spinner.show();
    var data = {
      activityId: activityData.activityId
    };
    this.ModuleService.deleteActivity(data).then(
      rescompData => {
        // this.loader =false;
        this.spinner.hide();
        this.deleteActivityRes = rescompData['data'];
        console.log('Delete activity result ', rescompData);
        if (rescompData['type'] == true) {
          // var toast: Toast = {
          //   type: 'success',
          //   title: 'Activity',
          //   body: this.deleteActivityRes[0].msg,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          if(this.deleteActivityRes[0].type === 1){
            this.toastr.success(this.deleteActivityRes[0].msg, 'Success', {
              closeButton: false,
            });
          }else {
            this.toastr.warning(this.deleteActivityRes[0].msg, 'Warning', {
              closeButton: false,
            });
          }

          var param = {
            courseId: this.courseId,
            moduleId: activityData.moduleId,
            activityId: activityData.activityId,
            activityName: activityData.activityName,
            userId: activityData.usermodified,
            actionType: 'DELETE ACTIVITY',
            activityData: JSON.stringify(data),
            otherData: this.deleteActivityRes[0].msg,
            role: this.currentRoleId,
            origin: 'ADMIN',
            url: window.location.href,
            ip: '',
            crud: 'D',
            tenantId: this.tenantId,
            logType: this.activityLogId,
            changeString: this.logMsg.deleteActivity,
          }
          this.LogServices.saveLogged(param);
          this.getCourseModules();
          this.cdf.detectChanges();
        } else {
          // var toast: Toast = {
          //   type: 'error',
          //   title: 'Activity',
          //   body: 'Unable to delete activity',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        }
      },
      resUserError => {
        // this.loader =false;
        this.spinner.hide();
        this.errorMsg = resUserError;
        // var toast: Toast = {
        //   type: 'error',
        //   title: 'Activity',
        //   body: 'Unable to delete activity',
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);

        this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
          timeOut: 0,
          closeButton: true
        });
      }
    );
  }

  //new code end//

  //old code start//
  makeCourseDataReady() {
    var content;
    var optId;
    var catId;

    if (this.service.data != undefined) {
      content = this.service.data.data;
      optId = this.service.data.id;
      catId = this.service.data.catId;
      this.conData = this.service.data.data;
    } else {
      optId = 0;
    }

    console.log('Content to Edit', content);
    content?this.moduledisplaylist=false:this.moduledisplaylist=true
    console.log('OPTid', optId);
    console.log('CATid', catId);
    if (optId == 1) {
      this.title = 'Edit Content';
      this.formattedStartDate = content.startdate;
      this.formattedEndDate = content.enddate;
      var sDate: any =
        this.formattedStartDate == 'NA'
          ? ''
          : new Date(this.formattedStartDate);
      var eDate: any =
        this.formattedEndDate == 'NA' ? '' : new Date(this.formattedEndDate);

      this.formdata = {
        cacherev: content.cacherev,
        calendartype: content.calendartype,
        category: content.category,
        completionnotify: content.completionnotify,
        defaultgroupingid: content.defaultgroupingid,
        enablecompletion: content.enablecompletion,
        enddate:
          content.enddate == 'NA'
            ? []
            : {
              date: {
                year: eDate.getFullYear(),
                month: eDate.getMonth() + 1,
                day: eDate.getDate()
              }
            },
        format: content.format,
        fullname: content.fullname,
        groupmode: content.groupmode,
        groupmodeforce: content.groupmodeforce,
        id: content.id,
        idnumber: content.idnumber,
        lang: content.lang,
        legacyfiles: content.legacyfiles,
        marker: content.marker,
        maxbytes: content.maxbytes,
        newsitems: content.newsitems,
        requested: content.requested,
        shortname: content.shortname,
        showgrades: content.showgrades,
        showreports: content.showreports,
        sortorder: content.sortorder,
        startdate:
          content.startdate == 'NA'
            ? []
            : {
              date: {
                year: sDate.getFullYear(),
                month: sDate.getMonth() + 1,
                day: sDate.getDate()
              }
            },
        summary: content.summary,
        summaryformat: content.summaryformat,
        theme: content.theme,
        timecreated: content.timecreated,
        timemodified: content.timemodified,
        visible: content.visible,
        visibleold: content.visibleold,
        contextId: content.contextId,
        contextLevel: content.contextLevel
      };
    } else {
      this.title = 'Add Content';
      console.log(this.title + '  ' + optId);
      this.formdata = {
        cacherev: '',
        calendartype: '',
        // category: '',
        category: catId == null ? '' : catId,
        completionnotify: '',
        defaultgroupingid: '',
        enablecompletion: '',
        enddate: '',
        format: '',
        fullname: '',
        groupmode: '',
        groupmodeforce: '',
        id: 0,
        idnumber: '',
        lang: '',
        legacyfiles: '',
        marker: '',
        maxbytes: '',
        newsitems: '',
        requested: '',
        shortname: '',
        showgrades: '',
        showreports: '',
        sortorder: '',
        startdate: '',
        summary: '',
        summaryformat: '',
        theme: '',
        timecreated: '',
        timemodified: '',
        visible: '',
        visibleold: ''
      };
    }

    this.cData = {
      id: content == undefined ? 0 : content.courseId
    };

    // this.getTopics(this.cData.id);
    // this.getModules(this.cData.id);
  }

  /* topicAddEdit(){
    this.ModuleService.addEditTopics(this.formdataTopic1)
    .then(rescompData => {
      // this.loader =false;
      this.addTopicResult = rescompData['data'][0];
      // this.topic = rescompData.data[0];
      console.log('Add Topics Result',this.addTopicResult);
      // if(this.NewTopicData.id == 0){
        this.getTopics(this.cData.id);
      // }
      this.cdf.detectChanges();
    },
    resUserError => {
      // this.loader =false;
      this.errorMsg = resUserError
    });

  }*/

  /* getTopics(courseId){
    var courseIdData :any = {
      id : courseId
    }
    this.ModuleService.getTopics(courseIdData)
    .then(rescompData => {
      // this.loader =false;
      console.log('Res Compdata '+rescompData['data'][0]);
      this.topicResult = rescompData['data'][0];
      this.topic = rescompData['data'][0];
      console.log('Topics Result',this.topicResult);
      if(this.NewTopicData != null)
        {
          if(this.NewTopicData.id == 0){
            this.newTopicAdd();
          }
        }
        this.cdf.detectChanges();
    },
    resUserError => {
      // this.loader =false;
      this.errorMsg = resUserError
    });
  }*/

  modulesResult: any = [];

  /* getModules(courseId){
    var courseIdData :any = {
      id : courseId
    }
    this.ModuleService.getResource(courseIdData)
    .then(rescompData => {
      // this.loader =false;
      console.log('Res Compdata '+rescompData['data'][0]);
      this.modulesResult = rescompData['data'][0];
      // this.topic = rescompData.data[0];
      console.log('Modules Result',this.modulesResult);
      // if(this.NewTopicData != null)
      //   {
      //     if(this.NewTopicData.id == 0){
      //       this.newTopicAdd();
      //     }
      //   }
      this.cdf.detectChanges();
    },
    resUserError => {
      // this.loader =false;
      this.errorMsg = resUserError
    });
  }*/

  activeButtonTop(topicData, id) {
    var topicdata = topicData;
    this.formdataTopic = {
      tid: topicdata.tid,
      tName: topicdata.tname,
      tTags: topicdata.ttags,
      tSummary: topicdata.tsumm
    };

    this.formdataTopic1 = {
      id: topicdata.id,
      course: topicdata.course,
      section: topicdata.section,
      name: topicdata.name,
      summary: topicdata.summary,
      summaryformat: topicdata.summaryformat,
      sequence: topicdata.sequence,
      visible: topicdata.visible,
      availability: topicdata.availability
    };

    if (topicdata != this.tempTopData) {
      if (this.isActive == true && this.isActiveRes == true) {
        this.isActiveRes = false;
        this.tempResData = null;
        this.isActiveTop = true;
        this.isActive = true;
      } else if (this.isActiveTop == false) {
        // this.isActive = !this.isActive;
        this.isActive = true;
        this.isActiveTop = true;
      }
      // else{
      //   this.isActive = !this.isActive;
      //   this.isActiveTop  = false;
      // }
    } else {
      topicdata = null;
      // this.isActive = !this.isActive;
      this.isActive = false;
      this.isActiveTop = false;
    }
    this.tempTopData = topicdata;
  }

  newTopicAdd() {
    for (let i = 0; i < this.topicResult.length; i++) {
      if (this.topicResult[i].section == this.NewTopicData.section) {
        this.formdataTopic1 = {
          id: this.topicResult[i].id,
          course: this.topicResult[i].course,
          section: this.topicResult[i].section,
          name: this.topicResult[i].name,
          summary: this.topicResult[i].summary,
          summaryformat: this.topicResult[i].summaryformat,
          sequence: this.topicResult[i].sequence,
          visible: this.topicResult[i].visible,
          availability: this.topicResult[i].availability
        };
      }
    }

    var NewTopData = this.NewTopicData;

    if (NewTopData != this.tempNewTopData) {
      if (this.isActive == true && this.isActiveRes == true) {
        this.isActiveRes = false;
        this.isActiveTop = true;
        this.isActive = true;
      } else if (this.isActiveTop == false) {
        // this.isActive = !this.isActive;
        this.isActive = true;
        this.isActiveTop = true;
      }
      // else{
      //   this.isActive = !this.isActive;
      //   this.isActiveTop  = false;
      // }
    } else {
      NewTopData = null;
      // this.isActive = !this.isActive;
      this.isActive = false;
      this.isActiveTop = false;
    }
    this.tempNewTopData = NewTopData;
  }

  saveCourse() {
    this.tab1 = false;
    this.tab2 = true;
    console.log(
      'Form Input Data: ' + this.formdata.fullname + 'Full Data' + this.formdata
    );

    var course = {
      cacherev: this.formdata.cacherev,
      calendartype: this.formdata.calendartype,
      category: this.formdata.category,
      completionnotify: this.formdata.completionnotify,
      defaultgroupingid: this.formdata.defaultgroupingid,
      enablecompletion: this.formdata.enablecompletion,
      // enddate: this.formdata.enddate
      enddate:
        this.formdata.enddate.epoc == undefined
          ? this.formattedEndDate
          : this.formdata.enddate.epoc,
      format: this.formdata.format,
      fullname: this.formdata.fullname,
      groupmode: this.formdata.groupmode,
      groupmodeforce: this.formdata.groupmodeforce,
      id: this.formdata.id,
      idnumber: this.formdata.idnumber,
      lang: this.formdata.lang,
      legacyfiles: this.formdata.legacyfiles,
      marker: this.formdata.marker,
      maxbytes: this.formdata.maxbytes,
      newsitems: this.formdata.newsitems,
      requested: this.formdata.requested,
      shortname: this.formdata.shortname,
      showgrades: this.formdata.showgrades,
      showreports: this.formdata.showreports,
      sortorder: this.formdata.sortorder,
      //startdate: this.formdata.startdate,
      startdate:
        this.formdata.startdate.epoc == undefined
          ? this.formattedStartDate
          : this.formdata.startdate.epoc,
      summary: this.formdata.summary,
      summaryformat: this.formdata.summaryformat,
      theme: this.formdata.theme,
      timecreated: this.formdata.timecreated,
      timemodified: this.formdata.timemodified,
      visible: this.formdata.visible,
      visibleold: this.formdata.visibleold
    };

    // this.saveCourseID = course.id;
    var saveCourseID = course.id;

    var fileData = {
      id: 0,
      // contenthash : this.fileReadedArr,
      contenthash: this.fileUrl,
      pathnamehash: '',
      contextid: this.formdata.contextId,
      component: 'user',
      filearea: 'draft',
      itemid: 0,
      filepath: '/',
      filename:
        this.uploadFileData == undefined ? '' : this.uploadFileData.name,
      userid: 2,
      filesize:
        this.uploadFileData == undefined ? '' : this.uploadFileData.size,
      mimetype:
        this.uploadFileData == undefined ? '' : this.uploadFileData.type,
      status: '',
      source: '',
      author: 'admin User',
      license: 'allrightsreserved',
      timecreated: '',
      timemodified: '',
      sortorder: '',
      referencefileid: '',
      contextlevel: 'course',
      instanceid: course.id
    };

    console.log('Start Date', this.formdata.startdate);
    console.log('End date', this.formdata.enddate);
    const formData: any = new FormData();
    // const files: Array<File> = this.fileReadedArr;
    console.log('Readed File Data' + this.fileReaded);
    // for(let i =0; i < files.length; i++){
    formData.append('file', this.fileReaded, this.fileReaded.name);
    // }

    var courseData = {
      course: course,
      file: fileData,
      fileRead: formData
    };

    console.log('Form Data', this.formdata);
    console.log('Course Data', courseData);
    //  this.createUpdateCourse(courseData,saveCourseID);
  }

  /*createUpdateCourse(courseData,saveCourseID){
    this.ModuleService.createUpdateCourse(courseData)
    .then(rescompData => {
      // this.loader =false;
      this.result = rescompData;
      console.log('Course Result',this.result)
      if(this.result.type == true){
        // this.router.navigate(['/pages/ccategory/ccategorydata']);
        this.tab1 = false;
        this.tab2 = true;
      //  this.getTopics(saveCourseID);
      }
      // this.getTopics(course.id);
      this.cdf.detectChanges();
    },
    resUserError => {
      // this.loader =false;
      this.errorMsg = resUserError
    });
  }*/

  onExpectDateChange(event: IMyDateModel): void {
    if (event.jsdate != null) {
      let d: Date = new Date(event.jsdate.getTime());

      // set previous of selected date
      d.setDate(d.getDate() - 1);
      //this.myDate=new Date();
      //let finalDate=this.myDate;

      // Get new copy of options in order the date picker detect change
      let copy: INgxMyDpOptions = this.getCopyOfEndDateOptions();
      copy.disableSince = {
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate()
      };
      //this.myDatePickerOptions1.disableSince=copy.disableSince;
      // this.startDate = copy;
      // copy.disableUntil={
      //   year:d.getFullYear()+50,
      //   month:d.getMonth()+(50*12);
      //     day:d.getDay()
      // };
      this.selectedStartDate = d;
      this.myDatePickerOptions1 = {
        dateFormat: 'yyyy-mm-dd',
        disableUntil: {
          year: d.getFullYear(),
          month: d.getMonth() + 1,
          day: d.getDate() + 1
        }
      };
      this.endDate = copy;
    }
  }

  onStartDateChanged(event: IMyDateModel): void {
    // date selected
    if (event.jsdate != null) {
      let d: Date = new Date(event.jsdate.getTime());

      // set previous of selected date
      d.setDate(d.getDate() - 1);
      //this.myDate=new Date();
      //let finalDate=this.myDate;

      // Get new copy of options in order the date picker detect change
      let copy: INgxMyDpOptions = this.getCopyOfEndDateOptions();
      copy.disableSince = {
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate()
      };
      //this.myDatePickerOptions1.disableSince=copy.disableSince;
      // this.startDate = copy;
      // copy.disableUntil={
      //   year:d.getFullYear()+50,
      //   month:d.getMonth()+(50*12);
      //     day:d.getDay()
      // };
      this.selectedStartDate = d;
      this.myDatePickerOptions1 = {
        dateFormat: 'yyyy-mm-dd',
        disableUntil: {
          year: d.getFullYear(),
          month: d.getMonth() + 1,
          day: d.getDate() + 1
        }
      };
      this.endDate = copy;
      this.startdateSelected = true;
    }
  }

  onEndDateChanged(event: IMyDateModel) {
    if (event.jsdate != null) {
      let d: Date = new Date(event.jsdate.getTime());

      // set previous of selected date
      d.setDate(d.getDate() + 1);

      // Get new copy of options in order the date picker detect change
      let copy: INgxMyDpOptions = this.getCopyOfStartDateOptions();
      copy.disableSince = {
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate()
      };
      this.startDate = copy;
      // end date changed...
    } else {
      let copy: INgxMyDpOptions = this.getCopyOfStartDateOptions();
      copy.disableSince = { year: 0, month: 0, day: 0 };
      this.startDate = copy;
    }
  }

  getCopyOfEndDateOptions(): INgxMyDpOptions {
    return JSON.parse(JSON.stringify(this.endDate));
  }

  getCopyOfStartDateOptions(): INgxMyDpOptions {
    return JSON.parse(JSON.stringify(this.startDate));
  }

  initRules() {
    return this._fb.group({
      FilterOpt: [''],
      Value1: [''],
      Value2: ['']
    });
  }

  addFilter() {
    const control = <FormArray>this.myForm.controls['parameters'];
    control.push(this.initRules());
    console.log(this.myForm.controls['parameters']);
    this.controlFlag = true;
    this.controlList.push(0);
    this.strArrayType.push([]);
  }

  removeFilter(i: number) {
    const control = <FormArray>this.myForm.controls['parameters'];
    control.removeAt(i);
    // this.enableSelect(this.strArrayType[i]);
    this.selectedFilterOption.splice(i, 1);
    this.disableSelect();
    this.controlFlag = true;
    this.controlList.splice(i, 1);
    this.strArrayType.splice(i, 1);
  }

  clearFilter() {
    this.myForm.reset({
      FilterOpt: [''],
      Value1: [''],
      Value2: ['']
    });
  }

  clearResults() {
    this.users = [];
    // this.rowData=[];
  }

  save(model) {
    console.log(model);
  }

  sort(key) {
    this.key = key;
    this.reverse = !this.reverse;
  }

  disableSelect() {
    this.strArrayPar.forEach((data, key) => {
      if (this.selectedFilterOption.indexOf(data.pDBName) >= 0) {
        this.strArrayPar[key].pSelected = 'true';
      } else {
        this.strArrayPar[key].pSelected = 'false';
      }
    });
    console.log('Selected Disabled', this.strArrayPar);
  }

  callType(id: any, index: any) {
    if (this.strArrayType[index]) {
      this.strArrayType[index] = [];
    }

    this.ValueId = parseInt((id.srcElement || id.target).value);
    //this.disableSelect();

    this.controlList[index] = this.ValueId;
    for (let i = 0; i < this.strArrayTypePar.length; i++) {
      if (this.strArrayTypePar[i].pId == this.ValueId) {
        this.strArrayType[index].push(this.strArrayTypePar[i]);
        if (this.selectedFilterOption.length > 0) {
          this.selectedFilterOption[index] = this.strArrayTypePar[i].pDBName;
        } else {
          this.selectedFilterOption.push(this.strArrayTypePar[i].pDBName);
        }
      }
    }
    this.disableSelect();
  }

  format(date) {
    var DT = new Date(date);
    var month = DT.getMonth() + 1;
    var day = DT.getDate();
    var year = DT.getFullYear();
    return { date: { year: year, month: month, day: day } };
  }

  clear() {
    this.search2 = {};
  }

  formattedString() {
    var para = this.formattedPara;
    var string = '';
    if (para.length > 0) {
      for (var i = 0; i < para.length; i++) {
        var parameter = para[i];
        if (string != '') {
          string += ',';
        }
        if (parameter.pDBName != '' && parameter.Value1 != '') {
          string += parameter.pDBName + '|' + parameter.Value1;
        }
        if (parameter.Value2 != '') {
          string += '|' + parameter.Value2;
        }
      }
    }
    console.log(string);
  }

  clearFilterData() {
    this.filterData = {
      userProfFields: null,
      gradeGreatEqual: null,
      gradeLess: null,
      compTrack: null,
      dateUntil: null,
      dateFrom: null,
      remarks: null
    };
  }

  submitForm() {
    this.loader = true;
    console.log(this.myForm.value);
    let parameters: any = this.myForm.value.parameters;

    for (let d = 0; d < parameters.length; d++) {
      if (parameters[d].Value1 != null) {
        if (parameters[d].Value1.epoc != undefined) {
          parameters[d].Value1 = parameters[d].Value1.epoc;
        }
      }
      if (parameters[d].Value1 != null) {
        if (parameters[d].Value2.epoc != undefined) {
          parameters[d].Value2 = parameters[d].Value2.epoc;
        }
      }
    }
    console.log('Filter Date', parameters);

    this.formattedPara = parameters;

    for (let i = 0; i < this.formattedPara.length; i++) {
      for (let j = 0; j < this.strArrayPar.length; j++) {
        if (
          parseInt(this.formattedPara[i].FilterOpt) === this.strArrayPar[j].pId
        ) {
          // parameters.push(this.strArrayPar[i].pDBName);
          this.formattedPara[i].pDBName = this.strArrayPar[j].pDBName;
          this.formattedPara[i].pIsMultiple = this.strArrayPar[j].pIsMultiple;
        }
      }
    }
    console.log('Filter Data', this.formattedPara);

    for (let j = 0; j < this.formattedPara.length; j++) {
      if (this.formattedPara[j].pIsMultiple == 'false') {
        this.filterData[this.formattedPara[j].pDBName] = this.formattedPara[
          j
        ].Value1;
      } else {
        this.filterData[this.formattedPara[j].pDBName].to = this.formattedPara[
          j
        ].Value1;
        this.filterData[
          this.formattedPara[j].pDBName
        ].from = this.formattedPara[j].Value2;
      }
    }
    console.log('Filter Final Data', this.filterData);
  }

  submit() {
    this.router.navigate(['/pages/plan/courses/content']);
  }

  back() {
    this.router.navigate(['/pages/plan/courses/content']);
  }

  readUrl(event: any) {
    var validExts = new Array('.xlsx', '.xls');
    var fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
      //this.toastr.onClickToast()
      //.subscribe( toast => {
      // this.router.navigate(['/pages/users/induction']);
      ///});
      //this.toastr.error('valid files are of ' + validExts.toString() + ' types.', 'Invalid file selected!', {dismiss: 'click',toastLife: 5000});
      //this.cancel();
    } else {
      // return true;
      if (event.target.files && event.target.files[0]) {
        this.fileName = event.target.files[0].name;
        //read file from input

        this.fileReaded = event.target.files[0];

        if (
          this.fileReaded != '' ||
          this.fileReaded != null ||
          this.fileReaded != undefined
        ) {
          this.enableUpload = true;
        }

        let file = event.target.files[0];
        // this.xlsxToJsonService.processFileToJson({}, file).subscribe(data => {

        //   this.resultSheets = Object.getOwnPropertyNames(data['sheets']);
        //   console.log('File Property Names ',this.resultSheets);
        //   let sheetName = this.resultSheets[0];
        //   this.result = data['sheets'][sheetName];

        //   if(this.result.length > 0){
        //     this.uploadedData = this.result;
        //   }
        // })
        var reader = new FileReader();
        reader.onload = (event: ProgressEvent) => {
          this.fileUrl = (<FileReader>event.target).result;
        };
        reader.readAsDataURL(event.target.files[0]);
      }
    }
  }

  public editableChangeCallbackRes(
    newValue: string,
    oldValue: string,
    elementRef: ElementRef
  ) {
    //  handle new value
    this.newValueRes = newValue;
    this.oldValueRes = oldValue;
    this.elementRefRes = elementRef;
    console.log('oldValueRes', this.oldValueRes);
    console.log('newValueRes', this.newValueRes);
    console.log('elementRefRes', this.elementRefRes);
  }

  enableOption(optId) {
    if (optId == 1) {
      // this.showTags = true;
      this.showTags = !this.showTags;
      this.showRestrictTop = !this.showTagsTop;
    }
    if (optId == 2) {
      // this.showRestrict = true;
      this.showRestrict = !this.showRestrict;
      this.showTagsTop = !this.showRestrictTop;
    }
  }

  enableOptionTop(optId) {
    if (optId == 1) {
      // this.showTagsTop = true;
      this.showTagsTop = !this.showTagsTop;
      this.showRestrictTop = !this.showTagsTop;
    }
    if (optId == 2) {
      // this.showRestrictTop = true;
      this.showRestrictTop = !this.showRestrictTop;
      this.showTagsTop = !this.showRestrictTop;
    }
  }

  postfileTab(event) {
    // this.tab1 = !this.tab1;

    if (event == 0) {
      this.tab1 = true;
      this.tab2 = false;
      this.tab3 = false;
      this.tab4 = false;
      this.tab5 = false;
    } else if (event == 1) {
      this.tab1 = false;
      this.tab2 = true;
      this.tab3 = false;
      this.tab4 = false;
      this.tab5 = false;
    } else if (event == 2) {
      this.tab1 = false;
      this.tab2 = false;
      this.tab3 = true;
      this.tab4 = false;
      this.tab5 = false;
    } else if (event == 3) {
      this.tab1 = false;
      this.tab2 = false;
      this.tab3 = false;
      this.tab4 = true;
      this.tab5 = false;
    } else if (event == 4) {
      this.tab1 = false;
      this.tab2 = false;
      this.tab3 = false;
      this.tab4 = false;
      this.tab5 = true;
    }
  }

  cancel() {
    // this.fileUpload.nativeElement.files = [];
    console.log(this.fileUpload.nativeElement.files);
    this.fileUpload.nativeElement.value = '';
    console.log(this.fileUpload.nativeElement.files);
    this.fileName = 'You can drag and drop files here to add them.';
    this.enableUpload = false;
  }

  getLength() {
    for (let i = 0; i < this.resourse.length; i++) {
      // for(let j=0; j<this.topic.length; j++){
      for (let j = 0; j < this.topicResult.length; j++) {
        // if(this.resourse[i].tid == this.topic[j].tid){
        if (this.resourse[i].tid == this.topicResult[j].id) {
          this.resOfTopic.push(this.resourse[i]);
        }
      }
    }

    if (this.resOfTopic.length > 1) {
      this.removeRes = true;
    } else {
      this.removeRes = false;
    }

    if (this.topic.length > 1) {
      this.removeTop = true;
    } else {
      this.removeTop = false;
    }

    this.resOfTopic = [];
  }

  // topicAll:any = [];
  addtopic() {
    // this.activeButtonTop();
    var topicLength = this.topicResult.length;
    var tid = topicLength;
    var tname = 'Module ' + topicLength;

    this.NewTopicData = {
      id: 0,
      course: this.cData.id,
      section: tid,
      name: tname,
      summary: '',
      summaryformat: '',
      sequence: '',
      visible: '',
      availability: ''
    };
    //  this.addTopics(this.NewTopicData);
  }

  /* addTopics(courseData){
    // var courseIdData :any = {
    //   id : courseId
    // }
    this.ModuleService.addEditTopics(courseData)
    .then(rescompData => {
      // this.loader =false;
      this.addTopicResult = rescompData['data'][0];
      // this.topic = rescompData.data[0];
      console.log('Add Topics Result',this.addTopicResult);
      if(this.NewTopicData.id == 0){
        this.getTopics(this.cData.id);
      }
      this.cdf.detectChanges();
    },
    resUserError => {
      // this.loader =false;
      this.errorMsg = resUserError
    });
  }*/

  addResource(topic) {
    var topicId = topic.id;
    for (let i = 0; i < this.resourse.length; i++) {
      if (this.resourse[i].tid == topicId) {
        this.topicRes.push(this.resourse[i]);
      }
    }
    var topicResLength1: any = this.resourse.length + 1;

    // var topicResLength1 = this.topicRes.length + 1;
    var rid = topicResLength1;
    var rname = 'Activity ' + topicResLength1;
    var data = {
      tid: topicId,
      rid: rid,
      rtype: 1,
      rname: rname,
      rsumm: '',
      rtags: ''
    };

    this.resourse.push(data);
    this.topicRes = [];
    // this.activeButtonRes();

    for (let i = 0; i < this.resourse.length; i++) {
      if (this.resourse[i].tid == topicId && this.resourse[i].rid == rid) {
        this.formdataRes = {
          tid: this.resourse[i].tid,
          rid: this.resourse[i].rid,
          rType: this.resourse[i].rtype,
          rName: this.resourse[i].rname,
          rSummary: this.resourse[i].rsumm,
          rTags: this.resourse[i].rtags
        };
      }
    }

    var NewResData = data;

    if (NewResData != this.tempNewResData) {
      if (this.isActive == true && this.isActiveTop == true) {
        this.isActiveTop = false;
        this.isActiveRes = true;
        this.isActive = true;
      } else if (this.isActiveRes == false) {
        // this.isActive = !this.isActive;
        this.isActive = true;
        this.isActiveRes = true;
      }
      // else{
      //   this.isActive = !this.isActive;
      //   this.isActiveRes  = false;
      // }
    } else {
      NewResData = null;
      // this.isActive = !this.isActive;
      this.isActive = false;
      this.isActiveRes = false;
    }
    this.tempNewResData = NewResData;
  }

  removeResource(resourceData) {
    var resData = [];
    var topicId = resourceData.tid;
    var resourceId = resourceData.rid;
    for (let i = 0; i < this.resourse.length; i++) {
      if (
        this.resourse[i].tid == topicId &&
        this.resourse[i].rid == resourceId
      ) {
        // resData.push(this.resourse[i]);
        // this.resourse.splice(this.resourse.indexOf(resourceData), 1);
        if (resourceId == this.formdataRes.rid) {
          this.resourse.splice(this.resourse.indexOf(resourceData), 1);
          this.closeFormRes();
        } else {
          this.resourse.splice(this.resourse.indexOf(resourceData), 1);
        }
      }
    }
    // this.resourse.splice(resData.indexOf(resourceData), 1);
  }

  removeTopic(topicData) {
    var topResData = [];
    var topicId = topicData.id;
    for (let i = 0; i < this.topic.length; i++) {
      if (this.topic[i].id == topicId) {
        if (topicId == this.formdataTopic.tid) {
          this.topic.splice(this.topic.indexOf(topicData), 1);
          this.closeFormTop();
        } else {
          this.topic.splice(this.topic.indexOf(topicData), 1);
        }
      }
    }
    for (let i = 0; i < this.resourse.length; i++) {
      if (this.resourse[i].tid == topicId) {
        if (topicId == this.formdataRes.tid) {
          this.resourse.splice(this.resourse.indexOf(this.resourse[i]), 1);
          this.closeFormRes();
        } else {
          this.resourse.splice(this.resourse.indexOf(this.resourse[i]), 1);
        }
      }
    }
  }

  activeButtonRes(ResourceData) {
    var ResData = ResourceData;
    this.formdataRes = {
      tid: ResData.tid,
      rid: ResData.rid,
      rType: ResData.rtype,
      rName: ResData.rname,
      rSummary: ResData.rsumm,
      rTags: ResData.rtags
    };

    if (ResData != this.tempResData) {
      if (this.isActive == true && this.isActiveTop == true) {
        this.isActiveTop = false;
        this.tempTopData = null;
        this.isActiveRes = true;
        this.isActive = true;
      } else if (this.isActiveRes == false) {
        // this.isActive = !this.isActive;
        this.isActive = true;
        this.isActiveRes = true;
      }
      // else{
      //   this.isActive = !this.isActive;
      //   this.isActiveRes  = false;
      // }
    } else {
      ResData = null;
      // this.isActive = !this.isActive;
      this.isActive = false;
      this.isActiveRes = false;
    }
    this.tempResData = ResData;
  }

  updateTopName(formdataTopic) {
    console.log('formdataTopic', formdataTopic);
    for (let i = 0; i < this.topic.length; i++) {
      if (formdataTopic.tid == this.topic[i].tid) {
        this.topic[i].tname = formdataTopic.tName;
      }
    }
  }

  updateResName(formdataRes) {
    // console.log('formdataRes',formdataRes);
    for (let i = 0; i < this.resourse.length; i++) {
      if (
        formdataRes.rid == this.resourse[i].rid &&
        formdataRes.tid == this.resourse[i].tid
      ) {
        this.resourse[i].rname = formdataRes.rName;
      }
    }
  }

  closeFormTop() {
    this.isActiveTop = false;
    this.isActive = false;
    this.tempTopData = null;
  }

  closeFormRes() {
    this.isActiveRes = false;
    this.isActive = false;
    this.tempResData = null;
  }

  public editableChangeCallbackTop(
    newValue: string,
    oldValue: string,
    elementRef: ElementRef
  ) {
    //  handle new value
    this.newValueTop = newValue;
    this.oldValueTop = oldValue;
    this.elementRefTop = elementRef;
    console.log('oldValueTop', this.oldValueTop);
    console.log('newValueTop', this.newValueTop);
    console.log('elementRefTop', this.elementRefTop);
  }

  onCheckBoxClick(event, courseReviewCheck) {
    if (event == false) {
      this.reviewCheck = {};
    }
    // console.log('$event',$event);
    console.log('courseReviewCheck', courseReviewCheck);
  }

  getEnrolledData(cb) {
    const req = new XMLHttpRequest();
    req.open('GET', `assets/data/100k.json`);

    req.onload = () => {
      cb(JSON.parse(req.response));
    };

    req.send();
  }

  enrolUser() {
    this.showEnrolpage = !this.showEnrolpage;
  }

  getTimeLoaded(index: number) {
    if (!this.tabLoadTimes[index]) {
      this.tabLoadTimes[index] = new Date();
    }

    return this.tabLoadTimes[index];
  }

  activeActTypeId: any;
  activeTypeIndex: any;
  activeType: any;
  activeTypeId: any = 1;
  activeTypeName: any;

  assessmentTypeList: any = [
    {
      id: 1,
      name: 'QUIZ',
      icon: 'ion ion-ios-list'
    },
    {
      id: 2,
      name: 'FEEDBACK',
      icon: 'ion ion-ios-paper'
    },
    {
      id: 3,
      name: 'SURVEY',
      icon: 'ion ion-ios-paper'
    },
    {
      id: 4,
      name: 'POLL',
      icon: 'ion ion-ios-paper'
    }
  ];

  activityType: any = {
    content: true,
    assesment: false,
    management: false,
    communication: false,
    assignment: false,
    evaluation: false
  };

  activeItemId: any;
  activeItemName: any;
  activeItem: any;
  activeIndex: any;

  setActiveItem(currentIndex, currentItem) {
    this.activeIndex = currentIndex;
    this.activeItem = currentItem;
    this.activeItemId = currentItem.id;
    this.activeItemName = currentItem.name;
  }

  setActiveType(currentIndex, currentItem) {
    this.activeTypeIndex = currentIndex;
    this.activeType = currentItem;
    this.activeTypeId = currentItem.id;
    this.activeTypeName = currentItem.name;
  }

  contentCat = [
    {
      catId: 1,
      catName: 'Category 1',
      contentId: 1,
      content: [
        {
          catId: 1,
          conId: 1,
          conName: 'Content 1',
          conIcon: 'assets/images/con1.png',
          conDescri:
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book'
        },
        {
          catId: 1,
          conId: 2,
          conName: 'Content 2',
          conIcon: 'assets/images/con2.png',
          conDescri:
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book'
        },
        {
          catId: 1,
          conId: 3,
          conName: 'Content 3',
          conIcon: 'assets/images/con3.png',
          conDescri:
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book'
        }
      ],
      // selected : false
      selected: true
    },
    {
      catId: 2,
      catName: 'Category 2',
      contentId: 2,
      content: [
        {
          catId: 2,
          conId: 1,
          conName: 'Content 1',
          conIcon: 'assets/images/con1.png',
          conDescri:
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book'
        },
        {
          catId: 2,
          conId: 2,
          conName: 'Content 2',
          conIcon: 'assets/images/con2.png',
          conDescri:
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book'
        },
        {
          catId: 2,
          conId: 3,
          conName: 'Content 3',
          conIcon: 'assets/images/con3.png',
          conDescri:
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book'
        }
      ],
      selected: false
    },
    {
      catId: 3,
      catName: 'Category 3',
      contentId: 3,
      content: [
        {
          catId: 3,
          conId: 1,
          conName: 'Content 1',
          conIcon: 'assets/images/con1.png',
          conDescri:
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book'
        },
        {
          catId: 3,
          conId: 2,
          conName: 'Content 2',
          conIcon: 'assets/images/con2.png',
          conDescri:
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book'
        },
        {
          catId: 3,
          conId: 3,
          conName: 'Content 3',
          conIcon: 'assets/images/con3.png',
          conDescri:
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book'
        }
      ],
      selected: false
    }
  ];

  // selectedContent:any = {
  //   conId : '',
  //   catId : ''
  // }

  condata: any;

  contentSelected: any;

  contentShow: boolean = false;

  selectedContent: any = {
    catId: '',
    conId: '',
    conIcon: '',
    conName: '',
    conDescri: ''
  };

  browseContent() {
    this.contentShow = true;
  }

  closeContentModel() {
    this.contentShow = false;
  }

  saveSelectedContent() {
    this.selectedContent = {
      catId: this.condata.catId,
      conId: this.condata.conId,
      conIcon: this.condata.conIcon,
      conName: this.condata.conName,
      conDescri: this.condata.conDescri
    };
    this.contentShow = false;
    this.contentSelected = true;

    for (let i = 0; i < this.contentCat.length; i++) {
      var category = this.contentCat[i];
      if (category.catId == this.selectedContent.catId) {
        category.selected = true;
      } else {
        category.selected = false;
      }
    }
  }

  onCheckBoxselecClick(item) {
    this.condata = {};

    this.condata = item;
  }

  removeContent() {
    this.selectedContent = {};
    this.activeContentId = '';

    this.contentSelected = false;

    for (let i = 0; i < this.contentCat.length; i++) {
      var category = this.contentCat[i];
      if (i == 0) {
        category.selected = true;
      } else {
        category.selected = false;
      }
    }
    this.showContentSelected = false;
    this.showContentSelectedData.name = '';
    this.showContentSelectedData.refImage = '';
  }

  activeContentIndex: any;
  activeContent: any;
  activeContentId: any;
  activeContentName: any;
  activeContentCatId: any;

  setActiveContent(currentIndex, currentContent) {
    this.activeContentIndex = currentIndex;
    this.activeContent = currentContent;
    this.activeContentId = currentContent.conId;
    this.activeContentName = currentContent.conName;
    this.activeContentCatId = currentContent.catId;
  }

  contentCatSelected(event, category) {
    // console.log('Seleted category ',event,category);
    for (let i = 0; i < this.contentCat.length; i++) {
      var cat = this.contentCat[i];
      if (cat.catId == category.catId) {
        cat.selected = true;
      } else {
        cat.selected = false;
      }
    }
  }

  returnActiveId(id) {
    return '#Activity_' + id;
  }
  returnModuleId(id) {
    return 'Module_' + id;
  }

  @ViewChild('activityForm') activityForm: ElementRef;
  selectedActInd: any = 0;

  addRemoveClassElement(element: any, classname) {
    if (element) {
      var elementContent = element.nativeElement;
      if (elementContent.classList.length > 0) {
        var index = elementContent.className.indexOf(classname);
        if (index > -1) {
          //exist
          elementContent.classList.remove(classname);
        } else {
          elementContent.classList.add(classname);
        }
      }
    }
  }

  /*Trainer Multi select Dropdown*/
  search: any = [];
  demoUsers: any = [];
  onCreatorSearch(evt: any) {
    console.log(evt.target.value);
    const val = evt.target.value;
    this.usersList = [];
    const temp = this.demoUsers.filter(function (d) {
      return (
        String(d.trainerName)
          .toLowerCase()
          .indexOf(val) !== -1 ||
        d.agencyName.toLowerCase().indexOf(val) !== -1 ||
        !val
      );
    });

    // update the rows
    this.usersList = temp;
    var nomi = {
      id: 0,
      trainerName: 'Request for Nomination',
      standardId: '',
      agencyName: ''
    };
    this.usersList.push(nomi);
    console.log(this.usersList, "userList");
  }

  onCreatorSelect(item: any, k) {
    this.formDataModule.trainers.forEach(res => {
      if (res.train == '') {
        this.trainer = false;
      }
      else {
        this.trainer = true;
      }

    })

    console.log(item);
    console.log(this.selectedCreator);
    for (let j = 0; j < this.formDataModule.trainers.length; j++) {
      if (k == j) {
        this.formDataModule.trainers[j].trainerId = item.id;
      }
    }
    // for(let i=0;i<this.demoUsers.length;i++)
    // {
    //   if(this.demoUsers[i].id == item.id)
    //   {
    //     this.demoUsers.splice(1,i)
    //   }
    // }
    // this.search ='';
  }
  OnCreatorDeSelect(item: any, k) {
    this.trainer = false;
    console.log(item);
    for (let j = 0; j < this.formDataModule.trainers.length; j++) {
      if (k == j) {
        this.formDataModule.trainers[j].trainerId = '';
        this.formDataModule.trainers[j].train = [];
      }
    }
    //  for(let i=0;i<this.tempUsers.length;i++)
    // {
    //   if(this.demoUsers[i].id == item.id)
    //   {
    //     this.demoUsers.push(item)
    //   }
    // }
    // this.search ='';
    console.log(this.selectedCreator);
  }
  nomination(event) {
    // this.formDataModule.trainers.forEach(res=>{
    //   if(res.train == []){
    //     this.trainer = false;
    //   }
    //   // else{
    //   //   this.trainer = !this.trainer
    //   // }

    //   })
    console.log(event, "nomination")
    if (event.target.checked == true) {
      this.trainer = true;
    } else {
      this.formDataModule.trainers.forEach(res => {
        console.log(res, "res")
        if (res.train == '') {
          this.trainer = false;
        }
      })
      //this.trainer = false
    }

  }
  trainerDataChange(stdId) {
    this.demoUsers = [];
    this.selectedCreator = [];
    for (let i = 0; i < this.tempUsers.length; i++) {
      if (Number(stdId) == this.tempUsers[i].standardId) {
        this.demoUsers.push(this.tempUsers[i]);
      }
    }

    // this.search ='';
  }

  Addtrainer(visible) {
    this.trainerVisible = false
    this.trainer = false
    var data = {
      standardId: '',
      enrolId: 0,
      trainerId: '',
      train: [],
      visible: '1',
      traineresId: 0,
      isCheck : 0
    };

    this.formDataModule.trainers.push(data);
    this.formDataModule.trainers.forEach(res => {
      if (res.train == '') {
        this.trainer = false;
      }

    })
    console.log(this.formDataModule.trainers, "this.formDataModule.trainers")
  }
  // this.formDataModule.moduleId = 0;
  // Removetrainer(data, type, j, modf, train) {
  //   this.trainer = true
  //   console.log('type', type);
  //   console.log('data', data);
  //   if (data.moduleId == 0) {
  //     for (let i = 0; i < this.formDataModule.trainers.length; i++) {
  //       if (j == i) {
  //         //this.demoUsers.push(tyep.train)
  //         this.formDataModule.trainers.splice(i, 1);
  //       }
  //     }
  //   } else {
  //     const param = {
  //       flag: type.trainerId == '' ? 1 : 2,
  //       cId: data.courseId,
  //       modId: data.moduleId,
  //       trnId: type.trainerId ? type.trainerId : null,
  //     }
  //     this.spinner.show();
  //     // this.ModuleService.removeTrainerEnrolment.subscribe(
  //     //   rescompData => {

  //     //   },
  //     //   resUserError => {

  //     //     this.errorMsg = resUserError;
  //     //   },
  //     // );
  //     // this.ModuleService.removeTrainerEnrolment(param).then(
  //     //   result => {
  //     //     this.spinner.hide();
  //     //     console.log('Unenrolment result', result)
  //     //     if (result['type'] == true) {

  //     for (let i = 0; i < this.formDataModule.trainers.length; i++) {
  //       if (j == i) {
  //         this.formDataModule.trainers.splice(i, 1);
  //       }
  //     }

  //     this.cdf.detectChanges();

  //     // this.toastr.success('Successfully Removed', 'Success', {
  //     //   closeButton: false,
  //     // });
  //     this.spinner.hide()
  //     this.saveModuleDetails(modf, train)

  //     //   } else {
  //     //     this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
  //     //       timeOut: 0,
  //     //       closeButton: true,
  //     //     });
  //     //   }
  //     // },
  //     // resUserError => {
  //     //   this.spinner.hide();
  //     // this.errorMsg = resUserError;
  //     // this.notFound = true;
  //     // this.router.navigate(['**']);
  //     //   }
  //     // );
  //   }
  // }
  Removetrainer(data, type, j, modf, train) {
    this.trainer = true
    console.log('type', type);
    console.log('data', data);
    if (data.moduleId == 0) {
      for (let i = 0; i < this.formDataModule.trainers.length; i++) {
        if (j == i) {
          //this.demoUsers.push(tyep.train)
          this.formDataModule.trainers.splice(i, 1);
        }
      }
    } else {
      const param = {
        flag: type.trainerId == '' ? 1 : 2,
        cId: data.courseId,
        modId: data.moduleId,
        trnId: type.trainerId ? type.trainerId : null,
        enId:type.enrolId
      }
      this.spinner.show();
      // this.ModuleService.removeTrainerEnrolment.subscribe(
      //   rescompData => {

      //   },
      //   resUserError => {

      //     this.errorMsg = resUserError;
      //   },
      // );
      if(type.enrolId != 0){
      this.ModuleService.removeTrainerEnrolment(param).then(
        result => {
          this.spinner.hide();
          console.log('Unenrolment result', result)
          if (result['type'] == true) {

      //original one

      // for (let i = 0; i < this.formDataModule.trainers.length; i++) {
      //   if (j == i) {
      //     this.formDataModule.trainers.splice(i, 1);
      //   }
      // }

      this.cdf.detectChanges();

      // this.toastr.success('Successfully Removed', 'Success', {
      //   closeButton: false,
      // });
      this.spinner.hide()
      // this.saveModuleDetails(modf, train)
      this.getTrainers(data.moduleId)

        } else {
          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true,
          });
        }
      },
      resUserError => {
        this.spinner.hide();
      this.errorMsg = resUserError;
      // this.notFound = true;
      this.router.navigate(['**']);
        }
      );
    }else{
      for (let i = 0; i < this.formDataModule.trainers.length; i++) {
        if (j == i) {
          this.formDataModule.trainers.splice(i, 1);
        }
      }
      this.spinner.hide()
      // this.saveModuleDetails(modf, train)

    }
    }
  }
  /*******************CREDIT POINTS*********************/
  roles: any;
  pointFormat: any;
  notFound: boolean = false;
  getallDrop() {
    var datadrop = {
      tId: this.tenantId
    };

    this.spinner.show();

    this.ModuleService.getallDropdown(datadrop).then(
      rescompData => {
        if(this.spinnerHide == true){
        this.spinner.hide();
        }
        var data1 = rescompData['data'];
        this.roles = data1.roles;
        this.pointFormat = data1.pointFormat;
        if (this.formDataActivity.craditpoints.length > 0) {
          for (let i = 0; i < this.formDataActivity.craditpoints.length; i++) {
            var credit = this.formDataActivity.craditpoints[i];
            this.roleTypeSelected(i, credit);
          }
        }
        console.log('quiz Drop Result', rescompData);
      },
      resUserError => {
        this.spinner.hide();
        this.errorMsg = resUserError;
        this.notFound = true;
        // this.router.navigate(['**']);
      }
    );
  }
  addPointsList() {
    // let defualtCreditsObjCopy = Object.assign({}, this.defualtCreditsObj);
    let defualtCreditsObj = {
      crid: 0,
      roleId: '',
      pformatid: '',
      bcpoints: '',
      acpoints: ''
    };
    // console.log(defualtCreditsObj);
    // this.credits.push(defualtCreditsObj);
    if (this.formDataActivity.craditpoints.length < 2) {
      this.formDataActivity.craditpoints.push(defualtCreditsObj);
    }
  }

  removePointsList(i: number) {
    // this.credits.removeAt(i);
    // this.credits.splice(i,1);
    this.formDataActivity.craditpoints.splice(i, 1);
    this.roleSelectedList.splice(i, 1);
    this.selectedRole.splice(i, 1);
    // this.addPointsDisableEnable();
    this.disableSelectedRole();
  }

  disableSelectedRole() {
    this.roles.forEach((data, key) => {
      if (this.selectedRole.indexOf(data.name) >= 0) {
        this.roles[key].isSelected = 1;
      } else {
        this.roles[key].isSelected = 0;
      }
    });
    console.log('Selected Disabled', this.roles);
  }

  // disableAddCredit: boolean = false;
  // addPointsDisableEnable() {
  //   if (this.roles.length == this.roleSelectedList.length) {
  //     this.disableAddCredit = true;
  //   } else {
  //     this.disableAddCredit = false;
  //   }
  // }
  // roles=[];
  roleSelectedList = [];
  selectedRole = [];
  disableIfLearner: any = false;
  roleTypeSelected(currentIndex, currentItem) {
    console.log('currentItem ', currentItem);
    console.log('this.pointFormat', this.pointFormat);
    if (currentItem.roleId == '8') {
      this.formDataActivity.craditpoints[currentIndex].pformatid = 1;
      this.disableIfLearner = true;
    } else {
      this.disableIfLearner = false;
    }

    for (let i = 0; i < this.roles.length; i++) {
      var role = this.roles[i];
      if (role.id == Number(currentItem.roleId)) {
        this.roleSelectedList[currentIndex] = role;
        if (this.selectedRole.length > 0) {
          this.selectedRole[currentIndex] = role.name;
        } else {
          this.selectedRole.push(role.name);
        }
      }
    }

    // this.addPointsDisableEnable();
    this.disableSelectedRole();
  }

  marksTypeSelected(currentEvent, currentIndex, currentItem) {
    console.log('currentItem ', currentItem);
  }

  // disable course activities
  visibiltyRes: any;
  disableActivity(activityData, i, j) {
    this.spinner.show();
    var visibilityData = {
      activityId: activityData.activityId,
      visible: activityData.activityVisible == 0 ? 1 : 0,
    }

    this.ModuleService.enableDisableActivity(visibilityData)
      .then(rescompData => {
        this.spinner.hide();
        this.visibiltyRes = rescompData;
        console.log('Activity Visibility Result', this.visibiltyRes);
        if (this.visibiltyRes.type == false) {
          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });

        } else {
          this.toastr.success(this.visibiltyRes.data, 'Success', {
            closeButton: false
          });

          this.courseModules[i].activity[j].activityVisible = visibilityData.visible;
          this.cdf.detectChanges();
          var param = {
            courseId: this.courseModules[i].courseId,
            moduleId: activityData.moduleId,
            activityId: activityData.activityId,
            activityName: activityData.activityName,
            userId: activityData.usermodified,
            actionType: visibilityData.visible == 1 ? ' ENABLE ACTIVITY' : 'DISABLE ACTIVITY',
            activityData: JSON.stringify(visibilityData),
            otherData: this.visibiltyRes.data,
            role: this.currentRoleId,
            origin: 'ADMIN',
            url: window.location.href,
            ip: '',
            crud: 'U',
            tenantId: this.tenantId,
            logType: this.activityLogId,
            changeString: visibilityData.visible == 1 ? this.logMsg.enableActivity : this.logMsg.disableActivity,

          }
          this.LogServices.saveLogged(param);

        }
      },
        resUserError => {
          this.spinner.hide();
          this.errorMsg = resUserError;
        });
  };

  // Set Date
  // setMinDate(startDate, date) {
  //   if (String(Date.parse(startDate)) === 'NaN' || String(Date.parse(date)) === 'NaN') {
  //     return new Date();
  //   } else if (startDate > new Date() || date > new Date()) {
  //     return new Date();
  //   } else {
  //     return startDate;
  //   }
  // }
  // disable course Module
  disableModule(modData, i) {
    this.spinner.show();
    var visibilityData = {
      moduleId: modData.moduleId,
      visible: modData.visible == 0 ? 1 : 0,
    }

    this.ModuleService.moduleEnableDisable(visibilityData)
      .then(rescompData => {
        this.spinner.hide();
        this.visibiltyRes = rescompData;
        console.log('Module Visibility Result', this.visibiltyRes);
        if (this.visibiltyRes.type == false) {
          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });

        } else {
          this.toastr.success(this.visibiltyRes.data, 'Success', {
            closeButton: false
          });
          this.courseModules[i].visible = visibilityData.visible;
          for (let j = 0; j < this.courseModules[i].activity.length; j++) {
            console.log('j', j);
            this.courseModules[i].activity[j].activityVisible = visibilityData.visible;
          };
          this.cdf.detectChanges();

        }
      },
        resUserError => {
          this.spinner.hide();
          this.errorMsg = resUserError;
        });
  };
  // hear we modifed data for log
  createLogJson(object, base) {
    return transform(object, (result, value, key) => {
      if (!isEqual(value, base[key])) {
        result[key] = isObject(value) && isObject(base[key]) ? this.createLogJson(value, base[key]) : value;
      }
    });

  }
  // here we made the statment which we want to store in database
  changeString: String = '';
  createStatmentsforActivityLog(modifyData) {
    this.changeString = '';
    var oKeys = Object.keys(modifyData);
    for (let i = 0; i < oKeys.length; i++) {
      var lableName = ACTIVITYLABELS[oKeys[i]];
      if (this.changeString == '') {
        this.changeString = lableName + this.logMsg.editActivity + '\n';
      } else {
        this.changeString = this.changeString + lableName + this.logMsg.editActivity + '\n';
      }

      // console.log('this.changeString', this.changeString);
    }

  }

  //tags data
  onTagsSelect(item: any) {
    console.log(item);
    console.log(this.selectedTags);
  }
  OnTagDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedTags);
  }
  onTagSearch(evt: any) {
    console.log(evt.target.value);
    const val = evt.target.value;
    this.tagList = [];
    const temp = this.tempTags.filter(function (d) {
      return (
        String(d.name)
          .toLowerCase()
          .indexOf(val) !== -1 ||
        !val
      );
    });
    // update the rows
    this.tagList = temp;
    console.log('filtered Tag LIst', this.tagList);
  }

  formattedActTags: any;
  makeTagDataReady(tagsData) {
    this.formDataActivity.tagIds = ''
    tagsData.forEach((tag) => {
      if (this.formDataActivity.tagIds == '') {
        this.formDataActivity.tagIds = tag.id;
      } else {
        this.formDataActivity.tagIds = this.formDataActivity.tagIds + '|' + tag.id;
      }
      console.log('this.formDataActivity.tagIds', this.formDataActivity.tagIds);
    });
    // var tagsData = this.formDataActivity.tags;
    // if (tagsData.length > 0) {
    //   var tagsString = "";
    //   for (let i = 0; i < tagsData.length; i++) {
    //     var tag = tagsData[i];
    //     if (tagsString != "") {
    //       tagsString += "|";
    //     }
    //     if (tag.value) {
    //       if (String(tag.value) != "" && String(tag.value) != "null") {
    //         tagsString += tag.value;
    //       }
    //     } else {
    //       if (String(tag) != "" && String(tag) != "null") {
    //         tagsString += tag;
    //       }
    //     }
    //   }
    //   this.formattedActTags = tagsString;
    // }
  }

  // Help Code Start Here //

  helpContent: any;
  getHelpContent() {
    return new Promise(resolve => {
      this.http1
        .get('../../../../../../assets/help-content/addEditCourseContent.json')
        .subscribe(
          data => {
            this.helpContent = data;
            console.log('Help Array', this.helpContent);
          },
          err => {
            resolve('err');
          }
        );
    });
    // return this.helpContent;
  }

  // Help Code Ends Here //
  // log messages
  logMsg: any;
  getlogMsg() {
    return new Promise(resolve => {
      this.http1
        .get('../../../../../../assets/log/log.json')
        .subscribe(
          data => {
            this.logMsg = data;
            // console.log('logMsg', this.logMsg);
          },
          err => {
            resolve('err');
          }
        );
    });
  }


  showGenrateButton = false;
  shortUrl = '';
  linkGenerationInProgress = false;
  copytext(data) {
    console.log("Assets url:>", data);
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = data;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.toastr.success('Link Copied', 'Success');
  }

  checkActivityUrl(content) {
    if (content['redirectionLink'] && content['redirectionLink'] !== null) {
      this.shortUrl = content['redirectionLink'];
      this.showGenrateButton = true;
    } else {
      this.showGenrateButton = false;
    }
  }

  generateAcitivtyUrl() {
    this.linkGenerationInProgress = true;
    this.cdf.detectChanges();
    const _generateURL: string = webApi.domain + webApi.url.genearteFirebaseUrl;
    const tenant_Info = JSON.parse(localStorage.getItem('tenant_Info'));
    let portalUrl = '';
    if(tenant_Info){
      let domainURL = window.location.hostname;
      // let domainURL = 'devadmin.edgelearning.co.in';
      if(domainURL === 'localhost'){
        portalUrl = '';
      }else {
        if(domainURL){
          domainURL =  domainURL.substring(domainURL.indexOf('.')+1);
        }
        portalUrl = tenant_Info.tenantCode + '.' + domainURL;
      }
    }
    const params = {
      'instanceId': Number(this.formDataActivity.activityId),
      'areaId': 5,
      'indentifier': 'activity',
      'portalUrl': portalUrl,
      'platform': 3,
    };
    params['link'] = 'pages/dynamic-link-learn/' + '5' + '/' + Number(this.formDataActivity.activityId) + '/' + 'activity';
    this.commonFunctionService.httpPostRequest(_generateURL, params).then((res) => {
      console.log('Response', res);
      if (res['statusCode'] === 200) {
        this.shortUrl = res['data']['shortLink'];
        this.showGenrateButton = true;
        this.cdf.detectChanges();
        this.linkGenerationInProgress = false;
      } else {
        this.toastr.error('Unable to generate link at this time', 'Error', {
          closeButton: false,
        });
        this.linkGenerationInProgress = false;
      }
    }).catch(function (err) {
      // next(new Error('user not found'));
      this.toastr.error('Unable to generate link at this time', 'Error', {
        closeButton: false,
      });
      this.linkGenerationInProgress = false;
    });
  }


  // toggleCheckbox(event) {
  //   if (event.target.id === 'byTrainer') {
  //     this.formDataActivity.byTrainer = true;
  //     this.formDataActivity.byLearner = false;
  //     this.checkedAnyOneCheckBox = true;
  //   } else {
  //     this.formDataActivity.byLearner = true;
  //     this.formDataActivity.byTrainer = false;
  //     this.checkedAnyOneCheckBox = false;
  //   }
  // }

  // setDefaultValues() {
  //   this.user.checkbox1 = true;
  //   this.user.checkbox2 = false;
  // }
  // preview data
  assetrefLink = '';
  files: any;
  isLocalfile: boolean = false;

  convertFileUrl(fileurl) {
    console.log(fileurl, "fileUrl");
    if (!fileurl) {
      return fileurl;
    } else if (fileurl.includes('C:\fakepath')) {
      return fileurl.split('C:\fakepath')[1];
    } else {
      return fileurl;
    }
  }
  removeFile() {
    this.activityFileData = null;
    this.fileName = 'You can drag and drop files here to add them.';
    this.selectFileTitle = 'No file chosen';
    this.formDataActivity.assetRef = '';
    this.assetrefLink = ''
    this.isLocalfile = false;
    // this.nativeElementConfigure();
  }

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }
  // formatChange(event) {
  //   this.fileName = 'You can drag and drop files here to add them.';
  //   this.selectFileTitle = 'No file chosen';
  //   this.formDataActivity.assetRef = '';
  //   this.assetrefLink = ''
  //   this.isLocalfile = false;
  //   console.log('formDataActivity.formatId', this.formDataActivity.formatId);
  //   this.cdf.detectChanges();
  //   // this.nativeElementConfigure();
  // }
  // nativeElementConfigure() {
  //   setTimeout(() => {
  //     if (this.imagefile) {
  //       this.render.listen(this.imagefile.nativeElement, 'change', () => {
  //         this.isLocalfile = true;
  //         var size = 100000000;
  //         var validExts = new Array('image');
  //         this.files = this.imagefile.nativeElement.files;
  //         var fileType = this.files[0].type;
  //         console.log('fileType', fileType);
  //         var fileExt = false;
  //         for (let i = 0; i < validExts.length; i++) {
  //           if (fileType.includes(validExts[i])) {
  //             fileExt = true;
  //             break;
  //           }
  //         }
  //         if (!fileExt) {
  //           this.presentToast('warning', 'Please select a file of type image');
  //           this.removeFile();
  //         } else if (size <= this.files[0].size) {
  //           this.presentToast('warning', 'File size should be less than 100 MB');
  //           this.removeFile();
  //         } else {
  //           try {
  //             console.log('image', this.files[0]);
  //             this.formDataActivity.assetRef = this.files[0].name;
  //             this.assetrefLink = '';
  //             this.fileName = this.files[0].name;
  //             this.activityFileData = this.files[0];
  //             this.selectFileTitle = this.files[0].name;
  //             this.enableUpload = true;
  //             console.log('this.fileName', this.fileName);
  //             console.log('this.activityFileData', this.activityFileData);
  //             console.log('this.islocal', this.isLocalfile)
  //             setTimeout(() => {
  //               this.cdf.detectChanges();
  //               let $source: any = $('#image_here');
  //               $source[0].src = URL.createObjectURL(this.files[0]);
  //               // $source.parent()[0].load();
  //             }, 500);
  //           } catch (e) {
  //             console.log(e);
  //           }
  //         }
  //       }); ``
  //     }

  //     // $(document).on('change','#videofile',function(evt){
  //     if (this.videofile) {
  //       this.render.listen(this.videofile.nativeElement, 'change', () => {
  //         this.isLocalfile = true;
  //         var size = 1000000000;
  //         var validExts = new Array('video');
  //         this.files = this.videofile.nativeElement.files;
  //         var fileType = this.files[0].type;
  //         console.log('fileType', fileType);
  //         var fileExt = false;
  //         for (let i = 0; i < validExts.length; i++) {
  //           if (fileType.includes(validExts[i])) {
  //             fileExt = true;
  //             break;
  //           }
  //         }
  //         if (!fileExt) {
  //           this.presentToast('warning', 'Please select a file of type video');
  //           this.removeFile();
  //         } else if (size <= this.files[0].size) {
  //           this.presentToast('warning', 'File size should be less than 100 MB');
  //           this.removeFile();
  //         } else {
  //           try {
  //             console.log('Video', this.files[0]);
  //             this.formDataActivity.assetRef = this.files[0].name;
  //             this.assetrefLink = '';
  //             this.fileName = this.files[0].name;
  //             this.activityFileData = this.files[0];
  //             this.selectFileTitle = this.files[0].name;
  //             this.enableUpload = true;
  //             console.log('this.fileName', this.fileName);
  //             console.log('this.activityFileData', this.activityFileData);
  //             setTimeout(() => {
  //               this.cdf.detectChanges();
  //               let $source: any = $('#video_here');
  //               $source[0].src = window.URL.createObjectURL(this.files[0]); // URL.createObjectURL(this.files[0]);
  //               // $source.parent()[0].load();
  //             }, 500);
  //           } catch (e) {
  //             console.log(e);
  //           }
  //         }
  //       });
  //     }
  //     ////////////////Code for fetch audio file//////////////

  //     // $(document).on('change','#audiofile',function(evt){
  //     if (this.audiofile) {
  //       this.render.listen(this.audiofile.nativeElement, 'change', () => {
  //         // this.isLocalfile = true;
  //         var size = 1000000000;
  //         this.files = this.audiofile.nativeElement.files;
  //         var validExts = new Array('audio');
  //         var fileType = this.files[0].type;
  //         console.log('fileType', fileType);
  //         var fileExt = false;
  //         for (let i = 0; i < validExts.length; i++) {
  //           if (fileType.includes(validExts[i])) {
  //             fileExt = true;
  //             break;
  //           }
  //         }
  //         if (!fileExt) {
  //           this.presentToast('warning', 'Please select a file type of audio');
  //           this.removeFile();
  //         } else if (size <= this.files[0].size) {
  //           this.presentToast('warning', 'File size should be less than 100 MB');
  //           this.removeFile();
  //         } else {
  //           try {
  //             console.log('audio', this.files[0]);
  //             this.formDataActivity.assetRef = this.files[0].name;
  //             this.assetrefLink = '';
  //             this.fileName = this.files[0].name;
  //             this.activityFileData = this.files[0];
  //             this.selectFileTitle = this.files[0].name;
  //             this.enableUpload = true;
  //             console.log('this.fileName', this.fileName);
  //             console.log('this.activityFileData', this.activityFileData);
  //             setTimeout(() => {
  //               this.cdf.detectChanges();
  //               let $source: any = $('#audio_here');
  //               $source[0].src = URL.createObjectURL(this.files[0]);
  //               // $source.parent()[0].load();
  //             }, 500);
  //           } catch (e) {
  //             console.log(e);
  //           }
  //         }
  //       });
  //     }
  //     ///////////////////audio file End///////////////////////

  //     // $(document).on('change','#iframefile',function(evt){
  //     if (this.iframe) {
  //       this.render.listen(this.iframe.nativeElement, 'change', () => {
  //         this.isLocalfile = true;
  //         var size = 1000000000;
  //         this.files = this.iframe.nativeElement.files;
  //         var validExts = new Array('pdf');
  //         var fileType = this.files[0].type;
  //         console.log('fileType', fileType);
  //         var fileExt = false;
  //         for (let i = 0; i < validExts.length; i++) {
  //           if (fileType.includes(validExts[i])) {
  //             fileExt = true;
  //             break;
  //           }
  //         }
  //         if (!fileExt) {
  //           this.presentToast('warning', 'Please select a file of type pdf');
  //           this.removeFile();
  //         } else if (size <= this.files[0].size) {
  //           this.presentToast('warning', 'File size should be less than 100 MB');
  //           this.removeFile();
  //         } else {
  //           try {
  //             console.log('pdf', this.files[0]);
  //             this.formDataActivity.assetRef = this.files[0].name;
  //             this.assetrefLink = '';
  //             this.fileName = this.files[0].name;
  //             this.activityFileData = this.files[0];
  //             this.selectFileTitle = this.files[0].name;
  //             this.enableUpload = true;
  //             console.log('this.fileName', this.fileName);
  //             console.log('this.activityFileData', this.activityFileData);
  //             setTimeout(() => {
  //               this.cdf.detectChanges();
  //               let $source: any = $('#iframe');
  //               $source[0].src = URL.createObjectURL(this.files[0]);
  //               // $source.parent()[0].load();
  //             }, 500);
  //           } catch (e) {
  //             console.log(e);
  //           }
  //         }
  //       });
  //     }

  //     if (this.ppt) {
  //       this.render.listen(this.ppt.nativeElement, 'change', () => {
  //         this.isLocalfile = true;
  //         var size = 1000000000;
  //         this.files = this.ppt.nativeElement.files;
  //         console.log("this.files", this.files)
  //         var validExts = new Array('application/vnd.ms-powerpoint', 'application/vnd.openxmlformats-officedocument.presentationml.presentation');
  //         var fileType = this.files[0].type;
  //         console.log('fileType', fileType);
  //         var fileExt = false;
  //         //var fileExt = true;
  //         for (let i = 0; i < validExts.length; i++) {
  //           if (fileType.includes(validExts[i])) {
  //             fileExt = true;
  //             break;
  //           }
  //         }
  //         if (!fileExt) {
  //           this.presentToast('warning', 'Please select a file of type ppt');
  //           this.removeFile();
  //         } else if (size <= this.files[0].size) {
  //           this.presentToast('warning', 'File size should be less than 100 MB');
  //           this.removeFile();
  //         } else {
  //           try {
  //             console.log('ppt', this.files[0]);
  //             this.formDataActivity.assetRef = this.files[0].name;
  //             this.assetrefLink = '';
  //             this.fileName = this.files[0].name;
  //             this.activityFileData = this.files[0];
  //             this.selectFileTitle = this.files[0].name;
  //             this.enableUpload = true;
  //             this.cdf.detectChanges();
  //             console.log('this.fileName', this.fileName);
  //             console.log('this.activityFileData', this.activityFileData);
  //             // setTimeout(() => {
  //             //   let $source: any = $('#iframe');
  //             //   $source[0].src = URL.createObjectURL(this.files[0]);
  //             // $source.parent()[0].load();
  //             // }, 200);
  //           } catch (e) {
  //             console.log(e);
  //           }
  //         }
  //       });
  //     }


  //     if (this.word) {
  //       this.render.listen(this.word.nativeElement, 'change', () => {
  //         this.isLocalfile = true;
  //         var size = 1000000000;
  //         this.files = this.word.nativeElement.files;
  //         console.log("this.files", this.files)
  //         var validExts = new Array('application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document');
  //         var fileType = this.files[0].type;
  //         console.log('fileType', fileType);
  //         var fileExt = false;
  //         //var fileExt = true;
  //         for (let i = 0; i < validExts.length; i++) {
  //           if (fileType.includes(validExts[i])) {
  //             fileExt = true;
  //             break;
  //           }
  //         }
  //         if (!fileExt) {
  //           this.presentToast('warning', 'Please select a file of type word');
  //           this.removeFile();
  //         } else if (size <= this.files[0].size) {
  //           this.presentToast('warning', 'File size should be less than 100 MB');
  //           this.removeFile();
  //         } else {
  //           try {
  //             console.log('ppt', this.files[0]);
  //             this.formDataActivity.assetRef = this.files[0].name;
  //             this.assetrefLink = '';
  //             this.fileName = this.files[0].name;
  //             this.activityFileData = this.files[0];
  //             this.selectFileTitle = this.files[0].name;
  //             this.enableUpload = true;
  //             this.cdf.detectChanges();
  //             console.log('this.fileName', this.fileName);
  //             console.log('this.activityFileData', this.activityFileData);
  //             // setTimeout(() => {
  //             //   let $source: any = $('#iframe');
  //             //   $source[0].src = URL.createObjectURL(this.files[0]);
  //             // $source.parent()[0].load();
  //             // }, 200);
  //           } catch (e) {
  //             console.log(e);
  //           }
  //         }
  //       });
  //     }


  //     if (this.excel) {
  //       this.render.listen(this.excel.nativeElement, 'change', () => {
  //         this.isLocalfile = true;
  //         var size = 1000000000;
  //         this.files = this.excel.nativeElement.files;
  //         console.log("this.files", this.files)
  //         var validExts = new Array('application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  //         var fileType = this.files[0].type;
  //         console.log('fileType', fileType);
  //         var fileExt = false;
  //         //var fileExt = true;
  //         for (let i = 0; i < validExts.length; i++) {
  //           if (fileType.includes(validExts[i])) {
  //             fileExt = true;
  //             break;
  //           }
  //         }
  //         if (!fileExt) {
  //           this.presentToast('warning', 'Please select a file of type word');
  //           this.removeFile();
  //         } else if (size <= this.files[0].size) {
  //           this.presentToast('warning', 'File size should be less than 100 MB');
  //           this.removeFile();
  //         } else {
  //           try {
  //             console.log('ppt', this.files[0]);
  //             this.formDataActivity.assetRef = this.files[0].name;
  //             this.assetrefLink = '';
  //             this.fileName = this.files[0].name;
  //             this.activityFileData = this.files[0];
  //             this.selectFileTitle = this.files[0].name;
  //             this.enableUpload = true;
  //             this.cdf.detectChanges();
  //             console.log('this.fileName', this.fileName);
  //             console.log('this.activityFileData', this.activityFileData);
  //             // setTimeout(() => {
  //             //   let $source: any = $('#iframe');
  //             //   $source[0].src = URL.createObjectURL(this.files[0]);
  //             // $source.parent()[0].load();
  //             // }, 200);
  //           } catch (e) {
  //             console.log(e);
  //           }
  //         }
  //       });
  //     }




  //     if (this.scormfile) {
  //       this.render.listen(this.scormfile.nativeElement, 'change', () => {
  //         this.isLocalfile = true;
  //         var size = 100000000;
  //         this.files = this.scormfile.nativeElement.files;
  //         var validExts = new Array('zip');
  //         var fileType = this.files[0].type;
  //         console.log('fileType', fileType);
  //         var fileExt = false;
  //         for (let i = 0; i < validExts.length; i++) {
  //           if (fileType.includes(validExts[i])) {
  //             fileExt = true;
  //             break;
  //           }
  //         }
  //         if (!fileExt) {
  //           this.presentToast('warning', 'Please select a zip file');
  //           this.removeFile();
  //         } else if (size <= this.files[0].size) {
  //           this.presentToast('warning', 'File size should be less than 100 MB');
  //           this.removeFile();
  //         } else {
  //           try {
  //             console.log('zip', this.files[0]);
  //             this.formDataActivity.assetRef = this.files[0].name;
  //             this.assetrefLink = '';
  //             this.fileName = this.files[0].name;
  //             this.selectFileTitle = this.files[0].name;
  //             this.activityFileData = this.files[0];
  //             this.enableUpload = true;
  //             this.cdf.detectChanges();
  //             console.log('this.fileName', this.fileName);
  //             console.log('this.activityFileData', this.activityFileData);
  //             //this.pdfUpload = true;
  //             // setTimeout( () => {
  //             //   let $source: any = $('#iframe');
  //             //   $source[0].src = URL.createObjectURL(this.files[0]);
  //             //   $source.parent()[0].load();
  //             // }, 200)
  //           } catch (e) {
  //             console.log(e);
  //           }
  //         }
  //       });
  //     }
  //   }, 500);
  // }

  // getActivityIcon(activityData){
  //   if(activityData.supertypeId == 1 || activityData.supertypeId == 2){
  //     switch (Number(activityData.formatId)) {
  //       case 1: // 'video icon';
  //              return ('fa fa-video');
  //             // break;
  //       case 2: // Audio
  //               return ('fa fa-video');
  //             //  break;
  //       case 3:  // PDF
  //                return ('fa fa-file');
  //         //  break;
  //       case 5:  // Scrom
  //             return ('fa fa-file');

  //           //  break;
  //       case 7: // Image
  //              return ('fa fa-file');
  //         // break;

  //       case 10: // Power point
  //                  return ('fa fa-file');
  //               // break;
  //       case 11: // Excel
  //                return ('fa fa-file');
  //               //  break;
  //       case 12: // Word
  //               return ('fa fa-file');
  //               // break;
  //       default: return ('fa fa-file');
  //     }
  //   }else  {
  //     // Quiz or Feedback icon
  //     return ('fa fa-file');
  //   }

  // }
  selectedType = {
    'supTypeName': '',
    'subTypeName': '',
  }
  addAcitivity(activityType, clickedModule, index, supType){
    // this.addEditAcitivityContentData = {
    //   'roles': this.roles,
    //   'contentSelected': this.contentSelected,
    //   'selectedContent': this.selectedContent,
    //   'activitieslist': this.activitieslist,
    //   'selectedTags': this.selectedTags,
    //   'tagList': this.tagList,
    //   'currentActIndex': this.currentActIndex,
    //   'activityCompTrack': this.activityCompTrack,
    //   'days': this.days,
    //   'pointFormat': this.pointFormat,
    //   'subtypeId': this.selectedActivitySubType,
    //   'formDataActivity': this.formDataActivity,
    // };
    // this.selectedActivitySubTypeNew = activityType.id
    this.selectedActivitySubType = activityType.id;
    this.selectedActivitySupType = supType.typeId;
    this.selectedType.subTypeName = activityType.name;
    this.selectedType.supTypeName = supType.typeName;
    // this.formDataActivity.moduleId = clickedModule.moduleId;
    // this.formDataActivity.moduleId = module.id;

    this.addEditActivity('',clickedModule,0,null,index);
    this.makeLangugageDataReady(null);
  }


  findActivityNameBySubType(){
    for (let index = 0; index < this.activitySubTypeList.length; index++) {
      // const element = array[index];
      if(this.activitySubTypeList[index].typeId === Number(this.selectedActivitySupType)){
        this.selectedType.supTypeName = this.activitySubTypeList[index].typeName;
          for (let j = 0; j < this.activitySubTypeList[index].subtype.length; j++) {
              if(Number(this.activitySubTypeList[index].subtype[j].id) === Number(this.selectedActivitySubType)){
                this.selectedType.subTypeName = this.activitySubTypeList[index].subtype[j].name;
              }
          }
        }
      }

    }
    browsePopup(event){
      switch(this.selectedActivitySubType)
      {
        case 2: this.browseActivityContent(event);
                break;
        case 6: this.browsefeedbackContent();
                break;
        case 5: this.browsequizContent();
                break;
        case 12: this.browsefeedbackContent();
                break;
        default:this.browseActivityContent(event);
                break;
      }
    }
    removeContentpopup(){
      switch(this.selectedActivitySubType)
      {
        case 2: this.removeContent();
                break;
        case 6: this.removeSelectedFeedbackContent();
                break;
        case 5: this.removeSelectedQuizContent();
                break;
        case 12: this.removeSelectedFeedbackContent();
                break;
        default:this.removeContent();
                break;
      }
    }

    doAction(event){
      console.log('Event ==>', event);
      if(event){
        // console.log('args ==>', event.argument.join(','));
        // console.log('args ==>', [...event.argument]);
        switch(event.action){
          case 'addAcitivity' : this.addAcitivity(event.argument[0],
                                    event.argument[1], event.argument[2],
                                    event.argument[3]);
                                break;
          case 'deleteModule' : this.deleteModule(event.argument[0]);
                                break;
          case 'disableModule' : this.disableModule(event.argument[0], event.argument[1]);
                                break;
          case 'addEditCourseModule' : this.addEditCourseModule(event.argument[0], event.argument[1]);
                                break;
          case 'addEditActivity' : this.selectedActivitySubType = event.argument[0].activityTypeId;
          this.addEditActivity(event.argument[0],
            event.argument[1], event.argument[2],
            event.argument[3],event.argument[4]);
                                break;
          case 'deleteActivity' : this.deleteActivity(event.argument[0]);
                                break;
          case 'disableActivity' : this.disableActivity(event.argument[0],
            event.argument[1], event.argument[2]);
                                break;
          case 'bulkCopy' : this.bulkCopyEnableDisable(event.argument[0], event.argument[1]);
                                                      break;
        }
      }
    }

    doActionOnModule(event){
      console.log('Event ==>', event);
      if(event){
        // console.log('args ==>', event.argument.join(','));
        // console.log('args ==>', [...event.argument]);
        switch(event.action){
          case 'saveModuleDetails' : this.saveModuleDetails(event.argument[0],event.argument[1]);
                                break;
          case 'closeModuleForm' : this.closeModuleForm();
                                break;
          case 'readModuleThumb' : this.readModuleThumb(event.argument[0]);
                                break;
          case 'onCreatorSelect' : this.onCreatorSelect(event.argument[0], event.argument[1]);
                                break;
          case 'OnCreatorDeSelect' : this.OnCreatorDeSelect(event.argument[0],event.argument[1]);
                                break;
          case 'deleteModuleThumb' : this.deleteModuleThumb();
                                break;
          case 'addtrainer' : this.Addtrainer(event.argument[0]);
                                break;
          case 'nomination' : this.nomination(event.argument[0]);
                                break;
          case 'removeTrainer' : this.Removetrainer(event.argument[0],event.argument[1],
            event.argument[2],event.argument[3],event.argument[4]);
                                break;
        }
      }
    }

  // MultiLangugae

  damAssetsList: any = [
    {
      checked: false,
      assetId: 1,
      name: "Asset 1",
      description: "Asset 1",
      language: "English",
      created_by: "XYZ",
      created_on: new Date(),
    },
    {
      checked: false,
      assetId: 2,
      name: "Asset 2",
      description: "Asset 2",
      language: "English",
      created_by: "XYZ",
      created_on: new Date(),
    },
    {
      checked: false,
      assetId: 3,
      name: "Asset 3",
      description: "Asset 3",
      language: "Marathi",
      created_by: "XYZ",
      created_on: new Date(),
    },
    {
      checked: false,
      name: "Asset 4",
      assetId: 4,
      description: "Asset 4",
      language: "Hindi",
      created_by: "XYZ",
      created_on: new Date(),
    },
    {
      checked: false,
      name: "Asset 5",
      assetId: 5,
      description: "Asset 5",
      language: "Telgu",
      created_by: "XYZ",
      created_on: new Date(),
    },
  ];
  languageList = [

  ];
  selectedMultiLanguageContent = [];
  removeActivityConfirmation = false;
  removeActivityContent = null;

  performActionOnData(event) {
    console.log("Event ==>", event);
    if (event) {
      // console.log('args ==>', event.argument.join(','));
      // console.log('args ==>', [...event.argument]);
      switch (event.action) {
        case "toggelMultiLanguage":
          // code...

          this.selectedMultiLanguageContent = event["argument"][0];
          // this.languageList = event['argument'][1];
          this.toggelMultiLanguage(event["argument"][1]);
          break;
        case "searchAsset":
          // code...
          this.selectedMultiLanguageContent = event["argument"][0];
          this.searchActivityContent(
            event["argument"][0],
            event["argument"][2]
          );
          this.languageList = event["argument"][1];
          break;
        case "save":
          this.setMultiContentData(event["argument"][0]);
          this.languageList = event["argument"][1];
          // code...
          break;
        case "clearSearch":
            this.clearSearch();
            // this.languageList = event["argument"][1];
            // code...
            break;
      }
    }
  }

  toggelMultiLanguage(action) {
    this.activityContentShow = action;
    this.cdf.detectChanges();
  }

  setMultiContentData(data) {
    this.selectedMultiLanguageContent = data;
    if (
      this.selectedMultiLanguageContent &&
      this.selectedMultiLanguageContent.length != 0
    ) {
      this.activeSelectedContent = this.selectedMultiLanguageContent[0];
      this.saveSelectedActivityContent();
    } else {
      this.toastr.warning("Please Select Content", "Warning");
    }
  }
  makeMulticontentDataReady() {
    if (
      this.selectedMultiLanguageContent &&
      this.selectedMultiLanguageContent.length > 1
    ) {
      console.log("Seperator Pipe ==>", this.dataSeparatorService.Pipe);
      console.log("Seperator Hash ==>", this.dataSeparatorService.Hash);
      console.log("Seperator Dollar ==>", this.dataSeparatorService.Dollar);
      let string = "";
      for (
        let index = 1;
        index < this.selectedMultiLanguageContent.length;
        index++
      ) {
        let singleAssetString =
          this.selectedMultiLanguageContent[index]["cmId"] +
          this.dataSeparatorService.Hash +
          this.selectedMultiLanguageContent[index]["langId"] +
          this.dataSeparatorService.Hash +
          this.selectedMultiLanguageContent[index]["assetRef"] +
          this.dataSeparatorService.Hash +
          this.selectedMultiLanguageContent[index]["referenceType"] +
          this.dataSeparatorService.Hash +
          this.selectedMultiLanguageContent[index]["mimeType"] +
          this.dataSeparatorService.Hash +
          this.selectedMultiLanguageContent[index]["formatId"] +
          this.dataSeparatorService.Hash +
          this.selectedMultiLanguageContent[index]["id"];
        if (string != "") {
          string += this.dataSeparatorService.Pipe;
        }
        string += singleAssetString;
      }
      return string;
    } else {
      return "";
    }
  }

  makeLangugageDataReady(selectedData) {
   this.clearSearch();
    if (selectedData) {
      for (let index = 0; index < this.languageList.length; index++) {
        this.languageList[index]["selected"] = false;
      }
      for (
        let indexj = 0;
        indexj < this.selectedMultiLanguageContent.length;
        indexj++
      ) {
        const item = this.selectedMultiLanguageContent[indexj];
        // item["presentInDB"] = true;
        item["presentInDB"] = false;
        item["formatIcon"] = true;
        for (let index = 0; index < this.languageList.length; index++) {
          if (
            Number(item["langId"]) ===
            Number(this.languageList[index]["languageId"])
          ) {
            this.languageList[index]["selected"] = true;
            item["langName"] = this.languageList[index]["languageName"];
            break;
          }
        }
      }
    } else {
      for (let index = 0; index < this.languageList.length; index++) {
        this.languageList[index]["selected"] = false;
      }
    }
  }

  preformActionOnRemove(event) {
    this.removeActivityContent = event;
    // this.removeActivityConfirmation = true;
    // this.toggelActivityConfirmation(true);
    this.removeLangugae(this.removeActivityContent);
  }

  toggelActivityConfirmation(event){
    this.removeActivityConfirmation = event;
    this.cdf.detectChanges();
    if(!event){
      this.removeActivityContent = null;
    }
  }
  removeLangugae(event){
    this.selectedMultiLanguageContent = event["selectedMultiLanguageContent"];
    // if (event["index"] == 0) {

    //   // const params = {
    //   //   cmid:
    //   // };
    //   if(event['formData'] && event['formData']['activityId'] != 0){

    //   }else {
    //     this.updateDeletedActivityData(event);
    //   }
    //   console.log('formData', event);
    // } else {
    //   this.updateDeletedActivityData(event);
    // }
    console.log('Activity Data ==>', event);
    // if(event['formData'] && event['formData']['activityId'] != 0){


    // }else {

    // }
    if(event['removedItem']['presentInDB']){
      let params = {
        cmid: "",
        contentId: "",
        langId : "",
        actId: "",
        deletflag: null,
      }
      if (event["index"] == 0) {
        if(this.selectedMultiLanguageContent.length > 1){

          if(this.selectedMultiLanguageContent[1]['presentInDB']){
            params = {
              cmid: this.selectedMultiLanguageContent[1].cmId,
              contentId: event['removedItem'].contentId,
              langId : event['removedItem'].langId,
              actId: event['formData'].activityId,
              deletflag: 1,
             };
             console.log('Activity Data ==>', params);
          }else {
            this.toastr.warning('First save other activity and then default activity.', 'Warning');
            this.toggelActivityConfirmation(false);
            return null;
          }
        }else {
          this.toastr.warning('You cannot delete default activity.', 'Warning');
          this.toggelActivityConfirmation(false);
          return null;
        }
     }else {
        params = {
         cmid: event['removedItem'].cmId,
         contentId: event['removedItem'].contentId,
         langId : event['removedItem'].langId,
         actId: event['formData'].activityId,
         deletflag: 2,
        };
        console.log('Activity Data ==>', params);
     };
     this.spinner.show();
     this.deleteMapActivityContent(params , (flag, response) =>{
       console.log('deleteMapActivityContent', response);

       if(flag) {
         if(response['data'] && response['data'].length !=0){
           const msgData =  response['data'][0];
           if(msgData['msgflag'] == 1){
             this.toastr.success(msgData['msg'], "Success", {
               closeButton: false,
             });
             this.updateDeletedActivityData(event);

           }else{
             this.toastr.warning(msgData['msg'], "Warning", {
               closeButton: false,
             });
           }
         }

         this.spinner.hide();
         this.toggelActivityConfirmation(false);
       }else {
         this.toastr.warning("Something Went Wrong", "Warning", {
           closeButton: false,
         });
         this.spinner.hide();
        }
      });
    }else {
      this.updateDeletedActivityData(event);
      this.toggelActivityConfirmation(false);
    }
  }
  updateDeletedActivityData(event){
    this.selectedMultiLanguageContent.splice(event["index"], 1);
    if (this.selectedMultiLanguageContent.length !== 0) {
      this.activeSelectedContent = this.selectedMultiLanguageContent[0];
      this.saveSelectedActivityContent();
    } else {
      this.removeSelectedActivityContent();
    }
    for (let j = 0; j < this.languageList.length; j++) {
      if (
        Number(event["removedItem"]["langId"]) ===  Number(this.languageList[j]["languageId"]) &&
        this.languageList[j]["selected"]
      ) {
        this.languageList[j]["selected"] = false;
        break;
      }
    }
    this.languageList = _.cloneDeepWith(this.languageList);
    this.selectedMultiLanguageContent = _.cloneDeepWith(
      this.selectedMultiLanguageContent,
    );
  }
  makeMultiActivitycontentDataReady(activityData) {
    if (activityData) {
      let object = {
        assetDate: new Date(),
        assetName: activityData.name,
        assetRef: activityData.reference,
        createdBy: null,
        createdOn: new Date(),
        description: "",
        file_type: activityData.referenceType,
        file_url: activityData.reference,
        formatId: activityData.formatId,
        id: activityData.contentRepId,
        langId: activityData.languageId,
        // langName: "English",
        mimeType: activityData.mimeType,
        mime_type: activityData.mimeType,
        referenceType: activityData.referenceType,
        contentId: activityData.contentId,
      };
      // if(this.languageList){
      //   for(let index = 0; index < this.languageList.length; index++){
      //     // this.languageList[index]['selected'] = false;
      //     if(Number(activityData['languageId']) === Number(this.languageList[index]['languageId'])){
      //       object['langName'] = this.languageList[index]['languageName'];
      //     }
      //   }
      // }
      if (activityData["multilanguage"]) {
        this.selectedMultiLanguageContent = [
          object,
          ...activityData["multilanguage"],
        ];
      } else {
        this.selectedMultiLanguageContent = [object];
      }
      this.makeLangugageDataReady(this.selectedMultiLanguageContent);
      this.languageList = _.cloneDeepWith(this.languageList);
      this.selectedMultiLanguageContent = _.cloneDeepWith(
        this.selectedMultiLanguageContent,
      );
    }
    console.log(
      "this.selectedMultiLanguageContent",
      this.selectedMultiLanguageContent,
    );
  }
  noDataFoundActivitySearch = {
    errorKey: "",
    message: "",
  };
  searchActivityContent(currentEvent, srchStr) {
    let srchQuery = currentEvent;
    console.log("contentSearchQry ", srchStr);
    const param = {
      searchString: srchStr,
      tId: this.tenantId,
      userId: this.currentUId,
      token: this.GlobalDamToken,
      assetType: this.selectedFormatId,
      // formatId: this.selectedFormatId,
    };
    if (this.GlobalDamToken != null) {
      this.spinner.show();
      this.ModuleService.ApprovedDamHanshakeData(param).then(
        (rescompData) => {
          this.spinner.hide();
          if (rescompData) {
            if (rescompData["type"] == true) {
              if (
                rescompData["AssetData"] &&
                rescompData["AssetData"].length == 0
              ) {
                this.noDataFoundActivitySearch.errorKey = "noData";
                this.noDataFoundActivitySearch.message = "No Asset Available";
                this.srchContentResult = [];
                // this.dataDetail = 'You have no access to this asset';
              } else {
                this.processSearchData(rescompData["AssetData"]);
              }
            } else {
              this.noDataFoundActivitySearch.errorKey = "token_key";
              this.noDataFoundActivitySearch.message =
                "You have no access to this asset";

              this.srchContentResult = [];
              // this.dataDetail = 'No Asset Available';
            }
          } else {
            this.srchContentResult = [];
            // this.dataDetail = 'No Asset Available';
            this.noDataFoundActivitySearch.errorKey = "noData";
            this.noDataFoundActivitySearch.message = "No Asset Available";
          }
          this.noDataFoundActivitySearch = _.cloneDeepWith(this.noDataFoundActivitySearch);
          console.log("Search Content Result ", rescompData);
          this.cdf.detectChanges();
        },
        (resUserError) => {
          // this.loader =false;
          this.spinner.hide();
          this.errorMsg = resUserError;
        }
      );
    }
  }

  processSearchData(data) {
    if (this.selectedMultiLanguageContent && data) {
      const result = data.filter(
        (x) =>
          !this.selectedMultiLanguageContent.filter(
            (y) => Number(y.id) === Number(x.id)
          ).length,
      );
      console.log("processSearchData", result);
      this.srchContentResult = result;
      if(result.length !=0){
        this.noDataFoundActivitySearch.errorKey = "noData";
        this.noDataFoundActivitySearch.message = "No Asset Available";
      }
      this.noDataFoundActivitySearch = _.cloneDeepWith(
        this.noDataFoundActivitySearch,
      );
    } else {
      this.srchContentResult = data;
    }
    // this.srchContentResult = _.cloneDeepWith(this.srchContentResult);
    this.noDataFoundActivitySearch = _.cloneDeepWith(
      this.noDataFoundActivitySearch,
    );
  }

  clearSearch(){
    this.noDataFoundActivitySearch = {
      errorKey: "searchContent",
      message: "Search content...",
    };
    this.srchContentResult = [];
  }

  deleteMapActivityContent(params, cb){
    const _deleteLanguageMapContent = webApi.domain + webApi.url.deleteLanguageMapContent;
    this.commonFunctionService
    .httpPostRequest(_deleteLanguageMapContent, params)
    .then((res) => {
      console.log("Response", res);
      if(res['type']){
        cb(true, res);
      }else {
        cb(false, res);
      }
      // this.toastr.success("Something Went Wrong", "Success");
    })
    .catch(function (err) {
      // next(new Error('user not found'));

      cb(false, err);
      // this.linkGenerationInProgress = false;
    });
  }

  preformActionOnFormatChange(event){
    if(event){
      console.log('preformActionOnFormatChange', event);
      this.selectedMultiLanguageContent = event['selectedMultiLanguageContent'];
      this.makeLangugageDataReady(null);
    }
  }

  // Bulk Activity Changes
  selectedBulkActivity = null;
  showBulkUploadContainer = false;
  config = {
    categoryList: {
      show: true,
      tabTitle: "Category",
      identifier: "category",
      disabled: false,
      showSave: false,
    },
    courseList: {
      show: true,
      tabTitle: "Batch",
      identifier: "courseList",
      disabled: true,
      showSave: true,
    },
    resultList: {
      show: true,
      tabTitle: "Result",
      identifier: "resultList",
      disabled: true,
      showSave: false,
    },
  };
  bulkCopyEnableDisable(activityData,flag){

    this.showBulkUploadContainer = flag;
    this.selectedBulkActivity = activityData;
    if(flag){
      this.selectedBulkActivity['courseId'] = this.courseId;
    }
  }

  performActionOnBulk(event){
    console.log("Event ==>", event);
    if (event) {
      // console.log('args ==>', event.argument.join(','));
      // console.log('args ==>', [...event.argument]);
      switch (event.action) {
       case 'closeCopy': this.bulkCopyEnableDisable(event["argument"][0],event["argument"][1]);
      }
    }
  }

  bindActivityIconsToActivity(course){
    for (let index = 0; index < course.length; index++) {
      const element = course[index];
      if(element['activity'] && element['activity'].length !=0){
        for (let indexj = 0; indexj < element['activity'].length; indexj++) {
          element['activity'][indexj]['actIcon'] = this.bindActivityIcon(element['activity'][indexj]);
        }
      }
    }
  }
 bindActivityIcon(detail) {
    // if (detail.activity_type === 'Quiz') {
    if (detail.activityTypeId == 5) {
      return "quiz";
      // } else if ( detail.activity_type  === 'Feedback') {
    } else if (detail.activityTypeId == 6 || detail.activityTypeId == 12) {
      return "feedback";
    } else if (detail.activityTypeId == 11) {
      return "webinar";
      // } else if ( detail.activity_type === 'Attendance') {
    } else if (detail.activityTypeId == 9) {
      return "attendance";
    } else if (detail.activityTypeId == 1 || detail.activityTypeId == 2) {
      switch (Number(detail.formatId)) {
        case 1: // video
          return "video";
        case 2: // Audio
          return "audio";
        case 3: // PDF
          return "pdf";
        case 4: // K-point
          return "kpoint";
        case 5: // Scrom
          return "scrom";
        case 6: // Youtube
          return "youtube";
        case 7: // Image
          return "image";
        case 8: // External link
          return "url";
        case 9: // Practice file
          return "practice_file";
        case 10: // PPT
          return "ppt";
        case 11: // Excel
          return "excel";
        case 12: // Word
          return "word";
        default:
          return "sync";
      }
    } else {
      return "sync";
    }

  }
}
