import { Component, OnInit } from '@angular/core';
import { SortablejsOptions } from 'angular-sortablejs';

@Component({
  selector: 'batch-workflow',
  templateUrl: './workflow.component.html',
  styleUrls: ['./workflow.component.scss']
})
export class WorkflowComponent implements OnInit {
  TasksSortable : SortablejsOptions = {
    group: {
      name: 'tasks',

    },
       sort: true,
    handle: '.drag-handle',
  };

  // assignedto:[
  //   { id: 1, name:"Test 1"},
  //   { id: 2, name:"Test 1"},
  //   { id: 3, name:"Test 1"},
  //   { id: 4, name:"Test 1"},
  // ]

  tasks : any = [
    { taskName: "Task 1", assignee: [
      { id: 1, name:"Test 1"},
      { id: 2, name:"Test 1"},
      { id: 3, name:"Test 1"},
      { id: 4, name:"Test 1"},
    ] },
    { taskName: "Task 2", assignee: [
      { id: 1, name:"Test 1"},
      { id: 2, name:"Test 1"},
      { id: 3, name:"Test 1"},
      { id: 4, name:"Test 1"},
    ] },
    { taskName: "Task 3", assignee: [
      { id: 1, name:"Test 1"},
      { id: 2, name:"Test 1"},
      { id: 3, name:"Test 1"},
      { id: 4, name:"Test 1"},
    ] },
    { taskName: "Task 4", assignee: [
      { id: 1, name:"Test 1"},
      { id: 2, name:"Test 1"},
      { id: 3, name:"Test 1"},
      { id: 4, name:"Test 1"},
    ] }
  ]
  constructor() { }

  ngOnInit() {
  }

}
