import {Injectable, Inject} from '@angular/core';
import {Http, Response, Headers, RequestOptions, Request} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppConfig} from '../../../../../app.module';
import { webAPIService } from '../../../../../service/webAPIService';
import {webApi} from '../../../../../service/webApi';
import { HttpClient } from "@angular/common/http";

@Injectable()

export class engageBlendService {

  public data: any;
  request: Request;

  private _url:string = "";
  private _urlfetchnoteventdropdown = webApi.domain + webApi.url.getnotevent;
  private _urlfetchnottemplatedropdown = webApi.domain + webApi.url.dropdownnotifytemplate;
  private _urlInsertUpdateCourseNotification: string = webApi.domain + webApi.url.insertupdateladdernotification;
  private _urlGetCourseNotification: string = webApi.domain + webApi.url.getcoursenotification;
  private _urlGetNotificationTags: string = webApi.domain + webApi.url.getNotificationTags;

  constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private _http: Http,private _httpClient:HttpClient){
      //this.busy = this._http.get('...').toPromise();
  }

  getData() {
     let url:any = `${this.config.FINAL_URL}`+this._url;
     // let url:any = `${this.config.DEV_URL}`+this._urlFilter;
     let headers = new Headers({ 'Content-Type': 'application/json' });
     let options = new RequestOptions({ headers: headers });
     //let body = JSON.stringify(user);
     return this._http.post(url, options ).map((res: Response) => res.json());
  }

  getNotEventDropdown(param) {
   // let url:any = this. _urlfetchnoteventdropdown;
   // return this._http.post(url,param)
   //     .map((response:Response)=>response.json())
   //     .catch(this._errorHandler);

       return new Promise(resolve => {
         this._httpClient.post(this._urlfetchnoteventdropdown, param)
         //.map(res => res.json())
         .subscribe(data => {
             resolve(data);
         },
         err => {
             resolve('err');
         });
     });
}

getNottemplateDropdown(param) {
   // let url:any = this._urlfetchnottemplatedropdown;
   // return this._http.post(url,param)
   //     .map((response:Response)=>response.json())
   //     .catch(this._errorHandler);

   return new Promise(resolve => {
      this._httpClient.post(this._urlfetchnottemplatedropdown, param)
      //.map(res => res.json())
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}

insertUpdateCouresNotifications(param) {
   // let url: any = this._urlInsertUpdateCourseNotification;
   // return this._http.post(url, param)
   //    .map((response: Response) => response.json())
   //    .catch(this._errorHandler);

   return new Promise(resolve => {
      this._httpClient.post(this._urlInsertUpdateCourseNotification, param)
      //.map(res => res.json())
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}

  _errorHandler(error: Response){
     console.error(error);
     return Observable.throw(error || "Server Error")
  }

  getcoursetemplate(param) {
   // let url:any = this._urlfetchnottemplatedropdown;
   // return this._http.post(url,param)
   //     .map((response:Response)=>response.json())
   //     .catch(this._errorHandler);
   return new Promise(resolve => {
      this._httpClient.post(this._urlfetchnottemplatedropdown, param)
      //.map(res => res.json())
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}

getCourseNotifications(param) {
   // let url:any = this._urlGetCourseNotification;
   // return this._http.post(url,param)
   //     .map((response:Response)=>response.json())
   //     .catch(this._errorHandler);

   return new Promise(resolve => {
      this._httpClient.post(this._urlGetCourseNotification, param)
      //.map(res => res.json())
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}

getNotificationsTags(param) {
  // let url:any = this._urlGetCourseNotification;
  // return this._http.post(url,param)
  //     .map((response:Response)=>response.json())
  //     .catch(this._errorHandler);

  return new Promise(resolve => {
     this._httpClient.post(this._urlGetNotificationTags, param)
     //.map(res => res.json())
     .subscribe(data => {
         resolve(data);
     },
     err => {
         resolve('err');
     });
 });
}

}
