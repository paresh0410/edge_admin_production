import { Component, OnInit, Output, EventEmitter, ViewChild, Input, SimpleChanges } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { SuubHeader } from '../../../components/models/subheader.model';
import { Router } from '@angular/router';
import { TabsetComponent } from 'ngx-tabset/src/ngx-tabset';
import { XlsxToJsonService } from '../../users/uploadusers/xlsx-to-json-service';
import { DataSeparatorService } from '../../../../service/data-separator.service';
import { CommonFunctionsService } from '../../../../service/common-functions.service';
import { webApi } from '../../../../service/webApi';
import { JsonToXlsxService } from '../../un-enrol-employess/json-to-xlsx.service';
import { NgxSpinnerService } from 'ngx-spinner';
var _this;
@Component({
  selector: 'ngx-batch-upload',
  templateUrl: './batch-upload.component.html',
  styleUrls: ['./batch-upload.component.scss']
})
export class BatchUploadComponent implements OnInit {
  file: any = '';
  fileName: any = 'Click here to upload an excel file to set notification in batches'
  uploadedFile: boolean = false;
  header: SuubHeader
  @Output() tabChanged = new EventEmitter<any>();
  @Input() parentTabEvent: any
  @Input() param: any


  sidebarForm: boolean = false;
  labels: any = [
    { labelname: 'Batch Code', bindingProperty: 'Batch Code', componentType: 'text' },
    { labelname: 'Batch Name', bindingProperty: 'Batch Name', componentType: 'text' },
    { labelname: 'Category Code', bindingProperty: 'Category Code', componentType: 'text' },
    { labelname: 'Program Code', bindingProperty: 'Program Code', componentType: 'text' },
    { labelname: 'Visibility', bindingProperty: 'Visibility', componentType: 'text' },
    { labelname: 'coursePicRef', bindingProperty: 'coursePicRef', componentType: 'text' },
    { labelname: 'Summary', bindingProperty: 'Summary', componentType: 'text' },
    { labelname: 'Tags', bindingProperty: 'Tags', componentType: 'text' },
    { labelname: 'Batch Level', bindingProperty: 'Batch Level', componentType: 'text' },
    { labelname: 'Batch Creator', bindingProperty: 'Batch Creator', componentType: 'text' },
    { labelname: 'Role', bindingProperty: 'Role', componentType: 'text' },
    { labelname: '    Credit Type     ', bindingProperty: 'Credit Type', componentType: 'text' },
    { labelname: 'Before Completion', bindingProperty: 'Before Completion ', componentType: 'text' },
    { labelname: 'After Completion', bindingProperty: 'After Completion', componentType: 'text' },
    { labelname: 'Batch Loaction', bindingProperty: 'Batch Loaction', componentType: 'text' },
    { labelname: 'Batch Venue', bindingProperty: 'Batch Venue', componentType: 'text' },
    { labelname: 'Batch Start Date', bindingProperty: 'Batch Start Date', componentType: 'date' },
    { labelname: 'Batch End Date', bindingProperty: 'Batch End Date', componentType: 'date' },

  ]

  labelsSession: any = [
    { labelname: 'Batch Code', bindingProperty: 'Batch Code', componentType: 'text' },
    { labelname: 'Session Code', bindingProperty: 'Session Code', componentType: 'text' },
    { labelname: 'Session Name', bindingProperty: 'Session Name', componentType: 'text' },
    { labelname: 'sessionPicRef', bindingProperty: 'sessionPicRef', componentType: 'text' },
    { labelname: 'Summary', bindingProperty: 'Summary', componentType: 'text' },
    { labelname: 'Session Start Date', bindingProperty: 'Session Start Date', componentType: 'date' },
    { labelname: 'Session End Date', bindingProperty: 'Session End Date', componentType: 'date' },
    { labelname: 'Request to Nomination', bindingProperty: 'Request to Nomination', componentType: 'text' },
    { labelname: 'Trainer Name', bindingProperty: 'Trainer Name', componentType: 'text' },
    { labelname: 'Visibility', bindingProperty: 'Visibility', componentType: 'text' },
  ]

  reviewlabels: any = [
    { labelname: 'Batch Code', bindingProperty: 'batchCode', componentType: 'text' },
    { labelname: 'Batch Name', bindingProperty: 'batchName', componentType: 'text' },
    { labelname: 'Category Code', bindingProperty: 'categoryCode', componentType: 'text' },
    { labelname: 'Program Code', bindingProperty: 'programCode', componentType: 'text' },
    { labelname: 'Visibility', bindingProperty: 'visibility', componentType: 'text' },
    { labelname: 'coursePicRef', bindingProperty: 'coursePicRef', componentType: 'text' },
    { labelname: 'Summary', bindingProperty: 'summary', componentType: 'text' },
    { labelname: 'Tags', bindingProperty: 'tags', componentType: 'text' },
    { labelname: 'Batch Level', bindingProperty: 'batchLevel', componentType: 'text' },
    { labelname: 'Batch Creator', bindingProperty: 'batchCreator', componentType: 'text' },
    { labelname: 'Role', bindingProperty: 'role', componentType: 'text' },
    { labelname: '    Credit Type     ', bindingProperty: 'creaditType', componentType: 'text' },
    { labelname: 'Before Completion', bindingProperty: 'beforeComp', componentType: 'text' },
    { labelname: 'After Completion', bindingProperty: 'afterComp', componentType: 'text' },
    { labelname: 'Batch Loaction', bindingProperty: 'batchLocation', componentType: 'text' },
    { labelname: 'Batch Venue', bindingProperty: 'batchVenue', componentType: 'text' },
    { labelname: 'Batch Start Date', bindingProperty: 'batchStartDate', componentType: 'date' },
    { labelname: 'Batch End Date', bindingProperty: 'batchEndDate', componentType: 'date' },

  ]

  reviewSession: any = [
    { labelname: 'Batch Code', bindingProperty: 'batchCode', componentType: 'text' },
    { labelname: 'Session Code', bindingProperty: 'sessionCode', componentType: 'text' },
    { labelname: 'Session Name', bindingProperty: 'sessionName', componentType: 'text' },
    { labelname: 'sessionPicRef', bindingProperty: 'modulePicRef', componentType: 'text' },
    { labelname: 'Summary', bindingProperty: 'summary', componentType: 'text' },
    { labelname: 'Session Start Date', bindingProperty: 'startDate', componentType: 'date' },
    { labelname: 'Session End Date', bindingProperty: 'EndDate', componentType: 'date' },
    { labelname: 'Request to Nomination', bindingProperty: 'requestForNomination', componentType: 'text' },
    { labelname: 'Trainer Name', bindingProperty: 'trainerCode', componentType: 'text' },
    { labelname: 'Visibility', bindingProperty: 'visibility', componentType: 'text' },
  ]
  tab1: boolean = true;
  tab2: boolean = false;
  tab3: boolean = false;
  fileSelected: boolean = false;
  resultSheets: string[];
  result: any;
  uploadedData: any;
  private xlsxToJsonService: XlsxToJsonService = new XlsxToJsonService();
  resultData: any = [];
  usersErrorsData: any;
  validCount: any;
  InvalidCount: any;
  dummyBulkArray =
    ['Batch Code', 'Batch Name', 'Category Code', 'Program Code', 'Visibility', 'coursePicRef',
      'Summary', 'Tags', 'Batch Level', 'Batch Creator', 'Role', 'Credit Type', 'Before Completion ',
      'After Completion', 'Batch Loaction', 'Batch Venue', 'Batch Start Date', 'Batch End Date']

  dummyBulkSessionArray =
    ['Batch Code', 'Session Code', 'Session Name', 'sessionPicRef', 'Summary', 'Session Start Date', 'Session End Date',
      'Request to Nomination', 'Trainer Name', 'Visibility'
    ];
  DateConvertionArrayList: any = { 'Batch Start Date': true, 'Batch End Date': true };
  DateConvertionArraySessionList: any = { 'Session Start Date': true, 'Session End Date': true };
  dupResultData: any;


  constructor(private toastr: ToastrService, private commonservice: CommonFunctionsService,
    private spinner: NgxSpinnerService,
    private router: Router, public dataSeparatorService: DataSeparatorService,
    protected exportService: JsonToXlsxService) {
    _this = this;
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('changes ====================>>>>>>>>>>>', changes);
    if (this.parentTabEvent && this.parentTabEvent.tabTitle == 'Session') {
      this.fileName = 'Click here to upload an excel file to upload Session'
    } else {
      this.fileName = 'Click here to upload an excel file to  upload batches in bulk'
    }
    if (this.param) {
      if (this.parentTabEvent.tabTitle != 'Session') {
        this.searchBatch(this.param.event)
      } else if (this.parentTabEvent.tabTitle == 'Session') {
        this.searchSession(this.param.event)
      }
    }

  }

  ngOnInit() {
    this.header = {
      title: 'Bulk-Batch',
      btnsSearch: true,
      btnBackshow: true,
      showBreadcrumb: true,
      btnAddshow: false,
      btnAdd: 'Save',
      breadCrumbList: [
        {
          'name': ' Blended Learning',
          'navigationPath': '/pages/blended-home',
        },
        {
          'name': 'bulk-batch',
          'navigationPath': '/pages/blended-home/bulk-batch',
        },
      ]
    };
  }

  onSelectTab(tabEvent, tabs) {
    console.log('tab Selected', tabs);

    // if (tabEvent.tabTitle == 'Add File') {
    if (tabEvent == 0) {
      this.header = {
        title: 'Batch Upload',
        btnsSearch: true,
        btnBackshow: true,
        showBreadcrumb: true,
        btnAddshow: false,
        btnAdd: 'Save',
        btnName1show: true,
        btnName1: 'Upload',
        btnNameupload: true,
        uploadsheetLink: "assets/images/Template_employee_upload.xlsx",
        breadCrumbList: [
          {
            'name': ' Blended Learning',
            'navigationPath': '/pages/blended-home',
          },
          {
            'name': 'bulk-batch',
            'navigationPath': '/pages/blended-home/bulk-batch',
          },
        ]
      };
      this.uploadedFile = false
      this.header.btnAddshow = false
      this.tab1 = true
      this.tab2 = false
      this.tab3 = false
    }
    // else if(tabEvent.tabTitle == 'Preview')
    else if (tabEvent == 1) {
      this.header = {
        title: 'Batch Upload',
        btnsSearch: true,
        btnBackshow: true,
        showBreadcrumb: true,
        btnAddshow: true,
        btnAdd: 'Save',
        breadCrumbList: [
          {
            'name': 'Blended Learning',
            'navigationPath': '/pages/blended-home',
          },
          {
            'name': 'bulk-batch',
            'navigationPath': '/pages/blended-home/bulk-batch',
          },
        ]
      };
      this.uploadedFile = true
      this.tab2 = true
      this.tab1 = false
      this.tab3 = false


    }
    else {
      this.header = {
        title: 'Batch Upload',
        btnsSearch: true,
        btnBackshow: true,
        showBreadcrumb: true,
        btnAddshow: false,
        btnAdd: 'Save',
        breadCrumbList: [
          {
            'name': 'Blended Learning',
            'navigationPath': '/pages/blended-home',
          },
          {
            'name': 'bulk-batch',
            'navigationPath': '/pages/blended-home/bulk-batch',
          },
        ]
      };
      this.header.btnAddshow = false
      this.uploadedFile = true
      this.tab3 = true
      this.tab1 = false
      this.tab2 = false

    }
    this.tabChanged.emit(tabEvent)


  }
  uploadFile() {
    if (this.file == '') {
      this.presentToast('warning', 'Please select a file')
    } else {
      this.uploadedFile = true
      // var event = {
      //   tabTitle: 'Preview'
      // }
      var event = 1
      this.onSelectTab(event, "jnhhh")
    }

  }
  upload() {
    // _this = this;
    this.file = ''
    if (this.parentTabEvent && this.parentTabEvent.tabTitle == 'Session') {
      this.fileName = 'Click here to upload an excel file to upload Session'
      this.uploadSession()
    } else {
      this.fileName = 'Click here to upload an excel file to  upload batches in bulk'

      this.spinner.show()

      var keys = []
      var valid = true
      // keys = Object.keys(this.uploadedData[0])
      let object = this.uploadedData.length > 0 ? this.uploadedData[0] : {};
      for (let key in object) {
        if (object.hasOwnProperty(key)) {
          //Now, object[key] is the current value
          if (String(key).startsWith('__EMPTY')) {
            delete object[key];
          } else {
            keys.push(key);
          }
        }
      }
      console.log("keys", keys);
      //VALIDATION
      for (var i = 0; i < keys.length; i++) {
        keys[i] = String(keys[i]).trim();
        this.dummyBulkArray[i] = String(this.dummyBulkArray[i]).trim();
        if (keys[i] !== this.dummyBulkArray[i]) {
          // if(keys[i].equals(this.dummyBulkArray[i])){
          var valid = false
          break;
        }
      }
      //end validation

      this.resultData = []
      this.usersErrorsData = []
      var hash = this.dataSeparatorService.Hash
      // var count = 18
      if ((keys.length == this.dummyBulkArray.length) && (valid == true)) {
        // for (let i = 0; i < this.uploadedData.length; i++) {
          var option: string = Array.prototype.map
            .call(this.uploadedData, function (item) {
              console.log('item', item);
              // for (let x in item) {
              //   if (_this.DateConvertionArrayList[x]) {
              //     item[x] = _this.commonservice.formatDateTimeForBulkUpload(item[x]);
              //   }
              // }
              return Object.values(item).join(hash);
            })
            .join(this.dataSeparatorService.Pipe);

        // }
        var param = {
          finalString: option,
          courseTypeId: 4
        }
        const _urlBulkUpload: string = webApi.domain + webApi.url.bulkUpload;
        this.commonservice.httpPostRequest(_urlBulkUpload, param).then(res => {
          console.log("res", res)
          if (res['type'] == true) {
            var result = res['output']
            this.spinner.hide()
            for (let i = 0; i < result.length; i++) {
              if (result[i].isValid == 0) {
                this.result[i]['status'] = 'Invalid';
                this.usersErrorsData.push(result[i]);
              }
              if (result[i].isValid == 1) {
                this.resultData.push(result[i]);
              }
              this.dupResultData = this.resultData;
            }

            this.validCount = this.resultData.length
            this.InvalidCount = this.usersErrorsData.length
            this.getErrorData()
            this.uploadedData = []
            // this.resultData = res['output']
            console.log(this.resultData, "resukjkk")
            console.log(option, "separator")
            console.log(typeof (option))
            var event = 2
            this.onSelectTab(event, "jnhhh")
          }
          else {
            this.spinner.hide()
            this.presentToast('warning', 'Something went wrong')
          }

        })

      }
      else {
        this.spinner.hide()
        this.presentToast('warning', 'Invalid File')
      }
    }

  }

  uploadSession() {
    this.spinner.show()
    var keys = []
    var valid = true
    // keys = Object.keys(this.uploadedData[0])
    let object = this.uploadedData.length > 0 ? this.uploadedData[0] : {};
    for (let key in object) {
      if (object.hasOwnProperty(key)) {
        //Now, object[key] is the current value
        if (String(key).startsWith('__EMPTY')) {
          delete object[key];
        } else {
          keys.push(key);
        }
      }
    }
    console.log("keys", keys)
    //VALIDATION
    for (var i = 0; i < keys.length; i++) {
      keys[i] = String(keys[i]).trim()
      this.dummyBulkSessionArray[i] = String(this.dummyBulkSessionArray[i]).trim()
      if (keys[i] != this.dummyBulkSessionArray[i]) {
        // if(keys[i].equals(this.dummyBulkArray[i])){
        var valid = false
        break;
      }
      //  else{
      //    valid = true
      //  }
    }
    //end validation

    this.resultData = []
    this.usersErrorsData = []
    var hash = this.dataSeparatorService.Hash
    // var count = 18
    if ((keys.length == this.dummyBulkSessionArray.length) && (valid == true)) {
      // for (let i = 0; i < this.uploadedData.length; i++) {
        var option: string = Array.prototype.map
          .call(this.uploadedData, function (item) {
            // console.log('Old item', item);
            // for (let x in item) {
            //   if (_this.DateConvertionArraySessionList[x]) {
            //     console.log('Original Date - ', item[x]);
            //     item[x] = _this.commonservice.formatDateTimeForBulkUpload(item[x]);
            //     console.log('converted Date - ', item[x]);
            //   }
            // }
            // console.log('new item', item);
            return Object.values(item).join(hash);
          })
          .join(this.dataSeparatorService.Pipe);

      // }
      var param = {
        moduleString: option
      }
      const _urlBulkUpload: string = webApi.domain + webApi.url.bulkUploadSession;
      this.commonservice.httpPostRequest(_urlBulkUpload, param).then(res => {
        console.log("res", res)
        this.spinner.hide()
        if (res['type'] == true) {
          var result = res['output']
          for (let i = 0; i < result.length; i++) {
            if (result[i].isValid == 0) {
              this.usersErrorsData['status'] = "Invalid";
              this.usersErrorsData.push(result[i]);
            }
            if (result[i].isValid == 1) {
              this.resultData.push(result[i]);
            }
            this.dupResultData = this.resultData
          }

          this.validCount = this.resultData.length
          this.InvalidCount = this.usersErrorsData.length
          this.getErrorSessionData()
          this.uploadedData = []
          console.log(this.resultData, "resukjkk")
          console.log(option, "separator")
          console.log(typeof (option))
          var event = 2
          this.onSelectTab(event, "jnhhh")
        } else {
          this.spinner.hide()
          this.presentToast('warning', 'Something went wrong')
        }

      })

    }
    else {
      this.spinner.hide()
      this.presentToast('warning', 'Invalid File')
    }

  }
  getErrorData() {
    var count = 0
    // var dummy = {}
    var dummarray = []
    this.usersErrorsData.forEach(element => {
      var dummy = {}
      dummy[this.dummyBulkArray[0]] = element.batchCode
      dummy[this.dummyBulkArray[1]] = element.batchName
      dummy[this.dummyBulkArray[2]] = element.categoryCode
      dummy[this.dummyBulkArray[3]] = element.programCode
      dummy[this.dummyBulkArray[4]] = element.visibility
      dummy[this.dummyBulkArray[5]] = element.coursePicRef
      dummy[this.dummyBulkArray[6]] = element.summary
      dummy[this.dummyBulkArray[7]] = element.tags
      dummy[this.dummyBulkArray[8]] = element.batchLevel
      dummy[this.dummyBulkArray[9]] = element.batchCreator
      dummy[this.dummyBulkArray[10]] = element.role
      dummy[this.dummyBulkArray[11]] = element.creaditType
      dummy[this.dummyBulkArray[12]] = element.beforeComp
      dummy[this.dummyBulkArray[13]] = element.afterComp
      dummy[this.dummyBulkArray[14]] = element.batchLocation
      dummy[this.dummyBulkArray[15]] = element.batchVenue
      dummy[this.dummyBulkArray[16]] = element.batchStartDate
      dummy[this.dummyBulkArray[17]] = element.batchEndDate
      dummy['Status'] = element.status
      dummy['Reason'] = element.reason

      dummarray.push(dummy)
      console.log(dummarray, "hvhvv")


    })
    this.usersErrorsData = dummarray


  }

  getErrorSessionData() {
    var count = 0
    var dummy = {}
    var dummarray = []
    this.usersErrorsData.forEach(element => {
      var dummy = {}
      dummy[this.dummyBulkSessionArray[0]] = element.batchCode
      dummy[this.dummyBulkSessionArray[1]] = element.sessionCode
      dummy[this.dummyBulkSessionArray[2]] = element.sessionName
      dummy[this.dummyBulkSessionArray[3]] = element.modulrPicRef
      dummy[this.dummyBulkSessionArray[4]] = element.summary
      dummy[this.dummyBulkSessionArray[5]] = element.startDate
      dummy[this.dummyBulkSessionArray[6]] = element.EndDate
      dummy[this.dummyBulkSessionArray[7]] = element.requestForNomination
      dummy[this.dummyBulkSessionArray[8]] = element.trainerCode
      dummy[this.dummyBulkSessionArray[9]] = element.visibility

      dummy['Status'] = element.status
      dummy['Reason'] = element.reason

      dummarray.push(dummy)
      console.log(dummarray, "hvhvv")


    })
    this.usersErrorsData = dummarray
  }

  exportToExcel() {
    var fileName = 'Invalid File'
    if (this.parentTabEvent.tabTitle == 'Batch') {
      fileName = 'Invalid Batch File'
    }
    else {
      fileName = 'Invalid Session File'
    }
    this.exportService.exportAsExcelFile(this.usersErrorsData, fileName);
  }

  cancelFile() {
    this.fileName = 'Click here to upload an excel file to  upload batches in bulk'
    this.file = ''
  }

  searchBatch(event) {
    if (this.param.tabId != 2) {
      var searchtext
      this.header.searchtext = event.target.value
      console.log(searchtext);
      searchtext = event.target.value;
      const val = searchtext.toLowerCase();
      if (val.length >= 3 || val.length == 0) {
        const tempcc = this.result.filter(function (d) {
          return String(d['Batch Name']).toLowerCase().indexOf(val) !== -1 ||
            d['Batch Code'].toLowerCase().indexOf(val) !== -1 ||
            !val
        });
        console.log(tempcc);
        this.uploadedData = tempcc;
        // if(this.sessionList.length == 0){
        //   this.noData = true
        // }else{
        //   this.noData = false
        // }
      }
    }
    else {
      var searchtext
      this.header.searchtext = event.target.value
      console.log(searchtext);
      searchtext = event.target.value;
      const val = searchtext.toLowerCase();
      if (val.length >= 3 || val.length == 0) {
        const tempcc = this.dupResultData.filter(function (d) {
          return String(d['batchName']).toLowerCase().indexOf(val) !== -1 ||
            d['batchCode'].toLowerCase().indexOf(val) !== -1 ||
            !val
        });
        console.log(tempcc);
        this.resultData = tempcc;
        // if(this.sessionList.length == 0){
        //   this.noData = true
        // }else{
        //   this.noData = false
        // }
      }



    }

  }

  searchSession(event) {
    if (this.param.tabId != 2) {
      var searchtext
      this.header.searchtext = event.target.value
      console.log(searchtext);
      searchtext = event.target.value;
      const val = searchtext.toLowerCase();
      if (val.length >= 3 || val.length == 0) {
        const tempcc = this.result.filter(function (d) {
          return String(d['Session Name']).toLowerCase().indexOf(val) !== -1 ||
            d['Session Code'].toLowerCase().indexOf(val) !== -1 ||
            !val
        });
        console.log(tempcc);
        this.uploadedData = tempcc;
        // if(this.sessionList.length == 0){
        //   this.noData = true
        // }else{
        //   this.noData = false
        // }



      }
    } else {
      var searchtext
      this.header.searchtext = event.target.value
      console.log(searchtext);
      searchtext = event.target.value;
      const val = searchtext.toLowerCase();
      if (val.length >= 3 || val.length == 0) {
        const tempcc = this.dupResultData.filter(function (d) {
          return String(d['sessionName']).toLowerCase().indexOf(val) !== -1 ||
            d['sessionCode'].toLowerCase().indexOf(val) !== -1 ||
            !val
        });
        console.log(tempcc);
        this.resultData = tempcc;
        // if(this.sessionList.length == 0){
        //   this.noData = true
        // }else{
        //   this.noData = false
        // }
      }


    }

  }
  onSelectFile(event) {
    var validExts

    validExts = new Array(
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel",
    )

    var fileType = event.target.files[0].type;
    var fileExt = false;
    for (let i = 0; i < validExts.length; i++) {
      if (fileType.includes(validExts[i])) {
        // return true;
        fileExt = true;
        break;
      }
    }
    // if(validExts.indexOf(fileType) < 0) {
    if (!fileExt) {


      this.presentToast('warning', 'Please select a valid file type');

    } else {
      this.file = event.target.files[0]
      this.fileName = event.target.files[0].name
      this.fileSelected = true
      this.xlsxToJsonService.processFileToJson({}, this.file).subscribe(data => {
        // this.xlsxToJsonService.convertXlsxToJson(file).subscribe(data => {
        this.resultSheets = Object.getOwnPropertyNames(data['sheets']);
        console.log('File Property Names ', this.resultSheets);
        let sheetName = this.resultSheets[0];
        this.result = data['sheets'][sheetName];

        if (this.result.length > 0) {
          this.uploadedData = this.result;
        }
        console.log(this.uploadedData, "uploaddeDAta")
        // this.result = JSON.stringify(data['sheets'].Sheet1);
      })

      // this.file =file
      for (let i = 0; i < event.target.files.length; i++) {

        // upload file using path
        var reader = new FileReader();
        reader.onload = (event: any) => {
          // console.log(event.target.result);

        }
        reader.readAsDataURL(event.target.files[i]);
      }
      // console.log(path,"path")
    }
    // this.convertDate(this.uploadedData)
  }
  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else if (type === 'info') {
      this.toastr.info(body, 'Success', {
        closeButton: false
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }
  back() {
    // this.router.navigate([''])
    this.router.navigate(['/pages/blended-home/blended-category/bulk-batch']);

  }
  backBatch() {
    this.router.navigate(['/pages/blended-home/blended-courses']);
  }

    // convertDate(date) {
    //   for(let i = 0;i<this.uploadedData.length;i++){
    //     if(this.uploadedData[i]['Batch Start Date']){
    //       if(this.uploadedData[i]['Batch Start Date'].includes('/')){
    //         this.uploadedData[i]['Batch Start Date'].replace('/','-')
    //       }
    //     }
    //     if(this.uploadedData[i]['Batch End Date']){
    //       if(this.uploadedData[i]['Batch End Date'].includes('/')){
    //         this.uploadedData[i]['Batch End Date'].replace('/','-')
    //       }
    //     }
    //     if(this.uploadedData[i]['Session Start Date']){
    //       if(this.uploadedData[i]['Session Start Date'].includes('/')){
    //         this.uploadedData[i]['Session Start Date'].replace('/','-')
    //       }
    //     }
    //     if(this.uploadedData[i]['Session End Date']){
    //       if(this.uploadedData[i]['Session End Date'].includes('/')){
    //         this.uploadedData[i]['Session End Date'].replace('/','-')
    //       }
    //     }
    //   }
    // }

}
