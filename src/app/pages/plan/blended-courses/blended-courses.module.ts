import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';


import { MyDatePickerModule } from 'mydatepicker';
import { FilterPipeModule  } from 'ngx-filter-pipe';
import { TagInputModule } from 'ngx-chips';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';



import { AddEditBlendCourseContentModule } from './addEditCourseContent/addEditCourseContent.module';
import { BlendedCoursesComponent } from './blended-courses.component';
import { BlendedService } from './blended.service';
import { BlendedHomeComponent } from './blended-home/blended-home.component';
import { BlendedCategoryModule } from './blended-category/blended-category.module';
import { BlendedProgramModule } from './blended-program/blended-program.module';
import { AddEditBlendCourseContentService } from './addEditCourseContent/addEditCourseContent.service';
import { NominationInstanceModule } from './nomination-instance/nomination-instance.module';
import { EepWorkflowModule } from './eep-workflow/eep-workflow.module';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgxContentLoadingModule } from 'ngx-content-loading';
import { ComponentModule } from '../../../component/component.module';
import { BulkBatchComponent } from './bulk-batch/bulk-batch.component';
// import { TabsModule } from 'ngx-tabs';
import { TabsModule } from 'ngx-tabset';

import { NbTabsetModule } from '@nebular/theme';
import { BatchUploadComponent } from './batch-upload/batch-upload.component';

// import { DetailsComponent } from './nomination-instance/add-edit-nomination/details/details.component';
// import { NomineesComponent } from './nomination-instance/add-edit-nomination/nominees/nominees.component';
// import { CalendarComponent } from './nomination-instance/add-edit-nomination/calendar/calendar.component';
// import { ContentComponent } from './nomination-instance/add-edit-nomination/content/content.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TabsModule,
    NbTabsetModule,
    MyDatePickerModule,
    FilterPipeModule,
    TagInputModule,
    AddEditBlendCourseContentModule,
    AngularMultiSelectModule,
    BlendedCategoryModule,
    BlendedProgramModule,
    NominationInstanceModule,
    TooltipModule.forRoot(),
    EepWorkflowModule,
    NgxSkeletonLoaderModule,
    ComponentModule.forRoot(),
  ],
  declarations: [
    BlendedCoursesComponent,
    BlendedHomeComponent,
    BulkBatchComponent,
    BatchUploadComponent,

    // NominationInstanceComponent,
    // DetailsComponent,
    // NomineesComponent,
    // CalendarComponent,
    // ContentComponent,
    // BlendedCategoryComponent,
    // BlendedProgramComponent
  ],
  providers: [
    BlendedService,
    AddEditBlendCourseContentService,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA,
  ]
})

export class BlendedCoursesModule {}
