import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlendedProgramComponent } from './blended-program.component';

describe('BlendedProgramComponent', () => {
  let component: BlendedProgramComponent;
  let fixture: ComponentFixture<BlendedProgramComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlendedProgramComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlendedProgramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
