import {Injectable,Inject} from '@angular/core';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppConfig} from '../../../../app.module';

import { webAPIService } from '../../../../service/webAPIService'
import { webApi } from '../../../../service/webApi'

@Injectable()
export class BlendedProgramService{
    // private _url:string = "/api/edge/category/getCategory";
    // private _url:string = "http://13.233.171.126:9845/api/web/getallcat";
    private _url:string = webApi.domain + webApi.url.getCategories;
     private _urlprogram:string = webApi.domain + webApi.url.getprograms;


    public data:any;

    // private _urlDisableCategory:string = webApi.domain + webApi.url.disableCategory;
    // public param:any = {
    //     tId: 1
    // };

    constructor(@Inject('APP_CONFIG_TOKEN') private config:AppConfig,private _http:Http){

    }
    
    getCategories(data){
        let url:any = this._url;
        return this._http.post(url,data)
            .map((response:Response)=>response.json())
            .catch(this._errorHandler);
    }
     getPrograms(data){
        let url:any = this._urlprogram;
        return this._http.post(url,data)
            .map((response:Response)=>response.json())
            .catch(this._errorHandler);
    }

    // disableCategory(visibleData){
    //     let url:any = this._urlDisableCategory;
    //     return this._http.post(url,visibleData)
    //         .map((response:Response)=>response.json())
    //         .catch(this._errorHandler);
    // }

    _errorHandler(error: Response){
        console.error(error);
        return Observable.throw(error || "Server Error")
      }
}