import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FilterPipeModule  } from 'ngx-filter-pipe';
import { BlendedProgramComponent } from './blended-program.component';
// import { CategoryService } from './category.service';
import { BlendedProgramService } from './blended-program.service';
import { BlendedService } from '../blended.service';
import { AddEditProgramBlendedModule } from '../blended-program/addEditProgramBlended/addEditProgramBlended.module';
import { AddEditProgramBlendedService } from '../blended-program/addEditProgramBlended/addEditProgramBlended.service';
import { NotFoundProgramComponent } from './not-found/not-found.component';
import { ThemeModule } from '../../../../@theme/theme.module';
import { TagInputModule } from 'ngx-chips';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgxContentLoadingModule } from 'ngx-content-loading';
import { ComponentModule } from '../../../../component/component.module';


// import { MiscellaneousRoutingModule, routedComponents } from './miscellaneous-routing.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FilterPipeModule,
    ThemeModule,
    TagInputModule,
    AddEditProgramBlendedModule,
    NgxSkeletonLoaderModule,
    NgxContentLoadingModule,
    ComponentModule,
  ],
  declarations: [
    BlendedProgramComponent,
    NotFoundProgramComponent
    // NotFoundComponent
  ],
  providers: [
    BlendedProgramService,
    AddEditProgramBlendedService,
    BlendedService
  ]
})

export class BlendedProgramModule {}
