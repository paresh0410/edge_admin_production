import { Component,ViewEncapsulation,ElementRef, ViewChild  } from '@angular/core';
import { Router, NavigationStart, Routes, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
// import { CategoryService } from './category.service';
import { BlendedProgramService } from './blended-program.service';
import { NotFoundProgramComponent } from './not-found/not-found.component';
import { AddEditProgramBlendedService } from '../blended-program/addEditProgramBlended/addEditProgramBlended.service';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { ToastrService } from 'ngx-toastr';
import { BlendedService } from '../blended.service';
import { webAPIService } from '../../../../service/webAPIService'
import { webApi } from '../../../../service/webApi'
import { NgxSpinnerService } from 'ngx-spinner';
import { from } from 'rxjs';
import { CommonFunctionsService } from '../../../../service/common-functions.service';
import { Card } from '../../../../models/card.model';
import { SuubHeader } from '../../../components/models/subheader.model';
import { noData } from '../../../../models/no-data.model';
@Component({
  selector: 'ngx-blended-program',
  templateUrl: './blended-program.component.html',
  styleUrls: ['./blended-program.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class BlendedProgramComponent  {
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  deleteCategoryModal:boolean;
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No Programs at this time.",
    desc:"Programs are defined by the admins. Programs are channels which help classify the courses.",
    titleShow:true,
    btnShow:true,
    descShow:true,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/learning-how-to-create-a-program',
  }
  header: SuubHeader  = {
    title:'Programs',
    btnsSearch: true,
    searchBar: true,
    catDropdown:true,
    placeHolder:"Search by program name",
    optionValue : 'All Categories',
    dropId:'categoryId',
    dropName:'categoryName',
    dropdownlabel: ' ',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: '',
    btnName3: '',
    btnAdd: 'Add Program',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: true,
    filter: false,
    showBreadcrumb: true,
    breadCrumbList:[
     
      {
        'name': 'Blended Learning',
        'navigationPath': '/pages/blended-home',
      },
    ]
  };

  count:number=8
  // public visibility:any = [
  //   { value: 0, name: 'Hide'},
  //   { value: 1, name: 'Show'}
  // ];

  cardModify: Card = {
    flag: 'program',
    titleProp : 'programName',
    discrption: 'description',
    image: 'progPicRef',
    hoverlable:   true,
    showImage: true,
    option:   true,
    eyeIcon:   true,
    courseCompletion: '',
    showCourseCompletion: true,
  
    bottomDiv:   true,
    bottomDiscription:   true,
    
    bottomTitle:   true,
    btnLabel: 'Edit',
    defaultImage:'assets/images/category.jpg'
  };
  categories="";
  errorMsg:any;
  notFound :boolean=false;

  public category:any = [
    { categoryName:'Test Caategory', categoryCode: 1234},
    { categoryName:'Media Category', categoryCode: 5678},
    { categoryName:'Demo Category', categoryCode: 9123},
    { categoryName:'Abc Category', categoryCode: 4567}
  ];

  programs : any = [];
  userData:any;
  tenantId:any;
  search:any;
  skeleton=false;
  searchText: any;
  enable: boolean;
  title: string;
  enableDisableModal: boolean;
  progData: any;

  constructor(protected passService:BlendedService,
     protected categoryService:BlendedProgramService,
     protected addEditProgramService:AddEditProgramBlendedService,
     private commonFunctionService: CommonFunctionsService,
     private router:Router, private routes: ActivatedRoute,private spinner: NgxSpinnerService,protected webApiService:webAPIService, 
    //  private toasterService: ToasterService,
     private toastr: ToastrService) {

    this.search = {};
    
    if(localStorage.getItem('LoginResData')){
      this.userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', this.userData.data);
      // this.userId = this.userData.data.data.id;
      this.tenantId = this.userData.data.data.tenantId;
    }
      let param = {
        tId: this.tenantId
      } 
     this.getDropdownList(param);
     this.getCategories(param);
     this.getPrograms(param);
     this.getStandDropdownList(param);
   
  }

  visibility:any = [];
    getDropdownList(param){
      this.spinner.show();
      let url = webApi.domain + webApi.url.getVisibiltyDropdown;
      this.webApiService.getService(url,param)
      .then(rescompData => { 
        // this.loader =false;
        this.spinner.hide();
        var temp:any = rescompData;
        if(temp == "err"){
        }else{
          this.visibility = temp.data;
        }
        // console.log('Visibility Dropdown',this.visibility);
      },
      resUserError=>{
        // this.loader =false;
        this.spinner.hide();
        this.errorMsg = resUserError;
      });
    }

     standard:any = [];
    getStandDropdownList(param){
      this.spinner.show();

      /* this url is using from batch session for dropdown*/
      let url = webApi.domain + webApi.url.getModuleActivityDropBatch; 
      this.webApiService.getService(url,param)
      .then(rescompData => { 
        // this.loader =false;
        this.spinner.hide();
        var temp:any = rescompData;
        if(temp == "err"){
        }else{
          this.standard = temp.data.trainerStandard;
        }
        // console.log('Visibility Dropdown',this.visibility);
      },
      resUserError=>{
        // this.loader =false;
        this.spinner.hide();
        this.errorMsg = resUserError;
      });
    }


 getCategories(param){
      // this.spinner.show();
      let url = webApi.domain + webApi.url.getCategories;
    

      this.webApiService.getService(url,param)
      .then(rescompData => { 
        console.log(rescompData);
        // this.loader =false;
        this.spinner.hide();
        this.notFound = false;
        this.skeleton=true;
        var temp:any = rescompData;
        if(temp == "err"){
          this.notFound = true;
          this.skeleton=true;
        }else{
          this.category = temp.data;
        }
        // console.log('Category Result',rescompData)
      },
      resUserError=>{
        // this.loader =false;
        this.spinner.hide();
        if(resUserError.statusText == "Unauthorized"){
          this.router.navigate(['/login']);
        }
        this.errorMsg = resUserError;
        this.notFound = true;
      });
    }
tempprograms:any;
dropprograms:any;
    getPrograms(param){
      this.spinner.show();
      let url = webApi.domain + webApi.url.getprograms;
    

      this.webApiService.getService(url,param)
      .then(rescompData => { 
        console.log(rescompData);
        // this.loader =false;
        this.spinner.hide();
        this.notFound = false;
        var temp:any = rescompData;
        if(temp == "err"){
          this.notFound = true;
        }else{
          this.programs = temp.data;
          this.addItems(0,this.sum,'push',this.programs);
          this.tempprograms=temp.data;
          this.dropprograms=temp.data;
        }
        // console.log('Category Result',rescompData)
      },
      resUserError=>{
        // this.loader =false;
        this.spinner.hide();
        if(resUserError.statusText == "Unauthorized"){
          this.router.navigate(['/login']);
        }
        this.errorMsg = resUserError;
        this.notFound = true;
      });
    }
 

  content2(){
    this.router.navigate([''],{relativeTo:this.routes});
  }

  // disableCategory(currentIndex,data){
  //   console.log("Index--> ",currentIndex," Data-->", data);
  //   if(data.visibility.value == 1){
  //     data.visibility.value = 0;
  //     // data.visibility.name = "Show";
  //     console.log(data.visibility);
  //   }
  //   else{
  //     data.visibility.value = 1;
  //     // data.visibility.name = "Hide";
  //     console.log(data.visibility);
  //   }
  // }

  addeditprogram(data1, value){
    console.log(data1);
    var data={
        id:value,
        program:data1,
        visibility:this.visibility,
        category:this.category,
        standard:this.standard
    }
    if(value == 0){
        var action = "ADD";
        this.addEditProgramService.getdataAddEdit= data;
        this.router.navigate(['add-edit-program'],{relativeTo:this.routes});
    }
    else if( value == 1){
        var action = "EDIT";
        console.log('VISIBLITY--->',this.visibility);
         this.addEditProgramService.getdataAddEdit= data;
         this.addEditProgramService.callData.programId = data1.programId;
        this.addEditProgramService.callData.standardId = data1.standardId;
        this.router.navigate(['add-edit-program'],{relativeTo:this.routes});
    }
  }

  gotoCardDetails(data1) {
    var data={
      id:1,
      program:data1,
      visibility:this.visibility,
      category:this.category,
      standard:this.standard
    }
    var action = "EDIT";
      console.log('VISIBLITY--->',this.visibility);
       this.addEditProgramService.getdataAddEdit= data;
       this.addEditProgramService.callData.programId = data1.programId;
      this.addEditProgramService.callData.standardId = data1.standardId;
      this.router.navigate(['add-edit-program'],{relativeTo:this.routes});
  
  }
  searchQuestion(event) {
    var val = event.target.value.toLowerCase();

    // filter our data
    var temp = this.tempprograms.filter(function(d) {
      return String(d.programName).toLowerCase().indexOf(val) !== -1 ||
      // String(d.qtype).toLowerCase().indexOf(val) !== -1 || 
      // String(d.qinstru).toLowerCase() === val || 
      // String(d.qfeed).toLowerCase().indexOf(val) !== -1 ||  
      !val;
    });

    // update the rows
    this.programs = temp;
    // Whenever the filter changes, always go back to the first page
    this.programs.offset = 0;
  }
    doropserch(catData){
      
      this.displayArray=[];
      // if(catData == '')
      if(!catData)
       {
          //this.programs =this.dropprograms;
          let param = {
            tId: this.tenantId
          } 
          // this.addItems(0,this.sum,'push',this.programs);
           this.getPrograms(param);
          // this.sum =50;
          // this.addItems(0,this.sum,'push',this.programs);
       }else{
        var tempdata =[];
          for(let i=0;i<this.dropprograms.length;i++)
          {
            if(this.dropprograms[i].categoryId == catData)
            {
              tempdata.push(this.dropprograms[i]);
            }
            this.sum =50;
            this.programs = tempdata;
          }
          if(this.programs==''){
            // this.toastr.warning("No Data Available",'Warning');
            this.notFound = true;
            // this.sum =50;
            // let param = {
            //   tId: this.tenantId
            // } 
            // // this.addItems(0,this.sum,'push',this.programs);
            //  this.getPrograms(param);

          }else{
             this.notFound = false;

             this.addItems(0,this.sum,'push',this.programs);
          }
      }
  }

  visibiltyRes:any;
    disableQcate(i,item)
    {
         this.spinner.show();
    var visibilityData = {
      id : item.programId,
      visible : item.visible==1?0:1
    }  
let url = webApi.domain + webApi.url.disableProgram;
    this.webApiService.getService(url,visibilityData)
      .then(rescompData => { 
        // this.loader =false;
        this.spinner.hide();
        this.visibiltyRes = rescompData;
        // console.log('Category Visibility Result',this.visibiltyRes);
        if(this.visibiltyRes.type == false){
          // var catUpdate : Toast = {
          //     type: 'error',
          //     title: "Program",
          //     body: "Unable to update visibility of program.",
          //     showCloseButton: true,
          //     timeout: 2000
          // };
          // // this.closeEnableDisableCategoryModal();
          // this.toasterService.pop(catUpdate);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        }else{
          // var catUpdate : Toast = {
          //     type: 'success',
          //     title: "Program",
          //     body: this.visibiltyRes.data,
          //     showCloseButton: true,
          //     timeout: 2000
          // };
          // // this.closeEnableDisableCategoryModal();
          // this.toasterService.pop(catUpdate);

          this.toastr.success(this.visibiltyRes.data, 'Success', {
            closeButton: false
           });
            this.disablevaluechnage(i,item)
          // this.getCategories(this.catdata);
        }
      },
      resUserError=>{
        // this.loader =false;
        this.spinner.hide();
        if(resUserError.statusText == "Unauthorized"){
          this.router.navigate(['/login']);
        }
        this.errorMsg = resUserError;
        // this.closeEnableDisableCategoryModal();
      });
    }

    clickTodisable(item) {
      // this.spinner.show();
      var visibilityData = {
        id : item.programId,
        visible : item.visible
      }  

     
      let url = webApi.domain + webApi.url.disableProgram;
      this.webApiService.getService(url,visibilityData)
        .then(rescompData => { 
          // this.spinner.hide();
          this.visibiltyRes = rescompData;
          if(this.visibiltyRes.type == false){
            
            this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
              timeOut: 0,
              closeButton: true
            });
          }else{
           
  
            this.toastr.success(this.visibiltyRes.data, 'Success', {
              closeButton: false
             });
             
          }
        },
        resUserError=>{
          this.spinner.hide();
          if(resUserError.statusText == "Unauthorized"){
            this.router.navigate(['/login']);
          }
          this.errorMsg = resUserError;
        });
        this.closeEnableDisableModal()
    }
    
    Disbaledata:any;
    disablevaluechnage(i,item)
    {
      this.Disbaledata =item;
      this.Disbaledata.visible=this.Disbaledata.visible==1?0:1
      // this.programs[i].visible=this.programs[i].visible==1?0:1
    }



  clear(){
    if(this.searchText.length>=3){
    this.search={};
    this.displayArray =[];
    this.newArray = [];
    this.sum = 50;
    this.addItems(0, this.sum, 'push', this.programs);
    this.notFound=false
  }
  else{
    this.search={};
  }
  };

  back(){
    //this.router.navigate(['/pages/learning/blended-home']);
    window.history.back();
  }
  onsearch(event){
    this.search.programName = event.target.value

  }
  ngOnDestroy(){
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0){
      e1[0].classList.add('with-scroll');
    }
  }
  ngAfterContentChecked() {
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0)
    {
      e1[0].classList.remove('with-scroll');
    }

   }
   conentHeight = '0px';
   offset: number = 100;
   ngOnInit() {
     this.conentHeight = '0px';
     const e = document.getElementsByTagName('nb-layout-column');
     console.log(e[0].clientHeight);
     // this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
     if (e[0].clientHeight > 1300) {
       this.conentHeight = '0px';
       this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
     } else {
       this.conentHeight = String(e[0].clientHeight) + 'px';
     }
   }
 
   sum = 40;
   addItems(startIndex, endIndex, _method, asset) {
     for (let i = startIndex; i < endIndex; ++i) {
       if (asset[i]) {
         this.displayArray[_method](asset[i]);
       } else {
         break;
       }
 
       // console.log("NIKHIL");
     }
   }
   // sum = 10;
   onScroll(event) {
 
     let element = this.myScrollContainer.nativeElement;
     // element.style.height = '500px';
     // element.style.height = '500px';
     // Math.ceil(element.scrollHeight - element.scrollTop) === element.clientHeight
 
     if (element.scrollHeight - element.scrollTop - element.clientHeight < 5) {
       if (this.newArray.length > 0) {
         if (this.newArray.length >= this.sum) {
           const start = this.sum;
           this.sum += 50;
           this.addItems(start, this.sum, 'push', this.newArray);
         } else if ((this.newArray.length - this.sum) > 0) {
           const start = this.sum;
           this.sum += this.newArray.length - this.sum;
           this.addItems(start, this.sum, 'push', this.newArray);
         }
       } else {
         if (this.programs.length >= this.sum) {
           const start = this.sum;
           this.sum += 50;
           this.addItems(start, this.sum, 'push', this.programs);
         } else if ((this.programs.length - this.sum) > 0) {
           const start = this.sum;
           this.sum += this.programs.length - this.sum;
           this.addItems(start, this.sum, 'push', this.programs);
         }
       }
 
 
       console.log('Reached End');
     }
   }
   newArray =[];
   displayArray =[];
    // this function is use to dynamicsearch on table
  searchData(event) {
    var temData = this.programs;
    var val = event.target.value.toLowerCase();
    var keys = [];
    this.searchText=val;
    if(event.target.value == '')
    {
      this.displayArray =[];
      this.newArray = [];
      this.sum = 50;
      this.addItems(0, this.sum, 'push', this.programs);
    }else {
         // filter our data
    if (temData.length > 0) {
      keys = Object.keys(temData[0]);
    }
    if(val.length>=3 ||val.length==0){
    var temp = temData.filter((d) => {
      for (const key of keys) {
        if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
          return true;
        }
      }
    });
    if(temp.length==0){
      this.notFound=true
      this.noDataVal={
        margin:'mt-5',
        imageSrc: '../../../../../assets/images/no-data-bg.svg',
        title:"Sorry we couldn't find any matches please try again",
        desc:".",
        titleShow:true,
        btnShow:false,
        descShow:false,
        btnText:'Learn More',
        btnLink:''
      }
    }
    this.displayArray =[];
    this.newArray = temp;
    this.sum = 50;
  }

    // update the rows
  
    this.addItems(0, this.sum, 'push', this.newArray);
  }
}
btnName:string='Yes'
clickTodisableProgram(item, i) {

  if(this.displayArray[i].visible == 1) {
    this.displayArray[i].visible = 0
  } else {
    this.displayArray[i].visible = 1
  }

  this.clickTodisable(item);

    // if (item.visible == 1 ) {
    // this.title="Disable Program"
    //   this.enable = true;
    //   this.progData = item;
    //   this.enableDisableModal = true;
    // } else {
    // this.title="Enable Program"
    //   this.enable = false;
    //   this.progData = item;
    //   this.enableDisableModal = true;

    // }
}
enableDisableProgAction(actionType) {
  if (actionType == true) {

    if (this.progData.visible == 1) {
      var courseData = this.progData;
      this.clickTodisable(courseData);
    } else {
      var courseData = this.progData;
      this.clickTodisable(courseData);
    }
  } else {
    this.closeEnableDisableModal();
  }
}
closeEnableDisableModal() {
  this.enableDisableModal = false;
}
}