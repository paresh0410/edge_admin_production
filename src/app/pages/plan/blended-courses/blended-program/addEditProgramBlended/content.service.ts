import {Injectable, Inject} from '@angular/core';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppConfig} from '../../../../../app.module';
import { webApi } from '../../../../../service/webApi';
import { HttpClient } from "@angular/common/http";

@Injectable()
export class ContentService {

    

        private _urlGetAllCourses : string = webApi.domain + webApi.url.getallcoursesclassroom;
        private _urlEnrolCourseForCCContent : string = webApi.domain + webApi.url.enrolcourseforclassroom;
        private _urlGetAllEnrollCourseModule : string = webApi.domain + webApi.url.getallenrolcoursemoduleclassroom;
        private _urlRemoveEnrolCourse : string = webApi.domain + webApi.url.removeenrolcourseclassroom
        public courseId:any;
        public programId:any;
        public tenantId:any;
  constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private _http: Http,private _httpClient:HttpClient){
      //this.busy = this._http.get('...').toPromise();
  }

  getAllCoursesCC(param){
    return new Promise(resolve => {
      this._httpClient.post(this._urlGetAllCourses, param)
      //.map(res => res.json())
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}

enrollCourseForCCContent(param){
  return new Promise(resolve => {
    this._httpClient.post(this._urlEnrolCourseForCCContent, param)
    //.map(res => res.json())
    .subscribe(data => {
        resolve(data);
    },
    err => {
        resolve('err');
    });
});
}


removeEnrolCourse(param){
  return new Promise(resolve => {
    this._httpClient.post(this._urlRemoveEnrolCourse, param)
    //.map(res => res.json())
    .subscribe(data => {
        resolve(data);
    },
    err => {
        resolve('err');
    });
});
}

getAllEnrolCourseModule(param){
  return new Promise(resolve => {
    this._httpClient.post(this._urlGetAllEnrollCourseModule, param)
    //.map(res => res.json())
    .subscribe(data => {
        resolve(data);
    },
    err => {
        resolve('err');
    });
});
}

_errorHandler(error: Response){
  console.error(error);
  return Observable.throw(error || "Server Error")
}


}
