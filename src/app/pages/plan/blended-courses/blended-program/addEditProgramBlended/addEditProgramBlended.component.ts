import {
  Component,
  ViewEncapsulation,
  ViewChild,
  ChangeDetectorRef
} from "@angular/core";
import {
  Validators,
  FormGroup,
  FormBuilder,
  FormGroupDirective,
  NgForm
} from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
// import { ToasterModule, ToasterService, Toast } from "angular2-toaster";
import { ToastrService } from 'ngx-toastr';
import { AddEditProgramBlendedService } from "./addEditProgramBlended.service";
import { ContentService } from "./content.service";
import { webAPIService } from "../../../../../service/webAPIService";
import { webApi } from "../../../../../service/webApi";
import { NgxSpinnerService } from "ngx-spinner";
import { HttpClient } from "@angular/common/http";
import { SuubHeader } from "../../../../components/models/subheader.model";

@Component({
  selector: "addEditCategoryBlended",
  templateUrl: "./addEditProgramBlended.html",
  styleUrls: ["./addEditProgramBlended.scss"],
  encapsulation: ViewEncapsulation.None
})
export class AddEditProgramBlended {
  @ViewChild(NgForm) programForm: NgForm;
  visibility: any = [];
  category: any = [];
  // programForm:FormGroup;
  program: any = [];
  id: any;
  defaultThumb: any = "assets/images/category.jpg";
  tenantId: any;
  call = false;
  member: any = {
    programId: 0,
    programName: "",
    programCode: "",
    categoryId: "",
    description: "",
    progPicRef: "",
    duration: "",
    visible: 1,
    tenantId: this.tenantId,
    tags: "",
    usermodified: "",
    standardId: ""
  };
  standard: any = [];
  getDataForEdit: any = [];
  addAction: boolean = false;
  userId: any;
  errorMsg: any;
  valid :boolean= true
  // call: boolean = false;
  callData: any = {
    'programId': 0,
    'standardId': 0,
  };
  settingsTagDrop ={};
  tempTags:any =[];
  tagList:any = [];
  selectedTags:any = [];
  isData: any;
  isdata: any='';
  constructor(
    private spinner: NgxSpinnerService,
    protected webApiService: webAPIService,
    protected addEditProgService: AddEditProgramBlendedService,
    public cdf: ChangeDetectorRef,
    // private toasterService: ToasterService,
    private ContentService: ContentService,
    private router: Router,
    private route: ActivatedRoute,
    private formbuilder: FormBuilder,
    private http1: HttpClient,
    private toastr: ToastrService,

  ) {
    // this.loader = true;
    this.settingsTagDrop = {
      badgeShowLimit: 4,
      text: 'Select Tags',
      singleSelection: false,
      classes: 'common-multi ',
      primaryKey: 'id',
      labelKey: 'name',
      noDataLabel: 'Search Tags...',
      enableSearchFilter: true,
      searchBy: ['name'],
      maxHeight:250,
      lazyLoading: true,
    };
    this.getDataForEdit = this.addEditProgService.getdataAddEdit;
    if (this.getDataForEdit == undefined) {
      this.router.navigate(["/pages/learning/blended-home/blended-programs"]);
    } else {
      console.log("this.getDataForEdit", this.getDataForEdit);
      if (localStorage.getItem("LoginResData")) {
        var userData = JSON.parse(localStorage.getItem("LoginResData"));
        console.log("userData", userData.data);
        this.userId = userData.data.data.id;
        this.tenantId = userData.data.data.tenantId;
      }
      this.category = this.getDataForEdit.category;
      this.visibility = this.getDataForEdit.visibility;
      this.standard = this.getDataForEdit.standard;
      console.log("program data=====================>",this.getDataForEdit)
      if (this.getDataForEdit.program != undefined) {
        this.program = this.getDataForEdit.program;
        this.id = this.getDataForEdit.id;
      } else {
        this.id = 0;
      }
      this.getallTagList(this.id,this.program);
      this.makeCategoryDataReady(this.id,this.program)
      this.getHelpContent();
    }
  }

  ngOnInit() {
    // this.programForm= this.formbuilder.group({
    //   programName:['', Validators.required],
    //   // programCode: ['', Validators. required],
    //   categoryId: ['', Validators. required],
    //   // categoryCode: ['', Validators. required],
    //   visible: ['', Validators. required],
    //   // category: ['', Validators. required],
    //   description: ['', Validators. required],
    //   // tags: ['', Validators. required],
    //   duration: ['', Validators. required],
    //   standardId: ['', Validators. required],
    // })
  }
  getallTagList(id,program) {
    this.spinner.show();
    let url = webApi.domain + webApi.url.getAllTagsComan;
    let param = {
      tenantId :this.tenantId
    };
    this.webApiService.getService(url, param)
      .then(rescompData => {
        // this.loader =false;
        this.spinner.hide();
        var temp: any = rescompData;
        this.tagList = temp.data;
        this.tempTags = [... this.tagList];
        this.makeCategoryDataReady(id, program);
        // console.log('Visibility Dropdown',this.visibility);
      },
        resUserError => {
          // this.loader =false;
          this.show =false;
          this.spinner.hide();
          this.errorMsg = resUserError;
        });
  }
  
  show:boolean =false;
  makeCategoryDataReady(id,program){
    this.spinner.show()
    if (id != 0) {
      this.member = {
        programId: program.programId,
        courseId: program.courseId,
        programName: program.programName,
        programCode: program.programCode,
        categoryId: program.categoryId == 0?'':program.categoryId,
        description: program.description,
        progPicRef: program.progPicRef
          ? program.progPicRef
          : this.defaultThumb,
        duration: program.duration,
        visible: program.visible,
        tenantId: program.tenantId,
        tags: program.tags
          ? program.tags.split(",")
          : program.tags,
        usermodified: this.userId,
        standardId: program.standardId,
        isWfPgm: program.isWorkflowProgram == 1 ? true : false,
      };
      if(program.tagIds)
            {
              var tagIds =program.tagIds.split(',');
              if(tagIds.length > 0){
                this.tempTags.forEach((tag) => {
                  tagIds.forEach((tagId)=>{
                    if (tag.id == tagId ) {
                      this.selectedTags.push(tag);
                    }
                  });
                });
              }
                }
            console.log(this.selectedTags,tagIds,'this.selectedTags')
      // this.addEditProgService.callData.programId = this.member.programId;
      // this.addEditProgService.callData.standardId = this.member.standard;
      console.log("this.member", this.member);
    } else {
      this.member = {
        programId: 0,
        courseId: 0,
        programName: "",
        programCode: "",
        categoryId: "",
        description: "",
        progPicRef: this.defaultThumb,
        duration: "",
        visible: 1,
        tenantId: this.tenantId,
        tags: "",
        usermodified: this.userId,
        standardId: "",
        isWfPgm: false,
      };
    }   
        this.show =true;
        this.spinner.hide();
        this.datapass();
      }


  categoryImgData: any;
  readCategoryThumb(event: any) {
    var validExts = new Array(".png", ".jpg", ".jpeg");
    var fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf("."));
    if (validExts.indexOf(fileExt) < 0) {
      // var toast: Toast = {
      //   type: "error",
      //   title: "Invalid file selected!",
      //   body: "Valid files are of " + validExts.toString() + " types.",
      //   showCloseButton: true,
      //   // tapToDismiss: false,
      //   timeout: 2000
      //   // onHideCallback: () => {
      //   //     this.router.navigate(['/pages/plan/users']);
      //   // }
      // };
      // this.toasterService.pop(toast);

      this.toastr.warning('Valid file types are ' + validExts.toString(), 'Warning', {
        closeButton: false
      })
      // this.deleteCourseThumb();
    } else {
      if (event.target.files && event.target.files[0]) {
        this.categoryImgData = event.target.files[0];

        var reader = new FileReader();

        reader.onload = (event: ProgressEvent) => {
          // this.defaultThumb = (<FileReader>event.target).result;
          this.member.progPicRef = (<FileReader>event.target).result;
        };
        reader.readAsDataURL(event.target.files[0]);
      }
    }
  }

  deleteCategoryThumb() {
    // this.defaultThumb = 'assets/images/category.jpg';
    this.member.progPicRef = this.defaultThumb;
    this.categoryImgData = undefined;
    this.member.categoryPicRefs = undefined;
  }

  onSubmit(data, programForm: NgForm) {
    console.log(programForm.value);
    // console.log(data);

    this.programForm.resetForm();
  }

  back() {
    window.history.back();
    // this.router.navigate(["/pages/learning/blended-home/blended-programs"]);
  }

  fileUploadRes: any;
  categoryAddEditRes: any;
  // res:any;
  addUpdateProgram(url, program) {
    this.spinner.show();
    this.webApiService.getService(url, program).then(
      rescompData => {
        this.spinner.hide();
        var temp: any = rescompData;
        console.log('temp', temp);
        this.categoryAddEditRes = temp.data;
        if (temp == "err") {
          // this.notFound = true;
          // var catUpdate: Toast = {
          //   type: "error",
          //   title: "Program",
          //   body: "Unable to update Program.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(catUpdate);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        } else if (temp.type == false) {
          // var catUpdate: Toast = {
          //   type: "error",
          //   title: "Program",
          //   body: this.categoryAddEditRes.msg,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(catUpdate);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        } else {
          if (this.categoryAddEditRes) {
            console.log('this.programAddEditRes', this.categoryAddEditRes);
            this.member.programId = this.categoryAddEditRes.id;
            this.ContentService.programId = this.categoryAddEditRes.id;
            this.addEditProgService.callData.programId = this.categoryAddEditRes.id;
            this.addEditProgService.callData.standardId = this.categoryAddEditRes.standardId;
            //this.call = true;
            // this.callData.programId = this.categoryAddEditRes.id;
            // this.callData.programId = this.categoryAddEditRes.standardId;
            // this.timewoutfun (this.categoryAddEditRes.id, this.categoryAddEditRes.standardId);
          }

          this.header = {
            title:this.getDataForEdit.program?this.member.programName:'Add Program ',
            btnsSearch: true,
            btnName2: '',
            btnName2show: false,
            btnBackshow: true,
            showBreadcrumb: true,
            breadCrumbList:[
              // {
              //   'name': 'Learning',
              //   'navigationPath': '/pages/learning',
              // },
              {
                'name': 'Blended Learning',
                'navigationPath': '/pages/blended-home',
              },
              {
                'name': 'Programs',
                'navigationPath': '/pages/blended-home/blended-programs',
              },
            ]
          };
          this.contentTab = true;
          this.Details = false;
          this.callTab = false;
          // this.router.navigate(['/pages/learning/blended-home/blended-programs']);
          // var catUpdate: Toast = {
          //   type: "success",
          //   title: "Program",
          //   // body: "Unable to update category.",
          //   body: this.categoryAddEditRes.msg,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(catUpdate);


          this.toastr.success(this.categoryAddEditRes.msg, 'Success', {
            closeButton: false
          });
        }
        this.datapass();
        // this.res =this.categoryAddEditRes.id;
        console.log("Category AddEdit Result ", this.categoryAddEditRes);
      },
      resUserError => {
        this.spinner.hide();
        // this.loader =false;
        this.errorMsg = resUserError;
      }
    );
  }


  submit(f) {
    if(this.selectedTags.length  == 0){
      this.valid = false
    }
    else{
      this.valid = true
    }
    if (f.valid && this.valid == true) {
      console.log('this.member', this.member);
      this.spinner.show();
      if (this.selectedTags.length > 0) {
        this.makeTagDataReady(this.selectedTags);
         // this.formdata.tags = this.formattedTags;
       }
      const program = {
        programId: this.member.programId,
        programName: this.member.programName,
        programCode: this.member.programCode,
        categoryId: this.member.categoryId,
        description: this.member.description,
        progPicRef: this.member.progPicRef,
        duration: this.member.duration,
        visible: this.member.visible,
        tenantId: this.member.tenantId,
        tags: this.member.tags,
        usermodified: this.userId,
        standardId: this.member.standardId,
        isWfPgm: this.member.isWfPgm ? 1 : 0,
      };

      console.log("program", program);
      var fd = new FormData();
      fd.append("content", JSON.stringify(program));
      fd.append("file", this.categoryImgData);
      console.log("File Data ", fd);

      console.log("Category Data Img", this.categoryImgData);
      console.log("Category Data ", program);

      const url = webApi.domain + webApi.url.addEditPrograms;
      const fileUploadUrl = webApi.domain + webApi.url.fileUpload;

      if (this.categoryImgData != undefined) {
        this.webApiService.getService(fileUploadUrl, fd).then(
          rescompData => {
            this.spinner.hide();
            const temp: any = rescompData;
            this.fileUploadRes = temp;
            if (temp == "err") {
              // this.notFound = true;
              // const thumbUpload: Toast = {
              //   type: "error",
              //   title: "Program Thumbnail",
              //   body: "Unable to upload Program thumbnail.",
              //   // body: temp.msg,
              //   showCloseButton: true,
              //   timeout: 2000
              // };
              // this.toasterService.pop(thumbUpload);

              this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
                timeOut: 0,
                closeButton: true
              });
            } else if (temp.type == false) {
              // const thumbUpload: Toast = {
              //   type: "error",
              //   title: "Program Thumbnail",
              //   body: "Unable to upload Program thumbnail.",
              //   // body: temp.msg,
              //   showCloseButton: true,
              //   timeout: 2000
              // };
              // this.toasterService.pop(thumbUpload);

              this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
                timeOut: 0,
                closeButton: true
              });
            } else {
              if (
                this.fileUploadRes.data != null ||
                this.fileUploadRes.fileError != true
              ) {
                program.progPicRef = this.fileUploadRes.data.file_url;
                this.addUpdateProgram(url, program);
              } else {
                // var thumbUpload: Toast = {
                //   type: "error",
                //   title: "Program Thumbnail",
                //   // body: "Unable to upload category thumbnail.",
                //   body: this.fileUploadRes.status,
                //   showCloseButton: true,
                //   timeout: 2000
                // };
                // this.toasterService.pop(thumbUpload);

                this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
                  timeOut: 0,
                  closeButton: true
                });
              }
            }
            console.log("File Upload Result", this.fileUploadRes);
          },
          resUserError => {
            this.spinner.hide();
            // this.loader =false;
            this.errorMsg = resUserError;
          }
        );
      } else {
        this.addUpdateProgram(url, program);
      }
    } else {
      // console.log('Please Fill all fields');
      // var thumbUpload: Toast = {
      //   type: 'error',
      //   title: 'Unable to update',
      //   body: 'Please Fill all fields',
      //   showCloseButton: true,
      //   timeout: 2000
      // };
      // this.toasterService.pop(thumbUpload);

      this.toastr.warning('Please fill in the required fields', 'Warning', {
        closeButton: false
      })
      Object.keys(f.controls).forEach(key => {
        f.controls[key].markAsDirty();
      });
    }

  }

  contentTab: boolean = false;
  Details: boolean = true;
  callTab: boolean = false;
  header: SuubHeader;  

  selectedTab(tabEvent) {
    // this.spinner.show();
    console.log("tab Selected", tabEvent);

    if (tabEvent.tabTitle == "Content") {
      this.spinner.hide()
      this.header = {
        title:this.getDataForEdit.program?this.member.programName:'Add Program ',
        btnsSearch: true,
        btnName2: '',
        btnName2show: false,
        btnBackshow: true,
        showBreadcrumb: true,
        breadCrumbList:[
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          {
            'name': 'Blended Learning',
            'navigationPath': '/pages/blended-home',
          },
          {
            'name': 'Programs',
            'navigationPath': '/pages/blended-home/blended-programs',
          },
        ]
      };
      this.contentTab = true;
      this.spinner.show();
      setTimeout(() => {
        this.spinner.hide()
      }, 2000);
      this.Details = false;
      this.callTab = false;
    } else if (tabEvent.tabTitle == "Details") {
      this.spinner.hide()
      this.header = {
        title:this.getDataForEdit.program?this.getDataForEdit.program.programName:'Add Program ',
        btnsSearch: true,
        btnName2: 'Save',
        btnName2show: true,
        btnBackshow: true,
        showBreadcrumb: true,
        breadCrumbList:[
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          {
            'name': 'Blended Learning',
            'navigationPath': '/pages/blended-home',
          },
          {
            'name': 'Programs',
            'navigationPath': '/pages/blended-home/blended-programs',
          },
        ]
      };
      this.contentTab = false;
      this.spinner.show();
      setTimeout(() => {
        this.spinner.hide()
      }, 2000);
      this.Details = true;
      this.callTab = false;
    } else if (tabEvent.tabTitle == "Call") {
      this.spinner.hide()
      this.header = {
        title:this.getDataForEdit.program?this.member.programName:'Add Program ',
        btnsSearch: true,
        btnName3: 'Add Call',
        btnName3show: true,
        btnBackshow: true,
        showBreadcrumb: true,
        breadCrumbList:[
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          {
            'name': 'Blended Learning',
            'navigationPath': '/pages/blended-home',
          },
          {
            'name': 'Programs',
            'navigationPath': '/pages/blended-home/blended-programs',
          },
        ]
      };
      this.spinner.show();
      setTimeout(() => {
        this.spinner.hide()
      }, 2000);
      // this.timewoutfun ( this.addEditProgService.callData.programId,  this.addEditProgService.callData.standardId);
      this.callData.standardId = this.addEditProgService.callData.standardId;
      this.callData.programId = this.addEditProgService.callData.programId;
      this.cdf.detectChanges();
      this.call = true;
      this.callTab = true;
      this.contentTab = false;
      this.Details = false;
    }

  }
 
  setheader(val){
    this.isdata=val
    this.header = {
      title:this.getDataForEdit.program?this.member.programName:'Add Program ',
      btnsSearch: true,
      btnName3: 'Add Call',
      btnName3show: true,
      btnBackshow: true,
      showBreadcrumb: true,
      breadCrumbList:[
        // {
        //   'name': 'Learning',
        //   'navigationPath': '/pages/learning',
        // },
        {
          'name': 'Blended Learning',
          'navigationPath': '/pages/blended-home',
        },
        {
          'name': 'Programs',
          'navigationPath': '/pages/blended-home/blended-programs',
        },
      ]
    };
    setTimeout(() => {
      this.isdata = '';
    }, 2000);
  }

  datapass() {
    console.log('this.member==========================>',this.member);
    (this.ContentService.courseId = this.member.courseId),
      (this.ContentService.programId = this.categoryAddEditRes
        ? this.categoryAddEditRes.id
        : this.member.programId),
      (this.ContentService.tenantId = this.member.tenantId);
  }

  // Help Code Start Here //

  helpContent: any;
  getHelpContent() {
    return new Promise(resolve => {
      this.http1
        .get("../../../../../../assets/help-content/addEditCourseContent.json")
        .subscribe(
          data => {
            this.helpContent = data;
            console.log("Help Array", this.helpContent);
          },
          err => {
            resolve("err");
          }
        );
    });
    // return this.helpContent;
  }
      // Tag cganges
      makeTagDataReady(tagsData) {
        this.member.tags  =''
         tagsData.forEach((tag)=>{
          if(this.member.tags  == '')
          {
            this.member.tags  = tag.id;
          }else
          {
            this.member.tags = this.member.tags +'|' + tag.id;
          }
          console.log('this.formdata.tags',this.member.tags);
         });
        // var tagsData = this.formdata.tags;
        // var tagsString = '';
        // if (tagsData) {
        //   for (let i = 0; i < tagsData.length; i++) {
        //     var tag = tagsData[i];
        //     if (tagsString != "") {
        //       tagsString += "|";
        //     }
        //     if (String(tag) != "" && String(tag) != "null") {
        //       tagsString += tag;
        //     }
        //   }
        //   this.formdata.tags1 = tagsString;
        // }
      }
onTagsSelect(item: any) {
  console.log(item);
  console.log(this.selectedTags);
}
OnTagDeSelect(item: any) {
  console.log(item);
  console.log(this.selectedTags);
}
onTagSearch(evt: any) {
  console.log(evt.target.value);
  const val = evt.target.value;
  this.tagList = [];
  const temp = this.tempTags.filter(function(d) {
    return (
      String(d.name)
        .toLowerCase()
        .indexOf(val) !== -1 ||
      !val
    );
  });
  
  // update the rows
  this.tagList = temp;
  console.log('filtered Tag LIst',this.tagList);
}

  ngOnDestroy() {
    // this.ContentService.programId = 0;
    // this.addEditProgService.callData.programId = 0;
    // this.addEditProgService.callData.standardId = 0;
  }

  timewoutfun(id, standardId) {
    setTimeout(() => {
      this.callData.programId = id;
      this.callData.standardId = standardId;
      this.cdf.detectChanges();
    }, 100);
  }
  // Help Code Ends Here //
}
