import { Injectable } from '@angular/core';
import { webAPIService } from '../../../../../../../service/webAPIService';
import { webApi } from '../../../../../../../service/webApi';
import { HttpClient } from "@angular/common/http";
import { AuthenticationService } from '../../../../../../../service/authentication.service';

@Injectable({
  providedIn: 'root'
})

export class CallService {
  private url_getCalls: string = webApi.domain + webApi.url.getAllCalls;
  private url_getTrainers: string = webApi.domain + webApi.url.gettrainerByStandard;
  private url_insertCalls: string = webApi.domain + webApi.url.insertCall;
  private url_show_hide_calls: string = webApi.domain + webApi.url.show_hide_calls;
  constructor(private _http: HttpClient, private authenticationService: AuthenticationService) { 

  }

  getCalls(data) {
    return new Promise(resolve => {
      this._http.post(this.url_getCalls, data)
        // .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  getTrainerDropDown(data) {
    let url: any = this.url_getTrainers;

    return new Promise(resolve => {
      this._http.post(url, data)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  addEditCallRecord(data) {
    let url: any = this.url_insertCalls;

    return new Promise(resolve => {
      this._http.post(url, data)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  callVisibility(data) {
    return new Promise(resolve => {
      this._http.post(this.url_show_hide_calls, data)
        // .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

}
