import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-course-detail',
  templateUrl: './course-detail.component.html',
  styleUrls: ['./course-detail.component.scss']
})
export class CourseDetailComponent implements OnInit {
   userData:any;
  tenantId:any;
  activityData: any = {
    activityDuration: "01:00:00",
activityId: 8,
activityNo: "02",
activityTitle: "Activity 2",
activitycompletionid: 9,
completionCriteria: "1",
completionDate: null,
completionDays: 1,
completionstatus: "UP",
contentId: 5,
contentRepId: "1552638914000",
contenttime: 329,
contentwatchedtime: 43,
courseId: 45,
creditAllocId: 244,
dependentActId: 7,
description: "",
employeeId: 4,
enrolId: 136,
fileinfo:{
documenttype: "video",
mimetype: "video/x-m4v"
},
mimeType: "video/x-m4v",
moduleId: 22,
name: "Demo Video",
points: 2,
reference: "https://bhaveshedgetest.s3.ap-south-1.amazonaws.com/Huf_And_Mwpa_A_Case_Study_compressed.m4v",
referenceType: "video",
show: true,
startDate: null,
tags: "Video",
tenantId: this.tenantId,
viewed: 1
  };
  constructor() { 
        if(localStorage.getItem('LoginResData')){
      this.userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', this.userData.data);
      // this.userId = this.userData.data.data.id;
      this.tenantId = this.userData.data.data.tenantId;
   }
  }

  ngOnInit() {
  }
  callbackevent(event){
    console.log(event);
  }
}
