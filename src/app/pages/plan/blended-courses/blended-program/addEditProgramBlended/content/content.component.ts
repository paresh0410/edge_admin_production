import { Component, OnInit } from "@angular/core";
import { AddEditProgramBlendedService } from "./../addEditProgramBlended.service";
import { ContentService } from "../content.service";
import { NgxSpinnerService } from "ngx-spinner";
// /import { ToasterModule, ToasterService, Toast } from "angular2-toaster";
import { ToastrService } from 'ngx-toastr';
import {
  Router,
  NavigationStart,
  Routes,
  ActivatedRoute
} from "@angular/router";
import { AddEditProgramBlended } from '../addEditProgramBlended.component';
import { noData } from "../../../../../../models/no-data.model";

@Component({
  selector: "ngx-content",
  templateUrl: "./content.component.html",
  styleUrls: ["./content.component.scss"]
})
export class ContentComponent implements OnInit {
  modal: boolean;
  search:any={};
  courseName: any;
  courseDescription: any;
  courses: any;
  name: string;
  id: number;
  contentDataArray: any = [];
  badgeTitle:string='Select Course'
  btnName:string='Select'
  contentArray: any = [
    {
      id: 1,
      moduleId: 22,
      moduleName: "Module 1",
      list: [
        {
          activityId: 7,
          activityName: "Activity 1",
          activityTypeId: 1,
          activity_type: "File",
          courseId: 45,
          courseOrder: 0,
          creditAllocId: 241,
          img: "assets/images/open-book-leaf.jpg",
          moduleId: 22,
          modulePic: "assets/images/course1.jpg",
          modulename: "Demo Module",
          points: 1,
          summary: "This is Demo Module ",
          supertypeId: 1,
          tenantId: 1,
          usermodified: 2,
          visible: 1
        },
        {
          activityId: 8,
          activityName: "Activity 2",
          activityTypeId: 1,
          activity_type: "File",
          courseId: 45,
          courseOrder: 0,
          creditAllocId: 241,
          img: "assets/images/open-book-leaf.jpg",
          moduleId: 22,
          modulePic: "assets/images/course1.jpg",
          modulename: "Demo Module",
          points: 1,
          summary: "This is Demo Module ",
          supertypeId: 1,
          tenantId: 1,
          usermodified: 2,
          visible: 1
        }
      ]
    },
    {
      id: 1,
      moduleId: 22,
      moduleName: "Module 2",
      list: [
        {
          activityId: 7,
          activityName: "Activity 1",
          activityTypeId: 1,
          activity_type: "File",
          courseId: 45,
          courseOrder: 0,
          creditAllocId: 241,
          img: "assets/images/open-book-leaf.jpg",
          moduleId: 22,
          modulePic: "assets/images/course1.jpg",
          modulename: "Demo Module",
          points: 1,
          summary: "This is Demo Module ",
          supertypeId: 1,
          tenantId: 1,
          usermodified: 2,
          visible: 1
        },
        {
          activityId: 8,
          activityName: "Activity 2",
          activityTypeId: 1,
          activity_type: "File",
          courseId: 45,
          courseOrder: 0,
          creditAllocId: 241,
          img: "assets/images/open-book-leaf.jpg",
          moduleId: 22,
          modulePic: "assets/images/course1.jpg",
          modulename: "Demo Module",
          points: 1,
          summary: "This is Demo Module ",
          supertypeId: 1,
          tenantId: 1,
          usermodified: 2,
          visible: 1
        }
      ]
    },
    {
      id: 2,
      moduleId: 22,
      moduleName: "Module 3",
      list: [
        {
          activityId: 7,
          activityName: "Activity 1",
          activityTypeId: 1,
          activity_type: "File",
          courseId: 45,
          courseOrder: 0,
          creditAllocId: 241,
          img: "assets/images/open-book-leaf.jpg",
          moduleId: 22,
          modulePic: "assets/images/course1.jpg",
          modulename: "Demo Module",
          points: 1,
          summary: "This is Demo Module ",
          supertypeId: 1,
          tenantId: 1,
          usermodified: 2,
          visible: 1
        },
        {
          activityId: 8,
          activityName: "Activity 2",
          activityTypeId: 1,
          activity_type: "File",
          courseId: 45,
          courseOrder: 0,
          creditAllocId: 241,
          img: "assets/images/open-book-leaf.jpg",
          moduleId: 22,
          modulePic: "assets/images/course1.jpg",
          modulename: "Demo Module",
          points: 1,
          summary: "This is Demo Module ",
          supertypeId: 1,
          tenantId: 1,
          usermodified: 2,
          visible: 1
        }
      ]
    }
  ];

  callArray: any = [
    {
      id: 1,
      name: "Pre-call",
      courseList: [
        {
          courseImg: "./assets/images/assessment1.png",
          courseName: "Course 1",
          courseDesc:
            "Lorem ipsum dolor sit amet,consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.",
          progress: 80
        },
        {
          courseImg: "./assets/images/assessment2.png",
          courseName: "Course 2",
          courseDesc:
            "Lorem ipsum dolor sit amet,consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.",
          progress: 80
        },
        {
          courseImg: "./assets/images/quiz.jpg",
          courseName: "Course 3",
          courseDesc:
            "Lorem ipsum dolor sit amet,consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.",
          progress: 80
        }
      ]
    },
    {
      id: 2,
      name: "On-call",
      courseList: [
        {
          courseImg: "./assets/images/assessment1.png",
          courseName: "Course 4",
          courseDesc:
            "Lorem ipsum dolor sit amet,consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.",
          progress: 80
        },
        {
          courseImg: "./assets/images/assessment2.png",
          courseName: "Course 5",
          courseDesc:
            "Lorem ipsum dolor sit amet,consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.",
          progress: 80
        },
        {
          courseImg: "./assets/images/quiz.jpg",
          courseName: "Course 6",
          courseDesc:
            "Lorem ipsum dolor sit amet,consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.",
          progress: 80
        }
      ]
    },
    {
      id: 3,
      name: "Post-call",
      courseList: [
        {
          courseImg: "./assets/images/assessment1.png",
          courseName: "Course 7",
          courseDesc:
            "Lorem ipsum dolor sit amet,consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.",
          progress: 80
        },
        {
          courseImg: "./assets/images/assessment2.png",
          courseName: "Course 8",
          courseDesc:
            "Lorem ipsum dolor sit amet,consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.",
          progress: 80
        },
        {
          courseImg: "./assets/images/quiz.jpg",
          courseName: "Course 9",
          courseDesc:
            "Lorem ipsum dolor sit amet,consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.",
          progress: 80
        }
      ]
    }
  ];

  allCourse: any = [
    {
      courseid: 1,
      courseImg: "./assets/images/assessment1.png",
      courseName: "Course 7",
      courseDesc:
        "Lorem ipsum dolor sit amet,consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.",
      progress: 80,
      isSelected: false
    },
    {
      courseid: 2,
      courseImg: "./assets/images/assessment2.png",
      courseName: "Course 8",
      courseDesc:
        "Lorem ipsum dolor sit amet,consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.",
      progress: 80,
      isSelected: false
    },
    {
      courseid: 3,
      courseImg: "./assets/images/quiz.jpg",
      courseName: "Course 9",
      courseDesc:
        "Lorem ipsum dolor sit amet,consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.",
      progress: 80,
      isSelected: false
    }
  ];
  nodata: boolean;
  noDataVal:noData={
    margin:'mt-5',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"Sorry we couldn't find any matches please try again",
    desc:".",
    titleShow:true,
    btnShow:false,
    descShow:false,
    btnText:'Learn More',
    btnLink:''
}
  tenantId: any;
  courseId: any;
  programId: any;
  userId: any;
  condataCourse: any;
  couresshow: boolean = false;
  courseSelected: boolean = false;
  constructor(
    private AddEditProgramBlendedService: AddEditProgramBlendedService,
    private contentService: ContentService,
    private spinner: NgxSpinnerService,
    private router: Router,
    public routes: ActivatedRoute,
    // private toasterService: ToasterService,
    private toastr: ToastrService,
    private addEditProgramBlendedComp: AddEditProgramBlended,
  ) {
    if(this.contentService.programId){
      this.courseId = this.contentService.courseId;
    }

    if(this.contentService.programId){
      this.programId = this.contentService.programId;
    }

    if (localStorage.getItem("LoginResData")) {
      var userData = JSON.parse(localStorage.getItem("LoginResData"));
      console.log("userData", userData.data);
      this.userId = userData.data.data.id;
      this.tenantId = userData.data.data.tenantId;

    }
    this.getAllCoursesCC();
    this.getAllEnrolCourseModules();
    this.ActiveTab = this.contentArray[0].id;
    this.contentDataArray = this.contentArray[0].list;
  
  }

  ngOnInit() {
    // this.modal = false;
 
  }

  coursesCC: any[];
  getAllCoursesCC() {
    let param = {
      tId: this.tenantId
    };
    this.contentService.getAllCoursesCC(param).then(
      rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log("getAllEmployessCC:", rescompData);
        if (result["type"] == true) {
          if (result["data"][0].length == 0) {
            // this.noEmployees = true;
          } else {
            this.coursesCC = result["data"][0];
            console.log("this.coursesCC", this.coursesCC);
            // this.participantsservice.allEmployess = this.allEmployees;
            // console.log('this.participantsservice.getAllEmployees',this.participantsservice.getAllEmployees);
          }
        } else {
          // var toast: Toast = {
          //   type: "error",
          //   //title: "Server Error!",
          //   body: "Something went wrong.please try again later.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        }
      },
      error => {
        this.spinner.hide();
        // var toast: Toast = {
        //   type: "error",
        //   //title: "Server Error!",
        //   body: "Something went wrong.please try again later.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);

        this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
          timeOut: 0,
          closeButton: true
        });
      }
    );
  }

  
errorHandler(event,item){
  if(event){
  console.log(event,"event")
  if(item && item['coursePicRef']){
  item.coursePicRef = 'assets/images/courseicon.jpg'
  }
 }
}

  openModal() {
    this.couresshow = true;
  }

  closeModal() {
    this.couresshow = false;
  }

  selectItem(item) {
    if (!item.isSelected) {
      item.isSelected = true;
    } else {
      item.isSelected = false;
    }
  }

  // addSelected(){
  //   // this.allCourse.forEach(obj => {

  //   // });
  // }

  activeSelectedCourseId: any;
  setActiveSelectedCourse(currentIndex, currentCourse) {
    console.log("currentCourseOld:", currentCourse);
    if(this.contentService.courseId){
      this.courseId = this.contentService.courseId;
    }

    if(this.contentService.programId){
      this.programId = this.contentService.programId;
    }
    this.condataCourse = {};
    this.activeSelectedCourseId = currentCourse.courseId;
    this.condataCourse = currentCourse;
    console.log("currentCourseNew:", this.condataCourse);
  }
  courseCCObj: any = {};
  saveCourse() {
    if(this.condataCourse){
    this.spinner.show();
    this.courseCCObj = {
      courseId: this.condataCourse.courseId,
      coursePicRef: this.condataCourse.coursePicRef,
      fullname: this.condataCourse.fullname,
      summary: this.condataCourse.summary
    };
    if(this.condataCourse.fullname==null){
      this.nodata=true
    }
    console.log("Selected Course:", this.condataCourse);
    this.couresshow = false;
    // this.badge = false;

    let param = {
      // courseId: this.contentService.courseId,
      courseId: this.condataCourse.courseId,
      programId: this.contentService.programId,
      tId: this.tenantId
    };

    this.contentService.enrollCourseForCCContent(param).then(
      rescompData => {
        this.spinner.hide();

        var res = rescompData;
        console.log("Selected Course:", res);
        if (res["type"] == true) {
          // var toast: Toast = {
          //   type: "success",
          //   body: "Course enrolled to Program .",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);

          this.toastr.success('Course enrolled to program', 'Success', {
            closeButton: false
           });
          this.courseSelected = true;
          //this.addEditProgramBlendedComp.call = true;
          this.getAllEnrolCourseModules();
        } else {
          // var toast: Toast = {
          //   type: "error",
          //   body: "Something went wrong.please try again later.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        }
      },
      resUserError => {
        this.spinner.hide();
        //this.errorMsg = resUserError;
        // var toast: Toast = {
        //   type: "error",
        //   body: "Something went wrong.please try again later.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);

        this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
          timeOut: 0,
          closeButton: true
        });
      }
    );
    }else{
      this.toastr.warning('Please select a Course','Warning')
    }
  }

  courseAvailable: boolean = false;
  enrolId: any;
  clear() {
    if(this.search.length>=3){
      this.search= {};
      this.getAllCoursesCC();
      this.nodata=false
    }
    else{
      this.search = {};
    }

  }
  getAllEnrolCourseModules() {
    let param = {
      // courseId: this.contentService.courseId,
      courseId: this.condataCourse?this.condataCourse.courseId:this.contentService.courseId,
      programId: this.contentService.programId,
      tId: this.tenantId
    };

    this.contentService.getAllEnrolCourseModule(param).then(
      rescompData => {
        this.spinner.hide();

        var res = rescompData;
        console.log("ModulesCC:", res);
        if (res["type"] == true) {
          var Finres: any = res;
          if (Finres.data.length == 0) {
            this.courseAvailable = false;
            this.noDataVal={
              margin:'mt-3',
              imageSrc: '../../../../../assets/images/no-data-bg.svg',
              title:"No course selected.",
              desc:"Programs are defined by the admins. Programs are channels which help classify the courses.",
              titleShow:true,
              btnShow:false,
              descShow:false,
              btnText:'Learn More',
              btnLink:'https://faq.edgelearning.co.in/kb/learning-how-to-create-a-program',
            }
          } else {
            this.courseAvailable = true;
            this.contentArray = Finres.data;
            this.courseName = this.contentArray[0].courseName;
            this.courseDescription = this.contentArray[0].courseDescription;
            for (var i = 0; i < this.contentArray.length; i++) {
              var tempModule: any = i + 1;
              this.contentArray[i].id = i;
              this.contentArray[i].moduleName = "Module" + " " + tempModule;
            }
            this.ActiveTab = this.contentArray[0].id;
            this.contentDataArray = this.contentArray[0].list;
            this.enrolId = this.contentDataArray[0].enrolId;
            console.log("enrolId", this.enrolId);
            console.log("this.contentDataArray", this.contentDataArray);
            for (var i = 0; i < this.contentDataArray.length; i++) {
              var tempActivity: any = i + 1;
              this.contentDataArray[i].img = "assets/images/open-book-leaf.jpg";
              this.contentDataArray[i].activityName =
                "Activity" + " " + tempActivity;
            }
            console.log("CD===>", this.contentArray);
            // var enrolId = this.contentArray.find(function (element){
            // 	return element.enrolId
            // });
            // console.log('enrolId',enrolId);
            this.spinner.hide();
            console.log("ModulesCC:", res);
            console.log("currentCourseNew:", this.condataCourse);
          }
        } else {
          // var toast: Toast = {
          //   type: "error",
          //   body: "Something went wrong.please try again later.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        }
      },
      resUserError => {
        this.spinner.hide();
        //this.errorMsg = resUserError;
        // var toast: Toast = {
        //   type: "error",
        //   body: "Something went wrong.please try again later.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);

        this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
          timeOut: 0,
          closeButton: true
        });
      }
    );
  }
  ActiveTab: any;

  tabChanged(data) {
    this.spinner.show();
    console.log("DATA--->", data);
    this.ActiveTab = data.id;
    this.contentDataArray = this.contentArray[data.id].list;
    for (var i = 0; i < this.contentDataArray.length; i++) {
      var temp: any = i + 1;
      this.contentDataArray[i].img = "assets/images/open-book-leaf.jpg";
      this.contentDataArray[i].activityName = "Activity" + " " + temp;
    }
    this.spinner.hide();
  }

  romoveCourseModules(data) {
    console.log("data", data);
    let param = {
      tId: this.tenantId,
      programId: this.contentService.programId
      // "aId":20,
      // "calId":this.callId,
      // "empId":this.empId
    };
    this.contentService.removeEnrolCourse(param).then(
      rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log("Remove Course Result", rescompData);
        if (result["type"] == true) {
          // var toast: Toast = {
          //   type: "success",
          //   //title: "Server Error!",
          //   body: "Course removed successfully.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);

          this.toastr.success('Course removed', 'Success', {
            closeButton: false
           });
          this.enrolId = null;
          this.courseAvailable = false;
        } else {
          // var toast: Toast = {
          //   type: "error",
          //   //title: "Server Error!",
          //   body: "Something went wrong.please try again later.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        }
      },
      error => {
        this.spinner.hide();
        // var toast: Toast = {
        //   type: "error",
        //   //title: "Server Error!",
        //   body: "Something went wrong.please try again later.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);

        this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
          timeOut: 0,
          closeButton: true
        });
      }
    );
  }

  goToCourseDetails(data) {
    this.router.navigate(["content/course-details"], {
      relativeTo: this.routes
    });
  }

  claer()
  {
    this.search ={}
  }
}
