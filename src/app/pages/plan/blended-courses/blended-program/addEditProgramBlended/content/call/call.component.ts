import { Component, OnInit, Input, EventEmitter, Output, OnChanges, ChangeDetectorRef } from '@angular/core';
import { DatePipe } from '@angular/common';
import { CallService } from './call.service';
import { NgxSpinnerService } from 'ngx-spinner';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { ToastrService } from 'ngx-toastr';
import { AddEditProgramBlendedService } from '../../addEditProgramBlended.service';
import { CommonFunctionsService } from '../../../../../../../service/common-functions.service';
import { DateTimeAdapter, OWL_DATE_TIME_LOCALE, OWL_DATE_TIME_FORMATS } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';
import { MY_CUSTOM_FORMATS } from '../../../../../employees/employees.component';
import { noData } from '../../../../../../../models/no-data.model';


@Component({
  selector: 'ngx-call',
  templateUrl: './call.component.html',
  styleUrls: ['./call.component.scss'],
  providers: [
    { provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE] },
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS }]
})
export class CallComponent implements OnInit, OnChanges {
  userData: any;
  tenantId: any;
  @Input() inputData: any;
  calls_List: any = [ ];
  openModal: boolean = false;
  callData: any;
  selected_trainers: any = [];
  trainer: any = [ ];
  noDataVal:noData={
    margin:'',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No call added under program.",
    desc:"",
    titleShow:true,
    btnShow:false,
    descShow:false,
    btnText:'Learn More',
    btnLink:'',
  }
  //minDate =  new Date()
  minDate =new Date(new Date().setDate(new Date().getDate()-1));


  settingsTrainerDrop: any;
  nameArr: any = [];
  userId: any;
  programData: any = {};
  labels: any = [
    { labelname: 'Trainer', bindingProperty: 'entityNames', componentType: 'text' },
    { labelname: 'Call Date', bindingProperty: 'cDate', componentType: 'text' },
    { labelname: 'From Time', bindingProperty: 'fromT', componentType: 'text' },
    { labelname: 'To Time', bindingProperty: 'toT', componentType: 'text' },
    { labelname: 'ACTION', bindingProperty: 'btntext', componentType: 'button' },
    { labelname: 'EDIT', bindingProperty: 'tenantId', componentType: 'icon' },
  ];
  action: any;
  noCall: boolean;
  title: string;
  @Output() addCall = new EventEmitter<any>();
  constructor( private call_service: CallService,
    private spinner: NgxSpinnerService,
    // private toasterService: ToasterService,
    private toastr: ToastrService,
    private addEditProgramBlendedService: AddEditProgramBlendedService,
    private datepipe: DatePipe,
    private commonFunctionsService: CommonFunctionsService,
    private cdf: ChangeDetectorRef,
    ) {
      console.log('this.addEditProgramBlendedService.callData', this.addEditProgramBlendedService.callData );
      if (this.addEditProgramBlendedService.callData) {
        this.programData = this.addEditProgramBlendedService.callData;
        console.log('this.programData on call page:', this.programData);
      }

    if (localStorage.getItem('LoginResData')) {
      this.userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', this.userData.data);
      this.tenantId = this.userData.data.data.tenantId;
      this.userId = this.userData.data.data.id;
   }

   this.getAllCalls();
   this.getTrainerList();

    this.settingsTrainerDrop = {
      text: 'Select Trainer',
      singleSelection: false,
      classes: 'common-multi ',
      primaryKey: 'id',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      labelKey: 'trainerName',
      noDataLabel: 'No data available',
      enableSearchFilter: true,
      searchBy: ['id', 'trainerName'],
      badgeShowLimit: 3,
      maxHeight:250,
      lazyLoading: true,
    };
   }

  ngOnInit() {
    console.log('Data passed ===>', this.inputData);
  }
  ngOnChanges() {
    // const name: SimpleChange
    this.cdf.detectChanges();
    if (this.inputData === 'call') {
      this.addEditCall(null, 0);
    }  
        // console.log('prev value: ', name.previousValue);
    // console.log('got name: ', name.currentValue);
    // this._name = name.currentValue.toUpperCase();
    console.log("hello 2");
    this.newData();
    console.log('Data passed Updated  2 ===>', this.inputData);
  }
  newData(){
    console.log('Data passed Updated ===>', this.inputData);
  }
  addAction: boolean = false;
  btnName:string='Save'
  addEditCall(data, action) {
    this.action = action
    console.log(data,"data call")
    this.selected_trainers = [];
    this.openModal = true;
    if (action === 0) {
      this.addAction = true;
      this.title="Add Call"
      this.callData = {
        id: 0,
        entityNames: '',
        entityIds: '',
        callDate: '',
        fromTime: '',
        toTime: '',
        visible: 1,
      }

    } else if (action === 1) {

      this.addAction = false;
      this.title="Edit Call"
      this.callData = {
        id: data.id,
        entityIds: data.entityIds,
        entityNames: data.entityNames,
        callDate: data.callDate,
        fromTime: data.fromTime,
        toTime: data.toTime,
        visible: data.visible,
      }

      const trainerNames = data.entityNames.split(',');
      const trainerIds = data.entityIds.split(',');
      // console.log(trainerNames);
      // console.log(trainerIds);
      // trainerIds.forEach((element, index) => {
      //     this.selected_trainers.push({'id': trainerIds[index], 'trainerName': trainerNames[index]});
      // });

      for (let j = 0; j < this.trainer.length; j++) {
        for (let i = 0; i < trainerIds.length; i++) {
          if (this.trainer[j].id == trainerIds[i]) {
            this.selected_trainers.push(this.trainer[j]);
          }
        }
      }

      console.log('prepared array', this.selected_trainers);

      console.log('Call data', this.callData);
    }
  }

  onsubmit(data, form){

    
 console.log('Form ===>',form);
 if(form && form.valid){
  console.log('callData', data);
  let trainerNameStr: string = '';
  let trainerIdStr: string = '';
  let trainerIds: string = '';
  // this.selected_trainers.forEach(element => {
  //   trainerNameStr +=  element.trainerName + ',';
  //   trainerIdStr += element.id + ', ' ;
  // });

  for (let i = 0; i < this.selected_trainers.length; i++) {

    const trainId = this.selected_trainers[i].id;
    const trainerName = this.selected_trainers[i].trainerName;

    // if ( i === 0) {
    //   trainerIdStr = trainId;
    // }else {
    //   trainerIdStr = trainerIdStr + '|' + trainId;
    // }
    trainerIdStr = (i === 0) ? trainId : trainerIdStr + ',' + trainId;
    trainerIds = (i === 0) ? trainId : trainerIds + '|' + trainId;
    trainerNameStr = (i === 0) ? trainerName : trainerNameStr + ',' + trainerName;

  }

  // this.callData = {
  //   id: this.addAction ? 0 :  data.id,
  //   entityIds: trainerIdStr,
  //   entityNames: trainerNameStr,
  //   callDate: data.callDate,
  //   fromTime: data.fromTime,
  //   toTime: data.toTime,
  //   visible: data.visible,
  // };

  this.openModal = false;
  this.inputData=''
  console.log('Submitted data', this.callData);

  const param = {
    pgmId : this.programData.programId,
    callId: this.addAction ? 0 :  data.id,
    rolId : 7,
    tId: this.tenantId,
    entIds: trainerIdStr,
    trainerId: trainerIds,
    entNames: trainerNameStr,
    callDate: this.commonFunctionsService.formatSendDateTime(data.callDate),
    fromTime: this.commonFunctionsService.formatSendDateTime(data.fromTime),
    toTime: this.commonFunctionsService.formatSendDateTime(data.toTime),
    userId: this.userId,
  };

  console.log('param', param);

  this.call_service.addEditCallRecord(param).then(
    rescompData => {
      this.spinner.hide();
    console.log('Insert Response', rescompData);

    if (rescompData['type'] === true) {
      if(this.action == 0){
      this.presentToast('success', 'Call added successfully.');
      }else{
      this.presentToast('success', 'Call edited successfully .');
      }
      this.getAllCalls();
    } else {
      this.presentToast('error', '');
    }

    },
    resUserError => {
      this.spinner.hide();
      this.errorMsg = resUserError;
      this.presentToast('error', '');
    });
 }else {
   Object.keys(form.controls).forEach(key => {
     form.controls[key].markAsDirty();
   });
 }
   

  }

  onTrainerSelect(item: any) {
    console.log('Trainer selected', item);
    // this.selected_trainers.push(item);
    console.log('Selected Trainers', this.selected_trainers);
  }

  OnTrainerDeSelect(item: any) {
    console.log('Trainer selected', item);
    // this.selected_trainers.splice(item, 1);
    console.log('Selected Trainers', this.selected_trainers);
  }

  trainerList: any;

  onTrainerSearch(evt: any) {
    console.log(evt.target.value);
    const val = evt.target.value;
    this.trainerList = [];
    const trainerDummy = this.trainer.filter(function(d) {
      return (
        String(d.id)
          .toLowerCase()
          .indexOf(val) !== -1 ||
        d.trainerName.toLowerCase().indexOf(val) !== -1
      );
    });

    this.trainerList = trainerDummy;
  }

  closeModal() {
    this.openModal = false;
    this.inputData=''
  }
  errorMsg: any;
  notFound: boolean = false;
  editTrainerList: any = [];
  getAllCalls() {
    const data = {
      pgmId: this.programData.programId,
      tId : this.tenantId,
    };
    this.call_service.getCalls(data)

    .then(rescompData => {
      console.log('getAllprogramcalls:', rescompData);
      if (rescompData['data'].length === 0) {
        this.notFound = true;
        console.log('data found', this.notFound);
      }
      else {
        this.notFound = false;
        rescompData['data'].forEach(element => {
          element.cDate = this.commonFunctionsService.formatSendDate(element.callDate);
          element.fromT = this.commonFunctionsService.formatSendTime(element.fromTime);
          element.toT = this.commonFunctionsService.formatSendTime(element.toTime);
          element.callDate = new Date(element.callDate);
          element.fromTime = new Date(element.fromTime);
          element.toTime = new Date(element.toTime);
        });
        this.calls_List = rescompData['data'];
        this.calls_List?this.noCall=false:this.noCall=true
        for (var i = 0; i < this.calls_List.length; i++) {
          this.calls_List[i].Date = new Date(this.calls_List[i].cDate);
        this.calls_List[i].cDate = this.formdate(this.calls_List[i].Date);
          if(this.calls_List[i].visible == 1) {
            this.calls_List[i].btntext = 'fa fa-eye';
          } else {
            this.calls_List[i].btntext = 'fa fa-eye-slash';
          }
        }
        this.cdf.detectChanges();
        console.log('Call List Response', this.calls_List);
      }
    },
    resUserError => {
      console.log(resUserError);
      this.errorMsg = resUserError;
    });
  }
  formdate(date) {

    if (date) {
      // const months = ["JAN", "FEB", "MAR","APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
      // var day = date.getDate();
      // var monthIndex = months[date.getMonth()];
      // var year = date.getFullYear();

      // return day + '_' + monthIndex + '_' +year;
      var formatted = this.datepipe.transform(date, 'dd-MM-yyyy');
      return formatted;
    }
  }
  visibilityTableRowRule(row) {
    let value;
    let status;

    if (row.visible == 1) {
      row.btntext = 'fa fa-eye-slash';
      value  = 'fa fa-eye-slash';
      row.visible = 0
      status = 0;
    } else {
      status = 1;
      value  = 'fa fa-eye';
      row.visible = 1;
      row.btntext = 'fa fa-eye';
    }

    // for(let i =0; i < this.calls_List.length; i++) {
    //   if(this.calls_List[i].employeeId == row.employeeId) {
    //     this.calls_List[i].btntext = row.btntext;
    //     this.calls_List[i].visible = row.visible
    //   }
    // } this.spinner.show();

    const data = {
      callId: row.id,
      userId: this.userId,
      tId : this.tenantId,
      vis: row.visible,
    };
    this.call_service.callVisibility(data)
    .then(
      rescompData => {
      this.spinner.hide();
      console.log('Visibility Response', rescompData);

      if (rescompData['type'] === true) {
        if(row.visible==1){
          this.presentToast('success', 'Call Enabled Successfully.');
        }
        else{
        this.presentToast('success', 'Call Disabled Successfully.');

        }
        // this.getAllCalls();
      } else {
        this.presentToast('error', '');
      }

      },
      resUserError => {
        this.spinner.hide();
        this.errorMsg = resUserError;
        this.presentToast('error', '');
      }
    );



    console.log('row', row);
  }

  // enableDisable( item, visibility ) {
  //   console.log('visibility', visibility);
  //   this.spinner.show();

  //   const data = {
  //     callId: item.id,
  //     userId: this.userId,
  //     tId : this.tenantId,
  //     vis: item.visible === 1 ? 0 : 1,
  //   };
  //   this.call_service.callVisibility(data)
  //   .then(
  //     rescompData => {
  //     this.spinner.hide();
  //     console.log('Visibility Response', rescompData);

  //     if (rescompData['type'] === true) {
  //       this.presentToast('success', 'Call updated.');
  //       this.getAllCalls();
  //     } else {
  //       this.presentToast('error', '');
  //     }

  //     },
  //     resUserError => {
  //       this.spinner.hide();
  //       this.errorMsg = resUserError;
  //       this.presentToast('error', '');
  //     }
  //   );


  // }

  getTrainerList() {
    const data = {
      standId: this.programData.standardId,
      tId : this.tenantId,
    };
    console.log('trainer data', data);
    this.call_service.getTrainerDropDown(data)
    .then(rescompData => {
        this.trainer = rescompData['data'];
        console.log('Trainer Response', this.trainer);
    },
    resUserError => {
      console.log(resUserError);
      this.errorMsg = resUserError;
    });
  }


  presentToast(type, body) {
    if(type === 'success'){
      this.toastr.success(body, 'Success', {
        closeButton: false
       });
    } else if(type === 'error'){
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else{
      this.toastr.warning(body, 'Warning', {
        closeButton: false
        })
    }
  }

  // formatTime(time) {
  //   const t = new Date(time);
  //   const formattedTime = this.datepipe.transform(t, 'h:mm a');
  //   return formattedTime;
  // }

  // formatDate(date) {
  //   // console.log('date with out format', date);
  //   // const d = new Date(date);
  //   // const formattedDate = this.datepipe.transform(d, 'dd-MMM-yyyy');
  //   // console.log('date with format', formattedDate);
  //   // return formattedDate;
  // }
}
