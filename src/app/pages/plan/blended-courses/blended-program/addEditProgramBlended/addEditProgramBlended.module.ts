import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { MyDatePickerModule } from 'mydatepicker';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { FilterPipeModule  } from 'ngx-filter-pipe';
import { TagInputModule } from 'ngx-chips';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { AddEditProgramBlended } from './addEditProgramBlended.component';
import { AddEditProgramBlendedService } from './addEditProgramBlended.service';
import { NbTabsetModule } from '@nebular/theme';
import { ContentComponent } from './../addEditProgramBlended/content/content.component';
import { CallDetailService } from './call-detail.service';
import { ContentService } from './content.service';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import{ComponentModule} from '../../../../../component/component.module';

/*prod */
import { CourseDetailComponent } from './../addEditProgramBlended/content/course-detail/course-detail.component';
import { CallComponent } from './content/call/call.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2SmartTableModule,
    TagInputModule,
    NbTabsetModule,
    TimepickerModule.forRoot(),
    FilterPipeModule,
    NgxDatatableModule,
    AngularMultiSelectModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    ComponentModule
  ],
  declarations: [
    AddEditProgramBlended,
    ContentComponent,
    CourseDetailComponent,
    CallComponent,
    // ChartistJs
  ],
  providers: [
    AddEditProgramBlendedService,
    CallDetailService,
    ContentService
  ],
  schemas: [ 
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA 
  ]
})
export class  AddEditProgramBlendedModule {}
