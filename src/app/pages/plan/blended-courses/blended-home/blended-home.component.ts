import { Component, OnDestroy, ViewEncapsulation } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators/takeWhile';
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from '../../../../app.service';
import { BlendedService } from '../blended.service';
import { AddEditBlendCourseContentService } from '../addEditCourseContent/addEditCourseContent.service';
import { SuubHeader } from '../../../components/models/subheader.model';

interface CardSettings {
  title: string;
  iconClass: string;
  type: string;
}

@Component({
  selector: 'ngx-blended-home',
  styleUrls: ['./blended-home.component.scss'],
  templateUrl: './blended-home.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class BlendedHomeComponent implements OnDestroy {
  header: SuubHeader  = {
    title: 'Blended Learning',
    btnsSearch: true,
    btnBackshow: false,
    showBreadcrumb: true,
    breadCrumbList:[
      // {
      //   'name': 'Learning',
      //   'navigationPath': '/pages/learning',
      // },
    ]
  };

  private alive = true;

  lightCard: CardSettings = {
    title: 'Light',
    iconClass: 'nb-lightbulb',
    type: 'primary',
  };
  rollerShadesCard: CardSettings = {
    title: 'Roller Shades',
    iconClass: 'nb-roller-shades',
    type: 'success',
  };
  wirelessAudioCard: CardSettings = {
    title: 'Wireless Audio',
    iconClass: 'nb-audio',
    type: 'info',
  };
  coffeeMakerCard: CardSettings = {
    title: 'Coffee Maker',
    iconClass: 'nb-coffee-maker',
    type: 'warning',
  };

  statusCards: string;

  commonStatusCardsSet: CardSettings[] = [
    this.lightCard,
    this.rollerShadesCard,
    this.wirelessAudioCard,
    this.coffeeMakerCard,
  ];

  statusCardsByThemes: {
    default: CardSettings[];
    cosmic: CardSettings[];
    corporate: CardSettings[];
  } = {
      default: this.commonStatusCardsSet,
      cosmic: this.commonStatusCardsSet,
      corporate: [
        {
          ...this.lightCard,
          type: 'warning',
        },
        {
          ...this.rollerShadesCard,
          type: 'primary',
        },
        {
          ...this.wirelessAudioCard,
          type: 'danger',
        },
        {
          ...this.coffeeMakerCard,
          type: 'secondary',
        },
      ],
    };
  showdata: any = [];
  employees: boolean = false;
  users: boolean = false;
  courses: boolean = false;
  evaluation: boolean = false;
  competancy: boolean = false;
  courseBundle: boolean = false;
  roleManagement: boolean = false;
  learnData: any = [];
  constructor(private themeService: NbThemeService, public router: Router, public routes: ActivatedRoute,
    private AppService: AppService, protected blendedService: BlendedService,
    private addEditBlendedCourseContentService: AddEditBlendCourseContentService ) {
      localStorage.removeItem('blended-category')

    this.themeService.getJsTheme()
      .pipe(takeWhile(() => this.alive))
      .subscribe(theme => {
        this.statusCards = this.statusCardsByThemes[theme.name];
      });
    this.showdata = this.AppService.getmenus();
    if (this.showdata) {
      for (let i = 0; i < this.showdata.length; i++) {
        if (Number(this.showdata[i].parentMenuId) === 15) {
          this.learnData.push(this.showdata[i]);
        }
      }
    }
    this.blendedService.parentCatId = null
    this.blendedService.countLevel = 0
    this.addEditBlendedCourseContentService.previousBreadCrumb = null
    this.addEditBlendedCourseContentService.breadcrumbArray = null
    this.addEditBlendedCourseContentService.breadtitle = null
  }

  gotopages(url, item) {
    if(url == "blended-courses"){
      var passData = {
        id: 0,
        data: undefined
      }
      this.blendedService.data = passData;
      this.blendedService.workflowId = null;
    }
    if(item.menuId){
      this.addEditBlendedCourseContentService.menuId = item.menuId;
      localStorage.setItem('menuId', item.menuId);
    }
    this.router.navigate([url], { relativeTo: this.routes });
  }
  // gotoBlendedCourses() {
  //   this.router.navigate(['blended-courses'], { relativeTo: this.routes });
  // }

  ngOnDestroy() {
    this.alive = false;
  }

  // gotoNominationInstance() {
  //   this.router.navigate(['nomination-instance'], { relativeTo: this.routes });
  // }

  // gotocategory() {
  //   this.router.navigate(['blended-category'], { relativeTo: this.routes });
  // }

  // gotoPrograms() {
  //   this.router.navigate(['blended-programs'], { relativeTo: this.routes });

  // }

  back() {
    this.router.navigate(['../'], { relativeTo: this.routes });
  }


}
