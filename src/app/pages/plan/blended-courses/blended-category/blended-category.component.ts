import { Component, ViewEncapsulation,ElementRef, ViewChild } from '@angular/core';
import { Router, NavigationStart, Routes, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
// import { CategoryService } from './category.service';
import { BlendedCategoryService } from './blended-category.service';
import { AddEditCategoryBlendedService } from '../blended-category/addEditCategoryBlended/addEditCategoryBlended.service';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { BlendedService } from '../blended.service';
import { webAPIService } from '../../../../service/webAPIService';
import { webApi } from '../../../../service/webApi';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CommonFunctionsService } from '../../../../service/common-functions.service';
import { Card } from '../../../../models/card.model';
import { SuubHeader } from '../../../components/models/subheader.model';
import { HttpClient } from '@angular/common/http';
import { ContextMenuComponent } from 'ngx-contextmenu';
import { AddEditBlendCourseContentService } from '../addEditCourseContent/addEditCourseContent.service';
import { detailsBlendService } from '../addEditCourseContent/details/details.service';
import * as _ from "lodash";
import { Filter } from '../../../../models/filter.modal';
import { noData } from '../../../../models/no-data.model';


@Component({
  selector: 'ngx-blended-category',
  templateUrl: './blended-category.component.html',
  styleUrls: ['./blended-category.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class BlendedCategoryComponent {
  @ViewChild('ContextMenuComponent' ) public folderMenu: ContextMenuComponent;
  @ViewChild('hover') hover?: ElementRef<HTMLElement>;
  sidebarForm: boolean;
  titleName = "Add Category"
  btnName = "Save"
  selectedTags:any = [];
  tagList:any = [];
  settingsTagDrop ={};
  tempTags:any =[];
  formdata: any;
  defaultThumb: any = 'assets/images/category.jpg';
  result: any;
  userData: any;
  tenantId: any;
  header: SuubHeader  = {
    title:'Blended Category',
    btnsSearch: true,
    searchBar: true,
    dropdownlabel: ' ',
    placeHolder:'Search by category Name',
    btnAdd: 'Add Category',
    btnBackshow: true,
    btnAddshow: true,
    showBreadcrumb: true,
    breadCrumbList:[
      // {
      //   'name': 'Learning',
      //   'navigationPath': '/pages/learning',
      // },
      {
        'name': 'Blended Learning',
        'navigationPath': '/pages/blended-home',
      },
    ]
  };
  count:number=12
  notFound: boolean = false;
  cardModify: Card = {
    flag: 'blendedCategory',
    titleProp : 'categoryName',
    discrption: 'description',
    
    image: 'categoryPicRef',
    hoverlable:   true,
    showImage: true,
    option:   true,
    eyeIcon:   true,
    editIcon: true,
    batches: 'coursecount',
    showBottomBatches: true,
    bottomDiv:   true,
   bottomDiscription:   true,
    showBottomList:   true,
    bottomTitle:   true,
    btnLabel: 'Details',
    defaultImage:'assets/images/category.jpg'
  };

  cardModifyCourse: Card = {
    flag: 'blendedCourse',
    titleProp : 'fullname',
    image: 'coursePicRef',
    category: 'catName',
    // discrption: 'description',
    courseCompletion: 'Completioncount',
    compleCount: 'Completioncount',
    totalCount: 'enrollcount',
    manager: 'cereaterName',
    maxpoints: 'maxPoints',
    showCompletedEnroll: true,
    showBottomCategory: true,
    showBottomManager: true,
    showBottomMaximumPoint: true,
    showCourseCompletion: true,
    hoverlable:   true,
    showImage: true,
    option:   true,
    eyeIcon:   true,
    copyIcon: true,
    bottomDiv:   true,
   // bottomDiscription:   true,
    showBottomList:   true,
    bottomTitle:   true,
    btnLabel: 'Edit',
    defaultImage:'assets/images/courseicon.jpg'
  };
  query: string = '';
  public getData;

  cat: any;
  category: any = [];
  errorMsg: string;

  content1: any = [];

  search: any;
  loader: any;
  skeleton=false;

  // public helpContent:[
  //  {
  //   'label':string,
  //   'content':string
  //   }
  // ]=
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  searchText: string;
  title: string;
  dummy: any;
  index: any;
  parentCatId: any  = null;
  courseList: any;
  showOld:boolean = false;
  enableDisableCourseModal: boolean;
  courseDropdownList: any;
  categoryId: any;
  categoryName: any;
  oldParentCatId: any;
  countLevel: any = 0;
  breadtitle: any;
  breadObj: {};
  breadcrumbArray: any = [
    {
      'name': ' Blended Learning',
      'navigationPath': '/pages/blended-home',
    }, 
    // {
    //   'name': 'Course Category',
    //   'navigationPath': '/pages/plan/courses/category',
    //   'id': {},
    //   'sameComp': true,
    //   'assetId': null
    // }
  ];

  previousBreadCrumb: any = [
    {
      'name': 'Blended Learning',
      'navigationPath': '/pages/blended-home',
    }, 
    {
      'name': 'Blended Category',
      'navigationPath': '/pages/blended-home/blended-category',
      'id': {},
      'sameComp': true,
      'assetId': null
    }
  ]
  courseListDummy: any;
  hoverMeasure: boolean = false;
  heigthover: any;
  creatorList: any;
  venueData: any;
  locationData: any;
  courseLevel: any;
  programs: any;
  categoryDummy: any;
  catSearch: any;
  courseSearch: any;
  filters: any = [];
  filter: boolean = false;
  filtercon: Filter = {
    ascending: true,
    descending: true,
    showDropdown: true,
    // showDropdown: false,
    dropdownList: [
      { drpName: 'Course Name', val: 1 },
      { drpName: 'Created Date', val: 0 },
    ]
  };
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:'No Categories at this time.',
    desc:'Categories will appear after they are added by the instructor. Categories are classification for managing the courses in the application.',
    titleShow:true,
    btnShow:true,
    descShow:true,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/setting-how-to-add-an-online-category',
  }
  courseCount: any = 0;
  constructor(private spinner: NgxSpinnerService, protected webApiService: webAPIService,
    private detailsService: detailsBlendService,
    protected contentservice: BlendedService,
    protected passService1: AddEditBlendCourseContentService,
    // private toasterService: ToasterService, 
    private toastr: ToastrService, protected passService: BlendedService,private http1: HttpClient,
    private commonFunctionService: CommonFunctionsService,
    protected categoryService: BlendedCategoryService, protected addCategoryService: AddEditCategoryBlendedService, private router: Router) {
      
      this.settingsTagDrop = {
      badgeShowLimit: 3,
        text: 'Select Tags',
        singleSelection: false,
        classes: 'common-multi',
        primaryKey: 'id',
        labelKey: 'name',
        noDataLabel: 'Search Tags...',
        enableSearchFilter: true,
        lazyloading: true,
        searchBy: ['name'],
      };

      if (localStorage.getItem('LoginResData')) {
        this.userData = JSON.parse(localStorage.getItem('LoginResData'));
        console.log('userData', this.userData.data);
        // this.userId = this.userData.data.data.id;
        this.tenantId = this.userData.data.data.tenantId;
      }

      // this.getallTagList(id,category);
      this.getCourseDropdownList()
      this.getDropdownList();
      this.getHelpContent();
  
      localStorage.setItem('blended-category', 'Category');

      this.loader = true;

    this.search = {};
    this.category;
    this.cat = {
      id: ''
    };
    if(this.showOld == false){
      
    if (this.passService1.breadcrumbArray) {
      this.breadcrumbArray = this.passService1.breadcrumbArray
      this.breadtitle = this.passService1.breadtitle
      this.previousBreadCrumb = this.passService1.previousBreadCrumb
    }
      
    if(this.passService.parentCatId){
      this.parentCatId = this.passService.parentCatId
    }
    if(this.passService.countLevel){
      this.countLevel = this.passService.countLevel
    }
    this.getCourseCat(this.parentCatId)
    var id
    var category
    this.getallTagList(id,category);
  }
    if(this.showOld == true){
    this.getCategories();
    }
    this.getDropdownList();
  }

  visibility: any = [];
  getDropdownList() {
    this.spinner.show();
    let url = webApi.domain + webApi.url.getVisibiltyDropdown;
    let param = {
      tId: 1
    }
    this.webApiService.getService(url, param)
      .then(rescompData => {
        // this.loader =false;
        this.spinner.hide();
        var temp: any = rescompData;
        if (temp == "err") {
        } else {
          this.visibility = temp.data;
          this.skeleton=true;
        }
        // console.log('Visibility Dropdown',this.visibility);
      },
        resUserError => {
          // this.loader =false;
          this.skeleton=true;
          this.spinner.hide();
          this.errorMsg = resUserError;
        });
  }

  getCategories() {
    this.spinner.show();
    let url = webApi.domain + webApi.url.getCategories;
    let param = {
      tId: 1
    }

    this.webApiService.getService(url, param)
      .then(rescompData => {
        // this.loader =false;
        this.spinner.hide();
        this.notFound = false;
        var temp: any = rescompData;
        if (temp == "err") {
          this.notFound = true;
          this.skeleton=true;
        } else {
          this.category = temp.data;
          this.displayArray = []
          this.addItems(0,this.sum,'push',this.category);
        }
        this.skeleton=true;
        // console.log('Category Result',rescompData)
      },
        resUserError => {
          // this.loader =false;
          this.spinner.hide();
          this.skeleton=true;
          this.errorMsg = resUserError;
          this.notFound = true;
        });
  }

  clear() {
    if(this.searchText.length>=3){
    this.search = {};
    this.displayArray =[];
    this.newArray = [];
    this.sum = 50;
    this.notFound = false
    this.courseList = this.courseListDummy
    this.addItems(0, this.sum, 'push', this.category);
    }else{
    this.search = {};

    }

  };

  
  back() {
    this.filter = false
    if (this.parentCatId == null) {
      this.countLevel = 0
      this.router.navigate(['/pages/blended-home']);
    }
    else {
      this.countLevel = this.countLevel - 1
      this.parentCatId = this.oldParentCatId
      var index = this.breadcrumbArray.length - 1
      this.breadtitle = this.breadcrumbArray[index].name
      this.breadcrumbArray.pop()
      this.previousBreadCrumb.pop()
      this.getCourseCat(this.oldParentCatId)
    }
  }

  content2(data, id) {
    var passData = {
      id: id,
      data: data
    }
    this.passService.data = passData;
    this.router.navigate(['/pages/blended-home/blended-courses']);
  }

  gotoCardDetails(data){
    var passData = {
      id: 0,
      data: data
    }
    this.passService.data = passData;
    this.router.navigate(['/pages/blended-home/blended-courses']);
 }
  public addeditccategory(data, id) {
    var data1 = {
      data: data,
      id: id,
      dropdownData: this.visibility
    }
    if(id == 0) {
      this.titleName = "Add Category"
    } else {
      this.titleName ="Edit Category"
      this.courseCount  = data.coursecount
    }

    // this.getallTagList(id,data);
    this.makeCategoryDataReady(id, data);
    this.sidebarForm = true;
   

    // if (data == undefined) {
    //   this.addCategoryService.data = data1;
    //   this.router.navigate(['/pages/learning/blended-home/blended-category/add-edit-blended-category']);
    // } else {
    //   this.addCategoryService.data = data1;
    //   this.router.navigate(['/pages/learning/blended-home/blended-category/add-edit-blended-category']);
    // }

  }

  // gotoCardaddedit(data) {
  //   var data1 = {
  //     data: data,
  //     id: 1,
  //     dropdownData: this.visibility,
  //   }

  //   this.addCategoryService.data = data1;
  //   this.router.navigate(['/pages/blended-home/blended-category/add-edit-blended-category']);
  // }

  deleteCategoryModal: boolean = false;
  deleteCategoryData: any;
  deleteCategory(Category) {
    // console.log('Category content',Category);
    this.deleteCategoryData = Category;
    this.deleteCategoryModal = true;
  }

  deleteCategoryAction(actionType) {
    if (actionType == true) {
      this.closeDeleteCategoryModal();
    } else {
      this.closeDeleteCategoryModal();
    }
  }

  closeDeleteCategoryModal() {
    this.deleteCategoryModal = false;
  }

  disableCat: boolean = false;
  visibiltyRes: any;
  enableDisableCategoryModal: boolean = false;
  enableDisableCategoryData: any;
  enableCat: boolean = false;
  catDisableIndex: any;
  // btnName:string='Yes'
  disableCategory(currentIndex, categoryData, status) {

    this.enableDisableCategoryData = categoryData;
    this.enableDisableCategoryModal = true;
    this.catDisableIndex = currentIndex;

    if (this.enableDisableCategoryData.visible == 0) {
      this.enableCat = false;
    } else {
      this.enableCat = true;
    }
  }

  clickTodisable(categoryData,i) {
    if(categoryData.coursecount>0){
      this.presentToast('warning', 'Category have batches, not able to hide this category');
      return;
    }

    if(this.displayArray[i].visible == 1) {
      this.displayArray[i].visible = 0
    } else {
      this.displayArray[i].visible = 1
    }

    this.enableDisableCategory(categoryData)
    // else if (categoryData.visible == 1) {
    //   this.title="Disable category"
    //   this.enableCat = true;
    //   this.enableDisableCategoryData = categoryData;
    //   this.enableDisableCategoryModal = true;
    // } else {
    //   this.title="Enable category"
    //   this.enableCat = false;
    //   this.enableDisableCategoryData = categoryData;
    //   this.enableDisableCategoryModal = true;
    // }
  }
  


  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }
  enableDisableCategory(categoryData) {
    // this.spinner.show();
    var visibilityData = {
      categoryId: categoryData.categoryId,
      visible: categoryData.visible
    }
    const _urlDisableCategory: string = webApi.domain + webApi.url.disableCategory;
    this.commonFunctionService.httpPostRequest(_urlDisableCategory,visibilityData)

    //this.categoryService.disableCategory(visibilityData)
      .then(rescompData => {
        // this.loader =false;
        // this.spinner.hide();
        this.visibiltyRes = rescompData;
        // console.log('Category Visibility Result',this.visibiltyRes);
        if (this.visibiltyRes.type == false) {
          // var catUpdate: Toast = {
          //   type: 'error',
          //   title: "Category",
          //   body: "Unable to update visibility of category.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          this.closeEnableDisableCategoryModal();
          // this.toasterService.pop(catUpdate);

          this.toastr.error( 'Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
            });
        } else {
          // var catUpdate: Toast = {
          //   type: 'success',
          //   title: "Category",
          //   body: this.visibiltyRes.data,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          this.closeEnableDisableCategoryModal();
          // this.toasterService.pop(catUpdate);

          this.toastr.success(this.visibiltyRes.data, 'Success', {
            closeButton: false
           });
        }
      },
        resUserError => {
          // this.loader =false;
          this.spinner.hide();
          this.errorMsg = resUserError;
          this.closeEnableDisableCategoryModal();
        });
  }

  enableDisableCategoryAction(actionType) {
    if (actionType == true) {
      if (this.enableDisableCategoryData.visible == 1) {
        this.enableDisableCategoryData.visible = 0;
        var catData = this.enableDisableCategoryData;
        this.enableDisableCategory(catData);
      } else {
        this.enableDisableCategoryData.visible = 1;
        var catData = this.enableDisableCategoryData;
        this.enableDisableCategory(catData);
      }
    } else {
      this.closeEnableDisableCategoryModal();
    }
  }

  closeEnableDisableCategoryModal() {
    this.enableDisableCategoryModal = false;
  }

  onsearch(event){
    console.log(event,"event")
    
    this.search.categoryName =  event.target.value
  }

  ngOnDestroy(){
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0){
      e1[0].classList.add('with-scroll');
    }
  }
  ngAfterContentChecked() {
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0)
    {
      e1[0].classList.remove('with-scroll');
    }

   }
   conentHeight = '0px';
   offset: number = 100;
   ngOnInit() {
     this.conentHeight = '0px';
     const e = document.getElementsByTagName('nb-layout-column');
     console.log(e[0].clientHeight);
     // this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
     if (e[0].clientHeight > 1300) {
       this.conentHeight = '0px';
       this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
     } else {
       this.conentHeight = String(e[0].clientHeight) + 'px';
     }
   }
 
   sum = 60;
   addItems(startIndex, endIndex, _method, asset) {
     for (let i = startIndex; i < endIndex; ++i) {
       if (asset[i]) {
         this.displayArray[_method](asset[i]);
       } else {
         break;
       }
 
       // console.log("NIKHIL");
     }
   }
   // sum = 10;
   onScroll(event) {
 
     let element = this.myScrollContainer.nativeElement;
     // element.style.height = '500px';
     // element.style.height = '500px';
     // Math.ceil(element.scrollHeight - element.scrollTop) === element.clientHeight
 
     if (element.scrollHeight - element.scrollTop - element.clientHeight < 5) {
       if (this.newArray.length > 0) {
         if (this.newArray.length >= this.sum) {
           const start = this.sum;
           this.sum += 50;
           this.addItems(start, this.sum, 'push', this.newArray);
         } else if ((this.newArray.length - this.sum) > 0) {
           const start = this.sum;
           this.sum += this.newArray.length - this.sum;
           this.addItems(start, this.sum, 'push', this.newArray);
         }
       } else {
         if (this.category.length >= this.sum) {
           const start = this.sum;
           this.sum += 50;
           this.addItems(start, this.sum, 'push', this.category);
         } else if ((this.category.length - this.sum) > 0) {
           const start = this.sum;
           this.sum += this.category.length - this.sum;
           this.addItems(start, this.sum, 'push', this.category);
         }
       }
 
 
       console.log('Reached End');
     }
   }
   newArray =[];
   displayArray =[];
    // this function is use to dynamicsearch on table
  searchData(event) {
    var temData = this.category;
    var course = this.courseListDummy
    var val = event.target.value.toLowerCase();
    var keys = [];
    var coursekeys = [];
    this.searchText = val;
    if (event.target.value == '') {
      this.displayArray = [];
      this.newArray = [];
      this.sum = 50;
      this.addItems(0, this.sum, 'push', this.category);
      this.courseList = course
    } else {
      // filter our data
      if (temData.length > 0) {
        keys = Object.keys(temData[0]);
      }

      if (val.length >= 3 || val.length == 0) {
        var temp = temData.filter((d) => {
          for (const key of keys) {
            if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
              return true;
            }
          }
        });



        // update the rows
        this.displayArray = [];
        this.newArray = temp;
        this.sum = 50;

        this.addItems(0, this.sum, 'push', this.newArray);
      }

      // new addition
      if (course.length > 0) {
        coursekeys = Object.keys(course[0]);
      }
      if (val.length >= 3 || val.length == 0) {
        var coursetemp = course.filter((d) => {
          for (const key of coursekeys) {
            if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
              return true;
            }
          }
        });
      this.courseList = coursetemp
    }
   
    // end 
  }
  if(this.courseList.length == 0 && this.displayArray.length == 0){
    this.notFound = true
  }else{
    this.notFound = false
  }
  }


getallTagList(id,category) {
  this.spinner.show();
  let url = webApi.domain + webApi.url.getAllTagsComan;
  let param = {
    tenantId :this.tenantId
  };
  this.webApiService.getService(url, param)
    .then(rescompData => {
      // this.loader =false;
      this.spinner.hide();
      var temp: any = rescompData;
      this.tagList = temp.data;
      this.tempTags = [... this.tagList];
      this.bindfilter(this.tagList)
      this.selectedTags = []
      // this.makeCategoryDataReady(id, category);
      // console.log('Visibility Dropdown',this.visibility);
    },
      resUserError => {
        // this.loader =false;
        this.show =false;
        this.spinner.hide();
        this.errorMsg = resUserError;
      });
}

show:boolean =false;
makeCategoryDataReady(id,category){
  this.selectedTags = []
      if (id == 1) {
        this.title = 'Edit Category';
        this.formdata = {
          categoryId: category.categoryId,
          categoryName: category.categoryName,
          categoryCode: category.categoryCode,
          description: category.description,
          categoryPicRef: category.categoryPicRef ? category.categoryPicRef : this.defaultThumb,
          visible: category.visible,
          tenantId: category.tenantId,
          tags: category.tags == null ? category.tags : category.tags.split(',')
        }
          if(category.tagIds)
          {
            var tagIds =category.tagIds.split(',');
            if(tagIds.length > 0){
              this.tempTags.forEach((tag) => {
                tagIds.forEach((tagId)=>{
                  if (tag.id == tagId ) {
                    this.selectedTags.push(tag);
                  }
                });
              });
              }
        }
      } else {
        this.title = 'Add Category';
        this.formdata = {
          categoryId: 0,
          categoryName: '',
          categoryCode: '',
          description: '',
          categoryPicRef: this.defaultThumb,
          visible: 1,
          tenantId: this.tenantId,
          tags: '',
        }
      }
      this.show =true;
    }



categoryImgData: any;
readCategoryThumb(event: any) {
  var validExts = new Array(".png", ".jpg", ".jpeg");
  var fileExt = event.target.files[0].name;
  fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
  if (validExts.indexOf(fileExt) < 0) {
    // var toast: Toast = {
    //   type: 'error',
    //   title: "Invalid file selected!",
    //   body: "Valid files are of " + validExts.toString() + " types.",
    //   showCloseButton: true,
    //   // tapToDismiss: false, 
    //   timeout: 2000
    //   // onHideCallback: () => {
    //   //     this.router.navigate(['/pages/plan/users']);
    //   // }
    // };
    // this.toasterService.pop(toast);

    this.toastr.warning('Valid files are of types ' + validExts.toString(), 'Warning', {
      closeButton: false
    })
    // this.deleteCourseThumb();
  } else {
    if (event.target.files && event.target.files[0]) {
      this.categoryImgData = event.target.files[0];

      var reader = new FileReader();

      reader.onload = (event: ProgressEvent) => {
        // this.defaultThumb = (<FileReader>event.target).result;
        this.formdata.categoryPicRef = (<FileReader>event.target).result;
      }
      reader.readAsDataURL(event.target.files[0]);
    }
  }
}

deleteCategoryThumb() {
  // this.defaultThumb = 'assets/images/category.jpg';
  this.formdata.categoryPicRef = 'assets/images/category.jpg';
  this.categoryImgData = undefined;
  this.formdata.categoryPicRefs = undefined;
}

makeTagDataReady(tagsData) {
  this.formdata.tags  =''
   tagsData.forEach((tag)=>{
    if(this.formdata.tags  == '')
    {
      this.formdata.tags  = tag.id;
    }else
    {
      this.formdata.tags = this.formdata.tags +'|' + tag.id;
    }
    console.log('this.formdata.tags',this.formdata.tags);
   });
  // var tagsData = this.formdata.tags;
  // var tagsString = '';
  // if (tagsData) {
  //   for (let i = 0; i < tagsData.length; i++) {
  //     var tag = tagsData[i];
  //     if (tagsString != "") {
  //       tagsString += "|";
  //     }
  //     if (String(tag) != "" && String(tag) != "null") {
  //       tagsString += tag;
  //     }
  //   }
  //   this.formdata.tags1 = tagsString;
  // }
}

fileUploadRes: any;
categoryAddEditRes: any;

addUpdateCategory(url, category) {
  this.spinner.show();
  this.webApiService.getService(url, category)
    .then(rescompData => {
      // this.loader =false;
      this.spinner.hide();
      var temp: any = rescompData;
      this.categoryAddEditRes = temp.data;
      if (temp == "err") {
        // var catUpdate: Toast = {
        //   type: 'error',
        //   title: "Category",
        //   body: "Unable to update category.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(catUpdate);

        this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
          timeOut: 0,
          closeButton: true
        });
      } else if (temp.type == false) {
        // var catUpdate: Toast = {
        //   type: 'error',
        //   title: "Category",
        //   body: this.categoryAddEditRes.msg,
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(catUpdate);

        this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
          timeOut: 0,
          closeButton: true
        });
      } else {
        // this.getCategories();
        this.getCourseCat(this.parentCatId)
        this.getDropdownList();
        this.sidebarForm = false;
        // this.router.navigate(['/pages/learning/blended-home/blended-category']);
        // var catUpdate: Toast = {
        //   type: 'success',
        //   title: "Category",
        //   body: this.categoryAddEditRes.msg,
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(catUpdate);

        this.toastr.success(this.categoryAddEditRes.msg, 'Success', {
          closeButton: false
        });
      }
      console.log('Category AddEdit Result ', this.categoryAddEditRes)
    },
      resUserError => {
        // this.loader =false;
        this.spinner.hide();
        this.errorMsg = resUserError;
      });
}

catCodeDupliRes: any;
submit(f) {
  if (f.valid) {
    // this.loader = true;
    this.spinner.show();
    // this.makeTagDataReady();
    this.checkCategoryValid();

    // var catData = {
    //   categoryId : this.formdata.categoryId,
    //   // categoryCode : this.formdata.categoryCode,
    //   categoryName : this.formdata.categoryName
    // }
    // console.log('category Data ',catData);
    // this.service.checkCategory(catData).then(rescompData => { 
    //   // this.loader =false;
    //   this.spinner.hide();
    //   this.catCodeDupliRes = rescompData;
    //   console.log('Category Code duplication result ',this.catCodeDupliRes);
    //   if(this.catCodeDupliRes.catCode.isPresent == "true"){
    //     var codeCheck : Toast = {
    //       type: 'error',
    //       title: "Category",
    //       body: this.catCodeDupliRes.catCode.msg,
    //       showCloseButton: true,
    //       timeout: 2000
    //     };
    //     this.toasterService.pop(codeCheck);
    //   }else{
    //     this.checkCategoryValid();
    //   }
    //   this.cdf.detectChanges();
    // },
    // resUserError => {
    //   // this.loader =false;
    //   this.spinner.hide();
    //   this.errorMsg = resUserError
    // });
  } else {
    console.log('Please Fill all fields');
    // const addEditF: Toast = {
    //   type: 'error',
    //   title: 'Unable to update',
    //   body: 'Please Fill all fields',
    //   showCloseButton: true,
    //   timeout: 2000
    // };
    // this.toasterService.pop(addEditF);

    this.toastr.warning('Please fill in the required fields', 'Warning', {
      closeButton: false
    })
    Object.keys(f.controls).forEach(key => {
      f.controls[key].markAsDirty();
    });
  }
}


checkCategoryValid() {
  // this.loader = true;
  this.spinner.show();
  if (this.selectedTags.length > 0) {
    this.makeTagDataReady(this.selectedTags);
     // this.formdata.tags = this.formattedTags;
   }
  var category = {
    categoryId: this.formdata.categoryId,
    categoryName: this.formdata.categoryName,
    categoryCode: this.formdata.categoryCode,
    description: this.formdata.description,
    // categoryPicRef : this.categoryImgData == undefined ? null : this.formdata.categoryPicRef,
    categoryPicRef: this.formdata.categoryPicRef,
    visible: this.formdata.visible,
    tenantId: this.formdata.tenantId,
    tags: this.formdata.tags,    
    catTypeId:1,
    parCatId:this.parentCatId
    // tagsList : this.formdata.tags,
  }

  var fd = new FormData();
  fd.append('content', JSON.stringify(category));
  fd.append('file', this.categoryImgData);
  console.log('File Data ', fd);

  console.log('Category Data Img', this.categoryImgData);
  console.log('Category Data ', category);

  let url = webApi.domain + webApi.url.addEditCategory;
  let fileUploadUrl = webApi.domain + webApi.url.fileUpload;
  let param = {
    tId: this.tenantId
  }

  if (this.categoryImgData != undefined) {
    this.webApiService.getService(fileUploadUrl, fd)
      .then(rescompData => {
        // this.loader =false;
        this.spinner.hide();
        var temp: any = rescompData;
        // this.fileUploadRes = JSON.parse(temp);
        this.fileUploadRes = temp;
        if (temp == "err") {
          // var thumbUpload: Toast = {
          //   type: 'error',
          //   title: "Category Thumbnail",
          //   body: "Unable to upload category thumbnail.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(thumbUpload);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        } else if (temp.type == false) {
          // var thumbUpload: Toast = {
          //   type: 'error',
          //   title: "Category Thumbnail",
          //   body: "Unable to upload category thumbnail.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(thumbUpload);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        }
        else {
          if (this.fileUploadRes.data != null || this.fileUploadRes.fileError != true) {
            category.categoryPicRef = this.fileUploadRes.data.file_url;
            this.addUpdateCategory(url, category);
          } else {
            // var thumbUpload: Toast = {
            //   type: 'error',
            //   title: "Category Thumbnail",
            //   body: this.fileUploadRes.status,
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(thumbUpload);

            this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
              timeOut: 0,
              closeButton: true
            });
          }
        }
        console.log('File Upload Result', this.fileUploadRes)
      },
        resUserError => {
          // this.loader =false;
          this.spinner.hide();
          this.errorMsg = resUserError;
        });
  } else {
    this.addUpdateCategory(url, category);
  }
}


// Help Code Start Here //

helpContent: any;
getHelpContent() {
  return new Promise(resolve => {

    this.http1.get('../../../../../../assets/help-content/addEditCourseContent.json').subscribe(
      data => {
        this.helpContent = data;
        console.log('Help Array', this.helpContent);
      },
      err => {
        resolve('err');
      },
    );
  });
  // return this.helpContent;
}


// Help Code Ends Here //

  // Tag cganges

onTagsSelect(item: any) {
console.log(item);
console.log(this.selectedTags);
}
OnTagDeSelect(item: any) {
console.log(item);
console.log(this.selectedTags);
}
onTagSearch(evt: any) {
console.log(evt.target.value);
const val = evt.target.value;
this.tagList = [];
const temp = this.tempTags.filter(function(d) {
  return (
    String(d.name)
      .toLowerCase()
      .indexOf(val) !== -1 ||
    !val
  );
});

// update the rows
this.tagList = temp;
console.log('filtered Tag LIst',this.tagList);
}

cloeSidebar() {
  this.sidebarForm =false;
}

  //new structure code
  contextItem(event,data) {
   
    console.log(event,data);
    var catData = event.item

    if (this.dummy == 'Enable' || this.dummy == 'Disable'){
      this.clickTodisable(catData,this.index)
    }
    if(this.dummy == 'Edit'){
      this.addeditccategory(catData,1)

    }
  }

  View(item,index){
    console.log(item,"cgffgxfgx")
    this.dummy = item;
    this.index = index
    
    }

    getCourseCat(id) {

      if(this.parentCatId == null){
        this.header = {
        title:'Blended Category',
        btnsSearch: true,
        placeHolder:"Search by category name",
        searchBar: true,
        dropdownlabel: ' ',
        drplabelshow: false,
        drpName1: '',
        drpName2: ' ',
        drpName3: '',
        drpName1show: false,
        drpName2show: false,
        drpName3show: false,
        btnName1: '',
        btnName2: '',
        btnName3: '',
        btnAdd: 'Add Category',
        btnName1show: false,
        btnName2show: false,
        btnName3show: false,
        btnBackshow: true,
        btnAddshow: true,
        filter: false,
        showBreadcrumb: true,
        breadCrumbList:this.breadcrumbArray
      };
      }
      else{
        this.header = {
        title:this.breadtitle,
        btnsSearch: true,
        placeHolder:"Search by category name",
        searchBar: true,
        dropdownlabel: ' ',
        drplabelshow: false,
        drpName1: '',
        drpName2: ' ',
        drpName3: '',
        drpName1show: false,
        drpName2show: false,
        drpName3show: false,
        btnName1: 'Add Batch',
        btnName2: '',
        btnName3: '',
        btnAdd: 'Add Category',
        btnName1show: true,
        btnName2show: false,
        btnName3show: false,
        btnBackshow: true,
        btnAddshow: true,
        filter: true,
        showBreadcrumb: true,
        breadCrumbList:this.breadcrumbArray
        }
      }

      if(this.countLevel>3){
        this.header.btnAddshow = false
        // this.header.btnName1show = false
      }
      this.skeleton = false
      // this.spinner.show();
      let url = webApi.domain + webApi.url.getCourseCat;
      let param = {
        courseTypeId:4,
        catId:id,
        wfId:null
      }
  
      this.webApiService.getService(url, param)
        .then(rescompData => {
          // this.loader =false;
          this.spinner.hide();
          this.notFound = false;
          var temp: any = rescompData;
          if (temp == "err") {
            this.notFound = true;
            this.skeleton=true;
          } else {
            const list = rescompData['list'][1];
            const list1 = rescompData['list'][2];
            if (rescompData['list'][0].length === 0) {
              this.oldParentCatId = null;
            } else {
              this.oldParentCatId = rescompData['list'][0][0].parentCatId;
            }
            this.category = list;
            this.categoryDummy = list
            this.catSearch = list
            this.courseList = list1
            this.courseListDummy = list1
            this.courseSearch = list
            if(this.category.length==0 && this.courseList.length == 0){
              this.notFound=true
              this.noDataVal={
                margin:'mt-3',
                imageSrc: '../../../assets/images/no-data-bg.svg',
                title:'No Courses at this time',
                desc:'Courses will appear after they are added by the instructor. Courses are a bundle of learning activities which are designed by the instructors to enable self-paced online learning',
                titleShow:true,
                btnShow:true,
                descShow:true,
                btnText:'Learn More',
                btnLink:'https://faq.edgelearning.co.in/kb/setting-how-to-create-an-online-course',
              }
            }
            this.displayArray = []
            this.addItems(0,this.sum,'push',this.category);
          }
          this.skeleton=true;
          // console.log('Category Result',rescompData)
        },
          resUserError => {
            // this.loader =false;
            this.spinner.hide();
            this.skeleton=true;
            this.errorMsg = resUserError;
            this.notFound = true;
          });
  
    }


    getAllSubCat(item){
      this.filter = false
      this.countLevel = this.countLevel+1
      this.parentCatId = item.categoryId
      this.categoryId = this.parentCatId
      this.categoryName = item.categoryName
      this.passService.parentCatId = this.parentCatId
      this.passService.countLevel = this.countLevel

            //breadCrumb
            this.breadtitle = item.categoryName
            console.log('assetData', item);
            //// breadCrumb 
            this.breadObj = {
              'name': item.categoryName,
              // 'navigationPath': '',
              'id': item,
              'navigationPath': '/pages/blended-home/blended-category',
              'sameComp': true,
              'assetId': item.categoryId
            }
      
            this.previousBreadCrumb.push(this.breadObj)
      
            this.breadcrumbArray.push(this.breadObj)
            for (let i = 0; i < this.previousBreadCrumb.length; i++) {
              if (this.breadtitle == this.previousBreadCrumb[i].name) {
                this.breadcrumbArray = this.previousBreadCrumb.slice(0, i)
              }
            }
            //End BreadCrumb
      this.getCourseCat(this.parentCatId)

    }

    clickTodisableBatch(courseData, i) {
      if(this.courseList[i].visible == 1) {
        this.courseList[i].visible = 0
      } else {
        this.courseList[i].visible = 1
      }
  
      this.enableDisableCourse(courseData)
  
      // if (courseData.visible == 1) {
      //   this.enableCourse = true;
      //   this.enableDisableCourseData = courseData;
      //   this.enableDisableCourseModal = true;
      // } else {
      //   this.enableCourse = false;
      //   this.enableDisableCourseData = courseData;
      //   this.enableDisableCourseModal = true;
      // }
    }
  
    enableDisableCourse(courseData) {
      // this.spinner.show();
      var visibilityData = {
        courseId: courseData.courseId,
        visible: courseData.visible
      }
      const _urlDisableCourse: string = webApi.domain + webApi.url.disableBatch;
      //this.commonFunctionService.httpPostRequest(_urlDisableCourse,visibilityData)
  
     this.contentservice.disableCourse(visibilityData)
        .then(rescompData => {
          // this.loader =false;
          // this.spinner.hide();
          this.visibiltyRes = rescompData;
          console.log('Course Visibility Result', this.visibiltyRes);
          if (this.visibiltyRes.type == false) {
            // var courseUpdate: Toast = {
            //   type: 'error',
            //   title: "Course",
            //   body: "Unable to update visibility of course.",
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            this.closeEnableDisableCourseModal();
            // this.toasterService.pop(courseUpdate);
  
            this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
              timeOut: 0,
              closeButton: true
            });
          } else {
            // var courseUpdate: Toast = {
            //   type: 'success',
            //   title: "Course",
            //   body: this.visibiltyRes.data,
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            this.closeEnableDisableCourseModal();
            // this.toasterService.pop(courseUpdate);
  
            // this.toastr.success(this.visibiltyRes.data, 'Success', {
            //   closeButton: false
            // });
          }
        },
          resUserError => {
            // this.loader =false;
            this.spinner.hide();
            this.errorMsg = resUserError;
            this.closeEnableDisableCourseModal();
          });
    }

    closeEnableDisableCourseModal() {
      this.enableDisableCourseModal = false;
    }

    formDataCourse: any;
    duplicateContent(courseData) {
      this.formDataCourse = {
        courseId: courseData.courseId,
        fullname: '',
        tenantId: courseData.tenantId,
      }
    }
  
  
    duplicateCourseModal: boolean = false;
    duplicateCourseData: any;

    copyCard(course) {
      console.log('Course content', course);
      this.duplicateCourseData = course;
      this.duplicateCourseModal = true;
      this.duplicateContent(course);
    }
  
    duplicateCourseAction(actionType) {
      if (actionType == true) {
        this.makeDuplicateCourseDataReady();
        this.closeDuplicateCourseModal();
      } else {
        this.closeDuplicateCourseModal();
      }
    }
  
    closeDuplicateCourseModal() {
      this.duplicateCourseModal = false;
    }

    makeDuplicateCourseDataReady() {
      var courseData = {
        courseId: this.formDataCourse.courseId,
        fullname: this.formDataCourse.fullname,
        userMod: this.userData.data.data.id,
      }
      console.log('Duplicate course data ', courseData);
  
      this.duplicateCourse(courseData);
    }
  
    duplicateCourseRes: any;
    duplicateCourse(course) {
      // this.showSpinner = true
  
      const _urlDublicateCourse: string = webApi.domain + webApi.url.duplicateCourse;
      //this.commonFunctionService.httpPostRequest(_urlDublicateCourse,course)
      this.contentservice.dublicateCourse(course)
        .then(rescompData => {
          this.loader = false;
          var temp: any = rescompData;
          this.duplicateCourseRes = temp.data;
          if (temp == "err") {
            // this.showSpinner = false
            // this.notFound = true;
            // var courseDuplicate: Toast = {
            //   type: 'error',
            //   title: "Course",
            //   body: "Unable to duplicate course.",
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(courseDuplicate);
  
            this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
              timeOut: 0,
              closeButton: true
            });
          } else if (temp.type == false) {
            // this.showSpinner = false
            // var courseDuplicate: Toast = {
            //   type: 'error',
            //   title: "Course",
            //   body: this.duplicateCourseRes[0].msg,
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(courseDuplicate);
  
            this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
              timeOut: 0,
              closeButton: true
            });
          } else {
            // this.showSpinner = false;
            // var courseDuplicate: Toast = {
            //   type: 'success',
            //   title: "Course",
            //   body: this.duplicateCourseRes[0].msg,
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(courseDuplicate);
  
            this.toastr.success(this.duplicateCourseRes[0].msg, 'Success', {
              closeButton: false
            });
          }
          console.log('Course duplicate Result ', this.duplicateCourseRes);
          this.skeleton = false
          // this.cdf.detectChanges();
          // this.getCourses();
          this.getCourseCat(this.parentCatId)
        },
          resUserError => {
            this.loader = false;
            this.errorMsg = resUserError;
          });
    }

    gotoCardaddedit(data) {
      var data1 = {
        data: data,
        id: 1,
        catId: this.categoryId,
        categoryName:this.categoryName,
        courseDropdowns: this.courseDropdownList,
      };
  
      // if(this.contentservice.workflowId != null){
      //   this.passService.workflowId = this.contentservice.workflowId;
      //   this.passService.isWorkflow = true;
      // } else {
      //   this.passService.workflowId = null;
      //   this.passService.isWorkflow = false;
      // }

      this.breadtitle = data.fullname
      this.breadObj = {
        'name': data.fullname,
        // 'navigationPath': '',
        'id': data,
        'navigationPath': '/pages/blended-home/blended-category',
        'sameComp': true,
        'assetId': data.categoryId
      }
    
      this.previousBreadCrumb.push(this.breadObj)
    
      this.breadcrumbArray.push(this.breadObj)
      for (let i = 0; i < this.previousBreadCrumb.length; i++) {
        if (this.breadtitle == this.previousBreadCrumb[i].name) {
          this.breadcrumbArray = this.previousBreadCrumb.slice(0, i)
        }
      }
  
      if(this.contentservice.workflowId != null){
        this.passService1.workflowId = this.contentservice.workflowId;
        this.passService1.isWorkflow = true;
      } else {
        this.passService1.workflowId = null;
        this.passService1.isWorkflow = false;
      }
  
  
      this.passService1.data = data1;
      this.passService.parentCatId = this.parentCatId
      this.passService.countLevel = this.countLevel
      this.passService1.breadtitle = this.breadtitle
      this.passService1.breadcrumbArray = this.breadcrumbArray
      this.passService1.previousBreadCrumb = this.previousBreadCrumb
      this.router.navigate(['/pages/blended-home/blended-courses/addEditCourseContent']);
        // this.router.navigate(['addEditCourseContent'], { relativeTo: this.route });
  
    }

    breadcrumbNavigate(data){
      this.filter = false
      console.log(data,"navigationData")
      this.breadtitle = data.categoryName
      this.parentCatId = data.categoryId;
      this.countLevel = data['index']-1
      this.breadcrumbArray =  this.breadcrumbArray.slice(0,data.index)
      this.previousBreadCrumb = this.previousBreadCrumb.slice(0,data.index+1)
      console.log(this.breadcrumbArray,"breadcrumbArray")
      console.log(this.previousBreadCrumb,"previousBreadCrumb")
      
    
      // this.header.breadCrumbList =  this.breadcrumbArray
      // this.parentCatId = this.oldParentCatId
      this.getCourseCat(this.parentCatId);
    
    }

    public addeditcontent(data, id) {
      var data1 = {
        data: data,
        id: id,
        catId: this.categoryId,
        categoryName:this.categoryName,
        courseDropdowns: this.courseDropdownList,
      };
      if(this.contentservice.workflowId != null){
        this.passService1.workflowId = this.contentservice.workflowId;
        this.passService1.isWorkflow = true;
      } else {
        this.passService1.workflowId = null;
        this.passService1.isWorkflow = false;
      }
      if (data == undefined) {
        this.passService1.data = data1;
        this.passService.parentCatId = this.parentCatId
        this.passService.countLevel = this.countLevel
        this.passService1.breadtitle = 'Add Batch'
        this.passService1.previousBreadCrumb = this.previousBreadCrumb
        this.passService1.breadcrumbArray = this.previousBreadCrumb
        this.router.navigate(['/pages/blended-home/blended-courses/addEditCourseContent']);
        // this.router.navigate(['addEditCourseContent'], { relativeTo: this.route });
        console.log('Data passed to service' + data1);
        console.log('ID Passed' + data1.id + ' ' + data1.data);
      } else {
        this.passService1.data = data1;
        this.passService.parentCatId = this.parentCatId
        this.passService.countLevel = this.countLevel
        this.router.navigate(['/pages/plan/blended-home/blended-courses/addEditCourseContent']);
        // this.router.navigate(['addEditCourseContent'], { relativeTo: this.route });
      }
    }

    getCourseDropdownList() {
      // this.spinner.show();
      let data = {
        tId: this.tenantId,
        typeId:4
      }
      const _urlGetCourseDropdown: string = webApi.domain + webApi.url.getBatchDropdown;
      //this.commonFunctionService.httpPostRequest(_urlGetCourseDropdown,data)
      this.detailsService.getDropdownList(data)
      .then(rescompData => {
        // this.loader =false;
        this.spinner.hide();
        this.courseDropdownList = rescompData['data'];
        // console.log('Course Dropdown List ', this.courseDropdownList);
        try {
          for (let i = 0; i < this.courseDropdownList.programData.length; i++) {
            this.courseDropdownList.programData[i].programId = this.courseDropdownList.programData[i].id;
          }
          for (let i = 0; i < this.courseDropdownList.courseLevel.length; i++) {
            this.courseDropdownList.courseLevel[i].courseLevelId = this.courseDropdownList.courseLevel[i].id;
          }
          for (let i = 0; i < this.courseDropdownList.creatorList.length, i++;) {
            this.courseDropdownList.creatorList[i].creatorId = this.courseDropdownList.creatorList[i].id;
          }
          for (let i = 0; i < this.courseDropdownList.locationData.length; i++) {
            this.courseDropdownList.locationData[i].locationId = this.courseDropdownList.locationData[i].id;
          }
          for (let i = 0; i < this.courseDropdownList.venueData.length; i++) {
            this.courseDropdownList.venueData[i].venueId = this.courseDropdownList.venueData[i].id;
          }
  
  
          // this.courseFormat = this.courseDropdownList.courseType;
          this.courseLevel = this.courseDropdownList.courseLevel;
          // this.visibility = this.courseDropdownList.visibility;
          // this.daysBucket = this.courseDropdownList.leadTime;
          // this.userRoles = this.courseDropdownList.userRoles;
          this.creatorList = this.courseDropdownList.creatorList;
          this.programs = this.courseDropdownList.programData;
          this.locationData = this.courseDropdownList.locationData;
          this.venueData = this.courseDropdownList.venueData;
          // this.workflow = this.courseDropdownList.workflow;
  
          this.bindfilter(this.programs);
          this.bindfilter(this.courseLevel);
          this.bindfilter(this.locationData);
          if (this.creatorList && this.creatorList.length > 0) {
            this.creatorList.forEach(element => {
              element.filterId = 'Batch Creator';
            })
          }
          this.bindfilter(this.creatorList);
          this.bindfilter(this.venueData);
  
        } catch (err) {
          console.log(err);
        }
        // this.noContent = false;
  
      },
        resUserError => {
          // this.loader =false;
          this.spinner.hide();
          this.errorMsg = resUserError
        });
    }

    //filters
bindfilter(obj) {
  let filtername, filterValueName, type, singleSelection;
  if (obj.length > 0) {
    filtername = obj[0]['filterId'];
    filterValueName = obj[0]['filterValue'];
    type = obj[0]['type'];
    singleSelection = obj[0]['singleSelection'];
  }
      const item = {
        count: "",
        value: "",
        tagname: obj.length > 0 ? obj[0]['filterId'] : '',
        isChecked: false,
        list: obj,
        filterValue: filterValueName,
        type: type,
        filterId: filtername,
        singleSelection: singleSelection,
      }
      if (filtername) {
        this.filters.push(item);
      }
  // if (result['data'] && result['data'].length > 0) {
  //   result['data'].forEach( (value, index) => {

  //   })
  // }
}
filteredChanged(event){
  if (event.empty) {
    this.courseList = this.courseListDummy;
    // this.folder = this.tempFolders;
  } else {
    // this.filterFolder(event);
    this.filterFiles(event);
  }
  this.courseSearch = this.courseList;
  this.catSearch = this.category;
}

filterFiles(event) {
  this.courseList = [];
  let indexObj = {};
  let filterId;
  let filterValue;
  const findElement = (element) => String(element[filterId]).toLowerCase() === String(filterValue).toLowerCase();
  // tslint:disable-next-line:forin
  for (const x in event) {
      filterId = x;
      if (event[x] && Array.isArray(event[x])) {
        event[x].forEach((value, key) => {
          filterValue = value[filterId];
          // for(let i = 0; i<this.tempFiles.length; i++) {
          //   // const indexnumber = this.tempFiles.findIndex(findElement);
          //   // indexObj[indexnumber] = true;
          // }
          let indexnumberArray = [];
          if (filterId.startsWith('tag')) {
            // tslint:disable-next-line:max-line-length
            indexnumberArray = this.courseListDummy.map((e, j) => String(e[filterId]).includes(String(filterValue).toLowerCase()) ? j : '').filter(String);
          } else {
          // tslint:disable-next-line:max-line-length
            indexnumberArray = this.courseListDummy.map((e, j) => String(e[filterId]).toLowerCase() === String(filterValue).toLowerCase() ? j : '').filter(String);
// indexObj[indexnumber] = true;
          }
          indexObj = Object.assign(indexObj, indexnumberArray.reduce((a,b)=> (a[b]=true,a),{}));
          // const indexnumber = this.tempFiles.findIndex(findElement);
          // indexObj[indexnumber] = true;
      });
      }
  }
  console.log(JSON.stringify(indexObj));
  // tslint:disable-next-line:forin
  for (const y in indexObj) {
    if (String(y) !== '-1'){
      this.courseList.push(this.courseListDummy[y]);
    }
  }
}
filterFolder(event:any) {
  this.category  = [];
  let indexObj = {};
  let filterValue;
  let filterId;
  // const findElement = (element) => element[filterId] === filterValue;
  // const findElement = map((e, i) => e[filterId] === filterValue ? i : '').filter(String);
  // tslint:disable-next-line:forin
  for (const x in event) {
      filterId = x;
      if (event[x] && Array.isArray(event[x])) {
        event[x].forEach((value, key) => {
            filterValue = value[filterId];
            // for(let i = 0; i<this.tempFolders.length; i++) {
            //   // const indexnumber = this.tempFolders.findIndex(findElement);
            //   // tslint:disable-next-line:max-line-length
            //   const indexnumberArray =
            // this.tempFolders.map((e, j) => e[filterId] === filterValue ? j : '').filter(String);
            //   // indexObj[indexnumber] = true;
            //   indexObj = indexnumberArray.reduce((a,b)=> (a[b]=true,a),{});
            // }
            let indexnumberArray = [];
            if (filterId.startsWith('tag')) {
              // tslint:disable-next-line:max-line-length
              indexnumberArray = this.categoryDummy.map((e, j) => String(e[filterId]).includes(String(filterValue).toLowerCase()) ? j : '').filter(String);
            } else {
              // tslint:disable-next-line:max-line-length
              indexnumberArray = this.categoryDummy.map((e, j) => String(e[filterId]).toLowerCase() === String(filterValue).toLowerCase() ? j : '').filter(String);
            }
              // indexObj[indexnumber] = true;
              indexObj = Object.assign(indexObj, indexnumberArray.reduce((a,b)=> (a[b]=true,a),{}), indexObj);
        });
      }
  }
  console.log(JSON.stringify(indexObj));
  // tslint:disable-next-line:forin
  for (const y in indexObj) {
    if (String(y) !== '-1') {
      this.category.push(this.categoryDummy[y]);
    }
  }
}
SearchFilter(event) {
  const val = event == undefined ? '' : event.target.value.toLowerCase();
  // const files = this.files;
  let keys = [];
  if (val) {
    if (this.courseSearch.length > 0) {
      keys = Object.keys(this.courseSearch[0]);
    }
    if(val.length>=3||val.length==0) {
      const tempFiles = this.courseSearch.filter((d) => {
        for (const key of keys) {
          if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
            return true;
          }
        }
      });
      this.courseList = _.cloneDeep(tempFiles);
      const tempFolder = this.courseSearch.filter((d) => {
        for (const key of keys) {
          if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
            return true;
          }
        }
      });
      this.category = _.cloneDeep(tempFolder);
  } else {
    this.courseList = _.cloneDeep(this.courseSearch);
    this.category = _.cloneDeep(this.catSearch);
  }
} else {
  this.courseList = _.cloneDeep(this.courseSearch);
  this.category = _.cloneDeep(this.catSearch);
}
}

gotoFilter(){
  this.filter = !this.filter
  if(this.filter == false)
  this.courseList = this.courseListDummy
}

getSortData(event){
  console.log(event,"sorting")
  if(event.selectedLevel == 1){
    var property = 'fullname'
  }else{
    var property = 'timecreated'
  }
  var array = this.courseList
  // console.log(this.displayArray,"huhuhuhu")
  var dummyArray = array.sort(this.commonFunctionService.sort(property,event.selectedRadio))
  console.log(dummyArray,"dummyArray")
  this.courseList = array


}
    hoverable() {
      this.heigthover = this.hover.nativeElement.getBoundingClientRect().top;
      var hoversec = this.hover.nativeElement;
      if (this.heigthover > 550 ) {
        hoversec.classList.add('window-top');
      }
    }
  
    mouseup(index, item) {
      if(item.description) {
        for (let k = 0; k < this.category.length; k++) {
          if (k == index) {
            this.category[k].isHoverDesc = 1;
            this.hoverMeasure = true;
          }
        }
      }
    }
  
    mousedown(index) {
      for (let k = 0; k < this.category.length; k++) {
        if (k == index) {
          this.category[k].isHoverDesc = 0;
        }
      }
    }
    ngAfterViewChecked() {
      
      if(this.hoverMeasure == true) {
        this.hoverable();
        this.hoverMeasure = false;
      }
    }
  //end new structure code


}

