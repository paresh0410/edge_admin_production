import { Component, Directive, ViewEncapsulation, forwardRef, Attribute, OnChanges, SimpleChanges, Input, ViewChild, ViewContainerRef, ChangeDetectorRef, OnInit } from '@angular/core';
import { NG_VALIDATORS, Validator, Validators, AbstractControl, ValidatorFn } from '@angular/forms';
import { Router, NavigationStart, ActivatedRoute } from '@angular/router';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { AddEditCategoryBlendedService } from './addEditCategoryBlended.service';
import { HttpClient } from '@angular/common/http';
import { webAPIService } from '../../../../../service/webAPIService';
import { webApi } from '../../../../../service/webApi';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CommonFunctionsService } from '../../../../../service/common-functions.service';
import { SuubHeader } from '../../../../components/models/subheader.model';

@Component({
  selector: 'addEditCategoryBlended',
  templateUrl: './addEditCategoryBlended.html',
  styleUrls: ['./addEditCategoryBlended.scss'],
  encapsulation: ViewEncapsulation.None
})

export class AddEditCategoryBlended implements OnInit {
  header: SuubHeader  = {
    btnsSearch: true,
    btnName1: 'Save',
    btnName1show: true,
    btnBackshow:true,
    showBreadcrumb: true,
    breadCrumbList:[
      // {
      //   'name': 'Learning',
      //   'navigationPath': '/pages/learning',
      // },
      {
        'name': 'Blended learning',
        'navigationPath': '/pages/blended-home',
      },
      {
        'name': 'Blended Category',
        'navigationPath': '/pages/blended-home/blended-category',
      },
    ]
  };

  query: string = '';
  public getData;

  settingsTagDrop ={};
  tempTags:any =[];
  tagList:any = [];
  selectedTags:any = [];
  title: any;
  formdata: any;
  defaultThumb: any = 'assets/images/category.jpg';
  result: any;
  errorMsg: any;

  visibility: any = []

  catData: any = undefined;

  loader: any = false;
  userData: any;
  tenantId: any;
  constructor(private spinner: NgxSpinnerService, protected webApiService: webAPIService,
    protected service: AddEditCategoryBlendedService, public cdf: ChangeDetectorRef,
    // private toasterService: ToasterService,
     private router: Router,
    private commonFunctionService: CommonFunctionsService,
    private route: ActivatedRoute, private http1: HttpClient, private toastr: ToastrService) {

      this.settingsTagDrop = {
        text: 'Select Tags',
        singleSelection: false,
        classes: 'myclass custom-class',
        primaryKey: 'id',
        labelKey: 'name',
        noDataLabel: 'Search Tags...',
        enableSearchFilter: true,
        lazyloading: true,
        searchBy: ['name'],
      };
    // this.loader = true;
    if (localStorage.getItem('LoginResData')) {
      this.userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', this.userData.data);
      // this.userId = this.userData.data.data.id;
      this.tenantId = this.userData.data.data.tenantId;
    }
    var category;
    var id;

    if (this.service.data != undefined) {
      category = this.service.data.data;
      id = this.service.data.id;
      this.catData = this.service.data.data;
    } else {
      id = 0;
      // this.back();
      // this.router.navigate(['/pages/dashboard']);
    }
    this.getallTagList(id,category);
    this.getDropdownList();
    this.getHelpContent();

    console.log("Service Data:", this.service.data);
    console.log('Category Info', category);
    console.log('id', id);
  }
  ngOnInit(){
    this.header.title=this.service.data.data?this.service.data.data.categoryName:"Add Blended Category"
    this.spinner.show()
  }

  getDropdownList() {
    if(this.service.data)
    {
      // console.log('this.service.data',this.service.data)
    this.visibility = this.service.data.dropdownData;
    }else{
      this.back();
    }
  }

    
  getallTagList(id,category) {
    this.spinner.show();
    let url = webApi.domain + webApi.url.getAllTagsComan;
    let param = {
      tenantId :this.tenantId
    };
    this.webApiService.getService(url, param)
      .then(rescompData => {
        // this.loader =false;
        this.spinner.hide();
        var temp: any = rescompData;
        this.tagList = temp.data;
        this.tempTags = [... this.tagList];
        this.makeCategoryDataReady(id, category);
        // console.log('Visibility Dropdown',this.visibility);
      },
        resUserError => {
          // this.loader =false;
          this.show =false;
          this.spinner.hide();
          this.errorMsg = resUserError;
        });
  }
  
  show:boolean =false;
  makeCategoryDataReady(id,category){
        if (id == 1) {
          this.title = 'Edit Category';
          this.formdata = {
            categoryId: category.categoryId,
            categoryName: category.categoryName,
            categoryCode: category.categoryCode,
            description: category.description,
            categoryPicRef: category.categoryPicRef ? category.categoryPicRef : this.defaultThumb,
            visible: category.visible,
            tenantId: category.tenantId,
            tags: category.tags == null ? category.tags : category.tags.split(',')
          }
            if(category.tagIds)
            {
              var tagIds =category.tagIds.split(',');
              if(tagIds.length > 0){
                this.tempTags.forEach((tag) => {
                  tagIds.forEach((tagId)=>{
                    if (tag.id == tagId ) {
                      this.selectedTags.push(tag);
                    }
                  });
                });
                }
          }
        } else {
          this.title = 'Add Category';
          this.formdata = {
            categoryId: 0,
            categoryName: '',
            categoryCode: '',
            description: '',
            categoryPicRef: this.defaultThumb,
            visible: 1,
            tenantId: this.tenantId,
            tags: '',
          }
        }
        this.show =true;
      }

  back() {
    // this.router.navigate(['/pages/learning/blended-home/blended-category']);
    window.history.back()
  }

  categoryImgData: any;
  readCategoryThumb(event: any) {
    var validExts = new Array(".png", ".jpg", ".jpeg");
    var fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
      // var toast: Toast = {
      //   type: 'error',
      //   title: "Invalid file selected!",
      //   body: "Valid files are of " + validExts.toString() + " types.",
      //   showCloseButton: true,
      //   // tapToDismiss: false, 
      //   timeout: 2000
      //   // onHideCallback: () => {
      //   //     this.router.navigate(['/pages/plan/users']);
      //   // }
      // };
      // this.toasterService.pop(toast);

      this.toastr.warning('Valid files are of types ' + validExts.toString(), 'Warning', {
        closeButton: false
      })
      // this.deleteCourseThumb();
    } else {
      if (event.target.files && event.target.files[0]) {
        this.categoryImgData = event.target.files[0];

        var reader = new FileReader();

        reader.onload = (event: ProgressEvent) => {
          // this.defaultThumb = (<FileReader>event.target).result;
          this.formdata.categoryPicRef = (<FileReader>event.target).result;
        }
        reader.readAsDataURL(event.target.files[0]);
      }
    }
  }

  deleteCategoryThumb() {
    // this.defaultThumb = 'assets/images/category.jpg';
    this.formdata.categoryPicRef = 'assets/images/category.jpg';
    this.categoryImgData = undefined;
    this.formdata.categoryPicRefs = undefined;
  }

  makeTagDataReady(tagsData) {
    this.formdata.tags  =''
     tagsData.forEach((tag)=>{
      if(this.formdata.tags  == '')
      {
        this.formdata.tags  = tag.id;
      }else
      {
        this.formdata.tags = this.formdata.tags +'|' + tag.id;
      }
      console.log('this.formdata.tags',this.formdata.tags);
     });
    // var tagsData = this.formdata.tags;
    // var tagsString = '';
    // if (tagsData) {
    //   for (let i = 0; i < tagsData.length; i++) {
    //     var tag = tagsData[i];
    //     if (tagsString != "") {
    //       tagsString += "|";
    //     }
    //     if (String(tag) != "" && String(tag) != "null") {
    //       tagsString += tag;
    //     }
    //   }
    //   this.formdata.tags1 = tagsString;
    // }
  }

  fileUploadRes: any;
  categoryAddEditRes: any;

  addUpdateCategory(url, category) {
    this.spinner.show();
    this.webApiService.getService(url, category)
      .then(rescompData => {
        // this.loader =false;
        this.spinner.hide();
        var temp: any = rescompData;
        this.categoryAddEditRes = temp.data;
        if (temp == "err") {
          // var catUpdate: Toast = {
          //   type: 'error',
          //   title: "Category",
          //   body: "Unable to update category.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(catUpdate);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        } else if (temp.type == false) {
          // var catUpdate: Toast = {
          //   type: 'error',
          //   title: "Category",
          //   body: this.categoryAddEditRes.msg,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(catUpdate);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        } else {
          this.router.navigate(['/pages/learning/blended-home/blended-category']);
          // var catUpdate: Toast = {
          //   type: 'success',
          //   title: "Category",
          //   body: this.categoryAddEditRes.msg,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(catUpdate);

          this.toastr.success(this.categoryAddEditRes.msg, 'Success', {
            closeButton: false
          });
        }
        console.log('Category AddEdit Result ', this.categoryAddEditRes)
      },
        resUserError => {
          // this.loader =false;
          this.spinner.hide();
          this.errorMsg = resUserError;
        });
  }

  catCodeDupliRes: any;
  submit(f) {
    if (f.valid) {
      // this.loader = true;
      this.spinner.show();
      // this.makeTagDataReady();
      this.checkCategoryValid();

      // var catData = {
      //   categoryId : this.formdata.categoryId,
      //   // categoryCode : this.formdata.categoryCode,
      //   categoryName : this.formdata.categoryName
      // }
      // console.log('category Data ',catData);
      // this.service.checkCategory(catData).then(rescompData => { 
      //   // this.loader =false;
      //   this.spinner.hide();
      //   this.catCodeDupliRes = rescompData;
      //   console.log('Category Code duplication result ',this.catCodeDupliRes);
      //   if(this.catCodeDupliRes.catCode.isPresent == "true"){
      //     var codeCheck : Toast = {
      //       type: 'error',
      //       title: "Category",
      //       body: this.catCodeDupliRes.catCode.msg,
      //       showCloseButton: true,
      //       timeout: 2000
      //     };
      //     this.toasterService.pop(codeCheck);
      //   }else{
      //     this.checkCategoryValid();
      //   }
      //   this.cdf.detectChanges();
      // },
      // resUserError => {
      //   // this.loader =false;
      //   this.spinner.hide();
      //   this.errorMsg = resUserError
      // });
    } else {
      console.log('Please Fill all fields');
      // const addEditF: Toast = {
      //   type: 'error',
      //   title: 'Unable to update',
      //   body: 'Please Fill all fields',
      //   showCloseButton: true,
      //   timeout: 2000
      // };
      // this.toasterService.pop(addEditF);

      this.toastr.warning('Please fill in the required fields', 'Warning', {
        closeButton: false
      })
      Object.keys(f.controls).forEach(key => {
        f.controls[key].markAsDirty();
      });
    }
  }


  checkCategoryValid() {
    // this.loader = true;
    this.spinner.show();
    if (this.selectedTags.length > 0) {
      this.makeTagDataReady(this.selectedTags);
       // this.formdata.tags = this.formattedTags;
     }
    var category = {
      categoryId: this.formdata.categoryId,
      categoryName: this.formdata.categoryName,
      categoryCode: this.formdata.categoryCode,
      description: this.formdata.description,
      // categoryPicRef : this.categoryImgData == undefined ? null : this.formdata.categoryPicRef,
      categoryPicRef: this.formdata.categoryPicRef,
      visible: this.formdata.visible,
      tenantId: this.formdata.tenantId,
      tags: this.formdata.tags,
      // tagsList : this.formdata.tags,
    }

    var fd = new FormData();
    fd.append('content', JSON.stringify(category));
    fd.append('file', this.categoryImgData);
    console.log('File Data ', fd);

    console.log('Category Data Img', this.categoryImgData);
    console.log('Category Data ', category);

    let url = webApi.domain + webApi.url.addEditCategory;
    let fileUploadUrl = webApi.domain + webApi.url.fileUpload;
    let param = {
      tId: this.tenantId
    }

    if (this.categoryImgData != undefined) {
      this.webApiService.getService(fileUploadUrl, fd)
        .then(rescompData => {
          // this.loader =false;
          this.spinner.hide();
          var temp: any = rescompData;
          // this.fileUploadRes = JSON.parse(temp);
          this.fileUploadRes = temp;
          if (temp == "err") {
            // var thumbUpload: Toast = {
            //   type: 'error',
            //   title: "Category Thumbnail",
            //   body: "Unable to upload category thumbnail.",
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(thumbUpload);

            this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
              timeOut: 0,
              closeButton: true
            });
          } else if (temp.type == false) {
            // var thumbUpload: Toast = {
            //   type: 'error',
            //   title: "Category Thumbnail",
            //   body: "Unable to upload category thumbnail.",
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(thumbUpload);

            this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
              timeOut: 0,
              closeButton: true
            });
          }
          else {
            if (this.fileUploadRes.data != null || this.fileUploadRes.fileError != true) {
              category.categoryPicRef = this.fileUploadRes.data.file_url;
              this.addUpdateCategory(url, category);
            } else {
              // var thumbUpload: Toast = {
              //   type: 'error',
              //   title: "Category Thumbnail",
              //   body: this.fileUploadRes.status,
              //   showCloseButton: true,
              //   timeout: 2000
              // };
              // this.toasterService.pop(thumbUpload);

              this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
                timeOut: 0,
                closeButton: true
              });
            }
          }
          console.log('File Upload Result', this.fileUploadRes)
        },
          resUserError => {
            // this.loader =false;
            this.spinner.hide();
            this.errorMsg = resUserError;
          });
    } else {
      this.addUpdateCategory(url, category);
    }
  }


  // Help Code Start Here //

  helpContent: any;
  getHelpContent() {
    return new Promise(resolve => {

      this.http1.get('../../../../../../assets/help-content/addEditCourseContent.json').subscribe(
        data => {
          this.helpContent = data;
          console.log('Help Array', this.helpContent);
        },
        err => {
          resolve('err');
        },
      );
    });
    // return this.helpContent;
  }


  // Help Code Ends Here //

    // Tag cganges

onTagsSelect(item: any) {
  console.log(item);
  console.log(this.selectedTags);
}
OnTagDeSelect(item: any) {
  console.log(item);
  console.log(this.selectedTags);
}
onTagSearch(evt: any) {
  console.log(evt.target.value);
  const val = evt.target.value;
  this.tagList = [];
  const temp = this.tempTags.filter(function(d) {
    return (
      String(d.name)
        .toLowerCase()
        .indexOf(val) !== -1 ||
      !val
    );
  });

  // update the rows
  this.tagList = temp;
  console.log('filtered Tag LIst',this.tagList);
}

}
