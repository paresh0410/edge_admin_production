import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { MyDatePickerModule } from 'mydatepicker';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { TagInputModule } from 'ngx-chips';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { AddEditCategoryBlended } from './addEditCategoryBlended.component';
import { AddEditCategoryBlendedService } from './addEditCategoryBlended.service';
import { NbTabsetModule } from '@nebular/theme';
import { ComponentModule } from '../../../../../component/component.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2SmartTableModule,
    TagInputModule,
    NbTabsetModule,
    AngularMultiSelectModule,
    ComponentModule.forRoot(),
  ],
  declarations: [
    AddEditCategoryBlended,
    // ChartistJs
  ],
  providers: [
    AddEditCategoryBlendedService,
  ],
  schemas: [ 
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA,
  ]
})
export class  AddEditCategoryBlendedModule {}
