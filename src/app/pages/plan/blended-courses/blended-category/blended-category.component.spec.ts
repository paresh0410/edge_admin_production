import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlendedCategoryComponent } from './blended-category.component';

describe('BlendedCategoryComponent', () => {
  let component: BlendedCategoryComponent;
  let fixture: ComponentFixture<BlendedCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlendedCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlendedCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
