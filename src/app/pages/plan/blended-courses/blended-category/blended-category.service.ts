import { Injectable, Inject } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { AppConfig } from "../../../../app.module";
import { HttpClient } from "@angular/common/http";
import { webAPIService } from "../../../../service/webAPIService";
import { webApi } from "../../../../service/webApi";

@Injectable()
export class BlendedCategoryService {
  // private _url:string = "/api/edge/category/getCategory";
  // private _url:string = "http://13.233.171.126:9845/api/web/getallcat";
  private _url: string = webApi.domain + webApi.url.getCategories;

  public data: any;
  userData:any;
  tenantId:any;
  private _urlDisableCategory: string =
    webApi.domain + webApi.url.disableCategory;


  constructor(
    @Inject("APP_CONFIG_TOKEN") private config: AppConfig,
    private _http: Http,
    private http1: HttpClient
  ) {
      if(localStorage.getItem('LoginResData')){
      this.userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', this.userData.data);
      // this.userId = this.userData.data.data.id;
      this.tenantId = this.userData.data.data.tenantId;
   }
  }
    public param: any = {
    tId: this.tenantId,
  };
  getCategories() {
    let url: any = this._url;
    return this._http
      .post(url, {})
      .map((response: Response) => response.json())
      .catch(this._errorHandler);
  }

  disableCategory(visibleData) {
    // let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    // let options = new RequestOptions({ headers: headers });
    // let url:any = this._urlDisableCategory;
    // return this._http.post(url,visibleData,options)
    //     .map((response:Response)=>response.json())
    //     .catch(this._errorHandler);

    return new Promise(resolve => {
      this.http1
        .post(this._urlDisableCategory, visibleData)
        //.map(res => res.json())
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            resolve("err");
          }
        );
    });
  }
  _errorHandler(error: Response) {
    console.error(error);
    return Observable.throw(error || "Server Error");
  }
}
