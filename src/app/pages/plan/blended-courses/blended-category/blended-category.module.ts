import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FilterPipeModule  } from 'ngx-filter-pipe';
import { BlendedCategoryComponent } from './blended-category.component';
// import { CategoryService } from './category.service';
import { BlendedCategoryService } from './blended-category.service';
import { BlendedService } from '../blended.service';
import { AddEditCategoryBlendedModule } from '../blended-category/addEditCategoryBlended/addEditCategoryBlended.module';
import { AddEditCategoryBlendedService } from '../blended-category/addEditCategoryBlended/addEditCategoryBlended.service';
import { NotFoundComponent } from './not-found/not-found.component';
import { ThemeModule } from '../../../../@theme/theme.module';
import { TagInputModule } from 'ngx-chips';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgxContentLoadingModule } from 'ngx-content-loading';
import { ComponentModule } from '../../../../component/component.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { ContextMenuModule } from 'ngx-contextmenu';
// import { MiscellaneousRoutingModule, routedComponents } from './miscellaneous-routing.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FilterPipeModule,
    ThemeModule,
    TagInputModule,
    AddEditCategoryBlendedModule,
    NgxSkeletonLoaderModule,
    AngularMultiSelectModule,

    NgxContentLoadingModule,
    ComponentModule.forRoot(),    
    ContextMenuModule.forRoot(),
  ],
  declarations: [
    BlendedCategoryComponent,
    NotFoundComponent
  ],
  providers: [
    BlendedCategoryService,
    AddEditCategoryBlendedService,
    BlendedService
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})

export class BlendedCategoryModule {}
