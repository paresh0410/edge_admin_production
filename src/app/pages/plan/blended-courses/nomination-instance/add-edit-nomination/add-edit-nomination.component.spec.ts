import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditNominationComponent } from './add-edit-nomination.component';

describe('AddEditNominationComponent', () => {
  let component: AddEditNominationComponent;
  let fixture: ComponentFixture<AddEditNominationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditNominationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditNominationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
