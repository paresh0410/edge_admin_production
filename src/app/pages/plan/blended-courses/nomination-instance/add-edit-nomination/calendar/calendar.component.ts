import { NgForm } from '@angular/forms';
import {
  Component,
  ChangeDetectionStrategy,
  ViewChild,
  ViewEncapsulation,
  TemplateRef,
  OnInit,
  ChangeDetectorRef,
  Input,
  OnChanges, SimpleChanges,
  Output,
  EventEmitter
} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours
} from 'date-fns';
import { MatTabChangeEvent } from '@angular/material';
import { Subject, from } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
// import { ToasterService, Toast } from 'angular2-toaster';
import { ToastrService } from 'ngx-toastr';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView
} from 'angular-calendar';
import { BlendedService } from '../../../blended.service';
import { select } from '@syncfusion/ej2-base';
import { DatePipe, Time } from '@angular/common';
import { ContentService } from './../content.service';
import { AddEditNominationComponent } from '../add-edit-nomination.component';
import * as _moment from 'moment';
import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';
import { noData } from '../../../../../../models/no-data.model';
import * as _ from "lodash";
const moment = (_moment as any).default ? (_moment as any).default : _moment;

export const MY_CUSTOM_FORMATS = {
  fullPickerInput: 'DD-MM-YYYY HH:mm:ss',
    parseInput: 'DD-MM-YYYY HH:mm:ss',
    datePickerInput: 'DD-MM-YYYY',
    timePickerInput: 'LT',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY'
};
export declare enum CalendarEventTimesChangedEventType {
  Drag = 'drag',
  Drop = 'drop',
  Resize = 'resize'
}
export interface CalendarEventTimesChangedEvent1<MetaType = any> {
  type: CalendarEventTimesChangedEventType;
  event: CalendarEvent1<MetaType>;
  newStart: Date;
  newEnd?: Date;
  allDay?: boolean;
}
export interface EventColor {
  primary: string;
  secondary: string;
}
export interface EventAction {
  label: string;
  cssClass?: string;
  onClick({ event }: {
    event: CalendarEvent1;
  }): any;
}

export interface CalendarEvent1<MetaType = any> {
  callId: string | number;
  selectedEmployee: any;
  selectedCoach: any;
  id?: string | number;
  start: Date;
  end?: Date;
  title: string;
  color?: EventColor;
  actions?: EventAction[];
  allDay?: boolean;
  cssClass?: string;
  resizable?: {
    beforeStart?: boolean;
    afterEnd?: boolean;
  };
  draggable?: boolean;
  meta?: MetaType;
  time: String;
}

const colors: any = {
  red: {
    primary: '#2be0f3',
    secondary: '#f9ccad'
  },
  blue: {
    primary: '#bcdd11',
    secondary: '#bae7f7'
  },
  yellow: {
    primary: '#ff82ab',
    secondary: '#deacfd'
  }
};

@Component({
  selector: 'nomination-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    {provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE]},
    {provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS},]
})



export class CalendarComponent implements OnInit {
  @ViewChild('modalContent') modalContent: TemplateRef<any>;
  @Input() inpdata: any;
  @Output() Clear  = new EventEmitter<any>();

  view: CalendarView = CalendarView.Month;

  CalendarView = CalendarView;

  viewDate: Date = new Date();

  modalData: {
    action: string;
    event: CalendarEvent1;
  };
  settings = {
     singleSelection: true,
      text: 'Select trainers',
      enableSearchFilter: true,
      class:"common-multi",
      badgeShowLimit: 3,
      searchBy: ['trainerName'],
      // maxHeight: 250,
      searchPlaceholderText: 'Search by trainername',
      lazyLoading: true,
      showCheckbox: false,
  };
  selectedItems = [];
  // actions: CalendarEventAction[] = [
  //   {
  //     label: '<i class="fas fa-edit"></i>',
  //     onClick: ({ event }: { event: CalendarEvent }): void => {
  //       this.handleEvent('Edited', event);
  //     }
  //   },
  //   {
  //     label: '<i class="fa fa-fw fa-times"></i>',
  //     onClick: ({ event }: { event: CalendarEvent }): void => {
  //       this.events = this.events.filter(iEvent => iEvent !== event);
  //       this.handleEvent('Deleted', event);
  //     }
  //   }
  // ];


  dateObj = new Date();
  month = this.dateObj.getMonth();
  year = this.dateObj.getFullYear();
  date = this.dateObj.getDate();
  storeDateFormat = this.date + '' + this.month + '' + this.year;

  refresh: Subject<any> = new Subject();

  events: CalendarEvent1[] = [
    // {
    //   start : new Date(),
    //   end: addDays(new Date(), 1),
    //   title: 'Day 1 Induction',
    //   color: colors.red,
    //   // actions: this.actions,
    //   allDay: true,
    //   // resizable: {
    //   //   beforeStart: true,
    //   //   afterEnd: true
    //   // },
    //   // draggable: true
    // },
    // {
    //   start: new Date(),
    //   title: 'Trainer Call',
    //   color: colors.yellow,
    //   // actions: this.actions
    // },
    // {
    //   start: new Date(),
    //   end: addDays(new Date(), 1),
    //   title: 'Event 3',
    //   color: colors.blue,
    //   allDay: true
    // },
    // {
    //   start: new Date(),
    //   end: new Date(),
    //   title: 'Event 4',
    //   color: colors.yellow,
    //   // actions: this.actions,
    //   // resizable: {
    //   //   beforeStart: true,
    //   //   afterEnd: true
    //   // },
    //   // draggable: true
    // }
  ];

  openevent;
  eventlist: any = [];
  activeDayIsOpen: boolean = true;
  userLoginData: any = [];
  userdata: any = [];
  trainers: any = [];
  serviceData: any = [];
  wfId: any;
  noDataVal:noData={
    margin:'mt-5 w-100',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No Events to display.",
    desc:"",
    titleShow:true,
    btnShow:false,
    descShow:false,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/reaction-how-to-create-a-survey',
  }
  noDataValue:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No Slots to display.",
    desc:"",
    titleShow:true,
    btnShow:false,
    descShow:false,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/reaction-how-to-create-a-survey',
  }
  constructor(private modal: NgbModal, private blendedService: BlendedService, private datepipe: DatePipe,
    private contentService: ContentService, private toastr: ToastrService,
    public AddEditNominationComponent: AddEditNominationComponent,
    //  private toasterService: ToasterService,
    private cdf: ChangeDetectorRef,  private http1: HttpClient) {
      this.getHelpContent();
    this.showAddEventbtn = true;
    this.userLoginData = JSON.parse(localStorage.getItem('LoginResData'));
    this.userdata = this.userLoginData.data.data;
    console.log(this.userdata);
    if (this.blendedService.program) {
      this.serviceData = this.blendedService.program;
    }

    if (this.blendedService.wfId) {
      this.wfId = this.blendedService.wfId;
    }

    console.log(this.serviceData);
    if (this.serviceData) {
      this.getExisitingWorflowSlots();
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
		if(this.inpdata === 'slot') {
		  this.addSlot();
		} else if(this.inpdata === 'addslot') {
		  this.addMoreSlots();
		} else if(this.inpdata === 'saveslot') {
		  this.saveSlots(this.evalutionSlotsArr);
		} else if(this.inpdata === 'cancel') {
		  this.back();
		}
	}

  // dayClicked({ date, events }: { date: Date; events: CalendarEvent1[] }): void {
  //   if (isSameMonth(date, this.viewDate)) {
  //     this.viewDate = date;
  //     if (
  //       (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
  //       events.length === 0
  //     ) {
  //       this.activeDayIsOpen = false;
  //     } else {
  //       this.activeDayIsOpen = true;
  //     }
  //   }
  // }
  dayClicked({ date, events }: { date: Date; events: CalendarEvent1[] }): void {
    console.log(date);
    var startDate1 = new Date(
      date.getFullYear(),
      date.getMonth(),
      1
    );
    var endDate1 = new Date(
      date.getFullYear(),
      date.getMonth() + 1,
      0
    );
    if (new Date(date) >= startDate1 &&
      new Date(date) <= endDate1
    ) {
      this.viewDate = date;
      this.closeOpenMonthViewDay();
    }
    this.eventlist = events;
    if (isSameMonth(date, this.viewDate)) {
      this.viewDate = date;
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        // this.activeDayIsOpen = false;;
        this.openevent = true;
      } else {
        this.openevent = false;
        // this.activeDayIsOpen = true;
      }
      this.cdf.detectChanges();
    }
  }

  showAddEventbtn: boolean = false;

  tabChanged = (tabChangeEvent: MatTabChangeEvent): void => {
    console.log('tabChangeEvent => ', tabChangeEvent);
    console.log('index => ', tabChangeEvent.index);
    if (tabChangeEvent.index == 0) {
      this.showAddEventbtn = true;
    }
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent1): void {
    this.events = this.events.map(iEvent => {
      if (iEvent === event) {
        return {
          ...event,
          start: newStart,
          end: newEnd
        };
      }
      return iEvent;
    });
    this.handleEvent('Dropped or resized', event);
  }

  onCreatorSearch(evt: any) {
    console.log(evt.target.value);
    const val = evt.target.value;
    this.tempTrainer = this.trainers;
    const temp = this.trainers.filter(function(d) {
      // return (
      //   String(d.ecn)
      //     .toLowerCase()
      //     .indexOf(val) !== -1 ||
      //   d.trainerName.toLowerCase().indexOf(val) !== -1 ||
      //   d.email.toLowerCase().indexOf(val) !== -1 ||
      //   d.ecn.toLowerCase().indexOf(val) !== -1 ||
      //   !val
      // );
      return (
          d.trainerName.toLowerCase().indexOf(val) !== -1
        );
    });

    // update the rows
    this.trainers = temp;
    console.log(this.trainers);
  }

  onCreatorSelect(item: any) {
    console.log(item);
    // console.log(this.selectedCreator);
  }
  OnCreatorDeSelect(item: any) {
    console.log(item);
    // console.log(this.selectedCreator);
  }

  handleEvent(action: string, event: CalendarEvent1): void {
    this.modalData = { event, action };
    this.modal.open(this.modalContent, { size: 'lg' });
  }

  addEvent(): void {
    this.events = [
      //   ...this.events,
      //   {
      //     title: 'New event',
      //     start: startOfDay(new Date()),
      //     end: endOfDay(new Date()),
      //     color: colors.red,
      //     draggable: true,
      //     resizable: {
      //       beforeStart: true,
      //       afterEnd: true
      //     }
      //   }
    ];
  }
  @ViewChild(NgForm) evaluationForm: NgForm;
  tempTrainer;

  deleteEvent(eventToDelete: CalendarEvent) {
    this.events = this.events.filter(event => event !== eventToDelete);
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    // this.activeDayIsOpen = false;
    this.eventlist = [];
    console.log(this.viewDate);
    if (this.viewDate) {
      this.listofevent(this.events, this.viewDate);
    }
  }
  listofevent(eventsfordisaply, todaydate) {
    this.eventlist = [];
    console.log(eventsfordisaply);
    var currentdate = new Date(todaydate);
    var startDate = new Date(
      currentdate.getFullYear(),
      currentdate.getMonth(),
      1
    );
    var endDate = new Date(
      currentdate.getFullYear(),
      currentdate.getMonth() + 1,
      0,
    );
    for (let i = 0; i < eventsfordisaply.length; i++) {
      if (
        // this.formdate(eventsfordisaply[i].eventDate) ==
        // this.formdate(currentdate)
        new Date(eventsfordisaply[i].callDate) >= startDate &&
        new Date(eventsfordisaply[i].callDate) <= endDate
      ) {
        // this.eventlist = eventsfordisaply;
        this.eventlist.push(eventsfordisaply[i]);
      }
      this.cdf.detectChanges();
    }
  }
  addEditSlot: boolean = false;

  addSlot() {
    this.addEditSlot = true;
    if (this.addEditSlot) {
      try {
        this.trainerlist();
      } catch (e) {
      }
    }
  }

  back() {
    this.addEditSlot = false;
  }

  minDateto: Date;

  validateEndDate(data) {
    this.minDateto = data;
  }


  evalutionSlotsArr: any = [{
    startdate: null,
    enddate: null,
    time: null,
    trainer: [],
    trainerList:[]
  }];
  getExisitingWorflowSlots() {
    const param = {
      'wfId': this.wfId,
      'tId': this.userdata.tenantId,
    };
    this.contentService.getAllWorkflowSlots(param).then(res => {
      console.log('SLotsRes', res);
      if (res['type'] === true) {
        if(res['data'] && res['data'].length != 0){
          let evalSlotArr = [];
         evalSlotArr = _.cloneDeepWith(res['data']);
        for (let i = 0; i < evalSlotArr.length; i++) {
          evalSlotArr[i].start = new Date(this.formatDate(evalSlotArr[i].callDate));
          evalSlotArr[i].starttime = new Date(evalSlotArr[i].callStartTime);
          evalSlotArr[i]['displaytime'] = this.formatDisplayDateTime(evalSlotArr[i].callStartTime);
          evalSlotArr[i]['trainerList'] = [];
          evalSlotArr[i]['minDate'] =  new Date(evalSlotArr[i].callDate);
          evalSlotArr[i]['callDate'] =  new Date(evalSlotArr[i].callDate);
          evalSlotArr[i]['callEndTime'] =  new Date(evalSlotArr[i].callEndTime);
          evalSlotArr[i]['minCallStartTime'] =   new Date(evalSlotArr[i].callStartTime);
        }
        console.log('evalSlotArr', evalSlotArr);
        let levents = [];
        levents = _.cloneDeepWith(res['data']);
        for (let j = 0; j < levents.length; j++) {
          levents[j].callStartTime = new Date(levents[j].callStartTime);
          levents[j].callEndTime = new Date(levents[j].callEndTime);
          levents[j]['displaytime'] = this.formatDisplayDateTime(levents[j].callStartTime);
          levents[j]['minDate'] =  new Date(this.formatDate(  levents[j].callDate));
          levents[j]['minCallStartTime'] =   new Date(levents[j].callStartTime);
        }
         console.log('levents', levents);
        this.events = _.cloneDeepWith(levents);
        this.evalutionSlotsArr = _.cloneDeepWith(evalSlotArr);
        console.log('this.evalutionSlotsArr', this.evalutionSlotsArr);
        var todaydate = new Date();
        // this.eventlist = eventArr;
        this.listofevent(this.events, todaydate);
        console.log("List ===>", this.eventlist);
        }else {
          this.evalutionSlotsArr = [];
          this.events = [];
        }

      }
      else {
        console.log('Something went wrong.');
      }
    }, err => {
      console.log(err);
    });
  }
  addMoreSlots() {
    let evaluationSlot = {
      callDate: null,
      callStartTime: null,
      callEndTime: null,
      trainer: null,
      minDate: new Date(),
      minCallStartTime: new Date(),
    };
    evaluationSlot.minDate.setDate(evaluationSlot.minDate.getDate() - 1);
    evaluationSlot.minCallStartTime.setMinutes(evaluationSlot.minCallStartTime.getMinutes() + 2);
    const slots = _.cloneDeepWith(this.evalutionSlotsArr);
    this.evalutionSlotsArr = [];
    this.cdf.detectChanges();
    slots.push(evaluationSlot);
    this.evalutionSlotsArr = slots;
    console.log('evalutionSlotsArr', this.evalutionSlotsArr);
  }

  // calculateCurrentTimePlusTwoMin(){
  //     let t = new Date();
  //     const year = t.getFullYear() +  '-' + (t.getMonth() + 1 )+ '-' + t.getDate();
  //     const time = ' ' + t.getHours() + ':' + (t.getMinutes() + 5);
  //     const formattedDateTime = year + time;
  //     return new Date(formattedDateTime);
  // }

  deleteSlot(i, item) {
    for (let j = 0; j < this.evalutionSlotsArr.length; j++) {
      var selectedItem = i;
      console.log("Selected Index ", selectedItem);
      if (selectedItem === j) {
        this.evalutionSlotsArr.splice(selectedItem, 1);
      }
    }
    console.log('evaluationSlotArr', this.evalutionSlotsArr);
  }

  saveSlots(slots) {
    console.log('evaluation slots data -->', slots);
    console.log("this.evaluationform======>",this.evaluationForm)
    let slotArr = [];
    if(slots.length!=0){
      if(this.evaluationForm.valid){
    slots.forEach(element => {
      if (element.employeeId == null) {
        let slotObj = {
          trainerId: element.trainers[0].ID,
          startdate: this.formatSendDate(element.callDate),
          // enddate : this.formatDate(element.enddate),
          fromtime: this.formatSendDateTime(element.callStartTime),
          totime: this.formatSendDateTime(element.callEndTime),
        };
        slotArr.push(slotObj);
      }
    });

    console.log('slotArr', slotArr);
    // for (var i = 0; i < slots.length; i++) {
    //   let array: any = {
    //     start: slots[i].startdate,
    //     end: slots[i].enddate,
    //     title: slots[i].trainerName,
    //     time: slots[i].time
    //   };
    //   this.events.push(array);
    //   console.log('Events', this.events);
    //   this.addEditSlot = false;
    // }
    var option: string = Array.prototype.map.call(slotArr, function (item) {
      console.log('item', item);
      return Object.values(item).join('|');
    }).join('#');
    console.log('option', option);

    const evaluationdata = {
      wfId: this.wfId,
      stepsId: 3,
      allstr: option,
      tId: this.userdata.tenantId,
      userId: this.userdata.id,
    };
    this.blendedService.workflowevaluation(evaluationdata).then(res => {
      console.log('saveslotres', res);
      this.presentToast('success', 'Slot added.');
      this.addEditSlot = false;
      this.getExisitingWorflowSlots();
      this.Clear.emit();
    }, err => {
      console.log(err);
      this.presentToast('error', '');
    });
   }else{
    Object.keys(this.evaluationForm.controls).forEach(key => {
      this.evaluationForm.controls[key].markAsDirty();
    });
    setTimeout(() => this.presentToast('warning', 'Please fill all the mandatory fields') )
  }
 }
  else{
    this.presentToast('warning', 'Add Section to add a new slot');
  }
  }

  ngOnInit() { }

  formatSendDateTime(Dtime){
    const t = new Date(Dtime);
    // const formattedDateTime = this.datepipe.transform(t, 'yyyy-MM-dd h:mm');
    const year = t.getFullYear()+  '-' + (Number(t.getMonth())  + 1) + '-' + t.getDate() ;
    const time = ' ' + t.getHours() + ':' + t.getMinutes();
    const formattedDateTime = year + time;
    return formattedDateTime;
  }


  formatDisplayDateTime(dateTime){
    const dateTimeData = new Date(dateTime);
    let hours = dateTimeData.getHours();
      let minutes = dateTimeData.getMinutes();
      let ampm = hours >= 12 ? 'PM' : 'AM';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      let minutes2 = minutes < 10 ? '0'+ minutes : minutes;
      let strTime = hours + ':' + minutes2 + ' ' + ampm;
      return strTime;
    // const seconds = new Date()
  }

  formatSendDate(date) {
    const d = new Date(date);
    // const formatted = this.datepipe.transform(d, 'yyyy-MM-dd');
    const formattedTime =d.getFullYear()+ "-" + (Number(d.getMonth())  + 1) + "-" +  d.getDate() ;
    return formattedTime;
  }
  trainerlist() {
    const data = {
      tId: this.userdata.tenantId,
    };
    this.blendedService.trainerList(data).then(res => {
      if (res['type'] === true) {
        try {
          this.trainers = res['data'];
          this.tempTrainer = res['data'];
        } catch (e) {
          console.log(e);
        }
      }
      console.log('this.trainers', this.trainers);
    }, err => {
      console.log(err);
    });
  }

  formatDate(date) {
    const d = new Date(date);
    let formatted = "";
    if(!isNaN(d.valueOf())){
      formatted = this.datepipe.transform(d, 'dd-MM-yyyy');
    }else {
      formatted = this.datepipe.transform(new Date(), 'dd-MM-yyyy');
    }

    return formatted;
  }

  formatTime(time) {
    const t = new Date(time);
    const formattedTime = this.datepipe.transform(t, 'h:mm a');
    return formattedTime;
  }

  formatDateTime(time) {
    const t = new Date(time);
    const formattedDateTime = this.datepipe.transform(t, 'dd-MM-yyyy h:mm');
    return formattedDateTime;
  }

  presentToast(type, body) {
    if(type === 'success'){
      this.toastr.success(body, 'Success', {
        closeButton: false
       });
    } else if(type === 'error'){
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else{
      this.toastr.warning(body, 'Warning', {
        closeButton: false
        })
    }
  }

      // Help Code Start Here //

      helpContent: any;
      getHelpContent() {
        return new Promise(resolve => {

          this.http1.get('../../../../../../assets/help-content/addEditCourseContent.json').subscribe(
            data => {
              this.helpContent = data;
              console.log('Help Array', this.helpContent);
            },
            err => {
              resolve('err');
            },
          );
        });
        // return this.helpContent;
      }


      // Help Code Ends Here //
      onItemSelect(item:any){
        console.log(item);
        console.log(this.selectedItems);
      }
      OnItemDeSelect(item:any){
        console.log(item);
        console.log(this.selectedItems);
      }
      onSelectAll(items: any){
        console.log(items);
      }
       onDeSelectAll(items: any){
        console.log(items);
      }
}
