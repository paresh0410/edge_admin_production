import { Component, OnInit, ViewEncapsulation, ChangeDetectorRef, ViewChild } from '@angular/core';
import {
  Router,
  NavigationStart,
  Routes,
  ActivatedRoute,
} from '@angular/router';
import { DatePipe } from '@angular/common';
import { BlendedService } from '../../blended.service';
import { AddEditCourseContentService } from '../../../courses/addEditCourseContent/addEditCourseContent.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { ToastrService } from 'ngx-toastr';
import { EnrolNomineeService } from './enrol-nominee/enrol-nominee.service';
import { XlsxToJsonService } from '../../../../plan/users/uploadusers/xlsx-to-json-service';
import { JsonToXlsxService } from '../../../../coaching/participants/bulk-upload-coaching/json-to-xlsx.service';

//import { CodeOfConductComponent } from './code-of-conduct/code-of-conduct.component';
// import { truncate } from 'fs';
//import { JsonToXlsxService } from '../../../../coaching/participants/bulk-upload-coaching/json-to-xlsx.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonFunctionsService } from '../../../../../service/common-functions.service';
import { webApi } from '../../../../../service/webApi';
import { SuubHeader } from '../../../../components/models/subheader.model';
import { BrandDetailsService } from '../../../../../service/brand-details.service';
import { noData } from '../../../../../models/no-data.model';
@Component({
  selector: 'ngx-add-edit-nomination',
  templateUrl: './add-edit-nomination.component.html',
  styleUrls: ['./add-edit-nomination.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AddEditNominationComponent implements OnInit {

  @ViewChild('myTable') table: any;
  @ViewChild('fileUpload') fileUpload: any;
  @ViewChild(DatatableComponent) tableData: DatatableComponent;
  private xlsxToJsonService: XlsxToJsonService = new XlsxToJsonService();
  generateRandom;
  header: SuubHeader;
  /*BULk ENROL*/
  fileName: any;
  fileReaded: any;
  enableUpload: any;
  file: any = [];
  datasetPreview: any;
  preview: boolean = false;
  resultSheets: any = [];
  result: any = [];
  uploadedData: any = [];
  bulkUploadData: any = [];
  fileUrl: any;
  validdata: any = [];
  invaliddata: any = [];
  AllEmployess: any = [];
  empobj: any;
  nomiObj: any;
  showInvalidExcel: boolean = false;
  addEditModRes: any = [];
  contentData: any = [];
  resultdata: any = [];
  errorMsg;
  backFlag: number;
  searchText: any;
  currentBrandData: any;
  tabEvent(event) {
    console.log(event.tabTitle);
    switch (event.tabTitle) {
      case 'Details': console.log('1 details');
        //  if (this.cocComp.blendedservice.wfId) {
        //   this.cocComp.wfId = this.cocComp.blendedservice.wfId;
        //  }
        //  if (localStorage.getItem('LoginResData')) {
        //   this.cocComp.userdata = JSON.parse(localStorage.getItem('LoginResData'));
        // }
        // if (this.cocComp.blendedservice.program) {
        //   this.cocComp.nominationdata = this.cocComp.blendedservice.program;
        // }
        // this.cocComp.code();
        break;
      case 'Code of conduct': console.log('1 Code of conduct');
        break;
      case 'Content': console.log('1 Content');
        break;
      case 'Steps': console.log('1 Steps');
        break;
      case 'Enrol': console.log('1 Enrol');
        break;
      case 'Nominees': console.log('1 Nominees');
        break;
      case 'Calendar': console.log('1 Calendars');
        break;
      case 'Steps Data': console.log('Steps Data');
        break;
    }
  }

  tabEnabler = {
    detailsTab: true,
    nomineeTab: false,
    steps: false,
    calendar: false,
    content: false,
    enrolTab: false,
    codeOfConduct: false,
  };
  codeOfConduct: boolean = false;
  detailsTab: boolean = true;
  nomineeTab: boolean = false;
  enrolTab: boolean = false;
  calendar: boolean = false;
  content: boolean = false;
  steps: boolean = false;
  steps_data: boolean = false;
  showEnrolpage: boolean = false;
  showBulkpage: boolean = false;
  rows: any = [];
  enrolldata: any;
  enableCourse: boolean = false;
  tenantId: any;
  searchvalue: any = {
    value: ''
  };
  labels: any = [
    { labelname: 'ECN', bindingProperty: 'ecn', componentType: 'text' },
    { labelname: 'FULL NAME', bindingProperty: 'fullname', componentType: 'text' },
    { labelname: 'EMAIL', bindingProperty: 'emailId', componentType: 'text' },
    { labelname: 'MOBILE', bindingProperty: 'phoneNo', componentType: 'text' },
    { labelname: 'D.O.E', bindingProperty: 'enrolDate', componentType: 'text' },
    { labelname: 'MODE', bindingProperty: 'enrolmode', componentType: 'text' },
    { labelname: 'Re - Certification', bindingProperty: 'isRecert', componentType: 'text' },
    { labelname: 'ACTION', bindingProperty: 'btntext', componentType: 'button' },
  ];
  labelsPreview: any = [
    { labelname: 'ECN', bindingProperty: 'ecn', componentType: 'text' },
    { labelname: 'FULL NAME', bindingProperty: 'fullname', componentType: 'text' },
    { labelname: 'EMAIL', bindingProperty: 'emailId', componentType: 'text' },
    { labelname: 'MOBILE', bindingProperty: 'phoneNo', componentType: 'text' },
    { labelname: 'D.O.E', bindingProperty: 'enrolDate', componentType: 'text' },
    { labelname: 'MODE', bindingProperty: 'enrolmode', componentType: 'text' },
    { labelname: 'Re - Certification', bindingProperty: 'isRecert', componentType: 'text' },
  ];

  userId: number;
  temp: any = [];
  isdata: any;
  noDataVal:noData={
		margin:'mt-3',
		imageSrc: '../../../../../assets/images/no-data-bg.svg',
		title:"No Enrolment at this time.",
		desc:"",
		titleShow:true,
		btnShow:false,
		descShow:false,
		btnText:'Learn More',
		btnLink:'https://faq.edgelearning.co.in/kb/learning-how-to-add-notification-to-a-batch',
	  }
  constructor(public routes: ActivatedRoute, private router: Router, private blendedService: BlendedService,
    public addEditCourseService: AddEditCourseContentService, private datePipe: DatePipe,
    public brandService: BrandDetailsService,
    private commonFunctionService: CommonFunctionsService,
    public cdf: ChangeDetectorRef, private toastr: ToastrService, private exportService: JsonToXlsxService,
    private spinner: NgxSpinnerService, private enrolService: EnrolNomineeService) {
    var userData = JSON.parse(localStorage.getItem('LoginResData'));
    console.log('userData', userData.data);
    this.userId = userData.data.data.id;
    this.tenantId = userData.data.data.tenantId;
    this.allEnrolUser();

  }

  ngOnInit() {
    if (this.blendedService.program) {
      this.contentData = this.blendedService.program;
    }
    if (this.blendedService.wfId) {
      this.contentData.courseId = this.blendedService.wfId;
    }
    this.currentBrandData = this.brandService.getCurrentBrandData();
    this.fileName = 'Click here to upload an excel file to enrol ' + this.currentBrandData.employee.toLowerCase() + ' to this course'
  }

  selectedTab(tabEvent) {
    console.log('tab Selected', tabEvent);

    if (tabEvent.tabTitle === 'Details') {
      this.header = {
        title: this.contentData.instName!=""?this.contentData.instName:'Add Nomination',
        btnsSearch: true,
        searchBar: false,
        btnName4: 'Save',
        btnName4show: true,
        btnBackshow: true,
        showBreadcrumb: true,
        breadCrumbList: [
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          // {
          //   'name': 'Blended Courses',
          //   'navigationPath': '/pages/learning/blended-home',
          // },
          {
            'name': 'Nomination Instance',
            'navigationPath': '/pages/nomination-instance',
          }]
      }
      this.backFlag = 1;
      this.spinner.show();
    setTimeout(() => {
      this.spinner.hide()
    }, 2000);
      this.detailsTab = true;
      this.codeOfConduct = false;
      this.nomineeTab = false;
      this.calendar = false;
      this.content = false;
      this.steps = false;
      this.enrolTab = false;
      this.steps_data = false;
    } else if (tabEvent.tabTitle === 'T&C') {
      this.header = {
        title: this.contentData.instName!=""?this.contentData.instName:'Add Nomination',
        btnsSearch: true,
        btnName5: 'Add Section',
        btnName6: 'Save',
        btnName5show: true,
        btnName6show: true,
        btnBackshow: true,
        showBreadcrumb: true,
        breadCrumbList: [
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          // {
          //   'name': 'Blended Courses',
          //   'navigationPath': '/pages/learning/blended-home',
          // },
          {
            'name': 'Nomination Instance',
            'navigationPath': '/pages/nomination-instance',
          },
        ]
      };
      this.backFlag = 1;
      this.spinner.show();
      setTimeout(() => {
        this.spinner.hide()
      }, 2000);
      this.detailsTab = false;
      this.codeOfConduct = true;
      this.nomineeTab = false;
      this.calendar = false;
      this.content = false;
      this.steps = false;
      this.enrolTab = false;
      this.steps_data = false;
    } else if (tabEvent.tabTitle === 'Enrol') {
      this.header = {
        title: this.contentData.instName!=""?this.contentData.instName:'Add Nomination',
        btnsSearch: true,
        btnName2: 'Enrol',
        btnName3: 'Bulk Enrol',
        btnName2show: true,
        btnName3show: true,
        btnBackshow: true,
        showBreadcrumb: true,
        breadCrumbList: [
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          // {
          //   'name': 'Blended Courses',
          //   'navigationPath': '/pages/learning/blended-home',
          // },
          {
            'name': 'Nomination Instance',
            'navigationPath': '/pages/nomination-instance',
          },
        ]
      };
      this.backFlag = 1;
      this.spinner.show();
      setTimeout(() => {
        this.spinner.hide()
      }, 2000);
      this.detailsTab = false;
      this.codeOfConduct = false;
      this.nomineeTab = false;
      this.calendar = false;
      this.content = false;
      this.steps = false;
      this.enrolTab = true;
      this.steps_data = false;
    } else if (tabEvent.tabTitle === 'Settings') {
      this.header = {
        title: this.contentData.instName!=""?this.contentData.instName:'Add Nomination',
        btnsSearch: true,
        btnName7: 'Save',
        btnName7show: true,
        btnBackshow: true,
        showBreadcrumb: true,
        breadCrumbList: [
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          // {
          //   'name': 'Blended Courses',
          //   'navigationPath': '/pages/learning/blended-home',
          // },
          {
            'name': 'Nomination Instance',
            'navigationPath': '/pages/nomination-instance',
          },
        ]
      };
      this.backFlag = 1;

      this.detailsTab = false;
      this.codeOfConduct = false;
      this.nomineeTab = false;
      this.calendar = false;
      this.content = false;
      this.steps = true;
      this.enrolTab = false;
      this.steps_data = false;
      // this.generateRandom = Math.random();
    } else if (tabEvent.tabTitle === 'Nominees') {
      this.header = {
        title: this.contentData.instName!=""?this.contentData.instName:'Add Nomination',
        btnsSearch: true,
        btnBackshow: true,
        showBreadcrumb: true,
        breadCrumbList: [
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          // {
          //   'name': 'Blended Courses',
          //   'navigationPath': '/pages/learning/blended-home',
          // },
          {
            'name': 'Nomination Instance',
            'navigationPath': '/pages/nomination-instance',
          },
        ]
      };
      this.backFlag = 1;

      this.detailsTab = false;
      this.nomineeTab = true;
      this.calendar = false;
      this.content = false;
      this.steps = false;
      this.enrolTab = false;
      this.steps_data = false;
    } else if (tabEvent.tabTitle === 'Calendar') {
      this.header = {
        title: this.contentData.instName!=""?this.contentData.instName:'Add Nomination',
        btnsSearch: true,
        btnName8: 'Slot',
        btnName9: 'Add Slot',
        btnName10: 'Save',
        btnName11: 'Cancel',
        btnName8show: true,
        btnName9show: false,
        btnName10show: false,
        btnName11show: false,
        btnBackshow: true,
        showBreadcrumb: true,
        breadCrumbList: [
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          // {
          //   'name': 'Blended Courses',
          //   'navigationPath': '/pages/learning/blended-home',
          // },
          {
            'name': 'Nomination Instance',
            'navigationPath': '/pages/nomination-instance',
          },
        ]
      };
      this.backFlag = 1;
      this.spinner.show();
      setTimeout(() => {
        this.spinner.hide()
      }, 2000);
      this.detailsTab = false;
      this.nomineeTab = false;
      this.calendar = true;
      this.content = false;
      this.steps = false;
      this.enrolTab = false;
      this.steps_data = false;
    } else if (tabEvent.tabTitle === 'Content') {
      this.header = {
        title: this.contentData.instName!=""?this.contentData.instName:'Add Nomination',
        btnsSearch: true,
        btnBackshow: true,
        showBreadcrumb: true,
        breadCrumbList: [
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          // {
          //   'name': 'Blended Courses',
          //   'navigationPath': '/pages/learning/blended-home',
          // },
          {
            'name': 'Nomination Instance',
            'navigationPath': '/pages/nomination-instance',
          },
        ]
      };
      this.backFlag = 1;
      this.spinner.show();
      setTimeout(() => {
        this.spinner.hide()
      }, 2000);
      this.detailsTab = false;
      this.nomineeTab = false;
      this.calendar = false;
      this.content = true;
      this.steps = false;
      this.codeOfConduct = false;
      this.enrolTab = false;
      this.steps_data = false;
      // } else if (tabEvent.tabTitle === 'Steps Data') {
    } else if (tabEvent.tabTitle === 'Workflow') {
      this.header = {
        title: this.contentData.instName!=""?this.contentData.instName:'Add Nomination',
        btnsSearch: true,
        btnBackshow: true,
        showBreadcrumb: true,
        breadCrumbList: [
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          // {
          //   'name': 'Blended Courses',
          //   'navigationPath': '/pages/learning/blended-home',
          // },
          {
            'name': 'Nomination Instance',
            'navigationPath': '/pages/nomination-instance',
          },
        ]
      };
      this.backFlag = 1;
      this.detailsTab = false;
      this.nomineeTab = false;
      this.calendar = false;
      this.content = false;
      this.steps = false;
      this.enrolTab = false;
      this.steps_data = true;
    }
  }

  back() {
    if (this.backFlag == 2) {
      this.header = {
        title: this.contentData.instName!=""?this.contentData.instName:'Add Nomination',
        btnsSearch: true,
        btnName2: 'Enrol',
        btnName3: 'Bulk Enrol',
        btnName2show: true,
        btnName3show: true,
        btnBackshow: true,
        showBreadcrumb: true,
        breadCrumbList: [
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          // {
          //   'name': 'Blended Courses',
          //   'navigationPath': '/pages/learning/blended-home',
          // },
          {
            'name': 'Nomination Instance',
            'navigationPath': '/pages/nomination-instance',
          },
        ]
      };
      this.backFlag = 1;
      this.showEnrolpage = false;
      this.showBulkpage = false;
    }
    else {
      this.router.navigate(['../'], { relativeTo: this.routes });
    }
  }
  saveEnrol() {
    this.header = {
      title: this.contentData.instName!=""?this.contentData.instName:'Add Nomination',
      btnsSearch: true,
      btnName3: 'Bulk Enrol',
      btnName3show: true,
      btnBackshow: true,
      showBreadcrumb: true,
      breadCrumbList: [
        // {
        //   'name': 'Learning',
        //   'navigationPath': '/pages/learning',
        // },
        // {
        //   'name': 'Blended Courses',
        //   'navigationPath': '/pages/learning/blended-home',
        // },
        {
          'name': 'Nomination Instance',
          'navigationPath': '/pages/nomination-instance',
        },
      ]
    };
    this.backFlag = 2;
    // this.detailsTab = false;
    // this.modulesTab = false;
    // this.enrolTab = false;
    // this.engageTab = true;
    // this.rewardsTab = false;

    // this.courseEnrol = false;
    // this.courseEngage = true;
    this.showEnrolpage = true;
    this.showBulkpage = false;
  }
  bulkEnrol() {
    this.header = {
      title: this.contentData.instName!=""?this.contentData.instName:'Add Nomination',
      btnsSearch: true,
      btnName2: 'Enrol',
      btnName2show: true,
      btnBackshow: true,
      showBreadcrumb: true,
      breadCrumbList: [
        // {
        //   'name': 'Learning',
        //   'navigationPath': '/pages/learning',
        // },
        // {
        //   'name': 'Blended Courses',
        //   'navigationPath': '/pages/learning/blended-home',
        // },
        {
          'name': 'Nomination Instance',
          'navigationPath': '/pages/nomination-instance',
        },
      ]
    };
    this.backFlag = 2;
    this.showEnrolpage = false;
    this.showBulkpage = true;
  }
  backToEnrol() {

    this.showEnrolpage = false;
    this.showBulkpage = false;
    this.preview = false;
    // this.file = [];
    // this.preview = false;
    // this.fileName = 'Click here to upload an excel file to enrol employee to this course.';
    // this.enableUpload = false;
    // this.allEnrolUser(this.content);
  }
  ngOnDestroy() {
    this.blendedService.wfId = 0;
  }
  allEnrolUser() {
    // this.spinner.show();
    var data = {
      areaId: 29,
      instanceId: this.blendedService.wfId,
      tId: this.tenantId,
      mode: 0,
    };
    console.log(data);
    const allErnroledusers: string = webApi.domain + webApi.url.getAllEnrolUser;
    this.commonFunctionService.httpPostRequest(allErnroledusers, data)
      //this.addEditCourseService.getallenroluser(data)
      .then(enrolData => {
        // this.spinner.hide();
        this.enrolldata = enrolData['data'];
        this.rows = enrolData['data'];
        this.rows = [...this.rows];
        for (let i = 0; i < this.rows.length; i++) {
          // this.rows[i].Date = new Date(this.rows[i].enrolDate);
          // this.rows[i].enrolDate = this.formdate(this.rows[i].Date);
          if (this.rows[i].visible == 1) {
            this.rows[i].btntext = 'fa fa-eye';
          } else {
            this.rows[i].btntext = 'fa fa-eye-slash';
          }
        }
        this.cdf.detectChanges();
      });
  }
  searchEnrolUser(event) {
    const val = event.target.value.toLowerCase();

    // this.allEnrolUser( this.courseDataService.data.data)

    this.temp = [...this.enrolldata];
    console.log(this.temp);
    // filter our data
    this.searchText = val;
    if (val.length >= 3 || val.length == 0) {
      const temp = this.temp.filter(function (d) {
        return String(d.ecn).toLowerCase().indexOf(val) !== -1 ||
          d.fullname.toLowerCase().indexOf(val) !== -1 ||
          d.emailId.toLowerCase().indexOf(val) !== -1 ||
          d.enrolmode.toLowerCase().indexOf(val) !== -1 ||
          d.enrolDate.toLowerCase().indexOf(val) !== -1 ||
          d.phoneNo.toLowerCase().indexOf(val) !== -1 ||
          String(d.enrolmode).toLowerCase().indexOf(val) !== -1 ||
          // d.mode.toLowerCase() === val || !val;
          !val
      });

      // update the rows
      this.rows = [...temp];
      // Whenever the filter changes, always go back to the first page
      this.tableData.offset = 0;
    }
  }
  clear() {
    // this.temp = [...this.enrolldata];
    if (this.searchText.length >= 3) {
      this.rows = [...this.rows];
      this.allEnrolUser();

      this.searchvalue = {}
    }
    else {
      this.searchvalue = {}
    }

  }
  readUrl(event: any) {

    var validExts = new Array('.xlsx', '.xls');
    var fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
      // alert('Invalid file selected, valid files are of ' +
      //          validExts.toString() + ' types.');
      // var toast: Toast = {
      //   type: 'error',
      //   title: 'Invalid file selected!',
      //   body: 'Valid files are of ' + validExts.toString() + ' types.',
      //   showCloseButton: true,
      //   // tapToDismiss: false,
      //   timeout: 2000
      //   // onHideCallback: () => {
      //   //     this.router.navigate(['/pages/plan/users']);
      //   // }
      // };
      // this.toasterService.pop(toast);

      this.toastr.warning('Valid file types are ' + validExts.toString(), 'Warning', {
        closeButton: false
      })
      // setTimeout(data => {
      //   this.router.navigate(['/pages/users']);
      // },1000);
      // return false;
      this.cancel();
    } else {
      // return true;
      if (event.target.files && event.target.files[0]) {
        this.fileName = event.target.files[0].name;
        //read file from input
        this.fileReaded = event.target.files[0];

        if (this.fileReaded != '' || this.fileReaded != null || this.fileReaded != undefined) {
          this.enableUpload = true;
          this.showInvalidExcel = false;
        }

        this.file = event.target.files[0];

        this.bulkUploadData = event.target.files[0];
        console.log('this.bulkUploadData', this.bulkUploadData);

        this.xlsxToJsonService.processFileToJson({}, this.file).subscribe(data => {
          this.resultSheets = Object.getOwnPropertyNames(data['sheets']);
          console.log('File Property Names ', this.resultSheets);
          let sheetName = this.resultSheets[0];
          this.result = data['sheets'][sheetName];
          console.log('dataSheet', data);
          console.log('this.result', this.result);

          if (this.result.length > 0) {
            this.uploadedData = this.result;
          }
          console.log('this.uploadedData', this.uploadedData);
        });

        var reader = new FileReader();
        reader.onload = (event: ProgressEvent) => {
          let fileUrl = (<FileReader>event.target).result;
          // event.setAttribute('data-title', this.fileName);
          // console.log(this.fileUrl);
        }
        reader.readAsDataURL(event.target.files[0]);
      }
    }
  }
  cancel() {
    // this.fileUpload.nativeElement.files = [];
    console.log(this.fileUpload.nativeElement.files);
    this.fileUpload.nativeElement.value = '';
    console.log(this.fileUpload.nativeElement.files);
    this.fileName = 'Click here to upload an excel file to enrol ' + this.currentBrandData.employee.toLowerCase() + ' to this course'
    this.enableUpload = false;
    this.file = [];
    this.preview = false;
  }
  onEnrolDataClickEvent(data) {

  }
  uploadfile() {
    this.spinner.show();


    var fd = new FormData();
    this.contentData.areaId = 29;
    this.contentData.userId = this.userId;
    fd.append('content', JSON.stringify(this.contentData));
    fd.append('file', this.file);
    console.log(fd);
    this.addEditCourseService.TempManEnrolBulk(fd).then(result => {
      console.log(result);
      // this.loader =false;

      var res = result;
      try {
        if (res['data1'][0]) {
          this.resultdata = res['data1'][0];
          for (let i = 0; i < this.resultdata.length; i++) {
            this.resultdata[i].enrolDate = this.formdate(this.resultdata[i].enrolDate);
          }

          this.datasetPreview = this.resultdata;
          this.preview = true;
          this.cdf.detectChanges();
          this.spinner.hide();
        }
      } catch (e) {

        // var courseUpdate: Toast = {
        //   type: 'error',
        //   title: 'Enrol',
        //   body: 'Something went wrong.please try again later.',
        //   showCloseButton: true,
        //   timeout: 2000
        // };

        // this.closeEnableDisableCourseModal();
        // this.toasterService.pop(courseUpdate);

        this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
          timeOut: 0,
          closeButton: true
        });
      }
      this.spinner.hide();

    },
      resUserError => {
        // this.loader =false;
        this.spinner.hide();
        this.errorMsg = resUserError;
        // this.closeEnableDisableCourseModal();
      });
  }
  uploadfile1() {
    this.validdata = [];
    this.invaliddata = [];
    console.log('this.AllEmployess', this.AllEmployess);
    console.log('this.uploadedData', this.uploadedData);
    if (this.AllEmployess.length > 1) {
      // this.AllEmployess.some((item,i) => {
      this.uploadedData.forEach((data, j) => {
        if (!data.UserEcn) {
          this.uploadedData[j].status = 'Invalid';
          this.uploadedData[j].reason = 'ECN is required';
          return;
        } else if (!this.empobj[data.UserEcn]) {
          this.uploadedData[j].status = 'Invalid';
          this.uploadedData[j].reason = 'Invalid ECN';
          return;
        } else if (this.nomiObj[data.UserEcn]) {
          this.uploadedData[j].status = 'Invalid';
          this.uploadedData[j].reason = 'User already nominated';
          return;
        } else if (!data.EnrolDate) {
          this.uploadedData[j].status = 'Invalid';
          this.uploadedData[j].reason = 'Date is required ';
          return;
        } else if (!(/^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/g.test(data.date))) {
          this.uploadedData[j].status = 'Invalid';
          this.uploadedData[j].reason = 'Invalid date format.Use DD/MM/YYYY date format';
          return;
        } else {
          this.uploadedData[j].status = 'Valid';
          this.uploadedData[j].reason = '';
          return;
        }
      });

      console.log('this.uploadedData', this.uploadedData);
    }
    if (this.uploadedData.length > 0) {
      this.uploadedData.forEach(element => {
        if (element.status === 'Valid') {
          delete element.status;
          delete element.reason;
          element.date = element.date.split("/").reverse().join("-")
          this.validdata.push(element);
        } else {
          this.invaliddata.push(element);
        }
      });
      this.datasetPreview = this.resultdata;
      this.preview = true;
      this.cdf.detectChanges();
    }
    this.spinner.hide();
    if (this.invaliddata.length > 0) {
      this.showInvalidExcel = true;
    }
  }
  exportToExcelInvalid() {
    this.exportService.exportAsExcelFile(this.invaliddata, 'Invalid Data')
  }
  formdate(date) {

    if (date) {
      // const months = ["JAN", "FEB", "MAR","APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
      // var day = date.getDate();
      // var monthIndex = months[date.getMonth()];
      // var year = date.getFullYear();

      // return day + '_' + monthIndex + '_' +year;
      var formatted = this.datePipe.transform(date, 'dd-MM-yyyy');
      return formatted;
    }
  }
  // savebukupload() {
  //   var data = {
  //     userId: this.contentData.userId,
  //     coursId: this.contentData.courseId,
  //     areaId: 2,
  //     tId: this.tenantId,
  //   }
  //   // this.loader = true;
  //   this.spinner.show();
  //   this.addEditCourseService.finalManEnrolBulk(data)
  //     .then(rescompData => {
  //       // this.loader =false;
  //       this.spinner.hide();
  //       var temp: any = rescompData;
  //       this.addEditModRes = temp.data[0];
  //       if (temp == 'err') {
  //         // var modUpdate: Toast = {
  //         //   type: 'error',
  //         //   title: 'Enrol',
  //         //   body: 'Something went wrong',
  //         //   showCloseButton: true,
  //         //   timeout: 2000
  //         // };
  //         // this.toasterService.pop(modUpdate);

  //         this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
  //           timeOut: 0,
  //           closeButton: true,
  //         });
  //       } else if (temp.type == false) {
  //         // var modUpdate: Toast = {
  //         //   type: 'error',
  //         //   title: 'Enrol',
  //         //   body: this.addEditModRes[0].msg,
  //         //   showCloseButton: true,
  //         //   timeout: 2000
  //         // };
  //         // this.toasterService.pop(modUpdate);

  //         this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
  //           timeOut: 0,
  //           closeButton: true
  //         });
  //       } else {
  //         // var modUpdate: Toast = {
  //         //   type: 'success',
  //         //   title: 'Enrol',
  //         //   body: this.addEditModRes[0].msg,
  //         //   showCloseButton: true,
  //         //   timeout: 2000
  //         // };
  //         // this.toasterService.pop(modUpdate);

  //         this.toastr.success(this.addEditModRes[0].msg, 'Success', {
  //           closeButton: false
  //         });
  //         this.backToEnrol()
  //         this.cdf.detectChanges();
  //         this.preview = false;

  //       }
  //       console.log('Enrol bulk Result ', this.addEditModRes);
  //       this.cdf.detectChanges();
  //     },
  //       resUserError => {
  //         // this.loader = false;
  //         this.errorMsg = resUserError;
  //       });
  // }
  // getRowClass(row){
  //   return {
  //     'recert_row': row.isRecert === 1,
  //   };
  // }
  visibilityTableRow(row) {
    let value;
    let status;

    if (row.visible == 1) {
      row.btntext = 'fa fa-eye-slash';
      value = 'fa fa-eye-slash';
      row.visible = 0
      status = 0;
    } else {
      status = 1;
      value = 'fa fa-eye';
      row.visible = 1;
      row.btntext = 'fa fa-eye';
    }

    for (let i = 0; i < this.rows.length; i++) {
      if (this.rows[i].employeeId == row.employeeId) {
        this.rows[i].btntext = row.btntext;
        this.rows[i].visible = row.visible
      }
    }
    var visibilityData = {
      employeeId: row.employeeId,
      visible: status,
      courseId: this.contentData.courseId,
      tId: this.tenantId,
      aId: 29,
    }
    this.enrolService.disableEnrol(visibilityData).then(result => {
      console.log('result', result);
      // this.loader = false;
      this.spinner.hide();
      this.resultdata = result;
      if (this.resultdata.type == false) {
        // var courseUpdate: Toast = {
        //   type: 'error',
        //   title: "Course",
        //   body: "Unable to update visibility of User.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // // this.closeEnableDisableCourseModal();
        // this.toasterService.pop(courseUpdate);

        this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
          timeOut: 0,
          closeButton: true
        });
      } else {
        // var courseUpdate: Toast = {
        //   type: 'success',
        //   title: "Course",
        //   body: this.resultdata.data,
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // row.visible = !row.visible;
        console.log("after", row.visible)
        this.allEnrolUser();
        // this.toasterService.pop(courseUpdate);

        this.toastr.success(this.resultdata.data, 'Success', {
          closeButton: false
        });
      }
    },

      resUserError => {
        this.spinner.hide();
        this.errorMsg = resUserError;
        // this.closeEnableDisableCourseModal();
      });

    console.log('row', row);
  }
  disableCourseVisibility(row, status) {
    console.log(row, status);
    this.spinner.show();
    var visibilityData = {
      employeeId: row.employeeId,
      visible: status,
      courseId: this.contentData.courseId,
      tId: this.tenantId,
      aId: 29,
    }
    this.enrolService.disableEnrol(visibilityData).then(result => {
      console.log('result', result);
      // this.loader = false;
      this.spinner.hide();
      this.resultdata = result;
      if (this.resultdata.type == false) {
        // var courseUpdate: Toast = {
        //   type: 'error',
        //   title: "Course",
        //   body: "Unable to update visibility of User.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // // this.closeEnableDisableCourseModal();
        // this.toasterService.pop(courseUpdate);

        this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
          timeOut: 0,
          closeButton: true
        });
      } else {
        // var courseUpdate: Toast = {
        //   type: 'success',
        //   title: "Course",
        //   body: this.resultdata.data,
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // row.visible = !row.visible;
        console.log("after", row.visible)
        this.allEnrolUser();
        // this.toasterService.pop(courseUpdate);

        this.toastr.success(this.resultdata.data, 'Success', {
          closeButton: false
        });
      }
    },

      resUserError => {
        this.spinner.hide();
        this.errorMsg = resUserError;
        // this.closeEnableDisableCourseModal();
      });


  }

  savebukupload() {
    // this.loader = true;
    if (this.uploadedData.length > 0 && this.uploadedData.length <= 2000) {
      this.spinner.show();

      // var option: string = Array.prototype.map
      //   .call(this.uploadedData, function (item) {
      //     console.log("item", item);
      //     return Object.values(item).join("#");
      //   })
      //   .join("|");
      // New Function as per requirement.

      var option: string = Array.prototype.map
        .call(this.uploadedData, (item) => {
          const array = Object.keys(item);
          let string = '';
          array.forEach(element => {
            if (element === 'EnrolDate') {
              string += this.commonFunctionService.formatDateTimeForBulkUpload(item[element]);
            } else {
              string = item[element] + '#';
            }
          });
          // console.log("item", item);
          // return Object.values(item).join("#");
          return string;
        })
        .join("|");



      //  console.log('this.courseDataService.workflowId', this.courseDataService.workflowId);

      const data = {
        wfId: null,
        aId: 29,
        iId: this.contentData.courseId,
        // userId: this.content.userId,
        upString: option
      }
      const _urlAreaBulEnrol: string = webApi.domain + webApi.url.area_bulk_enrol;
      this.commonFunctionService.httpPostRequest(_urlAreaBulEnrol, data)
        //console.log('data', data);
        //this.addEditCourseService.areaBulkEnrol(data)
        .then(rescompData => {
          // this.loader =false;
          this.spinner.hide();
          var temp: any = rescompData;
          this.addEditModRes = temp.data;
          if (temp == 'err') {
            this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
              timeOut: 0,
              closeButton: true
            });
          } else if (temp.type == false) {
            this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
              timeOut: 0,
              closeButton: true
            });
          } else {
            this.enableUpload = false;
            this.fileName = 'Click here to upload an excel file to enrol ' + this.currentBrandData.employee.toLowerCase() + ' to this course'
            if (temp['data'].length > 0) {
              if (temp['data'].length === temp['invalidCnt'].invalidCount) {
                this.toastr.warning('No valid ' + this.currentBrandData.employee.toLowerCase() + 's found to enrol.', 'Warning', {
                  closeButton: false
                });
              } else {
                this.toastr.success('Enrol successfully.', 'Success', {
                  closeButton: false
                });
                this.allEnrolUser();
              }
              this.invaliddata = temp['data'];
              this.showInvalidExcel = true;

              // this.invaliddata = temp['data'];
              // if (this.uploadedData.length == this.invaliddata.length) {
              //   this.toastr.warning('No valid employees found to enrol.', 'Warning', {
              //     closeButton: false
              //     });
              // } else {
              //   this.toastr.success('Enrol successfully.', 'Success', {
              //     closeButton: false
              //     });
              // }
            } else {
              this.invaliddata = [];
              this.showInvalidExcel = false;
            }




            // this.backToEnrol()
            this.cdf.detectChanges();
            this.preview = false;
            this.showInvalidExcel = true;

          }
          console.log('Enrol bulk Result ', this.addEditModRes);
          this.cdf.detectChanges();
        },
          resUserError => {
            //this.loader = false;
            this.errorMsg = resUserError;
          });
    } else {
      if (this.uploadedData.length == 0) {
        this.toastr.warning('No data found.', 'warning', {
          closeButton: true
        });
      } else {
        this.toastr.warning('File data can not exceed more than 2000.', 'warning', {
          closeButton: true
        });
      }
    }

  }

  passData(val) {
    this.isdata = val;
    // this.eventsSubject.next();
    if (val === 'slot') {
      this.header = {
        title: this.contentData.instName!=""?this.contentData.instName:'Add Nomination',
        btnsSearch: true,
        searchBar: false,
        searchtext: '',
        dropdownlabel: ' ',
        drplabelshow: false,
        drpName1: '',
        drpName2: ' ',
        drpName3: '',
        drpName1show: false,
        drpName2show: false,
        drpName3show: false,
        btnName1: '',
        btnName2: '',
        btnName3: '',
        btnName4: '',
        btnName5: '',
        btnName6: '',
        btnName7: '',
        btnName8: 'Slot',
        btnName9: 'Add Slot',
        btnName10: 'Save',
        btnName11: 'Cancel',
        btnAdd: '',
        btnName1show: false,
        btnName2show: false,
        btnName3show: false,
        btnName4show: false,
        btnName5show: false,
        btnName6show: false,
        btnName7show: false,
        btnName8show: false,
        btnName9show: true,
        btnName10show: true,
        btnName11show: true,
        btnBackshow: true,
        btnAddshow: false,
        filter: false,
        showBreadcrumb: true,
        breadCrumbList: [
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          // {
          //   'name': 'Blended Courses',
          //   'navigationPath': '/pages/learning/blended-home',
          // },
          {
            'name': 'Nomination Instance',
            'navigationPath': '/pages/nomination-instance',
          },
        ]
      };
    } else if (val === 'cancel') {
      this.header = {
        title: this.contentData.instName!=""?this.contentData.instName:'Add Nomination',
        btnsSearch: true,
        searchBar: false,
        searchtext: '',
        dropdownlabel: ' ',
        drplabelshow: false,
        drpName1: '',
        drpName2: ' ',
        drpName3: '',
        drpName1show: false,
        drpName2show: false,
        drpName3show: false,
        btnName1: '',
        btnName2: '',
        btnName3: '',
        btnName4: '',
        btnName5: '',
        btnName6: '',
        btnName7: '',
        btnName8: 'Slot',
        btnName9: 'Add Slot',
        btnName10: 'Save',
        btnName11: 'Cancel',
        btnAdd: '',
        btnName1show: false,
        btnName2show: false,
        btnName3show: false,
        btnName4show: false,
        btnName5show: false,
        btnName6show: false,
        btnName7show: false,
        btnName8show: true,
        btnName9show: false,
        btnName10show: false,
        btnName11show: false,
        btnBackshow: true,
        btnAddshow: false,
        filter: false,
        showBreadcrumb: true,
        breadCrumbList: [
          // {
          //   'name': 'Learning',
          //   'navigationPath': '/pages/learning',
          // },
          // {
          //   'name': 'Blended Courses',
          //   'navigationPath': '/pages/learning/blended-home',
          // },
          {
            'name': 'Nomination Instance',
            'navigationPath': '/pages/nomination-instance',
          },
        ]
      };
    }
    setTimeout(() => {
      this.isdata = '';
    }, 2000);
  }
}
