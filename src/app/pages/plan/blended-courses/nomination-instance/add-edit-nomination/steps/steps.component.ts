import { Component, OnInit, AfterViewInit, ChangeDetectorRef, OnChanges, Input, SimpleChanges } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ContentService } from './../content.service';
import { DatePipe } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { BlendedService } from '../../../blended.service';
import { AddEditNominationComponent } from '../add-edit-nomination.component';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
import * as _moment from 'moment';
import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';

const moment = (_moment as any).default ? (_moment as any).default : _moment;

export const MY_CUSTOM_FORMATS = {
  fullPickerInput: 'DD-MM-YYYY HH:mm:ss',
    parseInput: 'DD-MM-YYYY HH:mm:ss',
    datePickerInput: 'DD-MM-YYYY',
    timePickerInput: 'LT',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY'
};
@Component({
  selector: 'steps',
  templateUrl: './steps.component.html',
  styleUrls: ['./steps.component.scss'],
  providers: [
    {provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE]},
    {provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS},]
})
export class StepsComponent implements OnInit, OnChanges {
  loginUserdata: any;
  serviceData: any = [];
  wfId: any;
  @Input() DataChanged: any;
  modules: any = [];
  programs: any = [];
  tempPrograms:any =[];
  categories: any = [];
  minDate =new Date();
  maxDate :any;
  @Input() inpdata: any;

  constructor(private contentService: ContentService, private datepipe: DatePipe,private spinner: NgxSpinnerService,
    // private toasterService: ToasterService,
    public router: Router,public routes: ActivatedRoute,
    private blendedService: BlendedService,private cdf: ChangeDetectorRef,
    public AddEditNominationComponent: AddEditNominationComponent, private toastr: ToastrService,
    private http1: HttpClient) {
      this.getHelpContent();

      if (localStorage.getItem('LoginResData')) {
        this.loginUserdata = JSON.parse(localStorage.getItem('LoginResData'));
      }

      if (this.blendedService.program) {
        this.serviceData = this.blendedService.program;
        this.minDate = this.serviceData.sDate;
        this.maxDate = this.serviceData.eDate;
      }
      console.log('this.serviceData', this.serviceData);
      if (this.blendedService.wfId) {
        this.wfId = this.blendedService.wfId;
        console.log('coc wfId', this.wfId);
      }
      if(this.serviceData) {
        this.getCourseActivities();

      }

    console.log('lastEndDate', this.lastEndDate);
   }

  ngOnInit() {
  }

  getCourseActivities() {
    this.spinner.show();
    const param = {
      'courseId': this.serviceData.eCsrId,
      'tId': this.loginUserdata.data.data.tenantId,
    };
    console.log('getCourseActivitiesParam', param);
    this.contentService.getCourseActivities(param)
    .then(res => {
      this.spinner.hide();
     const result = res;
      if (result['type'] === false) {
        this.presentToast('error', '');
      } else {
        console.log('courseActivities:', result);
        this.activities = result['activities'];
        this.modules = result['modules'];
        this.programs = result['programs'];
        this.categories = result['categories'];
      }
    },
      error => {
        this.spinner.hide();
        this.presentToast('error', '');
      });
  }

  getWorkflowSteps() {
    this.spinner.show();
    const param = {
      'wfId': this.blendedService.wfId,
      'tId': this.loginUserdata.data.data.tenantId,
    };

    this.contentService.getAllWorkflowSteps(param)
    .then(res => {
      this.spinner.hide();
     const result = res;
      if (result['type'] === false) {
        this.presentToast('error', '');
      } else {
        console.log('steps:', result['data']);
        this.steps = result['data'];
        this.steps.forEach((item)=>{
          if (item['isCategoryEnable']== '1' && item['isProgramEnable'] == 1) {
            if(item.categoryId != null){
              this.onChange(item.categoryId, item);
            }
          }
        });
      }
    },
      error => {
        this.spinner.hide();
        this.presentToast('error', '');
      });
  }

  stepForm: NgForm;

  // activityDropDown = [ "Assessment", "adbfkja", "dmfkjandslkjn", "ajkdjsnfkjcndsa"]
  activities: any = [];

  steps: any = [];

  onChecked(event: Event) {
    let data = (<HTMLInputElement>event.target).checked;
    console.log(data);

    // if (this.SelectValue.length === 0  || !data) {
    //   this.reviewFormData.ReviewText = false;
    // } else {
    //   this.reviewFormData.ReviewText = true;

    // }
  }

  minDateto : Date ;
  lastEndDate: Date = new Date();

	onSearchChange(data){
    this.minDateto = data;
}

greaterThanHundred: boolean = false;
inputValue: number;
getWeightage(event : any, stepForm: NgForm){
  if (event.target.value < 0) {
    event.target.value = 0;
  }
  if (event.target.value > 100) {
    event.target.value = 100;
  }
}

cnt: number = 0;
weightNotMatch: boolean = false;
checkedSteps: any = [];
  onSubmit(){

   this.checkedSteps = [];
    console.log('this.cntOnSubmit', this.cnt);
    console.log(this.steps);
    this.cnt = 0;

    this.steps.forEach(element => {
      if (element.checked) {
        element.startDate = this.formatDate(element.startDate);
        element.endDate = this.formatDate(element.endDate);
        this.checkedSteps.push(element);
      }
    });
    if(!this.checkforValidityForStep()) {
      this.toastr.warning('Please fill all the values', 'warning');
      this.spinner.hide();
      this.DataChanged = false
      return null;
    }
    this.checkedSteps.forEach(element => {
      if(element.weightage){
        this.cnt = this.cnt + Number(element.weightage);
      }
    });
    console.log('cnt', this.cnt);

    if(this.cnt == 100) {
      const stepsStr = this.prepareData(this.checkedSteps);
      console.log('checkedSteps', this.checkedSteps);
      this.spinner.show();
      const param = {
        'wfId': this.blendedService.wfId,
        'steps': stepsStr,
        'tId': this.loginUserdata.data.data.tenantId,
        'ucrId': this.loginUserdata.data.data.id,
      };
      console.log('stepsParam', param);
      this.contentService.insertWorkflowSteps(param)
      .then(res => {
        this.spinner.hide();
       const result = res;
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          this.presentToast('success', 'Steps added');
          this.getWorkflowSteps();
          // this.router.navigate(['pages/learning/blended-home/nomination-instance']);
           //this.TabSwitcher();
           const DataTab={
            tabTitle:'Enrol',
          }
          this.AddEditNominationComponent.selectedTab(DataTab);
          this.cdf.detectChanges();
        }
      },
        error => {
          this.spinner.hide();
          this.presentToast('error', '');
        });

    } else {
      this.spinner.hide();
      this.weightNotMatch = true;
    }
  }

  TabSwitcher(){
    this.AddEditNominationComponent.tabEnabler.steps = false;
    this.AddEditNominationComponent.tabEnabler.enrolTab = true;
    this.AddEditNominationComponent.tabEnabler.codeOfConduct = false;
    this.AddEditNominationComponent.tabEnabler.content = false;
    this.AddEditNominationComponent.tabEnabler.nomineeTab = false;
    this.AddEditNominationComponent.tabEnabler.detailsTab = false;
    console.log(this.AddEditNominationComponent);
  }

  checkforValidityForStep(){
    // this.checkedSteps.forEach((data) => {
    //     if(data['startDate'] === 'NaN-NaN-NaN' || data['endDate'] === 'NaN-NaN-NaN'){

    //     }
    // });
    for (let i = 0; i < this.checkedSteps.length; ++i) {
      if (this.checkedSteps[i]['startDate'] === 'NaN-NaN-NaN' || this.checkedSteps[i]['endDate'] === 'NaN-NaN-NaN'){
        return false;
      }
      if (this.checkedSteps[i]['isActivityEnable'] === 1 &&  this.checkedSteps[i]['activityId'] === 0){
        return false;
      }
      if (this.checkedSteps[i]['isModuleEnable'] === 1 &&  this.checkedSteps[i]['moduleId'] === 0){
        return false;
      }
      if (this.checkedSteps[i]['isProgramEnable'] === 1 &&  this.checkedSteps[i]['programId'] === 0){
        return false;
      }
      if (this.checkedSteps[i]['isCategoryEnable'] === 1 &&  this.checkedSteps[i]['categoryId'] === 0){
        return false;
      }
   }
   return true;
  }

  prepareData(steps) {
    let finalStr: any;
    console.log('prepareDatacheckedSteps', this.checkedSteps);
    for (let i = 0; i < this.checkedSteps.length; i++) {
      const act =  this.checkedSteps[i].activityId  == '' ||  this.checkedSteps[i].activityId  == null ?
                   0 : this.checkedSteps[i].activityId;
      const isRecertEnable = this.checkedSteps[i].isRecert ? 1 : 0;
      const mod = this.checkedSteps[i].moduleId  == '' ||  this.checkedSteps[i].moduleId  == null ?
               0 : this.checkedSteps[i].moduleId;
               console.log('mod', mod);
      const categoryId = this.checkedSteps[i].categoryId  == '' ||  this.checkedSteps[i].categoryId  == null ?
      0 : this.checkedSteps[i].categoryId;
      const programId = this.checkedSteps[i].programId  == '' ||  this.checkedSteps[i].programId  == null ?
      0 : this.checkedSteps[i].programId;
      const str = this.checkedSteps[i].id + '#' +  this.checkedSteps[i].weightage + '#' + this.checkedSteps[i].startDate
       + '#' + this.checkedSteps[i].endDate + '#' + act + '#' + isRecertEnable + '#' + mod + '#' + programId +'#'+ categoryId;

      if ( i === 0) {
        finalStr = str;
      }else {
        finalStr = finalStr + '|' + str;
      }
    }
    console.log('finalStr', finalStr);
    return finalStr;
  }

  formatDate(date) {
    if(date){
      const d = new Date(date);
      const formatSendDate = d.getFullYear() +  '-' + (d.getMonth() + 1 ) + '-' + d.getDate();
      // const formatted = this.datepipe.transform(d, 'yyyy-MM-dd');
      return formatSendDate;
    }else {
      return 'NaN-NaN-NaN';
    }
  }

  closeModal() {
    this.weightNotMatch = false;
    this.cnt = 0;
    console.log('cntClose', this.cnt);
    this.checkedSteps = [];
  }

  presentToast(type, body) {
    if(type === 'success'){
      this.toastr.success(body, 'Success', {
        closeButton: false
       });
    } else if(type === 'error'){
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else{
      this.toastr.warning(body, 'Warning', {
        closeButton: false
        })
    }
  }

     // Help Code Start Here //

     helpContent: any;
     getHelpContent() {
       return new Promise(resolve => {

         this.http1.get('../../../../../../assets/help-content/addEditCourseContent.json').subscribe(
           data => {
             this.helpContent = data;
             console.log('Help Array', this.helpContent);
           },
           err => {
             resolve('err');
           },
         );
       });
       // return this.helpContent;
     }


     // Help Code Ends Here //
     ngOnChanges(changes: SimpleChanges): void{
      if(this.inpdata === 'savesett') {
        this.onSubmit();
      } else if (this.DataChanged) {
        this.getCourseActivities();
        this.getWorkflowSteps();
      }
    }
    disabledProgram = true;
    onChange(value, item) {
      // console.log(newValue);
      console.log(value);
      if(value == 0){
        this.disabledProgram = true;
        item.categoryId = 0;
      }else{

        this.tempPrograms = this.programs.filter(function(obj) {
          return obj.categoryId == value;
        });
        console.log('programs List ==>', this.tempPrograms);
        this.disabledProgram = false;
      }
      // this.selectedDevice = newValue;
      // ... do other stuff here ...
  }
}
