import { Component, OnInit, ViewChild, ViewEncapsulation, Input, OnChanges } from '@angular/core';
import { ContentService } from './../content.service';
import { DatePipe } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { ToastrService } from 'ngx-toastr';
import { BlendedService } from '../../../blended.service';
import { AddEditNominationComponent } from '../add-edit-nomination.component';
import { DomSanitizer } from '@angular/platform-browser';
//import { Domain } from 'domain';
// import { webAPIService } from '../../../../../../service/webAPIService';
import { webApi } from '../../../../../../service/webApi';
import * as _moment from 'moment';
import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { noData } from '../../../../../../models/no-data.model';

const moment = (_moment as any).default ? (_moment as any).default : _moment;

export const MY_CUSTOM_FORMATS = {
  fullPickerInput: 'DD-MM-YYYY HH:mm:ss',
    parseInput: 'DD-MM-YYYY HH:mm:ss',
    datePickerInput: 'DD-MM-YYYY',
    timePickerInput: 'LT',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY'
};
@Component({
  selector: 'nominee-details',
  templateUrl: './nominee-details.component.html',
  styleUrls: ['./nominee-details.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    {provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE]},
    {provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS},]
})
export class NomineeDetailsComponent implements OnInit, OnChanges {
  @Input() DataChanged;
  searchString: string = '';

  steps: [
    {
      id: 1,
      name: "Nomination Details",
    },
    {
      id: 2,
      name: "BH Approval",
    },
    {
      id: 3,
      name: "Evaluation Call slot selection",
    },
    {
      id: 4,
      name: "Feedback",
    },
    {
      id: 5,
      name: "Assessment Details",
    },
    {
      id: 6,
      name: "Webinar Call",
    },
    {
      id: 7,
      name: "FG Assessment",
    },
    {
      id: 8,
      name: "Course Assigned",
    },
    {
      id: 9,
      name: "Certificate",
    }
  ]
  nominees: any = [];
  tempNominees: any = [];
  showTotal = false;
  showVideoDataContent;
  showVideoPopup = false;

  nominee_Detail: any = [
    {
      ecn: 111,
      full_name: "Shirish Sharma",
      phone: 382975080,
      email: "shirish@gmail.com",
      band: 2134,
      business: "NA",
      subDept: "Human Resource",
      reporting_Manager: "Abc",
      alternate_phone: 444448929,
      trainer_before: false,
      profile_pic: "assets/images/alan.png"
    },
    {
      ecn: 112,
      full_name: "Padmaja Pednekar",
      phone: 3297492020,
      email: "padmajaP12@gmail.com",
      band: 4235,
      business: "NA",
      subDept: "Risk",
      reporting_Manager: "Abc",
      alternate_phone: 444448929,
      trainer_before: true,
      profile_pic: "assets/images/lee.png"
    },
    {
      ecn: 113,
      full_name: "Shivraj Singh",
      phone: 382975080,
      email: "shivraj@gmail.com",
      band: 2134,
      business: "NA",
      subDept: "Collection",
      reporting_Manager: "Abc",
      alternate_phone: 444448929,
      trainer_before: false,
      profile_pic: "assets/images/jack.png"
    },
    {
      ecn: 114,
      full_name: "Anirudhsingh Rathod",
      phone: 382975080,
      email: "anirudh@gmail.com",
      band: 2134,
      business: "NA",
      subDept: "Insurance",
      reporting_Manager: "Abc",
      alternate_phone: 444448929,
      trainer_before: true,
      profile_pic: "assets/images/nick.png"
    },
    {
      ecn: 115,
      full_name: "Pushpadeep Shetty",
      phone: 382975080,
      email: "pushpadeep@gmail.com",
      band: 2134,
      business: "NA",
      subDept: "Human Resource",
      reporting_Manager: "Abc",
      alternate_phone: 444448929,
      trainer_before: false,
      profile_pic: "assets/images/alan.png"
    },
  ];
	noDataVal:noData={
		margin:'mt-3',
		imageSrc: '../../../../../assets/images/no-data-bg.svg',
		title:"No Nominees at this time.",
		desc:"",
		titleShow:true,
		btnShow:false,
		descShow:false,
		btnText:'Learn More',
		btnLink:'https://faq.edgelearning.co.in/kb/learning-how-to-add-notification-to-a-batch',
	  }
  todayDate = new Date();
  approvalStatus: boolean;

  BHapproval: any = [
    { ecn: 111, BHName: "Bhakti Joshi", status: 1, onDate: this.todayDate, onTime: this.todayDate, comments: "dcalnasnkdnasncln" },
    { ecn: 112, BHName: "Bhakti Joshi", status: 0, onDate: null, onTime: null, comments: "" },
    { ecn: 113, BHName: "Bhakti Joshi", status: 0, onDate: null, onTime: null, comments: "" },
    { ecn: 114, BHName: "Bhakti Joshi", status: 1, onDate: this.todayDate, onTime: this.todayDate, comments: "asdfcdhabkjfb" },
    { ecn: 115, BHName: "Bhakti Joshi", status: 1, onDate: this.todayDate, onTime: this.todayDate, comments: "abdkjcfhkjdb" },
  ];

  assessment: any = [];
  webinarList: any = [];
  webinarListNotFound = false;

  FGassessment: any = [
    // { id: 1, question: "gbjkakvlknlak ", answer: "Option A" },
    // { id: 2, question: "gbjkakvlknlak", answer: "Option A" },
    // { id: 3, question: "gbjkakvlknlak ", answer: "Option A" },
    // { id: 4, question: "gbjkakvlknlak ", answer: "Option A" },
    // { id: 5, question: "gbjkakvlknlak ", answer: "Option A" },
    // { id: 6, question: "gbjkakvlknlak ", answer: "Option A" },
    // { id: 7, question: "gbjkakvlknlak ", answer: "Option A" },
    // { id: 8, question: "gbjkakvlknlak ", answer: "Option A" },
    // { id: 9, question: "gbjkakvlknlak ", answer: "Option A" },
    // { id: 10, question: "gbjkakvlknlak ", answer: "Option A" }
  ];

  feedback: any = [];
  feedbackShow: boolean;
  certificateContent;
  showDownloadCertificate = false;
  showDownloadList = false;
  evaluationCallDetails: any =
    {
      // trainerName: "Padmaja Pednekar",
      // date: this.todayDate,
      // fromTime: this.todayDate,
      // toTime: this.todayDate
    };

  courseDetail: any =
    {
      courseName: "Padmaja Pednekar",
      venue: "Pune",
      date: this.todayDate,
      time: this.todayDate
    };
  webinar: any = {
    trainer : '0',
  };


  loginUserdata: any;
  serviceData: any;
  wfId: any;
  certificateDownloadUrl: string;
  searchText: any;
  constructor(private contentService: ContentService, private datepipe: DatePipe, private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    // private toasterService: ToasterService,
    public router: Router, public routes: ActivatedRoute,
    private blendedService: BlendedService,private sanitizer: DomSanitizer) {
     // this.showReason = true;
    if (localStorage.getItem('LoginResData')) {
      this.loginUserdata = JSON.parse(localStorage.getItem('LoginResData'));
    }

    if (this.blendedService.program) {
      this.serviceData = this.blendedService.program;
    }
    if (this.blendedService.wfId) {
      this.wfId = this.blendedService.wfId;
    }

    if (this.blendedService.program) {
      this.getAllNominees();
      this.trainerlist();
    }
  }
  noEmpEnrol: boolean = false;
  trainers: any = [];
  trainerlist() {
    const data = {
      tId: this.loginUserdata.data.data.tenantId,
    };
    this.blendedService.trainerList(data).then(res => {
      if (res['type'] === true) {
        try {
          this.trainers = res['data'];
        } catch (e) {
          console.log(e);
        }
      }
      console.log('this.trainers', this.trainers);
    }, err => {
      console.log(err);
    });
  }

  minDateto: Date;

  validateEndDate(data) {
    this.minDateto = data;
  }
  getAllNominees() {
    this.spinner.show();
    const param = {
      'wfId': this.blendedService.wfId,
      'tId': this.loginUserdata.data.data.tenantId,
    };
    this.contentService.getAllNominees(param)
      .then(res => {
        this.spinner.hide();
        const result = res;
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          console.log('nominees:', result['data']);
          const nomArr = result['data'];
          if (nomArr.length === 0) {
            this.noEmpEnrol = true;
            console.log('noEmpEnrol', this.noEmpEnrol);
          } else {
            this.noEmpEnrol = false;
            // nomArr.forEach(element => {
            //   if (element.workflow) {
            //     element.workflow = element.workflow.split(',');
            //   }

            //   // element.activityIds = element.activityIds.split(',');
            // });
            // console.log('nomArr', nomArr);
            this.nominees = nomArr;
            this.tempNominees = nomArr;
            this.insertingClickablePropertyInObject(nomArr);
            console.log(this.nominees);
          }

          //this.steps = result['data'];
        }
      },
        error => {
          this.spinner.hide();
          this.presentToast('error', '');
        });
  }

  presentToast(type, body) {
    if(type === 'success'){
      this.toastr.success(body, 'Success', {
        closeButton: false
       });
    } else if(type === 'error'){
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else{
      this.toastr.warning(body, 'Warning', {
        closeButton: false
        })
    }
  }

  certificate: any = {
    certificatePath: "assets/images/noImage2.png",
    status: true,
    date: this.todayDate,
    approver: "Approver Name"
  }

  ngOnInit() {
  }

  stepSelected: any;
  selectedRow: any
  showDetail: boolean = false;
  stepSelectedOrder: any;
  activityId: any;
  previousRowIndex: any;
  showRowDetail(rowIndex, stepIndex, rowData, step) {
    this.BHApproved = {};
    console.log("row index >>", rowIndex);
    console.log("step index >>", stepIndex);
    console.log("row Data >>", rowData);
    console.log("step  >>", step);
    this.stepSelectedOrder = stepIndex;
    if (this.showDetail && this.stepSelected != step.stepId) {
      this.stepFunctionToCallServices(rowData.empId, step);
      this.showDetail = true;
      this.stepSelected = step.stepId;
      this.selectedRow = rowIndex;
    } else if (this.showDetail && this.selectedRow == rowIndex) {
      this.showDetail = false;
    } else {
      this.stepFunctionToCallServices(rowData.empId, step);
      this.stepSelected = step.stepId;
      this.selectedRow = rowIndex;
    }

  }

  stepFunctionToCallServices(empId, step) {
    switch (parseInt(step.stepId)) {
      case 1: this.getNomineeDetails(empId, step);
        break;
      case 2: this.getBhapprovalStatus(empId, step);
        break;
      case 3: this.getEvaluationCallDetail(empId, step);
        break;
      // case 4: null;
      //   break;
      case 4: this.getWebinarEventByEmpId(empId, step);
        break;
      case 5: this.getObserverDetail(empId, step);
        break;
      case 6: this.getFgAssessmentDetails(empId, step);
        break;
      case 7: this.getKnowledgeTestDetails(empId, step);
        break;
      case 8: this.getVivaDetails(empId, step);
        break;
      case 9: this.getBatchDetail(empId);
        break;
      case 10: this.getMockDetails(empId, step);
        break;
      case 11: this.getAssesmentDetails(empId, step);;
        break;
      case 12: this.getCertificateDetails(empId);
        break;
      case 14: this.getMockDetails(empId, step);
        break;
      case 15: this.getAssesmentDetails(empId, step);
        break;
      default: return;
    }
  }

  getNomineeDetails(empId, step) {
    this.spinner.show();
    const param = {
      'wfId': this.blendedService.wfId,
      'empId': empId,
      'tId': this.loginUserdata.data.data.tenantId,
    };
    console.log('nomineeDetailsParams:', param, empId, step);
    this.contentService.getNomineeDetails(param)
      .then(res => {
        this.spinner.hide();
        const result = res;
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          console.log('nominee Details:', result['data']);
          const nomDetails = result['data'][0];
          this.employee_Detail = nomDetails;
          this.showDetail = true;
          this.stepSelected = step.stepId;
          console.log('nomDetails', nomDetails);
        }
      },
        error => {
          this.spinner.hide();
          this.presentToast('error', '');
        });
  }

  approveBtn: boolean = false;
  rejectBtn: boolean = false;
  getBhapprovalStatus(empId, step) {
    this.spinner.show();
    const param = {
      'wfId':this.blendedService.wfId,
      'empId': empId,
      'tId': this.loginUserdata.data.data.tenantId,
    };
    console.log('bhApprovalParams:', param, empId, step);
    this.contentService.getBhApprovalStatus(param)
      .then(res => {
        this.spinner.hide();
        const result = res;
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          console.log('BH Approval:', result['data']);
          if (result['data'].length > 0) {
            var bhstatus = result['data'][0];
          }

          if (bhstatus) {
            if (bhstatus.approveStatus == 1) {
              this.approveBtn = false;
              this.rejectBtn = true;
              this.BHApproved.status = true;
              this.BHApproved.BHName = bhstatus.BHName;
              this.BHApproved.onDate = bhstatus.approveDate;
              this.BHApproved.onTime = bhstatus.approveTime;
              this.BHApproved.approveStatus = bhstatus.approveStatus;
              this.BHApproved.comments = bhstatus.comments;
            } else if (bhstatus.approveStatus == 0) {
              this.BHApproved.status = true;
              this.approveBtn = true;
              this.rejectBtn = false;
              this.BHApproved.BHName = bhstatus.BHName;
              this.BHApproved.onDate = bhstatus.approveDate;
              this.BHApproved.onTime = bhstatus.approveTime;
              this.BHApproved.approveStatus = bhstatus.approveStatus;
              this.BHApproved.comments = bhstatus.comments;
            } else{
              this.approveBtn = true;
              this.rejectBtn = true;
              this.BHApproved.BHId = bhstatus.BHId;
              this.BHApproved.status = false;
              this.BHApproved.BHName = bhstatus.BHName;
            }
          }
          this.showDetail = true;
          this.stepSelected = step.stepId;
          console.log('bhstatus', bhstatus);
        }
      },
        error => {
          this.spinner.hide();
          this.presentToast('error', '');
        });
  }


  employee_Detail: any = {};
  BHApproved: any = {};

  webinarSubmit(row, data) {
    // this.spinner.show();
    console.log(row, data);
    const param = {
      'eventId': data.eventId,
      'eventStartDate': this.formatSendDate(data.startdate),
      // 'eventEndDate': this.formatDate(data.enddate),
      'eventstartTime': this.formatSendTime(data.fromtime),
      'eventtoTime': this.formatSendTime(data.totime),
      'wfId': this.blendedService.wfId,
      'empId': row.empId,
      'tId': this.loginUserdata.data.data.tenantId,
      'trainerId': data.trainer,
      'userId': this.loginUserdata.data.data.id,
      'stepId': 4,
    };
    console.log(param);
    this.contentService.insertWebinarEvent(param).then(
      res => {
        this.spinner.hide();
        const result = res;
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          this.presentToast('success', 'Event added.');
          //console.log('Webinar:', result['data']);
          this.showDetail  = false;
          this.getAllNominees();
        }
      },
      error => {
        this.spinner.hide();
        this.presentToast('error', '');
      },
    );
  }

  formatSendTime(Dtime){
    const t = new Date(Dtime);
    // const formattedDateTime = this.datepipe.transform(t, 'yyyy-MM-dd h:mm');
    const year = t.getFullYear() +  '-' + (t.getMonth() + 1 )+ '-' + t.getDate();
    const time = ' '+ t.getHours() + ':' + t.getMinutes() + ':' + t.getSeconds();
    const formattedDateTime = year + time;
    return formattedDateTime;
  }
  formatSendDate(date) {
    const d = new Date(date);
    // const formatted = this.datepipe.transform(d, 'yyyy-MM-dd');
    const formattedTime = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
    return formattedTime;
  }


  addWebinarEvent: boolean = false;
  webEventConfirm: boolean = false;
  webEventConfirmMsg: string;
  getWebinarEventByEmpId(empId, step) {
    // this.webinar = {
    //   trainer : '0',
    // };
    this.showDetail = false;
    this.spinner.show();
    console.log('Step value ==>', step);
    if(step.status ==1){
      const param = {
        'wfId': step.activityId,
        'empId': empId,
      };
      this.contentService.getWebinarDownloadList(param).then(
        res => {
          this.spinner.hide();
          const result = res;
          console.log('webinar call details', res);
          if (result['type'] === false) {
            this.presentToast('error', '');
          } else {
            this.showDownloadList = true;
            console.log('Webinar Event:', result['data']);
            if(result['data']){
                if(result['data'].length !== 0) {
                  this.webinarList = result['data'];
                }
                else{
                    this.webinarListNotFound = true;
                }
            }
            console.log('this.webinar:', this.webinar);
            this.showDetail = true;
            this.stepSelected = step.stepId;
          }
        },
        error => {
          this.spinner.hide();
          this.presentToast('error', '');
        },
      );
    }
    else {
      this.showDownloadList = false;
      const param = {
        'wfId': this.blendedService.wfId,
        'empId': empId,
        'tId': this.loginUserdata.data.data.id,
      };
      console.log(param);
      this.contentService.getExistingWebinarEventByEmpId(param).then(
        res => {
          this.spinner.hide();
          const result = res;
          console.log('webinar call details', res);
          if (result['type'] === false) {
            this.presentToast('error', '');
          } else {
            if (result['data'].length === 0) {
              this.addWebinarEvent = true;
              this.webinar.eventId = 0;
              this.webinar = {
                trainer: '0',
                startdate: null,
                enddate: null,
                time: null,
                eventId: null,
                bookDate: null,
                fromtime: null,
                totime : null,
              };
            } else {
              this.addWebinarEvent = false;
              const webEvent = result['data'][0];
              // if (webEvent.bookDate) {
              //   this.webEventConfirm = true;
              //   this.webEventConfirmMsg ='Webinar event confirmed by nominee on' +
              //   this.formatDisplayDate(webEvent.bookDate) + '.' +
              //   'Event date is' + ' ' + this.formatDisplayDate(webEvent.startdate) + '.' +
              //   'Event start time is' + ' ' + this.formatTime(webEvent.fromtime) + '.' + ' ' +
              //   'Event end date is' + ' ' + this.formatTime(webEvent.enddate)
              // } else {
              //   this.webEventConfirm = false;
              //   this.webinar = {
              //     trainer: webEvent.trainerId,
              //     startdate: new Date(webEvent.startdate),
              //     enddate: new Date(webEvent.callEndDate),
              //     time: new Date(webEvent.callTime),
              //     eventId: webEvent.eventId,
              //     bookDate: new Date(webEvent.bookDate),
              //     fromtime: new Date(webEvent.fromtime),
              //     totime :new Date(webEvent.totime)
              //   };
              // }
               this.webEventConfirm = false;
                this.webinar = {
                  trainer: webEvent.trainerId,
                  startdate: new Date(webEvent.startdate),
                  enddate: new Date(webEvent.callEndDate),
                  time: new Date(webEvent.callTime),
                  eventId: webEvent.eventId,
                  bookDate: new Date(webEvent.bookDate),
                  fromtime: new Date(webEvent.fromtime),
                  totime :new Date(webEvent.totime)
                };
              console.log(this.webEventConfirm);
            }
            //this.presentToast('success', 'Webinar', 'Event added successfully.');
            console.log('Webinar Event:', result['data']);
            console.log('this.webinar:', this.webinar);
            this.showDetail = true;
            this.stepSelected = step.stepId;
          }
        },
        error => {
          this.spinner.hide();
          this.presentToast('error', '');
        },
      );
    }
    //console.log(row, data);

  }


  onapprove(rowData, data, status) {
    this.spinner.show();
    console.log('rowData', rowData);
    //console.log('step', step);
    console.log('data', data);
    console.log('status', status);
    const param = {
      'appId': data.BHId ? data.BHId : null,
      'appStat': status,
      'appCmt': data.comments,
      'actId': this.activityId,
      'userId': this.loginUserdata.data.data.id,
      'wfId': this.blendedService.wfId,
      'empId': rowData.empId,
      'tId': this.loginUserdata.data.data.tenantId,
    };
    console.log('param', param);
    this.contentService.updateBhApprovalStatus(param).then(
      res => {
        this.spinner.hide();
        const result = res;
        console.log('result', result);
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          this.presentToast(result['data'][0].msgTitle, result['data'][0].msg);
          console.log('BH Approval on approve:', result['data']);
          this.BHApproved = {};
          this.showDetail = false;
          this.getAllNominees();
        }
      },
      error => {
        this.spinner.hide();
        this.presentToast('error', '');
      },
    );
  }

  /*******************************Evaluation Details / step 3**********************************************/

  callStartDateException: boolean = false;
  feedbackAvgobject: any;
  getEvaluationCallDetail(empId, step) {
    const param = {
      'wfId': this.blendedService.wfId,
      'stepId': step.stepId,
      'tId': this.loginUserdata.data.data.tenantId,
      'empId': empId,
      'actId': step.activityId,
    };
    console.log('param', param);
    this.contentService.getEvaluationCallDetail(param).then(
      res => {
        this.spinner.hide();
        const result = res;
        console.log('result', result);
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          console.log('Evaluation Call:', result['data']);
         // let res = result['data'][0];
         const evaluationCallDetailsRes = result['data'][0];
         if (evaluationCallDetailsRes.length > 0) {
          this.evaluationCallDetails = evaluationCallDetailsRes[0];
          this.evaluationCallDetails.callDate = this.evaluationCallDetails.callDate;
          this.evaluationCallDetails.callStartTime = this.evaluationCallDetails.callStartTime;
          this.evaluationCallDetails.callEndTime = this.evaluationCallDetails.callEndTime;
          if (evaluationCallDetailsRes[0].rejectionReason) {
            this.showReason = true;
            this.evaluationCallDetails.reason = evaluationCallDetailsRes[0].rejectionReason;
          } else {
            this.showReason = false;
            this.evaluationCallDetails.reason = null;
          }
          // this.evaluationCallDetails.callEndDate = this.formatDateMonth(this.evaluationCallDetails.callEndDate);
          // console.log(this.evaluationCallDetails.callStartDate);
          console.log(this.evaluationCallDetails);
         }
         if (result['data'].length != 0) {
          this.feedback = result['data'][1];
          this.feedbackAvgobject = result['data'][2][0];
         }
          if (this.feedback) {
            if (this.feedback.length == 0) {
              this.feedbackShow = false;
            } else {
              this.feedbackShow = true;

            }
          }
          if (evaluationCallDetailsRes.length === 0) {
            this.callStartDateException = false;
          } else {
            this.callStartDateException = true;
          }
          this.showDetail = true;
      }
        this.spinner.hide();
       // this.presentToast('error', 'Evaluation Call', 'Something went wrong.please try again later.');
      },
      error => {
        this.spinner.hide();
        this.presentToast('error', '');
      },
    );
  }

    /*******************************Knowledge Test / step 7**********************************************/

  feedbackShowKT: boolean = false;
  getKnowledgeTestDetails(empId, step) {
    const param = {
      'tId': this.loginUserdata.data.data.tenantId,
      'empId': empId,
      'actId': step.activityId,
    };
    console.log('param', param);
    this.contentService.getKnowledgeTestDetails(param).then(
      res => {
        this.spinner.hide();
        const result = res;
        console.log('result', result);
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          //this.presentToast('success', 'BH Approval', 'Approved successfully.');
          this.assessment = result['data'][0];
          console.log('Assessment', this.assessment);
          if (this.assessment.length == 0) {
            this.feedbackShowKT = false;
          } else {
            this.feedbackShowKT = true;
          }
          console.log('Knowledge Test:', this.assessment);
        }
      },
      error => {
        this.spinner.hide();
        this.presentToast('error', '');
      },
    );
  }

  /*******************************fg assessment / step 8**********************************************/
  fgAssessment: any;
  noQuizAttempted: boolean = false;
  quizData: any = {};
  getFgAssessmentDetails(empId, step) {
    const param = {
      'tId': this.loginUserdata.data.data.tenantId,
      'empId': empId,
      'aId': step.activityId,
    };
    // const param = {
    //   'tId': this.loginUserdata.data.data.tenantId,
    //   'empId': step.empId,
    //   'aId': step.activityId,
    // };
    console.log('param', param);
    this.contentService.getFgAssessmentDetails(param).then(
      res => {
        this.spinner.hide();
        const result = res;
        console.log('getFgAssessmentDetails', result);
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          let correctCount : any = 0;
          let wrongCount : any = 0;
          let pendingCount : any = 0;
          this.fgAssessment = result['data'][0];
          console.log('Fg Assessment:', this.fgAssessment);
          if (this.fgAssessment.length == 0) {
            this.noQuizAttempted = true;
          } else {
            this.noQuizAttempted = false;
            for (let i = 0; i < this.fgAssessment.length; i++) {
              this.quizData.totalQue = this.fgAssessment.length;
              if ( this.fgAssessment[i].answerCorrect == 2) {
                 correctCount++;
              } else if ( this.fgAssessment[i].answerCorrect == 1 ) {
                 pendingCount++;
              } else if ( this.fgAssessment[i].answerCorrect == 0 ) {
                 wrongCount++;
              }
              if (this.fgAssessment[i].questionFormatId == 1) {
                let str = new String(this.fgAssessment[i].answers);
                // const qt1 = str.replace(/#/g, '|');
                const qt1 = str.replace(/%hash%/g, '|');
                this.fgAssessment[i].formattedAns = qt1;
              }
              if (this.fgAssessment[i].questionFormatId == 2 || this.fgAssessment[i].questionFormatId == 3) {
                const str = new String (this.fgAssessment[i].answers);
                // const qt2 = str.replace(/#/g, '-');
                const qt2 = str.replace(/%hash%/g, '-');
                const str1 = new String (qt2);
                const qt21 = str1.replace(/%tild%/g, ',');
                this.fgAssessment[i].formattedAns = qt21;
              }
              if (this.fgAssessment[i].questionFormatId == 4) {
                this.fgAssessment[i].formattedAns = this.fgAssessment[i].answers;
              }
            }
            this.quizData.correctCount = correctCount;
            this.quizData.wrongCount = wrongCount;
            this.quizData.pendingCount = pendingCount;
            this.showDetail = true;
          }
        }
      },
      error => {
        this.spinner.hide();
        this.presentToast('error', '');
      },
    );
  }

  feedbackShowVD: boolean = false;
  vivaFeedBackObject: any;
  getVivaDetails(empId, step) {
    const param = {
      'tId': this.loginUserdata.data.data.tenantId,
      'empId': empId,
      'actId': step.activityId,
    };
    console.log('param', param);
    this.contentService.getVivaDetails(param).then(
      res => {
        this.spinner.hide();
        const result = res;
        console.log('getVivaDetails', result);
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          //this.presentToast('success', 'BH Approval', 'Approved successfully.');
          this.vivaFeedBackObject = result['data'] ? result['data'] : [];
          if(this.vivaFeedBackObject){
            if (this.vivaFeedBackObject.length == 0) {
              this.feedbackShowVD = false;
            } else {
              this.feedbackShowVD = true;
            }
            this.showDetail = true;
          }

          console.log('Viva Details:', this.vivaFeedBackObject);
        }
      },
      error => {
        this.spinner.hide();
        this.presentToast('error', '');
      },
    );
  }

  getObserverDetail(empId, step) {
    console.log(empId, step);
  }
  numberOfCol() {

    for (let i = 0; i < this.nominees.length; i++) {
      this.nominees[i].finalWflength = this.nominees[i].finalWf.length;
      var totalCols = this.nominees[i].finalWflength;
      console.log("Final workflow steps", this.nominees[i].finalWf);
    }
    return 'repeat(1fr ,' + totalCols + ' )';
    // console.log("Nomineee Array", this.nominees);
  }

  formatDate(date) {
    if(date)
    {
      const d = new Date(date);
      const formatted = this.datepipe.transform(d, 'yyyy-MM-dd');
      return formatted;
    }
  }
  formatDisplayDate(dateTime){
    if(dateTime){
      const dateTimeData = new Date(dateTime);
      const formattedTime = dateTimeData.getDate() + "-" + (dateTimeData.getMonth() + 1) + "-" + dateTimeData.getFullYear();
      return formattedTime;
    }


    // const seconds = new Date()
  }
  formatDisplayTime(dateTime){
    if(dateTime){
      const dateTimeData = new Date(dateTime);
      let hours = dateTimeData.getHours();
        let minutes = dateTimeData.getMinutes();
        let ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        let minutes2 = minutes < 10 ? '0'+ minutes : minutes;
        let strTime = hours + ':' + minutes2 + ' ' + ampm;
        return strTime;
      // const seconds = new Date()
    }

  }
  formatDateMonth(date) {
    if(date)
    {
      const d = new Date(date);
      const formatted = this.datepipe.transform(d, 'dd-MMM-yyyy');
      return formatted;
    }
  }

  formatTime(time) {
    if(time)
    {
    const t = new Date(time);
    const formattedTime = this.datepipe.transform(t, 'h:mm:ss');
    return formattedTime;
    }
  }

  insertingClickablePropertyInObject(nomArr) {
    for (const nomineedata of nomArr) {
      let count = 0;
      const arrayWF = nomineedata.finalWf;
      if (nomineedata.isRecert === 0) {
        for (let i = 0; i < arrayWF.length; i++) {
          if (arrayWF[i].status != 1) {
            if ((arrayWF[i].stepId >= 3) && (arrayWF[3].clickableType == true)) {
              arrayWF[i]['clickableType'] = true;
            }
            else {
              arrayWF[i]['clickableType'] = false;
            }
          }
          else {
            count++;
            arrayWF[i]['clickableType'] = true;
          }
          if(count < arrayWF.length){
            arrayWF[count]['clickableType'] = true;
          }

        }
      }
      if (nomineedata.isRecert === 1) {
        for (let i = 0; i < arrayWF.length; i++) {
          arrayWF[i]['clickableType'] = true;
        }
      }
    }
  }

  // approval: any = {
  //   reason: null,
  // };
approveStatus = 1;
showReason: boolean = false;
  evalutionApproval(row, approval) {
    console.log(approval);
      const param = {
        'appStat': parseInt(approval.approvalStatus),
        'rejReason': approval.reason ? approval.reason : null,
        'wfId':this.blendedService.wfId,
        'empId': row.empId,
        'stepId': 3,
        'userId': this.loginUserdata.data.data.id,
        'tId': this.loginUserdata.data.data.tenantId,
        'pubValue': approval.isPublished ? 1 : 0,
      };
      console.log('param', param);
      this.contentService.updateEvaluationApproval(param).then(
        res => {
          this.spinner.hide();
          const result = res;
          console.log('result', result);
          if (result['type'] === false) {
            this.presentToast('error', '');
          } else {
            console.log('Evaluation approval insertion:', result['data']);
            this.presentToast('success', res['data'][0].msg);
            this.showDetail = false;
            this.getAllNominees();
          }
        },
        error => {
          this.spinner.hide();
          this.presentToast('error', '');
        },
      );
  }
  approveReject(value) {
    console.log('value', value);
     this.approveStatus = value ? 1 : 0 ;
    if (this.approveStatus === 1) {
      this.showReason = false;
      this.evaluationCallDetails.reason = null;
    //  this.showReason = false;
    } else {
     // this.evaluationCallDetails.isPublished = false;
      this.showReason = true;
    }
  }
  functiona(){
    console.log(this.evaluationCallDetails);
    console.log(this.approveStatus);
  }

  /*******************************Batch Selection / step 10**********************************************/

  batchSelected: boolean = false;
  batchDetails:any ={};
  getBatchDetail(empId) {
    const param = {
      'wfId': this.blendedService.wfId,
      'tId': this.loginUserdata.data.data.tenantId,
      'empId': empId,
    };
    console.log('param', param);
    this.contentService.getbatchDetailStepTen(param).then(
      res => {
        this.spinner.hide();
        const result = res;
        console.log('result', result);
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          console.log('Batch Details:', result['data']);
         // let res = result['data'][0];
         let batchDetailsRes = result['data'];
         if (batchDetailsRes.length > 0) {
          this.batchDetails = batchDetailsRes[0];
          this.batchDetails.validFromDate = this.batchDetails.validFromDate;
          this.batchDetails.validToDate = this.batchDetails.validToDate;
          console.log(this.batchDetails.validFromDate);
          console.log(this.batchDetails.validToDate);
         }
          if (batchDetailsRes.length === 0) {
            this.batchSelected = false;
          } else {
            this.batchSelected = true;
          }
          this.showDetail = true;
      }
        this.spinner.hide();
       // this.presentToast('error', 'Evaluation Call', 'Something went wrong.please try again later.');
      },
      error => {
        this.spinner.hide();
        this.presentToast('error', '');
      },
    );
  }

/*******************************MOck feedback / step 11**********************************************/

  mockshowVD: boolean = false;
  mockFeedBackObject: any = [];
  mockFeedbackAvg: any;
  isFeedPublish: any;
  getMockDetails(empId, step) {
    const param = {
      'tId': this.loginUserdata.data.data.tenantId,
      'empId': empId,
      'actId': step.activityId,
      'wfId': this.blendedService.wfId,
      'stepId': step.stepId,
    };
    console.log('param', param);
    this.contentService.getmockStepEleven(param).then(
      res => {
        this.spinner.hide();
        const result = res;
        console.log('getMockDetails', result);
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          //this.presentToast('success', 'BH Approval', 'Approved successfully.');
          this.mockFeedBackObject = result['data'];
          if (this.mockFeedBackObject.length == 0) {
            this.mockshowVD = false;
          } else {
            this.mockshowVD = true;
          }
          if(result['avg'].length !== 0) {
            this.mockFeedbackAvg = result['avg'][0];
          }
          this.showDetail = true;
          console.log('Mock Details:', this.mockFeedBackObject);
          this.isFeedPublish = result['isFeedPublish'][0];
          console.log('isFeedback Published ==>', this.isFeedPublish.isFeedPublish);
        }
      },
      error => {
        this.spinner.hide();
        this.presentToast('error', '');
      },
    );
  }
  /*******************************Assesment feedback / step 12**********************************************/
  assesmentshowVD: boolean = false;
  assesmentFeedBackObject: any;
  assesmentFeedBackAvg:any;

  getAssesmentDetails(empId, step) {
    const param = {
      'tId': this.loginUserdata.data.data.tenantId,
      'empId': empId,
      'actId': step.activityId,
      'wfId': this.blendedService.wfId,
      'stepId': step.stepId,
    };
    console.log('param', param);
    this.contentService.getAssesmentSteptwelve(param).then(
      res => {
        this.spinner.hide();
        const result = res;
        console.log('getAssesmentDetails', result);
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          //this.presentToast('success', 'BH Approval', 'Approved successfully.');
          this.assesmentFeedBackObject = result['data'];
          if (this.assesmentFeedBackObject.length == 0) {
            this.assesmentshowVD = false;
          } else {
            this.assesmentshowVD = true;
          }
          if(result['avg'].length !== 0 ){
            this.assesmentFeedBackAvg = result['avg'][0];
          }
          this.showDetail = true;
          console.log('Assesment Feedback Details:', this.assesmentFeedBackObject);
          this.isFeedPublish = result['isFeedPublish'][0];
          console.log('isFeedback Published ==>', this.isFeedPublish.isFeedPublish);
        }

      },
      error => {
        this.spinner.hide();
        this.presentToast('error', '');
      },
    );
  }
    /******************************* GET CERTIFICATE / step 12**********************************************/
    CerPresent: boolean = false;
    CertificateObject: any;
    Certmsg: any;
    getCertificateContent(empId) {
      let url = webApi.domain + webApi.url.getCertificateLink;
     const param = {
      eId: empId,
      wfId: this.blendedService.wfId,
      tId: this.loginUserdata.data.data.tenantId,
      points: this.certificateContent['totalTab'].points,
      type: 'web',
      };
      const urlString =
      "empId=" +
      param.eId +
      "&wfId=" +
      param.wfId +
      "&tId=" +
      param.tId +
      "&points=" +
      param.points +
      "&type=web";
      url = url + urlString;
      this.certificateDownloadUrl = url;
      //   const url = webApi.domain + webApi.url.getCertificateLink + 'empId=' +
      //        empId + '&wfId=' + this.blendedService.wfId + '&tId=' + this.loginUserdata.data.data.tenantId + '&type=web';
      // console.log('url', url);
      // this.contentService.getCertifiacteUrl(url).then(
      //   res => {
      //     this.spinner.hide();
      //     const result = res;
      //     console.log('getCertificateDetails', result);
      //     if (result['type'] === false) {
      //       this.presentToast('error', '');
      //     } else {
      //       if (result['data'].filePath) {
      //         this.CertificateObject = result['data'];
      //         this.CertificateObject.demopth = webApi.domain + '/' + this.CertificateObject.filePath;
      //         if (this.CertificateObject.demopth) {
      //           this.CertificateObject.finalPath = this.transform(this.getDocumentUrl(this.CertificateObject.demopth));
      //         }
      //         // this.CerPresent = true;
      //       }else {
      //         this.Certmsg = result['data'];
      //         // this.CerPresent = false;
      //       }
      //       this.showDetail = true;
      //       console.log('Certificate Details:', this.CertificateObject);
      //     }
      //   },
      //   error => {
      //     this.spinner.hide();
      //     this.presentToast('error', '');
      //   },
      // );
    }

    getCertificateDetails(empId) {
      this.spinner.show();
     const param = {
       empId : empId,
       wfId : this.blendedService.wfId,
       tId : this.loginUserdata.data.data.tenantId,
     };
    console.log('param', param);
    this.contentService.getCertificateData(param).then(
      res => {
        this.certificateContent = res;
        console.log('getCertificateDetails', this.certificateContent);
        if (this.certificateContent['type'] === false) {
          this.presentToast('error', '');
          this.CerPresent = false;
          this.spinner.hide();
        } else {
          console.log('Certificate Details:', this.certificateContent);
          if (this.certificateContent['alltab'].length !== 0) {
            this.CerPresent = true;
           }
        if (this.certificateContent['certTab'] !== null) {
          let certTab = this.certificateContent['certTab'];
          if(certTab.certContent === 1 ) {
            this.showDownloadCertificate = true;
            this.getCertificateContent(empId);
          }
         }
         if (this.certificateContent['totalTab'] !== null ){
           let data = this.certificateContent['totalTab'];
           if(data.points !== 0){
            this.showTotal = true;
           }
         }
         this.spinner.hide();
         this.showDetail = true;
        }
      },
      error => {
        this.spinner.hide();
        this.CerPresent = false;
        this.presentToast('error', '');
      },
    );
  }

    transform(url) {
      return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }
    getDocumentUrl(url) {
      return ('https://docs.google.com/gview?url=' + encodeURI(url) + '&embedded=true');
    }

    viewDownloadVideo(item){
      this.showVideoDataContent = item;
      this.showVideoPopup = true;
    }
    closePopup(){
      this.showVideoPopup = false;
    }

    ngOnChanges(){
      if (this.blendedService.program) {
        this.getAllNominees();
        this.trainerlist();
      }
    }

    refreshSteps(){
      if (this.blendedService.program) {
        this.getAllNominees();
        this.trainerlist();
      }
    }
    clearesearch(){
      if(this.searchText.length>=3){
      this.searchString = '';
      this.tempNominees = this.nominees;
      this.getAllNominees();
      this.searchText=''
      }else{
        this.searchString=''
      }
      // if(this.tempNominees.length == 0){
      //   this.noEmpEnrol = true;
      // }else {
      //   this.noEmpEnrol = false;
      // }
    }

    searchEnrol(event) {
      // this.nominees = nomArr;
      // this.tempNominees = nomArr;
      var val = event.target.value.toLowerCase();
      var keys = [];
      let data = '';
      this.searchText=val;
      // filter our data
      if (this.nominees.length > 0) {
        keys = Object.keys(this.nominees[0]);
      }
      if(val.length>=3||val.length==0){
      var temp = this.nominees.filter((d) => {
        // keys.forEach(element => {
        // });
        for (const key of keys) {
          if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
            return true;
          }
        }
      });
      // update the rows
      this.tempNominees = temp;
    }
      // if(this.tempNominees.length == 0){
      //   this.noEmpEnrol = true;
      // }else {
      //   this.noEmpEnrol = false;
      // }
    }
}
