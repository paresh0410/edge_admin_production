import { Injectable, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AppConfig } from '../../../../../app.module';
import { webApi } from '../../../../../service/webApi';
import { HttpClient } from "@angular/common/http";

@Injectable()
export class ContentService {



    private _urlGetAllCourses: string = webApi.domain + webApi.url.getallcoursesinstance;
    private _urlEnrolCourseForCCContent: string = webApi.domain + webApi.url.enrolcourseforinstance;
    private _urlGetAllEnrollCourseModule: string = webApi.domain + webApi.url.getallenrolcoursemoduleinstance;
    private _urlRemoveEnrolCourse: string = webApi.domain + webApi.url.removeenrolcourseinstance;
    private _urlInsertWorklowSteps: string = webApi.domain + webApi.url.insertWorkflowSteps;
    private _urlGetWorklowSteps: string = webApi.domain + webApi.url.getallworkflowsteps;
    private _urlGetAllTttNominees: string = webApi.domain + webApi.url.getAllTttNominnes;
    private _urlGetNomineeDetails: string = webApi.domain + webApi.url.getNomineeDetails;
    private _urlGetBhApprovalStatus: string = webApi.domain + webApi.url.getBhapprovalStatus;
    private _urlGetAllWorkflowSlots: string = webApi.domain + webApi.url.getAllWorkflowSlots;
    private _urlUpdateBhapprovalStatus: string = webApi.domain + webApi.url.updateBhApprovalStatus;
    private _getActivityDropDown: string = webApi.domain + webApi.url.getistanceactivitydropdown;
    private _insertWebinarEvent: string = webApi.domain + webApi.url.insertWebinarEvent;
    private _getExistingWebinareventByEmpId: string = webApi.domain + webApi.url.getExistingWebinarEventByEmpId;
    private _getEvaluationCallDetail: string = webApi.domain + webApi.url.getEvaluationCallDetail;
    private _getKnowledgeTestDetails: string = webApi.domain + webApi.url.getKnowledgeTestDetails;
    private _getVivaDetails: string = webApi.domain + webApi.url.getVivaDetails;
    //private _getEvaluationApproval: string = webApi.domain + webApi.url.getEvaluationApproval;
    private _updateEvaluationApproval: string = webApi.domain + webApi.url.updateEvaluationApproval;
    private _getFgAssessmentDetails: string = webApi.domain + webApi.url.updateFgAssessmentDetails;
    private _getbatchDetailStepTen: string = webApi.domain + webApi.url.batchDetailStepTen;
    private _getmockStepEleven: string = webApi.domain + webApi.url.mockStepEleven;
    private _getAssesmentSteptwelve: string = webApi.domain + webApi.url.assesmentStepTwelve;
    private _getstepwisedataurl: string = webApi.domain + webApi.url.getstepwisedata;
    private _getstepwiseuserdata: string = webApi.domain + webApi.url.getstepwiseuserdata;
    private _getstepwisebhapproval: string = webApi.domain + webApi.url.stepwisebhapproval;
    private _stpwiseevalutionurl: string = webApi.domain + webApi.url.stpwiseevalution;
    private _enrolwebinar: string = webApi.domain + webApi.url.stepwisewebinar;
    private _getCertificateData: string = webApi.domain + webApi.url.getCertificate;
    private _getTTTWebinarDownloadList: string = webApi.domain + webApi.url.TTTgetWebinarDownloadList;
    private _urlAreaBulEnrol: string = webApi.domain + webApi.url.area_bulk_enrol;

    // private _getCertificateContent: string = webApi.domain + webApi.url.getCertificateContent;
    public courseId: any;
    public programId: any;
    public tenantId: any;
    constructor(@Inject('APP_CONFIG_TOKEN') private config: AppConfig, private _http: Http,
        private _httpClient: HttpClient) {
        //this.busy = this._http.get('...').toPromise();
    }

    getAllCoursesCC(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlGetAllCourses, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    enrollCourseForCCContent(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlEnrolCourseForCCContent, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }


    removeEnrolCourse(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlRemoveEnrolCourse, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    getAllEnrolCourseModule(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlGetAllEnrollCourseModule, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    insertWorkflowSteps(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlInsertWorklowSteps, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    getAllWorkflowSteps(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlGetWorklowSteps, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }


    getAllNominees(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlGetAllTttNominees, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }


    getNomineeDetails(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlGetNomineeDetails, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    getBhApprovalStatus(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlGetBhApprovalStatus, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    getAllWorkflowSlots(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlGetAllWorkflowSlots, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    updateBhApprovalStatus(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlUpdateBhapprovalStatus, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    getCourseActivities(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._getActivityDropDown, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    insertWebinarEvent(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._insertWebinarEvent, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    getExistingWebinarEventByEmpId(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._getExistingWebinareventByEmpId, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    getEvaluationCallDetail(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._getEvaluationCallDetail, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }
    getKnowledgeTestDetails(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._getKnowledgeTestDetails, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }
    getVivaDetails(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._getVivaDetails, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }
    // getEvaluationApproval(param) {
    updateEvaluationApproval(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._updateEvaluationApproval, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }
    getFgAssessmentDetails(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._getFgAssessmentDetails, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    getbatchDetailStepTen(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._getbatchDetailStepTen, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    getmockStepEleven(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._getmockStepEleven, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }
    getAssesmentSteptwelve(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._getAssesmentSteptwelve, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }
    getCertifiacteUrl(Cerurl) {
        return new Promise(resolve => {
            this._httpClient.get(Cerurl)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    getCertificateData(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._getCertificateData, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    stepwisedata(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._getstepwisedataurl, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    stepwiseuserdata(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._getstepwiseuserdata, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    stepwiseBhapproval(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._getstepwisebhapproval, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    stepwiseevelutionapproval(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._stpwiseevalutionurl, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }
    stepwiseenerolewebinar(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._enrolwebinar, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    getWebinarDownloadList(param) {
      return new Promise(resolve => {
          this._httpClient.post(this._getTTTWebinarDownloadList, param)
              //.map(res => res.json())
              .subscribe(data => {
                  resolve(data);
              },
                  err => {
                      resolve('err');
                  });
      });
  }

  areaBulkEnrol(param) {
    return new Promise(resolve => {
        this._httpClient.post(this._urlAreaBulEnrol, param)
            //.map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
                err => {
                    resolve('err');
                });
    });
}

//   areaBulkEnrol(item){
//     // return this._http.post(this._urlCheckCourseCode,courseData)
//     //   .map((response:Response) => response.json())
//     //   .catch(this._errorHandler);
//        let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
//         // let headers = new Headers({ 'Authorization': 'Bearer ' + 'myToken' });
//         let options = new RequestOptions({ headers: headers });
//     return new Promise(resolve => {
//       this._http.post(this._urlAreaBulEnrol, item, options)
//     .map(res => res.json())
//             .subscribe(data => {
//                 resolve(data);
//             },
//             err => {
//                 resolve('err');
//             });
//       });

//   }
    // getStepPoints(param) {
    //     return new Promise(resolve => {
    //         this._httpClient.post(this._getCertificateContent, param)
    //             //.map(res => res.json())
    //             .subscribe(data => {
    //                 resolve(data);
    //             },
    //                 err => {
    //                     resolve('err');
    //                 });
    //     });
    // }
    _errorHandler(error: Response) {
        console.error(error);
        return Observable.throw(error || 'Server Error')
    }


}
