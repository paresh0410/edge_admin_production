import { Component, OnInit, ViewChild, ChangeDetectorRef, Input, OnChanges, SimpleChanges } from '@angular/core';
import { NgForm } from '@angular/forms';
import { SortablejsOptions } from 'angular-sortablejs';
import { BlendedService } from '../../../blended.service';
import { ToastrService } from 'ngx-toastr';
// import { ToasterService, Toast } from 'angular2-toaster';
import { webApi } from '../../../../../../service/webApi';
import { webAPIService } from '../../../../../../service/webAPIService';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { AddEditNominationComponent } from '../add-edit-nomination.component';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'code-of-conduct',
  templateUrl: './code-of-conduct.component.html',
  styleUrls: ['./code-of-conduct.component.scss']
})
export class CodeOfConductComponent implements OnInit {
  @ViewChild(NgForm) codeOfConduct: NgForm;
  @Input() inpdata: any;
  @ViewChild("codeOfConduct") formval: any;
  DragableContent: SortablejsOptions = {
    group: {
      name: 'codeContents',
    },
    handle: ".drag-handle",
    sort: true,
  };

  codeOfConductData: any =
    {
      id: 1,
      sName: null,
      sDesc: null,
      attachedFile: null
    };

  errorMsg: any = [];
  codeContents: any = [];
  nominationdata: any = [];
  userdata: any = [];
  // tenantId: any;
   wfId: any;
  constructor(public blendedservice: BlendedService, 
    private toasterService: ToastrService,
    private BlendedService: BlendedService, private toastr: ToastrService,
    private webApiService: webAPIService, private router: Router, private spinner: NgxSpinnerService,
    public AddEditNominationComponent: AddEditNominationComponent, private cdf: ChangeDetectorRef,
    private http1: HttpClient) {
      this.getHelpContent();
      if (localStorage.getItem('LoginResData')) {
        this.userdata = JSON.parse(localStorage.getItem('LoginResData'));
      }
    // this.codeContents[0] = this.codeOfConductData;
    // if (this.BlendedService.program) {
    //   this.nominationdata = this.BlendedService.program;
    //   this.tenantId = this.nominationdata.tenantId;
    //   this.wfId = this.nominationdata.nId;
    // }
    if (this.blendedservice.wfId) {
      this.wfId = this.blendedservice.wfId;
    }
  }
  config = {
    height: '200px',
    uploader: {
      insertImageAsBase64URI: true,
    },
    allowResizeX: false,
    allowResizeY: false,
    placeholder: 'Section Description',
    limitChars: 3000,
    toolbarSticky: false,
  };
  ngOnInit() {
    if (this.blendedservice.program) {
      this.nominationdata = this.blendedservice.program;
    }
    this.code();
  }

  ngOnChanges(changes: SimpleChanges): void {
		if(this.inpdata === 'add') {
		  this.addSection();
		} else if(this.inpdata === 'save') {
		  this.onSubmit(this.formval.form.value);
		}
	}

  presentToast(type, body) {
    if(type === 'success'){
      this.toastr.success(body, 'Success', {
        closeButton: false
       });
    } else if(type === 'error'){
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else{
      this.toastr.warning(body, 'Warning', {
        closeButton: false
        })
    }
  }

  code() {
    const data = {
      wfId: this.blendedservice.wfId,
      tId: this.userdata.data.data.tenantId,
    };
    this.blendedservice.getallinstancecodeofcondunce(data).then(res => {
      console.log(res);
      if (res['type'] === true) {
        this.codeContents = res['data'];
        for(let i = 0; i < this.codeContents.length; i++)
        {
          this.codeContents[i].fileName = this.codeContents[i].sRef ? this.codeContents[i].sRef.substring(
            this.codeContents[i].sRef.lastIndexOf("/") + 1) : this.codeContents[i].sRef;
            this.codeContents[i]['requiredDescription'] = true;
            if(this.codeContents[i].fileName == ''){
              this.codeContents[i].fileName = 'Click here to upload an image file';
            }
        }
        if (this.codeContents.length == 0) {
          const data = {
            id: 1,
            sName: '',
            sDesc: '',
            attachedFile: null,
            fileName: 'Click here to upload an image file',
            requiredDescription: false,
          };
          this.codeContents.push(data);
        }
        console.log('this.codeContents', this.codeContents);
      }
    }, err => {
      console.log(err);
    });
  }
  addSection() {
    if(!this.formval.form.valid && !this.formval.form.value.sDesc) {
      this.toasterService.warning('Please fill required details');
      return;
    } else {
      const data = {
        id: 1,
        sName: '',
        sDesc: '',
        fileName: 'Click here to upload an image file',
        attachedFile: null,
        requiredDescription: false,
      };
      this.codeContents.push(data);
    }
  }
  mutimgarr: any = [];
  multTemparr: any = [];
  multcontent: any = [];
  multfileUploadRes: any = [];
  fileName= 'Upload File';


  onSubmit(data) {
    this.mutimgarr =[];
    if(!this.formval.form.valid) {
      this.toasterService.warning('Please fill required details');
      return;
    } else {
      if(this.codeContents.length === 0){
        this.toastr.warning('Please add section for code conduct', 'Warning');
        return null;
      }
      this.spinner.show();
       console.log("Data", data);
      // this.codeOfConduct.resetForm();
      console.log(this.codeContents);
      for (let i = 0; i < this.codeContents.length; i++) {
        this.codeContents[i].sOrder = i + 1;
        if (this.codeContents[i].sRef) {
          this.mutimgarr.push(this.codeContents[i].sRef);
          this.multTemparr.push(i);
          this.codeContents[i].name = this.codeContents[i].attachedFile;
          this.codeContents[i].author = this.codeContents[i].workflowId;
          this.multcontent.push(this.codeContents[i]);
        }
        this.codeContents[i].author = 'author';
      }
      var fd = new FormData();
      fd.append('content', JSON.stringify(this.multcontent));
      for (let i = 0; i < this.mutimgarr.length; i++) {
        fd.append('file', this.mutimgarr[i]);
      }
  
  
      let fileUploadUrl = webApi.domain + webApi.url.multifileUpload;
      if (this.mutimgarr.length > 0) {
        this.webApiService.getService(fileUploadUrl, fd)
          .then((rescompData: any) => {
             this.spinner.hide();
            var temp: any = rescompData;
            this.multfileUploadRes = rescompData;
            if (temp == "err") {
              // var thumbUpload: Toast = {
              //   type: 'error',
              //   title: "Question type",
              //   body: "Unable to upload Answer Images.",
              //   showCloseButton: true,
              //   timeout: 4000
              // };
              // this.toasterService.pop(thumbUpload);
              this.presentToast('error', '');
            }
            else {
              if (this.multfileUploadRes.length > 0) {
                var imageData = [];
                for (let i = 0; i < this.multfileUploadRes.length; i++) {
                  imageData.push(this.multfileUploadRes[i]);
                }
                this.saveMultiple(this.codeContents, imageData);
               // this.TabSwitcher();
              } else {
                // var thumbUpload: Toast = {
                //   type: 'error',
                //   title: "Question file",
                //   body: 'Unable to upload Answer Images',
                //   showCloseButton: true,
                //   timeout: 4000
                // };
                // this.toasterService.pop(thumbUpload);
                this.presentToast('error', '');
              }
            }
            console.log('File Upload Result', this.multfileUploadRes)
          },
            resUserError => {
              this.spinner.hide();
              this.errorMsg = resUserError;
            });
      } else {
        var imgData = [];
        this.saveMultiple(this.codeContents, imgData);
      }
    }
  }

  TabSwitcher(){
    this.AddEditNominationComponent.tabEnabler.codeOfConduct = false;
    this.AddEditNominationComponent.tabEnabler.content = true;
    this.AddEditNominationComponent.tabEnabler.enrolTab = false;
    this.AddEditNominationComponent.tabEnabler.nomineeTab = false;
    this.AddEditNominationComponent.tabEnabler.detailsTab = false;
    this.AddEditNominationComponent.tabEnabler.steps = false;
    console.log(this.AddEditNominationComponent);
  }

  closeSection(data, i) {
    console.log('Data', data);
    console.log(i);
    this.codeContents.splice(i, 1);
  }

  readmultimg(event: any, ind, item) {
    var size = 10000000;
    if(item.sDesc.trim() == '' && event.target.files[0].name != ''){
      item.requiredDescription = !item.requiredDescription;
    }
    this.enableCancelBtn=true
    var validExts = new Array('image');
    // console.log("Event", event);
    var fileExt = event.target.files[0].type;
    this.fileName = event.target.files[0].name;
    console.log("File Name =====>", this.fileName);
    // fileName
    fileExt = fileExt.substring(0, 5);
    if (size <= event.target.files[0].size) {
      // const toast: Toast = {
      //   type: 'error',
      //   title: 'file size exceeded!',
      //   body: 'files size should be less than 10MB',
      //   showCloseButton: true,
      //   timeout: 4000,

      // };
      // this.toasterService.pop(toast);
    this.enableCancelBtn=false
      this.presentToast('warning', 'File size should be less than 10MB');
      event.target.value = "";
      // this.deleteCourseThumb();
    } else {
      if (validExts.indexOf(fileExt) < 0) {
        // const toast: Toast = {
        //   type: 'error',
        //   title: 'Invalid file selected!',
        //   body: 'Valid files are of ' + validExts.toString() + ' types.',
        //   showCloseButton: true,
        //   timeout: 4000,
        // };
        // this.toasterService.pop(toast);
    this.enableCancelBtn=false
      this.presentToast('warning', 'Valid file types are ' + validExts.toString());

      } else
        if (event.target.files && event.target.files[0]) {
          for (let i = 0; i < this.codeContents.length; i++) {
            if (i === ind) {
              this.codeContents[i].fileName = event.target.files[0].name ? event.target.files[0].name : 'Click here to upload an image file';
              this.codeContents[i].sRef = event.target.files[0];
            }
          }
        }
    }
  }
  enableCancelBtn=false
  cancelmulti(ind) {
    console.log('nikhil');
    // for (let i = 0; i < this.codeContents.length; i++) {
    //   if (i === ind) {

    //   }

    // }
    this.enableCancelBtn=false
    this.fileName = 'No File Chosen';
    this.codeContents[ind].sRef = null;
    this.codeContents[ind].fileName = 'No File Chosen';
    this.cdf.detectChanges();
  }
  saveMultiple(mult, imageData) {
    this.spinner.show();
    console.log(mult);
    console.log(imageData);
    var section: any = [];
    for (let i = 0; i < mult.length; i++) {
      mult[i].sRef = null;
      for (let j = 0; j < imageData.length; j++) {
        let replaced = mult[i].fileName.split(' ').join('_');
        if (imageData[j].includes(replaced)) {
          mult[i].sRef = imageData[j];
          break;
        }
      }
      const data = {
        sName: mult[i].sName,
        sDesc: mult[i].sDesc,
        sRef: mult[i].sRef ? mult[i].sRef : null,
        sOrder: mult[i].sOrder,
      }
      section.push(data);
      // console.log('section',section)
    }
    const option: string = Array.prototype.map.call(section, function (item) {
      console.log('item', item)
      return Object.values(item).join('$');
    }).join('|');

    const finalData = {
      wfId: this.blendedservice.wfId,
      sections: option,
      tId: this.userdata.data.data.tenantId,
      ucrId: this.userdata.data.data.id,
    };
    console.log('finalData', finalData);
    this.blendedservice.addeditcodeofcontent(finalData).then(res => {
      this.spinner.hide();
      console.log(res);
      if ( res['type'] === true) {
        // const toast: Toast = {
        //   type: 'success',
        //   title: 'Code of content',
        //   body: res['data'][0].msg,
        //   showCloseButton: true,
        //   timeout: 4000,
        // };
        // this.toasterService.pop(toast);
      this.presentToast('success', res['data'][0].msg);
        const DataTab={
          tabTitle:'Content',
        }
        this.AddEditNominationComponent.selectedTab(DataTab);
        this.cdf.detectChanges();
       // this.TabSwitcher();
       // this.router.navigate(['pages/learning/blended-home/nomination-instance']);
      } else {
        // const toast: Toast = {
        //   type: 'error',
        //   title: 'Code of content',
        //   body: res['data'][0].msg,
        //   showCloseButton: true,
        //   timeout: 4000,
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      }
    }, err => {
      this.spinner.hide();
      console.log(err);
    });
  }

      // Help Code Start Here //

      helpContent: any;
      getHelpContent() {
        return new Promise(resolve => {

          this.http1.get('../../../../../../assets/help-content/addEditCourseContent.json').subscribe(
            data => {
              this.helpContent = data;
              console.log('Help Array', this.helpContent);
            },
            err => {
              resolve('err');
            },
          );
        });
        // return this.helpContent;
      }


      // Help Code Ends Here //


}
