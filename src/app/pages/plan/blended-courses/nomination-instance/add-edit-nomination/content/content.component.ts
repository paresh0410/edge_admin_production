import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
// import { AddEditProgramBlendedService } from './../addEditProgramBlended.service';
import { ContentService } from "../content.service";
import { NgxSpinnerService } from "ngx-spinner";
// import { ToasterModule, ToasterService, Toast } from "angular2-toaster";
import { ToastrService } from 'ngx-toastr';
import {
  Router,
  NavigationStart,
  Routes,
  ActivatedRoute
} from "@angular/router";
import { BlendedService } from "../../../blended.service";
import { AddEditNominationComponent } from '../add-edit-nomination.component';
import { detailsBlendService } from '../../../addEditCourseContent/details/details.service';
import { AddEditBlendCourseContentService } from '../../../addEditCourseContent/addEditCourseContent.service';
import { noData } from "../../../../../../models/no-data.model";

@Component({
  selector: "nomination-content",
  templateUrl: "./content.component.html",
  styleUrls: ["./content.component.scss"]
})
export class ContentComponent implements OnInit {
  modal: boolean;
  courses: any;
  name: string;
  id: number;
  badgeTitle:string='Select Batch'
  btnName:string='Select'
  contentDataArray: any = [];
  contentArray: any = [];
  callArray: any = [];
  search:any={};
  allCourse: any = [];
  noDataVal:noData={
    margin:'mt-5',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"Sorry we couldn't find any matches please try again",
    desc:".",
    titleShow:true,
    btnShow:false,
    descShow:false,
    btnText:'Learn More',
    btnLink:''
}
  tenantId: any;
  courseId: any;
  wfId: any = 0;
  userId: any;
  condataCourse: any;
  couresshow: boolean = false;
  courseSelected: boolean = false;
  nominationdata: any = [];
  courseDropdownList: any = [];
  enableSubmit: boolean=false;
  constructor(
    private contentService: ContentService,
    private spinner: NgxSpinnerService,
    private router: Router,
    public routes: ActivatedRoute,
    // private toasterService: ToasterService,
    private toastr: ToastrService,
    private BlendedService: BlendedService,
    public AddEditNominationComponent: AddEditNominationComponent,
    private cdf: ChangeDetectorRef,
    protected passService: AddEditBlendCourseContentService,
    private detailsService: detailsBlendService,
  ) {
    if (this.BlendedService.program) {
      this.nominationdata = this.BlendedService.program;
      this.tenantId = this.nominationdata.tenantId;
      this.courseId = this.nominationdata.eCsrId;
      this.wfId = this.BlendedService.wfId;
    }

    if (localStorage.getItem("LoginResData")) {
      var userData = JSON.parse(localStorage.getItem("LoginResData"));
      console.log("userData", userData.data);
	  this.userId = userData.data.data.id;
	  this.tenantId=userData.data.data.tenantId;
    }
    this.getAllCoursesCC();
    this.getAllEnrolCourseModules();
    //this.ActiveTab = this.contentArray[0].id;
    //this.contentDataArray = this.contentArray[0].list;
  }
  presentToast(type, body) {
    if(type === 'success'){
      this.toastr.success(body, 'Success', {
        closeButton: false
       });
    } else if(type === 'error'){
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else{
      this.toastr.warning(body, 'Warning', {
        closeButton: false
        })
    }
  }

  coursesCC: any[];
  getAllCoursesCC() {
    const param = {
      tId: this.tenantId,
    };
    this.contentService.getAllCoursesCC(param).then(
      rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log("getAllEmployessCC:", rescompData);
        if (result["type"] == true) {
          if (result["data"][0].length == 0) {
            // this.noEmployees = true;
          } else {
            this.coursesCC = result["data"][0];
            console.log("this.coursesCC", this.coursesCC);
            // this.participantsservice.allEmployess = this.allEmployees;
            // console.log('this.participantsservice.getAllEmployees',this.participantsservice.getAllEmployees);
          }
        } else {
          // var toast: Toast = {
          //   type: "error",
          //   //title: "Server Error!",
          //   body: "Something went wrong.please try again later.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);

          this.presentToast('error', '');
        }
      },
      error => {
        this.spinner.hide();
        // var toast: Toast = {
        //   type: "error",
        //   //title: "Server Error!",
        //   body: "Something went wrong.please try again later.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);

        this.presentToast('error','');
      }
    );
  }

  ngOnInit() {
    // this.modal = false;
  }

  openModal() {
    this.couresshow = true;
  }

  closeModal() {
    this.couresshow = false;
  }

  clear() {
    if(this.search.length>=3){
      this.search= {};
      this.getAllCoursesCC();
      // this.nodata=false
    }
    else{
      this.search = {};
    }

  }

  selectItem(item) {
    if (!item.isSelected) {
      item.isSelected = true;
    } else {
      item.isSelected = false;
    }
  }

  // addSelected(){
  //   // this.allCourse.forEach(obj => {

  //   // });
  // }

  activeSelectedCourseId: any;
  setActiveSelectedCourse(currentIndex, currentCourse) {
    console.log("currentCourseOld:", currentCourse);
    this.enableSubmit=true
    this.condataCourse = {};
    this.activeSelectedCourseId = currentCourse.courseId;
    this.condataCourse = currentCourse;
    console.log("currentCourseNew:", this.condataCourse);
  }
  courseCCObj: any = {};
  saveCourse() {
    this.spinner.show();
    this.courseCCObj = {
      courseId: this.condataCourse.courseId,
      coursePicRef: this.condataCourse.coursePicRef,
      fullname: this.condataCourse.fullname,
      summary: this.condataCourse.summary
    };
    console.log("Selected Course:", this.condataCourse);
    this.couresshow = false;
    // this.badge = false;

    let param = {
      courseId: this.condataCourse.courseId,
	      wfId: this.BlendedService.wfId,
	//    this.BlendedService.program ?  this.BlendedService.program.nId : 0,
      tId: this.tenantId,
    };
    this.BlendedService.program['eCsrId'] = this.condataCourse.courseId;
    this.contentService.enrollCourseForCCContent(param).then(
      rescompData => {
        this.spinner.hide();

        var res = rescompData;
        console.log("Selected Course:", res);
        if (res["type"] == true) {
          // var toast: Toast = {
          //   type: "success",
          //   body: "Course enrolled to course.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
            if(res['data'] && res['data'].length && res['data'][0] && res['data'][0].length ){
              const msg = res['data'][0][0].msg;
              this.presentToast('success', msg);
            }else {
              this.presentToast('success', 'Course enrolled');
            }
          // this.presentToast('success', 'Course enrolled');
          this.courseSelected = true;
          this.getAllEnrolCourseModules();
          const DataTab={
            tabTitle:'Steps',
          }
          this.AddEditNominationComponent.selectedTab(DataTab);
          this.cdf.detectChanges();
        } else {
          // var toast: Toast = {
          //   type: "error",
          //   body: "Something went wrong.please try again later.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }
      },
      resUserError => {
        this.spinner.hide();
        //this.errorMsg = resUserError;
        // var toast: Toast = {
        //   type: "error",
        //   body: "Something went wrong.please try again later.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      }
    );
  }

  courseAvailable: boolean = false;
  enrolId: any;
  courseData: any= {};
  getAllEnrolCourseModules() {
    const param = {
      courseId: this.condataCourse ? this.condataCourse.courseId : this.courseId,
      wfId: this.BlendedService.wfId,
      //   this.BlendedService.program ?  this.BlendedService.program.nId : 0,
      tId: this.tenantId,
    };

    this.contentService.getAllEnrolCourseModule(param).then(
      rescompData => {
        this.spinner.hide();

        const res = rescompData;
        console.log("ModulesCC:", res);
        if (res["type"] == true) {
          const Finres: any = res;
          if (Finres.data.length == 0) {
            this.courseAvailable = false;
          } else {
            this.courseAvailable = true;
            this.contentArray = Finres.data;
            console.log('this.contentArray', this.contentArray);
            // for (var i = 0; i < this.contentArray.length; i++) {
            //   var tempModule: any = i + 1;
            //   this.contentArray[i].id = i;
            //   this.contentArray[i].moduleName = 'Module' + ' ' + tempModule;
            // }
            this.ActiveTab = this.contentArray[0].id;
            this.contentDataArray = this.contentArray[0].list;
            //this.enrolId = this.contentDataArray[0].enrolId;
            //console.log("enrolId", this.enrolId);
            console.log("this.contentDataArray", this.contentDataArray);
            // for (var i = 0; i < this.contentDataArray.length; i++) {
            //   const tempActivity: any = i + 1;
            //   this.contentDataArray[i].img = "assets/images/open-book-leaf.jpg";
            //   this.contentDataArray[i].activityName = 'Activity' + ' ' + tempActivity;
            // }
            console.log("CD===>", this.contentArray);
            this.courseData = this.contentArray[0].list[0];
            console.log('this.courseData', this.courseData);
            // var enrolId = this.contentArray.find(function (element){
            // 	return element.enrolId
            // });
            // console.log('enrolId',enrolId);
            this.spinner.hide();
            console.log("ModulesCC:", res);
            console.log("currentCourseNew:", this.condataCourse);
            this.TabSwitcher();
          }
        } else {
          // var toast: Toast = {
          //   type: "error",
          //   body: "Something went wrong.please try again later.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }
      },
      resUserError => {
        this.spinner.hide();
        //this.errorMsg = resUserError;
        // var toast: Toast = {
        //   type: "error",
        //   body: "Something went wrong.please try again later.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      }
    );
  }
  ActiveTab: any;

  TabSwitcher(){
    this.AddEditNominationComponent.tabEnabler.codeOfConduct = false;
    this.AddEditNominationComponent.tabEnabler.content = false;
    this.AddEditNominationComponent.tabEnabler.enrolTab = false;
    this.AddEditNominationComponent.tabEnabler.nomineeTab = false;
    this.AddEditNominationComponent.tabEnabler.detailsTab = false;
    this.AddEditNominationComponent.tabEnabler.steps = true;
    console.log(this.AddEditNominationComponent);
  }

  goToBlendedCourse(){
    this.router.navigate(["content/course-details"], {
      relativeTo: this.routes
    });
  }
  // getCourseDropdownList() {
  //   this.spinner.show();
  //   let data = {
  //     tId: this.tenantId,
  //   }
  //   this.detailsService.getDropdownList(data).then(rescompData => {
  //     // this.loader =false;
  //     this.spinner.hide();
  //     this.courseDropdownList = rescompData['data'];
  //     // console.log('Course Dropdown List ', this.courseDropdownList);
  //     try {
  //       for (let i = 0; i < this.courseDropdownList.programData.length; i++) {
  //         this.courseDropdownList.programData[i].programId = this.courseDropdownList.programData[i].id;
  //       }
  //       for (let i = 0; i < this.courseDropdownList.courseLevel.length; i++) {
  //         this.courseDropdownList.courseLevel[i].courseLevelId = this.courseDropdownList.courseLevel[i].id;
  //       }
  //       for (let i = 0; i < this.courseDropdownList.creatorList.length, i++;) {
  //         this.courseDropdownList.creatorList[i].creatorId = this.courseDropdownList.creatorList[i].id;
  //       }
  //       for (let i = 0; i < this.courseDropdownList.locationData.length; i++) {
  //         this.courseDropdownList.locationData[i].locationId = this.courseDropdownList.locationData[i].id;
  //       }
  //       for (let i = 0; i < this.courseDropdownList.venueData.length; i++) {
  //         this.courseDropdownList.venueData[i].venueId = this.courseDropdownList.venueData[i].id;
  //       }

  //     } catch (err) {
  //       console.log(err);
  //     }
  //     // this.noContent = false;

  //   },
  //     resUserError => {
  //       // this.loader =false;
  //       this.spinner.hide();
  //       this.errorMsg = resUserError
  //     });
  // }

  // public addeditcontent(data, id) {
  //   var data1 = {
  //     data: data,
  //     id: id,
  //     catId: this.categoryId,
  //     courseDropdowns: this.courseDropdownList,
  //   }
  //   if (data == undefined) {
  //     this.passService.data = data1;
  //     // this.router.navigate(['/pages/plan/blended-home/blended-courses/addEditCourseContent']);
  //     this.router.navigate(['addEditCourseContent'], { relativeTo: this.route });
  //     console.log("Data passed to service" + data1);
  //     console.log("ID Passed" + data1.id + " " + data1.data);
  //   } else {
  //     this.passService.data = data1;
  //     // this.router.navigate(['/pages/plan/blended-home/blended-courses/addEditCourseContent']);
  //     this.router.navigate(['addEditCourseContent'], { relativeTo: this.route });
  //   }
  // }
  tabChanged(data) {
    this.spinner.show();
    console.log("DATA--->", data);
    this.ActiveTab = data.id;
    this.contentDataArray = this.contentArray[data.id].list;
    // for (var i = 0; i < this.contentDataArray.length; i++) {
    //   var temp: any = i + 1;
    //   this.contentDataArray[i].img = "assets/images/open-book-leaf.jpg";
    //   this.contentDataArray[i].activityName = "Activity" + " " + temp;
    // }
    this.spinner.hide();
  }

  romoveCourseModules(data) {
    console.log("data", data);
    let param = {
      tId: this.tenantId,
	  wfId:  this.BlendedService.wfId,
	//   this.BlendedService.program ?  this.BlendedService.program.nId : 0,
      // "aId":20,
      // "calId":this.callId,
      // "empId":this.empId
    };
    this.contentService.removeEnrolCourse(param).then(
      rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log("Remove Course Result", rescompData);
        if (result["type"] == true) {
          // var toast: Toast = {
          //   type: "success",
          //   //title: "Server Error!",
          //   body: "Course removed successfully.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          // this.presentToast('success', 'Course removed');
          if(result['data'] && result['data'].length && result['data'][0] && result['data'][0].length ){
            const msg = result['data'][0][0].msg;
            this.presentToast('success', msg);
          }else {
            this.presentToast('success', 'Course removed');
          }
          this.enrolId = null;
          this.courseAvailable = false;
          this.BlendedService.program.eCsrId = null;
        } else {
          // var toast: Toast = {
          //   type: "error",
          //   //title: "Server Error!",
          //   body: "Something went wrong.please try again later.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }
      },
      error => {
        this.spinner.hide();
        // var toast: Toast = {
        //   type: "error",
        //   //title: "Server Error!",
        //   body: "Something went wrong.please try again later.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      }
    );
  }

  claer()
  {
    this.search ={}
  }

  goToCourseDetails(data) {
    this.router.navigate(["content/course-details"], {
      relativeTo: this.routes
    });
  }
}
