import { Component, OnInit, ChangeDetectorRef, ViewEncapsulation, ViewChild } from '@angular/core';
import { BlendedService } from '../../../blended.service';
import { ContentService } from '../../add-edit-nomination/content.service';
import { DatePipe } from '@angular/common';
// import { ToasterService, Toast } from 'angular2-toaster';
import { NgxSpinnerService } from 'ngx-spinner';
import { webApi } from '../../../../../../service/webApi';
import { DomSanitizer } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import * as _moment from 'moment';
import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';
import { BrandDetailsService } from '../../../../../../service/brand-details.service';
import { noData } from '../../../../../../models/no-data.model';

const moment = (_moment as any).default ? (_moment as any).default : _moment;

export const MY_CUSTOM_FORMATS = {
  fullPickerInput: 'DD-MM-YYYY HH:mm:ss',
    parseInput: 'DD-MM-YYYY HH:mm:ss',
    datePickerInput: 'DD-MM-YYYY',
    timePickerInput: 'LT',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY'
};
@Component({
  selector: 'ngx-step-wise-data',
  templateUrl: './step-wise-data.component.html',
  styleUrls: ['./step-wise-data.component.scss'],
  providers: [
    {provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE]},
    {provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS}],
  encapsulation: ViewEncapsulation.None,
})
export class StepWiseDataComponent implements OnInit {
  @ViewChild('tab1')
  tabRef: any;
  loader: boolean = false;
  stepsData: any;
  selectedstep: any;
  data: any;
  allData: any;
  tabsArray: any = [];
  dataLoaded: boolean = false;
  BHApproved: any = [];
  loginUserdata: any = [];
  wfId: any;
  noParticipants = false;
  empId: any;
  dataPopUp: any;
  finaldata: any = [];
  approveparam: any = [];
  webinar: any = [];
  isapprove = 0;
  showPopup = false;
  marked = false;
  theCheckbox = false;
  modal_showPopup: boolean = false;
  showDetail: boolean = false;
  acceptBox = false;
  pubValue: any;
  trainers: any = [];
  step: any = [];
  employee_Detail: any = [];
  evaluationCallDetails: any = [];
  feedback: any = [];
  feedbackShow: boolean = false;
  callStartDateException: boolean = false;
  addWebinarEvent: boolean = false;
  webEventConfirm: boolean = false;
  webEventConfirmMsg: any;
  fgAssessment: any = [];
  noQuizAttempted: boolean = false;
  quizData: any = [];
  assessment: any = [];
  feedbackShowKT: boolean = false;
  vivaFeedBackObject: any = [];
  feedbackShowVD: boolean = false;
  batchDetails: any = [];
  batchSelected: boolean = false;
  mockFeedBackObject: any = [];
  mockshowVD: boolean = false;
  assesmentshowVD: boolean = false;
  assesmentFeedBackObject: any = [];
  CerPresent: boolean = false;
  CertificateObject: any = [];
  evalfbobject: any = {};
  fgAssessmentobj: any = {};
  Certmsg: any;
  disableSubmit = false;
  comment: any;
  selectedIndex = 0;
  errflag: Boolean = false;
  rejRes: any = '';
  errflag1: Boolean = false;
  ignoreColArray: any = [];
  currentTabLabel: string= '';
  selectedTrainer: any =[];
  currentBrandData: any;
  noDataVal:noData={
		margin:'mt-3',
		imageSrc: '../../../../../assets/images/no-data-bg.svg',
		title:"No Workflow at this time.",
		desc:"",
		titleShow:false,
		btnShow:false,
		descShow:false,
		btnText:'Learn More',
		btnLink:'https://faq.edgelearning.co.in/kb/learning-how-to-add-notification-to-a-batch',
	  }
  constructor(public blendedService: BlendedService, public contentservice: ContentService,
    private cdf: ChangeDetectorRef, private datepipe: DatePipe,
    public brandService: BrandDetailsService,
    // public toasterService: ToasterService,
    private spinner: NgxSpinnerService, private sanitizer: DomSanitizer, private toastr: ToastrService) {
  }
  settings = {
    text: 'Select Trainer',
    singleSelection: true,
    classes: 'myclass custom-class',
    primaryKey: 'ID',
    labelKey: 'trainerName',
    noDataLabel: 'Search Trainer...',
    enableSearchFilter: true,
    searchBy: ['trainerName'],
    lazyLoading: true,
    maxHeight: 250,
    enableFilterSelectAll: false,
 };
  AfterViewInIt() {
  }

  closeModal(event) {
    this.modal_showPopup = false;
  }
  ngOnInit() {
    this.loginUserdata = JSON.parse(localStorage.getItem('LoginResData'));
    this.wfId = this.blendedService.wfId;
    this.steps();
    this.trainerlist();
    this.currentBrandData = this.brandService.getCurrentBrandData();
  }

  trainerlist() {
    const data = {
      tId: this.loginUserdata.data.data.tenantId,
    };
    this.blendedService.trainerList(data).then(res => {
      if (res['type'] === true) {
        try {
          this.trainers = res['data'];
          console.log('Trainers List', this.trainers);
        } catch (e) {
          console.log(e);
        }
      }
    }, err => {
      console.log(err);
    });
  }
  noStepsFound: boolean = false;
  steps() {
    const param = {
      wfId: this.blendedService.wfId,
      tId: this.loginUserdata.data.data.tenantId,
    };
    console.log(param);
    this.contentservice.stepwisedata(param).then(res => {
      console.log(res);
      if (res['type'] === true) {
        try {
          this.stepsData = res['data'];
          if (this.stepsData.length > 0) {
            try {
              this.step = this.stepsData[0];
              this.selectedstep = this.stepsData[0].id;
              this.stepdetail(this.step);
              this.noStepsFound = false;
            } catch (e) {
              console.log(e);
            }
          } else {
            this.noStepsFound = true;
          }
        } catch (e) {
          console.log(e);
        }
      }
    }, err => {
      console.log(err);
    });
  }

  evaluationDropdown: any = [];

  stepdetail(step) {
    this.data = [];
    this.step = step;
    if( this.selectedstep != step.id){
      this.currentTabLabel = '';
    }
    this.selectedstep = step.id;
    this.dataLoaded = false;
    this.tabsArray = [];
    var param = {
      wfId: this.wfId,
      sId: this.selectedstep,
      tId: this.loginUserdata.data.data.tenantId,
    };
    console.log(param);
    this.contentservice.stepwiseuserdata(param).then(res => {
      console.log();
      if (res['type'] === true) {
        try {
          this.allData = res['data'];
            console.log('Data ===>', this.allData);
            if(this.allData) {
              console.log('Tab index old ==.', this.selectedIndex);
              this.tabsArray = Object.keys(this.allData);
              console.log('Tabs Array ==>', this.tabsArray);
              this.isapprove = 0;
              this.theCheckbox = false;
              let tabIndexSelected = null;
              if(this.tabsArray && this.tabsArray.length !== 0){
                for(let index = 0 ; index < this.tabsArray.length ; index ++){
                  if(this.tabsArray[index] == this.currentTabLabel){
                    tabIndexSelected = index;
                    break;
                  }else {
                    tabIndexSelected = 0;
                  }
                }

              }
              setTimeout(() =>{
                // this.selectedIndex = tabIndexSelected;
                 this.tabRef.selectedIndex = tabIndexSelected;
                // this.cdf.markForCheck();
                console.log('Tabs Array ==>', this.selectedIndex);
              }, 100)

              // this.tabsLoaded = true;
            }
            if(this.selectedstep == 3){
              this.evaluationDropdown = res['dropDown'];
            }
            this.data = this.allData[this.tabsArray[0]];
            if(this.data.length !== 0){
              this.dataLoaded = false;
            }else{
              this.dataLoaded = true;
            }
          // if(this.selectedstep == 2 || this.selectedstep == 3 || this.selectedstep == 4) {
          //   this.allData = res['data'];
          //   console.log('Data ===>', this.allData);
          //   if(this.allData) {
          //     this.tabsArray = Object.keys(this.allData);
          //     console.log('Tabs Array ==>', this.tabsArray);
          //   }
          //   this.data = this.allData[this.tabsArray[0]];
          // }else{
          //   this.data = res['data'];
          //   console.log('Data ===>', this.data);
          //   if (this.data) {
          //     for (let i = 0; i < this.data.length; i++) {
          //       if (this.data[i].isSelected) {
          //         this.data[i].isSelected = this.data[i].isSelected === 'true' ? true : false;
          //       }
          //     }
          //
          //   }
          // }
          this.noParticipants = true;
        } catch (e) {
          console.log(e);
        }
      }
    }, err => {
      console.log(err);
    });
  }

  tabChanged(data){
    console.log('Tab Changed ==>', data);
    console.log('Tab Changed ==>', this.selectedIndex);
    // if(data.tab)
    if(data.index !== -1) {
      this.currentTabLabel = data.tab.textLabel;
      // if(this.currentTabLabel == 'Rejected'){
      //   this.
      // }
      // if(this.currentTabLabel == 'Approved'){

      // }
      this.isapprove = null;
      this.data = this.allData[this.tabsArray[data.index]];
      if( this.currentTabLabel === 'Rejected') {
        this.disableSubmit = false;
      }
    }
  }
  repeatCssFunction(col) {
    return 'repeat(' + col + ', 1fr)';
  }
  displayDataFromChild(data) {
    console.log('Child Data ====>', data);
    this.finaldata = data;
  }
  updata(value) {
    // this.finaldata.appStat = value;
    if (this.comment) {
      if (this.finaldata.length == 0){
        this.toastr.info('Info', 'Please Select '+ this.currentBrandData.employee.toLowerCase() +'s first');
      }else {
      this.empbind(this.finaldata, response => {
        if (response) {
          this.finaldata.appStat = value;
          console.log(this.finaldata);
          const data = {
            appStat: this.finaldata.appStat,
            userId: this.loginUserdata.data.data.id,
            wfId: this.wfId,
            allstr: this.empId,
            tId: this.loginUserdata.data.data.tenantId,
            comment: this.comment
          };
          this.loader = true
          this.contentservice.stepwiseBhapproval(data).then(res => {
            console.log(res);
            this.loader = false;
            if (res['type'] === true) {
              this.presentToast('success', res['data'][0]['msg']);
              this.stepdetail(this.step);
              this.comment = '';
              // this.getBhapprovalStatus(this.dataPopUp.empId, this.step);
            }
          }, err => {
            this.loader = false;
            this.presentToast('error', '');
            console.log(err);
          });
        } else {
          this.loader = false;
          this.presentToast('error', '');
          console.log('something went wrong');
        }
      });
    }
    } else {
      this.errflag = true;
    }
  }

  evalutionapprove() {
    this.spinner.show()
    if(this.finaldata.length == 0){
      this.toastr.info('Info', 'Please Select '+ this.currentBrandData.employee.toLowerCase() +'s first');
      this.loader = false
      this.spinner.hide()
    }else{
      //this.loader = false;
      if(this.currentTabLabel === 'Pending'){
        this.loader = false;
      this.spinner.hide()
        if(this.isapprove == 1){
          this.empbind(this.finaldata, response => {
            if (response) {
              console.log(this.finaldata);
              this.callEvaluationApprove();
            }
          });
        }else if (this.isapprove == 0){
          if(this.rejRes != 0){
            this.empbind(this.finaldata, response => {
              if (response) {
                console.log(this.finaldata);
                this.callEvaluationApprove();
              }
            });
          }else {
            this.loader = false;
            this.spinner.hide()
            this.errflag1 = true;
          }
        }else{
          this.loader = false;
          this.spinner.hide()
            // this.errflag1 = true;
            this.toastr.warning('Please select approve or reject', 'Warning');
        }
      }
      if(this.currentTabLabel === 'Approved'){
        this.loader = false;
      this.spinner.hide()
       if (this.isapprove == 0){
          if(this.rejRes !== 0){
            this.empbind(this.finaldata, response => {
              if (response) {
                console.log(this.finaldata);
                this.callEvaluationApprove();
              }
            });
          }else {
            this.errflag1 = true;
          }
        }else{
          if(this.theCheckbox){
            this.isapprove = null;
            this.empbind(this.finaldata, response => {
              if (response) {
                console.log(this.finaldata);
                this.callEvaluationApprove();
              }
            });
          }else{
            this.loader = false;
            this.spinner.hide()
            this.toastr.warning('Please checked any one text box from options and then submit', 'Warning');

          }
            // this.errflag1 = true;
        }
      }
      if(this.currentTabLabel === 'Rejected'){
        this.loader = false;
      this.spinner.hide()
        if(this.isapprove == 1){
          this.empbind(this.finaldata, response => {
            if (response) {
              console.log(this.finaldata);
              this.callEvaluationApprove();
            }
          });
        }else{
           if(this.theCheckbox){
             this.isapprove = null;
             this.empbind(this.finaldata, response => {
              if (response) {
                console.log(this.finaldata);
                this.callEvaluationApprove();
              }
            });
           }else{
             this.loader = false;
             this.spinner.hide()
            this.toastr.warning('Please checked any one text box from options and then submit', 'Warning');

          }
         }
       }
    }

  }

  callEvaluationApprove(){
    if (this.marked === true) {
      this.pubValue = 1;
    } else {
      this.pubValue = 0;
    }
    const data = {
      appStat: this.isapprove,
      userId: this.loginUserdata.data.data.id,
      wfId: this.wfId,
      stepId: this.step.id,
      allstr: this.empId,
      tId: this.loginUserdata.data.data.tenantId,
      pubValue: this.pubValue,
      rejRes: this.rejRes,
    };
    console.log(data);
    this.contentservice.stepwiseevelutionapproval(data).then(res => {
      this.loader = false;
      this.spinner.hide()
      console.log(res);
      if (res['type'] === true) {
        this.presentToast('success', res['data'][0].msg);
        this.stepdetail(this.step);
      }
    }, err => {
      this.loader = false;
      this.spinner.hide()
      this.presentToast('error', '');
      console.log(err);
    });

  }

  empbind(data: any, cb) {
    this.empId = '';
    for (let i = 0; i < data.length; i++) {
      if (data[i].isSelected === true) {
        if (this.empId !== '') {
          this.empId += '|';
        }
        this.empId += data[i].empId;
      }
    }
    cb(this.empId);
  }

  validateEndDate(data) {
    // this.minDateto = data;
  }
  radioValue(data) {
    console.log("Data of Radio ===>", data);
    this.isapprove = data;
    if (data === 0) {
     
        this.rejRes = null;
      
        this.rejResTArea = true;
      
      this.disableSubmit = true;
    } else {
      this.disableSubmit = false;
    }
  }

  rejResTArea: boolean = false;
  onCheckValue(data, checkValue){
    if(checkValue == 'approve'){
      this.rejResTArea = false;
      // this.rejRes = null;
      this.disableSubmit = false;
      if(data.target.checked){
        this.isapprove = 1;
      }else {
        this.isapprove = null;
      }

    }
    else if(checkValue == 'reject'){
      this.rejResTArea = true;
      this.rejRes = null;
      this.isapprove = data.target.checked ? 0 : null;
        // if(data == 0){
        //   this.disableSubmit = true;

        // }else{
        //   this.disableSubmit = false;

        // }

        this.disableSubmit = true;
    }
}
  submit() {
    if (this.comment) {
      this.errflag = false;
    } else {
      this.errflag = true;
    }
  }
  submit1() {
    console.log(this.rejRes);
    if (this.rejRes) {
      this.errflag1 = false;
      this.disableSubmit = false;
    } else {
      this.errflag1 = true;
      this.disableSubmit = true;
    }
  }

  webinarSubmit(webinar) {
    console.log(webinar);
    if(this.finaldata.length == 0){
      this.toastr.info('Info', 'Please Select '+ this.currentBrandData.employee.toLowerCase() +'s first');
    }else {
      this.empbind(this.finaldata, response => {
        if (response) {
          const data = {
            userId: this.loginUserdata.data.data.id,
            wfId: this.wfId,
            allstr: this.empId,
            tId: this.loginUserdata.data.data.tenantId,
            trainerId: webinar.trainer[0]['ID'],
            eventDate: this.formatDate(webinar.startdate),
            eventStartTime: this.formatSendDateTime(webinar.fromtime),
            eventEndTime: this.formatSendDateTime(webinar.totime),
            stepId: this.step.id,
          };
          this.loader = true;
          console.log(data);
          this.contentservice.stepwiseenerolewebinar(data).then(res => {
            this.loader = false;
            console.log(res);
            if (res['type'] === true) {
              this.presentToast('success', 'Webinar updated');
              this.stepdetail(this.step);
            }
          }, err => {
            this.loader = false;
            this.presentToast('error', '');
            console.log(err);
          });
        } else {
          this.loader = false;
          this.presentToast('error', '');
        }
      });
    }

  }

  bindPopUpDate(data) {
    console.log('Bind Pop Data====>', data);
    this.dataPopUp = data;
    this.stepFunctionToCallServices(data.empId, this.step);
  }

  stepFunctionToCallServices(empId, step) {
    switch (parseInt(step.id)) {
      case 1: this.getNomineeDetails(empId, step);
        break;
      case 2: this.getBhapprovalStatus(empId, step);
        break;
      case 3: this.getEvaluationCallDetail(empId, step);
        break;
      // case 4: null;
      //   break;
      case 4: this.getWebinarEventByEmpId(empId, step);
        break;
      case 5: this.getObserverDetail(empId, step);
        break;
      case 6: this.getFgAssessmentDetails(empId, step);
        break;
      case 7: this.getKnowledgeTestDetails(empId, step);
        break;
      case 8: this.getVivaDetails(empId, step);
        break;
      case 9: this.getBatchDetail(empId);
        break;
      case 10: this.getMockDetails(empId, step);
        break;
      case 11: this.getAssesmentDetails(empId, step);
        break;
      case 12: this.getCertificateDetails(empId);
        break;
      case 14: this.getMockDetails(empId, step);
        break;
      default: return;
    }
  }

  getNomineeDetails(empId, step) {
    this.spinner.show();
    const param = {
      'wfId': this.blendedService.wfId,
      'empId': empId,
      'tId': this.loginUserdata.data.data.tenantId,
    };
    console.log('nomineeDetailsParams:', param, empId, step);
    this.contentservice.getNomineeDetails(param)
      .then(res => {
        this.spinner.hide();
        const result = res;
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          console.log('nominee Details:', result['data']);
          const nomDetails = result['data'][0];
          this.employee_Detail = nomDetails;
          this.showDetail = true;
          this.selectedstep = step.id;
          console.log('nomDetails', nomDetails);

          this.timewoutfun(this.employee_Detail, this.selectedstep);
        }
      },
        error => {
          this.spinner.hide();
          this.presentToast('error', '');
        });
  }
  getBhapprovalStatus(empId, step) {
    this.spinner.show();
    const param = {
      'wfId': this.blendedService.wfId,
      'empId': empId,
      'tId': this.loginUserdata.data.data.tenantId,
    };
    console.log('bhApprovalParams:', param, empId, step);
    this.contentservice.getBhApprovalStatus(param)
      .then(res => {
        this.spinner.hide();
        const result = res;
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          console.log('BH Approval:', result['data']);
          if (result['data'].length > 0) {
            var bhstatus = result['data'][0];
          }
          if (bhstatus) {
            if (bhstatus.approveStatus) {
              this.BHApproved.status = true;
              this.BHApproved.BHName = bhstatus.BHName;
              this.BHApproved.onDate = this.formatDate(bhstatus.approveDate);
              this.BHApproved.onTime = this.formatTime(bhstatus.approveDate);
              this.BHApproved.approveStatus = bhstatus.approveStatus;
            } else {
              this.BHApproved.BHId = bhstatus.BHId;
              this.BHApproved.status = false;
              this.BHApproved.BHName = bhstatus.BHName;
            }
          } else {
            this.BHApproved = [];
          }
          this.showDetail = true;
          // this.selectedstep = step.stepId;
          this.timewoutfun(this.BHApproved, this.selectedstep);
          console.log('bhstatus', bhstatus);
        }
      },
        error => {
          this.spinner.hide();
          this.presentToast('error', '');
        });
  }

  getEvaluationCallDetail(empId, step) {
    const param = {
      'wfId': this.blendedService.wfId,
      'stepId': step.id,
      'tId': this.loginUserdata.data.data.tenantId,
      'empId': empId,
      'actId': step.activityId,
    };
    console.log('param', param);
    this.contentservice.getEvaluationCallDetail(param).then(
      res => {
        this.spinner.hide();
        const result = res;
        console.log('result', result);
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          console.log('Evaluation Call:', result['data']);
          // let res = result['data'][0];
          const evaluationCallDetailsRes = result['data'][0];
          if (evaluationCallDetailsRes.length > 0) {
            this.evaluationCallDetails = evaluationCallDetailsRes[0];
            this.evaluationCallDetails.callDate = this.formatDate(this.evaluationCallDetails.callDate);
            // this.evaluationCallDetails.callEndDate = this.formatDate(this.evaluationCallDetails.callEndDate);
            this.evaluationCallDetails.callStartTime = this.formatTime(this.evaluationCallDetails.callStartTime);
            this.evaluationCallDetails.callEndTime = this.formatTime(this.evaluationCallDetails.callEndTime);
            console.log(this.evaluationCallDetails.callStartDate);
            console.log(this.evaluationCallDetails.callEndDate);
          } else {
            this.evaluationCallDetails = [];
          }

          this.feedback = result['data'][1];
          if (this.feedback) {
            if (this.feedback.length == 0) {
              this.feedbackShow = false;
            } else {
              this.feedbackShow = true;
              this.evalfbobject = {
                evaluationCallDetails: this.evaluationCallDetails,
                feedback: this.feedback,
              };
            }
          }
          this.timewoutfun(this.evalfbobject, this.selectedstep);
          if (evaluationCallDetailsRes.length === 0) {
            this.callStartDateException = false;
          } else {
            this.callStartDateException = true;
          }
          this.showDetail = true;
        }
        this.spinner.hide();
        // this.presentToast('error', 'Evaluation Call', 'Something went wrong.please try again later.');
      },
      error => {
        this.spinner.hide();
        this.presentToast('error', '');
      },
    );
  }

  getWebinarEventByEmpId(empId, step) {
    this.spinner.show();
    const param = {
      'wfId': this.blendedService.wfId,
      'empId': empId,
      'tId': this.loginUserdata.data.data.id,
    };
    console.log(param);
    this.contentservice.getExistingWebinarEventByEmpId(param).then(
      res => {
        this.spinner.hide();
        const result = res;
        console.log('webinar call details', res);
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          if (result['data'].length === 0) {
            this.addWebinarEvent = true;
            this.webinar.eventId = 0;
          } else {
            this.addWebinarEvent = false;
            const webEvent = result['data'][0];
            if (webEvent.bookDate) {
              this.webEventConfirm = true;
              this.webEventConfirmMsg = 'Webinar event confirmed by nominee on' +
                this.formatDate(webEvent.bookDate) + '.' +
                'Event date is' + ' ' + this.formatDate(webEvent.callDate) + '.' +
                'Event start tiem is' + ' ' + this.formatTime(webEvent.callStartTime) + '.' + ' ' +
                'Event end date is' + ' ' + this.formatTime(webEvent.callEndTime)
            } else {
              this.webEventConfirm = false;
              this.webinar = {
                trainer: webEvent.trainerId,
                startdate: new Date(webEvent.callStartDate),
                enddate: new Date(webEvent.callEndDate),
                time: new Date(webEvent.callTime),
                eventId: webEvent.eventId,
                bookDate: new Date(webEvent.bookDate),
              };
            }
            console.log(this.webEventConfirm);
          }
          //this.presentToast('success', 'Webinar', 'Event added successfully.');
          console.log('Webinar Event:', result['data']);
          console.log('this.webinar:', this.webinar);
          this.showDetail = true;
          // this.selectedstep = step.stepId;
          // this.timewoutfun(this.webinar, this.selectedstep);
        }
      },
      error => {
        this.spinner.hide();
        this.presentToast('error', '');
      },
    );
  }

  getObserverDetail(empId, step) {
    console.log(empId, step);
  }

  getFgAssessmentDetails(empId, step) {
    const param = {
      'tId': this.loginUserdata.data.data.tenantId,
      'empId': empId,
      'aId': step.activityId,
    };
    console.log('param', param);
    this.contentservice.getFgAssessmentDetails(param).then(
      res => {
        this.spinner.hide();
        const result = res;
        console.log('getFgAssessmentDetails', result);
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          let correctCount: any = 0;
          let wrongCount: any = 0;
          let pendingCount: any = 0;
          this.fgAssessment = result['data'][0];
          console.log('Fg Assessment:', this.fgAssessment);
          if (this.fgAssessment.length == 0) {
            this.noQuizAttempted = true;
          } else {
            this.noQuizAttempted = false;
            for (let i = 0; i < this.fgAssessment.length; i++) {
              this.quizData.totalQue = this.fgAssessment.length;
              if (this.fgAssessment[i].answerCorrect == 2) {
                correctCount++;
              } else if (this.fgAssessment[i].answerCorrect == 1) {
                pendingCount++;
              } else if (this.fgAssessment[i].answerCorrect == 0) {
                wrongCount++;
              }
              if (this.fgAssessment[i].questionFormatId == 1) {
                let str = new String(this.fgAssessment[i].answers);
                // const qt1 = str.replace(/#/g, '|');
                const qt1 = str.replace(/%hash%/g, '|');
                this.fgAssessment[i].formattedAns = qt1;
              }
              if (this.fgAssessment[i].questionFormatId == 2 || this.fgAssessment[i].questionFormatId == 3) {
                const str = new String(this.fgAssessment[i].answers);
                const qt2 = str.replace(/%hash%/g, '-');
                const str1 = new String(qt2);
                const qt21 = str1.replace(/'%pipe%'/g, ',');
                this.fgAssessment[i].formattedAns = qt21;
              }
              if (this.fgAssessment[i].questionFormatId == 4) {
                this.fgAssessment[i].formattedAns = this.fgAssessment[i].answers;
              }
            }
            this.quizData.correctCount = correctCount;
            this.quizData.wrongCount = wrongCount;
            this.quizData.pendingCount = pendingCount;
            this.fgAssessmentobj = {
              fgAssessment: this.fgAssessment,
              quizData: this.quizData,
            }
            this.timewoutfun(this.fgAssessmentobj, this.selectedstep);
          }
        }
      },
      error => {
        this.spinner.hide();
        this.presentToast('error', '');
      },
    );
  }

  getKnowledgeTestDetails(empId, step) {
    const param = {
      'tId': this.loginUserdata.data.data.tenantId,
      'empId': empId,
      'actId': step.activityId,
    };
    console.log('param', param);
    this.contentservice.getKnowledgeTestDetails(param).then(
      res => {
        this.spinner.hide();
        const result = res;
        console.log('result', result);
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          //this.presentToast('success', 'BH Approval', 'Approved successfully.');
          this.assessment = result['data'][0];
          console.log('Assessment', this.assessment);
          if (this.assessment.length == 0) {
            this.feedbackShowKT = false;
          } else {
            this.feedbackShowKT = true;
          }
          console.log('Knowledge Test:', this.assessment);
          this.timewoutfun(this.assessment, this.selectedstep);
        }
      },
      error => {
        this.spinner.hide();
        this.presentToast('error', '');
      },
    );
  }

  getVivaDetails(empId, step) {
    const param = {
      'tId': this.loginUserdata.data.data.tenantId,
      'empId': empId,
      'actId': step.activityId,
    };
    console.log('param', param);
    this.contentservice.getVivaDetails(param).then(
      res => {
        this.spinner.hide();
        const result = res;
        console.log('getVivaDetails', result);
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          //this.presentToast('success', 'BH Approval', 'Approved successfully.');
          this.vivaFeedBackObject = result['data'][0];
          if (this.vivaFeedBackObject.length == 0) {
            this.feedbackShowVD = false;
          } else {
            this.feedbackShowVD = true;
          }
          this.timewoutfun(this.vivaFeedBackObject, this.selectedstep);
          console.log('Viva Details:', this.vivaFeedBackObject);
        }
      },
      error => {
        this.spinner.hide();
        this.presentToast('error', '');
      },
    );
  }

  getBatchDetail(empId) {
    const param = {
      'wfId': this.blendedService.wfId,
      'tId': this.loginUserdata.data.data.tenantId,
      'empId': empId,
    };
    console.log('param', param);
    this.contentservice.getbatchDetailStepTen(param).then(
      res => {
        this.spinner.hide();
        const result = res;
        console.log('result', result);
        if (result['type'] === false) {
          this.batchDetails = [];
          this.presentToast('error', '');
        } else {
          console.log('Batch Details:', result['data']);
          // let res = result['data'][0];
          let batchDetailsRes = result['data'];
          if (batchDetailsRes.length > 0) {
            this.batchDetails = batchDetailsRes[0];
            this.batchDetails.validFromDate = this.formatDate(this.batchDetails.validFromDate);
            this.batchDetails.validToDate = this.formatDate(this.batchDetails.validToDate);
            console.log(this.batchDetails.validFromDate);
            console.log(this.batchDetails.validToDate);
          }
          if (batchDetailsRes.length === 0) {
            this.batchSelected = false;
            this.batchDetails = [];
          } else {
            this.batchSelected = true;
          }
          this.showDetail = true;

          this.timewoutfun(this.batchDetails, this.selectedstep);
        }
        this.spinner.hide();
        // this.presentToast('error', 'Evaluation Call', 'Something went wrong.please try again later.');
      },
      error => {
        this.spinner.hide();
        this.presentToast('error', '');
      },
    );
  }

  getMockDetails(empId, step) {
    const param = {
      'tId': this.loginUserdata.data.data.tenantId,
      'empId': empId,
      'actId': step.activityId,
    };
    console.log('param', param);
    this.contentservice.getmockStepEleven(param).then(
      res => {
        this.spinner.hide();
        const result = res;
        console.log('getMockDetails', result);
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          //this.presentToast('success', 'BH Approval', 'Approved successfully.');
          this.mockFeedBackObject = result['data'][0];
          if (this.mockFeedBackObject.length == 0) {
            this.mockshowVD = false;
          } else {
            this.mockshowVD = true;
          }
          this.showDetail = true;
          this.timewoutfun(this.mockFeedBackObject, this.selectedstep);
          console.log('Mock Details:', this.mockFeedBackObject);
        }
      },
      error => {
        this.spinner.hide();
        this.presentToast('error', '');
      },
    );
  }

  getAssesmentDetails(empId, step) {
    const param = {
      'tId': this.loginUserdata.data.data.tenantId,
      'empId': empId,
      'actId': step.activityId,
    };
    console.log('param', param);
    this.contentservice.getAssesmentSteptwelve(param).then(
      res => {
        this.spinner.hide();
        const result = res;
        console.log('getAssesmentDetails', result);
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          //this.presentToast('success', 'BH Approval', 'Approved successfully.');
          this.assesmentFeedBackObject = result['data'][0];
          if (this.assesmentFeedBackObject.length == 0) {
            this.assesmentshowVD = false;
          } else {
            this.assesmentshowVD = true;
          }
          this.showDetail = true;
          this.timewoutfun(this.assesmentFeedBackObject, this.selectedstep);
          console.log('Assesment Feedback Details:', this.assesmentFeedBackObject);
        }
      },
      error => {
        this.spinner.hide();
        this.presentToast('error', '');
      },
    );
  }

  getCertificateDetails(empId) {
    const url = webApi.domain + webApi.url.getCertificateLink + 'empId=' +
      empId + '&wfId=' + this.blendedService.wfId + '&tId=' + this.loginUserdata.data.data.tenantId + '&type=app';
    console.log('url', url);
    this.contentservice.getCertifiacteUrl(url).then(
      res => {
        this.spinner.hide();
        const result = res;
        console.log('getCertificateDetails', result);
        if (result['type'] === false) {
          this.presentToast('error', '');
        } else {
          if (result['data'].filePath) {
            this.CertificateObject = result['data'];
            this.CertificateObject.demopth = webApi.domain + '/' + this.CertificateObject.filePath;
            if (this.CertificateObject.demopth) {
              this.CertificateObject.finalPath = this.transform(this.getDocumentUrl(this.CertificateObject.demopth));
            }
            this.CerPresent = true;
          } else {
            this.Certmsg = result['data'];
            this.CerPresent = false;
          }
          this.showDetail = true;
          this.timewoutfun(this.CertificateObject, this.selectedstep)
          console.log('Certificate Details:', this.CertificateObject);
        }
      },
      error => {
        this.spinner.hide();
        this.presentToast('error', '');
      },
    );
  }

  transform(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
  getDocumentUrl(url) {
    return ('https://docs.google.com/gview?url=' + encodeURI(url) + '&embedded=true');
  }

  toggleVisibility(e) {
    this.marked = e.target.checked;
    console.log(this.marked);
  }

  toggleVisibility1(value) {
    this.isapprove = value;
  }

  formatDate(date) {
    if (date) {
      const d = new Date(date);
      const formatted = this.datepipe.transform(d, 'yyyy-MM-dd');
      return formatted;
    }
  }

  formatTime(time) {
    const t = new Date(time);
    const formattedTime = this.datepipe.transform(t, 'h:mm:ss');
    return formattedTime;
  }

  formatSendDateTime(Dtime){
    const t = new Date(Dtime);
    // const formattedDateTime = this.datepipe.transform(t, 'yyyy-MM-dd h:mm');
    const year = t.getFullYear() +  '-' + (t.getMonth() + 1 ) + '-' + t.getDate();
    const time = ' ' + t.getHours() + ':' + t.getMinutes() + ':' + t.getSeconds();
    const formattedDateTime = year + time;
    return formattedDateTime;
  }

  timewoutfun(data, stepId) {
    setTimeout(() => {
      this.dataPopUp = data;
      this.selectedstep = stepId;
      this.showPopup = true;
      this.showDetail = true;
      this.modal_showPopup = true;
      this.cdf.detectChanges();
    }, 100);
  }
  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }
}
