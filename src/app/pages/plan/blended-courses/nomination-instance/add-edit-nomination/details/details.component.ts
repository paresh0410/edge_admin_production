import { Component, OnInit, ViewChild, ViewEncapsulation, ChangeDetectorRef, Input, OnChanges, SimpleChanges } from '@angular/core';
import { BsDatepickerDirective } from 'ngx-bootstrap/datepicker';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { DatePipe } from '@angular/common';
import { BlendedService } from '../../../blended.service';
import {
  NG_VALIDATORS,
  Validator,
  Validators,
  AbstractControl,
  ValidatorFn
} from '@angular/forms';
import { NgForm } from '@angular/forms';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { AddEditNominationComponent } from '../add-edit-nomination.component';
import * as _moment from 'moment';
import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';

const moment = (_moment as any).default ? (_moment as any).default : _moment;

export const MY_CUSTOM_FORMATS = {
  fullPickerInput: 'DD-MM-YYYY HH:mm:ss',
    parseInput: 'DD-MM-YYYY HH:mm:ss',
    datePickerInput: 'DD-MM-YYYY',
    timePickerInput: 'LT',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY'
};
@Component({
  selector: 'nomination-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [DatePipe,
    {provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE]},
    {provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS},]
})
export class DetailsComponent implements OnInit {
  @ViewChild(NgForm) nominationForm: NgForm;

  nominationData: any = {
    instName: '',
    pId: '',
    visible: '',
    sDate: '',
    eDate: '',
    nDesc: ''
  };
  direction='bottom'
  programs: any = [];

  visibilityOptions: any = [];
  currentUser: any = [];
  serviceData: any = [];
  wfId: any;
  minDate: Date;
  @Input() inpdata: any;
  @ViewChild("nominationForm") formval: any;

  constructor(
    private blendedService: BlendedService,
    private toasterService: ToastrService,
    private toastr: ToastrService,
    public router: Router,
    public routes: ActivatedRoute,
    private http1: HttpClient,
    public AddEditNominationComponent: AddEditNominationComponent,
    public cdf: ChangeDetectorRef,
  ) {
    this.currentUser = JSON.parse(localStorage.getItem('LoginResData'));
    this.serviceData = this.blendedService.program;
    this.getDropdownData();
    // console.log("current user" ,this.currentUser)
  }

  ngOnInit() {
    console.log("this.visible===",this.visibilityOptions)
    this.cdf.detectChanges();
    console.log(this.nominationData);
    this.getHelpContent();
  }
  submitRes: any;

  errorMsg: string;

  ngOnChanges(changes: SimpleChanges): void {
		if(this.inpdata === 'details') {
		  this.onSubmit(this.nominationData);
		}
	}

  presentToast(type, body) {
    if(type === 'success'){
      this.toastr.success(body, 'Success', {
        closeButton: false
       });
    } else if(type === 'error'){
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else{
      this.toastr.warning(body, 'Warning', {
        closeButton: false
        })
    }
  }

  getDropdownData() {
    const data = {
      // wfId: nId,
      tId: this.currentUser.data.data.tenantId,
    };
    console.log(data);
    // this.spinner.show();
    this.blendedService.getnominationDropdown(data).then(
      rescompData => {
        console.log('dropdown data', rescompData);
        // this.spinner.hide();
        this.programs = rescompData['data'][1];
        this.visibilityOptions = rescompData['data'][0];
        if (this.serviceData === undefined) {
          this.nominationData = {
            instName: '',
            pId: '',
            visible: '',
            sDate: '',
            eDate: '',
            nDesc: ''
          };
          this.minDate = new Date((new Date()).valueOf() - 1000*60*60*24);
          // this.minDate.setDate(this.minDate.getDate() - 1);
          console.log('this.minDate', this.minDate);
        } else {
          this.nominationData = {
            instName: this.serviceData.instName,
            pId: this.serviceData.pId,
            visible: this.serviceData.visible,
            sDate: new Date(this.serviceData.sDate),
            eDate: new Date(this.serviceData.eDate),
            nDesc: this.serviceData.nDesc,
          };
          const newDate = new Date();
          if(this.nominationData.sDate < newDate){
            this.minDate =  this.nominationData.sDate;
          }else {
            this.minDate = new Date((new Date()).valueOf() - 1000*60*60*24);
          }
        }
        console.log('Program data', this.programs);
        // this.loader = false;
      },
      resUserError => {
        // this.spinner.hide();
        this.errorMsg = resUserError;
      }
    );
  }
  nID:any;
  onSubmit(data) {
    if(!this.formval.form.valid) {
      this.toasterService.warning('Please fill required details',"Warning");
      Object.keys(this.formval.controls).forEach(key => {
        console.log('this.formval.controls[key]', this.formval.controls[key]);
        this.formval.controls[key].markAsDirty();
      });
      return;
    } else {
      console.log('form data >> ', data);
      console.log('form data >> ', this.nominationForm);
      if (this.serviceData) {
        this.nID = this.serviceData.nId;
      } else {
        this.nID = null;
      }
      const details = {
        nId: this.blendedService.wfId ? this.blendedService.wfId : null,
        pId: data.pId,
        instName: data.instName,
        nDesc: data.nDesc,
        sDate: this.formatSendDateTime(data.sDate),
        eDate:  this.formatSendDateTime(data.eDate),
        visible: data.visible,
        tId: this.currentUser.data.data.tenantId,
        ucrId: this.currentUser.data.data.id,
      };
      // if (!details.nId) {
      //   details.nId = '';
      // }
      // this.nominationForm.resetForm();
  
      console.log(details);
      this.blendedService.getAddEditInstance(details).then(
        rescompData => {
          console.log(rescompData);
          // this.spinner.hide();
          this.submitRes = rescompData;
          if (this.submitRes.type === false) {
  
            // const catUpdate: Toast = {
            //   type: 'error',
            //   title: 'Nomination Details',
            //   body: this.submitRes.data[0].msg,
            //   showCloseButton: true,
            //   timeout: 4000
            // };
            // this.toasterService.pop(catUpdate);
            this.presentToast('error', '');
            console.log();
  
          } else {
            // const catUpdate: Toast = {
            //   type: 'success',
            //   title: 'Nomination Details',
            //   body: this.submitRes.data[0].msg,
            //   showCloseButton: true,
            //   timeout: 4000,
            // };
            // this.toasterService.pop(catUpdate);
            this.presentToast('success', this.submitRes.data[0].msg);
         //   this.blendedService.wfId = this.submitRes.data[0].instanceId;
           // console.log('this.blendedService.wfId', this.blendedService.wfId);
          // this.nID = this.submitRes.data[0].instanceId;
            const DataTab= {
              tabTitle:'T&C',
            }
            try{
              this.blendedService.wfId = this.submitRes.data[0].instanceId ?
              this.submitRes.data[0].instanceId : this.serviceData.nId;
              // this.BlendedService.wfId = this.submitRes.data[0].instanceId
              this.AddEditNominationComponent.selectedTab(DataTab);
            }catch(e)
            {
              // console.log('course result is wrong',this.courseAddEditRes)
              //this.service.courseId =undefined;
            }
            this.cdf.detectChanges();
            // this.TabSwitcher();
           // this.dataSwitcher();
          }
        },
        resUserError => {
          // this.spinner.hide();
          if (resUserError.statusText === 'Unauthorized') {
            // this.router.navigate(['/login']);
          }
          this.errorMsg = resUserError;
        }
      );
    }
  }

  minDateto: Date;

  TabSwitcher(){
    this.AddEditNominationComponent.tabEnabler.codeOfConduct = true;
    this.AddEditNominationComponent.tabEnabler.content = false;
    this.AddEditNominationComponent.tabEnabler.enrolTab = false;
    this.AddEditNominationComponent.tabEnabler.nomineeTab = false;
    this.AddEditNominationComponent.tabEnabler.detailsTab = false;
    this.AddEditNominationComponent.tabEnabler.steps = false;
    console.log(this.AddEditNominationComponent);
  }

  formatSendDateTime(Dtime){
    const t = new Date(Dtime);
    // const formattedDateTime = this.datepipe.transform(t, 'yyyy-MM-dd h:mm');
    const year = t.getFullYear() +  '-' + (t.getMonth() + 1 )+ '-' + t.getDate();
    const time = ' ' + t.getHours() + ':' + t.getMinutes();
    const formattedDateTime = year + time;
    return formattedDateTime;
  }
  onSearchChange(data) {
    this.minDateto = data;
  }

  // displayInstances() {
  //   this.spinner.show();
  //   var data1 = {
  //     tId: 1
  //   }
  //   this.blendedService.getnomination(data1)
  //     .then(rescompData => {
  //       this.spinner.hide();
  //       console.log(rescompData);
  //       if (rescompData['type'] == true) {
  //         this.nominationInstances = rescompData['data'];
  //         console.log("Nomination Instances >>", this.nominationInstances);
  //       }
  //       else {
  //         this.nodata = 'true';
  //       }
  //     });
  // }

  // Help Code Start Here //

  helpContent: any;
  getHelpContent() {
    return new Promise(resolve => {
      this.http1
        .get(
          '../../../../../../../assets/help-content/addEditCourseContent.json'
        )
        .subscribe(
          data => {
            this.helpContent = data;
            console.log('Help Array', this.helpContent);
          },
          err => {
            resolve('err');
          }
        );
    });
    // return this.helpContent;
  }

  // Help Code Ends Here //
}
