import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA }  from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';

import { MyDatePickerModule } from 'mydatepicker';
import { FilterPipeModule  } from 'ngx-filter-pipe';
import { TagInputModule } from 'ngx-chips';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { NominationInstanceComponent } from './nomination-instance.component';
import { AddEditNominationInstanceModule } from './add-edit-nomination/add-edit-nomination.module';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgxContentLoadingModule } from 'ngx-content-loading';
import { ComponentModule } from '../../../../component/component.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MyDatePickerModule,
    FilterPipeModule,
    TagInputModule,
    AngularMultiSelectModule,
    AddEditNominationInstanceModule,
    TooltipModule.forRoot(),
    NgxSkeletonLoaderModule,
    NgxContentLoadingModule,
    ComponentModule,
  ],
  declarations: [
    NominationInstanceComponent,

  ],
  providers: [
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})

export class NominationInstanceModule {}
