import { Component, OnInit, ViewEncapsulation ,ElementRef, ViewChild} from '@angular/core';
import { Router, NavigationStart, Routes, ActivatedRoute } from '@angular/router';
import { BlendedService } from '../blended.service';
import { NgxSpinnerService } from 'ngx-spinner';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { ToastrService } from 'ngx-toastr';
import { CommonFunctionsService } from '../../../../service/common-functions.service';
import { webApi } from '../../../../service/webApi';
import { Card } from '../../../../models/card.model';
import { SuubHeader } from '../../../components/models/subheader.model';
import { noData } from '../../../../models/no-data.model';
import { PassService } from '../../../../service/passService';

@Component({
  selector: 'ngx-nomination-instance',
  templateUrl: './nomination-instance.component.html',
  styleUrls: ['./nomination-instance.component.scss'],
  encapsulation: ViewEncapsulation.None,

})
export class NominationInstanceComponent implements OnInit {
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  header: SuubHeader  = {
    title:'Nomination Instance',
    btnsSearch: true,
    searchBar: true,
    dropdownlabel: ' ',
    placeHolder:'Search by instance name',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: '',
    btnName3: '',
    btnAdd: 'Add Instance',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: false,
    btnAddshow: true,
    filter: false,
    showBreadcrumb: true,
    breadCrumbList:[
    ]
  };
  count:number=8
  currentUser: any = [];
  skeleton= false;
  searchinst: any= [];
  cardModify: Card = {
    flag: 'nominationInstance',
    titleProp : 'instName',
    // discrption: 'description',
    hoverlable:   true,
    showImage: true,
    option:   true,
    bottomDiv: true,
    eyeIcon:   true,
    showBottomList:   true,
    bottomTitle:   true,
    batchIcon: true,
    btnLabel: 'Edit',
    defaultImage:'assets/images/courseicon.jpg'
  };
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No Nominations at this time.",
    desc:"Admins will be able to add API's to the organisation.",
    titleShow:true,
    btnShow:false,
    descShow:false,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/setting-how-to-add-modules-to-an-online-course',
  }
  // tId =  this.currentUser.data.data.tenantId;
  nominationInstances: any = [
    // {
    //   categoryId: 1,
    //   instanceName: "Test 1",
    //   instanceCode: "test",
    //   instanceId: 12,
    //   programId: 1,
    //   startDate: new Date(),
    //   endDate: new Date(),
    //   role: "",
    //   pointsBefore: 50,
    //   pointsAfter: 80,
    //   managerCredits: 50,
    //   batchLocation:"",
    //   batchLocationId:1,
    //   batchVenueId: 1,
    //   batchVenue: "",
    //   batchLevelId: 1,
    //   batchOrder: 0,
    //   instancePicRef: "https://bhaveshedgetest.s3.ap-south-1.amazonaws.com/splash.png",
    //   tenantId: 1,
    //   visible: 1,
    //   bcId:1,

    // },
    // {
    //   categoryId: 2,
    //   instanceName: "Test 2",
    //   instanceCode: "demo",
    //   instanceId: 2,
    //   programId: 5,
    //   startDate: new Date(),
    //   endDate: new Date(),
    //   role: "",
    //   pointsBefore: 50,
    //   pointsAfter: 80,
    //   managerCredits: 50,
    //   batchLocation:"",
    //   batchLocationId:1,
    //   batchVenueId: 1,
    //   batchVenue: "",
    //   batchLevelId: 1,
    //   batchOrder: 0,
    //   instancePicRef: "https://bhaveshedgetest.s3.ap-south-1.amazonaws.com/splash.png",
    //   tenantId: 1,
    //   visible: 1,
    //   bcId:1,
    // },
    // {
    //   categoryId: 3,
    //   instanceName: "Test 2",
    //   instanceCode: "test3",
    //   instanceId: 10,
    //   programId: 3,
    //   startDate: new Date(),
    //   endDate: new Date(),
    //   role: "",
    //   pointsBefore: 50,
    //   pointsAfter: 80,
    //   managerCredits: 50,
    //   batchLocation:"",
    //    batchLocationId:1,
    //   batchVenueId: 1,
    //   batchVenue: "",
    //   batchLevelId: 1,
    //   batchOrder: 0,
    //   instancePicRef: "https://bhaveshedgetest.s3.ap-south-1.amazonaws.com/splash.png",
    //   tenantId: 1,
    //   visible: 1,
    //   bcId:1,
    // }
  ];
  searchText: any;
  title: string;
  enable: boolean;
  enableDisableModal: boolean;
  instData: any;
  NominationData: any;
  constructor(private spinner: NgxSpinnerService, public routes: ActivatedRoute,
    //  private toasterService: ToasterService, 
     private router: Router,
     private commonFunctionService: CommonFunctionsService,
     private blendedService: BlendedService, private toastr: ToastrService,
     private passService: PassService) {

  }

  // ngOnInit() {
    
  // }

  errorMsg: string;
  nodata: any = false;
  visibiltyRes: any;
  noNominationInstance: boolean = false;
  displayInstances() {
    // this.spinner.show();
    var data1 = {
      tId:  this.currentUser.data.data.tenantId,
    };
    const _nominationInstanceList: string = webApi.domain + webApi.url.getnomination;
    this.commonFunctionService.httpPostRequest(_nominationInstanceList,data1)
    //this.blendedService.getnomination(data1)
      .then(rescompData => {
        this.spinner.hide();
        console.log(rescompData);
        this.skeleton=true;
        if (rescompData['type'] === true) {
          if (rescompData['data'].length === 0) {
            this.skeleton=false;
            this.noNominationInstance = true;
            this.nodata=true;
          } else {
            this.skeleton=true;
            this.noNominationInstance = false;
            this.nominationInstances = rescompData['data'];
            this.addItems(0,this.sum,'push',this.nominationInstances);
          }

          console.log('Nomination Instances >>', this.nominationInstances);
        } else {
          this.noNominationInstance = true;
          this.skeleton=true;
        }
      });
  }

  presentToast(type, body) {
    if(type === 'success'){
      this.toastr.success(body, 'Success', {
        closeButton: false
       });
    } else if(type === 'error'){
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
      this.skeleton=true;
    } else{
      this.toastr.warning(body, 'Warning', {
        closeButton: false
        })
    }
  }

  disableInstance(i, item) {
    var visibilityData = {
      nid: item.nId,
      visible: item.visible == 1 ? 0 : 1,
    }
    const _urlDisableInstance: string = webApi.domain + webApi.url.disableNominationInstances;
    this.commonFunctionService.httpPostRequest(_urlDisableInstance,visibilityData)
    // let url = webApi.domain + webApi.url.disablequiz;
    // this.blendedService.disableInstance(visibilityData)
      .then(rescompData => {
        this.spinner.hide();
        this.visibiltyRes = rescompData;
        if (this.visibiltyRes.type == false) {
          // var catUpdate: Toast = {
          //   type: 'error',
          //   title: "Nomination Visibility",
          //   body: "Unable to update visibility of Instance",
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
        } else {
          // var catUpdate: Toast = {
          //   type: 'success',
          //   title: "Nomination Visibility",
          //   body: this.visibiltyRes.data,
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('success', this.visibiltyRes.data);
          this.disablevaluechnage(i, item)
        }
      },
        resUserError => {
          this.spinner.hide();
          if (resUserError.statusText == "Unauthorized") {
            this.router.navigate(['/login']);
          }
          this.errorMsg = resUserError;
        });
  }

  clickTodisable(item) {
    var visibilityData = {
      nid: item.nId,
      visible: item.visible,
    }

    // if(item.visible == 0) {
    //   item.visible = 1;
    // } else {
    //   item.visible = 0;
    // }
    const _urlDisableInstance: string = webApi.domain + webApi.url.disableNominationInstances;
    this.commonFunctionService.httpPostRequest(_urlDisableInstance,visibilityData)
   
      .then(rescompData => {
        // this.spinner.hide();
        this.visibiltyRes = rescompData;
        if (this.visibiltyRes.type == false) {
         
          this.presentToast('error', '');
        } else {
         if(item.visible==1){
           this.toastr.success('Instance Enabled Succesfully','Success')
         }
         if(item.visible==0){
          this.toastr.success('Instance Disabled Succesfully','Success')
        }
          // this.presentToast('success', this.visibiltyRes.data);
        }
      },
        resUserError => {
          this.spinner.hide();
          if (resUserError.statusText == "Unauthorized") {
            this.router.navigate(['/login']);
          }
          this.errorMsg = resUserError;
        });
    this.closeEnableDisableModal();
  }

  disablevaluechnage(i, item) {
    this.nominationInstances[i].visible = this.nominationInstances[i].visible == 1 ? 0 : 1;
  }

  clear(){
    if(this.searchText.length>=3){
    this.searchinst={};
    this.searchText=''
    // this.displayInstances();
    this.displayArray =[];
    this.newArray = [];
    this.sum = 50;
    this.addItems(0, this.sum, 'push', this.nominationInstances);
    this.nominationInstances=false
    }
    else{
    this.searchinst={};
    }
  };

  public addeditcontent(data, id) {
    if ( data ) {
      this.blendedService.wfId = data.nId;

    }
    if(id === 0){
      this.blendedService.program = {
        nId: null,
        catName: null,
        catId: null,
        pName: null,
        pId: '',
        instName: '',
        nDesc: '',
        sDate: '',
        eDate: '',
        eCsrId: null,
        visible: '1',
        instituteName: '',
        instituteId: '',
        partnerName: '',
        distributionPartnerId: '',
        budget: '',
        seatCost: '',
    };
    }else {
      this.blendedService.program = data;
    }
    console.log("data", data);
    // this.blendedService.program = data;
    this.router.navigate(['add-edit-nomination'], { relativeTo: this.routes });
  }

  gotoCardaddedit(data) {
    if ( data ) {
      this.blendedService.wfId = data.nId;
    }
    this.blendedService.program = data;
    this.router.navigate(['add-edit-nomination'], { relativeTo: this.routes });
  }

  back() {
    this.router.navigate(['../'], { relativeTo: this.routes });
  }

  goToBatch(item){
    // this.blendedService.showWorkFlowBatche = true;
    this.blendedService.workflowId = item.nId;
    console.log(item.nId,"item.nId")
    this.router.navigate(['../blended-courses'], { relativeTo: this.routes });
  }

  batchClick(item) {
    this.passService.isWorkflow = true;
    this.blendedService.workflowId = item.nId;
    console.log(item.nId,"item.nId")
    this.router.navigate(['../blended-home/blended-courses'], { relativeTo: this.routes });
  }
  onsearch(event){
    console.log(event,"event")
    const val=event.target.value
    if(val.length>=3||val.length==0){
    this.searchinst.instName =  event.target.value
    }
  }
  ngOnDestroy(){
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0){
      e1[0].classList.add('with-scroll');
    }
  }
  ngAfterContentChecked() {
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0)
    {
      e1[0].classList.remove('with-scroll');
    }

   }
   conentHeight = '0px';
   offset: number = 100;
   ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('LoginResData'));
    this.displayInstances();
     this.conentHeight = '0px';
     const e = document.getElementsByTagName('nb-layout-column');
     console.log(e[0].clientHeight);
     // this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
     if (e[0].clientHeight > 1300) {
       this.conentHeight = '0px';
       this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
     } else {
       this.conentHeight = String(e[0].clientHeight) + 'px';
     }
   }
 
   sum = 40;
   addItems(startIndex, endIndex, _method, asset) {
     for (let i = startIndex; i < endIndex; ++i) {
       if (asset[i]) {
         this.displayArray[_method](asset[i]);
       } else {
         break;
       }
 
       // console.log("NIKHIL");
     }
   }
   // sum = 10;
   onScroll(event) {
 
     let element = this.myScrollContainer.nativeElement;
     // element.style.height = '500px';
     // element.style.height = '500px';
     // Math.ceil(element.scrollHeight - element.scrollTop) === element.clientHeight
 
     if (element.scrollHeight - element.scrollTop - element.clientHeight < 5) {
       if (this.newArray.length > 0) {
         if (this.newArray.length >= this.sum) {
           const start = this.sum;
           this.sum += 50;
           this.addItems(start, this.sum, 'push', this.newArray);
         } else if ((this.newArray.length - this.sum) > 0) {
           const start = this.sum;
           this.sum += this.newArray.length - this.sum;
           this.addItems(start, this.sum, 'push', this.newArray);
         }
       } else {
         if (this.nominationInstances.length >= this.sum) {
           const start = this.sum;
           this.sum += 50;
           this.addItems(start, this.sum, 'push', this.nominationInstances);
         } else if ((this.nominationInstances.length - this.sum) > 0) {
           const start = this.sum;
           this.sum += this.nominationInstances.length - this.sum;
           this.addItems(start, this.sum, 'push', this.nominationInstances);
         }
       }
 
 
       console.log('Reached End');
     }
   }
   newArray =[];
   displayArray =[];
    // this function is use to dynamicsearch on table
  searchData(event) {
    var temData = this.nominationInstances;
    var val = event.target.value.toLowerCase();
    this.searchText=val;
    var keys = [];
  this.noNominationInstance=false;
    if(event.target.value == '')
    {
      this.displayArray =[];
      this.newArray = [];
      this.sum = 50;
      this.addItems(0, this.sum, 'push', this.nominationInstances);
    }else{
         // filter our data
    if(val.length>=3||val.length==0){

    // if (temData.length > 0) {
    //   keys = Object.keys(temData[0]);
    // }
      const temp = this.nominationInstances.filter(function (d) {
        return String(d.instName).toLowerCase().indexOf(val) !== -1 ||
          // d.sdesc.toLowerCase().indexOf(val) !== -1 ||
          // d.relayDate.toLowerCase().indexOf(val) !== -1 ||
          // d.manager.toLowerCase().indexOf(val) !== -1 ||
          !val
      })
    // var temp = temData.filter((d) => {
    //   for (const key of keys) {
    //     if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
    //       return true;
    //     }
    //   }
    // });
if(temp.length==0){
  this.noNominationInstance=true;
  this.noDataVal={
    margin:'mt-5',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"Sorry we couldn't find any matches please try again",
    desc:".",
    titleShow:true,
    btnShow:false,
    descShow:false,
    btnText:'Learn More',
    btnLink:''
}
  
}
    // update the rows
    this.displayArray =[];
    this.newArray = temp;
    this.sum = 50;
    this.addItems(0, this.sum, 'push', this.newArray);
    }
  }
}
btnName:string='Yes'
clickTodisableInst(item, i) {

  if(this.displayArray[i].visible == 1) {
    this.displayArray[i].visible = 0
  } else {
    this.displayArray[i].visible = 1
  }

  this.clickTodisable(item);

    // if (item.visible == 1 ) {
    // this.title="Disable Instance"
    //   this.enable = true;
    //   this.NominationData = item;
    //   this.enableDisableModal = true;
    // } else {
    // this.title="Enable Instance"
    //   this.enable = false;
    //   this.NominationData = item;
    //   this.enableDisableModal = true;

    // }
}
enableDisableInstAction(actionType) {
  if (actionType == true) {

    if (this.NominationData.visible == 1) {
      var courseData = this.NominationData;
      this.clickTodisable(courseData);
    } else {
      var courseData = this.NominationData;
      this.clickTodisable(courseData);
    }
  } else {
    this.closeEnableDisableModal();
  }
}
closeEnableDisableModal() {
  this.enableDisableModal = false;
}
}
