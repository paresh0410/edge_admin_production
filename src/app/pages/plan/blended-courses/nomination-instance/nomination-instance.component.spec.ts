import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NominationInstanceComponent } from './nomination-instance.component';

describe('NominationInstanceComponent', () => {
  let component: NominationInstanceComponent;
  let fixture: ComponentFixture<NominationInstanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NominationInstanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NominationInstanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
