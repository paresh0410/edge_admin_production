import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlendedCoursesComponent } from './blended-courses.component';

describe('BlendedCoursesComponent', () => {
  let component: BlendedCoursesComponent;
  let fixture: ComponentFixture<BlendedCoursesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlendedCoursesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlendedCoursesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
