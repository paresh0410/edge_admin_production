import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { IntitutesService } from './intitutes.service';
import { PassService } from '../../../service/passService';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { webApi } from '../../../service/webApi';
import { SuubHeader } from '../../components/models/subheader.model';
import { noData } from '../../../models/no-data.model';


@Component({
  selector: 'ngx-institutes',
  templateUrl: './institutes.component.html',
  styleUrls: ['./institutes.component.scss']
})
export class InstitutesComponent implements OnInit {
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No Institutes at this time.",
    desc:"Institutes will appear after they are added by the admin. Institutes can be mapped to Programs.",
    titleShow:true,
    btnShow:true,
    descShow:true,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/add-institute',
  }
  header: SuubHeader  = {
    title:'Institute',
    btnsSearch: true,
    placeHolder:"Search by Intitute name ",
    searchBar: true,
    dropdownlabel: ' ',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: '',
    btnName3: '',
    btnAdd: 'Add',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: true,
    filter: false,
    showBreadcrumb: true,
    breadCrumbList:[]  
  };
  btnName: string = 'Save';
  list: any = [];
  institutes: any = [];
  helpContent: any;
  sectionshow: boolean = false;
  title: any;
  loader: any;
  pager: any = [];
  instituteslist: any = [];
  pageSize = 10;
  nodata: boolean = false;
  drowDownValue: any = [];
  instituteData: any = [];
  isPending: boolean = true;
  drowDownVisibleData: any = [];
  searchTags: any = [];
  labels: any = [
    // { labelname: 'Sr No', bindingProperty: 'sr_no', componentType: 'text' },
		{ labelname: 'NAME', bindingProperty: 'instituteName', componentType: 'text' },
    { labelname: 'DISCRIPTION', bindingProperty: 'description', componentType: 'text' },
		{ labelname: 'ACTION', bindingProperty: 'btntext', componentType: 'button' },
    { labelname: 'EDIT', bindingProperty: 'tenantId', componentType: 'icon' },
  ]
  constructor(public IntiService: IntitutesService, public http: HttpClient,
    public toastr: ToastrService, private pagservice: PassService, private spinner: NgxSpinnerService,private commonFunctionService: CommonFunctionsService ) {

  }

  ngOnInit() {
    this.header.breadCrumbList=[{
      'name': 'Settings',
      'navigationPath': '/pages/plan',
      }]
    this.getinstitutelist();
    this.getHelpContent();
  }
  getinstitutelist() {
    this.loader = true;
    this.spinner.show();
    const get_institutes: string = webApi.domain + webApi.url.fetch_institute_list;
    this.commonFunctionService.httpPostRequest(get_institutes,{})
    //this.IntiService.getallinstitutes()
    .then(res => {
      this.loader=false;
      this.spinner.hide();
      console.log(res);
      if (res['type'] == true) {
        this.list = res['data'][0];
        this.instituteslist = this.list;
        // this.setpage(1);
        for (let index = 0; index < this.instituteslist.length; index++) {
          if(this.instituteslist[index].visible == 1) {
            this.instituteslist[index].btntext = 'fa fa-eye';
          } else {
            this.instituteslist[index].btntext = 'fa fa-eye-slash';
          }
        }
        if(this.instituteslist.length == 0){
          this.nodata =  true
        }
        else{
          this.nodata = false
        }
      }
      this.loader = false;
    }, err => {
      this.loader = false;
      this.spinner.hide();
      this.presentToast('error', '');
      console.log(err);
      this.nodata =  true
    })
  }
  setpage(page) {
    // this.nodata = false;
    this.pager = this.pagservice.getPager(this.instituteslist.length, page, this.pageSize);
    console.log(this.pager);
    this.displaydata();
  }
  displaydata() {
    this.institutes = this.instituteslist.slice(this.pager.startIndex, this.pager.endIndex + 1);
    for (let index = 0; index < this.institutes.length; index++) {
      this.institutes[index] = { id: index + 1, ...this.institutes[index] };
      // this.displayTableData[index]['Download'] = '';
      this.institutes[index]['sr_no'] = index + 1;
      this.institutes[index]['Edit'] = '';

    }
    this.spinner.hide();
    console.log(this.institutes);
  }
  Addinstitue(data, status) {
    this.instituteData = [];
    this.getDropDownData();
    if (status == 0) {
      this.instituteData = {
        id: 0,
        instituteName: '',
        visible: 1,
        description: '',
      }
      this.title = "Add Institute";
    } else {
      this.instituteData = {
        id: data.id,
        instituteName: data.instituteName,
        visible: data.visible,
        description: data.description,
      }
      this.title = "Edit Institute";
    }
    this.sectionshow = true;
  }
  getDropDownData() {
    const get_dropDown = webApi.domain + webApi.url.getDropDown;
    this.commonFunctionService.httpPostRequest(get_dropDown,{})
    //this.IntiService.getDropdown()
    .then(
      (data) => {
        this.drowDownValue = data
        this.drowDownVisibleData = this.drowDownValue.data.visibleDropData
        this.isPending = false
      },
      (error: any) => console.error(error))
  }
  back() {
    window.history.back();
  }
  closesectionModel() {
    this.instituteData = [];
    this.sectionshow = false;
  }
  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }
  clear() {
    if(this.searchTags.length>=3){
    this.searchTags = '';
    // this.onsearch(event, this.searchTags);
    this.instituteslist = this.list
    this.setpage(1)
    this.nodata=false;
    }
    else{
      this.searchTags=''
    }
  }
  onsearch(evt: any, text) {
    console.log(evt);
   // const val = text;
    const val = evt.target.value.toLowerCase()
    this.searchTags=evt.target.value;
    if(val.length>=3 ||val.length==0){
    const temp = this.list.filter(function (d) {
      return String(d.instituteName).toLowerCase().indexOf(val) !== -1 ||
        !val
      // d.description.toLowerCase().indexOf(val) !== -1 ||
    })
  
    console.log(temp);
    if(temp.length==0){
      this.nodata=true
    }
    this.instituteslist = temp;
    this.setpage(1);
  }
  }
  saveinstitute(form) {
    if(form && form.valid){
      this.spinner.show();
    const param = {
      id: this.instituteData.id,
      instituteName: this.instituteData.instituteName,
      description: this.instituteData.description,
      visible: this.instituteData.visible,
    }
    const add_edit_institutes = webApi.domain + webApi.url.add_edit_instituts;
    this.commonFunctionService.httpPostRequest(add_edit_institutes,param)
    //this.IntiService.addedit_institutes(param)
    .then(res => {
      console.log(res);
      if (res['type'] == true) {
        var response = res['data'][0];
        if (response[0].status == 0) {
          this.presentToast('warning', response[0].msg);
        } else {
          this.presentToast('success', response[0].msg);
          this.getinstitutelist();
          this.closesectionModel();
        }
      } else {
        this.presentToast('error', '')
      }
      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      this.presentToast('error', '')
    })
    }else {
      Object.keys(form.controls).forEach(key => {
        form.controls[key].markAsDirty();
      });
    }
    
  }
  visibilityTableRow(row) {
    let value;
    let status;

    if (row.visible == 1) {
      row.btntext = 'fa fa-eye-slash';
      value  = 'fa fa-eye-slash';
      row.visible = 0
      status = 0;
    } else {
      status = 1;
      value  = 'fa fa-eye';
      row.visible = 1;
      row.btntext = 'fa fa-eye';
    }
    const param = {
      id: row.id,
      visible: status
    }
    // this.spinner.show();
    const update_institutes = webApi.domain + webApi.url.enable_disable_institute;
    this.commonFunctionService.httpPostRequest(update_institutes,param)
    //this.IntiService.enableinstitutes(param)
    .then(res => {
      console.log(res);
      if (res['type'] == true) {
        this.presentToast('success', res['data']);
        row.visible = status;
      }
      this.spinner.hide();
    }, err => {
      console.log(err);
      this.spinner.hide();
      this.presentToast('error', '');
    });

    console.log('row', row);
  }
  
  getHelpContent() {
    return new Promise(resolve => {

      this.http.get('../../../../../../assets/help-content/addEditCourseContent.json').subscribe(
        data => {
          this.helpContent = data;
          console.log('Help Array', this.helpContent);
        },
        err => {
          resolve('err');
        },
      );
    });
    // return this.helpContent;
  }
}
