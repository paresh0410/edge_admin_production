import { Injectable } from '@angular/core';
import { webApi } from '../../../service/webApi';
import { HttpClient } from "@angular/common/http";
@Injectable({
  providedIn: 'root'
})
export class IntitutesService {

  private get_institutes: string = webApi.domain + webApi.url.fetch_institute_list;
  private add_edit_institutes = webApi.domain + webApi.url.add_edit_instituts;
  private update_institutes = webApi.domain + webApi.url.enable_disable_institute;
  private get_dropDown = webApi.domain + webApi.url.getDropDown;
  constructor(private _http: HttpClient) { }


  getallinstitutes() {

    return new Promise(resolve => {
      this._http.post(this.get_institutes, {})
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  addedit_institutes(data) {

    return new Promise(resolve => {
      this._http.post(this.add_edit_institutes, data)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  enableinstitutes(data) {

    return new Promise(resolve => {
      this._http.post(this.update_institutes, data)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  getDropdown() {
    return new Promise(resolve => {
      this._http.post(this.get_dropDown, {})
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
}
