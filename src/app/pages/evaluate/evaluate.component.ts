import { Component } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
// import { Angular2Csv } from 'angular2-csv/Angular2-csv';
// import { PaginatePipe, PaginationControlsDirective, PaginationService } from 'ng2-pagination';
import { Router, NavigationStart, Routes, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EvaluateService } from './evaluate.service';
import { AppService } from '../../app.service';
// import { AddEditUserService } from '../addEditUser/addEditUser.service';
// import { DeviceMapMasterService } from '../deviceMapMaster/deviceMapMaster.service';
// import { AddEditDeviceMapService } from '../addEditDeviceMap/addEditDeviceMap.service';
// import { MachineMapMasterService } from '../machineMapMaster/machineMapMaster.service';
// import { AddEditMachineMapService } from '../addEditMachineMap/addEditMachineMap.service';
// import { RecipeMapMasterService } from '../recipeMapMaster/recipeMapMaster.service';
// import { AddEditRecipeMapService } from '../addEditRecipeMap/addEditRecipeMap.service';
// import { UserSettingMasterService } from '../userSettingMaster/userSettingMaster.service';
// import { AddEditUserSettingService } from '../addEditUserSetting/addEditUserSetting.service';
// import {SlimLoadingBarService} from 'ng2-slim-loading-bar';
// import {BusyModule} from 'angular2-busy';
// import { LoadingIndicator } from 'ng2-loading-indicator';
// import { BaImageLoaderService, BaThemePreloader, BaThemeSpinner } from '../../../../theme/services';
// import 'easy-pie-chart/dist/jquery.easypiechart.js';
import {BaseChartDirective} from 'ng2-charts/ng2-charts';
@Component({
  selector: 'evaluate',
  styleUrls: ['./evaluate.scss'],
  templateUrl: './evaluate.html'
})
 export class Evaluate {
    
    query: string = '';
    public getData;

    
    // userTableData:any = [];
    // compleation:any=[];
    // grades:any=[];
    // users:any=[];


    // source: LocalDataSource = new LocalDataSource();
    // isDesc :any ;
    // column :any ;
    // search :any = {};
    // search1 :any = {};
    // search2 :any = {};
  
    
    // errorMsg: string;
    // loader :any;
    // charts:any;
    // doughnutChartLabels :any;
    // doughnutChartData : any;
    // doughnutChartType:any;
    evaluate:any;
    showdata:any=[];
      reporting:boolean=false;
      analytics:boolean=false;
      // reommendation:boolean=false;
      // trainingPreferance:boolean=false;
    constructor(protected service: EvaluateService, private router:Router,public routes:ActivatedRoute,private AppService:AppService) {
      //this.evaluate=true;
      this.showdata =this.AppService.getfeatures();
      if(this.showdata)
      {
        for(let i=0;i<this.showdata.length;i++)
        {
          if(this.showdata[i] == 'MR')
          {
            this.reporting =true;
          }
           if(this.showdata[i] == 'rules')
          {
            this.analytics =true;
          }
          //  if(this.showdata[i] == 'rec')
          // {
          //   this.reommendation =true;
          // }
          //  if(this.showdata[i] == 'training')
          // {
          //   this.trainingPreferance =true;
          // }
        }
      }
    //   this.charts =[
    //   {
    //     color: 'rgba(255,255,255,0.8)',
    //     description: 'dashboard.new_visits',
    //     stats: '57,820',
    //     icon: 'person',
    //     percent:20
    //   }, {
    //     color: '#545',
    //     description: 'dashboard.purchases',
    //     stats: '89,745',
    //     icon: 'money',
    //      percent:40
    //   }, {
    //     color: '#545',
    //     description: 'dashboard.active_users',
    //     stats: '178,391',
    //     icon: 'face',
    //      percent:60
    //   }, {
    //     color: '#545',
    //     description: 'dashboard.returned',
    //     stats: '32,592',
    //     icon: 'refresh',
    //      percent:90
    //   }
    // ];
     
     // this.loader = true;

     //  this.service.getUserTableData()
     //  .subscribe(resUserData => { 
     //    this.loader =false;
     //    this.userTableData = resUserData.data;
     //    console.log(this.userTableData , 'this.userTableData')
     //  },
     //  resUserError => {
     //    this.loader =false;
     //    this.errorMsg = resUserError
     //  });

      // var user={
      //   userid:1
      // }
      // this.service.getcompletion(user)
      // .subscribe(rescompData => { 
      //   this.loader =false;
      //   this.compleation = rescompData.data[0];
      //   console.log(this.compleation , 'this.compleation')
      // },
      // resUserError => {
      //   this.loader =false;
      //   this.errorMsg = resUserError
      // });

      // this.service.getgrades()
      // .subscribe(resgradeData => { 
      //   this.loader =false;
      //   this.grades = resgradeData.data[0];
      //   for(let i = 0; i < this.grades.length; i++)
      //      {
      //        this.grades[i].alldata=[];
      //       var setData=[];
          
      //        setData.push(this.grades[i].g25)
      //        setData.push(this.grades[i].g50)
      //        setData.push(this.grades[i].g100)
           
      //        this.grades[i].alldata=setData;
      //    }
      //   console.log(this.grades , 'this.grades')
      // },
      // resUserError => {
      //   this.loader =false;
      //   this.errorMsg = resUserError
      // });

      //  this.service.getusers()
      // .subscribe(resUserData => { 
      //   this.loader =false;
      //   this.users = resUserData.data[0];
      //   console.log(this.users , 'this.users')
      // },
      // resUserError => {
      //   this.loader =false;
      //   this.errorMsg = resUserError
      // });

      // this._loadPieCharts();
      // this._updatePieCharts();
    
      //  this.doughnutChartLabels= ['<25', '<50', '<100'];
      // this.doughnutChartData=  this.newData;
      // this.doughnutChartType = 'doughnut';


    }

    // OnInit(){
    //   this.evaluate=true;
    // }
    gotoreporting(){
      // this.router.navigate['../../reporting/analyticsData'];
      //this.evaluate=false;
      this.router.navigate(['reporting'],{relativeTo:this.routes});
    }
  }

  //   private _loadPieCharts() {

  //   jQuery('.chart').each(function () {
  //     let chart = jQuery(this);
  //     chart.easyPieChart({
  //       easing: 'easeOutBounce',
  //       onStep: function (from, to, percent) {
  //         jQuery(this.el).find('.percent').text(Math.round(percent));
  //       },
  //       barColor: jQuery(this).attr('data-rel'),
  //       trackColor: 'rgba(0,0,0,0)',
  //       size: 84,
  //       scaleLength: 0,
  //       animation: 2000,
  //       lineWidth: 9,
  //       lineCap: 'round',
  //     });
  //   });
  // }


  // private _updatePieCharts() {
  //   let getRandomArbitrary = (min, max) => { return Math.random() * (max - min) + min; };

  //   jQuery('.pie-charts .chart').each(function(index, chart) {
  //     jQuery(chart).data('easyPieChart').update(getRandomArbitrary(55, 90));
  //   });
  // }
  //    newData:any=[];
  // setData:any=[];
       
  //    comp(i)
  //   {
  //       var styles={
  //           'width':i +'%',
  //           'height':'20px',
  //           'background-color':'#51c9e8'
  //       }
  //       return styles
  //   }

  //  incomp(i)
  //   {
  //       var styles={
  //           'width':i +'%',
  //           'height':'20px',
  //           'background-color':'#efc5c5'
  //       }
  //       return styles
  //   }

  // g100(i)
  //   {
  //       var styles={
  //           'width':i +'%',
  //           'height':'14px',
  //           'background-color':'#51c9e8',
  //           'margin': '2px 0px'
  //       }
  //       return styles
  //   }

  //   g25(i)
  //   {
  //       var styles={
  //           'width':i +'%',
  //           'height':'14px',
  //           'background-color':'#e87051',
  //           'margin': '2px 0px'
  //       }
  //       return styles
  //   }
  //  g50(i)
  //   {
  //       var styles={
  //           'width':i +'%',
  //           'height':'14px',
  //           'background-color':'#e8bc51',
  //           'margin-bottom': '2px'
  //       }
  //       return styles
  //   }

 
  // // events
  // public chartClicked(e:any):void {
  //   console.log(e);
  // }
 
  // public chartHovered(e:any):void {
  //   console.log(e);
  // }



 
        //randon color genrator
              // public getRandomColor() {
              //             var letters = '0123456789ABCDEF'.split('');
              //             var color = '#';
              //             for (var i = 0; i < 6; i++){
              //                 color += letters[Math.floor(Math.random() * 16)];
              //             }
              //             return color;
              //         }
    // newData:any = {};
    // xlsData:any = [];

    // Download(){

    //     for(let i = 0; i < this.userTableData.length; i++){
    //     this.newData = {
    //             'User Name' : this.userTableData[i].userName,
    //             'Phone No ' : this.userTableData[i].userPhNo,
    //             'E-Mail' : this.userTableData[i].userEmail,
    //             'Ownership Type' : this.userTableData[i].ownerShipName,
    //             'Is Verified' : this.userTableData[i].verifiedName,
    //             'Country Code' : this.userTableData[i].countryCode,
    //             'Location' : this.userTableData[i].location,
    //             'last Updated Date' : this.userTableData[i].lastUpdateDate,
    //             'Status' : this.userTableData[i].isActiveName
    //            }
    //     this.xlsData.push(this.newData);
    //   }
    //     // new Angular2Csv(this.xlsData, 'User Data', { headers: Object.keys(this.xlsData[0]) });
    // }

    // Clear(){
    //     this.search = {};
    // }
    // Clear1(){
    //     this.search1 = {};
    // }
    // Clear2(){
    //     this.search2 = {};
    // }

    // sort(property){
    //     this.isDesc = !this.isDesc; //change the direction    
    //     this.column = property;
    //     let direction = this.isDesc ? 1 : -1;

    //     this.userTableData.sort(function(a, b){
    //         if(a[property] < b[property]){
    //             return -1 * direction;
    //         }
    //         else if( a[property] > b[property]){
    //             return 1 * direction;
    //         }
    //         else{
    //             return 0;
    //         }
    //     });
    // };


// key: string; //set default
//      reverse: boolean = false;
//      sort(key){
//        this.key = key;
//        this.reverse = !this.reverse;
//      }

//     onDeleteConfirm(event) {
//         if (window.confirm('Are you sure you want to delete?')) {
//           event.confirm.resolve();
//         } else {
//           event.confirm.reject();
//         }
//     }
     
    // formData(data , id){
    //     var data1 ={
    //         data :data,
    //         id: id
    //     }
    //     this.passService.data =data1;
    //     this.router.navigate(['/pages/master/addEditUser']);
    // }

    // deviceMap(data){
    //   this.service1.data =data.userId;
    //   this.router.navigate(['/pages/master/deviceMapMaster']);
    // }

    // machineMap(data){
    //   this.service2.data =data.userId;
    //   this.router.navigate(['/pages/master/machineMapMaster']);
    // }

    // recipeMap(data){
    //   this.service3.data =data.userId;
    //   this.router.navigate(['/pages/master/recipeMapMaster']);
    // }

    // userSetting(data){
    //   this.service4.data =data.userId;
    //   this.router.navigate(['/pages/master/userSettingMaster']);
    // }
// }
