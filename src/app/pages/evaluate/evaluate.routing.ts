 import { Routes, RouterModule }  from '@angular/router';

import { Evaluate } from './evaluate.component';
import { ModuleWithProviders } from '@angular/core';


// noinspection TypeScriptValidateTypes
export const routes: Routes = [
  {path: '',component: Evaluate}
];

// export const routing: ModuleWithProviders = RouterModule.forChild(routes);
export const routing = RouterModule.forChild(routes);