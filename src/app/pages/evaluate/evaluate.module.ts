import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
// import { AppTranslationModule } from '../../app.translation.module';
// import { NgaModule } from '../../theme/nga.module';
import { Evaluate } from './evaluate.component';
import { EvaluateService } from './evaluate.service';
import { routing } from './evaluate.routing';
// import {Ng2PaginationModule} from 'ng2-pagination';
// import { Ng2FilterPipeModule } from 'ng2-filter-pipe';
// import { Ng2OrderModule } from 'ng2-order-pipe';
// import { PieChart } from './pieChart';
// import { PieChartService } from './pieChart/pieChart.service';
// import { MyDatePickerModule } from 'mydatepicker';
// import { TruncateModule } from 'ng2-truncate';

import { ChartsModule } from 'ng2-charts';
import { TranslateModule } from '@ngx-translate/core';
import { AngularSlickgridModule } from 'angular-slickgrid';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    // AppTranslationModule,
    // NgaModule,
    routing,
    TranslateModule.forRoot(),
    // Ng2PaginationModule,
    // Ng2FilterPipeModule,
    // Ng2OrderModule,
    // MyDatePickerModule,
    // TruncateModule,
    ChartsModule,
    AngularSlickgridModule.forRoot()
  ],
  declarations: [
    Evaluate,
   
   
   
     // PieChart
  ],
  providers: [
    EvaluateService
    // PieChartService
  ]
})
export class EvaluateModule {}
