import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { AppService } from '../../../app.service';

@Component({
  selector: 'ngx-reporting',
  templateUrl: './reporting.component.html',
  styleUrls: ['./reporting.component.scss']
})
export class ReportingComponent {
   showdata:any=[];
      learning:boolean=false;
      marketing:boolean=false;
      huddle:boolean=false;
      // trainingPreferance:boolean=false;

  constructor(private router:Router,private routes:ActivatedRoute,private AppService:AppService) { 
      this.showdata =this.AppService.getfeatures();
      if(this.showdata)
      {
        for(let i=0;i<this.showdata.length;i++)
        {
          if(this.showdata[i] == 'ML')
          {
            this.learning =true;
          }
           if(this.showdata[i] == 'rules')
          {
            this.marketing =true;
          }
           if(this.showdata[i] == 'rec')
          {
            this.huddle =true;
          }
          //  if(this.showdata[i] == 'training')
          // {
          //   this.trainingPreferance =true;
          // }
        }
      }
   
  }


  gotolearning(){
      
  		this.router.navigate(['learning'],{relativeTo:this.routes});
  }

  gotomarketing(){
  		this.router.navigate(['marketing'],{relativeTo:this.routes});
  }

  gotohuddle(){
  		this.router.navigate(['huddle'],{relativeTo:this.routes});
  }

  back(){
      
      this.router.navigate(['pages/evaluate']);
  }

}
