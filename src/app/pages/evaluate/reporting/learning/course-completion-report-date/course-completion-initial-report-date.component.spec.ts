import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseCompletionReportDateComponent } from './course-completion-initial-report-date.component';

describe('CourseCompletionInitialReportDateComponent', () => {
  let component: CourseCompletionInitialReportDateComponent;
  let fixture: ComponentFixture<CourseCompletionReportDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseCompletionInitialReportDateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseCompletionInitialReportDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
