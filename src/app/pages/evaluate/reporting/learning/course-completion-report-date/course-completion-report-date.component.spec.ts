import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseCompletionReportDateComponent } from './course-completion-report-date.component';

describe('CourseCompletionReportDateComponent', () => {
  let component: CourseCompletionReportDateComponent;
  let fixture: ComponentFixture<CourseCompletionReportDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseCompletionReportDateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseCompletionReportDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
