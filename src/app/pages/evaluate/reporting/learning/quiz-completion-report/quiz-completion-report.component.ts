import {Component, OnInit, OnDestroy, ChangeDetectorRef,NgZone, ViewChild} from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators/takeWhile' ;
import { AppService } from '../../../../../app.service';
//import { EmployeeInductionReportService } from './employee-induction-report.service';
import { Router, NavigationStart, Routes, ActivatedRoute } from '@angular/router';
import {QuizCompletionReportService} from './quiz-completion-report.service'
import { Column, GridOption, AngularGridInstance, FieldType, Filters, Formatters, GridStateChange, OperatorType, Statistic } from 'angular-slickgrid';

interface CardSettings {
  title: string;
  iconClass: string;
  type: string;
}

@Component({
  selector: 'ngx-quiz-completion-report',
  templateUrl: './quiz-completion-report.component.html',
  styleUrls: ['./quiz-completion-report.component.scss']
  // template: `<router-outlet></router-outlet>`,
})
export class QuizCompletionReportComponent implements OnInit {
 @ViewChild('grid') Grid:any;
 
  angularGrid: AngularGridInstance;
  columnDefinitions: Column[];
 columnDefinitions1: Column[];
 columnDeflat:any[];
  gridOptions: GridOption;
  dataset: any[];
  statistics: Statistic;
  selectedTitles: any[];
  selectedTitle: any;
  gridObj: any;
  grid:any;
  errorMsg:any;
getData:any;
coloumName1:any;
coloumName2:any;
columnDes:any=[];
statcoldata:any=[];
 quizlistdrop:any=[];
userInductedData:any=[];
show:boolean=false;
  formdata:any ;
gridShow:boolean=false;
dataView:any;// angularGrid.dataView;
   // coloumName1:any=
   //  {   
   //  };
   loader:any;
 name:any;
 workflowname:any;  
 coursename:any;
  contdata: any

 ngOnInit(): void {
       // this.dataset = this.prepareData();
        this.gridOptions = {
        enableAutoResize: true,       // true by default
        enableCellNavigation: true,
        enableExcelCopyBuffer: true,
        enableFiltering: true,
        enableColumnReorder:false,
        rowSelectionOptions: {
          // True (Single Selection), False (Multiple Selections)
          selectActiveRow: false
        },
        enableGridMenu:true,
        // preselectedRows: [0, 2],
        enableCheckboxSelector: true,
        enableRowSelection: true,
        gridMenu:{
          hideRefreshDatasetCommand:false
        }
      };

   }


  constructor(private themeService: NbThemeService,private routes:ActivatedRoute, private router:Router,private quizreportService :QuizCompletionReportService,public cd : ChangeDetectorRef, public zone : NgZone,private AppService:AppService) {
    this.contdata =this.AppService.getuserdata();
    this.loader=true;
      //    this.quizreportService.getQuizDrop()
      // .subscribe(rescompData => { 
      // //  this.loader =false;
      //   this.quizlistdrop = rescompData.data[0];
      //   console.log(this.quizlistdrop , 'this.workdrop')
      // },
      // resUserError => {
      //   //this.loader =false;
      //   this.errorMsg = resUserError
      // });
     //  let param= this.routes.queryParams
     // .subscribe(params => {
     //   // Defaults to 0 if no query param provided.
     //   let quiz = +params['qid'] || 0;
     //   this.formdata={
     //     qid:quiz
     //   }
     //   })
      var data =this.quizreportService.data;

     if(data)
     {
        
         console.log('data',data) ;
         this.formdata={
          qid:data.quizId,
          userId:this.contdata.userId,
          roleId:this.contdata.roleId,
    
        }

            this.name =data.name;
           this.workflowname=data.workflowname;
           this.coursename=data.coursename;
         this.quizreportService.getcoursecompletion(this.formdata)
      .subscribe(rescompData => { 
        this.loader =false;
         var userInductedData=rescompData;
         console.log('userInductedData', userInductedData);
        this.userInductedData = rescompData.data = undefined ? null : rescompData.data;
        this.statcoldata =rescompData.data1 = undefined ? null :rescompData.data1;
        if(this.statcoldata)
        {
             this.gridShow =true;
        }else{
              this.gridShow =false;
        }
        console.log('this.creportdata', this.userInductedData);
         if(this.userInductedData)
           {
              var filterav;
             this.coloumName1=this.userInductedData[0];
            for (let key in this.coloumName1) {
                  // console.log(this.coloumName1[key]);
                    if(key != "userid" && key.length > 2 )
                    {
                       if(key != "attempt" && key.length > 2)
                      { 
                        if(key == "date of birth" || key == "date of joining")
                            {
                               filterav =  { model: Filters.compoundDate } 
                            }
                            else
                            {
                               filterav =  { model: Filters.compoundInput}
                            }
                    var id =key;
                     var field =key;
                       var name =key.toUpperCase();
                      
                   this.getData ={
                         id:id,
                       name:name,
                        field:field,
                        //filterParams: { newRowsAction: "keep" }
                        sortable: true,
                        minWidth: 100,
                        maxWidth:200,
                       type: FieldType.string, 
                       filterable: true,
                      filter: filterav
                    // this.columnTog.push(key);
                  }
                  this.columnDes.push(this.getData); 
                }
              }
              }
              for (let key in this.coloumName1) {
                  // console.log(this.coloumName1[key]);
                    if(key.length <= 2)
                    {
                    var id =key;
                     var field =key;
                       var name ='Q-'+key.toUpperCase();
                      
                   this.getData ={
                         id:id+100,
                       name:name,
                        field:field,
                        //filterParams: { newRowsAction: "keep" }
                        sortable: true,
                        minWidth: 100,
                        maxWidth:200,
                       type: FieldType.string, 
                       filterable: true,
                      filter: { model: Filters.compoundInput}
                    // this.columnTog.push(key);
                  }
                  this.columnDes.push(this.getData); 
                }
              }
              
            for(let i=0;i<this.userInductedData.length;i++)
            {
              
                this.userInductedData[i].id =i+1;
             
            }
           }

           this.columnDefinitions =this.columnDes;
         

            this.dataset = this.userInductedData;
           
             console.log('this.columnDefinitions', this.columnDefinitions);
        console.log('this.dataset', this.dataset);
      this.show =true;
       
        
      },
      resUserError => {
        this.loader =false;
        this.errorMsg = resUserError
      });
     }
  }


  // prepareGrid(){
  //   this.columnDefinitions = [
  //     { id: 'title', name: 'Title', field: 'title', sortable: true, minWidth: 55,
  //       type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
  //     },
  //     // { id: 'description', name: 'Description', field: 'description', filterable: true, sortable: true, minWidth: 80,
  //     //   type: FieldType.string,
  //     //   filter: {
  //     //     model: new CustomInputFilter() // create a new instance to make each Filter independent from each other
  //     //   }
  //     // },
  //     { id: 'duration', name: 'Duration (days)', field: 'duration', sortable: true, type: FieldType.number, exportCsvForceToKeepAsString: true,
  //       minWidth: 55,
  //       filterable: true,
  //     },
  //     { id: '%', name: '% Complete', field: 'percentComplete', sortable: true, formatter: Formatters.percentCompleteBar, minWidth: 70, type: FieldType.number,
  //       filterable: true, filter: { model: Filters.compoundInput } 
  //     },
  //     { id: 'start', name: 'Start', field: 'start', formatter: Formatters.dateIso, sortable: true, minWidth: 75, exportWithFormatter: true,
  //       type: FieldType.date, filterable: true, filter: { model: Filters.compoundDate } 
  //     },
  //     { id: 'finish', name: 'Finish', field: 'finish', formatter: Formatters.dateIso, sortable: true, minWidth: 75, exportWithFormatter: true,
  //       type: FieldType.date, filterable: true, filter: { model: Filters.compoundDate } 
  //     },
  //     { id: 'effort-driven', name: 'Effort Driven', field: 'effortDriven', minWidth: 85, maxWidth: 85,
  //       type: FieldType.boolean,
  //       sortable: true,
  //       filterable: true,
  //     }
  //   ];
  //   this.gridOptions = {
  //     enableAutoResize: true,       // true by default
  //     enableCellNavigation: true,
  //     enableExcelCopyBuffer: true,
  //     enableFiltering: true,
  //     rowSelectionOptions: {
  //       // True (Single Selection), False (Multiple Selections)
  //       selectActiveRow: false
  //     },
  //     // preselectedRows: [0, 2],
  //     enableCheckboxSelector: true,
  //     enableRowSelection: true,
  //   };

  //   // fill the dataset with your data
  //   // VERY IMPORTANT, Angular-Slickgrid uses Slickgrid DataView which REQUIRES a unique "id" and it has to be lowercase "id" and be part of the dataset
  //   // this.dataset = [];

  //   this.dataset = this.prepareData();
    
  // }

  // prepareData() {
  //   // mock a dataset
  //   const mockDataset = [];
  //   // for demo purpose, let's mock a 1000 lines of data
  //   for (let i = 0; i < 1000; i++) {
  //     const randomYear = 2000 + Math.floor(Math.random() * 10);
  //     const randomMonth = Math.floor(Math.random() * 11);
  //     const randomDay = Math.floor((Math.random() * 28));
  //     const randomPercent = Math.round(Math.random() * 100);

  //     mockDataset[i] = {
  //       id: i, // again VERY IMPORTANT to fill the "id" with unique values
  //       title: 'Task ' + i,
  //       duration: Math.round(Math.random() * 100) + '',
  //       percentComplete: randomPercent,
  //       start: `${randomMonth}/${randomDay}/${randomYear}`,
  //       finish: `${randomMonth}/${randomDay}/${randomYear}`,
  //       effortDriven: (i % 5 === 0)
  //     };
  //   }
  //   return mockDataset;
  // }

  angularGridReady(angularGrid: any) {
    //this.gridShow =true;

          if(this.statcoldata)
           {
this.columnDefinitions1=[];
      this.columnDefinitions1=[
       {
        field: "Employee ID",
        filterable: true,
        id: "Employee ID",
        maxWidth: 200,
        minWidth: 100,
        name: "EMPLOYEE ID",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
    {
        field: "Employee Name",
        filterable: true,
        id: "Employee Name",
        maxWidth: 200,
        minWidth: 100,
        name: "EMPLOYEE NAME",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
   
    {
        field: "Employee band",
        filterable: true,
        id: "Employee band",
        maxWidth: 200,
        minWidth: 100,
        name: "EMPLOYEE BAND",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
    {
        field: "RM code",
        filterable: true,
        id: "RM code",
        maxWidth: 200,
        minWidth: 100,
        name: "RM CODE",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
    {
        field: "RM name",
        filterable: true,
        id: "RM name",
        maxWidth: 200,
        minWidth: 100,
        name: "RM NAME",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
    {
        field: "department",
        filterable: true,
        id: "department",
        maxWidth: 200,
        minWidth: 100,
        name: "DEPARTMENT",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
    ]
    // this.columnDeflat =this.columnDefinitions1
               var filterav1;
             this.coloumName2=this.statcoldata[0];
             for (let key in this.coloumName2) {
                  // console.log(this.coloumName1[key]);

                    if(key != "userid" && key.length > 2)
                    {
                        if(key != "attempt" && key.length > 2)
                      { 
                         if(key == "endtime" || key == "starttime")
                            {
                               filterav1 =  { model: Filters.compoundDate } 
                            }
                            else
                            {
                               filterav1 =  { model: Filters.compoundInput}
                            }
                     var id =key;
                     var field =key;
                       var name =key.toUpperCase();
                      
                   this.getData ={
                         id:id,
                       name:name,
                        field:field,
                        //filterParams: { newRowsAction: "keep" }
                        sortable: true,
                        minWidth: 100,
                        maxWidth:200,
                       type: FieldType.string, 
                       filterable: true,
                      filter: filterav1
                    // this.columnTog.push(key);
                  }
                  this.columnDefinitions1.push(this.getData); 
                }
              }
              }
              for (let key in this.coloumName2) {
                  // console.log(this.coloumName1[key]);
                    if(key.length <= 2)
                    {
                    var id =key;
                     var field =key;
                       var name ='Q-'+key.toUpperCase();
                      
                   this.getData ={
                         id:id+100,
                       name:name,
                        field:field,
                        //filterParams: { newRowsAction: "keep" }
                        sortable: true,
                        minWidth: 100,
                        maxWidth:200,
                       type: FieldType.string, 
                       filterable: true,
                      filter: { model: Filters.compoundInput}
                    // this.columnTog.push(key);
                  }
                  this.columnDefinitions1.push(this.getData); 
                }
              }
            // for (let key in this.coloumName2) {
            //       // console.log(this.coloumName2[key]);
            //         if(key != "userid")
            //         {
            //         var id =key;
            //          var field =key;
            //          var name ='Q-'+key.toUpperCase();
                      
            //        this.getData ={
            //              id:id+100,
            //            name:name,
            //             field:field,
            //             //filterParams: { newRowsAction: "keep" }
            //             sortable: true,
            //             minWidth: 100,
            //             maxWidth:200,
            //            type: FieldType.string, 
            //            filterable: true,
            //           filter: { model: Filters.compoundInput}
            //         // this.columnTog.push(key);
            //       }
            //       this.columnDefinitions1.push(this.getData); 
            //     }
            //   }
              
           }
     //this.gridOptions._columnDefinitions = this.columnDefinitions1;
     this.angularGrid = angularGrid;
     this.dataView = angularGrid.dataView;
      this.grid = angularGrid.slickGrid;
     // angularGrid.gridService.controlAndPluginService.gridMenuControl.onColumnsChanged.subscribe(data=>{

     // });
     // var data =angularGrid.slickGrid.getColumns();
     console.log('col',this.angularGrid);
     this.grid.setColumns(this.columnDefinitions1);
      //this.grid.slickGrid.setColumns(this.columnDefinitions1);
      //this.grid.render();
    this.gridObj = angularGrid && angularGrid.slickGrid || {};
    //angularGrid.gridStateService.controlAndPluginService.gridMenuControl.init(angularGrid);
    let body = document.getElementsByTagName('body');
    let chaDe :any = this.cd;
    if(body){
      chaDe.rootNodes[1]=body[0];
    }
    chaDe.detectChanges();
    //this.processOutsideAngularZone();
    // angularGrid.gridService.controlAndPluginService.columnPickerControl.init(this.gridObj)
    // setTimeout(()=>{
    //     this.cd.detectChanges();
    // },500);
    //angularGrid.updateColumnDefinitionsList(this.columnDefinitions1);
  }

processOutsideAngularZone() {
  this.zone.runOutsideAngular(() => {
    
  });
}

  handleSelectedRowsChanged(e, args) {
    if (Array.isArray(args.rows)) {
      this.selectedTitles = args.rows.map(idx => {
        const item = this.gridObj.getDataItem(idx);
        return item.title || '';
      });
    }
  }

  /** Dispatched event of a Grid State Changed event */
  gridStateChanged(gridState: GridStateChange) {
    console.log('Client sample, Grid State changed:: ', gridState);
  }

  /** Save current Filters, Sorters in LocaleStorage or DB */
  saveCurrentGridState(grid) {
    this.grid =grid;
    // grid.GridStateService.controlAndPluginService.columnPickerControl.destroy();
    console.log('Client sample, last Grid State:: ', this.angularGrid.gridStateService.getCurrentGridState());

  }


  // onGridCreated(event)
  // {
  //   console.log('event ', event);
  // }
  colclear(angularGrid)
  {
    this.columnDefinitions =[];
    this.columnDefinitions1 =[];
    this.columnDes=[];
    this.statcoldata=[];

    //this.angularGrid.slickGrid.setColumns(this.columnDefinitions1);
    //angularGrid.gridStateService.controlAndPluginService.columnPickerControl.destroy()
    //angularGrid.gridStateService.resetColumns(this.columnDefinitions1=[])
     // this.gridOptions = {
     //    enableAutoResize: true,       // true by default
     //    enableCellNavigation: true,
     //    enableExcelCopyBuffer: true,
     //    enableFiltering: true,
     //    rowSelectionOptions: {
     //      // True (Single Selection), False (Multiple Selections)
     //      selectActiveRow: false
     //    },
     //    // preselectedRows: [0, 2],
     //    enableCheckboxSelector: true,
     //    enableRowSelection: true,
     //  };
     // this.saveCurrentGridState(this.grid);
  }

  callType(id)
    {
      //this.loader=true;
      //  this.angularGrid.dataView.refresh();
      // this.colclear(this.angularGrid);
      //this.gridShow =false;
      var calldata={qid:id};
    
      //  this.columnDefinitions.splice(1);
      //  console.log('this.columnDefinitions',this.columnDefinitions)
      // // this.columnDes=[];
       this.quizreportService.getcoursecompletion(calldata)
      .subscribe(rescompData => { 
        //this.loader =false;
        //this.creportdata = rescompData.data;
        this.userInductedData = rescompData.data.joinedData != undefined ? rescompData.data.joinedData.data :null ;
        this.statcoldata =rescompData.data.output != undefined ? rescompData.data.output :null;
        console.log('this.creportdata', this.userInductedData);
         if(this.userInductedData)
           {
             this.coloumName1=this.userInductedData[0];
            for (let key in this.coloumName1) {
                  // console.log(this.coloumName1[key]);
                    if(key != "userid")
                    {
                    var id =key;
                     var field =key;

                     if(key.length <=2)
                     {
                       var name ='DAY'+key.toUpperCase();
                     }
                     else{
                       var name =key.toUpperCase();
                     }
                      
                   this.getData ={
                         id:id,
                       name:name,
                        field:field,
                        //filterParams: { newRowsAction: "keep" }
                        sortable: true,
                        minWidth: 100,
                        maxWidth:200,
                       type: FieldType.string, 
                       filterable: true,
                      filter: { model: Filters.compoundInput}
                    // this.columnTog.push(key);
                  }
                  this.columnDes.push(this.getData); 
                }
              }

                for(let i=0;i<this.userInductedData.length;i++)
            {
              
                this.userInductedData[i].id =i+2;
             
            }
              
           }

           this.columnDefinitions =this.columnDes;
           

          
            this.dataset = this.userInductedData;
            
           
             console.log('this.columnDefinitions', this.columnDefinitions);
        console.log('this.dataset', this.dataset);
      this.show =true;
      this.angularGridReady(this.angularGrid);
      
      },
      resUserError => {
        //this.loader =false;
        this.errorMsg = resUserError
      });
        
    } 

    back(){
      
      this.router.navigate(['pages/evaluate/reporting/learning/quiz-completion-initial-report']);
  }
}
