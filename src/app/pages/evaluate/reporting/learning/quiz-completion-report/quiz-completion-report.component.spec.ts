import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuizCompletionReportComponent } from './quiz-completion-report.component';

describe('QuizCompletionReportComponent', () => {
  let component: QuizCompletionReportComponent;
  let fixture: ComponentFixture<QuizCompletionReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizCompletionReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizCompletionReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
