import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuizCompletionInitialReportComponent } from './quiz-completion-initial-report.component';

describe('QuizCompletionInitialReportComponent', () => {
  let component: QuizCompletionInitialReportComponent;
  let fixture: ComponentFixture<QuizCompletionInitialReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizCompletionInitialReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizCompletionInitialReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
