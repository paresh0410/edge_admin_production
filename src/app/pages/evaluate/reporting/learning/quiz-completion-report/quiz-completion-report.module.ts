import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

import { NgxEchartsModule } from 'ngx-echarts';

import { ThemeModule } from '../../../../../@theme/theme.module';
//import { slikgridDemo } from './../../..../slikgridDemo.component';
//import { slikgridDemoService } from './slikgridDemo.service';

import { QuizCompletionReportComponent } from './quiz-completion-report.component';

import { TranslateModule } from '@ngx-translate/core';
import { AngularSlickgridModule } from 'angular-slickgrid';

// const PLAN_COMPONENTS = [
//   slikgridDemo
// ];

@NgModule({
  imports: [
    ThemeModule,
    NgxEchartsModule,
    // UsersModule,
    AngularSlickgridModule.forRoot(),
    TranslateModule.forRoot()
  ],
  declarations: [
    QuizCompletionReportComponent,
  ],
  providers: [
    //slikgridDemoService,
  ],
  schemas: [ 
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA 
  ]
})
export class QuizCompletionReportModule { }
