import {Component, OnInit, OnDestroy, ChangeDetectorRef,NgZone, ViewChild} from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators/takeWhile' ;
//import { EmployeeInductionReportService } from './employee-induction-report.service';
import { Router, NavigationStart, Routes, ActivatedRoute } from '@angular/router';
import {QuizCompletionInitialReportService} from './quiz-completion-initial-report.service'
import { Column, GridOption, AngularGridInstance, FieldType, Filters, Formatters, GridStateChange, OperatorType, Statistic } from 'angular-slickgrid';
import {QuizCompletionReportService} from './quiz-completion-report.service'
interface CardSettings {
  title: string;
  iconClass: string;
  type: string;
}

@Component({
  selector: 'ngx-quiz-completion-initial-report',
  templateUrl: './quiz-completion-initial-report.component.html',
  styleUrls: ['./quiz-completion-initial-report.component.scss']
  // template: `<router-outlet></router-outlet>`,
})
export class QuizCompletionInitialReportComponent implements OnInit {
 @ViewChild('grid') Grid:any;
 
  angularGrid: AngularGridInstance;
  columnDefinitions: Column[];
 columnDefinitions1: Column[];
 columnDeflat:any[];
  gridOptions: GridOption;
  dataset: any[];
  statistics: Statistic;
  selectedTitles: any[];
  selectedTitle: any;
  gridObj: any;
  grid:any;
  errorMsg:any;
  loader:any;
getData:any;
coloumName1:any;
coloumName2:any;
columnDes:any=[];
statcoldata:any=[];
 quizlistdrop:any=[];
userInductedData:any=[];
show:boolean=false;
  formdata:any ={
            qid:14,
          }
gridShow:boolean=false;
dataView:any;// angularGrid.dataView;
   // coloumName1:any=
   //  {   
   //  };

 ngOnInit(): void {
       // this.dataset = this.prepareData();
        this.gridOptions = {
        enableAutoResize: true,       // true by default
        enableCellNavigation: true,
        enableExcelCopyBuffer: true,
        enableFiltering: true,
        enableColumnReorder:false,
        rowSelectionOptions: {
          // True (Single Selection), False (Multiple Selections)
          selectActiveRow: true,
        },
        enableGridMenu:true,
        // preselectedRows: [0, 2],
        enableCheckboxSelector: true,
        enableRowSelection: true,
        gridMenu:{
          hideRefreshDatasetCommand:false
        }
      };

   }


  constructor(private themeService: NbThemeService, private router:Router,private quizreportService :QuizCompletionInitialReportService,public cd : ChangeDetectorRef, public zone : NgZone,
   private routes:ActivatedRoute,private passservice:QuizCompletionReportService) {
    this.gridShow =false;
     this.loader =true;
         this.quizreportService.getQuizDrop()
      .subscribe(rescompData => { 
      //  this.loader =false;
        this.quizlistdrop = rescompData.data[0];
        console.log(this.quizlistdrop , 'this.workdrop')
         if(this.userInductedData)
           {
            //  this.coloumName1=this.quizlistdrop[0];
            // for (let key in this.coloumName1) {
            //       // console.log(this.coloumName1[key]);
            //         if(key != "quizId")
            //         {
            //         var id =key;
            //         var field =key;
            //         var name =key.toUpperCase();                      
            //        this.getData ={
            //              id:id,
            //            name:name,
            //             field:field,
            //             //filterParams: { newRowsAction: "keep" }
            //             sortable: true,
            //             // minWidth: 100,
            //             // maxWidth:200,
            //            type: FieldType.string, 
            //            filterable: true,
            //           filter: { model: Filters.compoundInput}
            //         // this.columnTog.push(key);
            //       }
            //       this.columnDes.push(this.getData); 
            //     }
            //   }
           
      this.columnDefinitions=[
    {
        field: "name",
        filterable: true,
        id: "name",
        name: "QUIZ NAME",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
    {
        field: "coursename",
        filterable: true,
        id: "coursename",
        name: "COURSE NAME",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
   
    {
        field: "workflowname",
        filterable: true,
        id: "workflowname",
        name: "WORKFLOW NAME",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
    ]
              
            for(let i=0;i<this.quizlistdrop.length;i++)
            {
              
                this.quizlistdrop[i].id =i+1;
             
            }
           }

           // this.columnDefinitions =this.columnDes;
         

            this.dataset = this.quizlistdrop;
           
             console.log('this.columnDefinitions', this.columnDefinitions);
        console.log('this.dataset', this.dataset);
         this.loader =false;
      this.show =true;
      },
      resUserError => {
        this.loader =false;
        this.errorMsg = resUserError
      });


      //    this.quizreportService.getcoursecompletion(this.formdata)
      // .subscribe(rescompData => { 
      //   //this.loader =false;
      //   this.userInductedData = rescompData.data.joinedData = undefined ? null : rescompData.data.joinedData.data;
      //   this.statcoldata =rescompData.data.output = undefined ? null :rescompData.data.output;
      //   console.log('this.creportdata', this.userInductedData);
      //    if(this.userInductedData)
      //      {
      //        this.coloumName1=this.userInductedData[0];
      //       for (let key in this.coloumName1) {
      //             // console.log(this.coloumName1[key]);
      //               if(key != "userid")
      //               {
      //               var id =key;
      //                var field =key;

      //                if(key.length <=2)
      //                {
      //                  var name ='DAY'+key.toUpperCase();
      //                }
      //                else{
      //                  var name =key.toUpperCase();
      //                }
                      
      //              this.getData ={
      //                    id:id,
      //                  name:name,
      //                   field:field,
      //                   //filterParams: { newRowsAction: "keep" }
      //                   sortable: true,
      //                   minWidth: 100,
      //                   maxWidth:200,
      //                  type: FieldType.string, 
      //                  filterable: true,
      //                 filter: { model: Filters.compoundInput}
      //               // this.columnTog.push(key);
      //             }
      //             this.columnDes.push(this.getData); 
      //           }
      //         }
              
      //       for(let i=0;i<this.userInductedData.length;i++)
      //       {
              
      //           this.userInductedData[i].id =i+1;
             
      //       }
      //      }

      //      this.columnDefinitions =this.columnDes;
         

      //       this.dataset = this.userInductedData;
           
      //        console.log('this.columnDefinitions', this.columnDefinitions);
      //   console.log('this.dataset', this.dataset);
      // this.show =true;
        
      // },
      // resUserError => {
      //   // this.loader =false;
      //   this.errorMsg = resUserError
      // });
    
  }


  // prepareGrid(){
  //   this.columnDefinitions = [
  //     { id: 'title', name: 'Title', field: 'title', sortable: true, minWidth: 55,
  //       type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
  //     },
  //     // { id: 'description', name: 'Description', field: 'description', filterable: true, sortable: true, minWidth: 80,
  //     //   type: FieldType.string,
  //     //   filter: {
  //     //     model: new CustomInputFilter() // create a new instance to make each Filter independent from each other
  //     //   }
  //     // },
  //     { id: 'duration', name: 'Duration (days)', field: 'duration', sortable: true, type: FieldType.number, exportCsvForceToKeepAsString: true,
  //       minWidth: 55,
  //       filterable: true,
  //     },
  //     { id: '%', name: '% Complete', field: 'percentComplete', sortable: true, formatter: Formatters.percentCompleteBar, minWidth: 70, type: FieldType.number,
  //       filterable: true, filter: { model: Filters.compoundInput } 
  //     },
  //     { id: 'start', name: 'Start', field: 'start', formatter: Formatters.dateIso, sortable: true, minWidth: 75, exportWithFormatter: true,
  //       type: FieldType.date, filterable: true, filter: { model: Filters.compoundDate } 
  //     },
  //     { id: 'finish', name: 'Finish', field: 'finish', formatter: Formatters.dateIso, sortable: true, minWidth: 75, exportWithFormatter: true,
  //       type: FieldType.date, filterable: true, filter: { model: Filters.compoundDate } 
  //     },
  //     { id: 'effort-driven', name: 'Effort Driven', field: 'effortDriven', minWidth: 85, maxWidth: 85,
  //       type: FieldType.boolean,
  //       sortable: true,
  //       filterable: true,
  //     }
  //   ];
  //   this.gridOptions = {
  //     enableAutoResize: true,       // true by default
  //     enableCellNavigation: true,
  //     enableExcelCopyBuffer: true,
  //     enableFiltering: true,
  //     rowSelectionOptions: {
  //       // True (Single Selection), False (Multiple Selections)
  //       selectActiveRow: false
  //     },
  //     // preselectedRows: [0, 2],
  //     enableCheckboxSelector: true,
  //     enableRowSelection: true,
  //   };

  //   // fill the dataset with your data
  //   // VERY IMPORTANT, Angular-Slickgrid uses Slickgrid DataView which REQUIRES a unique "id" and it has to be lowercase "id" and be part of the dataset
  //   // this.dataset = [];

  //   this.dataset = this.prepareData();
    
  // }

  // prepareData() {
  //   // mock a dataset
  //   const mockDataset = [];
  //   // for demo purpose, let's mock a 1000 lines of data
  //   for (let i = 0; i < 1000; i++) {
  //     const randomYear = 2000 + Math.floor(Math.random() * 10);
  //     const randomMonth = Math.floor(Math.random() * 11);
  //     const randomDay = Math.floor((Math.random() * 28));
  //     const randomPercent = Math.round(Math.random() * 100);

  //     mockDataset[i] = {
  //       id: i, // again VERY IMPORTANT to fill the "id" with unique values
  //       title: 'Task ' + i,
  //       duration: Math.round(Math.random() * 100) + '',
  //       percentComplete: randomPercent,
  //       start: `${randomMonth}/${randomDay}/${randomYear}`,
  //       finish: `${randomMonth}/${randomDay}/${randomYear}`,
  //       effortDriven: (i % 5 === 0)
  //     };
  //   }
  //   return mockDataset;
  // }

  angularGridReady(angularGrid: any) {
    this.gridShow =true;
// this.columnDefinitions1=[];
//       this.columnDefinitions1=[
//     {
//         field: "Employee Code",
//         filterable: true,
//         id: "Employee Code",
//         maxWidth: 200,
//         minWidth: 100,
//         name: "EMPLOYEE CODE",
//         sortable: true,
//         type: FieldType.string, 
//         filter: { model: Filters.compoundInput}
//     },
//     {
//         field: "Employee Name",
//         filterable: true,
//         id: "Employee Name",
//         maxWidth: 200,
//         minWidth: 100,
//         name: "EMPLOYEE NAME",
//         sortable: true,
//         type: FieldType.string, 
//         filter: { model: Filters.compoundInput}
//     },
   
//     {
//         field: "Employee band",
//         filterable: true,
//         id: "Employee band",
//         maxWidth: 200,
//         minWidth: 100,
//         name: "EMPLOYEE BAND",
//         sortable: true,
//         type: FieldType.string, 
//         filter: { model: Filters.compoundInput}
//     },
//     {
//         field: "Reportingmanager code",
//         filterable: true,
//         id: "Reportingmanager code",
//         maxWidth: 200,
//         minWidth: 100,
//         name: "REPORTINGMANAGER CODE",
//         sortable: true,
//         type: FieldType.string, 
//         filter: { model: Filters.compoundInput}
//     },
//     {
//         field: "Reportingmanager name",
//         filterable: true,
//         id: "Reportingmanager name",
//         maxWidth: 200,
//         minWidth: 100,
//         name: "REPORTINGMANAGER NAME",
//         sortable: true,
//         type: FieldType.string, 
//         filter: { model: Filters.compoundInput}
//     },
//     {
//         field: "department",
//         filterable: true,
//         id: "department",
//         maxWidth: 200,
//         minWidth: 100,
//         name: "DEPARTMENT",
//         sortable: true,
//         type: FieldType.string, 
//         filter: { model: Filters.compoundInput}
//     },
//     ]
//     // this.columnDeflat =this.columnDefinitions1
//       if(this.statcoldata)
//            {
//              this.coloumName2=this.statcoldata[0];
//             for (let key in this.coloumName2) {
//                   // console.log(this.coloumName2[key]);
//                     if(key != "userid")
//                     {
//                     var id =key;
//                      var field =key;
//                      var name =key;
                      
//                    this.getData ={
//                          id:id,
//                        name:name,
//                         field:field,
//                         //filterParams: { newRowsAction: "keep" }
//                         sortable: true,
//                         minWidth: 100,
//                         maxWidth:200,
//                        type: FieldType.string, 
//                        filterable: true,
//                       filter: { model: Filters.compoundInput}
//                     // this.columnTog.push(key);
//                   }
//                   this.columnDefinitions1.push(this.getData); 
//                 }
//               }
              
//            }
     //this.gridOptions._columnDefinitions = this.columnDefinitions1;
     this.angularGrid = angularGrid;
     this.dataView = angularGrid.dataView;
      this.grid = angularGrid.slickGrid;
     // angularGrid.gridService.controlAndPluginService.gridMenuControl.onColumnsChanged.subscribe(data=>{

     // });
     // var data =angularGrid.slickGrid.getColumns();
     console.log('col',this.angularGrid);
     // this.grid.setColumns(this.columnDefinitions1);
      //this.grid.slickGrid.setColumns(this.columnDefinitions1);
      //this.grid.render();
    this.gridObj = angularGrid && angularGrid.slickGrid || {};
    //angularGrid.gridStateService.controlAndPluginService.gridMenuControl.init(angularGrid);
    let body = document.getElementsByTagName('body');
    let chaDe :any = this.cd;
    if(body){
      chaDe.rootNodes[1]=body[0];
    }
    chaDe.detectChanges();
    //this.processOutsideAngularZone();
    // angularGrid.gridService.controlAndPluginService.columnPickerControl.init(this.gridObj)
    // setTimeout(()=>{
    //     this.cd.detectChanges();
    // },500);
    //angularGrid.updateColumnDefinitionsList(this.columnDefinitions1);
  }

processOutsideAngularZone() {
  this.zone.runOutsideAngular(() => {
    
  });
}
 gotoquizcompletionreport(item){
   var data1=
   {
     name:item.name,
     quizId:item.quizId,
     workflowname:item.workflowname,
     coursename:item.coursename

   }
   this.passservice.data =data1;
    this.router.navigate(['quiz-completion-report'],{relativeTo:this.routes});
  }

  handleSelectedRowsChanged(e, args) {
    if (Array.isArray(args.rows)) {
      this.selectedTitles = args.rows.map(idx => {
        const item = this.gridObj.getDataItem(idx);
        // var id =item.quizId;
        this.gotoquizcompletionreport(item)
        return item.title || '';
      });
    }
  }


  /** Dispatched event of a Grid State Changed event */
  gridStateChanged(gridState: GridStateChange) {
    console.log('Client sample, Grid State changed:: ', gridState);
  }

  /** Save current Filters, Sorters in LocaleStorage or DB */
  saveCurrentGridState(grid) {
    this.grid =grid;
    // grid.GridStateService.controlAndPluginService.columnPickerControl.destroy();
    console.log('Client sample, last Grid State:: ', this.angularGrid.gridStateService.getCurrentGridState());

  }


  // onGridCreated(event)
  // {
  //   console.log('event ', event);
  // }
  colclear(angularGrid)
  {
    this.columnDefinitions =[];
    this.columnDefinitions1 =[];
    this.columnDes=[];
    this.statcoldata=[];

    //this.angularGrid.slickGrid.setColumns(this.columnDefinitions1);
    //angularGrid.gridStateService.controlAndPluginService.columnPickerControl.destroy()
    //angularGrid.gridStateService.resetColumns(this.columnDefinitions1=[])
     // this.gridOptions = {
     //    enableAutoResize: true,       // true by default
     //    enableCellNavigation: true,
     //    enableExcelCopyBuffer: true,
     //    enableFiltering: true,
     //    rowSelectionOptions: {
     //      // True (Single Selection), False (Multiple Selections)
     //      selectActiveRow: false
     //    },
     //    // preselectedRows: [0, 2],
     //    enableCheckboxSelector: true,
     //    enableRowSelection: true,
     //  };
     // this.saveCurrentGridState(this.grid);
  }

  // callType(id)
  //   {
  //     //this.loader=true;
  //     //  this.angularGrid.dataView.refresh();
  //     // this.colclear(this.angularGrid);
  //     this.gridShow =false;
  //     var calldata={qid:id};
    
  //     //  this.columnDefinitions.splice(1);
  //     //  console.log('this.columnDefinitions',this.columnDefinitions)
  //     // // this.columnDes=[];
  //      this.quizreportService.getcoursecompletion(calldata)
  //     .subscribe(rescompData => { 
  //       //this.loader =false;
  //       //this.creportdata = rescompData.data;
  //       this.userInductedData = rescompData.data.joinedData != undefined ? rescompData.data.joinedData.data :null ;
  //       this.statcoldata =rescompData.data.output != undefined ? rescompData.data.output :null;
  //       console.log('this.creportdata', this.userInductedData);
  //        if(this.userInductedData)
  //          {
  //            this.coloumName1=this.userInductedData[0];
  //           for (let key in this.coloumName1) {
  //                 // console.log(this.coloumName1[key]);
  //                   if(key != "userid")
  //                   {
  //                   var id =key;
  //                    var field =key;

  //                    if(key.length <=2)
  //                    {
  //                      var name ='DAY'+key.toUpperCase();
  //                    }
  //                    else{
  //                      var name =key.toUpperCase();
  //                    }
                      
  //                  this.getData ={
  //                        id:id,
  //                      name:name,
  //                       field:field,
  //                       //filterParams: { newRowsAction: "keep" }
  //                       sortable: true,
  //                       minWidth: 100,
  //                       maxWidth:200,
  //                      type: FieldType.string, 
  //                      filterable: true,
  //                     filter: { model: Filters.compoundInput}
  //                   // this.columnTog.push(key);
  //                 }
  //                 this.columnDes.push(this.getData); 
  //               }
  //             }

  //               for(let i=0;i<this.userInductedData.length;i++)
  //           {
              
  //               this.userInductedData[i].id =i+2;
             
  //           }
              
  //          }

  //          this.columnDefinitions =this.columnDes;
           

          
  //           this.dataset = this.userInductedData;
            
           
  //            console.log('this.columnDefinitions', this.columnDefinitions);
  //       console.log('this.dataset', this.dataset);
  //     this.show =true;
  //     this.angularGridReady(this.angularGrid);
      
  //     },
  //     resUserError => {
  //       //this.loader =false;
  //       this.errorMsg = resUserError
  //     });
        
  //   } 
 back(){
      
      this.router.navigate(['pages/evaluate/reporting/learning']);
  }
 
}
