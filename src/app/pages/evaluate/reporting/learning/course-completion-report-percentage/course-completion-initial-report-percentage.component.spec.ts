import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseCompletionInitialReportPercentageComponent } from './course-completion-initial-report-percentage.component';

describe('CourseCompletionInitialReportPercentageComponent', () => {
  let component: CourseCompletionInitialReportPercentageComponent;
  let fixture: ComponentFixture<CourseCompletionInitialReportPercentageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseCompletionInitialReportPercentageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseCompletionInitialReportPercentageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
