import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseCompletionReportPercentageComponent } from './course-completion-report-percentage.component';

describe('CourseCompletionReportPercentageComponent', () => {
  let component: CourseCompletionReportPercentageComponent;
  let fixture: ComponentFixture<CourseCompletionReportPercentageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseCompletionReportPercentageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseCompletionReportPercentageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
