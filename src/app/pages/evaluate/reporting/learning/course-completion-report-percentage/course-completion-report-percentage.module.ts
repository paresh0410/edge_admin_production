import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

import { NgxEchartsModule } from 'ngx-echarts';

import { ThemeModule } from '../../../../../@theme/theme.module';
//import { slikgridDemo } from './../../..../slikgridDemo.component';
//import { slikgridDemoService } from './slikgridDemo.service';
import { CourseCompletionReportPercentageService } from './course-completion-report-percentage.service';
import { CourseCompletionReportPercentageComponent } from './course-completion-report-percentage.component';

import { TranslateModule } from '@ngx-translate/core';
import { AngularSlickgridModule } from 'angular-slickgrid';

// const PLAN_COMPONENTS = [
//   slikgridDemo
// ];

@NgModule({
  imports: [
    ThemeModule,
    NgxEchartsModule,
    // UsersModule,
    AngularSlickgridModule.forRoot(),
    TranslateModule.forRoot()
  ],
  declarations: [
    CourseCompletionReportPercentageComponent,
  ],
  providers: [
    CourseCompletionReportPercentageService
    //slikgridDemoService,
  ],
  schemas: [ 
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA 
  ]
})
export class CourseCompletionReportPercentageModule { }
