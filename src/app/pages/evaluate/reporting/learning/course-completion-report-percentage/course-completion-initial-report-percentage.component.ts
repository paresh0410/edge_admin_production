import {Component, OnInit, OnDestroy,ViewEncapsulation} from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators/takeWhile' ;
import { CourseCompletionInitialReportPercentageService } from './course-completion-initial-report-percentage.service';
import { CourseCompletionReportPercentageService } from './course-completion-report-percentage.service';
import { Router, NavigationStart, Routes, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { Column, GridOption, AngularGridInstance, FieldType, Filters, Formatters, GridStateChange, OperatorType, Statistic } from 'angular-slickgrid';

interface CardSettings {
  title: string;
  iconClass: string;
  type: string;
}

@Component({
  selector: 'ngx-course-completion-initial-report-percentage',
  templateUrl: './course-completion-initial-report-percentage.component.html',
  styleUrls: ['./course-completion-initial-report-percentage.component.scss'],
     encapsulation: ViewEncapsulation.None,
   providers:[DatePipe] 
  // template: `<router-outlet></router-outlet>`,
})
export class CourseCompletionInitialReportPercentageComponent implements OnInit {

  angularGrid: AngularGridInstance;
  columnDefinitions: Column[];
  columnDefinitions1: Column[];
  gridOptions: GridOption;
  dataset: any[];
  statistics: Statistic;
  selectedTitles: any[];
  selectedTitle: any;
  gridObj: any;
   formdata:any ={
            id:38,
          };
  coloumName1:any=
  {   
  }
  errorMsg: string;
  loader :any;

  creportdata:any=[];
  columnDes:any=[];
  columnTog:any=[];
  workdrop:any=[];
  getData:any;
//coloumName1:any;
coloumName2:any;
//columnDes:any=[];
statcoldata:any=[];
 quizlistdrop:any=[];
userInductedData:any=[];
show:boolean=false;
 

  ngOnInit(): void {
     this.gridOptions = {
        enableAutoResize: true,       // true by default
        enableCellNavigation: true,
        enableExcelCopyBuffer: true,
        enableFiltering: true,
        enableColumnReorder:false,
        rowSelectionOptions: {
          // True (Single Selection), False (Multiple Selections)
          selectActiveRow: false
        },
        // preselectedRows: [0, 2],
        enableCheckboxSelector: true,
        enableRowSelection: true,
      };
  }

  constructor(private themeService: NbThemeService,private routes:ActivatedRoute,private datePipe:DatePipe, private router:Router,private ccreportService :CourseCompletionInitialReportPercentageService,private passservice :CourseCompletionReportPercentageService) {
      this.loader =true;
     this.ccreportService.getCohortDrop()
      .subscribe(rescompData => { 
       // this.loader =false;
        this.workdrop = rescompData.data[0];
        console.log(this.workdrop , 'this.workdrop');
               if(this.workdrop)
           {
             this.coloumName1=this.workdrop[0];
            for (let key in this.coloumName1) {
                  // console.log(this.coloumName1[key]);
                    if(key != "cId")
                    {
                    var id =key;
                     var field =key;

                   
                       var name =key.toUpperCase();
                     
                      
                   this.getData ={
                         id:id,
                       name:name,
                        field:field,
                        //filterParams: { newRowsAction: "keep" }
                        sortable: true,
                        // minWidth: 100,
                        // maxWidth:200,
                       type: FieldType.string, 
                       filterable: true,
                      filter: { model: Filters.compoundInput}
                    // this.columnTog.push(key);
                  }
                  this.columnDes.push(this.getData); 
                   for(let i=0;i<this.workdrop.length;i++)
            {
              
                this.workdrop[i].id =i+1;
             
            }
                }
                // for(var i=0;i<this.creportdata.length;i++)
                //    { 
                //        for(let key in this.creportdata[i])
                //        {
                //          var ckey =key
                //          if(ckey === "Student")
                //          {
                //            this.creportdata[i][ckey];
                //          }else
                //          {
                //            if(this.creportdata[i][ckey] != 0)
                //            {
                //              this.creportdata[i][ckey]=this.datePipe.transform(new Date(this.creportdata[i][key] * 1000),'dd-MM-yyyy');
                //               console.log(this.creportdata[i][ckey],'this.creportdata[i][key]')
                //            }else
                //            {
                //              this.creportdata[i][ckey]='NA';
                //            }    
                //          }
                //        }
                //    } 
           }

}

          this.columnDefinitions =this.columnDes;
          this.dataset = this.workdrop;
          this.loader =false;
          this.show =true;
      },
      resUserError => {
        this.loader =false;
        this.errorMsg = resUserError
      });

//       this.ccreportService.getcoursecompletion(this.formdata)
//       .subscribe(rescompData => { 
//         this.loader =false;
//         this.creportdata = rescompData.data = undefined ? null : rescompData.data;
//          if(this.creportdata)
//            {
//              this.coloumName1=this.creportdata[0];
//             for (let key in this.coloumName1) {
//                   // console.log(this.coloumName1[key]);
//                     if(key != "userid")
//                     {
//                     var id =key;
//                      var field =key;

                   
//                        var name =key.toUpperCase();
                     
                      
//                    this.getData ={
//                          id:id,
//                        name:name,
//                         field:field,
//                         //filterParams: { newRowsAction: "keep" }
//                         sortable: true,
//                         minWidth: 100,
//                         maxWidth:200,
//                        type: FieldType.string, 
//                        filterable: true,
//                       filter: { model: Filters.compoundInput}
//                     // this.columnTog.push(key);
//                   }
//                   this.columnDes.push(this.getData); 
//                    for(let i=0;i<this.creportdata.length;i++)
//             {
              
//                 this.creportdata[i].id =i+1;
             
//             }
//                 }
//                 // for(var i=0;i<this.creportdata.length;i++)
//                 //    { 
//                 //        for(let key in this.creportdata[i])
//                 //        {
//                 //          var ckey =key
//                 //          if(ckey === "Student")
//                 //          {
//                 //            this.creportdata[i][ckey];
//                 //          }else
//                 //          {
//                 //            if(this.creportdata[i][ckey] != 0)
//                 //            {
//                 //              this.creportdata[i][ckey]=this.datePipe.transform(new Date(this.creportdata[i][key] * 1000),'dd-MM-yyyy');
//                 //               console.log(this.creportdata[i][ckey],'this.creportdata[i][key]')
//                 //            }else
//                 //            {
//                 //              this.creportdata[i][ckey]='NA';
//                 //            }    
//                 //          }
//                 //        }
//                 //    } 
//            }

// }

//           this.columnDefinitions =this.columnDes;
//           this.dataset = this.creportdata;
//           this.show =true;
//         console.log(this.creportdata , 'this.creportdata')
//       },
//       resUserError => {
//         this.loader =false;
//         this.errorMsg = resUserError
//       });
    // this.dataset = this.prepareData();
  }

  // prepareGrid(){
  //   this.columnDefinitions = [
  //     { id: 'title', name: 'Title', field: 'title', sortable: true, minWidth: 55,
  //       type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
  //     },
  //     // { id: 'description', name: 'Description', field: 'description', filterable: true, sortable: true, minWidth: 80,
  //     //   type: FieldType.string,
  //     //   filter: {
  //     //     model: new CustomInputFilter() // create a new instance to make each Filter independent from each other
  //     //   }
  //     // },
  //     { id: 'duration', name: 'Duration (days)', field: 'duration', sortable: true, type: FieldType.number, exportCsvForceToKeepAsString: true,
  //       minWidth: 55,
  //       filterable: true,
  //     },
  //     { id: '%', name: '% Complete', field: 'percentComplete', sortable: true, formatter: Formatters.percentCompleteBar, minWidth: 70, type: FieldType.number,
  //       filterable: true, filter: { model: Filters.compoundInput } 
  //     },
  //     { id: 'start', name: 'Start', field: 'start', formatter: Formatters.dateIso, sortable: true, minWidth: 75, exportWithFormatter: true,
  //       type: FieldType.date, filterable: true, filter: { model: Filters.compoundDate } 
  //     },
  //     { id: 'finish', name: 'Finish', field: 'finish', formatter: Formatters.dateIso, sortable: true, minWidth: 75, exportWithFormatter: true,
  //       type: FieldType.date, filterable: true, filter: { model: Filters.compoundDate } 
  //     },
  //     { id: 'effort-driven', name: 'Effort Driven', field: 'effortDriven', minWidth: 85, maxWidth: 85,
  //       type: FieldType.boolean,
  //       sortable: true,
  //       filterable: true,
  //     }
  //   ];
  //   this.gridOptions = {
  //     enableAutoResize: true,       // true by default
  //     enableCellNavigation: true,
  //     enableExcelCopyBuffer: true,
  //     enableFiltering: true,
  //     rowSelectionOptions: {
  //       // True (Single Selection), False (Multiple Selections)
  //       selectActiveRow: false
  //     },
  //     // preselectedRows: [0, 2],
  //     enableCheckboxSelector: true,
  //     enableRowSelection: true,
  //   };

  //   // fill the dataset with your data
  //   // VERY IMPORTANT, Angular-Slickgrid uses Slickgrid DataView which REQUIRES a unique "id" and it has to be lowercase "id" and be part of the dataset
  //   // this.dataset = [];

  //   this.dataset = this.prepareData();
    
  // }

  // prepareData() {
  //   // mock a dataset
  //   const mockDataset = [];
  //   // for demo purpose, let's mock a 1000 lines of data
  //   for (let i = 0; i < 1000; i++) {
  //     const randomYear = 2000 + Math.floor(Math.random() * 10);
  //     const randomMonth = Math.floor(Math.random() * 11);
  //     const randomDay = Math.floor((Math.random() * 28));
  //     const randomPercent = Math.round(Math.random() * 100);

  //     mockDataset[i] = {
  //       id: i, // again VERY IMPORTANT to fill the "id" with unique values
  //       title: 'Task ' + i,
  //       duration: Math.round(Math.random() * 100) + '',
  //       percentComplete: randomPercent,
  //       start: `${randomMonth}/${randomDay}/${randomYear}`,
  //       finish: `${randomMonth}/${randomDay}/${randomYear}`,
  //       effortDriven: (i % 5 === 0)
  //     };
  //   }
  //   return mockDataset;
  // }

  angularGridReady(angularGrid: any) {
    //       this.columnDefinitions1=[
    // {
    //     field: "Employee Code",
    //     filterable: true,
    //     id: "Employee Code",
    //     maxWidth: 200,
    //     minWidth: 100,
    //     name: "EMPLOYEE CODE",
    //     sortable: true,
    //     type: FieldType.string, 
    //     filter: { model: Filters.compoundInput}
    // },
    // {
    //     field: "Employee Name",
    //     filterable: true,
    //     id: "Employee Name",
    //     maxWidth: 200,
    //     minWidth: 100,
    //     name: "EMPLOYEE NAME",
    //     sortable: true,
    //     type: FieldType.string, 
    //     filter: { model: Filters.compoundInput}
    // },
   
    // {
    //     field: "Employee band",
    //     filterable: true,
    //     id: "Employee band",
    //     maxWidth: 200,
    //     minWidth: 100,
    //     name: "EMPLOYEE BAND",
    //     sortable: true,
    //     type: FieldType.string, 
    //     filter: { model: Filters.compoundInput}
    // },
    // {
    //     field: "Reportingmanager code",
    //     filterable: true,
    //     id: "Reportingmanager code",
    //     maxWidth: 200,
    //     minWidth: 100,
    //     name: "REPORTINGMANAGER CODE",
    //     sortable: true,
    //     type: FieldType.string, 
    //     filter: { model: Filters.compoundInput}
    // },
    // {
    //     field: "Reportingmanager name",
    //     filterable: true,
    //     id: "Reportingmanager name",
    //     maxWidth: 200,
    //     minWidth: 100,
    //     name: "REPORTINGMANAGER NAME",
    //     sortable: true,
    //     type: FieldType.string, 
    //     filter: { model: Filters.compoundInput}
    // },
    // {
    //     field: "department",
    //     filterable: true,
    //     id: "department",
    //     maxWidth: 200,
    //     minWidth: 100,
    //     name: "DEPARTMENT",
    //     sortable: true,
    //     type: FieldType.string, 
    //     filter: { model: Filters.compoundInput}
    // },
    // ]
    // this.columnDeflat =this.columnDefinitions1
      // if(this.statcoldata)
      //      {
      //        this.coloumName2=this.statcoldata[0];
      //       for (let key in this.coloumName2) {
      //             // console.log(this.coloumName2[key]);
      //               if(key != "userid")
      //               {
      //               var id =key;
      //                var field =key;
      //                var name =key;
                      
      //              this.getData ={
      //                    id:id,
      //                  name:name,
      //                   field:field,
      //                   //filterParams: { newRowsAction: "keep" }
      //                   sortable: true,
      //                   minWidth: 100,
      //                   maxWidth:200,
      //                  type: FieldType.string, 
      //                  filterable: true,
      //                 filter: { model: Filters.compoundInput}
      //               // this.columnTog.push(key);
      //             }
      //             this.columnDefinitions1.push(this.getData); 
      //           }
      //         }
              
      //      }
    this.angularGrid = angularGrid;
    this.gridObj = angularGrid && angularGrid.slickGrid || {};
  }

  gotoquizcompletionreport(item){
   var data1=
   {
     name:item.name,
     cId:item.cId,
     workflowname:item.workflowname,
     //coursename:item.coursename

   }
   this.passservice.data =data1;
    this.router.navigate(['course-completion-report-percentage'],{relativeTo:this.routes});
  }
   back(){
      
      this.router.navigate(['pages/evaluate/reporting/learning']);
  }

  handleSelectedRowsChanged(e, args) {
    if (Array.isArray(args.rows)) {
      this.selectedTitles = args.rows.map(idx => {
        const item = this.gridObj.getDataItem(idx);
        this.gotoquizcompletionreport(item);
        return item.title || '';
      });
    }
  }


  /** Dispatched event of a Grid State Changed event */
  gridStateChanged(gridState: GridStateChange) {
    console.log('Client sample, Grid State changed:: ', gridState);
  }

  /** Save current Filters, Sorters in LocaleStorage or DB */
  saveCurrentGridState(grid) {
    console.log('Client sample, last Grid State:: ', this.angularGrid.gridStateService.getCurrentGridState());
  }
   colclear(angularGrid)
  {
    this.columnDefinitions =[];
    this.columnDefinitions1 =[];
    this.columnDes=[];
    this.statcoldata=[];
  }
  callType(id)
    {
      //this.loader=true;
      //  this.angularGrid.dataView.refresh();
       this.colclear(this.angularGrid);
      // this.gridShow =false;
      var calldata={id:id};
    
      //  this.columnDefinitions.splice(1);
      //  console.log('this.columnDefinitions',this.columnDefinitions)
      // // this.columnDes=[];
       this.ccreportService.getcoursecompletion(calldata)
      .subscribe(rescompData => { 
        this.loader =false;
        this.creportdata = rescompData.data = undefined ? null : rescompData.data;
         if(this.creportdata)
           {
             this.coloumName1=this.creportdata[0];
            for (let key in this.coloumName1) {
                  // console.log(this.coloumName1[key]);
                    if(key != "userid")
                    {
                    var id =key;
                     var field =key;

                   
                       var name =key.toUpperCase();
                     
                      
                   this.getData ={
                         id:id,
                       name:name,
                        field:field,
                        //filterParams: { newRowsAction: "keep" }
                        sortable: true,
                        minWidth: 100,
                        maxWidth:200,
                       type: FieldType.string, 
                       filterable: true,
                      filter: { model: Filters.compoundInput}
                    // this.columnTog.push(key);
                  }
                  this.columnDes.push(this.getData); 
                   for(let i=0;i<this.creportdata.length;i++)
            {
              
                this.creportdata[i].id =i+2;
             
            }
          }
                
                // for(var i=0;i<this.creportdata.length;i++)
                //    { 
                //        for(let key in this.creportdata[i])
                //        {
                //          if(key != "Student")
                //          {
                //            if(this.creportdata[i][key] != 0)
                //            {
                //              this.creportdata[i][key]=this.datePipe.transform(new Date(this.creportdata[i][key] * 1000),'dd-MM-yyyy');
                //               console.log(this.creportdata[i][key],'this.creportdata[i][key]')
                //            }else
                //            {
                //              this.creportdata[i][key]='NA'
                //            }    
                //          }
                //        }
                //    } 
           }
           for(var j=0;j<this.creportdata.length;j++)
                   { 
                       for(let key in this.creportdata[j])
                       {
                         if(key != 'Student')
                         {  
                           var reportdata =this.creportdata[j][key];
                             this.creportdata[j][key]=reportdata +'%';  
                         }
                       }

                   } 

}

          this.columnDefinitions =this.columnDes;
          this.dataset = this.creportdata;
          this.show =true;
        console.log(this.creportdata , 'this.creportdata')
      },
      resUserError => {
        this.loader =false;
        this.errorMsg = resUserError
      });

        
    } 

 
}
