import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeInductionReportComponent } from './employee-induction-report.component';

describe('EmployeeInductionReportComponent', () => {
  let component: EmployeeInductionReportComponent;
  let fixture: ComponentFixture<EmployeeInductionReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeInductionReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeInductionReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
