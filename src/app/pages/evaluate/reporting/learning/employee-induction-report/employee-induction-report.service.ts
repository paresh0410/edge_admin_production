import {Injectable, Inject} from '@angular/core';
//import {BaThemeConfigProvider, layoutPaths} from '../../../../../theme';
import {Http, Response, Headers, RequestOptions, Request} from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import {AppConfig} from '../../../../../app.module';
@Injectable()
export class EmployeeInductionReportService{
    
    private _url:string="/api/dashboard/induction_emp_filter";
    constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private http: Http) {
    }
    
    updateUser(user) {
        let url:any = `${this.config.FINAL_URL}`+this._url;
        // let url:any = `${this.config.DEV_URL}`+this._urlFilter;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        //let body = JSON.stringify(user);
        return this.http.post(url, user, options ).map((res: Response) => res.json());
      }
    
      httpGet(url, data) {
        return new Promise(resolve => {
                 this.http.post(url, data)
                   .map(res => res.json())
                   .subscribe(data => {
                     //this.data = data;
                     resolve(data);
                   },
                   err => {
                     //this.data = err;
                     resolve(err);
                   });
               });
       }
}