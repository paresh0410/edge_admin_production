import {Component, OnInit, OnDestroy} from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators/takeWhile' ;
import { AppService } from '../../../../../app.service';
//import { EmployeeInductionReportService } from './employee-induction-report.service';
import { Router, NavigationStart, Routes, ActivatedRoute } from '@angular/router';

import { Column, GridOption, AngularGridInstance, FieldType, Filters, Formatters, GridStateChange, OperatorType, Statistic } from 'angular-slickgrid';
import { EmployeeInductionReportService } from './employee-induction-report.service';
import { BrandDetailsService } from '../../../../../service/brand-details.service';


interface CardSettings {
  title: string;
  iconClass: string;
  type: string;
}

@Component({
  selector: 'ngx-employee-induction-report',
  templateUrl: './employee-induction-report.component.html',
  styleUrls: ['./employee-induction-report.component.scss']
  // template: `<router-outlet></router-outlet>`,
})
export class EmployeeInductionReportComponent implements OnInit {

  private rowData: any[];
  angularGrid: AngularGridInstance;
  columnDefinitions: Column[];
  columnDefinitions1: Column[];

  gridOptions: GridOption;
  dataset: any[];
  statistics: Statistic;
  selectedTitles: any[];
  employeeInductionData:any[];
  selectedTitle: any;
  gridObj: any;
  users:any=[];
  show:boolean=false;
  forceFitColumns=true;
  loader:boolean;
   contdata:any;
  filterData:any;
  currentBrandData: any;

  ngOnInit(): void {
    this.currentBrandData = this.brandService.getCurrentBrandData();
    //this.prepareGrid();
    this.gridOptions = {
      enableAutoResize: true,       // true by default
      enableCellNavigation: true,
      enableExcelCopyBuffer: true,
      enableFiltering: true,
      enableColumnReorder:false,
      rowSelectionOptions: {
        // True (Single Selection), False (Multiple Selections)
        selectActiveRow: false
      },
      // preselectedRows: [0, 2],
      enableCheckboxSelector: true,
      enableRowSelection: true,
      leaveSpaceForNewRows : true
    };
  }

  constructor(private themeService: NbThemeService, private router:Router,private service :EmployeeInductionReportService,
    public brandService: BrandDetailsService,
    private AppService:AppService) {
    // this.dataset = this.prepareData();
       this.contdata =this.AppService.getuserdata();
    this.loader =true;
    this.filterData={
    code:null,
    codeRange:{
      to:null,
      from:null
    },
    name:null,
    invited:null,
    attendance:null,
    joined:null,
    doj:null,
    dojRange:{
      to:null,
      from:null
    },
    inductionLocation:null,
    designation:null,
    department:null,
    grade:null,
    contactDetails:null,
    trainingDate:null,
    trainingDateRange:{
      to:null,
      from:null
    },
    remarks:null,
    userId:this.contdata.userId,
    roleId:this.contdata.roleId,

  };
    this.prepareGrid();

  }

  prepareGrid(){
    this.service.updateUser(this.filterData).subscribe(
      data => {
        // this.loader =false;
        //this.users=data.data[1];
        var date = new Date();
         this.users = data.data[1];
         console.log('users Data', this.users);
         console.log('1st time ==>',date);
        // this.clearFilterData();
          this.rowData = this.users;
         // console.log("Data ",data);
         if(this.users.length>0){
          for(let i=0;i<this.users.length;i++){
            this.users[i].id=i+1;
          }
       }
        //this.dataset=[];
         this.loader =false;
        this.show=true;
        this.forceFitColumns=false;
      console.log('Users Data with id ',this.users);
      var date1 = new Date();
      console.log('2nd time ==>',date1);
       this.dataset=this.users;
       
       //console.log("Slick Grid Dataset "+this.dataset);
       
      },
      error => {
        this.loader =false;
        console.error("Error !");
        //return Observable.throw(error);
      }
   ); 
   //console.log("Users Data after calling service"+this.users);
    this.columnDefinitions = [
      { id: 'code', name: this.currentBrandData.employee.toUpperCase()+' ID / USERNAME', field: 'code', sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'name', name: this.currentBrandData.employee.toUpperCase()+' NAME', field: 'name', sortable: true,  minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
       { id: 'email', name: this.currentBrandData.employee.toUpperCase()+' EMAIL', field: 'email', 
       sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'band', name: this.currentBrandData.employee.toUpperCase()+' BAND', field: 'band', 
       sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'reportingmanagername', name:'RM NAME', field: 'reportingmanagername',
      sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'reportingmanagercode', name:'RM CODE', field: 'reportingmanagercode', 
      sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'department', name: 'DEPARTMENT', field: 'department',
      sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'logins', name: 'NO. OF LOGINS', field: 'logins',
      sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'ljclicks', name: 'CLICKS ON LEARNING JOURNEY', field: 'ljclicks', 
        sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'invited', name: 'INVITED', field: 'invited', sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'joined', name: 'JOINED', field: 'joined', sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'attendance', name: 'ATTENDANCE', field: 'attendance',  sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      //     { id: 'attendance', name: 'ATTENDANCE', field: 'attendance', formatter: Formatters.dateIso, sortable: true,
      //   minWidth: 100,maxWidth:200, 
      //   exportWithFormatter: true,
      //   type: FieldType.date, filterable: true, filter: { model: Filters.compoundDate } 
      // },
      // { id: 'logins', name: 'NO. OF LOGINS', field: 'logins', minWidth: 85, maxWidth: 85,
      //   type: FieldType.string,
      //   sortable: true,
      //   filterable: true,
      // },
      // { id: 'ljclicks', name: 'Clicks On Learning Journey', field: 'ljclicks', minWidth: 85, maxWidth: 85,
      //   type: FieldType.string,
      //   sortable: true,
      //   filterable: true,
      // },
      { id: 'inductionLocation', name: 'INDUCTION LOCATION', field: 'inductionLocation', 
      sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'designation', name: 'DESIGNATION', field: 'designation', 
      sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      // { id: 'department', name: 'Department', field: 'department', minWidth: 85, maxWidth: 85,
      // type: FieldType.string,
      // sortable: true,
      // filterable: true,
      // },
      { id: 'grade', name: 'GRADE', field: 'grade', 
       sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'contactDetails', name: 'CONTACT DETAILS', field: 'contactDetails', 
       sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'trainingDate', name: 'TRAINING DATE', field: 'trainingDate', 
      sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundDate } 
      },
      { id: 'SkillLevel', name: 'SKILL LEVEL', field: 'SkillLevel', 
       sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      // { id: 'attendance', name: 'ATTENDANCE FLAG', field: 'attendance', 
      // sortable: true, 
      // minWidth: 100,
      //   maxWidth:200,
      //   type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      // },
      { id: 'attinfodata', name: 'ATTENDANCE INFO', field: 'attinfodata', 
      sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'dob', name: 'DATE OF BIRTH', field: 'dob', 
      sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true,filter: { model: Filters.compoundDate } 
      },
      { id: 'doj', name: 'DATE OF JOINING', field: 'doj', 
       sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundDate }  
      },     
      { id: 'business', name: 'BUSINESS GROUP', field: 'business', 
       sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'sdepartment', name: 'SUB DEPARTMENT', field: 'sdepartment', 
      sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'ssdepartment', name: 'SUB SUB DEPARTMENT', field: 'ssdepartment', 
      sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'orgcode', name: 'ORGANISATION CODE', field: 'orgcode', 
      sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'aops', name: 'AOP SEGMENT', field: 'aops',
       sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'aopn', name: 'AOP NAME', field: 'aopn', 
       sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'aopc', name: 'AOP CODE', field: 'aopc', 
       sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'ccn', name: 'COST CENTER NAME', field: 'ccn',
      sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'ccc', name: 'COST CENTER CODE', field: 'ccc', 
      sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'tier', name: 'TIER', field: 'tier', 
      sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'zone', name: 'ZONE', field: 'zone', 
       sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'state', name: 'STATE', field: 'state',
       sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'locationcode', name: 'LOCATION CODE', field: 'locationcode', 
      sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'positioncode', name: 'POSITION CODE', field: 'positioncode', 
      sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      // { id: 'band', name: 'BAND', field: 'band', minWidth: 85, maxWidth: 85,
      // type: FieldType.string,
      // sortable: true,
      // filterable: true,
      // },
      { id: 'level', name: 'LEVEL', field: 'level', 
      sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'jobcode', name:'JOB CODE', field: 'jobcode', 
      sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'empclassification', name:this.currentBrandData.employee.toUpperCase()+' CLASSIFICATION', field: 'empclassification', 
       sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'gender', name:'GENDER', field: 'gender', 
   sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'father', name:'FATHER NAME', field: 'father', 
      sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'presentaddress', name:'PRESENT ADDRESS', field: 'presentaddress', 
       sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'presentcity', name:'PRESENT CITY', field: 'presentcity',
      sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'presentstate', name:'PRESENT STATE', field: 'presentstate', 
       sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'presentpincode', name:'PRESENT ADDRESS PINCODE', field: 'presentpincode', 
       sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'permanentadd', name:'PERMANENT ADDRESS', field: 'permanentadd', 
      sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'permanentcity', name:'PERMANENT ADDRESS CITY', field: 'permanentcity', 
       sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'permanentstate', name:'PERMANENT STATE', field: 'permanentstate', 
     sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'personalmobile', name:'PERS,ONAL MOBILE NUMBER', field: 'personalmobile', 
      sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'personalemail', name:'PERSONAL EMAIL', field: 'personalemail', 
      sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'martialstatus', name:'MARTIAL STATUS', field: 'martialstatus', 
      sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'currenttenureyear', name:'CURRENT TENURE YEAR', field: 'currenttenureyear', 
       sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'currenttenuremonth', name:'CURRENT TENURE IN MONTH', field: 'currenttenuremonth', 
       sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'age', name:'AGE', field: 'age',
       sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'employeestatus', name:this.currentBrandData.employee.toUpperCase()+' STATUS', field: 'employeestatus', 
       sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      // { id: 'reportingmanagercode', name:'REPORTING MANAGER CODE', field: 'reportingmanagercode', minWidth: 85, maxWidth: 85,
      // type: FieldType.string,
      // sortable: true,
      // filterable: true,
      // },
      // { id: 'reportingmanagername', name:'REPORTING MANAGER NAME', field: 'reportingmanagername', minWidth: 85, maxWidth: 85,
      // type: FieldType.string,
      // sortable: true,
      // filterable: true,
      // },
      { id: 'skiplevelmcode', name:'SKIP LEVEL MANAGER CODE', field: 'skiplevelmcode', 
       sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'skiplevelmname', name:'SKIP LEVEL MANAGER NAME', field: 'skiplevelmname', 
      sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'bloodgroup', name:'BLOOD GROUP', field: 'bloodgroup', 
      sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'spousename', name:'SPOUSE NAME', field: 'spousename', 
       sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'rmemail', name:'REPORTING MANAGER EMAIL', field: 'rmemail', 
      sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'trainingvenue', name:'TRAINING VENUE', field: 'trainingvenue', 
      sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      }
    ];
    
    // fill the dataset with your data
    // VERY IMPORTANT, Angular-Slickgrid uses Slickgrid DataView which REQUIRES a unique "id" and it has to be lowercase "id" and be part of the dataset
    // this.dataset = [];
    //this.service.httpGet
   // this.dataset = this.rowData;
    
  }
  submitform(){
      // $("#tabsNavigation").collapse("hide");
      }



  

  angularGridReady(angularGrid: any) {
    this.columnDefinitions1=[
  { id: 'code', name: this.currentBrandData.employee.toUpperCase()+' ID / USERNAME', field: 'code', sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'name', name: this.currentBrandData.employee.toUpperCase()+' NAME', field: 'name', sortable: true,  minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
        { id: 'email', name: this.currentBrandData.employee.toUpperCase()+' EMAIL', field: 'email', 
       sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'band', name: this.currentBrandData.employee.toUpperCase()+' BAND', field: 'band', 
       sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'reportingmanagername', name:'RM NAME', field: 'reportingmanagername',
      sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'reportingmanagercode', name:'RM CODE', field: 'reportingmanagercode', 
      sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'department', name: 'DEPARTMENT', field: 'department',
      sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'logins', name: 'NO. OF LOGINS', field: 'logins',
      sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'ljclicks', name: 'CLICKS ON LEARNING JOURNEY', field: 'ljclicks', 
        sortable: true, 
      minWidth: 100,
        maxWidth:200,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      ];
    this.angularGrid = angularGrid;
    angularGrid.slickGrid.setColumns(this.columnDefinitions1);
    this.gridObj = angularGrid && angularGrid.slickGrid || {};
  }

  handleSelectedRowsChanged(e, args) {
    if (Array.isArray(args.rows)) {
      this.selectedTitles = args.rows.map(idx => {
        const item = this.gridObj.getDataItem(idx);
        return item.title || '';
      });
    }
  }

  /** Dispatched event of a Grid State Changed event */
  gridStateChanged(gridState: GridStateChange) {
    console.log('Client sample, Grid State changed:: ', gridState);
  }

  /** Save current Filters, Sorters in LocaleStorage or DB */
  saveCurrentGridState(grid) {
    console.log('Client sample, last Grid State:: ', this.angularGrid.gridStateService.getCurrentGridState());
  }
   back(){
      
      this.router.navigate(['pages/evaluate/reporting/learning']);
  }

 
}
