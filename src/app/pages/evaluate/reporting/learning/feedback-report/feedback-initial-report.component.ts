import {Component, OnInit, OnDestroy} from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators/takeWhile' ;
import { FeedbackInitialReportService } from './feedback-initial-report.service';
import { Router, NavigationStart, Routes, ActivatedRoute } from '@angular/router';
import { FeedbackReportService } from './feedback-report.service';

import { Column, GridOption, AngularGridInstance, FieldType, Filters, Formatters, GridStateChange, OperatorType, Statistic } from 'angular-slickgrid';

interface CardSettings {
  title: string;
  iconClass: string;
  type: string;
}

@Component({
   selector: 'ngx-feedback-initial-report',
  templateUrl: './feedback-initial-report.component.html',
  styleUrls: ['./feedback-initial-report.component.scss']
  // template: `<router-outlet></router-outlet>`,
})
export class FeedbackInitialReportComponent implements OnInit {

  angularGrid: AngularGridInstance;
  columnDefinitions: Column[];
  gridOptions: GridOption;
  dataset: any[];
  statistics: Statistic;
  selectedTitles: any[];
  selectedTitle: any;
  gridObj: any;
   userInductedData:any[];
  errorMsg:any;
  formdata:any ={
            fid:6,
          };
  getData:any={};
  coloumName1:any={};
  creportdata:any=[];
  columnDes:any=[];
  columnTog:any=[];
  feedbacklistdrop:any=[];
  users:any=[];
   //errorMsg: string;
  loader :any;
  show:boolean=false;


  ngOnInit(): void {
      this.gridOptions = {
      enableAutoResize: true,       // true by default
      enableCellNavigation: true,
      enableExcelCopyBuffer: true,
      enableFiltering: true,
      enableColumnReorder:false,
      rowSelectionOptions: {
        // True (Single Selection), False (Multiple Selections)
        selectActiveRow: false
      },
      // preselectedRows: [0, 2],
      enableCheckboxSelector: true,
      enableRowSelection: true,
    };
  }

  constructor(private themeService: NbThemeService,private routes:ActivatedRoute,private router:Router,private feedbackreportService:FeedbackInitialReportService,private passservice:FeedbackReportService) {
    // this.dataset = this.prepareData();
       this.loader =true;
      this.feedbackreportService.getFeedbackDrop()
      .subscribe(rescompData => { 
       // this.loader =false;
        this.feedbacklistdrop = rescompData.data[0];
        console.log(this.feedbacklistdrop , 'this.feedbacklistdrop')
         if(this.feedbacklistdrop)
           {
             this.coloumName1=this.feedbacklistdrop[0];
            for (let key in this.coloumName1) {

                       if(key != "feedbackId")
                    {

                    var id =key;
                     var field =key;
                       var name =key.toUpperCase();
                      
                   this.getData ={
                         id:id,
                       name:name,
                        field:field,
                        //filterParams: { newRowsAction: "keep" }
                        sortable: true,
                        // minWidth: 100,
                        // maxWidth:200,
                       type: FieldType.string, 
                       filterable: true,
                      filter: { model: Filters.compoundInput}
                    // this.columnTog.push(key);
                  }
                  this.columnDes.push(this.getData); 
                }
                 
                  
                }
                for(var i=0;i<this.feedbacklistdrop.length;i++)
                   {
                          this.feedbacklistdrop[i].id =i+1;       
                   } 
           }

            this.columnDefinitions =this.columnDes;
          // this.columnDefs =this.columnDes.reverse();
          this.dataset = this.feedbacklistdrop;
        console.log(this.feedbacklistdrop , 'this.feedbacklistdrop')
         this.loader =false;
        this.show =true;
      },
      resUserError => {
        this.loader =false;
        this.errorMsg = resUserError
      });

      // var formdata1={qid:14}
      // this.feedbackreportService.getcoursecompletion(this.formdata)
      // .subscribe(rescompData => { 
      //   this.loader =false;
      //   this.creportdata = rescompData.data;
      //    if(this.creportdata)
      //      {
      //        this.coloumName1=this.creportdata[0];
      //       for (let key in this.coloumName1) {

      //                  if(key != "userid")
      //               {
      //               var id =key;
      //                var field =key;
      //                  var name ='DAY'+key.toUpperCase();
                      
      //              this.getData ={
      //                    id:id,
      //                  name:name,
      //                   field:field,
      //                   //filterParams: { newRowsAction: "keep" }
      //                   sortable: true,
      //                   minWidth: 100,
      //                   maxWidth:200,
      //                  type: FieldType.string, 
      //                  filterable: true,
      //                 filter: { model: Filters.compoundInput}
      //               // this.columnTog.push(key);
      //             }
      //             this.columnDes.push(this.getData); 
      //           }
                 
                  
      //           }
      //           for(var i=0;i<this.creportdata.length;i++)
      //              {
      //                      if(this.creportdata[i].User == 'ANONYMOUS')
      //                      {
      //                        this.creportdata.splice(i,1)
      //                       }
      //                     this.creportdata[i].id =i+1;       
      //              } 
      //      }

      //       this.columnDefinitions =this.columnDes;
      //     // this.columnDefs =this.columnDes.reverse();
      //     this.dataset = this.creportdata;
      //   console.log(this.creportdata , 'this.creportdata')
      //   this.show =true;
      // },
      // resUserError => {
      //   this.loader =false;
      //   this.errorMsg = resUserError
      // });
        
  }

  prepareGrid(){
    this.columnDefinitions = [
     
      // { id: 'description', name: 'Description', field: 'description', filterable: true, sortable: true, minWidth: 80,
      //   type: FieldType.string,
      //   filter: {
      //     model: new CustomInputFilter() // create a new instance to make each Filter independent from each other
      //   }
      // },
      { id: 'id', name: 'id', field: 'id', sortable: true, minWidth: 55,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'name', name: 'name', field: 'name', sortable: true, minWidth: 55,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      }
    ];
    this.gridOptions = {
      enableAutoResize: true,       // true by default
      enableCellNavigation: true,
      enableExcelCopyBuffer: true,
      enableFiltering: true,
      enableColumnReorder:false,
      rowSelectionOptions: {
        // True (Single Selection), False (Multiple Selections)
        selectActiveRow: false
      },
      // preselectedRows: [0, 2],
      enableCheckboxSelector: true,
      enableRowSelection: true,
    };

    // fill the dataset with your data
    // VERY IMPORTANT, Angular-Slickgrid uses Slickgrid DataView which REQUIRES a unique "id" and it has to be lowercase "id" and be part of the dataset
    // this.dataset = [];

    this.dataset = this.prepareData();
    
  }

  prepareData() {
    // mock a dataset
    const mockDataset = [];
    // for demo purpose, let's mock a 1000 lines of data
    for (let i = 0; i < 1000; i++) {
      const randomYear = 2000 + Math.floor(Math.random() * 10);
      const randomMonth = Math.floor(Math.random() * 11);
      const randomDay = Math.floor((Math.random() * 28));
      const randomPercent = Math.round(Math.random() * 100);

      mockDataset[i] = {
        id: i, // again VERY IMPORTANT to fill the "id" with unique values
        title: 'Task ' + i,
        duration: Math.round(Math.random() * 100) + '',
        percentComplete: randomPercent,
        start: `${randomMonth}/${randomDay}/${randomYear}`,
        finish: `${randomMonth}/${randomDay}/${randomYear}`,
        effortDriven: (i % 5 === 0)
      };
    }
    return mockDataset;
  }

  angularGridReady(angularGrid: any) {
    this.angularGrid = angularGrid;
    this.gridObj = angularGrid && angularGrid.slickGrid || {};
  }
   gotofeedbackcompletionreport(item){
   var data1=
   {
     name:item.name,
     feedbackId:item.feedbackId,
     workflowname:item.workflowname,
     coursename:item.coursename

   }
   this.passservice.data =data1;
    this.router.navigate(['feedback-report'],{relativeTo:this.routes});
  }

  handleSelectedRowsChanged(e, args) {
    if (Array.isArray(args.rows)) {
      this.selectedTitles = args.rows.map(idx => {
        const item = this.gridObj.getDataItem(idx);
         this.gotofeedbackcompletionreport(item);
        return item.title || '';
      });
    }
  }

  /** Dispatched event of a Grid State Changed event */
  gridStateChanged(gridState: GridStateChange) {
    console.log('Client sample, Grid State changed:: ', gridState);
  }

  /** Save current Filters, Sorters in LocaleStorage or DB */
  saveCurrentGridState(grid) {
    console.log('Client sample, last Grid State:: ', this.angularGrid.gridStateService.getCurrentGridState());
  }


   colclear(angularGrid)
  {
    this.columnDefinitions =[];
    //this.columnDefinitions1 =[];
    this.columnDes=[];
   // this.statcoldata=[];

    //this.angularGrid.slickGrid.setColumns(this.columnDefinitions1);
    //angularGrid.gridStateService.controlAndPluginService.columnPickerControl.destroy()
    //angularGrid.gridStateService.resetColumns(this.columnDefinitions1=[])
     // this.gridOptions = {
     //    enableAutoResize: true,       // true by default
     //    enableCellNavigation: true,
     //    enableExcelCopyBuffer: true,
     //    enableFiltering: true,
     //    rowSelectionOptions: {
     //      // True (Single Selection), False (Multiple Selections)
     //      selectActiveRow: false
     //    },
     //    // preselectedRows: [0, 2],
     //    enableCheckboxSelector: true,
     //    enableRowSelection: true,
     //  };
     // this.saveCurrentGridState(this.grid);
  }

  callType(id)
    {
      //this.loader=true;
      //  this.angularGrid.dataView.refresh();
      // this.colclear(this.angularGrid);
     var calldata={fid:id};
      //  this.columnDefinitions.splice(1);
      //  console.log('this.columnDefinitions',this.columnDefinitions)
      // // this.columnDes=[];
       this.feedbackreportService.getcoursecompletion(this.formdata)
      .subscribe(rescompData => { 
        this.loader =false;
        this.creportdata = rescompData.data;
         if(this.creportdata)
           {
             this.coloumName1=this.creportdata[0];
            for (let key in this.coloumName1) {

                       if(key != "userid")
                    {
                    var id =key;
                     var field =key;
                       var name =key.toUpperCase();
                      
                   this.getData ={
                         id:id,
                       name:name,
                        field:field,
                        //filterParams: { newRowsAction: "keep" }
                        sortable: true,
                        minWidth: 100,
                        maxWidth:200,
                       type: FieldType.string, 
                       filterable: true,
                      filter: { model: Filters.compoundInput}
                    // this.columnTog.push(key);
                  }
                  this.columnDes.push(this.getData); 
                }
                 
                  
                }
                for(var i=0;i<this.creportdata.length;i++)
                   {
                           if(this.creportdata[i].User == 'ANONYMOUS')
                           {
                             this.creportdata.splice(i,1)
                            }
                          this.creportdata[i].id =i+1;       
                   } 
           }

            this.columnDefinitions =this.columnDes;
          // this.columnDefs =this.columnDes.reverse();
          this.dataset = this.creportdata;
        console.log(this.creportdata , 'this.creportdata')
        this.show =true;
      },
      resUserError => {
        this.loader =false;
        this.errorMsg = resUserError
      });
        
    } 

  back(){
      
      this.router.navigate(['pages/evaluate/reporting/learning']);
  }
}

