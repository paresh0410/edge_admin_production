import {Injectable, Inject} from '@angular/core';
import {Http, Response, Headers, RequestOptions, Request} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppConfig} from '../../../../../app.module';

@Injectable()
export class FeedbackInitialReportService {

  private _urldrop:string = "/api/dashboard/feedbacklistdrop" // Dev
  private _url1:string = "/api/dashboard/get_FeedbackCompletionReport"
  request: Request;
  public data: any;

  constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private http: Http){

  }

   getFeedbackDrop(){
    let url:any = `${this.config.FINAL_URL}`+this._urldrop;
    return this.http.post(url,{})
      .map((response:Response) => response.json())
      .catch(this._errorHandler);
  }
  getcoursecompletion(user){
    let url:any = `${this.config.FINAL_URL}`+this._url1;
    return this.http.post(url,user)
      .map((response:Response) => response.json())
      .catch(this._errorHandler);
  }
    _errorHandler(error: Response){
    console.error(error);
    return Observable.throw(error || "Server Error")
  }

// public data :any;
  
}
