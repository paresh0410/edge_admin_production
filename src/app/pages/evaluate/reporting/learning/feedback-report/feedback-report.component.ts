import {Component, OnInit, OnDestroy} from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { AppService } from '../../../../../app.service';
import { takeWhile } from 'rxjs/operators/takeWhile' ;
import { FeedbackReportService } from './feedback-report.service';
import { Router, NavigationStart, Routes, ActivatedRoute } from '@angular/router';

import { Column, GridOption, AngularGridInstance, FieldType, Filters, Formatters, GridStateChange, OperatorType, Statistic } from 'angular-slickgrid';

interface CardSettings {
  title: string;
  iconClass: string;
  type: string;
}

@Component({
   selector: 'ngx-feedback-report',
  templateUrl: './feedback-report.component.html',
  styleUrls: ['./feedback-report.component.scss']
  // template: `<router-outlet></router-outlet>`,
})
export class FeedbackReportComponent implements OnInit {

  angularGrid: AngularGridInstance;
  columnDefinitions: Column[];
    columnDefinitions1: Column[];
  gridOptions: GridOption;
  dataset: any[];
  statistics: Statistic;
  selectedTitles: any[];
  selectedTitle: any;
  gridObj: any;
   userInductedData:any[];
  errorMsg:any;
  formdata:any;
   getData:any={};
  coloumName1:any={};
  creportdata:any=[];
   coloumName2:any={};
  columnDes:any=[];
  columnTog:any=[];
  feedbacklistdrop:any=[];
  users:any=[];
   //errorMsg: string;
  loader :any;
  show:boolean=false;
  gridShow:boolean=false;
   name:any;
 workflowname:any;  
 coursename:any;
 statcoldata:any=[];
    contdata:any;


  ngOnInit(): void {
      this.gridOptions = {
      enableAutoResize: true,       // true by default
      enableCellNavigation: true,
      enableExcelCopyBuffer: true,
      enableFiltering: true,
      rowSelectionOptions: {
        // True (Single Selection), False (Multiple Selections)
        selectActiveRow: false
      },
      // preselectedRows: [0, 2],
      enableCheckboxSelector: true,
      enableRowSelection: true,
    };
  }

  constructor(private themeService: NbThemeService, private router:Router,private feedbackreportService:FeedbackReportService,private AppService:AppService) {
    // this.dataset = this.prepareData();
          this.contdata =this.AppService.getuserdata();
      this.loader=true;
      // this.feedbackreportService.getFeedbackDrop()
      // .subscribe(rescompData => { 
      //  // this.loader =false;
      //   this.feedbacklistdrop = rescompData.data[0];
      //   console.log(this.feedbacklistdrop , 'this.feedbacklistdrop')
      // },
      // resUserError => {
      //   this.loader =false;
      //   this.errorMsg = resUserError
      // });
        var data =this.feedbackreportService.data;

     if(data)
     {
        
         console.log('data',data) ;
         this.formdata={
          fid:data.feedbackId,
          userId:this.contdata.userId,
          roleId:this.contdata.roleId,
    
        }
         this.name =data.name;
           this.workflowname=data.workflowname;
           this.coursename=data.coursename;
      // var formdata1={qid:14}
      this.feedbackreportService.getcoursecompletion(this.formdata)
      .subscribe(rescompData => { 
        this.loader =false;
      this.statcoldata= rescompData.data1=undefined ? null : rescompData.data1;
        this.creportdata = rescompData.data = undefined ? null : rescompData.data;
          if(this.statcoldata)
        {
             this.gridShow =true;
        }else{
              this.gridShow =false;
        }
         if(this.creportdata)
           {
              var filterav;
             this.coloumName1=this.creportdata[0];
            for (let key in this.coloumName1) {

                       if(key != "userid")
                    {
                          if( key != 'fdid')
                      {
                         if(key == "date of birth" || key == "date of joining")
                            {
                               filterav =  { model: Filters.compoundDate } 
                            }
                            else
                            {
                               filterav =  { model: Filters.compoundInput}
                            }
                    var id =key;
                     var field =key;
                       var name =key.toUpperCase();
                      
                   this.getData ={
                         id:id,
                       name:name,
                        field:field,
                        //filterParams: { newRowsAction: "keep" }
                        sortable: true,
                        minWidth: 100,
                        maxWidth:200,
                       type: FieldType.string, 
                       filterable: true,
                      filter: filterav
                    // this.columnTog.push(key);
                  }
                  this.columnDes.push(this.getData); 
                }
                 }
                  
                }
                for(var i=0;i<this.creportdata.length;i++)
                   {
                           // if(this.creportdata[i].User == 'ANONYMOUS')
                           // {
                           //   this.creportdata.splice(i,1)
                           //  }
                          this.creportdata[i].id =i+1;       
                   } 
           }

            this.columnDefinitions =this.columnDes;
          // this.columnDefs =this.columnDes.reverse();
          this.dataset = this.creportdata;
        console.log(this.creportdata , 'this.creportdata')
        this.show =true;
      },
      resUserError => {
        this.loader =false;
        this.errorMsg = resUserError
      });
    }
        
  }

  prepareGrid(){
    this.columnDefinitions = [
     
      // { id: 'description', name: 'Description', field: 'description', filterable: true, sortable: true, minWidth: 80,
      //   type: FieldType.string,
      //   filter: {
      //     model: new CustomInputFilter() // create a new instance to make each Filter independent from each other
      //   }
      // },
      { id: 'id', name: 'id', field: 'id', sortable: true, minWidth: 55,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'name', name: 'name', field: 'name', sortable: true, minWidth: 55,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      }
    ];
    this.gridOptions = {
      enableAutoResize: true,       // true by default
      enableCellNavigation: true,
      enableExcelCopyBuffer: true,
      enableFiltering: true,
      enableColumnReorder:false,
      rowSelectionOptions: {
        // True (Single Selection), False (Multiple Selections)
        selectActiveRow: false
      },
      // preselectedRows: [0, 2],
      enableCheckboxSelector: true,
      enableRowSelection: true,
    };

    // fill the dataset with your data
    // VERY IMPORTANT, Angular-Slickgrid uses Slickgrid DataView which REQUIRES a unique "id" and it has to be lowercase "id" and be part of the dataset
    // this.dataset = [];

    this.dataset = this.prepareData();
    
  }

  prepareData() {
    // mock a dataset
    const mockDataset = [];
    // for demo purpose, let's mock a 1000 lines of data
    for (let i = 0; i < 1000; i++) {
      const randomYear = 2000 + Math.floor(Math.random() * 10);
      const randomMonth = Math.floor(Math.random() * 11);
      const randomDay = Math.floor((Math.random() * 28));
      const randomPercent = Math.round(Math.random() * 100);

      mockDataset[i] = {
        id: i, // again VERY IMPORTANT to fill the "id" with unique values
        title: 'Task ' + i,
        duration: Math.round(Math.random() * 100) + '',
        percentComplete: randomPercent,
        start: `${randomMonth}/${randomDay}/${randomYear}`,
        finish: `${randomMonth}/${randomDay}/${randomYear}`,
        effortDriven: (i % 5 === 0)
      };
    }
    return mockDataset;
  }

  // angularGridReady(angularGrid: any) {
  //   this.angularGrid = angularGrid;
  //   this.gridObj = angularGrid && angularGrid.slickGrid || {};
  // }
   angularGridReady(angularGrid: any) {
          this.columnDefinitions1=[
       {
        field: "Employee ID",
        filterable: true,
        id: "Employee ID",
        maxWidth: 200,
        minWidth: 100,
        name: "EMPLOYEE ID",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
    {
        field: "Employee Name",
        filterable: true,
        id: "Employee Name",
        maxWidth: 200,
        minWidth: 100,
        name: "EMPLOYEE NAME",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
   
    {
        field: "Employee band",
        filterable: true,
        id: "Employee band",
        maxWidth: 200,
        minWidth: 100,
        name: "EMPLOYEE BAND",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
    {
        field: "RM code",
        filterable: true,
        id: "RM code",
        maxWidth: 200,
        minWidth: 100,
        name: "RM CODE",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
    {
        field: "RM name",
        filterable: true,
        id: "RM name",
        maxWidth: 200,
        minWidth: 100,
        name: "RM NAME",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
    {
        field: "department",
        filterable: true,
        id: "department",
        maxWidth: 200,
        minWidth: 100,
        name: "DEPARTMENT",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
    ]
    // this.columnDeflat =this.columnDefinitions1
      if(this.statcoldata)
           {
             this.coloumName2=this.statcoldata[0];
            for (let key in this.coloumName2) {
                  // console.log(this.coloumName2[key]);
                    if(key != "id")
                    {
                          if( key != 'fdid')
                      {
                    var id =key;
                     var field =key;
                     var name =key.toUpperCase();
                      
                   this.getData ={
                         id:id,
                       name:name,
                        field:field,
                        //filterParams: { newRowsAction: "keep" }
                        sortable: true,
                        minWidth: 100,
                        maxWidth:200,
                       type: FieldType.string, 
                       filterable: true,
                      filter: { model: Filters.compoundInput}
                    // this.columnTog.push(key);
                  }
                  this.columnDefinitions1.push(this.getData); 
                }
              }
              }
              
           }
    this.angularGrid = angularGrid;
    angularGrid.slickGrid.setColumns(this.columnDefinitions1);
    this.gridObj = angularGrid && angularGrid.slickGrid || {};
  }

  handleSelectedRowsChanged(e, args) {
    if (Array.isArray(args.rows)) {
      this.selectedTitles = args.rows.map(idx => {
        const item = this.gridObj.getDataItem(idx);
        return item.title || '';
      });
    }
  }

  /** Dispatched event of a Grid State Changed event */
  gridStateChanged(gridState: GridStateChange) {
    console.log('Client sample, Grid State changed:: ', gridState);
  }

  /** Save current Filters, Sorters in LocaleStorage or DB */
  saveCurrentGridState(grid) {
    console.log('Client sample, last Grid State:: ', this.angularGrid.gridStateService.getCurrentGridState());
  }


   colclear(angularGrid)
  {
    this.columnDefinitions =[];
    //this.columnDefinitions1 =[];
    this.columnDes=[];
   // this.statcoldata=[];

    //this.angularGrid.slickGrid.setColumns(this.columnDefinitions1);
    //angularGrid.gridStateService.controlAndPluginService.columnPickerControl.destroy()
    //angularGrid.gridStateService.resetColumns(this.columnDefinitions1=[])
     // this.gridOptions = {
     //    enableAutoResize: true,       // true by default
     //    enableCellNavigation: true,
     //    enableExcelCopyBuffer: true,
     //    enableFiltering: true,
     //    rowSelectionOptions: {
     //      // True (Single Selection), False (Multiple Selections)
     //      selectActiveRow: false
     //    },
     //    // preselectedRows: [0, 2],
     //    enableCheckboxSelector: true,
     //    enableRowSelection: true,
     //  };
     // this.saveCurrentGridState(this.grid);
  }

  // callType(id)
  //   {
  //     //this.loader=true;
  //     //  this.angularGrid.dataView.refresh();
  //     // this.colclear(this.angularGrid);
  //    var calldata={fid:id};
  //     //  this.columnDefinitions.splice(1);
  //     //  console.log('this.columnDefinitions',this.columnDefinitions)
  //     // // this.columnDes=[];
  //      this.feedbackreportService.getcoursecompletion(this.formdata)
  //     .subscribe(rescompData => { 
  //       this.loader =false;
  //       this.creportdata = rescompData.data;
  //        if(this.creportdata)
  //          {
  //            this.coloumName1=this.creportdata[0];
  //           for (let key in this.coloumName1) {

  //                      if(key != "userid")
  //                   {
  //                   var id =key;
  //                    var field =key;
  //                      var name =key.toUpperCase();
                      
  //                  this.getData ={
  //                        id:id,
  //                      name:name,
  //                       field:field,
  //                       //filterParams: { newRowsAction: "keep" }
  //                       sortable: true,
  //                       minWidth: 100,
  //                       maxWidth:200,
  //                      type: FieldType.string, 
  //                      filterable: true,
  //                     filter: { model: Filters.compoundInput}
  //                   // this.columnTog.push(key);
  //                 }
  //                 this.columnDes.push(this.getData); 
  //               }
                 
                  
  //               }
  //               for(var i=0;i<this.creportdata.length;i++)
  //                  {
  //                          if(this.creportdata[i].User == 'ANONYMOUS')
  //                          {
  //                            this.creportdata.splice(i,1)
  //                           }
  //                         this.creportdata[i].id =i+1;       
  //                  } 
  //          }

  //           this.columnDefinitions =this.columnDes;
  //         // this.columnDefs =this.columnDes.reverse();
  //         this.dataset = this.creportdata;
  //       console.log(this.creportdata , 'this.creportdata')
  //       this.show =true;
  //     },
  //     resUserError => {
  //       this.loader =false;
  //       this.errorMsg = resUserError
  //     });
        
  //   } 

  back(){
      
      this.router.navigate(['pages/evaluate/reporting/learning/feedback-initial-report']);
  }
}

