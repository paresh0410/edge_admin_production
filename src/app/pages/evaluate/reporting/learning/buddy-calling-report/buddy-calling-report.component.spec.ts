import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuddyCallingReportComponent } from './buddy-calling-report.component';

describe('BuddyCallingReportComponent', () => {
  let component: BuddyCallingReportComponent;
  let fixture: ComponentFixture<BuddyCallingReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuddyCallingReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuddyCallingReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
