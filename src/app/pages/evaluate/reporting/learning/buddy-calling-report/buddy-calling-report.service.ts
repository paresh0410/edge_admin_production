import {Injectable, Inject} from '@angular/core';
//import {BaThemeConfigProvider, layoutPaths} from '../../../../../theme';
import {Http, Response, Headers, RequestOptions, Request} from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import {AppConfig} from '../../../../../app.module';


@Injectable()
export class buddycallingreportService {

   // private _urlLearn:string = "/api/dashboard/course_completion_analytics"
  // private _urlFilter:string = "http://52.33.226.70:9852/api/dashboard/induction_emp_filter" // Dev
  private _url:string = "/api/dashboard/get_CallingBuddyReport" // Dev
  // private _url1:string = "/api/dashboard/get_QuizCompletionReport"

  constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private http: Http) {
  }

  // login(formData:auth):Observable<any>{
  //   return this.http.get(
  //     `${this.config.BASE_URL}/api/dashboard/course_completion_analytics
  //   ?UserName=`+formData.UserName+`&Password=`+formData.Password+``)
  // }
  getbuddyData(userdata){
    let url:any = `${this.config.FINAL_URL}`+this._url;
    return this.http.post(url,userdata)
      .map((response:Response) => response.json())
      .catch(this._errorHandler);
  }
  // getcoursecompletion(user){
  //   let url:any = `${this.config.FINAL_URL}`+this._url1;
  //   return this.http.post(url,user)
  //     .map((response:Response) => response.json())
  //     .catch(this._errorHandler);
  // }
    _errorHandler(error: Response){
    console.error(error);
    return Observable.throw(error || "Server Error")
  }

  // updateUser(user) {
  //   let url:any = `${this.config.FINAL_URL}`+this._urldrop;
  //   // let url:any = `${this.config.DEV_URL}`+this._urlFilter;
  //   let headers = new Headers({ 'Content-Type': 'application/json' });
  //   let options = new RequestOptions({ headers: headers });
  //   //let body = JSON.stringify(user);
  //   return this.http.post(url, user, options ).map((res: Response) => res.json());
  // }

  // httpGet(url, data) {
  //   return new Promise(resolve => {
  //            this.http.post(url, data)
  //              .map(res => res.json())
  //              .subscribe(data => {
  //                //this.data = data;
  //                resolve(data);
  //              },
  //              err => {
  //                //this.data = err;
  //                resolve(err);
  //              });
  //          });
  //  }
   
}