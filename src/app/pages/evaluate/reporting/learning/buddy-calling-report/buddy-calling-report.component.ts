import {Component, OnInit, OnDestroy} from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators/takeWhile' ;
import { AppService } from '../../../../../app.service';
//import { EmployeeInductionReportService } from './employee-induction-report.service';
import { Router, NavigationStart, Routes, ActivatedRoute } from '@angular/router';
import {buddycallingreportService} from './buddy-calling-report.service'
import { Column, GridOption, AngularGridInstance, FieldType, Filters, Formatters, GridStateChange, OperatorType, Statistic } from 'angular-slickgrid';
import { BrandDetailsService } from '../../../../../service/brand-details.service';

interface CardSettings {
  title: string;
  iconClass: string;
  type: string;
}

@Component({
  selector: 'ngx-buddy-calling-report',
  templateUrl: './buddy-calling-report.component.html',
  styleUrls: ['./buddy-calling-report.component.scss']
  // template: `<router-outlet></router-outlet>`,
})
export class BuddyCallingReportComponent implements OnInit {

  angularGrid: AngularGridInstance;
  columnDefinitions: Column[];
 columnDefinitions1: Column[];
  gridOptions: GridOption;
  dataset: any[];
  statistics: Statistic;
  selectedTitles: any[];
  selectedTitle: any;
  gridObj: any;
  errorMsg:any;
getData:any;
coloumName1:any;
columnDes:any=[];
userInductedData:any=[];
show:boolean=false;
loader:boolean;
  currentBrandData: any;


  ngOnInit(): void {
    
    this.currentBrandData = this.brandService.getCurrentBrandData();
       // this.dataset = this.prepareData();
        this.gridOptions = {
        enableAutoResize: true,       // true by default
        enableCellNavigation: true,
        enableExcelCopyBuffer: true,
        enableFiltering: true,
         enableColumnReorder:false,
          rowSelectionOptions: {
          // True (Single Selection), False (Multiple Selections)
          selectActiveRow: false
        },
        // preselectedRows: [0, 2],
        enableCheckboxSelector: true,
        enableRowSelection: true,
      };
   }
   contdata:any;
   formdata:any;

  constructor(private themeService: NbThemeService, private router:Router,private service :buddycallingreportService,
    public brandService: BrandDetailsService,
    private AppService:AppService) {
       this.contdata =this.AppService.getuserdata();
         console.log('contdata',this.contdata)
        this.prepareGrid();
        this.loader =true;

       }



  prepareGrid(){
    this.formdata={
      userId:this.contdata.userId,
      roleId:this.contdata.roleId,
    }
  
       this.service.getbuddyData(this.formdata)
    // this.service.getInductedData()
      .subscribe(resUserData => { 
        this.loader =false;
        // this.loader =false;
       this.userInductedData = resUserData.data.data;
        //this.dataset = resUserData.data;
        console.log('this.userInductedData',this.userInductedData);
     if(this.userInductedData.length > 0)
           {   var filterav;
             this.coloumName1=this.userInductedData[0];
            for (let key in this.coloumName1) {
                  console.log(this.coloumName1[key]);
                    if(key != "userid")
                    {
                            if(key == "date of birth" || key == "date of joining")
                            {
                              filterav =  { model: Filters.compoundDate } 
                            }
                            else
                            {
                               filterav =  { model: Filters.compoundInput}
                            }
                    var id =key;
                     var field =key;

                     if(key.length <=2)
                     {
                       var name ='DA'+key.toUpperCase();
                     }
                     else{
                       var name =key.toUpperCase();
                     }
                      
                   this.getData ={
                         id:id,
                       name:name,
                        field:field,
                        //filterParams: { newRowsAction: "keep" }
                        sortable: true,
                        minWidth: 100,
                        maxWidth:200,
                       type: FieldType.string, 
                       filterable: true,
                      filter: filterav
                    // this.columnTog.push(key);
                  }
                  this.columnDes.push(this.getData); 
                }
              }
              
           }

         // move(this.columnDes,15,0);
          // move(this.columnDes,17,0);
          //   move(this.columnDes,16,1);
          //    move(this.columnDes,20,2);
          //    move(this.columnDes,20,3);
          //    move(this.columnDes,20,4);
          //    move(this.columnDes,20,5);
           this.columnDefinitions =this.columnDes;
         

            for(let i=0;i<this.userInductedData.length;i++)
            {
              
                this.userInductedData[i].id =i+1;
             
            }
            this.dataset = this.userInductedData;
           
             console.log('this.columnDefinitions', this.columnDefinitions);
           // this.columnDefs =this.columnDes.reverse();
          //this.rowData = this.creportdata;
        //onsole.log(this.dataset , 'this.dataset')
        // this.source.load(this.userInductedData);
        console.log('this.dataset', this.dataset);
      // var grid:any;
      // var dynamicColumns =this.columnDefinitions;
       this.loader =false;
      //   grid.setColumns(dynamicColumns);
      this.show =true;
        
      },
      resUserError => {
        this.loader =false;
        this.errorMsg = resUserError
      });
    
  }

  // prepareData() {
  //   // mock a dataset
  //   const mockDataset = [];
  //   // for demo purpose, let's mock a 1000 lines of data
  //   for (let i = 0; i < 1000; i++) {
  //     const randomYear = 2000 + Math.floor(Math.random() * 10);
  //     const randomMonth = Math.floor(Math.random() * 11);
  //     const randomDay = Math.floor((Math.random() * 28));
  //     const randomPercent = Math.round(Math.random() * 100);

  //     mockDataset[i] = {
  //       id: i, // again VERY IMPORTANT to fill the "id" with unique values
  //       title: 'Task ' + i,
  //       duration: Math.round(Math.random() * 100) + '',
  //       percentComplete: randomPercent,
  //       start: `${randomMonth}/${randomDay}/${randomYear}`,
  //       finish: `${randomMonth}/${randomDay}/${randomYear}`,
  //       effortDriven: (i % 5 === 0)
  //     };
  //   }
  //   return mockDataset;
  // }

  angularGridReady(angularGrid: any) {
    this.columnDefinitions1=[
    {
        field: "Employee ID",
        filterable: true,
        id: "Employee ID",
        maxWidth: 200,
        minWidth: 100,
        name: this.currentBrandData.employee.toUpperCase()+" ID",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
    {
        field: "Employee Name",
        filterable: true,
        id: "Employee Name",
        maxWidth: 200,
        minWidth: 100,
        name: this.currentBrandData.employee.toUpperCase()+" NAME",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
   
    {
        field: "Employee band",
        filterable: true,
        id: "Employee band",
        maxWidth: 200,
        minWidth: 100,
        name: this.currentBrandData.employee.toUpperCase()+" BAND",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
    {
        field: "RM code",
        filterable: true,
        id: "RM code",
        maxWidth: 200,
        minWidth: 100,
        name: "RM CODE",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
    {
        field: "RM name",
        filterable: true,
        id: "RM name",
        maxWidth: 200,
        minWidth: 100,
        name: "RM NAME",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
    {
        field: "department",
        filterable: true,
        id: "department",
        maxWidth: 200,
        minWidth: 100,
        name: "DEPARTMENT",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
    {
        field: "y1",
        filterable: true,
        id: "y1",
        maxWidth: 200,
        minWidth: 100,
        name: "DAY1",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
    {
        field: "y2",
        filterable: true,
        id: "y2",
        maxWidth: 200,
        minWidth: 100,
        name: "DAY2",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
    {
        field: "y3",
        filterable: true,
        id: "y3",
        maxWidth: 200,
        minWidth: 100,
        name: "DAY3",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
    {
        field: "y4",
        filterable: true,
        id: "y4",
        maxWidth: 200,
        minWidth: 100,
        name: "DAY4",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
    {
        field: "y5",
        filterable: true,
        id: "y5",
        maxWidth: 200,
        minWidth: 100,
        name: "DAY5",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
    {
        field: "y6",
        filterable: true,
        id: "y6",
        maxWidth: 200,
        minWidth: 100,
        name: "DAY6",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
    {
        field: "y7",
        filterable: true,
        id: "y7",
        maxWidth: 200,
        minWidth: 100,
        name: "DAY7",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
    {
        field: "y8",
        filterable: true,
        id: "y8",
        maxWidth: 200,
        minWidth: 100,
        name: "DAY8",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
    {
        field: "y9",
        filterable: true,
        id: "y9",
        maxWidth: 200,
        minWidth: 100,
        name: "DAY9",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
    {
        field: "y10",
        filterable: true,
        id: "y10",
        maxWidth: 200,
        minWidth: 100,
        name: "DAY10",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
    {
        field: "y11",
        filterable: true,
        id: "y11",
        maxWidth: 200,
        minWidth: 100,
        name: "DAY11",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
    {
        field: "y12",
        filterable: true,
        id: "y12",
        maxWidth: 200,
        minWidth: 100,
        name: "DAY12",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
    {
        field: "y13",
        filterable: true,
        id: "y13",
        maxWidth: 200,
        minWidth: 100,
        name: "DAY13",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
     {
        field: "y14",
        filterable: true,
        id: "y14",
        maxWidth: 200,
        minWidth: 100,
        name: "DAY14",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
     {
        field: "y15",
        filterable: true,
        id: "y15",
        maxWidth: 200,
        minWidth: 100,
        name: "DAY15",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
    ]
    this.angularGrid = angularGrid;
     angularGrid.slickGrid.setColumns(this.columnDefinitions1);
    this.gridObj = angularGrid && angularGrid.slickGrid || {};
  }

  handleSelectedRowsChanged(e, args) {
    if (Array.isArray(args.rows)) {
      this.selectedTitles = args.rows.map(idx => {
        const item = this.gridObj.getDataItem(idx);
        return item.title || '';
      });
    }
  }

  /** Dispatched event of a Grid State Changed event */
  gridStateChanged(gridState: GridStateChange) {
    console.log('Client sample, Grid State changed:: ', gridState);
  }

  /** Save current Filters, Sorters in LocaleStorage or DB */
  saveCurrentGridState(grid) {
    console.log('Client sample, last Grid State:: ', this.angularGrid.gridStateService.getCurrentGridState());
  }
    back(){
      
      this.router.navigate(['pages/evaluate/reporting/learning']);
  }
 
}

