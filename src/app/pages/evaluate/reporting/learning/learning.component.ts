import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'
import { AppService } from '../../../../app.service';

@Component({
  selector: 'ngx-learning',
  templateUrl: './learning.component.html',
  styleUrls: ['./learning.component.scss']
})
export class LearningComponent implements OnInit {

      showdata:any=[];
      employeeMasterReport:boolean=false;
      employeeLoginReport:boolean=false;
      courseCompleationReportDate:boolean=false;
      courseCompleationReportPerc:boolean=false;
       quizCompleationReport:boolean=false;
      buddycalingReport:boolean=false;
      feedbackReport:boolean=false;
      courseModuleCompleationReport:boolean=false;
  constructor(private router:Router,private routes:ActivatedRoute,private AppService:AppService){ 
       this.showdata =this.AppService.getfeatures();
      if(this.showdata)
      {
        for(let i=0;i<this.showdata.length;i++)
        {
          if(this.showdata[i] == 'MEMR')
          {
            this.employeeMasterReport =true;
          }
           if(this.showdata[i] == 'MELR')
          {
            this.employeeLoginReport =true;
          }
           if(this.showdata[i] == 'MCCRD')
          {
            this.courseCompleationReportDate =true;
          }
           if(this.showdata[i] == 'MCCRP')
          {
            this.courseCompleationReportPerc =true;
          }
           if(this.showdata[i] == 'MOCR')
          {
            this.quizCompleationReport =true;
          }
           if(this.showdata[i] == 'MFR')
          {
            this.feedbackReport =true;
          }
           if(this.showdata[i] == 'MBCR')
          {
            this.buddycalingReport =true;
          }
           if(this.showdata[i] == 'MCMCR')
          {
            this.courseModuleCompleationReport =true;
          }
        }
      }
     }
  ngOnInit() {
  }

  gotopivot(){
  	this.router.navigate(['pivot'],{relativeTo:this.routes});
  }

  gotoEmpInductionReport(){
  	this.router.navigate(['employee-induction-report'],{relativeTo:this.routes});
  }

  gotoEmpLoginReport(){
  	this.router.navigate(['employee-login-report'],{relativeTo:this.routes});
  }

  gotoUserActivityReport(){
  	this.router.navigate(['user-activity-report'],{relativeTo:this.routes});
  }

  gotocoursecompletionreportdate(){
    this.router.navigate(['course-completion-initial-report-date'],{relativeTo:this.routes});
  }

  gotoquizcompletionreport(){
    this.router.navigate(['quiz-completion-initial-report'],{relativeTo:this.routes});
  }

  gotofeedbackreport(){
    this.router.navigate(['feedback-initial-report'],{relativeTo:this.routes});
  }

  gotobuddycallingreport(){
    this.router.navigate(['buddy-calling-report'],{relativeTo:this.routes});
  }

  gotocoursecompletionreportpercentage(){
     this.router.navigate(['course-completion-initial-report-percentage'],{relativeTo:this.routes});
  }
  gotoModuleCompletionInitialReport(){
    this.router.navigate(['course-module-completion-initial-report'],{relativeTo:this.routes});
  }

   gotoCourseSummaryInitialReport(){
    this.router.navigate(['course-summery-initial-report'],{relativeTo:this.routes});
  }
  back(){
      
      this.router.navigate(['pages/evaluate/reporting']);
  }
}
