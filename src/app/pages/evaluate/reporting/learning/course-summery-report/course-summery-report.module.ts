import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { AngularSlickgridModule } from 'angular-slickgrid';
import { NgxEchartsModule } from 'ngx-echarts';

import { ThemeModule } from '../../../../../@theme/theme.module';
import { CourseSummeryReportService } from './course-summery-report.service';
import { CourseSummeryReportComponent } from './course-summery-report.component';
@NgModule({
    imports:[
        ThemeModule,
        NgxEchartsModule,
        AngularSlickgridModule.forRoot(),
        TranslateModule.forRoot()
    ],
    declarations:[
        CourseSummeryReportComponent
    ],
    providers:[
        CourseSummeryReportService
    ],
    schemas: [ 
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA 
      ]

})
export class CourseSummeryReportModule{}