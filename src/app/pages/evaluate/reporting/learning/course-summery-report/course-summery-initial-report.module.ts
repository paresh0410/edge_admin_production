import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

import { NgxEchartsModule } from 'ngx-echarts';

import { ThemeModule } from '../../../../../@theme/theme.module';
 import { CourseSummeryInitialReportService } from './course-summery-initial-report.service';

import { TranslateModule } from '@ngx-translate/core';
import { AngularSlickgridModule } from 'angular-slickgrid';
import { CourseSummeryInitialReportComponent } from './course-summery-initial-report.component'

@NgModule({
    imports: [
      ThemeModule,
      NgxEchartsModule,
      // UsersModule,
      AngularSlickgridModule.forRoot(),
      TranslateModule.forRoot()
    ],
    declarations: [
        CourseSummeryInitialReportComponent,
    ],
    providers: [
     CourseSummeryInitialReportService
      //slikgridDemoService,
    ],
    schemas: [ 
      CUSTOM_ELEMENTS_SCHEMA,
      NO_ERRORS_SCHEMA 
    ]
  })
  export class CourseSummeryInitialReportModule{

  }