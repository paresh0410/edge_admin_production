import {Component, OnInit, OnDestroy, ChangeDetectorRef,NgZone, ViewChild} from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { AppService } from '../../../../../app.service';
import { Router, NavigationStart, Routes, ActivatedRoute } from '@angular/router';
import {CourseSummeryReportService} from './course-summery-report.service';
import { ExportAsService, ExportAsConfig } from 'ngx-export-as'
import { Column, GridOption, AngularGridInstance, FieldType, Filters, Formatters, GridStateChange, OperatorType, Statistic } from 'angular-slickgrid';


@Component({
    selector: 'ngx-course-summery-report',
    templateUrl: './course-summery-report.html',
    styleUrls: ['./course-summery-report.scss']
    // template: `<router-outlet></router-outlet>`,
  })
export class CourseSummeryReportComponent implements OnInit {

    @ViewChild('grid') Grid:any;
 
config: ExportAsConfig = {
  type: 'xlsx', // the type you want to download
  elementId: 'table1', // the id of html/table element,
  options: { // html-docx-js document options
    orientation: 'landscape',
    margins: {
      top: '20'
    }
  }
}
show:boolean;
errorMsg:any;
loader:any;
formdata:any;
    course:any;
 module:any;
 activity:any;
 fianlData:any;
 finalCourseData:any;
 finalMainData:any;
 finalModuleData:any=[];
 finalActivityData:any=[];
  name:any;

    ngOnInit():void{

    }
    constructor(private themeService: NbThemeService,private routes:ActivatedRoute, private router:Router,private modulereportService :CourseSummeryReportService,public cd : ChangeDetectorRef, public zone : NgZone,private AppService:AppService,private exportAsService: ExportAsService) {
       
         
          var data =this.modulereportService.data;
          console.log("Data",data);
    
         if(data)
         {
              this.loader=true;
            this.name =data.name;
            console.log('data',data) ;
             this.formdata={
               wflid:data.wflid,

            }

               this.modulereportService.getcourseSummary(this.formdata)
      .subscribe(rescompData => { 
       
      if(rescompData.data[3].length >1)
       {

        this.course = rescompData.data[0][0].Corsnames.split('|');
        this.module=rescompData.data[1][0].Modnames.split('|');
        this.activity=rescompData.data[2][0].Actnames.split('|');
        this.fianlData=rescompData.data[3];
        var moduleArry=[];
        for(let i=0;i<this.fianlData.length;i++)
        {
          this.fianlData[i].copmdata =this.fianlData[i].compl.split('|')
        }
        console.log('this.fianlData',this.fianlData)

        var ACTDATA = [];
        var MODDATA=[];
        for(let i=0;i<this.activity.length;i++){
          var data  = this.activity[i].split('~');
          // console.log('data i'+i,data.length);
          
          for(let j=0;j < data.length;j++){
            ACTDATA.push(data[j])
            var da = data[j].split('^');
            for(let k=0;k < da.length;k++){
              this.finalActivityData.push(da[k]);
            }
            
          }
        }

        for(let i=0;i< this.module.length;i++){
          var data  = this.module[i].split('~');
           for(let j=0;j<data.length;j++)
           {
             MODDATA.push(data[j])
           }
          
        }
        console.log('MODDATA',MODDATA); 
        console.log('ACTDATA',ACTDATA); 
        var CourseData=[];
        var course={};
        var modul={};
        var act={};
        for(let i=0;i<this.course.length;i++)
        {
          course={
            course:this.course[i],
            module:[],
            modact:[],
            count:0,
            modlen : 0,
            newcount:0
             // modules:[],
          }
          CourseData[i] = course;
          for(let j=0;j<this.module.length;j++)
          {
            if(i == j)
            {   
              for(let k=0;k<this.activity.length;k++)
              {
                if(i==k)
                {
                  var modData = this.module[j].split('~');
                  var actData = this.activity[k].split('~');

                  for(let l=0;l<modData.length;l++){
                    for(let m=0;m<actData.length;m++){
                      if(l == m){
                        var actD = actData[m].split('^');
                        var modObj = {
                          modname: modData[l],
                          act : actD
                          // act:[]
                        }
                        CourseData[i].count = CourseData[i].count + actD.length
                        this.finalModuleData.push(modObj)
                        CourseData[i].module.push(modObj);
                        CourseData[i].modact.push(actData[m]);
                        CourseData[i].modlen = CourseData[i].module.length
                      }
                    }

                  }    
                }
              }
            } 
          }
        }


        var totalmod = 0;
        var actCount = 0;
        for(let i=0;i< CourseData.length;i++){
          totalmod = totalmod + CourseData[i].modlen;
          actCount = actCount + CourseData[i].count;
        }
         console.log('totalmod',totalmod);
         console.log('actCount',actCount);

        this.finalCourseData =CourseData;
        console.log('this.finalModuleData',this.finalModuleData);
        console.log('this.course',this.finalCourseData);
        this.loader =false;
        this.show=true;
       }else
       {
           this.loader =false;
          this.show =false;
       }
      },
      resUserError => {
        this.loader =false;
        this.errorMsg = resUserError
      });


         }
    }

        back(){
      
        this.router.navigate(['pages/evaluate/reporting/learning/course-summery-initial-report']);
    }


       export() {
    // download the file using old school javascript method
    this.exportAsService.save(this.config, 'Course Summary Report');
    // get the data as base64 or json object for json type - this will be helpful in ionic or SSR
    this.exportAsService.get(this.config).subscribe(content => {
      console.log(content);
    });
  }
}