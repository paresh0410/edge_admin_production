import {Injectable, Inject} from '@angular/core';
//import {BaThemeConfigProvider, layoutPaths} from '../../../../theme';
import {Http, Response, Headers, RequestOptions, Request} from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import {AppConfig} from '../../../../../app.module';

@Injectable()
export class CourseSummeryReportService{
    private _urldrop:string = "/api/dashboard/quizlistdrop" // Dev
   private _url1:string = "/api/dashboard/getActivityData"
    constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private http: Http) {
    }
  
    getQuizDrop(){
        let url:any = `${this.config.FINAL_URL}`+this._urldrop;
        return this.http.post(url,{})
          .map((response:Response) => response.json())
          .catch(this._errorHandler);
      }
       getcourseSummary(user){
    let url:any = `${this.config.FINAL_URL}`+this._url1;
    return this.http.post(url,user)
      .map((response:Response) => response.json())
      .catch(this._errorHandler);
  }
        _errorHandler(error: Response){
        console.error(error);
        return Observable.throw(error || "Server Error")
      }
    public data:any;
}