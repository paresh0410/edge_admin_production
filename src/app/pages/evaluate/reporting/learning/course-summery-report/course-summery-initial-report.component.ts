import {Component, OnInit, OnDestroy, ChangeDetectorRef,NgZone, ViewChild} from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators/takeWhile' ;

//import { EmployeeInductionReportService } from './employee-induction-report.service';
import { Router, NavigationStart, Routes, ActivatedRoute } from '@angular/router';
import { CourseSummeryInitialReportService } from './course-summery-initial-report.service'
import { Column, GridOption, AngularGridInstance, FieldType, Filters, Formatters, GridStateChange, OperatorType, Statistic } from 'angular-slickgrid';
import {CourseSummeryReportService} from './course-summery-report.service'
@Component({
    selector: 'ngx-course-summery-initial-report',
    templateUrl: './course-summery-initial-report.component.html',
    styleUrls: ['./course-summery-initial-report.component.scss']
    // template: `<router-outlet></router-outlet>`,
  })
export class CourseSummeryInitialReportComponent{

    @ViewChild('grid') Grid:any;
// config: ExportAsConfig = {
//   type: 'xlsx', // the type you want to download
//   elementId: 'table1', // the id of html/table element,
//   options: { // html-docx-js document options
//     orientation: 'landscape',
//     margins: {
//       top: '20'
//     }
//   }
// }
 
  angularGrid: AngularGridInstance;
  columnDefinitions: Column[];
 columnDefinitions1: Column[];
 columnDeflat:any[];
  gridOptions: GridOption;
  dataset: any[];
  statistics: Statistic;
  selectedTitles: any[];
  selectedTitle: any;
  gridObj: any;
  grid:any;
  errorMsg:any;
  loader:any;
getData:any;
coloumName1:any;
coloumName2:any;
columnDes:any=[];
statcoldata:any=[];
 courselistdrop:any=[];
userInductedData:any=[];
show:boolean=false;
  formdata:any ={
            qid:14,
          }
gridShow:boolean=false;
dataView:any;// angularGrid.dataView;
   // coloumName1:any=
   //  {   
   //  };

 course:any;
 module:any;
 activity:any;
 fianlData=[];
 finalCourseData:any;
 finalMainData:any;
 finalModuleData:any=[];
 finalActivityData:any=[];

 ngOnInit(): void {
       // this.dataset = this.prepareData();
        this.gridOptions = {
        enableAutoResize: true,       // true by default
        enableCellNavigation: true,
        enableExcelCopyBuffer: true,
        enableColumnReorder:false,
        enableFiltering: true,
        rowSelectionOptions: {
          // True (Single Selection), False (Multiple Selections)
          selectActiveRow: true,
        },
        enableGridMenu:true,
        // preselectedRows: [0, 2],
        enableCheckboxSelector: false,
        enableRowSelection: true,
        gridMenu:{
          hideRefreshDatasetCommand:false
        }
      };

   }


  constructor(private themeService: NbThemeService, private router:Router,private initialreportService :CourseSummeryInitialReportService,public cd : ChangeDetectorRef, public zone : NgZone,
   private routes:ActivatedRoute,private passservice:CourseSummeryReportService) {
    this.gridShow =false;
     this.loader =true;
         this.initialreportService.getCourseDrop()
      .subscribe(rescompData => { 
      //  this.loader =false;
        this.courselistdrop = rescompData.data[0];
        console.log(this.courselistdrop , 'this.workdrop')
         if(this.userInductedData)
           {
     
    this.columnDefinitions=[
    {
        field: "name",
        filterable: true,
        id: "name",
        name: "WORKFLOW NAME",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
    // {
    //     field: "coursename",
    //     filterable: true,
    //     id: "coursename",
    //     name: "COURSE NAME",
    //     sortable: true,
    //     type: FieldType.string, 
    //     filter: { model: Filters.compoundInput}
    // },
   
    // {
    //     field: "workflowname",
    //     filterable: true,
    //     id: "workflowname",
    //     name: "WORKFLOW NAME",
    //     sortable: true,
    //     type: FieldType.string, 
    //     filter: { model: Filters.compoundInput}
    // },
    ]
              
            for(let i=0;i<this.courselistdrop.length;i++)
            {
              
                this.courselistdrop[i].id =i+1;
             
            }
           }

           // this.columnDefinitions =this.columnDes;
         

            this.dataset = this.courselistdrop;
           
             console.log('this.columnDefinitions', this.columnDefinitions);
        console.log('this.dataset', this.dataset);
         this.loader =false;
      this.show =true;
      },
      resUserError => {
        this.loader =false;
        this.errorMsg = resUserError
      });

      // var workflow={
      //   wflid:31
      // }


 

      
    
  }



  angularGridReady(angularGrid: any) {
    this.gridShow =true;
     this.angularGrid = angularGrid;
     this.dataView = angularGrid.dataView;
      this.grid = angularGrid.slickGrid;
     // angularGrid.gridService.controlAndPluginService.gridMenuControl.onColumnsChanged.subscribe(data=>{

     // });
     // var data =angularGrid.slickGrid.getColumns();
     console.log('col',this.angularGrid);
     // this.grid.setColumns(this.columnDefinitions1);
      //this.grid.slickGrid.setColumns(this.columnDefinitions1);
      //this.grid.render();
    this.gridObj = angularGrid && angularGrid.slickGrid || {};
    //angularGrid.gridStateService.controlAndPluginService.gridMenuControl.init(angularGrid);
    let body = document.getElementsByTagName('body');
    let chaDe :any = this.cd;
    if(body){
      chaDe.rootNodes[1]=body[0];
    }
    chaDe.detectChanges();
    
  }

processOutsideAngularZone() {
  this.zone.runOutsideAngular(() => {
    
  });
}
 gotoCourseModuleCompletionReport(item){
   var data1=
   {
     name:item.name,
     wflid:item.wid,
     // workflowname:item.workflowname,
     // coursename:item.coursename

   }
   this.passservice.data =data1;
    this.router.navigate(['course-summery-report'],{relativeTo:this.routes});
  }

  handleSelectedRowsChanged(e, args) {
    if (Array.isArray(args.rows)) {
      this.selectedTitles = args.rows.map(idx => {
        const item = this.gridObj.getDataItem(idx);
        // var id =item.quizId;
        this.gotoCourseModuleCompletionReport(item)
        return item.title || '';
      });
    }
  }


  /** Dispatched event of a Grid State Changed event */
  gridStateChanged(gridState: GridStateChange) {
    console.log('Client sample, Grid State changed:: ', gridState);
  }

  /** Save current Filters, Sorters in LocaleStorage or DB */
  saveCurrentGridState(grid) {
    this.grid =grid;
    // grid.GridStateService.controlAndPluginService.columnPickerControl.destroy();
    console.log('Client sample, last Grid State:: ', this.angularGrid.gridStateService.getCurrentGridState());

  }
      

    colclear(angularGrid)
  {
    this.columnDefinitions =[];
    this.columnDefinitions1 =[];
    this.columnDes=[];
    this.statcoldata=[];
  }
    back(){
      
        this.router.navigate(['pages/evaluate/reporting/learning']);
    }


  //   export() {
  //   // download the file using old school javascript method
  //   this.exportAsService.save(this.config, 'My File Name');
  //   // get the data as base64 or json object for json type - this will be helpful in ionic or SSR
  //   this.exportAsService.get(this.config).subscribe(content => {
  //     console.log(content);
  //   });
  // }
   

}