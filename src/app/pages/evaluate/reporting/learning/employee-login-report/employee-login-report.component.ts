import {Component, OnInit, OnDestroy,ViewEncapsulation} from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators/takeWhile' ;
import { AppService } from '../../../../../app.service';
// import { IMyDpOptions ,IMyDateModel} from 'mydatepicker';
import {INgxMyDpOptions, IMyDateModel} from 'ngx-mydatepicker';
//import { EmployeeInductionReportService } from './employee-induction-report.service';
import { Router, NavigationStart, Routes, ActivatedRoute } from '@angular/router';
import { EmployeeLoginReportService } from './employee-login-report.service';
import { DatePipe } from '@angular/common';

import { Column, GridOption, AngularGridInstance, FieldType, Filters, Formatters, GridStateChange, OperatorType, Statistic } from 'angular-slickgrid';

interface CardSettings {
  title: string;
  iconClass: string;
  type: string;
}

@Component({
  selector: 'ngx-employee-login-report',
  templateUrl: './employee-login-report.component.html',
  styleUrls: ['./employee-login-report.component.scss'],
  encapsulation: ViewEncapsulation.None,
   providers:[DatePipe]  
  // template: `<router-outlet></router-outlet>`,
})
export class EmployeeLoginReportComponent implements OnInit {

  angularGrid: AngularGridInstance;
  columnDefinitions: Column[];
   columnDefinitions1: Column[];
  gridOptions: GridOption;
  dataset: any[];
  statistics: Statistic;
  selectedTitles: any[];
  selectedTitle: any;
  gridObj: any;
  getData:any;
coloumName1:any;
coloumName2:any;
columnDes:any=[];
statcoldata:any=[];
 quizlistdrop:any=[];
userInductedData:any=[];
show:boolean=false;
loader:boolean;
  fildata:any={
        logid:"",
        dateid:7,
        toDate:"",
        fromDate:"",
        isCustomDt:0
      }
  users:any=[];
  private rowData: any[];
     public  today=new Date();
  //var year=myDate.getFullYear();
  public startDate: INgxMyDpOptions = {
        // other options...
        dateFormat: 'yyyy-mm-dd',
        disableSince: {year: this.today.getFullYear(), month: this.today.getMonth()+1, day: this.today.getDate()+1},

    };
    public endDate: INgxMyDpOptions = {
      // other options...
      dateFormat: 'yyyy-mm-dd',
       disableSince: {year: this.today.getFullYear(), month: this.today.getMonth()+1, day: this.today.getDate()+1},
      //disableSince: {year: 2018, month: 10, day: 10},

  };
 // private placeHolder: string = 'Select a date';
     private startDatePlaceHolder: string = 'From Date';
    private endDatePlaceHolder: string = 'To Date';

 loggeddrop:any=[{
          id:0,
          name:'Not Active'
      },
      {
          id:1,
          name:'Active'
      },
           ]
      datedrop:any=[{
          id:7,
          name:'7 Days'
      },
      {
          id:15,
          name:'15 days'
      },
      {
          id:30,
          name:'1 Month'
      },
       {
          id:180,
          name:'6 Months'
      },
      {
          id:0,
          name:'Custom Dates'
      },
      ]
  // filters:any=[];
  // report_id:any={
  //   id:1,
  // }

  ngOnInit(): void {
     this.gridOptions = {
      enableAutoResize: true,       // true by default
      enableCellNavigation: true,
      enableExcelCopyBuffer: true,
      enableFiltering: true,
      enableColumnReorder:false,
      rowSelectionOptions: {
        // True (Single Selection), False (Multiple Selections)
        selectActiveRow: false
      },
      // preselectedRows: [0, 2],
      enableCheckboxSelector: true,
      enableRowSelection: true,
    };
  }
  contdata:any;

  constructor(private themeService: NbThemeService, private router:Router,private userDashService:EmployeeLoginReportService,private AppService:AppService) {
    // this.dataset = this.prepareData();
    this.contdata =this.AppService.getuserdata();
    this.loader =true;
    this.fildata={
        logid:"",
        dateid:7,
        toDate:"",
        fromDate:"",
        isCustomDt:0,

      }
      if(this.fildata.dateid == 0 )
    {
      var isCustomDt=1;
    }else{
        var isCustomDt=0;
    }
   var filterdata={
        logInStatus:this.fildata.logid,
        days:this.fildata.dateid,
        isCustomDt:isCustomDt,
        fromDt:this.fildata.fromDate,
        toDt:this.fildata.toDate, 
        userId:this.contdata.userId,
        roleId:this.contdata.roleId,
    }
    console.log('this.filterdata',filterdata);
      this.userDashService.updateUser(filterdata).subscribe(
       data => {
           this.loader =false;
          this.users = data.data;
          console.log('users Data', this.users)
          // this.clearFilterData();
         // this.rowData = this.users;
           if(this.users)
           {
              var filterav;
             this.coloumName1=this.users[0];
            for (let key in this.coloumName1) {
                  // console.log(this.coloumName1[key]);
                    if(key != "userid")
                    {
                      if(key != "emid")
                    {
                      if(key == "date of birth" || key == "date of joining")
                      {
                         filterav =  { model: Filters.compoundDate } 
                      }
                      else
                      {
                         filterav =  { model: Filters.compoundInput}
                      }
                    var id =key;
                     var field =key;

                       var name =key.toUpperCase();
                      
                   this.getData ={
                         id:id,
                       name:name,
                        field:field,
                        //filterParams: { newRowsAction: "keep" }
                        sortable: true,
                        minWidth: 100,
                        maxWidth:200,
                       type: FieldType.string, 
                       filterable: true,
                      filter: filterav
                    // this.columnTog.push(key);
                  }
                  this.columnDes.push(this.getData); 
                }
              }
              }
              
            for(let i=0;i<this.users.length;i++)
            {
              
                this.users[i].id =i+1;
             
            }
           }

           this.columnDefinitions =this.columnDes;
         

            this.dataset = this.users;
           
             console.log('this.columnDefinitions', this.columnDefinitions);
        console.log('this.dataset', this.dataset);
        this.show=true;
          // console.log("Data ",data);
       },
       error => {
         // this.loader =false;
         console.error("Error !");
         //return Observable.throw(error);
       }
    ); 
  //  this.prepareGrid();
  }

  prepareGrid(){
    this.columnDefinitions = [
      { id: 'username', name: 'Employee Code / Username', field: 'username', sortable: true, minWidth: 100,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'firstname', name: 'Firstname', field: 'firstname', sortable: true, type: FieldType.number, exportCsvForceToKeepAsString: true,
        minWidth: 100,
        filterable: true,
      },
      { id: 'lastname', name: 'Lastname', field: 'lastname', sortable: true, minWidth: 100,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'lastlogin', name: 'Last Login', field: 'lastlogin', formatter: Formatters.dateIso, sortable: true, minWidth: 100, exportWithFormatter: true,
        type: FieldType.date, filterable: true, filter: { model: Filters.compoundDate } 
      },
      { id: 'doj', name: 'Date Of Joining', field: 'doj', formatter: Formatters.dateIso, sortable: true, minWidth: 100, exportWithFormatter: true,
        type: FieldType.date, filterable: true, filter: { model: Filters.compoundDate } 
      },
      { id: 'inductionLocation', name: 'Induction Location', field: 'inductionLocation', minWidth: 100, maxWidth: 100,
        type: FieldType.boolean,sortable: true,filterable: true,
      },
      { id: 'designation', name: 'Designation', field: 'designation', minWidth: 100, maxWidth: 100,
        type: FieldType.boolean,sortable: true,filterable: true,
      },
      { id: 'department', name: 'Department', field: 'department', minWidth: 100, maxWidth: 100,
        type: FieldType.boolean,sortable: true,filterable: true,
      },
      { id: 'grade', name: 'Grade', field: 'grade', minWidth: 100, maxWidth: 100,
        type: FieldType.boolean,sortable: true,filterable: true,
      }
    ];
    this.gridOptions = {
      enableAutoResize: true,       // true by default
      enableCellNavigation: true,
      enableExcelCopyBuffer: true,
      enableFiltering: true,
      enableColumnReorder:false,
      rowSelectionOptions: {
        // True (Single Selection), False (Multiple Selections)
        selectActiveRow: false
      },
      // preselectedRows: [0, 2],
      enableCheckboxSelector: true,
      enableRowSelection: true,
    };

    // fill the dataset with your data
    // VERY IMPORTANT, Angular-Slickgrid uses Slickgrid DataView which REQUIRES a unique "id" and it has to be lowercase "id" and be part of the dataset
    // this.dataset = [];

    //this.dataset = this.prepareData();
    
  }

  prepareData() {
    // mock a dataset
    const mockDataset = [];
    // for demo purpose, let's mock a 1000 lines of data
    for (let i = 0; i < 1000; i++) {
      const randomYear = 2000 + Math.floor(Math.random() * 10);
      const randomMonth = Math.floor(Math.random() * 11);
      const randomDay = Math.floor((Math.random() * 28));
      const randomPercent = Math.round(Math.random() * 100);

      mockDataset[i] = {
        id: i, // again VERY IMPORTANT to fill the "id" with unique values
        title: 'Task ' + i,
        duration: Math.round(Math.random() * 100) + '',
        percentComplete: randomPercent,
        start: `${randomMonth}/${randomDay}/${randomYear}`,
        finish: `${randomMonth}/${randomDay}/${randomYear}`,
        effortDriven: (i % 5 === 0)
      };
    }
    return mockDataset;
  }

  angularGridReady(angularGrid: any) {
    this.columnDefinitions1=[];
      this.columnDefinitions1=[
       {
        field: "Employee ID",
        filterable: true,
        id: "Employee ID",
        maxWidth: 200,
        minWidth: 100,
        name: "EMPLOYEE ID",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
    {
        field: "Employee Name",
        filterable: true,
        id: "Employee Name",
        maxWidth: 200,
        minWidth: 100,
        name: "EMPLOYEE NAME",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
   
    {
        field: "Employee band",
        filterable: true,
        id: "Employee band",
        maxWidth: 200,
        minWidth: 100,
        name: "EMPLOYEE BAND",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
    {
        field: "RM code",
        filterable: true,
        id: "RM code",
        maxWidth: 200,
        minWidth: 100,
        name: "RM CODE",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
    {
        field: "RM name",
        filterable: true,
        id: "RM name",
        maxWidth: 200,
        minWidth: 100,
        name: "RM NAME",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
    {
        field: "department",
        filterable: true,
        id: "department",
        maxWidth: 200,
        minWidth: 100,
        name: "DEPARTMENT",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundInput}
    },
     {
        field: "upload Date",
        filterable: true,
        id: "upload Date",
        maxWidth: 200,
        minWidth: 100,
        name: "UPLOAD DATE",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundDate } 
    },
     {
        field: "registered date",
        filterable: true,
        id: "registered date",
        maxWidth: 200,
        minWidth: 100,
        name: "REGISTERED DATE",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundDate } 
    },
    {
        field: "first login",
        filterable: true,
        id: "first login",
        maxWidth: 200,
        minWidth: 100,
        name: "FIRST LOGIN",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundDate } 
    },
   
    {
        field: "last access",
        filterable: true,
        id: "last access",
        maxWidth: 200,
        minWidth: 100,
        name: "LAST ACCESS",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundDate } 
    },
     {
        field: "last login",
        filterable: true,
        id: "last login",
        maxWidth: 200,
        minWidth: 100,
        name: "LAST LOGIN",
        sortable: true,
        type: FieldType.string, 
        filter: { model: Filters.compoundDate } 
    },
    // {
    //     field: "grade",
    //     filterable: true,
    //     id: "grade",
    //     maxWidth: 200,
    //     minWidth: 100,
    //     name: "GRADE",
    //     sortable: true,
    //     type: FieldType.string, 
    //     filter: { model: Filters.compoundInput}
    // },
    ]
     angularGrid.slickGrid.setColumns(this.columnDefinitions1);
    this.angularGrid = angularGrid;
    this.gridObj = angularGrid && angularGrid.slickGrid || {};
  }

  handleSelectedRowsChanged(e, args) {
    if (Array.isArray(args.rows)) {
      this.selectedTitles = args.rows.map(idx => {
        const item = this.gridObj.getDataItem(idx);
        return item.title || '';
      });
    }
  }

  /** Dispatched event of a Grid State Changed event */
  gridStateChanged(gridState: GridStateChange) {
    console.log('Client sample, Grid State changed:: ', gridState);
  }

  /** Save current Filters, Sorters in LocaleStorage or DB */
  saveCurrentGridState(grid) {
    console.log('Client sample, last Grid State:: ', this.angularGrid.gridStateService.getCurrentGridState());
  }
   onStartDateChanged(event: IMyDateModel) {
     if(event.jsdate != null)
      {
      let d: Date = new Date(event.jsdate.getTime());

      // set previous of selected date
      d.setDate(d.getDate() - 1);

      // Get new copy of options in order the date picker detect change
      let copy: INgxMyDpOptions = this.getCopyOfEndDateOptions();
      copy.disableUntil = {year: d.getFullYear(), 
                           month: d.getMonth() + 1, 
                           day: d.getDate()};
      this.endDate = copy;
    }else{
        let copy: INgxMyDpOptions = this.getCopyOfEndDateOptions();
        copy.disableUntil = {year: 0, month: 0, day: 0}
        this.endDate = copy;
    }
  }

    onEndDateChanged(event: IMyDateModel) {
      if(event.jsdate != null)
      {
         let d: Date = new Date(event.jsdate.getTime());

      // set previous of selected date
      d.setDate(d.getDate() + 1);

      // Get new copy of options in order the date picker detect change
      let copy: INgxMyDpOptions = this.getCopyOfStartDateOptions();
      copy.disableSince = {year: d.getFullYear(), 
                           month: d.getMonth() + 1, 
                           day: d.getDate()};
      this.startDate = copy;
        // end date changed...
      }
      else{
        let copy: INgxMyDpOptions = this.getCopyOfStartDateOptions();
        copy.disableSince = {year: 0, month: 0, day: 0}
        this.startDate = copy;
    }
     } 
    getCopyOfEndDateOptions(): INgxMyDpOptions {
        return JSON.parse(JSON.stringify(this.endDate));
    }
     getCopyOfStartDateOptions(): INgxMyDpOptions {
        return JSON.parse(JSON.stringify(this.startDate));
    }

  // colclear(angularGrid)
  // {
  //   this.columnDefinitions =[];
  //    this.columnDefinitions1 =[];
  //   this.columnDes=[];
  //   this.statcoldata=[];
  // }
  submitForm(){
          this.loader =true;
          //console.log(this.myForm.value);
          //let parameters:any = this.myForm.value.parameters;
          if(this.fildata.dateid == 0 )
          {
            var isCustomDt=1;
          }else{
              var isCustomDt=0;
          }
          if(this.fildata.toDate == "")
          {
            var toDate= this.fildata.toDate;
          }
          else
          {
            var toDate= this.fildata.toDate.formatted;
          }
          if(this.fildata.fromDate == "")
          {
            var fromDate= this.fildata.fromDate;
          }
          else
          {
            var fromDate= this.fildata.fromDate.formatted;
          }
           
          var filterdata={
              logInStatus:this.fildata.logid,
              days:this.fildata.dateid,
              isCustomDt:isCustomDt,
              fromDt:fromDate,
              toDt:toDate,
              userId:this.contdata.userId,
              roleId:this.contdata.roleId, 
          }
          console.log('filterdata',filterdata)

          

          this.userDashService.updateUser(filterdata).subscribe(
             data => {
                //this.loader =false;
            this.users = data.data;
          console.log('users Data', this.users)
          // this.clearFilterData();
         // this.rowData = this.users;
           if(this.users)
           {
             this.coloumName1=this.users[0];
            // for (let key in this.coloumName1) {
            //       // console.log(this.coloumName1[key]);
            //         if(key != "userid")
            //         {
            //         var id =key;
            //          var field =key;

            //            var name =key.toUpperCase();
                      
            //        this.getData ={
            //              id:id,
            //            name:name,
            //             field:field,
            //             //filterParams: { newRowsAction: "keep" }
            //             sortable: true,
            //             minWidth: 100,
            //             maxWidth:200,
            //            type: FieldType.string, 
            //            filterable: true,
            //           filter: { model: Filters.compoundInput}
            //         // this.columnTog.push(key);
            //       }
            //       this.columnDes.push(this.getData); 
            //     }
            //   }
              
            for(let i=0;i<this.users.length;i++)
            {
              
                this.users[i].id =i+2;
             
            }
           }

           this.columnDefinitions =this.columnDes;
         

            this.dataset = this.users;
             this.angularGridReady(this.angularGrid);
             this.show=true;
              this.loader =false;
           
             console.log('this.columnDefinitions', this.columnDefinitions);
        console.log('this.dataset', this.dataset);
          // console.log("Data ",data);
       },
       error => {
         // this.loader =false;
         console.error("Error !");
         //return Observable.throw(error);
       }
          ); 
         

  }
   back(){
      
      this.router.navigate(['pages/evaluate/reporting/learning']);
  }

 
}
