import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeLoginReportComponent } from './employee-login-report.component';

describe('EmployeeLoginReportComponent', () => {
  let component: EmployeeLoginReportComponent;
  let fixture: ComponentFixture<EmployeeLoginReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeLoginReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeLoginReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
