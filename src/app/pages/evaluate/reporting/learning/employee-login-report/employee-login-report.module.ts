import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

import { NgxEchartsModule } from 'ngx-echarts';

import { ThemeModule } from '../../../../../@theme/theme.module';
//import { slikgridDemo } from './../../..../slikgridDemo.component';
//import { slikgridDemoService } from './slikgridDemo.service';

import { EmployeeLoginReportComponent } from './employee-login-report.component';
import { MyDatePickerModule } from 'mydatepicker';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { TranslateModule } from '@ngx-translate/core';
import { AngularSlickgridModule } from 'angular-slickgrid';

// const PLAN_COMPONENTS = [
//   slikgridDemo
// ];

@NgModule({
  imports: [
    ThemeModule,
    NgxEchartsModule,
    MyDatePickerModule,
      NgxMyDatePickerModule,
    // UsersModule,
    AngularSlickgridModule.forRoot(),
    TranslateModule.forRoot()
  ],
  declarations: [
    EmployeeLoginReportComponent,
  ],
  providers: [
    //slikgridDemoService,
  ],
  schemas: [ 
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA 
  ]
})
export class EmployeeLoginReportModule { }
