import {Component, OnInit, OnDestroy, ChangeDetectorRef,NgZone, ViewChild} from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { AppService } from '../../../../../app.service';
import { Router, NavigationStart, Routes, ActivatedRoute } from '@angular/router';
import {CourseModuleCompletionReportService} from './course-module-completion-report.service';
import { Column, GridOption, AngularGridInstance, FieldType, Filters, Formatters, GridStateChange, OperatorType, Statistic } from 'angular-slickgrid';
import { BrandDetailsService } from '../../../../../service/brand-details.service';


@Component({
    selector: 'ngx-course-module-completion-report',
    templateUrl: './course-module-completion-report.html',
    styleUrls: ['./course-module-completion-report.scss']
    // template: `<router-outlet></router-outlet>`,
  })
export class CourseModuleCompletionReportComponent implements OnInit {

    @ViewChild('grid') Grid:any;
    angularGrid: AngularGridInstance;
    columnDefinitions: Column[];
   columnDefinitions1: Column[];
   columnDeflat:any[];
    gridOptions: GridOption;
    dataset: any[];
    statistics: Statistic;
    selectedTitles: any[];
    selectedTitle: any;
    gridObj: any;
    grid:any;
    errorMsg:any;
  getData:any;
  coloumName1:any;
  coloumName2:any;
  columnDes:any=[];
  statcoldata:any=[];
   quizlistdrop:any=[];
  userInductedData:any=[];
  show:boolean=false;
    formdata:any ;
  gridShow:boolean=false;
  dataView:any;// angularGrid.dataView;
   
     loader:any;
   name:any;
   workflowname:any;  
   coursename:any;
   contdata:any;
  currentBrandData: any;
  

    ngOnInit():void{ 
    this.currentBrandData = this.brandService.getCurrentBrandData();
        this.gridOptions = {
            enableAutoResize: true,       // true by default
            enableCellNavigation: true,
            enableExcelCopyBuffer: true,
            enableFiltering: true,
            enableColumnReorder:false,
            rowSelectionOptions: {
              // True (Single Selection), False (Multiple Selections)
              selectActiveRow: false
            },
            enableGridMenu:true,
            // preselectedRows: [0, 2],
            enableCheckboxSelector: true,
            enableRowSelection: true,
            gridMenu:{
              hideRefreshDatasetCommand:false
            }
          };
    }
    constructor(private themeService: NbThemeService,private routes:ActivatedRoute, private router:Router,private modulereportService :CourseModuleCompletionReportService,
    public brandService: BrandDetailsService,
    public cd : ChangeDetectorRef, public zone : NgZone,private AppService:AppService) {
      this.contdata =this.AppService.getuserdata();
        this.loader=true;
         
          var data =this.modulereportService.data;
          console.log("Data",data);
    
         if(data)
         {
            
             console.log('data',data) ;
             this.formdata={
               cid:data.quizId,
               userId:this.contdata.userId,
               roleId:this.contdata.roleId,
             // cid:53
            }
    
                this.name =data.name;
               this.workflowname=data.workflowname;
               this.coursename=data.coursename;
             this.modulereportService.getcoursecompletion(this.formdata)
            .subscribe(rescompData => { 
            this.loader =false;
             var userInductedData=rescompData;
             console.log('userInductedData', userInductedData);
            this.userInductedData = rescompData.data = undefined ? null : rescompData.data;
            this.statcoldata =rescompData.data1 = undefined ? null :rescompData.data1;
            if(this.statcoldata!=null &&this.statcoldata.length>0)
            {
                 this.gridShow =true;
            }
            else{
                  this.gridShow =false;
            }
            console.log('this.creportdata', this.userInductedData);
             if(this.userInductedData)
               {
                  var filterav;
                 this.coloumName1=this.userInductedData[0];
                for (let key in this.coloumName1) {
                      // console.log(this.coloumName1[key]);
                        if(key != "userid" && key.length > 2 )
                        {
                          if(key!="cmid"){

                            if(key == "date of birth" || key == "date of joining")
                            {
                               filterav =  { model: Filters.compoundDate } 
                            }
                            else
                            {
                               filterav =  { model: Filters.compoundInput}
                            }
                    var id =key;
                     var field =key;
                       var name =key.toUpperCase();
                      
                   this.getData ={
                         id:id,
                       name:name,
                        field:field,
                        //filterParams: { newRowsAction: "keep" }
                        sortable: true,
                        minWidth: 100,
                        maxWidth:200,
                       type: FieldType.string, 
                       filterable: true,
                      filter: filterav
                    // this.columnTog.push(key);
                  }
                  this.columnDes.push(this.getData); 
                          }
                          
                    
                  }
                  }
                  // for (let key in this.coloumName1) {
                  //     // console.log(this.coloumName1[key]);
                  //       if(key.length <= 2)
                  //       {
                  //       var id =key;
                  //        var field =key;
                  //          var name ='Q-'+key.toUpperCase();
                          
                  //      this.getData ={
                  //            id:id+100,
                  //          name:name,
                  //           field:field,
                  //           //filterParams: { newRowsAction: "keep" }
                  //           sortable: true,
                  //           minWidth: 100,
                  //           maxWidth:200,
                  //          type: FieldType.string, 
                  //          filterable: true,
                  //         filter: { model: Filters.compoundInput}
                  //       // this.columnTog.push(key);
                  //     }
                  //     this.columnDes.push(this.getData); 
                  //   }
                  // }
                if(this.userInductedData!=null&&this.userInductedData.length>0){
                  
                for(let i=0;i<this.userInductedData.length;i++)
                {
                  
                    this.userInductedData[i].id =i+1;
                 
                }
              }
               }
    
               this.columnDefinitions =this.columnDes;
             
    
                this.dataset = this.userInductedData;
               
                 console.log('this.columnDefinitions', this.columnDefinitions);
            console.log('this.dataset', this.dataset);
          this.show =true;
           
            
          },
          resUserError => {
            this.loader =false;
            this.errorMsg = resUserError
          });
         }
      }
    
      angularGridReady(angularGrid: any) {
        //this.gridShow =true;
    
              if(this.statcoldata)
               {
    this.columnDefinitions1=[];
          this.columnDefinitions1=[
           {
            field: "Employee ID",
            filterable: true,
            id: "Employee ID",
            maxWidth: 200,
            minWidth: 100,
            name: this.currentBrandData.employee.toUpperCase()+" ID",
            sortable: true,
            type: FieldType.string, 
            filter: { model: Filters.compoundInput}
        },
        {
            field: "Employee Name",
            filterable: true,
            id: "Employee Name",
            maxWidth: 200,
            minWidth: 100,
            name: this.currentBrandData.employee.toUpperCase()+" NAME",
            sortable: true,
            type: FieldType.string, 
            filter: { model: Filters.compoundInput}
        },
       
        {
            field: "Employee band",
            filterable: true,
            id: "Employee band",
            maxWidth: 200,
            minWidth: 100,
            name: this.currentBrandData.employee.toUpperCase()+" BAND",
            sortable: true,
            type: FieldType.string, 
            filter: { model: Filters.compoundInput}
        },
        {
            field: "RM code",
            filterable: true,
            id: "RM code",
            maxWidth: 200,
            minWidth: 100,
            name: "RM CODE",
            sortable: true,
            type: FieldType.string, 
            filter: { model: Filters.compoundInput}
        },
        {
            field: "RM name",
            filterable: true,
            id: "RM name",
            maxWidth: 200,
            minWidth: 100,
            name: "RM NAME",
            sortable: true,
            type: FieldType.string, 
            filter: { model: Filters.compoundInput}
        },
        {
            field: "department",
            filterable: true,
            id: "department",
            maxWidth: 200,
            minWidth: 100,
            name: "DEPARTMENT",
            sortable: true,
            type: FieldType.string, 
            filter: { model: Filters.compoundInput}
        },
        ]
        // this.columnDeflat =this.columnDefinitions1
                   var filterav1;
                 this.coloumName2=this.statcoldata[0];
                 for (let key in this.coloumName2) {
                      // console.log(this.coloumName1[key]);
    
                        if(key != "userid")
                        {
                           if(key!="id"){
                            if(key!="cmid"){
                              if(key == "date of birth" || key == "date of joining")
                                 {
                                    filterav1 =  { model: Filters.compoundDate } 
                                 }
                                 else
                                 {
                                    filterav1 =  { model: Filters.compoundInput}
                                 }
                          var id =key;
                          var field =key;
                            var name =key.toUpperCase();
                           
                        this.getData ={
                              id:id,
                            name:name,
                             field:field,
                             //filterParams: { newRowsAction: "keep" }
                             sortable: true,
                             minWidth: 100,
                             maxWidth:200,
                            type: FieldType.string, 
                            filterable: true,
                           filter: filterav1
                         // this.columnTog.push(key);
                       }
                       this.columnDefinitions1.push(this.getData);
                     } 
                           }
                    
                  }
                  }
                  // for (let key in this.coloumName2) {
                  //     // console.log(this.coloumName1[key]);
                  //       if(key.length <= 2)
                  //       {
                  //       var id =key;
                  //        var field =key;
                  //          var name =key.toUpperCase();
                          
                  //      this.getData ={
                  //            id:id+100,
                  //          name:name,
                  //           field:field,
                  //           //filterParams: { newRowsAction: "keep" }
                  //           sortable: true,
                  //           minWidth: 100,
                  //           maxWidth:200,
                  //          type: FieldType.string, 
                  //          filterable: true,
                  //         filter: { model: Filters.compoundInput}
                  //       // this.columnTog.push(key);
                  //     }
                  //     this.columnDefinitions1.push(this.getData); 
                  //   }
                  // }
           
               }
         //this.gridOptions._columnDefinitions = this.columnDefinitions1;
         this.angularGrid = angularGrid;
         this.dataView = angularGrid.dataView;
          this.grid = angularGrid.slickGrid;
         // angularGrid.gridService.controlAndPluginService.gridMenuControl.onColumnsChanged.subscribe(data=>{
    
         // });
         // var data =angularGrid.slickGrid.getColumns();
         console.log('col',this.angularGrid);
         this.grid.setColumns(this.columnDefinitions1);
          //this.grid.slickGrid.setColumns(this.columnDefinitions1);
          //this.grid.render();
        this.gridObj = angularGrid && angularGrid.slickGrid || {};
        //angularGrid.gridStateService.controlAndPluginService.gridMenuControl.init(angularGrid);
        let body = document.getElementsByTagName('body');
        let chaDe :any = this.cd;
        if(body){
          chaDe.rootNodes[1]=body[0];
        }
        chaDe.detectChanges();
        //this.processOutsideAngularZone();
        // angularGrid.gridService.controlAndPluginService.columnPickerControl.init(this.gridObj)
        // setTimeout(()=>{
        //     this.cd.detectChanges();
        // },500);
        //angularGrid.updateColumnDefinitionsList(this.columnDefinitions1);
      }
      processOutsideAngularZone() {
        this.zone.runOutsideAngular(() => {
          
        });
      }
      
    handleSelectedRowsChanged(e, args) {
          if (Array.isArray(args.rows)) {
            this.selectedTitles = args.rows.map(idx => {
              const item = this.gridObj.getDataItem(idx);
              return item.title || '';
            });
          }
        }
      gridStateChanged(gridState: GridStateChange) {
        console.log('Client sample, Grid State changed:: ', gridState);
      }
    
      /** Save current Filters, Sorters in LocaleStorage or DB */
      saveCurrentGridState(grid) {
        this.grid =grid;
        // grid.GridStateService.controlAndPluginService.columnPickerControl.destroy();
        console.log('Client sample, last Grid State:: ', this.angularGrid.gridStateService.getCurrentGridState());
    
      }
      colclear(angularGrid){
            this.columnDefinitions =[];
            this.columnDefinitions1 =[];
            this.columnDes=[];
            this.statcoldata=[];
    }
    callType(id)
    {
      
      var calldata={qid:id};
       this.modulereportService.getcoursecompletion(calldata)
      .subscribe(rescompData => { 
        //this.loader =false;
        //this.creportdata = rescompData.data;
        this.userInductedData = rescompData.data.joinedData != undefined ? rescompData.data.joinedData.data :null ;
        this.statcoldata =rescompData.data.output != undefined ? rescompData.data.output :null;
        console.log('this.creportdata', this.userInductedData);
         if(this.userInductedData)
           {
             this.coloumName1=this.userInductedData[0];
            for (let key in this.coloumName1) {
                  // console.log(this.coloumName1[key]);
                    if(key != "userid")
                    {
                    var id =key;
                     var field =key;

                     if(key.length <=2)
                     {
                       var name ='DAY'+key.toUpperCase();
                     }
                     else{
                       var name =key.toUpperCase();
                     }
                      
                   this.getData ={
                         id:id,
                       name:name,
                        field:field,
                        //filterParams: { newRowsAction: "keep" }
                        sortable: true,
                        minWidth: 100,
                        maxWidth:200,
                       type: FieldType.string, 
                       filterable: true,
                      filter: { model: Filters.compoundInput}
                    // this.columnTog.push(key);
                  }
                  this.columnDes.push(this.getData); 
                }
              }

                for(let i=0;i<this.userInductedData.length;i++)
            {
              
                this.userInductedData[i].id =i+2;
             
            }
              
           }

           this.columnDefinitions =this.columnDes;
           

          
            this.dataset = this.userInductedData;
            
           
             console.log('this.columnDefinitions', this.columnDefinitions);
        console.log('this.dataset', this.dataset);
      this.show =true;
      this.angularGridReady(this.angularGrid);
      
      },
      resUserError => {
        //this.loader =false;
        this.errorMsg = resUserError
      });
        
    } 

    back(){
      
        this.router.navigate(['pages/evaluate/reporting/learning']);
    }

}