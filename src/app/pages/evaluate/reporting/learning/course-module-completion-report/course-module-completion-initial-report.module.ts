import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

import { NgxEchartsModule } from 'ngx-echarts';

import { ThemeModule } from '../../../../../@theme/theme.module';
import { CourseModuleCompletionInitialReportService } from './course-module-completion-initial-report.service';

import { TranslateModule } from '@ngx-translate/core';
import { AngularSlickgridModule } from 'angular-slickgrid';
import { CourseModuleCompletionInitialReportComponent } from './course-module-completion-initial-report.component'

@NgModule({
    imports: [
      ThemeModule,
      NgxEchartsModule,
      // UsersModule,
      AngularSlickgridModule.forRoot(),
      TranslateModule.forRoot()
    ],
    declarations: [
        CourseModuleCompletionInitialReportComponent,
    ],
    providers: [
      //slikgridDemoService,
    ],
    schemas: [ 
      CUSTOM_ELEMENTS_SCHEMA,
      NO_ERRORS_SCHEMA 
    ]
  })
  export class CourseModuleCompletionInitialReportModule{

  }