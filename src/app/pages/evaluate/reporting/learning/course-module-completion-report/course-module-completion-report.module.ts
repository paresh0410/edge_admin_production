import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { AngularSlickgridModule } from 'angular-slickgrid';
import { NgxEchartsModule } from 'ngx-echarts';

import { ThemeModule } from '../../../../../@theme/theme.module';
import { CourseModuleCompletionReportService } from './course-module-completion-report.service';
import { CourseModuleCompletionReportComponent } from './course-module-completion-report.component';
@NgModule({
    imports:[
        ThemeModule,
        NgxEchartsModule,
        AngularSlickgridModule.forRoot(),
        TranslateModule.forRoot()
    ],
    declarations:[
        CourseModuleCompletionReportComponent
    ],
    providers:[
        CourseModuleCompletionReportService
    ],
    schemas: [ 
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA 
      ]

})
export class ModuleCompletionReportModule{}