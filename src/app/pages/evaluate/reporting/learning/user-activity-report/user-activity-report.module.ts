import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

import { NgxEchartsModule } from 'ngx-echarts';

import { ThemeModule } from '../../../../../@theme/theme.module';
//import { slikgridDemo } from './../../..../slikgridDemo.component';
//import { slikgridDemoService } from './slikgridDemo.service';

import { UserActivityReportComponent } from './user-activity-report.component';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { TranslateModule } from '@ngx-translate/core';
import { AngularSlickgridModule } from 'angular-slickgrid';

// const PLAN_COMPONENTS = [
//   slikgridDemo
// ];

@NgModule({
  imports: [
    ThemeModule,
    NgxEchartsModule,
    // UsersModule,
     NgxMyDatePickerModule,
    AngularSlickgridModule.forRoot(),
    TranslateModule.forRoot()
  ],
  declarations: [
    UserActivityReportComponent,
  ],
  providers: [
    //slikgridDemoService,
  ],
  schemas: [ 
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA 
  ]
})
export class UserActivityReportModule { }
