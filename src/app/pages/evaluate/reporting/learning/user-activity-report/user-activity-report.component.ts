import {Component, OnInit, OnDestroy} from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators/takeWhile' ;
import {INgxMyDpOptions, IMyDateModel} from 'ngx-mydatepicker'
//import { EmployeeInductionReportService } from './employee-induction-report.service';
import { Router, NavigationStart, Routes, ActivatedRoute } from '@angular/router';
import { UserActivityReportService } from './user-activity-report.service';

import { Column, GridOption, AngularGridInstance, FieldType, Filters, Formatters, GridStateChange, OperatorType, Statistic } from 'angular-slickgrid';

interface CardSettings {
  title: string;
  iconClass: string;
  type: string;
}

@Component({
  selector: 'ngx-user-activity-report',
  templateUrl: './user-activity-report.component.html',
  styleUrls: ['./user-activity-report.component.scss'],
  // template: `<router-outlet></router-outlet>`,
})
export class UserActivityReportComponent implements OnInit {
  fildata:any={};
  angularGrid: AngularGridInstance;
  columnDefinitions: Column[];
  gridOptions: GridOption;
  dataset: any[];
  statistics: Statistic;
  selectedTitles: any[];
  selectedTitle: any;
  show:boolean=false;
  loader:boolean;
  gridObj: any;
  users:any=[];
  
      public  today=new Date();
  //var year=myDate.getFullYear();
  public startDate: INgxMyDpOptions = {
        // other options...
        dateFormat: 'yyyy-mm-dd',
        disableSince: {year: this.today.getFullYear(), month: this.today.getMonth()+1, day: this.today.getDate()+1},

    };
    public endDate: INgxMyDpOptions = {
      // other options...
      dateFormat: 'yyyy-mm-dd',
      disableSince: {year: this.today.getFullYear(), month: this.today.getMonth()+1, day: this.today.getDate()+1},

  };
 // private placeHolder: string = 'Select a date';
     public startDatePlaceHolder: string = 'From Date';
     public endDatePlaceHolder: string = 'To Date';

  ngOnInit(): void {
    this.prepareGrid();
  }

  constructor(private themeService: NbThemeService, private router:Router,private userDashService:UserActivityReportService) {
    this.fildata={     
        toDate:"",
        fromDate:"",
      }
      
     

    // this.dataset = this.prepareData();
  }

  prepareGrid(){
    this.columnDefinitions = [
     { id: 'Date', name: 'Date', field: 'Date', sortable: true, minWidth: 100,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundDate },exportWithFormatter: true, 
      },
      { id: 'Unique Users', name: 'Unique Users', field: 'Unique Users', sortable: true, type: FieldType.string,
        filterable: true,
        minWidth: 100,
        filter: { model: Filters.compoundInput } 
      },
      { id: 'User Hits (K)', name: 'User Hits (K)', field: 'User Hits (K)', sortable: true, minWidth: 100,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'Quizzes', name: 'Quizzes', field: 'Quizzes', sortable: true, minWidth: 100,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      },
      { id: 'Files (Resource)', name: 'Videos & Pdfs', field: 'Files (Resource)', sortable: true, minWidth: 100, exportWithFormatter: true,
        type: FieldType.string, filterable: true, filter: { model: Filters.compoundInput } 
      }
    ];
    this.gridOptions = {
      enableAutoResize: true,       // true by default
      enableCellNavigation: true,
      enableExcelCopyBuffer: true,
      enableFiltering: true,
      enableColumnReorder:false,
      rowSelectionOptions: {
        // True (Single Selection), False (Multiple Selections)
        selectActiveRow: false
      },
      // preselectedRows: [0, 2],
      enableCheckboxSelector: true,
      enableRowSelection: true,
    };

    // fill the dataset with your data
    // VERY IMPORTANT, Angular-Slickgrid uses Slickgrid DataView which REQUIRES a unique "id" and it has to be lowercase "id" and be part of the dataset
    // this.dataset = [];

   // this.dataset = this.prepareData();
    
  }

  prepareData() {
    // mock a dataset
    const mockDataset = [];
    // for demo purpose, let's mock a 1000 lines of data
    for (let i = 0; i < 1000; i++) {
      const randomYear = 2000 + Math.floor(Math.random() * 10);
      const randomMonth = Math.floor(Math.random() * 11);
      const randomDay = Math.floor((Math.random() * 28));
      const randomPercent = Math.round(Math.random() * 100);

      mockDataset[i] = {
        id: i, // again VERY IMPORTANT to fill the "id" with unique values
        title: 'Task ' + i,
        duration: Math.round(Math.random() * 100) + '',
        percentComplete: randomPercent,
        start: `${randomMonth}/${randomDay}/${randomYear}`,
        finish: `${randomMonth}/${randomDay}/${randomYear}`,
        effortDriven: (i % 5 === 0)
      };
    }
    return mockDataset;
  }

  angularGridReady(angularGrid: any) {
    this.angularGrid = angularGrid;
    this.gridObj = angularGrid && angularGrid.slickGrid || {};
  }

  handleSelectedRowsChanged(e, args) {
    if (Array.isArray(args.rows)) {
      this.selectedTitles = args.rows.map(idx => {
        const item = this.gridObj.getDataItem(idx);
        return item.title || '';
      });
    }
  }

  /** Dispatched event of a Grid State Changed event */
  gridStateChanged(gridState: GridStateChange) {
    console.log('Client sample, Grid State changed:: ', gridState);
  }

  /** Save current Filters, Sorters in LocaleStorage or DB */
  saveCurrentGridState(grid) {
    console.log('Client sample, last Grid State:: ', this.angularGrid.gridStateService.getCurrentGridState());
  }

  onStartDateChanged(event: IMyDateModel) {
     if(event.jsdate != null)
      {
      let d: Date = new Date(event.jsdate.getTime());

      // set previous of selected date
      d.setDate(d.getDate() -1);

      // Get new copy of options in order the date picker detect change
      let copy: INgxMyDpOptions = this.getCopyOfEndDateOptions();
      copy.disableUntil = {year: d.getFullYear(), 
                           month: d.getMonth() + 1, 
                           day: d.getDate()};
      this.endDate = copy;
    }else{
        let copy: INgxMyDpOptions = this.getCopyOfEndDateOptions();
        copy.disableUntil = {year: 0, month: 0, day: 0}
        this.endDate = copy;
    }
  }

    onEndDateChanged(event: IMyDateModel) {
      if(event.jsdate != null)
      {
         let d: Date = new Date(event.jsdate.getTime());

      // set previous of selected date
      d.setDate(d.getDate() + 1);

      // Get new copy of options in order the date picker detect change
      let copy: INgxMyDpOptions = this.getCopyOfStartDateOptions();
      copy.disableSince = {year: d.getFullYear(), 
                           month: d.getMonth() + 1, 
                           day: d.getDate()};
      this.startDate = copy;
        // end date changed...
      }
      else{
        let copy: INgxMyDpOptions = this.getCopyOfStartDateOptions();
        copy.disableSince = {year: 0, month: 0, day: 0}
        this.startDate = copy;
    }
     } 
    getCopyOfEndDateOptions(): INgxMyDpOptions {
        return JSON.parse(JSON.stringify(this.endDate));
    }
     getCopyOfStartDateOptions(): INgxMyDpOptions {
        return JSON.parse(JSON.stringify(this.startDate));
    }

    submitForm(){
    this.loader =true;
    //console.log(this.myForm.value);
   // let parameters:any = this.myForm.value.parameters;

     
    var filterdata={
        actFromDt:this.fildata.fromDate.formatted,
        actToDt:this.fildata.toDate.formatted, 
    }
    console.log('filterdata',filterdata)

    
      
    this.userDashService.updateUser(filterdata).subscribe(
       data => {
         console.log('receivedData',data);
         this.users=data.data[0]
         this.loader=false;
         console.log('users',this.users);
         for(let i=0;i<this.users.length;i++)
                  {
                      this.users[i].id =i+1;
                  }
                  this.dataset = this.users;
                  this.show=true;
                  this.loader =false;
       },
       error => {
        this.loader =false;
         console.error("Error !");
         //return Observable.throw(error);
       }
    ); 


 }
  back(){
      
      this.router.navigate(['pages/evaluate/reporting/learning']);
  }
}
