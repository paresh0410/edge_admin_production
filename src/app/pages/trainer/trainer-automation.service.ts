import { Injectable } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class TrainerAutomationServiceProvider {
  trainerId: any;

  get_dashboard_assess_data: any;
  get_npa_data:any;
  get_feedback_data: any;
  get_TA_program: any = [];
  show_data_on_level:string;
  show_data_on_level2:string;
  dataFromAttFeedAss: boolean = false;

  attendanceData: any = {};
  feedbackData: any = {};
  assessmentData: any = {};

  lastActiveTabLearner: boolean = false;
  lastActiveTabLearnerIndex: any;
  lastActiveTabLearnerContentData: any;

  lastActiveTabTrainer: boolean = false;
  lastActiveTabTrainerIndex: any;
  lastActiveTabTrainerContentData: any;

  level1_Data: any = [];

  batchData: any = {
    batchCourseId: null,
    batchCourseName: null,
    batchCourseDescription: null,
    trainerCourseId: null,
    trainerCourseName: null,
    trainerCourseDescription: null,
  };
  trainerworkflow: any = [];
  // batchCourseId:any = 83;
  // batchCourseName:any="Demo Batch 16";
  // batchCourseDescription:any="This is Demo Batch 16 May";
  // trainerCourseId:any = 82;
  // trainerCourseName:any = "Trainer Workflow";
  // trainerCourseDescription:any="This is Trainer Workflow";

  constructor(private http: HttpClient, private toastr: ToastrService) { }

  getDashboardData(url, param) {
    return new Promise(resolve => {
      this.http
        .post(url, param)
        // .map(res => res.json())
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            resolve(err);
            if (err) {
              this.toastr.error(
                "Please check server connection",
                "Server Error!"
              );
            }
          }
        );
    });
  }

  getParticipant(url, param) {
    return new Promise(resolve => {
      this.http
        .post(url, param)
        // .map(res => res.json())
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            resolve(err);
            if (err) {
              this.toastr.error(
                "Please check server connection",
                "Server Error!"
              );
            }
          }
        );
    });
  }

  getParticipantAttendance(url, param) {
    return new Promise(resolve => {
      this.http
        .post(url, param)
        // .map(res => res.json())
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            resolve(err);
            if (err) {
              this.toastr.error(
                "Please check server connection",
                "Server Error!"
              );
            }
          }
        );
    });
  }

  getAssessmentData(url, param) {
    return new Promise(resolve => {
      this.http
        .post(url, param)
        // .map(res => res.json())
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            resolve(err);
            if (err) {
              this.toastr.error(
                "Please check server connection",
                "Server Error!"
              );
            }
          }
        );
    });
  }

  getFeedbackData(url, param) {
    return new Promise(resolve => {
      this.http
        .post(url, param)
        // .map(res => res.json())
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            resolve(err);
            if (err) {
              this.toastr.error(
                "Please check server connection",
                "Server Error!"
              );
            }
          }
        );
    });
  }

  insertDeleteAttendance(url, param) {
    return new Promise(resolve => {
      this.http
        .post(url, param)
        // .map(res => res.json())
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            resolve(err);
            if (err) {
              this.toastr.error(
                "Please check server connection",
                "Server Error!"
              );
            }
          }
        );
    });
  }

  getCourseModules(url, param) {
    return new Promise(resolve => {
      this.http
        .post(url, param)
        // .map(res => res.json())
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            resolve(err);
            if (err) {
              this.toastr.error(
                "Please check server connection",
                "Server Error!"
              );
            }
          }
        );
    });
  }

  enrolTrainer(url, param) {
    return new Promise(resolve => {
      this.http
        .post(url, param)
        // .map(res => res.json())
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            resolve(err);
            if (err) {
              this.toastr.error(
                "Please check server connection",
                "Server Error!"
              );
            }
          }
        );
    });
  }

  // getLevel1Feedback(url, param) {
  //   return new Promise(resolve => {
  //     this.http
  //       .post(url, param)
  //       // .map(res => res.json())
  //       .subscribe(
  //         data => {
  //           resolve(data);
  //         },
  //         err => {
  //           resolve(err);
  //           if (err) {
  //             this.toastr.error(
  //               "Please check server connection",
  //               "Server Error!"
  //             );
  //           }
  //         }
  //       );
  //   });
  // }

  // getLevel2Feedback(url, param) {
  //   return new Promise(resolve => {
  //     this.http
  //       .post(url, param)
  //       // .map(res => res.json())
  //       .subscribe(
  //         data => {
  //           resolve(data);
  //         },
  //         err => {
  //           resolve(err);
  //           if (err) {
  //             this.toastr.error(
  //               "Please check server connection",
  //               "Server Error!"
  //             );
  //           }
  //         }
  //       );
  //   });
  // }


  // getLevel1Assess(url, param) {
  //   return new Promise(resolve => {
  //     this.http
  //       .post(url, param)
  //       // .map(res => res.json())
  //       .subscribe(
  //         data => {
  //           resolve(data);
  //         },
  //         err => {
  //           resolve(err);
  //           if (err) {
  //             this.toastr.error(
  //               "Please check server connection",
  //               "Server Error!"
  //             );
  //           }
  //         }
  //       );
  //   });
  // }

  // getLevel2Assess(url, param) {
  //   return new Promise(resolve => {
  //     this.http
  //       .post(url, param)
  //       // .map(res => res.json())
  //       .subscribe(
  //         data => {
  //           resolve(data);
  //         },
  //         err => {
  //           resolve(err);
  //           if (err) {
  //             this.toastr.error(
  //               "Please check server connection",
  //               "Server Error!"
  //             );
  //           }
  //         }
  //       );
  //   });
  // }

  get(url, param) {
    return new Promise(resolve => {
      this.http.post(url, param)
        // .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        }, err => {
          resolve(err);
          if (err) {
            this.toastr.error("Please check server connection", "Server Error!");
          }
        });
    });
  }

  getProgramList(url, param) {
    return new Promise(resolve => {
      this.http
        .post(url, param)
        // .map(res => res.json())
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            resolve(err);
            if (err) {
              this.toastr.error(
                "Please check server connection",
                "Server Error!"
              );
            }
          }
        );
    });
  }

  getTrainerList(url) {
    return new Promise(resolve => {
      this.http
        .post(url,{})
        // .map(res => res.json())
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            resolve(err);
            if (err) {
              this.toastr.error(
                "Please check server connection",
                "Server Error!"
              );
            }
          }
        );
    });
  }
  // getFeedbackSection(url, param) {
  //   return new Promise(resolve => {
  //     this.http
  //       .post(url, param)
  //       // .map(res => res.json())
  //       .subscribe(
  //         data => {
  //           resolve(data);
  //         },
  //         err => {
  //           resolve(err);
  //           if (err) {
  //             this.toastr.error(
  //               "Please check server connection",
  //               "Server Error!"
  //             );
  //           }
  //         }
  //       );
  //   });
  // }

  getLevel1(url, param){
    return new Promise(resolve => {
      this.http
        .post(url, param)
        // .map(res => res.json())
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            resolve(err);
            if (err) {
              this.toastr.error(
                "Please check server connection",
                "Server Error!"
              );
            }
          }
        );
    });
  }

  getLevel2(url, param){
    return new Promise(resolve => {
      this.http
        .post(url, param)
        // .map(res => res.json())
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            resolve(err);
            if (err) {
              this.toastr.error(
                "Please check server connection",
                "Server Error!"
              );
            }
          }
        );
    });
  }

  getIntialTrainerDashboard(url, param){
    return new Promise(resolve => {
      this.http
        .post(url, param)
        // .map(res => res.json())
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            resolve(err);
            if (err) {
              this.toastr.error(
                "Please check server connection",
                "Server Error!"
              );
            }
          }
        );
    });
  }

}
