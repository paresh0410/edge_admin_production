import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { ModuleActivityContainerComponent } from './module-activity-container/module-activity-container.component';
import { AttendanceDataComponent } from './attendance-data/attendance-data.component';
import { FeedbackDataComponent } from './feedback-data/feedback-data.component';
import { AssessmentDataComponent } from './assessment-data/assessment-data.component';
import { TrainerDashboardComponent } from './trainer-dashboard/trainer-dashboard.component';
import { Level1Component } from './level1/level1.component';
import { Level2Component } from './level2/level2.component';
import { TrainerDetailsComponent } from './trainer-details/trainer-details.component';

const routes: Routes = [
  {path: '', component: TrainerDashboardComponent},
    {
      path: 'level-1',
      component: Level1Component,
    },
    {
      path: 'level-2',
      component: Level2Component,
    },
    {
      path: 'feedback',
      component: FeedbackDataComponent,
    },
    {
      path: 'attendance',
      component: AttendanceDataComponent,
    },
    {
      path: 'assessment',
      component: AssessmentDataComponent,
    },
    { path: 'trainer-details',
    component: TrainerDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TrainerDashboardRoutingModule { }
