import { TrainerDashboardModule } from './trainer-dashboard.module';

describe('TrainerDashboardModule', () => {
  let trainerDashboardModule: TrainerDashboardModule;

  beforeEach(() => {
    trainerDashboardModule = new TrainerDashboardModule();
  });

  it('should create an instance', () => {
    expect(trainerDashboardModule).toBeTruthy();
  });
});
