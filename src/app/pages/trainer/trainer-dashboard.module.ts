import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';
import { DemoMaterialModule } from '../material-module';

import { TrainerDashboardComponent } from './trainer-dashboard/trainer-dashboard.component';
import { Level1Component } from './level1/level1.component';
import { Level2Component } from './level2/level2.component';
import { TruncateModule } from 'ng2-truncate';
import { TrainerDetailsComponent } from './trainer-details/trainer-details.component';
import { TrainerCoursesContentComponent } from './trainer-courses-content/trainer-courses-content.component';
import { ParticipantsComponent } from './participants/participants.component';
import { TrainerParticipantCourseContentComponent } from './trainer-participant-course-content/trainer-participant-course-content.component';
import { AttendanceDataComponent } from './attendance-data/attendance-data.component';
import { FeedbackDataComponent } from './feedback-data/feedback-data.component';
import { AssessmentDataComponent } from './assessment-data/assessment-data.component';
import { TrainerTAevalutionfbComponent } from './trainer-taevalutionfb/trainer-taevalutionfb.component';
import { StarRatingModule } from 'angular-star-rating';
import { TrainerDashboardRoutingModule } from './trainer-dashboard-routing.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DemoMaterialModule,
    TruncateModule,
    StarRatingModule,
    TrainerDashboardRoutingModule,
  ],
  declarations: [
    TrainerDashboardComponent,
    Level1Component,
    Level2Component,
    TrainerDetailsComponent,
    TrainerCoursesContentComponent,
    ParticipantsComponent,
    TrainerParticipantCourseContentComponent,
    AttendanceDataComponent,
    FeedbackDataComponent,
    AssessmentDataComponent,
    TrainerTAevalutionfbComponent
  ]
})
export class TrainerDashboardModule { }
