import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { TrainerAutomationServiceProvider } from "../trainer-automation.service";
import { webApi } from "../../../service/webApi";
import { DatePipe } from "@angular/common";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from 'ngx-toastr';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
// import { CallCoachingService } from "../../service/call-coaching.service";

@Component({
  selector: "ngx-trainer-dashboard",
  templateUrl: "./trainer-dashboard.component.html",
  styleUrls: ["./trainer-dashboard.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class TrainerDashboardComponent implements OnInit {
  showhide: any;
  openclose: any;
  arrow: any;
  feedbackData: any = [];
  assessmentScore: any = [];
  npmScore: any = [];
  batchArray: any = [];
  batchcount: any = [];
  instanceArray: any = [];
  trainerData: any = [];
  upcomingbatch: any = [];
  ongoingbatch: any = [];
  trainerList:any = [];
  feedbacktext:SafeHtml;
  showfb:boolean = false;
  // callIcon: boolean = true;
  // tttworkFlowIcon: boolean = true;
  // eepIcon: boolean = true;
  // dontShowSide: boolean = true;
  // callData: any;

  // {
  //   name: "batch name 1",
  //   project: "pro 1",
  //   venue: "abc",
  //   date: "12/06/2019"
  // },
  // {
  //   name: "batch name 1",
  //   project: "pro 1",
  //   venue: "abc",
  //   date: "12/06/2019"
  // },
  // {
  //   name: "batch name 1",
  //   project: "pro 1",
  //   venue: "abc",
  //   date: "12/06/2019"
  // },
  // {
  //   name: "batch name 1",
  //   project: "pro 1",
  //   venue: "abc",
  //   date: "12/06/2019"
  // },
  // {
  //   name: "batch name 1",
  //   project: "pro 1",
  //   venue: "abc",
  //   date: "12/06/2019"
  // }
  selectedProgram = {
    id: null,
    programName: '',
  };
  // selectedProgramName = 'Select Program';

  selectedTrainer: any = {
    trainerId: null,
    trainerName: ''
  };
  selectedTrainerName = 'Please Select trainer';

  // myTasks: any = [
  //   {
  //     id: 1,
  //     statName: "Total task completed",
  //     percent: 92
  //   },
  //   {
  //     id: 2,
  //     statName: "Tasks-on-time",
  //     percent: 88
  //   },
  //   {
  //     id: 3,
  //     statName: "Tasks late",
  //     percent: 18
  //   }
  // ];
  sessiondata: any = [];

  // tttList = [
  //   {
  //     id: "1",
  //     title: "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
  //     discr: "Is simply dummy text of the printing and typesetting industry"
  //   },
  //   {
  //     id: "2",
  //     title: "Ipsum is simply dummy text of the printing and typesetting industry",
  //     discr: "Is simply dummy text of the printing and typesetting industry"
  //   },
  //   {
  //     id: "3",
  //     title: "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
  //     discr: "Is simply dummy text of the printing and typesetting industry"
  //   },
  //   {
  //     id: "4",
  //     title: "Ipsum is simply dummy text of the printing and typesetting industry",
  //     discr: "Is simply dummy text of the printing and typesetting industry"
  //   },
  //   {
  //     id: "5",
  //     title: "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
  //     discr: "Is simply dummy text of the printing and typesetting industry"
  //   },
  //   {
  //     id: "6",
  //     title: "Ipsum is simply dummy text of the printing and typesetting industry",
  //     discr: "Is simply dummy text of the printing and typesetting industry"
  //   }
  // ];

  trainerDataAssess: any;
  trainerId: any;
  userDetails: any;
  tenantId: any;
  hideback: boolean = false;
  program: any = [{ id: null, programName: 'All Programs'}];
  // this.program.push({);
  nodataFound = false;

  disabledProgram = true;
  // selectTrainerFlag = false;


  constructor(
    private router: Router,
    private routes: ActivatedRoute,
    private datePipe: DatePipe,
    // private callservice: CallCoachingService,
    private toaster: ToastrService,
    private TAServiceProvider: TrainerAutomationServiceProvider,
    private spinner: NgxSpinnerService,
    private sanitizer: DomSanitizer
    // private cdf: ChangeDetectorRef,
  ) {
    // this.selectedTrainer = this.trainerList[0];
    // if (localStorage.getItem("userOtherDetails")) {
    //   let userOtherDetails = JSON.parse(
    //     localStorage.getItem("userOtherDetails")
    //   );
    //   console.log("userOtherDetails", userOtherDetails);
    //   let userOthersDetails = userOtherDetails[1];

    //   for (let i = 0; i < userOthersDetails.length; i++) {
    //     if (userOthersDetails[i].roleId == 7) {
    //       this.trainerId = userOthersDetails[i].fieldmasterId;
    //     }
    //   }
    //   console.log("trainerId", this.trainerId);
    // }



    // this.tenantId = this.TAServiceProvider.tenantId;
    //this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
    // console.log("this.tenantId", this.tenantId);

    //this.getDashboardData();



  }

  ngOnInit() {
    this.getIntialDashboardData();
    this.getTrainerList();
    this.selectedProgram = this.program[0];
    console.log('Selected Program',this.selectedProgram);
    // this.getProgramList();
  }
  // pass_dashboard_data: any;


  getTrainerList(){

    let url = webApi.domain + webApi.url.getTrainerList;
    this.TAServiceProvider.getTrainerList(url)
      .then((result: any) => {
        this.spinner.show();
        console.log("Trainer list", result);
        // this.trainerId = 57;
         console.log('SelectedTrainer ==>', this.selectedTrainer);
        if(result.data.length > 0){
          this.trainerList = result.data;
          this.selectedTrainer =  result.data[0];
          console.log('SelectedTrainer ==>', this.selectedTrainer);
          if(this.selectedTrainer.trainerName=='All'){
            this.disabledProgram = true;
           // this.toaster.warning("Select a different trainer",'Attention')
          }
        }
        // else{
        //   this.trainerList = [{
        //     trainerId: null,
        //     trainerName: 'Select Trainer',
        //   }]
        // }
        if(this.trainerList.length < 0){
          this.nodataFound = true;

        }
        this.spinner.hide();
      }).catch(result => {
        console.log('RESULT', result);
        this.nodataFound = true;
        this.spinner.hide();
      })
  }

  goto(itemId) {
    console.log(itemId);
    // if (itemId == 0) {
    //   this.gotoTrainerDashboardDetail();
    // }
    this.TAServiceProvider.trainerId = this.trainerId;
    if(itemId == 3){
      this.TAServiceProvider.show_data_on_level = 'feedback';
      this.gotoTrainerDashboardDetail('feedback');
    }
    if(itemId == 4){
      this.TAServiceProvider.show_data_on_level = 'quiz';
      this.gotoTrainerDashboardDetail('quiz');
    }
  }

  // gotoCal() {
  //   this.router.navigate(["pages/calendar"]);
  // }

  gotoTrainerDashboardDetail(flag) {

    if(flag == 'feedback')
    {
      this.TAServiceProvider.get_feedback_data = this.trainerData[3].cnt;
      this.TAServiceProvider.get_npa_data = this.trainerData[2];
      this.TAServiceProvider.get_TA_program = this.selectedProgram;
      this.router.navigate(["level-1"], { relativeTo: this.routes });
    }
    if(flag == 'quiz'){
      this.TAServiceProvider.get_feedback_data = this.trainerData[3].cnt;
      this.TAServiceProvider.get_npa_data = this.trainerData[2];
      this.TAServiceProvider.get_TA_program = this.selectedProgram;
      this.TAServiceProvider.get_dashboard_assess_data = this.assessmentScore;
      this.router.navigate(["level-1"], { relativeTo: this.routes });
    }

    // if(this.feedbackData.length > 0){
    //   this.TAServiceProvider.get_feedback_data = this.feedbackData[0].cnt;
    // }


  }

  lastcon: any;
  batchCourseId: any;
  batchCourseName: any;
  batchCourseDesc: any;
  batchesFound: boolean;
  trainerCourseId: any;
  trainerCourseName: any;
  trainerCourseDesc: any;
  batchEnrolDate: any;
  workflow: any = [];
  batchNames: any = [];
  assessmentData: any = [];
  upcomingcount: any = [];
  batchConducted: any;
  participantAttended: any;
  batchPlanned: any;
  batches: any;
  batchFound: boolean = false;
  dashboardData;
  ccData: any = {};
  getDashboardData(program) {

    this.spinner.show();
    let url = webApi.domain + webApi.url.getTrainerDashboardData;
    let param = {
      pgmId: program.id,
      trainId: this.trainerId,
      // tId: this.tenantId
    };
    console.log('param', param);
    this.TAServiceProvider.getParticipant(url, param)
      .then((result: any) => {

        console.log("RESULT Success===>", result);
        try {
          let response = result.data;
          this.prepareDashboardData(response);

        } catch (e) {
          console.log(e);
          this.spinner.hide();
        }
      })
      .catch(result => {
        this.spinner.hide();
        console.log("RESULT Error===>", result);
      });
  }

  formatDate(date) {
    let d = new Date(date);
    let formatted = this.datePipe.transform(d, "dd-MMM-yy");
    return formatted;
  }

  gotoTrainerDetails(batch) {
    if(this.selectedProgram.id != null || this.selectedProgram.programName != "All Programs"){
      console.log("batch data", batch);
      this.TAServiceProvider.batchData = batch;
      // this.TAServiceProvider.batchData.batchCourseId = batch.batchId;
      // this.TAServiceProvider.batchData.trainerCourseId = batch.courseId;
      // this.TAServiceProvider.batchData.batchCourseName = batch.batchname;
      // this.TAServiceProvider.batchData.batchCourseDescription = batch.batchsummary;
      // this.TAServiceProvider.batchData.trainerCourseId = batch.batchId;
      // this.TAServiceProvider.batchData.trainerCourseName = batch.batchName;
      // this.TAServiceProvider.batchData.trainerCourseDescription = batch.coursesummary;
      // this.TAServiceProvider.dataFromAttFeedAss = false;
      this.router.navigate(["trainer-details"], { relativeTo: this.routes });
    }
    else{
      this.toaster.warning("Select a different trainer",'Attention')
    }

  }
  // gotoworkflowDetails(workflow) {
  //   this.hideback = false;
  //   this.TAServiceProvider.trainerworkflow = workflow;
  //   this.router.navigate(["../ttt-trainer-detail"], {
  //     relativeTo: this.routes,
  //   });
  // }
  // gotocoachingnomination() {
  //   this.callservice.frompage = 'trainer';
  //   this.router.navigate(["../train-nomination"], {
  //     relativeTo: this.routes,
  //   });
  // }

  visible() {
    this.showhide = !this.showhide;
    this.openclose = false;
    // this.arrow = !this.arrow;
  }
  visible1() {
    this.openclose = !this.openclose;
    this.showhide = false;
  }

  // openNav() {
  //   document.getElementById("mySidenav").style.width = "120px";
  // }
  // closeNav() {
  //   document.getElementById("mySidenav").style.width = "0";
  // }

  // gotttpopup() {
  //   this.hideback = true;
  // }
  // closetttpop() {
  //   this.hideback = false;
  // }

  // checkSideMenuExist() {
  //   // const callData = this.dashboardData[8];
  //   console.log('this.callIcon ==>', this.callData);
  //   if (this.callData[0].cnt === 0) {
  //     this.callIcon = false;
  //   }
  //   // if (this.instanceArray.length === 0) {
  //   //    this.tttworkFlowIcon = false;
  //   // }
  //   if (!this.callIcon && !this.tttworkFlowIcon) {
  //     this.dontShowSide = false;
  //   }
  //   console.log('this.callIcon ==>', this.callIcon);
  //   console.log('this.tttworkFlowIcon ==>', this.tttworkFlowIcon);
  //   // console.log('this.eepIcon ==>', this.eepIcon);
  //   console.log('this.dontShowSide ==>', this.dontShowSide);
  //   // this.cdf.detectChanges();
  // }


  getProgramList(data) {
    this.program = [];
    let url = webApi.domain + webApi.url.programList;
    let param = {
      trainId: data.trainerId,
      // tId: this.tenantId
    };
    this.TAServiceProvider.getProgramList(url, param)
      .then((result: any) => {
        if(result.data.length > 0){
          this.program = result.data;
        } else{
          this.program = [{
            programId: null,
            programName: 'Select Program',
          }];
          // this.selectTrainerFlag = true;
        }


        // if (this.program.length !== 0) {
        //   this.nodataFound = false;

          this.selectedProgram = {
            id: this.program[0].programId,
            programName: this.program[0].programName,
          };
          console.log('Selected Program',this.selectedProgram);
          // this.selectedProgramName =  this.selectedProgram.programName;
          this.getDashboardData(this.selectedProgram);
        // } else if (this.program.length === 0) {
        //   this.nodataFound = true;
        // }

        console.log("Program list", this.program);
      }).catch(result => {
        console.log('RESULT', result);
      })
  }

  getSelectedProgramData(item) {
    console.log("Program selected", item);
    this.selectedProgram = {
      id: item.programId,
      programName: item.programName
    };
    console.log('Selected Program',this.selectedProgram);
    this.getDashboardData(this.selectedProgram);
  }


  getSelectedTrainerData(item) {
    console.log("Trainer selected", item);
    console.log('TrainerList ==>', this.trainerList);
  
    if(item.trainerId == null){
      this.getIntialDashboardData();
      this.disabledProgram = true;
      this.toaster.warning("Select a different trainer",'Attention')
      this.program =[];
      this.program.push({
        id: null,
        programName: 'All Programs',
      });
      this.selectedProgram = this.program[0];
      console.log('Selected Program',this.selectedProgram);
    }
    else{
      this.selectedTrainer = {
        trainerId: item.trainerId,
        trainerName: item.trainerName
      };
      this.trainerId = item.trainerId;
      this.selectedTrainerName = item.trainerName;
      // this.selectTrainerFlag = true;
      this.getProgramList(this.selectedTrainer);
      this.disabledProgram = false;
    }
    this.TAServiceProvider.trainerId = item.trainerId;
  }

  getIntialDashboardData(){
    this.spinner.show();
    let url = webApi.domain + webApi.url.getIntialTrainerDashbaordData;
    let param ={};
    this.TAServiceProvider.getProgramList(url, param)
      .then((result: any) => {
        if(result.data.length != 0){
          console.log('Overall Trainer Data ==>', result);
          this.prepareDashboardData(result.data);
          this.spinner.hide();
        } else{
          this.toaster.info('Data not found');
          this.spinner.hide();
          this.nodataFound = true;
        }
        // console.log("Program list", this.program);
      }).catch(result => {
        this.spinner.hide();
        console.log('RESULT', result);
      })
  }

  prepareDashboardData(response){
    this.trainerData=[];
    this.batchcount = {};
    this.upcomingbatch =[];
    this.upcomingcount ={};
    this.sessiondata =[];
    this.upcomingbatch =[];
    this.assessmentScore ={};
    if(response.total_no_batches_cnt){
      this.trainerData.push(response.total_no_batches_cnt[0]);
    }
    if(response.participant_attended){
      this.trainerData.push(response.participant_attended[0]);
    }
    if(response.nps){
      this.trainerData.push(response.nps);
    }
    // if(response.ongoing_batches_cnt){
    //   this.batchcount = response.ongoing_batches_cnt;
    // }
    if(response.ongoing_batches_list.length !=0){
      this.ongoingbatch = response.ongoing_batches_list;
    }
    // if(response.upcoming_batches_list){
    //   this.upcomingcount = response.upcoming_batches_cnt;
    // }
    if(response.upcoming_batches_list.length !=0){
      this.upcomingbatch = response.upcoming_batches_list;
    }
    if(response.feedback_question_avg.length !=0){
      this.sessiondata = response.feedback_question_avg;
    }
    if(response.feedback_avg.length !=0){
      let fb = response.feedback_avg[0];
      this.trainerData.push(fb);
    }
    // this.upcomingbatch = response.upcoming_batches_list;
    // this.sessiondata = response.feedback_question_avg;

    // if(fb.cnt != 0){
    //   // this.feedbackData.push(fb);

    // }
    // this.assessmentScore = fb.score;
    if(response.quiz_avg.length !=0){
      let qb = response.quiz_avg[0];
      qb.cnt = qb.score;
      qb.label = 'Avg Assessment Score';
      this.trainerData.push(qb);
      this.assessmentScore = qb.score;
    }


    this.spinner.hide();
    // if(qb.score){
    //   qb.cnt = qb.score;
    //   qb.label = 'Avg Assessment Score';
    //   this.trainerData.push(qb);
    //   // this.feedbackData.push(qb);
    //   this.assessmentScore = qb.score;
    // }
    // console.log('Trainer Data ==>', this.trainerData);
    // this.callData = response.cc;
    // this.instanceArray = response.ttt;

    // this.checkSideMenuExist();
  }

  makefeedback(question): SafeHtml {
    if(question){
      var fb = question.replace(/<[^>]+>/g, '');
      fb=fb.replace(/&nbsp;/g, '');
      if(fb.length > 40){
        return fb.substring(0, 42);
      }
      else{
        return fb;
      }
    }
  }

  readfb(fbtext) {
    this.feedbacktext = this.sanitizer.bypassSecurityTrustHtml(fbtext);
    this.showfb = true;
  }
  closefb(){
    this.showfb = false;
  }
}
