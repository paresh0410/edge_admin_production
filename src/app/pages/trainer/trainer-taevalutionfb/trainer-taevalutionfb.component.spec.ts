import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainerTAevalutionfbComponent } from './trainer-taevalutionfb.component';

describe('TrainerTAevalutionfbComponent', () => {
  let component: TrainerTAevalutionfbComponent;
  let fixture: ComponentFixture<TrainerTAevalutionfbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrainerTAevalutionfbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainerTAevalutionfbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
