import { Component, OnInit } from '@angular/core';
import { TrainerAutomationServiceProvider } from '../trainer-automation.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { webApi } from '../../../service/webApi';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
@Component({
  selector: 'ngx-trainer-taevalutionfb',
  templateUrl: './trainer-taevalutionfb.component.html',
  styleUrls: ['./trainer-taevalutionfb.component.scss']
})
export class TrainerTAevalutionfbComponent implements OnInit {
  userDetails: any = [];
  trainerbatch: any = [];
  batchCourseId: any = [];
  tenantId: any;
  noParticipants: boolean = false;
  giveFeedback: boolean;
  participants: any = [];
  participantemp: any = [];
  questions: any = [];
  title: any;
  mandatoryQuesString: any = [];
  constructor(public TAServiceProvider: TrainerAutomationServiceProvider,
    public spinner: NgxSpinnerService, private Toastr: ToastrService, private sanitizer: DomSanitizer) {
    this.userDetails = JSON.parse(localStorage.getItem("userdetail"));
    this.trainerbatch = this.TAServiceProvider.feedbackData;
    console.log(this.trainerbatch);
    if (this.trainerbatch.stepId === 10) {
      this.title = 'Mock';
    } else if (this.trainerbatch.stepId === 11) {
      this.title = 'Assessment';
    } else if (this.trainerbatch.stepId === 14) {
      this.title = 'Mock';
    } else {
      this.title = 'Viva Feedback';
    }
    this.batchCourseId = this.TAServiceProvider.batchData.batchId;
    // this.coursecontentdata = this.tttService.coursecontent;
    // console.log(this.coursecontentdata);
    this.userDetails = JSON.parse(localStorage.getItem('LoginResData'));
		if(this.userDetails.data.data){
		  this.tenantId = this.userDetails.data.data.tenantId;
		}
    this.getparticipants();
  }

  ngOnInit() {
    console.log("NIKHIL");
  }

  getparticipants() {
    this.spinner.show();
    let url = webApi.domain + webApi.url.getAllUshaParticipants;
    let param = {
      "reqFrom": "WEB",
      "participantCourseId": this.batchCourseId,
      "tId": this.tenantId,
      "lmt": null,
      "pNo": null
    }
    this.TAServiceProvider.getParticipant(url, param)
      .then((result: any) => {
        this.spinner.hide();
        console.log('RESULT Success participant===>', result);
        if (result.data.length == 0) {
          this.noParticipants = true;
        } else {
          this.noParticipants = false;
          this.participants = result.data;
        }

      }).catch(result => {
        this.spinner.hide();
        console.log('RESULT Error participant===>', result);
      })
  }

  viewFeedback(element) {
    this.participantemp = element;
    console.log(this.participantemp);
    this.giveFeedback = true;
    this.fbquestion();
  }
  goback() {
    console.log("feedback Data >>");
    this.giveFeedback = false;
  }
  backtobatch(){
    window.history.back();
  }
  fbquestion() {
    var param = {
      fId: this.trainerbatch.feedbackId,
      cId: this.trainerbatch.courseId,
      mId: this.trainerbatch.moduleId,
      actId: this.trainerbatch.activityId,
      // fId: this.coursecontentdata.,
      tId: this.userDetails.tenantId,
      empId: this.participantemp.empId,
    };
    var url = webApi.domain + webApi.url.getEvalutionfbquestion;
    this.TAServiceProvider
      .get(url, param)
      .then((result: any) => {
        this.questions = result.data;
        for (var i = 0; i < this.questions.length; i++) {
          this.questions[i].qId = i + 1;
          if (
            this.questions[i].questionTypeId == 2 ||
            this.questions[i].questionTypeId == 3
          ) {
            for (var j = 0; j < this.questions[i].optionList.length; j++) {
              this.questions[i].optionList[j].sFlag = false;
            }
          } else if (this.questions[i].questionTypeId == 1) {
            this.questions[i].questionPlaceholder =
              "Minimum " +
              this.questions[i].optionList[0].minLength +
              " and maximum " +
              this.questions[i].optionList[0].maxLength +
              " characters";
            this.questions[i].minLen = parseInt(
              this.questions[i].optionList[0].minLength
            );
            this.questions[i].maxLen = parseInt(
              this.questions[i].optionList[0].maxLength
            );
            this.questions[i].optionList[0].sAns = "";
          } else {
            var temp = parseInt(this.questions[i].optionList[0].noStar);
            var tempRatingObj = {};
            var tempRatingArr = [];
            for (var j = 0; j < temp; j++) {
              tempRatingObj = {
                option: j + 1
              };
              tempRatingArr.push(tempRatingObj);
            }
            this.questions[i].rating = tempRatingArr;
            this.questions[i].optionList[0].sRateAns = 0;
          }
        }
        // this.saveFeedbackCompletion("UP");
        console.log("RESULT===>", this.questions);
      })
      .catch(result => {
        console.log("RESULT===>", result);
      });
  }
  selectOption(mainData, index) {
    if (mainData.questionTypeId != 1) {
      for (var j = 0; j < mainData.optionList.length; j++) {
        mainData.optionList[j].sFlag = false;
      }
      if (mainData.optionList[index].sFlag == true) {
        mainData.optionList[index].sFlag = false;
      } else {
        mainData.optionList[index].sFlag = true;
      }
      // console.log('MainData===>',mainData);
      // console.log('Index',index);
    }
  }

  onRateChange(count, index) {
    var count = count.rating;
    for (let i = 0; i < this.questions.length; i++) {
      if (i == index) {
        console.log(this.questions[i]);
        this.questions[i].sRateAns = count;
      }
    }
  }

  submitFeedback() {
    // const mandatFlag = this.checkForMandatoryQues();
    // if (mandatFlag) {
    // var employeeId = localStorage.getItem("employeeId");
    // var userDetails = JSON.parse(localStorage.getItem("userDetails"));
    // var feedId = this.summary.feedbackId;
    // var courseId = this.summary.courseId;
    // var moduleId = this.summary.moduleId;
    var quesId = [];
    var value = [];
    for (var i = 0; i < this.questions.length; i++) {
      if (this.questions[i].optionList) {
        quesId.push(this.questions[i].questionId);
        for (var j = 0; j < this.questions[i].optionList.length; j++) {
          if (
            this.questions[i].questionTypeId === 2 ||
            this.questions[i].questionTypeId === 3
          ) {
            if (this.questions[i].optionList[j].sFlag == true) {
              value.push(this.questions[i].optionList[j].option);
            } else {
            }
          } else if (this.questions[i].optionList[j].sAns) {
            value.push(this.questions[i].optionList[j].sAns);
          } else {
            value.push(this.questions[i].sRateAns);
          }
        }
      }
      // if (this.questions[i].rating) {
      //   quesId.push(this.questions[i].questionId);
      //   for (var j = 0; j < this.questions[i].rating.length; j++) {
      //     if (this.questions[i].rating[j].sRateAns != 0) {
      //       value.push(this.questions[i].rating[j].sRateAns);
      //     }
      //   }
      // }
    }
    var valueStr = value.join("|");
    var quesIdStr = quesId.join("|");
    var param = {
      actId: this.trainerbatch.activityId,
      empId: this.participantemp.empId,
      fId: this.trainerbatch.feedbackId,
      qId: quesIdStr,
      val: valueStr,
      valLen: value.length,
      tId: this.userDetails.tenantId,
      uId: this.userDetails.id,
      cId: this.trainerbatch.courseId,
      mId: this.trainerbatch.moduleId,
    };
    console.log("QUESTIONANSWER===>", param);
    var url = webApi.domain + webApi.url.submitTAevalutionfb;
    this.TAServiceProvider
      .get(url, param)
      .then((result: any) => {
        console.log("RESULT===>", result);
        if (result.type == true) {
          this.Toastr.success(" Submitted Successfully", "Success!");
          // this.saveFeedbackCompletion("Y");
          this.giveFeedback = false;
          this.getparticipants();
        } else {
          this.Toastr.error("Please resubmit ", "Error Occured!");
        }
      })
      .catch(result => {
        console.log("RESULT===>", result);
        this.Toastr.error("Please resubmit ", "Error Occured!");
      });
    // } else {
    //   const mandatString =
    //     "Question(s) " + this.mandatoryQuesString + " are mandatory";
    //   this.Toastr.error(mandatString, "Cannot submit feedback");
    // }
  }
  //////////////////check mendatory question/////////////
  checkForMandatoryQues() {
    this.mandatoryQuesString = null;
    console.log('MandatoryQues===>', this.questions);
    for (let i = 0; i < this.questions.length; i++) {
      let count = 0;
      if (this.questions[i].isMandatory === 1) {
        let temp = this.questions[i];
        for (let j = 0; j < temp.optionList.length; j++) {
          if (temp.questionTypeId === 2 || temp.questionTypeId === 3) {
            if (!temp.optionList[j].sFlag) {
              count++;
            }
            if (count === temp.optionList.length) {
              let unId = i + 1;
              if (this.mandatoryQuesString) {
                this.mandatoryQuesString = this.mandatoryQuesString + ',' + ' ' + unId;
              } else {
                this.mandatoryQuesString = '' + unId + '';
              }
            }
          } else if (temp.questionTypeId === 1) {
            if (temp.optionList[j].sAns === '') {
              count++;
            }
            if (count === temp.optionList.length) {
              let unId = i + 1;
              if (this.mandatoryQuesString) {
                this.mandatoryQuesString = this.mandatoryQuesString + ',' + ' ' + unId;
              } else {
                this.mandatoryQuesString = '' + unId + '';
              }
            }
          } else {
            if (!temp.sRateAns) {
              count++;
            }
            if (count === temp.optionList.length) {
              let unId = i + 1;
              if (this.mandatoryQuesString) {
                this.mandatoryQuesString = this.mandatoryQuesString + ',' + ' ' + unId;
              } else {
                this.mandatoryQuesString = '' + unId + '';
              }
            }
          }
        }
      }
    }
    if (this.mandatoryQuesString) {
      return false;
    } else {
      return true;
    }
    // console.log('UnansweredQues===>', this.mandatoryQuesString);
  }
  makeFeedbackQuestionReady(question): SafeHtml {
    if(question.isMandatory == 1){
      let questionFinal = question.fName + ' <b class="mandatoryQues">*</b>';
      return this.sanitizer.bypassSecurityTrustHtml(questionFinal);
    }else {
      return this.sanitizer.bypassSecurityTrustHtml(question.fName);
    }
  }
}
