import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { TrainerAutomationServiceProvider } from '../trainer-automation.service'


@Component({
  selector: 'ngx-trainer-details',
  templateUrl: './trainer-details.component.html',
  styleUrls: ['./trainer-details.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TrainerDetailsComponent implements OnInit {

  person: any;
  trainingContent: any = [
    {
      activityId: 7,
      activityName: "Activity 1",
      img: "assets/images/open-book-leaf-2.jpg",
      modulename: "Demo Module",
      summary: "This is Demo Module ",
      bookmarked: false
    },
    {
      activityId: 2,
      activityName: "Activity 2",
      img: "assets/images/open-book-leaf-2.jpg",
      modulename: "Demo Module",
      summary: "This is Demo Module ",
      bookmarked: false
    },
    {
      activityId: 3,
      activityName: "Activity 3",
      img: "assets/images/open-book-leaf-2.jpg",
      modulename: "Demo Module",
      summary: "This is Demo Module ",
      bookmarked: false
    },
    {
      activityId: 6,
      activityName: "Activity 4",
      img: "assets/images/open-book-leaf-2.jpg",
      modulename: "Demo Module",
      summary: "This is Demo Module ",
      bookmarked: false
    },
    {
      activityId: 1,
      activityName: "Activity 5",
      img: "assets/images/open-book-leaf-2.jpg",
      modulename: "Demo Module",
      summary: "This is Demo Module ",
      bookmarked: false
    },
  ];

  Assesment_Data: any = [
    { participants: "Unmesh", question1: "Option X", question2: "Option A", question3: "Option X" },
    { participants: "Ashish", question1: "Option y", question2: "Option B", question3: "Option Y" },
    { participants: "Aditya", question1: "Option Z", question2: "Option C", question3: "Option Z" },
    { participants: "Niket", question1: "Option A", question2: "Option X", question3: "Option A" },
    { participants: "Dinesh", question1: "Option B", question2: "Option Y", question3: "Option B" },
    { participants: "Vivek", question1: "Option C", question2: "Option Z", question3: "Option C" },
  ];

  Feedback_Data: any = [
    { participants: "Unmesh", Objective: 3, Content: 4, Trainer: 3, Overall: 4 },
    { participants: "Ashish", Objective: 2, Content: 5, Trainer: 4, Overall: 3 },
    { participants: "Aditya", Objective: 5, Content: 2, Trainer: 2, Overall: 4 },
    { participants: "Niket", Objective: 4, Content: 6, Trainer: 1, Overall: 2 },
    { participants: "Dinesh", Objective: 6, Content: 7, Trainer: 5, Overall: 1 },
    { participants: "Vivek", Objective: 2, Content: 3, Trainer: 6, Overall: 2 },
  ];


  Attendance_Data: any = [
    { ecnNo: 111, name: "Unmesh", designation: "Manager", time: "9:20 pm", attended: false },
    { ecnNo: 111, name: "Aditya", designation: "Developer", time: "9:20 pm", attended: false },
    { ecnNo: 111, name: "Ashish", designation: "Manager", time: "9:20 pm", attended: false },
    { ecnNo: 111, name: "Bhavesh", designation: "Developer", time: "9:20 pm", attended: false },
    { ecnNo: 111, name: "Dinesh", designation: "Developer", time: "9:20 pm", attended: false },
    { ecnNo: 111, name: "Niket", designation: "Developer", time: "9:20 pm", attended: false },
  ];

  displayedColumns: string[] = ['participantName', 'ecnNo', 'phone', 'email', 'designation'];
  participants = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);


  displayedColumns2: string[] = ['participants', 'question1', 'question2', 'question3'];
  assessments = new MatTableDataSource(this.Assesment_Data);

  displayedColumns3: string[] = ['participants', 'Objective', 'Content', 'Trainer', 'Overall'];
  feedback = new MatTableDataSource(this.Feedback_Data);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  // @ViewChild(MatPaginator) paginator: MatPaginator;

  activeTabInd;

  // userId: any;
  tenantId: any;
  trainerCourseId: any;
  trainerCourseName: any;
  trainerCourseDescription: any;
  batchCourseId: any;
  batchCourseName: any;
  batchCourseDescription: any;
  userDetails: any;
  batchdata: any = [];
  constructor(private router: Router, private routes: ActivatedRoute,
     private TAServiceProvider: TrainerAutomationServiceProvider) {

    if (this.TAServiceProvider.dataFromAttFeedAss) {
      this.activeTabInd = 2;
    } else {
      this.activeTabInd = 0;
    }

    // let userDetails = JSON.parse(localStorage.getItem('userDetails'));
    // this.userId = userDetails.id;
    this.userDetails = JSON.parse(localStorage.getItem('LoginResData'));
    if(this.userDetails.data.data){
      this.tenantId = this.userDetails.data.data.tenantId;
    }
    // this.tenantId = this.TAServiceProvider.tenantId;
    this.batchdata = this.TAServiceProvider.batchData;
    // if (this.TAServiceProvider.batchData) {
    // 	this.trainerCourseId = this.TAServiceProvider.batchData.trainerCourseId;
    // 	this.trainerCourseName = this.TAServiceProvider.batchData.trainerCourseName;
    // 	this.trainerCourseDescription = this.TAServiceProvider.batchData.trainerCourseDescription;
    // 	this.batchCourseId = this.TAServiceProvider.batchData.batchCourseId;
    // 	this.batchCourseName = this.TAServiceProvider.batchData.batchCourseName;
    // 	this.batchCourseDescription = this.TAServiceProvider.batchData.batchCourseDescription;
    // }
  }

  ngOnInit() {
    this.participants.paginator = this.paginator;
    // this.assessments.paginator = this.paginator;

  }

  gotoDash() {
    this.router.navigate(['pages/trainer-dashboard']);
  }

  gotoCal() {
    this.router.navigate(['pages/calendar']);
  }

  goBack() {
    this.router.navigate(['../'], { relativeTo: this.routes });
  }

  markattendance(index) {
    if (this.Attendance_Data[index].attended == false) {
      this.Attendance_Data[index].attended = true;
    }
    else {
      this.Attendance_Data[index].attended = false;
    }
  }

}

export interface PeriodicElement {
  participantName: string;
  ecnNo: number;
  phone: number;
  email: string;
  designation: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  { ecnNo: 111, participantName: 'Unmesh', phone: 9837484848, email: 'unmesh@mobeserv.com', designation: 'Manager' },
  { ecnNo: 111, participantName: 'Unmesh', phone: 9837484848, email: 'unmesh@mobeserv.com', designation: 'Manager' },
  { ecnNo: 111, participantName: 'Unmesh', phone: 9837484848, email: 'unmesh@mobeserv.com', designation: 'Manager' },
  { ecnNo: 111, participantName: 'Unmesh', phone: 9837484848, email: 'unmesh@mobeserv.com', designation: 'Manager' },
  { ecnNo: 111, participantName: 'Unmesh', phone: 9837484848, email: 'unmesh@mobeserv.com', designation: 'Manager' },
  { ecnNo: 111, participantName: 'Unmesh', phone: 9837484848, email: 'unmesh@mobeserv.com', designation: 'Manager' },
  { ecnNo: 111, participantName: 'Unmesh', phone: 9837484848, email: 'unmesh@mobeserv.com', designation: 'Manager' },
  { ecnNo: 111, participantName: 'Unmesh', phone: 9837484848, email: 'unmesh@mobeserv.com', designation: 'Manager' },
  { ecnNo: 111, participantName: 'Unmesh', phone: 9837484848, email: 'unmesh@mobeserv.com', designation: 'Manager' },
  { ecnNo: 111, participantName: 'Unmesh', phone: 9837484848, email: 'unmesh@mobeserv.com', designation: 'Manager' }
];

