import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainerParticipantCourseContentComponent } from './trainer-participant-course-content.component';

describe('TrainerParticipantCourseContentComponent', () => {
  let component: TrainerParticipantCourseContentComponent;
  let fixture: ComponentFixture<TrainerParticipantCourseContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrainerParticipantCourseContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainerParticipantCourseContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
