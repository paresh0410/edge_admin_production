import { Component, OnInit } from '@angular/core';
import { webApi } from '../../../service/webApi';
import { TrainerAutomationServiceProvider } from '../trainer-automation.service';
import { Router, ActivatedRoute } from '@angular/router';
// import { CourseServiceProvider } from '../../service/course-service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
	selector: 'trainer-participant-course-content',
	templateUrl: './trainer-participant-course-content.component.html',
	styleUrls: ['./trainer-participant-course-content.component.scss']
})
export class TrainerParticipantCourseContentComponent implements OnInit {
	contentDataArray: any = [];
	noDataFound:boolean = false;
	contentArray: any = [];

	trainerCourseId: any;
	trainerCourseName: any;
	trainerCourseDescription: any;
	batchCourseId: any;
	batchCourseName: any;
	batchCourseDescription: any;
	userDetails: any;

	ActiveTab: any;
	tenantId: any;
	batchdata: any = [];
	activeRole: any;
	// private CSP: CourseServiceProvider,
	constructor(private TAServiceProvider: TrainerAutomationServiceProvider, private router: Router,
		private routes: ActivatedRoute, public spinner: NgxSpinnerService) {

		if (this.TAServiceProvider.lastActiveTabLearner) {
			this.ActiveTab = this.TAServiceProvider.lastActiveTabLearnerIndex;
			this.contentDataArray = this.TAServiceProvider.lastActiveTabLearnerContentData;
		}
		this.batchdata = this.TAServiceProvider.batchData;

		this.activeRole = localStorage.getItem("roleName");
		console.log('this.activeRole:', this.activeRole);
		// if (this.TAServiceProvider.batchData) {
		// 	this.trainerCourseId = this.TAServiceProvider.batchData.trainerCourseId;
		// 	this.trainerCourseName = this.TAServiceProvider.batchData.trainerCourseName;
		// 	this.trainerCourseDescription = this.TAServiceProvider.batchData.trainerCourseDescription;
		// 	this.batchCourseId = this.TAServiceProvider.batchData.batchCourseId;
		// 	this.batchCourseName = this.TAServiceProvider.batchData.batchCourseName;
		// 	this.batchCourseDescription = this.TAServiceProvider.batchData.batchCourseDescription
		// }

		// this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
		// this.tenantId = 1;
		this.userDetails = JSON.parse(localStorage.getItem('LoginResData'));
		if(this.userDetails.data.data){
		  this.tenantId = this.userDetails.data.data.tenantId;
		}
		this.getCourseModuls();
	}

	ngOnInit() {
	}


	tabChanged(data) {
		console.log('DATA--->', data);
		this.ActiveTab = data.id;
		this.TAServiceProvider.lastActiveTabLearner = true;
		this.TAServiceProvider.lastActiveTabLearnerIndex = data.id;
		this.TAServiceProvider.lastActiveTabLearnerContentData = this.contentArray[data.id].list;
		this.contentDataArray = this.contentArray[data.id].list;
		console.log('Tab changed =====>', this.contentDataArray);
	}

	getCourseModuls() {
		this.spinner.show();
		let url = webApi.domain + webApi.url.getCourseModulesTA;
		let param = {
			'cid': this.batchdata.batchId,
			'tId': this.tenantId,
		}
		console.log('trainer side course data param:', param);
		this.TAServiceProvider.getCourseModules(url, param)
			.then((result: any) => {
				this.spinner.hide();
				console.log('RESULT Course modules Success===>', result);
				this.contentArray = result.data;
				if (!this.TAServiceProvider.lastActiveTabLearner) {
					this.ActiveTab = this.contentArray[0].id;
					this.contentDataArray = this.contentArray[0].list;
					console.log("CD===>", this.contentDataArray);
				}
				if(this.contentArray.length == 0){
					this.noDataFound = true;
				}
			}).catch(result => {
				this.spinner.hide();
				this.noDataFound = true;
				console.log('RESULT Course modules Error===>', result);
			})
	}

	goToCourse(data) {
		console.log('activityData:', data);
		if (data.supertypeId == 2 && data.activityTypeId == 6) {
			this.TAServiceProvider.feedbackData = data;
			this.router.navigate(['../feedback'], { relativeTo: this.routes });
		} else if (data.supertypeId == 6 && data.activityTypeId == 12) {
			this.TAServiceProvider.feedbackData = data;
			this.router.navigate(['../TrainerTAevalutionfb'], { relativeTo: this.routes });
		} else if (data.supertypeId == 4 && data.activityTypeId == 9) {
			this.TAServiceProvider.attendanceData = data;
			this.router.navigate(['../attendance'], { relativeTo: this.routes });
		} else if (data.supertypeId == 2 && data.activityTypeId == 5) {
			this.TAServiceProvider.assessmentData = data;
			this.router.navigate(['../assessment'], { relativeTo: this.routes });
		} else {
			var courseDetailSummary = {
				courseTitle: this.batchCourseName,
				courseFrom: 1,
			}
			// this.CSP.setCourse(courseDetailSummary, data);
			// this.router.navigate(['course'], { relativeTo: this.routes });
		}
	}
	bindBackgroundImage(detail) {
		// detail.img = 'assets/images/open-book-leaf-2.jpg';
		// return {'background-image': 'linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)),url(' + detail.img + ')'};
		if (detail.activity_type === 'Quiz') {
			detail.img = 'assets/images/activity_image/quiz.jpg';
			return { 'background-image': 'url(' + detail.img + ')', 'background-position': 'left' };
		} else if (detail.activity_type === 'Feedback') {
			detail.img = 'assets/images/activity_image/feedback.jpg';
			return { 'background-image': 'url(' + detail.img + ')', 'background-position': 'left' };
		} else if (detail.referenceType === 'video' || detail.mimeType === 'video/mp4') {
			detail.img = 'assets/images/activity_image/video.jpg';
			return { 'background-image': 'url(' + detail.img + ')', 'background-position': 'left' };
		} else if (detail.referenceType === 'audio' || detail.mimeType === 'audio/mpeg') {
			detail.img = 'assets/images/activity_image/audio.jpg';
			return { 'background-image': 'url(' + detail.img + ')', 'background-position': 'left' };
		} else if (detail.referenceType === 'application' && detail.mimeType === 'application/zip') {
			detail.img = 'assets/images/activity_image/scrom.jpg';
			return { 'background-image': 'url(' + detail.img + ')', 'background-position': 'left' };
		} else if (detail.referenceType === 'application' && detail.mimeType === 'application/pdf') {
			detail.img = 'assets/images/activity_image/pdf.jpg';
			return { 'background-image': 'url(' + detail.img + ')', 'background-position': 'left' };
		} else if (detail.referenceType === 'kpoint' || detail.mimeType === 'embedded/kpoint') {
			detail.img = 'assets/images/activity_image/video.jpg';
			return { 'background-image': 'url(' + detail.img + ')', 'background-position': 'left' };
		} else if (detail.referenceType === 'image') {
			detail.img = 'assets/images/activity_image/image.jpg';
			return { 'background-image': 'url(' + detail.img + ')', 'background-position': 'left' };
		} else if (detail.referenceType === 'application' && detail.mimeType === 'application/x-msdownload') {
			detail.img = 'assets/images/activity_image/url.jpg';
			return { 'background-image': 'url(' + detail.img + ')', 'background-position': 'left' };
		} else if (detail.formatId == 9) {
			detail.img = 'assets/images/activity_image/practice_file.jpg';
			return { 'background-image': 'url(' + detail.img + ')', 'background-position': 'left' };
		} else {
			detail.img = 'assets/images/open-book-leaf-2.jpg';
			return { 'background-image': 'linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)),url(' + detail.img + ')' };
		}
	}
}
