import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreonboardingComponent } from './preonboarding.component';

describe('PreonboardingComponent', () => {
  let component: PreonboardingComponent;
  let fixture: ComponentFixture<PreonboardingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreonboardingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreonboardingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
