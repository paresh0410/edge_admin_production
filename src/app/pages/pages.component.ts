import { Component } from '@angular/core';
import { NbMenuService } from '@nebular/theme';
// import { MENU_ITEMS } from "./pages-menu";
import { AppService } from '../app.service';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';

import { PopupComponent } from './plan/blended-courses/eep-workflow/add-edit-eep-workflow/popup/popup.component';
@Component({
  selector: 'ngx-pages',
  template: `
    <ngx-sample-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-sample-layout>
  `
})
export class PagesComponent{
  menu = [];
  alive = false;
  subscription :any;
  showdata:any =[];

  constructor(private AppService: AppService, private router: Router) {


    // if(this.AppService.getmenus() && this.AppService.getmenus().length !=0){
    //   this.showdata =this.AppService.getmenus();
    // } else {
    //   if(localStorage.hasOwnProperty('MenuList')){
    //   }

    // }
    this.showdata = JSON.parse(localStorage.getItem('MenuList'));
    this.AppService.setmenus(this.showdata);

    console.log('Side menu Data ===>', this.showdata);
    for (let i = 0; i < this.showdata.length; i++)
    {
      if (this.showdata[i] && Number(this.showdata[i].parentMenuId) === 0)
      {
        const data =
          {
            title: this.showdata[i].menuName,
            icon: this.showdata[i].menuIcon,
            link: this.showdata[i].menuRoute,
            menuId: this.showdata[i].menuId,
          }
          this.menu.push(data);
      }
    }
    // this.menuService.onItemClick().subscribe((event) => {
    //   // if (event.item.title === 'Log out') {

    //   // }
    //   console.log('Menu Clicked', event);
    // });

    // this.menuService.getSelectedItem('menu')
    // .pipe(takeWhile(() => this.alive))
    // .subscribe( (menuBag) => {
    //   console.log('Menu item ===>', menuBag);
    //   // this.selectedItem = menuBag.item.title;
    // });

  }


  ngOnInit() {
    this.subscription = this.router.events.filter(event => event instanceof NavigationEnd).subscribe((val) => {
      console.log('Router Url ===>', this.router.url);
      if(this.router.url.includes('pages/news')) {
        localStorage.setItem('menuId', '6' );
      }
  });
}
  ngdestroy(){
    if (this.subscription){
      this.subscription.unsubscribe();
    }

  }
}
