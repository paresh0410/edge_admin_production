import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { AssetCategoriesService } from './asset-categories.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { webAPIService } from '../../../service/webAPIService';
import { webApi } from '../../../service/webApi';
import { AssetService } from './../asset/asset.service';
import { FormBuilder, FormGroup, Validators, FormControl, NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from "@angular/common/http";
import { ToastrService } from 'ngx-toastr';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { Card } from '../../../models/card.model';
import { SuubHeader } from '../../components/models/subheader.model';

@Component({
  selector: 'ngx-asset-categories',
  templateUrl: './asset-categories.component.html',
  styleUrls: ['./asset-categories.component.scss']
})
export class AssetCategoriesComponent implements OnInit {
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;


  category: any = [];
  storedVisible: any;
  addEditCategoryModal: boolean = false;
  addAction: boolean = false;
  categoryForm: any = {};
  tenantId: any;
  userId: any;
  search: any;
  skeleton = false
  categorylist: any;
  // categoryFormValidation: NgForm;
  searchtext: any;
  enableDisableCategoryModal: boolean = false;
  categoryData: any;
  changetitle: any;
  msg: any;
  cardModify: Card ={
    flag: 'assetCategory',
    titleProp: 'categoryName',
    image: 'categoryPicRef',
    discrption: 'description',
    cnt: 'cnt',
    showImage:   true,
    hoverlable:   true,
    option:   true,
    eyeIcon:   true,
    editIcon:   true,
    bottomDiv:   true,
    bottomTitle:   true,
    bottomDiscription:   true,
    showBottomList:   true,
    showBottomAssets: true,

    btnLabel: 'Details',
  };
  header: SuubHeader  = {
    title:'Asset Category',
    btnsSearch: true,
    searchBar: true,
    dropdownlabel: ' ',
    placeHolder:'Search by category name',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: '',
    btnName3: '',
    btnAdd: 'Add Category',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: true,
    filter: false,
    showBreadcrumb: true,
    breadCrumbList:[
      {
        'name': 'DAM',
        'navigationPath': '/pages/dam',
      },]
  };
  isSave: boolean = false;
  constructor(
    // private toasterService: ToasterService,
    public cdf: ChangeDetectorRef,
    private assetcategoryservice: AssetCategoriesService,
    private spinner: NgxSpinnerService, private toastr: ToastrService,
    protected webApiService: webAPIService, private formBuilder: FormBuilder,
    private assetservice: AssetService, public router: Router,
    public routes: ActivatedRoute, private http1: HttpClient,
    private commonFunctionService: CommonFunctionsService,) {
     // this.spinner.show()
    this.getHelpContent();
    //this.spinner.hide()
    this.tenantId = this.assetcategoryservice.tenantId;

    if (localStorage.getItem('LoginResData')) {
      var userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', userData.data);
      this.userId = userData.data.data.id;
    }
    // this.search={
    //   categoryName:''
    // };
    this.getAllAssetCategories();
  }

  categoryFormValidation: FormGroup;
  submitted = false;

  // ngOnInit() {


    // this.categoryFormValidation = this.formBuilder.group({
    //   catName: ['', [Validators.required, Validators.maxLength(50)]],
    //   desc: ['', [Validators.required, Validators.minLength(20), Validators.maxLength(500)]],
    //   catImg: ['', Validators.required]
    // });
  // }
  // conentHeight = '0px';
  offset: number = 100;
  ngOnInit() {
    // this.conentHeight = '0px';
    // const e = document.getElementsByTagName('nb-layout-column');
    // console.log(e[0].clientHeight);
    // this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
    // if (e[0].clientHeight > 1300) {
    //   this.conentHeight = '0px';
    //   this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
    // } else {
    //   this.conentHeight = String(e[0].clientHeight) + 'px';
    // }
  }
  sum = 40;
  displayArray=[];
  addItems(startIndex, endIndex, _method, category) {
    var demo =[]
    for (let i = startIndex; i <= endIndex; i++) {
      if (category[i]) {
        this.displayArray[_method](category[i]);
        demo.splice(0,0,category[i]);
        this.cdf.detectChanges()
      } else {
        break;
      }
            //console.log(this.displayArray[_method](category[i]),"method");
      // console.log("NIKHIL");
    }
    console.log(demo,"demo")
    console.log(this.noCategory,"nocategory")
      console.log(this.displayArray,"displayArray")

  }
  newArray = [];

  onScroll(event) {

    let element = this.myScrollContainer.nativeElement;
    // element.style.height = '500px';
    // element.style.height = '500px';
    // Math.ceil(element.scrollHeight - element.scrollTop) === element.clientHeight

    // if (element.scrollHeight - element.scrollTop - element.clientHeight < 5) {
    if((Math.ceil(element.scrollHeight - element.scrollTop) - element.clientHeight) < 30){
      if (this.newArray.length > 0) {
        if (this.newArray.length >= this.sum) {
          const start = this.sum;
          this.sum += 100;
          this.addItems(start, this.sum, 'push', this.newArray);
        } else if ((this.newArray.length - this.sum) > 0) {
          const start = this.sum;
          this.sum += this.newArray.length - this.sum;
          this.addItems(start, this.sum, 'push', this.newArray);
        }
      } else {
        if (this.category.length >= this.sum) {
          const start = this.sum;
          this.sum += 100;
          this.addItems(start, this.sum, 'push', this.category);
        } else if ((this.category.length - this.sum) > 0) {
          const start = this.sum;
          this.sum += this.category.length - this.sum;
          this.addItems(start, this.sum, 'push', this.category);
        }
      }


      console.log('Reached End');
    }
  }
  back() {
    this.router.navigate(['../../dam'], { relativeTo: this.routes });
  }

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }

  noCategory: boolean = false;
  noDataFound: boolean = false;
  getAllAssetCategories() {
    // this.spinner.show();
    let param = {
      "tId": this.tenantId
    }
    const _urlGetAllAssetCategories:string = webApi.domain + webApi.url.getAllAssetCategory;
    this.commonFunctionService.httpPostRequest(_urlGetAllAssetCategories,param)
    //this.assetcategoryservice.getAllAssetCategories(param)
      .then(rescompData => {
        this.skeleton = true
        var result = rescompData;
        console.log('getAssetCategoryResponse:', rescompData);
        if (result['type'] == true) {
          if (result['data'][0].length == 0) {
            this.skeleton = true
            this.noCategory = true;
          } else {
            this.categorylist = result['data'][0]
            this.category = this.categorylist;

            // for(let i = 0; i< this.category.length; i++) {
            //   this.category[i].disable =  this.category[i].visible;
            //   this.category[i].title = this.category[i].categoryName;
            //   this.category[i].discrption = this.category[i].description;
            //   if(this.category[i].categoryPicRef == '') {
            //     this.category[i].image = 'assets/images/category.jpg';
            //   } else {
            //     this.category[i].image = this.category[i].categoryPicRef;
            //   }
            // }
            for (let i = 0; i < this.category.length; i++) {
              if (this.category[i].tags) {
                this.category[i].tags = this.category[i].tags.split(',');
              }
            }

            console.log('this.category', this.category);
            this.displayArray = []
            this.addItems(0, this.sum, 'push', this.category);
            this.noCategory = false;
            this.skeleton = true
          }

        } else {
          this.spinner.hide();
          this.skeleton = true
          this.noDataFound = true;
          // var toast: Toast = {
          //   type: 'error',
          //   //title: "Server Error!",
          //   body: "Something went wrong.please try again later.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }


      }, error => {
        this.spinner.hide();
        this.skeleton = true
        this.noDataFound = true;
        // var toast: Toast = {
        //   type: 'error',
        //   //title: "Server Error!",
        //   body: "Something went wrong.please try again later.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
  }

// newArray=[];
ngOnDestroy(){
  let e1 = document.getElementsByTagName('nb-layout');
  if(e1 && e1.length !=0){
    e1[0].classList.add('with-scroll');
  }
}
ngAfterContentChecked() {
  let e1 = document.getElementsByTagName('nb-layout');
  if(e1 && e1.length !=0)
  {
    e1[0].classList.remove('with-scroll');
  }

 }
  SarchFilter(event) {
    var searchtext
    this.header.searchtext = event.target.value
    console.log(searchtext);
    searchtext= event.target.value;
    const val = searchtext.toLowerCase();
    if(val.length>=3 || val.length == 0){
    this.displayArray=[];
    const tempcc = this.categorylist.filter(function (d) {
      return String(d.code).toLowerCase().indexOf(val) !== -1 ||
        d.categoryName.toLowerCase().indexOf(val) !== -1 ||
        d.description.toLowerCase().indexOf(val) !== -1 ||
        // d.department.toLowerCase().indexOf(val) !== -1 ||
        // d.manager.toLowerCase().indexOf(val) !== -1 ||
        !val
    });
    console.log(tempcc);
    this.newArray = tempcc;
    this.sum=50;
    this.addItems(0, this.sum, 'push', this.newArray);

    if (!this.newArray.length) {
      this.noCategory = true;
    } else {
      this.noCategory = false;
    }
  }
  }
  clear() {
    if(this.header.searchtext.length>=3){
    this.searchtext = '';
    this.header.searchtext = '';
    this.getAllAssetCategories();
    }
    else{
     this.searchtext = '';
    }
  }

  addeditccategory(data, id) {

    if (data == undefined) {
      this.categoryForm = {
        id: '',
        name: '',
        code: '',
        desc: '',
        categoryPicRef: '',
      };
    }

    this.addEditCategoryModal = true;
    console.log('data', data);

    if (id == 0) {
      this.addAction = true;
    } else {
      this.addAction = false;
      console.log('data2', data);
      this.categoryForm = {
        id: data.categoryId,
        name: data.categoryName,
        code: data.categoryCode,
        desc: data.description,
        categoryPicRef: data.categoryPicRef
      }

    }
  }

  EditSection(data) {
    this.addEditCategoryModal = true;
    this.addAction = false;
    console.log('data2', data);
    this.categoryForm = {
      id: data.categoryId,
      name: data.categoryName,
      code: data.categoryCode,
      desc: data.description,
      categoryPicRef: data.categoryPicRef,
    }
  }

  catName: any;
  catIdStatus: any;
  catVisibleStatus: any;
  disableCategory(value, data) {
    console.log('data', data);
    if (data.cnt > 0) {
      this.presentToast('warning', 'Category have assets, not able to hide this category');
    } else {
      this.enableDisableCategoryModal = true;
      this.categoryData = data;
      if (data.visible == 0) {
        this.catVisibleStatus = 1;
        // this.changetitle = 'Enable';
        this.msg = "Asset category Enabled Successfully";
      } else {
        this.catVisibleStatus = 0;
        // this.changetitle = 'Disable';
        this.msg = "Asset category Disabled Successfully";
      }
    }
  }

  clickTodisable(data) {

    if(data.visible==0){
      this.changetitle='Enable';

    }
    else{
      this.changetitle='Disable';
    }
    if (data.cnt > 0) {
      this.presentToast('warning', 'Category have assets, not able to hide this category');
    } else {
      this.enableDisableCategoryModal = true;
      this.categoryData = data;
      this.storedVisible = data;

    }
  }

  enableDisableAction(status) {
    console.log(status);

    // kv
    if (status == true) {
      if (this.storedVisible.visible == 0) {
        this.catVisibleStatus = 1;
        this.storedVisible.disable = 1;
        // this.changetitle = 'Enable';
        this.msg = "Asset category Enabled Successfully";
      } else {
        this.catVisibleStatus = 0;
        this.storedVisible.disable = 0;
        // this.changetitle = 'Disable';
        this.msg = "Asset category Disabled Successfully";
      }

    // kv
      this.changeCategoryStatus();
    } else {
      this.enableDisableCategoryModal = false;
    }
  }
  // changeCategoryStatus(catId, catVisible) {
  changeCategoryStatus() {
    this.spinner.show();
    // let param = {
    //   "catId": catId,
    //   "catVisible": catVisible,
    //   "tId": this.tenantId
    // }
    let param = {
      "catId": this.categoryData.categoryId,
      "catVisible": this.catVisibleStatus,
      "tId": this.tenantId
    }
    const _urlChangeAssetCateorystatus:string = webApi.domain + webApi.url.changeAssetCategoryStatus;
    this.commonFunctionService.httpPostRequest(_urlChangeAssetCateorystatus,param)
    //this.assetcategoryservice.changeAssetCategoryStatus(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('UpdateCategoryStatusResponse:', rescompData);
        if (result['type'] == true) {
          // var catUpdate: Toast = {
          //   type: 'success',
          //   title: "Asset category Updated!",
          //   body: "Asset updated successfully.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('success', this.msg);
          this.getAllAssetCategories();
          this.enableDisableCategoryModal = false;
        } else {
          // var catUpdate: Toast = {
          //   type: 'error',
          //   title: "Asset category Updated!",
          //   body: "Unable to update Asset Category.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
        }
      }, error => {
        this.spinner.hide();
        // var toast: Toast = {
        //   type: 'error',
        //   //title: "Server Error!",
        //   body: "Something went wrong.please try again later.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
  }
  closeEnableDisableCategoryModal() {
    this.enableDisableCategoryModal = false;
  }
  closeModal() {
    this.addEditCategoryModal = false;
  }

  fileexterror: any;
  assetCatImgData: any;

  readAssetCatImage(event: any) {
    this.fileexterror = false;
    var validExts = new Array(".png", ".jpg", ".jpeg");
    var fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
      this.fileexterror = true;
    } else {
      if (event.target.files && event.target.files[0]) {
        this.assetCatImgData = event.target.files[0];

        var reader = new FileReader();

        reader.onload = (event: ProgressEvent) => {
          this.categoryForm.categoryPicRef = (<FileReader>event.target).result;
        }
        reader.readAsDataURL(event.target.files[0]);
      }
    }
  }

  deleteAssetCatImage() {
    this.assetCatImgData = null;
    this.categoryForm.categoryPicRef = 'https://bhaveshedgetest.s3.ap-south-1.amazonaws.com/BFL.jpg';
  }

  // convenience getter for easy access to form fields
  // get f() { return this.categoryFormValidation.controls; }

  passParams: any;
  fileUploadRes: any;
  fileres: any;
  errorMsg: any;
  submit(data, f) {
    this.isSave = true
    this.submitted = true;
    if (f.valid) {
      if (!data) {
        // return;
        console.log(data);
      }
      else {
        console.log('data', data);

        this.addEditCategoryModal = false;
        this.spinner.show();

        let param = {
          "catId": this.addAction ? 0 : data.id,
          "catCode": data.code,
          "catName": data.name,
          "catDesc": data.desc,
          "catPicRef": data.categoryPicRef,
          "tId": this.tenantId,
          "userId": this.userId
        }
        console.log('param', param);
        this.passParams = param;

        var fd = new FormData();
        fd.append('content', JSON.stringify(this.passParams));
        fd.append('file', this.assetCatImgData);
        console.log('File Data ', fd);

        console.log('Asset Category Img', this.assetCatImgData);
        console.log('Asset Data ', this.passParams);
        let fileUploadUrl = webApi.domain + webApi.url.fileUpload;

        if (this.assetCatImgData) {
          this.webApiService.getService(fileUploadUrl, fd)
            .then(rescompData => {
              this.isSave = false
              var temp: any = rescompData;
              this.fileUploadRes = temp;
              console.log('rescompData', this.fileUploadRes)
              this.assetCatImgData = null;
              if (temp == "err") {
                // this.notFound = true;
                // var thumbUpload: Toast = {
                //   type: 'error',
                //   title: "Asset Category Image",
                //   body: "Unable to upload Asset Category Image.",
                //   // body: temp.msg,
                //   showCloseButton: true,
                //   timeout: 2000
                // };
                // this.toasterService.pop(thumbUpload);
                this.presentToast('error', '');
                this.spinner.hide();
              } else if (temp.type == false) {
                // var thumbUpload: Toast = {
                //   type: 'error',
                //   title: "Asset Category Image",
                //   body: "Unable to upload Asset Category Image.",
                //   // body: temp.msg,
                //   showCloseButton: true,
                //   timeout: 2000
                // };
                // this.toasterService.pop(thumbUpload);
                this.presentToast('error', '');
                this.spinner.hide();
              }
              else {
                if (this.fileUploadRes.data != null || this.fileUploadRes.fileError != true) {
                  this.passParams.catPicRef = this.fileUploadRes.data.file_url;
                  console.log('this.passparams.catPicRef', this.passParams.catPicRef);
                  // this.isSave= false
                  this.spinner.hide();
                  this.addEditAssetCategory(this.passParams);
                } else {
                  // var thumbUpload: Toast = {
                  //   type: 'error',
                  //   title: "Asset Category Image",
                  //   // body: "Unable to upload course thumbnail.",
                  //   body: this.fileUploadRes.status,
                  //   showCloseButton: true,
                  //   timeout: 2000
                  // };
                  // this.toasterService.pop(thumbUpload);
                  this.presentToast('error', '');
                  this.spinner.hide();
                }
              }
              console.log('File Upload Result', this.fileUploadRes);
              var res = this.fileUploadRes;
              this.fileres = res.data.file_url;
            },
              resUserError => {
                this.errorMsg = resUserError;
                console.log('File upload this.errorMsg', this.errorMsg);
                this.spinner.hide();
              });
        } else {
          this.spinner.hide();
          this.addEditAssetCategory(this.passParams);
        }
      }
    } else {
      console.log('Please Fill all fields');
      this.isSave = false

      // const detForm: Toast = {
      //   type: 'error',
      //   title: 'Unable to update',
      //   body: 'Please Fill all fields',
      //   showCloseButton: true,
      //   timeout: 2000
      // };
      // this.toasterService.pop(detForm);
      this.presentToast('warning', 'Please fill in the required fields');
      Object.keys(f.controls).forEach(key => {
        f.controls[key].markAsDirty();
      });
    }

  }

  addEditAssetCategory(params) {
    console.log('paramNew', params);
    this.spinner.show();
    const _urlAddEditAssetCategories:string = webApi.domain + webApi.url.addEditAssetCategory;
    this.commonFunctionService.httpPostRequest(_urlAddEditAssetCategories,params)
    //this.assetcategoryservice.addEditAssetCategories(params)
      .then(rescompData => {
        this.isSave = false

        this.spinner.hide();

        var temp = rescompData;
        var result = temp['data'];
        if (params.catId == 0) {
          if (temp['type'] == true) {
            // this.fetchBadgeCateory();
            console.log('Add Result ', result)
            // var catUpdate: Toast = {
            //   type: 'success',
            //   title: "Asset Category Inserted!",
            //   body: "New Asset Category added successfully.",
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(catUpdate);
            this.presentToast('success', 'New asset category added');
            this.getAllAssetCategories();

          } else {
            //this.fetchBadgeCateory();
            // var catUpdate: Toast = {
            //   type: 'error',
            //   title: "Asset Category Inserted!",
            //   body: "Unable to add Asset Category.",
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(catUpdate);
            this.presentToast('error', '');
          }
        } else {
          console.log('Asset Category Edit Result ', result)
          if (temp['type'] == true) {
            // this.fetchBadgeCateory();
            // var catUpdate: Toast = {
            //   type: 'success',
            //   title: "Asset Category Updated!",
            //   body: "Asset category updated successfully.",
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(catUpdate);
            this.presentToast('success', 'Asset category updated');
            this.getAllAssetCategories();
          } else {
            //this.fetchBadgeCateory();
            // var catUpdate: Toast = {
            //   type: 'error',
            //   title: "Asset Category updated!",
            //   body: "Unable to update Asset category.",
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(catUpdate);
            this.presentToast('error', '');
          }
        }

      },
        resUserError => {
          this.spinner.hide();
          this.errorMsg = resUserError;
        });
  }

  gotoAsset(data) {
    console.log('catdata', data);
    this.assetservice.getPendingAssets = false;
    this.assetservice.categoryId = data.categoryId;
    this.assetservice.dataFromAssetCategory = true;
    this.router.navigate(['asset'], { relativeTo: this.routes });

  }

  gotodetails(data) {
    this.assetservice.getPendingAssets = false;
    this.assetservice.categoryId = data.categoryId;
    this.assetservice.dataFromAssetCategory = true;
    this.router.navigate(['asset'], { relativeTo: this.routes });
  }

  // Help Code Start Here //

  helpContent: any;
  getHelpContent() {
    return new Promise(resolve => {
      this.http1
        .get("../../../../../../assets/help-content/addEditCourseContent.json")
        .subscribe(
          data => {
            this.helpContent = data;
            console.log("Help Array", this.helpContent);
          },
          err => {
            resolve("err");
          }
        );
    });
    // return this.helpContent;
  }

  // Help Code Ends Here //

  onSearch(event){
    this.search.categoryName = event.target.value

  }
}
