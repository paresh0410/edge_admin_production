import {Injectable, Inject} from '@angular/core';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppConfig} from '../../../app.module';
import { webApi } from '../../../service/webApi';
import { HttpClient } from "@angular/common/http";

@Injectable()
export class AssetCategoriesService {

    tenantId : any = 1;
    userData
   
  private _urlGetAllAssetCategories:string = webApi.domain + webApi.url.getAllAssetCategory;
  private _urlAddEditAssetCategories:string = webApi.domain + webApi.url.addEditAssetCategory;
  private _urlChangeAssetCateorystatus:string = webApi.domain + webApi.url.changeAssetCategoryStatus;


  constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private _http: Http,private _httpClient:HttpClient){
      //this.busy = this._http.get('...').toPromise();
      if(localStorage.getItem('LoginResData')){
      this.userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', this.userData.data);
      // this.userId = this.userData.data.data.id;
      this.tenantId = this.userData.data.data.tenantId;
   }
  }

  getAllAssetCategories(param){
    return new Promise(resolve => {
      this._httpClient.post(this._urlGetAllAssetCategories, param)
      //.map(res => res.json())
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}

addEditAssetCategories(param){
    return new Promise(resolve => {
      this._httpClient.post(this._urlAddEditAssetCategories, param)
      //.map(res => res.json())
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}

changeAssetCategoryStatus(param){
    return new Promise(resolve => {
      this._httpClient.post(this._urlChangeAssetCateorystatus, param)
      //.map(res => res.json())
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}




_errorHandler(error: Response){
  console.error(error);
  return Observable.throw(error || "Server Error")
}

}
