import { Component, OnInit, ViewChild, Renderer2 } from '@angular/core';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { NgxSpinnerService } from 'ngx-spinner';
import * as $ from 'jquery';
import * as _ from 'lodash';
import { Router, ActivatedRoute } from '@angular/router';
import { AssetVesrionService } from './asset-version.service';
import { DatePipe } from '@angular/common';
import { webAPIService } from '../../../service/webAPIService';
import { webApi } from '../../../service/webApi';
import { ToastrService } from 'ngx-toastr';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { SuubHeader } from '../../components/models/subheader.model';
import { DomSanitizer } from '@angular/platform-browser';
import { AddAssetService } from '../asset/add-asset/add-asset.service';
let __ = _.noConflict();
// let __ = ld.noConflict();
// this.LoDashExplicitWrapper.noConflict();
declare var kPoint;

@Component({
  selector: 'ngx-asset-version',
  templateUrl: './asset-version.component.html',
  styleUrls: ['./asset-version.component.scss']
})
export class AssetVersionComponent implements OnInit {
  @ViewChild('video_file') videofile: any;


  tenantId: any;
  versionForm: any = {};
  settingsEmployeeDrop: any = {};
  settingsCreatorDrop:any ={};
  status:any;
  languages:any;
  formats:any;
  channels:any;
  categories:any;
  userId:any;
  getAssetDataForApproval:any=[];
  getAssetDataForVersion: any = [];
  header: SuubHeader  = {
    title:'ADD ASSET VERSION',
    btnsSearch: true,
    searchBar: false,
    dropdownlabel: ' ',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: '',
    btnName3: '',
    btnAdd: 'Save',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: true,
    filter: false,
    showBreadcrumb:true,
    breadCrumbList:[]

  };
  @ViewChild('kpplayer') kpplayer: any;

  files: any;
  file: any;
  pdfReference: any;
  allowedTypes = []
  dummyFiles = []
  settingsTagDrop: {}
  tagList: any = [];
  tempTags: any = [];
  selectedTags: any = [];
  kpointurl: any;
  breadCrumbList: any = [];
  title: string;
  formatExtensions: any;
  constructor(
    // private toasterService: ToasterService, 
    private spinner: NgxSpinnerService,private datePipe: DatePipe,
    private addassetservice: AddAssetService,
    private sanitizer: DomSanitizer,
    private render: Renderer2,
    // private dom:DomSanitizer,
    public router: Router, public routes: ActivatedRoute,private assetVesrionService : AssetVesrionService,
    protected webApiService:webAPIService,  private toastr: ToastrService,private commonFunctionService: CommonFunctionsService) { 

      if (localStorage.getItem('LoginResData')) {
        var userData = JSON.parse(localStorage.getItem('LoginResData'));
        console.log('userData', userData.data);
        this.userId = userData.data.data.id;
        console.log('userId', userData.data.data.id);
        }
        if (localStorage.getItem('formatExtensions')) {
          this.formatExtensions = JSON.parse(localStorage.getItem('formatExtensions'));
        }

      this.tenantId = this.assetVesrionService.tenantId;
      this.getAllAssetDropdown();
      this.getAllEmployeesDAM();
      this.getAllEdgeEmployee();
      this.getallTagList()
      
    console.log('allEmployeeAutoDrop', this.allEmployeeAutoDrop);
    //this.prepareApproverDropdown();
    this.settingsCreatorDrop = {
      text: "Select Approver",
      singleSelection: true,
      classes: "myclass custom-class",
      primaryKey: "empId",
      labelKey: "empName",
      noDataLabel: "Search Employee...",
      enableSearchFilter: true,
      searchBy: ['empId', 'empName'],
      disabled: true,
      maxHeight:250,
      lazyLoading: true,
    };

    this.settingsEmployeeDrop = {
      text: "Select Author",
      singleSelection: false,
      classes: "myclass custom-class",
      primaryKey: "edgeempId",
      labelKey: "edgeempName",
      noDataLabel: "Search Employee...",
      enableSearchFilter: true,
      searchBy: ['edgeempId', 'edgeempName'],
      disabled: true,
      maxHeight:250,
      lazyLoading: true,
      };

      this.settingsTagDrop = {
        text: 'Select Tags',
        singleSelection: false,
        classes: 'myclass custom-class',
        primaryKey: 'id',
        labelKey: 'name',
        noDataLabel: 'Search Tags...',
        enableSearchFilter: true,
        searchBy: ['name'],
        maxHeight:250,
        lazyLoading: true,
        disabled:true
      };
    }

  ngOnInit() {
    if(this.addassetservice.breadCrumbArray){
      this.breadCrumbList = this.addassetservice.breadCrumbArray
      this.title = this.addassetservice.title
      this.header.breadCrumbList = this.breadCrumbList
      this.header.title =  this.title
    }
  }
  getallTagList() {
    this.spinner.show();
    let url = webApi.domain + webApi.url.getAllTagsComan;
    let param = {
      tenantId :this.tenantId
    };
    this.webApiService.getService(url, param)
      .then(rescompData => {
        // this.loader =false;
        this.spinner.hide();
        var temp: any = rescompData;
        this.tagList = temp.data;
        console.log(this.tagList,"taglist")
        this.tempTags = [... this.tagList];
        console.log(this.tempTags,"this.temptags")
        // console.log('Visibility Dropdown',this.visibility);

        // this.getDataForAddEdit = this.addassetservice.addEditAssetData;
        // if(this.getDataForAddEdit[0] === 'EDIT'){
        // if(this.getDataForAddEdit[1].tagIds)
        // {
          if(this.getAssetDataForApproval.tagIds){
          var tagIds =this.getAssetDataForApproval.tagIds.split(',');
          if(tagIds.length > 0){
            this.tempTags.forEach((tag) => {
              tagIds.forEach((tagId)=>{
                if (tag.id == tagId ) {
                  this.selectedTags.push(tag);
                  console.log(this.selectedTags,"if selectedTags")
                }
              });
            });
            }
          }
        // }
      // }

      },
        resUserError => {
          // this.loader =false;
          // this.show =false;
          this.spinner.hide();
          this.errorMsg = resUserError;
        });
  }

  back(){
    var length = this.addassetservice.breadCrumbArray.length-1
    this.title = this.addassetservice.breadCrumbArray[length].name
    this.addassetservice.title = this.title
    this.addassetservice.breadCrumbArray = this.addassetservice.breadCrumbArray.slice(0,length)
  // this.title = this.addassetservice.breadCrumbArray[]
  this.addassetservice.previousBreadCrumb = this.addassetservice.previousBreadCrumb.slice(0,length+1)
    console.log(this.title,"title")
    // this.router.navigate(['../../asset'],{relativeTo:this.routes});
    this.router.navigate(['../'],{relativeTo:this.routes});
  }
  backToAsset(data){
    this.addassetservice.categoryId = data.assetId
    this.addassetservice.title = data.assetName
    this.addassetservice.breadCrumbArray = this.addassetservice.breadCrumbArray.slice(0,data.index)
    // this.title = this.addassetservice.breadCrumbArray[]
    this.addassetservice.previousBreadCrumb = this.addassetservice.previousBreadCrumb.slice(0,data.index+1)

    this.router.navigate(['../'], { relativeTo: this.routes });


  }

  onTagsSelect(eve){

  }
  OnTagDeSelect(eve) {

  }


  prepareVersionFormEmployee() {
    this.getAssetDataForApproval = this.assetVesrionService.getAssetDataForVesrsioning;
    console.log('getAssetDataForApproval', this.getAssetDataForApproval);

    if (this.getAssetDataForApproval.estLength) {
      var resStr = this.getAssetDataForApproval.estLength.split(".");
      var formattedDuration = new Date();
      formattedDuration.setHours(resStr[0], resStr[1], resStr[2]);
    }

    let selectedCreator: any = [];
    this.allEmployeeAutoDrop.forEach((emp) => {
      if (emp.empId == this.getAssetDataForApproval.reviewerId) {
      // if (emp.empId == this.getAssetDataForApproval.author) {
        let allEmployeeAutoDropObj = {};
        allEmployeeAutoDropObj = {
          empId: emp.empId,
          empName: emp.empName
        }
        selectedCreator.push(allEmployeeAutoDropObj);
      }
    });
    if(this.getAssetDataForApproval.author){
    var author =this.getAssetDataForApproval.author.split(',');
    

    var selectedEmployee: any = [];
    this.allEmployeeEdgeAutoDrop.forEach((emp) => {
      author.forEach((auth)=>{
      if (emp.edgeempId == auth) {
        let allEdgeEmployeeAutoDropObj = {};
        allEdgeEmployeeAutoDropObj = {
          edgeempId: emp.edgeempId,
          edgeempName: emp.edgeempName
        }
        selectedEmployee.push(allEdgeEmployeeAutoDropObj);
      }
    })
    });
  }

    //let cTags = this.getAssetDataForApproval.tags.split(',');
    let cTags = this.getAssetDataForApproval.tags;
    this.versionForm = {
      assetId: this.getAssetDataForApproval.assetId,
      assetName: this.getAssetDataForApproval.assetName,
      assetRef: null,
      bitlyLink: null,
      category: this.getAssetDataForApproval.categoryId,
      channel: this.getAssetDataForApproval.channelId,
      cost: null,
      description: this.getAssetDataForApproval.description?this.getAssetDataForApproval.description:null,
      format: this.getAssetDataForApproval.formatId,
      language: this.getAssetDataForApproval.languageId,
      status: this.getAssetDataForApproval.status,
      assetTags:this.getAssetDataForApproval.tags == "" ? [] : cTags,
      visible: this.getAssetDataForApproval.visible,
      selectedCreator: selectedCreator,
      selectedEmployee:selectedEmployee,
      time:formattedDuration
    }
    // this.native(author)
    console.log('this.versionForm', this.versionForm);
  }

  prepareEdgeEmpFormEmployee() {
    this.getAssetDataForApproval = this.assetVesrionService.getAssetDataForVesrsioning;
    console.log('getAssetDataForApproval', this.getAssetDataForApproval);

    
  }

  fileUrl:any;
  fileName:any = "You can drag and drop files here to add them.";
  fileIcon:any = "assets/img/app/profile/avatar4.png";
  fileReaded:any;
  enableUpload:any = false;
  selectFileTitle:any = "No file chosen";
  fileUpload:any;
  assetVersionFileData:any;

  presentToast(type, body) {
    if(type === 'success'){
    this.toastr.success(body, 'Success', {
    closeButton: false
    });
    } else if(type === 'error'){
    this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
    timeOut: 0,
    closeButton: true
    });
    } else{
    this.toastr.warning(body, 'Warning', {
    closeButton: false
    })
    }
    }

  // readFileUrl(event:any) {
  //   var size = 100000000;
  //   if(this.versionForm.format == 1){
  //   var validExts = new Array( "video" );
  //   }
  //   else if(this.versionForm.format == 2){
  //   var validExts = new Array( "audio" );
  //   }
  //   else if(this.versionForm.format == 3){
  //   var validExts = new Array( "pdf" );
  //   }
  //   else if(this.versionForm.format == 7){
  //   var validExts = new Array( "image" );
  //   }
  //   else if(this.versionForm.format == 10){
  //   var validExts = new Array( "application/vnd.ms-powerpoint","application/vnd.openxmlformats-officedocument.presentationml.presentation" );
  //   }
  //   else if(this.versionForm.format == 11){
  //   var validExts = new Array( "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/vnd.ms-excel" );
  //   }
  //   else if(this.versionForm.format == 12){
  //   var validExts = new Array( "application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document" );
  //   }
  //   else if(this.versionForm.format == 5){
  //   var validExts = new Array( "application/zip" );
  //   }
  //   // var validExts  "application/zip"= new Array("image", "video", "audio", "pdf","application/vnd.ms-powerpoint","application/vnd.openxmlformats-officedocument.presentationml.presentation","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/vnd.ms-excel","application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document");
    
  //   var fileType = event.target.files[0].type;
  //   console.log(fileType,"fileType")
  //   // fileType = fileType.substring(0,5);

  //   // var validExts = new Array(".png", ".jpg", ".jpeg",);
  //   // var fileExt = event.target.files[0].name;
  //   // fileExt = fileExt.substring(fileExt.lastIndexOf('.'));

  //   var fileExt = false;
  //   for(let i=0; i<validExts.length; i++){
  //     if(fileType.includes(validExts[i])){
  //       // return true;
  //       fileExt = true;
  //       break;
  //     }
  //   }

  //   // if(validExts.indexOf(fileType) < 0) {
  //   if(!fileExt) {
  //     // var toast : Toast = {
  //     //   type: 'error',
  //     //   title: "Invalid file selected!",
  //     //   body: "Valid files are of " + validExts.toString() + " types.",
  //     //   showCloseButton: true,
  //     //   timeout: 2000
  //     // };
  //     // this.toasterService.pop(toast);
  //     // this.presentToast('warning', 'Valid file types are ' + validExts.toString());
  //     this.presentToast('warning', 'Please select a valid file type');

  //     this.cancelFile();
  //   } else {
  //     if(size <= event.target.files[0].size){
  //       // var toast : Toast = {
  //       //     type: 'error',
  //       //     title: "File size exceeded!",
  //       //     body: "File size should be less than 100MB",
  //       //     showCloseButton: true,
  //       //     timeout: 2000
  //       // };
  //       // this.toasterService.pop(toast);
  //       this.presentToast('warning', 'File size should be less than 100MB');
  //       this.cancelFile();
  //     } else {
  //       if (event.target.files && event.target.files[0] ) {
  //         this.fileName = event.target.files[0].name;
  //         // this.selectFileTitle = this.files[0].name;
  //         this.selectFileTitle = this.fileName

  //         // this.pdfReference = event.target.files[0];
  //         // this.pdfReference = this.dom.bypassSecurityTrustResourceUrl(this.convertURL(this.fileName));
  //         // console.log(this.pdfReference,"pdfreference")
  //         //read file from input
  //         this.fileReaded = event.target.files[0];
  //         this.assetVersionFileData = event.target.files[0];

  //         if(this.fileReaded != '' || this.fileReaded != null || this.fileReaded != undefined){
  //           this.enableUpload = true;
  //         }
  //         let file = event.target.files[0];
  //         var reader = new FileReader();
  //         reader.onload = (event: ProgressEvent) => {
  //           this.fileUrl = (<FileReader>event.target).result;
  //         }
  //         reader.readAsDataURL(event.target.files[0]);
  //         if(this.versionForm.format == 3){
  //           this.pdfReference = URL.createObjectURL(event.target.files[0])
  //           this.pdfReference = this.sanitizer.bypassSecurityTrustResourceUrl(
  //             this.pdfReference);
  //             // this.pdfReference = this.pdfReference.name
  //           console.log(this.pdfReference,"pdfreference")
            
  //         }
  //       }
  //       else{
  //         this.native(event)
  //       }
  //       /// New Requirement
        
  //       /// end
  //     }
  //   }
  // }
  readFileUrl(event:any) {
    var size = 100000000;
    if(this.versionForm.format == 1){
      var format = 1
    // var validExts = new Array( "video" );
    }
    else if(this.versionForm.format == 2){
      var format = 2

    // var validExts = new Array( "audio" );
    }
    else if(this.versionForm.format == 3){
    // var validExts = new Array( "pdf" );
    }
    else if(this.versionForm.format == 7){
    // var validExts = new Array( "image" );
    }
    else if(this.versionForm.format == 10){
    // var validExts = new Array( "application/vnd.ms-powerpoint","application/vnd.openxmlformats-officedocument.presentationml.presentation" );
    }
    else if(this.versionForm.format == 11){
    // var validExts = new Array( "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/vnd.ms-excel" );
    }
    else if(this.versionForm.format == 12){
    // var validExts = new Array( "application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document" );
    }
    else if(this.versionForm.format == 5){
    // var validExts = new Array( "application/zip" );
    }
    // var validExts  "application/zip"= new Array("image", "video", "audio", "pdf","application/vnd.ms-powerpoint","application/vnd.openxmlformats-officedocument.presentationml.presentation","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/vnd.ms-excel","application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document");
    var validExts = this.formatExtensions
    // var fileType = event.target.files[0].type;
    // var fileType = event.target.files[0].name.split('.')[1];
    var last_dot = this.files[0].name.lastIndexOf('.')
    var fileType = this.files[0].name.slice(last_dot + 1)
    console.log(fileType,"fileType")
    // fileType = fileType.substring(0,5);

    // var validExts = new Array(".png", ".jpg", ".jpeg",);
    // var fileExt = event.target.files[0].name;
    // fileExt = fileExt.substring(fileExt.lastIndexOf('.'));

    var fileExt = false;
    for(let i=0; i<validExts.length; i++){
      if(this.versionForm.format == validExts[i].formatId){
      if(fileType.includes(validExts[i].extension)){
        // return true;
        fileExt = true;
        break;
      }
    }
    }

    // if(validExts.indexOf(fileType) < 0) {
    if(!fileExt) {
      // var toast : Toast = {
      //   type: 'error',
      //   title: "Invalid file selected!",
      //   body: "Valid files are of " + validExts.toString() + " types.",
      //   showCloseButton: true,
      //   timeout: 2000
      // };
      // this.toasterService.pop(toast);
      // this.presentToast('warning', 'Valid file types are ' + validExts.toString());
      this.presentToast('warning', 'Please select a valid file type');

      this.cancelFile();
    } else {
      if(size <= event.target.files[0].size){
        // var toast : Toast = {
        //     type: 'error',
        //     title: "File size exceeded!",
        //     body: "File size should be less than 100MB",
        //     showCloseButton: true,
        //     timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('warning', 'File size should be less than 100MB');
        this.cancelFile();
      } else {
        if (event.target.files && event.target.files[0] ) {
          this.fileName = event.target.files[0].name;
          // this.selectFileTitle = this.files[0].name;
          this.selectFileTitle = this.fileName

          // this.pdfReference = event.target.files[0];
          // this.pdfReference = this.dom.bypassSecurityTrustResourceUrl(this.convertURL(this.fileName));
          // console.log(this.pdfReference,"pdfreference")
          //read file from input
          this.fileReaded = event.target.files[0];
          this.assetVersionFileData = event.target.files[0];

          if(this.fileReaded != '' || this.fileReaded != null || this.fileReaded != undefined){
            this.enableUpload = true;
          }
          let file = event.target.files[0];
          var reader = new FileReader();
          reader.onload = (event: ProgressEvent) => {
            this.fileUrl = (<FileReader>event.target).result;
          }
          reader.readAsDataURL(event.target.files[0]);
          if(this.versionForm.format == 3){
            this.pdfReference = URL.createObjectURL(event.target.files[0])
            this.pdfReference = this.sanitizer.bypassSecurityTrustResourceUrl(
              this.pdfReference);
              // this.pdfReference = this.pdfReference.name
            console.log(this.pdfReference,"pdfreference")
            
          }
        }
        else{
          this.native(event)
        }
        /// New Requirement
        
        /// end
      }
    }
  }
  convertURL (url) {
    return webApi.domain + webApi.url.pdfviewerurl + url;
  }
  kpointConfigure(kpointlink) {
    //  this.addEditAssetForm.assetRef = kpointlink;
      this.kpointurl = kpointlink;
      try {
        if (!this.kpointurl) {
          alert('Please Enter Url');
        } else {
          const kpoint = this.populateKpointObj(this.kpointurl);
          const player = kPoint.Player(this.kpplayer.nativeElement, {
            kvideoId: kpoint.id,
            videoHost: kpoint.host,
            params: { autoplay: true }
          });
        }
      } catch (e) {
        console.log(e);
      }
    }
    populateKpointObj(url) {
      // tslint:disable-next-line:prefer-const
      let kpoint: any = {
        host: '',
        id: '',
        src: '',
        url: ''
      };
      if (url.split('/').length > 0) {
        kpoint.host = url.split('/')[2];
        kpoint.id = url.split('/')[4];
        kpoint.src = url;
        kpoint.url = url.split('/')[0] + '//' + url.split('/')[2];
      }
      return kpoint;
    }

  native(eve){
    setTimeout(() => {
      if (this.videofile) {
        this.render.listen(this.videofile.nativeElement, 'change', () => {
          // this.isLocalfile = true;
          var size = 1000000000;
          var validExts = new Array('video');
          this.files = this.videofile.nativeElement.files;
          var fileType = this.files[0].type;
          console.log('fileType', fileType);
          var fileExt = false;
          for (let i = 0; i < validExts.length; i++) {
            if (fileType.includes(validExts[i])) {
              fileExt = true;
              break;
            }
          }
          if (!fileExt) {
            this.presentToast('warning', 'Please select a file of type video');
            this.cancelFile();
          } else if (size <= this.files[0].size) {
            this.presentToast('warning', 'File size should be less than 100 MB');
            this.cancelFile();
          } else {
            try {
              console.log('Video', this.files[0]);
              // this.addEditAssetForm.assetRef = this.files[0].name;
              // this.assetrefLink ='';
              this.versionForm.assetRef = this.files[0].name;
              this.fileName = this.files[0].name;
              this.assetFileData = this.files[0];
              this.assetVersionFileData = this.files[0];
              this.selectFileTitle = this.files[0].name;
              console.log('this.fileName', this.fileName);
              console.log('this.assetFileData', this.assetFileData);
              setTimeout(() => {
                let $source: any = $('#video_here');
                $source[0].src = window.URL.createObjectURL(this.files[0]); // URL.createObjectURL(this.files[0]);
                // $source.parent()[0].load();
                this.file =$source[0].src
              }, 200);
            } catch (e) {
              console.log(e);
            }
          }
        });
      }

    },500)
  }

  passFormatType() {
    // if(this.formDataActivity.formatId === 1){
    //   this.allowedTypes = [];
    // }
    switch (Number(this.versionForm.format)) {
      case 1:
        this.allowedTypes = new Array('video');
        break;
      case 2:
        this.allowedTypes = new Array('audio');
        break;
      case 3:
        this.allowedTypes = new Array('pdf');
        break;
      // case 4: this.allowedTypes =  new Array('k-point');
      //         break;
      case 5:
        this.allowedTypes = new Array('zip');
        break;
      case 7:
        this.allowedTypes = new Array('image');
        break;
      // case 8: this.allowedTypes =  new Array('image');
      //         break;
      case 10:
        this.allowedTypes = new Array(
          'application/vnd.ms-powerpoint',
          'application/vnd.openxmlformats-officedocument.presentationml.presentation'
        );
        break;
      case 11:
        this.allowedTypes = new Array(
          'application/vnd.ms-excel',
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        );
        break;
      case 12:
          this.allowedTypes =  new Array('application/msword',
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document')
          break;

      // this.allowedTypes =
      // new Array(
      //   'application/vnd.ms-powerpoint',
      //   'application/vnd.openxmlformats-officedocument.presentationml.presentation'
      // );
    }
  }
  displayOutput(event) {
    console.log('Output files', event);
    this.files = event;
    this.assetVersionFileData = this.files[0]
  }


  

  convertFileUrl(fileurl) {
    console.log(fileurl,"fileUrl");
    if (!fileurl) {
      return fileurl;
    } else if (fileurl.includes('C:\fakepath')) {
      return fileurl.split('C:\fakepath')[1];
    } else {
      return fileurl;
    }
  }
  

  cancelFile(){
    this.fileUrl =null
    // this.fileUpload.nativeElement.files = [];
    this.assetVersionFileData = null;
    this.versionForm.assetRef = ''
    // console.log(this.fileUpload.nativeElement.files);
    if(this.fileUpload){
      if(this.fileUpload.nativeElement){
        this.fileUpload.nativeElement.value = "";
      }
    }

    // console.log(this.fileUpload.nativeElement.files);
    this.fileName = 'You can drag and drop files here to add them.';
    this.selectFileTitle = "No file chosen";
    this.enableUpload = false;
  }

  getAllAssetDropdown() {
    let param = {
      "tId": this.tenantId
    }
    const _urlGetAllDropdown:string = webApi.domain + webApi.url.getAllAssetDropDown;
    this.commonFunctionService.httpPostRequest(_urlGetAllDropdown,param)
    // this.assetVesrionService.getAllAssetDropdown(param)
      .then(rescompData => {
        var result = rescompData;
        console.log('All Asset Dropdown1', result);
        if (result['type'] == true) {
          console.log('All Asset Dropdown2', result);
          this.status = result['data'][0];
          this.languages = result['data'][1];
          this.formats = result['data'][2];
          this.channels = result['data'][3];
          this.categories = result['data'][4];
          console.log(this.status, this.languages, this.formats, this.channels, this.categories);
        } else {
          // var toast: Toast = {
          //   type: 'error',
          //   //title: "Server Error!",
          //   body: "Something went wrong.please try again later.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }

      },
        resUserError => {
          // this.loader =false;
          // var toast: Toast = {
          //   type: 'error',
          //   //title: "Server Error!",
          //   body: "Something went wrong.please try again later.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        });
  }

  selectedCreator: any;
  usersList: any = [];
  allEmployeeAutoDrop: any = [];

  prepareApproverDropdown() {
    this.allEmployeesAuto.forEach(element => {
      let allEmployeeAutoDropObj = {};
      allEmployeeAutoDropObj = {
        empId: element.id,
        empName: element.name
      }
      this.allEmployeeAutoDrop.push(allEmployeeAutoDropObj);
    });
    console.log('this.allEmployeeAutoDrop', this.allEmployeeAutoDrop);
  }

  onCreatorSelect(item: any) {
    console.log(item);
    console.log(this.selectedCreator);
  }

  OnCreatorDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedCreator);
  }

  onCreatorSearch(evt: any) {
    console.log(evt.target.value);
    const val = evt.target.value;
    this.usersList = [];
    const tuser = this.allEmployeeAutoDrop.filter(function (d) {
      return String(d.empId).toLowerCase().indexOf(val) !== -1 ||
        d.empName.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.usersList = tuser;
  }


  noEmployees: boolean = false;
  allEmployeesAuto: any = [];
  getAllEmployeesDAM() {
    this.spinner.show();
    let param = {
      "tId": this.tenantId
    }
    const _urlGetAllEmployees:string = webApi.domain + webApi.url.getAllEmployeeDAM;
    this.commonFunctionService.httpPostRequest(_urlGetAllEmployees,param)
    // this.assetVesrionService.getAllEmployee(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('getAllEmployessDAM:', rescompData);
        if (result['type'] == true) {
          if (result['data'][0].length == 0) {
            this.noEmployees = true;
          } else {
            this.allEmployeesAuto = result['data'][0];
            console.log('this.allEmployeesAuto', this.allEmployeesAuto);
            this.prepareApproverDropdown();
            
          }

        } else {
          // var toast: Toast = {
          //   type: 'error',
          //   //title: "Server Error!",
          //   body: "Something went wrong.please try again later.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }


      }, error => {
        this.spinner.hide();
        // var toast: Toast = {
        //   type: 'error',
        //   //title: "Server Error!",
        //   body: "Something went wrong.please try again later.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
  }


  noEdgeEmployees:boolean=false;
  allEmployeeEdgeAuto:any;
  getAllEdgeEmployee(){
    this.spinner.show();
    let param = {
        "tId":this.tenantId
    }
    const _urlGetAllEdgeEmployee:string = webApi.domain + webApi.url.getAllEdgeEmployee;
    this.commonFunctionService.httpPostRequest(_urlGetAllEdgeEmployee,param)
    // this.assetVesrionService.getAllEdgeEmployee(param)
    .then(rescompData => {
      this.spinner.hide();
      var result = rescompData;
      console.log('getAllEdgeEmployessDAM:',rescompData);
      if(result['type'] == true){
        if(result['data'][0].length == 0){
          this.noEdgeEmployees = true;
        }else{
          this.allEmployeeEdgeAuto = result['data'][0];
          console.log('this.allEmployeeEdgeAuto',this.allEmployeeEdgeAuto);
          this.prepareEmployeeDropdown();
          this.prepareVersionFormEmployee();
          this.passFormatType()

        }
        
      }else{
        //   var toast : Toast = {
        //     type: 'error',
        //     //title: "Server Error!",
        //     body: "Something went wrong.please try again later.",
        //     showCloseButton: true,
        //     timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      }
      
     
    },error=>{
      this.spinner.hide();
      //   var toast : Toast = {
      //     type: 'error',
      //     //title: "Server Error!",
      //     body: "Something went wrong.please try again later.",
      //     showCloseButton: true,
      //     timeout: 2000
      // };
      // this.toasterService.pop(toast);
      this.presentToast('error', '');
    });
  }

  selectedEmployee:any;
    empList:any=[];

    allEmployeeEdgeAutoDrop:any=[];

    prepareEmployeeDropdown(){
      this.allEmployeeEdgeAuto.forEach(element => {
        let allEmployeeEdgeAutoDropObj = {};
        allEmployeeEdgeAutoDropObj = {
          edgeempId : element.id,
          edgeempName : element.name
          }
          this.allEmployeeEdgeAutoDrop.push(allEmployeeEdgeAutoDropObj);
      });
      console.log('this.allEmployeeEdgeAutoDrop',this.allEmployeeEdgeAutoDrop);
    }

    onEmployeeSelect(item:any){
      console.log(item);
      console.log(this.selectedEmployee); 
    }

    OnEmployeeDeSelect(item:any){
      console.log(item);
      console.log(this.selectedEmployee);
    }
  
    onEmployeeSearch(evt: any) {
      console.log(evt.target.value);
      const val = evt.target.value;
      this.empList = [];
      const tuser = this.allEmployeeEdgeAutoDrop.filter(function(d) {
      return String(d.edgeempId).toLowerCase().indexOf(val) !== -1 || 
      d.edgeempName.toLowerCase().indexOf(val) !== -1 || !val;
    });
  
    // update the rows
    this.empList = tuser;
  }


      passParams:any;
      assetFileData:any;
      fileUploadRes:any;
      fileres:any;
      errorMsg:any;
  insertAssetVersion(data){
    if(data.assetRef!= null){
    let duration = null
    this.spinner.show();
    console.log('data',data);
    if(data.time){
    let time = data.time;
    let duration = this.formatTime(time);
    }
     

    let param = {
      "assetsId":data.assetId,
      "assetsRef":data.assetRef,
      "assetsBitly":null,
      "assetsDesc":data.description,
      "assetsLen":duration,
      'assetsStatus':3,
      "assetsCost":data.cost,
      "tId":this.tenantId,
      "userId":this.userId,
      // "approverId":data.selectedCreator[0].empId,
      "appReq":this.getAssetDataForApproval.appReq
      //"author":data.selectedEmployee[0].edgeempId
    }

    console.log('param',param);

    this.passParams = param;
        console.log('this.passParams',this.passParams);
        var fd = new FormData();
        fd.append('content',JSON.stringify(this.passParams));
        fd.append('file',this.assetVersionFileData);
        console.log('File Data ',fd);
    
        console.log('Asset File',this.assetVersionFileData);
        console.log('Asset Data ',this.passParams);
        let fileUploadUrl = webApi.domain + webApi.url.fileUpload;
    
        if(this.assetVersionFileData){
          this.webApiService.getService(fileUploadUrl,fd)
            .then(rescompData => { 
              var temp:any = rescompData;
              this.fileUploadRes = temp;
              console.log('rescompData',this.fileUploadRes)
              this.assetVersionFileData = null;
              if(temp == "err"){
                // this.notFound = true;
                // var thumbUpload : Toast = {
                //     type: 'error',
                //     title: "Asset File",
                //     body: "Unable to upload Asset File.",
                //     // body: temp.msg,
                //     showCloseButton: true,
                //     timeout: 2000
                // };
                // this.toasterService.pop(thumbUpload);
                this.presentToast('error', '');
                this.spinner.hide();
              }else if(temp.type == false){
                // var thumbUpload : Toast = {
                //     type: 'error',
                //     title: "Asset File",
                //     body: "Unable to upload Asset File.",
                //     // body: temp.msg,
                //     showCloseButton: true,
                //     timeout: 2000
                // };
                // this.toasterService.pop(thumbUpload);
                this.presentToast('error', '');
                this.spinner.hide();
              }
              else{
                if(this.fileUploadRes.data != null || this.fileUploadRes.fileError != true){
                  this.passParams.assetsRef = this.fileUploadRes.data.file_url;
                  console.log('this.passparams.assetsRef',this.passParams.assetsRef);
                  this.spinner.hide();
                 this.addAssetVersion(this.passParams);
                }else{
                  // var thumbUpload : Toast = {
                  //     type: 'error',
                  //     title: "Asset File",
                  //     // body: "Unable to upload course thumbnail.",
                  //     body: this.fileUploadRes.status,
                  //     showCloseButton: true,
                  //     timeout: 2000
                  // };
                  // this.toasterService.pop(thumbUpload);
                  this.presentToast('error', '');
                  this.spinner.hide();
                }
              }
              console.log('File Upload Result',this.fileUploadRes);
              var res = this.fileUploadRes;
              this.fileres = res.data.file_url;
            },
            resUserError=>{
              this.errorMsg = resUserError;
              console.log('File upload this.errorMsg', this.errorMsg);
              this.spinner.hide();
            });
        }else{
         this.spinner.hide();
        this.addAssetVersion(this.passParams);
        }
      }else{
        this.toastr.warning('warning',' Please fill all fields')
      }
  }

  addAssetVersion(passParam){
    console.log('passParam',passParam);
    this.spinner.show();
    const _urlInsertAssetVersion: string = webApi.domain + webApi.url.insertAssetVersion;
    this.commonFunctionService.httpPostRequest(_urlInsertAssetVersion,passParam)
    // this.assetVesrionService.insertAssetVersion(passParam)
    .then(rescompData => { 
   
      this.spinner.hide();
     
      var temp = rescompData;
      var result = temp['data'];
      console.log('Add Result ',result)
        if(temp['type'] == true){
          
                //   var catUpdate : Toast = {
                //     type: 'success',
                //     title: "Asset Version Inserted!",
                //     body: "New asset version added successfully.",
                //     showCloseButton: true,
                //     timeout: 2000
                // };
                // this.toasterService.pop(catUpdate);
                var length = this.addassetservice.breadCrumbArray.length-1
                this.title = this.addassetservice.breadCrumbArray[length].name
                this.addassetservice.title = this.title
                this.addassetservice.breadCrumbArray = this.addassetservice.breadCrumbArray.slice(0,length)
              // this.title = this.addassetservice.breadCrumbArray[]
              this.addassetservice.previousBreadCrumb = this.addassetservice.previousBreadCrumb.slice(0,length+1)
                console.log(this.title,"title")
          this.presentToast('success', 'New File version added');
          this.router.navigate(['../'],{relativeTo:this.routes});


            }else{
              //this.fetchBadgeCateory();
              //   var catUpdate : Toast = {
              //     type: 'error',
              //     title: "Asset Version Inserted!",
              //     body: "Unable to add asset version.",
              //     showCloseButton: true,
              //     timeout: 2000
              // };
              // this.toasterService.pop(catUpdate);
              this.presentToast('error', '');
            }
      
    },
    resUserError=>{
      this.spinner.hide();
      this.errorMsg = resUserError;
    });
  }

  formatTime(time){
    let duration = new Date(time);
    let formatted = this.datePipe.transform(duration, 'H:mm:ss');
    return formatted;
  }
}
