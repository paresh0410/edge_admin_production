import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetVersionComponent } from './asset-version.component';

describe('AssetVersionComponent', () => {
  let component: AssetVersionComponent;
  let fixture: ComponentFixture<AssetVersionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetVersionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetVersionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
