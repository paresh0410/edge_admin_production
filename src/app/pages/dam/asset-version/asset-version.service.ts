import { Injectable, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AppConfig } from '../../../app.module';
import { webApi } from '../../../service/webApi';
import { HttpClient } from "@angular/common/http";

@Injectable()
export class AssetVesrionService {

    tenantId: any = 1;
   // categoryId: any;
   userData;
    getAssetDataForVesrsioning : any;
    tabId : any

    private _urlInsertAssetVersion: string = webApi.domain + webApi.url.insertAssetVersion;
    private _urlGetAllDropdown:string = webApi.domain + webApi.url.getAllAssetDropDown;
    private _urlGetAllEmployees:string = webApi.domain + webApi.url.getAllEmployeeDAM;
    private _urlGetAllEdgeEmployee:string = webApi.domain + webApi.url.getAllEdgeEmployee


    constructor(@Inject('APP_CONFIG_TOKEN') private config: AppConfig, private _http: Http, private _httpClient: HttpClient) {
        //this.busy = this._http.get('...').toPromise();
        if(localStorage.getItem('LoginResData')){
            this.userData = JSON.parse(localStorage.getItem('LoginResData'));
            console.log('userData', this.userData.data);
            // this.userId = this.userData.data.data.id;
            this.tenantId = this.userData.data.data.tenantId;
         }
    }

    insertAssetVersion(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlInsertAssetVersion, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    getAllEmployee(param){
        return new Promise(resolve => {
          this._httpClient.post(this._urlGetAllEmployees, param)
          //.map(res => res.json())
          .subscribe(data => {
              resolve(data);
          },
          err => {
              resolve('err');
          });
      });
      }

      getAllAssetDropdown(param){
        return new Promise(resolve => {
          this._httpClient.post(this._urlGetAllDropdown, param)
          //.map(res => res.json())
          .subscribe(data => {
              resolve(data);
          },
          err => {
              resolve('err');
          });
      });
    }

    getAllEdgeEmployee(param){
        return new Promise(resolve => {
          this._httpClient.post(this._urlGetAllEdgeEmployee, param)
          //.map(res => res.json())
          .subscribe(data => {
              resolve(data);
          },
          err => {
              resolve('err');
          });
      });
    }


    _errorHandler(error: Response) {
        console.error(error);
        return Observable.throw(error || "Server Error")
    }

}
