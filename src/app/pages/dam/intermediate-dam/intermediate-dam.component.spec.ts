import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntermediateDamComponent } from './intermediate-dam.component';

describe('IntermediateDamComponent', () => {
  let component: IntermediateDamComponent;
  let fixture: ComponentFixture<IntermediateDamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntermediateDamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntermediateDamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
