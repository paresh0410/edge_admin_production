import { Component, OnInit, ViewChild, ElementRef,
  AfterViewChecked,OnDestroy, ChangeDetectorRef } from '@angular/core';
import { DomSanitizer } from "@angular/platform-browser";
// import { NgxSpinnerService } from 'ngx-spinner';
import { AssetService } from '../asset/asset.service';
import { SuubHeader } from '../../components/models/subheader.model';
import { webApi } from '../../../service/webApi';
import { HttpClient, HttpEventType } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { ContextMenuComponent } from 'ngx-contextmenu';
import { FormGroup, NgForm } from '@angular/forms';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { webAPIService } from '../../../service/webAPIService';
import { Filter } from '../../../models/filter.modal';
import { NbMenuService, NbSidebarService } from '@nebular/theme';
import { LayoutService } from '../../../@core/data/layout.service';
import { DatePipe } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { AddAssetService } from '../asset/add-asset/add-asset.service';
import * as _ from "lodash";
import { UriBuilder } from 'uribuilder';
import { cleanPath } from 'cleanpath';
import { NgxSpinnerService } from 'ngx-spinner';
import { noData } from '../../../models/no-data.model';
import { ApproveAssetService } from './../approve-asset/approve-asset.service';
import { AssetReviewPolicyService } from './../asset-review-policy/asset-review-policy.service';
declare var mime;

@Component({
  selector: 'ngx-folder-file',
  templateUrl: './folder-file.component.html',
  styleUrls: ['./folder-file.component.scss']

})
export class FolderFileComponent implements OnInit, AfterViewChecked, OnDestroy {
  @ViewChild('ContextMenuComponent' ) public folderMenu: ContextMenuComponent;
  @ViewChild('ContextMenuComponent' ) public fileMenu: ContextMenuComponent;
  @ViewChild('parent') parent?: ElementRef<HTMLElement>;
  @ViewChild('hover') hover?: ElementRef;
  @ViewChild('kpplayer1') kpplayer1: any;
  @ViewChild('assetFo') assetForm: any;
  @ViewChild('fileFolderForm') fileFolderForm: any;
  @ViewChild('categoryFormValidation') categoryFormValidation: any;



  minDesc="20"
  disablePop: boolean = true;
  youtubeurl: any; // = 'https://www.youtube.com/watch?v=n_GFN3a0yj0';
  show: boolean = true;
  disable: boolean = false;
  fileFolder: boolean = false;
  noTabs: boolean = false;
  activeFolder: boolean = false;
  showSpinner: boolean = false;
  getApprovedAssets = false;
  filtersInner: any;
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No file or folder at this time.",
    desc:"",
    titleShow:true,
    btnShow:true,
    descShow:false,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/how-to-add-an-asset-in-the-dam',
  };
  // activeTab: boolean = false;
  treeFolder: boolean = false;
  view: boolean = false;
  valueChanged: number;

  config = {
    height: '200px',
    uploader: {
      insertImageAsBase64URI: true,
    },
    allowResizeX: true,
    allowResizeY: false,
    placeholder: 'Enter Description',
    limitChars: 3000,
    toolbarSticky: false,
  };
  settingsTagDrop = {
    text: 'Select Tags',
    singleSelection: false,
    classes: 'custom-class-example smallHeight',
    primaryKey: 'id',
    labelKey: 'name',
    noDataLabel: 'Search Tags...',
    enableSearchFilter: true,
    searchBy: ['name'],
    maxHeight:250,
    badgeShowLimit:3,
    lazyLoading: true,
  };
  addEditAssetForm: any = {
    assetId: 0,
    assetName: '',
    description: '',
    category: '',
    format: '',
    channel: '',
    language: '',
    estimateCost: '',
    assetRef: '',
    customTags: [],
    selectedCreator: [],
    selectedEmployee: [],
    preViewUrl: '',
    appReq: null,
  };
  addEditAssetFormChangeStore: any;
  tabs: any = [
    {
      name: "All Asset",
    },
    {
      name: "My Asset",
    },
    {
      name: "Share with me",
    },
  ]
  tree: any = [

  ]
  folder: any = [

  ]
  detailsTab: any = [
    {
      id: 0,
      name: "Details",
      active: false,
    },
    {
      id: 1,
      name: "Version",
      active: false,
    },
  ]
  context: any = [
    // {
    //   name: "View Details",
    //   icon: "fas fa-info-circle",
    // },
    // {
    //   name: "Delete Folder",
    //   icon: "fa fa-trash",
    // },
    {
      name: "Edit Folder",
      icon: "fa fa-folder",
    },
    {
      name: "Disable",
      icon: "fa fa-eye-slash",
    },
  ]
  fileContext1: any = [
    {
      name: "View Details",
      icon: "fas fa-info-circle",
    },
    {
      name: "version",
      icon: "fa fa-trash",
    },
    {
      name: "Edit Folder",
      icon: "fa fa-folder",
    },
    {
      name: "Disable",
      icon: "fa fa-eye-slash",
    },
  ]
  Noeye: any = [
    {
      name: "View Details",
      icon: "fas fa-info-circle",
    },
    {
      name: "version",
      icon: "fa fa-trash",
    },
    {
      name: "Edit Folder",
      icon: "fa fa-folder",
    },

  ]
  onlyEye: any = [

    {
      name: "Disable",
      icon: "fa fa-eye-slash",
    },
  ]
  // fileContext: any = [
  //   {
  //     name: "View Details",
  //     icon: "fas fa-info-circle",
  //   },
  //   {
  //     name: "version",
  //     icon: "fa fa-trash",
  //   },
  //   {
  //     name: "Edit Folder",
  //     icon: "fa fa-folder",
  //   },
  //   {
  //     name: "Disable",
  //     icon: "fa fa-eye-slash",
  //   },
  // ]

  version: any = [
    {
      username: "Username",
      dateTime: "03-07-2020 7:43 PM",
    },
    {
      username: "Username",
      dateTime: "03-07-2020 7:43 PM",
    },
    {
      username: "Username",
      dateTime: "03-07-2020 7:43 PM",
    },
    {
      username: "Username",
      dateTime: "03-07-2020 7:43 PM",
    },
    {
      username: "Username",
      dateTime: "03-07-2020 7:43 PM",
    },
    {
      username: "Username",
      dateTime: "03-07-2020 7:43 PM",
    },
    {
      username: "Username",
      dateTime: "03-07-2020 7:43 PM",
    },
  ]
  header: SuubHeader = {
    title:'Digital Assets'
  };

  heigthover: any;
  hoverMeasure: boolean = false;
  top: any;
  calc: any;
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  mytime: Date = new Date();
  roleId: any;
  showSave = false
  // categoryFormValidation: FormGroup;
  // fileFolderForm: FormGroup;
  // assetForm:FormGroup
  tabId: number;
  files: any = [];
  title: string = 'Folder';
  type: any;
  progress: number;
  iconClose: boolean = true;
  enableFileUploadModal: boolean = false;
  file: any;
  fileName = 'Select a file';
  fileUrl: any[];
  url:any
  formatExtensions: any;
  parentCatId: any = null;
  parentCatName: any = null;
  innerTabId: any = null;
  stopMeasure: boolean = false;
  categoryForm: any = {
  categoryPicRef:'assets/images/category.jpg'
  };
  categoryFormChangeStore: any;
  passParams: any;
  fileUploadRes: any;
  fileres: any;
  assetCatImgData: any;
  addAction: any;
  tenantId: any;
  userData: any;
  msg: any;
  showFile: boolean = false;
  tagList: any;
  status: any;
  languages: any;
  filtercon: Filter = {
    ascending: true,
    descending: true,
    showDropdown: true,
    // showDropdown: false,
    dropdownList: [
      { drpName: 'File Name', val: 1 },
      { drpName: 'Created Date', val: 2 },
    ]
  };
  formats: any;
  channels: any;
  categories: any;
  allEmployeesAuto: any;
  allEmployeeEdgeAuto: any;
  settingsCreatorDrop = {
    text: 'Search & Select Approver',
    singleSelection: true,
    classes: 'common-multi',
    primaryKey: 'appId',
    labelKey: 'name',
    noDataLabel: 'Search Approver...',
    enableSearchFilter: true,
    searchBy: ['id', 'name'],
    position: 'top',
    maxHeight:250,
    badgeShowLimit:3,
    lazyLoading: true,
  };

  settingsEmployeeDrop = {
    text: 'Search & Select Author',
    singleSelection: false,
    classes: 'common-multi',
    primaryKey: 'id',
    labelKey: 'name',
    noDataLabel: 'Search Author...',
    enableSearchFilter: true,
    // searchBy: ['code', 'name'],
    maxHeight:250,
    lazyLoading: true,
    badgeShowLimit:3,
    addNewItemOnFilter: false,
    limitSelection: null
  };
  selectedCreator: any = [];
  selectedEmployee: any = [];
  selectedTags: any= [];
  filters: any = [];
  filter: boolean;
  isShowApprover: boolean = false;
  isApprover = 0;
  userId: any;
  errorMsg: any;
  dupFileError: any;
  fileExistModal: boolean;
  assetFileData: any;
  isLocalfile: any;
  category: any;
  categoryName: any;
  isAdmin: any;
  // tabs: any;

  /**
   * Temp Cache Value
   */
  tempFolders:any = [];
  tempFiles: any = [];
  tempSearchFiles: any = [];
  tempSearchFolders: any = [];
  filterClearData: boolean = false;
  dummy: any;
  name: any;
  icon: any;
  External: boolean = false;
  special: boolean = false;
  previewPopup: boolean;
  breadcrumbArray: any = [
    {
    'name': 'DAM',
    'navigationPath': '/pages/dam',
  },{
    'name': 'Folder ',
    'navigationPath': '/pages/dam/folders',
    'id':{},
    'sameComp':true,
    'assetId':null
  }
];
  previousBreadCrumb: any = [
    {
      'name': 'DAM',
      'navigationPath': '/pages/dam',
    }, {
      'name': 'Folder',
      'navigationPath': '/pages/dam/folders',
      'id': {},
      'sameComp': true,
      'assetId': null
    }
  ]
  breadObj: {
  name: any;
    // 'navigationPath': '',
    id: any; navigationPath: string; sameComp: boolean; assetId: any;
  };
  selectedTab: any;
  assetFo: any;
  assetFoFormChanges: any;
  oldParentCatId: any;
  selectedEmployees: any=[];
  selectedEdgeEmployee: any=[];
  isview: number;
  defaultValFlag: any;
  nodata: boolean=false;
  vieweFileFolderField: boolean;
  estLen: any;
  index: any;
  defaultValJson: any;
  isAprrove: any;
  isEdit: any;
  isEditFlag: boolean=false;
  statusId: any;
  isAuthor: number=0;
  catCpy: any;
  catNameCpy: any;
  versionShow: any;
  primaryButtonFlag: any;
  // Active: any;
  constructor(private sanitizer: DomSanitizer,private assetservice: AssetService,
    private approveAssetService: ApproveAssetService,
    private cdf: ChangeDetectorRef,
    private spinner: NgxSpinnerService,
    private http1: HttpClient,private toastr: ToastrService,
    public router: Router,
    public routes: ActivatedRoute,
    private datePipe: DatePipe,
    private addassetservice: AddAssetService,
    protected webApiService: webAPIService,
    private assetReviewPolicyService: AssetReviewPolicyService,
    private sidebarService: NbSidebarService, private layoutService: LayoutService,
    private commonFunctionService: CommonFunctionsService) {
      if (localStorage.getItem('LoginResData')) {
        this.userData = JSON.parse(localStorage.getItem('LoginResData'));
        console.log('userData', this.userData.data);
         this.userId = this.userData.data.data.id;
        this.tenantId = this.userData.data.data.tenantId;
      }
    if (localStorage.getItem('damTabs')) {
      this.tabs = JSON.parse(localStorage.getItem('damTabs'));
    }
    if (localStorage.getItem('isAdmin')) {
      this.isAdmin = JSON.parse(localStorage.getItem('isAdmin'));
      console.log(this.isAdmin,"fftftyf")
      this.primaryButtonFlag = this.isAdmin.primaryBtnFlag

    }
    if (localStorage.getItem('defaultValFlag')) {
      this.defaultValJson = JSON.parse(localStorage.getItem('defaultValFlag'));
      this.defaultValFlag=this.defaultValJson.defaultValueReq;
      this.isAprrove=this.defaultValJson.isApprove;
      this.isEdit=this.defaultValJson.fileEditFlag;
      console.log(this.defaultValJson,"defaultValFlag")

    }
    this.isAdmin = this.isAdmin.isAdmin

    this.roleId = this.tabs[0].roleId
    this.tabId = this.tabs[0].tabId
    // this.Active = this.tabs[0].isActive
    if (localStorage.getItem('formatExtensions')) {
      this.formatExtensions = JSON.parse(localStorage.getItem('formatExtensions'));
    }
    this.header ={
      title:'Digital Assets'
    };
    this.cdf.markForCheck()
    // if(this.addassetservice.categoryId!=null){
    //   this.parentCatId = this.addassetservice.categoryId
    //   this.tabId = this.addassetservice.tabId
    //   for(let index = 0; index < this.tabs ; index ++){
    //           if(this.tabId === this.tabs[index].tabId){
    //               this.roleId = this.tabs[index].roleId;
    //             }
    //     }
    //   this.innerTabId = this.addassetservice.innerTabId
    // }
    // if(this.addassetservice.backFlag!=true){
    if(this.addassetservice.shareCatId!= null || this.addassetservice.categoryId!=null){
      this.title = this.addassetservice.title
      this.parentCatId = this.addassetservice.categoryId
      this.breadcrumbArray = this.addassetservice.breadCrumbArray
      this.previousBreadCrumb = this.addassetservice.previousBreadCrumb
      this.header.breadCrumbList = this.breadcrumbArray
      this.header.title = this.title
      this.innerTabId=this.addassetservice.innerTabId
      this.roleId=this.addassetservice.roleId
      this.tabId=this.addassetservice.tabId
    }
  // }
  if(this.isAprrove==0){
    this.getFileAndFolders(this.parentCatId,this.roleId,this.tabId,this.innerTabId)
    this.getTree(this.tabs[0], false)
    this.getallTagList();
    this.getAllAssetDropdown();
    // this.getAllCatWise(this.parentCatID)
    this.getAllEmployeesDAM();
    this.fetchDAMStorageDetails();
    this.getAllEdgeEmployee('');
  }
  if(this.isAprrove==1){
    this.getAllFolders(this.tabs[0])
  }

    if(this.addassetservice.valuechanged ){
      this.valueChanged = this.addassetservice.valuechanged
    }
    // this.tabs[0].expand = true;
    // this.getallTagList();
    // this.getAllAssetDropdown();
    // // this.getAllCatWise(this.parentCatID)
    // this.getAllEmployeesDAM();
    // this.fetchDAMStorageDetails();
    // this.getAllEdgeEmployee('');
    this.filterClearData = true;
   }

  ngOnInit() {
    this.spinner.show()
    setTimeout(() => {
    this.spinner.hide()
    }, 2000);
    this.dropdownList = [
      { "id": 1, "itemName": "India" },
      { "id": 2, "itemName": "Singapore" },
      { "id": 3, "itemName": "Australia" },
      { "id": 4, "itemName": "Canada" },
      { "id": 5, "itemName": "South Korea" },
      { "id": 6, "itemName": "Germany" },
      { "id": 7, "itemName": "France" },
      { "id": 8, "itemName": "Russia" },
      { "id": 9, "itemName": "Italy" },
      { "id": 10, "itemName": "Sweden" }
    ];
    this.selectedItems = [
      { "id": 2, "itemName": "Singapore" },
      { "id": 3, "itemName": "Australia" },
      { "id": 4, "itemName": "Canada" },
      { "id": 5, "itemName": "South Korea" }
    ];
    this.dropdownSettings = {
      badgeShowLimit: 1,
      lazyLoading: true,
      showCheckbox: true,
      escapeToClose: false,
      singleSelection: false,
      enableSearchFilter: true,
      text: "Select Countries",
      selectAllText: 'Select All',
      noDataLabel: "No data found",
      unSelectAllText: 'UnSelect All',
      classes: "custom-class-example smallHeight"
    };
    this.toggleSidebar('close');
  }

  ngAfterViewChecked() {
    // console.log('this.hover', this.hover);
    if (this.view == true && this.stopMeasure == true) {
      this.detailSection();
      this.stopMeasure = false;
    }
    if(this.hoverMeasure == true) {
      this.hoverable();
      this.hoverMeasure = false;
    }
  }

  showFolder(item,tab) {
  if(item['isActive'] != 1){
    this.valueChanged = null
  }else{
    if(this.selectedFolder.assetId){
      this.valueChanged =  this.selectedFolder.assetId
    }
  }
  //new code
    // this.getActive()
    // item['isActive'] = 1
    item['expand'] = !item['expand'];
    // this.getExpand()
    //end new code
    this.selectedTab = item

    this.getTree(item , false)

    // this.valueChanged = this.category
  }

  detailSection() {
    var rect = this.parent.nativeElement.getBoundingClientRect();
    var normalh = rect.height.toFixed() + 'px';
    console.log('normalh', normalh);
    setTimeout(() => {
      this.calc = this.sanitizer.bypassSecurityTrustStyle(`calc(100vh - (${normalh} + 114px))`);
      this.top = this.sanitizer.bypassSecurityTrustStyle(normalh);
    }, 100);
  }

  hoverable() {
    this.heigthover = this.hover.nativeElement.getBoundingClientRect().top;
    var hoversec = this.hover.nativeElement;
    if (this.heigthover > 550 ) {
      hoversec.classList.add('window-top');
    }
    // var queryElement = document.querySelector('#hover_folder')
  }

  mouseup(index, item, element) {
    if(item.description) {
      for (let k = 0; k < this.folder.length; k++) {
        if (k == index) {
          this.folder[k].isHoverDesc = 1;
          this.hoverMeasure = true;
          element['hidden'] = false;
          this.cdf.detectChanges();
          break;
        }
      }
    }
  }

  mousedown(index,element, menuItem) {
    for (let k = 0; k < this.folder.length; k++) {
      if (k == index) {
        this.folder[k].isHoverDesc = 0;
        this.folder[k].isMenu = 0;
        element['hidden'] = true;
        menuItem['hidden'] = true;
        this.cdf.detectChanges();
        break;
      }
    }
  }

  openfolderCtxt(index, menuItem) {
    for (let k = 0; k < this.folder.length; k++) {
      if (k == index) {
        menuItem['hidden']= false;
        this.folder[k].isMenu = 1;
        break;
      }
    }
  }

  openfileCtxt(index, fileMenu) {
    for (let k = 0; k < this.files.length; k++) {
      if (k == index) {
        fileMenu['hidden']= false;
        this.files[k].isMenu = 1;
        break;
      }
    }
  }

  closeFilectxt(index, filemenu) {
    for (let k = 0; k < this.files.length; k++) {
      if (k == index) {
        this.files[k].isMenu = 0;
        filemenu['hidden'] = true;
        break;
      }
    }
  }

  treeMoves = [];
  noTressMoves = false;
  getTree(item, moveFlag){
    var demo = [];
    let param = {};
    if(moveFlag){
      param = {
        flag: 2,
        tabId: 1,
        roleId: 12,
        userId: this.clickedAssetData.userCreated,
        moveFlag: moveFlag,
      }
    }else {
     param = {
        flag:2,
        tabId:item.tabId,
        roleId:item.roleId,
        userId: this.clickedAssetData ? this.clickedAssetData.userCreated:null,
        moveFlag: moveFlag,
      }
    }
    if(moveFlag){
      this.spinner.show();
    }
    this.assetservice.getAllTree(param).then(res=>{
      console.log(res,"tree")
      if(moveFlag){
        this.treeMoves = res['data'];
        if(this.treeMoves){
          for(let i = 0;i<this.treeMoves.length;i++){

            if(this.treeMoves[i].parentCatId == null){
              demo.push(this.treeMoves[i])
            }
          }
        }
        if(this.treeMoves.length === 0){
          this.noTressMoves = true;
        }else {
          this.noTressMoves = false;
        }
        this.treeMoves = demo;
        this.assetservice.tree = res['data'];
        // if(moveFlag){
          this.spinner.hide();
        // }
      }else {
        this.tree = res['data'];
        if(this.tree){
          for(let i = 0;i<this.tree.length;i++){

            if(this.tree[i].parentCatId == null){
              demo.push(this.tree[i])
            }
            }
        }

        this.tree = demo
        this.assetservice.tabActive = item.isActive
        this.assetservice.tree = res['data']
      }


})
  }
  back1(){
    this.filter = false
    if(this.parentCatId == null) {
      this.router.navigate(['/pages/dam']);

    } else {
      this.view=false;
      var index = this.breadcrumbArray.length - 1
      // for(let i =0 ;i<this.breadcrumbArray.length;i++){
        this.title = this.breadcrumbArray[index].name
      // }
      this.breadcrumbArray.pop()
      this.previousBreadCrumb.pop()
      this.getFileAndFolders(this.oldParentCatId,this.roleId,this.tabId,this.innerTabId);
      this.parentCatId = this.oldParentCatId
    // this.router.navigate(['../../dam'], { relativeTo: this.routes });
    }
  }

  bindfilter(obj) {
    let filtername, filterValueName, type, singleSelection;
    if (obj.length > 0) {
      filtername = obj[0]['filterId'];
      filterValueName = obj[0]['filterValue'];
      type = obj[0]['type'];
      singleSelection = obj[0]['singleSelection'];
    }
        const item = {
          count: "",
          value: "",
          tagname: obj.length > 0 ? obj[0]['filterId'] : '',
          isChecked: false,
          list: obj,
          filterValue: filterValueName,
          type: type,
          filterId: filtername,
          singleSelection: singleSelection,
        }
        if (filtername) {
          this.filters.push(item);
        }
    // if (result['data'] && result['data'].length > 0) {
    //   result['data'].forEach( (value, index) => {

    //   })
    // }
  }
  filteredChanged(event){
    if (event.empty) {
      this.files = this.tempFiles;
      // this.folder = this.tempFolders;
    } else {
      // this.filterFolder(event);
      this.filterFiles(event);
    }
    this.tempSearchFiles = this.files;
    this.tempSearchFolders = this.folder;
    this.allFolderCombined = this.folder.concat(this.files);
  }

  filterFiles(event) {
    this.files = [];
    let indexObj = {};
    let filterId;
    let filterValue;
    const findElement = (element) => String(element[filterId]).toLowerCase() === String(filterValue).toLowerCase();
    // tslint:disable-next-line:forin
    for (const x in event) {
        filterId = x;
        if (event[x] && Array.isArray(event[x])) {
          event[x].forEach((value, key) => {
            filterValue = value[filterId];
            // for(let i = 0; i<this.tempFiles.length; i++) {
            //   // const indexnumber = this.tempFiles.findIndex(findElement);
            //   // indexObj[indexnumber] = true;
            // }
            let indexnumberArray = [];
            if (filterId.startsWith('tag')) {
              // tslint:disable-next-line:max-line-length
              indexnumberArray = this.tempFiles.map((e, j) => String(e[filterId]).includes(String(filterValue).toLowerCase()) ? j : '').filter(String);
            } else {
            // tslint:disable-next-line:max-line-length
              indexnumberArray = this.tempFiles.map((e, j) => String(e[filterId]).toLowerCase() === String(filterValue).toLowerCase() ? j : '').filter(String);
// indexObj[indexnumber] = true;
            }
            indexObj = Object.assign(indexObj, indexnumberArray.reduce((a,b)=> (a[b]=true,a),{}));
            // const indexnumber = this.tempFiles.findIndex(findElement);
            // indexObj[indexnumber] = true;
        });
        }
    }
    console.log(JSON.stringify(indexObj));
    // tslint:disable-next-line:forin
    for (const y in indexObj) {
      if (String(y) !== '-1'){
        this.files.push(this.tempFiles[y]);
      }
    }
  }
  filterFolder(event:any) {
    this.folder = [];
    let indexObj = {};
    let filterValue;
    let filterId;
    // const findElement = (element) => element[filterId] === filterValue;
    // const findElement = map((e, i) => e[filterId] === filterValue ? i : '').filter(String);
    // tslint:disable-next-line:forin
    for (const x in event) {
        filterId = x;
        if (event[x] && Array.isArray(event[x])) {
          event[x].forEach((value, key) => {
              filterValue = value[filterId];
              // for(let i = 0; i<this.tempFolders.length; i++) {
              //   // const indexnumber = this.tempFolders.findIndex(findElement);
              //   // tslint:disable-next-line:max-line-length
              //   const indexnumberArray =
              // this.tempFolders.map((e, j) => e[filterId] === filterValue ? j : '').filter(String);
              //   // indexObj[indexnumber] = true;
              //   indexObj = indexnumberArray.reduce((a,b)=> (a[b]=true,a),{});
              // }
              let indexnumberArray = [];
              if (filterId.startsWith('tag')) {
                // tslint:disable-next-line:max-line-length
                indexnumberArray = this.tempFolders.map((e, j) => String(e[filterId]).includes(String(filterValue).toLowerCase()) ? j : '').filter(String);
              } else {
                // tslint:disable-next-line:max-line-length
                indexnumberArray = this.tempFolders.map((e, j) => String(e[filterId]).toLowerCase() === String(filterValue).toLowerCase() ? j : '').filter(String);
              }
                // indexObj[indexnumber] = true;
                indexObj = Object.assign(indexObj, indexnumberArray.reduce((a,b)=> (a[b]=true,a),{}), indexObj);
          });
        }
    }
    console.log(JSON.stringify(indexObj));
    // tslint:disable-next-line:forin
    for (const y in indexObj) {
      if (String(y) !== '-1') {
        this.folder.push(this.tempFolders[y]);
      }
    }
  }
  SearchFilter(event) {
    this.nodata=false;
    const val = event == undefined ? '' : event.target.value.toLowerCase();
    // const files = this.files;
    // let keys = [];
    if (val) {
      // if (this.tempSearchFiles.length > 0) {
      //   keys = Object.keys(this.tempSearchFiles[0]);
      // }
      if(val.length>=3) {
        // console.log("tempSearchFiles", this.tempSearchFiles.length);
        // console.log("tempFolder", this.tempSearchFolders.length);
        // const tempFiles = this.tempSearchFiles.filter((d) => {
        //   for (const key of keys) {
        //     if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
        //       return true;
        //     }
        //   }
        // });
        const tempFiles = this.tempSearchFiles.filter(function (d) {
          return String(d.assetName).toLowerCase().indexOf(val) !== -1 ||
            // d.description.toLowerCase().indexOf(val) !== -1 ||
            // d.relayDate.toLowerCase().indexOf(text) !== -1 ||
            // d.manager.toLowerCase().indexOf(val) !== -1 ||
            !val
        })
        this.files = _.cloneDeep(tempFiles);
        // const tempFolder = this.tempSearchFolders.filter((d) => {
        //   for (const key of keys) {
        //     if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
        //       return true;
        //     }
        //   }
        // });
        const tempFolder = this.tempSearchFolders.filter(function (d) {
          return String(d.assetName).toLowerCase().indexOf(val) !== -1 ||
            // d.description.toLowerCase().indexOf(val) !== -1 ||
            // d.relayDate.toLowerCase().indexOf(text) !== -1 ||
            // d.manager.toLowerCase().indexOf(val) !== -1 ||
            !val
        })
        if(tempFiles.length==0&&tempFolder.length==0){
          this.nodata=true;
          this.noDataVal={
            margin:'mt-5',
            imageSrc: '../../../../../assets/images/no-data-bg.svg',
            title:"Sorry we couldn't find any matches please try again",
            desc:".",
            titleShow:true,
            btnShow:false,
            descShow:false,
            btnText:'Learn More',
            btnLink:''
          }
        }
        this.folder = _.cloneDeep(tempFolder);
        this.allFolderCombined = this.folder.concat(this.files);
    } else {
      if(val.length==0){
        this.files = _.cloneDeep(this.tempSearchFiles);
        this.folder = _.cloneDeep(this.tempSearchFolders);
        this.allFolderCombined = this.folder.concat(this.files);
      }
    }
  } else {
    this.files = _.cloneDeep(this.tempSearchFiles);
    this.folder = _.cloneDeep(this.tempSearchFolders);
    this.allFolderCombined = this.folder.concat(this.files);
 }

}

getSortData(event){
  console.log(event,"sorting")
  if(event.selectedLevel == 1){
    var property = 'assetName'
  }else{
    var property = 'timemodified'
  }
  var array = this.files
  // console.log(this.displayArray,"huhuhuhu")
  var dummyArray = array.sort(this.commonFunctionService.sort(property,event.selectedRadio))
  console.log(dummyArray,"dummyArray")
  this.files = array
  this.allFolderCombined = this.folder.concat(this.files);
}
showContentBoxSpinner = false;
// SearchFilter(event) {

//   this.showContentBoxSpinner = true;
//   let params = {
//     query: event == undefined ? '' : event.target.value.toLowerCase(),
//   };
//   // this.spinner.show();
//   const _urlgetsearchDAMContent: string = webApi.domain + webApi.url.searchDAMContent;
//   this.commonFunctionService.httpPostRequest(_urlgetsearchDAMContent,params)
//   .then(
//     result => {
//       // this.spinner.hide();
//       console.log('DAM search', result);
//       let responseData = result['result']['response'];
//       if(responseData && responseData['numFound']){
//         this.files = responseData['docs'];
//         this.folder = [];
//       }else{
//         this.files = [];
//         this.folder = [];
//       }
//       this.showContentBoxSpinner = false;
//     },
//     resUserError => {
//       this.showContentBoxSpinner = false;
//       // this.spinner.hide();
//       // this.errorMsg = resUserError;
//     }
//   );
// }
  onSubmit(data, assetForm) {
    // this.spinner.show();
    // this.cdf.detectChanges();
    if(this.files.length!=0&&this.addAction){
      for(var x in this.files){
        if(this.files[x].assetName.toLowerCase()==data.assetName.toLowerCase()){
          this.presentToast('warning',"File name already exist");
          return
        }
      }
    }
    if(this.files.length!=0&&!this.addAction){
      for(var x in this.files){
        if(this.files[x].assetName.toLowerCase()==data.assetName.toLowerCase()&&this.index.toString()!=x){
          this.presentToast('warning',"File name already exist");
          return
        }
      }
    }
    if(this.isShowApprover){
      if(this.selectedCreator && Array.isArray(this.selectedCreator) &&this.selectedCreator.length == 0){
        this.presentToast('warning',"Please select approver");
        return
      }
    }
    console.log(data.assetRef,"assetRef")
    console.log(this.assetForm,"assetForm")
    console.log(this.selectedTags,"selected Tags")
    // this.submitted = true;
    console.log('assetForm', assetForm);
    console.log('data', data);
    // this.selectedTags.length > 0
    if (this.assetForm.valid) {
      if (data.assetRef) {
        console.log('data', data);
        this.spinner.show();
        console.log('data', data);
        // var tagStr = this.getTagDataReady(this.selectedTags);
        // console.log('tagStr', tagStr);
        let mimeType = null;
      let referenceType = null;
      if (data.assetRef) {
        const document = this.getMimeType(data.assetRef);
        console.log(document,"document")
        if (data.assetRef.includes('kapsule') || data.assetRef.includes('kpoint')) {
          mimeType = 'embedded/kpoint';
          referenceType = 'kpoint';
        } else if (data.assetRef.includes('youtube')) {
          mimeType = 'embedded/youtube';
          referenceType = 'youtube';
        } else if (document) {
          mimeType = document['mimetype'];
          referenceType = document['documenttype'];
        } else if (data.assetRef.includes('http') || data.assetRef.includes('https')){
          mimeType = 'application/x-msdownload';
          referenceType = 'application';
        }
      }
      if (this.selectedTags.length > 0) {
        this.makeTagDataReady(this.selectedTags);
         // this.formdata.tags = this.formattedTags;
       }
      //  if(data.selectedEmployee){
       if(this.selectedEmployee){
       this.makeAuthorDataReady(data.selectedEmployee)
       }
        const param = {
          assetsId: this.addAction ? 0 : data.assetId,
          assetsName: data.assetName,
          // 'assetsRef': this.addAction ? null : this.getDataForAddEdit[1].assetRef,
          assetsRef: data.assetRef,
          assetsBitly: null,
          assetsDesc: data.description?data.description:null,
          // assetsCatId: data.category,
          assetsCatId: this.category,
          assetsFormatId: data.format,
          assetsChannelId: data.channel,
          assetsLanguageId: data.language,
          assetsCost: data.estimateCost,
          tId: this.tenantId,
          userId: this.userId,
          assetsStatus: this.addAction ? 3 : data.statusId,
          assetsTags: this.tagstrAsset,
          approverId: this.isShowApprover?this.selectedCreator[0].appId:null,
          appReq:this.isApprover,
          // assetsAuthor: data.selectedEmployee[0].edgeempId,
          assetsAuthor :this.author,
          assetsLen: data.time ? data.format == 3 || data.format == 5 || data.format == 7 ?
                     null : this.formatTime(data.time) : null,
          mimeType: mimeType || data.mimeType,
          referenceType: referenceType || data.referenceType,
          // Solr Content Params
          name: data.assetName,
          tagging: this.tagstrAsset,
          description: data.description,
          // author: data.selectedEmployee[0].edgeempName,
          author:this.author,
          categoryName: this.categoryName,
          // assetSize:null,
          // assetKey:null
        };
        this.isShowApprover=false
        if(this.addAction){
            if(this.isApprover){
              param['assetsStatus'] = 3;
            }else {
              param['assetsStatus'] = 1;
            }
        }
        this.passParams = param;

        if (
          this.passParams.assetRef == 4 ||
          this.passParams.assetsFormatId == 4
        ) {
          this.passParams.mimeType = mimeType || 'embedded/kpoint';
          this.passParams.referenceType = referenceType || 'kpoint';
        }

        if (
          this.passParams.assetRef == 6 ||
          this.passParams.assetsFormatId == 6
        ) {
          this.assetFileData = null;
          this.passParams.mimeType = mimeType || 'embedded/youtube';
          this.passParams.referenceType = referenceType || 'youtube';
        }
        if (
          this.passParams.assetRef == 8 ||
          this.passParams.assetsFormatId == 8
        ) {
          this.assetFileData = null;
          this.passParams.mimeType = mimeType || 'application/x-msdownload';
          this.passParams.referenceType =  referenceType || 'application';
        }
        console.log('this.passParams', this.passParams);
        if (this.isLocalfile) {
          var fd = new FormData();
          fd.append('content', JSON.stringify(this.passParams));
          fd.append('file', this.assetFileData);
          console.log('File Data ', fd);

          console.log('Asset File', this.assetFileData);
          console.log('Asset Data ', this.passParams);
          let fileUploadUrl = webApi.domain + webApi.url.fileUpload;

          if (this.assetFileData) {
            this.webApiService.getService(fileUploadUrl, fd).then(
              rescompData => {
                const temp: any = rescompData;
                this.fileUploadRes = temp;
                console.log('rescompData', this.fileUploadRes);

                if (temp == 'err') {
                  this.presentToast('error', '');
                  this.spinner.hide();
                } else if (temp.type === false) {
                  this.presentToast('error', '');
                  this.spinner.hide();
                } else {
                  if (this.fileUploadRes.data != null || this.fileUploadRes.fileError !== true) {
                    // this.assetFileData = null;
                    if ( this.fileUploadRes.fileExists ) {
                      this.fileExistModal = true;
                      this.dupFileError = this.fileUploadRes.status;
                      this.spinner.hide();
                    } else {
                      this.passParams.assetsRef = this.fileUploadRes.data.file_url;
                      this.passParams.mimeType = this.fileUploadRes.data.mime_type;
                      this.passParams.referenceType = this.fileUploadRes.data.file_type;
                      this.passParams.assetKey = this.fileUploadRes.data.filename;
                      this.passParams.assetSize = this.fileUploadRes.data.size;
                      this.addEditAsset(this.passParams);
                      console.log('this.passparams', this.passParams);
                    }
                    // this.spinner.hide();
                  } else {
                    this.presentToast('error', '');
                    this.spinner.hide();
                  }
                }
                console.log('File Upload Result', this.fileUploadRes);
                const res = this.fileUploadRes;
              },
              resUserError => {
                this.errorMsg = resUserError;
                console.log('File upload this.errorMsg', this.errorMsg);
                this.spinner.hide();
              }
            );
          }else {
            this.passParams.assetKey = null
            this.passParams.assetSize = null
           this.spinner.hide();
            this.addEditAsset(this.passParams);
          }
        } else {
          this.passParams.assetKey = null
          this.passParams.assetSize = null
          this.addEditAsset(this.passParams);
        }
      } else {
        this.presentToast('warning', 'Please select file.');
        console.log('please select file');
      }
    } else {
      this.presentToast('warning', 'Please fill in the required fields');
      Object.keys(this.assetForm.controls).forEach(key => {
        console.log('this.assetForm.controls[key]', this.assetForm.controls[key]);
        this.assetForm.controls[key].markAsDirty();
      });
    }

  }
  formatTime(time) {
    let duration = new Date(time);
    let formatted = this.datePipe.transform(duration, 'H:mm:ss');
    console.log('formatted', formatted);
    let str = new String(formatted);
    let formattedFinal = str.replace(/:/g, '.');
    console.log('formattedFinal', formattedFinal);
    return formattedFinal;
  }
  FileEntity: any = {
    assetName: 'Name',
    description: 'Description',
    selectedEmployee: 'Author',
    selectedCreator: 'Approver',
    customTags: 'Custom Tags',
    channel:'Channel',
    language:'Language',
    estimateCost:'Estimated Cost ( INR )',
    time:'Estimated Time '
  }

  addEditAsset(params) {
    console.log('AssetPushData:', params);
    this.spinner.show();
    this.addEditAssetForm.selectedCreator = this.selectedCreator;
    this.addEditAssetForm.selectedEmployee = this.selectedEmployee;
    this.addEditAssetForm.customTags = this.selectedTags;
    const changeproperty = this.compareChangedData(this.addEditAssetForm, this.addEditAssetFormChangeStore,this.FileEntity);
    const _urlAddEditAsset: string = webApi.domain + webApi.url.addEditAsset;
    params.changeproperty = changeproperty;
    this.commonFunctionService.httpPostRequest(_urlAddEditAsset,params)
    // this.addassetservice.addEditAsset(params)
    .then(
      rescompData => {
        this.spinner.hide();

        var temp = rescompData;
        var result = temp['data'];
        console.log('Add Result ', result);
        if (params.assetsId == 0) {
          if (temp['type'] == true) {
          this.getFileAndFolders(this.parentCatId,this.roleId,this.tabId,this.innerTabId);
          this.fetchDAMStorageDetails();
          this.closeDetail()
          this.presentToast('success', 'A new file added.');
          if(this.assetForm){
              this.assetForm.reset();
          }
            // this.router.navigate(['../'], { relativeTo: this.routes });
            // this.router.navigate(['../../newasset'], { relativeTo: this.routes });
          } else {
            this.presentToast('error', '');
          }
        } else {
          console.log('Asset Edit Result ', result);
          if (temp['type'] == true) {
          this.getFileAndFolders(this.parentCatId,this.roleId,this.tabId,this.innerTabId);

              this.closeDetail()
              this.presentToast('success', 'File updated.');


            // this.router.navigate(['../'], { relativeTo: this.routes });
            // this.router.navigate(['../../newasset'], { relativeTo: this.routes });

          } else {
            this.presentToast('error', '');
          }
        }
      },
      resUserError => {
        this.spinner.hide();
        // this.errorMsg = resUserError;
      }
    );
  }

  tagstrAsset:string ='';
  makeTagDataReady(tagsData) {
    this.tagstrAsset  =''
     tagsData.forEach((tag)=>{
      if(this.tagstrAsset  == '')
      {
        this.tagstrAsset  = tag.id;
      }else
      {
        this.tagstrAsset = this.tagstrAsset +'|' + tag.id;
      }
      console.log('this.tagstrAsset',this.tagstrAsset);
     });
    // var tagsData = this.formdata.tags;
    // var tagsString = '';
    // if (tagsData) {
    //   for (let i = 0; i < tagsData.length; i++) {
    //     var tag = tagsData[i];
    //     if (tagsString != "") {
    //       tagsString += "|";
    //     }
    //     if (String(tag) != "" && String(tag) != "null") {
    //       tagsString += tag;
    //     }
    //   }
    //   this.formdata.tags1 = tagsString;
    // }
  }
  author:string

  makeAuthorDataReady(Data) {
    this.author  =''
     Data.forEach((tag)=>{
      if(this.author  == '')
      {
        this.author  = tag.id;
      }else
      {
        this.author = this.author +'|' + tag.id;
      }
      console.log('this.author',this.author);
     });
    // var tagsData = this.formdata.tags;
    // var tagsString = '';
    // if (tagsData) {
    //   for (let i = 0; i < tagsData.length; i++) {
    //     var tag = tagsData[i];
    //     if (tagsString != "") {
    //       tagsString += "|";
    //     }
    //     if (String(tag) != "" && String(tag) != "null") {
    //       tagsString += tag;
    //     }
    //   }
    //   this.formdata.tags1 = tagsString;
    // }
  }
  getMimeType(url) {
    const mimevalue = mime.getType(url);
    if (mimevalue) {
        const documenttype = mimevalue.split("/")[0];
        const result = {
            mimetype: mimevalue,
            documenttype: documenttype
        };
        return result;
    } else {
        return mimevalue;
    }
}
  addeditccategory(){
    this.setEmployeeSettingToDefault();
    this.addAction = true
    this.categoryForm = {
      id: '',
      name: '',
      code: '',
      desc: '',
      categoryPicRef: '',
    };
    this.iconClose = true
    this.showSave = true
    this.view = true
    this.fileFolder = true
    this.showFile = false;
    this.vieweFileFolderField=false;
    this.name ='';
    this.icon = ''
  }
  deleteAssetCatImage() {
    this.assetCatImgData = null;
    // this.categoryForm.categoryPicRef ='https://bhaveshedgetest.s3.ap-south-1.amazonaws.com/BFL.jpg';
    this.categoryForm.categoryPicRef ='assets/images/category.jpg';

  }
  readAssetCatImage(event: any) {
    // this.fileexterror = false;
    var validExts = new Array(".png", ".jpg", ".jpeg");
    var fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
      // this.fileexterror = true;
    } else {
      if (event.target.files && event.target.files[0]) {
        this.assetCatImgData = event.target.files[0];

        var reader = new FileReader();

        reader.onload = (event: ProgressEvent) => {
          this.categoryForm.categoryPicRef = (<FileReader>event.target).result;
        }
        reader.readAsDataURL(event.target.files[0]);
      }
    }
  }

  contextItem(event,data) {
    this.icon = ''
    this.addAction = false
    console.log(event,data);
    if(event.item){
    var catData = event.item
    }else{
      catData = event
    }

    if (this.dummy == 'Enable' || this.dummy == 'Disable'){
      this.fileFolder = false
      this.showFile = false
      this.view = false;
      this.changeCategoryStatus(catData)
    }
    else if(this.dummy == 'Delete'){
      this.deleteFolderorFile(catData,1, 1)
    }else if(this.dummy == 'Restore'){
      this.deleteFolderorFile(catData,1, 0)
    }else if(this.dummy == 'deleteForever'){
      this.deleteFileFolderForever(catData);
    }else if(this.dummy == 'shareAsset'){
      this.clickedAssetData = catData;
      this.toggelShareAsset(true);
    }else if(this.dummy == 'moveAsset'){
      this.clickedAssetData = catData;
      // this.getTree(null, true);
      this.getMoveAssetTree(null,true)
      this.toggelMoveAssets(true);
    }else if(this.dummy == 'Download'){
      this.fileFolder = false
      this.showFile = false
      this.view = false;
    }
    else{
    this.name  = catData.assetName
    var selectedEdgeEmployee=[];
    if(this.allEmployeeEdgeAuto.length>0){
    if(catData['catAuthors']){
    var author = catData['catAuthors'].split(',');
  //
    this.allEmployeeEdgeAuto.forEach((emp) => {
      author.forEach((auth) =>{
      if (emp.id == auth) {
         selectedEdgeEmployee.push(emp);
      }
    });
    });
  }
}
   this.updateDropDownSetting(catData['catAuthors']);
    this.categoryForm = {
      id: catData.assetId,
      name: catData.assetName,
      code: catData.categoryCode,
      desc: catData.description,
      categoryPicRef: catData.categoryPicRef,
      selectedEmployee:selectedEdgeEmployee
    }
    this.categoryFormChangeStore = _.cloneDeep(this.categoryForm);
    //new change
    // this.showSave = true
    if(this.dummy == 'viewDetails'){
      this.showSave = false
      }else{
        this.showSave = true
      }
      //end change
    this.showFile = false
    this.fileFolder = true
    this.view = true;
    }
    this.detailSection()
    this.stopMeasure = true


  }
  addVersion(){
    this.dummy="Version"
    var data=this.files[this.index];
    this.activeTab=this.detailsTab[0]
    this.getAssestDetail(data)
  }
  selectedAction = 'editFile1';
  clickedAssetData = null;
  showBrowse = false;

  contextFileItem(event){
    console.log(event,this.dummy,"jvcxxrer")
    this.addAction = false
    if(event.item){
    var assetData = event.item
    }else{
      var assetData = event
    }
    if(this.dummy == 'Download'){

    }
    else if(this.dummy == 'Enable' || this.dummy == 'Disable'){
      this.fileFolder = false
      this.showFile = false
      this.view = false;
      this.changevisiblestatus(assetData)
    }else if(this.dummy == 'Delete'){
      this.deleteFolderorFile(assetData,2, 1)
    } else if(this.dummy == 'Restore'){
      this.deleteFolderorFile(assetData,2, 0)
    }else if(this.dummy == 'deleteForever'){
      this.deleteFileFolderForever(assetData);
    }else if(this.dummy == 'shareAsset'){
      this.clickedAssetData = assetData;
      this.toggelShareAsset(true);
    }else if(this.dummy == 'moveAsset'){
      this.clickedAssetData = assetData;
      // this.getTree(null, true);
      this.getMoveAssetTree(null,true)
      this.toggelMoveAssets(true);
    }else if(this.dummy == 'approveAsset'){
      // this.clickedAssetData = assetData;
      // this.getTree(null, true);
      // this.toggelMoveAssets(true);
      this.gotocardAprrove(assetData);
    }
    else if(this.dummy == 'reviewAsset'){
      // this.clickedAssetData = assetData;
      // this.getTree(null, true);
      // this.toggelMoveAssets(true);
      this.gotoReviewAsset(assetData);
    }
    // var assetData = data
    // else if(this.dummy == 'Edit'){
    else {
      this.selectedEmployee=[]
      this.selectedEmployees=[]
      this.name  = assetData.assetName
      this.icon = assetData.formatIcon
    this.category = assetData.categoryId
    this.showBrowse = true;
    // this.selectedTags = [];
   this.getAssestDetail(assetData);

    //tags
    // if(assetData.tagIds){
    // var tagIds =assetData.tagIds.split(',');
    // if(tagIds.length > 0){
    //   this.tagList.forEach((tag) => {
    //     tagIds.forEach((tagId)=>{
    //       if (tag.id == tagId ) {
    //         this.selectedTags.push(tag);
    //         console.log(this.selectedTags,"if selectedTags")
    //       }
    //     });
    //   });
    //   }
    // }
      /// Approver
    // const selectedEmployees: any = [];
    // // console.log(this.allEmployeeAutoDrop,"edit approver")
    // if(this.allEmployeesAuto && this.allEmployeesAuto.length !=0){
    //   this.allEmployeesAuto.forEach(emp => {
    //     console.log(emp,"emp")
    //     console.log(emp.appId,"empId")
    //     console.log(assetData.reviewerId,"reviewerId")

    //     if (emp.appId == assetData.reviewerId) {
    //     // if (emp.appId == this.getDataForAddEdit[1].author) {

    //       selectedEmployees.push(emp);
    //     }
    //   });
    // }

    // console.log('selectedEmployees', selectedEmployees);
    ///Author
  //   if(assetData.author){
  //   var author = assetData.author.split(',');
  //   // var allEmployeeEdgeAutoDropObj ={}
  //   // console.log(this.allEmployeeEdgeAutoDrop,"data author")
  //   var selectedEdgeEmployee: any = [];
  //   if(this.allEmployeesAuto && this.allEmployeesAuto.length !=0){
  //   this.allEmployeeEdgeAuto.forEach((emp) => {
  //     author.forEach((auth) =>{
  //     if (emp.id == auth) {

  //       selectedEdgeEmployee.push(emp);
  //     }
  //   });
  //   });
  // }
  // }

  //time
  // if (assetData.estLength) {
  //   const resStr = assetData.estLength.split('.');
  //   var  formattedDuration = new Date();
  //   formattedDuration.setHours(resStr[0], resStr[1], resStr[2]);
  // }

    // this.addEditAssetForm = {
    //   assetId: assetData.assetId,
    //   assetName: assetData.assetName,
    //   description: assetData.description,
    //   category: assetData.categoryId,
    //   format: assetData.formatId,
    //   channel: assetData.channelId,
    //   language: assetData.languageId,
    //   estimateCost: assetData.cost,
    //   assetRef: assetData.assetRef,
    //   customTags: [],
    //   // selectedCreator: this.selectedEmployees,
    //   // selectedEmployee: this.selectedEdgeEmployee,
    //   preViewUrl: assetData.assetRef,
    //   appReq:assetData.appReq,
    //   time:formattedDuration,
    //   statusId: assetData.statusId,
    // }
      // this.selectedCreator= this.selectedEmployees,
      // this.selectedEmployee= this.selectedEdgeEmployee


    this.fileFolder = false
    this.showSave = false
    this.showFile = true
    this.view = true;

    this.detailSection()
    this.stopMeasure = true;

    this.activeTab = this.detailsTab[0];

    // console.log(this.dummy,"this.dummy");
    this.getAssetVersions(assetData.assetId);
    if(this.dummy === 'Edit'){
      this.statusId = assetData.statusId
      this.versionShow = assetData.isVersion
      this.showPreview = true;
      this.isLocalfile = false;
      if(assetData.assetRef){
        this.fileName = assetData.assetRef.substring(
          assetData.assetRef.lastIndexOf("/") + 1
        );
        // this.selectFileTitle = this.fileName;
      }

      if(assetData.formatId == 3){
        this.generatePdfLink(assetData.assetRef);
      }
      // this.assignValueToSelectDropDown(assetData.formatId);
      // this.showPreview = true;
      // this.External = true;
      // if(assetData.formatId == "8" || assetData.formatId == "6" || assetData.formatId == "4"){
      //   this.showPreview = true
      //   this.External = true
      // }
    }

    // if(this.dummy === 'Version'){
    //   this.isview=0
    //   this.fileName = 'Select a file';
    //   this.showPreview = false;
    //   this.isLocalfile = false;
    //   this.showBrowse = false;

      // this.addEditAssetForm = {
      //   assetId: assetData.assetId,
      //   assetName: assetData.assetName,
      //   description: assetData.description,
      //   category: assetData.categoryId,
      //   format: assetData.formatId,
      //   channel: assetData.channelId,
      //   language: assetData.languageId,
      //   estimateCost: assetData.cost,
      //   assetRef: '',
      //   customTags: [],
      //   // selectedCreator: this.selectedEmployees,
      //   // selectedEmployee: this.selectedEdgeEmployee,
      //   preViewUrl: '',
      //   appReq:assetData.appReq,
      //   time:formattedDuration,
      //   statusId: assetData.statusId,
      // }
      // this.assignValueToSelectDropDown(this.addEditAssetForm.format);
    // }
    }
  }

  downloadfolder(data) {
    this.fileFolder=false
  this.spinner.show();
  let param = {
    "categoryId":data.assetId,
    "categoryName":data.assetName,
    "parentCatId":this.parentCatId,
    "parentCatName":this.parentCatName
  }

  this.assetservice.downloadFolder(param)
    .then(rescompData => {
      console.log("downloaded sucessfully");
      this.spinner.hide();
      var result = rescompData;
      console.log('UpdateAssetResponse:', rescompData);
      if (result['type'] == true) {
        if(result['Link']){
        this.presentToast('success', result['message']);
        // this.download(result['Link'],'' ,result['Link'])
        let element = document.createElement('a');
        // var filename = result['Link'].replace(/^.*[\\\/]/, '')
         element.href = result['Link']
         element.download = 'download';
         element.style.display = 'none';
         document.body.appendChild(element);

         element.click();

         document.body.removeChild(element);
    }else{
          this.toastr.warning(result['message'],"Warning",);
        }
        // this.enableDisableCategoryModal = false;
      } else {
        this.toastr.warning(result['message'],"Warning",);
      }
    }, error => {
      this.spinner.hide();
      this.presentToast('error', '');
    });
  }

  View(item,isSpecial,i,menu){
    console.log(item,"cgffgxfgx")
    this.dummy = item;
    this.index=i;
    if(item==='Edit'){
      this.isview=1;
      this.vieweFileFolderField=false;
    }else{
    this.isview=0;
    }
    if(isSpecial == 'isSpecial'){
      this.special = true
    } else {
      this.special = false
    }

    // if(menu){
      if(menu && menu != 'notcall'){
      this.contextFileItem(menu);
    }

    // this.categoryFormValidation = f;
    // setTimeout( () => {
    //   if (this.categoryFormValidation) {
    //     this.categoryFormValidation.valueChanges.subscribe(selectedValue  => {
    //       this.assetFoFormChanges = selectedValue;
    //     })
    //   }
    // }, 2000)
    }

    onlyEdit(item,i){
      this.index=i
      this.statusId = item.statusId
      this.versionShow = item.isVersion
      if(this.isAdmin == 0){
        if(item.tabId == 2 || item.tabId == 3 || this.tabId == 5){
          this.presentToast('Warning','Sorry You are not allowed to edit this File')
        }else{
      this.dummy  = 'Edit'
      this.isview=1;
        var assetData = item
        this.name  = assetData.assetName
        this.icon = assetData.formatIcon
        this.category = assetData.categoryId
        this.getAssestDetail(assetData);

      //tags
      // if(assetData.tagIds){
      // var tagIds =assetData.tagIds.split(',');
      // if(tagIds.length > 0){
      //   this.tagList.forEach((tag) => {
      //     tagIds.forEach((tagId)=>{
      //       if (tag.id == tagId ) {
      //         this.selectedTags.push(tag);
      //         console.log(this.selectedTags,"if selectedTags")
      //       }
      //     });
      //   });
      //   }
      // }
        /// Approver
      // const selectedEmployees: any = [];
      // // console.log(this.allEmployeeAutoDrop,"edit approver")
      // this.allEmployeesAuto.forEach(emp => {
      //   console.log(emp,"emp")
      //   console.log(emp.appId,"empId")
      //   console.log(assetData.reviewerId,"reviewerId")

      //   if (emp.appId == assetData.reviewerId) {
      //   // if (emp.appId == this.getDataForAddEdit[1].author) {

      //     selectedEmployees.push(emp);
      //   }
      // });
      // console.log('selectedEmployees', selectedEmployees);
      ///Author
    //   if(assetData.author){
    //   var author = assetData.author.split(',');
    //   // var allEmployeeEdgeAutoDropObj ={}
    //   // console.log(this.allEmployeeEdgeAutoDrop,"data author")
    //   var selectedEdgeEmployee: any = [];
    //   this.allEmployeeEdgeAuto.forEach((emp) => {
    //     author.forEach((auth) =>{
    //     if (emp.id == auth) {

    //       selectedEdgeEmployee.push(emp);
    //     }
    //   });
    //   });
    // }

    //time
    // if (assetData.estLength) {
    //   const resStr = assetData.estLength.split('.');
    //   var  formattedDuration = new Date();
    //   formattedDuration.setHours(resStr[0], resStr[1], resStr[2]);
    // }
    //   this.addEditAssetForm = {
    //     assetId: assetData.assetId,
    //     assetName: assetData.assetName,
    //     description: assetData.description,
    //     category: assetData.categoryId,
    //     format: assetData.formatId,
    //     channel: assetData.channelId,
    //     language: assetData.languageId,
    //     estimateCost: assetData.cost,
    //     assetRef: assetData.assetRef,
    //     customTags: [],
    //     // selectedCreator: this.selectedEmployees,
    //     // selectedEmployee: this.selectedEdgeEmployee,
    //     preViewUrl: assetData.assetRef,
    //     appReq:assetData.appReq,
    //     time:formattedDuration,
    //     statusId: assetData.statusId,
    //   }
        // this.selectedCreator= this.selectedEmployees,
        // this.selectedEmployee= this.selectedEdgeEmployee


      this.fileFolder = false
      this.showSave = false
      this.showFile = true
      this.view = true;

      this.detailSection()
      this.stopMeasure = true;

      this.activeTab = this.detailsTab[0];

      // console.log(this.dummy,"this.dummy");
      this.getAssetVersions(assetData.assetId);
      if(this.dummy === 'Edit'){
        this.showPreview = true;
        this.isLocalfile = false;
        if(assetData.assetRef){
          this.fileName = assetData.assetRef.substring(
            assetData.assetRef.lastIndexOf("/") + 1
          );
          // this.selectFileTitle = this.fileName;
        }

        if(assetData.formatId == 3){
          this.generatePdfLink(assetData.assetRef);
        }
        if(assetData.formatId == "8" || assetData.formatId == "6" || assetData.formatId == "4"){
          this.showPreview = true
          this.External = true
        }
      }
    }
    }
    else if(this.isAdmin == 1){
      this.edit2(item,i)
    }
  }
  edit2(item,i){
    if((item.tabId == 3 && item.statusId == 1) || (item.tabId == 3 && item.statusId == 3) || this.tabId == 5){
      this.presentToast('Warning','Sorry You are not allowed to edit this File')
    }else{
    this.dummy  = 'Edit'
    this.isview=1;
    this.vieweFileFolderField=false
    var assetData = item
    this.name  = assetData.assetName
    this.icon = assetData.formatIcon
    this.category = assetData.categoryId
  //tags
  // if(assetData.tagIds){
  // var tagIds =assetData.tagIds.split(',');
  // if(tagIds.length > 0){
  //   this.tagList.forEach((tag) => {
  //     tagIds.forEach((tagId)=>{
  //       if (tag.id == tagId ) {
  //         this.selectedTags.push(tag);
  //         console.log(this.selectedTags,"if selectedTags")
  //       }
  //     });
  //   });
  //   }
  // }
    /// Approver
  // const selectedEmployees: any = [];
  // // console.log(this.allEmployeeAutoDrop,"edit approver")
  // this.allEmployeesAuto.forEach(emp => {
  //   console.log(emp,"emp")
  //   console.log(emp.appId,"empId")
  //   console.log(assetData.reviewerId,"reviewerId")

  //   if (emp.appId == assetData.reviewerId) {
  //   // if (emp.appId == this.getDataForAddEdit[1].author) {

  //     selectedEmployees.push(emp);
  //   }
  // });
// zX

//time
// if (assetData.estLength) {
//   const resStr = assetData.estLength.split('.');
//   var  formattedDuration = new Date();
//   formattedDuration.setHours(resStr[0], resStr[1], resStr[2]);
// }
this.getAssestDetail(assetData);
  this.fileFolder = false
  this.showSave = false
  this.showFile = true
  this.view = true;

  this.detailSection()
  this.stopMeasure = true;

  this.activeTab = this.detailsTab[0];

  // console.log(this.dummy,"this.dummy");
  this.getAssetVersions(assetData.assetId);
  if(this.dummy === 'Edit'){
    this.showPreview = true;
    this.isLocalfile = false;
    if(assetData.assetRef){
      this.fileName = assetData.assetRef.substring(
        assetData.assetRef.lastIndexOf("/") + 1
      );
      // this.selectFileTitle = this.fileName;
    }

    if(assetData.formatId == 3){
      this.generatePdfLink(assetData.assetRef);
    }
    if(assetData.formatId == "8" || assetData.formatId == "6" || assetData.formatId == "4"){
      this.showPreview = true
      this.External = true
    }
  }
}
  }
  getAssestDetail(data) {
    this.selectedTags=[];
    this.isEditFlag=false;
    if(this.dummy=="Edit"&&this.isEdit==0){
      this.isEditFlag=true;
    }
    this.spinner.show();
    let param = {
      'iId': data.assetId,
      'instanceName':data.assetName,
      'isView':this.isview,
    }
    this.assetservice.getAssetDetails(param)
      .then(rescompData => {
        this.spinner.hide();
        var  result = rescompData;
        var assetData=result['details']
        if(assetData.appReq==1){
          this.isShowApprover=true
          this.isApprover=1;
        }else{
          this.isShowApprover=false
          this.isApprover=0;
        }
        console.log('UpdateAssetResponse:', rescompData);
        if (result['type'] == true) {
          if (assetData.estLength) {
            const resStr = assetData.estLength.split('.');
            var  formattedDuration = new Date();
            formattedDuration.setHours(resStr[0], resStr[1], resStr[2]);
          }
        //tags
        if(result['tagIds'][0]['tagIds']){
        var tagIds =result['tagIds'][0]['tagIds'].split(',');
        if(tagIds.length > 0){
          this.tagList.forEach((tag) => {
            tagIds.forEach((tagId)=>{
              if (tag.id == tagId ) {
                this.selectedTags.push(tag);
                console.log(this.selectedTags,"if selectedTags")
              }
            });
          });
          }
        }
          /// Approver
        var selectedEmployees=[]
        if(this.allEmployeesAuto.length>0){
        this.allEmployeesAuto.forEach(emp => {
          console.log(emp,"emp")
          console.log(emp.appId,"empId");
          console.log(result['reviewer'],"reviewer")

          if (emp.appId == result['reviewer']) {
            selectedEmployees.push(emp);
          }
        });
      }
        console.log('selectedEmployees', selectedEmployees);
        ///Author
        var selectedEdgeEmployee=[];
        if(this.allEmployeeEdgeAuto.length>0){
        if(result['author']){
        var author = result['author'].split(',');
      //
      this.updateDropDownSetting(result['author']);
        this.allEmployeeEdgeAuto.forEach((emp) => {
          author.forEach((auth) =>{
          if (emp.id == auth) {
             selectedEdgeEmployee.push(emp);
          }
        });
        });
      }
    }
    if(this.dummy === 'Version'){
      this.isview=0
      this.fileName = 'Select a file';
      this.showPreview = false;
      this.isLocalfile = false;
      this.showBrowse = false;
      this.categoryName = assetData.categoryName

      this.addEditAssetForm = {
        assetId: data.assetId,
        assetName: data.assetName,
        description: data.description,
        category: assetData.categoryId,
        format: assetData.formatId,
        channel: assetData.channelId,
        language: assetData.languageId,
        estimateCost: assetData.cost,
        assetRef: '',
        customTags: [],
        selectedCreator: selectedEmployees,
        selectedEmployee: selectedEdgeEmployee,
        preViewUrl: '',
        appReq:assetData.appReq,
        time:formattedDuration,
        statusId: data.statusId,
      }
      this.assignValueToSelectDropDown(this.addEditAssetForm.format);
    }
    if(this.dummy=='Edit'){
      this.showBrowse=true;
      this.addAction=false;
      this.categoryName = assetData.categoryName
      this.addEditAssetForm = {
        assetId: data.assetId,
        assetName: data.assetName,
        description: data.description,
        category: assetData.categoryId,
        format: assetData.formatId,
        channel: assetData.channelId,
        language: assetData.languageId,
        estimateCost: assetData.cost,
        assetRef: assetData.assetRef,
        customTags: this.selectedTags,
        // approverId:this.selectedCreator[0].appId,
        // selectedCreator: this.selectedCreator,
        selectedCreator: selectedEmployees,
        selectedEmployee: selectedEdgeEmployee,
        preViewUrl: assetData.assetRef,
        appReq:assetData.appReq,
        time: formattedDuration,
        statusId: data.statusId,
      }
      this.assignValueToSelectDropDown(assetData.formatId);
    }
    this.selectedCreator= selectedEmployees;
    this.selectedEmployee= selectedEdgeEmployee;
    this.addEditAssetFormChangeStore = _.cloneDeep(this.addEditAssetForm);
        } else {
          this.presentToast('error', '');
        }
      }, error => {
        this.spinner.hide();
        this.presentToast('error', '');
      });
  }
    download(text,event,data) {
      var element = document.createElement('a');
     var filename = text.replace(/^.*[\\\/]/, '')
      element.href = text.replaceAll(" ", "%20");
      element.download = 'download';
      element.style.display = 'none';
      element.setAttribute("target", "_blank")
      document.body.appendChild(element);

      element.click();

      document.body.removeChild(element);
      if(event){
      event.preventDefault()
      }
    // this.spinner.show();
    let param = {
      // 'fileType': type,
      'instanceId': data.assetId,
      'instanceName':filename,
      'parentCatId' : this.parentCatId,
      'parentCatName' :this.parentCatName

    }

    this.assetservice.downloadFile(param)
      .then(rescompData => {
        console.log("downloaded sucessfully");
        // this.spinner.hide();
      //   var result = rescompData;
      //   console.log('UpdateAssetResponse:', rescompData);
      //   if (result['type'] == true) {
      //     // this.presentToast('success', result['data']);
      //     // this.enableDisableCategoryModal = false;
      //   } else {
      //     // this.presentToast('error', '');
      //   }
      // }, error => {
      //   this.spinner.hide();
      //   // this.presentToast('error', '');
      });


    }
    save(filename, data) {
      // var blob = new Blob([data], {type: 'text/csv'});
      // if(window.navigator.msSaveOrOpenBlob) {
      //     window.navigator.msSaveBlob(blob, filename);
      // }
      // else{
      //     var elem = window.document.createElement('a');
      //     elem.href = window.URL.createObjectURL(blob);
      //     elem.download = filename;
      //     document.body.appendChild(elem);
      //     elem.click();
      //     document.body.removeChild(elem);
      // }
      var blob = new Blob([filename], {
        type: "text/plain;charset=utf-8"
       });
  }

  // onSubmit(){

  // }

  deleteFolderorFile(data,type,action){
    // var msg;
    // if(action === 1){
    //   if(type == 1){
    //     msg = 'Folder Deleted Successfully'
    //   }
    //   else{
    //     msg = 'File Deleted Successfully'
    //   }
    // }else {
    //   if(type == 1){
    //     msg = 'Folder Deleted Successfully'
    //   }
    //   else{
    //     msg = 'File Deleted Successfully'
    //   }
    // }

    this.spinner.show();
    let param = {
      'fileType': type,
      'instanceId': data.assetId,
      'instanceName':data.assetName,
      'actionType': action,
      'parentCatId' : action==1?this.parentCatId:null,
      'parentCatName' :action==1?this.parentCatName:null
      // 'tId': this.tenantId

    }

    this.assetservice.deleteFolderFile(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('UpdateAssetResponse:', rescompData);
        if (result['type'] == true) {
          this.presentToast(result['action'], result['data']);
          // this.enableDisableCategoryModal = false;
          this.getFileAndFolders(this.parentCatId,this.roleId,this.tabId,this.innerTabId);
          const param= {
            tabId:this.tabId,
            roleId:this.roleId,

          }
          this.view=false;
          this.getTree(param, false);
        } else {
          this.presentToast('error', '');
        }
      }, error => {
        this.spinner.hide();
        this.presentToast('error', '');
      });
  }

  changevisiblestatus(data) {
    var visible
    var msg
    if(this.dummy == 'Enable'){
     visible = 1
     msg = "File Enabled Successfully"
    }else{
      visible = 0
      msg = "File Disabled Successfully"
    }
    this.spinner.show();
    let param = {
      'assetsId': data.assetId,
      'assetsVisible': visible,
      'tId': this.tenantId,
      'instanceName':data.assetName,
      'parentCatId':this.parentCatId,
      'parentCatName':this.parentCatName,
    }

    this.assetservice.changeAssetStatus(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('UpdateAssetResponse:', rescompData);
        if (result['type'] == true) {
          this.presentToast('success', msg);
          // this.enableDisableCategoryModal = false;
          this.getFileAndFolders(this.parentCatId,this.roleId,this.tabId,this.innerTabId);
        } else {
          this.presentToast('error', '');
        }
      }, error => {
        this.spinner.hide();
        this.presentToast('error', '');
      });
  }
  getAssestData() {
    // this.spinner.show();
    let url = webApi.domain + webApi.url.getAllTagsComan;
    let param = {
      tenantId :this.tenantId
    };
    this.webApiService.getService(url, param)
      .then(rescompData => {
        // this.loader =false;
        // this.spinner.hide();
        var assetData: any = rescompData;
      // this.addEditAssetForm={
      //   assetId: assetData.assetId,
      //   assetName: assetData.assetName,
      //   description: assetData.description,
      //   category: assetData.categoryId,
      //   format: assetData.formatId,
      //   channel: assetData.channelId,
      //   language: assetData.languageId,
      //   estimateCost: assetData.cost,
      //   assetRef: '',
      //   customTags: [],
      //   selectedCreator: selectedEmployees,
      //   selectedEmployee: selectedEdgeEmployee,
      //   preViewUrl: '',
      //   appReq:assetData.appReq,
      //   time:formattedDuration,
      //   statusId: assetData.statusId,
      // }
      },
        resUserError => {
          // this.loader =false;
          this.show =false;
          // this.spinner.hide();
          // this.errorMsg = resUserError;
        });
  }
  getallTagList() {
    // this.spinner.show();
    let url = webApi.domain + webApi.url.getAllTagsComan;
    let param = {
      tenantId :this.tenantId
    };
    this.webApiService.getService(url, param)
      .then(rescompData => {
        // this.loader =false;
        // this.spinner.hide();
        var temp: any = rescompData;
        this.tagList = temp.data;
        this.bindfilter(this.tagList)
        console.log(this.tagList,"taglist")
        // this.tempTags = [... this.tagList];
        // console.log(this.tempTags,"this.temptags")
        // console.log('Visibility Dropdown',this.visibility);

        // this.getDataForAddEdit = this.addassetservice.addEditAssetData;
      //   if(this.getDataForAddEdit[0] === 'EDIT'){
      //   if(this.getDataForAddEdit[1].tagIds)
      //   {
      //     var tagIds =this.getDataForAddEdit[1].tagIds.split(',');
      //     if(tagIds.length > 0){
      //       this.tempTags.forEach((tag) => {
      //         tagIds.forEach((tagId)=>{
      //           if (tag.id == tagId ) {
      //             this.selectedTags.push(tag);
      //             console.log(this.selectedTags,"if selectedTags")
      //           }
      //         });
      //       });
      //       }
      //   }
      // }

      },
        resUserError => {
          // this.loader =false;
          this.show =false;
          // this.spinner.hide();
          // this.errorMsg = resUserError;
        });
  }
  addeditAsset(item){
    this.addAction = true;
    this.showBrowse=true;
    this.isEditFlag=true;
    this.isApprover = 0;
    this.category = this.catCpy
    this.categoryName = this.catNameCpy;
    this.setEmployeeSettingToDefault();
    this.addEditAssetForm = {
      assetId: 0,
      assetName: '',
      description: '',
      category: '',
      format: '',
      channel: '',
      language: '',
      estimateCost: '',
      assetRef: '',
      customTags: [],
      selectedCreator: [],
      selectedEmployee: [],
      preViewUrl:'',
      appReq: null,
      statusId: null,
      categoryName: null
    };
    this.addEditAssetFormChangeStore = _.cloneDeep(this.addEditAssetForm);
    this.selectedCreator = []
    this.icon = 'fa fa-file'
    this.selectedTags = [];
    this.fileFolder = false
    this.view = true
    this.vieweFileFolderField=false;
    this.isShowApprover=false
    this.showFile = true
    this.showSave = false
    this.showPreview =false
    this.assetFileData = null;
    this.fileName = 'Select a file';
    this.dummy = 'addFile';
    this.name ='';
    this.special = false;
    this.selectedAssetFormatData ={
      filterId: '',
      filterValue: '',
      formatId: '',
      formatMode: '',
      formatName: '',
      isSelected: '',
      pickerType: null,
      previewEnable: null,
      tagId: '',
      tagName: '',
    } ;
  }
  gotoFilter() {
    this.filter = !this.filter;
  }
  getAllEmployeesDAM() {
    // this.spinner.show();
    let param = {
      tId: this.tenantId
    };
    const _urlGetAllEmployees: string = webApi.domain + webApi.url.getAllEmployeeDAM;
    this.commonFunctionService.httpPostRequest(_urlGetAllEmployees,param)
    // this.addassetservice.getAllEmployee(param)
    .then(
      rescompData => {
        // this.spinner.hide();
        var result = rescompData;
        console.log('getAllEmployessDAM:', rescompData);
        if (result['type'] == true) {
          if (result['data'][0].length == 0) {
            // this.noEmployees = true;
          } else {
            this.allEmployeesAuto = result['data'][0];
            // console.log('this.allEmployeesAuto', this.allEmployeesAuto);
            // this.prepareApproverDropdown();
            // this.getAllEdgeEmployee('');
            // this.prepareDataForAddEditAsset();
          }
        } else {
          // var toast: Toast = {
          //   type: 'error',
          //   //title: 'Server Error!',
          //   body: 'Something went wrong.please try again later.',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }
      },
      error => {
        // this.spinner.hide();
        // var toast: Toast = {
        //   type: 'error',
        //   //title: 'Server Error!',
        //   body: 'Something went wrong.please try again later.',
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      }
    );
  }
  getAllEdgeEmployee(str) {
    // if (str.length > 3) {
      // this.spinner.show();
      const param = {
        srcStr: str,
        tId: this.tenantId
      };
     const _urlGetAllEdgeEmployee: string = webApi.domain + webApi.url.getAllEdgeEmployee;
     this.commonFunctionService.httpPostRequest(_urlGetAllEdgeEmployee,param)
      // this.addassetservice.getAllEdgeEmployee(param)
      .then(
        rescompData => {
          // this.spinner.hide();
          var result = rescompData;
          console.log('getAllEdgeEmployessDAM:', rescompData);
          if (result['type'] == true) {
            if (result['data'][0].length == 0) {
              // this.noEdgeEmployees = true;
            } else {
              this.allEmployeeEdgeAuto = result['data'][0];
              console.log('this.allEmployeeEdgeAuto', this.allEmployeeEdgeAuto);
              // this.prepareEmployeeDropdown();
              // this.prepareDataForAddEditAsset();
            }
          } else {
            // var toast: Toast = {
            //   type: 'error',
            //   //title: 'Server Error!',
            //   body: 'Something went wrong.please try again later.',
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(toast);
          this.presentToast('error', '');
          }
        },
        error => {
          // this.spinner.hide();
          // var toast: Toast = {
          //   type: 'error',
          //   //title: 'Server Error!',
          //   body: 'Something went wrong.please try again later.',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }
      );
    // }
  }
  getAllAssetDropdown() {
    const param = {
      tId: this.tenantId,
    };
    const _urlGetAllDropdown: string = webApi.domain + webApi.url.getAllAssetDropDown;
    this.commonFunctionService.httpPostRequest(_urlGetAllDropdown,param)
    // this.addassetservice.getAllAssetDropdown(param)
    .then(
      rescompData => {
        var result = rescompData;
        console.log('All Asset Dropdown1', result);
        if (result['type'] == true) {
          console.log('All Asset Dropdown2', result);
          this.status = result['data'][0];
          if(result['data'][1]){
          this.languages = result['data'][1];
          }
          this.formats = result['data'][2];
          console.log(this.formats,"this.formats")
          this.channels = result['data'][3];
          this.categories = result['data'][4];
          this.bindfilter(this.languages);
          this.bindfilter(this.channels);
          this.bindfilter(this.status);
          // this.bindfilter(this.channels);
          if (this.categories.length === 0) {
            // this.noCategory = true;
          }
        } else {
          // var toast: Toast = {
          //   type: 'error',
          //   //title: 'Server Error!',
          //   body: 'Something went wrong.please try again later.',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }
      },
      resUserError => {
        // this.loader =false;
        // var toast: Toast = {
        //   type: 'error',
        //   //title: 'Server Error!',
        //   body: 'Something went wrong.please try again later.',
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      }
    );
  }
  check(event){
    console.log(event.target.checked)
    if(event.target.checked === true){
    this.isShowApprover = true
    this.isApprover = 1
    }
    else{
      this.isShowApprover = false
      this.isApprover = 0
    }
  }
  // onEmployeeSelect(item: any) {
  //   console.log(item);
  //   if(item.edgeempId == 0){
  //     this.settingsEmployeeDrop = {
  //       text: 'Search & Select Author',
  //       singleSelection: false,
  //       classes: 'myclass custom-class',
  //       primaryKey: 'edgeempId',
  //       labelKey: 'edgeempName',
  //       noDataLabel: 'Search Author...',
  //       enableSearchFilter: true,
  //       limitSelection:1,
  //       searchBy: ['edgeempId', 'edgeempName'],
  //       maxHeight:250,
  //       lazyLoading: true,
  //     };
  //     var dummy = []
  //     dummy.push(item)
  //     this.addEditAssetForm.selectedEmployee = dummy
  //   }
  //   console.log(this.selectedEmployee);
  // }

  // OnEmployeeDeSelect(item: any) {
  //   console.log(item);
  //   if(item.edgeempId == 0){
  //     this.settingsEmployeeDrop = {
  //       text: 'Search & Select Author',
  //       singleSelection: false,
  //       classes: 'myclass custom-class',
  //       primaryKey: 'edgeempId',
  //       labelKey: 'edgeempName',
  //       noDataLabel: 'Search Author...',
  //       enableSearchFilter: true,
  //       limitSelection:this.allEmployeeEdgeAutoDrop.length,
  //       searchBy: ['edgeempId', 'edgeempName'],
  //       maxHeight:250,
  //       lazyLoading: true,
  //     };
  //   }
  //   console.log(this.selectedEmployee);
  // }

  singleClick(item) {
    this.activeFolder = !this.activeFolder;
    // this.previewPopup = true
  }

  doubleClick() {
    this.disable = !this.disable;
  }

  closeDetail() {
    this.view = false;
    this.assetFileData = null;
    this.showBrowse = false;
    this.isApprover = 0;
    this.vieweFileFolderField=false;
    this.deletePdf();
  }

  activeTab = this.detailsTab[0];
  tabActive(tab) {
    // console.log(tab);
    if(tab){
      this.activeTab = tab;
      // for (let i = 0; i < this.detailsTab.length;) {
      //  if(this.detailsTab.id === tab.id){
      //   this.detailsTab[i]['active'] = true;
      //  }else {
      //   this.detailsTab[i]['active'] = false;
      //  }
      // }
    }

    // if (tab == content) {
    // this.activeTab = true;
    // }
  }

  mouseEnter() {
    this.show = false;
  }
  compareChangedData(x, y, customObj) {
    var customFlag = false;
    if (customObj) {
      customFlag = true;
    }
  var output = {};
  // if(x.selectedEmployee&&y.selectedEmployee){
  // x.Author=x.selectedEmployee;
  // y.Author=y.selectedEmployee;
  // delete x.selectedEmployee;
  // delete y.selectedEmployee;
  // }
  // if(x.assetName&&y.assetName){
  //   x.Name=x.assetName;
  //   y.Name=y.assetName;
  //   delete x.assetName;
  //   delete y.assetName;
  // }
    if(x && y){
      if (JSON.stringify(x) !== JSON.stringify(y)) {
        for(const a in x) {
          if (JSON.stringify(x[a]) !== JSON.stringify(y[a])) {
            let objname = a;
            if (customFlag) {
              objname = customObj[a] == undefined ? a : customObj[a];
            }
            output[objname] = {
              current: x[a],
              previous: y[a],
              status: true,
            };
          }
        }
      }
    }
    return output;
  }
  folderEntity:any={
    categoryPicRef:"Folder Image",
    name:'Folder Name',
    desc:"Description",
    selectedEmployee:'Author'

  }
  submit(data) {
    // this.isSave = true
    // this.submitted = true;

    if (this.categoryFormValidation.valid) {
      if(this.folder.length>0&&this.addAction){
        for(var x in this.folder){
          if(this.folder[x].assetName.toLowerCase()==data.name.toLowerCase()){
            this.presentToast('warning',"Folder name already exist");
            return
          }
        }
      }
      if(this.folder.length>0&&!this.addAction){
      for(var x in this.folder){
        if(this.folder[x].assetName.toLowerCase()==data.name.toLowerCase()&&this.index.toString()!=x){
          this.presentToast('warning',"Folder name already exist");
          return
        }
      }
    }
    const changeproperty = this.compareChangedData(data, this.categoryFormChangeStore,this.folderEntity); // current and previous
      if (!data) {
        // return;
        console.log(data);
      }
      else {
        console.log('data', data);

        // this.addEditCategoryModal = false;
        // this.spinner.show();
        if(this.defaultValFlag == 1){
          if(data.selectedEmployee && Array.isArray(data.selectedEmployee) && data.selectedEmployee.length !=0){
            this.makeAuthorDataReady(data.selectedEmployee);
          }


          // else {

          // }
        }

          // if(!this.author){
          //   this.author=null;
          // }
        let param = {
          "parCatId":this.parentCatId,
          "catId": this.addAction ? 0 : data.id,
          // "catId": data.id?data.id:0,
          "catCode": data.code,
          "catName": data.name,
          "catDesc": data.desc?data.desc:null,
          "catPicRef": data.categoryPicRef,
          "tId": this.tenantId,
          "userId": this.tenantId,
          "parCatName": this.parentCatName,
          "authStr":this.defaultValFlag == 0 ? null: this.author,
          "changeproperty" : changeproperty,
        }
        console.log('param', param);
        this.passParams = param;

        var fd = new FormData();
        fd.append('content', JSON.stringify(this.passParams));
        fd.append('file', this.assetCatImgData);
        console.log('File Data ', fd);

        console.log('Asset Category Img', this.assetCatImgData);
        console.log('Asset Data ', this.passParams);
        let fileUploadUrl = webApi.domain + webApi.url.fileUpload;

        if (this.assetCatImgData) {
          this.webApiService.getService(fileUploadUrl, fd)
            .then(rescompData => {
              // this.isSave = false
              var temp: any = rescompData;
              this.fileUploadRes = temp;
              console.log('rescompData', this.fileUploadRes)
              this.assetCatImgData = null;
              if (temp == "err") {
                // this.notFound = true;
                // var thumbUpload: Toast = {
                //   type: 'error',
                //   title: "Asset Category Image",
                //   body: "Unable to upload Asset Category Image.",
                //   // body: temp.msg,
                //   showCloseButton: true,
                //   timeout: 2000
                // };
                // this.toasterService.pop(thumbUpload);
                this.presentToast('error', '');
                // this.spinner.hide();
              } else if (temp.type == false) {
                // var thumbUpload: Toast = {
                //   type: 'error',
                //   title: "Asset Category Image",
                //   body: "Unable to upload Asset Category Image.",
                //   // body: temp.msg,
                //   showCloseButton: true,
                //   timeout: 2000
                // };
                // this.toasterService.pop(thumbUpload);
                this.presentToast('error', '');
                // this.spinner.hide();
              }
              else {
                if (this.fileUploadRes.data != null || this.fileUploadRes.fileError != true) {
                  this.passParams.catPicRef = this.fileUploadRes.data.file_url;
                  console.log('this.passparams.catPicRef', this.passParams.catPicRef);
                  // this.isSave= false
                  // this.spinner.hide();
                  this.addEditAssetCategory(this.passParams);
                } else {
                  // var thumbUpload: Toast = {
                  //   type: 'error',
                  //   title: "Asset Category Image",
                  //   // body: "Unable to upload course thumbnail.",
                  //   body: this.fileUploadRes.status,
                  //   showCloseButton: true,
                  //   timeout: 2000
                  // };
                  // this.toasterService.pop(thumbUpload);
                  this.presentToast('error', '');
                  // this.spinner.hide();
                }
              }
              console.log('File Upload Result', this.fileUploadRes);
              var res = this.fileUploadRes;
              this.fileres = res.data.file_url;
            },
              resUserError => {
                // this.errorMsg = resUserError;
                // console.log('File upload this.errorMsg', this.errorMsg);
                // this.spinner.hide();
              });
        } else {
          // this.spinner.hide();
          this.addEditAssetCategory(this.passParams);
        }
      }
    } else {
      console.log('Please Fill all fields');
      // this.isSave = false

      // const detForm: Toast = {
      //   type: 'error',
      //   title: 'Unable to update',
      //   body: 'Please Fill all fields',
      //   showCloseButton: true,
      //   timeout: 2000
      // };
      // this.toasterService.pop(detForm);
      this.presentToast('warning', 'Please fill in the required fields');
      Object.keys(this.categoryFormValidation.controls).forEach(key => {
        this.categoryFormValidation.controls[key].markAsDirty();
      });
    }

  }
  changeCategoryStatus(data) {
    var visible
    var msg
    if(data.visible == 1){
      data.visible = 0
     msg = "Folder Disabled Successfully"

    }else if (data.visible == 0){
      data.visible =1
     msg = "Folder Enabled Successfully"

    }
    this.spinner.show()
    // let param = {
    //   "catId": catId,
    //   "catVisible": catVisible,
    //   "tId": this.tenantId
    // }
    let param = {
      "catId": data.assetId,
      "catVisible": data.visible,
      "tId": this.tenantId,
      "categoryName": data.assetName,
      "parentCatName": this.parentCatName,
      "parentCatId": this.parentCatId,
    }
    const _urlChangeAssetCateorystatus:string = webApi.domain + webApi.url.changeAssetCategoryStatus;
    this.commonFunctionService.httpPostRequest(_urlChangeAssetCateorystatus,param)
    //this.assetcategoryservice.changeAssetCategoryStatus(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('UpdateCategoryStatusResponse:', rescompData);
        if (result['type'] == true) {
          // var catUpdate: Toast = {
          //   type: 'success',
          //   title: "Asset category Updated!",
          //   body: "Asset updated successfully.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('success', msg);
          this.getFileAndFolders(this.parentCatId,this.roleId,this.tabId,this.innerTabId);
          // this.enableDisableCategoryModal = false;
        } else {
          // var catUpdate: Toast = {
          //   type: 'error',
          //   title: "Asset category Updated!",
          //   body: "Unable to update Asset Category.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
        }
      }, error => {
        this.spinner.hide();
        // var toast: Toast = {
        //   type: 'error',
        //   //title: "Server Error!",
        //   body: "Something went wrong.please try again later.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
  }



  addEditAssetCategory(params) {
    console.log('paramNew', params);
    // this.spinner.show();
    const _urlAddEditAssetCategories:string = webApi.domain + webApi.url.addEditAssetCategory;
    this.commonFunctionService.httpPostRequest(_urlAddEditAssetCategories,params)
    //this.assetcategoryservice.addEditAssetCategories(params)
      .then(rescompData => {
        // this.isSave = false

        // this.spinner.hide();

        var temp = rescompData;
        var result = temp['data'];
        if (params.catId == 0) {
          if (temp['type'] == true) {
            // this.fetchBadgeCateory();
            console.log('Add Result ', result)
            // var catUpdate: Toast = {
            //   type: 'success',
            //   title: "Asset Category Inserted!",
            //   body: "New Asset Category added successfully.",
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(catUpdate);
            this.fileFolder = false
            this.closeDetail()
            this.presentToast('success', 'New folder added');
            this.getFileAndFolders(this.parentCatId,this.roleId,this.tabId,this.innerTabId);
            const param= {
              tabId:this.tabId,
              roleId:this.roleId,

            }
            this.getTree(param,false);
            // this.valueChanged = this.folder[0].assetId
            // console.log(this.folder[0].assetId,this.valueChanged,"gcgcfcfffxxdz")
          } else {
            //this.fetchBadgeCateory();
            // var catUpdate: Toast = {
            //   type: 'error',
            //   title: "Asset Category Inserted!",
            //   body: "Unable to add Asset Category.",
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(catUpdate);
            this.presentToast('error', '');
          }
        } else {
          console.log('Asset Category Edit Result ', result)
          if (temp['type'] == true) {
            // this.fetchBadgeCateory();
            // var catUpdate: Toast = {
            //   type: 'success',
            //   title: "Asset Category Updated!",
            //   body: "Asset category updated successfully.",
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(catUpdate);
            this.fileFolder = false
            this.closeDetail()
            this.presentToast('success', 'Folder updated');
            const param= {
              tabId:this.tabId,
              roleId:this.roleId,

            }
            this.getTree(param, false);
            this.getFileAndFolders(this.parentCatId,this.roleId,this.tabId,this.innerTabId);
          } else {
            //this.fetchBadgeCateory();
            // var catUpdate: Toast = {
            //   type: 'error',
            //   title: "Asset Category updated!",
            //   body: "Unable to update Asset category.",
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(catUpdate);
            this.presentToast('error', '');
          }
        }

      },
        resUserError => {
          // this.spinner.hide();
          // this.errorMsg = resUserError;
        });
  }

getActive(){
  for(let i=0;i<this.tabs.length;i++){
    // if(this.tabs[i]['isActive'] != 1){
    this.tabs[i]['isActive'] = 0
    // if(this.tabs[i]['expand']){
    // this.tabs[i]['expand'] = false
    // }
  }

}
getExpand(){
  for(let i=0;i<this.tabs.length;i++){
    // if(this.tabs[i]['isActive'] != 1){
    // this.tabs[i]['isActive'] = 0
    if(this.tabs[i]['expand'] && this.tabs[i]['isActive'] == 0){
    this.tabs[i]['expand'] = false
    }
  }
}

allFolderCombined = [];
getAllFolders(item){
  this.breadcrumbArray =[{
    'name': 'DAM',
    'navigationPath': '/pages/dam',
  },
  // {
  //   'name': 'Folder ',
  //   'navigationPath': '/pages/dam/folders',
  //   'id':{},
  //   'sameComp':true,
  //   'assetId':null
  // }
]
if(this.isAprrove==1){
  this.previousBreadCrumb =[{
    'name': 'DAM',
    'navigationPath': '/pages/dam',
  }]
}else{
  this.previousBreadCrumb =[{
    'name': 'DAM',
    'navigationPath': '/pages/dam',
  },
  {
    'name': 'Folder',
    'navigationPath': '/pages/dam/folders',
    'id':{},
    'sameComp':true,
    'assetId':null
  }
]
}
  this.fileFolder = false
  this.showFile = false
  this.view = false;
  this.spinner.show()
  // this.cdf.detectChanges()
  // this.showSpinner = true
  this.getActive()
  item['isActive'] = 1
  //new code
  // this.getExpand()
  // end new code
  this.assetservice.tabActive  = item['isActive']
  this.selectedTab = item
  console.log(item,"ggghg")
  this.valueChanged = null
  this.selectedFolder.assetId = null
  this.roleId = item.roleId
  this.tabId = item.tabId
  let param = {
    // tenantId: this.tenantId,
    parentCatId: null,
    roleId : item.roleId,
    tabId:item.tabId,
    innerTabId:null
  }
  // this.parentCatId = null
  // this.innerTabId = null;
  // this.filter = false;
  if(item.tabId !=6){
    this.tabId = item.tabId;
    this.parentCatId = null
    this.innerTabId = null;
    this.filter = false;
  }
  if(this.tabId == 1){
  this.header = {
    title: 'Folder',
    // title:this.title,
    btnsSearch: true,
    searchBar: true,
    placeHolder:'Search by keywords',
    dropdownlabel: 'Folder ',
    drplabelshow: true,
    drpName1: 'Upload Folder',
    drpName2: 'Add Folder ',
    drpName3: '',
    folder:true,
    drpName1show: true,
    drpName2show: true,
    drpName3show: false,
    dropdownlabel1:'Add',
    drplabel1show:false,
    drpName_1:'Add Folder',
    drpName_2: ' ',
    drpName_1show: true,
    drpName_2show: false,
    btnName1: '',
    btnName2: 'Add Folder',
    btnName3: 'Upload Folder',
    // btnAdd: 'Add Asset',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: false,
    filter: false,
    showBreadcrumb:true,
    breadCrumbList:[
      // {
      //   'name': 'DAM',
      //   'navigationPath': '/pages/dam',
      // },
    ]
  };
  this.header.breadCrumbList = this.breadcrumbArray
}
else if(this.tabId == 2){
  this.header = {
    title: 'Folder',
    // title:this.title,
    btnsSearch: true,
    searchBar: true,
    placeHolder:'Search by keywords',
    dropdownlabel: ' ',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: 'Add Folder',
    // btnName3: 'File Upload',
    // btnAdd: 'Add Asset',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: false,
    filter: false,
    showBreadcrumb:true,
    breadCrumbList:[
      // {
      //   'name': 'DAM',
      //   'navigationPath': '/pages/dam',
      // },
    ]
  };
  this.header.breadCrumbList = this.breadcrumbArray

}
else if(this.tabId == 3){
  this.header = {
    title: 'Folder',
    // title: this.title,
    btnsSearch: true,
    searchBar: true,
    placeHolder:'Search by keywords',
    dropdownlabel: ' ',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: 'Add Folder',
    // btnName3: 'File Upload',
    // btnAdd: 'Add Asset',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: false,
    filter: false,
    showBreadcrumb:true,
    breadCrumbList:[
      // {
      //   'name': 'DAM',
      //   'navigationPath': '/pages/dam',
      // },
    ]


  };
  this.header.breadCrumbList = this.breadcrumbArray

}else if(this.tabId == 5){
  this.header = {
    title: 'Folder',
    // title: this.title,
    btnsSearch: true,
    searchBar: true,
    placeHolder:'Search by keywords',
    dropdownlabel: ' ',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: 'Add Folder',
    // btnName3: 'File Upload',
    // btnAdd: 'Add Asset',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: false,
    filter: false,
    showBreadcrumb:true,
    breadCrumbList:[
      // {
      //   'name': 'DAM',
      //   'navigationPath': '/pages/dam',
      // },
    ]


  };
  this.header.breadCrumbList = this.breadcrumbArray

}

else if(item.tabId == 6){
   this.gotoApproveAsset();
}else {
  this.header = {
    title: 'Folder ',
    // title:this.title,
    btnsSearch: true,
    searchBar: true,
    placeHolder:'Search by keywords',
    dropdownlabel: 'Folder ',
    drplabelshow: true,
    drpName1: 'Upload Folder',
    drpName2: 'Add Folder ',
    drpName3: '',
    drpName1show: true,
    drpName2show: true,
    drpName3show: false,
    folder:true,
    dropdownlabel1:'File',
    drplabel1show:false,
    drpName_1:'Add Folder',
    drpName_2: ' ',
    drpName_1show: true,
    drpName_2show: false,
    btnName1: '',
    btnName2: 'Add Folder',
    btnName3: 'Upload Folder',
    // btnAdd: 'Add Asset',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: false,
    filter: false,
    showBreadcrumb:true,
    breadCrumbList:[
      // {
      //   'name': 'DAM',
      //   'navigationPath': '/pages/dam',
      // },
    ]
  };
  this.header.breadCrumbList = this.breadcrumbArray
  if(this.primaryButtonFlag == 0){
    this.header.drplabelshow = false
  }
  else{
    this.header.drplabelshow = true
  }

}
  this.assetservice.getAllAssetCategories(param)
  .then(rescompData => {
  this.spinner.hide()
  // this.showSpinner = false
    const result = rescompData;
    this.folder = result['data'][1];
    this.tempFolders = _.cloneDeep(this.folder);
    this.tempSearchFolders = _.cloneDeep(this.folder);
    this.files = result['data'][2];
    if(result['data'][1].length==0 && result['data'][2].length==0){
      this.nodata=true
    }else{
      this.nodata = false
    }
    this.tempFiles = _.cloneDeep(this.files);
    this.tempSearchFiles = _.cloneDeep(this.files);
    this.allFolderCombined = this.tempFolders.concat(this.files);
  });
}
getFolders(item,identity,tabItem){

  this.fileFolder = false
  this.showFile = false
  this.view = false;
  if(tabItem){
    this.selectedTab = tabItem
  }
  if(this.selectedTab){
    this.getActive()
    this.selectedTab['isActive'] = 1
    // if(this.selectedTab['tabId'] == 5)
    // // this.getExpand()
    this.tabId = this.selectedTab['tabId']
    this.roleId = this.selectedTab['roleId']
  }
  if(this.tabId==5){
    this.toastr.warning("Sorry, You are not allowed to edit this Folder","Warning")
    return;
  }
  // if(tabItem){
  //   this.tabId = tabItem['tabId']
  //   this.roleId = tabItem['roleId']
  //  }


  this.spinner.show()
  // if(item.tabId != 5) {
  // this.showSpinner = true
  this.filterClearData = true;
  this.filter = false;
  if(identity == 'tree'){
  var id = item.id
  var name = item.categoryName
  this.valueChanged = id
  this.selectedFolder.assetId = id
  }else{
  var id = item.assetId
  var name = item.assetName
  }
  this.category = id
  this.categoryName = name
  this.catCpy=id
  this.catNameCpy=name
  if(identity != 'tree'){
    this.title = name
    console.log('assetData', item);
    //// breadCrumb
    this.breadObj = {
    'name': name,
    // 'navigationPath': '',
    'id':item,
    'navigationPath': '/pages/dam/folders',
    'sameComp':true,
    'assetId' : id
    }
    // var array ={}
    // if(this.array){
    //    array = {
    //     'name': this.array.categoryName,
    // // 'navigationPath': '',
    //   'id':this.array,
    //   'navigationPath': '/pages/dam/folders',
    //   'sameComp':true,
    //   'assetId' : this.array.id
    //   }
    // }
    // else{
    //     array = {}
    // }


    this.previousBreadCrumb.push(this.breadObj)
    this.breadcrumbArray.push(this.breadObj)

    // }
    // else{
    //   this.openDeepyNestedFolder()
    // }
    for(let i= 0;i<this.previousBreadCrumb.length;i++){
      if(this.title == this.previousBreadCrumb[i].name){
        this.breadcrumbArray = this.previousBreadCrumb.slice(0,i)
      }
      }
    }


  var tabId = item.tabId
  console.log(item,"ggghg")
  let param = {
    // tenantId: this.tenantId,
    parentCatId: id,
    roleId : this.roleId,
    tabId:this.tabId,
    innerTabId:tabId,
    instance: item.assetName,
    instanceId: item.assetId,
    areaId: null,
  }
  this.parentCatId = id
  this.parentCatName = item.assetName;
  this.innerTabId = tabId
  if(this.tabId == 1){
    this.header = {
      // title: 'Category Asset',
      title:this.title,
      btnsSearch: true,
      searchBar: true,
      dropdownlabel: 'Folder ',
      drplabelshow: true,
      drpName1: 'Upload Folder',
      drpName2: 'Add Folder ',
      drpName3: '',
      drpName1show: true,
      drpName2show: true,
      drpName3show: false,
      folder:true,
      file:true,
      dropdownlabel1:'File',
      drplabel1show:true,
      drpName_1:'Upload File',
      drpName_2: 'Add File ',
      drpName_1show: true,
      drpName_2show: true,
      btnName1: '',
      btnName2: 'Add Folder',
      btnName3: 'Upload Folder',
      btnName4: 'Upload File',
      btnAdd: 'Add File',
      btnName1show: false,
      btnName2show: false,
      btnName3show: false,
      // btnName4show: true,
      btnName4show: false,
      btnBackshow: true,
      btnAddshow: false,
      filter: true,
      showBreadcrumb:true,
      breadCrumbList:[
        // {
        //   'name': 'DAM',
        //   'navigationPath': '/pages/dam',
        // },
      ]
    }
    this.header.breadCrumbList = this.breadcrumbArray
  }
  else if(this.tabId == 2){
    this.header = {
      // title: 'Category Asset',
      title:this.title,
      btnsSearch: true,
      searchBar: true,
      placeHolder:'Search by keywords',
      dropdownlabel: ' ',
      drplabelshow: false,
      drpName1: '',
      drpName2: ' ',
      drpName3: '',
      drpName1show: false,
      drpName2show: false,
      drpName3show: false,
      btnName1: '',
      btnName2: 'Add Folder',
      // btnName3: 'File Upload',
      // btnAdd: 'Add Asset',
      btnName1show: false,
      btnName2show: false,
      btnName3show: false,
      btnBackshow: true,
      btnAddshow: false,
      filter: true,
      showBreadcrumb:true,
      breadCrumbList:[]

    };
    this.header.breadCrumbList = this.breadcrumbArray
     if(item.isAuthor == 1){
      this.header = {
        // title: 'Category Asset',
        title:this.title,
        btnsSearch: true,
        searchBar: true,
        placeHolder:'Search by keywords',
        dropdownlabel: 'Folder ',
        drplabelshow: true,
        drpName1: 'Upload Folder',
        drpName2: 'Add Folder ',
        drpName3: '',
        drpName1show: true,
        drpName2show: true,
        drpName3show: false,
        folder:true,
        file:true,
        dropdownlabel1:'File',
          drplabel1show:true,
          drpName_1:'Upload File',
          drpName_2: ' Add File',
          drpName_1show: true,
          drpName_2show: true,
        btnName1: '',
        btnName2: 'Add Folder',
        btnName3: 'Upload Folder',
        btnName4: 'Upload File',
        btnAdd: 'Add File',
        btnName1show: false,
        btnName2show: false,
        btnName3show: false,
        // btnName4show:true,
        btnName4show: false,
        btnBackshow: true,
        btnAddshow: false,
        filter: true,
        showBreadcrumb:true,
        breadCrumbList:[
          // {
          //   'name': 'DAM',
          //   'navigationPath': '/pages/dam',
          // },
        ]
      };
      this.header.breadCrumbList = this.breadcrumbArray
     }
  }
  else if(this.tabId == 3){
    this.header = {
      title: 'Folder',
      // title: this.title,
      btnsSearch: true,
      searchBar: true,
      placeHolder:'Search by keywords',
      dropdownlabel: ' ',
      drplabelshow: false,
      drpName1: '',
      drpName2: ' ',
      drpName3: '',
      drpName1show: false,
      drpName2show: false,
      drpName3show: false,
      btnName1: '',
      btnName2: 'Add Folder',
      // btnName3: 'File Upload',
      // btnAdd: 'Add Asset',
      btnName1show: false,
      btnName2show: false,
      btnName3show: false,
      btnBackshow: true,
      btnAddshow: false,
      filter: true,
      showBreadcrumb:true,
      breadCrumbList:[
        // {
        //   'name': 'DAM',
        //   'navigationPath': '/pages/dam',
        // },
      ]

    };
    this.header.breadCrumbList = this.breadcrumbArray

  }
  else if(this.tabId == 4) {
    this.header = {
      // title: 'Category Asset',
      title:this.title,
      btnsSearch: true,
      searchBar: true,
      placeHolder:'Search by keywords',
      dropdownlabel: 'Folder ',
      drplabelshow: true,
      drpName1: 'Upload Folder',
      drpName2: 'Add Folder ',
      drpName3: '',
      drpName1show: true,
      drpName2show: true,
      drpName3show: false,
      folder:true,
      file:true,
      dropdownlabel1:'File',
        drplabel1show:true,
        drpName_1:'Upload File',
        drpName_2: ' Add File',
        drpName_1show: true,
        drpName_2show: true,
      btnName1: '',
      btnName2: 'Add Folder',
      btnName3: 'Upload Folder',
      btnName4: 'Upload File',
      btnAdd: 'Add File',
      btnName1show: false,
      btnName2show: false,
      btnName3show: false,
      // btnName4show:true,
      btnName4show: false,
      btnBackshow: true,
      btnAddshow: false,
      filter: true,
      showBreadcrumb:true,
      breadCrumbList:[
        // {
        //   'name': 'DAM',
        //   'navigationPath': '/pages/dam',
        // },
      ]
    };
    this.header.breadCrumbList = this.breadcrumbArray
     if((item.tabId ==1) || ( item.isAuthor == 1)) {
      this.header = {
        // title: 'Category Asset',
        title:this.title,
        btnsSearch: true,
        searchBar: true,
        placeHolder:'Search by keywords',
        dropdownlabel: 'Folder ',
        drplabelshow: true,
        drpName1: 'Upload Folder',
        drpName2: 'Add Folder ',
        drpName3: '',
        drpName1show: true,
        drpName2show: true,
        drpName3show: false,
        folder:true,
        file:true,
        dropdownlabel1:'File',
          drplabel1show:true,
          drpName_1:'Upload File',
          drpName_2: ' Add File',
          drpName_1show: true,
          drpName_2show: true,
        btnName1: '',
        btnName2: 'Add Folder',
        btnName3: 'Upload Folder',
        btnName4: 'Upload File',
        btnAdd: 'Add File',
        btnName1show: false,
        btnName2show: false,
        btnName3show: false,
        // btnName4show:true,
        btnName4show: false,
        btnBackshow: true,
        btnAddshow: false,
        filter: true,
        showBreadcrumb:true,
        breadCrumbList:[
          // {
          //   'name': 'DAM',
          //   'navigationPath': '/pages/dam',
          // },
        ]
      };
      this.header.breadCrumbList = this.breadcrumbArray
     } else{
      this.header = {
        // title: 'Category Asset',
        title:this.title,
        btnsSearch: true,
        searchBar: true,
        placeHolder:'Search by keywords',
        dropdownlabel: ' ',
        drplabelshow: false,
        drpName1: '',
        drpName2: ' ',
        drpName3: '',
        drpName1show: false,
        drpName2show: false,
        drpName3show: false,
        btnName1: '',
        btnName2: 'Add Folder',
        // btnName3: 'File Upload',
        // btnAdd: 'Add Asset',
        btnName1show: false,
        btnName2show: false,
        btnName3show: false,
        btnBackshow: true,
        btnAddshow: false,
        filter: true,
        showBreadcrumb:true,
        breadCrumbList:[]

      };
      this.header.breadCrumbList = this.breadcrumbArray
     }

  }
   else{

    this.breadcrumbArray =[{
    'name': 'DAM',
    'navigationPath': '/pages/dam',
  },
  // {
  //   'name': 'Folder ',
  //   'navigationPath': '/pages/dam/folders',
  //   'id':{},
  //   'sameComp':true,
  //   'assetId':null
  // }
]
  this.previousBreadCrumb =[{
    'name': 'DAM',
    'navigationPath': '/pages/dam',
  },
  {
    'name': 'Folder',
    'navigationPath': '/pages/dam/folders',
    'id':{},
    'sameComp':true,
    'assetId':null
  }]
  this.header = {
    title: 'Folder',
    // title:this.title,
    btnsSearch: true,
    searchBar: true,
    placeHolder:'Search by keywords',
    dropdownlabel: 'Folder ',
    drplabelshow: true,
    drpName1: 'Upload Folder',
    drpName2: 'Add Folder ',
    drpName3: '',
    folder:true,
    drpName1show: true,
    drpName2show: true,
    drpName3show: false,
    dropdownlabel1:'Add',
    drplabel1show:false,
    drpName_1:'Add Folder',
    drpName_2: ' ',
    drpName_1show: true,
    drpName_2show: false,
    btnName1: '',
    btnName2: 'Add Folder',
    btnName3: 'Upload Folder',
    // btnAdd: 'Add Asset',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: false,
    filter: false,
    showBreadcrumb:true,
    breadCrumbList:[
      // {
      //   'name': 'DAM',
      //   'navigationPath': '/pages/dam',
      // },
    ]
  };
  this.header.breadCrumbList = this.breadcrumbArray
}

  this.assetservice.getAllAssetCategories(param)
  .then(rescompData => {
    this.spinner.hide()
    this.showSpinner = false
    const result = rescompData;
    this.folder = result['data'][1];
    this.tempFolders = _.cloneDeep(this.folder);
    this.tempSearchFolders = _.cloneDeep(this.folder);

    if (result['data'][0].length === 0) {
      this.oldParentCatId = null;
    } else {
      this.oldParentCatId = result['data'][0][0].parentCatId;
      this.isAuthor=result['data'][0][0].isAuthor;
    }
    this.files = result['data'][2];
    if(result['data'][1].length==0 && result['data'][2].length==0){
      this.nodata=true
    }
    this.tempFiles = _.cloneDeep(this.files);
    this.tempSearchFiles = _.cloneDeep(this.files);
    this.allFolderCombined = this.tempFolders.concat(this.files);
  });
// }
}
fileUpload(){
  this.type = 2
  let element:HTMLElement = document.getElementById('checkFile') as HTMLElement;
  element.click()
  this.icon = 'fa fa-file'
  this.closeDetail();
  this.setEmployeeSettingToDefault();
  this.name="Upload File"
  // this.fileInput.nativeElement
  // this.router.navigate(['file-upload'], { relativeTo: this.routes });
}

folderUpload(){
  this.type = 1
  this.name="Upload Folder"
  this.icon=false;
  this.closeDetail();
  this.setEmployeeSettingToDefault();
  let element:HTMLElement = document.getElementById('folderFile') as HTMLElement;
  element.click()
  // this.router.navigate(['file-upload'], { relativeTo: this.routes });
}
onSelectFile(event) {
  if(this.defaultValFlag==0){
 this.selectedEmployees=[]
 this.selectedCreator=[]
 this.selectedTags=[];
 this.tagstrAsset = ''
 this.vieweFileFolderField=true;
 this.view=true;
 this.fileFolderForm={
  channel: '',
  language: '',
  }
  }
 this.fileFolder=false;
 this.showFile=false;
 this.isShowApprover=false;
//  this.selectedEmployee='';
//  this.tagstrAsset='';
//  this.selectedTags='';
  console.log(event.target.files[0],"file Extension")
  var validExts
  if(this.type == 1){
   validExts = new Array( "zip" );
  }else{
  validExts = this.formatExtensions

  //   validExts = new Array('video','audio','application/vnd.ms-powerpoint','application/vnd.openxmlformats-officedocument.presentationml.presentation',
  //   'pdf','image','audio',"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/vnd.ms-excel",
  //   "application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document")
  }
  // var fileType = event.target.files[0].type;
  // var fileType = event.target.files[0].name.split('.')[1];
  var last_dot = event.target.files[0].name.lastIndexOf('.')
  var fileType = event.target.files[0].name.slice(last_dot + 1)

  // extension.value = inputfile.value.split('.')[1];

  var fileExt = false;
  for(let i=0; i<validExts.length; i++){
    if(this.type == 2 ){
    if(fileType == validExts[i].extension){

      fileExt = true;
      break;
    }
  }
  if(this.type == 1 ){
    if(fileType == validExts[i]){

      fileExt = true;
      break;
    }
  }
  }
  // if(validExts.indexOf(fileType) < 0) {
  if(!fileExt) {
    // var toast : Toast = {
    //   type: 'error',
    //   title: "Invalid file selected!",
    //   body: "Valid files are of " + validExts.toString() + " types.",
    //   showCloseButton: true,
    //   timeout: 2000
    // };
    // this.toasterService.pop(toast);
    // this.presentToast('warning', 'Valid file types are ' + validExts.toString());
  if(this.type == 1){
    this.presentToast('warning', 'Please select only zip folder');
    this.vieweFileFolderField=false;
    this.view=false;
  }else{
    this.presentToast('warning', 'Please select a valid file type');
  }
  }else{
  this.fileUrl = []
  var file
  this.file = event.target.files[0]
  this.fileName = event.target.files[0].name
  if(this.type==1&&this.folder.length!=0){
  for(var x in this.folder){
    if(this.folder[x].assetName.toLowerCase()==this.fileName.split('.').slice(0, -1).join('.').toLowerCase()){
      this.presentToast('warning',"Folder name already exist");
      this.view=false;
      this.vieweFileFolderField=false;
      return
    }
  }
}
if(this.type==2&&this.files.length!=0){
  for(var x in this.files){
    if(this.files[x].assetName.toLowerCase()==this.fileName.split('.').slice(0, -1).join('.').toLowerCase()){
      this.presentToast('warning',"File name already exist");
      this.view=false;
      this.vieweFileFolderField=false;
      return
    }
  }
}
  // this.folderPopupshow = true
  // this.file =file
  console.log(this.file,"files")
  for (let i = 0; i < event.target.files.length; i++) {
      // const file = file[i];
      // var dummy = file[i]
      // var path = dummy.webkitRelativePath.split('/');
    // upload file using path
    var reader = new FileReader();
    reader.onload = (event:any) => {
      // console.log(event.target.result);
      this.fileUrl.push(event.target.result);
      console.log(this.fileUrl,"fileUrl")
    }
    reader.readAsDataURL(event.target.files[i]);
  }
  // console.log(path,"path")
  console.log(this.file,"fileUrl")
  if(this.defaultValFlag==1){
  this.upload()
  }
}
}
showPreview = false;
readFileUrl(event:any) {
  if(!this.addEditAssetForm.format || this.addEditAssetForm.format === ''){
    this.presentToast('warning', 'Select Format before uploading file');
    return null;
  }
  var size = 100000000;
  // if(this.addEditAssetForm.format == 1){
  // // var validExts = new Array( "video" );
  // }
  // else if(this.addEditAssetForm.format == 2){
  // // var validExts = new Array( "audio" );
  // }
  // else if(this.addEditAssetForm.format == 3){
  // // var validExts = new Array( "pdf" );
  // }
  // else if(this.addEditAssetForm.format == 7){
  // // var validExts = new Array( "image" );
  // }
  // else if(this.addEditAssetForm.format == 10){
  // // var validExts = new Array( "application/vnd.ms-powerpoint","application/vnd.openxmlformats-officedocument.presentationml.presentation" );
  // }
  // else if(this.addEditAssetForm.format == 11){
  // // var validExts = new Array( "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/vnd.ms-excel" );
  // }
  // else if(this.addEditAssetForm.format == 12){
  // // var validExts = new Array( "application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document" );
  // }
  // else if(this.addEditAssetForm.format == 5){
  // // var validExts = new Array( "application/zip" );
  // }
  // // var validExts  "application/zip"= new Array("image", "video", "audio", "pdf","application/vnd.ms-powerpoint","application/vnd.openxmlformats-officedocument.presentationml.presentation","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/vnd.ms-excel","application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document");

  // var fileType = event.target.files[0].type;
  var validExts = this.formatExtensions
  // var fileType = event.target.files[0].type;
  // var fileType = event.target.files[0].name.split('.')[1];
  var last_dot = event.target.files[0].name.lastIndexOf('.')
  var fileType = event.target.files[0].name.slice(last_dot + 1)

  console.log(last_dot,"fileType")
  console.log(fileType,"fileType")
  // fileType = fileType.substring(0,5);

  // var validExts = new Array(".png", ".jpg", ".jpeg",);
  // var fileExt = event.target.files[0].name;
  // fileExt = fileExt.substring(fileExt.lastIndexOf('.'));

  var fileExt = false;
  // for(let i=0; i<validExts.length; i++){
  //   if(fileType.includes(validExts[i])){
  //     // return true;
  //     fileExt = true;
  //     break;
  //   }
  // }

  for(let i=0; i<validExts.length; i++){
    if(this.addEditAssetForm.format == validExts[i].formatId){
    if(fileType.includes(validExts[i].extension)){
      // return true;
      fileExt = true;
      break;
    }
  }
  }

  // if(validExts.indexOf(fileType) < 0) {
  if(!fileExt) {
    // var toast : Toast = {
    //   type: 'error',
    //   title: "Invalid file selected!",
    //   body: "Valid files are of " + validExts.toString() + " types.",
    //   showCloseButton: true,
    //   timeout: 2000
    // };
    // this.toasterService.pop(toast);
    // this.presentToast('warning', 'Valid file types are ' + validExts.toString());
    this.presentToast('warning', 'Please select a valid file type');

    // this.cancelFile();
  } else {
    if(size <= event.target.files[0].size){
      // var toast : Toast = {
      //     type: 'error',
      //     title: "File size exceeded!",
      //     body: "File size should be less than 100MB",
      //     showCloseButton: true,
      //     timeout: 2000
      // };
      // this.toasterService.pop(toast);
      this.presentToast('warning', 'File size should be less than 100MB');
      // this.cancelFile();
    } else {
      if (event.target.files && event.target.files[0] ) {
        this.fileName = event.target.files[0].name;
        this.showBrowse = true;
        this.isEditFlag=false;
        // this.selectFileTitle = this.fileName


        //read file from input
        // this.fileReaded = event.target.files[0];
        // this.read = event.target.files[0];

        // if(this.fileReaded != '' || this.fileReaded != null || this.fileReaded != undefined){
        //   this.enableUpload = true;
        // }
        let file = event.target.files[0];
        this.addEditAssetForm.assetRef = event.target.files[0].name;
        // this.assetrefLink ='';
        this.fileName = event.target.files[0].name;
        this.assetFileData = event.target.files[0];
        var fileUrl
        var reader = new FileReader();
        reader.onload = (event: ProgressEvent) => {
        this.url = (<FileReader>event.target).result;
        }
        reader.readAsDataURL(event.target.files[0]);
        reader.onload = (event) => {
          this.addEditAssetForm.preViewUrl = this.sanitizer.bypassSecurityTrustResourceUrl(
            reader.result as string
          );
        };
      }
      event.target.value = ''
      this.showPreview = true;
    this.isLocalfile = true;

      /// New Requirement

      /// end
    }
  }
}
uploadFileFolder(){
if(this.fileFolderForm.valid){
  if (this.selectedTags.length > 0) {
    this.makeTagDataReady(this.selectedTags);
     // this.formdata.tags = this.formattedTags;
   }
  //  if(data.selectedEmployee){
  //  if(this.selectedEmployee){
    this.estLen=this.fileFolderForm.value.time?this.formatTime(this.fileFolderForm.value.time) : null
   this.makeAuthorDataReady(this.fileFolderForm.value.author)
  //  }

  if(this.defaultValFlag == 0 && this.isApprover == 1 &&
    Array.isArray(this.selectedCreator) &&
    this.selectedCreator.length == 0){
      this.presentToast('warning',"Please select approver");
      return
  }
  this.upload();
}else{
  this.presentToast('warning', 'Please fill in the required fields');
  Object.keys(this.fileFolderForm.controls).forEach(key => {
    console.log('this.fileFolderForm.controls[key]', this.fileFolderForm.controls[key]);
    this.fileFolderForm.controls[key].markAsDirty();
  });
}
}
upload() {
  this.enableFileUploadModal = true
  var content = {name: 'Bhavesh'};
  var param = {
    parCatId:this.parentCatId,

  }
  this.progress=1
  // let param = this.bulkUploadService.parCatId?this.bulkUploadService.parCatId:null
  var fd = new FormData();
  // fd.append('content', JSON.stringify(content));
  fd.append('file', this.file);
  fd.append('fileType',this.type)
  // fd.append('reportProgress','true')
  fd.append('parCatId',this.parentCatId);
  fd.append('parCatName',this.parentCatName)
  fd.append('defaultValueFlag', this.defaultValFlag);
  if(this.defaultValFlag==0){
    fd.append('chanId',this.fileFolderForm.value.channel)
    fd.append('langId', this.fileFolderForm.value.language);
    fd.append('estCost',this.fileFolderForm.value.cost)
    fd.append('authStr',this.author)
    fd.append('appReq',this.isApprover.toString());
    fd.append('approverId',this.isShowApprover?this.selectedCreator[0].appId:null)
    fd.append('tagStr',this.tagstrAsset)
    fd.append('estLen',this.estLen)
  }else{
    fd.append('chanId',null)
    fd.append('langId', null);
    fd.append('estCost',null)
    fd.append('authStr',null)
    fd.append('appReq',null)
    fd.append('approverId',null)
    fd.append('tagStr',null)
    fd.append('estLen',null)
  }
  // fd.append("data", JSON.stringify(fd));
  const url = webApi.domain + webApi.url.insertAssetBulkUploadNew;
  // let headers = new HttpHeaders();
  // headers = headers.set('Content-Type', 'multipart/form-data');
  let options = {
    search:{},
    reportProgress: true,
};
  this.http1
    .post(url,
       fd, {
        reportProgress: true,
        observe: 'events'
    })
    .pipe(
      map((event: any) => {
        if (event.type == HttpEventType.UploadProgress) {
          // this.progress = Math.round((100 / event.total) * event.loaded);
          if(this.progress < 90){
            // this.spinner.show();
            // this.cdf.detectChanges();
            this.progress = Math.round((100 / event.total) * event.loaded);
          }
        } else if (event.type == HttpEventType.Response) {
          this.progress = null;
          // this.spinner.hide();
          // this.cdf.detectChanges();
          this.progress = 100;
          this.enableFileUploadModal = false
          this.closeDetail();
          if (event['body']['type'] == true) {
            this.file = ''
            // this.fileName = 'Click here to upload File'
            // this.folderPopupshow = false;
            // if(this.type == 1){
            //   this.fileName = 'Click here to upload Folder'
            // }
            // else{
            //   this.fileName = 'Click here to upload File'
            // }
            // const toast: Toast = {
            //   type: 'success',
            //   title: 'Bulk Upload.',
            //   body: 'Assets uploaded successfully.',
            //   showCloseButton: true,
            //   timeout: 2000,
            // };
            // this.spinner.hide();
            // this.toasterService.pop(toast);
            setTimeout(() => {
            this.presentToast('success', event['body']['message']);
            }, 500);
            this.getFileAndFolders(this.parentCatId,this.roleId,this.tabId,this.innerTabId)
            var param= {
              tabId:this.tabId,
              roleId:this.roleId

            }
            this.getTree(param, false)
            // this.getAllAssetandCategories(this.parentCatId,this.roleId,this.tabId,this.innerTabId)
            // this.router.navigate(['../'],{relativeTo:this.routes});
          } else {
            // var toast: Toast = {
            //   type: 'error',
            //   title: 'Bulk Upload.',
            //   body: 'Unable to upload assets.',
            //   showCloseButton: true,
            //   timeout: 2000,
            // };
            // this.spinner.hide();
            // this.toasterService.pop(toast);
            // this.presentToast('error', '');
            this.presentToast('Warning', event['body']['message']);
          }
        }
      }),
      catchError((err:any) => {
        // this.spinner.hide();
        // this.cdf.detectChanges();
        this.progress = null;
        this.enableFileUploadModal = false
        this.presentToast('warning', '');
        // this.presentToast('warning', );
        // alert(err.message);
        return throwError(err.message);
      })
    )
    .toPromise();
}
getFileAndFolders(parentCatId,roleId,tabId,innerTabId){
  // this.fileFolder = false
  // this.showFile = false
  this.showSpinner = true
  // this.view = false
this.nodata=false
  // this.showSpinner = true
  // this.spinner.show()
  let param = {
    // tenantId: this.tenantId,
    parentCatId: parentCatId,
    roleId : roleId,
    tabId:tabId,
    innerTabId:innerTabId
  }
  if(this.tabId==1){
    if(parentCatId == null ){
      this.header = {
        title: 'Folder',
        // title:this.title,
        btnsSearch: true,
        searchBar: true,
        placeHolder:'Search by keywords',
        dropdownlabel: 'Folder ',
        drplabelshow: true,
        drpName1: 'Upload Folder',
        drpName2: 'Add Folder ',
        drpName3: '',
        folder:true,
        drpName1show: true,
        drpName2show: true,
        drpName3show: false,
        dropdownlabel1:'Add',
        drplabel1show:false,
        drpName_1:'Add Folder',
        drpName_2: ' ',
        drpName_1show: true,
        drpName_2show: false,
        btnName1: '',
        btnName2: 'Add Folder',
        btnName3: 'Upload Folder',
        // btnAdd: 'Add Asset',
        btnName1show: false,
        btnName2show: false,
        btnName3show: false,
        btnBackshow: true,
        btnAddshow: false,
        filter: false,
        showBreadcrumb:true,
        breadCrumbList:[
          {
            'name': 'DAM',
            'navigationPath': '/pages/dam',
          },
        ]
      };
    }else{
      this.header = {
        // title: 'Category Asset',
        title:this.title,
        btnsSearch: true,
        searchBar: true,
        dropdownlabel: 'Folder ',
        drplabelshow: true,
        drpName1: 'Upload Folder',
        drpName2: 'Add Folder ',
        drpName3: '',
        drpName1show: true,
        drpName2show: true,
        drpName3show: false,
        folder:true,
        file:true,
        dropdownlabel1:'File',
        drplabel1show:true,
        drpName_1:'Upload File',
        drpName_2: 'Add File ',
        drpName_1show: true,
        drpName_2show: true,
        btnName1: '',
        btnName2: 'Add Folder',
        btnName3: 'Upload Folder',
        btnName4: 'Upload File',
        btnAdd: 'Add File',
        btnName1show: false,
        btnName2show: false,
        btnName3show: false,
        // btnName4show: true,
        btnName4show: false,
        btnBackshow: true,
        btnAddshow: false,
        filter: true,
        showBreadcrumb:true,
        breadCrumbList:[
          {
            'name': 'DAM',
            'navigationPath': '/pages/dam',
          },
        ]
      }
      console.log(this.breadcrumbArray)
      this.header.breadCrumbList =  this.breadcrumbArray
    }
  }else if(this.tabId == 2){
    if(parentCatId == null ){
    this.header = {
      title: 'Folder',
      // title:this.title,
      btnsSearch: true,
      searchBar: true,
      placeHolder:'Search by keywords',
      dropdownlabel: ' ',
      drplabelshow: false,
      drpName1: '',
      drpName2: ' ',
      drpName3: '',
      drpName1show: false,
      drpName2show: false,
      drpName3show: false,
      btnName1: '',
      btnName2: 'Add Folder',
      // btnName3: 'File Upload',
      // btnAdd: 'Add Asset',
      btnName1show: false,
      btnName2show: false,
      btnName3show: false,
      btnBackshow: true,
      btnAddshow: false,
      filter: false,
      showBreadcrumb:true,
      breadCrumbList:[
        {
          'name': 'DAM',
          'navigationPath': '/pages/dam',
        },
      ]
    };
    // this.header.breadCrumbList =  this.breadcrumbArray
    // if(parentCatId == null && this.isAuthor==1){
    //   this.header = {
    //     // title: 'Category Asset',
    //     title:this.title,
    //     btnsSearch: true,
    //     searchBar: true,
    //     placeHolder:'Search by keywords',
    //     dropdownlabel: 'Folder ',
    //     drplabelshow: true,
    //     drpName1: 'Upload Folder',
    //     drpName2: 'Add Folder ',
    //     drpName3: '',
    //     drpName1show: true,
    //     drpName2show: true,
    //     drpName3show: false,
    //     folder:true,
    //     file:true,
    //     dropdownlabel1:'File',
    //       drplabel1show:true,
    //       drpName_1:'Upload File',
    //       drpName_2: ' Add File',
    //       drpName_1show: true,
    //       drpName_2show: true,
    //     btnName1: '',
    //     btnName2: 'Add Folder',
    //     btnName3: 'Upload Folder',
    //     btnName4: 'Upload File',
    //     btnAdd: 'Add File',
    //     btnName1show: false,
    //     btnName2show: false,
    //     btnName3show: false,
    //     // btnName4show:true,
    //     btnName4show: false,
    //     btnBackshow: true,
    //     btnAddshow: false,
    //     filter: true,
    //     showBreadcrumb:true,
    //     breadCrumbList:[
    //       // {
    //       //   'name': 'DAM',
    //       //   'navigationPath': '/pages/dam',
    //       // },
    //     ]
    //   };
    //   this.header.breadCrumbList = this.breadcrumbArray
    // }
    }else {
      this.header = {
        // title: 'Category Asset',
        title:this.title,
        btnsSearch: true,
        searchBar: true,
        placeHolder:'Search by keywords',
        dropdownlabel: ' ',
        drplabelshow: false,
        drpName1: '',
        drpName2: ' ',
        drpName3: '',
        drpName1show: false,
        drpName2show: false,
        drpName3show: false,
        btnName1: '',
        btnName2: 'Add Folder',
        // btnName3: 'File Upload',
        // btnAdd: 'Add Asset',
        btnName1show: false,
        btnName2show: false,
        btnName3show: false,
        btnBackshow: true,
        btnAddshow: false,
        filter: true,
        showBreadcrumb:true,
        breadCrumbList:[]

      };
      this.header.breadCrumbList =  this.breadcrumbArray
      if(this.isAuthor == 0){
        this.header = {
          // title: 'Category Asset',
          title:this.title,
          btnsSearch: true,
          searchBar: true,
          placeHolder:'Search by keywords',
          dropdownlabel: ' ',
          drplabelshow: false,
          drpName1: '',
          drpName2: ' ',
          drpName3: '',
          drpName1show: false,
          drpName2show: false,
          drpName3show: false,
          btnName1: '',
          btnName2: 'Add Folder',
          // btnName3: 'File Upload',
          // btnAdd: 'Add Asset',
          btnName1show: false,
          btnName2show: false,
          btnName3show: false,
          btnBackshow: true,
          btnAddshow: false,
          filter: true,
          showBreadcrumb:true,
          breadCrumbList:[]

        };
        this.header.breadCrumbList =  this.breadcrumbArray
      }else if(this.isAuthor==1){
        this.header = {
          // title: 'Category Asset',
          title:this.title,
          btnsSearch: true,
          searchBar: true,
          placeHolder:'Search by keywords',
          dropdownlabel: 'Folder ',
          drplabelshow: true,
          drpName1: 'Upload Folder',
          drpName2: 'Add Folder ',
          drpName3: '',
          drpName1show: true,
          drpName2show: true,
          drpName3show: false,
          folder:true,
          file:true,
          dropdownlabel1:'File',
            drplabel1show:true,
            drpName_1:'Upload File',
            drpName_2: ' Add File',
            drpName_1show: true,
            drpName_2show: true,
          btnName1: '',
          btnName2: 'Add Folder',
          btnName3: 'Upload Folder',
          btnName4: 'Upload File',
          btnAdd: 'Add File',
          btnName1show: false,
          btnName2show: false,
          btnName3show: false,
          // btnName4show:true,
          btnName4show: false,
          btnBackshow: true,
          btnAddshow: false,
          filter: true,
          showBreadcrumb:true,
          breadCrumbList:[
            // {
            //   'name': 'DAM',
            //   'navigationPath': '/pages/dam',
            // },
          ]
        };
        this.header.breadCrumbList = this.breadcrumbArray
      }
    }
  }else if(this.tabId == 3){
    this.header = {
      title: 'Folder',
      // title: this.title,
      btnsSearch: true,
      searchBar: true,
      placeHolder:'Search by keywords',
      dropdownlabel: ' ',
      drplabelshow: false,
      drpName1: '',
      drpName2: ' ',
      drpName3: '',
      drpName1show: false,
      drpName2show: false,
      drpName3show: false,
      btnName1: '',
      btnName2: 'Add Folder',
      // btnName3: 'File Upload',
      // btnAdd: 'Add Asset',
      btnName1show: false,
      btnName2show: false,
      btnName3show: false,
      btnBackshow: true,
      btnAddshow: false,
      filter: true,
      showBreadcrumb:true,
      breadCrumbList:[
        {
          'name': 'DAM',
          'navigationPath': '/pages/dam',
        },
      ]

    };
    // this.header.breadCrumbList =  this.breadcrumbArray

  }
  // else if(this.tabId == 4){
  //   if(this.parentCatId == null ){
  //     this.header = {
  //       title: 'Category Asset',
  //       btnsSearch: true,
  //       searchBar: true,
  //       placeHolder:'Search by Category',
  //       dropdownlabel: ' ',
  //       drplabelshow: false,
  //       drpName1: '',
  //       drpName2: ' ',
  //       drpName3: '',
  //       drpName1show: false,
  //       drpName2show: false,
  //       drpName3show: false,
  //       btnName1: '',
  //       btnName2: 'Add Folder',
  //       btnName3: 'Upload Folder',
  //       // btnAdd: 'Add Asset',
  //       btnName1show: false,
  //       btnName2show: true,
  //       btnName3show: true,
  //       btnBackshow: true,
  //       btnAddshow: false,
  //       filter: false,
  //       showBreadcrumb:true,
  //       breadCrumbList:[
  //         {
  //           'name': 'DAM',
  //           'navigationPath': '/pages/dam',
  //         },
  //       ]
  //     };
  //     }
  // }
  else if(parentCatId == null && this.tabId == 4){
    this.header = {
      title: 'Folder',
      btnsSearch: true,
      searchBar: true,
      placeHolder:'Search by keywords',
      dropdownlabel: 'Folder ',
      drplabelshow: true,
      drpName1: 'Upload Folder',
      drpName2: 'Add Folder ',
      drpName3: '',
      drpName1show: true,
      drpName2show: true,
      drpName3show: false,
      folder:true,
      dropdownlabel1:'File',
      drplabel1show:false,
      drpName_1:'Add Folder',
      drpName_2: ' ',
      drpName_1show: true,
      drpName_2show: false,
      btnName1: '',
      btnName2: 'Add Folder',
      btnName3: 'Upload Folder',
      // btnAdd: 'Add Asset',
      btnName1show: false,
      btnName2show: false,
      btnName3show: false,
      btnBackshow: true,
      btnAddshow: false,
      filter: false,
      showBreadcrumb:true,
      breadCrumbList:[
        {
          'name': 'DAM',
          'navigationPath': '/pages/dam',
        },
      ]
    };
    if(this.primaryButtonFlag == 0){
      this.header.drplabelshow = false
    }
    else{
      this.header.drplabelshow = true
    }
  }
  else if(parentCatId != null && this.tabId == 4 ){
    this.header = {
      // title: 'Category Asset',
      title:this.title,
      btnsSearch: true,
      searchBar: true,
      placeHolder:'Search by keywords',
      dropdownlabel: 'Folder ',
      drplabelshow: true,
      drpName1: 'Upload Folder',
      drpName2: 'Add Folder ',
      drpName3: '',
      drpName1show: true,
      drpName2show: true,
      drpName3show: false,
      folder:true,
      file:true,
      dropdownlabel1:'File',
        drplabel1show:true,
        drpName_1:'Upload File',
        drpName_2: ' Add File',
        drpName_1show: true,
        drpName_2show: true,
      btnName1: '',
      btnName2: 'Add Folder',
      btnName3: 'Upload Folder',
      btnName4: 'Upload File',
      btnAdd: 'Add File',
      btnName1show: false,
      btnName2show: false,
      btnName3show: false,
      // btnName4show:true,
      btnName4show: false,
      btnBackshow: true,
      btnAddshow: false,
      filter: true,
      showBreadcrumb:true,
      breadCrumbList:[
        // {
        //   'name': 'DAM',
        //   'navigationPath': '/pages/dam',
        // },
      ]
    };
    this.header.breadCrumbList =  this.breadcrumbArray
  }
  // else if(this.parentCatId != null && this.tabId == 4 && this.addAssetService.view == 1){
  //   this.header = {
  //     title: 'Category Asset',
  //     btnsSearch: true,
  //     searchBar: true,
  //     placeHolder:'Search by Category name',
  //     dropdownlabel: ' ',
  //     drplabelshow: false,
  //     drpName1: '',
  //     drpName2: ' ',
  //     drpName3: '',
  //     drpName1show: false,
  //     drpName2show: false,
  //     drpName3show: false,
  //     btnName1: '',
  //     btnName2: 'Add Folder',
  //     btnName3: 'Upload Folder',
  //     btnName4: 'Upload File',
  //     btnAdd: 'Add File',
  //     btnName1show: false,
  //     btnName2show: true,
  //     btnName3show: true,
  //     btnBackshow: true,
  //     btnAddshow: true,
  //     filter: false,
  //     showBreadcrumb:true,
  //     breadCrumbList:[
  //       {
  //         'name': 'DAM',
  //         'navigationPath': '/pages/dam',
  //       },
  //     ]
  //   };
  // }
  // else if(this.parentCatId != null && this.tabId == 4 && this.addAssetService.view == 2){
  //   this.header = {
  //     title: 'Category Asset',
  //     btnsSearch: true,
  //     searchBar: true,
  //     placeHolder:'Search by Category name',
  //     dropdownlabel: ' ',
  //     drplabelshow: false,
  //     drpName1: '',
  //     drpName2: ' ',
  //     drpName3: '',
  //     drpName1show: false,
  //     drpName2show: false,
  //     drpName3show: false,
  //     btnName1: '',
  //     btnName2: 'Add Folder',
  //     btnName3: 'Folder Upload',
  //     // btnAdd: 'Add Asset',
  //     btnName1show: false,
  //     btnName2show: false,
  //     btnName3show: false,
  //     btnBackshow: true,
  //     btnAddshow: false,
  //     filter: false,
  //     showBreadcrumb:true,
  //     breadCrumbList:[
  //       {
  //         'name': 'DAM',
  //         'navigationPath': '/pages/dam',
  //       },
  //     ]
  //   };
  // }
  // this.spinner.show()
  this.cdf.markForCheck()
  this.assetservice.getAllAssetCategories(param)
  .then(rescompData => {
    this.showSpinner = false
    this.cdf.detectChanges()
    const result = rescompData;
    this.folder=result['data'][1]
    this.tempFolders = _.cloneDeep(this.folder);
    if (result['data'][0].length === 0) {
      this.oldParentCatId = null;
    } else {
      this.oldParentCatId = result['data'][0][0].parentCatId;
    }
    var folder=this.folder
    this.tempSearchFolders = _.cloneDeep(this.folder);
    this.files = result['data'][2];
    this.tempFiles = _.cloneDeep(this.files);
    this.tempSearchFiles = _.cloneDeep(this.files);
    this.allFolderCombined = this.tempFolders.concat(this.files);
    // this.spinner.hide()
    var files=this.files;
    if(result['data'][1].length==0 && result['data'][2].length==0){
      this.nodata=true
    }
  },error=>{
    this.nodata=true
  });
}
presentToast(type, body) {
  if (type === 'success') {
    this.toastr.success(body, 'Success', {
      closeButton: false
    });
  } else if (type === 'error') {
    this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
      timeOut: 0,
      closeButton: true
    });
  } else {
    this.toastr.warning(body, 'Warning', {
      closeButton: false
    })
  }
}


  mouseLeave() {
    this.show = true;
  }

  heightMesure() {
  }
  // selectedFolder = null;
  selectedFolder:any = {};
  passDataToAppchild(item){
    console.log(item,"jftyddtrerzerzrzrz")
    this.valueChanged = item.assetId;
    // this.selectedFolder = item;
    this.selectedFolder.assetId = item.assetId;
    }

    toggleSidebar(val) {
      // this.sidebarService.toggle(true, 'menu-sidebar');
      // this.layoutService.changeLayoutSize();
      // return false;
      if(val === 'open'){
        if(!document.getElementsByTagName('nb-sidebar')[0].classList.contains('expanded')){
          document.getElementsByTagName('nb-sidebar')[0].classList.add('expanded')
        }
        if(document.getElementsByTagName('nb-sidebar')[0].classList.contains('compacted')){
          document.getElementsByTagName('nb-sidebar')[0].classList.remove('compacted')
        }

      }else {
        if(!document.getElementsByTagName('nb-sidebar')[0].classList.contains('compacted')){
          document.getElementsByTagName('nb-sidebar')[0].classList.add('compacted')
        }
        if(document.getElementsByTagName('nb-sidebar')[0].classList.contains('expanded')){
          document.getElementsByTagName('nb-sidebar')[0].classList.remove('expanded')
        }
      }
      this.layoutService.changeLayoutSize();
    }


    onFormatChange(){
      // this.showPreview = false;
      this.addEditAssetForm.preViewUrl = null;
      this.addEditAssetForm.assetRef = null;
      this.assetFileData = null;
      this.showBrowse = false;
      this.fileName = 'Select a file';
      // if(this.addEditAssetForm.format == "8" || this.addEditAssetForm.format == "6" || this.addEditAssetForm.format == "4" )
      // {
      //   this.showPreview = true;
      //   this.External = true
      // }
      this.assignValueToSelectDropDown(this.addEditAssetForm.format);
    }
    selectedAssetFormatData ={
      filterId: '',
      filterValue: '',
      formatId: '',
      formatMode: '',
      formatName: '',
      isSelected: '',
      pickerType: null,
      previewEnable: null,
      tagId: '',
      tagName: '',
    } ;
    assignValueToSelectDropDown(selectedFormatId){
      if(selectedFormatId && this.formats && this.formats.length){
        for(let i = 0 ; i < this.formats.length; i ++){
          if(selectedFormatId == this.formats[i].formatId){
            this.selectedAssetFormatData = this.formats[i];
            break;
          }
        }
      }else {
         this.setSelectedValueDefault();
      }
    }
    setSelectedValueDefault(){
      this.selectedAssetFormatData ={
        filterId: '',
        filterValue: '',
        formatId: '',
        formatMode: '',
        formatName: '',
        isSelected: '',
        pickerType: null,
        previewEnable: null,
        tagId: '',
        tagName: '',
      } ;
    }
    assetVersions = [];
    getAssetVersions(id){
      // this.spinner.show();
      let param = {

        'aId': id
      }

      this.assetservice.getAllAssetVersion(param)
        .then(rescompData => {
          var result = rescompData;
          console.log('versions:', result);
          if (result['type'] == true) {
            if (result['data'].length == 0) {
              // this.noAsset = true;
              // this.skeleton = true;
              // this.noDataFound = true;
              // this.presentToast('error', '');
            } else {
              this.assetVersions = result['data'];
              console.log(this.assetVersions,"this.assetVersions")
              // this.versionshow = true


            }
            //this.skeleton = true;
          } else {
            // this.spinner.hide();
            // this.skeleton = true
            // this.noDataFound = true;

          }


        }, error => {
          // this.spinner.hide();
          // this.skeleton = true
          // this.noDataFound = true

          this.presentToast('error', '');
        });

    }

    copytext(data){
      console.log("Assets url:>",data);
      let selBox = document.createElement('textarea');
      selBox.style.position = 'fixed';
      selBox.style.left = '0';
      selBox.style.top = '0';
      selBox.style.opacity = '0';
      selBox.value = data;
      document.body.appendChild(selBox);
      selBox.focus();
      selBox.select();
      document.execCommand('copy');
      document.body.removeChild(selBox);
      this.toastr.success('Link Copied', 'Success');
    }
    deleteAssetVersion(data){
      var  param= {
       'aId':data.assetId,
       'avId':data.avId,
       'assetkey':data.assetRefKey,
       'assetRef':data.assetRef,
       }
     this.assetservice.deleteAssetVersion(param)
     .then(rescompData => {
       var result = rescompData;
       console.log('versions:', result);
       if (result['type'] == true) {
         this.toastr.success('File version Deleted Successfully','Success')
         this.getFileAndFolders(this.parentCatId,this.roleId,this.tabId,this.innerTabId)
         this.getAssetVersions(data.assetId);
         this.detailsTab[1];
        //  this.closeDetail();
         //this.skeleton = true;
       } else {
         // this.spinner.hide();
         // this.skeleton = true
         // this.noDataFound = true;

       }


     }, error => {
       // this.spinner.hide();
       // this.skeleton = true
       // this.noDataFound = true

       this.presentToast('error', '');
     });

   }
   previewData = null;
   previewName = '';
   previewShow = false;
   pdfserverlink = '';
   pdfReference: any;
   preview(data){
    this.previewData = data;
    // if(this.previewData.formatId != 10 || this.previewData.formatId != 11 || this.previewData.formatId != 12){
    if (this.previewData.formatId == 10) {
      this.previewName = 'PPT'
    }
    if (this.previewData.formatId == 11) {
      this.previewName = 'Excel'
    }
    if (this.previewData.formatId == 12) {
      this.previewName = 'Word'
    }
    if (this.previewData.formatId == 7) {
      this.previewName = 'Image'
    }
    if (this.previewData.formatId == 1) {
      this.previewName = 'Video'
    }
    if (this.previewData.formatId == 2) {
      this.previewName = 'Audio'
      this.previewData.preViewUrl=this.previewData.assetRef
    }
    if (this.previewData.formatId == 8) {
      this.previewName = 'External Link'
    }
    if (this.previewData.formatId == 4) {
      this.previewName = 'KPoint'
      //this.nativeElementConfigure();\
      setTimeout(() => {
        this.kpointConfigure1(this.previewData.assetRef);
      }, 500);
    }
    if (this.previewData.formatId == 3) {
      this.previewName = 'PDF'
      this.generatePdfLink(this.previewData.assetRef);
  }
    // this.addEditAssetForm.reference = this.transform(
    //   this.convertURL(this.addEditAssetForm.assetRef)
    // );
    // this.transform(this.addEditAssetForm.assetRef);
    console.log('PDF file', this.pdfReference);
    // this.cdf.detectChanges();

    if (this.previewData.formatId == 6) {
      setTimeout(() => {
        this.youtubeconfigure(this.previewData.assetRef);
      }, 500);
    }
    this.assignValueToSelectDropDown(this.previewData.formatId);
    this.previewShow  = true;
  // }else{
  //   this.presentToast('warning','Sorry preview is not availab')
  // }


  }
  clsosePreview(){
    this.deletePdf();
    this.previewShow = false;
    this.previewData = null;
  }

  deletePdf(){
    if (this.pdfserverlink != '') {
      this.addassetservice.deletePdfLink(this.pdfserverlink).then((result: any) => {
        console.log('Link ===>', result);
        this.pdfserverlink = null;
      })
      .catch(result => {
        console.log('RESULT===>',result);
      });
    }
  }

  generatePdfLink(data){
    this.addassetservice.getPdfLink(data).then((result: any) => {
      this.pdfserverlink = result.data;
      this.pdfReference = this.sanitizer.bypassSecurityTrustResourceUrl(
                                          this.convertURL(this.pdfserverlink));

      console.log('Link ===>', result);
      this.isLocalfile = false
    })
    .catch(result => {
      console.log('RESULT===>',result);
    });
  }
  convertURL (url) {
    return webApi.domain + webApi.url.pdfviewerurl + url;
  }
  ngOnDestroy() {

    this.toggleSidebar('open');
  }
  kpointConfigure1(kpointlink) {
    //  this.addEditAssetForm.assetRef = kpointlink;
      // this.kpointurl = kpointlink;
      try {
        if (!kpointlink) {
          alert('Please Enter Url');
        } else {
          const kpoint = this.populateKpointObj(kpointlink);
          const player = kpoint.Player(this.kpplayer1.nativeElement, {
            kvideoId: kpoint.id,
            videoHost: kpoint.host,
            params: { autoplay: true }
          });
        }
      } catch (e) {
        console.log(e);
      }
    }
  populateKpointObj(url) {
    // tslint:disable-next-line:prefer-const
    let kpoint: any = {
      host: '',
      id: '',
      src: '',
      url: ''
    };
    if (url.split('/').length > 0) {
      kpoint.host = url.split('/')[2];
      kpoint.id = url.split('/')[4];
      kpoint.src = url;
      kpoint.url = url.split('/')[0] + '//' + url.split('/')[2];
    }
    return kpoint;
  }
  youtubeconfigure(link) {
   // this.addEditAssetForm.assetRef = link;
    // this.youtubeurl = link;
    console.log('youtubeurl', link);
    try {
      if (!link) {
        alert('Please Enter url');
      } else {
        const videoid = this.populateYoutubeObj(link);
        // if (this.youtubeplayer) {
        //   this.youtubeplayer.loadVideoById(videoid);
        // } else {
        //   this.youtubeplay.id = videoid;
        // }
        const url =
          'https://www.youtube.com/embed/' + videoid + '?autoplay=1&showinfo=0';
        this.youtubeurl = this.transform(url);
      }
    } catch (e) {
      console.log(e);
    }
  }
  transform(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
  youtubeplayer: YT.Player;
  savePlayer(player) {
    this.youtubeplayer = player;
    console.log('player instance', player);
  }
  onStateChange(event) {
    console.log('player state', event.data);
  }
  populateYoutubeObj(url) {
    try {
      if (url) {
        let video_id = '';
        if (url.includes('embed')) {
          const urlsplit = url.split('/');
          video_id = urlsplit[urlsplit.length - 1];
        } else {
          video_id = url.split('v=')[1];
          const ampersandPosition = video_id.indexOf('&');
          if (ampersandPosition !== -1) {
            video_id = video_id.substring(0, ampersandPosition);
          }
        }
        return video_id;
      } else {
        return null;
      }
    } catch (e) {
      console.log(e);
      return null;
    }
  }
  encodeURI(url) {
    if (UriBuilder.parse(url).isRelative()) {
      url = cleanPath(document.baseURI + '/' + url);
    }
    return encodeURIComponent(url);
  }

  getDocumentUrl(url) {
    return (
      'https://docs.google.com/gview?url=' + encodeURI(url) + '&embedded=true'
    );
  }
  // insertAssetVersion(data){
  //   if(data.assetRef!= null&&data.comment!=undefined&&data.assetRef!=''&&data.comment!=''){
  //   // let duration = null
  //   this.spinner.show();
  //   if(this.isShowApprover){
  //     if(this.selectedCreator && Array.isArray(this.selectedCreator) &&this.selectedCreator.length == 0){
  //       this.presentToast('warning',"Please select approver");
  //       return
  //     }
  //   }
  //   console.log('data',data);
  //   let param = {
  //     "assetsId":data.assetId,
  //     "assetsRef":data.assetRef,
  //     "assetsBitly":null,
  //     "assetsDesc":data.description,
  //     "assetsLen":data.time?this.formatTime(data.time):null,
  //     'assetsStatus': 1,
  //     "assetsCost":data.estimateCost,
  //     "tId":this.tenantId,
  //     "instanceName":data.assetName,
  //     "parentCatName" :this.parentCatName,
  //     "userId":this.userId,
  //     "approverId":  this.isShowApprover?this.selectedCreator[0].appId:null,
  //     "appReq":this.addEditAssetForm.appReq,
  //     "assetComment":data.comment
  //     //"author":data.selectedEmployee[0].edgeempId
  //   }

  //   console.log('param',param);

  //   this.passParams = param;
  //       console.log('this.passParams',this.passParams);
  //       var fd = new FormData();
  //       fd.append('content',JSON.stringify(this.passParams));
  //       fd.append('file',this.assetFileData);
  //       console.log('File Data ',fd);

  //       console.log('Asset File',this.assetFileData);
  //       console.log('Asset Data ',this.passParams);
  //       let fileUploadUrl = webApi.domain + webApi.url.fileUpload;

  //       if(this.assetFileData){
  //         this.webApiService.getService(fileUploadUrl,fd)
  //           .then(rescompData => {
  //             var temp:any = rescompData;
  //             this.fileUploadRes = temp;
  //             console.log('rescompData',this.fileUploadRes)
  //             this.assetFileData = null;
  //             if(temp == "err"){
  //               // this.notFound = true;
  //               // var thumbUpload : Toast = {
  //               //     type: 'error',
  //               //     title: "Asset File",
  //               //     body: "Unable to upload Asset File.",
  //               //     // body: temp.msg,
  //               //     showCloseButton: true,
  //               //     timeout: 2000
  //               // };
  //               // this.toasterService.pop(thumbUpload);
  //               this.presentToast('error', '');
  //               this.spinner.hide();
  //             }else if(temp.type == false){
  //               // var thumbUpload : Toast = {
  //               //     type: 'error',
  //               //     title: "Asset File",
  //               //     body: "Unable to upload Asset File.",
  //               //     // body: temp.msg,
  //               //     showCloseButton: true,
  //               //     timeout: 2000
  //               // };
  //               // this.toasterService.pop(thumbUpload);
  //               this.presentToast('error', '');
  //               this.spinner.hide();
  //             }
  //             else{
  //               if(this.fileUploadRes.data != null || this.fileUploadRes.fileError != true){
  //                 this.passParams.assetsRef = this.fileUploadRes.data.file_url;
  //                 this.passParams.assetKey = this.fileUploadRes.data.filename;
  //                 this.passParams.assetSize = this.fileUploadRes.data.size;
  //                 console.log('this.passparams.assetsRef',this.passParams.assetsRef);
  //                 this.spinner.hide();
  //                this.addAssetVersion(this.passParams);
  //               }else{
  //                 // var thumbUpload : Toast = {
  //                 //     type: 'error',
  //                 //     title: "Asset File",
  //                 //     // body: "Unable to upload course thumbnail.",
  //                 //     body: this.fileUploadRes.status,
  //                 //     showCloseButton: true,
  //                 //     timeout: 2000
  //                 // };
  //                 // this.toasterService.pop(thumbUpload);
  //                 this.presentToast('error', '');
  //                 this.spinner.hide();
  //               }
  //             }
  //             console.log('File Upload Result',this.fileUploadRes);
  //             var res = this.fileUploadRes;
  //             this.fileres = res.data.file_url;
  //           },
  //           resUserError=>{
  //             this.errorMsg = resUserError;
  //             console.log('File upload this.errorMsg', this.errorMsg);
  //             this.spinner.hide();
  //           });
  //       }else{
  //        this.spinner.hide();
  //       this.getAssetVersions(this.passParams);
  //       }
  //     }else{
  //       this.presentToast('warning','Please fill all fields');
  //     }
  // }


  insertAssetVersion(data, assetForm){
    if(data.assetRef!= null&&data.comment!=undefined&&data.assetRef!=''&&data.comment!=''){
    // let duration = null
    if(this.isShowApprover){
      if(this.selectedCreator && Array.isArray(this.selectedCreator) &&this.selectedCreator.length == 0){
        this.presentToast('warning',"Please select approver");
        return
      }
    }
    if(data['comment'] && String(data.comment).length != 0){
      if(data.comment.length > 20 && data.comment.length < 500){

      }else {
        Object.keys(this.assetForm.controls).forEach(key => {
          this.assetForm.controls[key].markAsDirty();
        });
        this.presentToast('warning','Please fill all fields');
        return
      }
    }
     // Asset version handled in backend
    if(this.selectedAssetFormatData.pickerType == 1){

      console.log('data',data);
      let param = {
        "assetsId":data.assetId,
        "assetsRef":data.assetRef,
        "assetsBitly":null,
        "assetsDesc":data.description,
        "assetsLen":data.time?this.formatTime(data.time):null,
        'assetsStatus': null,
        "assetsCost":data.estimateCost,
        "tId":this.tenantId,
        "instanceName":data.assetName,
        "parentCatName" :this.parentCatName,
        "userId":this.userId,
        "approverId":  this.isShowApprover?this.selectedCreator[0].appId:null,
        "appReq":this.addEditAssetForm.appReq,
        "assetComment":data.comment
        //"author":data.selectedEmployee[0].edgeempId
      }
          if(this.assetFileData){
            this.spinner.show();
            this.passParams = param;
            console.log('this.passParams',this.passParams);
            var fd = new FormData();
            fd.append('content',JSON.stringify(this.passParams));
            fd.append('file',this.assetFileData);
            console.log('File Data ',fd);
            let fileUploadUrl = webApi.domain + webApi.url.fileUpload;
            this.webApiService.getService(fileUploadUrl,fd)
            .then(rescompData => {
              var temp:any = rescompData;
              this.fileUploadRes = temp;
              console.log('rescompData',this.fileUploadRes)

              if(temp == "err"){
                // this.notFound = true;
                // var thumbUpload : Toast = {
                //     type: 'error',
                //     title: "Asset File",
                //     body: "Unable to upload Asset File.",
                //     // body: temp.msg,
                //     showCloseButton: true,
                //     timeout: 2000
                // };
                // this.toasterService.pop(thumbUpload);
                this.presentToast('error', '');
                this.spinner.hide();
              }else if(temp.type == false){
                // var thumbUpload : Toast = {
                //     type: 'error',
                //     title: "Asset File",
                //     body: "Unable to upload Asset File.",
                //     // body: temp.msg,
                //     showCloseButton: true,
                //     timeout: 2000
                // };
                // this.toasterService.pop(thumbUpload);
                this.presentToast('error', '');
                this.spinner.hide();
              }
              else{
                this.assetFileData = null;
                if(this.fileUploadRes['data'] != null || this.fileUploadRes['fileError'] != true){
                  this.passParams['assetsRef'] = this.fileUploadRes['data']['file_url'];
                  this.fileres =  this.fileUploadRes['data']['file_url'];
                  this.passParams['assetKey'] = this.fileUploadRes['data']['filename'];
                  this.passParams['assetSize'] = this.fileUploadRes['data']['size'];
                  console.log('this.passparams.assetsRef',this.passParams['assetsRef']);
                  this.spinner.hide();
                  this.addAssetVersion(this.passParams);
                }else{
                  // var thumbUpload : Toast = {
                  //     type: 'error',
                  //     title: "Asset File",
                  //     // body: "Unable to upload course thumbnail.",
                  //     body: this.fileUploadRes.status,
                  //     showCloseButton: true,
                  //     timeout: 2000
                  // };
                  // this.toasterService.pop(thumbUpload);
                  this.presentToast('error', '');
                  this.spinner.hide();
                }
              }
              console.log('File Upload Result',this.fileUploadRes);
              // var res = this.fileUploadRes;

            },
            resUserError=>{
              this.errorMsg = resUserError;
              console.log('File upload this.errorMsg', this.errorMsg);
              this.spinner.hide();
            });
          }else {
            this.presentToast('warning','File not present');
          }

    }else {
      let param = {
        "assetsId": data.assetId,
        "assetsRef": data.assetRef,
        "assetsBitly":null,
        "assetsDesc": data.description,
        "assetsLen": data.time?this.formatTime(data.time):null,
        'assetsStatus': null,
        "assetsCost": data.estimateCost,
        "tId": this.tenantId,
        "instanceName": data.assetName,
        "parentCatName" : this.parentCatName,
        "userId": this.userId,
        "approverId":  this.isShowApprover?this.selectedCreator[0].appId:null,
        "appReq": this.addEditAssetForm.appReq,
        "assetComment": data.comment,
        //"author":data.selectedEmployee[0].edgeempId
      }
      this.passParams = param;
      let mimeType = null;
      let referenceType = null;
      if (String(data.assetRef).includes('kapsule') || String(data.assetRef).includes('kpoint')) {
        mimeType = 'embedded/kpoint';
        referenceType = 'kpoint';
      } else if (String(data.assetRef).includes('youtube')) {
        mimeType = 'embedded/youtube';
        referenceType = 'youtube';
      } else{
        mimeType = 'application/x-msdownload';
        referenceType = 'application';
      }
      this.passParams['mimeType'] = mimeType;
      this.passParams['referenceType'] = referenceType;
      console.log('this.passparams.assetsRef',this.passParams.assetsRef);
      this.addAssetVersion(this.passParams);
    }
        console.log('selectedAssetFormatData',this.selectedAssetFormatData);
      }else{
        this.presentToast('warning','Please fill all fields');
      }
  }


  openDeepyNestedFolder(event){
    this.breadcrumbArray = [
      {
      'name': 'DAM',
      'navigationPath': '/pages/dam',
    },{
      'name': 'Folder ',
      'navigationPath': '/pages/dam/folders',
      'id':{},
      'sameComp':true,
      'assetId':null
    }
  ];
    this.previousBreadCrumb = [
      {
        'name': 'DAM',
        'navigationPath': '/pages/dam',
      }, {
        'name': 'Folder ',
        'navigationPath': '/pages/dam/folders',
        'id': {},
        'sameComp': true,
        'assetId': null
      }
    ]
    var dummy = {}
    // var array  = this.assetservice.breadcrumbArray
    var array  = event
    var length =  array.length-1
    // this.title = array[length].categoryName
    for(let i = 0; i<array.length;i++){
      dummy = {
        'name': array[i].categoryName,
        'id':array[i],
        'navigationPath': '/pages/dam/folders',
        'sameComp':true,
        'assetId' : array[i].id,
      }
      this.previousBreadCrumb.push(dummy)
      this.breadcrumbArray.push(dummy)
    }
    console.log(this.breadcrumbArray,"vgvvtfvftftfggarravfvffARRAY")
    var length =  this.breadcrumbArray.length-1
    this.header.title = this.breadcrumbArray[length].name
    this.title = this.header.title
    console.log(this.title,"TITLE")
    for(let i= 0;i<this.previousBreadCrumb.length;i++){
      if(this.title == this.previousBreadCrumb[i].name){
        this.breadcrumbArray = this.previousBreadCrumb.slice(0,i)
      }
      }
    this.header.breadCrumbList = this.breadcrumbArray

  }
  breadcrumbNavigate(data){
    console.log(data,"navigationData")
    this.view=false;
    if(data.categoryName){
      // this.openDeepyNestedFolder(data.id)
      this.title = data.categoryName;
      this.parentCatId = data.id;
      this.parentCatName = data.categoryName;
    }else{
    this.title = data.assetName;
    this.parentCatId = data.assetId;
    this.parentCatName = data.assetName;
    }
    if(!this.parentCatName) this.parentCatName = null;
    if(!this.parentCatId){
      this.parentCatId = null
    }
    this.breadcrumbArray =  this.breadcrumbArray.slice(0,data.index)
    this.previousBreadCrumb = this.previousBreadCrumb.slice(0,data.index+1)
    console.log(this.breadcrumbArray,"breadcrumbArray")
    console.log(this.previousBreadCrumb,"previousBreadCrumb")


    // this.header.breadCrumbList =  this.breadcrumbArray
    // this.parentCatId = this.oldParentCatId
    this.valueChanged = this.parentCatId
    this.selectedFolder.assetId = this.parentCatId

    this.getFileAndFolders(this.parentCatId,this.roleId,this.tabId,this.innerTabId);

  }


  addAssetVersion(passParam){
    console.log('passParam',passParam);
    this.spinner.show();
    const _urlInsertAssetVersion: string = webApi.domain + webApi.url.insertAssetVersion;
    this.commonFunctionService.httpPostRequest(_urlInsertAssetVersion,passParam)
    // this.assetVesrionService.insertAssetVersion(passParam)
    .then(rescompData => {

      this.spinner.hide();

      var temp = rescompData;
      var result = temp['data'];
      console.log('Add Result ',result)
        if(temp['type'] == true){

                //   var catUpdate : Toast = {
                //     type: 'success',
                //     title: "Asset Version Inserted!",
                //     body: "New asset version added successfully.",
                //     showCloseButton: true,
                //     timeout: 2000
                // };
                // this.toasterService.pop(catUpdate);
              //   var length = this.addassetservice.breadCrumbArray.length-1
              //   this.title = this.addassetservice.breadCrumbArray[length].name
              //   this.addassetservice.title = this.title
              //   this.addassetservice.breadCrumbArray = this.addassetservice.breadCrumbArray.slice(0,length)
              // // this.title = this.addassetservice.breadCrumbArray[]
              // this.addassetservice.previousBreadCrumb = this.addassetservice.previousBreadCrumb.slice(0,length+1)
                console.log(this.title,"title")
                this.fetchDAMStorageDetails();
                this.getFileAndFolders(this.parentCatId,this.roleId,this.tabId,this.innerTabId);
            // this.closeDetail()
              this.presentToast('success', 'New File version added');
             this.dummy="Edit";
            this.activeTab=this.detailsTab[1];
            this.showFile=true;
            this.getAssetVersions(passParam.assetsId);
          // this.router.navigate(['../'],{relativeTo:this.routes});
            if(passParam['appReq'] == 1){
              this.closeDetail();
            }

            }else{
              //this.fetchBadgeCateory();
              //   var catUpdate : Toast = {
              //     type: 'error',
              //     title: "Asset Version Inserted!",
              //     body: "Unable to add asset version.",
              //     showCloseButton: true,
              //     timeout: 2000
              // };
              // this.toasterService.pop(catUpdate);
              this.closeDetail()
              this.presentToast('error', '');
            }

    },
    resUserError=>{
      this.spinner.hide();
      this.errorMsg = resUserError;
    });
  }

  onEmployeeSelect(item: any) {
    console.log(item);
    console.log('this.allEmployeeEdgeAuto', this.allEmployeeEdgeAuto);
    if(item.id == 0){
      this.settingsEmployeeDrop = {
        text: 'Search & Select Author',
        singleSelection: false,
        classes: 'common-multi',
        primaryKey: 'id',
        labelKey: 'name',
        noDataLabel: 'Search Author...',
        limitSelection:1,
        enableSearchFilter: true,
        // searchBy: ['code', 'name'],
        badgeShowLimit:3,
        maxHeight:250,
        lazyLoading: true,
        addNewItemOnFilter: false,
      };
      var dummy = []
      dummy.push(item)
      this.addEditAssetForm.selectedEmployee = dummy
      this.categoryForm.selectedEmployee = dummy
      this.fileFolderForm.selectedEmployee = dummy
      this.selectedEmployee = dummy
    }
    if(item.isLoggedInUser == 1){
      console.log(this.selectedEmployee,"selectedEmployee")
      this.addEditAssetForm.selectedEmployee.pop()
      this.selectedEmployee.pop()
      this.presentToast('warning','This Author cannot be selected for this content')
      };
    console.log(this.selectedEmployee);
  }

  OnEmployeeDeSelect(item: any) {
    console.log(item);
    if(item.id == 0){
      this.settingsEmployeeDrop = {
        text: 'Search & Select Author',
        singleSelection: false,
        classes: 'common-multi',
        primaryKey: 'id',
        labelKey: 'name',
        noDataLabel: 'Search Author...',
        enableSearchFilter: true,
        limitSelection:this.allEmployeeEdgeAuto.length,
        // searchBy: ['code', 'name'],
        maxHeight:250,
        badgeShowLimit:3,
        lazyLoading: true,
        addNewItemOnFilter: false,
      };
    }
    console.log(this.selectedEmployee);
    console.log('this.allEmployeeEdgeAuto', this.allEmployeeEdgeAuto);
  }
  allEmployeeEdgeAutoDrop = [];
  onEmployeeSearch(evt: any) {
    console.log(evt.target.value);
    const val = evt.target.value;
    this.allEmployeeEdgeAutoDrop = [];
    this.getAllEdgeEmployee(val);
    // this.empList = [];
    // const tuser = this.allEmployeeEdgeAutoDrop.filter(function (d) {
    //   return String(d.edgeempId).toLowerCase().indexOf(val) !== -1 ||
    //     d.edgeempName.toLowerCase().indexOf(val) !== -1 || !val;
    // });

    // update the rows
    //this.empList = tuser;
  }
  fileExistModalSubmit(value) {
    if (value === 1) {
      this.passParams.assetsRef = this.fileUploadRes.data.docs[0].file_url;
      console.log('this.passParams.assetRef', this.passParams.assetRef);
      this.addEditAsset(this.passParams);
    } else {
      this.passParams.assetRef = null;
      this.addEditAssetForm.assetRef = null;
      this.isLocalfile = false;
      this.fileName = 'Select a file';
      this.assetFileData = null;
    }
    this.fileExistModal = false;
   }

   deleteFileFolderForever(data) {
    console.log('AssetPushData:', data);
    if(data && data.caFlag == 2){
      this.assignValueToSelectDropDown(data.formatId);
    }
    let params = {
      'fileType': data.caFlag,
      'categoryOrAssetId': data.assetId,
      'deleteFromS3bucket':  this.selectedAssetFormatData.pickerType == 1 ? true : false,
      'instanceName':data.assetName,
      'parentCatId' : this.parentCatId,
      'parentCatName' :this.parentCatName
    };
    this.spinner.show();
    const _urldeleteForever: string = webApi.domain + webApi.url.deleteForever;
    this.commonFunctionService.httpPostRequest(_urldeleteForever,params)
    .then(
      result => {
        this.spinner.hide();
        console.log('Delete Forever', result);
        if (result['type'] == true) {
          this.presentToast('success', result['message']);
          // this.enableDisableCategoryModal = false;
          this.setSelectedValueDefault();
          this.fetchDAMStorageDetails();
          this.getFileAndFolders(this.parentCatId,this.roleId,this.tabId,this.innerTabId);
          // const param= {
          //   tabId:this.tabId,
          //   roleId:this.roleId,

          // }
          // this.getTree(param);
        } else {
          this.presentToast('error', '');
        }
      },
      resUserError => {
        this.spinner.hide();
        // this.errorMsg = resUserError;
      }
    );
  }


  // Share asset
    showShareAsset = false;

    toggelShareAsset(flag){
      this.showShareAsset = flag;

    }
    // moveFile

    showMoveAsset = false;
    toggelMoveAssets(flag){
      this.showMoveAsset = flag;
      if(!flag){
        this.moveToFolderData = null;
        this.treeMoves = [];
        const param= {
          tabId:this.tabId,
          roleId:this.roleId,
        }
        this.getTree(param, false);
      }
    }

    getMoveAssetTree(item, moveFlag){
      var demo = [];
      let param = {};
      param = {
        userId:this.clickedAssetData ? this.clickedAssetData.userCreated:null,
        iId:this.clickedAssetData.assetId,
        fType:this.clickedAssetData.caFlag
      }
      console.log(this.clickedAssetData,"clickedAssetDAta")
      // if(moveFlag){
      //   param = {
      //     flag: 2,
      //     tabId: 1,
      //     roleId: 12,
      //     userId: this.clickedAssetData.userCreated,
      //     moveFlag: moveFlag,
      //   }
      // }else {
      //  param = {
      //     flag:2,
      //     tabId:item.tabId,
      //     roleId:item.roleId,
      //     userId: this.clickedAssetData ? this.clickedAssetData.userCreated:null,
      //     moveFlag: moveFlag,
      //   }
      // }
      if(moveFlag){
        this.spinner.show();
      }
      const _urlgetBatch:string = webApi.domain + webApi.url.move_asset_tree;
      this.commonFunctionService.httpPostRequest(_urlgetBatch,param).then(res=>{
      // this.assetservice.getAllTree(param).then(res=>{
        console.log(res,"tree")
        if(moveFlag){
          this.treeMoves = res['data'];
          if(this.treeMoves){
            for(let i = 0;i<this.treeMoves.length;i++){

              if(this.treeMoves[i].parentCatId == null){
                demo.push(this.treeMoves[i])
              }
            }
          }
          if(this.treeMoves.length === 0){
            this.noTressMoves = true;
          }else {
            this.noTressMoves = false;
          }
          this.treeMoves = demo;
          this.assetservice.tree = res['data'];
          // if(moveFlag){
            this.spinner.hide();
          // }
        }else {
          this.tree = res['data'];
          if(this.tree){
            for(let i = 0;i<this.tree.length;i++){

              if(this.tree[i].parentCatId == null){
                demo.push(this.tree[i])
              }
              }
          }

          this.tree = demo
          this.assetservice.tabActive = item.isActive
          this.assetservice.tree = res['data']
        }


  })
    }



    moveToFolderData = null;
    performActionOnData(event) {
      console.log("Event ==>", event);
      if (event) {
        switch (event.action) {
          // Tab Events
          case "moveFileToFolder":
            // code...
           this.moveToFolderData = event.data[0];
            // this.moveFileFolder(event.data[0]);
            break;
        }
      }
    }

    moveFileFolder(){
      console.log('AssetPushData:', this.moveToFolderData);
      if(!this.moveToFolderData){
        this.toastr.warning('Please select a folder to move content', 'Warning');
        return null;
      }
      let params = {
        'caFlag': this.clickedAssetData.caFlag,
        'iId': this.clickedAssetData.assetId,
        'fId':  this.moveToFolderData.id,
        'parentCatId' : this.parentCatId,
        'sourceFolderName' :this.parentCatName,
        'destFolderName' : this.moveToFolderData.categoryName,
        'instaceName':this.clickedAssetData.assetName,
      };
      this.spinner.show();
      const _urlMoveFileFolder: string = webApi.domain + webApi.url.moveDAMAsset;
      this.commonFunctionService.httpPostRequest(_urlMoveFileFolder,params)
      .then(
        result => {
          this.spinner.hide();
          console.log('Move File', result);
          if (result['type'] == true) {
            this.presentToast(result['action'], result['data']);
            // this.enableDisableCategoryModal = false;
            this.view=false;
             this.toggelMoveAssets(false);
             const param= {
              tabId:this.tabId,
              roleId:this.roleId,
            }
            this.getTree(param, false);
            this.getFileAndFolders(this.parentCatId,this.roleId,this.tabId,this.innerTabId);
            // const param= {
            //   tabId:this.tabId,
            //   roleId:this.roleId,

            // }
            // this.getTree(param);
          } else {
            this.presentToast('error', '');
          }
        },
        resUserError => {
          this.spinner.hide();
          // this.errorMsg = resUserError;
        }
      );
    }

    damStorageDetails = {
      'totalSize': 'NA',
      'usedSize': 'NA',
      'unUsedSize': 'NA',
      'usedPercentage': 'NA',
    };
    fetchDAMStorageDetails() {

      let params = {
      };
      // this.spinner.show();
      const _urlgetDAMStorageDetails: string = webApi.domain + webApi.url.getDAMStorageDetails;
      this.commonFunctionService.httpPostRequest(_urlgetDAMStorageDetails,params)
      .then(
        result => {
          // this.spinner.hide();
          console.log('DAM Storage', result);
          if (result['type'] == true) {
            if (result['data'] && result['data'].length != 0 && result['data'][0] && result['data'][0].length != 0) {
              // this.damStorageDetails['']
              this.damStorageDetails = result['data'][0][0];
              // this.damStorageDetails['totalSize'] = result['data'][0] ?  result['data'][0][0]['totalSize'] : 'NA';
              // this.damStorageDetails['usedSize'] = result['data'][1] ? result['data'][1][0]['usedSize'] : 'NA';
              // this.damStorageDetails['remSize'] = result['data'][2] ? result['data'][2][0]['remSize'] : 'NA';
              // this.damStorageDetails['perFull'] = result['data'][3] ? result['data'][3][0]['perFull']: 'NA';
            }
          } else {
            // this.presentToast('error', '');
          }
        },
        resUserError => {
          // this.spinner.hide();
          // this.errorMsg = resUserError;
        }
      );
    }



    closeFileExistModal(){
        this.fileExistModal = false;
    }

    gotocardAprrove(assetData) {
      console.log('assetData', assetData);
      this.approveAssetService.getAssetDataForApproval = assetData;
      /*breadcrum code start*/
      this.title = assetData.assetName
      console.log('assetData', assetData);
      //// breadCrumb
      this.breadObj = {
        'name': assetData.assetName,
        // 'navigationPath': '',
        'id':assetData,
        'navigationPath': '/pages/dam/assets',
        'sameComp':true,
        'assetId' : assetData.assetId
      }

      this.previousBreadCrumb.push(this.breadObj)

      this.breadcrumbArray.push(this.breadObj)
      for(let i= 0;i<this.previousBreadCrumb.length;i++){
        if(this.title == this.previousBreadCrumb[i].name){
          this.breadcrumbArray = this.previousBreadCrumb.slice(0,i)
        }
      }
      /*breadcrum code end*/
      this.addassetservice.title = this.title
      this.addassetservice.breadCrumbArray = this.breadcrumbArray
      this.addassetservice.previousBreadCrumb = this.previousBreadCrumb
      this.addassetservice.categoryId = assetData.categoryId
      this.addassetservice.roleId = this.roleId
      this.addassetservice.tabId = this.tabId
      this.addassetservice.innerTabId = this.innerTabId
      this.addassetservice.valuechanged = this.valueChanged
      this.router.navigate(['../asset/approve-asset'], { relativeTo: this.routes });
      this.view=false;
    }
    gotoReviewAsset(assetData) {
      this.assetReviewPolicyService.getAssetDataForReview = assetData;
      this.title = assetData.assetName
      this.breadObj = {
        'name': assetData.assetName,
        // 'navigationPath': '',
        'id':assetData,
        'navigationPath': '/pages/dam/assets',
        'sameComp':true,
        'assetId' : assetData.assetId
      }

      this.previousBreadCrumb.push(this.breadObj)

      this.breadcrumbArray.push(this.breadObj)
      for(let i= 0;i<this.previousBreadCrumb.length;i++){
        if(this.title == this.previousBreadCrumb[i].name){
          this.breadcrumbArray = this.previousBreadCrumb.slice(0,i)
        }
      }
      this.view=false;
      console.log('assetData', assetData);
      this.addassetservice.title = this.title
      this.addassetservice.breadCrumbArray = this.breadcrumbArray
      this.addassetservice.previousBreadCrumb = this.previousBreadCrumb
      this.addassetservice.categoryId = assetData.categoryId
      this.addassetservice.roleId = this.roleId
      this.addassetservice.tabId = this.tabId
      this.addassetservice.innerTabId = this.innerTabId
      this.router.navigate(['../asset/asset-review-policy'], { relativeTo: this.routes });
    }

  gotoApproveAsset() {
    //this.router.navigate(['approve-asset'],{relativeTo:this.routes});
    this.assetservice.getPendingAssets = true;
    this.assetservice.getApprovedAssets = false;
    this.addassetservice.title = this.title;
    this.addassetservice.breadCrumbArray = this.breadcrumbArray;
    this.addassetservice.previousBreadCrumb = this.previousBreadCrumb;
    // this.addassetservice.categoryId = this.parentCatId;
    this.addassetservice.categoryId = null;
    // this.addassetservice.roleId = this.roleId;
    this.addassetservice.roleId = this.tabs[0].roleId;
    // this.addassetservice.tabId = this.tabId;
    this.addassetservice.tabId = this.tabs[0].tabId;
    // this.addassetservice.innerTabId = this.innerTabId;
    this.addassetservice.innerTabId = null;
    this.addassetservice.valuechanged = this.valueChanged;
    this.router.navigate(['../asset'], { relativeTo: this.routes });
    this.view = false;
  }

    checkIfCondition(index){
      if(index == 0){
        return true
      }
      if(this.allFolderCombined[index]['caFlag'] != this.allFolderCombined[index - 1]['caFlag'] ) {
        return true
      }
      return false
    }

    checkIfTrue(item){
      return item.isHoverDesc == 1 && item.description
    }


    updateDropDownSetting(list){
      if(list && String(list).length != 0){
        const author = list.split(',');
      //   author.forEach((auth) =>{

      // }
      let setting = null;
      for (let index = 0; index < author.length; index++) {
        const element = author[index];
        if (element == 0) {
          setting = {
            text: 'Search & Select Author',
            singleSelection: false,
            classes: 'common-multi',
            primaryKey: 'id',
            labelKey: 'name',
            noDataLabel: 'Search Author...',
            limitSelection:1,
            enableSearchFilter: true,
            // searchBy: ['code', 'name'],
            badgeShowLimit:3,
            maxHeight:250,
            lazyLoading: true,
            addNewItemOnFilter: false,
          };
          break;
        }else {
          setting = {
            text: 'Search & Select Author',
            singleSelection: false,
            classes: 'common-multi',
            primaryKey: 'id',
            labelKey: 'name',
            noDataLabel: 'Search Author...',
            enableSearchFilter: true,
            // searchBy: ['code', 'name'],
            badgeShowLimit:3,
            maxHeight:250,
            lazyLoading: true,
            addNewItemOnFilter: false,
            // limitSelection: this.allEmployeeEdgeAuto.length,
            limitSelection: null,
          }
          break;
        }

      }
      this.settingsEmployeeDrop = _.cloneDeepWith(setting);
    }
    }

    setEmployeeSettingToDefault(){
     const setting = {
        text: 'Search & Select Author',
        singleSelection: false,
        classes: 'common-multi',
        primaryKey: 'id',
        labelKey: 'name',
        noDataLabel: 'Search Author...',
        enableSearchFilter: true,
        // searchBy: ['code', 'name'],
        badgeShowLimit:3,
        maxHeight:250,
        lazyLoading: true,
        addNewItemOnFilter: false,
        // limitSelection: this.allEmployeeEdgeAuto.length,
        limitSelection: null,
      }
      this.settingsEmployeeDrop = _.cloneDeepWith(setting);
    }
  }
