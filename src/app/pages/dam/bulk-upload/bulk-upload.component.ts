import { Component, OnInit, ViewChild } from '@angular/core';
// import { ToasterService, Toast } from 'angular2-toaster';
import { NgxSpinnerService } from 'ngx-spinner';
import { webAPIService } from '../../../service/webAPIService';
// import { webApi } from '../../../service/webApi';
import { DatePipe } from '@angular/common';
import { BulkUploadService } from './bulk-upload.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AddAssetService } from '../asset/add-asset/add-asset.service';
import { XlsxToJsonService } from '../../plan/users/uploadusers/xlsx-to-json-service';
import { JsonToXlsxService } from './json-to-xlsx.service';
import { ToastrService } from 'ngx-toastr';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { webApi } from '../../../service/webApi';
import { SuubHeader } from '../../components/models/subheader.model';
// var mime = require('mime-types');
// import * as mime from 'mime-types';
declare var mime;

@Component({
  selector: 'ngx-bulk-upload',
  templateUrl: './bulk-upload.component.html',
  styleUrls: ['./bulk-upload.component.scss']
})
export class BulkUploadComponent implements OnInit {
  @ViewChild('myTable') table: any;
  private xlsxToJsonService: XlsxToJsonService = new XlsxToJsonService();
  rows = [];
  selected = [];
  templateExelLink: any = 'assets/images/Asset_Upload_Template.xlsx';
  /*
    uploadDetailsOne: any = [
      { assetName: "B2B sales", date: '12/04/2019', category: 'Abc', format: '.xls', channel: 'akdf', language: 'english' },
      { assetName: "B2B sales", date: '12/04/2019', category: 'Abc', format: '.xls', channel: 'akdf', language: 'english' },
      { assetName: "B2B sales", date: '12/04/2019', category: 'Abc', format: '.xls', channel: 'akdf', language: 'english' },
      { assetName: "B2B sales", date: '12/04/2019', category: 'Abc', format: '.xls', channel: 'akdf', language: 'english' },
      { assetName: "B2B sales", date: '12/04/2019', category: 'Abc', format: '.xls', channel: 'akdf', language: 'english' },
      { assetName: "B2B sales", date: '12/04/2019', category: 'Abc', format: '.xls', channel: 'akdf', language: 'english' },
      { assetName: "B2B sales", date: '12/04/2019', category: 'Abc', format: '.xls', channel: 'akdf', language: 'english' },
    ]
    */
  userId: any;
  AllEmployessDAM: any = [];
  AllEdgeEmployessDAM: any = [];
  assetDropDownData: any = [];
  resultSheets: any = [];
  result: any = [];
  uploadedData: any = [];
  damApproverList: any = [];
  lang: any = [];
  damFileFormat: any = [];
  damFileChannel: any = [];
  cat: any = [];
  validdata: any = [];
  invaliddata: any = [];
  tenantId: any = 1;
  dropDownData: any = {};
  dateFormatNew: any = new RegExp('^[0-9]{4}[\-][0-9]{2}[\-][0-9]{2}$');
  labels: any = [
		{ labelname: 'NAME', bindingProperty: 'name', componentType: 'text' },
		{ labelname: 'CATEGORY', bindingProperty: 'category', componentType: 'text' },
		{ labelname: 'FORMAT', bindingProperty: 'format', componentType: 'text' },
		{ labelname: 'CHANNEL', bindingProperty: 'channel', componentType: 'text' },
		{ labelname: 'LANGUAGES', bindingProperty: 'language', componentType: 'text' },
		{ labelname: 'VALID', bindingProperty: 'valid', componentType: 'text' },
    ];
    header: SuubHeader  = {
      title:'Bulk Upload',
      btnsSearch: true,
      searchBar: false,
      dropdownlabel: ' ',
      drplabelshow: false,
      drpName1: '',
      drpName2: ' ',
      drpName3: '',
      drpName1show: false,
      drpName2show: false,
      drpName3show: false,
      btnName1: '',
      btnName2: '',
      btnName3: '',
      btnAdd: 'File Upload',
      btnName1show: false,
      btnName2show: false,
      btnName3show: false,
      btnBackshow: true,
      btnAddshow: false,
      filter: false,
      showBreadcrumb: true,
      breadCrumbList:[
    {
      'name': 'DAM',
      'navigationPath': '/pages/dam',
    },]
    };
  constructor(
    // private toasterService: ToasterService,
    private spinner: NgxSpinnerService, public router: Router,
    public routes: ActivatedRoute, protected webApiService: webAPIService, private datePipe: DatePipe,
    private bulkUploadService: BulkUploadService, private addassetservice: AddAssetService,
    private exportService:JsonToXlsxService, private toastr: ToastrService,private commonFunctionService: CommonFunctionsService,) {
    // this.rows = this.uploadDetailsOne;
    if (localStorage.getItem('LoginResData')) {
      const userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', userData.data);
      this.userId = userData.data.data.id;
      console.log('userId', userData.data.data.id);
      this.tenantId = userData.data.data.tenantId;
    }

    this.getAllEmployeesDAM();
    this.getAllEdgeEmployee();
    this.getAllAssetDropdown();
  }

  ngOnInit() {
  }

  // excelSheet : any;
  // excelSheetFile : any;
  // filechnage(event){
  //   if (event.target.files && event.target.files[0]) {
  //     var reader = new FileReader();
  //     //this.locations.file.push(event.target.files[0]);
  //     this.excelSheetFile = event.target.files[0];
  //       reader.onload = (event: ProgressEvent) => {
  //         this.excelSheet = (<FileReader>event.target).result;
  //       }
  //       reader.readAsDataURL(event.target.files[0]);
  //         // this.homecategory.img = event.target.files[0];
  //         console.log('excelSheet',this.excelSheet);
  //         console.log('excelSheetFile',this.excelSheetFile);
  //   }

  // }

  bulkUploadAssetData: any = null;
  fileReaded: any;
  enableUpload: boolean = false;
  fileUrl: any;


  fileUpload: any;
  // fileName: any;
  // selectFileTitle: any;
  // fileUrl: any;
  fileName: String = 'Click here to upload an excel file in DAM';
  fileIcon: any = 'assets/img/app/profile/avatar4.png';
  // fileReaded: any;
  // enableUpload: any = false;
  selectFileTitle: any = 'No file chosen';
  cancelFile() {
    // this.fileUpload.nativeElement.files = [];
    this.bulkUploadAssetData = null;
    // console.log(this.fileUpload.nativeElement.files);
    if (this.fileUpload) {
      if (this.fileUpload.nativeElement) {
        this.fileUpload.nativeElement.value = null;
      }
    }

    // console.log(this.fileUpload.nativeElement.files);
    this.fileName = 'Click here to upload an excel file in DAM';
    this.selectFileTitle = 'No file chosen';
    // this.enableUpload = false;
  }

  passParams: any;
  assetFileData: any;
  fileUploadRes: any;
  fileres: any;
  errorMsg: any;

  uploadedFile: any;

  formatDate(date) {
    const d = new Date(date);
    const formatted = this.datePipe.transform(d, 'dd-MMM-yyyy');
    return formatted;
  }

  presentToast(type, body) {
    if(type === 'success'){
    this.toastr.success(body, 'Success', {
    closeButton: false
    });
    } else if(type === 'error'){
    this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
    timeOut: 0,
    closeButton: true
    });
    }else if(type === 'info'){
      this.toastr.info(body, 'Success', {
      closeButton: false
      });
    }else{
    this.toastr.warning(body, 'Warning', {
    closeButton: false
    })
    }
    }

  //googleLink : any = 'https://bhaveshedgetest.s3.ap-south-1.amazonaws.com/train-the-trainer41561805467169.png';

  submit() {
    this.spinner.show()
    const param = {
      'tId': this.tenantId,
      'userId': this.userId,
    };
    this.bulkUploadService.insertAssetBulkUpload(param)
      .then(rescompData => {
        this.spinner.hide();
        const result = rescompData;
        console.log('Bulk Upload assets:', rescompData);
        if (result['type'] == true) {
          // const toast: Toast = {
          //   type: 'success',
          //   title: 'Bulk Upload.',
          //   body: 'Assets uploaded successfully.',
          //   showCloseButton: true,
          //   timeout: 2000,
          // };
          this.spinner.hide();
          // this.toasterService.pop(toast);
          this.presentToast('success', 'Assets uploaded');
        } else {
          // var toast: Toast = {
          //   type: 'error',
          //   title: 'Bulk Upload.',
          //   body: 'Unable to upload assets.',
          //   showCloseButton: true,
          //   timeout: 2000,
          // };
          this.spinner.hide();
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }

      }, error => {
        this.spinner.hide();
        // var toast: Toast = {
        //   type: 'error',
        //   //title: "Server Error!",
        //   body: 'Something went wrong.please try again later.',
        //   showCloseButton: true,
        //   timeout: 2000,
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
  }

  back() {
    this.router.navigate(['../../dam'], { relativeTo: this.routes });
  }

  getAllEmployeesDAM() {
    const param = {
      'tId': this.tenantId,
    };
    const _urlGetAllEmployees: string = webApi.domain + webApi.url.getAllEmployeeDAM;
    this.commonFunctionService.httpPostRequest(_urlGetAllEmployees,param)
    // this.addassetservice.getAllEmployee(param)
      .then(rescompData => {
        this.AllEmployessDAM = rescompData['data'][0];
        console.log('AllEmployessDAM:', this.AllEmployessDAM);
      });
  }

  folderUpload(){
    this.bulkUploadService.parCatId = null
    this.router.navigate(['file-upload'], { relativeTo: this.routes });
    // this.router.navigate(['dam/bulk-upload/file-upload'])
    // this.router.navigate(['/pages/plan/courses/addEditCourseContent']);
  }
  getAllEdgeEmployee() {
    const param = {
      'srcStr': null,
      'tId': this.tenantId,
    };
     const _urlGetAllEdgeEmployee: string = webApi.domain + webApi.url.getAllEdgeEmployee;
     this.commonFunctionService.httpPostRequest(_urlGetAllEdgeEmployee,param)
    // this.addassetservice.getAllEdgeEmployee(param)
    .then(res => {
      this.AllEdgeEmployessDAM = res['data'][0];
      console.log('AllEdgeEmployessDAM:', this.AllEdgeEmployessDAM);
    });
  }
  getAllAssetDropdown() {
    const param = {
      'tId': this.tenantId,
    };
    const _urlGetAllDropdown: string = webApi.domain + webApi.url.getAllAssetDropDown;
    this.commonFunctionService.httpPostRequest(_urlGetAllDropdown,param)
    // this.addassetservice.getAllAssetDropdown(param)
    .then(res => {
      this.damApproverList = res['data'][0];
      this.lang = res['data'][1];
      this.damFileFormat = res['data'][2];
      this.damFileChannel = res['data'][3];
      this.cat = res['data'][4];

      // this.dropDownData['ApproverList'] = res['data'][0];
      // this.dropDownData['Language'] = res['data'][1];
      // this.dropDownData['Format'] = res['data'][2];
      // this.dropDownData['Channel'] = res['data'][3];
      // this.dropDownData['Category'] = res['data'][4];

      // res['data'].forEach(assetData => {
        //   // Object.assign(this.dropDownData, { [assetData[0].statusName] : [''] });
        //   assetData
        // });
        console.log('dropDownData ---->', res['data']);
        // let Obj = {};
        // let temp = {};
        // for(const data of res['data'][0]){
        //   Object.assign(temp, { [data.statusName] : true });
        // }
        // Object.assign(this.dropDownData, { ['statusName'] : temp });
        // temp = {};
        // for(const data of res['data'][1]){
        //   Object.assign(temp, { [data.languageName] : true });
        // }
        // Object.assign(this.dropDownData, { ['languageName'] : temp });
        // temp = {};
        // for(const data of res['data'][2]){
        //   Object.assign(temp, { [data.formatName] : true });
        // }
        // Object.assign(this.dropDownData, { ['formatName'] : temp });
        // temp = {};
        // for(const data of res['data'][3]){
        //   Object.assign(temp, { [data.channelName] : true });
        // }
        // Object.assign(this.dropDownData, { ['channelName'] : temp });
        // temp = {};
        // for(const data of res['data'][4]){
        //   Object.assign(temp, { [data.catName] : true });
        // }
        // Object.assign(this.dropDownData, { ['catName'] : temp });
    });

    // console.log('dropDownData ---->', this.dropDownData);

  }



  readFileUrl(event: any) {
    const validExts = new Array('.xlsx', '.xls');
    let fileExt = event.target.files[0].name;
    this.uploadedData = [];
    this.rows = [];
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
      // var toast: Toast = {
      //   type: 'error',
      //   title: 'Invalid file selected!',
      //   body: 'Valid files are of ' + validExts.toString() + 'types.',
      //   showCloseButton: true,
      //   // tapToDismiss: false,
      //   timeout: 2000,
      // };
      // this.toasterService.pop(toast);
      this.presentToast('warning', 'Valid file types are ' + validExts.toString());
      this.cancelFile();
    } else {
      // return true;
      if (event.target.files && event.target.files[0]) {
        this.fileName = event.target.files[0].name;
        this.selectFileTitle = event.target.files[0].name;
        // read file from input
        this.fileReaded = event.target.files[0];

        if (this.fileReaded != '' || this.fileReaded != null || this.fileReaded != undefined) {
          this.enableUpload = true;
        }

        let file = event.target.files[0];
        this.bulkUploadAssetData = event.target.files[0];
        console.log('this.bulkUploadAssetDataFileRead',this.bulkUploadAssetData);

        this.xlsxToJsonService.processFileToJson({}, file).subscribe(data => {
          this.resultSheets = Object.getOwnPropertyNames(data['sheets']);
          console.log('File Property Names ', this.resultSheets);
          let sheetName = this.resultSheets[0];
          this.result = data['sheets'][sheetName];
          console.log('dataSheet', data);

          if (this.result.length > 0) {
            this.uploadedData = this.result;
          }
        });
        var reader = new FileReader();
        reader.onload = (event: ProgressEvent) => {
          this.fileUrl = (<FileReader>event.target).result;
          /// console.log(this.fileUrl);
        };
        reader.readAsDataURL(event.target.files[0]);
      }
    }
  }
  uploadSheet() {
    this.spinner.show()
    let t0 = performance.now();
    this.validdata = [];
    this.invaliddata = [];
    this.showInvalidExcel = false;
    // this.spinner.show();
    if (this.uploadedData.length) {
      console.log(this.uploadedData);
      this.rows = [];
      for (let i = 0; i < this.uploadedData.length; i++) {
        this.uploadedData[i].valid = 'Valid';
        this.uploadedData[i].reason = '';
        if (this.uploadedData[i].name == '' || this.uploadedData[i].name == null ||
          this.uploadedData[i].name == undefined) {
          this.uploadedData[i].valid = 'Invalid';
          this.uploadedData[i].reason = 'Assets Name required';
        } else {
          // this.uploadedData[i].valid = 'Valid';
            if (this.uploadedData[i].description.length < 20 || this.uploadedData[i].description.length > 500) {
              this.uploadedData[i].valid = 'Invalid';
              this.uploadedData[i].reason = 'Description length should be between 20 to 500';
            } else {
              // this.uploadedData[i].valid = 'Valid';
              if (!this.uploadedData[i].category) {
                this.uploadedData[i].valid = 'Invalid';
                this.uploadedData[i].reason = 'Category required';
              } else {
                for (let j = 0; j < this.cat.length; j++) {
                  if (this.uploadedData[i].category == this.cat[j].catName) {
                    //console.log("category", i, this.uploadedData[i].category == this.cat[j].catName);
                    this.uploadedData[i].valid = 'Valid';
                    this.uploadedData[i].reason = '';
                    // break;
                    if (!this.uploadedData[i].channel) {
                      this.uploadedData[i].valid = 'Invalid';
                      this.uploadedData[i].reason = 'Channel Name required';
                    } else
                      for (let k = 0; k < this.damFileChannel.length; k++) {
                        if (this.uploadedData[i].channel.toLowerCase() == this.damFileChannel[k].channelName.toLowerCase()) {
                          //console.log("channel", i, this.uploadedData[i].channel.toLowerCase() == this.damFileChannel[j].channelName.toLowerCase());
                          this.uploadedData[i].valid = 'Valid';
                          this.uploadedData[i].reason = '';
                          // break;
                          if (!this.uploadedData[i].format) {
                            this.uploadedData[i].valid = 'Invalid';
                            this.uploadedData[i].reason = 'File format required';
                          } else {
                            for (let l = 0; l < this.damFileFormat.length; l++) {
                              if (String(this.uploadedData[i].format).trim().toLowerCase() == String(this.damFileFormat[l].formatName).toLowerCase()) {
                                this.uploadedData[i].valid = 'Valid';
                                this.uploadedData[i].reason = '';
                                // break;
                                if (!this.uploadedData[i].language) {
                                  this.uploadedData[i].valid = 'Invalid';
                                  this.uploadedData[i].reason = 'Language required';
                                } else {
                                  for (let m = 0; m < this.lang.length; m++) {
                                    if (String(this.uploadedData[i].language).toLowerCase() == String(this.lang[m].languageName).toLowerCase()) {
                                      this.uploadedData[i].valid = 'Valid';
                                      this.uploadedData[i].reason = '';
                                      // break;
                                      if (!this.uploadedData[i].status) {
                                        this.uploadedData[i].valid = 'Invalid';
                                        this.uploadedData[i].reason = 'Status required';
                                      } else {
                                        for (let n = 0; n < this.damApproverList.length; n++) {
                                          if (String(this.uploadedData[i].status.toLowerCase()) == String(this.damApproverList[n].statusName).toLowerCase()) {
                                            this.uploadedData[i].valid = 'Valid';
                                            this.uploadedData[i].reason = '';
                                            if (this.uploadedData[i].cost) {
                                              this.uploadedData[i].valid = 'Valid';
                                              this.uploadedData[i].reason = '';
                                              if (!this.uploadedData[i].approver) {
                                                this.uploadedData[i].valid = 'Invalid';
                                                this.uploadedData[i].reason = 'Approver id required';
                                              } else {
                                                for (let p = 0; p < this.AllEmployessDAM.length; p++) {
                                                  if (this.uploadedData[i].approver == this.AllEmployessDAM[p].id) {
                                                    this.uploadedData[i].valid = 'Valid';
                                                    this.uploadedData[i].reason = '';
                                                    this.uploadedData[i].approver = this.AllEmployessDAM[p].appId
                                                    if (this.uploadedData[i].author === ''
                                                          || !this.uploadedData[i].author) {
                                                      this.uploadedData[i].valid = 'Invalid';
                                                      this.uploadedData[i].reason = 'Author Name is required';
                                                    } else {
                                                      for (let r = 0; r < this.AllEdgeEmployessDAM.length; r++) {
                                                        if (this.uploadedData[i].author == this.AllEdgeEmployessDAM[r].id) {
                                                          this.uploadedData[i].valid = 'Valid';
                                                          this.uploadedData[i].reason = '';
                                                          this.uploadedData[i].author = this.AllEdgeEmployessDAM[r].empid;
                                                          break;
                                                        } else {
                                                          this.uploadedData[i].valid = 'Invalid';
                                                          this.uploadedData[i].reason = 'Author id not present';
                                                        }
                                                      }
                                                    }
                                                    break;
                                                  } else {
                                                    this.uploadedData[i].valid = 'Invalid';
                                                    this.uploadedData[i].reason = 'Approver not present';
                                                  }
                                                }
                                              }
                                              break;
                                            } else {
                                              this.uploadedData[i].valid = 'Invalid';
                                              this.uploadedData[i].reason = 'Cost required';
                                            }
                                            break;
                                          } else {
                                            this.uploadedData[i].valid = 'Invalid';
                                            this.uploadedData[i].reason = 'Status not present';
                                          }
                                        }
                                      }
                                      break;
                                    } else {
                                      this.uploadedData[i].valid = 'Invalid';
                                      this.uploadedData[i].reason = 'Language not present';
                                    }
                                  }
                                }
                                break;
                              } else {
                                this.uploadedData[i].valid = 'Invalid';
                                this.uploadedData[i].reason = 'File format not present';
                              }
                            }
                          }
                          break;
                        } else {
                          this.uploadedData[i].valid = 'Invalid';
                          this.uploadedData[i].reason = 'Channel Name not present';
                        }
                      }
                      break;
                  } else {
                    this.uploadedData[i].valid = 'Invalid';
                    this.uploadedData[i].reason = 'Category not present';
                  }
                // }
              }
            }
          }
        }
      }
      for (let i = 0; i < this.uploadedData.length; i++) {
        if (this.uploadedData[i].valid == 'Valid') {
          this.validdata.push(this.uploadedData[i]);
        } else {
          this.invaliddata.push(this.uploadedData[i]);
        }
      }

      if(this.validdata.length > 0){
        this.insertDataInTempTeble(this.validdata);
      }

      if(this.invaliddata.length > 0){
        this.showInvalidExcel = true;
        this.spinner.hide();
        this.presentToast('Info', 'Please Download Invalid List by clicking on download button');
      }
      console.log('valid data:', this.validdata);
      console.log('invalid data:', this.invaliddata);
    } else {
      this.spinner.hide();
      // var toast: Toast = {
      //   type: 'error',
      //   title: 'file',
      //   body: 'Please select a file first',
      //   showCloseButton: true,
      //   timeout: 2000,
      // };
      // this.toasterService.pop(toast);
      this.presentToast('warning', 'Please select a file');
    }
    let t1 = performance.now();
    console.log("Call to doSomething took " + (t1 - t0) + " milliseconds.");

  }
// a;
// b;
// c;
// d;
// e;
  // uploadSheet(){
  //   // this.spinner.show();
  //   console.log('uploadedData', this.uploadedData);
  //   this.uploadedData.forEach(asset => {
  //     // if(asset.description || asset.name || asset.reference){
  //     // this.dropDownData['channelName']
  //     // }
  //     this.a = this.dropDownData[asset.channel];
  //     this.b = this.dropDownData[asset.category];
  //     this.c = this.dropDownData[asset.approver];
  //     this.d = this.dropDownData[asset.format];
  //     this.e = this.dropDownData[asset.language];

  //   });
  // }

  showInvalidExcel:boolean=false;

  exportToExcelInvalid(){
    this.exportService.exportAsExcelFile(this.invaliddata,'DAM Asset Status')
  }



  insertDataInTempTeble(validArr){
    //  this.spinner.show();
    console.log('this.tenantId', this.tenantId);
    console.log('ValidArr',validArr);
    var allstr = this.getDataReady(validArr);
    console.log('allstr',allstr);
    let param = {
      "allstr":allstr,
      "tId":this.tenantId
    }
    console.log('param',param);
    this.bulkUploadService.insertValidDataTemp(param)
      .then(rescompData => {
        this.spinner.hide();
        const result = rescompData;
        console.log('InserVAlidDaat:', rescompData);
        if (result['type'] == true) {
          // const toast: Toast = {
          //   type: 'success',
          //   title: 'Bulk Upload.',
          //   body: 'Assets uploaded successfully.',
          //   showCloseButton: true,
          //   timeout: 2000,
          // };
          // this.toasterService.pop(toast);
          this.rows = this.uploadedData;
          console.log('Valid Data Inserted Successfully.');
        } else {
          this.spinner.hide()
          // var toast: Toast = {
          //   type: 'error',
          //   title: 'Bulk Upload.',
          //   body: 'Unable to upload assets.',
          //   showCloseButton: true,
          //   timeout: 2000,
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
          console.log('Unable to Insert Data.')
        }

      }, error => {
        this.spinner.hide();
        // var toast: Toast = {
        //   type: 'error',
        //   //title: "Server Error!",
        //   body: 'Something went wrong.please try again later.',
        //   showCloseButton: true,
        //   timeout: 2000,
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
  }
  getDataReady(validArr){
    console.log('ValidArr1',validArr);
    var allstr;
    for(let i=0;i<validArr.length;i++){
      let assetName = validArr[i].name;
      //let assetDate = new Date(validArr[i].date);
      let assetRef = validArr[i].reference;
      let description = validArr[i].description;
      let category = validArr[i].category;
      let formatName = validArr[i].format;
      let estLength = validArr[i].length;
      let channelName = validArr[i].channel;
      let languageName = validArr[i].language;
      let cost = validArr[i].cost;
      let status = validArr[i].status;
      let author = validArr[i].author;
      let approver = validArr[i].approver;
      let courseId = validArr[i].courseId;
      let moduleId = validArr[i].moduleId;
      let activityId = validArr[i].activityId;
      let isValid = validArr[i].valid;

      /**
       * Added mime type and reference type
       */
      let mimeType = null;
      let referenceType = null;
      if (assetRef) {
        const document = this.getMimeType(assetRef);
        if (assetRef.includes('kapsule') || assetRef.includes('kpoint')) {
          mimeType = 'embedded/kpoint';
          referenceType = 'kpoint';
        } else if (assetRef.includes('youtube')) {
          mimeType = 'embedded/youtube';
          referenceType = 'youtube';
        } else if (document) {
          mimeType = document['mimetype'];
          referenceType = document['documenttype'];
        } else if (assetRef.includes('http') || assetRef.includes('https')){
          mimeType = 'application/x-msdownload';
          referenceType = 'application';
        }
      }


      var str = assetName  + "|" + assetRef + "|" + description + "|"  + category
              + "|" + formatName + "|" + estLength  + "|" + channelName  + "|" + languageName +
               "|" + cost  + "|" + status +  "|" + author  + "|" + approver  + "|" + courseId + "|" + moduleId
               + "|" + activityId + "|" + isValid + "|" + referenceType + "|" + mimeType;

			if (i == 0) {
				allstr = str;
			} else {
				allstr += "#" + str;
      }
    }
    return allstr;
  }
getMimeType(url) {
    const mimevalue = mime.getType(url);
    if (mimevalue) {
        const documenttype = mimevalue.split("/")[0];
        const result = {
            mimetype: mimevalue,
            documenttype: documenttype
        };
        return result;
    } else {
        return mimevalue;
    }
}
}
