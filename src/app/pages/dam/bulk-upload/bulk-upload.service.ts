import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, RequestOptions, Request } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AppConfig } from '../../../app.module';
import { webApi } from '../../../service/webApi';
import { HttpClient } from "@angular/common/http";
import { AuthenticationService } from '../../../service/authentication.service';

@Injectable()

export class BulkUploadService {




    userData:any;
    parCatId: any;
    tenantId:any;
    type:any;
    private _urlInsertAssetBulkUpload = webApi.domain + webApi.url.insertAssetBulkUpload;
    private _urlInsertAssetBulkUploadNew = webApi.domain + webApi.url.insertAssetBulkUploadNew;
    private _urlInsertValidDataTemp = webApi.domain + webApi.url.insetValidDataTemp;


    constructor(@Inject('APP_CONFIG_TOKEN') private config: AppConfig, private _http: Http, private _httpClient: HttpClient,private authenticationService: AuthenticationService) {
        //this.busy = this._http.get('...').toPromise();
          if(localStorage.getItem('LoginResData')){
            this.userData = JSON.parse(localStorage.getItem('LoginResData'));
            console.log('userData', this.userData.data);
            //   this.userId = this.userData.data.data.id;
            this.tenantId = this.userData.data.data.tenantId;
        }
    }


    insertAssetBulkUpload(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlInsertAssetBulkUpload, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    bulkUploadNew(param){
        let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
        // let headers = new Headers({ 'Authorization': 'Bearer ' + 'myToken' });
        let options : any = new RequestOptions({ headers: headers });
        return new Promise(resolve => {
            this._http.post(this._urlInsertAssetBulkUploadNew, param,options)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }
    bulkUploadNew1(param){
      // let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
      // let headers = new Headers({ 'Authorization': 'Bearer ' + 'myToken' });
      // let options : any = new RequestOptions({ headers: headers });
      return new Promise(resolve => {
          this._httpClient.post(this._urlInsertAssetBulkUploadNew, param,{
            reportProgress: true,
            observe: 'events'
        })
              //.map(res => res.json())
              .subscribe(data => {
                  resolve(data);
              },
                  err => {
                      resolve('err');
                  });
      });
  }
    insertValidDataTemp(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlInsertValidDataTemp, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    _errorHandler(error: Response) {
        console.error(error);
        return Observable.throw(error || "Server Error")
    }


}
