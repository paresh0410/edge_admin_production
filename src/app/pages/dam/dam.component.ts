import { Component, OnInit, ViewEncapsulation, Output, EventEmitter } from '@angular/core';
import { Router,ActivatedRoute} from '@angular/router';
import { AssetService } from './asset/asset.service';
import { AddAssetService } from './asset/add-asset/add-asset.service';
import { AppService } from '../../app.service';
import { Item } from '@syncfusion/ej2-navigations';
import { SuubHeader } from '../components/models/subheader.model';
import { ApproveAssetService } from './approve-asset/approve-asset.service';

@Component({
  selector: 'ngx-dam',
  templateUrl: './dam.component.html',
  styleUrls: ['./dam.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DamComponent implements OnInit {
  showdata:any=[];
  learnData:any=[];
  @Output() changeHeader = new EventEmitter<any>();

  constructor(public router:Router,public routes:ActivatedRoute,private assetservice:AssetService,private AppService:AppService,
    private addassetservice : AddAssetService,private approveAssetService: ApproveAssetService,) {
      this.showdata =this.AppService.getmenus();
      if(this.showdata)
      {
          for (let i = 0; i < this.showdata.length; i++)
          {
            if (Number(this.showdata[i].parentMenuId) === 5)
            {
              this.learnData.push(this.showdata[i]);
            }
          }
      }
     }

  ngOnInit() {
    
    this.addassetservice.categoryId = null
    this.addassetservice.shareCatId = null
    this.addassetservice.reviewId = null
    this.addassetservice.addEditCategoryName = null
    this.addassetservice.innerTabId = null
    this.addassetservice.valuechanged = null
    this.addassetservice.tabId = null
    this.addassetservice.roleId = null
    this.assetservice.breadcrumbArray = null
    this.addassetservice.selectedIndex = null
    this.approveAssetService.assetStatus = null

  }
  header: SuubHeader  = {
    title:'Digital Assest Management',
    showBreadcrumb:true,
    breadCrumbList:[]
  };
  gotopages(Item){
    if(Item.menuId === 21){
      this.assetservice.getApprovedAssets = false;
      this.assetservice.getPendingAssets = false;
      this.addassetservice.dataFromDAM = false;
    }else if(Item.menuId ===22 )
    {
      this.addassetservice.dataFromDAM = true;
    this.addassetservice.dataFromAssetCategory = false;
    this.addassetservice.addEditAssetData[0]="ADD";
    }else if(Item.menuId === 23)
    {
      this.assetservice.getPendingAssets = true;
      this.assetservice.getApprovedAssets = false;
    }else if(Item.menuId ===24 )
    {
      this.assetservice.getApprovedAssets = true;
      this.assetservice.getPendingAssets = false;
    }
    else if(Item.menuId === 139){
      this.assetservice.getApprovedAssets = false;
      this.assetservice.getPendingAssets = false;
      this.addassetservice.dataFromDAM = false;
    }


    this.router.navigate([Item.menuRoute],{relativeTo:this.routes});
  }

  gotoAssetCategory(){
    this.assetservice.getApprovedAssets = false;
    this.assetservice.getPendingAssets = false;
    this.addassetservice.dataFromDAM = false;
    this.router.navigate(['asset-category'],{relativeTo:this.routes});
  }

  gotoAddAsset(){
    this.addassetservice.dataFromDAM = true;
    this.addassetservice.dataFromAssetCategory = false;
    this.addassetservice.addEditAssetData[0]="ADD";
    this.changeHeader.emit();
    this.router.navigate(['add-asset'],{relativeTo:this.routes});
  }
  gotoShareReviewAsset(){
    this.assetservice.getApprovedAssets = true;
    this.assetservice.getPendingAssets = false;
    this.router.navigate(['asset'],{relativeTo:this.routes});
  }

  gotoApproveAsset(){
    //this.router.navigate(['approve-asset'],{relativeTo:this.routes});
    this.assetservice.getPendingAssets = true;
    this.assetservice.getApprovedAssets = false;
    this.router.navigate(['asset'],{relativeTo:this.routes});
  }
  gotoAssetReviewPolicy(){
    this.router.navigate(['asset-review-policy'],{relativeTo:this.routes});
  }

  gotoAsset(){
    this.router.navigate(['asset'],{relativeTo:this.routes});
  }

  gotoBulkUpload(){
    this.router.navigate(['bulk-upload'],{relativeTo:this.routes});
  }

}
