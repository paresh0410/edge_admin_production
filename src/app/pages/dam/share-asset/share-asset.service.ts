import {Injectable, Inject} from '@angular/core';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppConfig} from '../../../app.module';
import { webApi } from '../../../service/webApi';
import { HttpClient } from "@angular/common/http";

@Injectable()
export class ShareAssetService {

    tenantId: any = 1;
    userData;
    getAssetDataForShare:any;
   tabId:any
  private _urlGetAssetShareEmployees:string = webApi.domain + webApi.url.getAssetShareEmployee;
  private _urlInsertAssetShare:string = webApi.domain + webApi.url.insertAssetShare;



  constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private _http: Http,private _httpClient:HttpClient){
      //this.busy = this._http.get('...').toPromise();
      if(localStorage.getItem('LoginResData')){
        this.userData = JSON.parse(localStorage.getItem('LoginResData'));
        console.log('userData', this.userData.data);
        // this.userId = this.userData.data.data.id;
        this.tenantId = this.userData.data.data.tenantId;
     }
  }

getAssetShareEmployee(param){
  return new Promise(resolve => {
    this._httpClient.post(this._urlGetAssetShareEmployees, param)
    //.map(res => res.json())
    .subscribe(data => {
        resolve(data);
    },
    err => {
        resolve('err');
    });
});
}

insertAssetShare(param){
    return new Promise(resolve => {
      this._httpClient.post(this._urlInsertAssetShare, param)
      //.map(res => res.json())
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
  }


_errorHandler(error: Response){
  console.error(error);
  return Observable.throw(error || "Server Error")
}

}
