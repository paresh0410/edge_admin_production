import { Component, OnInit, ViewChild } from '@angular/core';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { NgxSpinnerService } from 'ngx-spinner';
import { webAPIService } from '../../../service/webAPIService';
import { webApi } from '../../../service/webApi';
import { ShareAssetService } from './share-asset.service';
import { Router,ActivatedRoute} from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { SuubHeader } from '../../components/models/subheader.model';
import { ThrowStmt } from '@angular/compiler';
import { AddAssetService } from '../asset/add-asset/add-asset.service';

@Component({
  selector: 'ngx-share-asset',
  templateUrl: './share-asset.component.html',
  styleUrls: ['./share-asset.component.scss']
})
export class ShareAssetComponent implements OnInit {

  @ViewChild('myTable') table: any;
  rows = [];
  selected:any = [];


  tenantId : any;
  assetId : any;
  userId : any;
  labels: any = [
    { labelname: '', bindingProperty: '', componentType: 'checkbox' },
    { labelname: 'ecn', bindingProperty: 'idnumber', componentType: 'text' },
    { labelname: 'CONSUMER', bindingProperty: 'name', componentType: 'text' },
    { labelname: 'E-MAIL', bindingProperty: 'email', componentType: 'text' },
    ];
    header:SuubHeader
    // header: SuubHeader  = {
    //   title:'SHARE ASSET',
    //   btnsSearch: true,
    //   searchBar: true,
    //   dropdownlabel: ' ',
    //   placeHolder:'Search user...',
    //   drplabelshow: false,
    //   drpName1: '',
    //   drpName2: ' ',
    //   drpName3: '',
    //   drpName1show: false,
    //   drpName2show: false,
    //   drpName3show: false,
    //   btnName2: '',
    //   btnName1: 'Save',
    //   btnName3: '',
    //   btnAdd: '',
    //   btnName1show: true,
    //   btnName2show: false,
    //   btnName3show: false,
    //   btnBackshow: true,
    //   btnAddshow: false,
    //   filter: false,
    //   showBreadcrumb: true,
    //   breadCrumbList:[
    // {
    //   'name': ' DAM',
    //   'navigationPath': '/pages/dam',
    // },]
    // };
    
  search: any;
  searchtext: string;
  checked: boolean;
  breadCrumbList: any  = [];
  title: any;
  constructor(
    // private toasterService: ToasterService, 
    private addAssetService: AddAssetService,
    private spinner: NgxSpinnerService,
    protected webApiService:webAPIService,private shareassetservice : ShareAssetService,public router:Router,
    public routes:ActivatedRoute,  private toastr: ToastrService,private commonFunctionService: CommonFunctionsService) {

      if(this.shareassetservice.tenantId){
        this.tenantId = this.shareassetservice.tenantId;
        console.log('this.tenantId',this.tenantId);
      }
  
      if(this.shareassetservice.getAssetDataForShare){
        this.assetId = this.shareassetservice.getAssetDataForShare.assetId;
        console.log('assetId',this.assetId);
      }
  
      if (localStorage.getItem('LoginResData')) {
        var userData = JSON.parse(localStorage.getItem('LoginResData'));
        console.log('userData', userData.data);
        this.userId = userData.data.data.id;
        console.log('userId', userData.data.data.id);
      }
    // this.rows = this.team;
    this.getAssetShareEmployeesDAM()
  }

  ngOnInit() {
    if(this.addAssetService.breadCrumbArray){
      this.breadCrumbList = this.addAssetService.breadCrumbArray
      this.title = this.addAssetService.title
    }
    this.header= {
      // title:'SHARE File',
      title:this.title,
      btnsSearch: true,
      searchBar: true,
      dropdownlabel: ' ',
      placeHolder:'Search user...',
      drplabelshow: false,
      drpName1: '',
      drpName2: ' ',
      drpName3: '',
      drpName1show: false,
      drpName2show: false,
      drpName3show: false,
      btnName2: '',
      btnName1: 'Save',
      btnName3: '',
      btnAdd: '',
      btnName1show: true,
      btnName2show: false,
      btnName3show: false,
      btnBackshow: true,
      btnAddshow: false,
      filter: false,
      showBreadcrumb: true,
    //   breadCrumbList:[
    // {
    //   'name': ' DAM',
    //   'navigationPath': '/pages/dam',
    // },]
    };
    this.header.breadCrumbList = this.breadCrumbList
  }

  backToAsset(data){
    // this.addAssetService.categoryId = data.assetId
    this.addAssetService.shareCatId = data.assetId
    this.addAssetService.title = data.assetName
    this.addAssetService.breadCrumbArray = this.addAssetService.breadCrumbArray.slice(0,data.index)
    // this.title = this.addassetservice.breadCrumbArray[]
    this.addAssetService.previousBreadCrumb = this.addAssetService.previousBreadCrumb.slice(0,data.index+1)

    this.router.navigate(['../'], { relativeTo: this.routes });


  }



  presentToast(type, body) {
    if(type === 'success'){
    this.toastr.success(body, 'Success', {
    closeButton: false
    });
    } else if(type === 'error'){
    this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
    timeOut: 0,
    closeButton: true
    });
    } else{
    this.toastr.warning(body, 'Warning', {
    closeButton: false
    })
    }
    }

  noEmployees:boolean=false;
  allEmployees:any
  getAssetShareEmployeesDAM(){
    this.spinner.show();
      let param = {
          "assetsId":this.assetId,
          "tId":this.tenantId
      }
      const _urlGetAssetShareEmployees:string = webApi.domain + webApi.url.getAssetShareEmployee;
      this.commonFunctionService.httpPostRequest(_urlGetAssetShareEmployees,param)
      // this.shareassetservice.getAssetShareEmployee(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('getShareEmployessDAM:',rescompData);
        if(result['type'] == true){
          if(result['data'][0].length == 0){
            this.noEmployees = true;
            this.selected = [];
            
          }else{
            this.allEmployees = result['data'][0];
            console.log('this.allEmployees',this.allEmployees);
            this.rows = this.allEmployees;
            this.selected = [];
            for(let i = 0;i<this.allEmployees.length;i++){
              if(this.allEmployees[i].checked == 1){
                this.selected.push(this.allEmployees[i]);
              }
            }
            if(this.selected.length>0){
              this.checked=true;
            }
            else{
              this.checked=false;
            }
            console.log('selected',this.selected);
          }
          
        }else{
          //   var toast : Toast = {
          //     type: 'error',
          //     //title: "Server Error!",
          //     body: "Something went wrong.please try again later.",
          //     showCloseButton: true,
          //     timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }
        
       
      },error=>{
        this.spinner.hide();
        //   var toast : Toast = {
        //     type: 'error',
        //     //title: "Server Error!",
        //     body: "Something went wrong.please try again later.",
        //     showCloseButton: true,
        //     timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
  }

  onSelect(data){
    console.log('selected row:',data);
    this.selected = data
    if(this.selected.length>0){
      this.checked=true;
    }
    else{
      this.checked=false;
    }
  //this.selected = data
  
    // console.log('this.selected',this.selected);
    
  }
  // onSelectedItems(data){
  //   this.selected = data;
  // }

  searchvalue:any = {
    value:''
  };
  searchShareUser(event,searchtext) {  
    searchtext= event.target.value;

    const val = event.target.value.toLowerCase();
    this.searchtext=val;
    // this.allEnrolUser( this.courseDataService.data.data)
    
  //  this.temp = [...this.enrolldata];
  //   console.log(this.temp);
    // filter our data
    if(val.length>=3||val.length==0){
    const searchArr = this.allEmployees.filter(function(d) {
      return String(d.ecn).toLowerCase().indexOf(val) !== -1 || 
      d.name.toLowerCase().indexOf(val) !== -1 || 
      d.email.toLowerCase().indexOf(val) !== -1 ||  
      // d.mode.toLowerCase() === val || !val;
       !val
    });

    // update the rows
    this.rows = [...searchArr];
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }
  }

  clearSearch(){
    // this.searchvalue={
    //   value :''
    // };
    if(this.searchtext.length>=3){
    this.rows = this.allEmployees;
    this.header.searchtext = '';
    this.searchtext='';
    // this.searchtext = this.header.searchtext;
    // this.fetchCertificates();
    // this.getAssetShareEmployeesDAM();
  }
  else{
    this.header.searchtext = '';

  }
}

  save(){
    var userStr = this.getUserdataReady(this.selected);
    console.log('userStr',userStr);
    let param = {
      "assetsId":this.assetId,
      "tId":this.tenantId,
      "userId":this.userId,
      "qOptions":userStr
    }
    const  _urlInsertAssetShare:string = webApi.domain + webApi.url.insertAssetShare;
    this.commonFunctionService.httpPostRequest(_urlInsertAssetShare,param)
    // this.shareassetservice.insertAssetShare(param)
    .then(rescompData => {
      this.spinner.hide();
      this.checked=true;
      var result = rescompData;
      console.log('getShareEmployessDAM:',rescompData);
      if(result['type'] == true){
      //   var toast : Toast = {
      //     type: 'success',
      //     title: "Share Asset",
      //     body: "Asset share successfully.",
      //     showCloseButton: true,
      //     timeout: 2000
      // };
      // this.toasterService.pop(toast);
      var length = this.addAssetService.breadCrumbArray.length-1
      this.title = this.addAssetService.breadCrumbArray[length].name
      this.addAssetService.title = this.title
      this.addAssetService.breadCrumbArray = this.addAssetService.breadCrumbArray.slice(0,length)
    // this.title = this.addassetservice.breadCrumbArray[]
    this.addAssetService.previousBreadCrumb = this.addAssetService.previousBreadCrumb.slice(0,length+1)
      this.presentToast('success', 'File Shared Successfully');
      this.getAssetShareEmployeesDAM();
      this.router.navigate(['../'],{relativeTo:this.routes});

        
      }else{
        //   var toast : Toast = {
        //     type: 'error',
        //     //title: "Server Error!",
        //     body: "Something went wrong.please try again later.",
        //     showCloseButton: true,
        //     timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      }
      
     
    },error=>{
      this.spinner.hide();
      //   var toast : Toast = {
      //     type: 'error',
      //     //title: "Server Error!",
      //     body: "Something went wrong.please try again later.",
      //     showCloseButton: true,
      //     timeout: 2000
      // };
      // this.toasterService.pop(toast);
      this.presentToast('error', '');
    });
  }

  getUserdataReady(selectedUserArr){
    console.log('selectedUserArr',selectedUserArr);
    var userString = "";
    if(selectedUserArr.length > 0){
      for(let i=0; i < selectedUserArr.length; i++){
        var user = selectedUserArr[i].ecn;
        if(userString != ""){
          userString += "|";
        }
        if(user.value){
          if(String(user.value) != "" && String(user.value) != "null"){
            userString += user.value;
          } 
        }else{
          if(String(user) != "" && String(user) != "null"){
            userString += user;
          } 
        }  
      }
      return userString;
    }
    else{
      return null;
    }
  }

  back(){
    // this.router.navigate(['../../asset'],{relativeTo:this.routes});
    var length = this.addAssetService.breadCrumbArray.length-1
    this.title = this.addAssetService.breadCrumbArray[length].name
    this.addAssetService.title = this.title
    this.addAssetService.breadCrumbArray = this.addAssetService.breadCrumbArray.slice(0,length)
  // this.title = this.addassetservice.breadCrumbArray[]
  this.addAssetService.previousBreadCrumb = this.addAssetService.previousBreadCrumb.slice(0,length+1)
    console.log(this.title,"title")
    this.router.navigate(['../'],{relativeTo:this.routes});

  }

  onSearch(event){
    this.search.name = event.target.value

  }
}
