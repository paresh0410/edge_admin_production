import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShareAssetComponent } from './share-asset.component';

describe('ShareAssetComponent', () => {
  let component: ShareAssetComponent;
  let fixture: ComponentFixture<ShareAssetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShareAssetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShareAssetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
