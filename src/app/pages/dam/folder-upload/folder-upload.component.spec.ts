import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FolderUploadComponent } from './folder-upload.component';

describe('FolderUploadComponent', () => {
  let component: FolderUploadComponent;
  let fixture: ComponentFixture<FolderUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FolderUploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FolderUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
