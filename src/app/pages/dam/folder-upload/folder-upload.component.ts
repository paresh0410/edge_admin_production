import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { map, catchError } from "rxjs/operators";
// import { BulkUploadService } from '../pages/dam/bulk-upload/bulk-upload.service';
import { ToastrService } from 'ngx-toastr';
import { BulkUploadService } from '../bulk-upload/bulk-upload.service';
import { SuubHeader } from '../../components/models/subheader.model';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, ActivatedRoute } from '@angular/router';
import { AddAssetService } from '../asset/add-asset/add-asset.service';
import {
  HttpClient,
  HttpEventType,
  HttpErrorResponse,
  HttpHeaders,
} from "@angular/common/http";
import { throwError } from "rxjs";
import { webApi } from '../../../service/webApi';
@Component({
  selector: 'ngx-folder-upload',
  templateUrl: './folder-upload.component.html',
  styleUrls: ['./folder-upload.component.scss']
})
export class FolderUploadComponent implements OnInit {
  folderPopupshow: boolean = false;
  progress: number = null;
  fileUrl: any  = [];
  file: any;
  fileName : any = 'Click here to upload File'
  header: SuubHeader  = {
    title:'File Upload',
    btnsSearch: true,
    searchBar: false,
    dropdownlabel: ' ',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: '',
    btnName3: '',
    // btnAdd: 'File Upload',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    // btnAddshow: true,
    filter: false,
    showBreadcrumb: true,
    breadCrumbList:[]
  };
  type:any
  breadCrumbList: any = [];
  title: any;
  constructor(private bulkUploadService: BulkUploadService,private toastr: ToastrService,
    private cdf: ChangeDetectorRef,
    private httpClient: HttpClient,
    private spinner: NgxSpinnerService,
    private addassetservice: AddAssetService,
    public router: Router,
    public routes: ActivatedRoute,) { }
  ngOnInit() {
    if(this.addassetservice.breadCrumbArray){
      this.breadCrumbList = this.addassetservice.breadCrumbArray
      this.title = this.addassetservice.title
      this.header.breadCrumbList = this.breadCrumbList
      this.header.title =  this.title
    }
    console.log(this.bulkUploadService.parCatId,"parcatId")
    if(this.bulkUploadService.parCatId == null){
      this.header.title = ' Folder Upload'
    }
    if(this.bulkUploadService.type){
    this.type = this.bulkUploadService.type
    }
    if(this.type == 1){
      this.fileName = 'Click here to upload Folder'
      // this.header.title = 'Folder Upload'
    }
    else{
      this.fileName = 'Click here to upload File'
      // this.header.title = 'File Upload'
    }
  }
folder(){
  this.folderPopupshow = true
}
onSelectFile(event) {
  var validExts
  if(this.type == 1){
   validExts = new Array( "zip" );
  }else{
    validExts = new Array('video','audio','application/vnd.ms-powerpoint','application/vnd.openxmlformats-officedocument.presentationml.presentation',
    'pdf','image','audio',"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/vnd.ms-excel",
    "application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document")
  }
  var fileType = event.target.files[0].type;
  var fileExt = false;
  for(let i=0; i<validExts.length; i++){
    if(fileType.includes(validExts[i])){
      // return true;
      fileExt = true;
      break;
    }
  }
  // if(validExts.indexOf(fileType) < 0) {
  if(!fileExt) {
    // var toast : Toast = {
    //   type: 'error',
    //   title: "Invalid file selected!",
    //   body: "Valid files are of " + validExts.toString() + " types.",
    //   showCloseButton: true,
    //   timeout: 2000
    // };
    // this.toasterService.pop(toast);
    // this.presentToast('warning', 'Valid file types are ' + validExts.toString());
  if(this.type == 1){
    this.presentToast('warning', 'Please select only zip folder');
  }else{
    this.presentToast('warning', 'Please select a valid file type');
  }
  }else{
  this.fileUrl = []
  var file
  this.file = event.target.files[0]
  this.fileName = event.target.files[0].name
  this.folderPopupshow = true
  // this.file =file
  console.log(this.file,"files")
  for (let i = 0; i < event.target.files.length; i++) {
      // const file = file[i];
      // var dummy = file[i]
      // var path = dummy.webkitRelativePath.split('/');
    // upload file using path
    var reader = new FileReader();
    reader.onload = (event:any) => {
      // console.log(event.target.result);
      this.fileUrl.push(event.target.result);
      console.log(this.fileUrl,"fileUrl")
    }
    reader.readAsDataURL(event.target.files[i]);
  }
  // console.log(path,"path")
  console.log(this.file,"fileUrl")
}
}
submit() {
  var content = {name: 'Bhavesh'};
  var param = {
    parCatId:this.bulkUploadService.parCatId
  }
  // let param = this.bulkUploadService.parCatId?this.bulkUploadService.parCatId:null
  var fd = new FormData();
  // fd.append('content', JSON.stringify(content));
  fd.append('file', this.file);
  fd.append('fileType',this.type)
  fd.append('parCatId',this.bulkUploadService.parCatId)
  // fd.append('parCatId',param)
  this.spinner.show()
  // const param = {
  //   file:this.file
  // };
  this.bulkUploadService.bulkUploadNew(fd)
    .then(rescompData => {
      // this.spinner.hide();
      const result = rescompData;
      console.log('Bulk Upload assets:', rescompData);
       JSON.parse(localStorage.getItem('LoginResData'));
      var body = JSON.parse(result['_body']);
      console.log(body,"jsonparse")
      // if (result['type'] == true) {
      if (body['type'] == true) {
        this.file = ''
        // this.fileName = 'Click here to upload File'
        this.folderPopupshow = false
        if(this.type == 1){
          this.fileName = 'Click here to upload Folder'
        }
        else{
          this.fileName = 'Click here to upload File'
        }
        // const toast: Toast = {
        //   type: 'success',
        //   title: 'Bulk Upload.',
        //   body: 'Assets uploaded successfully.',
        //   showCloseButton: true,
        //   timeout: 2000,
        // };
        this.spinner.hide();
        // this.toasterService.pop(toast);
        this.presentToast('success', body['message']);
        this.router.navigate(['../'],{relativeTo:this.routes});
      } else {
        // var toast: Toast = {
        //   type: 'error',
        //   title: 'Bulk Upload.',
        //   body: 'Unable to upload assets.',
        //   showCloseButton: true,
        //   timeout: 2000,
        // };
        this.spinner.hide();
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      }
    }, error => {
      this.spinner.hide();
      // var toast: Toast = {
      //   type: 'error',
      //   //title: "Server Error!",
      //   body: 'Something went wrong.please try again later.',
      //   showCloseButton: true,
      //   timeout: 2000,
      // };
      // this.toasterService.pop(toast);
      this.presentToast('error', '');
    });
}
submit1() {
  var content = {name: 'Bhavesh'};
  var param = {
    parCatId:this.bulkUploadService.parCatId
  }
  this.progress=1
  // let param = this.bulkUploadService.parCatId?this.bulkUploadService.parCatId:null
  var fd = new FormData();
  // fd.append('content', JSON.stringify(content));
  fd.append('file', this.file);
  fd.append('fileType',this.type)
  // fd.append('reportProgress','true')
  fd.append('parCatId',this.bulkUploadService.parCatId)
  // fd.append('parCatId',param)
  // this.spinner.show()
  // const param = {
  //   file:this.file
  // };
  this.bulkUploadService.bulkUploadNew(fd)
    .then((event: any) => {
      console.log(event,"ffff")
      if (event.type == HttpEventType.UploadProgress) {
        this.progress = Math.round((100 / event.total) * event.loaded);
        console.log(this.progress,"progress")
      } else if (event.type == HttpEventType.Response) {
        this.progress = null;
        console.log(this.progress,"progress")
      }
    }),
    catchError((err: any) => {
      this.progress = null;
      alert(err.message);
      return throwError(err.message);
    })
}
cancel(){
  this.fileName = 'Click here to upload File'
  this.folderPopupshow = false
}
presentToast(type, body) {
  if(type === 'success'){
  this.toastr.success(body, 'Success', {
  closeButton: false
  });
  } else if(type === 'error'){
  this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
  timeOut: 0,
  closeButton: true
  });
  }else if(type === 'info'){
    this.toastr.info(body, 'Success', {
    closeButton: false
    });
  }else{
  this.toastr.warning(body, 'Warning', {
  closeButton: false
  })
  }
  }
  back(){
    // window.history.back();
    // this.router.navigate(['../../assets'], { relativeTo: this.routes });
    this.router.navigate(['../'],{relativeTo:this.routes});
  }
  backToAsset(data){
    this.addassetservice.categoryId = data.assetId
    this.addassetservice.title = data.assetName
    this.addassetservice.breadCrumbArray = this.addassetservice.breadCrumbArray.slice(0,data.index)
    // this.title = this.addassetservice.breadCrumbArray[]
    this.addassetservice.previousBreadCrumb = this.addassetservice.previousBreadCrumb.slice(0,data.index+1)
    this.router.navigate(['../'],{relativeTo:this.routes});
  }
  disabled = false;
  upload() {
    var content = {name: 'Bhavesh'};
    var param = {
      parCatId:this.bulkUploadService.parCatId
    }
    this.progress=1
    // let param = this.bulkUploadService.parCatId?this.bulkUploadService.parCatId:null
    var fd = new FormData();
    // fd.append('content', JSON.stringify(content));
    fd.append('file', this.file);
    fd.append('fileType',this.type)
    // fd.append('reportProgress','true')
    fd.append('parCatId',this.bulkUploadService.parCatId);
    // fd.append("data", JSON.stringify(fd));
    const url = webApi.domain + webApi.url.insertAssetBulkUploadNew;
    // let headers = new HttpHeaders();
    // headers = headers.set('Content-Type', 'multipart/form-data');
    let options = {
      search:{},
      reportProgress: true,
  };
    this.httpClient
      .post(url,
         fd, {
          reportProgress: true,
          observe: 'events'
      })
      .pipe(
        map((event: any) => {
          if (event.type == HttpEventType.UploadProgress) {
            this.progress = Math.round((100 / event.total) * event.loaded);
            if(this.progress === 100){
              this.spinner.show();
              this.cdf.detectChanges();
            }
          } else if (event.type == HttpEventType.Response) {
            this.progress = null;
            this.spinner.hide();
            this.cdf.detectChanges();
            if (event['body']['type'] == true) {
              this.file = ''
              // this.fileName = 'Click here to upload File'
              this.folderPopupshow = false;
              if(this.type == 1){
                this.fileName = 'Click here to upload Folder'
              }
              else{
                this.fileName = 'Click here to upload File'
              }
              // const toast: Toast = {
              //   type: 'success',
              //   title: 'Bulk Upload.',
              //   body: 'Assets uploaded successfully.',
              //   showCloseButton: true,
              //   timeout: 2000,
              // };
              // this.spinner.hide();
              // this.toasterService.pop(toast);
              this.presentToast('success', event['body']['message']);
              this.router.navigate(['../'],{relativeTo:this.routes});
            } else {
              // var toast: Toast = {
              //   type: 'error',
              //   title: 'Bulk Upload.',
              //   body: 'Unable to upload assets.',
              //   showCloseButton: true,
              //   timeout: 2000,
              // };
              // this.spinner.hide();
              // this.toasterService.pop(toast);
              this.presentToast('error', '');
            }
          }
        }),
        catchError((err: any) => {
          this.spinner.hide();
          this.cdf.detectChanges();
          this.progress = null;
          // alert(err.message);
          return throwError(err.message);
        })
      )
      .toPromise();
  }
}
