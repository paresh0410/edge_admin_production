import { Component, OnInit, ChangeDetectorRef, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
// import { AssetService } from './asset.service';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, ActivatedRoute } from '@angular/router';
// import { AddAssetService } from './add-asset/add-asset.service';
import { ApproveAssetService } from './../approve-asset/approve-asset.service'
//import { MatTreeFlatDataSource } from '@angular/material';
import { ShareAssetService } from './../share-asset/share-asset.service';
import { AssetReviewPolicyService } from './../asset-review-policy/asset-review-policy.service'
import { AssetVesrionService } from '../asset-version/asset-version.service';
import { isString } from 'util';
import { NbMenuService, NbSidebarService } from '@nebular/theme';
import { LayoutService } from '../../../@core/data/layout.service';
//import { AddAssetService } from './add-asset/add-asset.service'
import { HostListener } from '@angular/core';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { webApi } from '../../../service/webApi';
import { Card } from '../../../models/card.model';
import { SuubHeader } from '../../components/models/subheader.model';
import { AssetService } from '../asset/asset.service';
import { AddAssetService } from '../asset/add-asset/add-asset.service';
import { HttpClient, HttpEventType } from '@angular/common/http';
import { FormGroup } from '@angular/forms';
import { webAPIService } from '../../../service/webAPIService';
import { MatTabChangeEvent } from '@angular/material';
import { noData } from '../../../models/no-data.model';
import { BulkUploadService } from '../bulk-upload/bulk-upload.service';
import { Filter } from '../../../models/filter.modal';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
//import { InfiniteScrollDirective } from 'ngx-infinite-scroll';
//import { InfiniteScroll } from 'ngx-infinite-scroll';
//const nisPackage = require('../../../../../package.json');
@Component({
  selector: 'ngx-newasset',
  templateUrl: './new-asset.component.html',
  styleUrls: ['./new-asset.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class NewAssetComponent implements OnInit {
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  // @ViewChild('fileInput') fileInput: ElementRef;
  title = 'demo-app';

  isworking = false;
  Status = [];
  Languages = [];
  Formats = [];
  Channels = [];
  Categories = [];
  Tags = [];
  breadcrumbArray: any = [
    {
    'name': 'DAM',
    'navigationPath': '/pages/dam',
  },{
    'name': 'Folder Files',
    'navigationPath': '/pages/dam/assets',
    'id':{},
    'sameComp':true,
    'assetId':null
  }
];
  header: SuubHeader;
  // ngOnInit() {
  // }
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No file or folder at this time.",
    desc:"",
    titleShow:true,
    btnShow:true,
    descShow:false,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/how-to-add-an-asset-in-the-dam',
  };
  cardModify: Card = {
    flag: 'assets',
    titleProp: 'assetName',
    discrption: 'description',
    contentType: 'formatId',
    showImage: true,
    hoverlable: true,
    option: true,
    eyeIcon: true,
    copyIcon: false,
    bottomDiv: true,
    bottomTitle: true,
    showBottomNoCat:true,
    Nocat:'Yes',
    bottomDiscription: true,
    upgradeIcon:true,
    customCard: '',
    btnLabel: 'View',
    download : true,
    identifier:true
  };
  count:number=8
  cardModifyPending: Card = {
    flag: 'assets',
    titleProp: 'assetName',
    discrption: 'description',
    contentType: 'formatId',
    showImage: true,
    hoverlable: true,
    bottomDiv: true,
    bottomTitle: true,
    bottomDiscription: true,
    customCard: '',
    btnLabel: 'Edit',
  }
  cardModifyApproved: Card = {
    flag: 'assets',
    titleProp: 'assetName',
    discrption: 'description',
    contentType: 'formatId',
    showImage: true,
    option: true,
    shareIcon: true,
    retweet: true,
    // retweet:false,
    bottomDiv: true,
    bottomTitle: true,
    bottomDiscription: true,
    showBottomNoCat:true,
    identifier:true,
    Nocat:'Yes',
    customCard: '',
  }
  cardModifyApprovedNew: Card = {
    flag: 'newasset',
    titleProp: 'assetName',
    discrption: 'description',
    contentType: 'formatId',
    showImage: true,
    option: false,
    shareIcon: false,
    hoverlable:   true,
    identifier:true,
    // retweet: true,
    retweet:false,
    bottomDiv: true,
    bottomTitle: true,
    bottomDiscription: true,
    showBottomCat:true,
    cat:'Yes',
    btnLabel: 'Details',
    customCard: '',
  }

  cardModifyAsset: Card = {
    flag: 'newasset',
    titleProp: 'assetName',
    image: 'categoryPicRef',
    discrption: 'description',
    cnt: 'cnt',
    catCnt:'catCnt',
    showImage: true,
    hoverlable:   true,
    option:   true,
    eyeIcon:   true,
    editIcon:   true,
    bottomDiv:   true,
    bottomTitle:   true,
    bottomDiscription:   true,
    showBottomList:   true,
    showBottomAssets: true,
    showBottomCat:true,
    identifier:true,
    cat:'Yes',
    showBottomCatAssets:true,
    btnLabel: 'Details',

  }

  cardModifyAsset2: Card ={
    flag: 'assetCategory',
    titleProp: 'categoryName',
    image: 'categoryPicRef',
    discrption: 'description',
    cnt: 'cnt',
    showImage:   true,
    hoverlable:   true,
    option:   true,
    eyeIcon:   true,
    editIcon:   true,
    bottomDiv:   true,
    bottomTitle:   true,
    bottomDiscription:   true,
    showBottomList:   true,
    showBottomAssets: true,
    btnLabel: 'Details',
  };
  storedVisible: any;
  categoryId: any;
  tenantId: any;
  getPendingAssets: boolean = false;
  getApprovedAssets: boolean = false;
  search: any;
  searchTags: any;
  skeleton = false
  dataFromAssetCategory: boolean = false;
  pagename: any;
  status: any = [];
  categories: any = [];
  formats: any = [];
  channels: any = [];
  languages: any = [];
  noCategory: boolean = false;
  enableDisableCategoryModal: boolean = false;
  assetsdata: any;
  changetitle: any;
  avisible: any;
  msg: any;
  userData: any;
  categoryForm:any = {};
  addEditCategoryModal: boolean = false;
  addAction: boolean;
  helpContent: any= [];
  categoryFormValidation: FormGroup;
  parentCatId: any = null;
  innerTabId:any = null;
  oldParentCatId: any = null;
  passParams: any
  fileUploadRes: any;
  fileres: any;
  assetCatImgData: any;
  catVisibleStatus: number;
  categoryData: any;
  identifier: any;
  searchtext: string;
  demoasset: any;
  assetVersion: any = [];


  themesclr: any = [
    {
      name: "Admin",
      // items: [
      //   { name: 'Header', value: '#fff' },
      //   { name: 'Header Text', value: '#000' },
      //   { name: 'Sidemenu', value: '#f00' }
      // ]
    },
    {
      name: "Portal",
      // items: [
      //   { name: 'Header', value: '#fff' },
      //   { name: 'Header Text', value: '#000' },
      //   { name: 'Sidemenu', value: '#f00' }
      // ]
    },
    {
      name: "App",
      // items: [
      //   { name: 'Header', value: '#fff' },
      //   { name: 'Header Text', value: '#000' },
      //   { name: 'Sidemenu', value: '#f00' }
      // ]
    },
  ];
  damTabs: any;
  roleId: any;
  tabId: any;
  downloadLink: any;
  selectedIndex: any = 0;
  filters: any = [];
  filtercon: Filter = {
    ascending: true,
    descending: true,
    // showDropdown: true,
    showDropdown: false,
    dropdownList: [
      { drpName: 'Enrol Date', val: 1 },
      { drpName: 'Created Date', val: 2 },
    ]
  };
  filtersInner: any = [];
  filter: boolean =false;
  breadObj: {}
  display: boolean = false;
  progress: any;
  type: any;
  enableFileUploadModal:any = false
  fileName: string;
  folderPopupshow: boolean;
  file: string;
  fileUrl: any[];
  isVisible: any;
  isAdmin: any;
  formatExtensions: any;
  constructor(private assetservice: AssetService,private http1: HttpClient,protected webApiService: webAPIService,
    // private toasterService: ToasterService,
    private bulkUploadService: BulkUploadService,
    private spinner: NgxSpinnerService,
    // private httpClient: HttpClient,
    public router: Router, public routes: ActivatedRoute, private addAssetService: AddAssetService,
    private approveAssetService: ApproveAssetService, private shareAssetService: ShareAssetService,
    private assetReviewPolicyService: AssetReviewPolicyService, private assetVesrionService: AssetVesrionService,
    private sidebarService: NbSidebarService, private layoutService: LayoutService, private cdf: ChangeDetectorRef,
    private toastr: ToastrService, private commonFunctionService: CommonFunctionsService) {

    this.toggleSidebar();
    if (localStorage.getItem('LoginResData')) {
      this.userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', this.userData.data);
      //  this.userId = this.userData.data.data.id;
      this.tenantId = this.userData.data.data.tenantId;
    }
    if(this.addAssetService.categoryId!=null || this.addAssetService.shareCatId!= null){
      this.title = this.addAssetService.title
      this.breadcrumbArray = this.addAssetService.breadCrumbArray
      this.previousBreadCrumb = this.addAssetService.previousBreadCrumb
    }
    // if (this.addAssetService.tabId) {
    //   this.tabId = this.addAssetService.tabId
    //   this.roleId =this.addAssetService.roleId
    //   if(this.tabId == 2){
    //     this.selectedIndex = 2
    //   }else{
    //     this.selectedIndex = this.tabId
      
    //   }
    //   this.getAllAssetandCategories(this.parentCatId,this.roleId,this.tabId)
    // } else {
      if (localStorage.getItem('damTabs')) {
        this.damTabs = JSON.parse(localStorage.getItem('damTabs'));
      }
      if (localStorage.getItem('formatExtensions')) {
        this.formatExtensions = JSON.parse(localStorage.getItem('formatExtensions'));
      }
      if (localStorage.getItem('isVisible')) {
        this.isVisible = JSON.parse(localStorage.getItem('isVisible'));
      }
      console.log(this.isVisible.isVisible,"visible")
      this.isVisible = this.isVisible.isVisible

      if (localStorage.getItem('isAdmin')) {
        this.isAdmin = JSON.parse(localStorage.getItem('isAdmin'));
      }
      console.log(this.isAdmin.isAdmin,"visible")
      this.isAdmin = this.isAdmin.isAdmin


      if(this.addAssetService.selectedIndex){
        this.selectedIndex = this.addAssetService.selectedIndex
      }
      console.log(this.damTabs, "this.damTabs")
      if (this.damTabs.length == 1) {
        this.roleId = this.damTabs[0].roleId
        this.tabId = this.damTabs[0].tabId
        if(this.addAssetService.roleId){
          this.roleId = this.addAssetService.roleId
        }
        if (this.addAssetService.tabId) {
            this.tabId = this.addAssetService.tabId
            // this.selectedIndex = this.tabId
            if(this.tabId == 2){
              // this.selectedIndex = 1
              }else if(this.tabId == 3){
                // this.selectedIndex= 2
              }
              else{
                // this.selectedIndex = 0
              }
        }else{
          // this.selectedIndex = this.tabId
          if(this.tabId == 2){
            // this.selectedIndex = 1
            }else if(this.tabId == 3){
              // this.selectedIndex= 2
            }else{
              // this.selectedIndex= 0
            }
        }
      } else {
        this.roleId = this.damTabs[0].roleId
        this.tabId = this.damTabs[0].tabId
        if(this.addAssetService.roleId){
          this.roleId = this.addAssetService.roleId
        }
        if (this.addAssetService.tabId) {
            this.tabId = this.addAssetService.tabId
            if(this.tabId == 2){
              // this.selectedIndex = 1
              }
              else if(this.tabId == 3){
                // this.selectedIndex= 2
              }else{
                // this.selectedIndex= 0
              }
            // this.selectedIndex = this.tabId
        }else{
          if(this.tabId == 2){
            // this.selectedIndex = 1
            }
            else if(this.tabId == 3){
              // this.selectedIndex= 2
            }else{
              // this.selectedIndex= 0
            }
          // this.selectedIndex = this.tabId
        }

        if (this.assetservice.getApprovedAssets) {
        //   this.breadcrumbArray = [{
        //     'name': 'DAM',
        //     'navigationPath': '/pages/dam',
        //   },{
        //     'name': 'Share Asset',
        //     'navigationPath': '/pages/dam/assets',
        //     'id':{},
        //     'sameComp':true,
        //     'assetId':null
        //   }
        // ];
    //     this.previousBreadCrumb =  [ {
    //       'name': 'DAM',
    //     'navigationPath': '/pages/dam',
    //   },{
    //    'name': 'Share Asset',
    //    'navigationPath': '/pages/dam/assets',
    //    'id':{},
    //    'sameComp':true,
    //    'assetId':null
    //  }]
          // if(this.shareAssetService.tabId){
          //   this.tabId = this.shareAssetService.tabId
          //   if(this.tabId == 2){
          //     this.selectedIndex = 1
          //     }else{
          //       this.selectedIndex= 0
          //     }
          // }
          if(this.addAssetService.shareCatId){
            this.parentCatId = this.addAssetService.shareCatId
          }
          this.getApprovedAssetsNew(this.parentCatId)
        } else {
    //       this.breadcrumbArray = [{
    //         'name': 'DAM',
    //         'navigationPath': '/pages/dam',
    //       },{
    //         'name': 'Category Asset',
    //         'navigationPath': '/pages/dam/assets',
    //         'id':{},
    //         'sameComp':true,
    //         'assetId':null
    //       }
    //     ];
    //     this.previousBreadCrumb =  [ {'name': 'DAM',
    //     'navigationPath': '/pages/dam',
    //   },{
    //    'name': 'Category Asset',
    //    'navigationPath': '/pages/dam/assets',
    //    'id':{},
    //    'sameComp':true,
    //    'assetId':null
    //  }]
          // if(this.assetVersion.tabId){
          //   this.tabId = this.assetVersion.tabId
          //   if(this.tabId == 2){
          //     this.selectedIndex = 1
          //     }else{
          //       this.selectedIndex= 0
          //     }
          // }
          this.categoryId = this.addAssetService.categoryId;
          this.addAssetService.categoryId = this.categoryId;
          if (this.categoryId) {
            this.parentCatId = this.addAssetService.categoryId;
          }
          if(this.addAssetService.view){
            this.innerTabId = this.addAssetService.view
          }
          this.getAllAssetandCategories(this.parentCatId, this.roleId, this.tabId, this.innerTabId)
        }


      }
    // }

    
    

    //this.tenantId = this.assetservice.tenantId;

    // if(this.assetservice.dataFromAssetCategory){
    //     this.dataFromAssetCategory = true;
    //     this.getPendingAssets=false;
    //     this.getApprovedAssets=false;
    //     this.addAssetService.dataFromAssetCategory = true;
    // }else{
    //     this.dataFromAssetCategory = false;
    //     this.addAssetService.dataFromAssetCategory = false;
    // }

    if (this.assetservice.getPendingAssets) {
      this.getPendingAssets = true;
      this.pagename = "Approve Asset";
      this.header = {
        title: 'Approve Asset',
        btnsSearch: true,
        searchBar: false,
        dropdownlabel: ' ',
        drplabelshow: false,
        drpName1: '',
        drpName2: ' ',
        drpName3: '',
        drpName1show: false,
        drpName2show: false,
        drpName3show: false,
        btnName1: '',
        btnName2: '',
        btnName3: '',
        btnAdd: '',
        btnName1show: false,
        btnName2show: false,
        btnName3show: false,
        btnBackshow: true,
        btnAddshow: false,
        filter: false,
        showBreadcrumb:true,
        breadCrumbList:[
          {
            'name': 'DAM',
            'navigationPath': '/pages/dam',
          },
        ]
      };
      this.dataFromAssetCategory = false;
      this.addAssetService.dataFromAssetCategory = false;
      // this.getPendingAsset();

    } else if (this.assetservice.getApprovedAssets) {
      this.getApprovedAssets = true;
      this.pagename = "Share File";
      this.addAssetService.dataFromAssetCategory = false;
      if(this.addAssetService.shareCatId){
        this.parentCatId = this.addAssetService.shareCatId
      }
      this.getApprovedAssetsNew(this.parentCatId);
      
      // this.header = {
      //   title: 'Share Asset',
      //   btnsSearch: true,
      //   searchBar: true,
      //   placeHolder:'Search by category name',
      //   dropdownlabel: ' ',
      //   drplabelshow: false,
      //   drpName1: '',
      //   drpName2: ' ',
      //   drpName3: '',
      //   drpName1show: false,
      //   drpName2show: false,
      //   drpName3show: false,
      //   btnName1: '',
      //   btnName2: '',
      //   btnName3: '',
      //   btnAdd: '',
      //   btnName1show: false,
      //   btnName2show: false,
      //   btnName3show: false,
      //   btnBackshow: true,
      //   btnAddshow: false,
      //   filter: false,
      //   showBreadcrumb:true,
      //   breadCrumbList:[
      //     {
      //       'name': 'DAM',
      //       'navigationPath': '/pages/dam',
      //     },
      //   ]
      // };
    } else {
      this.getPendingAssets = false;
      this.getApprovedAssets = false
      this.dataFromAssetCategory = false;
      this.dataFromAssetCategory = true;
      this.addAssetService.dataFromAssetCategory = true;
      // this.categoryId = this.assetservice.categoryId;
      this.categoryId = this.addAssetService.categoryId;
      this.addAssetService.categoryId = this.categoryId;
      if(this.categoryId){
      this.parentCatId = this.addAssetService.categoryId;
      }
      // this.addAssetService.categoryId = this.parentCatId;
      console.log('this.categoryId', this.parentCatId);
      this.header = {
        title: 'Folder File',
        btnsSearch: true,
        searchBar: false,
        dropdownlabel: ' ',
        drplabelshow: false,
        drpName1: '',
        drpName2: ' ',
        drpName3: '',
        drpName1show: false,
        drpName2show: false,
        drpName3show: false,
        btnName1: '',
        // btnName2: 'Add Category',
        btnName3: '',
        btnAdd: 'Add File',
        btnName1show: false,
        btnName2show: false,
        btnName3show: false,
        btnBackshow: true,
        btnAddshow: true,
        filter: false,
        showBreadcrumb:true,
        breadCrumbList:[
          {
            'name': 'DAM',
            'navigationPath': '/pages/dam',
          },
        ]
      };
      // this.displayArray = this.asset;
      // this.getAllAsset();
      this.getAllAssetandCategories(this.parentCatId,this.roleId,this.tabId,this.innerTabId);


      //   location.onPopState(() => {
      //     this.toggleSidebar();
      //     // console.log('pressed back!');

      // });
    }



    this.search = {
      // assetName:''
    };

    this.searchTags = {
      // assetName:''
    };
    
    this.getAllTags();
    this.getAllAssetDropdown();
    this.getHelpContent();
    


  }

  getHelpContent() {
    return new Promise(resolve => {
      this.http1
        .get("../../../../../assets/help-content/addEditCourseContent.json")
        .subscribe(
          data => {
            this.helpContent = data;
            console.log("Help Array", this.helpContent);
          },
          err => {
            resolve("err");
          }
        );
    });
    // return this.helpContent;
  }

  selectedTabTest(event,item){
    console.log(event.index,"selectedTabTest")
    var parentCatId = null
    var roleId =  event.roleId
    var tabId = event.tabId
    // this.getAllAssetandCategories(parentCatId,roleId,tabId,innerTabID)

  }
  onSequenceChangeEvent(event: MatTabChangeEvent){
    this.skeleton = false
    console.log(event,"event")
    console.log('index => ', event.index);
    this.filter = false;
    var parentCatId = null
    var roleId =  this.damTabs[event.index].roleId
    var tabId = this.damTabs[event.index].tabId
    this.parentCatId = parentCatId,
    this.roleId = roleId,
    this.tabId = tabId
    this.selectedIndex = event.index
    if(this.tabId == 2){
    // this.selectedIndex = 1
    }
    else if(this.tabId == 3){
      // this.selectedIndex= 2
    }else{
      // this.selectedIndex= 0
    }
    if(this.roleId != 1){
      // this.cardModify['download'] = false
    }

    if(this.tabId == 2){
      this.breadcrumbArray =[{
        'name': 'DAM',
        'navigationPath': '/pages/dam',
      },]
      this.previousBreadCrumb =[{
        'name': 'DAM',
        'navigationPath': '/pages/dam',
      },
      {
        'name': 'Folder File',
        'navigationPath': '/pages/dam/assets',
        'id':{},
        'sameComp':true,
        'assetId':null
      }]
      // if(this.parentCatId == null ){
        this.header = {
          title: 'Folder File',
          btnsSearch: true,
          searchBar: true,
          placeHolder:'Search by keywords',
          dropdownlabel: ' ',
          drplabelshow: false,
          drpName1: '',
          drpName2: ' ',
          drpName3: '',
          drpName1show: false,
          drpName2show: false,
          drpName3show: false,
          btnName1: '',
          // btnName2: 'Add Category',
          btnName3: '',
          // btnAdd: 'Add Asset',
          btnName1show: false,
          btnName2show: false,
          btnName3show: false,
          btnBackshow: true,
          btnAddshow: false,
          filter: true,
          showBreadcrumb:true,
          breadCrumbList:this.breadcrumbArray
          // breadCrumbList:[
          //   {
          //     'name': 'DAM',
          //     'navigationPath': '/pages/dam',
          //   },
          // ]
        };
    }else{
      this.breadcrumbArray =[{
        'name': 'DAM',
        'navigationPath': '/pages/dam',
      },]
      this.previousBreadCrumb =[{
        'name': 'DAM',
        'navigationPath': '/pages/dam',
      },
      {
        'name': 'Folder File',
        'navigationPath': '/pages/dam/assets',
        'id':{},
        'sameComp':true,
        'assetId':null
      }]
      this.header = {
        title: 'Folder File',
        btnsSearch: true,
        searchBar: false,
        // placeHolder:'Search by Category',
        placeHolder:'Search by keywords',
        dropdownlabel: ' ',
        drplabelshow: false,
        drpName1: '',
        drpName2: ' ',
        drpName3: '',
        drpName1show: false,
        drpName2show: false,
        drpName3show: false,
        btnName1: '',
        btnName2: 'Add Folder',
        btnName3: '',
        // btnAdd: 'Add Asset',
        btnName1show: false,
        btnName2show: true,
        btnName3show: false,
        btnBackshow: true,
        btnAddshow: false,
        filter: false,
        showBreadcrumb:true,
        breadCrumbList:this.breadcrumbArray
        // breadCrumbList:[
        //   {
        //     'name': 'DAM',
        //     'navigationPath': '/pages/dam',
        //   },
        // ]
      };
    }

    if(this.assetservice.getApprovedAssets){
      this.getApprovedAssetsNew(this.parentCatId)
      }else{
      this.getAllAssetandCategories(parentCatId,roleId,tabId,this.innerTabId)
      }
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    this.layoutService.changeLayoutSize();
    return false;
  }

  back() {
    this.toggleSidebar();
    console.log('this.dataFromAssetCategory', this.dataFromAssetCategory);
    if (this.dataFromAssetCategory) {
      console.log('this.dataFromAssetCategory1', this.dataFromAssetCategory);
      this.router.navigate(['../../asset-category'], { relativeTo: this.routes });
    } else {
      console.log('this.dataFromAssetCategory2', this.dataFromAssetCategory);
      this.router.navigate(['../../dam'], { relativeTo: this.routes });
    }


  }
  back1(event) {
    this.noFilterAsset = false
    this.skeleton = false
    this.filter = false;
    this.SelectedCategoryLength = {}
    console.log('back pressed',event);
    console.log('this.oldParentCatId', this.oldParentCatId);
    // this.parentCatId = this.oldParentCatId
    if(this.parentCatId == null) {
      this.router.navigate(['/pages/dam']);

    } else {
      var index = this.breadcrumbArray.length - 1
      // for(let i =0 ;i<this.breadcrumbArray.length;i++){
        this.title = this.breadcrumbArray[index].name
      // }
      this.breadcrumbArray.pop()
      this.previousBreadCrumb.pop()
      this.getAllAssetandCategories(this.oldParentCatId,this.roleId,this.tabId,this.innerTabId);
      this.parentCatId = this.oldParentCatId
      
      if(this.parentCatId == null && this.tabId == 1){
        this.breadcrumbArray = [{
          'name': 'DAM',
          'navigationPath': '/pages/dam',
        },
        // {
        //   'name': 'Category Asset',
        //   'navigationPath': '/pages/dam/assets',
        //   'id':{},
        //   'sameComp':true,
        //   'assetId':null
        // }
      ]
        this.previousBreadCrumb = [{
          'name': 'DAM',
          'navigationPath': '/pages/dam',
        },{
          'name': 'Folder File',
          'navigationPath': '/pages/dam/assets',
          'id':{},
          'sameComp':true,
          'assetId':null
        }]
        this.header = {
          title: 'Folder File',
          btnsSearch: true,
          searchBar: true,
          placeHolder:'Search by keywords',
          dropdownlabel: 'Folder ',
          drplabelshow: true,
          drpName1: 'Upload Folder',
          drpName2: ' Add Folder',
          drpName3: '',
          drpName1show: true,
          drpName2show: true,
          drpName3show: false,
          folder:true,
          dropdownlabel1:'File',
          drplabel1show:false,
          drpName_1:'Add Folder',
          drpName_2: ' ',
          drpName_1show: true,
          drpName_2show: false,
          btnName1: '',
          btnName2: 'Add Folder',
          btnName3: 'Upload Folder',
          // btnAdd: 'Add Asset',
          btnName1show: false,
          btnName2show: false,
          btnName3show: false,
          btnBackshow: true,
          btnAddshow: false,
          filter: false,
          showBreadcrumb:true,
          breadCrumbList:this.breadcrumbArray
          // breadCrumbList:[
          //   {
          //     'name': 'DAM',
          //     'navigationPath': '/pages/dam',
          //   },
          // ]
        };
      }
      else if (this.parentCatId == null && this.tabId ==2){
        this.breadcrumbArray = [{
          'name': 'DAM',
          'navigationPath': '/pages/dam',
        },
        // {
        //   'name': 'Category Asset',
        //   'navigationPath': '/pages/dam/assets',
        //   'id':{},
        //   'sameComp':true,
        //   'assetId':null
        // }
      ]
        this.previousBreadCrumb = [{
          'name': 'DAM',
          'navigationPath': '/pages/dam',
        },{
          'name': 'Folder File',
          'navigationPath': '/pages/dam/assets',
          'id':{},
          'sameComp':true,
          'assetId':null
        }]
        // if(this.tabId == 2){
          // if(this.parentCatId == null ){
            this.header = {
              title: 'Folder File',
              btnsSearch: true,
              searchBar: true,
              placeHolder:'Search by keywords',
              dropdownlabel: ' ',
              drplabelshow: false,
              drpName1: '',
              drpName2: ' ',
              drpName3: '',
              drpName1show: false,
              drpName2show: false,
              drpName3show: false,
              btnName1: '',
              btnName2: 'Add Folder',
              // btnName3: 'File Upload',
              // btnAdd: 'Add Asset',
              btnName1show: false,
              btnName2show: false,
              btnName3show: false,
              btnBackshow: true,
              btnAddshow: false,
              filter: false,
              showBreadcrumb:true,
              breadCrumbList:this.breadcrumbArray
              // breadCrumbList:[]
            };
      }
      else if(this.parentCatId != null && this.tabId == 1){
        this.header = {
          // title: 'Category Asset',
          title:this.title,
          btnsSearch: true,
          searchBar: true,
          placeHolder:'Search by keywords',
          dropdownlabel: 'Folder ',
          drplabelshow: true,
          drpName1: 'Upload Folder',
          drpName2: 'Upload File',
          drpName3: '',
          drpName1show: true,
          drpName2show: true,
          drpName3show: false,
          folder:true,
          file:true,
          dropdownlabel1:'File',
        drplabel1show:true,
        drpName_1:'Add Folder',
        drpName_2: 'Add File ',
        drpName_1show: true,
        drpName_2show: true,
          btnName1: '',
          btnName2: 'Add Folder',
          btnName3: 'Upload Folder',
          btnName4: 'Upload File',
          btnAdd: 'Add File',
          btnName1show: false,
          btnName2show: false,
          btnName3show: false,
          // btnName4show: true,
          btnName4show: false,
          btnBackshow: true,
          btnAddshow: false,
          filter: true,
          showBreadcrumb:true,
          breadCrumbList:this.breadcrumbArray
          // breadCrumbList:[
          //   {
          //     'name': 'DAM',
          //     'navigationPath': '/pages/dam',
          //   },
          // ]
        };
      }
      else if(this.tabId == 3){
        this.header = {
          title: 'Folder File',
          btnsSearch: true,
          searchBar: true,
          placeHolder:'Search by keywords',
          dropdownlabel: ' ',
          drplabelshow: false,
          drpName1: '',
          drpName2: ' ',
          drpName3: '',
          drpName1show: false,
          drpName2show: false,
          drpName3show: false,
          btnName1: '',
          btnName2: 'Add Folder',
          // btnName3: 'File Upload',
          btnAdd: 'Add File',
          btnName1show: false,
          btnName2show: false,
          btnName3show: false,
          btnBackshow: true,
          btnAddshow: false,
          filter: false,
          showBreadcrumb:true,
          breadCrumbList:this.breadcrumbArray
          // breadCrumbList:[]
        };
      }
      else if(this.parentCatId == null && this.tabId == 4){
        this.breadcrumbArray = [{
          'name': 'DAM',
          'navigationPath': '/pages/dam',
        },
        // {
        //   'name': 'Category Asset',
        //   'navigationPath': '/pages/dam/assets',
        //   'id':{},
        //   'sameComp':true,
        //   'assetId':null
        // }
      ]
        this.previousBreadCrumb = [{
          'name': 'DAM',
          'navigationPath': '/pages/dam',
        },{
          'name': 'Folder File',
          'navigationPath': '/pages/dam/assets',
          'id':{},
          'sameComp':true,
          'assetId':null
        }]
        this.header = {
          title: 'Folder File',
          btnsSearch: true,
          searchBar: true,
          placeHolder:'Search by keywords',
          dropdownlabel: 'Folder ',
          drplabelshow: true,
          drpName1: 'Upload Folder',
          drpName2: 'Add Folder ',
          drpName3: '',
          drpName1show: true,
          drpName2show: true,
          drpName3show: false,
          folder:true,
          dropdownlabel1:'Add',
          drplabel1show:false,
          drpName_1:'Add Folder',
          drpName_2: ' ',
          drpName_1show: true,
          drpName_2show: false,
          btnName1: '',
          btnName2: 'Add Folder',
          btnName3: 'Upload Folder',
          // btnAdd: 'Add Asset',
          btnName1show: false,
          btnName2show: false,
          btnName3show: false,
          btnBackshow: true,
          btnAddshow: false,
          filter: false,
          showBreadcrumb:true,
          breadCrumbList:this.breadcrumbArray
          // breadCrumbList:[
          //   {
          //     'name': 'DAM',
          //     'navigationPath': '/pages/dam',
          //   },
          // ]
        };
      }
      else if(this.parentCatId != null && this.tabId == 4 ){
          this.header = {
            // title: 'Category Asset',
            title:this.title,
            btnsSearch: true,
            searchBar: true,
            placeHolder:'Search by keywords',
            dropdownlabel: 'Folder ',
            drplabelshow: true,
            drpName1: 'Upload Folder',
            drpName2: ' Upload File',
            drpName3: '',
            drpName1show: true,
            drpName2show: true,
            drpName3show: false,
            folder:true,
            file:true,
            dropdownlabel1:'File',
            drplabel1show:true,
            drpName_1:'Add Folder',
            drpName_2: 'Add File ',
            drpName_1show: true,
            drpName_2show: true,
            btnName1: '',
            btnName2: 'Add Folder',
            btnName3: 'Upload Folder',
            btnName4: 'Upload File',
            btnAdd: 'Add File',
            btnName1show: false,
            btnName2show: false,
            btnName3show: false,
            // btnName4show:true,
            btnName4show: false,
            btnBackshow: true,
            btnAddshow: false,
            filter: true,
            showBreadcrumb:true,
            breadCrumbList:this.breadcrumbArray
            // breadCrumbList:[
            //   {
            //     'name': 'DAM',
            //     'navigationPath': '/pages/dam',
            //   },
            // ]
          };
        }
      // else if(this.parentCatId != null && this.tabId == 4 && this.addAssetService.view == 1){
      //   this.header = {
      //     title: 'Category Asset',
      //     btnsSearch: true,
      //     searchBar: true,
      //     placeHolder:'Search by Category name',
      //     dropdownlabel: ' ',
      //     drplabelshow: false,
      //     drpName1: '',
      //     drpName2: ' ',
      //     drpName3: '',
      //     drpName1show: false,
      //     drpName2show: false,
      //     drpName3show: false,
      //     btnName1: '',
      //     btnName2: 'Add Folder',
      //     btnName3: 'Upload Folder',
      //     btnName4: 'Upload File',
      //     btnAdd: 'Add File',
      //     btnName1show: false,
      //     btnName2show: true,
      //     btnName3show: true,
      //     btnBackshow: true,
      //     btnAddshow: true,
      //     filter: false,
      //     showBreadcrumb:true,
      //     breadCrumbList:[
      //       {
      //         'name': 'DAM',
      //         'navigationPath': '/pages/dam',
      //       },
      //     ]
      //   };
      // }
      // else if(this.parentCatId != null && this.tabId == 4 && this.addAssetService.view == 2){
      //   this.header = {
      //     title: 'Category Asset',
      //     btnsSearch: true,
      //     searchBar: true,
      //     placeHolder:'Search by Category name',
      //     dropdownlabel: ' ',
      //     drplabelshow: false,
      //     drpName1: '',
      //     drpName2: ' ',
      //     drpName3: '',
      //     drpName1show: false,
      //     drpName2show: false,
      //     drpName3show: false,
      //     btnName1: '',
      //     btnName2: 'Add Folder',
      //     btnName3: 'Folder Upload',
      //     // btnAdd: 'Add Asset',
      //     btnName1show: false,
      //     btnName2show: false,
      //     btnName3show: false,
      //     btnBackshow: true,
      //     btnAddshow: false,
      //     filter: false,
      //     showBreadcrumb:true,
      //     breadCrumbList:[
      //       {
      //         'name': 'DAM',
      //         'navigationPath': '/pages/dam',
      //       },
      //     ]
      //   };
      // }
      if((this.tabId == 4 || this.tabId == 1) && this.parentCatId == null){
        if(this.isVisible == 1){
          this.header.drplabel1show = false
          this.header.drplabelshow = true
        }else{
          // this.header.drplabel1show = true
          this.header.drplabelshow = false
    
        }
      }

      if((this.tabId == 4 || this.tabId == 1) && this.parentCatId != null){
        if(this.isVisible == 1){
          this.header.drplabel1show = true
          this.header.drplabelshow = true
        }else{
          this.header.drplabel1show = false
          this.header.drplabelshow = false
    
        }
      }
    }
  }
  back2(){
    this.noFilterAsset = false
    this.filter = false;
    this.SelectedCategoryLength = {}
    this.skeleton = false
    console.log('back pressed',event);
    console.log('this.oldParentCatId', this.oldParentCatId);
    // this.parentCatId = this.oldParentCatId
    if(this.parentCatId == null) {
      this.router.navigate(['/pages/dam']);

    } else {
      var index = this.breadcrumbArray.length - 1
      // for(let i =0 ;i<this.breadcrumbArray.length;i++){
        this.title = this.breadcrumbArray[index].name
      // }
      this.breadcrumbArray.pop()
      this.previousBreadCrumb.pop()
      this.getApprovedAssetsNew(this.oldParentCatId);
      this.parentCatId = this.oldParentCatId

  }
}
fileUpload(){
  this.addAssetService.tabId = this.tabId
  this.addAssetService.roleId = this.roleId
  this.addAssetService.selectedIndex = this.selectedIndex
  this.bulkUploadService.parCatId = this.parentCatId
  this.addAssetService.categoryId = this.parentCatId
  this.bulkUploadService.type = 2
  this.type = 2
  this.addAssetService.title = this.title
  this.addAssetService.breadCrumbArray = this.breadcrumbArray
  this.addAssetService.previousBreadCrumb = this.previousBreadCrumb
  let element:HTMLElement = document.getElementById('checkFile') as HTMLElement;
  element.click()
  // this.fileInput.nativeElement
  // this.router.navigate(['file-upload'], { relativeTo: this.routes });
}

folderUpload(){
  this.addAssetService.tabId = this.tabId
  this.addAssetService.roleId = this.roleId
  this.addAssetService.selectedIndex = this.selectedIndex
  this.bulkUploadService.parCatId = this.parentCatId
  this.addAssetService.categoryId = this.parentCatId
  this.bulkUploadService.type = 1
  this.type = 1
  this.addAssetService.title = this.title
  this.addAssetService.breadCrumbArray = this.breadcrumbArray
  this.addAssetService.previousBreadCrumb = this.previousBreadCrumb
  let element:HTMLElement = document.getElementById('folderFile') as HTMLElement;
  element.click()
  // this.router.navigate(['file-upload'], { relativeTo: this.routes });
}

onSelectFile(event) {
  console.log(event.target.files[0],"file Extension")
  var validExts
  if(this.type == 1){
   validExts = new Array( "zip" );
  }else{
  validExts = this.formatExtensions

  //   validExts = new Array('video','audio','application/vnd.ms-powerpoint','application/vnd.openxmlformats-officedocument.presentationml.presentation',
  //   'pdf','image','audio',"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/vnd.ms-excel",
  //   "application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document")
  }
  // var fileType = event.target.files[0].type;
  var fileType = event.target.files[0].name.split('.')[1];
  
  // extension.value = inputfile.value.split('.')[1];

  var fileExt = false;
  for(let i=0; i<validExts.length; i++){
    if(this.type == 2 ){
    if(fileType == validExts[i].extension){
      
      fileExt = true;
      break;
    }
  }
  if(this.type == 1 ){
    if(fileType == validExts[i]){
      
      fileExt = true;
      break;
    }
  }
  }
  // if(validExts.indexOf(fileType) < 0) {
  if(!fileExt) {
    // var toast : Toast = {
    //   type: 'error',
    //   title: "Invalid file selected!",
    //   body: "Valid files are of " + validExts.toString() + " types.",
    //   showCloseButton: true,
    //   timeout: 2000
    // };
    // this.toasterService.pop(toast);
    // this.presentToast('warning', 'Valid file types are ' + validExts.toString());
  if(this.type == 1){
    this.presentToast('warning', 'Please select only zip folder');
  }else{
    this.presentToast('warning', 'Please select a valid file type');
  }
  }else{
  this.fileUrl = []
  var file
  this.file = event.target.files[0]
  this.fileName = event.target.files[0].name
  this.folderPopupshow = true
  // this.file =file
  console.log(this.file,"files")
  for (let i = 0; i < event.target.files.length; i++) {
      // const file = file[i];
      // var dummy = file[i]
      // var path = dummy.webkitRelativePath.split('/');
    // upload file using path
    var reader = new FileReader();
    reader.onload = (event:any) => {
      // console.log(event.target.result);
      this.fileUrl.push(event.target.result);
      console.log(this.fileUrl,"fileUrl")
    }
    reader.readAsDataURL(event.target.files[i]);
  }
  // console.log(path,"path")
  console.log(this.file,"fileUrl")
  this.upload()
}
}

upload() {
  this.enableFileUploadModal = true
  var content = {name: 'Bhavesh'};
  var param = {
    parCatId:this.bulkUploadService.parCatId
  }
  this.progress=1
  // let param = this.bulkUploadService.parCatId?this.bulkUploadService.parCatId:null
  var fd = new FormData();
  // fd.append('content', JSON.stringify(content));
  fd.append('file', this.file);
  fd.append('fileType',this.type)
  // fd.append('reportProgress','true')
  fd.append('parCatId',this.bulkUploadService.parCatId);
  // fd.append("data", JSON.stringify(fd));
  const url = webApi.domain + webApi.url.insertAssetBulkUploadNew;
  // let headers = new HttpHeaders();
  // headers = headers.set('Content-Type', 'multipart/form-data');
  let options = {
    search:{},
    reportProgress: true,
};
  this.http1
    .post(url,
       fd, {
        reportProgress: true,
        observe: 'events'
    })
    .pipe(
      map((event: any) => {
        if (event.type == HttpEventType.UploadProgress) {
          // this.progress = Math.round((100 / event.total) * event.loaded);
          if(this.progress < 90){
            // this.spinner.show();
            // this.cdf.detectChanges();
            this.progress = Math.round((100 / event.total) * event.loaded);
          }
        } else if (event.type == HttpEventType.Response) {
          this.progress = null;
          // this.spinner.hide();
          // this.cdf.detectChanges();
          this.progress = 100;
          this.enableFileUploadModal = false
          if (event['body']['type'] == true) {
            this.file = ''
            // this.fileName = 'Click here to upload File'
            this.folderPopupshow = false;
            if(this.type == 1){
              this.fileName = 'Click here to upload Folder'
            }
            else{
              this.fileName = 'Click here to upload File'
            }
            // const toast: Toast = {
            //   type: 'success',
            //   title: 'Bulk Upload.',
            //   body: 'Assets uploaded successfully.',
            //   showCloseButton: true,
            //   timeout: 2000,
            // };
            // this.spinner.hide();
            // this.toasterService.pop(toast);
            setTimeout(() => {
            this.presentToast('success', event['body']['message']);              
            }, 500);
            this.getAllAssetandCategories(this.parentCatId,this.roleId,this.tabId,this.innerTabId)
            // this.router.navigate(['../'],{relativeTo:this.routes});
          } else {
            // var toast: Toast = {
            //   type: 'error',
            //   title: 'Bulk Upload.',
            //   body: 'Unable to upload assets.',
            //   showCloseButton: true,
            //   timeout: 2000,
            // };
            // this.spinner.hide();
            // this.toasterService.pop(toast);
            this.presentToast('error', '');
          }
        }
      }),
      catchError((err: any) => {
        this.spinner.hide();
        this.cdf.detectChanges();
        this.progress = null;
        this.enableFileUploadModal = false
        // alert(err.message);
        return throwError(err.message);
      })
    )
    .toPromise();
}
  modalName:any
  clickTodisableCat(data,identifier) {
    console.log(data,"data")
    // this.modalName = 'Folder'
    this.modalName = data.assetName
    this.identifier = identifier

    if(data.visible==0){
      this.changetitle='Enable';

    }
    else{
      this.changetitle='Disable';
    }
    if (data.cnt > 0) {
      this.presentToast('warning', 'Folder have files, not able to hide this folder');
    } else {
      this.enableDisableCategoryModal = true;
      this.categoryData = data;
      this.storedVisible = data;

    }
  }

  enableDisableActionCat(status) {
    console.log(status);

    // kv
    if (status == true) {
      if (this.storedVisible.visible == 0) {
        this.catVisibleStatus = 1;
        this.storedVisible.disable = 1;
        // this.changetitle = 'Enable';
        this.msg = "Folder Enabled Successfully";
      } else {
        this.catVisibleStatus = 0;
        this.storedVisible.disable = 0;
        // this.changetitle = 'Disable';
        this.msg = "Folder Disabled Successfully";
      }

    // kv
      this.changeCategoryStatus();
    } else {
      this.enableDisableCategoryModal = false;
    }
  }
  // changeCategoryStatus(catId, catVisible) {
  changeCategoryStatus() {
    this.spinner.show();
    // let param = {
    //   "catId": catId,
    //   "catVisible": catVisible,
    //   "tId": this.tenantId
    // }
    let param = {
      "catId": this.categoryData.assetId,
      "catVisible": this.catVisibleStatus,
      "tId": this.tenantId
    }
    const _urlChangeAssetCateorystatus:string = webApi.domain + webApi.url.changeAssetCategoryStatus;
    this.commonFunctionService.httpPostRequest(_urlChangeAssetCateorystatus,param)
    //this.assetcategoryservice.changeAssetCategoryStatus(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('UpdateCategoryStatusResponse:', rescompData);
        if (result['type'] == true) {
          // var catUpdate: Toast = {
          //   type: 'success',
          //   title: "Asset category Updated!",
          //   body: "Asset updated successfully.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('success', this.msg);
          this.getAllAssetandCategories(this.parentCatId,this.roleId,this.tabId,this.innerTabId);
          this.enableDisableCategoryModal = false;
        } else {
          // var catUpdate: Toast = {
          //   type: 'error',
          //   title: "Asset category Updated!",
          //   body: "Unable to update Asset Category.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
        }
      }, error => {
        this.spinner.hide();
        // var toast: Toast = {
        //   type: 'error',
        //   //title: "Server Error!",
        //   body: "Something went wrong.please try again later.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
  }
  

  // clear() {
  //   this.search = {};
  //   this.searchTags = {};
  // };

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }

  // noTags:boolean=false;
  // getAllTags() {
  //    this.spinner.show();
  //   // const param = {
  //   //   'aId': 25,
  //   //   'tId': this.tenantId,
  //   //   'cId': 0,
  //   // }
  //   this.assetservice.getAllTags(param)
  //     .then(rescompData => {
  //        this.spinner.hide();
  //       var result = rescompData;
  //       console.log('getTagsResponse:', rescompData);
  //       if (result['type'] == true) {
  //         if (result['data'][0].length == 0) {
  //           this.noDataFound = true;
  //           this.skeleton = true
  //         } else {
  //           const tagArr = result['data'][0];
  //           for (let i = 0; i < tagArr.length; i++) {
  //             tagArr[i].status = false;
  //           }
  //           this.Tags = tagArr;
  //           console.log('this.Tags', this.Tags);
  //         }
  //       } else {
  //         this.spinner.hide();
  //         this.skeleton = true
  //         this.noDataFound = true;
  //         // var toast: Toast = {
  //         //   type: 'error',
  //         //   //title: "Server Error!",
  //         //   body: 'Something went wrong.please try again later.',
  //         //   showCloseButton: true,
  //         //   timeout: 2000
  //         // };
  //         // this.toasterService.pop(toast);
  //         this.presentToast('error', '');
  //       }
  //     }, error => {
  //       // this.spinner.hide();
  //       this.skeleton = true
  //       this.noDataFound = true;
  //       // var toast: Toast = {
  //       //   type: 'error',
  //       //   //title: "Server Error!",
  //       //   body: 'Something went wrong.please try again later.',
  //       //   showCloseButton: true,
  //       //   timeout: 2000
  //       // };
  //       // this.toasterService.pop(toast);
  //       this.presentToast('error', '');
  //     });
  // }
  getAllTags() {
    this.spinner.show();
   // const param = {
   //   'aId': 25,
   //   'tId': this.tenantId,
   //   'cId': 0,
   // }
   let param = {
     tenantId : this.tenantId
   };
   this.assetservice.getAllTags(param)
     .then(rescompData => {
        this.spinner.hide();
        var temp: any = rescompData;
       //  this.tagList = temp.data;
       console.log('getTagsResponse:', rescompData);

           const tagArr = temp.data;
           for (let i = 0; i < tagArr.length; i++) {
             tagArr[i].status = false;
           }
           this.Tags = tagArr;
           this.bindfilter(this.Tags);
           console.log('this.Tags', this.Tags);

     }, error => {
       // this.spinner.hide();
       this.skeleton = true
       this.noDataFound = true;
       // var toast: Toast = {
       //   type: 'error',
       //   //title: "Server Error!",
       //   body: 'Something went wrong.please try again later.',
       //   showCloseButton: true,
       //   timeout: 2000
       // };
       // this.toasterService.pop(toast);
       this.presentToast('error', '');
     });
 }

  noAsset: boolean = false;
  // asset: any;
  noDataFound: boolean = false;
  getAllAsset() {
    // this.spinner.show();
    let param = {
      'tId': this.tenantId,
      'catId': this.categoryId
    }

    this.assetservice.getAllAsset(param)
      .then(rescompData => {
        // this.spinner.hide();
        var result = rescompData;
        console.log('getAssetResponse:', rescompData);
        if (result['type'] == true) {
          if (result['data'][0].length == 0) {
            this.noAsset = true;
            this.skeleton = true
          } else {
            // this.noAsset = false;
            // this.skeleton = true;
            // this.noDataFound = false;
            this.asset = result['data'][0];
            for (let i = 0; i < this.asset.length; i++) {
              if (this.asset[i].tags) {
                this.asset[i].tags = this.asset[i].tags.split(',');
              }
            }
            if(this.asset.length==0){
            this.noDataFound = true;
            this.noDataVal={
              margin:'mt-3',
              imageSrc: '../../../../../assets/images/no-data-bg.svg',
              // title:"No Asset at this time.",
    title:"No file or folder at this time.",
              desc:"",
              titleShow:true,
              btnShow:true,
              descShow:false,
              btnText:'Learn More',
              btnLink:'https://faq.edgelearning.co.in/kb/how-to-add-an-asset-in-the-dam',
            }
            }else{
            this.noDataFound = false;
            }
            // this.displayArray = this.asset;
            this.displayArray = []
            this.addItems(0, this.sum, 'push', this.asset);

            // for(let i = 0; i < this.displayArray.length; i++ ) {
            //   this.displayArray[i].disable = this.displayArray[i].visible;
            //   this.displayArray[i].discrption = this.displayArray[i].description;
            //   this.displayArray[i].title = this.displayArray[i].assetName;
            //   if(this.displayArray[i].formatId == 1 || this.displayArray[i].formatId == 4 || this.displayArray[i].formatId == 6 || this.displayArray[i].formatId == 7 || this.displayArray[i].formatId == 8) {
            //     this.displayArray[i].image = 'assets/images/videoNew.png';
            //   } else if(this.displayArray[i].formatId == 2 ){
            //     this.displayArray[i].image = 'assets/images/audioNew.png';
            //   } else if(this.displayArray[i].formatId == 3 || this.displayArray[i].formatId == 5) {
            //     this.displayArray[i].image = 'assets/images/pdfNew.png';
            //   } else if(this.displayArray[i].formatId == 10) {
            //     this.displayArray[i].image = 'assets/images/ppt.jpg';
            //   } else if(this.displayArray[i].formatId == 11) {
            //     this.displayArray[i].image = 'assets/images/excel.jpg';
            //   } else if(this.displayArray[i].formatId == 12) {
            //     this.displayArray[i].image = 'assets/images/doc.jpg';
            //   }
            // }

            this.noDataFound = false;
            this.skeleton = true;
            this.noAsset = false;
            console.log('this.asset', this.asset);
            console.log('this.displayArray', this.displayArray);
            // this.cdf.detectChanges();
          }
        } else {
          this.spinner.hide();
          this.skeleton = true
          this.noDataFound = true;
          // var toast: Toast = {
          //   type: 'error',
          //   //title: "Server Error!",
          //   body: 'Something went wrong.please try again later.',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }
        //this.skeleton = true

      }, error => {
        // this.spinner.hide();
        this.skeleton = true
        this.noDataFound = true;
        // var toast: Toast = {
        //   type: 'error',
        //   //title: "Server Error!",
        //   body: 'Something went wrong.please try again later.',
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
  }

  getAllAssetandCategories(parent,roleId,tabId,inner){
    if(tabId==3){
      this.cardModify['option'] = false
      parent = 0
    }else if(tabId!=3){
      this.cardModify['option'] = true
    }
    // else if(this.tabId == 4 && this.addAssetService.view == 3){

    //   parent = null
    //   this.parentCatId = null
    // }
    
    else{
      parent = parent
    }
     if(this.tabId == 4 && this.addAssetService.view == 3){

      parent = null
      this.parentCatId = null
    }

    let param = {
      // tenantId: this.tenantId,
      parentCatId: parent,
      roleId : roleId,
      tabId:tabId,
      innerTabId:inner
    }
    if(this.tabId==1){
    if(this.parentCatId == null ){
      this.header = {
        title: 'Folder File',
        // title:this.title,
        btnsSearch: true,
        searchBar: true,
        placeHolder:'Search by keywords',
        dropdownlabel: 'Folder ',
        drplabelshow: true,
        drpName1: 'Upload Folder',
        drpName2: 'Add Folder ',
        drpName3: '',
        folder:true,
        drpName1show: true,
        drpName2show: true,
        drpName3show: false,
        dropdownlabel1:'Add',
        drplabel1show:false,
        drpName_1:'Add Folder',
        drpName_2: ' ',
        drpName_1show: true,
        drpName_2show: false,
        btnName1: '',
        btnName2: 'Add Folder',
        btnName3: 'Upload Folder',
        // btnAdd: 'Add Asset',
        btnName1show: false,
        btnName2show: false,
        btnName3show: false,
        btnBackshow: true,
        btnAddshow: false,
        filter: false,
        showBreadcrumb:true,
        breadCrumbList:[
          {
            'name': 'DAM',
            'navigationPath': '/pages/dam',
          },
        ]
      };
    }else{
      this.header = {
        // title: 'Category Asset',
        title:this.title,
        btnsSearch: true,
        searchBar: true,
        dropdownlabel: 'Folder ',
        drplabelshow: true,
        drpName1: 'Upload Folder',
        drpName2: 'Add Folder ',
        drpName3: '',
        drpName1show: true,
        drpName2show: true,
        drpName3show: false,
        folder:true,
        file:true,
        dropdownlabel1:'File',
        drplabel1show:true,
        drpName_1:'Upload File',
        drpName_2: 'Add File ',
        drpName_1show: true,
        drpName_2show: true,
        btnName1: '',
        btnName2: 'Add Folder',
        btnName3: 'Upload Folder',
        btnName4: 'Upload File',
        btnAdd: 'Add File',
        btnName1show: false,
        btnName2show: false,
        btnName3show: false,
        // btnName4show: true,
        btnName4show: false,
        btnBackshow: true,
        btnAddshow: false,
        filter: true,
        showBreadcrumb:true,
        breadCrumbList:[
          {
            'name': 'DAM',
            'navigationPath': '/pages/dam',
          },
        ]
      }
      console.log(this.breadcrumbArray)
      this.header.breadCrumbList =  this.breadcrumbArray
    }
  }else if(this.tabId == 2){
    if(this.parentCatId == null ){
    this.header = {
      title: 'Folder File',
      // title:this.title,
      btnsSearch: true,
      searchBar: true,
      placeHolder:'Search by keywords',
      dropdownlabel: ' ',
      drplabelshow: false,
      drpName1: '',
      drpName2: ' ',
      drpName3: '',
      drpName1show: false,
      drpName2show: false,
      drpName3show: false,
      btnName1: '',
      btnName2: 'Add Folder',
      // btnName3: 'File Upload',
      // btnAdd: 'Add Asset',
      btnName1show: false,
      btnName2show: false,
      btnName3show: false,
      btnBackshow: true,
      btnAddshow: false,
      filter: false,
      showBreadcrumb:true,
      breadCrumbList:[
        {
          'name': 'DAM',
          'navigationPath': '/pages/dam',
        },
      ]
    };
    // this.header.breadCrumbList =  this.breadcrumbArray
    }else{
      this.header = {
        // title: 'Category Asset',
        title:this.title,
        btnsSearch: true,
        searchBar: true,
        placeHolder:'Search by keywords',
        dropdownlabel: ' ',
        drplabelshow: false,
        drpName1: '',
        drpName2: ' ',
        drpName3: '',
        drpName1show: false,
        drpName2show: false,
        drpName3show: false,
        btnName1: '',
        btnName2: 'Add Folder',
        // btnName3: 'File Upload',
        // btnAdd: 'Add Asset',
        btnName1show: false,
        btnName2show: false,
        btnName3show: false,
        btnBackshow: true,
        btnAddshow: false,
        filter: true,
        showBreadcrumb:true,
        breadCrumbList:[]

      };
      this.header.breadCrumbList =  this.breadcrumbArray
    }
  }else if(this.tabId == 3){
    this.header = {
      title: 'Folder File',
      // title: this.title,
      btnsSearch: true,
      searchBar: true,
      placeHolder:'Search by keywords',
      dropdownlabel: ' ',
      drplabelshow: false,
      drpName1: '',
      drpName2: ' ',
      drpName3: '',
      drpName1show: false,
      drpName2show: false,
      drpName3show: false,
      btnName1: '',
      btnName2: 'Add Folder',
      // btnName3: 'File Upload',
      // btnAdd: 'Add Asset',
      btnName1show: false,
      btnName2show: false,
      btnName3show: false,
      btnBackshow: true,
      btnAddshow: false,
      filter: false,
      showBreadcrumb:true,
      breadCrumbList:[
        {
          'name': 'DAM',
          'navigationPath': '/pages/dam',
        },
      ]

    };
    // this.header.breadCrumbList =  this.breadcrumbArray

  }
  // else if(this.tabId == 4){
  //   if(this.parentCatId == null ){
  //     this.header = {
  //       title: 'Category Asset',
  //       btnsSearch: true,
  //       searchBar: true,
  //       placeHolder:'Search by Category',
  //       dropdownlabel: ' ',
  //       drplabelshow: false,
  //       drpName1: '',
  //       drpName2: ' ',
  //       drpName3: '',
  //       drpName1show: false,
  //       drpName2show: false,
  //       drpName3show: false,
  //       btnName1: '',
  //       btnName2: 'Add Folder',
  //       btnName3: 'Upload Folder',
  //       // btnAdd: 'Add Asset',
  //       btnName1show: false,
  //       btnName2show: true,
  //       btnName3show: true,
  //       btnBackshow: true,
  //       btnAddshow: false,
  //       filter: false,
  //       showBreadcrumb:true,
  //       breadCrumbList:[
  //         {
  //           'name': 'DAM',
  //           'navigationPath': '/pages/dam',
  //         },
  //       ]
  //     };
  //     }
  // }
  else if(this.parentCatId == null && this.tabId == 4){
    this.header = {
      title: 'Folder File',
      btnsSearch: true,
      searchBar: true,
      placeHolder:'Search by keywords',
      dropdownlabel: 'Folder ',
      drplabelshow: true,
      drpName1: 'Upload Folder',
      drpName2: 'Add Folder ',
      drpName3: '',
      drpName1show: true,
      drpName2show: true,
      drpName3show: false,
      folder:true,
      dropdownlabel1:'File',
      drplabel1show:false,
      drpName_1:'Add Folder',
      drpName_2: ' ',
      drpName_1show: true,
      drpName_2show: false,
      btnName1: '',
      btnName2: 'Add Folder',
      btnName3: 'Upload Folder',
      // btnAdd: 'Add Asset',
      btnName1show: false,
      btnName2show: false,
      btnName3show: false,
      btnBackshow: true,
      btnAddshow: false,
      filter: false,
      showBreadcrumb:true,
      breadCrumbList:[
        {
          'name': 'DAM',
          'navigationPath': '/pages/dam',
        },
      ]
    };
  }
  else if(this.parentCatId != null && this.tabId == 4 ){
    this.header = {
      // title: 'Category Asset',
      title:this.title,
      btnsSearch: true,
      searchBar: true,
      placeHolder:'Search by keywords',
      dropdownlabel: 'Folder ',
      drplabelshow: true,
      drpName1: 'Upload Folder',
      drpName2: 'Add Folder ',
      drpName3: '',
      drpName1show: true,
      drpName2show: true,
      drpName3show: false,
      folder:true,
      file:true,
      dropdownlabel1:'File',
        drplabel1show:true,
        drpName_1:'Upload File',
        drpName_2: ' Add File',
        drpName_1show: true,
        drpName_2show: true,
      btnName1: '',
      btnName2: 'Add Folder',
      btnName3: 'Upload Folder',
      btnName4: 'Upload File',
      btnAdd: 'Add File',
      btnName1show: false,
      btnName2show: false,
      btnName3show: false,
      // btnName4show:true,
      btnName4show: false,
      btnBackshow: true,
      btnAddshow: false,
      filter: true,
      showBreadcrumb:true,
      breadCrumbList:[
        {
          'name': 'DAM',
          'navigationPath': '/pages/dam',
        },
      ]
    };
    this.header.breadCrumbList =  this.breadcrumbArray
  }
  // else if(this.parentCatId != null && this.tabId == 4 && this.addAssetService.view == 1){
  //   this.header = {
  //     title: 'Category Asset',
  //     btnsSearch: true,
  //     searchBar: true,
  //     placeHolder:'Search by Category name',
  //     dropdownlabel: ' ',
  //     drplabelshow: false,
  //     drpName1: '',
  //     drpName2: ' ',
  //     drpName3: '',
  //     drpName1show: false,
  //     drpName2show: false,
  //     drpName3show: false,
  //     btnName1: '',
  //     btnName2: 'Add Folder',
  //     btnName3: 'Upload Folder',
  //     btnName4: 'Upload File',
  //     btnAdd: 'Add File',
  //     btnName1show: false,
  //     btnName2show: true,
  //     btnName3show: true,
  //     btnBackshow: true,
  //     btnAddshow: true,
  //     filter: false,
  //     showBreadcrumb:true,
  //     breadCrumbList:[
  //       {
  //         'name': 'DAM',
  //         'navigationPath': '/pages/dam',
  //       },
  //     ]
  //   };
  // }
  // else if(this.parentCatId != null && this.tabId == 4 && this.addAssetService.view == 2){
  //   this.header = {
  //     title: 'Category Asset',
  //     btnsSearch: true,
  //     searchBar: true,
  //     placeHolder:'Search by Category name',
  //     dropdownlabel: ' ',
  //     drplabelshow: false,
  //     drpName1: '',
  //     drpName2: ' ',
  //     drpName3: '',
  //     drpName1show: false,
  //     drpName2show: false,
  //     drpName3show: false,
  //     btnName1: '',
  //     btnName2: 'Add Folder',
  //     btnName3: 'Folder Upload',
  //     // btnAdd: 'Add Asset',
  //     btnName1show: false,
  //     btnName2show: false,
  //     btnName3show: false,
  //     btnBackshow: true,
  //     btnAddshow: false,
  //     filter: false,
  //     showBreadcrumb:true,
  //     breadCrumbList:[
  //       {
  //         'name': 'DAM',
  //         'navigationPath': '/pages/dam',
  //       },
  //     ]
  //   };
  // }
  if((this.tabId == 4 || this.tabId == 1) && this.parentCatId == null){
    if(this.isVisible == 1){
      this.header.drplabel1show = false
      this.header.drplabelshow = true
    }else{
      // this.header.drplabel1show = true
      this.header.drplabelshow = false

    }
  }
  if((this.tabId == 4 || this.tabId == 1) && this.parentCatId != null){
    if(this.isVisible == 1){
      this.header.drplabel1show = true
      this.header.drplabelshow = true
    }else{
      this.header.drplabel1show = false
      this.header.drplabelshow = false

    }
  }

    this.assetservice.getAllAssetCategories(param)
      .then(rescompData => {
        // this.spinner.hide();
        var result = rescompData;
        console.log('getAssetResponse:', rescompData);
        if (result['type'] == true) {
          if (result['data'][0].length === 0) {
            this.oldParentCatId = null;
          } else {
            this.oldParentCatId = result['data'][0][0].parentCatId;
          }
          if (result['data'][1].length == 0) {
            this.noAsset = true;
            this.skeleton = true
            this.noFilterAsset =true
            this.spinner.hide()
          } else {

            this.asset = result['data'][1];
            this.demoasset  = result['data'][1];
            for (let i = 0; i < this.asset.length; i++) {
              if (this.asset[i].tags) {
                this.asset[i].tags = this.asset[i].tags.split(',');
              }
            }
            // this.displayArray = this.asset;
            this.displayArray = []
            this.addItems(0, this.sum, 'push', this.asset);

            
            this.noDataFound = false;
            this.skeleton = true;
            this.noAsset = false;
            console.log('this.asset', this.asset);
            console.log('this.displayArray', this.displayArray);
            for(let i = 0;i<this.displayArray.length;i++){
              if(this.displayArray[i].caFlag == 2){
                if(this.isAdmin == 0){
              if(this.displayArray[i].tabId == 3){
              this.cardModify['option'] = false
                
              }else if(this.displayArray[i].tabId == 2){
              this.cardModify['option'] = true
                this.cardModify['eyeIcon'] = false
                 this.cardModify['upgradeIcon'] = true;
                  this.cardModify['download'] = true;
                  
                }
                else if(this.displayArray[i].tabId == 1){
                  this.cardModify['option'] = true
                  this.cardModify['upgradeIcon'] = false;
                  this.cardModify['download'] = false;
                  this.cardModify['eyeIcon'] = true;

                    
                  }
              // else{
              //   this.cardModify['option'] =  true
              // }
              
            }
             else if(this.isAdmin == 1){
              if(this.displayArray[i].tabId == 3 && this.displayArray[i].statusId == 1){
                // this.cardModify['option'] = true
                this.cardModify['option'] = true
                this.cardModify['eyeIcon'] = false
                 this.cardModify['upgradeIcon'] = true;
                  this.cardModify['download'] = true;
                
              }
              else if(this.displayArray[i].tabId == 1 && this.displayArray[i].statusId == 1){
                // this.cardModify['option'] = true
                this.cardModify['option'] = true
                this.cardModify['eyeIcon'] = true
                 this.cardModify['upgradeIcon'] = true;
                  this.cardModify['download'] = true;
                
              }
            }
          }
          }
            // this.cdf.detectChanges();
          }
        } else {
          this.spinner.hide();
          this.skeleton = true
          this.noDataFound = true;
          this.noDataVal={
            margin:'mt-3',
            imageSrc: '../../../../../assets/images/no-data-bg.svg',
            // title:"No Asset at this time.",
    title:"No file or folder at this time.",

            desc:"",
            titleShow:true,
            btnShow:true,
            descShow:false,
            btnText:'Learn More',
            btnLink:'https://faq.edgelearning.co.in/kb/how-to-add-an-asset-in-the-dam',
          }
          // var toast: Toast = {
          //   type: 'error',
          //   //title: "Server Error!",
          //   body: 'Something went wrong.please try again later.',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }
        //this.skeleton = true

      }, error => {
        // this.spinner.hide();
        this.skeleton = true
        this.noDataFound = true;
        // var toast: Toast = {
        //   type: 'error',
        //   //title: "Server Error!",
        //   body: 'Something went wrong.please try again later.',
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });

      // })

  }
  getAssetVersions(id){
    this.spinner.show();
    let param = {
      
      'aId': id
    }

    this.assetservice.getAllAssetVersion(param)
      .then(rescompData => {
        var result = rescompData;
        console.log('versions:', result);
        if (result['type'] == true) {
          if (result['data'][0].length == 0) {
            this.noAsset = true;
            this.skeleton = true;
            // this.noDataFound = true;
            // this.presentToast('error', '');
          } else {
            this.assetVersion = result['data'][0];
            this.addAssetService.assetVersions = this.assetVersion
            

          }
          //this.skeleton = true;
        } else {
          // this.spinner.hide();
          // this.skeleton = true
          // this.noDataFound = true;
          
        }


      }, error => {
        // this.spinner.hide();
        // this.skeleton = true
        // this.noDataFound = true
        
        this.presentToast('error', '');
      });
  
  }

  assetForFilter = [];
  getPendingAsset() {
    this.spinner.show();
    let param = {
      'tId': this.tenantId,
      'assetStatus': 3
    }

    this.assetservice.getPendingAsset(param)
      .then(rescompData => {
        var result = rescompData;
        console.log('getPendingAssetResponse:', rescompData);
        if (result['type'] == true) {
          if (result['data'][0].length == 0) {
            this.noAsset = true;
            this.skeleton = true;
            // this.noDataFound = true;
            // this.presentToast('error', '');
          } else {
            this.asset = result['data'][0];
            for (let i = 0; i < this.asset.length; i++) {
              if (this.asset[i].tags) {
                this.asset[i].tags = this.asset[i].tags.split(',');
              }
            }
            this.displayArray =[]
            this.addItems(0, this.sum, 'push', this.asset);
            this.assetForFilter = this.asset;
            this.noDataFound = false;
            this.skeleton = true;
            this.noAsset = false;
            console.log('this.assetPending:', this.asset);
            // this.cdf.detectChanges();
            this.spinner.hide();
          }
          //this.skeleton = true;
        } else {
          // this.spinner.hide();
          this.skeleton = true
          this.noDataFound = true;
          // var toast: Toast = {
          //   type: 'error',
          //   //title: "Server Error!",
          //   body: 'Something went wrong.please try again later.',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
        }


      }, error => {
        // this.spinner.hide();
        this.skeleton = true
        this.noDataFound = true;
        // var toast: Toast = {
        //   type: 'error',
        //   //title: "Server Error!",
        //   body: 'Something went wrong.please try again later.',
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
  }
  conentHeight = '0px';
  offset: number = 100;
  ngOnInit() {
    this.conentHeight = '0px';
    const e = document.getElementsByTagName('nb-layout-column');
    console.log(e[0].clientHeight);
    // this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
    if (e[0].clientHeight > 1300) {
      this.conentHeight = '0px';
      this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
    } else {
      this.conentHeight = String(e[0].clientHeight) + 'px';
    }
  }

  sum = 40;
  addItems(startIndex, endIndex, _method, asset) {
    for (let i = startIndex; i < endIndex; ++i) {
      if (asset[i]) {
        this.displayArray[_method](asset[i]);
      } else {
        break;
      }

      // console.log("NIKHIL");
    }
  }
  // sum = 10;
  onScroll(event) {

    let element = this.myScrollContainer.nativeElement;
    // element.style.height = '500px';
    // element.style.height = '500px';
    // Math.ceil(element.scrollHeight - element.scrollTop) === element.clientHeight

    // if (element.scrollHeight - element.scrollTop - element.clientHeight < 5) {
    if((Math.ceil(element.scrollHeight - element.scrollTop) - element.clientHeight) < 30){
      if (this.newArray.length > 0) {
        if (this.newArray.length >= this.sum) {
          const start = this.sum;
          this.sum += 100;
          this.addItems(start, this.sum, 'push', this.newArray);
        } else if ((this.newArray.length - this.sum) > 0) {
          const start = this.sum;
          this.sum += this.newArray.length - this.sum;
          this.addItems(start, this.sum, 'push', this.newArray);
        }
      } else {
        if (this.asset.length >= this.sum) {
          const start = this.sum;
          this.sum += 100;
          this.addItems(start, this.sum, 'push', this.asset);
        } else if ((this.asset.length - this.sum) > 0) {
          const start = this.sum;
          this.sum += this.asset.length - this.sum;
          this.addItems(start, this.sum, 'push', this.asset);
        }
      }


      console.log('Reached End');
    }
  }

  getApprovedAsset() {
    // this.spinner.show();
    let param = {
      'tId': this.tenantId,
      'assetStatus': 1
    }

    this.assetservice.getApprovedAsset(param)
      .then(rescompData => {
        // this.spinner.hide();
        var result = rescompData;
        console.log('getApprovedAssetResponse:', rescompData);
        if (result['type'] == true) {
          if (result['data'][0].length == 0) {
            this.noAsset = true;
            this.skeleton = true;
          } else {
            // this.skeleton = true;
            // this.noAsset = false;
            // this.noDataFound = false;
            this.asset = result['data'][0];
            for (let i = 0; i < this.asset.length; i++) {
              if (this.asset[i].tags) {
                this.asset[i].tags = this.asset[i].tags.split(',');
              }
            }
            this.displayArray =[]
            this.addItems(0, this.sum, 'push', this.asset);
            // this.displayArray = this.asset;

            this.noDataFound = false;
            this.skeleton = true;
            this.noAsset = false;
            console.log('this.assetApproved:', this.asset);
            // this.cdf.detectChanges();
          }

        } else {
          this.spinner.hide();
          this.skeleton = true
          this.noDataFound = true;
          // var toast: Toast = {
          //   type: 'error',
          //   //title: "Server Error!",
          //   body: 'Something went wrong.please try again later.',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }


      }, error => {
        this.spinner.hide();
        this.skeleton = true
        this.noDataFound = true;
        // var toast: Toast = {
        //   type: 'error',
        //   //title: "Server Error!",
        //   body: 'Something went wrong.please try again later.',
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
  }

  getApprovedAssetsNew(id){

    let param = {
      'catId': id,
      'assetStat': 1,
      roleId: this.roleId,
      tabId:this.tabId,
    }
    if(id == null){
      // this.previousBreadCrumb = [{
      //   'name': 'DAM',
      //   'navigationPath': '/pages/dam',
      // },{
      //   'name': 'Share File',
      //   'navigationPath': '/pages/dam/assets',
      //   'id':{},
      //   'sameComp':true,
      //   'assetId':null
      // }]
      // this.breadcrumbArray = [{
      //   'name': 'DAM',
      //   'navigationPath': '/pages/dam',
      // },{
      //   'name': 'Share File',
      //   'navigationPath': '/pages/dam/assets',
      //   'id':{},
      //   'sameComp':true,
      //   'assetId':null
      // }]
    this.header = {
      title: 'Share File',
      btnsSearch: true,
      searchBar: true,
      placeHolder:'Search by keywords',
      dropdownlabel: ' ',
      drplabelshow: false,
      drpName1: '',
      drpName2: ' ',
      drpName3: '',
      drpName1show: false,
      drpName2show: false,
      drpName3show: false,
      btnName1: '',
      btnName2: '',
      btnName3: '',
      btnAdd: '',
      btnName1show: false,
      btnName2show: false,
      btnName3show: false,
      btnBackshow: true,
      btnAddshow: false,
      filter: false,
      showBreadcrumb:true,
      // breadCrumbList:this.breadcrumbArray
      breadCrumbList:[
        {
          'name': 'DAM',
          'navigationPath': '/pages/dam',
        },
      ]
    };
  }else{
    this.header = {
      // title: 'Share Asset',
      title:this.title,
      btnsSearch: true,
      searchBar: true,
      placeHolder:'Search by keywords',
      dropdownlabel: ' ',
      drplabelshow: false,
      drpName1: '',
      drpName2: ' ',
      drpName3: '',
      drpName1show: false,
      drpName2show: false,
      drpName3show: false,
      btnName1: '',
      btnName2: '',
      btnName3: '',
      btnAdd: '',
      btnName1show: false,
      btnName2show: false,
      btnName3show: false,
      btnBackshow: true,
      btnAddshow: false,
      filter: true,
      showBreadcrumb:true,
      // breadCrumbList:[
      //   {
      //     'name': 'DAM',
      //     'navigationPath': '/pages/dam',
      //   },
      // ]
    };
    this.header.breadCrumbList= this.breadcrumbArray
  }
  this.assetservice.getApprovedAssetNew(param)
  .then(rescompData => {
    // this.spinner.hide();
    var result = rescompData;
    console.log('getAssetResponse:', rescompData);
    if (result['type'] == true) {
      if (result['data'][0].length === 0) {
        this.oldParentCatId = null;
      } else {
        this.oldParentCatId = result['data'][0][0].parentCatId;
      }
      if (result['data'][1].length == 0) {
        this.noDataVal={
          margin:'mt-3',
          imageSrc: '../../../../../assets/images/no-data-bg.svg',
          // title:"No Asset Categories at this time.",
    title:"No file or folder at this time.",

          desc:"File Folders will appear after they are added by the admin. Folders are classification for managing the files",
          titleShow:true,
          btnShow:true,
          descShow:true,
          btnText:'Learn More',
          btnLink:'https://faq.edgelearning.co.in/kb/dam-how-to-create-new-asset-category',
        }
        this.noAsset = true;
        this.skeleton = true
        this.noFilterAsset = true
      } else {

        this.asset = result['data'][1];
        this.demoasset  = result['data'][1];
        for (let i = 0; i < this.asset.length; i++) {
          if (this.asset[i].tags) {
            this.asset[i].tags = this.asset[i].tags.split(',');
          }
        }
        // this.displayArray = this.asset;
        this.displayArray = []
        this.addItems(0, this.sum, 'push', this.asset);

        
        this.noDataFound = false;
        this.skeleton = true;
        this.noAsset = false;
        console.log('this.asset', this.asset);
        console.log('this.displayArray', this.displayArray);
        // this.cdf.detectChanges();
      }
    } else {
      this.spinner.hide();
      this.skeleton = true
      this.noDataFound = true;
      // var toast: Toast = {
      //   type: 'error',
      //   //title: "Server Error!",
      //   body: 'Something went wrong.please try again later.',
      //   showCloseButton: true,
      //   timeout: 2000
      // };
      // this.toasterService.pop(toast);
      this.presentToast('error', '');
    }
    //this.skeleton = true

  }, error => {
    // this.spinner.hide();
    this.skeleton = true
    this.noDataFound = true;
    // var toast: Toast = {
    //   type: 'error',
    //   //title: "Server Error!",
    //   body: 'Something went wrong.please try again later.',
    //   showCloseButton: true,
    //   timeout: 2000
    // };
    // this.toasterService.pop(toast);
    this.presentToast('error', '');
  });
}

  gotoApproveAsset(assetData) {
    console.log('assetData', assetData);
    this.approveAssetService.getAssetDataForApproval = assetData;
    this.router.navigate(['approve-asset'], { relativeTo: this.routes });
  }

  gotocardAprrove(assetData) {
    console.log('assetData', assetData);
    this.approveAssetService.getAssetDataForApproval = assetData;
    this.router.navigate(['approve-asset'], { relativeTo: this.routes });
  }

  gotoShareAsset(assetData) {
     ///breadCrumb
     this.breadcrumbArray[1].name = 'Share File'
     this.previousBreadCrumb[1].name = 'Share File'
     this.title = assetData.assetName
     console.log('assetData', assetData);
     //// breadCrumb 
     this.breadObj = {
       'name': assetData.assetName,
       // 'navigationPath': '',
       'id':assetData,
       'navigationPath': '/pages/dam/assets',
       'sameComp':true,
       'assetId' : assetData.assetId
     }
     
     this.previousBreadCrumb.push(this.breadObj)
     
     this.breadcrumbArray.push(this.breadObj)
     for(let i= 0;i<this.previousBreadCrumb.length;i++){
       if(this.title == this.previousBreadCrumb[i].name){
         this.breadcrumbArray = this.previousBreadCrumb.slice(0,i)
       }
     }

    console.log('assetData', assetData);
    this.shareAssetService.getAssetDataForShare = assetData;
    // this.shareAssetService.tabId = this.tabId
    this.addAssetService.shareCatId = assetData.categoryId
    this.addAssetService.title = this.title
    this.addAssetService.breadCrumbArray = this.breadcrumbArray
    this.addAssetService.previousBreadCrumb = this.previousBreadCrumb
    this.router.navigate(['share-asset'], { relativeTo: this.routes });
  }

  gotoCardShareAssets(assetData) {
    console.log('assetData', assetData);
    this.shareAssetService.getAssetDataForShare = assetData;
    this.addAssetService.shareCatId = assetData.categoryId
    this.router.navigate(['share-asset'], { relativeTo: this.routes });
  }

  gotoReviewAsset(assetData) {
            ///breadCrumb
            this.title = assetData.assetName
            console.log('assetData', assetData);
            //// breadCrumb 
            this.breadObj = {
              'name': assetData.assetName,
              // 'navigationPath': '',
              'id':assetData,
              'navigationPath': '/pages/dam/assets',
              'sameComp':true,
              'assetId' : assetData.assetId
            }
            
            this.previousBreadCrumb.push(this.breadObj)
            
            this.breadcrumbArray.push(this.breadObj)
            for(let i= 0;i<this.previousBreadCrumb.length;i++){
              if(this.title == this.previousBreadCrumb[i].name){
                this.breadcrumbArray = this.previousBreadCrumb.slice(0,i)
              }
            }
    console.log('assetData', assetData);
    this.assetReviewPolicyService.getAssetDataForReview = assetData;
    this.addAssetService.reviewId = assetData.categoryId
    this.addAssetService.shareCatId = assetData.categoryId
    this.addAssetService.title = this.title
    this.addAssetService.breadCrumbArray = this.breadcrumbArray
    this.addAssetService.previousBreadCrumb = this.previousBreadCrumb
    this.router.navigate(['asset-review-policy'], { relativeTo: this.routes });
  }

  gotoCardReviewAsset(assetData) {
    console.log('assetData', assetData);
    this.assetReviewPolicyService.getAssetDataForReview = assetData;
    this.router.navigate(['asset-review-policy'], { relativeTo: this.routes });
  }
  previousBreadCrumb:any =  [ 
    {'name': 'DAM',
   'navigationPath': '/pages/dam',
 },{
  'name': 'Folder File',
  'navigationPath': '/pages/dam/assets',
  'id':{},
  'sameComp':true,
  'assetId':null
}
]
  gotoAssetCategory(assetData) {
    this.skeleton = false
    this.title = assetData.assetName
  console.log('assetData', assetData);
  //// breadCrumb 
  this.breadObj = {
    'name': assetData.assetName,
    // 'navigationPath': '',
    'id':assetData,
    'navigationPath': '/pages/dam/assets',
    'sameComp':true,
    'assetId' : assetData.assetId
  }
  
  this.previousBreadCrumb.push(this.breadObj)
  
  this.breadcrumbArray.push(this.breadObj)
  for(let i= 0;i<this.previousBreadCrumb.length;i++){
    if(this.title == this.previousBreadCrumb[i].name){
      this.breadcrumbArray = this.previousBreadCrumb.slice(0,i)
    }
  }
  // this.header.breadCrumbList.push(breadObj)
  /////end breadCrumb
  if (assetData.caFlag == 1) {
    this.addAssetService.view = assetData.tabId

    // this.toolsService.parentSecId = data.id;
    // this.oldParentSecId = data.parentSectionId;
    this.addAssetService.addEditCategoryName = assetData.assetName;//new onennn
    this.parentCatId = assetData.assetId;
    this.innerTabId = assetData.tabId
    this.getAllAssetandCategories(this.parentCatId,this.roleId,this.tabId,this.innerTabId);
    // if(this.tabId ==  4 ){
    //   this.getAssetOptions(assetData.tabId)
    // }

  } else {
    var action = 'EDIT';
    if(this.tabId == 4)
    {
      this.addAssetService.view = assetData.tabId
    }
    else{
      this.addAssetService.view = assetData.tabId
    }
    // this.getAssetVersions(assetData.assetId)
    console.log(this.assetVersion,"versions")
    this.dataFromAssetCategory = true
    this.addAssetService.dataFromAssetCategory = this.dataFromAssetCategory;
    // this.addAssetService.categoryId = assetData.assetId
    this.addAssetService.categoryId = assetData.categoryId

    this.addAssetService.addEditAssetData[0] = action;
    this.addAssetService.addEditAssetData[1] = assetData;
    this.addAssetService.tabId = this.tabId
    this.addAssetService.roleId = this.roleId
    this.addAssetService.selectedIndex = this.selectedIndex

    // this.addAssetService.addEditCategoryName = assetData;
    // this.addAssetService.assetVersions = this.assetVersion
    this.addAssetService.title = this.title
    this.addAssetService.breadCrumbArray = this.breadcrumbArray
    this.addAssetService.previousBreadCrumb = this.previousBreadCrumb
    this.router.navigate(['add-edit-asset'], { relativeTo: this.routes });
    // this.toolsService.parentSecId = data.id;
    // this.router.navigate(['add-edit-asset'], { relativeTo: this.routes });
  }
  }
  getAssetOptions(id){
    if(id==1){
      if(this.parentCatId == null ){
        this.header = {
          title: 'Folder File',
          btnsSearch: true,
          searchBar: true,
          placeHolder:'Search by keywords',
          dropdownlabel: ' ',
          drplabelshow: false,
          drpName1: '',
          drpName2: ' ',
          drpName3: '',
          drpName1show: false,
          drpName2show: false,
          drpName3show: false,
          btnName1: '',
          btnName2: 'Add Folder',
          btnName3: 'Upload Folder',
          // btnAdd: 'Add Asset',
          btnName1show: false,
          btnName2show: true,
          btnName3show: true,
          btnBackshow: true,
          btnAddshow: false,
          filter: false,
          showBreadcrumb:true,
          breadCrumbList:[
            {
              'name': 'DAM',
              'navigationPath': '/pages/dam',
            },
          ]
        };
      }else{
        this.header = {
          title: 'Folder File',
          btnsSearch: true,
          searchBar: true,
          dropdownlabel: ' ',
          drplabelshow: false,
          drpName1: '',
          drpName2: ' ',
          drpName3: '',
          drpName1show: false,
          drpName2show: false,
          drpName3show: false,
          btnName1: '',
          btnName2: 'Add Folder',
          btnName3: 'Upload Folder',
          btnName4: 'Upload File',
          btnAdd: 'Add File',
          btnName1show: false,
          btnName2show: true,
          btnName3show: true,
          // btnName4show: true,
          btnName4show: true,
          btnBackshow: true,
          btnAddshow: true,
          filter: true,
          showBreadcrumb:true,
          breadCrumbList:[
            {
              'name': 'DAM',
              'navigationPath': '/pages/dam',
            },
          ]
        }
      }
    }else if(id == 2){
      if(this.parentCatId == null ){
      this.header = {
        title: 'Folder File',
        btnsSearch: true,
        searchBar: true,
        placeHolder:'Search by keywords',
        dropdownlabel: ' ',
        drplabelshow: false,
        drpName1: '',
        drpName2: ' ',
        drpName3: '',
        drpName1show: false,
        drpName2show: false,
        drpName3show: false,
        btnName1: '',
        btnName2: 'Add Folder',
        // btnName3: 'File Upload',
        // btnAdd: 'Add Asset',
        btnName1show: false,
        btnName2show: false,
        btnName3show: false,
        btnBackshow: true,
        btnAddshow: false,
        filter: false,
        showBreadcrumb:true,
        breadCrumbList:[
          {
            'name': 'DAM',
            'navigationPath': '/pages/dam',
          },
        ]
      };
      }else{
        this.header = {
          title: 'Folder File',
          btnsSearch: true,
          searchBar: false,
          placeHolder:'Search by keywords',
          dropdownlabel: ' ',
          drplabelshow: false,
          drpName1: '',
          drpName2: ' ',
          drpName3: '',
          drpName1show: false,
          drpName2show: false,
          drpName3show: false,
          btnName1: '',
          btnName2: 'Add Folder',
          // btnName3: 'File Upload',
          // btnAdd: 'Add Asset',
          btnName1show: false,
          btnName2show: false,
          btnName3show: false,
          btnBackshow: true,
          btnAddshow: false,
          filter: false,
          showBreadcrumb:true,
          breadCrumbList:[]
  
        };
      }
    }else if(id == 3){
      this.header = {
        title: 'Folder File',
        btnsSearch: true,
        searchBar: false,
        placeHolder:'Search by keywords',
        dropdownlabel: ' ',
        drplabelshow: false,
        drpName1: '',
        drpName2: ' ',
        drpName3: '',
        drpName1show: false,
        drpName2show: false,
        drpName3show: false,
        btnName1: '',
        btnName2: 'Add Folder',
        // btnName3: 'File Upload',
        // btnAdd: 'Add Asset',
        btnName1show: false,
        btnName2show: false,
        btnName3show: false,
        btnBackshow: true,
        btnAddshow: false,
        filter: false,
        showBreadcrumb:true,
        breadCrumbList:[]
  
      };
    }
  
  }

  gotoShareAssetCategory(assetData) {
    ///breadCrumb
    this.title = assetData.assetName
    if(this.breadcrumbArray[1]) {
    this.breadcrumbArray[1].name = 'Share File'
    }
    this.previousBreadCrumb[1].name = 'Share File'
    console.log('assetData', assetData);
    //// breadCrumb 
    this.breadObj = {
      'name': assetData.assetName,
      // 'navigationPath': '',
      'id':assetData,
      'navigationPath': '/pages/dam/assets',
      'sameComp':true,
      'assetId' : assetData.assetId
    }
    
    this.previousBreadCrumb.push(this.breadObj)
    
    this.breadcrumbArray.push(this.breadObj)
    for(let i= 0;i<this.previousBreadCrumb.length;i++){
      if(this.title == this.previousBreadCrumb[i].name){
        this.breadcrumbArray = this.previousBreadCrumb.slice(0,i)
      }
    }
    ///end BreadCrumb
    console.log('assetData', assetData);
    if (assetData.caFlag == 1) {
      // this.toolsService.parentSecId = data.id;
      // this.oldParentSecId = data.parentSectionId;
      this.parentCatId = assetData.assetId;
      this.getApprovedAssetsNew(this.parentCatId);
  
    }
  }
  submit(data, f) {
    // this.isSave = true
    // this.submitted = true;
    if (f.valid) {
      if (!data) {
        // return;
        console.log(data);
      }
      else {
        console.log('data', data);

        this.addEditCategoryModal = false;
        this.spinner.show();

        let param = {
          "parCatId":this.parentCatId,
          "catId": this.addAction ? 0 : data.id,
          "catCode": data.code,
          "catName": data.name,
          "catDesc": data.desc?data.desc:null,
          "catPicRef": data.categoryPicRef,
          "tId": this.tenantId,
          "userId": this.tenantId
        }
        console.log('param', param);
        this.passParams = param;

        var fd = new FormData();
        fd.append('content', JSON.stringify(this.passParams));
        fd.append('file', this.assetCatImgData);
        console.log('File Data ', fd);

        console.log('Asset Category Img', this.assetCatImgData);
        console.log('Asset Data ', this.passParams);
        let fileUploadUrl = webApi.domain + webApi.url.fileUpload;

        if (this.assetCatImgData) {
          this.webApiService.getService(fileUploadUrl, fd)
            .then(rescompData => {
              // this.isSave = false
              var temp: any = rescompData;
              this.fileUploadRes = temp;
              console.log('rescompData', this.fileUploadRes)
              this.assetCatImgData = null;
              if (temp == "err") {
                // this.notFound = true;
                // var thumbUpload: Toast = {
                //   type: 'error',
                //   title: "Asset Category Image",
                //   body: "Unable to upload Asset Category Image.",
                //   // body: temp.msg,
                //   showCloseButton: true,
                //   timeout: 2000
                // };
                // this.toasterService.pop(thumbUpload);
                this.presentToast('error', '');
                this.spinner.hide();
              } else if (temp.type == false) {
                // var thumbUpload: Toast = {
                //   type: 'error',
                //   title: "Asset Category Image",
                //   body: "Unable to upload Asset Category Image.",
                //   // body: temp.msg,
                //   showCloseButton: true,
                //   timeout: 2000
                // };
                // this.toasterService.pop(thumbUpload);
                this.presentToast('error', '');
                this.spinner.hide();
              }
              else {
                if (this.fileUploadRes.data != null || this.fileUploadRes.fileError != true) {
                  this.passParams.catPicRef = this.fileUploadRes.data.file_url;
                  console.log('this.passparams.catPicRef', this.passParams.catPicRef);
                  // this.isSave= false
                  this.spinner.hide();
                  this.addEditAssetCategory(this.passParams);
                } else {
                  // var thumbUpload: Toast = {
                  //   type: 'error',
                  //   title: "Asset Category Image",
                  //   // body: "Unable to upload course thumbnail.",
                  //   body: this.fileUploadRes.status,
                  //   showCloseButton: true,
                  //   timeout: 2000
                  // };
                  // this.toasterService.pop(thumbUpload);
                  this.presentToast('error', '');
                  this.spinner.hide();
                }
              }
              console.log('File Upload Result', this.fileUploadRes);
              var res = this.fileUploadRes;
              this.fileres = res.data.file_url;
            },
              resUserError => {
                // this.errorMsg = resUserError;
                // console.log('File upload this.errorMsg', this.errorMsg);
                this.spinner.hide();
              });
        } else {
          this.spinner.hide();
          this.addEditAssetCategory(this.passParams);
        }
      }
    } else {
      console.log('Please Fill all fields');
      // this.isSave = false

      // const detForm: Toast = {
      //   type: 'error',
      //   title: 'Unable to update',
      //   body: 'Please Fill all fields',
      //   showCloseButton: true,
      //   timeout: 2000
      // };
      // this.toasterService.pop(detForm);
      this.presentToast('warning', 'Please fill in the required fields');
      Object.keys(f.controls).forEach(key => {
        f.controls[key].markAsDirty();
      });
    }

  }



  addEditAssetCategory(params) {
    console.log('paramNew', params);
    this.spinner.show();
    const _urlAddEditAssetCategories:string = webApi.domain + webApi.url.addEditAssetCategory;
    this.commonFunctionService.httpPostRequest(_urlAddEditAssetCategories,params)
    //this.assetcategoryservice.addEditAssetCategories(params)
      .then(rescompData => {
        // this.isSave = false

        this.spinner.hide();

        var temp = rescompData;
        var result = temp['data'];
        if (params.catId == 0) {
          if (temp['type'] == true) {
            // this.fetchBadgeCateory();
            console.log('Add Result ', result)
            // var catUpdate: Toast = {
            //   type: 'success',
            //   title: "Asset Category Inserted!",
            //   body: "New Asset Category added successfully.",
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(catUpdate);
            this.presentToast('success', 'New folder added');
            this.getAllAssetandCategories(this.parentCatId,this.roleId,this.tabId,this.innerTabId);

          } else {
            //this.fetchBadgeCateory();
            // var catUpdate: Toast = {
            //   type: 'error',
            //   title: "Asset Category Inserted!",
            //   body: "Unable to add Asset Category.",
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(catUpdate);
            this.presentToast('error', '');
          }
        } else {
          console.log('Asset Category Edit Result ', result)
          if (temp['type'] == true) {
            // this.fetchBadgeCateory();
            // var catUpdate: Toast = {
            //   type: 'success',
            //   title: "Asset Category Updated!",
            //   body: "Asset category updated successfully.",
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(catUpdate);
            this.presentToast('success', 'Folder updated');
            this.getAllAssetandCategories(this.parentCatId,this.roleId,this.tabId,this.innerTabId);
          } else {
            //this.fetchBadgeCateory();
            // var catUpdate: Toast = {
            //   type: 'error',
            //   title: "Asset Category updated!",
            //   body: "Unable to update Asset category.",
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(catUpdate);
            this.presentToast('error', '');
          }
        }

      },
        resUserError => {
          this.spinner.hide();
          // this.errorMsg = resUserError;
        });
  }
    
  
  



  disableAsset(data, status) {

    this.enableDisableCategoryModal = true;
    this.assetsdata = data;
    if (data.visible == 0) {
      this.avisible = 1;
      this.changetitle = 'Enable';
      this.msg = "File Enable Successfully"
    } else {
      this.avisible = 0;
      this.changetitle = 'Disable';
      this.msg = "File Disable Successfully"
    }
  }

  clickTodisable(data,identifier) {
    // this.modalName = 'File'
    this.modalName = data.assetName
    this.identifier = identifier
    if(data.visible==0){
      this.changetitle='Enable';
    }
    else{
      this.changetitle='Disable';
    }
    this.enableDisableCategoryModal = true;
    this.assetsdata = data;
    this.storedVisible = data;
  }
  enableDisableAction(status) {
    if (status == true) {
      // kv
      if (this.storedVisible.visible == 0) {
        this.avisible = 1;
        this.storedVisible.disable = 1;
        this.changetitle = 'Enable';
        this.msg = "File Enable Successfully"
      } else {
        this.avisible = 0;
        this.storedVisible.disable = 0;
        this.changetitle = 'Disable';
        this.msg = "File Disable Successfully"
      }
      // kv
      this.changevisiblestatus();
    } else {
      this.enableDisableCategoryModal = false;
    }
  }
  changevisiblestatus() {
    this.spinner.show();
    let param = {
      'assetsId': this.assetsdata.assetId,
      'assetsVisible': this.avisible,
      'tId': this.tenantId

    }

    this.assetservice.changeAssetStatus(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('UpdateAssetResponse:', rescompData);
        if (result['type'] == true) {
          this.presentToast('success', this.msg);
          this.enableDisableCategoryModal = false;
          this.getAllAssetandCategories(this.parentCatId,this.roleId,this.tabId,this.innerTabId);
        } else {
          this.presentToast('error', '');
        }
      }, error => {
        this.spinner.hide();
        this.presentToast('error', '');
      });
  }
  closeEnableDisableCategoryModal() {
    this.enableDisableCategoryModal = false;
  }

  getAllAssetDropdown() {
    let param = {
      "tId": this.tenantId
    }
    const _urlGetAllDropdown: string = webApi.domain + webApi.url.getAllAssetDropDown;
    this.commonFunctionService.httpPostRequest(_urlGetAllDropdown, param)
      // this.addAssetService.getAllAssetDropdown(param)
      .then(rescompData => {
        var result = rescompData;
        console.log('All Asset Dropdown1', result);
        if (result['type'] == true) {
          console.log('All Asset Dropdown2', result);
          this.status = result['data'][0];
          this.languages = result['data'][1];
          this.formats = result['data'][2];
          this.channels = result['data'][3];
          this.categories = result['data'][4];
          this.bindfilter(this.languages);
          this.bindfilter(this.channels);
          if(!this.getApprovedAssets && !this.getPendingAssets) {
            this.bindfilter(this.status);  
          }
          this.bindfilter(this.formats);
          // if (result['data'] && result['data'].length > 0) {
          //   result['data'].forEach( (value, index) => {
          //     const filtername = value.length > 0 ? value[0]['filterId'] : '';
          //     const item = {
          //       count: "",
          //       value: "",
          //       tagname: value.length > 0 ? value[0]['filterId'] : '',
          //       isChecked: false,
          //       list: value,
          //     }
          //     if (filtername) {
          //       this.filters.push(item);
          //     }
          //   })
          // }
          if (this.categories.length == 0) {
            this.noCategory = true;
          }
          console.log(this.status, this.languages, this.formats, this.channels, this.categories);
        } else {
          // var toast: Toast = {
          //   type: 'error',
          //   //title: "Server Error!",
          //   body: "Something went wrong.please try again later.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }

      },
        resUserError => {
          // this.loader =false;
          // var toast: Toast = {
          //   type: 'error',
          //   //title: "Server Error!",
          //   body: "Something went wrong.please try again later.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        });
  }


  addeditAsset(assetData) {
    console.log('assetData:', assetData);
    console.log(this.displayArray, "this.displayArray")
    // if (value == 0) {
    var action = 'ADD';
    //this.addAssetService.pollId = 0;
    this.addAssetService.addEditAssetData[0] = action;
    this.addAssetService.categoryId = this.parentCatId
    this.addAssetService.addEditAssetData[1] = null;
    this.addAssetService.title = this.title
    this.addAssetService.breadCrumbArray = this.breadcrumbArray
    this.addAssetService.previousBreadCrumb = this.previousBreadCrumb
    // this.addAssetService.addEditAssetData[1] = assetData;
    this.router.navigate(['add-edit-asset'], { relativeTo: this.routes });
    // } else {
    //   var action = 'EDIT';
    //   //this.addPollService.pollId = data.id;
    //   this.addAssetService.addEditAssetData[0] = action;
    //   this.addAssetService.addEditAssetData[1] = assetData;

    //   this.router.navigate(['add-edit-asset'], { relativeTo: this.routes });

    // }
  }

  EditSection(assetData) {
    var action = 'EDIT';
    this.addAssetService.addEditAssetData[0] = action;
    this.addAssetService.addEditAssetData[1] = assetData;

    this.router.navigate(['add-edit-asset'], { relativeTo: this.routes });
  }
  EditSectionAsset(data) {
    this.addEditCategoryModal = true;
    this.addAction = false;
    console.log('data2', data);
    this.categoryForm = {
      id: data.assetId,
      name: data.assetName,
      code: data.categoryCode,
      desc: data.description,
      categoryPicRef: data.categoryPicRef,
    }
  }
  fileexterror: any;
  // assetCatImgData: any;

  readAssetCatImage(event: any) {
    this.fileexterror = false;
    var validExts = new Array(".png", ".jpg", ".jpeg");
    var fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
      this.fileexterror = true;
    } else {
      if (event.target.files && event.target.files[0]) {
        this.assetCatImgData = event.target.files[0];

        var reader = new FileReader();

        reader.onload = (event: ProgressEvent) => {
          this.categoryForm.categoryPicRef = (<FileReader>event.target).result;
        }
        reader.readAsDataURL(event.target.files[0]);
      }
    }
  }

  deleteAssetCatImage() {
    this.assetCatImgData = null;
    // this.categoryForm.categoryPicRef ='https://bhaveshedgetest.s3.ap-south-1.amazonaws.com/BFL.jpg';
    this.categoryForm.categoryPicRef ='assets/images/category.jpg';

  }

  upgradeVersion(assetData) {
         ///breadCrumb
         this.title = assetData.assetName
         console.log('assetData', assetData);
         //// breadCrumb 
         this.breadObj = {
           'name': assetData.assetName,
           // 'navigationPath': '',
           'id':assetData,
           'navigationPath': '/pages/dam/assets',
           'sameComp':true,
           'assetId' : assetData.assetId
         }
         
         this.previousBreadCrumb.push(this.breadObj)
         
         this.breadcrumbArray.push(this.breadObj)
         for(let i= 0;i<this.previousBreadCrumb.length;i++){
           if(this.title == this.previousBreadCrumb[i].name){
             this.breadcrumbArray = this.previousBreadCrumb.slice(0,i)
           }
         }
    console.log('assetDataForVersion:', assetData);
    this.assetVesrionService.getAssetDataForVesrsioning = assetData;
    this.addAssetService.categoryId = assetData.categoryId
    this.addAssetService.view = assetData.tabId
    // this.assetVersion.tabId = this.tabId
    this.addAssetService.tabId = this.tabId
    this.addAssetService.roleId = this.roleId
    this.addAssetService.selectedIndex = this.selectedIndex
    this.addAssetService.title = this.title
    this.addAssetService.breadCrumbArray = this.breadcrumbArray
    this.addAssetService.previousBreadCrumb = this.previousBreadCrumb
    this.router.navigate(['asset-version'], { relativeTo: this.routes });
  }
  download(item){
    console.log(item,"downLoad item")
    this.downloadLink = item.assetRef

  }

  copyCard(assetData) {
    console.log('assetDataForVersion:', assetData);
    this.assetVesrionService.getAssetDataForVesrsioning = assetData;
    this.router.navigate(['asset-version'], { relativeTo: this.routes });
  }

  breadcrumbNavigate(data){
    console.log(data,"navigationData")
    this.title = data.assetName
    this.parentCatId = data.assetId;
    this.breadcrumbArray =  this.breadcrumbArray.slice(0,data.index)
    this.previousBreadCrumb = this.previousBreadCrumb.slice(0,data.index+1)
    console.log(this.breadcrumbArray,"breadcrumbArray")
    console.log(this.previousBreadCrumb,"previousBreadCrumb")
    

    // this.header.breadCrumbList =  this.breadcrumbArray
    // this.parentCatId = this.oldParentCatId
    this.getAllAssetandCategories(this.parentCatId,this.roleId,this.tabId,this.innerTabId);

  }
  breadcrumbNavigateShare(data){
    console.log(data,"navigationData")
    this.title = data.assetName
    this.parentCatId = data.assetId;
    this.breadcrumbArray =  this.breadcrumbArray.slice(0,data.index)
    this.previousBreadCrumb = this.previousBreadCrumb.slice(0,data.index+1)
    console.log(this.breadcrumbArray,"breadcrumbArray")
    console.log(this.previousBreadCrumb,"previousBreadCrumb")
    this.parentCatId = data.assetId;
    this.getApprovedAssetsNew(this.parentCatId)
    

    // this.header.breadCrumbList =  this.breadcrumbArray
    // this.parentCatId = this.oldParentCatId
  }

  // *********************************************Vikas*********************************************************

  asset: any = [];

  fieldValue = [];
  ObjectSetterValueArray = [];

  filterData = {};

  // searchTags:any;



  // subject filteration starts here
  // main array
  newArray = [];

  // array for display on html page
  // Initially without filter it will hold data of asset
  displayArray = this.asset;


  // nofilterAsset variable for validation
  noFilterAsset: boolean = false;


  // new array for tags
  tagArray = [];
  //
  filterTag: any = [];
  // number display to n
  SelectedCategoryLength: any = {};

  SubjectFilterArray = [];
  SubjectName_CheckBoxedStatus: boolean;
  // subj: any;



  /*

  After loading the screen
  ***********************************
  when we select the checkbox SubjectName function will be called.

  After execution of statement takes places

  */

  // tslint:disable-next-line: max-line-length
  filterDataObjectArrayCreation(filterData_CategoryNameForKey: any, FiltersubjectNameForArray: any, checkboxStatus: boolean) {


    let dataValue = null;
    let flag = false;
    for (const key in this.filterData) {
      if (filterData_CategoryNameForKey === key) {
        flag = true;
        dataValue = key;
        break;
      } else {
        flag = false;
      }
    }
    if (checkboxStatus) {
      if (!flag) {
        Object.assign(this.filterData, { [filterData_CategoryNameForKey]: [] });
        (this.filterData[filterData_CategoryNameForKey]).push(FiltersubjectNameForArray[filterData_CategoryNameForKey]);
        // console.log('Filter data after insertion object with array checkbox', this.filterData);
      } else {
        (this.filterData[dataValue]).push(FiltersubjectNameForArray[filterData_CategoryNameForKey]);
        // console.log('Filter data after insertion object with array checkbox', this.filterData[dataValue]);
        // console.log('true');
      }
      // console.log('Filter data after insertion object with array checkbox', this.filterData);
      // console.log(this.filterData);
    } else {
      for (let i = 0; i < this.filterData[dataValue].length; i++) {
        if (this.filterData[dataValue][i] === FiltersubjectNameForArray[dataValue]) {
          this.filterData[dataValue].splice(i, 1);
          if (this.filterData[dataValue].length === 0) {
            delete this.filterData[dataValue];
            this.SelectedCategoryLength[dataValue] = 0;
            delete this.objectSetter[dataValue];
            break;
          }
          // console.log('Filter data after Splicing object with array checkbox', this.filterData);
          // console.log(this.filterData[dataValue]);
        }
      }
    }
  }


  objectSetter = {};

  fieldValueCreatorFunction() {
    // const keys = Object.keys(this.filterData);
    this.fieldValue = Object.keys(this.filterData);
    // console.log('FieldValue Object Data', this.fieldValue);
    for (const ObjectSetterData of this.fieldValue) {
      Object.assign(this.objectSetter, { [ObjectSetterData]: false });
    }

    for (const filterDataCate of this.fieldValue) {
      Object.assign(this.SelectedCategoryLength, { [filterDataCate]: this.filterData[filterDataCate].length });
    }
    // console.log('object length', this.SelectedCategoryLength);
  }

  tagCreatorFunction(CategoryObject: any, StatusToPushOrPop: any) {
    this.filterData = [];
    if (StatusToPushOrPop) {
      this.tagArray.push(CategoryObject);
      for (const data of this.tagArray) {
        for (const key in data) {
          if (isString(data[key])) {
            // console.log('key type',isString(data[key]));
            // console.log(key);
            this.filterTag.push(key);
          }
        }
      }
    }
    console.log('Filter TagArray', this.filterTag);
    console.log('Tag Array', this.tagArray);
  }


  // subjectName(subj: any, cat: any) {
  //   // console.log(subj.name);
  //   // console.log('Category name', cat);
  //   // this.SubjectFilterArray.push(subj.name);

  //   if (cat === "id") {
  //     for (const tagArray of this.Tags) {
  //       if (subj === tagArray) {
  //         console.log('tag matches');
  //         tagArray.status = this.SubjectName_CheckBoxedStatus;
  //         break;
  //       }
  //     }
  //   }

  //   this.filterDataObjectArrayCreation(cat, subj, this.SubjectName_CheckBoxedStatus);
  //   this.fieldValueCreatorFunction();
  //   this.onFilterSubjectPush(cat);
  //   // this.tagCreatorFunction(subj, this.SubjectName_CheckBoxedStatus);


  // }

  subjectName(subj: any, cat: any) {
    // console.log(subj.name);
    // console.log('Category name', cat);
    // this.SubjectFilterArray.push(subj.name);

    if (cat === "id" || cat === 'tagId') {
      for (const tagArray of this.Tags) {
        if (subj === tagArray) {
          console.log('tag matches');
          tagArray.status = this.SubjectName_CheckBoxedStatus;
          break;
        }
      }
    }

    this.filterDataObjectArrayCreation(cat, subj, this.SubjectName_CheckBoxedStatus);
    this.fieldValueCreatorFunction();
    this.onFilterSubjectPush(cat);
    // this.tagCreatorFunction(subj, this.SubjectName_CheckBoxedStatus);


  }
  onFilterSubjectPush(filterData_CategoryNameForKey: any) {
    this.newArray = [];
    for (const data of this.asset) {

      for (const ObjectSetterData of this.fieldValue) {
        Object.assign(this.objectSetter, { [ObjectSetterData]: false });
      }

      for (const value of this.fieldValue) {
        // this.tagArray = [];
        // console.log('Value', value);
        // console.log('FilterData of particular value', this.filterData[value]);
        // console.log('Length of FilterData of particular value', this.filterData[value].length);
        // tslint:disable-next-line: prefer-for-of
        // if (value === filterData_CategoryNameForKey) {
        if (value == 'id' || value == 'tagId') {
          console.log(value);
          var tagIds = [];
          if(data['tagIds']){
           tagIds = data['tagIds'].split(',');
          }
          for (const tagData of tagIds) {
            for (let i = 0; i < this.filterData[value].length; i++) {

              // Pushing data into tag array
              // this.tagArray.push({[value]:this.filterData[value][i]});

              if (tagData == undefined || tagData != this.filterData[value][i]) {
                // console.log('ifTrue');
                // let checkData = data[value];
              } else {
                // console.log('ifFalse');
                this.objectSetter[value] = true;
                // let checkData = data[value];
                break;
              }
            }
          }

        }
        else {
          console.log('else part executed');

          for (let i = 0; i < this.filterData[value].length; i++) {

            // Pushing data into tag array
            // this.tagArray.push({[value]:this.filterData[value][i]});

            // if (data[value] === undefined || data[value] !== this.filterData[value][i]) {
            if (data[value] == undefined || data[value] != this.filterData[value][i]) {
              // console.log('ifTrue');
              // let checkData = data[value];
            } else {
              // console.log('ifFalse');
              this.objectSetter[value] = true;
              // let checkData = data[value];
              break;
            }
          }

        }
      }
      // }



      // let ObjectDataValue = null;
      let Objectflag = true;
      this.ObjectSetterValueArray = Object.values(this.objectSetter);

      for (const objectSetterValueArrayData of this.ObjectSetterValueArray) {
        if (Objectflag && objectSetterValueArrayData) {
          Objectflag = true;
        } else {
          Objectflag = false;
        }
      }

      if (Objectflag) {
        this.newArray.push(data);
      }
    }

    // console.log(this.tagArray);
    console.log(this.newArray);


    if ((this.fieldValue.length !== 0)) {
      if ((this.newArray.length !== 0)) {
        // this.displayArray = this.newArray;
        this.displayArray = [];
        this.sum = 50;
        this.addItems(0, this.sum, 'push', this.newArray);
        this.noFilterAsset = false;
        // this.noDataFound = false

      } else {
        this.displayArray = [];
        // this.noDataFound = true
        this.noFilterAsset = true;
      }
    } else {
      this.noFilterAsset = false;
      this.displayArray = [];
      this.sum = 50;
      this.addItems(0, this.sum = 50, 'push', this.asset);
      // this.displayArray = this.asset;
    }
  }

  // subject filteration ends here


  onUpdateSubjectName(event: Event) {
    this.SubjectName_CheckBoxedStatus = (<HTMLInputElement>event.target).checked;
    // console.log('Subject Name', this.SubjectName_CheckBoxedStatus);
  }
  ngOnDestroy() {
    this.toggleSidebar();
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0){
      e1[0].classList.add('with-scroll');
    }
  }
  // this function is use to dynamicsearch on table
  searchQuestion(event) {
    var temData = this.asset;
    var val = event.target.value.toLowerCase();
    var keys = [];
    // filter our data
    if (temData.length > 0) {
      keys = Object.keys(temData[0]);
    }
    if(val.length>=3||val.length==0){
    var temp = temData.filter((d) => {
      for (const key of keys) {
        if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
          return true;
        }
      }
    });

    // update the rows
    this.displayArray =[];
    this.newArray = temp;
    this.sum = 50;
    this.addItems(0, this.sum, 'push', this.newArray);
    if (!this.newArray.length) {
      this.noFilterAsset = true;
    } else {
      this.noFilterAsset = false;
    }
  }
  }

  addeditccategory(data, id) {

    if (data == undefined) {
      this.categoryForm = {
        id: '',
        name: '',
        code: '',
        desc: '',
        categoryPicRef: '',
      };
    }

    this.addEditCategoryModal = true;
    console.log('data', data);

    if (id == 0) {
      this.addAction = true;
    } else {
      this.addAction = false;
      console.log('data2', data);
      this.categoryForm = {
        id: data.categoryId,
        name: data.categoryName,
        code: data.categoryCode,
        desc: data.description,
        categoryPicRef: data.categoryPicRef
      }

    }
  }

  EditCatSection(data) {
    this.addEditCategoryModal = true;
    this.addAction = false;
    console.log('data2', data);
    this.categoryForm = {
      id: data.categoryId,
      name: data.categoryName,
      code: data.categoryCode,
      desc: data.description,
      categoryPicRef: data.categoryPicRef,
    }
  }
  SarchFilter(event) {
    var searchtext
    this.header.searchtext = event.target.value
    console.log(searchtext);
    searchtext= event.target.value;
    const val = searchtext.toLowerCase();
    if(val.length>=3 || val.length == 0){
    this.displayArray=[];
    const tempcc = this.demoasset.filter(function (d) {
      if(d.tags ){
      return String(d.code).toLowerCase().indexOf(val) !== -1 ||
        d.assetName.toLowerCase().indexOf(val) !== -1 ||
        d.tags.join().toLowerCase().indexOf(val) !== -1 ||
        // d.department.toLowerCase().indexOf(val) !== -1 ||
        // d.manager.toLowerCase().indexOf(val) !== -1 ||
        !val
      }else{
        return String(d.code).toLowerCase().indexOf(val) !== -1 ||
        d.assetName.toLowerCase().indexOf(val) !== -1 ||
        // d.tags.join().toLowerCase().indexOf(val) !== -1 ||
        !val
      }
    });
    console.log(tempcc);
    this.newArray = tempcc;
    this.sum=50;
    this.addItems(0, this.sum, 'push', this.newArray);

    if (!this.newArray.length) {
      // this.noCategory = true;
      this.noAsset = true
    } else {
      // this.noCategory = false;
      this.noAsset = false
    }
  }
  }
  clear() {
    if(this.header.searchtext){
    this.searchtext = '';
    this.header.searchtext = '';
    this.displayArray = []
    this.noAsset = false
    // this.sum = 50
    this.newArray = this.demoasset
    this.addItems(0, this.sum, 'push', this.asset)
    // this.getAllAssetandCategories(this.parentCatId,this.roleId,this.tabId,this.innerTabId);
    }
    else{
     this.searchtext = '';
    }
  }

  closeModal() {
    this.addEditCategoryModal = false;
  }

  ngAfterContentChecked() {
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0)
    {
      e1[0].classList.remove('with-scroll');
    }

    // const e = document.getElementsByTagName('nb-layout-column');
    // // console.log(e[0].clientHeight);
    // // this.conentHeight = String(e[0].clientHeight) + 'px';
    // if (e[0].clientHeight > 700) {
    //   this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
    // } else {
    //   this.conentHeight = String(e[0].clientHeight) + 'px';
    // }

  }
  filteredChanged(event) {
    console.log('Filtered Event - ', event);
    const obj = event;
    this.filterData = {};
    this.SelectedCategoryLength = {};
    this.objectSetter = {};
    let allempty = true;
    if (obj) {
      for(const key in obj){
        const list = obj[key];
        if(list){
        list.forEach( (value, index) => {
          allempty = false;
          console.log(key, ' - ', value);
          this.SubjectName_CheckBoxedStatus = true;
          this.subjectName(value, key);
        });
      }
      }
      if (allempty) {
        this.displayArray = this.asset;
      }
    } else {
      this.displayArray = this.asset;
    }
  }
  gotoFilter() {
    this.filter = !this.filter;
  }

  bindfilter(obj) {
    const filtername = obj.length > 0 ? obj[0]['filterId'] : '';
        const item = {
          count: "",
          value: "",
          tagname: obj.length > 0 ? obj[0]['filterId'] : '',
          isChecked: false,
          list: obj,
        }
        if (filtername) {
          this.filters.push(item);
        }
    // if (result['data'] && result['data'].length > 0) {
    //   result['data'].forEach( (value, index) => {
        
    //   })
    // }
  }
}  