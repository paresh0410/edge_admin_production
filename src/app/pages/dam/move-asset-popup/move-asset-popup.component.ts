import { Component, OnInit, Input , Output , EventEmitter, OnChanges } from '@angular/core';
import * as _ from "lodash";
import { noData } from '../../../models/no-data.model';
@Component({
  selector: 'ngx-move-asset-popup',
  templateUrl: './move-asset-popup.component.html',
  styleUrls: ['./move-asset-popup.component.scss']
})
export class MoveAssetPopupComponent implements OnInit, OnChanges {

  @Input() trees = [];
  @Input() selectedItem;
  preparedCategoriesList = [];
  @Input() showCurrentSelected = false;
  valueChanged = null;
  selectedFolderArray = [];
  nocategoryFound: noData = {
    margin: "mt-0, w-100",
    imageSrc: "assets/images/no-data-bg.svg",
    title: "No Data Found to display.",
    desc: "",
    titleShow: true,
    // btnShow:true,
    descShow: false,
    // btnText:'Learn More',
    // btnLink:'https://faq.edgelearning.co.in/kb/how-to-add-an-asset-in-the-dam',
  };
  @Output() sendEventToParent = new EventEmitter<any>();
  constructor() { }

  ngOnInit() {
   
   
  }

  ngOnChanges(){
    if(this.trees && this.trees.length !=0){
      this.categoryNotFound = false;
      this.preparedCategoriesList = _.cloneDeepWith(this.trees);
    }else {   
      this.categoryNotFound = true;
    }
  }

  passEventToParent(action, ...args){
    // this.showSearch = false;
    const event = {
      'action': action,
      'data': args,
    };
    // console.log('action ===>',action);
    console.log('event ===>', event);
    this.sendEventToParent.emit(event);
  }

  showSearchCategory = false;
  categoryNotFound = true;
  searchTextCategory = "";
  showSearchCategoryToggel() {
    this.showSearchCategory = true;
  }
  clearSearchCategory() {
    this.searchTextCategory = "";
    this.valueChanged = Math.random();
    this.preparedCategoriesList = _.cloneDeepWith(
      this.trees,
    );
    if(this.selectedItem){
      this.valueChanged = this.selectedItem["id"];
    }
  }

  searchInTree(event) {
    const searchString = event.target.value;
    if(event.target.value.length>=3 || event.target.value.length == 0){
    // if (
    //   this.preparedCategoriesList &&
    //   Array.isArray(this.preparedCategoriesList) &&
    //   this.preparedCategoriesList.length != 0
    // ) {
    //   const newArray = this.preparedCategoriesList.filter((item) => {
    //     // return (String(item['categoryName']).includes(searchString)) )
    //     if (String(item["categoryName"]).includes(searchString)) {
    //       return true;
    //     } else {
    //       if (
    //         this.selectedFolderArray &&
    //         item["id"] === this.selectedFolderArray["id"]
    //       ) {
    //         return true;
    //       } else {
    //         return false;
    //       }
    //     }
    //   });
    this.valueChanged = Math.random();
    if (
      this.trees &&
      Array.isArray(this.trees) &&
      this.trees.length != 0
    ) {
      const newArray = this.trees.filter((item) => {
        // return (String(item['categoryName']).includes(searchString)) )
        if (String(item["categoryName"]).toLowerCase().includes(String(searchString).toLowerCase()) && item["parentCatId"] == null) {
          return true;
        } else {
          // return false;
          if (
            this.selectedFolderArray &&  this.selectedFolderArray.length !=0
          ) {
            // for(let i = 0; i < this.selectedFolderArray.length ; i++ ){
            //     if(item["id"] === this.selectedFolderArray[i]["id"] ){
            //       return true;
            //     }else {
            //       return false;
            //     }
            // }
            if(item["id"] === this.selectedFolderArray[0]["id"] ){
                    return true;
                  }else {
                    return false;
                  }
          } else {
            return false;
          }
        }
      });
      console.log("newArray", newArray);
      this.preparedCategoriesList = newArray;
      if(this.selectedItem){
        this.valueChanged = this.selectedItem["id"];
      }

      if (this.preparedCategoriesList.length === 0) {
        this.categoryNotFound = true;
        // this.categoryListLoading = false
      } else {
        this.categoryNotFound = false;
        // this.categoryListLoading = true
      }
    } 
  }
  }

  openDeepyNestedFolder(event) {
    console.log("event", event);
    if (event && event.length != 0) {
      this.selectedFolderArray = event;
    }
  }

}
