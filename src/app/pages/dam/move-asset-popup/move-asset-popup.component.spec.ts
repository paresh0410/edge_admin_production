import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoveAssetPopupComponent } from './move-asset-popup.component';

describe('MoveAssetPopupComponent', () => {
  let component: MoveAssetPopupComponent;
  let fixture: ComponentFixture<MoveAssetPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoveAssetPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoveAssetPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
