import { Injectable, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AppConfig } from '../../../app.module';
import { webApi } from '../../../service/webApi';
import { HttpClient } from "@angular/common/http";
import * as Rx from 'rxjs/Rx';

@Injectable({
  providedIn: "root"
})
export class AssetService {
    // mySubject = new Rx.Subject();
    tenantId;
    userData;
    categoryId: any;
    dataFromAssetCategory: boolean = false;

    getPendingAssets: boolean = false;
    getApprovedAssets: boolean = false;
    // Tree
    tree:any =[]
    tabActive:any
    ref:any
    breadcrumbArray:any = []


    private _urlGetAllAsset: string = webApi.domain + webApi.url.getAllAsset;
    private _urlGetAllAssetVersions: string = webApi.domain + webApi.url.getAllAssetVersions;
    private _urlgetTree: string = webApi.domain + webApi.url.getTree;
    private _urlDeleteAllAssetVersions: string = webApi.domain + webApi.url.deleteAssetVersion;
    private _urlShareAllAsset:string = webApi.domain + webApi.url.getShareAsset;
    private _urlGetAllAssetCategories: string = webApi.domain + webApi.url.getAllAssetCategories;
    private _urlGetPendingAsset: string = webApi.domain + webApi.url.getPendingAsset;
    private _urlGetApprovedAsset: string = webApi.domain + webApi.url.getApprovedAsset;
    private _urlAddEditAsset: string = webApi.domain + webApi.url.addEditAsset;
    private _urlChangeAssetStatus: string = webApi.domain + webApi.url.changeAssetStatus;
    private _urldeleteFolder: string = webApi.domain + webApi.url.deleteFolder;
    private _urldownloadFile:string=webApi.domain+webApi.url.downloadFile;
    private _urldownloadFolder:string=webApi.domain+webApi.url.downloadFolder;
    private _urlgetAllAssest:string=webApi.domain+webApi.url.getAssetDetails;
    // private _urlGetAllTags: string = webApi.domain + webApi.url.getAllTags;
    private _urlGetAllTags: string = webApi.domain + webApi.url.getAllTagsComan;

    constructor(@Inject('APP_CONFIG_TOKEN') private config: AppConfig, private _http: Http, private _httpClient: HttpClient) {
        //this.busy = this._http.get('...').toPromise();
        if(localStorage.getItem('LoginResData')){
             this.userData = JSON.parse(localStorage.getItem('LoginResData'));
             console.log('userData', this.userData.data);
            //  this.userId = this.userData.data.data.id;
             this.tenantId = this.userData.data.data.tenantId;
          }
    }

    getAllAsset(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlGetAllAsset, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    getAllAssetCategories(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlGetAllAssetCategories, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    getAllAssetVersion(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlGetAllAssetVersions, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    deleteAssetVersion(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlDeleteAllAssetVersions, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    getApprovedAssetNew(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlShareAllAsset, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }


    getPendingAsset(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlGetPendingAsset, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    getApprovedAsset(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlGetApprovedAsset, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    addEditAsset(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlAddEditAsset, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    changeAssetStatus(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlChangeAssetStatus, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }
    deleteFolderFile(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urldeleteFolder, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    downloadFile(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urldownloadFile, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }
    downloadFolder(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urldownloadFolder, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }
    getAssetDetails(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlgetAllAssest, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }
    getAllTags(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlGetAllTags, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    /// Tree
    getAllTree(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlgetTree,param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }



    _errorHandler(error: Response) {
        console.error(error);
        return Observable.throw(error || "Server Error")
    }

}
