import { Injectable, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AppConfig } from '../../../../app.module';
import { webApi } from '../../../../service/webApi';
import { HttpClient } from "@angular/common/http";

@Injectable()
export class AddAssetService {
  userData;
  //tenantId: any = 1;
  addEditAssetData: any = [];
  tabId:any ;
  roleId:any;
  title:any;
  breadCrumbArray:any =[]
  previousBreadCrumb:any = []
  selectedIndex : any;
  addEditCategoryName: any 
  assetVersions:any = [];
  categoryId: any;
  view:any;
  shareCatId:any;
  reviewId:any
  dataFromAssetCategory: any = false;
  dataFromDAM: any = false; ///Direct From DAM

  private _urlGetAllDropdown: string = webApi.domain + webApi.url.getAllAssetDropDown;
  private _urlAddEditAsset: string = webApi.domain + webApi.url.addEditAsset;
  private _urlGetAllEmployees: string = webApi.domain + webApi.url.getAllEmployeeDAM;
  private _urlGetAllEdgeEmployee: string = webApi.domain + webApi.url.getAllEdgeEmployee;

  private getAllAuthorListForDamUrl: string = webApi.domain + webApi.url.getAllAuthorListForDam;

  private _urlGetPdfUrl: string = webApi.domain + webApi.url.getPdfUrl;
  private _urldeletePdfUrl: string = webApi.domain + webApi.url.deletePdfUrl;
  innerTabId: any;
  valuechanged: number;

  constructor(
    @Inject('APP_CONFIG_TOKEN')
    private config: AppConfig,
    private _http: Http,
    private _httpClient: HttpClient,
  ) {
    //this.busy = this._http.get('...').toPromise();
    if(localStorage.getItem('LoginResData')){
      this.userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', this.userData.data);
      // this.userId = this.userData.data.data.id;
      //this.tenantId = this.userData.data.data.tenantId;
   }
  }

  getAllAssetDropdown(param) {
    return new Promise(resolve => {
      this._httpClient.post(this._urlGetAllDropdown, param)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  addEditAsset(param) {

    return new Promise(resolve => {
      this._httpClient.post(this._urlAddEditAsset,param)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  getAllEmployee(param) {
    return new Promise(resolve => {
      this._httpClient.post(this._urlGetAllEmployees, param)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  getAllEdgeEmployee(param) {
    return new Promise(resolve => {
      this._httpClient.post(this._urlGetAllEdgeEmployee, param)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  getAllAuthorListForDamBySearch(param) {
    return new Promise(resolve => {
      this._httpClient.post(this.getAllAuthorListForDamUrl, param)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  getPdfLink(link) {
    const linkUrl = this._urlGetPdfUrl + link;
    return new Promise(resolve => {
      this._httpClient.get(linkUrl)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  deletePdfLink(link) { // server pdf link
    const linkUrl = this._urldeletePdfUrl + link;
    return new Promise(resolve => {
            this._httpClient.get(linkUrl)
            // .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve(null);
            });
        });
  }

  _errorHandler(error: Response) {
    console.error(error);
    return Observable.throw(error || "Server Error")
  }

}
