import {
  Component,
  OnInit,
  AfterViewInit,
  Renderer2,
  ViewChild,
  ChangeDetectorRef,
  OnDestroy,
  ViewEncapsulation,
} from '@angular/core';
import {
  FormBuilder,
  NgForm
} from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
//import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
// import { ToasterService } from 'angular2-toaster';
import { AddAssetService } from './add-asset.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { webAPIService } from '../../../../service/webAPIService';
import { webApi } from '../../../../service/webApi';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';
import { UriBuilder } from 'uribuilder';
import { cleanPath } from 'cleanpath';
import { HttpClient } from '@angular/common/http';
import * as $ from 'jquery';
import * as _ from 'lodash';
import { CommonFunctionsService } from '../../../../service/common-functions.service';
import { SuubHeader } from '../../../components/models/subheader.model';
import { BrandDetailsService } from '../../../../service/brand-details.service';
import { AssetService } from '../asset.service';
let __ = _.noConflict();
// let __ = ld.noConflict();
// this.LoDashExplicitWrapper.noConflict();
declare var kPoint;
declare var mime;
@Component({
  selector: 'ngx-add-asset',
  templateUrl: './add-asset.component.html',
  styleUrls: ['./add-asset.component.scss'],
  // encapsulation: ViewEncapsulation.None
})
export class AddAssetComponent implements OnDestroy, OnInit, AfterViewInit {
  CategoryName:any= ''
  submitted: boolean = false;

  @ViewChild('fileUpload') fileUpload: any;
  @ViewChild('video_file') videofile: any;
  @ViewChild('image_file') imagefile: any;
  @ViewChild('audio_file') audiofile: any;
  @ViewChild('iframe_file') iframe: any;
  @ViewChild('ppt_file') ppt: any;
  @ViewChild('word_file') word: any;
  @ViewChild('excel_file') excel: any;
  @ViewChild('scorm_file') scormfile: any;
  @ViewChild('kpplayer') kpplayer: any;
  @ViewChild('kpplayer1') kpplayer1: any;
  intialCheck = false;
  kpointurl: any =
    'https://showcase.kpoint.com/kapsule/gcc-1b568768-ad89-4d6c-b1aa-ecb0958617b4/nv3/embedded&autoplay=true';
  youtubeplayer: YT.Player;
  youtubeplay: any = {
    id: 'https://www.youtube.com/watch?v=n_GFN3a0yj0'
  };
  isLocalfile: boolean = false;
  youtubeurl: any; // = 'https://www.youtube.com/watch?v=n_GFN3a0yj0';
  fileUrl: any;
  fileName: any = 'You can drag and drop files here to add them.';
  fileIcon: any = 'assets/img/app/profile/avatar4.png';
  fileReaded: any;
  enableUpload: any = false;
  selectFileTitle: any = 'No file chosen';
  status: any = [];
  categories: any = [];
  formats: any = [];
  channels: any = [];
  languages: any = [];
  pdfUrlFlag = false;
  addEditAssetForm: any = {
    assetId: 0,
    assetName: '',
    description: '',
    category: '',
    format: '',
    channel: '',
    language: '',
    estimateCost: '',
    assetRef: '',
    customTags: [],
    selectedCreator: [],
    selectedEmployee: []
  };
  allEmployeesAuto: any = [];
  settingsCreatorDrop: any = {};
  settingsEmployeeDrop: any = {};
  tenantId: any;
  estLength: boolean = false;
  userId: any;
  getDataForAddEdit: any;
  addAction: boolean = false;
  dataFromAssetCategory: boolean = false;
  dataFromDAM: boolean = false;
  categoryId: any;
  pdfserverlink:string;
  kPointDiv: any = false;
  pngScormVideoAudioDiv: any = false;
  files: any;
  settingsTagDrop ={};
  tempTags:any =[];
  tagList:any = [];
  selectedTags:any = [];
  header:SuubHeader={
  title:'',
  btnsSearch: true,
  searchBar: false,
  dropdownlabel: ' ',
  drplabelshow: false,
  drpName1: '',
  drpName2: ' ',
  drpName3: '',
  drpName1show: false,
  drpName2show: false,
  drpName3show: false,
  btnName1: '',
  btnName2: 'Save',
  btnName3: '',
  btnAdd: '',
  btnName1show: false,
  btnName2show: true,
  btnName3show: false,
  btnBackshow: true,
  btnAddshow: false,
  filter: false,
  showBreadcrumb: true,
  breadCrumbList:[]

};
  assetrefLink: any;
  currentBrandData: any;
  isShowApprover: boolean = false;
  isApprover: any = 0;
  catshow: boolean = false;
  oldParentCatId: any;
  assetCat: any = [];
  demoassetCat: any;
  noCatwise: boolean = false;
  parentCatID: any = null;
  assetVersions: any = [];
  versionshow: boolean = false;
  search:any
  previewShow: boolean = false;
  previewData: any;
  pdfReference: any;
  previewName: string;
  demoVersions: any = [];
  breadCrumb: any ;
  track: any;
  tabId: any;
  breadCrumbList: any;
  title: any;
  formatExtensions: any;
  // _this: any;
  constructor(
    // private toasterService: ToasterService,
    private formBuilder: FormBuilder,
    private assetservice: AssetService,
    private addassetservice: AddAssetService,
    private commonFunctionService: CommonFunctionsService,
    private spinner: NgxSpinnerService,
    private datePipe: DatePipe,
    protected webApiService: webAPIService,
    public router: Router,
    public routes: ActivatedRoute,
    private sanitizer: DomSanitizer,
    private render: Renderer2,
    private cdf: ChangeDetectorRef,
    private http1: HttpClient,
    private toastr: ToastrService,
    public brandService: BrandDetailsService,
  ) {
    // this.getHelpContent();
    this.settingsTagDrop = {
      text: 'Select Tags',
      singleSelection: false,
      classes: 'myclass custom-class',
      primaryKey: 'id',
      labelKey: 'name',
      noDataLabel: 'Search Tags...',
      enableSearchFilter: true,
      searchBy: ['name'],
      maxHeight:250,
      lazyLoading: true,
    };
    // this.loadashwrapper.noConflict();

    if (this.addassetservice.dataFromAssetCategory) {
      this.dataFromAssetCategory = true;
      this.categoryId = this.addassetservice.categoryId;
    } else {
      this.dataFromAssetCategory = false;
    }

    if (this.addassetservice.dataFromDAM) {
      this.dataFromDAM = true;
      this.addAction = true;
    } else {
      this.dataFromDAM = false;
    }

    if (localStorage.getItem('LoginResData')) {
      var userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', userData.data);
      this.userId = userData.data.data.id;
      this.tenantId = userData.data.data.tenantId;
    }
    if (localStorage.getItem('formatExtensions')) {
      this.formatExtensions = JSON.parse(localStorage.getItem('formatExtensions'));
    }



    this.getallTagList();
    this.getAllAssetDropdown();
    this.getAllCatWise(this.parentCatID)
    this.getAllEmployeesDAM();

    this.getAllEdgeEmployee('');



    this.settingsCreatorDrop = {
      text: 'Search & Select Approver',
      singleSelection: true,
      classes: 'myclass custom-class',
      primaryKey: 'empId',
      labelKey: 'empName',
      noDataLabel: 'Search Approver...',
      enableSearchFilter: true,
      searchBy: ['empId', 'empName'],
      position: 'top',
      maxHeight:250,
      lazyLoading: true,
    };

    this.settingsEmployeeDrop = {
      text: 'Search & Select Author',
      singleSelection: false,
      classes: 'myclass custom-class',
      primaryKey: 'edgeempId',
      labelKey: 'edgeempName',
      noDataLabel: 'Search Author...',
      enableSearchFilter: true,
      searchBy: ['edgeempId', 'edgeempName'],
      maxHeight:250,
      lazyLoading: true,
    };
  }

  getallTagList() {
    this.spinner.show();
    let url = webApi.domain + webApi.url.getAllTagsComan;
    let param = {
      tenantId :this.tenantId
    };
    this.webApiService.getService(url, param)
      .then(rescompData => {
        // this.loader =false;
        this.spinner.hide();
        var temp: any = rescompData;
        this.tagList = temp.data;
        console.log(this.tagList,"taglist")
        this.tempTags = [... this.tagList];
        console.log(this.tempTags,"this.temptags")
        // console.log('Visibility Dropdown',this.visibility);

        this.getDataForAddEdit = this.addassetservice.addEditAssetData;
        if(this.getDataForAddEdit[0] === 'EDIT'){
        if(this.getDataForAddEdit[1].tagIds)
        {
          var tagIds =this.getDataForAddEdit[1].tagIds.split(',');
          if(tagIds.length > 0){
            this.tempTags.forEach((tag) => {
              tagIds.forEach((tagId)=>{
                if (tag.id == tagId ) {
                  this.selectedTags.push(tag);
                  console.log(this.selectedTags,"if selectedTags")
                }
              });
            });
            }
        }
      }

      },
        resUserError => {
          // this.loader =false;
          this.show =false;
          this.spinner.hide();
          this.errorMsg = resUserError;
        });
  }
  setbreadcrum(){
    this.header={
      title:'Add File',
      btnsSearch: true,
      searchBar: false,
      dropdownlabel: ' ',
      drplabelshow: false,
      drpName1: '',
      drpName2: ' ',
      drpName3: '',
      drpName1show: false,
      drpName2show: false,
      drpName3show: false,
      btnName1: '',
      btnName2: 'Save',
      btnName3: '',
      btnAdd: '',
      btnName1show: false,
      btnName2show: true,
      btnName3show: false,
      btnBackshow: true,
      btnAddshow: false,
      filter: false,
      showBreadcrumb: true,
      breadCrumbList:[
        {
          'name': 'DAM',
          'navigationPath': '/pages/dam',
        },]
  };

  }
  ngOnInit() {
    if(this.addassetservice.breadCrumbArray){
      this.breadCrumbList = this.addassetservice.breadCrumbArray
      this.title = this.addassetservice.title
    }
    if(this.addAction){
    this.header.title='Add File'
  }
    else{
    this.header.title='Edit File'
   }

    // this.header.breadCrumbList=[
    //   {
    //     'name': 'DAM',
    //     'navigationPath': '/pages/dam',
    //   },
    //   {
    //     'name': 'Asset Category',
    //     'navigationPath': '/pages/dam/asset-category',
    //   },
    //   {
    //     'name': 'Category Asset',
    //     'navigationPath': '/pages/dam/asset-category/asset',
    //   },]



    if(this.addassetservice.addEditCategoryName){
      this.CategoryName = this.addassetservice.addEditCategoryName
    }
    if(this.addassetservice.tabId){
      this.tabId = this.addassetservice.tabId
    }
    this.currentBrandData = this.brandService.getCurrentBrandData();
    if(this.currentBrandData.employee=="Employee"){
      this.getHelpContent();
    }else{
      this.getContent();
    }
    // this.settingsCreatorDrop.text='Search & select'+this.currentBrandData.employee
    // this.settingsEmployeeDrop.text='Search & select'+this.currentBrandData.employee
    // this.settingsEmployeeDrop.noDataLabel='Search '+this.currentBrandData.employee+'...'
    // this.settingsCreatorDrop.noDataLabel='Search '+this.currentBrandData.employee+'...'
  }

  removeFile() {
    this.assetFileData = null;
    this.fileName = 'You can drag and drop files here to add them.';
    this.selectFileTitle = 'No file chosen';
    this.addEditAssetForm.assetRef = '';
    this.assetrefLink =''
    this.isLocalfile = false;
    // this.nativeElementConfigure();
  }

  presentToast(type, body) {
    if(type === 'success'){
    this.toastr.success(body, 'Success', {
    closeButton: false
    });
    } else if(type === 'error'){
    this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
    timeOut: 0,
    closeButton: true
    });
    } else{
    this.toastr.warning(body, 'Warning', {
    closeButton: false
    })
    }
    }

  // add() {
  //   console.log(this.addEditAssetForm);
  // }

  formatSelection(data) {
    console.log('selected format:', data);
  }

  formatChange(event) {
    this.fileName = 'You can drag and drop files here to add them.';
    this.selectFileTitle = 'No file chosen';
    this.isLocalfile = false;
    this.addEditAssetForm.assetRef = '';
    this.assetrefLink =''
    this.cdf.detectChanges();
    this.nativeElementConfigure();
  }
  searchQuestion(event) {
    var temData = this.demoassetCat;
    var val = event.target.value.toLowerCase();
    var keys = [];
    // filter our data
    if (temData.length > 0) {
      keys = Object.keys(temData[0]);
    }
    if(val.length>=3||val.length==0){
    var temp = temData.filter((d) => {
      for (const key of keys) {
        if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
          return true;
        }
      }
    });
  }

  this.assetCat = temp
}

  prepareDataForAddEditAsset() {
    this.getDataForAddEdit = this.addassetservice.addEditAssetData;
    console.log('this.getDataForAddEdit', this.getDataForAddEdit);
    if (this.addassetservice.addEditAssetData) {
      if (this.addassetservice.addEditAssetData[0] === 'ADD') {
        this.header={
          // title:'Add File',
          title:this.title,
          btnsSearch: true,
          searchBar: false,
          dropdownlabel: ' ',
          drplabelshow: false,
          drpName1: '',
          drpName2: ' ',
          drpName3: '',
          drpName1show: false,
          drpName2show: false,
          drpName3show: false,
          btnName1: '',
          btnName2: 'Save',
          btnName3: '',
          btnAdd: '',
          btnName1show: false,
          btnName2show: true,
          btnName3show: false,
          btnBackshow: true,
          btnAddshow: false,
          filter: false,
         showBreadcrumb:true,
         breadCrumbList:this.breadCrumbList
          // breadCrumbList:[]
        };
        this.addAction = true;
        // if(this.categoryId){
        //   for (let i = 0; i < this.assetCat.length; i++) {
        //     if (this.assetCat[i].id == this.categoryId) {
        //       this.CategoryName = this.assetCat[i].categoryName
        //     }
        //   }
        // }
        this.addEditAssetForm = {
          assetId: 0,
          assetName: '',
          description: '',
          category: this.dataFromAssetCategory ? this.categoryId : '',
          format: '',
          channel: '',
          language: '',
          estimateCost: '',
          assetRef: '',
          customTags: [],
          appReq:0,
          selectedCreator: [],
          selectedEmployee: []
        };
      } else if (this.getDataForAddEdit[0] === 'EDIT') {
        this.getAssetVersions(this.getDataForAddEdit[1].assetId)
        // this.assetVersions =this.addassetservice.assetVersions
        if(this.tabId == 1 || this.tabId == 4){
        this.header={
          // title:'Edit File',
          title:this.title,
          btnsSearch: true,
          searchBar: false,
          dropdownlabel: ' ',
          drplabelshow: false,
          drpName1: '',
          drpName2: ' ',
          drpName3: '',
          drpName1show: false,
          drpName2show: false,
          drpName3show: false,
          btnName1: '',
          btnName2: 'Save',
          btnName3: '',
          btnAdd: '',
          btnName1show: false,
          btnName2show: true,
          btnName3show: false,
          btnBackshow: true,
          btnAddshow: false,
          filter: false,
      showBreadcrumb:true,
      breadCrumbList:this.breadCrumbList
        };
      }else{
        this.header={
          // title:'Edit File',
          title:this.title,
          btnsSearch: true,
          searchBar: false,
          dropdownlabel: ' ',
          drplabelshow: false,
          drpName1: '',
          drpName2: ' ',
          drpName3: '',
          drpName1show: false,
          drpName2show: false,
          drpName3show: false,
          btnName1: '',
          btnName2: 'Save',
          btnName3: '',
          btnAdd: '',
          btnName1show: false,
          btnName2show: false,
          btnName3show: false,
          btnBackshow: true,
          btnAddshow: false,
          filter: false,
          showBreadcrumb:true,
          breadCrumbList:this.breadCrumbList



        };
      }
        this.addAction = false;
        // if(this.categoryId){
        //   for (let i = 0; i < this.assetCat.length; i++) {
        //     if (this.assetCat[i].id == this.categoryId) {
        //       this.CategoryName = this.assetCat[i].categoryName
        //     }
        //   }
        // }
        const selectedEmployees: any = [];
        console.log(this.allEmployeeAutoDrop,"edit approver")
        this.allEmployeeAutoDrop.forEach(emp => {
          console.log(emp,"emp")
          console.log(emp.appId,"empId")
          console.log(this.getDataForAddEdit[1].reviewerId,"reviewerId")

          if (emp.appId == this.getDataForAddEdit[1].reviewerId) {
          // if (emp.appId == this.getDataForAddEdit[1].author) {
            let allEmployeeAutoDropObj = {};
            allEmployeeAutoDropObj = {
              empId: emp.empId,
              empName: emp.empName,
              appId:emp.appId
            };
            selectedEmployees.push(allEmployeeAutoDropObj);
          }
        });
        console.log('selectedEmployees', selectedEmployees);
        if(this.getDataForAddEdit[1].author){
        var author =this.getDataForAddEdit[1].author.split(',');
        // var allEmployeeEdgeAutoDropObj ={}
        console.log(this.allEmployeeEdgeAutoDrop,"data author")
        var selectedEdgeEmployee: any = [];
        this.allEmployeeEdgeAutoDrop.forEach((emp) => {
          author.forEach((auth) =>{
          if (emp.edgeempId == auth) {
            let allEmployeeEdgeAutoDropObj = {};
            allEmployeeEdgeAutoDropObj = {
              edgeempId: emp.edgeempId,
              edgeempName: emp.edgeempName,
              edgeempCode : emp.edgeempCode,
            }
            selectedEdgeEmployee.push(allEmployeeEdgeAutoDropObj);
          }
        });
        });
      }
        // const authorObj = {
        //   edgeempId: this.getDataForAddEdit[1].author,
        //   edgeempName: this.getDataForAddEdit[1].authorName,
        //   edgeempCode: this.getDataForAddEdit[1].authorCode,
        // };
        // selectedEdgeEmployee.push(authorObj);
        // this.allEmployeeEdgeAutoDrop.push(authorObj);
        console.log('selectedEdgeEmployee', selectedEdgeEmployee);

        if (this.getDataForAddEdit[1].estLength) {
          const resStr = this.getDataForAddEdit[1].estLength.split('.');
          var  formattedDuration = new Date();
          formattedDuration.setHours(resStr[0], resStr[1], resStr[2]);
        }

        //let cTags = this.getDataForAddEdit[1].tags.split(',');
        if (this.getDataForAddEdit[1].assetRef) {
          console.log(
            'fileNamexx',
            this.getDataForAddEdit[1].assetRef.lastIndexOf('/') + 1
          );
          this.fileName = this.getDataForAddEdit[1].assetRef.substr(
            this.getDataForAddEdit[1].assetRef.lastIndexOf('/') + 1
          );
          this.selectFileTitle = this.fileName
          console.log('fileName', this.fileName);
        }

        //console.log('cTags', cTags);
        this.addEditAssetForm = {
          assetId: this.getDataForAddEdit[1].assetId,
          assetName: this.getDataForAddEdit[1].assetName,
          description: this.getDataForAddEdit[1].description,
          category: this.getDataForAddEdit[1].categoryId,
          format: this.getDataForAddEdit[1].formatId,
          channel: this.getDataForAddEdit[1].channelId,
          language: this.getDataForAddEdit[1].languageId,
          estimateCost: this.getDataForAddEdit[1].cost,
          assetRef: this.getDataForAddEdit[1].assetRef,
          customTags:
            this.getDataForAddEdit[1].tags == ''
              ? []
              : this.getDataForAddEdit[1].tags,
          selectedCreator: selectedEmployees,
          appReq:this.getDataForAddEdit[1].appReq,
          selectedEmployee: selectedEdgeEmployee,
          time: formattedDuration,
          mimeType: this.getDataForAddEdit[1].mimeType,
          referenceType: this.getDataForAddEdit[1].referenceType
        };
        this.assetrefLink = this.getDataForAddEdit[1].assetRef

        // if(this.getDataForAddEdit[1].tagIds)
        // {
        //   var tagIds =this.getDataForAddEdit[1].tagIds.split(',');
        //   if(tagIds.length > 0){
        //     this.tempTags.forEach((tag) => {
        //       tagIds.forEach((tagId)=>{
        //         if (tag.id == tagId ) {
        //           this.selectedTags.push(tag);
        //           console.log(this.selectedTags,"if selectedTags")
        //         }
        //       });
        //     });
        //     }
        // }
        if(this.addEditAssetForm.appReq === 1){
          this.isShowApprover = true
          this.isApprover = 1
        }
        else{
          this.isShowApprover = false
          this.isApprover = 0

        }

            console.log(this.selectedTags,"selected tags")
        this.nativeElementConfigure();

        if (this.getDataForAddEdit[1].formatId == 3) {
          // Commented By Bhavesh Tandel
          // this.addEditAssetForm.reference = this.transform(
          //   this.getDocumentUrl(this.addEditAssetForm.assetRef)
          // );
          this.addassetservice.getPdfLink(this.addEditAssetForm.assetRef).then((result: any) => {
            this.pdfserverlink = result.data;
            this.addEditAssetForm.reference = this.sanitizer.bypassSecurityTrustResourceUrl(
                                                this.convertURL(this.pdfserverlink));
            if(result.data != null){
                this.pdfUrlFlag = true;
                this.intialCheck = true;
            }
            console.log('Link ===>', result);
          })
          .catch(result => {
            console.log('RESULT===>',result);
          });
          // this.addEditAssetForm.reference = this.transform(
          //   this.convertURL(this.addEditAssetForm.assetRef)
          // );
          // this.transform(this.addEditAssetForm.assetRef);
          console.log('PDF file', this.addEditAssetForm.reference);
          this.cdf.detectChanges();
        }

        if (this.getDataForAddEdit[1].formatId == 4) {
          //this.nativeElementConfigure();\
          setTimeout(() => {
            this.kpointConfigure(this.getDataForAddEdit[1].assetRef);
          }, 500);
        }
        if (this.getDataForAddEdit[1].formatId == 6) {
          setTimeout(() => {
            this.youtubeconfigure(this.getDataForAddEdit[1].assetRef);
          }, 500);
        }

        console.log('this.addEditAssetForm', this.addEditAssetForm);
      }
    }
    this.show =true;
  }

  activityFileData: any = null;
  // readFileUrl(event: any) {
  //   var size = 100000000;

  //   var validExts = new Array('image', 'video', 'audio', 'pdf');

  //   var fileType = event.target.files[0].type;

  //   var fileExt = false;
  //   for (let i = 0; i < validExts.length; i++) {
  //     if (fileType.includes(validExts[i])) {
  //       // return true;
  //       fileExt = true;
  //       break;
  //     }
  //   }

  //   // if(validExts.indexOf(fileType) < 0) {
  //   if (!fileExt) {
  //     var toast: Toast = {
  //       type: 'error',
  //       title: 'Invalid file selected!',
  //       body: 'Valid files are of ' + validExts.toString() + ' types.',
  //       showCloseButton: true,
  //       timeout: 2000
  //     };
  //     this.toasterService.pop(toast);
  //     this.cancelFile();
  //   } else {
  //     if (size <= event.target.files[0].size) {
  //       var toast: Toast = {
  //         type: 'error',
  //         title: 'File size exceeded!',
  //         body: 'File size should be less than 100MB',
  //         showCloseButton: true,
  //         timeout: 2000
  //       };
  //       this.toasterService.pop(toast);
  //       this.cancelFile();
  //     } else {
  //       if (event.target.files && event.target.files[0]) {
  //         this.fileName = event.target.files[0].name;
  //         //read file from input
  //         this.fileReaded = event.target.files[0];
  //         this.assetFileData = event.target.files[0];
  //         console.log('this.assetFileData', this.assetFileData);

  //         if (this.fileReaded != '' || this.fileReaded != null || this.fileReaded != undefined) {
  //           this.enableUpload = true;
  //         }
  //         let file = event.target.files[0];
  //         var reader = new FileReader();
  //         reader.onload = (event: ProgressEvent) => {
  //           this.fileUrl = (<FileReader>event.target).result;
  //         }
  //         reader.readAsDataURL(event.target.files[0]);
  //       }
  //     }
  //   }
  // }

  cancelFile() {
    // this.fileUpload.nativeElement.files = [];
    this.assetFileData = null;
    // console.log(this.fileUpload.nativeElement.files);
    if (this.fileUpload) {
      if (this.fileUpload.nativeElement) {
        this.fileUpload.nativeElement.value = null;
      }
    }

    // console.log(this.fileUpload.nativeElement.files);
    this.fileName = 'You can drag and drop files here to add them.';
    this.selectFileTitle = 'No file chosen';
    this.enableUpload = false;
  }

  formatTime(time) {
    let duration = new Date(time);
    let formatted = this.datePipe.transform(duration, 'H:mm:ss');
    console.log('formatted', formatted);
    let str = new String(formatted);
    let formattedFinal = str.replace(/:/g, '.');
    console.log('formattedFinal', formattedFinal);
    return formattedFinal;
  }

  getData: any = {};
  passParams: any;
  assetFileData: any;
  fileUploadRes: any;
  fileres: any;
  errorMsg: any;

  limit(event: any) {
    if (event.target.value < 0) {
      event.target.value = 0;
    }
    if (event.target.value > 999999) {
      event.target.value = 999999;
    }
  }

  backToAsset(data){
    this.addassetservice.categoryId = data.assetId
    this.addassetservice.title = data.assetName
    this.addassetservice.breadCrumbArray = this.addassetservice.breadCrumbArray.slice(0,data.index)
    // this.title = this.addassetservice.breadCrumbArray[]
    this.addassetservice.previousBreadCrumb = this.addassetservice.previousBreadCrumb.slice(0,data.index+1)

    this.router.navigate(['../'], { relativeTo: this.routes });


  }

  getAssetVersions(id){
    // this.spinner.show();
    let param = {

      'aId': id
    }

    this.assetservice.getAllAssetVersion(param)
      .then(rescompData => {
        var result = rescompData;
        console.log('versions:', result);
        if (result['type'] == true) {
          if (result['data'].length == 0) {
            // this.noAsset = true;
            // this.skeleton = true;
            // this.noDataFound = true;
            // this.presentToast('error', '');
          } else {
            this.assetVersions = result['data'];
            console.log(this.assetVersions,"this.assetVersions")
            this.versionshow = true


          }
          //this.skeleton = true;
        } else {
          // this.spinner.hide();
          // this.skeleton = true
          // this.noDataFound = true;

        }


      }, error => {
        // this.spinner.hide();
        // this.skeleton = true
        // this.noDataFound = true

        this.presentToast('error', '');
      });

  }

  deleteAssetVersion(data){
   var  param= {
    'aId':data.assetId,
    'avId':data.avId
    }
  this.assetservice.deleteAssetVersion(param)
  .then(rescompData => {
    var result = rescompData;
    console.log('versions:', result);
    if (result['type'] == true) {
      this.toastr.success('File version Deleted Successfully','Success')
      this.getAssetVersions(this.getDataForAddEdit[1].assetId)
      //this.skeleton = true;
    } else {
      // this.spinner.hide();
      // this.skeleton = true
      // this.noDataFound = true;

    }


  }, error => {
    // this.spinner.hide();
    // this.skeleton = true
    // this.noDataFound = true

    this.presentToast('error', '');
  });

}

  preview(data){
    this.previewData = data;
    if (this.previewData.formatId == 10) {
      this.previewName = 'PPT'
    }
    if (this.previewData.formatId == 11) {
      this.previewName = 'Excel'
    }
    if (this.previewData.formatId == 12) {
      this.previewName = 'Word'
    }
    if (this.previewData.formatId == 7) {
      this.previewName = 'Image'
    }
    if (this.previewData.formatId == 1) {
      this.previewName = 'Video'
    }
    if (this.previewData.formatId == 2) {
      this.previewName = 'Audio'
    }
    if (this.previewData.formatId == 8) {
      this.previewName = 'External Link'
    }
    if (this.previewData.formatId == 4) {
      this.previewName = 'KPoint'
      //this.nativeElementConfigure();\
      setTimeout(() => {
        this.kpointConfigure1(this.previewData.assetRef);
      }, 500);
    }
    if (this.previewData.formatId == 3) {
      this.previewName = 'PDF'
    this.addassetservice.getPdfLink(this.previewData.assetRef).then((result: any) => {
      this.pdfserverlink = result.data;
      this.pdfReference = this.sanitizer.bypassSecurityTrustResourceUrl(
                                          this.convertURL(this.pdfserverlink));

      console.log('Link ===>', result);
    })
    .catch(result => {
      console.log('RESULT===>',result);
    });
  }
    // this.addEditAssetForm.reference = this.transform(
    //   this.convertURL(this.addEditAssetForm.assetRef)
    // );
    // this.transform(this.addEditAssetForm.assetRef);
    console.log('PDF file', this.pdfReference);
    this.cdf.detectChanges();

    if (this.previewData.formatId == 6) {
      setTimeout(() => {
        this.youtubeconfigure(this.previewData.assetRef);
      }, 500);
    }

    this.previewShow  = true;


  }
  fileExistModal: boolean = false;
  dupFileError: string;
  show:boolean =false;
  onSubmit(data, assetForm: NgForm) {
    console.log(data.assetRef,"assetRef")
    console.log(assetForm,"assetForm")
    console.log(this.selectedTags,"selected Tags")
    this.submitted = true;
    console.log('assetForm', assetForm);
    console.log('data', data);
    // this.selectedTags.length > 0
    if (assetForm.valid && this.CategoryName.length>0) {
      if (data.assetRef) {
        console.log('data', data);

        this.spinner.show();
        console.log('data', data);
        // var tagStr = this.getTagDataReady(this.selectedTags);
        // console.log('tagStr', tagStr);
        let mimeType = null;
      let referenceType = null;
      if (data.assetRef) {
        const document = this.getMimeType(data.assetRef);
        console.log(document,"document")
        if (data.assetRef.includes('kapsule') || data.assetRef.includes('kpoint')) {
          mimeType = 'embedded/kpoint';
          referenceType = 'kpoint';
        } else if (data.assetRef.includes('youtube')) {
          mimeType = 'embedded/youtube';
          referenceType = 'youtube';
        } else if (document) {
          mimeType = document['mimetype'];
          referenceType = document['documenttype'];
        } else if (data.assetRef.includes('http') || data.assetRef.includes('https')){
          mimeType = 'application/x-msdownload';
          referenceType = 'application';
        }
      }
      if (this.selectedTags.length > 0) {
        this.makeTagDataReady(this.selectedTags);
         // this.formdata.tags = this.formattedTags;
       }
       if(data.selectedEmployee){
       this.makeAuthorDataReady(data.selectedEmployee)
       }
        const param = {
          assetsId: this.addAction ? 0 : data.assetId,
          assetsName: data.assetName,
          // 'assetsRef': this.addAction ? null : this.getDataForAddEdit[1].assetRef,
          assetsRef: data.assetRef,
          assetsBitly: null,
          assetsDesc: data.description?data.description:null,
          assetsCatId: data.category,
          assetsFormatId: data.format,
          assetsChannelId: data.channel,
          assetsLanguageId: data.language,
          assetsCost: data.estimateCost,
          tId: this.tenantId,
          userId: this.userId,
          assetsStatus: 3,
          assetsTags: this.tagstrAsset,
          approverId: this.isShowApprover?data.selectedCreator[0].appId:null,
          appReq:this.isApprover,
          // assetsAuthor: data.selectedEmployee[0].edgeempId,
          assetsAuthor:this.author,
          assetsLen: data.time ? data.format == 3 || data.format == 5 || data.format == 7 ?
                     null : this.formatTime(data.time) : null,
          mimeType: mimeType || data.mimeType,
          referenceType: referenceType || data.referenceType,
          // Solr Content Params
          name: data.assetName,
          tagging: this.tagstrAsset,
          description: data.description,
          // author: data.selectedEmployee[0].edgeempName,
          author:this.author
        };
        this.passParams = param;

        if (
          this.passParams.assetRef == 4 ||
          this.passParams.assetsFormatId == 4
        ) {
          this.passParams.mimeType = mimeType || 'embedded/kpoint';
          this.passParams.referenceType = referenceType || 'kpoint';
        }

        if (
          this.passParams.assetRef == 6 ||
          this.passParams.assetsFormatId == 6
        ) {
          this.assetFileData = null;
          this.passParams.mimeType = mimeType || 'embedded/youtube';
          this.passParams.referenceType = referenceType || 'youtube';
        }
        if (
          this.passParams.assetRef == 8 ||
          this.passParams.assetsFormatId == 8
        ) {
          this.assetFileData = null;
          this.passParams.mimeType = mimeType || 'application/x-msdownload';
          this.passParams.referenceType =  referenceType || 'application';
        }
        console.log('this.passParams', this.passParams);
        if (this.isLocalfile) {
          var fd = new FormData();
          fd.append('content', JSON.stringify(this.passParams));
          fd.append('file', this.assetFileData);
          console.log('File Data ', fd);

          console.log('Asset File', this.assetFileData);
          console.log('Asset Data ', this.passParams);
          let fileUploadUrl = webApi.domain + webApi.url.fileUpload;

          if (this.assetFileData) {
            this.webApiService.getService(fileUploadUrl, fd).then(
              rescompData => {
                const temp: any = rescompData;
                this.fileUploadRes = temp;
                console.log('rescompData', this.fileUploadRes);

                if (temp == 'err') {
                  this.presentToast('error', '');
                  this.spinner.hide();
                } else if (temp.type === false) {
                  this.presentToast('error', '');
                  this.spinner.hide();
                } else {
                  if (this.fileUploadRes.data != null || this.fileUploadRes.fileError !== true) {
                    this.assetFileData = null;
                    if ( this.fileUploadRes.fileExists ) {
                      this.fileExistModal = true;
                      this.dupFileError = this.fileUploadRes.status;
                    } else {
                      this.passParams.assetsRef = this.fileUploadRes.data.file_url;
                      this.passParams.mimeType = this.fileUploadRes.data.mime_type;
                      this.passParams.referenceType = this.fileUploadRes.data.file_type;
                      this.addEditAsset(this.passParams);
                      console.log('this.passparams', this.passParams);
                    }
                    this.spinner.hide();
                  } else {
                    this.presentToast('error', '');
                    this.spinner.hide();
                  }
                }
                console.log('File Upload Result', this.fileUploadRes);
                const res = this.fileUploadRes;
              },
              resUserError => {
                this.errorMsg = resUserError;
                console.log('File upload this.errorMsg', this.errorMsg);
                this.spinner.hide();
              }
            );
          } else {
            this.spinner.hide();
            this.addEditAsset(this.passParams);
          }
        } else {
          this.addEditAsset(this.passParams);
        }
      } else {
        this.presentToast('warning', 'Please select file.');
        console.log('please select file');
      }
    } else {
      this.presentToast('warning', 'Please fill in the required fields');
      Object.keys(assetForm.controls).forEach(key => {
        assetForm.controls[key].markAsDirty();
      });
    }

  }
    // Tag cganges
    tagstrAsset:string ='';
    makeTagDataReady(tagsData) {
      this.tagstrAsset  =''
       tagsData.forEach((tag)=>{
        if(this.tagstrAsset  == '')
        {
          this.tagstrAsset  = tag.id;
        }else
        {
          this.tagstrAsset = this.tagstrAsset +'|' + tag.id;
        }
        console.log('this.tagstrAsset',this.tagstrAsset);
       });
      // var tagsData = this.formdata.tags;
      // var tagsString = '';
      // if (tagsData) {
      //   for (let i = 0; i < tagsData.length; i++) {
      //     var tag = tagsData[i];
      //     if (tagsString != "") {
      //       tagsString += "|";
      //     }
      //     if (String(tag) != "" && String(tag) != "null") {
      //       tagsString += tag;
      //     }
      //   }
      //   this.formdata.tags1 = tagsString;
      // }
    }
    author:string

    makeAuthorDataReady(Data) {
      this.author  =''
       Data.forEach((tag)=>{
        if(this.author  == '')
        {
          this.author  = tag.edgeempId;
        }else
        {
          this.author = this.author +'|' + tag.edgeempId;
        }
        console.log('this.author',this.author);
       });
      // var tagsData = this.formdata.tags;
      // var tagsString = '';
      // if (tagsData) {
      //   for (let i = 0; i < tagsData.length; i++) {
      //     var tag = tagsData[i];
      //     if (tagsString != "") {
      //       tagsString += "|";
      //     }
      //     if (String(tag) != "" && String(tag) != "null") {
      //       tagsString += tag;
      //     }
      //   }
      //   this.formdata.tags1 = tagsString;
      // }
    }

onTagsSelect(item: any) {
  console.log(item);
  console.log(this.selectedTags);
}
OnTagDeSelect(item: any) {
  console.log(item);
  console.log(this.selectedTags);
}
onTagSearch(evt: any) {
  console.log(evt.target.value);
  const val = evt.target.value;
  this.tagList = [];
  const temp = this.tempTags.filter(function(d) {
    return (
      String(d.name)
        .toLowerCase()
        .indexOf(val) !== -1 ||
      !val
    );
  });

  // update the rows
  this.tagList = temp;
  console.log('filtered Tag LIst',this.tagList);
}
  closeFileExistModal() {
    this.fileExistModal = false;
  }

  fileExistModalSubmit(value) {
    if (value === 1) {
      this.passParams.assetsRef = this.fileUploadRes.data.docs[0].file_url;
      console.log('this.passParams.assetRef', this.passParams.assetRef);
      this.addEditAsset(this.passParams);
    } else {
      this.passParams.assetRef = null;
      this.addEditAssetForm.assetRef = null;
      this.isLocalfile = false;
      this.fileName = 'You can drag and drop files here to add them.';
      this.assetFileData = null;
    }
    this.fileExistModal = false;
   }

  addEditAsset(params) {
    console.log('AssetPushData:', params);
    this.spinner.show();
    const _urlAddEditAsset: string = webApi.domain + webApi.url.addEditAsset;
    this.commonFunctionService.httpPostRequest(_urlAddEditAsset,params)
    // this.addassetservice.addEditAsset(params)
    .then(
      rescompData => {
        this.spinner.hide();

        var temp = rescompData;
        var result = temp['data'];
        console.log('Add Result ', result);
        if (params.assetsId == 0) {
          if (temp['type'] == true) {
            this.presentToast('success', 'A new file added.');
            this.router.navigate(['../'], { relativeTo: this.routes });
            // this.router.navigate(['../../newasset'], { relativeTo: this.routes });
          } else {
            this.presentToast('error', '');
          }
        } else {
          console.log('Asset Edit Result ', result);
          if (temp['type'] == true) {
            if(!this.addAction){
              var length = this.addassetservice.breadCrumbArray.length-1
              this.title = this.addassetservice.breadCrumbArray[length].name
              this.addassetservice.title = this.title
              this.addassetservice.breadCrumbArray = this.addassetservice.breadCrumbArray.slice(0,length)
              // this.title = this.addassetservice.breadCrumbArray[]
              this.addassetservice.previousBreadCrumb = this.addassetservice.previousBreadCrumb.slice(0,length+1)
              console.log(this.title,"title")
              }

              this.presentToast('success', 'File updated.');


            this.router.navigate(['../'], { relativeTo: this.routes });
            // this.router.navigate(['../../newasset'], { relativeTo: this.routes });

          } else {
            this.presentToast('error', '');
          }
        }
      },
      resUserError => {
        this.spinner.hide();
        this.errorMsg = resUserError;
      }
    );
  }

  /////////////Need to improve(NULL value should be in tagString)//////////////

  getTagDataReady(customTags) {
    if (customTags.length > 0) {
      let tagsString = '';
      for (let i = 0; i < customTags.length; i++) {
        let tag = customTags[i];
        if (tagsString != '') {
          tagsString += '|';
        }
        if (tag.value) {
          if (String(tag.value) != '' && String(tag.value) != 'null') {
            tagsString += tag.value;
          }
        } else {
          if (String(tag) != '' && String(tag) != 'null') {
            tagsString += tag;
          }
        }
      }
      return tagsString;
    }
    // else if(customTags.length == 0){
    //   return this.assetForm.invalid;
    // }
  }

  noCategory: boolean = false;
  getAllAssetDropdown() {
    const param = {
      tId: this.tenantId,
    };
    const _urlGetAllDropdown: string = webApi.domain + webApi.url.getAllAssetDropDown;
    this.commonFunctionService.httpPostRequest(_urlGetAllDropdown,param)
    // this.addassetservice.getAllAssetDropdown(param)
    .then(
      rescompData => {
        var result = rescompData;
        console.log('All Asset Dropdown1', result);
        if (result['type'] == true) {
          console.log('All Asset Dropdown2', result);
          this.status = result['data'][0];
          if(result['data'][1]){
          this.languages = result['data'][1];
          }
          this.formats = result['data'][2];
          console.log(this.formats,"this.formats")
          this.channels = result['data'][3];
          this.categories = result['data'][4];

          if (this.categories.length === 0) {
            // this.noCategory = true;
          }
        } else {
          // var toast: Toast = {
          //   type: 'error',
          //   //title: 'Server Error!',
          //   body: 'Something went wrong.please try again later.',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }
      },
      resUserError => {
        // this.loader =false;
        // var toast: Toast = {
        //   type: 'error',
        //   //title: 'Server Error!',
        //   body: 'Something went wrong.please try again later.',
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      }
    );
  }

  selectedCreator: any = [];
  usersList: any = [];
  allEmployeeAutoDrop: any = [];

  prepareApproverDropdown() {
    this.allEmployeesAuto.forEach(element => {
      let allEmployeeAutoDropObj = {};
      allEmployeeAutoDropObj = {
        empId: element.id,
        empName: element.name,
        appId:element.appId
      };
      this.allEmployeeAutoDrop.push(allEmployeeAutoDropObj);
    });
    console.log('this.allEmployeeAutoDrop', this.allEmployeeAutoDrop);
  }

  onCreatorSelect(item: any) {
    console.log(item);
    console.log(this.selectedCreator);
  }

  OnCreatorDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedCreator);
  }

  onCreatorSearch(evt: any) {
    console.log(evt.target.value);
    const val = evt.target.value;
    this.usersList = [];
    console.log(this.allEmployeeAutoDrop);
    const tuser = this.allEmployeeAutoDrop.filter(function(d) {
      return (
        String(d.empId)
          .toLowerCase()
          .indexOf(val) !== -1 ||
        d.empName.toLowerCase().indexOf(val) !== -1 ||
        !val
      );
    });

    // update the rows
    this.usersList = tuser;
  }

  selectedEmployee: any = [];
  empList: any = [];
  allEmployeeEdgeAuto: any = [
    {
      id: 1,
      name: 'tag-1'
    },
    {
      id: 2,
      name: 'tag-2'
    }
  ];

  allEmployeeEdgeAutoDrop: any = [];

  prepareEmployeeDropdown() {
    this.allEmployeeEdgeAuto.forEach(element => {
      let allEmployeeEdgeAutoDropObj = {};
      allEmployeeEdgeAutoDropObj = {
        edgeempId: element.id,
        edgeempName: element.name,
        edgeempCode: element.code
      };
      this.allEmployeeEdgeAutoDrop.push(allEmployeeEdgeAutoDropObj);
    });
    console.log('this.allEmployeeEdgeAutoDrop', this.allEmployeeEdgeAutoDrop);
  }

  onEmployeeSelect(item: any) {
    console.log(item);
    if(item.edgeempId == 0){
      this.settingsEmployeeDrop = {
        text: 'Search & Select Author',
        singleSelection: false,
        classes: 'myclass custom-class',
        primaryKey: 'edgeempId',
        labelKey: 'edgeempName',
        noDataLabel: 'Search Author...',
        enableSearchFilter: true,
        limitSelection:1,
        searchBy: ['edgeempId', 'edgeempName'],
        maxHeight:250,
        lazyLoading: true,
      };
      var dummy = []
      dummy.push(item)
      this.addEditAssetForm.selectedEmployee = dummy
    }
    console.log(this.selectedEmployee);
  }

  OnEmployeeDeSelect(item: any) {
    console.log(item);
    if(item.edgeempId == 0){
      this.settingsEmployeeDrop = {
        text: 'Search & Select Author',
        singleSelection: false,
        classes: 'myclass custom-class',
        primaryKey: 'edgeempId',
        labelKey: 'edgeempName',
        noDataLabel: 'Search Author...',
        enableSearchFilter: true,
        limitSelection:this.allEmployeeEdgeAutoDrop.length,
        searchBy: ['edgeempId', 'edgeempName'],
        maxHeight:250,
        lazyLoading: true,
      };
    }
    console.log(this.selectedEmployee);
  }

  onEmployeeSearch(evt: any) {
    console.log(evt.target.value);
    const val = evt.target.value;
    this.allEmployeeEdgeAutoDrop = [];
    this.getAllEdgeEmployee(val);
    // this.empList = [];
    // const tuser = this.allEmployeeEdgeAutoDrop.filter(function (d) {
    //   return String(d.edgeempId).toLowerCase().indexOf(val) !== -1 ||
    //     d.edgeempName.toLowerCase().indexOf(val) !== -1 || !val;
    // });

    // update the rows
    //this.empList = tuser;
  }

  onAuthorSearch(evt: any) {
    console.log(evt.target.value);
    // this.allUNEnrolUser(evt.target.value);
    const val = evt.target.value;
    if (val.length >= 3) {
      this.fetchAllAuthorListBySearchAsync(val, authorData => {
        if (authorData.type === true) {
          // this.enrolldata = enrolData['data'];
          this.allEmployeeEdgeAuto = authorData['data'][0];
          console.log('this.allEmployeeEdgeAuto', this.allEmployeeEdgeAuto);
          this.prepareEmployeeDropdown();
          this.prepareDataForAddEditAsset();

          // this.allEmployeeEdgeAutoDrop = authorData['data'];
          this.allEmployeeEdgeAutoDrop = [...this.allEmployeeEdgeAutoDrop];
          console.log('Author list ', this.allEmployeeEdgeAutoDrop);

          this.empList = [];

          const tuser = this.allEmployeeEdgeAutoDrop.filter(function(d) {
            return (
              String(d.edgeempId)
                .toLowerCase()
                .indexOf(val) !== -1 ||
              d.edgeempName.toLowerCase().indexOf(val) !== -1 ||
              !val
            );
          });

          // update the rows
          this.empList = tuser;
          this.cdf.detectChanges();
        } else {
          console.log('Error getting author list ', authorData);
          this.allEmployeeEdgeAutoDrop = [];
          this.allEmployeeEdgeAutoDrop = [...this.allEmployeeEdgeAutoDrop];
          this.empList = [];
          this.cdf.detectChanges();
        }
      });
    }
    // else if (val.length === 0) {
    //   this.tempUsers = [];
    //   this.tempUsers = [...this.tempUsers];
    //   this.usersList = [];
    //   this.cdf.detectChanges();
    // }
    else {
      // this.subscription.unsubscribe();
    }
  }

  fetchAllAuthorListBySearchAsync(params: any, cb) {
    const data = {
      searchStr: params,
      tId: this.tenantId
    };
    const getAllAuthorListForDamUrl: string = webApi.domain + webApi.url.getAllAuthorListForDam;
    this.commonFunctionService.httpPostRequest(getAllAuthorListForDamUrl,data)
    // this.addassetservice.getAllAuthorListForDamBySearch(data)
    .then(
      res => {
        cb(res);
      },
      err => {
        console.log(err);
      }
    );
  }

  noEmployees: boolean = false;
  getAllEmployeesDAM() {
    this.spinner.show();
    let param = {
      tId: this.tenantId
    };
    const _urlGetAllEmployees: string = webApi.domain + webApi.url.getAllEmployeeDAM;
    this.commonFunctionService.httpPostRequest(_urlGetAllEmployees,param)
    // this.addassetservice.getAllEmployee(param)
    .then(
      rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('getAllEmployessDAM:', rescompData);
        if (result['type'] == true) {
          if (result['data'][0].length == 0) {
            this.noEmployees = true;
          } else {
            this.allEmployeesAuto = result['data'][0];
            console.log('this.allEmployeesAuto', this.allEmployeesAuto);
            this.prepareApproverDropdown();
            // this.getAllEdgeEmployee('');
            // this.prepareDataForAddEditAsset();
          }
        } else {
          // var toast: Toast = {
          //   type: 'error',
          //   //title: 'Server Error!',
          //   body: 'Something went wrong.please try again later.',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }
      },
      error => {
        this.spinner.hide();
        // var toast: Toast = {
        //   type: 'error',
        //   //title: 'Server Error!',
        //   body: 'Something went wrong.please try again later.',
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      }
    );
  }

  noEdgeEmployees: boolean = false;
  getAllEdgeEmployee(str) {
    // if (str.length > 3) {
      this.spinner.show();
      const param = {
        srcStr: str,
        tId: this.tenantId
      };
     const _urlGetAllEdgeEmployee: string = webApi.domain + webApi.url.getAllEdgeEmployee;
     this.commonFunctionService.httpPostRequest(_urlGetAllEdgeEmployee,param)
      // this.addassetservice.getAllEdgeEmployee(param)
      .then(
        rescompData => {
          this.spinner.hide();
          var result = rescompData;
          console.log('getAllEdgeEmployessDAM:', rescompData);
          if (result['type'] == true) {
            if (result['data'][0].length == 0) {
              this.noEdgeEmployees = true;
            } else {
              this.allEmployeeEdgeAuto = result['data'][0];
              console.log('this.allEmployeeEdgeAuto', this.allEmployeeEdgeAuto);
              this.prepareEmployeeDropdown();
              this.prepareDataForAddEditAsset();
            }
          } else {
            // var toast: Toast = {
            //   type: 'error',
            //   //title: 'Server Error!',
            //   body: 'Something went wrong.please try again later.',
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(toast);
          this.presentToast('error', '');
          }
        },
        error => {
          this.spinner.hide();
          // var toast: Toast = {
          //   type: 'error',
          //   //title: 'Server Error!',
          //   body: 'Something went wrong.please try again later.',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }
      );
    // }
  }

  back() {
    if (this.dataFromDAM) {
      // this.addassetservice.breadCrumbArray = this.addassetservice.breadCrumbArray.slice(0,data.index)

      window.history.back();
    }

    if (this.dataFromAssetCategory) {
      console.log(this.categoryId,"addhhhhh")
      // this.router.navigate(['../../asset'], { relativeTo: this.routes });
      // this.router.navigate(['../../newasset'], { relativeTo: this.routes });
      if(!this.addAction){
      var length = this.addassetservice.breadCrumbArray.length-1
      this.title = this.addassetservice.breadCrumbArray[length].name
      this.addassetservice.title = this.title
      this.addassetservice.breadCrumbArray = this.addassetservice.breadCrumbArray.slice(0,length)
      // this.title = this.addassetservice.breadCrumbArray[]
      this.addassetservice.previousBreadCrumb = this.addassetservice.previousBreadCrumb.slice(0,length+1)
      console.log(this.title,"title")
      }
      this.router.navigate(['../../assets'], { relativeTo: this.routes });

    }
  }

  file() {
    console.log(this.assetFileData);
  }

  ngAfterViewInit() {}

  // nativeElementConfigure() {
  //   setTimeout(() => {
  //     if (this.imagefile) {
  //       this.render.listen(this.imagefile.nativeElement, 'change', () => {
  //         this.isLocalfile = true;
  //         var size = 100000000;
  //         var validExts = new Array('image');
  //         this.files = this.imagefile.nativeElement.files;
  //         var fileType = this.files[0].type;
  //         console.log('fileType', fileType);
  //         var fileExt = false;
  //         for (let i = 0; i < validExts.length; i++) {
  //           if (fileType.includes(validExts[i])) {
  //             fileExt = true;
  //             break;
  //           }
  //         }
  //         if (!fileExt) {
  //           this.presentToast('warning', 'Please select a file of type image');
  //           this.removeFile();
  //         } else if (size <= this.files[0].size) {
  //           this.presentToast('warning', 'File size should be less than 100 MB');
  //           this.removeFile();
  //         } else {
  //           try {
  //             console.log('image', this.files[0]);
  //             this.addEditAssetForm.assetRef = this.files[0].name;
  //             this.assetrefLink ='';
  //             this.fileName = this.files[0].name;
  //             this.assetFileData = this.files[0];
  //             this.selectFileTitle = this.files[0].name;
  //             console.log('this.fileName', this.fileName);
  //             console.log('this.assetFileData', this.assetFileData);
  //             setTimeout(() => {
  //               let $source: any = $('#image_here');
  //               $source[0].src = URL.createObjectURL(this.files[0]);
  //               // $source.parent()[0].load();
  //             }, 200);
  //           } catch (e) {
  //             console.log(e);
  //           }
  //         }
  //       });
  //     }

  //     // $(document).on('change','#videofile',function(evt){
  //     if (this.videofile) {
  //       this.render.listen(this.videofile.nativeElement, 'change', () => {
  //         this.isLocalfile = true;
  //         var size = 1000000000;
  //         var validExts = new Array('video');
  //         this.files = this.videofile.nativeElement.files;
  //         var fileType = this.files[0].type;
  //         console.log('fileType', fileType);
  //         var fileExt = false;
  //         for (let i = 0; i < validExts.length; i++) {
  //           if (fileType.includes(validExts[i])) {
  //             fileExt = true;
  //             break;
  //           }
  //         }
  //         if (!fileExt) {
  //           this.presentToast('warning', 'Please select a file of type video');
  //           this.removeFile();
  //         } else if (size <= this.files[0].size) {
  //           this.presentToast('warning', 'File size should be less than 100 MB');
  //           this.removeFile();
  //         } else {
  //           try {
  //             console.log('Video', this.files[0]);
  //             this.addEditAssetForm.assetRef = this.files[0].name;
  //             this.assetrefLink ='';
  //             this.fileName = this.files[0].name;
  //             this.assetFileData = this.files[0];
  //             this.selectFileTitle = this.files[0].name;
  //             console.log('this.fileName', this.fileName);
  //             console.log('this.assetFileData', this.assetFileData);
  //             setTimeout(() => {
  //               let $source: any = $('#video_here');
  //               $source[0].src = window.URL.createObjectURL(this.files[0]); // URL.createObjectURL(this.files[0]);
  //               // $source.parent()[0].load();
  //             }, 200);
  //           } catch (e) {
  //             console.log(e);
  //           }
  //         }
  //       });
  //     }
  //     ////////////////Code for fetch audio file//////////////

  //     // $(document).on('change','#audiofile',function(evt){
  //     if (this.audiofile) {
  //       this.render.listen(this.audiofile.nativeElement, 'change', () => {
  //         this.isLocalfile = true;
  //         var size = 1000000000;
  //         this.files = this.audiofile.nativeElement.files;
  //         var validExts = new Array('audio');
  //         var fileType = this.files[0].type;
  //         console.log('fileType', fileType);
  //         var fileExt = false;
  //         for (let i = 0; i < validExts.length; i++) {
  //           if (fileType.includes(validExts[i])) {
  //             fileExt = true;
  //             break;
  //           }
  //         }
  //         if (!fileExt) {
  //           this.presentToast('warning', 'Please select a file type of audio');
  //           this.removeFile();
  //         } else if (size <= this.files[0].size) {
  //           this.presentToast('warning', 'File size should be less than 100 MB');
  //           this.removeFile();
  //         } else {
  //           try {
  //             console.log('audio', this.files[0]);
  //             this.addEditAssetForm.assetRef = this.files[0].name;
  //             this.assetrefLink ='';
  //             this.fileName = this.files[0].name;
  //             this.assetFileData = this.files[0];
  //             this.selectFileTitle = this.files[0].name;
  //             console.log('this.fileName', this.fileName);
  //             console.log('this.assetFileData', this.assetFileData);
  //             setTimeout(() => {
  //               let $source: any = $('#audio_here');
  //               $source[0].src = URL.createObjectURL(this.files[0]);
  //               // $source.parent()[0].load();
  //             }, 200);
  //           } catch (e) {
  //             console.log(e);
  //           }
  //         }
  //       });
  //     }
  //     ///////////////////audio file End///////////////////////

  //     // $(document).on('change','#iframefile',function(evt){
  //     if (this.iframe) {
  //       this.render.listen(this.iframe.nativeElement, 'change', () => {
  //         this.isLocalfile = true;
  //         var size = 1000000000;
  //         this.files = this.iframe.nativeElement.files;
  //         var validExts = new Array('pdf');
  //         var fileType = this.files[0].type;
  //         console.log('fileType', fileType);
  //         var fileExt = false;
  //         for (let i = 0; i < validExts.length; i++) {
  //           if (fileType.includes(validExts[i])) {
  //             fileExt = true;
  //             break;
  //           }
  //         }
  //         if (!fileExt) {
  //           this.presentToast('warning', 'Please select a file of type pdf');
  //           this.removeFile();
  //         } else if (size <= this.files[0].size) {
  //           this.presentToast('warning', 'File size should be less than 100 MB');
  //           this.removeFile();
  //         } else {
  //           try {
  //             console.log('pdf', this.files[0]);
  //             this.addEditAssetForm.assetRef = this.files[0].name;
  //             this.assetrefLink ='';
  //             this.fileName = this.files[0].name;
  //             this.assetFileData = this.files[0];
  //             this.selectFileTitle = this.files[0].name;
  //             console.log('this.fileName', this.fileName);
  //             console.log('this.assetFileData', this.assetFileData);
  //             setTimeout(() => {
  //               let $source: any = $('#iframe');
  //               $source[0].src = URL.createObjectURL(this.files[0]);
  //               // $source.parent()[0].load();
  //             }, 200);
  //           } catch (e) {
  //             console.log(e);
  //           }
  //         }
  //       });
  //     }

  //     if (this.ppt) {
  //       this.render.listen(this.ppt.nativeElement, 'change', () => {
  //         this.isLocalfile = true;
  //         var size = 1000000000;
  //         this.files = this.ppt.nativeElement.files;
  //         console.log("this.files",this.files)
  //         var validExts = new Array('application/vnd.ms-powerpoint','application/vnd.openxmlformats-officedocument.presentationml.presentation');
  //         var fileType = this.files[0].type;
  //         console.log('fileType', fileType);
  //         var fileExt = false;
  //         //var fileExt = true;
  //         for (let i = 0; i < validExts.length; i++) {
  //           if (fileType.includes(validExts[i])) {
  //             fileExt = true;
  //             break;
  //           }
  //         }
  //         if (!fileExt) {
  //           this.presentToast('warning', 'Please select a file of type ppt');
  //           this.removeFile();
  //         } else if (size <= this.files[0].size) {
  //           this.presentToast('warning', 'File size should be less than 100 MB');
  //           this.removeFile();
  //         } else {
  //           try {
  //             console.log('ppt', this.files[0]);
  //             this.addEditAssetForm.assetRef = this.files[0].name;
  //             this.assetrefLink ='';
  //             this.fileName = this.files[0].name;
  //             this.assetFileData = this.files[0];
  //             this.selectFileTitle = this.files[0].name;
  //             console.log('this.fileName', this.fileName);
  //             console.log('this.assetFileData', this.assetFileData);
  //             // setTimeout(() => {
  //             //   let $source: any = $('#iframe');
  //             //   $source[0].src = URL.createObjectURL(this.files[0]);
  //               // $source.parent()[0].load();
  //            // }, 200);
  //           } catch (e) {
  //             console.log(e);
  //           }
  //         }
  //       });
  //     }


  //     if (this.word) {
  //       this.render.listen(this.word.nativeElement, 'change', () => {
  //         this.isLocalfile = true;
  //         var size = 1000000000;
  //         this.files = this.word.nativeElement.files;
  //         console.log("this.files",this.files)
  //         var validExts = new Array('application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document');
  //         var fileType = this.files[0].type;
  //         console.log('fileType', fileType);
  //         var fileExt = false;
  //         //var fileExt = true;
  //         for (let i = 0; i < validExts.length; i++) {
  //           if (fileType.includes(validExts[i])) {
  //             fileExt = true;
  //             break;
  //           }
  //         }
  //         if (!fileExt) {
  //           this.presentToast('warning', 'Please select a file of type word');
  //           this.removeFile();
  //         } else if (size <= this.files[0].size) {
  //           this.presentToast('warning', 'File size should be less than 100 MB');
  //           this.removeFile();
  //         } else {
  //           try {
  //             console.log('ppt', this.files[0]);
  //             this.addEditAssetForm.assetRef = this.files[0].name;
  //             this.assetrefLink ='';
  //             this.fileName = this.files[0].name;
  //             this.assetFileData = this.files[0];
  //             this.selectFileTitle = this.files[0].name;
  //             console.log('this.fileName', this.fileName);
  //             console.log('this.assetFileData', this.assetFileData);
  //             // setTimeout(() => {
  //             //   let $source: any = $('#iframe');
  //             //   $source[0].src = URL.createObjectURL(this.files[0]);
  //               // $source.parent()[0].load();
  //            // }, 200);
  //           } catch (e) {
  //             console.log(e);
  //           }
  //         }
  //       });
  //     }


  //     if (this.excel) {
  //       this.render.listen(this.excel.nativeElement, 'change', () => {
  //         this.isLocalfile = true;
  //         var size = 1000000000;
  //         this.files = this.excel.nativeElement.files;
  //         console.log("this.files",this.files)
  //         var validExts = new Array('application/vnd.ms-excel','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  //         var fileType = this.files[0].type;
  //         console.log('fileType', fileType);
  //         var fileExt = false;
  //         //var fileExt = true;
  //         for (let i = 0; i < validExts.length; i++) {
  //           if (fileType.includes(validExts[i])) {
  //             fileExt = true;
  //             break;
  //           }
  //         }
  //         if (!fileExt) {
  //           this.presentToast('warning', 'Please select a file of type word');
  //           this.removeFile();
  //         } else if (size <= this.files[0].size) {
  //           this.presentToast('warning', 'File size should be less than 100 MB');
  //           this.removeFile();
  //         } else {
  //           try {
  //             console.log('ppt', this.files[0]);
  //             this.addEditAssetForm.assetRef = this.files[0].name;
  //             this.assetrefLink ='';
  //             this.fileName = this.files[0].name;
  //             this.assetFileData = this.files[0];
  //             this.selectFileTitle = this.files[0].name;
  //             console.log('this.fileName', this.fileName);
  //             console.log('this.assetFileData', this.assetFileData);
  //             // setTimeout(() => {
  //             //   let $source: any = $('#iframe');
  //             //   $source[0].src = URL.createObjectURL(this.files[0]);
  //               // $source.parent()[0].load();
  //            // }, 200);
  //           } catch (e) {
  //             console.log(e);
  //           }
  //         }
  //       });
  //     }




  //     if (this.scormfile) {
  //       this.render.listen(this.scormfile.nativeElement, 'change', () => {
  //         this.isLocalfile = true;
  //         var size = 100000000;
  //         this.files = this.scormfile.nativeElement.files;
  //         var validExts = new Array('zip');
  //         var fileType = this.files[0].type;
  //         console.log('fileType', fileType);
  //         var fileExt = false;
  //         for (let i = 0; i < validExts.length; i++) {
  //           if (fileType.includes(validExts[i])) {
  //             fileExt = true;
  //             break;
  //           }
  //         }
  //         if (!fileExt) {
  //           this.presentToast('warning', 'Please select a zip file');
  //           this.removeFile();
  //         } else if (size <= this.files[0].size) {
  //           this.presentToast('warning', 'File size should be less than 100 MB');
  //           this.removeFile();
  //         } else {
  //           try {
  //             console.log('zip', this.files[0]);
  //             this.addEditAssetForm.assetRef = this.files[0].name;
  //             this.assetrefLink ='';
  //             this.fileName = this.files[0].name;
  //             this.selectFileTitle = this.files[0].name;
  //             this.assetFileData = this.files[0];
  //             console.log('this.fileName', this.fileName);
  //             console.log('this.assetFileData', this.assetFileData);
  //             //this.pdfUpload = true;
  //             // setTimeout( () => {
  //             //   let $source: any = $('#iframe');
  //             //   $source[0].src = URL.createObjectURL(this.files[0]);
  //             //   $source.parent()[0].load();
  //             // }, 200)
  //           } catch (e) {
  //             console.log(e);
  //           }
  //         }
  //       });
  //     }
  //   }, 500);
  // }
  nativeElementConfigure() {
    var validExts = this.formatExtensions
    setTimeout(() => {
      if (this.imagefile) {
        this.render.listen(this.imagefile.nativeElement, 'change', () => {
          this.isLocalfile = true;
          var size = 100000000;
          // var validExts = new Array('image');
          this.files = this.imagefile.nativeElement.files;
          // var fileType = this.files[0].type;
          // var fileType = this.files[0].name.split('.')[1];
          var last_dot = this.files[0].name.lastIndexOf('.')
          var fileType = this.files[0].name.slice(last_dot + 1)
          console.log('fileType', fileType);
          var fileExt = false;
          for (let i = 0; i < validExts.length; i++) {
            if(validExts[i].formatId == 7){
            if (fileType.includes(validExts[i].extension)) {
              fileExt = true;
              break;
            }
          }
          }
          if (!fileExt) {
            this.presentToast('warning', 'Please select a file of type image');
            this.removeFile();
          } else if (size <= this.files[0].size) {
            this.presentToast('warning', 'File size should be less than 100 MB');
            this.removeFile();
          } else {
            try {
              console.log('image', this.files[0]);
              this.addEditAssetForm.assetRef = this.files[0].name;
              this.assetrefLink ='';
              this.fileName = this.files[0].name;
              this.assetFileData = this.files[0];
              this.selectFileTitle = this.files[0].name;
              console.log('this.fileName', this.fileName);
              console.log('this.assetFileData', this.assetFileData);
              setTimeout(() => {
                let $source: any = $('#image_here');
                $source[0].src = URL.createObjectURL(this.files[0]);
                // $source.parent()[0].load();
              }, 200);
            } catch (e) {
              console.log(e);
            }
          }
        });
      }

      // $(document).on('change','#videofile',function(evt){
      if (this.videofile) {
        this.render.listen(this.videofile.nativeElement, 'change', () => {
          this.isLocalfile = true;
          var size = 1000000000;
          // var validExts = new Array('video');
          this.files = this.videofile.nativeElement.files;
          // var fileType = this.files[0].type;
          // var fileType = this.files[0].name.split('.')[1];
          var last_dot = this.files[0].name.lastIndexOf('.')
          var fileType = this.files[0].name.slice(last_dot + 1)
          console.log('fileType', fileType);
          var fileExt = false;
          for (let i = 0; i < validExts.length; i++) {
            if(validExts[i].formatId == 1){
            if (fileType.includes(validExts[i].extension)) {
              fileExt = true;
              break;
            }
          }
          }
          if (!fileExt) {
            this.presentToast('warning', 'Please select a file of type video');
            this.removeFile();
          } else if (size <= this.files[0].size) {
            this.presentToast('warning', 'File size should be less than 100 MB');
            this.removeFile();
          } else {
            try {
              console.log('Video', this.files[0]);
              this.addEditAssetForm.assetRef = this.files[0].name;
              this.assetrefLink ='';
              this.fileName = this.files[0].name;
              this.assetFileData = this.files[0];
              this.selectFileTitle = this.files[0].name;
              console.log('this.fileName', this.fileName);
              console.log('this.assetFileData', this.assetFileData);
              setTimeout(() => {
                let $source: any = $('#video_here');
                $source[0].src = window.URL.createObjectURL(this.files[0]); // URL.createObjectURL(this.files[0]);
                // $source.parent()[0].load();
              }, 200);
            } catch (e) {
              console.log(e);
            }
          }
        });
      }
      ////////////////Code for fetch audio file//////////////

      // $(document).on('change','#audiofile',function(evt){
      if (this.audiofile) {
        this.render.listen(this.audiofile.nativeElement, 'change', () => {
          this.isLocalfile = true;
          var size = 1000000000;
          this.files = this.audiofile.nativeElement.files;
          // var validExts = new Array('audio');
          // var fileType = this.files[0].type;
          // var fileType = this.files[0].name.split('.')[1];
          var last_dot = this.files[0].name.lastIndexOf('.')
          var fileType = this.files[0].name.slice(last_dot + 1)
          console.log('fileType', fileType);
          var fileExt = false;
          for (let i = 0; i < validExts.length; i++) {
            if(validExts[i].formatId == 2){
            if (fileType.includes(validExts[i].extension)) {
              fileExt = true;
              break;
            }
          }
          }
          if (!fileExt) {
            this.presentToast('warning', 'Please select a file type of audio');
            this.removeFile();
          } else if (size <= this.files[0].size) {
            this.presentToast('warning', 'File size should be less than 100 MB');
            this.removeFile();
          } else {
            try {
              console.log('audio', this.files[0]);
              this.addEditAssetForm.assetRef = this.files[0].name;
              this.assetrefLink ='';
              this.fileName = this.files[0].name;
              this.assetFileData = this.files[0];
              this.selectFileTitle = this.files[0].name;
              console.log('this.fileName', this.fileName);
              console.log('this.assetFileData', this.assetFileData);
              setTimeout(() => {
                let $source: any = $('#audio_here');
                $source[0].src = URL.createObjectURL(this.files[0]);
                // $source.parent()[0].load();
              }, 200);
            } catch (e) {
              console.log(e);
            }
          }
        });
      }
      ///////////////////audio file End///////////////////////

      // $(document).on('change','#iframefile',function(evt){
      if (this.iframe) {
        this.render.listen(this.iframe.nativeElement, 'change', () => {
          this.isLocalfile = true;
          var size = 1000000000;
          this.files = this.iframe.nativeElement.files;
          // var validExts = new Array('pdf');
          // var fileType = this.files[0].type;
          // var fileType = this.files[0].name.split('.')[1];
          var last_dot = this.files[0].name.lastIndexOf('.')
          var fileType = this.files[0].name.slice(last_dot + 1)
          console.log('fileType', fileType);
          var fileExt = false;
          for (let i = 0; i < validExts.length; i++) {
            if(validExts[i].formatId == 3){
            if (fileType.includes(validExts[i].extension)) {
              fileExt = true;
              break;
            }
          }
          }
          if (!fileExt) {
            this.presentToast('warning', 'Please select a file of type pdf');
            this.removeFile();
          } else if (size <= this.files[0].size) {
            this.presentToast('warning', 'File size should be less than 100 MB');
            this.removeFile();
          } else {
            try {
              console.log('pdf', this.files[0]);
              this.addEditAssetForm.assetRef = this.files[0].name;
              this.assetrefLink ='';
              this.fileName = this.files[0].name;
              this.assetFileData = this.files[0];
              this.selectFileTitle = this.files[0].name;
              console.log('this.fileName', this.fileName);
              console.log('this.assetFileData', this.assetFileData);
              setTimeout(() => {
                let $source: any = $('#iframe');
                $source[0].src = URL.createObjectURL(this.files[0]);
                // $source.parent()[0].load();
              }, 200);
            } catch (e) {
              console.log(e);
            }
          }
        });
      }

      if (this.ppt) {
        this.render.listen(this.ppt.nativeElement, 'change', () => {
          this.isLocalfile = true;
          var size = 1000000000;
          this.files = this.ppt.nativeElement.files;
          console.log("this.files",this.files)
          // var validExts = new Array('application/vnd.ms-powerpoint','application/vnd.openxmlformats-officedocument.presentationml.presentation');
          // var fileType = this.files[0].type;
          // var fileType = this.files[0].name.split('.')[1];
          var last_dot = this.files[0].name.lastIndexOf('.')
          var fileType = this.files[0].name.slice(last_dot + 1)
          console.log('fileType', fileType);
          var fileExt = false;
          //var fileExt = true;
          for (let i = 0; i < validExts.length; i++) {
            if(validExts[i].formatId == 10){
            if (fileType.includes(validExts[i].extension)) {
              fileExt = true;
              break;
            }
          }
          }
          if (!fileExt) {
            this.presentToast('warning', 'Please select a file of type ppt');
            this.removeFile();
          } else if (size <= this.files[0].size) {
            this.presentToast('warning', 'File size should be less than 100 MB');
            this.removeFile();
          } else {
            try {
              console.log('ppt', this.files[0]);
              this.addEditAssetForm.assetRef = this.files[0].name;
              this.assetrefLink ='';
              this.fileName = this.files[0].name;
              this.assetFileData = this.files[0];
              this.selectFileTitle = this.files[0].name;
              console.log('this.fileName', this.fileName);
              console.log('this.assetFileData', this.assetFileData);
              // setTimeout(() => {
              //   let $source: any = $('#iframe');
              //   $source[0].src = URL.createObjectURL(this.files[0]);
                // $source.parent()[0].load();
             // }, 200);
            } catch (e) {
              console.log(e);
            }
          }
        });
      }


      if (this.word) {
        this.render.listen(this.word.nativeElement, 'change', () => {
          this.isLocalfile = true;
          var size = 1000000000;
          this.files = this.word.nativeElement.files;
          console.log("this.files",this.files)
          // var validExts = new Array('application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document');
          // var fileType = this.files[0].type;
          // var fileType = this.files[0].name.split('.')[1];
          var last_dot = this.files[0].name.lastIndexOf('.')
          var fileType = this.files[0].name.slice(last_dot + 1)
          console.log('fileType', fileType);
          var fileExt = false;
          //var fileExt = true;
          for (let i = 0; i < validExts.length; i++) {
            if(validExts[i].formatId == 12){
            if (fileType.includes(validExts[i].extension)) {
              fileExt = true;
              break;
            }
          }
          }
          if (!fileExt) {
            this.presentToast('warning', 'Please select a file of type word');
            this.removeFile();
          } else if (size <= this.files[0].size) {
            this.presentToast('warning', 'File size should be less than 100 MB');
            this.removeFile();
          } else {
            try {
              console.log('ppt', this.files[0]);
              this.addEditAssetForm.assetRef = this.files[0].name;
              this.assetrefLink ='';
              this.fileName = this.files[0].name;
              this.assetFileData = this.files[0];
              this.selectFileTitle = this.files[0].name;
              console.log('this.fileName', this.fileName);
              console.log('this.assetFileData', this.assetFileData);
              // setTimeout(() => {
              //   let $source: any = $('#iframe');
              //   $source[0].src = URL.createObjectURL(this.files[0]);
                // $source.parent()[0].load();
             // }, 200);
            } catch (e) {
              console.log(e);
            }
          }
        });
      }


      if (this.excel) {
        this.render.listen(this.excel.nativeElement, 'change', () => {
          this.isLocalfile = true;
          var size = 1000000000;
          this.files = this.excel.nativeElement.files;
          console.log("this.files",this.files)
          // var validExts = new Array('application/vnd.ms-excel','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
          // var fileType = this.files[0].type;
          // var fileType = this.files[0].name.split('.')[1];
          var last_dot = this.files[0].name.lastIndexOf('.')
          var fileType = this.files[0].name.slice(last_dot + 1)
          console.log('fileType', fileType);
          var fileExt = false;
          //var fileExt = true;
          for (let i = 0; i < validExts.length; i++) {
            if(validExts[i].formatId == 11){
            if (fileType.includes(validExts[i].extension)) {
              fileExt = true;
              break;
            }
          }
          }
          if (!fileExt) {
            this.presentToast('warning', 'Please select a file of type word');
            this.removeFile();
          } else if (size <= this.files[0].size) {
            this.presentToast('warning', 'File size should be less than 100 MB');
            this.removeFile();
          } else {
            try {
              console.log('ppt', this.files[0]);
              this.addEditAssetForm.assetRef = this.files[0].name;
              this.assetrefLink ='';
              this.fileName = this.files[0].name;
              this.assetFileData = this.files[0];
              this.selectFileTitle = this.files[0].name;
              console.log('this.fileName', this.fileName);
              console.log('this.assetFileData', this.assetFileData);
              // setTimeout(() => {
              //   let $source: any = $('#iframe');
              //   $source[0].src = URL.createObjectURL(this.files[0]);
                // $source.parent()[0].load();
             // }, 200);
            } catch (e) {
              console.log(e);
            }
          }
        });
      }




      if (this.scormfile) {
        this.render.listen(this.scormfile.nativeElement, 'change', () => {
          this.isLocalfile = true;
          var size = 100000000;
          this.files = this.scormfile.nativeElement.files;
          // var validExts = new Array('zip');
          // var fileType = this.files[0].type;
          var fileType = this.files[0].name.split('.')[1];
          console.log('fileType', fileType);
          var fileExt = false;
          for (let i = 0; i < validExts.length; i++) {
            if(validExts[i].formatId == 5){
            if (fileType.includes(validExts[i].extension)) {
              fileExt = true;
              break;
            }
          }
          }
          if (!fileExt) {
            this.presentToast('warning', 'Please select a zip file');
            this.removeFile();
          } else if (size <= this.files[0].size) {
            this.presentToast('warning', 'File size should be less than 100 MB');
            this.removeFile();
          } else {
            try {
              console.log('zip', this.files[0]);
              this.addEditAssetForm.assetRef = this.files[0].name;
              this.assetrefLink ='';
              this.fileName = this.files[0].name;
              this.selectFileTitle = this.files[0].name;
              this.assetFileData = this.files[0];
              console.log('this.fileName', this.fileName);
              console.log('this.assetFileData', this.assetFileData);
              //this.pdfUpload = true;
              // setTimeout( () => {
              //   let $source: any = $('#iframe');
              //   $source[0].src = URL.createObjectURL(this.files[0]);
              //   $source.parent()[0].load();
              // }, 200)
            } catch (e) {
              console.log(e);
            }
          }
        });
      }
    }, 500);
  }
  convertFileUrl(fileurl) {
    console.log(fileurl,"fileUrl");
    if (!fileurl) {
      return fileurl;
    } else if (fileurl.includes('C:\fakepath')) {
      return fileurl.split('C:\fakepath')[1];
    } else {
      return fileurl;
    }
  }
  kpointConfigure(kpointlink) {
  //  this.addEditAssetForm.assetRef = kpointlink;
    this.kpointurl = kpointlink;
    try {
      if (!this.kpointurl) {
        alert('Please Enter Url');
      } else {
        const kpoint = this.populateKpointObj(this.kpointurl);
        const player = kPoint.Player(this.kpplayer.nativeElement, {
          kvideoId: kpoint.id,
          videoHost: kpoint.host,
          params: { autoplay: true }
        });
      }
    } catch (e) {
      console.log(e);
    }
  }
  kpointConfigure1(kpointlink) {
    //  this.addEditAssetForm.assetRef = kpointlink;
      // this.kpointurl = kpointlink;
      try {
        if (!kpointlink) {
          alert('Please Enter Url');
        } else {
          const kpoint = this.populateKpointObj(kpointlink);
          const player = kPoint.Player(this.kpplayer1.nativeElement, {
            kvideoId: kpoint.id,
            videoHost: kpoint.host,
            params: { autoplay: true }
          });
        }
      } catch (e) {
        console.log(e);
      }
    }
  populateKpointObj(url) {
    // tslint:disable-next-line:prefer-const
    let kpoint: any = {
      host: '',
      id: '',
      src: '',
      url: ''
    };
    if (url.split('/').length > 0) {
      kpoint.host = url.split('/')[2];
      kpoint.id = url.split('/')[4];
      kpoint.src = url;
      kpoint.url = url.split('/')[0] + '//' + url.split('/')[2];
    }
    return kpoint;
  }
  youtubeconfigure(link) {
   // this.addEditAssetForm.assetRef = link;
    // this.youtubeurl = link;
    console.log('youtubeurl', link);
    try {
      if (!link) {
        alert('Please Enter url');
      } else {
        const videoid = this.populateYoutubeObj(link);
        // if (this.youtubeplayer) {
        //   this.youtubeplayer.loadVideoById(videoid);
        // } else {
        //   this.youtubeplay.id = videoid;
        // }
        const url =
          'https://www.youtube.com/embed/' + videoid + '?autoplay=1&showinfo=0';
        this.youtubeurl = this.transform(url);
      }
    } catch (e) {
      console.log(e);
    }
  }
  transform(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
  convertURL (url) {
    return webApi.domain + webApi.url.pdfviewerurl + url;
  }
  savePlayer(player) {
    this.youtubeplayer = player;
    console.log('player instance', player);
  }
  onStateChange(event) {
    console.log('player state', event.data);
  }
  populateYoutubeObj(url) {
    try {
      if (url) {
        let video_id = '';
        if (url.includes('embed')) {
          const urlsplit = url.split('/');
          video_id = urlsplit[urlsplit.length - 1];
        } else {
          video_id = url.split('v=')[1];
          const ampersandPosition = video_id.indexOf('&');
          if (ampersandPosition !== -1) {
            video_id = video_id.substring(0, ampersandPosition);
          }
        }
        return video_id;
      } else {
        return null;
      }
    } catch (e) {
      console.log(e);
      return null;
    }
  }
  encodeURI(url) {
    if (UriBuilder.parse(url).isRelative()) {
      url = cleanPath(document.baseURI + '/' + url);
    }
    return encodeURIComponent(url);
  }

  getDocumentUrl(url) {
    return (
      'https://docs.google.com/gview?url=' + encodeURI(url) + '&embedded=true'
    );
  }

  // Help Code Start Here //

  helpContent: any;
  getHelpContent() {
    return new Promise(resolve => {
      this.http1
        .get('../../../../../../assets/help-content/addEditCourseContent.json')
        .subscribe(
          data => {
            this.helpContent = data;
            console.log('Help Array', this.helpContent);
          },
          err => {
            resolve('err');
          }
        );
    });
    // return this.helpContent;
  }

  //functions of New Requirement

  CatModal(){
    console.log("enters")
    this.catshow = true
    parent = null
    this.getAllCatWise(parent)
  }

  getAllCatWise(parent){
    var param ={
      parentCatId:parent
    }
    const _urlgetcat: string = webApi.domain + webApi.url.getAllAssetCatWise;
    this.commonFunctionService.httpPostRequest(_urlgetcat,param).then(rescompData=>{
      console.log(rescompData,'catdata')
              // this.spinner.hide();
              var result = rescompData;
              console.log('getAssetResponse:', rescompData);
              if (result['type'] == true) {
                if (result['data'][0].length === 0) {
                  this.oldParentCatId = null;
                } else {
                  this.oldParentCatId = result['data'][0][0].parCatId;
                }
                if (result['data'][1].length == 0) {
                  this.noCatwise = true
                  // this.noAsset = true;
                  // this.skeleton = true
                } else {

                  this.assetCat = result['data'][1];
                  if(this.assetCat.length === 0){
                    this.noCategory = true
                  }
                  this.demoassetCat  = result['data'][1];
                  this.noCatwise = false
                  if(this.categoryId){
                  for (let i = 0; i < this.assetCat.length; i++) {
                    if (this.assetCat[i].id == this.categoryId) {
                      this.CategoryName = this.assetCat[i].categoryName
                    }
                  }
                }

                  // this.displayArray = this.asset;
                  // this.displayArray = []
                  // this.addItems(0, this.sum, 'push', this.asset);


                  // this.noDataFound = false;
                  // this.skeleton = true;
                  // this.noAsset = false;
                  // console.log('this.asset', this.asset);
                  // console.log('this.displayArray', this.displayArray);
                  // this.cdf.detectChanges();
                }
              } else {
                this.spinner.hide();
                // this.skeleton = true
                // this.noDataFound = true;
                // var toast: Toast = {
                //   type: 'error',
                //   //title: "Server Error!",
                //   body: 'Something went wrong.please try again later.',
                //   showCloseButton: true,
                //   timeout: 2000
                // };
                // this.toasterService.pop(toast);
                this.presentToast('error', '');
              }
              //this.skeleton = true

            }, error => {
              // this.spinner.hide();
              // this.skeleton = true
              // this.noDataFound = true;
              // var toast: Toast = {
              //   type: 'error',
              //   //title: "Server Error!",
              //   body: 'Something went wrong.please try again later.',
              //   showCloseButton: true,
              //   timeout: 2000
              // };
              // this.toasterService.pop(toast);
              this.presentToast('error', '');
            });


  }
  activeSelectedCatId:any
  selectedItem:any
  setActiveSelectedCat(index,item){
    this.activeSelectedCatId = item.id;
    this.selectedItem = item
  }

  selectCat(){
    this.addEditAssetForm.category = this.activeSelectedCatId
    this.categoryId = this.activeSelectedCatId
    this.CategoryName = this.selectedItem.categoryName
    this.catshow = false
  }
  gotoCat(item){
    this.getAllCatWise(item.id)
    this.breadCrumb.push(item.categoryName)
    this.breadCrumb.join('/')
    if(item){
    this.track = item
    }
  }

  backCat(){
    this.getAllCatWise(this.oldParentCatId);
    this.breadCrumb = []
    this.breadCrumb = this.track.categoryName

  }
  clear(){
    this.search = ''
    this.assetCat = this.demoassetCat
  }

  closeModal(){
    this.assetCat = this.demoassetCat
    this.catshow = false
    this.previewShow = false
  }


  getContent() {
    return new Promise(resolve => {
      this.http1
        .get('../../../../../../assets/help-content/help_content.json')
        .subscribe(
          data => {
            this.helpContent = data;
            console.log('Help Array', this.helpContent);
          },
          err => {
            resolve('err');
          }
        );
    });
  }
  getMimeType(url) {
    const mimevalue = mime.getType(url);
    if (mimevalue) {
        const documenttype = mimevalue.split("/")[0];
        const result = {
            mimetype: mimevalue,
            documenttype: documenttype
        };
        return result;
    } else {
        return mimevalue;
    }
}
  copytext(data){
    console.log("Assets url:>",data);
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = data;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.toastr.success('Link Copied', 'Success');
  }
  // Help Code Ends Here //

  ngOnDestroy() {
      if (this.pdfserverlink != '') {
        this.addassetservice.deletePdfLink(this.pdfserverlink).then((result: any) => {
          console.log('Link ===>', result);
        })
        .catch(result => {
          console.log('RESULT===>',result);
        });
      }
  }

  check(event){
    console.log(event.target.checked)
    if(event.target.checked === true){
    this.isShowApprover = true
    this.isApprover = 1
    }
    else{
      this.isShowApprover = false
      this.isApprover = 0
    }
  }
}
