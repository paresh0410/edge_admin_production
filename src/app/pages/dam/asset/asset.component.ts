import { Component, OnInit, ChangeDetectorRef, OnDestroy,
  ViewChild, ElementRef, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { AssetService } from './asset.service';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, ActivatedRoute } from '@angular/router';
import { AddAssetService } from './add-asset/add-asset.service';
import { ApproveAssetService } from './../approve-asset/approve-asset.service'
//import { MatTreeFlatDataSource } from '@angular/material';
import { ShareAssetService } from './../share-asset/share-asset.service';
import { AssetReviewPolicyService } from './../asset-review-policy/asset-review-policy.service'
import { AssetVesrionService } from '../asset-version/asset-version.service';
import { isString } from 'util';
import { NbMenuService, NbSidebarService } from '@nebular/theme';
import { LayoutService } from '../../../@core/data/layout.service';
//import { AddAssetService } from './add-asset/add-asset.service'
import { HostListener } from '@angular/core';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { webApi } from '../../../service/webApi';
import { Card } from '../../../models/card.model';
import { SuubHeader } from '../../components/models/subheader.model';
import { MatTabChangeEvent } from '@angular/material';
import { noData } from '../../../models/no-data.model';
import { Filter } from '../../../models/filter.modal';
//import { InfiniteScrollDirective } from 'ngx-infinite-scroll';
//import { InfiniteScroll } from 'ngx-infinite-scroll';
//const nisPackage = require('../../../../../package.json');
@Component({
  selector: 'ngx-asset',
  templateUrl: './asset.component.html',
  styleUrls: ['./asset.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AssetComponent implements OnInit, OnDestroy {
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  @Output() changeheader = new EventEmitter<any>();

  title = 'demo-app';
  noDataVal:noData;
  isworking = false;
  Status = [];
  Languages = [];
  Formats = [];
  Channels = [];
  Categories = [];
  Tags = [];
  header: SuubHeader;
  filter: boolean = false;
  filtercon: Filter = {
    ascending: false,
    descending: false,
    showDropdown: false,
    dropdownList: [
      { drpName: 'Enrol Date', val: 1 },
      { drpName: 'Created Date', val: 2 },
    ]
  };
  count:number=8
  filters: any = [
    // {
    //   count: "0",
    //   value: "",
    //   tagname: "Business",
    //   isChecked: true,
    // },
    // {
    //   count: "",
    //   value: "2",
    //   tagname: "Course level",
    //   isChecked: false,
    // },
    // {
    //   count: "",
    //   value: "3",
    //   tagname: "Categories",
    //   isChecked: false,
    // },
    // {
    //   count: "",
    //   value: "4",
    //   tagname: "Employees",
    //   isChecked: false,
    // },
    // {
    //   count: "",
    //   value: "",
    //   tagname: "Business",
    //   isChecked: false,
    // },

  ];

  filtersInner: any = [
    { tagname: "Employees", isSelect: true},
    { tagname: "Categories", isSelect: true},
    { tagname: "Course level", isSelect: false},
    { tagname: "Buusiness", isSelect: false},
    { tagname: "Employees", isSelect: false},
    { tagname: "Categories", isSelect: false},
    { tagname: "Course level", isSelect: false},
    { tagname: "Buusiness", isSelect: false},
    { tagname: "Employees", isSelect: false},
    { tagname: "Categories", isSelect: false},
    { tagname: "Course level", isSelect: false},
    { tagname: "Buusiness", isSelect: false},
    { tagname: "Employees", isSelect: false},
    { tagname: "Categories", isSelect: false},
    { tagname: "Course level", isSelect: false},
    { tagname: "Buusiness", isSelect: false},
    { tagname: "Employees", isSelect: false},
    { tagname: "Categories", isSelect: false},
    { tagname: "Course level", isSelect: false},
    { tagname: "Buusiness", isSelect: false},
    { tagname: "Employees", isSelect: false},
    { tagname: "Categories", isSelect: false},
    { tagname: "Course level", isSelect: false},
    { tagname: "Buusiness", isSelect: false},
    { tagname: "Employees", isSelect: false},
    { tagname: "Categories", isSelect: false},
    { tagname: "Course level", isSelect: false},
    { tagname: "Buusiness", isSelect: false},
    { tagname: "Employees", isSelect: false},
    { tagname: "Categories", isSelect: false},
    { tagname: "Course level", isSelect: false},
    { tagname: "Buusiness", isSelect: false},
    { tagname: "Employees", isSelect: false},
    { tagname: "Categories", isSelect: false},
    { tagname: "Course level", isSelect: false},
    { tagname: "Buusiness", isSelect: false},
    { tagname: "Employees", isSelect: false},
    { tagname: "Categories", isSelect: false},
    { tagname: "Course level", isSelect: false},
    { tagname: "Buusiness", isSelect: false},
    { tagname: "Employees", isSelect: false},
    { tagname: "Categories", isSelect: false},
    { tagname: "Course level", isSelect: false},
    { tagname: "Buusiness", isSelect: false},
    { tagname: "Employees", isSelect: false},
    { tagname: "Categories", isSelect: false},
    { tagname: "Course level", isSelect: false},
    { tagname: "Buusiness", isSelect: false},
    { tagname: "Employees", isSelect: false},
    { tagname: "Categories", isSelect: false},
    { tagname: "Course level", isSelect: false},
    { tagname: "Buusiness", isSelect: false},
    { tagname: "Employees", isSelect: false},
    { tagname: "Categories", isSelect: false},
    { tagname: "Course level", isSelect: false},
    { tagname: "Buusiness", isSelect: false},
    { tagname: "Employees", isSelect: false},
    { tagname: "Categories", isSelect: false},
    { tagname: "Course level", isSelect: false},
    { tagname: "Buusiness", isSelect: false},
    { tagname: "Employees", isSelect: false},
    { tagname: "Categories", isSelect: false},
    { tagname: "Course level", isSelect: false},
    { tagname: "Buusiness", isSelect: false},
    { tagname: "Employees", isSelect: false},
    { tagname: "Categories", isSelect: false},
    { tagname: "Course level", isSelect: false},
    { tagname: "Buusiness", isSelect: false},
    { tagname: "Employees", isSelect: false},
    { tagname: "Categories", isSelect: false},
    { tagname: "Course level", isSelect: false},
    { tagname: "Buusiness", isSelect: false},
    { tagname: "Employees", isSelect: false},
    { tagname: "Categories", isSelect: false},
    { tagname: "Course level", isSelect: false},
    { tagname: "Buusiness", isSelect: false},
    { tagname: "Employees", isSelect: false},
    { tagname: "Categories", isSelect: false},
    { tagname: "Course level", isSelect: false},
    { tagname: "Buusiness", isSelect: false},
    { tagname: "Employees", isSelect: false},
    { tagname: "Categories", isSelect: false},
    { tagname: "Course level", isSelect: false},
    { tagname: "Buusiness", isSelect: false},
    { tagname: "Employees", isSelect: false},
    { tagname: "Categories", isSelect: false},
    { tagname: "Course level", isSelect: false},
    { tagname: "Buusiness", isSelect: false},
    { tagname: "Employees", isSelect: false},
    { tagname: "Categories", isSelect: false},
    { tagname: "Course level", isSelect: false},
    { tagname: "Buusiness", isSelect: false},
    { tagname: "Employees", isSelect: false},
    { tagname: "Categories", isSelect: false},
    { tagname: "Course level", isSelect: false},
    { tagname: "Buusiness", isSelect: false},
    { tagname: "Employees", isSelect: false},
    { tagname: "Categories", isSelect: false},
    { tagname: "Course level", isSelect: false},
  ];

  cardModify: Card = {
    flag: 'assets',
    titleProp: 'assetName',
    discrption: 'description',
    contentType: 'formatId',
    showImage: true,
    hoverlable: true,
    option: true,
    eyeIcon: true,
    copyIcon: false,
    bottomDiv: true,
    bottomTitle: true,
    bottomDiscription: true,
    customCard: '',
    btnLabel: 'Edit',
  };

  cardModifyPending: Card = {
    flag: 'assets',
    titleProp: 'assetName',
    discrption: 'description',
    contentType: 'formatId',
    showImage: true,
    hoverlable: true,
    bottomDiv: true,
    bottomTitle: true,
    bottomDiscription: true,
    customCard: '',
    btnLabel: 'Edit',
  }
  cardModifyApproved: Card = {
    flag: 'assets',
    titleProp: 'assetName',
    discrption: 'description',
    contentType: 'formatId',
    showImage: true,
    option: true,
    shareIcon: true,
    // retweet: true,
    retweet: false,
    bottomDiv: true,
    bottomTitle: true,
    bottomDiscription: true,
    customCard: '',
  }
  storedVisible: any;
  categoryId: any;
  tenantId: any;
  getPendingAssets: boolean = false;
  getApprovedAssets: boolean = false;
  search: any;
  searchTags: any;
  skeleton = false
  dataFromAssetCategory: boolean = false;
  pagename: any;
  status: any = [];
  categories: any = [];
  formats: any = [];
  channels: any = [];
  languages: any = [];
  noCategory: boolean = false;
  enableDisableCategoryModal: boolean = false;
  assetsdata: any;
  changetitle: any;
  avisible: any;
  msg: any;
  userData: any;
  Tabs:any =[{'tabId':1,'tabName':'Approved Assets' },
  {'tabId':3,'tabName':'To Be Approved' },
  {'tabId':1,'tabName':'Rejected Assets' }
]
  damTabs: any;
  roleId: any;
  tabId: any;
  id: any  = 1;
  selectedIndex: any = 0;
  dummyasset: any = [];
  demoAsset: any;
  defaultValJson: any;
  defaultValFlag: any;
  isAprrove: any;
  isEdit: any;
  dummyDisplay: any;
  constructor(private assetservice: AssetService,
    // private toasterService: ToasterService,
    private spinner: NgxSpinnerService,
    public router: Router, public routes: ActivatedRoute, private addAssetService: AddAssetService,
    private approveAssetService: ApproveAssetService, private shareAssetService: ShareAssetService,
    private assetReviewPolicyService: AssetReviewPolicyService, private assetVesrionService: AssetVesrionService,
    private sidebarService: NbSidebarService, private layoutService: LayoutService, private cdf: ChangeDetectorRef,
    private toastr: ToastrService, private commonFunctionService: CommonFunctionsService) {

    this.toggleSidebar('close');
    if (localStorage.getItem('LoginResData')) {
      this.userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', this.userData.data);
      //  this.userId = this.userData.data.data.id;
      this.tenantId = this.userData.data.data.tenantId;
    }
    if(localStorage.getItem('ApprovedamTabs')){
      this.damTabs = JSON.parse(localStorage.getItem('ApprovedamTabs'));
    }
    if (localStorage.getItem('defaultValFlag')) {
      this.defaultValJson = JSON.parse(localStorage.getItem('defaultValFlag'));
      this.defaultValFlag=this.defaultValJson.defaultValueReq;
      this.isAprrove=this.defaultValJson.isApprove;
      this.isEdit=this.defaultValJson.fileEditFlag;
      console.log(this.defaultValJson,"defaultValFlag")

    }
    console.log(this.damTabs,"this.damTabs")
    // if(this.damTabs.length == 1){
      this.roleId = this.damTabs[0].roleId
      this.tabId = this.damTabs[0].tabId
      if(this.approveAssetService.assetStatus){
        this.tabId = this.approveAssetService.assetStatus
      }
      for(let index in this.damTabs){
        if(this.tabId == this.damTabs[index].tabId)
        this.selectedIndex = index
      }
    // }

    //this.tenantId = this.assetservice.tenantId;

    // if(this.assetservice.dataFromAssetCategory){
    //     this.dataFromAssetCategory = true;
    //     this.getPendingAssets=false;
    //     this.getApprovedAssets=false;
    //     this.addAssetService.dataFromAssetCategory = true;
    // }else{
    //     this.dataFromAssetCategory = false;
    //     this.addAssetService.dataFromAssetCategory = false;
    // }

    if (this.assetservice.getPendingAssets) {
      this.getPendingAssets = true;
      this.pagename = "Approve File";
      this.header = {
        title: 'Approve File',
        btnsSearch: true,
        searchBar: true,
        dropdownlabel: ' ',
        drplabelshow: false,
        drpName1: '',
        drpName2: ' ',
        drpName3: '',
        drpName1show: false,
        placeHolder:'Search by keywords',
        drpName2show: false,
        drpName3show: false,
        btnName1: '',
        btnName2: '',
        btnName3: '',
        btnAdd: '',
        btnName1show: false,
        btnName2show: false,
        btnName3show: false,
        btnBackshow: true,
        btnAddshow: false,
        filter: true,
        showBreadcrumb: true,
        breadCrumbList: [
          {
            'name': 'DAM',
            'navigationPath': '/pages/dam',
          },
          {
             'name': 'Folder',
            'navigationPath': 'pages/dam/folders',
          }
        ]
      };
      this.dataFromAssetCategory = false;
      this.addAssetService.dataFromAssetCategory = false;
      this.getPendingAsset();

    } else if (this.assetservice.getApprovedAssets) {
      this.getApprovedAssets = true;
      this.pagename = "Share Asset";
      this.addAssetService.dataFromAssetCategory = false;
      this.getApprovedAsset();
      this.header = {
        title: 'Share Asset',
        btnsSearch: true,
        searchBar: false,
        dropdownlabel: ' ',
        drplabelshow: false,
        drpName1: '',
        drpName2: ' ',
        drpName3: '',
        drpName1show: false,
        drpName2show: false,
        drpName3show: false,
        btnName1: '',
        btnName2: '',
        btnName3: '',
        btnAdd: '',
        btnName1show: false,
        btnName2show: false,
        btnName3show: false,
        btnBackshow: true,
        btnAddshow: false,
        filter: false,
        showBreadcrumb: true,
        breadCrumbList: [
          {
            'name': 'DAM',
            'navigationPath': '/pages/dam',
          },]
      };
    } else {
      this.getPendingAssets = false;
      this.dataFromAssetCategory = false;
      this.dataFromAssetCategory = true;
      this.addAssetService.dataFromAssetCategory = true;
      this.categoryId = this.assetservice.categoryId;
      this.addAssetService.categoryId = this.categoryId;
      console.log('this.categoryId', this.categoryId);
      this.header = {
        title: 'Category Asset',
        btnsSearch: true,
        searchBar: false,
        dropdownlabel: ' ',
        drplabelshow: false,
        drpName1: '',
        drpName2: ' ',
        drpName3: '',
        drpName1show: false,
        drpName2show: false,
        drpName3show: false,
        btnName1: '',
        btnName2: '',
        btnName3: '',
        btnAdd: 'Add Asset',
        btnName1show: false,
        btnName2show: false,
        btnName3show: false,
        btnBackshow: true,
        btnAddshow: true,
        filter: false,
        showBreadcrumb: true,
        breadCrumbList: [
          {
            'name': 'DAM',
            'navigationPath': '/pages/dam',
          },
          {
            'name': 'Asset Category',
            'navigationPath': '/pages/dam/asset-category',
          },]
      };
      this.displayArray = this.asset;
      this.getAllAsset();
      //   location.onPopState(() => {
      //     this.toggleSidebar();
      //     // console.log('pressed back!');

      // });
    }


    this.search = {
      // assetName:''
    };

    this.searchTags = {
      // assetName:''
    };

    this.getAllTags();
    this.getAllAssetDropdown();

  }

  // toggleSidebar(): boolean {
  //   // this.sidebarService.toggle(true, 'menu-sidebar');
  //   this.layoutService.changeLayoutSize();
  //   return false;
  // }
  toggleSidebar(val) {
    // this.sidebarService.toggle(true, 'menu-sidebar');
    // this.layoutService.changeLayoutSize();
    // return false;
    if(val === 'open'){
      if(!document.getElementsByTagName('nb-sidebar')[0].classList.contains('expanded')){
        document.getElementsByTagName('nb-sidebar')[0].classList.add('expanded')
      }
      if(document.getElementsByTagName('nb-sidebar')[0].classList.contains('compacted')){
        document.getElementsByTagName('nb-sidebar')[0].classList.remove('compacted')
      }

    }else {
      if(!document.getElementsByTagName('nb-sidebar')[0].classList.contains('compacted')){
        document.getElementsByTagName('nb-sidebar')[0].classList.add('compacted')
      }
      if(document.getElementsByTagName('nb-sidebar')[0].classList.contains('expanded')){
        document.getElementsByTagName('nb-sidebar')[0].classList.remove('expanded')
      }
    }
    this.layoutService.changeLayoutSize();
  }
  onSequenceChangeEvent(event:MatTabChangeEvent)
  {
    this.skeleton = false
   this.tabId = this.damTabs[event.index].tabId
   this.roleId = this.damTabs[event.index].roleId
   this.selectedIndex = event.index
    this.getPendingAsset()
  }


  back() {

    console.log('this.dataFromAssetCategory', this.dataFromAssetCategory);
    if (this.dataFromAssetCategory) {
      console.log('this.dataFromAssetCategory1', this.dataFromAssetCategory);
      this.router.navigate(['../../asset-category'], { relativeTo: this.routes });
    } else {
      console.log('this.dataFromAssetCategory2', this.dataFromAssetCategory);
      if(this.isAprrove==1){
      this.router.navigate(['../../dam'], { relativeTo: this.routes });
      }else{
      this.router.navigate(['../../dam/folders'], { relativeTo: this.routes });
        
      }
      // window.history.back();
    }


  }

  clear() {
    this.noAsset =false
    this.search = {};
    this.header.searchtext = ''
    this.searchTags = {};
    this.getPendingAsset()
  };

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }

  // noTags:boolean=false;
  // getAllTags() {
  //    this.spinner.show();
  //   // const param = {
  //   //   'aId': 25,
  //   //   'tId': this.tenantId,
  //   //   'cId': 0,
  //   // }
  //   this.assetservice.getAllTags(param)
  //     .then(rescompData => {
  //        this.spinner.hide();
  //       var result = rescompData;
  //       console.log('getTagsResponse:', rescompData);
  //       if (result['type'] == true) {
  //         if (result['data'][0].length == 0) {
  //           this.noDataFound = true;
  //           this.skeleton = true
  //         } else {
  //           const tagArr = result['data'][0];
  //           for (let i = 0; i < tagArr.length; i++) {
  //             tagArr[i].status = false;
  //           }
  //           this.Tags = tagArr;
  //           console.log('this.Tags', this.Tags);
  //         }
  //       } else {
  //         this.spinner.hide();
  //         this.skeleton = true
  //         this.noDataFound = true;
  //         // var toast: Toast = {
  //         //   type: 'error',
  //         //   //title: "Server Error!",
  //         //   body: 'Something went wrong.please try again later.',
  //         //   showCloseButton: true,
  //         //   timeout: 2000
  //         // };
  //         // this.toasterService.pop(toast);
  //         this.presentToast('error', '');
  //       }
  //     }, error => {
  //       // this.spinner.hide();
  //       this.skeleton = true
  //       this.noDataFound = true;
  //       // var toast: Toast = {
  //       //   type: 'error',
  //       //   //title: "Server Error!",
  //       //   body: 'Something went wrong.please try again later.',
  //       //   showCloseButton: true,
  //       //   timeout: 2000
  //       // };
  //       // this.toasterService.pop(toast);
  //       this.presentToast('error', '');
  //     });
  // }
  getAllTags() {
    this.spinner.show();
    // const param = {
    //   'aId': 25,
    //   'tId': this.tenantId,
    //   'cId': 0,
    // }
    let param = {
      tenantId: this.tenantId
    };
    this.assetservice.getAllTags(param)
      .then(rescompData => {
        this.spinner.hide();
        var temp: any = rescompData;
        //  this.tagList = temp.data;
        console.log('getTagsResponse:', rescompData);

        const tagArr = temp.data;
        for (let i = 0; i < tagArr.length; i++) {
          tagArr[i].status = false;
        }
        this.Tags = tagArr;
        this.bindfilter(this.Tags);
        console.log('this.Tags', this.Tags);

      }, error => {
        // this.spinner.hide();
        this.skeleton = true
        this.noDataFound = true;
        // var toast: Toast = {
        //   type: 'error',
        //   //title: "Server Error!",
        //   body: 'Something went wrong.please try again later.',
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
  }

  noAsset: boolean = false;
  // asset: any;
  noDataFound: boolean = false;
  getAllAsset() {
    // this.spinner.show();
    let param = {
      'tId': this.tenantId,
      'catId': this.categoryId
    }

    this.assetservice.getAllAsset(param)
      .then(rescompData => {
        // this.spinner.hide();
        var result = rescompData;
        console.log('getAssetResponse:', rescompData);
        if (result['type'] == true) {
          if (result['data'][0].length == 0) {
            this.noAsset = true;
            this.skeleton = true
          } else {
            // this.noAsset = false;
            // this.skeleton = true;
            // this.noDataFound = false;
            this.asset = result['data'][0];
            for (let i = 0; i < this.asset.length; i++) {
              if (this.asset[i].tags) {
                this.asset[i].tags = this.asset[i].tags.split(',');
              }
            }
            // this.displayArray = this.asset;
            this.displayArray = []
            this.addItems(0, this.sum, 'push', this.asset);
            this.noFilterAsset = false;
            // for(let i = 0; i < this.displayArray.length; i++ ) {
            //   this.displayArray[i].disable = this.displayArray[i].visible;
            //   this.displayArray[i].discrption = this.displayArray[i].description;
            //   this.displayArray[i].title = this.displayArray[i].assetName;
            //   if(this.displayArray[i].formatId == 1 || this.displayArray[i].formatId == 4 || this.displayArray[i].formatId == 6 || this.displayArray[i].formatId == 7 || this.displayArray[i].formatId == 8) {
            //     this.displayArray[i].image = 'assets/images/videoNew.png';
            //   } else if(this.displayArray[i].formatId == 2 ){
            //     this.displayArray[i].image = 'assets/images/audioNew.png';
            //   } else if(this.displayArray[i].formatId == 3 || this.displayArray[i].formatId == 5) {
            //     this.displayArray[i].image = 'assets/images/pdfNew.png';
            //   } else if(this.displayArray[i].formatId == 10) {
            //     this.displayArray[i].image = 'assets/images/ppt.jpg';
            //   } else if(this.displayArray[i].formatId == 11) {
            //     this.displayArray[i].image = 'assets/images/excel.jpg';
            //   } else if(this.displayArray[i].formatId == 12) {
            //     this.displayArray[i].image = 'assets/images/doc.jpg';
            //   }
            // }

            this.noDataFound = false;
            this.skeleton = true;
            this.noAsset = false;
            console.log('this.asset', this.asset);
            console.log('this.displayArray', this.displayArray);
            this.cdf.detectChanges();
          }
        } else {
          this.spinner.hide();
          this.skeleton = true
          this.noDataFound = true;
          // var toast: Toast = {
          //   type: 'error',
          //   //title: "Server Error!",
          //   body: 'Something went wrong.please try again later.',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }
        //this.skeleton = true

      }, error => {
        // this.spinner.hide();
        this.skeleton = true
        this.noDataFound = true;
        // var toast: Toast = {
        //   type: 'error',
        //   //title: "Server Error!",
        //   body: 'Something went wrong.please try again later.',
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
  }

  assetForFilter = [];
  getPendingAsset() {
    this.skeleton = false
    // this.spinner.show();
    let param = {
      'tId': this.tenantId,
      'assetStatus': this.tabId,
       roleId:this.roleId
    }

    this.assetservice.getPendingAsset(param)
      .then(rescompData => {
        var result = rescompData;
        console.log('getPendingAssetResponse:', rescompData);
        if (result['type'] == true) {
          if (result['data'][0].length == 0) {
            this.noDataVal={
              margin:'mt-3',
              imageSrc: '../../../../../assets/images/no-data-bg.svg',
              title:"No file available for approval at this time.",
              desc:"Asset will appear for approval after a new asset is added by the contributor. Asset approval is a 4 eye process to avoid irregularities in digital asset creation and management",
              titleShow:true,
              btnShow:true,
              descShow:true,
              btnText:'Learn More',
              btnLink:'https://faq.edgelearning.co.in/kb/dam-how-to-approve-an-asset-in-dam',
            }
            this.noAsset = true;
            this.skeleton = true;
            this.noFilterAsset = true;
            this.spinner.hide()
            // this.noDataFound = true;
            // this.presentToast('error', '');
          } else {
            this.asset = result['data'][0];
            for (let i = 0; i < this.asset.length; i++) {
              if (this.asset[i].tags) {
                this.asset[i].tags = this.asset[i].tags.split(',');
              }
            }
            this.dummyasset = this.asset
            this.demoAsset = this.asset
            this.displayArray = []
            this.addItems(0, this.sum, 'push', this.asset);
            this.dummyDisplay = this.displayArray
            this.assetForFilter = this.asset;
            this.noDataFound = false;
            this.skeleton = true;
            this.noFilterAsset = false;
            this.noAsset = false;
            console.log('this.assetPending:', this.asset);
            this.cdf.detectChanges();
            this.spinner.hide();
          }
          //this.skeleton = true;
        } else {
          // this.spinner.hide();
          this.skeleton = true
          this.noDataFound = true;
          this.noFilterAsset = false;
          this.spinner.hide()
          // var toast: Toast = {
          //   type: 'error',
          //   //title: "Server Error!",
          //   body: 'Something went wrong.please try again later.',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
        }


      }, error => {
        // this.spinner.hide();
        this.skeleton = true
        this.noDataFound = true;
        this.spinner.hide()
        // var toast: Toast = {
        //   type: 'error',
        //   //title: "Server Error!",
        //   body: 'Something went wrong.please try again later.',
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
  }
  conentHeight = '0px';
  offset: number = 100;
  ngOnInit() {
    this.conentHeight = '0px';
    const e = document.getElementsByTagName('nb-layout-column');
    console.log(e[0].clientHeight);
    // this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
    if (e[0].clientHeight > 1300) {
      this.conentHeight = '0px';
      this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
    } else {
      this.conentHeight = String(e[0].clientHeight) + 'px';
    }
  }

  sum = 40;
  addItems(startIndex, endIndex, _method, asset) {
    for (let i = startIndex; i < endIndex; ++i) {
      if (asset[i]) {
        this.displayArray[_method](asset[i]);
      } else {
        break;
      }

      // console.log("NIKHIL");
    }
  }
  // sum = 10;
  onScroll(event) {

    let element = this.myScrollContainer.nativeElement;
    // element.style.height = '500px';
    // element.style.height = '500px';
    // Math.ceil(element.scrollHeight - element.scrollTop) === element.clientHeight

    // if (element.scrollHeight - element.scrollTop - element.clientHeight < 5) {
    if ((Math.ceil(element.scrollHeight - element.scrollTop) - element.clientHeight) < 30) {
      if (this.newArray.length > 0) {
        if (this.newArray.length >= this.sum) {
          const start = this.sum;
          this.sum += 100;
          this.addItems(start, this.sum, 'push', this.newArray);
        } else if ((this.newArray.length - this.sum) > 0) {
          const start = this.sum;
          this.sum += this.newArray.length - this.sum;
          this.addItems(start, this.sum, 'push', this.newArray);
        }
      } else {
        if (this.asset.length >= this.sum) {
          const start = this.sum;
          this.sum += 100;
          this.addItems(start, this.sum, 'push', this.asset);
        } else if ((this.asset.length - this.sum) > 0) {
          const start = this.sum;
          this.sum += this.asset.length - this.sum;
          this.addItems(start, this.sum, 'push', this.asset);
        }
      }


      console.log('Reached End');
    }
  }

  getApprovedAsset() {
    // this.spinner.show();
    let param = {
      'tId': this.tenantId,
      'assetStatus': 1
    }

    this.assetservice.getApprovedAsset(param)
      .then(rescompData => {
        // this.spinner.hide();
        var result = rescompData;
        console.log('getApprovedAssetResponse:', rescompData);
        if (result['type'] == true) {
          if (result['data'][0].length == 0) {
            this.noAsset = true;
            this.noDataVal={
              margin:'mt-3',
              imageSrc: '../../../../../assets/images/no-data-bg.svg',
              title:"No file available for approval at this time.",
              desc:"Asset will appear for approval after a new asset is added by the contributor. Asset approval is a 4 eye process to avoid irregularities in digital asset creation and management",
              titleShow:true,
              btnShow:true,
              descShow:true,
              btnText:'Learn More',
              btnLink:'https://faq.edgelearning.co.in/kb/dam-how-to-approve-an-asset-in-dam',
            }
            this.skeleton = true;
          } else {
            // this.skeleton = true;
            // this.noAsset = false;
            // this.noDataFound = false;
            this.asset = result['data'][0];
            for (let i = 0; i < this.asset.length; i++) {
              if (this.asset[i].tags) {
                this.asset[i].tags = this.asset[i].tags.split(',');
              }
            }
            this.displayArray = []
            this.addItems(0, this.sum, 'push', this.asset);
            // this.displayArray = this.asset;

            this.noDataFound = false;
            this.skeleton = true;
            this.noAsset = false;
            this.noFilterAsset = false;
            console.log('this.assetApproved:', this.asset);
            this.cdf.detectChanges();
          }

        } else {
          this.spinner.hide();
          this.skeleton = true
          this.noDataFound = true;
          // var toast: Toast = {
          //   type: 'error',
          //   //title: "Server Error!",
          //   body: 'Something went wrong.please try again later.',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }


      }, error => {
        this.spinner.hide();
        this.skeleton = true
        this.noDataFound = true;
        // var toast: Toast = {
        //   type: 'error',
        //   //title: "Server Error!",
        //   body: 'Something went wrong.please try again later.',
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
  }
  gotoApproveAsset(assetData) {
    console.log('assetData', assetData);
    this.approveAssetService.title = assetData.assetName
    this.approveAssetService.getAssetDataForApproval = assetData;
    this.approveAssetService.tabId = this.tabId
    console.log()
    this.router.navigate(['approve-asset'], { relativeTo: this.routes });
  }

  gotocardAprrove(assetData) {
    console.log('assetData', assetData);
    this.approveAssetService.title = assetData.assetName
    this.approveAssetService.getAssetDataForApproval = assetData;
    this.approveAssetService.tabId = this.tabId
    this.router.navigate(['approve-asset'], { relativeTo: this.routes });
  }

  gotoShareAsset(assetData) {
    console.log('assetData', assetData);
    this.shareAssetService.getAssetDataForShare = assetData;
    this.router.navigate(['share-asset'], { relativeTo: this.routes });
  }

  gotoCardShareAssets(assetData) {
    console.log('assetData', assetData);
    this.shareAssetService.getAssetDataForShare = assetData;
    this.router.navigate(['share-asset'], { relativeTo: this.routes });
  }

  gotoReviewAsset(assetData) {
    console.log('assetData', assetData);
    this.assetReviewPolicyService.getAssetDataForReview = assetData;
    this.router.navigate(['asset-review-policy'], { relativeTo: this.routes });
  }

  gotoCardReviewAsset(assetData) {
    console.log('assetData', assetData);
    this.assetReviewPolicyService.getAssetDataForReview = assetData;
    this.router.navigate(['asset-review-policy'], { relativeTo: this.routes });
  }


  disableAsset(data, status) {

    this.enableDisableCategoryModal = true;
    this.assetsdata = data;
    if (data.visible == 0) {
      this.avisible = 1;
      this.changetitle = 'Enable';
      this.msg = "Asset Enable Successfully"
    } else {
      this.avisible = 0;
      this.changetitle = 'Disable';
      this.msg = "Asset Disable Successfully"
    }
  }

  clickTodisable(data) {
    this.enableDisableCategoryModal = true;
    this.assetsdata = data;
    this.storedVisible = data;
  }
  enableDisableAction(status) {
    if (status == true) {
      // kv
      if (this.storedVisible.visible == 0) {
        this.avisible = 1;
        this.storedVisible.disable = 1;
        this.changetitle = 'Enable';
        this.msg = "Asset Enable Successfully"
      } else {
        this.avisible = 0;
        this.storedVisible.disable = 0;
        this.changetitle = 'Disable';
        this.msg = "Asset Disable Successfully"
      }
      // kv
      this.changevisiblestatus();
    } else {
      this.enableDisableCategoryModal = false;
    }
  }
  changevisiblestatus() {
    this.spinner.show();
    let param = {
      'assetsId': this.assetsdata.assetId,
      'assetsVisible': this.avisible,
      'tId': this.tenantId

    }

    this.assetservice.changeAssetStatus(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('UpdateAssetResponse:', rescompData);
        if (result['type'] == true) {
          this.presentToast('success', this.msg);
          this.enableDisableCategoryModal = false;
          this.getAllAsset();
        } else {
          this.presentToast('error', '');
        }
      }, error => {
        this.spinner.hide();
        this.presentToast('error', '');
      });
  }
  closeEnableDisableCategoryModal() {
    this.enableDisableCategoryModal = false;
  }

  getAllAssetDropdown() {
    let param = {
      "tId": this.tenantId
    }
    const _urlGetAllDropdown: string = webApi.domain + webApi.url.getAllAssetDropDown;
    this.commonFunctionService.httpPostRequest(_urlGetAllDropdown, param)
      // this.addAssetService.getAllAssetDropdown(param)
      .then(rescompData => {
        var result = rescompData;
        console.log('All Asset Dropdown1', result);
        if (result['type'] == true) {
          console.log('All Asset Dropdown2', result);
          this.status = result['data'][0];
          this.languages = result['data'][1];
          this.formats = result['data'][2];
          this.channels = result['data'][3];
          this.categories = result['data'][4];
          // if (result['data'] && result['data'].length > 0) {
          //   result['data'].forEach( (value, index) => {
          //     const filtername = value.length > 0 ? value[0]['filterId'] : '';
          //     const item = {
          //       count: "",
          //       value: "",
          //       tagname: value.length > 0 ? value[0]['filterId'] : '',
          //       isChecked: false,
          //       list: value,
          //     }
          //     if (filtername) {
          //       this.filters.push(item);
          //     }
          //   })
          // }
          this.bindfilter(this.languages);
          this.bindfilter(this.channels);
          // if(!this.getApprovedAssets && !this.getPendingAssets) {
          //   this.bindfilter(this.status);
          // }
          this.bindfilter(this.formats);
          if (this.categories.length == 0) {
            this.noCategory = true;
          }
          console.log(this.status, this.languages, this.formats, this.channels, this.categories);
        } else {
          // var toast: Toast = {
          //   type: 'error',
          //   //title: "Server Error!",
          //   body: "Something went wrong.please try again later.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }

      },
        resUserError => {
          // this.loader =false;
          // var toast: Toast = {
          //   type: 'error',
          //   //title: "Server Error!",
          //   body: "Something went wrong.please try again later.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        });
  }
  filteredData = []

  filteredChanged1(event) {
    console.log('Filtered Event - ', event);
    const obj = event;
    this.filterData = {};
    this.SelectedCategoryLength = {};
    this.objectSetter = {};
    let allempty = true;
    if (obj) {
      for(const key in obj){
        const list = obj[key];
        if(list && list.length && list.length>0){
        list.forEach( (value, index) => {
          allempty = false;
          console.log(key, ' - ', value);
          this.SubjectName_CheckBoxedStatus = true;
          this.subjectName(value, key);
        });
      }
    }
      if (allempty) {
        // this.displayArray = this.asset;
        this.SubjectName_CheckBoxedStatus = false
        this.displayArray = this.dummyDisplay;
      }
    } else {
      this.displayArray = this.asset;
    }
  }

  SarchFilter(event) {
    var searchtext
    this.header.searchtext = event.target.value
    console.log(searchtext);
    searchtext= event.target.value;
    const val = searchtext.toLowerCase();
    if(val.length>=3 || val.length == 0){
    this.displayArray=[];
    const tempcc = this.demoAsset.filter(function (d) {
      return String(d.code).toLowerCase().indexOf(val) !== -1 ||
        d.assetName.toLowerCase().indexOf(val) !== -1 ||
        // d.description.toLowerCase().indexOf(val) !== -1 ||
        // d.department.toLowerCase().indexOf(val) !== -1 ||
        // d.manager.toLowerCase().indexOf(val) !== -1 ||
        !val
    });
    console.log(tempcc);
    this.newArray = tempcc;
    this.sum=50;
    this.addItems(0, this.sum, 'push', this.newArray);

    if (!this.newArray.length) {
      // this.noCategory = true;
      this.noAsset = true
    } else {
      // this.noCategory = false;
      this.noAsset = false
    }
  }
  }

  bindfilter(obj) {
    const filtername = obj.length > 0 ? obj[0]['filterId'] : '';
        const item = {
          count: "",
          value: "",
          tagname: obj.length > 0 ? obj[0]['filterId'] : '',
          isChecked: false,
          list: obj,
        }
        if (filtername) {
          this.filters.push(item);
        }
    // if (result['data'] && result['data'].length > 0) {
    //   result['data'].forEach( (value, index) => {

    //   })
    // }
  }
// }



  addeditAsset(assetData) {
    this.changeheader.emit();
    console.log('assetData:', assetData);
    console.log(this.displayArray, "this.displayArray")
    // if (value == 0) {
    var action = 'ADD';
    //this.addAssetService.pollId = 0;
    this.addAssetService.addEditAssetData[0] = action;
    this.addAssetService.addEditAssetData[1] = null;
    this.router.navigate(['add-edit-asset'], { relativeTo: this.routes });

    // } else {
    //   var action = 'EDIT';
    //   //this.addPollService.pollId = data.id;
    //   this.addAssetService.addEditAssetData[0] = action;
    //   this.addAssetService.addEditAssetData[1] = assetData;

    //   this.router.navigate(['add-edit-asset'], { relativeTo: this.routes });

    // }
  }

  EditSection(assetData) {
this.changeheader.emit();
    var action = 'EDIT';
    this.addAssetService.addEditAssetData[0] = action;
    this.addAssetService.addEditAssetData[1] = assetData;

    this.router.navigate(['add-edit-asset'], { relativeTo: this.routes });
  }

  upgradeVersion(assetData) {

    console.log('assetDataForVersion:', assetData);
    this.assetVesrionService.getAssetDataForVesrsioning = assetData;
    this.router.navigate(['asset-version'], { relativeTo: this.routes });
  }

  copyCard(assetData) {
    console.log('assetDataForVersion:', assetData);
    this.assetVesrionService.getAssetDataForVesrsioning = assetData;
    this.router.navigate(['asset-version'], { relativeTo: this.routes });
  }

  // *********************************************Vikas*********************************************************

  asset: any = [];

  fieldValue = [];
  ObjectSetterValueArray = [];

  filterData = {};

  // searchTags:any;



  // subject filteration starts here
  // main array
  newArray = [];

  // array for display on html page
  // Initially without filter it will hold data of asset
  displayArray = this.asset;


  // nofilterAsset variable for validation
  noFilterAsset: boolean = false;


  // new array for tags
  tagArray = [];
  //
  filterTag: any = [];
  // number display to n
  SelectedCategoryLength: any = {};

  SubjectFilterArray = [];
  SubjectName_CheckBoxedStatus: boolean;
  // subj: any;



  /*

  After loading the screen
  ***********************************
  when we select the checkbox SubjectName function will be called.

  After execution of statement takes places

  */

  // tslint:disable-next-line: max-line-length
  filterDataObjectArrayCreation(filterData_CategoryNameForKey: any, FiltersubjectNameForArray: any, checkboxStatus: boolean) {


    let dataValue = null;
    let flag = false;
    for (const key in this.filterData) {
      if (filterData_CategoryNameForKey === key) {
        flag = true;
        dataValue = key;
        break;
      } else {
        flag = false;
      }
    }
    if (checkboxStatus) {
      if (!flag) {
        Object.assign(this.filterData, { [filterData_CategoryNameForKey]: [] });
        (this.filterData[filterData_CategoryNameForKey]).push(FiltersubjectNameForArray[filterData_CategoryNameForKey]);
        // console.log('Filter data after insertion object with array checkbox', this.filterData);
      } else {
        (this.filterData[dataValue]).push(FiltersubjectNameForArray[filterData_CategoryNameForKey]);
        // console.log('Filter data after insertion object with array checkbox', this.filterData[dataValue]);
        // console.log('true');
      }
      // console.log('Filter data after insertion object with array checkbox', this.filterData);
      // console.log(this.filterData);
    } else {
      for (let i = 0; i < this.filterData[dataValue].length; i++) {
        if (this.filterData[dataValue][i] === FiltersubjectNameForArray[dataValue]) {
          this.filterData[dataValue].splice(i, 1);
          if (this.filterData[dataValue].length === 0) {
            delete this.filterData[dataValue];
            this.SelectedCategoryLength[dataValue] = 0;
            delete this.objectSetter[dataValue];
            break;
          }
          // console.log('Filter data after Splicing object with array checkbox', this.filterData);
          // console.log(this.filterData[dataValue]);
        }
      }
    }
  }


  objectSetter = {};

  fieldValueCreatorFunction() {
    // const keys = Object.keys(this.filterData);
    this.fieldValue = Object.keys(this.filterData);
    console.log('FieldValue Object Data',"filter" ,this.fieldValue,this.filterData);
    for (const ObjectSetterData of this.fieldValue) {
      Object.assign(this.objectSetter, { [ObjectSetterData]: false });
    }

    for (const filterDataCate of this.fieldValue) {
      Object.assign(this.SelectedCategoryLength, { [filterDataCate]: this.filterData[filterDataCate].length });
    }
    // console.log('object length', this.SelectedCategoryLength);
  }

  tagCreatorFunction(CategoryObject: any, StatusToPushOrPop: any) {
    this.filterData = [];
    if (StatusToPushOrPop) {
      this.tagArray.push(CategoryObject);
      for (const data of this.tagArray) {
        for (const key in data) {
          if (isString(data[key])) {
            // console.log('key type',isString(data[key]));
            // console.log(key);
            this.filterTag.push(key);
          }
        }
      }
    }
    console.log('Filter TagArray', this.filterTag);
    console.log('Tag Array', this.tagArray);
  }


  // subjectName(subj: any, cat: any) {
  //   // console.log(subj.name);
  //   // console.log('Category name', cat);
  //   // this.SubjectFilterArray.push(subj.name);

  //   if (cat === "id") {
  //     for (const tagArray of this.Tags) {
  //       if (subj === tagArray) {
  //         console.log('tag matches');
  //         tagArray.status = this.SubjectName_CheckBoxedStatus;
  //         break;
  //       }
  //     }
  //   }

  //   this.filterDataObjectArrayCreation(cat, subj, this.SubjectName_CheckBoxedStatus);
  //   this.fieldValueCreatorFunction();
  //   this.onFilterSubjectPush(cat);
  //   // this.tagCreatorFunction(subj, this.SubjectName_CheckBoxedStatus);


  // }

  subjectName(subj: any, cat: any) {
    // console.log(subj.name);
    // console.log('Category name', cat);
    // this.SubjectFilterArray.push(subj.name);

    if (cat === "id") {
      for (const tagArray of this.Tags) {
        if (subj === tagArray) {
          console.log('tag matches');
          tagArray.status = this.SubjectName_CheckBoxedStatus;
          break;
        }
      }
    }

    this.filterDataObjectArrayCreation(cat, subj, this.SubjectName_CheckBoxedStatus);
    this.fieldValueCreatorFunction();
    this.onFilterSubjectPush(cat);
    // this.tagCreatorFunction(subj, this.SubjectName_CheckBoxedStatus);


  }
  onFilterSubjectPush(filterData_CategoryNameForKey: any) {
    this.newArray = [];
    // for (const data of this.asset) {
      for(const data of this.dummyDisplay){

      for (const ObjectSetterData of this.fieldValue) {
        Object.assign(this.objectSetter, { [ObjectSetterData]: false });
      }
      for (const value of this.fieldValue) {
      // for (let value = 0;  this.fieldValue.length-1;value++) {
        // this.tagArray = [];
        // console.log('Value', value);
        // console.log('FilterData of particular value', this.filterData[value]);
        // console.log('Length of FilterData of particular value', this.filterData[value].length);
        // tslint:disable-next-line: prefer-for-of
        // if (value === filterData_CategoryNameForKey) {
        // if (value == 'id') {
        if (this.fieldValue[value] == 'id') {
          console.log(value);
          var tagIds = [];
          if (data['tagIds']) {
            tagIds = data['tagIds'].split(',');
          }
          for (const tagData of tagIds) {
            for (let i = 0; i < this.filterData[value].length; i++) {

              // Pushing data into tag array
              // this.tagArray.push({[value]:this.filterData[value][i]});

              if (tagData == undefined || tagData != this.filterData[value][i]) {
                // console.log('ifTrue');
                // let checkData = data[value];
              } else {
                // console.log('ifFalse');
                this.objectSetter[value] = true;
                // let checkData = data[value];
                break;
              }
            }
          }

        }
        else {
          console.log('else part executed');

          for (let i = 0; i < this.filterData[value].length; i++) {
          // for (let i = 0; i < this.filterData[this.fieldValue[value]].length; i++) {
            // console.log(this.filterData[value].length)

            // Pushing data into tag array
            // this.tagArray.push({[value]:this.filterData[value][i]});

            // if (data[value] === undefined || data[value] !== this.filterData[value][i]) {
            if (data[value] == undefined || data[value] != this.filterData[value][i]) {
              // console.log('ifTrue');
              // let checkData = data[value];
            } else {
              // console.log('ifFalse');
              this.objectSetter[value] = true;
              // let checkData = data[value];
              break;
            }
          }

        }
      }
      // }



      // let ObjectDataValue = null;
      let Objectflag = true;
      this.ObjectSetterValueArray = Object.values(this.objectSetter);

      for (const objectSetterValueArrayData of this.ObjectSetterValueArray) {
        if (Objectflag && objectSetterValueArrayData) {
          Objectflag = true;
        } else {
          Objectflag = false;
        }
      }

      if (Objectflag) {
        this.newArray.push(data);
      }
    }

    // console.log(this.tagArray);
    console.log(this.newArray);


    if ((this.fieldValue.length !== 0)) {
      if ((this.newArray.length !== 0)) {
        // this.displayArray = this.newArray;
        this.displayArray = [];
        this.sum = 50;
        this.addItems(0, this.sum, 'push', this.newArray);
        this.noFilterAsset = false;

      } else {
        this.displayArray = [];
        // this.noFilterAsset = true;
      }
    } else {
      this.noFilterAsset = false;
      this.displayArray = [];
      this.sum = 50;
      this.addItems(0, this.sum = 50, 'push', this.asset);
      // this.displayArray = this.asset;
    }
  }

  // subject filteration ends here


  onUpdateSubjectName(event: Event) {
    this.SubjectName_CheckBoxedStatus = (<HTMLInputElement>event.target).checked;
    // console.log('Subject Name', this.SubjectName_CheckBoxedStatus);
  }
  ngOnDestroy() {
    this.toggleSidebar('open');
    let e1 = document.getElementsByTagName('nb-layout');
    if (e1 && e1.length != 0) {
      e1[0].classList.add('with-scroll');
    }
  }
  // this function is use to dynamicsearch on table
  searchQuestion(event) {
    var temData = this.asset;
    var val = event.target.value.toLowerCase();
    var keys = [];
    // filter our data
    if (temData.length > 0) {
      keys = Object.keys(temData[0]);
    }
    if (val.length >= 3 || val.length == 0) {
      var temp = temData.filter((d) => {
        for (const key of keys) {
          if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
            return true;
          }
        }
      });

      // update the rows
      this.displayArray = [];
      this.newArray = temp;
      this.sum = 50;
      this.addItems(0, this.sum, 'push', this.newArray);
    }
  }
  ngAfterContentChecked() {
    let e1 = document.getElementsByTagName('nb-layout');
    if (e1 && e1.length != 0) {
      e1[0].classList.remove('with-scroll');
    }

    // const e = document.getElementsByTagName('nb-layout-column');
    // // console.log(e[0].clientHeight);
    // // this.conentHeight = String(e[0].clientHeight) + 'px';
    // if (e[0].clientHeight > 700) {
    //   this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
    // } else {
    //   this.conentHeight = String(e[0].clientHeight) + 'px';
    // }

  }

  gotoFilter() {
    this.filter = !this.filter;
  }
}
