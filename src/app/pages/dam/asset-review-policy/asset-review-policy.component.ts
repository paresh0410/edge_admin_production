import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, NgForm } from '@angular/forms';
import { AssetReviewPolicyService } from './asset-review-policy.service';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationtemplateServiceService } from '../../plan/notification-templates/notification-templates.service';
import { DatePipe } from '@angular/common';
import { HttpClient } from "@angular/common/http";
import { ToastrService } from 'ngx-toastr';
import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';
import { webApi } from '../../../service/webApi';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { SuubHeader } from '../../components/models/subheader.model';
import { AddAssetService } from '../asset/add-asset/add-asset.service';
export const MY_CUSTOM_FORMATS = {
  fullPickerInput: 'DD-MM-YYYY HH:mm:ss',
  parseInput: 'DD-MM-YYYY HH:mm:ss',
  datePickerInput: 'DD-MM-YYYY',
  timePickerInput: 'LT',
  monthYearLabel: 'MMM YYYY',
  dateA11yLabel: 'LL',
  monthYearA11yLabel: 'MMMM YYYY'
};

@Component({
  selector: 'ngx-asset-review-policy',
  templateUrl: './asset-review-policy.component.html',
  styleUrls: ['./asset-review-policy.component.scss'],
  providers: [
    { provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE] },
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS }]

})

export class AssetReviewPolicyComponent implements OnInit {
  submitted = false;


  settingsCreatorDrop: any = {};
  reviewFormData: any = {
    // EditTemplateData: null,
    // ReviewText: false,
  };
  config = {
    height: '200px',
    uploader: {
      insertImageAsBase64URI: true,
    },
    allowResizeX: false,
    allowResizeY: false,
    placeholder: 'Edit Template',
    limitChars: 3000,
  };
  userId: any;
  tenantId: any;
  assetId: any;
  header: SuubHeader
  // {
  //   title:'ASSET REVIEW POLICY',
  //   btnsSearch: true,
  //   searchBar: false,
  //   dropdownlabel: ' ',
  //   drplabelshow: false,
  //   drpName1: '',
  //   drpName2: ' ',
  //   drpName3: '',
  //   drpName1show: false,
  //   drpName2show: false,
  //   drpName3show: false,
  //   btnName1: '',
  //   btnName2: '',
  //   btnName3: '',
  //   btnAdd: 'Save',
  //   btnName1show: false,
  //   btnName2show: false,
  //   btnName3show: false,
  //   btnBackshow: true,
  //   btnAddshow: true,
  //   filter: false,
  //   breadCrumbList:[],
  //   showBreadcrumb:true
  // };
  title: any;
  breadCrumbList: any = [];
  constructor(private assetreviewpolicyservice: AssetReviewPolicyService,
    private addAssetService: AddAssetService,
    // private toasterService: ToasterService,
    private spinner: NgxSpinnerService, public router: Router, public routes: ActivatedRoute,
    private notitempservice: NotificationtemplateServiceService, private datePipe: DatePipe,
    private cdr: ChangeDetectorRef, private toastr: ToastrService,private commonFunctionService: CommonFunctionsService,
    private http1: HttpClient) {
      this.getHelpContent();

    if (this.assetreviewpolicyservice.tenantId) {
      this.tenantId = this.assetreviewpolicyservice.tenantId;
      console.log('this.tenantId', this.tenantId);
    }

    if (this.assetreviewpolicyservice.getAssetDataForReview) {
      this.assetId = this.assetreviewpolicyservice.getAssetDataForReview.assetId;
      console.log('assetId', this.assetId);
    }

    if (localStorage.getItem('LoginResData')) {
      var userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', userData.data);
      this.userId = userData.data.data.id;
      console.log('userId', userData.data.data.id);
    }

    this.fetcheventsdropdown();
    this.fetchtemplatedropdown();
    this.fetchFrequencydropdown();
    this.getAllEmployeesDAM('');
    this.getAssetReviewPolicyById();



    this.settingsCreatorDrop = {
      text: "Select employee",
      singleSelection: true,
      classes: "common-multi",
      primaryKey: "empId",
      labelKey: "empName",
      noDataLabel: "Search & select employee...",
      enableSearchFilter: true,
      searchBy: ['empId', 'empName'],
      maxHeight:250,
      lazyLoading: true,
      // singleSelection: false,
      // text: "Select User",
      // selectAllText: 'Select All',
      // unSelectAllText: 'UnSelect All',
      // enableSearchFilter: true,
      // // badgeShowLimit: 2,
      // classes: "myclass custom-class"
    };
  }

  ngOnInit() {
    // if(this.addAssetService.breadCrumbArray){
    //   this.breadCrumbList = this.addAssetService.breadCrumbArray
    //   this.title = this.addAssetService.title
    // }
    this.header = {
      // title:'ASSET REVIEW POLICY',
      title : this.title,
      btnsSearch: true,
      searchBar: false,
      dropdownlabel: ' ',
      drplabelshow: false,
      drpName1: '',
      drpName2: ' ',
      drpName3: '',
      drpName1show: false,
      drpName2show: false,
      drpName3show: false,
      btnName1: '',
      btnName2: '',
      btnName3: '',
      btnAdd: 'Save',
      btnName1show: false,
      btnName2show: false,
      btnName3show: false,
      btnBackshow: true,
      btnAddshow: true,
      filter: false,
      // breadCrumbList:[],
      breadCrumbList:this.breadCrumbList,
      showBreadcrumb:true
    };
    if(this.addAssetService.breadCrumbArray){
      this.header.breadCrumbList = this.addAssetService.breadCrumbArray
      this.header.title = this.addAssetService.title
    }
  }



  presentToast(type, body) {
    if(type === 'success'){
    this.toastr.success(body, 'Success', {
    closeButton: false
    });
    } else if(type === 'error'){
    this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
    timeOut: 0,
    closeButton: true
    });
    } else{
    this.toastr.warning(body, 'Warning', {
    closeButton: false
    })
    }
    }


  allEmployeesAuto: any;
  noEmployees: boolean = false;
  getAllEmployeesDAM(val) {
    this.spinner.show();
    let param = {
      'srcStr': val,
      'tId': this.tenantId,
    };
    const _urlGetAllEmployees: string = webApi.domain + webApi.url.getAllEdgeEmployee;
    this.commonFunctionService.httpPostRequest(_urlGetAllEmployees,param)
    // this.assetreviewpolicyservice.getAllEmployee(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('getAllEmployessDAM:', rescompData);
        if (result['type'] == true) {
          if (result['data'][0].length == 0) {
            this.noEmployees = true;
          } else {
            this.allEmployeesAuto = result['data'][0];
            console.log('this.allEmployeesAuto', this.allEmployeesAuto);
            this.prepareEmployeeDropdown();
          }

        } else {
          // var toast: Toast = {
          //   type: 'error',
          //   //title: "Server Error!",
          //   body: "Something went wrong.please try again later.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }


      }, error => {
        this.spinner.hide();
        // var toast: Toast = {
        //   type: 'error',
        //   //title: "Server Error!",
        //   body: "Something went wrong.please try again later.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
  }

  frequencyDrop: any;
  fetchFrequencydropdown() {
    let param = {
      "tId": this.tenantId
    }
    const _urlfetchReviewFrequency = webApi.domain + webApi.url.getReviewFrequency;
    this.commonFunctionService.httpPostRequest(_urlfetchReviewFrequency,param)
    // this.assetreviewpolicyservice.getReviewFrequency(param)
      .then(rescompData => {

        var result = rescompData;
        var temp = result['data'][0];
        if (result['type'] == true) {
          console.log('FrequencyDropdown:', rescompData)
          this.frequencyDrop = temp;
        } else {
          // this.loader = false;
          // var toast: Toast = {
          //   type: 'error',
          //   //title: "Server Error!",
          //   body: "Something went wrong.please try again later.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }


      }, error => {
        //this.loader = false;
        // var toast: Toast = {
        //   type: 'error',
        //   //title: "Server Error!",
        //   body: "Something went wrong.please try again later.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
  }

  eventsDrop: any;
  fetcheventsdropdown() {
    let param = {
      "tId": this.tenantId,
      "aId": 27
    }
    const _urlfetchnoteventdropdown = webApi.domain + webApi.url.getnotevent;
    this.commonFunctionService.httpPostRequest(_urlfetchnoteventdropdown,param)
    // this.assetreviewpolicyservice.getNotEventDropdown(param)
      .then(rescompData => {

        var result = rescompData;
        var temp = result['data'][0];
        if (result['type'] == true) {
          console.log('EventNotDropdown:', rescompData)
          this.eventsDrop = temp;
        } else {
          // this.loader = false;
          // var toast: Toast = {
          //   type: 'error',
          //   //title: "Server Error!",
          //   body: "Something went wrong.please try again later.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }


      }, error => {
        //this.loader = false;
        // var toast: Toast = {
        //   type: 'error',
        //   //title: "Server Error!",
        //   body: "Something went wrong.please try again later.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
  }

  templateDrop: any = [];
  fetchtemplatedropdown() {
    let param = {
      //"nEventId": '1,2,3,4',
      "tid": this.tenantId,
      "aId": 27
    }
    const _urlfetchnottemplatedropdown = webApi.domain + webApi.url.dropdownnotifytemplate;
    this.commonFunctionService.httpPostRequest(_urlfetchnottemplatedropdown,param)
    // this.assetreviewpolicyservice.getNottemplateDropdown(param)
      .then(rescompData => {

        var result = rescompData;
        var temp = result['data'][0];
        if (result['type'] == true) {
          console.log('TemplateNotDropdown:', rescompData)
          this.templateDrop = temp;
        } else {
          // this.loader = false;
          // var toast: Toast = {
          //   type: 'error',
          //   //title: "Server Error!",
          //   body: "Something went wrong.please try again later.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }


      }, error => {
        //this.loader = false;
        // var toast: Toast = {
        //   type: 'error',
        //   //title: "Server Error!",
        //   body: "Something went wrong.please try again later.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
  }

  addAction: boolean = false;
  getAssetReviewPolicyById() {
    let param = {
      "tId": this.tenantId,
      "assetsId": this.assetId
    }
    const _urlGetAssetReviewPolicyById:string = webApi.domain + webApi.url.getAssetReviewPolicyById;
    this.commonFunctionService.httpPostRequest(_urlGetAssetReviewPolicyById,param)
    // this.assetreviewpolicyservice.getAssetReviewPolicyById(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('getAssetReveiwPolicyById:', rescompData);
        if (result['type'] === true) {
          if (result['data'][0].length === 0) {
            this.addAction = true;
            this.reviewFormData = {
              freq: '',
              event: '',
            };
          } else {
            this.addAction = false;
            let reviewData = result['data'][0];
            this.reviewFormData = reviewData[0];
            console.log('this.reviewFormData', this.reviewFormData);
            this.readyDataforEdit(this.reviewFormData);
            //this.prepareEmployeeDropdown();
          }

        } else {
          // var toast: Toast = {
          //   type: 'error',
          //   //title: "Server Error!",
          //   body: "Something went wrong.please try again later.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }


      }, error => {
        this.spinner.hide();
        // var toast: Toast = {
        //   type: 'error',
        //   //title: "Server Error!",
        //   body: "Something went wrong.please try again later.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });

  }

  modeArr: any = [];
  templateArr: any = [];
  templateNameArr:any = [];
  readyDataforEdit(editData) {
    console.log(editData);

    if (editData.modeIds) {
      this.modeArr = editData.modeIds.split(",");
      console.log('this.modeArr', this.modeArr);
    }

    if ( editData.templateIds ) {
      this.templateArr = editData.templateIds.split(",");
      console.log('this.templateArr', this.templateArr);
    }

    if ( editData.templates ) {
      this.templateNameArr = editData.templates.split(",");
    }

    if (editData.eventId) {
      this.onChange2(editData.eventId);
    }


    if ( this.modeArr.length > 0 ) {
      for (let i = 0; i < this.modeArr.length; i++) {
        if (this.modeArr[i] == "1" && this.templateArr[i] != null) {
          this.reviewFormData.lnmsg = true;
          this.reviewFormData.reviewTextSms = true;
          this.reviewFormData.lnmsgtemp = this.templateArr[i];
          this.reviewFormData.editSmsTemplateData = this.templateNameArr[i];
        }

        if (this.modeArr[i] == "2" && this.templateArr[i] != null) {
          this.reviewFormData.lnemail = true;
          this.reviewFormData.reviewTextEmail = true;
          this.reviewFormData.lnemailTemp = this.templateArr[i];
          this.reviewFormData.editEmailTemplateData = this.templateNameArr[i];
        }

        if (this.modeArr[i] == "3" && this.templateArr[i] != null) {
          this.reviewFormData.lnnot = true;
          this.reviewFormData.reviewTextNot = true;
          this.reviewFormData.lnnottemp = this.templateArr[i];
          this.reviewFormData.editNotTemplateData = this.templateNameArr[i];
        }
      }
    }





    console.log('this.reviewFormData', this.reviewFormData);

   // let selectedEmployee: any = [];
   var allEmployeeAutoDrop:any = [];
    this.allEmployeeAutoDrop.forEach((emp) => {
      if (emp.empId == this.reviewFormData.author) {
        let allEmployeeAutoDropObj = {};
        allEmployeeAutoDropObj = {
          empId: emp.empId,
          empName: emp.empName,
          empCode: emp.empCode
        }
        allEmployeeAutoDrop.push(allEmployeeAutoDropObj);
        // selectedEmployee.push(allEmployeeAutoDropObj);
      }
    });

    // new comment
    const empObj = {
      empId: this.reviewFormData.author,
      empName: this.reviewFormData.authorName,
    // this.allEmployeeAutoDrop.push(empObj);

    }
    // End New Comment

    //console.log('selectedEmployee', selectedEmployee);

    this.reviewFormData = {
      "incDate": this.reviewFormData.inceptionDate,
      "freq": this.reviewFormData.expiryPeriod,
      "expDate": this.reviewFormData.expiryDate,
      // "selectedCreator": this.allEmployeeAutoDrop,
      "selectedCreator": allEmployeeAutoDrop,
      "assetId": this.reviewFormData.assetId,
      "event": this.reviewFormData.eventId,
      "lnemail": this.reviewFormData.lnemail,
      "lnemailTemp": this.reviewFormData.lnemailTemp,
      "lnmsg": this.reviewFormData.lnmsg,
      "lnmsgtemp": this.reviewFormData.lnmsgtemp,
      "lnnot": this.reviewFormData.lnnot,
      "lnnottemp": this.reviewFormData.lnnottemp,
      "revPolId": this.reviewFormData.revPolId,
      "reviewTextSms":this.reviewFormData.reviewTextSms,
      "reviewTextNot":this.reviewFormData.reviewTextNot,
      "reviewTextEmail":this.reviewFormData.reviewTextEmail,
      "editSmsTemplateData": this.reviewFormData.editSmsTemplateData,
      "editNotTemplateData": this.reviewFormData.editNotTemplateData,
      "editEmailTemplateData": this.reviewFormData.editEmailTemplateData
    }

    console.log('New reviewFormData:', this.reviewFormData);
  }


  templatesms: any = [];
  templatenot: any = [];
  templateemail: any = [];
  onChange2(notEventId) {

    this.templatesms = [];
    this.templatenot = [];
    this.templateemail = [];



    for (let i = 0; i < this.templateDrop.length; i++) {
      if (this.templateDrop[i].notEventId == notEventId) {
        if (this.templateDrop[i].notModeId == 1) {
          let tempObj = {
            id: this.templateDrop[i].templateId,
            name: this.templateDrop[i].templateName,
            desc:this.templateDrop[i].description,
            template:this.templateDrop[i].template
          }
          this.templatesms.push(tempObj)
        } else if (this.templateDrop[i].notModeId == 2) {
          let tempObj = {
            id: this.templateDrop[i].templateId,
            name: this.templateDrop[i].templateName,
            desc:this.templateDrop[i].description,
            template:this.templateDrop[i].template,
            subject:this.templateDrop[i].subject
          }
          this.templateemail.push(tempObj)
        } else if (this.templateDrop[i].notModeId == 3) {
          let tempObj = {
            id: this.templateDrop[i].templateId,
            name: this.templateDrop[i].templateName,
            desc:this.templateDrop[i].description,
            template:this.templateDrop[i].template
          }
          this.templatenot.push(tempObj)
        }
      }
    }

    this.cdr.detectChanges();

    console.log('this.templatesms', this.templatesms);
    console.log('this.templateemail', this.templateemail);
    console.log('this.templatenot', this.templatenot);

  }
  SelectValue: any = "";
  onChangeSms(event: Event) {
    let data = (<HTMLInputElement>event.target).checked;
    // console.log(data);
    if (this.SelectValue.length === 0  || !data) {
      this.reviewFormData.ReviewText = false;
    } else {
      this.reviewFormData.ReviewText = true;

    }
  }

  onChangeDropDownSms(event: Event) {
   let SelectedValue = (<HTMLInputElement>event.target).value;
    console.log(event);
    console.log(SelectedValue);

      this.reviewFormData.reviewTextSms = true;
      for(let i = 0;i< this.templatesms.length;i++){
        if(SelectedValue == this.templatesms[i].id){
          this.reviewFormData.editSmsTemplateData = this.templatesms[i].template;
        }
      }

  }

  onChangeDropDownNot(event: Event) {
    let SelectedValue = (<HTMLInputElement>event.target).value;
     console.log(event);
     console.log(SelectedValue);

     this.reviewFormData.reviewTextNot = true;
      for(let i = 0;i< this.templatenot.length;i++){
        if(SelectedValue == this.templatenot[i].id){
          this.reviewFormData.editNotTemplateData = this.templatenot[i].template;
        }
      }
   }

   onChangeDropDownEmail(event: Event) {
    let SelectedValue = (<HTMLInputElement>event.target).value;
     console.log(event);
     console.log(SelectedValue);

     this.reviewFormData.reviewTextEmail = true;
      for(let i = 0;i< this.templateemail.length;i++){
        if(SelectedValue == this.templateemail[i].id){
          this.reviewFormData.editEmailTemplateData = this.templateemail[i].name;
          this.reviewFormData.editEmailSubject = this.templateemail[i].subject
        }
      }
   }

  notDetailsModal: boolean = false;
  templateData: any = {};
  getTempData(value, data) {
    //this.addEngagePopup = false;
    this.spinner.show();
    console.log('previewData:', data);
    let param = {
      'tempId': data,
      'tId': this.tenantId,
    }
  const _urlFetchNotTempById:string = webApi.domain + webApi.url.fetchnottempbyid;
  this.commonFunctionService.httpPostRequest(_urlFetchNotTempById,param)
    this.notitempservice.getNottepById(param)
      .then(rescompData => {

        if (rescompData != "err") {
          this.spinner.hide();
          var temp = rescompData;
          var data = temp['data'][0];
          var res = data[0];

          if (temp['type'] == true) {
            this.templateData = {
              name: res.tname,
              desc: res.description,
              template: res.template
            }
            console.log('notifytemplateById', this.templateData);
            this.notDetailsModal = true;
          } else {
            // var toast: Toast = {
            //   type: 'error',
            //   //title: "Server Error!",
            //   body: "Something went wrong.please try again later.",
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(toast);
          this.presentToast('error', '');
            //this.addEngagePopup = true;
          }
        } else {
          this.spinner.hide();
          // var toast: Toast = {
          //   type: 'error',
          //   //title: "Server Error!",
          //   body: "Something went wrong.please try again later.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
          //this.addEngagePopup = true;
        }

      })

  }

  closeNotDetailModal() {
    this.notDetailsModal = false;
  }

  selectedCreator: any;
  usersList: any = [];
  allEmployeeAutoDrop: any = [];

  prepareEmployeeDropdown() {
    this.allEmployeesAuto.forEach(element => {
      let allEmployeeAutoDropObj = {};
      allEmployeeAutoDropObj = {
        empId: element.id,
        empName: element.name,
        empCode : element.code
      }
      this.allEmployeeAutoDrop.push(allEmployeeAutoDropObj);
    });
    console.log('this.allEmployeeAutoDrop', this.allEmployeeAutoDrop);
  }

  onCreatorSelect(item: any) {
    console.log(item);
    console.log(this.selectedCreator);
  }

  OnCreatorDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedCreator);
  }

  onCreatorSearch(evt: any) {
    console.log(evt.target.value);
    const val = evt.target.value;
    this.allEmployeeAutoDrop = [];
    if (val.length > 3) {
      this.getAllEmployeesDAM(val);
    }
    // const tuser = this.allEmployeeAutoDrop.filter(function (d) {
    //   return String(d.empId).toLowerCase().indexOf(val) !== -1 ||
    //     d.empName.toLowerCase().indexOf(val) !== -1 || !val;
    // });

    // this.usersList = tuser;
  }

  expDate: any;
  setExpDate(incDate, freq) {
    console.log('incDate', incDate);

    var d = new Date(incDate);
    console.log(d.toLocaleDateString());

    if (freq == 1) {
      this.reviewFormData.expDate = new Date(d.setMonth(d.getMonth() + 1));
      console.log(d.toLocaleDateString());
    } else if (freq == 3) {
      this.reviewFormData.expDate = new Date(d.setMonth(d.getMonth() + 3));
      console.log(d.toLocaleDateString());
    } else if (freq == 6) {
      this.reviewFormData.expDate = new Date(d.setMonth(d.getMonth() + 6));
      console.log(d.toLocaleDateString());
    }
    this.expDate = this.reviewFormData.expDate;
    console.log('this.reviewFormData.expDate', this.reviewFormData.expDate);
  }

  onSubmit(data , reviewForm : NgForm) {
    console.log('data', data);
    this.submitted = true;

    if(reviewForm.valid == true){
      this.spinner.show()
    let qOptions = this.getdatareadyforNotifications(data);

    let param = {
      "revPolId": this.addAction ? 0 : this.reviewFormData.revPolId,
      "assetsId": this.assetId,
      "author": data.selectedCreator[0].empId,
      "inceptionDate": this.formatDate(data.incDate),
      "expiryPeriod": data.freq,
      "expiryDate": this.formatDate(data.expDate),
      "tId": this.tenantId,
      "userId": this.userId,
      "nEventId": data.event,
      "qOptions": qOptions
    }

    console.log('param', param);
    const _urlAddEditAssetReviewPolicy: string = webApi.domain + webApi.url.addEditAssetReviewPolicy;
    this.commonFunctionService.httpPostRequest(_urlAddEditAssetReviewPolicy,param)
    // this.assetreviewpolicyservice.addEditAssetReviewPolicy(param)
      .then(rescompData => {
        var result = rescompData;
        console.log('AssetReviewPolicyAddEditResponse:', rescompData);
        if (this.addAction) {
          if (result['type'] == true) {
          this.spinner.hide();
            // var toast: Toast = {
            //   type: 'success',
            //   title: "Asset Review Policy",
            //   body: "Asset review policy inserted successfully.",
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(toast);
            // var length = this.addAssetService.breadCrumbArray.length-1
            // this.title = this.addAssetService.breadCrumbArray[length].name
            // this.addAssetService.title = this.title
            // this.addAssetService.breadCrumbArray = this.addAssetService.breadCrumbArray.slice(0,length)
          // this.title = this.addassetservice.breadCrumbArray[]
          // this.addAssetService.previousBreadCrumb = this.addAssetService.previousBreadCrumb.slice(0,length+1)
          this.presentToast('success', 'File review policy added');
            // this.router.navigate(['../../asset'], { relativeTo: this.routes });
          // this.router.navigate(['../'],{relativeTo:this.routes});
            this.router.navigate(['../../folders'], { relativeTo: this.routes });
          } else {
            // var toast: Toast = {
            //   type: 'error',
            //   title: "Asset Review Policy",
            //   body: "Unable to insert asset review policy.",
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(toast);
            this.spinner.hide();
            this.presentToast('error', '');
          }
        } else {
          if (result['type'] == true) {
          this.spinner.hide();
            // var toast: Toast = {
            //   type: 'success',
            //   title: "Asset Review Policy",
            //   body: "Asset review policy updated successfully.",
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(toast);
            // var length = this.addAssetService.breadCrumbArray.length-1
            // this.title = this.addAssetService.breadCrumbArray[length].name
            // this.addAssetService.title = this.title
            // this.addAssetService.breadCrumbArray = this.addAssetService.breadCrumbArray.slice(0,length)
          // this.title = this.addassetservice.breadCrumbArray[]
          // this.addAssetService.previousBreadCrumb = this.addAssetService.previousBreadCrumb.slice(0,length+1)
            this.presentToast('success', 'File review policy updated');
            // this.router.navigate(['../../asset'], { relativeTo: this.routes });
              //  this.router.navigate(['../'],{relativeTo:this.routes});
            this.router.navigate(['../../folders'], { relativeTo: this.routes });


          } else {
            this.spinner.hide();
            // var toast: Toast = {
            //   type: 'error',
            //   title: "Asset Review Policy",
            //   body: "Unable to update asset review policy.",
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(toast);
            this.presentToast('error', '');
          }
        }



      }, error => {
        this.spinner.hide();
        // var toast: Toast = {
        //   type: 'error',
        //   //title: "Server Error!",
        //   body: "Something went wrong.please try again later.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });

      console.log(this.reviewFormData);
    }
    else{
      console.log('Please Fill all fields');
      // const detForm: Toast = {
      //   type: 'error',
      //   title: 'Unable to update',
      //   body: 'Please Fill all fields',
      //   showCloseButton: true,
      //   timeout: 2000
      // };
      // this.toasterService.pop(detForm);
      this.presentToast('warning', 'Please fill in the required fields');
          Object.keys( reviewForm.controls).forEach(key => {
            reviewForm.controls[key].markAsDirty();
           });
    }
  }


  rulename: any;
  //assetId: any = 1;
  getdatareadyforNotifications(notidetails) {

    if (notidetails.lnmsg == true) {
      var modeidsms = 1;
      var templateidsms = notidetails.lnmsgtemp;
      var templatesms = notidetails.editSmsTemplateData;
      //var rulenamesms = "course" + "_" + this.courseId + "_" + "SMS";
      var rulenamesms = "ASSET" + "_" + this.assetId + "_" + "SMS";
      var ruledescsms = "ASSET" + "_" + this.assetId + "_" + "SMS";
      var noteventid = notidetails.event;

      var smsString = rulenamesms + "#" + ruledescsms + "#" + noteventid + "#" + modeidsms + "#" + templateidsms + "#" + templatesms + "#" + 1;
    } else {
      modeidsms = null;
      templateidsms = null;
    }

    if (notidetails.lnemail == true) {
      var modeidemail = 2;
      var templateidemail = notidetails.lnemailTemp;
      var templateemail = notidetails.editEmailTemplateData;
      var templateSubject =  notidetails.editEmailSubject
      //	var rulenameemail = "course" + "_" + this.courseId + "_" + "EMAIL";
      var rulenameemail = "ASSET" + "_" + this.assetId + "_" + "EMAIL";
      var ruledescemail = "ASSET" + "_" + this.assetId + "_" + "EMAIL";
      var noteventid = notidetails.event;

      var emailString = rulenameemail + "#" + ruledescemail + "#" + noteventid + "#" + modeidemail + "#" + templateidemail + "#" + templateemail + "#" + 1;
    } else {
      modeidemail = null;
      templateidemail = null;
    }

    if (notidetails.lnnot == true) {
      var modeidnot = 3;
      var templateidnot = notidetails.lnnottemp;
      var templatenot = notidetails.editNotTemplateData;
      //	var rulenamenotify = "course" + "_" + this.courseId + "_" + "NOTIFY";
      var rulenamenotify = "ASSET" + "_" + this.assetId + "_" + "APP NOTIFICATION";
      var ruledescnotify = "ASSET" + "_" + this.assetId + "_" + "APP NOTIFICATION";
      var noteventid = notidetails.event;

      var notifyString = rulenamenotify + "#" + ruledescnotify + "#" + noteventid + "#" + modeidnot + "#" + templateidnot + "#" + templatenot + "#" + 1;
    } else {
      modeidnot = null;
      templateidnot = null;
    }

    if (notidetails.lnmsg == true && notidetails.lnemail == true && notidetails.lnnot == true) {
      this.rulename = smsString + "|" + emailString + "|" + notifyString;
    } else if (notidetails.lnemail == true && notidetails.lnnot == true) {
      this.rulename = emailString + "|" + notifyString;
    } else if (notidetails.lnmsg == true && notidetails.lnnot == true) {
      this.rulename = smsString + "|" + notifyString;
    } else if (notidetails.lnmsg == true && notidetails.lnemail == true) {
      this.rulename = smsString + "|" + emailString;
    } else if (notidetails.lnnot == true) {
      this.rulename = notifyString;
    } else if (notidetails.lnmsg == true) {
      this.rulename = smsString;
    } else if (notidetails.lnemail == true) {
      this.rulename = emailString;
    }
    // else if(notidetails.lnmsg == false && notidetails.lnemail == false && notidetails.lnnot == false){
    //   var toast: Toast = {
    //     type: 'error',
    //     //title: "Server Error!",
    //     body: "Please Select atleast one notification template",
    //     showCloseButton: true,
    //     timeout: 4000
    //   };
    //   this.toasterService.pop(toast);
    // }

    //this.ruledescription = this.rulename;
    console.log('thi.rulename', this.rulename)
    return this.rulename;


  }

  formatDate(date) {
    var d = new Date(date)
    var formatted = this.datePipe.transform(d, 'yyyy-MM-dd');
    return formatted;
  }


  back() {
    // var length = this.addAssetService.breadCrumbArray.length-1
    // this.title = this.addAssetService.breadCrumbArray[length].name
    // this.addAssetService.title = this.title
    // this.addAssetService.breadCrumbArray = this.addAssetService.breadCrumbArray.slice(0,length)
    // this.title = this.addassetservice.breadCrumbArray[]
    // this.addAssetService.previousBreadCrumb = this.addAssetService.previousBreadCrumb.slice(0,length+1)
    console.log(this.title,"title")
    // this.addAssetService.breadCrumbArray.pop();
    this.addAssetService.previousBreadCrumb.pop();
    this.addAssetService.breadCrumbArray.pop()
    this.addAssetService.title = this.addAssetService.previousBreadCrumb[this.addAssetService.breadCrumbArray.length].name
    // this.addAssetService.backFlag=true;
    // this.router.navigate(['../'],{relativeTo:this.routes});
    // this.router.navigate(['../../asset'], { relativeTo: this.routes });
    // this.router.navigate(['../'],{relativeTo:this.routes});
    this.router.navigate(["../../folders"], { relativeTo: this.routes });

  }
  backToAsset(data){
    // this.addAssetService.categoryId = data.assetId
    // this.addAssetService.shareCatId = data.assetId
    // this.addAssetService.title = data.assetName
    this.addAssetService.categoryId = data.assetId
    this.addAssetService.title = data.assetName
    this.addAssetService.breadCrumbArray = this.addAssetService.breadCrumbArray.slice(0,data.index)
    // this.title = this.addAssetService.breadCrumbArray[]
    this.addAssetService.previousBreadCrumb = this.addAssetService.previousBreadCrumb.slice(0,data.index+1)
    // this.addAssetService.breadCrumbArray = this.addAssetService.breadCrumbArray.slice(0,data.index)
    // this.title = this.addassetservice.breadCrumbArray[]
    // this.addAssetService.previousBreadCrumb = this.addAssetService.previousBreadCrumb.slice(0,data.index+1)

    // this.router.navigate(['../'], { relativeTo: this.routes });


  }


      // Help Code Start Here //

      helpContent: any;
      getHelpContent() {
      return new Promise(resolve => {
        this.http1
        .get("../../../../../../assets/help-content/addEditCourseContent.json")
        .subscribe(
          data => {
          this.helpContent = data;
          console.log("Help Array", this.helpContent);
          },
          err => {
          resolve("err");
          }
        );
      });
      // return this.helpContent;
      }

      // Help Code Ends Here //

}
