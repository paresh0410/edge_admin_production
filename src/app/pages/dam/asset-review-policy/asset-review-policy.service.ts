import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, RequestOptions, Request } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AppConfig } from '../../../app.module';
import { webApi } from '../../../service/webApi';
import { HttpClient } from "@angular/common/http";

@Injectable()

export class AssetReviewPolicyService {

    public data: any;
    request: Request;

    tenantId : any = 1;
    getAssetDataForReview : any;
    userData;
    private _url: string = "";
    private _urlfetchnoteventdropdown = webApi.domain + webApi.url.getnotevent;
    private _urlfetchnottemplatedropdown = webApi.domain + webApi.url.dropdownnotifytemplate;
    private _urlfetchReviewFrequency = webApi.domain + webApi.url.getReviewFrequency;
    private _urlGetAllEmployees: string = webApi.domain + webApi.url.getAllEdgeEmployee;
    private _urlAddEditAssetReviewPolicy: string = webApi.domain + webApi.url.addEditAssetReviewPolicy;
    private _urlGetAssetReviewPolicyById:string = webApi.domain + webApi.url.getAssetReviewPolicyById;


    constructor(@Inject('APP_CONFIG_TOKEN') private config: AppConfig, private _http: Http, private _httpClient: HttpClient) {
        //this.busy = this._http.get('...').toPromise();
        if(localStorage.getItem('LoginResData')){
            this.userData = JSON.parse(localStorage.getItem('LoginResData'));
            console.log('userData', this.userData.data);
            // this.userId = this.userData.data.data.id;
            this.tenantId = this.userData.data.data.tenantId;
         }
    }

    getData() {
        let url: any = `${this.config.FINAL_URL}` + this._url;
        // let url:any = `${this.config.DEV_URL}`+this._urlFilter;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        //let body = JSON.stringify(user);
        return this._http.post(url, options).map((res: Response) => res.json());
    }

    getNotEventDropdown(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlfetchnoteventdropdown, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    getNottemplateDropdown(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlfetchnottemplatedropdown, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    getReviewFrequency(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlfetchReviewFrequency, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    getAllEmployee(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlGetAllEmployees, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    addEditAssetReviewPolicy(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlAddEditAssetReviewPolicy, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    getAssetReviewPolicyById(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlGetAssetReviewPolicyById, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    _errorHandler(error: Response) {
        console.error(error);
        return Observable.throw(error || "Server Error")
    }


}
