import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetReviewPolicyComponent } from './asset-review-policy.component';

describe('AssetReviewPolicyComponent', () => {
  let component: AssetReviewPolicyComponent;
  let fixture: ComponentFixture<AssetReviewPolicyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetReviewPolicyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetReviewPolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
