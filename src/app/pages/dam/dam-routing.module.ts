import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DamComponent } from './dam.component';
import { AddAssetComponent } from './asset/add-asset/add-asset.component';
import { ShareAssetComponent } from './share-asset/share-asset.component';
import { ApproveAssetComponent } from './approve-asset/approve-asset.component';
import { AssetReviewPolicyComponent } from './asset-review-policy/asset-review-policy.component';
// import { IntermediateDamComponent } from './intermediate-dam/intermediate-dam.component';
import { AssetComponent } from './asset/asset.component';
import { AssetVersionComponent } from './asset-version/asset-version.component';
// import { AssetCategoryComponent } from './dam/asset-category/asset-category.component';
// import { AddEditAssetCategoryComponent} from './dam/asset-category/add-edit-asset-category/add-edit-asset-category.component'
//import { AssetComponent } from './dam/asset/asset.component'
import { AssetCategoriesComponent } from './asset-categories/asset-categories.component';
import { BulkUploadComponent } from './bulk-upload/bulk-upload.component'
import { NewAssetComponent } from './new-asset/new-asset.component';
import { FolderFileComponent } from './folder-file/folder-file.component';
import { FolderUploadComponent } from './folder-upload/folder-upload.component';

const routes: Routes = [
  {path: '', component: DamComponent},
  { path: 'asset-category', component: AssetCategoriesComponent },
  { path: 'assets', component: NewAssetComponent },
  { path: 'folders', component: FolderFileComponent },
  { path: 'asset-category/asset/add-edit-asset', component: AddAssetComponent },//Add Asset >asset-cateory>asset
  { path: 'assets/add-edit-asset', component: AddAssetComponent },//Add Asset >asset-cateory>asset
  { path: 'add-asset', component: AddAssetComponent },//Add Asset Direct
  { path: 'asset/share-asset', component: ShareAssetComponent },
  { path: 'assets/share-asset', component: ShareAssetComponent },
  { path: 'asset/asset-review-policy', component: AssetReviewPolicyComponent },
  { path: 'assets/asset-review-policy', component: AssetReviewPolicyComponent },
  { path: 'asset/approve-asset', component: ApproveAssetComponent },
  { path: 'asset-category/asset', component: AssetComponent },//Asset from category
  { path: 'asset', component: AssetComponent },//Direct Asset
  { path: 'asset-category/newasset', component: NewAssetComponent },//Asset from category
  { path: 'assets', component: NewAssetComponent },//Direct Asset
  { path: 'folders', component: FolderFileComponent },

  { path: 'asset-category/asset/asset-version', component: AssetVersionComponent },
  { path: 'assets/asset-version', component: AssetVersionComponent },
  { path: 'bulk-upload', component: BulkUploadComponent },
  { path: 'bulk-upload/file-upload', component: FolderUploadComponent },
  { path: 'assets/file-upload', component: FolderUploadComponent },
  // {path : 'dam/asset-category',component : AssetCategoryComponent },
  // {path : 'dam/asset-category/add-edit-asset-category',component : AddEditAssetCategoryComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DamRoutingModule { }
