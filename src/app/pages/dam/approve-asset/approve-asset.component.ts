import {
  Component,
  OnInit,
  AfterViewInit,
  Renderer2,
  ViewChild,
  Pipe,
  PipeTransform,
  ChangeDetectorRef,
  OnDestroy,
} from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
  NgForm,
} from "@angular/forms";
import { ApproveAssetService } from "./approve-asset.service";
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { NgxSpinnerService } from "ngx-spinner";
import { Router, ActivatedRoute } from "@angular/router";
import { DatePipe } from "@angular/common";
import * as $ from "jquery";
import { webAPIService } from "../../../service/webAPIService";
import { webApi } from "../../../service/webApi";
import { DomSanitizer } from "@angular/platform-browser";
import { UriBuilder } from "uribuilder";
import { cleanPath } from "cleanpath";
import { HttpClient } from "@angular/common/http";
// import { stream } from 'xlsx/types';
declare var kPoint;
import { ToastrService } from "ngx-toastr";
import { CommonFunctionsService } from "../../../service/common-functions.service";
import { SuubHeader } from "../../components/models/subheader.model";
import { AddAssetService } from "../asset/add-asset/add-asset.service";
declare var $kPointQuery;
//import { Pipe, PipeTransform } from '@angular/core';
// let _this: any;
declare var mime;
@Component({
  selector: "ngx-approve-asset",
  templateUrl: "./approve-asset.component.html",
  styleUrls: ["./approve-asset.component.scss"],
})
@Pipe({ name: "safe" })
export class ApproveAssetComponent implements OnInit, AfterViewInit, OnDestroy {
  youtubeplayer: YT.Player;
  youtubeplay: any = {
    id: "",
  };
  youtubeurl: any = "https://www.youtube.com/watch?v=n_GFN3a0yj0";

  submitted: boolean = false;
  @ViewChild("video_file") videofile: any;
  @ViewChild("audio_file") audiofile: any;
  @ViewChild("image_file") imagefile: any;
  @ViewChild("iframe_file") iframe: any;
  @ViewChild("ppt_file") ppt: any;
  @ViewChild("word_file") word: any;
  @ViewChild("excel_file") excel: any;
  @ViewChild("kpplayer") kpplayer: any;
  @ViewChild("scorm_file") scormfile: any;
  pdfUrlFlag = false;
  isLocalfile: boolean;

  kpointurl: any =
    "https://showcase.kpoint.com/kapsule/gcc-1b568768-ad89-4d6c-b1aa-ecb0958617b4/nv3/embedded&autoplay=true";

  categories: any = [
    { value: 1, name: "cat 1" },
    { value: 2, name: "cat 2" },
    { value: 3, name: "cat 3" },
    { value: 4, name: "cat 4" },
  ];

  formats: any = [
    // { value: 1, name: "Format 1" },
    // { value: 2, name: "Format 2" },
    // { value: 3, name: "Format 3" },
    // { value: 4, name: "Format 4" },
  ];
  channels: any = [
    { value: 1, name: "Channel 1" },
    { value: 2, name: "Channel 2" },
    { value: 3, name: "Channel 3" },
    { value: 4, name: "Channel 4" },
  ];
  languages: any = [
    { value: 1, name: "Language 1" },
    { value: 2, name: "Language 2" },
    { value: 3, name: "Language 3" },
    { value: 4, name: "Language 4" },
  ];
  reasons: any = [
    { value: 1, name: "Reason 1" },
    { value: 2, name: "Reason 2" },
    { value: 3, name: "Reason 3" },
    { value: 4, name: "Reason 4" },
  ];
  pdfserverlink = "";
  status: any = [];
  tenantId: any;
  getAssetDataForApproval: any = [];
  approveForm: any = {};
  settingsEmployeeDrop: any = {};
  userId: any;
  files: any;
  settingsTagDrop = {};
  tempTags: any = [];
  tagList: any = [];
  selectedTags: any = [];
  show: boolean = false;
  header: SuubHeader = {
    title: "APPROVE ASSET",
    btnsSearch: true,
    searchBar: false,
    dropdownlabel: " ",
    drplabelshow: false,
    drpName1: "",
    drpName2: " ",
    drpName3: "",
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: "",
    btnName2: "",
    btnName3: "",
    btnAdd: "Save",
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: true,
    filter: false,
    showBreadcrumb: true,
    breadCrumbList:[
      {
        'name': 'DAM',
        'navigationPath': '/pages/dam',
      },
      {
        'name': 'Folder',
        'navigationPath': '/pages/dam/folders',
      },
      {
      'name': 'Approve File',
      'navigationPath': '/pages/dam/asset',
      },
    ]
  };
  tabId: any;
  CategoryName: any = '';
  catshow: boolean;
  categoryId: any;
  oldParentCatId: any;
  noCatwise: boolean;
  assetCat: any;
  demoassetCat: any;
  valid: boolean;
  formatExtensions: any;
  url: any;
  popupMsg: any;
  checkstatus: boolean=false;
  constructor(
    private approveassetservice: ApproveAssetService,
    // private toasterService: ToasterService,
    private commonFunctionService: CommonFunctionsService,
    private spinner: NgxSpinnerService,
    public router: Router,
    public routes: ActivatedRoute,
    private addassetservice: AddAssetService,
    private datePipe: DatePipe,
    protected webApiService: webAPIService,
    private sanitizer: DomSanitizer,
    private render: Renderer2,
    private cdf: ChangeDetectorRef,
    private toastr: ToastrService,
    private http1: HttpClient
  ) {
    this.getHelpContent();
    // this = this;
    if (localStorage.getItem("LoginResData")) {
      var userData = JSON.parse(localStorage.getItem("LoginResData"));
      console.log("userData", userData.data);
      this.userId = userData.data.data.id;
      console.log("userId", userData.data.data.id);
    }
    if (localStorage.getItem('formatExtensions')) {
      this.formatExtensions = JSON.parse(localStorage.getItem('formatExtensions'));
    }
    if(this.approveassetservice.title){
      this.header.title = this.approveassetservice.title
    }
    // this.approveAssetService.tabId = this.tabId
    if(this.approveassetservice.tabId){
      this.tabId = this.approveassetservice.tabId
    }
    this.settingsTagDrop = {
      text: "Select Tags",
      singleSelection: false,
      classes: "common-multi",
      primaryKey: "id",
      labelKey: "name",
      noDataLabel: "Search Tags...",
      enableSearchFilter: true,
      searchBy: ["name"],
      maxHeight: 250,
      lazyLoading: true,
    };

    this.tenantId = this.approveassetservice.tenantId;
    this.getAllAssetDropdown();
    this.getallTagList();
    this.getAllEdgeEmployees('');

    console.log("allEmployeeAutoDrop", this.allEmployeeAutoDrop);
    //this.prepareEmployeeDropdown();
    this.settingsEmployeeDrop = {
      text: "Search & Select Employee",
      singleSelection: false,
      classes: "common-multi",
      primaryKey: "empId",
      labelKey: "empName",
      noDataLabel: "Search Employee...",
      enableSearchFilter: true,
      searchBy: ["empId", "empName"],
      maxHeight: 250,
      lazyLoading: true,
      disabled: true
    };
  }

  fileName: any = "You can drag and drop files here to add them.";
  fileIcon: any = "assets/img/app/profile/avatar4.png";
  selectFileTitle: any = "No file chosen";

  assetFileData: any;
  audioUpload: boolean = false;
  videoUpload: boolean = false;
  pdfUpload: boolean = false;
  ngOnInit() {
    this.spinner.show();
    setTimeout(() => {
    this.spinner.hide();
    }, 2000);
    this.showReasons = false;
    this.nativeElementConfigure();
    if(this.addassetservice.breadCrumbArray){
      this.header.breadCrumbList = this.addassetservice.breadCrumbArray
      this.header.title = this.addassetservice.title
    }
  }
  getallTagList() {
    this.spinner.show();
    let url = webApi.domain + webApi.url.getAllTagsComan;
    let param = {
      tenantId: this.tenantId,
    };
    this.webApiService.getService(url, param).then(
      (rescompData) => {
        // this.loader =false;
        this.spinner.hide();
        var temp: any = rescompData;
        this.tagList = temp.data;
        this.tempTags = [...this.tagList];
        this.prepareApproveFormEmployee();
        // console.log('Visibility Dropdown',this.visibility);
      },
      (resUserError) => {
        // this.loader =false;
        this.show = false;
        this.spinner.hide();
        this.errorMsg = resUserError;
      }
    );
  }
  removeFile() {
    this.assetFileData = null;
    this.fileName = "You can drag and drop files here to add them.";
    this.selectFileTitle = "No file chosen";
    // this.approveForm.assetRef = null;
    this.approveForm.assetRef = '';
    this.isLocalfile = false

  }

  readFileUrl(event:any) {
    if(!this.approveForm.format || this.approveForm.format === ''){
      this.presentToast('warning', 'Select Format before uploading file');
      return null;
    }
    var size = 100000000;
    // if(this.addEditAssetForm.format == 1){
    // // var validExts = new Array( "video" );
    // }
    // else if(this.addEditAssetForm.format == 2){
    // // var validExts = new Array( "audio" );
    // }
    // else if(this.addEditAssetForm.format == 3){
    // // var validExts = new Array( "pdf" );
    // }
    // else if(this.addEditAssetForm.format == 7){
    // // var validExts = new Array( "image" );
    // }
    // else if(this.addEditAssetForm.format == 10){
    // // var validExts = new Array( "application/vnd.ms-powerpoint","application/vnd.openxmlformats-officedocument.presentationml.presentation" );
    // }
    // else if(this.addEditAssetForm.format == 11){
    // // var validExts = new Array( "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/vnd.ms-excel" );
    // }
    // else if(this.addEditAssetForm.format == 12){
    // // var validExts = new Array( "application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document" );
    // }
    // else if(this.addEditAssetForm.format == 5){
    // // var validExts = new Array( "application/zip" );
    // }
    // // var validExts  "application/zip"= new Array("image", "video", "audio", "pdf","application/vnd.ms-powerpoint","application/vnd.openxmlformats-officedocument.presentationml.presentation","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/vnd.ms-excel","application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document");

    // var fileType = event.target.files[0].type;
    var validExts = this.formatExtensions
    // var fileType = event.target.files[0].type;
    // var fileType = event.target.files[0].name.split('.')[1];
    var last_dot = event.target.files[0].name.lastIndexOf('.')
    var fileType = event.target.files[0].name.slice(last_dot + 1)

    console.log(last_dot,"fileType")
    console.log(fileType,"fileType")
    // fileType = fileType.substring(0,5);

    // var validExts = new Array(".png", ".jpg", ".jpeg",);
    // var fileExt = event.target.files[0].name;
    // fileExt = fileExt.substring(fileExt.lastIndexOf('.'));

    var fileExt = false;
    // for(let i=0; i<validExts.length; i++){
    //   if(fileType.includes(validExts[i])){
    //     // return true;
    //     fileExt = true;
    //     break;
    //   }
    // }

    for(let i=0; i<validExts.length; i++){
      if(this.approveForm.format == validExts[i].formatId){
      if(fileType.includes(validExts[i].extension)){
        // return true;
        fileExt = true;
        break;
      }
    }
    }

    // if(validExts.indexOf(fileType) < 0) {
    if(!fileExt) {
      // var toast : Toast = {
      //   type: 'error',
      //   title: "Invalid file selected!",
      //   body: "Valid files are of " + validExts.toString() + " types.",
      //   showCloseButton: true,
      //   timeout: 2000
      // };
      // this.toasterService.pop(toast);
      // this.presentToast('warning', 'Valid file types are ' + validExts.toString());
      this.presentToast('warning', 'Please select a valid file type');

      // this.cancelFile();
    } else {
      if(size <= event.target.files[0].size){
        // var toast : Toast = {
        //     type: 'error',
        //     title: "File size exceeded!",
        //     body: "File size should be less than 100MB",
        //     showCloseButton: true,
        //     timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('warning', 'File size should be less than 100MB');
        // this.cancelFile();
      } else {
        if (event.target.files && event.target.files[0] ) {
          this.fileName = event.target.files[0].name;
          // this.selectFileTitle = this.fileName


          //read file from input
          // this.fileReaded = event.target.files[0];
          // this.read = event.target.files[0];

          // if(this.fileReaded != '' || this.fileReaded != null || this.fileReaded != undefined){
          //   this.enableUpload = true;
          // }
          let file = event.target.files[0];
          this.approveForm.assetRef = event.target.files[0].name;
          // this.assetrefLink ='';
          this.fileName = event.target.files[0].name;
          this.assetFileData = event.target.files[0];
          var fileUrl
          var reader = new FileReader();
          reader.onload = (event: ProgressEvent) => {
          this.url = (<FileReader>event.target).result;
          }
          reader.readAsDataURL(event.target.files[0]);
          reader.onload = (event) => {
            this.url = (<FileReader>event.target).result;
            this.approveForm.previewUrl = this.sanitizer.bypassSecurityTrustResourceUrl(
              reader.result as string
            );
          };
        }
        // if(this.approveForm.format == 1){
        //   this.url = (<FileReader>event.target).result;
        //   this.approveForm.previewUrl = this.url

        //   console.log(this.url,"hhhhh")
        // }
        event.target.value = ''
        // this.showPreview = true;
      this.isLocalfile = true;

        /// New Requirement

        /// end
      }
    }
  }
  showReasons: boolean;

  rejected() {
    this.showReasons = true;
  }

  back() {
    this.router.navigate(["../../asset"], { relativeTo: this.routes });
    // this.router.navigate(["../../folders"], { relativeTo: this.routes });

  }

  backToAsset(data){
    this.addassetservice.categoryId = data.assetId
    this.addassetservice.title = data.assetName
    this.addassetservice.breadCrumbArray = this.addassetservice.breadCrumbArray.slice(0,data.index)
    // this.title = this.addassetservice.breadCrumbArray[]
    this.addassetservice.previousBreadCrumb = this.addassetservice.previousBreadCrumb.slice(0,data.index+1)

    // this.router.navigate(['../'], { relativeTo: this.routes });
  }

  prepareApproveFormEmployee() {
    this.getAssetDataForApproval = this.approveassetservice.getAssetDataForApproval;
    console.log("getAssetDataForApproval", this.getAssetDataForApproval);

    this.CategoryName = this.getAssetDataForApproval.categoryName

    // const selectedEmployee: any = [];
    // let empObj = {
    //   empId: this.getAssetDataForApproval.author,
    //   empName: this.getAssetDataForApproval.authorName,
    //   empCode: this.getAssetDataForApproval.authorCode,
    // };
    // selectedEmployee.push(empObj);


    //new code
    if(this.getAssetDataForApproval.author){
    var author =this.getAssetDataForApproval.author.split(',');
    // var allEmployeeEdgeAutoDropObj ={}
    console.log(this.allEmployeeAutoDrop,"data author")
    // const selectedEdgeEmployee: any = [];
    var selectedEmployee: any = [];
    this.allEmployeeAutoDrop.forEach((emp) => {
      author.forEach((auth) =>{
      if (emp.empId == auth) {
        let allEmployeeEdgeAutoDropObj = {};
        allEmployeeEdgeAutoDropObj = {
          empId: emp.empId,
          empName: emp.empName,
          empCode : emp.empCode,
        }
        selectedEmployee.push(allEmployeeEdgeAutoDropObj);
      }
    });
    });
  }
    // end new code

    if (this.getAssetDataForApproval.estLength) {
      var resStr = this.getAssetDataForApproval.estLength.split(".");
      var formattedDuration = new Date();
      formattedDuration.setHours(resStr[0], resStr[1], resStr[2]);
    }

    if (
      this.getAssetDataForApproval.formatId != 4 ||
      this.getAssetDataForApproval.formatId != 6
    ) {
      if (this.getAssetDataForApproval.assetRef) {
        console.log(
          "fileNamexx",
          this.getAssetDataForApproval.assetRef.lastIndexOf("/") + 1
        );
        this.fileName = this.getAssetDataForApproval.assetRef.substr(
          this.getAssetDataForApproval.assetRef.lastIndexOf("/") + 1
        );
        this.selectFileTitle = this.fileName
        console.log("fileName", this.fileName);
        this.nativeElementConfigure();
      }
    }

    if (this.getAssetDataForApproval.formatId == 4) {
      //this.nativeElementConfigure();\
      setTimeout(() => {
        this.kpointConfigure(this.getAssetDataForApproval.assetRef);
      }, 500);
    }
    if (this.getAssetDataForApproval.formatId == 6) {
      this.youtubeurl = this.getAssetDataForApproval.assetRef;
      setTimeout(() => {
        this.youtubeconfigure(this.getAssetDataForApproval.assetRef);
      }, 500);
    }

    //let cTags = this.getAssetDataForApproval.tags.split(',');
    this.approveForm = {
      assetId: this.getAssetDataForApproval.assetId,
      assetName: this.getAssetDataForApproval.assetName,
      assetRef: this.getAssetDataForApproval.assetRef,
      bitlyLink: this.getAssetDataForApproval.bitlyLink,
      category: this.getAssetDataForApproval.categoryId,
      channel: this.getAssetDataForApproval.channelId,
      cost: this.getAssetDataForApproval.cost,
      description: this.getAssetDataForApproval.description,
      format: this.getAssetDataForApproval.formatId,
      language: this.getAssetDataForApproval.languageId,
      status: this.getAssetDataForApproval.status,
      assetTags:
        this.getAssetDataForApproval.tags == ""
          ? []
          : this.getAssetDataForApproval.tags,
      visible: this.getAssetDataForApproval.visible,
      selectedEmployee: selectedEmployee,
      aVId: this.getAssetDataForApproval.aVId,
      aAId: this.getAssetDataForApproval.aAId,
      time: formattedDuration,
      mimeType: this.getAssetDataForApproval.mimeType,
      referenceType: this.getAssetDataForApproval.referenceType,
    };
    if (this.getAssetDataForApproval.tagIds) {
      var tagIds = this.getAssetDataForApproval.tagIds.split(",");
      if (tagIds.length > 0) {
        this.tempTags.forEach((tag) => {
          tagIds.forEach((tagId) => {
            if (tag.id == tagId) {
              this.selectedTags.push(tag);
            }
          });
        });
      }
    }
    if(this.formats){
    this.assignValueToSelectDropDown(this.getAssetDataForApproval.formatId);
    }
    this.show = true;
    this.cdf.detectChanges();
    console.log("this.approveForm", this.approveForm);

    // if (this.getAssetDataForApproval.formatId == 6) {
    //   this.youtubeurl = this.getAssetDataForApproval.assetRef;
    //    this.youtubeconfigure( this.youtubeurl);
    // }

    if (this.getAssetDataForApproval.formatId == 3) {
      // Commented by Bhavesh Tandel
      // this.approveForm.reference = this.transform(
      //   this.getDocumentUrl(this.approveForm.assetRef)
      // );
      console.log("this.approveForm", this.approveForm.assetRef);
      this.approveassetservice
        .getPdfLink(this.approveForm.assetRef)
        .then((result: any) => {
          this.pdfserverlink = result.data;
          this.approveForm.reference = this.sanitizer.bypassSecurityTrustResourceUrl(
            this.convertURL(this.pdfserverlink)
          );
          if (result.data != null) {
            this.pdfUrlFlag = true;
          }
          console.log("Link ===>", result);
        })
        .catch((result) => {
          console.log("RESULT===>", result);
        });

      // this.approveForm.reference = this.transform(
      //   this.convertURL(this.approveForm.assetRef)
      // );
      // this.transform(this.addEditAssetForm.assetRef);
      console.log("PDF file", this.approveForm.reference);
      this.cdf.detectChanges();
    }

    // if(this.getAssetDataForApproval.formatId == 3){
    //   this.getAssetDataForApproval.assetRef = this.sanitizer.bypassSecurityTrustResourceUrl(this.getAssetDataForApproval.assetRef);
    // }
    this.nativeElementConfigure();
    // if (this.getAssetDataForApproval.formatId == 4) {
    //   //this.nativeElementConfigure();\
    //   setTimeout(() => {
    //     this.kpointConfigure(this.getAssetDataForApproval.assetRef);
    //   }, 500);
    // }
    // if (this.getAssetDataForApproval.formatId == 6) {
    //   this.youtubeurl = this.getAssetDataForApproval.assetRef;
    //   setTimeout(() => {
    //     this.youtubeconfigure(this.getAssetDataForApproval.assetRef);
    //   }, 500);
    // }
  }

  encodeURI(url) {
    if (UriBuilder.parse(url).isRelative()) {
      url = cleanPath(document.baseURI + "/" + url);
    }
    return encodeURIComponent(url);
  }

  getDocumentUrl(url) {
    return (
      "https://docs.google.com/gview?url=" + encodeURI(url) + "&embedded=true"
    );
  }
  transform(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
  convertURL(url) {
    return webApi.domain + webApi.url.pdfviewerurl + url;
  }

  getAllAssetDropdown() {
    this.spinner.show();
    let param = {
      tId: this.tenantId,
    };
    const _urlGetAllDropdown: string =
      webApi.domain + webApi.url.getAllAssetDropDown;
    this.commonFunctionService
      .httpPostRequest(_urlGetAllDropdown, param)
      // this.approveassetservice.getAllAssetDropdown(param)
      .then(
        (rescompData) => {
          this.spinner.hide();
          var result = rescompData;
          console.log("All Asset Dropdown1", result);
          if (result["type"] == true) {
            console.log("All Asset Dropdown2", result);
            this.status = result["data"][0];
            this.languages = result["data"][1];
            this.formats = result["data"][2];
            this.assignValueToSelectDropDown(this.getAssetDataForApproval.formatId)
            this.channels = result["data"][3];
            this.categories = result["data"][4];
            // console.log(
            //   this.status,
            //   this.languages,
            //   this.formats,
            //   this.channels,
            //   this.categories
            // );
          } else {
            // var toast: Toast = {
            //   type: 'error',
            //   //title: 'Server Error!',
            //   body: 'Something went wrong.please try again later.',
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(toast);
            this.presentToast("error", "");
          }
        },
        (resUserError) => {
          // this.loader =false;
          // var toast: Toast = {
          //   type: 'error',
          //   //title: 'Server Error!',
          //   body: 'Something went wrong.please try again later.',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast("error", "");
        }
      );
  }

  selectedCreator: any;
  usersList: any = [];
  allEmployeeAutoDrop: any = [];

  prepareEmployeeDropdown() {
    this.allEmployeesAuto.forEach((element) => {
      let allEmployeeAutoDropObj = {};
      allEmployeeAutoDropObj = {
        empId: element.id,
        empName: element.name,
        empCode:element.code
      };
      this.allEmployeeAutoDrop.push(allEmployeeAutoDropObj);
    });
    console.log("this.allEmployeeAutoDrop", this.allEmployeeAutoDrop);
  }

  onCreatorSelect(item: any) {
    console.log(item);
    if(item.empId == 0){
      this.settingsEmployeeDrop = {
        text: "Search & Select Employee",
        singleSelection: false,
        classes: "myclass custom-class",
        primaryKey: "empId",
        labelKey: "empName",
        noDataLabel: "Search Employee...",
        enableSearchFilter: true,
        limitSelection:true,
        searchBy: ["empId", "empName"],
        maxHeight: 250,
        lazyLoading: true,
        disabled: true
      };
      var dummy = []
      dummy.push(item)
      this.approveForm.selectedEmployee = dummy
    }
    console.log(this.selectedCreator);
  }

  OnCreatorDeSelect(item: any) {
    console.log(item);
    if(item.empId == 0){
      this.settingsEmployeeDrop = {
        text: "Search & Select Employee",
        singleSelection: false,
        classes: "myclass custom-class",
        primaryKey: "empId",
        labelKey: "empName",
        noDataLabel: "Search Employee...",
        enableSearchFilter: true,
        limitSelection:this.allEmployeeAutoDrop.length,
        searchBy: ["empId", "empName"],
        maxHeight: 250,
        lazyLoading: true,
        disabled: true
      };
    }
    console.log(this.selectedCreator);
  }

  onCreatorSearch(evt: any) {
    console.log(evt.target.value);
    const val = evt.target.value;
    this.allEmployeeAutoDrop = [];
    this.getAllEdgeEmployees(val);
    this.usersList = [];
    // const tuser = this.allEmployeeAutoDrop.filter(function(d) {
    //   return (
    //     String(d.empId)
    //       .toLowerCase()
    //       .indexOf(val) !== -1 ||
    //     d.empName.toLowerCase().indexOf(val) !== -1 ||
    //     !val
    //   );
    // });

    // update the rows
    //this.usersList = tuser;
  }

  noEmployees: boolean = false;
  allEmployeesAuto: any = [];
  getAllEdgeEmployees(str) {
    // if (str.length > 3) {
      this.spinner.show();
      let param = {
        srcStr: str,
        tId: this.tenantId,
      };
      const _urlGetAllEdgeEmployees: string =
        webApi.domain + webApi.url.getAllEdgeEmployee;
      this.commonFunctionService
        .httpPostRequest(_urlGetAllEdgeEmployees, param)
        // this.approveassetservice.getAllEmployee(param)
        .then(
          (rescompData) => {
            this.spinner.hide();
            var result = rescompData;
            console.log("getAllEmployessDAM:", rescompData);
            if (result["type"] == true) {
              if (result["data"][0].length == 0) {
                this.noEmployees = true;
              } else {
                this.allEmployeesAuto = result["data"][0];
                console.log("this.allEmployeesAuto", this.allEmployeesAuto);
                this.prepareEmployeeDropdown();
                this.prepareApproveFormEmployee();
                this.nativeElementConfigure();
              }
            } else {
              // var toast: Toast = {
              //   type: 'error',
              //   //title: 'Server Error!',
              //   body: 'Something went wrong.please try again later.',
              //   showCloseButton: true,
              //   timeout: 2000
              // };
              // this.toasterService.pop(toast);
              this.presentToast("error", "");
            }
          },
          (error) => {
            this.spinner.hide();
            // var toast: Toast = {
            //   type: 'error',
            //   //title: 'Server Error!',
            //   body: 'Something went wrong.please try again later.',
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(toast);
            this.presentToast("error", "");
          }
        );
    // }
  }

  formatTime(time) {
    let duration = new Date(time);
    let formatted = this.datePipe.transform(duration, "H:mm:ss");
    console.log("formatted", formatted);
    let str = new String(formatted);
    let formattedFinal = str.replace(/:/g, ".");
    console.log("formattedFinal", formattedFinal);
    return formattedFinal;
  }
  checkApproveStatus(data){
    this.spinner.show();
    var param={
      assetId:data.assetId,
    }
    console.log("param22", param);
    const _checkApproveStatus: string =
      webApi.domain + webApi.url.checkApproveStatus;
    this.commonFunctionService
      .httpPostRequest(_checkApproveStatus, param)
      .then(
        (rescompData) => {
          this.spinner.hide();
          var result = rescompData;
          console.log("updateAssetStatus:", rescompData);
          if (result["type"] == true) {
            if(result['data'][0][0].isMapped == 1){
              this.checkstatus=true
             this.popupMsg= result['data'][0][0].msg
            }
            else{
              this.checkstatus=false
              this.assetStatus = 1;
              this.showReasons = false;
              this.doNextProcess(data)
            }
          } else {
            this.presentToast("error", "");
          }
        },
        (error) => {
          this.spinner.hide();
          this.presentToast("error", "");
        }
      );
  }
  closeApproveStatus(flag){
    if(flag){
      this.router.navigate(["../../asset"], { relativeTo: this.routes });
    }else {
      this.checkstatus=false
      this.showReasons = false;
    }

  }

  passParams: any;
  fileUploadRes: any;
  fileres: any;
  errorMsg: any;
  fileExistModal: boolean = false;
  dupFileError: string;
  clickedData = null;
  updateAssetStatus(data, approve: NgForm) {
    this.submitted = true;
    let valid = false;
    // && this.selectedTags.length > 0
    if (approve.form.valid ) {
      valid = true;
    } else {
      // valid = approve.valid;
      valid = false;
      this.valid = false
      this.presentToast("warning","Please enter a valid reason for rejection.")
    }
    if (valid) {
      if(this.assetStatus == 1){
        this.checkApproveStatus(data);
        this.clickedData = data;
      }else {
        this.doNextProcess(data);
      }
    } else {

      return;
    }
  }

  doNextProcess(data){
    if (data.assetRef) {
      // this.spinner.show();
      console.log("data.", data);
      // var tagStr = this.getTagDataReady(data.assetTags);
      // console.log('tagStr', tagStr);

      let mimeType = null;
      let referenceType = null;
      if (data.assetRef) {
        const document = this.getMimeType(data.assetRef);
        if (
          data.assetRef.includes("kapsule") ||
          data.assetRef.includes("kpoint")
        ) {
          mimeType = "embedded/kpoint";
          referenceType = "kpoint";
        } else if (data.assetRef.includes("youtube")) {
          mimeType = "embedded/youtube";
          referenceType = "youtube";
        } else if (document) {
          mimeType = document["mimetype"];
          referenceType = document["documenttype"];
        } else if (
          data.assetRef.includes("http") ||
          data.assetRef.includes("https")
        ) {
          mimeType = "application/x-msdownload";
          referenceType = "application";
        }
      }
      if (this.selectedTags.length > 0) {
        this.makeTagDataReady(this.selectedTags);
        // this.formdata.tags = this.formattedTags;
      }
      if(data.selectedEmployee){
        this.makeAuthorDataReady(data.selectedEmployee)
      }
      let param = {
        assetId: data.assetId,
        assetStatus: this.assetStatus,
        rejectionReason: data.rejectionReason ? data.rejectionReason : null,
        tId: this.tenantId,
        reviewerId: data.selectedEmployee[0].empId,
        userId: this.userId,
        assetApprovalId: data.aAId,
        assetVersionId: data.aVId,
        assetName: data.assetName,
        assetRef: data.assetRef ? data.assetRef : null,
        bitlyLink: null,
        assetDesc: data.description?data.description:null,
        // assetAuthor: data.selectedEmployee[0].empId,
        assetAuthor: this.author,
        assetCat: data.category,
        assetFormat: data.format,
        assetLen: data.time
          ? data.format == 3 ||
            data.format == 5 ||
            data.format == 7 ||
            data.format == 8
            ? null
            : this.formatTime(data.time)
          : null,
        assetChannel: data.channel,
        assetLang: data.language,
        assetCost: data.cost,
        assetTags: this.getAssetDataForApproval.tags1,
        mimeType: mimeType || data.mimeType,
        referenceType: referenceType || data.referenceType,
      };
      console.log("param", param);
      this.passParams = param;

      if (
        data.format == "4" ||
        data.format == "6" ||
        data.format == "8" ||
        !this.isLocalfile
      ) {
        if (data.format == "4") {
          this.passParams.mimeType = mimeType || "embedded/kpoint";
          this.passParams.referenceType = referenceType || "kpoint";
        }

        if (data.format == "6") {
          this.passParams.mimeType = mimeType || "embedded/youtube";
          this.passParams.referenceType = referenceType || "youtube";
        }

        if (data.format == "8") {
          this.passParams.mimeType = mimeType || "application/x-msdownload";
          this.passParams.referenceType = referenceType || "application";
        }
        this.approveAsset(this.passParams);
      } else {
        var fd = new FormData();
        fd.append("content", JSON.stringify(this.passParams));
        fd.append("file", this.assetFileData);
        console.log("File Data ", fd);

        console.log("Asset File", this.assetFileData);
        console.log("Asset Data ", this.passParams);
        let fileUploadUrl = webApi.domain + webApi.url.fileUpload;

        if (this.assetFileData) {
          this.webApiService.getService(fileUploadUrl, fd).then(
            (rescompData) => {
              var temp: any = rescompData;
              this.fileUploadRes = temp;
              console.log("rescompData", this.fileUploadRes);
              this.assetFileData = null;
              if (temp == "err") {
                // this.notFound = true;
                // var thumbUpload: Toast = {
                //   type: 'error',
                //   title: 'Asset File',
                //   body: 'Unable to upload Asset File.',
                //   // body: temp.msg,
                //   showCloseButton: true,
                //   timeout: 2000
                // };
                // this.toasterService.pop(thumbUpload);
                this.presentToast("error", "");
                this.spinner.hide();
              } else if (temp.type == false) {
                // var thumbUpload: Toast = {
                //   type: 'error',
                //   title: 'Asset File',
                //   body: 'Unable to upload Asset File.',
                //   // body: temp.msg,
                //   showCloseButton: true,
                //   timeout: 2000
                // };
                // this.toasterService.pop(thumbUpload);
                this.presentToast("error", "");
                this.spinner.hide();
              } else {
                if (
                  this.fileUploadRes.data != null ||
                  this.fileUploadRes.fileError != true
                ) {
                  this.assetFileData = null;
                  if (this.fileUploadRes.fileExists) {
                    this.fileExistModal = true;
                    this.dupFileError = this.fileUploadRes.status;
                  } else {
                    this.passParams.assetRef = this.fileUploadRes.data.file_url;
                    this.passParams.mimeType = this.fileUploadRes.data.mime_type;
                    this.passParams.referenceType = this.fileUploadRes.data.file_type;
                    console.log("this.passparams", this.passParams);
                    this.approveAsset(this.passParams);
                  }
                  this.spinner.hide();
                  // this.approveAsset(this.passParams);
                } else {
                  // var thumbUpload: Toast = {
                  //   type: 'error',
                  //   title: 'Asset File',
                  //   // body: 'Unable to upload course thumbnail.',
                  //   body: this.fileUploadRes.status,
                  //   showCloseButton: true,
                  //   timeout: 2000
                  // };
                  // this.toasterService.pop(thumbUpload);
                  this.presentToast("error", "");
                  this.spinner.hide();
                }
              }
              console.log("File Upload Result", this.fileUploadRes);
              var res = this.fileUploadRes;
              this.fileres = res.data.file_url;
            },
            (resUserError) => {
              this.errorMsg = resUserError;
              console.log("File upload this.errorMsg", this.errorMsg);
              this.spinner.hide();
            }
          );
        } else {
          this.spinner.hide();
          this.approveAsset(this.passParams);
        }
      }
    } else {
      this.spinner.hide();
      // let fileselect: Toast = {
      //   type: 'error',
      //   title: 'File!',
      //   body: 'Please select file.',
      //   showCloseButton: true,
      //   timeout: 2000
      // };
      // this.toasterService.pop(fileselect);
      this.presentToast("warning", "Please select a file");
      console.log("please select file");
    }
  }

  fileExistModalSubmit(value) {
    if (value === 1) {
      this.passParams.assetRef = this.fileUploadRes.data.docs[0].file_url;
      console.log("this.passParams.assetRef", this.passParams.assetRef);
      this.approveAsset(this.passParams);
    } else {
      this.passParams.assetRef = null;
      this.approveForm.assetRef = null;
      this.isLocalfile = false;
      this.fileName = "You can drag and drop files here to add them.";
      this.assetFileData = null;
    }
    this.fileExistModal = false;
  }

  approveAsset(param) {
    this.spinner.show();
    console.log("param22", param);
    const _urlUpdateAssetStatus: string =
      webApi.domain + webApi.url.approveRejectAsset;
    this.commonFunctionService
      .httpPostRequest(_urlUpdateAssetStatus, param)
      // this.approveassetservice.updateAssetStatus(param)
      .then(
        (rescompData) => {
          this.spinner.hide();
          var result = rescompData;
          console.log("updateAssetStatus:", rescompData);
          if (result["type"] == true) {
            // var toast: Toast = {
            //   type: 'success',
            //   title: 'Asset Update',
            //   body: 'Asset updated successfully.',
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(toast);
            if(this.assetStatus == 1){
            this.presentToast("success", "File Approved successfully");
            }else{
              this.presentToast("success", "File Rejected successfully");
            }
            this.router.navigate(["../../asset"], { relativeTo: this.routes });

              this.approveassetservice.assetStatus = this.assetStatus



          } else {
            // var toast: Toast = {
            //   type: 'error',
            //   //title: 'Server Error!',
            //   body: 'Something went wrong.please try again later.',
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(toast);
            this.presentToast("error", "");
          }
        },
        (error) => {
          this.spinner.hide();
          // var toast: Toast = {
          //   type: 'error',
          //   //title: 'Server Error!',
          //   body: 'Something went wrong.please try again later.',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast("error", "");
        }
      );
  }

  getTagDataReady(customTags) {
    if (customTags.length > 0) {
      var tagsString = "";
      for (let i = 0; i < customTags.length; i++) {
        var tag = customTags[i];
        if (tagsString != "") {
          tagsString += "|";
        }
        if (tag.value) {
          if (String(tag.value) != "" && String(tag.value) != "null") {
            tagsString += tag.value;
          }
        } else {
          if (String(tag) != "" && String(tag) != "null") {
            tagsString += tag;
          }
        }
      }
      return tagsString;
    }
    // else if(customTags.length == 0){
    //   return this.assetForm.invalid;
    // }
  }

  assetStatus: any = 1;
  approveReject(value) {
    if (value == true) {
      this.assetStatus = 1;
      this.showReasons = false;
      this.checkstatus=false
    } else {
      this.assetStatus = 2;
      this.showReasons = true;
    }
  }

  pdfModal: boolean = false;
  viewPdf() {
    this.pdfModal = true;
  }
  closePdfModal() {
    this.pdfModal = false;
  }
  transformUrl(url): any {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  formatChange() {
    console.log(this.approveForm.time,"time")
    this.approveForm.assetRef = ''
    this.approveForm.previewUrl = ''
    this.fileName = "You can drag and drop files here to add them.";
    this.nativeElementConfigure();
    this.assignValueToSelectDropDown(this.approveForm.format);
  }

  presentToast(type, body) {
    if (type === "success") {
      this.toastr.success(body, "Success", {
        closeButton: false,
      });
    } else if (type === "error") {
      this.toastr.error(
        'Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.',
        "Error",
        {
          timeOut: 0,
          closeButton: true,
        }
      );
    } else {
      this.toastr.warning(body, "Warning", {
        closeButton: false,
      });
    }
  }
  ngAfterViewInit() {}

  // nativeElementConfigure() {
  //   setTimeout(() => {
  //     if (this.imagefile) {
  //       this.render.listen(this.imagefile.nativeElement, "change", () => {
  //         this.isLocalfile = true;
  //         var size = 1000000;
  //         // var validExts = new Array("image");
  //         var validExts = this.formatExtensions
  //         this.files = this.imagefile.nativeElement.files;
  //         // var fileType = this.files[0].type;
  //         var fileType = this.files[0].name.split('.')[1];
  //         console.log("fileType", fileType);
  //         var fileExt = false;
  //         for (let i = 0; i < validExts.length; i++) {
  //           if (fileType.includes(validExts[i].extension)) {
  //             fileExt = true;
  //             break;
  //           }
  //         }
  //         if (!fileExt) {
  //           // let type = 'error';
  //           // let title = 'Invalid file selected!';
  //           // let body = 'Valid files are of image types.';
  //           this.presentToast("warning", "Please select a file of type image");
  //           this.removeFile();
  //         } else if (size <= this.files[0].size) {
  //           // let type = 'error';
  //           // let title = 'File size exceeded!';
  //           // let body = 'File size should be less than 100 MB';
  //           this.presentToast(
  //             "warning",
  //             "File size should be less than 100 MB"
  //           );
  //           this.removeFile();
  //         } else {
  //           try {
  //             console.log("image", this.files[0]);
  //             this.fileName = this.files[0].name;
  //             this.assetFileData = this.files[0];
  //             this.selectFileTitle = this.files[0].name;
  //             console.log("this.fileName", this.fileName);
  //             console.log("this.assetFileData", this.assetFileData);
  //             setTimeout(() => {
  //               let $source: any = $("#image_here");
  //               $source[0].src = URL.createObjectURL(this.files[0]);
  //               // $source.parent()[0].load();
  //             }, 200);
  //           } catch (e) {
  //             console.log(e);
  //           }
  //         }
  //       });
  //     }
  //     // $(document).on('change','#videofile',function(evt){
  //     if (this.videofile) {
  //       this.render.listen(this.videofile.nativeElement, "change", () => {
  //         this.isLocalfile = true;
  //         var size = 1000000;
  //         var validExts = new Array("video");
  //         this.files = this.videofile.nativeElement.files;
  //         var fileType = this.files[0].type;
  //         console.log("fileType", fileType);
  //         var fileExt = false;
  //         for (let i = 0; i < validExts.length; i++) {
  //           if (fileType.includes(validExts[i])) {
  //             fileExt = true;
  //             break;
  //           }
  //         }
  //         if (!fileExt) {
  //           // let type = 'error';
  //           // let title = 'Invalid file selected!';
  //           // let body = 'Valid files are of video types.';
  //           this.presentToast("warning", "Please select a file of type video");
  //           this.removeFile();
  //         } else if (size <= this.files[0].size) {
  //           // let type = 'error';
  //           // let title = 'File size exceeded!';
  //           // let body = 'File size should be less than 100 MB';
  //           this.presentToast(
  //             "warning",
  //             "File size should be less than 100 MB"
  //           );
  //           this.removeFile();
  //         } else {
  //           try {
  //             console.log("Video", this.files[0]);
  //             this.fileName = this.files[0].name;
  //             this.selectFileTitle = this.files[0].name;
  //             this.assetFileData = this.files[0];
  //             console.log("this.fileName", this.fileName);
  //             console.log("this.assetFileData", this.assetFileData);
  //             setTimeout(() => {
  //               let $source: any = $("#video_here");
  //               $source[0].src = URL.createObjectURL(this.files[0]);
  //               $source.parent()[0].load();
  //             }, 200);
  //           } catch (e) {
  //             console.log(e);
  //           }
  //         }
  //       });
  //     }
  //     ////////////////Code for fetch audio file//////////////

  //     // $(document).on('change','#audiofile',function(evt){
  //     if (this.audiofile) {
  //       this.render.listen(this.audiofile.nativeElement, "change", () => {
  //         this.isLocalfile = true;
  //         var size = 1000000;
  //         this.files = this.audiofile.nativeElement.files;
  //         var validExts = new Array("audio");
  //         var fileType = this.files[0].type;
  //         console.log("fileType", fileType);
  //         var fileExt = false;
  //         for (let i = 0; i < validExts.length; i++) {
  //           if (fileType.includes(validExts[i])) {
  //             fileExt = true;
  //             break;
  //           }
  //         }
  //         if (!fileExt) {
  //           // let type = 'error';
  //           // let title = 'Invalid file selected!';
  //           // let body = 'Valid files are of audio types.';
  //           this.presentToast("warning", "Please select a file of type audio");
  //           this.removeFile();
  //         } else if (size <= this.files[0].size) {
  //           // let type = 'error';
  //           // let title = 'File size exceeded!';
  //           // let body = 'File size should be less than 100 MB';
  //           this.presentToast(
  //             "warning",
  //             "File size should be less than 100 MB"
  //           );
  //           this.removeFile();
  //         } else {
  //           try {
  //             console.log("audio", this.files[0]);
  //             this.fileName = this.files[0].name;
  //             this.assetFileData = this.files[0];
  //             this.selectFileTitle = this.files[0].name;
  //             console.log("this.fileName", this.fileName);
  //             console.log("this.assetFileData", this.assetFileData);
  //             setTimeout(() => {
  //               let $source: any = $("#audio_here");
  //               $source[0].src = URL.createObjectURL(this.files[0]);
  //               $source.parent()[0].load();
  //             }, 200);
  //           } catch (e) {
  //             console.log(e);
  //           }
  //         }
  //       });
  //     }
  //     ///////////////////audio file End///////////////////////

  //     // $(document).on('change','#iframefile',function(evt){
  //     if (this.iframe) {
  //       this.render.listen(this.iframe.nativeElement, "change", () => {
  //         this.isLocalfile = true;
  //         var size = 1000000;
  //         this.files = this.iframe.nativeElement.files;
  //         var validExts = new Array("pdf");
  //         var fileType = this.files[0].type;
  //         console.log("fileType", fileType);
  //         var fileExt = false;
  //         for (let i = 0; i < validExts.length; i++) {
  //           if (fileType.includes(validExts[i])) {
  //             fileExt = true;
  //             break;
  //           }
  //         }
  //         if (!fileExt) {
  //           // let type = 'error';
  //           // let title = 'Invalid file selected!';
  //           // let body = 'Valid files are of pdf types.';
  //           this.presentToast("warning", "Please select a file of type pdf");
  //           this.removeFile();
  //         } else if (size <= this.files[0].size) {
  //           // let type = 'error';
  //           // let title = 'File size exceeded!';
  //           // let body = 'File size should be less than 100 MB';
  //           this.presentToast(
  //             "warning",
  //             "File size should be less than 100 MB"
  //           );
  //           this.removeFile();
  //         } else {
  //           try {
  //             console.log("pdf", this.files[0]);
  //             this.fileName = this.files[0].name;
  //             this.assetFileData = this.files[0];
  //             this.selectFileTitle = this.files[0].name;
  //             console.log("this.fileName", this.fileName);
  //             console.log("this.assetFileData", this.assetFileData);
  //             setTimeout(() => {
  //               let $source: any = $("#iframe");
  //               $source[0].src = URL.createObjectURL(this.files[0]);
  //               // $source.parent()[0].load();
  //             }, 200);
  //           } catch (e) {
  //             console.log(e);
  //           }
  //         }
  //       });
  //     }

  //     if (this.ppt) {
  //       this.render.listen(this.ppt.nativeElement, "change", () => {
  //         this.isLocalfile = true;
  //         var size = 1000000;
  //         this.files = this.ppt.nativeElement.files;
  //         var validExts = new Array(
  //           "application/vnd.ms-powerpoint",
  //           "application/vnd.openxmlformats-officedocument.presentationml.presentation"
  //         );
  //         var fileType = this.files[0].type;
  //         console.log("fileType", fileType);
  //         var fileExt = false;
  //         for (let i = 0; i < validExts.length; i++) {
  //           if (fileType.includes(validExts[i])) {
  //             fileExt = true;
  //             break;
  //           }
  //         }
  //         if (!fileExt) {
  //           // let type = 'error';
  //           // let title = 'Invalid file selected!';
  //           // let body = 'Valid files are of pdf types.';
  //           this.presentToast("warning", "Please select a file of type ppt");
  //           this.removeFile();
  //         } else if (size <= this.files[0].size) {
  //           // let type = 'error';
  //           // let title = 'File size exceeded!';
  //           // let body = 'File size should be less than 100 MB';
  //           this.presentToast(
  //             "warning",
  //             "File size should be less than 100 MB"
  //           );
  //           this.removeFile();
  //         } else {
  //           try {
  //             console.log("pdf", this.files[0]);
  //             this.fileName = this.files[0].name;
  //             this.assetFileData = this.files[0];
  //             this.selectFileTitle = this.files[0].name;
  //             console.log("this.fileName", this.fileName);
  //             console.log("this.assetFileData", this.assetFileData);
  //             // setTimeout(() => {
  //             //   let $source: any = $('#iframe');
  //             //   $source[0].src = URL.createObjectURL(this.files[0]);
  //             //   // $source.parent()[0].load();
  //             // }, 200);
  //           } catch (e) {
  //             console.log(e);
  //           }
  //         }
  //       });
  //     }

  //     if (this.word) {
  //       this.render.listen(this.word.nativeElement, "change", () => {
  //         this.isLocalfile = true;
  //         var size = 1000000;
  //         this.files = this.word.nativeElement.files;
  //         var validExts = new Array(
  //           "application/msword",
  //           "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
  //         );
  //         var fileType = this.files[0].type;
  //         console.log("fileType", fileType);
  //         var fileExt = false;
  //         for (let i = 0; i < validExts.length; i++) {
  //           if (fileType.includes(validExts[i])) {
  //             fileExt = true;
  //             break;
  //           }
  //         }
  //         if (!fileExt) {
  //           // let type = 'error';
  //           // let title = 'Invalid file selected!';
  //           // let body = 'Valid files are of pdf types.';
  //           this.presentToast("warning", "Please select a file ");
  //           this.removeFile();
  //         } else if (size <= this.files[0].size) {
  //           // let type = 'error';
  //           // let title = 'File size exceeded!';
  //           // let body = 'File size should be less than 100 MB';
  //           this.presentToast(
  //             "warning",
  //             "File size should be less than 100 MB"
  //           );
  //           this.removeFile();
  //         } else {
  //           try {
  //             console.log("pdf", this.files[0]);
  //             this.fileName = this.files[0].name;
  //             this.assetFileData = this.files[0];
  //             this.selectFileTitle = this.files[0].name;
  //             console.log("this.fileName", this.fileName);
  //             console.log("this.assetFileData", this.assetFileData);
  //             // setTimeout(() => {
  //             //   let $source: any = $('#iframe');
  //             //   $source[0].src = URL.createObjectURL(this.files[0]);
  //             //   // $source.parent()[0].load();
  //             // }, 200);
  //           } catch (e) {
  //             console.log(e);
  //           }
  //         }
  //       });
  //     }

  //     if (this.excel) {
  //       this.render.listen(this.excel.nativeElement, "change", () => {
  //         this.isLocalfile = true;
  //         var size = 1000000;
  //         this.files = this.excel.nativeElement.files;
  //         var validExts = new Array(
  //           "application/vnd.ms-excel",
  //           "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  //         );
  //         var fileType = this.files[0].type;
  //         console.log("fileType", fileType);
  //         var fileExt = false;
  //         for (let i = 0; i < validExts.length; i++) {
  //           if (fileType.includes(validExts[i])) {
  //             fileExt = true;
  //             break;
  //           }
  //         }
  //         if (!fileExt) {
  //           // let type = 'error';
  //           // let title = 'Invalid file selected!';
  //           // let body = 'Valid files are of pdf types.';
  //           this.presentToast("warning", "Please select a file ");
  //           this.removeFile();
  //         } else if (size <= this.files[0].size) {
  //           // let type = 'error';
  //           // let title = 'File size exceeded!';
  //           // let body = 'File size should be less than 100 MB';
  //           this.presentToast(
  //             "warning",
  //             "File size should be less than 100 MB"
  //           );
  //           this.removeFile();
  //         } else {
  //           try {
  //             console.log("pdf", this.files[0]);
  //             this.fileName = this.files[0].name;
  //             this.assetFileData = this.files[0];
  //             this.selectFileTitle = this.files[0].name;
  //             console.log("this.fileName", this.fileName);
  //             console.log("this.assetFileData", this.assetFileData);
  //             // setTimeout(() => {
  //             //   let $source: any = $('#iframe');
  //             //   $source[0].src = URL.createObjectURL(this.files[0]);
  //             //   // $source.parent()[0].load();
  //             // }, 200);
  //           } catch (e) {
  //             console.log(e);
  //           }
  //         }
  //       });
  //     }

  //     if (this.scormfile) {
  //       this.render.listen(this.scormfile.nativeElement, "change", () => {
  //         this.isLocalfile = true;
  //         var size = 50000000;
  //         this.files = this.scormfile.nativeElement.files;
  //         var validExts = new Array("zip");
  //         var fileType = this.files[0].type;
  //         console.log("fileType", fileType);
  //         var fileExt = false;
  //         for (let i = 0; i < validExts.length; i++) {
  //           if (fileType.includes(validExts[i])) {
  //             fileExt = true;
  //             break;
  //           }
  //         }
  //         if (!fileExt) {
  //           // let type = 'error';
  //           // let title = 'Invalid file selected!';
  //           // let body = 'Valid files are of zip types.';
  //           this.presentToast("warning", "Please select a zip file");
  //           this.removeFile();
  //         } else if (size <= this.files[0].size) {
  //           // let type = 'error';
  //           // let title = 'File size exceeded!';
  //           // let body = 'File size should be less than 100 MB';
  //           this.presentToast("warning", "File size should be less than 50 MB");
  //           this.removeFile();
  //         } else {
  //           try {
  //             console.log("zip", this.files[0]);
  //             this.fileName = this.files[0].name;
  //             this.assetFileData = this.files[0];
  //             this.selectFileTitle = this.files[0].name;
  //             console.log("this.fileName", this.fileName);
  //             console.log("this.assetFileData", this.assetFileData);
  //             //this.pdfUpload = true;
  //             // setTimeout( () => {
  //             //   let $source: any = $('#iframe');
  //             //   $source[0].src = URL.createObjectURL(this.files[0]);
  //             //   $source.parent()[0].load();
  //             // }, 200)
  //           } catch (e) {
  //             console.log(e);
  //           }
  //         }
  //       });
  //     }
  //   }, 500);
  // }

  nativeElementConfigure() {
    var validExts = this.formatExtensions
    setTimeout(() => {
      if (this.imagefile) {
        this.render.listen(this.imagefile.nativeElement, "change", () => {
          this.isLocalfile = true;
          var size = 1000000;
          // var validExts = new Array("image");
          this.files = this.imagefile.nativeElement.files;
          // var fileType = this.files[0].type;
          // var fileType = this.files[0].name.split('.')[1];
          var last_dot = this.files[0].name.lastIndexOf('.')
          var fileType = this.files[0].name.slice(last_dot + 1)
          console.log("fileType", fileType);
          var fileExt = false;
          for (let i = 0; i < validExts.length; i++) {
            if(validExts[i].formatId == 7){
            if (fileType.includes(validExts[i].extension)) {
              fileExt = true;
              break;
            }
          }
          }
          if (!fileExt) {
            // let type = 'error';
            // let title = 'Invalid file selected!';
            // let body = 'Valid files are of image types.';
            this.presentToast("warning", "Please select a file of type image");
            this.removeFile();
          } else if (size <= this.files[0].size) {
            // let type = 'error';
            // let title = 'File size exceeded!';
            // let body = 'File size should be less than 100 MB';
            this.presentToast(
              "warning",
              "File size should be less than 100 MB"
            );
            this.removeFile();
          } else {
            try {
              console.log("image", this.files[0]);
              this.fileName = this.files[0].name;
              this.assetFileData = this.files[0];
              this.selectFileTitle = this.files[0].name;
              console.log("this.fileName", this.fileName);
              console.log("this.assetFileData", this.assetFileData);
              setTimeout(() => {
                let $source: any = $("#image_here");
                $source[0].src = URL.createObjectURL(this.files[0]);
                // $source.parent()[0].load();
              }, 200);
            } catch (e) {
              console.log(e);
            }
          }
        });
      }
      // $(document).on('change','#videofile',function(evt){
      if (this.videofile) {
        this.render.listen(this.videofile.nativeElement, "change", () => {
          this.isLocalfile = true;
          var size = 1000000;
          // var validExts = new Array("video");
          this.files = this.videofile.nativeElement.files;
          // var fileType = this.files[0].type;
          // var fileType = this.files[0].name.split('.')[1];
          var last_dot = this.files[0].name.lastIndexOf('.')
          var fileType = this.files[0].name.slice(last_dot + 1)
          console.log("fileType", fileType);
          var fileExt = false;
          for (let i = 0; i < validExts.length; i++) {
            if(validExts[i].formatId == 1){
            if (fileType.includes(validExts[i].extension)) {
              fileExt = true;
              break;
            }
          }
          }
          if (!fileExt) {
            // let type = 'error';
            // let title = 'Invalid file selected!';
            // let body = 'Valid files are of video types.';
            this.presentToast("warning", "Please select a file of type video");
            this.removeFile();
          } else if (size <= this.files[0].size) {
            // let type = 'error';
            // let title = 'File size exceeded!';
            // let body = 'File size should be less than 100 MB';
            this.presentToast(
              "warning",
              "File size should be less than 100 MB"
            );
            this.removeFile();
          } else {
            try {
              console.log("Video", this.files[0]);
              this.fileName = this.files[0].name;
              this.selectFileTitle = this.files[0].name;
              this.assetFileData = this.files[0];
              console.log("this.fileName", this.fileName);
              console.log("this.assetFileData", this.assetFileData);
              setTimeout(() => {
                let $source: any = $("#video_here");
                $source[0].src = URL.createObjectURL(this.files[0]);
                $source.parent()[0].load();
              }, 200);
            } catch (e) {
              console.log(e);
            }
          }
        });
      }
      ////////////////Code for fetch audio file//////////////

      // $(document).on('change','#audiofile',function(evt){
      if (this.audiofile) {
        this.render.listen(this.audiofile.nativeElement, "change", () => {
          this.isLocalfile = true;
          var size = 1000000;
          this.files = this.audiofile.nativeElement.files;
          // var validExts = new Array("audio");
          // var fileType = this.files[0].type;
          // var fileType = this.files[0].name.split('.')[1];
          var last_dot = this.files[0].name.lastIndexOf('.')
          var fileType = this.files[0].name.slice(last_dot + 1)
          console.log("fileType", fileType);
          var fileExt = false;
          for (let i = 0; i < validExts.length; i++) {
            if(validExts[i].formatId == 2){
            if (fileType.includes(validExts[i].extension)) {
              fileExt = true;
              break;
            }
          }
          }
          if (!fileExt) {
            // let type = 'error';
            // let title = 'Invalid file selected!';
            // let body = 'Valid files are of audio types.';
            this.presentToast("warning", "Please select a file of type audio");
            this.removeFile();
          } else if (size <= this.files[0].size) {
            // let type = 'error';
            // let title = 'File size exceeded!';
            // let body = 'File size should be less than 100 MB';
            this.presentToast(
              "warning",
              "File size should be less than 100 MB"
            );
            this.removeFile();
          } else {
            try {
              console.log("audio", this.files[0]);
              this.fileName = this.files[0].name;
              this.assetFileData = this.files[0];
              this.selectFileTitle = this.files[0].name;
              console.log("this.fileName", this.fileName);
              console.log("this.assetFileData", this.assetFileData);
              setTimeout(() => {
                let $source: any = $("#audio_here");
                $source[0].src = URL.createObjectURL(this.files[0]);
                $source.parent()[0].load();
              }, 200);
            } catch (e) {
              console.log(e);
            }
          }
        });
      }
      ///////////////////audio file End///////////////////////

      // $(document).on('change','#iframefile',function(evt){
      if (this.iframe) {
        this.render.listen(this.iframe.nativeElement, "change", () => {
          this.isLocalfile = true;
          var size = 1000000;
          this.files = this.iframe.nativeElement.files;
          // var validExts = new Array("pdf");
          // var fileType = this.files[0].type;
          // var fileType = this.files[0].name.split('.')[1];
          var last_dot = this.files[0].name.lastIndexOf('.')
          var fileType = this.files[0].name.slice(last_dot + 1)
          console.log("fileType", fileType);
          var fileExt = false;
          for (let i = 0; i < validExts.length; i++) {
            if(validExts[i].formatId == 3){
            if (fileType.includes(validExts[i].extension)) {
              fileExt = true;
              break;
            }
          }
          }
          if (!fileExt) {
            // let type = 'error';
            // let title = 'Invalid file selected!';
            // let body = 'Valid files are of pdf types.';
            this.presentToast("warning", "Please select a file of type pdf");
            this.removeFile();
          } else if (size <= this.files[0].size) {
            // let type = 'error';
            // let title = 'File size exceeded!';
            // let body = 'File size should be less than 100 MB';
            this.presentToast(
              "warning",
              "File size should be less than 100 MB"
            );
            this.removeFile();
          } else {
            try {
              console.log("pdf", this.files[0]);
              this.fileName = this.files[0].name;
              this.assetFileData = this.files[0];
              this.selectFileTitle = this.files[0].name;
              console.log("this.fileName", this.fileName);
              console.log("this.assetFileData", this.assetFileData);
              setTimeout(() => {
                let $source: any = $("#iframe");
                $source[0].src = URL.createObjectURL(this.files[0]);
                // $source.parent()[0].load();
              }, 200);
            } catch (e) {
              console.log(e);
            }
          }
        });
      }

      if (this.ppt) {
        this.render.listen(this.ppt.nativeElement, "change", () => {
          this.isLocalfile = true;
          var size = 1000000;
          this.files = this.ppt.nativeElement.files;
          // var validExts = new Array(
          //   "application/vnd.ms-powerpoint",
          //   "application/vnd.openxmlformats-officedocument.presentationml.presentation"
          // );
          // var fileType = this.files[0].type;
          // var fileType = this.files[0].name.split('.')[1];
          var last_dot = this.files[0].name.lastIndexOf('.')
          var fileType = this.files[0].name.slice(last_dot + 1)
          console.log("fileType", fileType);
          var fileExt = false;
          for (let i = 0; i < validExts.length; i++) {
            if(validExts[i].formatId == 10){
            if (fileType.includes(validExts[i].extension)) {
              fileExt = true;
              break;
            }
          }
          }
          if (!fileExt) {
            // let type = 'error';
            // let title = 'Invalid file selected!';
            // let body = 'Valid files are of pdf types.';
            this.presentToast("warning", "Please select a file of type ppt");
            this.removeFile();
          } else if (size <= this.files[0].size) {
            // let type = 'error';
            // let title = 'File size exceeded!';
            // let body = 'File size should be less than 100 MB';
            this.presentToast(
              "warning",
              "File size should be less than 100 MB"
            );
            this.removeFile();
          } else {
            try {
              console.log("pdf", this.files[0]);
              this.fileName = this.files[0].name;
              this.assetFileData = this.files[0];
              this.selectFileTitle = this.files[0].name;
              console.log("this.fileName", this.fileName);
              console.log("this.assetFileData", this.assetFileData);
              // setTimeout(() => {
              //   let $source: any = $('#iframe');
              //   $source[0].src = URL.createObjectURL(this.files[0]);
              //   // $source.parent()[0].load();
              // }, 200);
            } catch (e) {
              console.log(e);
            }
          }
        });
      }

      if (this.word) {
        this.render.listen(this.word.nativeElement, "change", () => {
          this.isLocalfile = true;
          var size = 1000000;
          this.files = this.word.nativeElement.files;
          // var validExts = new Array(
          //   "application/msword",
          //   "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
          // );
          // var fileType = this.files[0].type;
          // var fileType = this.files[0].name.split('.')[1];
          var last_dot = this.files[0].name.lastIndexOf('.')
          var fileType = this.files[0].name.slice(last_dot + 1)
          console.log("fileType", fileType);
          var fileExt = false;
          for (let i = 0; i < validExts.length; i++) {
            if(validExts[i].formatId == 12){
            if (fileType.includes(validExts[i].extension)) {
              fileExt = true;
              break;
            }
          }
          }
          if (!fileExt) {
            // let type = 'error';
            // let title = 'Invalid file selected!';
            // let body = 'Valid files are of pdf types.';
            this.presentToast("warning", "Please select a file ");
            this.removeFile();
          } else if (size <= this.files[0].size) {
            // let type = 'error';
            // let title = 'File size exceeded!';
            // let body = 'File size should be less than 100 MB';
            this.presentToast(
              "warning",
              "File size should be less than 100 MB"
            );
            this.removeFile();
          } else {
            try {
              console.log("pdf", this.files[0]);
              this.fileName = this.files[0].name;
              this.assetFileData = this.files[0];
              this.selectFileTitle = this.files[0].name;
              console.log("this.fileName", this.fileName);
              console.log("this.assetFileData", this.assetFileData);
              // setTimeout(() => {
              //   let $source: any = $('#iframe');
              //   $source[0].src = URL.createObjectURL(this.files[0]);
              //   // $source.parent()[0].load();
              // }, 200);
            } catch (e) {
              console.log(e);
            }
          }
        });
      }

      if (this.excel) {
        this.render.listen(this.excel.nativeElement, "change", () => {
          this.isLocalfile = true;
          var size = 1000000;
          this.files = this.excel.nativeElement.files;
          // var validExts = new Array(
          //   "application/vnd.ms-excel",
          //   "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
          // );
          // var fileType = this.files[0].type;
          // var fileType = this.files[0].name.split('.')[1];
          var last_dot = this.files[0].name.lastIndexOf('.')
          var fileType = this.files[0].name.slice(last_dot + 1)
          console.log("fileType", fileType);
          var fileExt = false;
          for (let i = 0; i < validExts.length; i++) {
            if(validExts[i].formatId == 11){
            if (fileType.includes(validExts[i].extension)) {
              fileExt = true;
              break;
            }
          }
          }
          if (!fileExt) {
            // let type = 'error';
            // let title = 'Invalid file selected!';
            // let body = 'Valid files are of pdf types.';
            this.presentToast("warning", "Please select a file ");
            this.removeFile();
          } else if (size <= this.files[0].size) {
            // let type = 'error';
            // let title = 'File size exceeded!';
            // let body = 'File size should be less than 100 MB';
            this.presentToast(
              "warning",
              "File size should be less than 100 MB"
            );
            this.removeFile();
          } else {
            try {
              console.log("pdf", this.files[0]);
              this.fileName = this.files[0].name;
              this.assetFileData = this.files[0];
              this.selectFileTitle = this.files[0].name;
              console.log("this.fileName", this.fileName);
              console.log("this.assetFileData", this.assetFileData);
              // setTimeout(() => {
              //   let $source: any = $('#iframe');
              //   $source[0].src = URL.createObjectURL(this.files[0]);
              //   // $source.parent()[0].load();
              // }, 200);
            } catch (e) {
              console.log(e);
            }
          }
        });
      }

      if (this.scormfile) {
        this.render.listen(this.scormfile.nativeElement, "change", () => {
          this.isLocalfile = true;
          var size = 50000000;
          this.files = this.scormfile.nativeElement.files;
          // var validExts = new Array("zip");
          // var fileType = this.files[0].type;
          // var fileType = this.files[0].name.split('.')[1];
          var last_dot = this.files[0].name.lastIndexOf('.')
          var fileType = this.files[0].name.slice(last_dot + 1)
          console.log("fileType", fileType);
          var fileExt = false;
          for (let i = 0; i < validExts.length; i++) {
            if(validExts[i].formatId == 5){
            if (fileType.includes(validExts[i].extension)) {
              fileExt = true;
              break;
            }
          }
          }
          if (!fileExt) {
            // let type = 'error';
            // let title = 'Invalid file selected!';
            // let body = 'Valid files are of zip types.';
            this.presentToast("warning", "Please select a zip file");
            this.removeFile();
          } else if (size <= this.files[0].size) {
            // let type = 'error';
            // let title = 'File size exceeded!';
            // let body = 'File size should be less than 100 MB';
            this.presentToast("warning", "File size should be less than 50 MB");
            this.removeFile();
          } else {
            try {
              console.log("zip", this.files[0]);
              this.fileName = this.files[0].name;
              this.assetFileData = this.files[0];
              this.selectFileTitle = this.files[0].name;
              console.log("this.fileName", this.fileName);
              console.log("this.assetFileData", this.assetFileData);
              //this.pdfUpload = true;
              // setTimeout( () => {
              //   let $source: any = $('#iframe');
              //   $source[0].src = URL.createObjectURL(this.files[0]);
              //   $source.parent()[0].load();
              // }, 200)
            } catch (e) {
              console.log(e);
            }
          }
        });
      }
    }, 500);
  }

  kpointConfigure(kpointlink) {
    this.kpointurl = kpointlink;
    try {
      if (!this.kpointurl) {
        alert("Please Enter Url");
      } else {
        const kpoint = this.populateKpointObj(this.kpointurl);
        const player = kPoint.Player(this.kpplayer.nativeElement, {
          kvideoId: kpoint.id,
          videoHost: kpoint.host,
          params: { autoplay: true },
        });
      }
    } catch (e) {
      console.log(e);
    }
  }
  populateKpointObj(url) {
    // tslint:disable-next-line:prefer-const
    let kpoint: any = {
      host: "",
      id: "",
      src: "",
      url: "",
    };
    if (url.split("/").length > 0) {
      kpoint.host = url.split("/")[2];
      kpoint.id = url.split("/")[4];
      kpoint.src = url;
      kpoint.url = url.split("/")[0] + "//" + url.split("/")[2];
    }
    return kpoint;
  }
  // youtubeconfigure(link) {
  //   this.youtubeurl = link;
  //   console.log('youtubeurl',this.youtubeurl);
  //   try {
  //     if (!this.youtubeurl) {
  //       alert('Please Enter url');
  //     } else {
  //       const videoid = this.populateYoutubeObj(this.youtubeurl);
  //       if (this.youtubeplayer) {
  //         this.youtubeplayer.loadVideoById(videoid);
  //       } else {
  //         this.youtubeplay.id = videoid;
  //       }
  //     }
  //   } catch (e) {
  //     console.log(e);
  //   }
  // }
  // savePlayer(player) {
  // //  this.youtubeplayer = player;
  //   console.log('player instance', player);
  // }
  // onStateChange(event) {
  //   console.log('player state', event.data);
  // }
  // populateYoutubeObj (url) {
  //   try {
  //     if (url) {
  //       let video_id = '';
  //       if (url.includes('embed')) {
  //         const urlsplit = url.split('/');
  //         video_id = urlsplit[urlsplit.length - 1];
  //       } else {
  //         video_id = url.split('v=')[1];
  //         const ampersandPosition = video_id.indexOf('&');
  //         if (ampersandPosition !== -1) {
  //           video_id = video_id.substring(0, ampersandPosition);
  //         }
  //       }
  //       return video_id;
  //     } else {
  //       return null;
  //     }
  //   } catch (e) {
  //     console.log(e);
  //     return null;
  //   }
  // }
  youtubeconfigure(link) {
    // this.youtubeurl = link;
    console.log("youtubeurl", link);
    try {
      if (!link) {
        alert("Please Enter url");
      } else {
        const videoid = this.populateYoutubeObj(link);
        // if (this.youtubeplayer) {
        //   this.youtubeplayer.loadVideoById(videoid);
        // } else {
        //   this.youtubeplay.id = videoid;
        // }
        const url =
          "https://www.youtube.com/embed/" + videoid + "?autoplay=1&showinfo=0";
        this.youtubeurl = this.transform(url);
      }
    } catch (e) {
      console.log(e);
    }
  }

  savePlayer(player) {
    this.youtubeplayer = player;
    console.log("player instance", player);
  }
  onStateChange(event) {
    console.log("player state", event.data);
  }
  populateYoutubeObj(url) {
    try {
      if (url) {
        let video_id = "";
        if (url.includes("embed")) {
          const urlsplit = url.split("/");
          video_id = urlsplit[urlsplit.length - 1];
        } else {
          video_id = url.split("v=")[1];
          const ampersandPosition = video_id.indexOf("&");
          if (ampersandPosition !== -1) {
            video_id = video_id.substring(0, ampersandPosition);
          }
        }
        return video_id;
      } else {
        return null;
      }
    } catch (e) {
      console.log(e);
      return null;
    }
  }

  // Help Code Start Here //

  helpContent: any;
  getHelpContent() {
    return new Promise((resolve) => {
      this.http1
        .get("../../../../../../assets/help-content/addEditCourseContent.json")
        .subscribe(
          (data) => {
            this.helpContent = data;
            console.log("Help Array", this.helpContent);
          },
          (err) => {
            resolve("err");
          }
        );
    });
    // return this.helpContent;
  }
  getMimeType(url) {
    const mimevalue = mime.getType(url);
    if (mimevalue) {
      const documenttype = mimevalue.split("/")[0];
      const result = {
        mimetype: mimevalue,
        documenttype: documenttype,
      };
      return result;
    } else {
      return mimevalue;
    }
  }

  makeTagDataReady(tagsData) {
    this.getAssetDataForApproval.tags1 = "";
    tagsData.forEach((tag) => {
      if (this.getAssetDataForApproval.tags1 == "") {
        this.getAssetDataForApproval.tags1 = tag.id;
      } else {
        this.getAssetDataForApproval.tags1 =
          this.getAssetDataForApproval.tags1 + "|" + tag.id;
      }
      console.log("this.formdata.tags", this.getAssetDataForApproval.tags1);
    });
    // var tagsData = this.formdata.tags;
    // var tagsString = '';
    // if (tagsData) {
    //   for (let i = 0; i < tagsData.length; i++) {
    //     var tag = tagsData[i];
    //     if (tagsString != "") {
    //       tagsString += "|";
    //     }
    //     if (String(tag) != "" && String(tag) != "null") {
    //       tagsString += tag;
    //     }
    //   }
    //   this.formdata.tags1 = tagsString;
    // }
  }
  author:string
  makeAuthorDataReady(Data) {
    this.author  =''
     Data.forEach((tag)=>{
      if(this.author  == '')
      {
        this.author  = tag.empId;
      }else
      {
        this.author = this.author +'|' + tag.empId;
      }
      console.log('this.author',this.author);
     });
    }

  onTagsSelect(item: any) {
    console.log(item);
    console.log(this.selectedTags);
  }
  OnTagDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedTags);
  }
  onTagSearch(evt: any) {
    console.log(evt.target.value);
    const val = evt.target.value;
    this.tagList = [];
    const temp = this.tempTags.filter(function (d) {
      return String(d.name).toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.tagList = temp;
    console.log("filtered Tag LIst", this.tagList);
  }
  // Help Code Ends Here //

  //functions of New Requirement

  CatModal(){
    console.log("enters")
    this.catshow = true
    parent = null
    this.getAllCatWise(parent)
  }

  getAllCatWise(parent){
    var param ={
      parentCatId:parent
    }
    const _urlgetcat: string = webApi.domain + webApi.url.getAllAssetCatWise;
    this.commonFunctionService.httpPostRequest(_urlgetcat,param).then(rescompData=>{
      console.log(rescompData,'catdata')
              // this.spinner.hide();
              var result = rescompData;
              console.log('getAssetResponse:', rescompData);
              if (result['type'] == true) {
                if (result['data'][0].length === 0) {
                  this.oldParentCatId = null;
                } else {
                  this.oldParentCatId = result['data'][0][0].parCatId;
                }
                if (result['data'][1].length == 0) {
                  this.noCatwise = true
                  // this.noAsset = true;
                  // this.skeleton = true
                } else {

                  this.assetCat = result['data'][1];
                  if(this.assetCat.length === 0){
                    // this.noCategory = true
                  }
                  this.demoassetCat  = result['data'][1];
                  this.noCatwise = false
                  if(this.categoryId){
                  for (let i = 0; i < this.assetCat.length; i++) {
                    if (this.assetCat[i].id == this.categoryId) {
                      this.CategoryName = this.assetCat[i].categoryName
                    }
                  }
                }

                  // this.displayArray = this.asset;
                  // this.displayArray = []
                  // this.addItems(0, this.sum, 'push', this.asset);


                  // this.noDataFound = false;
                  // this.skeleton = true;
                  // this.noAsset = false;
                  // console.log('this.asset', this.asset);
                  // console.log('this.displayArray', this.displayArray);
                  // this.cdf.detectChanges();
                }
              } else {
                this.spinner.hide();
                // this.skeleton = true
                // this.noDataFound = true;
                // var toast: Toast = {
                //   type: 'error',
                //   //title: "Server Error!",
                //   body: 'Something went wrong.please try again later.',
                //   showCloseButton: true,
                //   timeout: 2000
                // };
                // this.toasterService.pop(toast);
                this.presentToast('error', '');
              }
              //this.skeleton = true

            }, error => {
              // this.spinner.hide();
              // this.skeleton = true
              // this.noDataFound = true;
              // var toast: Toast = {
              //   type: 'error',
              //   //title: "Server Error!",
              //   body: 'Something went wrong.please try again later.',
              //   showCloseButton: true,
              //   timeout: 2000
              // };
              // this.toasterService.pop(toast);
              this.presentToast('error', '');
            });


  }
  activeSelectedCatId:any
  selectedItem:any
  setActiveSelectedCat(index,item){
    this.activeSelectedCatId = item.id;
    this.selectedItem = item
  }

  selectCat(){
    this.approveForm.category = this.activeSelectedCatId
    this.categoryId = this.activeSelectedCatId
    this.CategoryName = this.selectedItem.categoryName
    this.catshow = false
  }
  gotoCat(item){
    this.getAllCatWise(item.id)
    // this.breadCrumb.push(item.categoryName)
    // this.breadCrumb.join('/')
    // if(item){
    // this.track = item
    // }
  }

  backCat(){
    this.getAllCatWise(this.oldParentCatId);
    // this.breadCrumb = []
    // this.breadCrumb = this.track.categoryName

  }
  clear(){
    // this.search = ''
    this.assetCat = this.demoassetCat
  }

  closeModal(){
    this.assetCat = this.demoassetCat
    this.catshow = false
    // this.previewShow = false
  }
  searchQuestion(event) {
    var temData = this.demoassetCat;
    var val = event.target.value.toLowerCase();
    var keys = [];
    // filter our data
    if (temData.length > 0) {
      keys = Object.keys(temData[0]);
    }
    if(val.length>=3||val.length==0){
    var temp = temData.filter((d) => {
      for (const key of keys) {
        if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
          return true;
        }
      }
    });
  }

  this.assetCat = temp
}

  ngOnDestroy() {
    if (this.pdfserverlink != "") {
      this.approveassetservice
        .deletePdfLink(this.pdfserverlink)
        .then((result: any) => {
          console.log("Link ===>", result);
        })
        .catch((result) => {
          console.log("RESULT===>", result);
        });
    }
  }

  selectedAssetFormatData ={
    filterId: '',
    filterValue: '',
    formatId: '',
    formatMode: '',
    formatName: '',
    isSelected: '',
    pickerType: null,
    previewEnable: null,
    tagId: '',
    tagName: '',
  } ;
  assignValueToSelectDropDown(selectedFormatId){
    if(selectedFormatId && this.formats && this.formats.length){
      for(let i = 0 ; i < this.formats.length; i ++){
        if(selectedFormatId == this.formats[i].formatId){
          this.selectedAssetFormatData = this.formats[i];
          break;
        }
      }
    }
  }
}
