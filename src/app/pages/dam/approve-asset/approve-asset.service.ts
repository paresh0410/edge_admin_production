import {Injectable, Inject} from '@angular/core';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppConfig} from '../../../app.module';
import { webApi } from '../../../service/webApi';
import { HttpClient } from "@angular/common/http";

@Injectable()
export class ApproveAssetService {

    tenantId ;
    userData;
    userId;
    getAssetDataForApproval:any=[];
    assetStatus:any
    tabId:any
    title:any
       
  private _urlGetAllDropdown:string = webApi.domain + webApi.url.getAllAssetDropDown;
  private _urlGetAllEdgeEmployees:string = webApi.domain + webApi.url.getAllEdgeEmployee;
  private _urlUpdateAssetStatus:string = webApi.domain + webApi.url.approveRejectAsset;
  private _urlfetchnoteventdropdown = webApi.domain + webApi.url.getnotevent;
  private _urlfetchnottemplatedropdown = webApi.domain + webApi.url.dropdownnotifytemplate;

  private _urlGetPdfUrl: string = webApi.domain + webApi.url.getPdfUrl;
  private _urldeletePdfUrl: string = webApi.domain + webApi.url.deletePdfUrl;

  constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private _http: Http,private _httpClient:HttpClient){
      //this.busy = this._http.get('...').toPromise();
      if (localStorage.getItem('LoginResData')) {
         this.userData = JSON.parse(localStorage.getItem('LoginResData'));
         console.log('userData', this.userData.data);
         this.userId = this.userData.data.data.id;
        this.tenantId = this.userData.data.data.tenantId;
      }
  }

  getAllAssetDropdown(param){
    return new Promise(resolve => {
      this._httpClient.post(this._urlGetAllDropdown, param)
      //.map(res => res.json())
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}


getAllEmployee(param){
  return new Promise(resolve => {
    this._httpClient.post(this._urlGetAllEdgeEmployees, param)
    //.map(res => res.json())
    .subscribe(data => {
        resolve(data);
    },
    err => {
        resolve('err');
    });
});
}

updateAssetStatus(param){
    return new Promise(resolve => {
      this._httpClient.post(this._urlUpdateAssetStatus, param)
      //.map(res => res.json())
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
  }
  getPdfLink(link) {
    const linkUrl = this._urlGetPdfUrl + link;
    return new Promise(resolve => {
      this._httpClient.get(linkUrl)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  deletePdfLink(link) { // server pdf link
    const linkUrl = this._urldeletePdfUrl + link;
    return new Promise(resolve => {
            this._httpClient.get(linkUrl)
            // .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve(null);
            });
        });
  }

_errorHandler(error: Response){
  console.error(error);
  return Observable.throw(error || "Server Error")
}

}
