import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApproveAssetComponent } from './approve-asset.component';

describe('ApproveAssetComponent', () => {
  let component: ApproveAssetComponent;
  let fixture: ComponentFixture<ApproveAssetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApproveAssetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApproveAssetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
