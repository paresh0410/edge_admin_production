import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { TagInputModule } from 'ngx-chips';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { JoditAngularModule } from 'jodit-angular';
import { FlatpickrModule } from 'angularx-flatpickr';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { DamComponent } from './dam.component';
import { AddAssetComponent } from './asset/add-asset/add-asset.component';
import { ApproveAssetComponent } from './approve-asset/approve-asset.component';
import { ShareAssetComponent } from './share-asset/share-asset.component';
import { AssetReviewPolicyComponent } from './asset-review-policy/asset-review-policy.component';
import { AssetComponent } from './asset/asset.component';
import { ThemeModule } from '../../@theme/theme.module';
import { BulkUploadComponent } from './bulk-upload/bulk-upload.component'
import { ScrollingModule } from '@angular/cdk/scrolling';
//import { IntermediateDamComponent } from './intermediate-dam/intermediate-dam.component'
import { AssetCategoriesComponent } from './asset-categories/asset-categories.component'
import { AddAssetService } from './asset/add-asset/add-asset.service';
import { AssetCategoriesService } from './asset-categories/asset-categories.service';
import { AssetService } from './asset/asset.service';
import { ApproveAssetService } from './approve-asset/approve-asset.service';
import { ShareAssetService } from './share-asset/share-asset.service';
import { AssetReviewPolicyService } from './asset-review-policy/asset-review-policy.service'

import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';

//import { DocumentViewerComponent } from './../../component/document-viewer/document-viewer/documentviewer.component';
import { DocumentViewerService }  from './../../component/document-viewer/document-viewer/documentviewer.service';
import { AssetVersionComponent } from './asset-version/asset-version.component';
import { AssetVesrionService } from './asset-version/asset-version.service';

import { BulkUploadService } from './bulk-upload/bulk-upload.service';

import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { FilterPipeModule  } from 'ngx-filter-pipe';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { JsonToXlsxService } from './bulk-upload/json-to-xlsx.service';
import { QuillModule } from 'ngx-quill';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgxContentLoadingModule } from 'ngx-content-loading';
import { ComponentModule } from '../../component/component.module';
import 'quill/dist/quill.core.css';
import 'quill/dist/quill.snow.css';
import { NewAssetComponent } from './new-asset/new-asset.component';
import { NbTabsetModule } from '@nebular/theme';
import {MatTabsModule} from '@angular/material'
import { FolderUploadComponent } from './folder-upload/folder-upload.component';
import { FolderFileComponent } from './folder-file/folder-file.component';
import { DigitalAssetComponent } from '../components/digital-asset/digital-asset.component';
import { ContextMenuModule } from 'ngx-contextmenu';
// import { FolderUploadComponent } fro./folder-upload/folder-upload.componentent';
import { TruncateModule } from 'ng2-truncate';
import { ShareAssetPopupComponent } from './share-asset-popup/share-asset-popup.component';
import { MoveAssetPopupComponent } from './move-asset-popup/move-asset-popup.component';
// import { NewAssetComponent } from './asset/new-asset/new-asset.component';
// import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { DamRoutingModule } from './dam-routing.module';

@NgModule({
  imports: [
    // InfiniteScrollModule,
    NgMultiSelectDropDownModule.forRoot(),
    CommonModule,
    TagInputModule,
    NgxDatatableModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    FormsModule,
    ReactiveFormsModule,
    // ComponentModule,
    AngularMultiSelectModule,
    TimepickerModule,
    FilterPipeModule,
    QuillModule,
    NgxSkeletonLoaderModule,
    NgxContentLoadingModule,
    ComponentModule,
    NbTabsetModule,
    MatTabsModule,
    JoditAngularModule,
    TruncateModule,
    ContextMenuModule.forRoot(),
    DamRoutingModule,
    ScrollingModule,
],
declarations: [
    DamComponent,
    AddAssetComponent,
    ApproveAssetComponent,
    ShareAssetComponent,
    AssetReviewPolicyComponent,
    AssetComponent,
    AssetCategoriesComponent,
    //DocumentViewerComponent,
    AssetVersionComponent,
    BulkUploadComponent,
    NewAssetComponent,
    FolderUploadComponent,
    FolderFileComponent,
    DigitalAssetComponent,
    ShareAssetPopupComponent,
    MoveAssetPopupComponent


],
providers: [
    AddAssetService,
    AssetCategoriesService,
    AssetService,
    ApproveAssetService,
    ShareAssetService,
    AssetReviewPolicyService,
    DocumentViewerService,
    AssetVesrionService,
    BulkUploadService,
    JsonToXlsxService
],
schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
]
})

export class DamModule{

}
