import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShareAssetPopupComponent } from './share-asset-popup.component';

describe('ShareAssetPopupComponent', () => {
  let component: ShareAssetPopupComponent;
  let fixture: ComponentFixture<ShareAssetPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShareAssetPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShareAssetPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
