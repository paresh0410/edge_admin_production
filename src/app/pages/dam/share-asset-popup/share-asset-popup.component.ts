import { Component, OnInit, Input, Output , EventEmitter } from '@angular/core';
import { webApi } from '../../../service/webApi';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'ngx-share-asset-popup',
  templateUrl: './share-asset-popup.component.html',
  styleUrls: ['./share-asset-popup.component.scss']
})
export class ShareAssetPopupComponent implements OnInit {

  @Input() asset = null;
  @Input() assetParentId = null;
  @Input() assetParentCatName = null;

  @Output() closePopup = new EventEmitter<any>();
  labels: any = [
    { labelname: '', bindingProperty: '', componentType: 'checkbox' },
    { labelname: 'ecn', bindingProperty: 'idnumber', componentType: 'text' },
    { labelname: 'CONSUMER', bindingProperty: 'name', componentType: 'text' },
    { labelname: 'E-MAIL', bindingProperty: 'email', componentType: 'text' },
    ];
    selected = [];
    allEmployees = [];
    rows = [];
    noEmployees = false;
    tenantId = null;
    userId = null;
    checked: boolean;
    noDataVal={
      margin: 'mt-3',
      imageSrc: '../../../../../assets/images/no-data-bg.svg',
      title: "Sorry we couldn't find any matches please try again",
      desc: ".",
      titleShow: true,
      btnShow: false,
      descShow: false,
      btnText: 'Learn More',
      btnLink: ''
    }
  selectedEcn: any;
  constructor(private commonFunctionService: CommonFunctionsService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,) { }

  ngOnInit() {
    if (localStorage.getItem('LoginResData')) {
      var userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', userData.data);
      this.userId = userData.data.data.id;
      this.tenantId = userData.data.data.tenantId;
      console.log('userId', userData.data.data.id);
    }
  // this.rows = this.team;
  if(this.asset){
    this.getAssetShareEmployeesDAM();
  }

  }

  getAssetShareEmployeesDAM(){
    this.spinner.show();
      const param = {
          'assetsId': this.asset.assetId,
          'tId': this.tenantId,
          'aType': this.asset.caFlag,
      }
      const _urlGetAssetShareEmployees:string = webApi.domain + webApi.url.getAssetShareEmployee;
      this.commonFunctionService.httpPostRequest(_urlGetAssetShareEmployees,param)
      // this.shareassetservice.getAssetShareEmployee(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('getShareEmployessDAM:',rescompData);
        if(result['type'] == true){
          if(result['data'] && result['data'].length != 0 && result['data'][0].length == 0){
            this.noEmployees = true;
            this.selected = [];
            this.noDataVal = {
              margin: 'mt-3',
              imageSrc: '../../../../../assets/images/no-data-bg.svg',
              title: "Sorry no employees found. please try again",
              desc: ".",
              titleShow: true,
              btnShow: false,
              descShow: false,
              btnText: 'Learn More',
              btnLink: ''
            }
          }else{
            this.allEmployees = result['data'][0];
            console.log('this.allEmployees',this.allEmployees);
            this.rows = this.allEmployees;
            this.selected = [];
            for(let i = 0;i<this.allEmployees.length;i++){
              if(this.allEmployees[i].checked == 1){
                this.selected.push(this.allEmployees[i]);
              }
            }
            if(this.selected.length>0){
              this.checked=true;
            }
            else{
              this.checked=false;
            }
            console.log('selected',this.selected);
          }

        }else{
          //   var toast : Toast = {
          //     type: 'error',
          //     //title: 'Server Error!',
          //     body: 'Something went wrong.please try again later.',
          //     showCloseButton: true,
          //     timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }


      },error=>{
        this.spinner.hide();
        //   var toast : Toast = {
        //     type: 'error',
        //     //title: 'Server Error!',
        //     body: 'Something went wrong.please try again later.',
        //     showCloseButton: true,
        //     timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
  }

  onSelect(data){
    console.log('selected row:',data);
    this.selected = data
    if(this.selected.length>0){
      this.checked=true;
    }
    else{
      this.checked=false;
    }
  //this.selected = data

    // console.log('this.selected',this.selected);

  }

  save(){
    if(this.selected && this.selected.length === 0){
      this.toastr.warning('Please select user to share file', 'Warning');
      return null;
    }
    this.closePopup.emit();
    const userStr = this.getUserdataReady(this.selected);
    console.log('userStr',userStr);
    let param = {
      'assetsId': this.asset.assetId,
      'tId': this.tenantId,
      'userId': this.userId,
      'qOptions': userStr,
      'caFlag': this.asset.caFlag,
      "parentCatId" : this.assetParentId,
    	"parentCatName" : this.assetParentCatName,
      "sharedusers" : this.selectedEcn,
      "instanceName":this.asset.assetName,
    }
    const  _urlInsertAssetShare:string = webApi.domain + webApi.url.insertAssetShare;
    this.commonFunctionService.httpPostRequest(_urlInsertAssetShare,param)
    // this.shareassetservice.insertAssetShare(param)
    .then(rescompData => {
      this.spinner.hide();
      this.checked=true;
      var result = rescompData;
      console.log('getShareEmployessDAM:',rescompData);
      if(result['type'] == true){
      //   var toast : Toast = {
      //     type: 'success',
      //     title: 'Share Asset',
      //     body: 'Asset share successfully.',
      //     showCloseButton: true,
      //     timeout: 2000
      // };
      // this.toasterService.pop(toast);
      if(this.asset.caFlag === 1){
        this.presentToast('success', 'Folder Shared Successfully');
      }else {
        this.presentToast('success', 'File Shared Successfully');
      }

      // this.getAssetShareEmployeesDAM();
      // this.router.navigate(['../'],{relativeTo:this.routes});


      }else{
        //   var toast : Toast = {
        //     type: 'error',
        //     //title: 'Server Error!',
        //     body: 'Something went wrong.please try again later.',
        //     showCloseButton: true,
        //     timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      }


    },error=>{
      this.spinner.hide();
      //   var toast : Toast = {
      //     type: 'error',
      //     //title: 'Server Error!',
      //     body: 'Something went wrong.please try again later.',
      //     showCloseButton: true,
      //     timeout: 2000
      // };
      // this.toasterService.pop(toast);
      this.presentToast('error', '');
    });
  }

  presentToast(type, body) {
    if(type === 'success'){
    this.toastr.success(body, 'Success', {
    closeButton: false
    });
    } else if(type === 'error'){
    this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
    timeOut: 0,
    closeButton: true
    });
    } else{
    this.toastr.warning(body, 'Warning', {
    closeButton: false
    })
    }
    }

    getUserdataReady(selectedUserArr){
      console.log('selectedUserArr',selectedUserArr);
      var userString = '';
      this.selectedEcn='';
      if(selectedUserArr.length > 0){
        for(let i=0; i < selectedUserArr.length; i++){
          var user = selectedUserArr[i].ecn;
          var ecn = selectedUserArr[i].idnumber;
          if(userString != ''){
            userString += '|';
            this.selectedEcn+=',';

          }
          if(user.value){
            if(String(user.value) != '' && String(user.value) != 'null'){
              userString += user.value;
              this.selectedEcn+=ecn.value
            }
          }else{
            if(String(user) != '' && String(user) != 'null'){
              userString += user;
              this.selectedEcn+=ecn

            }
          }
        }
        return userString;
      }
      else{
        return null;
      }
    }
    searchtext = '';
    // searchShareUser(event,searchtext) {
    //   searchtext= event.target.value;

    //   const val = event.target.value.toLowerCase();
    //   this.searchtext = val;
    //   // this.allEnrolUser( this.courseDataService.data.data)

    // //  this.temp = [...this.enrolldata];
    // //   console.log(this.temp);
    //   // filter our data
    //   if(val.length>=3||val.length==0){
    //   const searchArr = this.allEmployees.filter(function(d) {
    //     return String(d.idnumber).toLowerCase().indexOf(val) !== -1 ||
    //     d.name.toLowerCase().indexOf(val) !== -1 ||
    //     d.email.toLowerCase().indexOf(val) !== -1 ||
    //     // d.mode.toLowerCase() === val || !val;
    //      !val
    //   });


    //   // update the rows
    //   this.rows = [...searchArr];
    //   // Whenever the filter changes, always go back to the first page
    // }else if (val.length == 0){
    //   this.rows = [...this.allEmployees];
    // }
    // if(this.rows &&  this.rows.length !== 0){
    //   this.noEmployees = false;
    //   this.noDataVal = {
    //     margin: 'mt-3',
    //     imageSrc: '../../../../../assets/images/no-data-bg.svg',
    //     title: "Sorry we couldn't find any matches please try again",
    //     desc: ".",
    //     titleShow: true,
    //     btnShow: false,
    //     descShow: false,
    //     btnText: 'Learn More',
    //     btnLink: ''
    //   }
    // }
    // }
    searchShareUser(event,searchtext) {
      searchtext= event.target.value;

      const val = event.target.value.toLowerCase();
      this.searchtext = val;
      let allEmps = []
      var preselected = []
      this.allEmployees.forEach(element => {
        if(element.checked){
          preselected.push(element);
        }else{
          allEmps.push(element)
        }
      });
      // this.allEnrolUser( this.courseDataService.data.data)
    // let allEmps  = this.allEmployees
    //  this.temp = [...this.enrolldata];
    //   console.log(this.temp);
      // filter our data
      if(val.length>=3||val.length==0){
      const searchArr =allEmps.filter(function(d) {
        return String(d.idnumber).toLowerCase().indexOf(val) !== -1 ||
        d.name.toLowerCase().indexOf(val) !== -1 ||
        d.email.toLowerCase().indexOf(val) !== -1 ||
        // d.mode.toLowerCase() === val || !val;
         !val
      });




      // update the rows
      this.rows = [...searchArr];
      this.rows = preselected.concat(searchArr)
      // Whenever the filter changes, always go back to the first page
    }else if (val.length == 0){
      this.rows = [...this.allEmployees];
    }
    if(this.rows &&  this.rows.length !== 0){
      this.noEmployees = false;
      this.noDataVal = {
        margin: 'mt-3',
        imageSrc: '../../../../../assets/images/no-data-bg.svg',
        title: "Sorry we couldn't find any matches please try again",
        desc: ".",
        titleShow: true,
        btnShow: false,
        descShow: false,
        btnText: 'Learn More',
        btnLink: ''
      }
    }
    }
    clearSearch(){
      this.searchtext = '';
      this.rows = this.allEmployees;
      if(this.rows &&  this.rows.length !== 0){
        this.noEmployees = false;
        this.noDataVal = {
          margin: 'mt-3',
          imageSrc: '../../../../../assets/images/no-data-bg.svg',
          title: "No Employees available to share.",
          desc: ".",
          titleShow: true,
          btnShow: false,
          descShow: false,
          btnText: 'Learn More',
          btnLink: ''
        }
      }
    }
}
