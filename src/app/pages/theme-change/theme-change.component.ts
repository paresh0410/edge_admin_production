import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { SuubHeader } from '../components/models/subheader.model';
import { Router } from '@angular/router';
import { OpenApiServiceService } from '../plan/open-api/open-api-service.service';
import { webApi } from '../../service/webApi';
import { webAPIService } from '../../service/webAPIService';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'ngx-theme-change',
  templateUrl: './theme-change.component.html',
  styleUrls: ['./theme-change.component.scss']
})
export class ThemeChangeComponent implements OnInit {
  header: SuubHeader = {
    title: 'Theme',
    btnsSearch: true,
    btnBackshow: true,
    showBreadcrumb: true,
    breadCrumbList:[]   
  };
  themesclr: any = [
    {
      name: "Admin",
      items: [
        { name: 'Header', value: '#fff' },
        { name: 'Header Text', value: '#000' },
        { name: 'Sidemenu', value: '#f00' }
      ]
    },
    {
      name: "Portal",
      items: [
        { name: 'Header', value: '#fff' },
        { name: 'Header Text', value: '#000' },
        { name: 'Sidemenu', value: '#f00' }
      ]
    },
    {
      name: "App",
      items: [
        { name: 'Header', value: '#fff' },
        { name: 'Header Text', value: '#000' },
        { name: 'Sidemenu', value: '#f00' }
      ]
    },
  ];

  courses: any = [
    {
      name: "Mandatory Courses",
      ongoing: "45",
      classroom: "12",
      online: "20",
    },
    {
      name: "Open Courses",
      ongoing: "27",
      classroom: "6",
      online: "10",
    },
  ];

  box: any = [
    {
      name: "Survey",
      icon: "fas fa-clipboard",
      text: "You have 1 survey"
    },
    {
      name: "Poll",
      icon: "fas fa-chart-bar",
      text: "You have 1 poll"
    },
    {
      name: "Earned Badges",
      icon: "fas fa-medal",
      text: "You have earned 1 badges !"
    },
    {
      name: "Earned Certificate",
      icon: "fas fa-medal",
      text: "You have no certificate !"
    },
  ];

  news: any = [
    {
      title: "Lorem Ipsum is simply dummy",
      discription: "Lorem Ipsum has been the industry's standard dummy text ever"
    },
    {
      title: "Lorem Ipsum is simply dummy",
      discription: "Lorem Ipsum has been the industry's standard dummy text ever"
    },
    {
      title: "Lorem Ipsum is simply dummy",
      discription: "Lorem Ipsum has been the industry's standard dummy text ever"
    },
    {
      title: "Lorem Ipsum is simply dummy",
      discription: "Lorem Ipsum has been the industry's standard dummy text ever"
    },
  ];

  leaders: any = [
    {
      name: "Lorem ipsum",
      points: "123"
    },
    {
      name: "Lorem ipsum",
      points: "43"
    },
    {
      name: "Lorem ipsum",
      points: "78"
    },
    {
      name: "Lorem ipsum",
      points: "533"
    },
    {
      name: "Lorem ipsum",
      points: "98"
    },
    {
      name: "Lorem ipsum",
      points: "288"
    },
  ];

  activities: any = [
    {
      name: "Audio file",
      time: "01:12:00",
      image: "../../../assets/images/activity_image/audio.jpg",
      discription: "Lorem Ipsum has been the industry's standard dummy text ever",
    },
    {
      name: "Fedback",
      time: "12:00:20",
      image: "../../../assets/images/activity_image/feedback.jpg",
      discription: "Lorem Ipsum has been the industry's standard dummy text ever",
    },
    {
      name: "Image file",
      time: "01:12:00",
      image: "../../../assets/images/activity_image/image.jpg",
      discription: "Lorem Ipsum has been the industry's standard dummy text ever",
    },
    {
      name: "Pdf file",
      time: "00:19:90",
      image: "../../../assets/images/activity_image/pdf.jpg",
      discription: "Lorem Ipsum has been the industry's standard dummy text ever",
    },
    {
      name: "Scrom file",
      time: "01:12:00",
      image: "../../../assets/images/activity_image/scrom.jpg",
      discription: "Lorem Ipsum has been the industry's standard dummy text ever",
    },
    {
      name: "Poll file",
      time: "00:19:90",
      image: "../../../assets/images/activity_image/poll.jpg",
      discription: "Lorem Ipsum has been the industry's standard dummy text ever",
    },
    {
      name: "Quiz file",
      time: "01:12:00",
      image: "../../../assets/images/activity_image/quiz.jpg",
      discription: "Lorem Ipsum has been the industry's standard dummy text ever",
    },
  ];

  preview: boolean = false;
  pheader: any;
  default: any = "assets/images/badgeNew.png";
  imgUrl: any
  imageFile: any;
  separators: any;
  componentData: string;
  buttonText: any;
  button: any;
  icon: any;
  common: any;
  sideMenu: any;
  headerData: any;
  userData: any;
  tenantId: any;
  countNo: number;
  themeId: any;
  fileUploadRes: any;
  active: string;
  private themes = document.querySelector('body');
  
  variables: any = {
    button: '--colorBgbtn',
    commonColor: '--colorCommon',
    header: '--colorHeader',
    icon: '--colorIcons',
    pageBackground: '--colorBgPage',
    sidemenu: '--colorSidebar',
  };

  portalTheme: any = {};
  sidemenuclr: any;
  btnclr: any;
  showHome: any = true;
  selectTab: string = 'Admin';

  constructor(private router: Router,private toastr: ToastrService,
    private openService:OpenApiServiceService,private spinner: NgxSpinnerService,
    private cdf: ChangeDetectorRef,
    protected webApiService: webAPIService,) {
    if(localStorage.getItem('userDetails')){
      this.userData = JSON.parse(localStorage.getItem('userDetails'));
      console.log('userData', this.userData.data);
     //  this.userId = this.userData.data.data.id;
      this.tenantId = this.userData.tenantId;
   }
   this.tenantId = this.tenantId?this.tenantId:1
   this.selectTab = 'Admin';
   }

  ngOnInit() {
    this.header.breadCrumbList=[{
      'name': 'Settings',
      'navigationPath': '/pages/plan',
      }]
    // this.active = 'Admin'
   this.selectTab = 'Admin';
    this.getAllThemeData();
    const data = {
      tabTitle : 'Admin'
    };
    // this.selectedTabTest(data);
    this.cdf.detectChanges();
    // this.selectedTabTest(data);
  }

  onEventLog(event, label, tab) {
    console.log("drag event ==========================================", event);
    console.log(label);
    console.log(tab);
    this.pheader = event.color;
    if(tab === 'Portal') {
      for(let key in this.portalTheme) {
        if(label === key) {
          this.portalTheme[key] = event.color;
          this.sidemenuclr = label === 'sidemenu' ? this.getContrastYIQ(event.color) : this.sidemenuclr;
          this.btnclr = label === 'button' ? this.getContrastYIQ(event.color) : this.btnclr;
        }
      }
    }
  }

  changeLayout(lname, tabname) {
    if(tabname === 'Portal') {
      this.showHome = lname === 'button' ? false : true;
    }
  }

  getAllThemeData(){
    this.spinner.show()
    this.openService.getAllThemeData().then(res=>{
      this.spinner.hide()
      console.log(res,"res")
      this.themesclr = res['List']
      this.themeId = res['themeId']
      this.separators = res['Seperator']
      this.imgUrl = res['logo']
      this.imgUrl = this.imgUrl?this.imgUrl:this.default
      let portalArr = this.themesclr[2].componentList;
      let localtheme = JSON.parse(localStorage.getItem('theme'));
      localtheme.logo = this.imgUrl;
      localStorage.setItem('theme', JSON.stringify(localtheme));
      // for(let i = 0; i < portalArr.length; i++) {
      //   this.portalTheme[portalArr[i].compCode] = portalArr[i].codeValue;
      // }
      // this.sidemenuclr = this.getContrastYIQ(this.portalTheme.sidemenu);
      // this.btnclr = this.getContrastYIQ(this.portalTheme.button);
      this.applyTheme(portalArr);
    })
  }

  addEditTheme(array,item){
    this.spinner.show()
    var data
    var demo :any=[]
    console.log(array,"value");
    const theme = {};
    for (let index = 0; index < array.length; index++) {
      const value = array[index];
      
      data =value.thcompId+this.separators.pipe+value.codeValue
      console.log(data,"data")

      demo.push(data)
      demo.join(this.separators.hash)
      console.log(demo,"hash")


      theme[array[index].compCode] = array[index].codeValue;

      // if (value.thcompId == 1) {
      //   var header = value.codeValue
      //   var headerId = value.thcompId
      //   this.headerData =  headerId + this.separators.pipe + header
      //   // console.log(value,header)
      // }
      // else if (value.thcompId == 2) {
      //   var sidemenu = value.codeValue
      //   var sidemenuId = value.thcompId
      //   this.sideMenu =  sidemenuId + this.separators.pipe + sidemenu
      //   // console.log(value,header)
  
      // }
      // else if (value.thcompId == 3) {
      //   var common = value.codeValue
      //   var commonId = value.thcompId
      //   this.common =  commonId + this.separators.pipe + common
      //   // console.log(value,header)
  
      // }
      // else if (value.thcompId == 4) {
      //   var icon = value.codeValue
      //   var iconId = value.thcompId
      //   this.icon =  iconId + this.separators.pipe + icon
      //   // console.log(value,header)
  
      // }
      // else if (value.thcompId == 5) {
      //   var button = value.codeValue
      //   var buttonId = value.thcompId
      //   this.button =  buttonId + this.separators.pipe + button
      //   // console.log(value,header)
  
      // }
      // else if (value.thcompId == 6) {
      //   var buttontext = value.codeValue
      //   var btextId = value.thcompId
      //   this.buttonText =  btextId + this.separators.pipe + buttontext
      //   // console.log(value,header)
  
      // }
    }
      var tenant = this.separators.hash+this.tenantId
      var componentData = demo
      componentData = componentData.join(this.separators.hash) + tenant
      // var componentData = this.headerData + this.separators.hash +this.sideMenu+this.separators.hash+
      // this.common+this.separators.hash+this.button+this.separators.hash+this.icon+this.separators.hash+this.buttonText+this.separators.hash+this.tenantId
        console.log(componentData,"componentData")

        var param = {
          thmId:this.themeId,
          ThemeId:this.tenantId,
	        thmName: 'theme_'+ this.tenantId,
          ComponentData:componentData,
          len:array.length,
          compTy:item.thCompTypeId
          
        }
        console.log('params', param);

    this.openService.addEditThemeData(param).then(res=>{
      this.spinner.hide()
      this.themeId = res['List'][0].id
      if(res['type'] == true){
      this.toastr.success( res['List'][0].msg , 'Success', {
        closeButton: false
      });
      this.getAllThemeData()
      if(item.thCompTypeId === 1) {
        localStorage.setItem('theme', JSON.stringify(theme));
        for(let key in theme) {
          let value = theme[key];
          this.changeTheme(key,value);
        }
      }
    }
      else{
        this.toastr.success( 'Unable to update theme ' , 'err', {
          closeButton: false
        });
      }
      // this.active = item.typeName
    })
  }
  gotocolors(array){
    // console.log(value, "value")
    for (let index = 0; index < array.length; index++) {
      const value = array[index];
      if (value.thcompId == 1) {
        var header = value.codeValue
        var headerId = value.thcompId
        this.headerData =  headerId + this.separators.pipe + header
        // console.log(value,header)
      }
      else if (value.thcompId == 2) {
        var sidemenu = value.codeValue
        var sidemenuId = value.thcompId
        this.sideMenu =  sidemenuId + this.separators.pipe + sidemenu
        // console.log(value,header)
  
      }
      else if (value.thcompId == 3) {
        var common = value.codeValue
        var commonId = value.thcompId
        this.common =  commonId + this.separators.pipe + common
        // console.log(value,header)
  
      }
      else if (value.thcompId == 4) {
        var icon = value.codeValue
        var iconId = value.thcompId
        this.icon =  iconId + this.separators.pipe + icon
        // console.log(value,header)
  
      }
      else if (value.thcompId == 5) {
        var button = value.codeValue
        var buttonId = value.thcompId
        this.button =  buttonId + this.separators.pipe + button
        // console.log(value,header)
  
      }
      else if (value.thcompId == 6) {
        var buttontext = value.codeValue
        var btextId = value.thcompId
        this.buttonText =  btextId + this.separators.pipe + buttontext
        // console.log(value,header)
  
      }
    }
    
    
      var componentData = this.headerData + this.separators.hash +this.sideMenu+this.separators.hash+
      this.common+this.separators.hash+this.button+this.separators.hash+this.icon+this.separators.hash+this.buttonText
    // this.componentData =  headerId + this.separators.pipe + header +this.separators.hash
    console.log(componentData,"Data")
    this.componentData = componentData
  
}

  changeImage(){
    this.spinner.show()
      var content = {name: 'Bhavesh'};
      var fd = new FormData();
      fd.append('content', JSON.stringify(content));
      fd.append('file', this.imageFile);
      console.log('form Data :- ', fd);
      this.openService.changeImageData(fd).then(res=>{
        console.log(res)
        this.spinner.hide()
        if(res['type'] === 2){
        this.toastr.success( 'Logo Saved' , 'Success', {
          closeButton: false
        });
          this.getAllThemeData()
        }else{
        this.toastr.error( 'Unable to change Logo', 'Error', {
        timeOut: 0,
          closeButton: true
              });
              this.getAllThemeData()
      }
      this.active = 'Logo'
        console.log('res',res);
      })

      //fd.append('data',null);
      

      // let fileUploadUrl = webApi.domain + webApi.url.fileUpload;
      // let param = {
      //   tId: this.tenantId,
      // }



      // this.webApiService.getService(fileUploadUrl, fd)
      //   .then(rescompData => {
      //     var temp: any = rescompData;
      //     this.fileUploadRes = temp;
      //     if (temp == "err") {
      // //       this.toastr.error( 'Unable to upload category image.', 'Error', {
      // //         timeOut: 0,
      // //         closeButton: true
      // // });

      //     } else if (temp.type == false) {
      //       // this.toastr.error( 'Unable to upload category image.', 'Error', {
      //       //   timeOut: 0,
      //       //   closeButton: true
      //       // });
      //     }
      //     else {
      //       if (this.fileUploadRes.data != null || this.fileUploadRes.fileError != true) {
      //         // category.categoryPicRef = this.fileUploadRes.data.file_url;
      //         var image = this.fileUploadRes.data.file_url;
          
      //       } else {
      //         // var thumbUpload: Toast = {
      //         //   type: 'error',
      //         //   title: "Category Thumbnail",
      //         //   body: this.fileUploadRes.status,
      //         //   showCloseButton: true,
      //         //   timeout: 2000
      //         // };
      //         // this.toasterService.pop(thumbUpload);


      //     //   this.toastr.error( this.fileUploadRes.status , 'Error', {
      //     //     timeOut: 0,
      //     //     closeButton: true
      //     // });
      //       }
      //     }
          
      //     console.log('File Upload Result', this.fileUploadRes)
      //   })
        

    
  }

  setDefault(item){
    this.spinner.show()
    var param
    param={
      thmId : this.themeId,
	    thmName: 'theme_'+this.tenantId,
	    compTy :item.thCompTypeId
    }
    this.openService.setThemeData(param).then(res=>{
      this.themesclr = res['List']
      this.spinner.hide()
      if(res['type'] == true){
        this.toastr.success( 'Theme updated to default' , 'Success', {
          closeButton: false
        });
      if(this.themesclr.length > 0) {
        let adminArr = this.themesclr[0].componentList;
        let portalArr = this.themesclr[2].componentList;
        this.applyTheme(portalArr);
        const theme = {};
        for(let i = 0; i < adminArr.length; i++) {
          theme[adminArr[i].compCode] = adminArr[i].codeValue;
        }
        localStorage.setItem('theme', JSON.stringify(theme));
        for(let key in theme) {
          let value = theme[key];
          this.changeTheme(key,value);
        }
      }
      }
        else{
          this.toastr.success( 'Unable to update theme ' , 'err', {
            closeButton: false
          });
        }

    })
  }

  back() {
    this.router.navigate(['/pages/plan']);
  }

  pickLogo(event: any) {
    this.imageFile = event.target.files[0];

    // this.imageFile = imagefile;

    const reader = new FileReader();
    reader.readAsDataURL(this.imageFile);
    reader.onload = (_event) => {
      // this.imgUrl = reader.result;
      this.imgUrl = (<FileReader>_event.target).result;
      // console.log(this.imgUrl,"this.imageUrl")
    };
  }
  count(){
    // this.countNo
    // this.count++

  }

  deleteLogo() {
    this.imgUrl = " ";
  }

  changeTheme(colorName, colorValue) {
    // Object.keys(this.variables).forEach(function(val) {
      // console.log('themes=========>>>>>>>>>>>', themes[key]);
      for(let val in this.variables) {
      if(val === colorName) {
        if (colorName === 'header') {
          this.themes.style.setProperty(this.variables[val], colorValue);
          const headerTxt = this.getContrastYIQ(colorValue);
          this.themes.style.setProperty('--colorHeadertxt', headerTxt);
        } else if (colorName === 'sidemenu') {
          this.themes.style.setProperty(this.variables[val], colorValue);
          const sidebarTxt = this.getContrastYIQ(colorValue);
          this.themes.style.setProperty('--colorSidebartxt', sidebarTxt);
        } else if (colorName === 'button') {
          this.themes.style.setProperty(this.variables[val], colorValue);
          const btnTxt = this.getContrastYIQ(colorValue);
          this.themes.style.setProperty('--colorBtnTxt', btnTxt);
        } else {
          this.themes.style.setProperty(this.variables[val], colorValue);
        }
      }
    }
    // });
  }

  getContrastYIQ(hexcolor){
    hexcolor = hexcolor.replace("#", "");
    var r = parseInt(hexcolor.substr(0,2),16);
    var g = parseInt(hexcolor.substr(2,2),16);
    var b = parseInt(hexcolor.substr(4,2),16);
    var match = ((r*299)+(g*587)+(b*114))/1000;
    return (match >= 128) ? '#000000' : '#ffffff';
  }

  selectedTabTest(event) {
    console.log('event=>>>>', event);
    if(event.tabTitle === 'Portal') {
      this.preview = true;
      this.selectTab = event.tabTitle;
      this.cdf.detectChanges();
    } else {
      this.preview = false;
      this.selectTab = event.tabTitle ? event.tabTitle : 'Admin';
      this.cdf.detectChanges();
    }
  }

  applyTheme(getArr) {
    const portalArr = getArr;
    for(let i = 0; i < portalArr.length; i++) {
      this.portalTheme[portalArr[i].compCode] = portalArr[i].codeValue;
    }
    this.sidemenuclr = this.getContrastYIQ(this.portalTheme.sidemenu);
    this.btnclr = this.getContrastYIQ(this.portalTheme.button);
  }
}
