import {Injectable, Inject} from '@angular/core';
import {Http, Response, Headers, RequestOptions, Request} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppConfig} from '../../app.module';

@Injectable()
export class LoginService {

   //busy: Promise<any>;
  private _urlUserData:string = "/api/usermaster/get_user_data"
  // private _url:string = "http://13.232.16.178/moodle/login/token.php" // live
  // private _url:string = "http://35.154.55.154/moodleTest/moodle/login/token.php" // Test 
  //private _url:string = "http://35.154.55.154/moodle/login/token.php" // Dev 
  
  //private _url:string = "http://35.154.55.154/moodle/login/token.php" // Dev 
   private _url:string = "https://finatics.in/moodleTest/moodle/login/token.php" // test
  // private _url:string = "https://www.edge-lms.co.in/moodle/login/token.php" // live


  // ${this.config.TEST_URL}
  
  // service="moodle_mobile_app"
  request: Request;

  constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig, private _http: Http){
      //this.busy = this._http.get('...').toPromise();
  }

  //@LoadingIndicator()
  getUserTableData(){
    return this._http.post(this._url,'')
      .map((response:Response) => response.json())
      .catch(this._errorHandler);
  }

  userData(user) {
     let url:any = `${this.config.FINAL_URL}`+this._urlUserData;
     // let url:any = `${this.config.DEV_URL}`+this._urlFilter;
     let headers = new Headers({ 'Content-Type': 'application/json' });
     let options = new RequestOptions({ headers: headers });
     //let body = JSON.stringify(user);
     return this._http.post(url, user, options ).map((res: Response) => res.json());
  }

  loginUser(user) {
    let Str = "username="+user.username+"&password="+user.password+"&service="+user.service;

    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions({ headers: headers });
    //let body = JSON.stringify(user);
    return this._http.post(this._url, Str, options ).map((res: Response) => res.json());
  }

  _errorHandler(error: Response){
    console.error(error);
    return Observable.throw(error || "Server Error")
  }

  public data: any;
}
