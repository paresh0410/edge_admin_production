import {
  Component,
  // Input,
  // ElementRef,
  OnInit,
  // ViewChild,
  ViewContainerRef,
} from "@angular/core";
import {
  // FormGroup,
  // AbstractControl,
  FormBuilder,
  // Validators
} from "@angular/forms";
// import {EmailValidator, EqualPasswordsValidator} from '../../theme/validators';
import { Router,
  //  NavigationStart
   } from "@angular/router";
import * as myGlobals from "../../app.module";
import { AppService } from "../../app.service";
// import { Http, Headers, Response, RequestOptions } from "@angular/http";
// import { Observable } from "rxjs";
// import { AppConfig } from "../../app.module";
import { feature } from '../../../environments/feature-environment';
import "rxjs/add/operator/do";
// import "rxjs/add/operator/map";
import { Compiler } from "@angular/core";
import { LoginService } from "./login.service";
import { AuthenticationService } from "../../service/authentication.service";
import { DataSeparatorService } from '../../service/data-separator.service';
import { BrandDetailsService } from '../../service/brand-details.service';

import {Md5} from 'ts-md5/dist/md5';
import { webApi } from '../../service/webApi';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CommonFunctionsService } from '../../service/common-functions.service';
@Component({
  selector: "login",
  templateUrl: "./login.html",
  styleUrls: ["./login.scss"]
})
export class Login implements OnInit {
  // @ViewChild(Ng2PopupComponent) popup: Ng2PopupComponent;
  show: boolean;
  query: string = "";
  featureConfig = feature;
  public getData;
  public login: any;
  // public form:FormGroup;
  // public email:AbstractControl;
  // public password:AbstractControl;
  public errorMessage: String;
  public error: boolean = false;
  public submitted: boolean = false;
  public pcname: any = [];
  public upcname: any = [];
  public cname: any = [];
  public msgs: any = [];
  public validdata: any = [];
  public idname: any;
  public ids: any = ["email", "password"];
  // userLoginData:any = [];
  spinner: boolean = false;
  errorMsg: string;
  contdata: any;
  userroleData: any;
  features: any = [];
  menus: any = [];
  accdata: any = [];
  meniutemp: any = [];
  notification: any = [
    {
      name: "Sms",
      flag:true,
      selectplace:'Select Template',
      Edittemp:'Edit Template',
    },
    {
      name: "Email",
      flag:true,
      selectplace:'Select Template',
      Edittemp:'Edit Template',
    },
    {
      name: "App Notification",
      flag:true,
      selectplace:'Select Template',
      Edittemp:'Edit Subject',
      Editbody:'Edit Body'

    },
  ];
  variables: any = {
    button: '--colorBgbtn',
    commonColor: '--colorCommon',
    header: '--colorHeader',
    icon: '--colorIcons',
    pageBackground: '--colorBgPage',
    sidemenu: '--colorSidebar',
  };
   themes = document.querySelector('body');
   currentBrandData: any;
  // private _spinner: BaThemeSpinner, public toastr: ToastsManager,rivate auth:AuthService,public utility : UtilityService
  constructor(
    fb: FormBuilder,
    private router: Router,
    private authenticationService: AuthenticationService,
    vRef: ViewContainerRef,
    private _compiler: Compiler,
    protected service1: LoginService,
    private AppService: AppService,
    public dataSeparatorService: DataSeparatorService,
    public brandService: BrandDetailsService,
    private spinnerNgx: NgxSpinnerService,
    private toastr: ToastrService,
    private commonFunctionsService: CommonFunctionsService,
  ) {
    // this.form = fb.group({
    //   'email': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
    //   'password': ['', Validators.compose([Validators.required, Validators.minLength(4)])]
    // });

    // this.email = this.form.controls['email'];
    // this.password = this.form.controls['password'];
    // window.localStorage.clear();
    //localStorage.removeItem(key);  for removing a single item
    //window.location.reload();
    var id = document.getElementById("nb-global-spinner");

    id.style.display = "none";

    this.login = [];
    console.log("myGlobals", myGlobals);
    // this._spinner.show();
    // this.toastr.setRootViewContainerRef(vRef);

    this._compiler.clearCache();
    this.show = false;



  }

  ngOnInit() {
    // this.utility.logged().then((result : boolean) => {
    //   if(result){
    //     this.router.navigate(['pages/dashboard']);
    //   }
    // })
    this.currentBrandData = this.brandService.getCurrentBrandData();
  }
  passwordEye() {
    this.show = !this.show;
  }

  showSuccess() {
    // this.toastr.success('You are awesome!', 'Success!');
  }

  userLoginData: any = [];
  // LoginData:any = [];
  // LoginResData:any = [];

  userData1: any = {
    email: "",
    password: ""
  };
  service: any = "moodle_mobile_app";

  loginUser(loginForm) {
    // this.spinner = true;
    this.error = false;
    if(loginForm.invalid){
      Object.keys(loginForm.controls).forEach(key => {
        loginForm.controls[key].markAsTouched();
      });
      return null;
    }
    this.spinnerNgx.show();
    this.notification;
    let userData = {
      username: this.userData1.email,
      password: this.userData1.password,
      // service: this.service
    };
    console.log("User Data ", userData);
    // this.service1.loginUser(userData).subscribe(
    //    data => {
    //       this.userLoginData = {
    //         username: this.userData1.email,
    //         password : this.userData1.password,
    //         data : data
    //       }
    //       console.log('this.userTableData', this.userLoginData);
    //       localStorage.setItem('LoginResData', JSON.stringify(this.userLoginData));
    //       if(data.error){
    //         this.spinner =false;
    //         this.error = true;
    //         this.errorMessage = 'Invalid Username or Password';
    //       }else{
    //         setTimeout(() => {
    //             this.spinner =false;
    //         },2000);
    //          this.userData(this.userLoginData);
    //         this.router.navigate(['pages/dashboard']);
    //       }
    //    },
    //    error => {
    //      this.spinner =false;
    //      console.error("Error login user!");
    //      this.errorMsg = error;
    //      this.error = true;
    //      this.errorMessage = 'Error login user!';
    //    }
    // );
    this.authenticationService.login(userData).subscribe(
      data => {
        this.userLoginData = {
          username: this.userData1.email,
          // password : this.userData1.password,
          data: data
        };




        this.spinnerNgx.hide();
        if (this.userLoginData.data.type == false) {
          // this.spinner = false;
          this.error = true;
          this.errorMessage = "Invalid Username or Password";
        } else {
          // setTimeout(() => {
          //   this.spinner = false;
          // }, 2000);

          // this.userData(this.userLoginData);
            //new dam reference setitem

        if(data['data'] && data['data']['damTabs'] && data['data']['damTabs']['assetPage']){
          localStorage.setItem(
            'damTabs',
            JSON.stringify(data['data']['damTabs']['assetPage'])
          );
        }

        if(data['data'] && data['data']['damTabs'] && data['data']['damTabs']['approvePage']){
          localStorage.setItem(
            'ApprovedamTabs',
            JSON.stringify(data['data']['damTabs']['approvePage'])
          );
        }

        if(data['data'] && data['data']['damTabs'] && data['data']['damTabs']['formatExtensions']){
          localStorage.setItem(
            'formatExtensions',
            JSON.stringify(data['data']['damTabs']['formatExtensions'])
          );
        }

        if(data['data'] && data['data']['damTabs'] && data['data']['damTabs']['isVisible']){
          localStorage.setItem(
            'isVisible',
            JSON.stringify(data['data']['damTabs']['isVisible'])
          );
        }
        if(data['data'] && data['data']['damTabs'] && data['data']['damTabs']['isAdmin']){
          localStorage.setItem(
            'isAdmin',
            JSON.stringify(data['data']['damTabs']['isAdmin'])
          );
        }

        if(data['data'] && data['data']['damTabs'] && data['data']['damTabs']['defaultValFlag']){
          localStorage.setItem(
            'defaultValFlag',
            JSON.stringify(data['data']['damTabs']['defaultValFlag'])
          );
        }
        // this.dataSeparatorService.
        localStorage.setItem(
          "LoginResData",
          JSON.stringify(this.userLoginData)
        );
        localStorage.setItem("Notification", JSON.stringify(this.notification));
        localStorage.setItem("token", this.userLoginData.data.token);
          console.log("this.userTableData", this.userLoginData);
          if(data['separators']){
            localStorage.setItem(
              'separators',
              JSON.stringify(data['separators'])
            );
            this.dataSeparatorService.setValuesToVariable(data['separators']);
          }
          this.makeUserRoleDataReady(this.userLoginData.data);
          console.log("Login res ", data);
          const res: any = data;
          if(res.theme) {
            const themes = res.theme;
              localStorage.setItem('theme', JSON.stringify(themes));
              for(let key in themes) {
                let value = themes[key];
                this.changeTheme(key,value);
              }
          }

          if(res['tenant_Info']){
            localStorage.setItem(
              'tenant_Info',
              JSON.stringify(res['tenant_Info'])
            );
          }else  {
            localStorage.setItem(
              'tenant_Info',
              JSON.stringify('')
            );
          }

          this.router.navigate(["pages/dashboard"]);
        }


      },
      error => {
        // this.spinner = false;
        this.spinnerNgx.hide();
        console.error("Error login user!");
        this.errorMsg = error;
        this.error = true;
        this.errorMessage = "Error login user!";
      }
    );
  }
  // disabled= false;
  checkDisabled() {
    if (this.userData1.password === '' && this.userData1.email === '') {
      return true;
    }
    else {
      return false;
    }
  }
  makeUserRoleDataReady(data) {
    this.features = [];
    this.menus = [];
    this.accdata = [];
    this.meniutemp = [];
    this.userroleData = data.otherdata;
    if (this.userroleData) {
      this.contdata = {
        roleId: this.userroleData[0][0].roleId,
        userId: this.userroleData[0][0].userId
      };
      if (this.userroleData[1]) {
        this.accdata = this.userroleData[1];
      }
      if (this.userroleData[2]) {
        for (let i = 0; i < this.userroleData[2].length; i++) {
          this.meniutemp.push(this.userroleData[2][i].featureIdentifier);
          this.meniutemp.push(this.userroleData[2][i].menuIdentifier);
        }
        this.features = Array.from(new Set(this.meniutemp));
      }
      if (this.userroleData[3]) {
        for (let i = 0; i < this.userroleData[3].length; i++) {
          // this.meniutemp.push(this.userroleData[2][i].featureIdentifier);
          this.meniutemp.push(this.userroleData[3][i]);
        }
        this.menus = Array.from(new Set(this.meniutemp));
      }
      localStorage.setItem('MenuList', JSON.stringify(this.menus));
      this.AppService.setuserdata(this.contdata);
      this.AppService.setmenus(this.menus);
      this.AppService.setfeatures(this.features);
    }
    console.log("this.menus", this.menus);
    console.log("this.userroleData ", this.userroleData);
    const userData = {
      email: data.data.email,
      firstname: data.data.firstname,
      id: data.data.id,
      lastname: data.data.lastname,
      username: data.data.username,
      tenantId: data.data.tenantId,
      roleId: this.userroleData[0][0].roleId,
      roleName: this.userroleData[0][0].roleName,
      token: data.token,
    }
    localStorage.setItem('currentUser', JSON.stringify(userData));
  }

  // userData(user) {
  //    this.service1.userData(user)
  //       .subscribe(data => {
  //           this.userroleData = data.data;
  //           if(this.userroleData)
  //           {
  //                this.contdata={
  //                     roleId:this.userroleData[0][0].roleId,
  //                     userId:this.userroleData[0][0].userId
  //                   }
  //           if(this.userroleData[2])
  //                 {
  //                      for(let i=0;i<this.userroleData[2].length;i++)
  //                   {
  //                     this.meniutemp.push(this.userroleData[2][i].featureIdentifier);
  //                     this.meniutemp.push(this.userroleData[2][i].menuIdentifier)
  //                   }
  //                   this.features= Array.from(new Set(this.meniutemp));
  //                 }

  //                 this.AppService.setuserdata(this.contdata);
  //                 this.AppService.setfeatures(this.features);
  //             if(this.userroleData[1])
  //               {
  //                 this.accdata =this.userroleData[1];
  //               }
  //           }
  //            console.log('this.userroleData ', this.userroleData);
  //       },
  //       error => {
  //           console.error("Reg Error !",error);
  //       });
  // }

  public saveUser = function (data, id) {
    let d = this.valid(data, id);
    let count = d[0];
    this.cname = [];
    this.msgs = [];
    this.cname = d[1];
    this.msgs = d[2];
    if (count === 0) {
      this.savedata(data);
      this.removeUser(id);
    } else {
      for (let i = 0; i < this.ids.length; i++) {
        this.idname = id + "-" + this.ids[i];
        document.getElementById(this.idname).innerHTML = "";
      }
      for (let i = 0; i < this.cname.length; i++) {
        this.idname = id + "-" + this.cname[i];
        let imsg = this.msgs[i];
        document.getElementById(this.idname).innerHTML = imsg;
      }
      this.pcname = this.cname;
      return "";
    }
  };

  public removeUser = function (empid) {
    for (let i = 0; i < this.ids.length; i++) {
      this.idname = empid + "-" + this.ids[i];
      document.getElementById(this.idname).innerHTML = "";
    }
    this.login = [];
  };

  public savedata = function (data) {
    this.error = false;
    this._spinner.show();
    console.log("this.login", this.login);
    if (this.login.email == "admin" && this.login.password == "admin") {
      this.router.navigate(["pages/dashboard"]);
      this._spinner.hide();
    } else {
      this._spinner.hide();
      this.error = true;
      this.errorMessage = "Invalid Username or Password";
    }
  };

  public valid = function (data, id) {
    let validatecount = 0;
    let msg = [];
    this.ename = [];
    let i = 0;
    if (data.email == "" || data.email == null || data.email == undefined) {
      msg[validatecount] = "Username Cannot Be Blank";
      validatecount++;
      this.ename.push("email");
    }
    if (
      data.password == "" ||
      data.password == null ||
      data.password == undefined
    ) {
      msg[validatecount] = "Password Cannot Be Blank";
      validatecount++;
      this.ename.push("password");
    }
    this.validdata = [];
    this.validdata.push(validatecount);
    this.validdata.push(this.ename);
    this.validdata.push(msg);
    return this.validdata;
  };

  gotoforgotpassword() {
    this.router.navigate(["forgot-password"]);
  }

  changeTheme(colorName, colorValue) {
    // Object.keys(this.variables).forEach(function(val) {
      // console.log('themes=========>>>>>>>>>>>', themes[key]);
      for(let val in this.variables) {
      if(val === colorName) {
        if (colorName === 'header') {
          this.themes.style.setProperty(this.variables[val], colorValue);
          const headerTxt = this.getContrastYIQ(colorValue);
          this.themes.style.setProperty('--colorHeadertxt', headerTxt);
        } else if (colorName === 'sidemenu') {
          this.themes.style.setProperty(this.variables[val], colorValue);
          const sidebarTxt = this.getContrastYIQ(colorValue);
          this.themes.style.setProperty('--colorSidebartxt', sidebarTxt);
        } else if (colorName === 'button') {
          this.themes.style.setProperty(this.variables[val], colorValue);
          const btnTxt = this.getContrastYIQ(colorValue);
          this.themes.style.setProperty('--colorBtnTxt', btnTxt);
        } else {
          this.themes.style.setProperty(this.variables[val], colorValue);
        }
      }
    }
    // });
  }

  getContrastYIQ(hexcolor){
    hexcolor = hexcolor.replace("#", "");
    var r = parseInt(hexcolor.substr(0,2),16);
    var g = parseInt(hexcolor.substr(2,2),16);
    var b = parseInt(hexcolor.substr(4,2),16);
    var match = ((r*299)+(g*587)+(b*114))/1000;
    return (match >= 128) ? '#000000' : '#ffffff';
  }

  signin: boolean = false;
  signUpDetails = {
    'firstName': '',
    'lastName':'',
    'email': '',
    'phone': '',
    'password': '',
    'confirmPassword': '',
    'termsAndCondition': false,
    'organization':'',
  }
  signIn(form) {

    this.router.navigate(["plans"]);
    // this.signin = !this.signin;
    // form.reset();
    // this.signUpDetails = {
    //   'firstName': '',
    //   'lastName':'',
    //   'email': '',
    //   'phone': '',
    //   'password': '',
    //   'confirmPassword': '',
    //   'termsAndCondition': false,
    //   'organization':'',
    // };
    // this.userData1 = {
    //   email: '',
    //   password: ''
    // };
    // this.mailSent = false;
    // this.count = 0;
  }

  sendVerificationLink(signupForm){
    if(signupForm.valid){
      console.log('Signup Data ==>', this.signUpDetails);
      console.log('Signup Data ==>', Md5.hashStr(this.signUpDetails.email));

      this.insertDataAndSendMail();
    }else {
    Object.keys(signupForm.controls).forEach(key => {
      signupForm.controls[key].markAsTouched();
    });
    console.log('Please fill all fields ==>', this.signUpDetails);
   }

  }

  mailSent = false;
  mailSentMessage = '';
  resendParameter = {};
  insertDataAndSendMail(){
    this.mailSent = false;
    this.mailSentMessage = '';
    this.resendParameter = {};
    const insertSignUpData = webApi.domain + webApi.url.insertSignUpData;
    this.spinnerNgx.show();
    const param = {
      firstName: this.signUpDetails.firstName,
      lastName: this.signUpDetails.lastName,
      userName: this.signUpDetails.email,
      phone: this.signUpDetails.phone,
      emailId: this.signUpDetails.email,
      uniqueId: Md5.hashStr(this.signUpDetails.email),
      password: this.signUpDetails.password,
      organization: this.signUpDetails.organization,
      link : 'domain-map/' + Md5.hashStr(this.signUpDetails.email),
      platform: 1,
    };
    // this.spinner.show();
    this.commonFunctionsService
      .httpPostRequest(insertSignUpData, param)
      // this.serveyService.getServey(param)
      .then(
        (rescompData) => {
          // this.spinner.hide();
          if(rescompData['type'] === true){
            this.mailSent = true;
            this.mailSentMessage = rescompData['message'];
            this.toastr.success(rescompData['message'], 'Success');
            param['shortLink'] = rescompData['link'];
            this.resendParameter = param;
          }
          this.spinnerNgx.hide();
          // this.router.navigate(['domain-map']);
          // console.log('addEditDiscountData ==>', rescompData);
          // if(rescompData && rescompData['type'] === true){
          //   if(rescompData && rescompData['data']){
          //   this.toastr.success(rescompData['data'][0].msg , 'Success');
          //   this.spinnerNgx.hide();
          // }else {
          //   this.toastr.warning('Something went wrong.', 'Warning');
          // }
        },
        (error) => {
          this.spinnerNgx.hide();
          this.toastr.warning('Something went wrong.', 'Warning');
        }
      );
  }

  count: number = 0;
  maxResendCount: number = 3;
  resendEmail(){
    if (this.count < this.maxResendCount) {
      this.mailSent = false;
      const resendVerificationMail = webApi.domain + webApi.url.resendVerificationMail;
      this.spinnerNgx.show();
      // this.spinner.show();
      this.commonFunctionsService
        .httpPostRequest(resendVerificationMail, this.resendParameter)
        // this.serveyService.getServey(param)
        .then(
          (rescompData) => {
            this.spinnerNgx.hide();
            if(rescompData['type'] === true){
              this.mailSent = true;
              this.mailSentMessage = rescompData['message'];
              this.toastr.success(rescompData['message'], 'Success');
              this.count ++;
            }
          },
          (error) => {
            this.spinnerNgx.hide();
            this.toastr.warning('Something went wrong.', 'Warning');
          }
        );
    }else {
      this.toastr.warning('Maximum resend count reached', 'Warning');
    }
  }
}
