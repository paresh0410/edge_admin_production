import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { AppTranslationModule } from '../../app.translation.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { NgaModule } from '../../theme/nga.module';

import { Login } from './login.component';
import { LoginService } from './login.service';
import { DemoMaterialModule } from '../material-module';
import { routing } from './login.routing';

// import {ToastModule} from 'ng2-toastr/ng2-toastr';
// import { UtilityService } from '../../shared/services/utility.service';
// import { Ng2PopupModule } from 'ng2-popup';

// import { Ng2OverlayModule } from 'ng2-overlay';

// import {PopupModule} from 'ng2-opd-popup';

// import {ModalModule} from "ng2-modal";

@NgModule({
  imports: [
    CommonModule,
    // AppTranslationModule,
    ReactiveFormsModule,
    FormsModule,
    // NgaModule,
    routing,
    DemoMaterialModule,
    // MatStepperModule,
    // ToastModule.forRoot()
  ],
  declarations: [
    Login
  ],
  providers: [
    LoginService,
    // UtilityService
  ]
})
export class LoginModule {}
