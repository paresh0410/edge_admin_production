import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  // {
  //   title: 'E-commerce',
  //   icon: 'nb-e-commerce',
  //   link: '/pages/dashboard',
  //   home: true,
  // },
  {
    title: 'Dashboard',
    icon: 'nb-home',
    link: '/pages/dashboard',
  },
  // {
  //   title: 'content-upload',
  //   icon: 'nb-home',
  //   link: '/pages/content-upload',
  // },
  {
    title: 'Settings',
    icon: 'nb-compose',
    link: '/pages/plan',
  },
  {
    title: 'Learning',
    icon: 'nb-lightbulb',
    link: '/pages/learning',
  },
  // {
  //   title: 'Enrollment',
  //   icon: 'nb-person',
  //   link: '/pages/enroll',
  // },
  {
    title: 'Assessment',
    icon: 'nb-plus-circled',
    link: '/pages/assessment',
  },
  {
    title: 'Reaction',
    icon: 'nb-plus-circled',
    link: '/pages/reactions',
  },
  {
    title: 'DAM',
    icon: 'nb-plus-circled',
    link: '/pages/dam',
  },
  {
    title: 'News',
    icon: 'nb-plus-circled',
    link: '/pages/news',
  },
  

  {
    title: 'Gamification',
    icon: 'nb-flame-circled',
    link: '/pages/gamification',
  },
  // {
  //   title: 'Enable',
  //   icon: 'nb-power',
  //   link: '/pages/iot-dashboard',
  // },
  // {
  //   title: 'Analytics',
  //   icon: 'nb-loop-circled',
  //   link: '/pages/evaluate/reporting',
  // },
  {
    title: 'Coaching',
    icon: 'nb-lightbulb',
    link: '/pages/coaching',
  },
  // {
  //   title: 'Task Manager',
  //   icon: 'fas fa-clipboard-list',
  //   link: '/pages/task-manager'
  // }
  // {
  //   title: 'Settings',
  //   icon: 'nb-gear',
  //   link: 'dashboard',
  // },
  // {
  //   title: 'SlikGrid',
  //   icon: 'nb-compose',
  //   link: 'slikgridDemo',
  // },

  //   {
  //   title: 'Auth',
  //   icon: 'nb-locked',
  //   children: [
  //     {
  //       title: 'Login',
  //       link: '/auth/login',
  //     },
  //     {
  //       title: 'Register',
  //       link: '/auth/register',
  //     },
  //     {
  //       title: 'Request Password',
  //       link: '/auth/request-password',
  //     },
  //     {
  //       title: 'Reset Password',
  //       link: '/auth/reset-password',
  //     },
  //   ],
  // },
  {
    title: 'Tools',
    icon: 'nb-lightbulb',
    link: '/pages/tools',
  },
];


// export const MENU_ITEMS: NbMenuItem[] = [
//   {
//     title: 'E-commerce',
//     icon: 'nb-e-commerce',
//     link: '/pages/dashboard',
//     home: true,
//   },
//   {
//     title: 'IoT Dashboard',
//     icon: 'nb-home',
//     link: '/pages/iot-dashboard',
//   },
//   {
//     title: 'FEATURES',
//     group: true,
//   },
//   {
//     title: 'UI Features',
//     icon: 'nb-keypad',
//     link: '/pages/ui-features',
//     children: [
//       {
//         title: 'Buttons',
//         link: '/pages/ui-features/buttons',
//       },
//       {
//         title: 'Grid',
//         link: '/pages/ui-features/grid',
//       },
//       {
//         title: 'Icons',
//         link: '/pages/ui-features/icons',
//       },
//       {
//         title: 'Modals',
//         link: '/pages/ui-features/modals',
//       },
//       {
//         title: 'Popovers',
//         link: '/pages/ui-features/popovers',
//       },
//       {
//         title: 'Typography',
//         link: '/pages/ui-features/typography',
//       },
//       {
//         title: 'Animated Searches',
//         link: '/pages/ui-features/search-fields',
//       },
//       {
//         title: 'Tabs',
//         link: '/pages/ui-features/tabs',
//       },
//     ],
//   },
//   {
//     title: 'Forms',
//     icon: 'nb-compose',
//     children: [
//       {
//         title: 'Form Inputs',
//         link: '/pages/forms/inputs',
//       },
//       {
//         title: 'Form Layouts',
//         link: '/pages/forms/layouts',
//       },
//     ],
//   },
//   {
//     title: 'Components',
//     icon: 'nb-gear',
//     children: [
//       {
//         title: 'Tree',
//         link: '/pages/components/tree',
//       }, {
//         title: 'Notifications',
//         link: '/pages/components/notifications',
//       },
//     ],
//   },
//   {
//     title: 'Maps',
//     icon: 'nb-location',
//     children: [
//       {
//         title: 'Google Maps',
//         link: '/pages/maps/gmaps',
//       },
//       {
//         title: 'Leaflet Maps',
//         link: '/pages/maps/leaflet',
//       },
//       {
//         title: 'Bubble Maps',
//         link: '/pages/maps/bubble',
//       },
//       {
//         title: 'Search Maps',
//         link: '/pages/maps/searchmap',
//       },
//     ],
//   },
//   {
//     title: 'Charts',
//     icon: 'nb-bar-chart',
//     children: [
//       {
//         title: 'Echarts',
//         link: '/pages/charts/echarts',
//       },
//       {
//         title: 'Charts.js',
//         link: '/pages/charts/chartjs',
//       },
//       {
//         title: 'D3',
//         link: '/pages/charts/d3',
//       },
//     ],
//   },
//   {
//     title: 'Editors',
//     icon: 'nb-title',
//     children: [
//       {
//         title: 'TinyMCE',
//         link: '/pages/editors/tinymce',
//       },
//       {
//         title: 'CKEditor',
//         link: '/pages/editors/ckeditor',
//       },
//     ],
//   },
//   {
//     title: 'Tables',
//     icon: 'nb-tables',
//     children: [
//       {
//         title: 'Smart Table',
//         link: '/pages/tables/smart-table',
//       },
//     ],
//   },
//   {
//     title: 'Miscellaneous',
//     icon: 'nb-shuffle',
//     children: [
//       {
//         title: '404',
//         link: '/pages/miscellaneous/404',
//       },
//     ],
//   },
//   {
//     title: 'Auth',
//     icon: 'nb-locked',
//     children: [
//       {
//         title: 'Login',
//         link: '/auth/login',
//       },
//       {
//         title: 'Register',
//         link: '/auth/register',
//       },
//       {
//         title: 'Request Password',
//         link: '/auth/request-password',
//       },
//       {
//         title: 'Reset Password',
//         link: '/auth/reset-password',
//       },
//     ],
//   },
// ];
