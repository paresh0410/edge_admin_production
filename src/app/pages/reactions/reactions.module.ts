import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { NgxEchartsModule } from 'ngx-echarts';
import { ThemeModule } from '../../@theme/theme.module';
import { ReactionsComponent } from './reactions.component'

import { ReactiveFormsModule } from '@angular/forms';

import { CommonModule } from '@angular/common';
import { SortablejsModule } from 'angular-sortablejs';
import { ComponentModule } from '../../component/component.module';

// import { CourseBundleModule } from './courseBundle/courseBundle.module';


@NgModule({
  imports: [
    ThemeModule,
    NgxEchartsModule,
    CommonModule,
    SortablejsModule,
    ReactiveFormsModule,
    ComponentModule,
    // CourseBundleModule
  ],
  declarations: [
    ReactionsComponent,
    // CourseBundle
  ],
  providers: [
  ],
  schemas: [ 
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA 
  ]
})
export class ReactionsModule { }
