import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from '../../app.service';
import { AddeditfeedbackService } from '../assessment/feedback/addEditfeedback/addEditfeedback.service';
import {AddeditsurveyService} from '../assessment/survey/addEditsurvey/addEditsurvey.service';
import {AddeditpoleService} from '../assessment/pole/addEditpole/addEditpole.service';
import { SuubHeader } from '../components/models/subheader.model';
@Component({
  selector: 'ngx-reactions',
  templateUrl: './reactions.component.html',
  styleUrls: ['./reactions.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ReactionsComponent implements OnInit {
  showdata: any = [];
  learnData: any = [];
  constructor(public router: Router, private AppService: AppService, public routes: ActivatedRoute,
    public addEditFeedbackService: AddeditfeedbackService ,
    public addEditSurveyService: AddeditsurveyService,
    public  addEditPoleService: AddeditpoleService) {
    this.showdata = this.AppService.getmenus();
    if (this.showdata) {
      for (let i = 0; i < this.showdata.length; i++) {
        if (Number(this.showdata[i].parentMenuId) === 4) {
          this.learnData.push(this.showdata[i]);
        }
      }
    }
  }
  header: SuubHeader  = {
    title:'Reaction',
    showBreadcrumb:true,
    breadCrumbList:[]
  };
  ngOnInit() {
  }

  gotopages(url, item) {
    if (item.menuId){
      this.addEditFeedbackService.menuId = item.menuId;
      this.addEditPoleService.menuId = item.menuId;
      this.addEditSurveyService.menuId = item.menuId;
      // this.preonaddEditCourseContentService.menuId = item.menuId;
      localStorage.setItem('menuId', item.menuId);
    }
    this.router.navigate([url], { relativeTo: this.routes });
  }
  // gotosurvey() {
  //   this.router.navigate(['survey'], { relativeTo: this.routes });
  // }
  // gotopole() {
  //   this.router.navigate(['poll'], { relativeTo: this.routes });
  // }
  // gotofeedback() {
  //   this.router.navigate(['feedback'], { relativeTo: this.routes });
  // }

}
