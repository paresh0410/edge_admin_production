
import { throwError as observableThrowError, Observable, from } from 'rxjs';
import { Injectable, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import { AppConfig } from '../../app.module';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { HttpClientModule, HttpErrorResponse } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { webApi } from '../../service/webApi';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonFunctionsService } from "../../service/common-functions.service";

@Injectable({
  providedIn: 'root',
})
export class ReportsService {
  courseName: any;
  ReportName: string;
  menuId: any;
  scheduleId: any;
  ReportTitle: string;
  CacheReportFilter: any;

  Report: any = {
    FMReport: [],
    FMReportList: [],
    FMReportName: null,
    ReportFilter: {}
  }
  // Display Report List And Schedule Flags
  displayScheduledList: boolean;
  displayReportList: boolean;

  direct_redirect: boolean = false;
  private _urlFilter: string = '/api/dashboard/induction_emp_filter'; // Dev
  private _getEmpInduction: string = webApi.url.getEMPInduction;
  private _reportfetchall: string = webApi.url.report_fetch_all;
  private _report_course_consumption_fetch_filter: string = webApi.url.report_course_consumption_fetch_filter;
  private _report_fetch_course_consumption_list: string = webApi.url.report_fetch_course_consumption_list;
  private _report_fetch_course_consumption_list_for_quiz: string = webApi.url.report_fetch_course_consumption_list_for_quiz;
  private _report_fetch_feedback_course_consumption_list: string = webApi.url.report_fetch_feedback_course_consumption_list;
  private _report_fetch_course_consumption: string = webApi.url.report_fetch_course_consumption;
  private _report_fetch_course_consumption_download: string = webApi.url.report_fetch_course_consumption_download;
  private _report_insert: string = webApi.url.report_insert;
  private _report_update: string = webApi.url.report_update;
  private _schedule_dropdown: string = webApi.url.schedule_dropdown;
  private add_schedule_immediate_url: string = webApi.url.add_scheduled_immediate;
  private schedule_detail_url: string = webApi.url.schedule_detail;
  // Feedback report urls
  private report_fetch_feedback_course_consumption_activitywise: string
              = webApi.url.report_fetch_feedback_consumption_activitywise;
  // Quiz - consumption
  private report_fetch_course_consumption_list_Quiz_Only:string
    =webApi.url.report_fetch_course_consumption_list_Quiz_Only
    //quiz-Activity
    private report_fetch_quiz_consumption_activitywise:string
    =webApi.url.report_fetch_quiz_consumption_activitywise
  // private report_fetch_feedback_course_consumption_list: string = webApi.url.report_fetch_feedback_consumption;
  private report_fetch_feedback_consumption_download: string = webApi.url.report_fetch_feedback_consumption_download;
  // Activity report urls
  private url_report_fetch_course_activity_consumption: string = webApi.url.report_fetch_course_activity_consumption;
  private url_report_fetch_course_activity_consumption_download: string = webApi.url.report_fetch_course_activity_consumption_download;

  // Quiz report urls
  private url_report_fetch_quiz_consumption: string = webApi.url.report_fetch_quiz_consumption;
  private url_report_fetch_quiz_consumption_download: string = webApi.url.report_fetch_quiz_consumption_download;
  private url_report_quiz_consumption_quiestion_wise: string = webApi.url.report_fetch_quiz_consumption_quiestion_wise;
  private url_report_quiz_consumption_quiestion_wise_download: string = webApi.url.report_fetch_quiz_consumption_quiestion_wise_download;

  //Employee Attendance
  private url_report_fetch_emp_attendance: string = webApi.url.report_fetch_emp_attendance;
  private url_report_fetch_emp_attendance_download: string = webApi.url.report_fetch_emp_attendance_download;

  // Employee detail Consumption
  private url_report_emplyee_consumption: string = webApi.url.report_emplyee_consumption;
  private url_report_type_employee_consumption: string = webApi.url.report_emplyee_consumption_list;
  private url_report_emplyee_consumption_download: string = webApi.url.report_emplyee_consumption_download;

  //workflow reports
  private url_report_fetch_workflow_list: string = webApi.url.report_fetch_workflow_list;
  private url_report_workflow_filter: string = webApi.url.report_workflow_filters;
  private url_get_workflow_report: string = webApi.url.report_workflow_structure;
  private url_download_workflow_report: string = webApi.url.report_workflow_download;
  private url_get_workflow_detail_report: string = webApi.url.report_workflow_detail;
  private url_get_workflow_consumption_report: string = webApi.url.report_workflow_consumption;
  private url_download_workflow_consumption_report: string = webApi.url.report_workflow_consumption_download;


  // TTT reports
  private url_report_fetch_trainer_details: string = webApi.url.report_ttt_trainer_detail;
  private url_report_fetch_TTT_workflow_list: string = webApi.url.report_fetch_TTT_workflow_list;
  private url_report_fetch_TTT_workflow_reports: string = webApi.url.report_fetch_TTT_workflow_reports;
  private url_report_fetch_TTT_workflow_reports_download: string = webApi.url.report_fetch_TTT_workflow_reports_download;
  private url_report_fetch_TTT_Steps_reports: string = webApi.url.report_fetch_TTT_Steps_reports;
  private url_report_fetch_TTT_Steps_reports_download: string = webApi.url.report_fetch_TTT_Steps_reports_download;
  private url_report_fetch_TTT_nominee__detail: string = webApi.url.report_fetch_TTT_nominee_detail;
  private url_report_fetch_TTT_nominee__detail_downlaod: string = webApi.url.report_fetch_TTT_nominee_detail_download;
  private url_report_fetch_TTT_bh_approval: string = webApi.url.report_fetch_TTT_bh_approval;
  private url_report_fetch_TTT_bh_approval_download: string = webApi.url.report_fetch_TTT_bh_approval_download;
  private Url_report_ttt_stepwise: string = webApi.url.report_ttt_stepwise;
  // TA reports

  private url_fetch_TA_reports: string = webApi.url.reports_TA_reports;
  private url_fetch_TA_Batch_wise_reports: string = webApi.url.reports_TA_GET_BATCHWISE_REPORT;
  private url_fet_TA_EMP_reports: string = webApi.url.reports_TA_GET_EMPWISE_REPORT;

  private url_fetch_program_list: string = webApi.url.reports_TA_Fetch_ProgramsList;
  private url_fetch_batch_list: string = webApi.url.reports_TA_Fetch_BatchList;
  private url_fetch_TA_filters: string = webApi.url.reports_TA_filters;

  private url_download_ta_reports: string = webApi.url.reports_TA_reports_download;
  private url_download_ta_Batchwise_reports: string = webApi.url.reports_TA_GET_BATCHWISE_REPORT_DOWNLOAD;
  private url_download_emp_ta_reports: string = webApi.url.reports_TA_GET_EMPWISE_REPORT_DOWNLOAD;

  // Pre Onboarding reports
  private url_get_report_preonboarding_report: string = webApi.url.report_preonboarding_report;

  //consolidated reports
  private url_get_report_consolidated_report: string = webApi.url.report_consolidated_report;

  //consolidated workflow reports
  private url_get_consolidated_workflow:string = webApi.url.report_consolidated_workflow_report;

  //consolidated classroom attendance reports
  private url_get_consolidated_attendance:string = webApi.url.report_consolidated_classroom_attendance_report;

  //consolidated classroom feedback reports
  private url_get_consolidated_feedback:string = webApi.url.report_consolidated_classroom_feedback_report;



  //Quiz summary reports
  private url_get_quiz_summary_report: string = webApi.url.report_quiz_summary_report;

  //Employee list
  private url_report_fetch_emp_list: string = webApi.url.report_fetch_emp_list;
  private url_report_fetch_emp_list_download: string = webApi.url.url_report_fetch_emp_list_download;
  private _fetch_All_course_employee_list: string = webApi.url.fetch_course_employee_list;

  // Get Report List To Download
  private url_fetch_report_download_list: string = webApi.url.fetch_report_download_list;
  private url_fetch_report_scheduleList_list: string = webApi.url.fetch_report_scheduleList_list;
  private url_enable_disable_scedule_List: string = webApi.url.enable_disable_scedule_List;

  // Quiz Cumulative Employee score report
  private url_report_quiz_cumulative: string = webApi.url.report_quiz_cumulative;
  private url_report_quiz_cumulative_download: string = webApi.url.report_quiz_cumulative_download;

  //survey report
  private getSurvey_List: string = webApi.url.getSurveyList
  private getSurvey_Report: string = webApi.url.getSurveyReport

  //poll report
  private getPoll_List: string = webApi.url.getPollList
  private getPoll_Report: string = webApi.url.getPollReport

  //eep report
  private geteep_List:string = webApi.url.geteepList
  private geteep_Report:string = webApi.url.getnominatedList

  //dam report
  private getdamreport_list:string=webApi.url.report_get_dam_category_list
  private getdamreport_Report:string=webApi.url.report_get_dam_asset_mapping
 private getdamreport_VersionReport:string=webApi.url.report_get_dam_asset_version_details
  constructor(@Inject('APP_CONFIG_TOKEN') private config: AppConfig, private _http: Http,
  private commonFunctionService: CommonFunctionsService,
    private httpClient: HttpClientModule, private http1: HttpClient, private spinner: NgxSpinnerService) {
    // this.busy = this._http.get('...').toPromise();
  }
  setmenuId(id) {
    this.menuId = id;
  }
  getmenuId() {
    return this.menuId;
  }
  setReportname(name) {
    this.ReportName = name;
  }
  setCacheReportFilter(value) {
    this.CacheReportFilter = value;
  }
  getCacheReportFilter() {
    return this.CacheReportFilter;
  }
  getReportname() {
    return this.ReportName;
  }
  setDirectRedirect(flag) {
    this.direct_redirect = flag; // true/false
  }
  getDirectRedirect() {
    return this.direct_redirect; // true/false
  }
  getUsers(user) {
    let url: any = `${this.config.FINAL_URL}` + this._getEmpInduction;
    return new Promise(resolve => {
      this.http1.post(url, user)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  GetReportList(param) {
    let url: any = `${this.config.FINAL_URL}` + this._reportfetchall;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
enableDisableReport(param) {
    let url: any = `${this.config.FINAL_URL}` + this.url_enable_disable_scedule_List;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  GetFilterInfo(param) {
    let url: any = `${this.config.FINAL_URL}` + this._report_course_consumption_fetch_filter;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  GetCourseConsumptionList(param) {
    // this.spinner.show();
    // let url: any = `${this.config.FINAL_URL}` + this._report_fetch_course_consumption_list;
    let url:any;
    if(param['menuId'] && (param['menuId'] == 48 || param['menuId'] == 60)){
      url = `${this.config.FINAL_URL}` + this._report_fetch_feedback_course_consumption_list;
    } else {
     url= `${this.config.FINAL_URL}` + this._report_fetch_course_consumption_list;
    }
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  GetReportCourseConsumption(param) {
    let url: any ;
    if(param['menuId'] && (param['menuId'] == 48 || param['menuId'] == 60)){
      url = `${this.config.FINAL_URL}` + this._report_fetch_feedback_course_consumption_list;
    } else {
     url= `${this.config.FINAL_URL}` + this._report_fetch_course_consumption;
    }


    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  GetQuizCourseConsumption(param) {
    let url: any ;
    if(param['menuId'] && (param['menuId'] == 48 || param['menuId'] == 60)){
      url = `${this.config.FINAL_URL}` + this._report_fetch_feedback_course_consumption_list;
    } else if(param['menuId'] &&
      (param['menuId'] == 59 || param['menuId'] == 111 || param['menuId'] == 47 || param['menuId'] == 108 ||
      param['menuId'] == 116 )){
      url= `${this.config.FINAL_URL}` + this.report_fetch_course_consumption_list_Quiz_Only;
     }
     else if(param['menuId'] &&
      (param['menuId'] == 120 || param['menuId'] == 119 )){
      url= `${this.config.FINAL_URL}` + this._report_fetch_course_consumption_list_for_quiz;
     }else {
      url= `${this.config.FINAL_URL}` + this._report_fetch_course_consumption_list;
    }


    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }


  /* Feedback get reports */
  GetReportFeedbackConsumption(param) {
    // let url: any = `${this.config.FINAL_URL}` + this.report_fetch_feedback_consumption;
     let url: any = `${this.config.FINAL_URL}` + this.report_fetch_feedback_course_consumption_activitywise;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  GetQuizActivityConsumption(param){
    let url: any = `${this.config.FINAL_URL}` + this.report_fetch_quiz_consumption_activitywise;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }

  // GetCourseFeedbackCourseList(param){
  //   let url: any = `${this.config.FINAL_URL}` + this._report_fetch_feedback_course_consumption_list;
  //   return new Promise(resolve => {
  //     this.http1.post(url, param)
  //       .subscribe(data => {
  //         resolve(data);
  //       },
  //         err => {
  //           resolve('err');
  //         });
  //   });
  // }


  /* Quiz get reports */

  GetReportQuizConsumption(param) {
    let url: any = `${this.config.FINAL_URL}` + this.url_report_fetch_quiz_consumption;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  GetReportQuizConsumption_quiestion_wise(param) {
    let url: any = `${this.config.FINAL_URL}` + this.url_report_quiz_consumption_quiestion_wise;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  /* Activity get reports */

  GetReportActivityConsumption(param) {
    let url: any = `${this.config.FINAL_URL}` + this.url_report_fetch_course_activity_consumption;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  /* Download Course */

  GetReportCourseConsumptionDownload(param) {
    let url: any = `${this.config.FINAL_URL}` + this._report_fetch_course_consumption_download; // + '?courseIds=' + param.courseIds;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  /* Feedback download reports */
  GetReportFeedbackConsumptionDownload(param) {
    let url: any = `${this.config.FINAL_URL}` + this.report_fetch_feedback_consumption_download;
    // + '?courseIds=' + param.courseIds;

    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }


  /* Quiz download reports */
  GetReportQuizConsumptionDownload(param) {
    let url: any = `${this.config.FINAL_URL}` + this.url_report_fetch_quiz_consumption_download;
    // + '?courseIds=' + param.courseIds;

    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  GetReportQuiz_questioniConsumptionDownload(param) {
    let url: any = `${this.config.FINAL_URL}` + this.url_report_quiz_consumption_quiestion_wise_download;
    // + '?courseIds=' + param.courseIds;

    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  /* Activity download reports */
  GetReportActivityConsumptionDownload(param) {
    let url: any = `${this.config.FINAL_URL}` + this.url_report_fetch_course_activity_consumption_download;
    // + '?courseIds=' + param.courseIds;

    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  /** Employee attendance */

  GetReportEmployeeAttendDownload(param) {
    let url: any = `${this.config.FINAL_URL}` + this.url_report_fetch_emp_attendance_download;
    // + '?courseIds=' + param.courseIds;

    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  GetReportEmployeeAttendance(param) {
    let url: any = `${this.config.FINAL_URL}` + this.url_report_fetch_emp_attendance;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  _errorHandler(error: Response) {
    console.error(error);
    return null;// observableThrowError(error || 'Server Error');
  }

  //Employe list
  GetCourseEmployeeList(param) {
    // this.spinner.show();
    let url=""
    if(this.menuId==152){
     url = `${this.config.FINAL_URL}` + this.getdamreport_list;
    }
    else{
      url = `${this.config.FINAL_URL}` + this._fetch_All_course_employee_list;
    }
      return new Promise(resolve => {
        this.http1.post(url, param)
          .subscribe(data => {
            resolve(data);
          },
            err => {
              resolve('err');
            });
      });
      
  }
  GetReportEmployeeList(param) {
    let url =""
    if(this.menuId==152){
       url = `${this.config.FINAL_URL}` + this.getdamreport_Report;
      }else{
        url = `${this.config.FINAL_URL}` + this.url_report_fetch_emp_list;
      }
      return new Promise(resolve => {
        this.http1.post(url, param)
          .subscribe(data => {
            resolve(data);
          },
            err => {
              resolve('err');
            });
      });  
  }
  GetReportDamList(param) {
      let url: any = `${this.config.FINAL_URL}` + this.getdamreport_VersionReport;
      return new Promise(resolve => {
        this.http1.post(url, param)
          .subscribe(data => {
            resolve(data);
          },
            err => {
              resolve('err');
            });
      });
    }
  GetReportEmployeeListDownload(param) {
    let url: any = `${this.config.FINAL_URL}` + this.url_report_fetch_emp_list_download;
    // + '?courseIds=' + param.courseIds;

    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  /**SET Report Data */
  SET_Report_Filter_Values(Report) {
    this.Report = {
      FMReport: Report.FMReport,
      FMReportList: Report.FMReportList,
      FMReportName: Report.FMReportName,
      ReportFilter: Report.ReportFilter
    }
  }
  /**GET Report Data */
  GET_Report_Filter_Values() {
    return this.Report;
  }

  /***
   * @description To insert formated report by user
   * @param param;
   * param = {
   *    report_name: 'Example name',
   *    metadata: '{Json}',
   *    menuId: 44
   * }
   */
  Insert_Report(param) {
    let url: any = `${this.config.FINAL_URL}` + this._report_insert;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  /***
  * @description To update existing formated report by user
  * @param param;
  * param = {
  *    report_id: 1,
  *    report_name: 'Example name',
  *    metadata: '{Json}',
  *    menuId: 44,
  *    isActive: 1
  * }
  */
  Update_Report(param) {
    let url: any = `${this.config.FINAL_URL}` + this._report_update;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  /**
   * Classroom
   */
  GetClassroomReportByMenuIdTimeOut(param, menuId, cb) {
    let url = ""
    if (menuId === 57) {
      //  url= `${this.config.FINAL_URL}` + this._report_fetch_course_consumption;
       if(param['menuId'] && (param['menuId'] == 48 || param['menuId'] == 60)){
        url = `${this.config.FINAL_URL}` + this._report_fetch_feedback_course_consumption_list;
      } else {
       url= `${this.config.FINAL_URL}` + this._report_fetch_course_consumption;
      }
    } else if (menuId === 58) {
       url = `${this.config.FINAL_URL}` + this.url_report_fetch_course_activity_consumption;
    } else if (menuId === 59) {
       url = `${this.config.FINAL_URL}` + this.report_fetch_quiz_consumption_activitywise;
    } else if (menuId === 60) {
       url = `${this.config.FINAL_URL}` + this.report_fetch_feedback_course_consumption_activitywise;
    } else if (menuId === 111) {
       url = `${this.config.FINAL_URL}` + this.url_report_quiz_consumption_quiestion_wise;
    } else if (menuId === 120) {
       url = `${this.config.FINAL_URL}` + this.url_report_quiz_cumulative;
    } else {
      cb([]);
    }
    this.commonFunctionService.reportPostRequestTimeOut(url, param).subscribe( (res) =>{
      console.log("res", res);
      cb(res);
    },
    error => {
        console.log('ERROR', error);
        // if(error === 'Timeout Exception'){

        // }
        cb(error);
      });
  }

  GetClassroomReportByMenuId(param, menuId, cb) {
    if (menuId === 57) {
      this.GetReportCourseConsumption(param).then((result) => {
        cb(result);
      }).catch((e) => {
        cb([]);
      })
    } else if (menuId === 58) {
      this.GetReportActivityConsumption(param).then((result) => {
        cb(result);
      }).catch((e) => {
        cb([]);
      })
    } else if (menuId === 59) {
      this.GetQuizActivityConsumption(param).then((result) => {
        cb(result);
      }).catch((e) => {
        cb([]);
      })
    } else if (menuId === 60) {
      this.GetReportFeedbackConsumption(param).then((result) => {
        cb(result);
      }).catch((e) => {
        cb([]);
      })
    } else if (menuId === 111) {
      this.GetReportQuizConsumption_quiestion_wise(param).then((result) => {
        cb(result);
      }).catch((e) => {
        cb([]);
      })
    } else if (menuId === 120) {
      this.GetReportQuizCumulative(param).then((result) => {
      //this.GetReportQuizConsumption_quiestion_wise(param).then((result) => {
        cb(result);
      }).catch((e) => {
        cb([]);
      })
    } else {
      cb([]);
    }
  }

  GetClassroomReportDownloadByMenuId(param, menuId, cb) {
    if (menuId === 57) {
      this.GetReportCourseConsumptionDownload(param).then((result) => {
        cb(result);
      }).catch((e) => {
        cb([]);
      })
    } else if (menuId === 58) {
      this.GetReportActivityConsumptionDownload(param).then((result) => {
        cb(result);
      }).catch((e) => {
        cb([]);
      })
    } else if (menuId === 59) {
      this.GetReportQuizConsumptionDownload(param).then((result) => {
        cb(result);
      }).catch((e) => {
        cb([]);
      })
    } else if (menuId === 60) {
      this.GetReportFeedbackConsumptionDownload(param).then((result) => {
        cb(result);
      }).catch((e) => {
        cb([]);
      })
    } else if (menuId === 111) {
      this.GetReportQuizConsumption_quiestion_wise(param).then((result) => {
        cb(result);
      }).catch((e) => {
        cb([]);
      })
    } else if (menuId === 120) {
      this.GetReportQuizCumulative_download(param).then((result) => {
        cb(result);
      }).catch((e) => {
        cb([]);
      })
    } else {
      cb([]);
    }
  }

  GetWorkflowStructureList(param) {
    // this.spinner.show();
    let url: any = `${this.config.FINAL_URL}` + this.url_report_fetch_workflow_list;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  GetWorkflowFilterInfo(param) {
    let url: any = `${this.config.FINAL_URL}` + this.url_report_workflow_filter;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  GetReportWorkflowConsumption(param) {
    let url: any = `${this.config.FINAL_URL}` + this.url_get_workflow_report;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  GetReportWorkflowDetail(param) {
    let url: any = `${this.config.FINAL_URL}` + this.url_get_workflow_detail_report;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }



  GetWorkflowReportByMenuId(param, menuId, cb) {
    if (menuId === 61) {
      this.GetReportWorkflowConsumption(param).then((result) => {
        cb(result);
      }).catch((e) => {
        cb([]);
      })
    } else if (menuId === 79) {
      this.GetWorkflowConsumpReport(param).then((result) => {
        cb(result);
      }).catch((e) => {
        cb([]);
      })
    }
  }

  GetReportWorkflowDownload(param) {
    let url: any = `${this.config.FINAL_URL}` + this.url_download_workflow_report; // + '?courseIds=' + param.courseIds;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  GetWorkflowReportDownloadByMenuId(param, menuId, cb) {
    if (menuId === 61) {
      this.GetReportWorkflowDownload(param).then((result) => {
        cb(result);
      }).catch((e) => {
        cb([]);
      })
    } else if (menuId === 79) {
      this.GetReportWorkflowConsumptionDownload(param).then((result) => {
        cb(result);
      }).catch((e) => {
        cb([]);
      })
    }
  }


  GetWorkflowConsumpReport(param) {
    let url: any = `${this.config.FINAL_URL}` + this.url_get_workflow_consumption_report;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  GetReportWorkflowConsumptionDownload(param) {
    let url: any = `${this.config.FINAL_URL}` + this.url_download_workflow_consumption_report;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  // ******************** TTT reports ****************** //

  Get_TTT_Report_ByMenuId(param, menuId, cb) {
    if (menuId) {
      this.GetTTTreports(param).then((result) => {
        cb(result);
      }).catch((e) => {
        cb([]);
      })
    }
    // if (menuId === 92) {
    //   this.GetTTTWorkflowConsumpReport(param).then((result) => {
    //     cb(result);
    //   }).catch((e) => {
    //     cb([]);
    //   })
    // } else if (menuId === 93 || menuId === 94 || menuId === 95 || menuId === 96 || menuId === 97 || menuId === 98) {
    //   this.GetTTTStepsConsumpReport(menuId, param).then((result) => {
    //     cb(result);
    //   }).catch((e) => {
    //     cb([]);
    //   })
    // }
  }
  GetTTTreports(param) {
    let url: any = `${this.config.FINAL_URL}` + this.Url_report_ttt_stepwise;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  Get_TTT_Report_Download_ByMenuId(param, menuId, cb) {
    if (menuId === 92) {
      this.GetTTTWorkflowConsumpReportDownload(param).then((result) => {
        cb(result);
      }).catch((e) => {
        cb([]);
      })
    } else if (menuId === 93 || menuId === 94 || menuId === 95 || menuId === 96 || menuId === 97 || menuId === 98) {
      this.GetTTTStepsConsumpReportDownload(menuId, param).then((result) => {
        cb(result);
      }).catch((e) => {
        cb([]);
      })
    }
  }

  GetTTTWorkflowConsumpList(param) {
    let url: any = `${this.config.FINAL_URL}` + this.url_report_fetch_TTT_workflow_list;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  GetTTT_Trainer_Reports(param) {
    // this.spinner.show();
    let url: any = `${this.config.FINAL_URL}` + this.url_report_fetch_trainer_details;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  GetTTTWorkflowConsumpReport(param) {
    let url: any = `${this.config.FINAL_URL}` + this.url_report_fetch_TTT_workflow_reports;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  GetTTTWorkflowConsumpReportDownload(param) {

    let url: any = `${this.config.FINAL_URL}` + this.url_report_fetch_TTT_workflow_reports_download;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }


  GetTTTStepsConsumpReport(menuId, param) {
    const url = this.findurl(menuId, 1);
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  GetTTTStepsConsumpReportDownload(menuId, param) {
    const url = this.findurl(menuId, 2);
    // let url: any = `${this.config.FINAL_URL}` + this.url_report_fetch_TTT_Steps_reports_download;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  findurl(menuId, status) {
    let reportURl = '';
    let reportdownloadURl = '';
    if (menuId === 94) {
      reportURl = `${this.config.FINAL_URL}` + this.url_report_fetch_TTT_nominee__detail;
      reportdownloadURl = `${this.config.FINAL_URL}` + this.url_report_fetch_TTT_nominee__detail_downlaod;
    } else if (menuId === 95) {
      reportURl = `${this.config.FINAL_URL}` + this.url_report_fetch_TTT_bh_approval;
      reportdownloadURl = `${this.config.FINAL_URL}` + this.url_report_fetch_TTT_bh_approval_download;
    } else {
      reportURl = `${this.config.FINAL_URL}` + this.url_report_fetch_TTT_Steps_reports;
      reportdownloadURl = `${this.config.FINAL_URL}` + this.url_report_fetch_TTT_Steps_reports_download;
    }
    if (status === 1) {
      return reportURl;
    } else if (status === 2) {
      return reportdownloadURl;
    }

  }



  // ********************* TA reports **************************** //


  //get reports by menu id
  GetTAReportByMenuId(param, menuId, cb) {
    if (menuId === 87) {
      this.GetTAReportService(param).then((result) => {
        cb(result);
      }).catch((e) => {
        cb([]);
      })
    } else if (menuId === 88) {
      this.GetTAReportBatchService(param).then((result) => {
        cb(result);
      }).catch((e) => {
        cb([]);
      })
    } else if (menuId === 89) {
      this.Get_TA_EMP_Report_Service(param).then((result) => {
        cb(result);
      }).catch((e) => {
        cb([]);
      })
    } else {
      cb([]);
    }
  }


  // get ta reports with program list
  GetTAReportService(param) {
    let url: any = `${this.config.FINAL_URL}` + this.url_fetch_TA_reports;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }


  // get ta reports for batch wise
  GetTAReportBatchService(param) {
    let url: any = `${this.config.FINAL_URL}` + this.url_fetch_TA_Batch_wise_reports;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  // get ta reports for employee with batch list
  Get_TA_EMP_Report_Service(param) {
    let url: any = `${this.config.FINAL_URL}` + this.url_fet_TA_EMP_reports;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  // get program list
  GetTAprogramList(param) {
    // this.spinner.show();
    let url: any = `${this.config.FINAL_URL}` + this.url_fetch_program_list;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  //get filters
  GetTAFilterInfo(param) {
    let url: any = `${this.config.FINAL_URL}` + this.url_fetch_TA_filters;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }


  //get batch list for employee reports
  GetTABatchList(param) {
    // this.spinner.show();
    let url: any = `${this.config.FINAL_URL}` + this.url_fetch_batch_list;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  //get reports download by menu id
  GetTAReportDownloadByMenuId(param, menuId, cb) {
    if (menuId === 87) {
      this.GetTAReportDownload(param).then((result) => {
        cb(result);
      }).catch((e) => {
        cb([]);
      })
    } else if (menuId === 88) {
      this.GetTABatchWiseReportDownload(param).then((result) => {
        cb(result);
      }).catch((e) => {
        cb([]);
      })
    } else if (menuId === 89) {
      this.GetTA_EMP_ReportDownload(param).then((result) => {
        cb(result);
      }).catch((e) => {
        cb([]);
      })
    } else {
      cb([]);
    }
  }

  // get reports download
  GetTAReportDownload(param) {
    let url: any = `${this.config.FINAL_URL}` + this.url_download_ta_reports;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  // get reports batchwise download
  GetTABatchWiseReportDownload(param) {
    let url: any = `${this.config.FINAL_URL}` + this.url_download_ta_Batchwise_reports;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  // get employee report download
  GetTA_EMP_ReportDownload(param) {
    let url: any = `${this.config.FINAL_URL}` + this.url_download_emp_ta_reports;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  getonboardingreport(param) {
    let url: any = `${this.config.FINAL_URL}` + this.url_get_report_preonboarding_report;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  GetOnlineConsolidatedReport(param) {
    let url: any = `${this.config.FINAL_URL}` + this.url_get_report_consolidated_report;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  GetConsolidatedWorkflowReport(param) {
    let url: any = `${this.config.FINAL_URL}` + this.url_get_consolidated_workflow;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  GetConsolidatedAttendanceReport(param) {
    let url: any = `${this.config.FINAL_URL}` + this.url_get_consolidated_attendance;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  GetConsolidatedFeedbackReport(param) {
    let url: any = `${this.config.FINAL_URL}` + this.url_get_consolidated_feedback;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }


  getQuizSummaryReport(param) {
    let url: any = `${this.config.FINAL_URL}` + this.url_get_quiz_summary_report;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  emplyeedetailreport(param) {
    let url: any = `${this.config.FINAL_URL}` + this.url_report_emplyee_consumption;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  getemployeedetailreportTypeList(param) {
    let url: any = `${this.config.FINAL_URL}` + this.url_report_type_employee_consumption;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  emplyeedetailreport_download(param) {
    let url: any = `${this.config.FINAL_URL}` + this.url_report_emplyee_consumption_download;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  getscheduledropdown() {
    let url: any = `${this.config.FINAL_URL}` + this._schedule_dropdown;
    return new Promise(resolve => {
      this.http1.post(url, {})
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  get_reports_download_list() {
    let url: any = `${this.config.FINAL_URL}` + this.url_fetch_report_download_list;
    return new Promise(resolve => {
      this.http1.post(url, {})
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  get_report_scheduleList_list() {
    let url: any = `${this.config.FINAL_URL}` + this.url_fetch_report_scheduleList_list;
    return new Promise(resolve => {
      this.http1.post(url, {})
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  add_Edit_schedule_report(param) {
    let url: any = `${this.config.FINAL_URL}` + this.add_schedule_immediate_url;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  fetchschedule_detail(param) {
    let url: any = `${this.config.FINAL_URL}` + this.schedule_detail_url;
    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
  GetReportQuizCumulative(param) {
    let url: any = `${this.config.FINAL_URL}` + this.url_report_quiz_cumulative;
    // + '?courseIds=' + param.courseIds;

    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  GetReportQuizCumulative_download(param) {
    let url: any = `${this.config.FINAL_URL}` + this.url_report_quiz_cumulative_download;
    // + '?courseIds=' + param.courseIds;

    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  getSurveyList(param){
    let url: any = `${this.config.FINAL_URL}` + this.getSurvey_List;
    // + '?courseIds=' + param.courseIds;

    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }

  getSurveyReport(param){
    let url: any = `${this.config.FINAL_URL}` + this.getSurvey_Report;
    // + '?courseIds=' + param.courseIds;

    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }

  getPollList(param){
    let url: any = `${this.config.FINAL_URL}` + this.getPoll_List;
    // + '?courseIds=' + param.courseIds;

    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }

  getPollReport(param){
    let url: any = `${this.config.FINAL_URL}` + this.getPoll_Report;
    // + '?courseIds=' + param.courseIds;

    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }

  getEEPReportList(param){
    let url: any = `${this.config.FINAL_URL}` + this.geteep_List;
    // + '?courseIds=' + param.courseIds;

    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }

  getEEPReport(param){
    let url: any = `${this.config.FINAL_URL}` + this.geteep_Report;
    // + '?courseIds=' + param.courseIds;

    return new Promise(resolve => {
      this.http1.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }
}


