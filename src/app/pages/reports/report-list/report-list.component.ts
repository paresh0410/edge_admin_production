import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ReportsService } from '../reports.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { PassService } from '../../../service/passService';
import { SuubHeader } from '../../components/models/subheader.model';
import { noData } from '../../../models/no-data.model';
@Component({
  selector: 'ngx-report-list',
  templateUrl: './report-list.component.html',
  styleUrls: ['./report-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ReportListComponent implements OnInit {
  temData: any = [];
  displayTableData: any = [];
  data: any = [];
  headerRow: any = [];
  noLinkAvailableText = "No Data Available.";
  noDataVal:noData={
    margin:'mt-5',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No data available at this time.",
    desc:"Data in a report will appear after some activity is recorded for that entity. The data in the report can also depend on filters applied on the report",
    titleShow:true,
    btnShow:false,
    descShow:true,
    btnText:'Learn More',
    btnLink:'',
  }
  header: SuubHeader;
  displayScheduledList = false;
  displayReportList = false;
  reportTitle;
  ignoreCol = 'scheduleId';
  selected = 'Schedule';
  scheduleId: number;
  scheduleParams = null;
  showSchedulePopup: boolean = false;
  scheduleTab: boolean = true;
  DownloadTab: boolean = false;
  nodata: boolean = false;
  searchtext: any;
  placeholder: any = 'Search by name and type';
  pager: any = [];
  pageSize = 10;
  tempdata: any = [];
  labelsSchedule: any = [
    // { labelname: '', bindingProperty: '', componentType: 'checkbox' },
      { labelname: 'Sr. No', bindingProperty: 'id', componentType: 'text' },
      { labelname: 'Name', bindingProperty: 'Name', componentType: 'text' },
      { labelname: 'Type', bindingProperty: 'Type', componentType: 'text' },
      { labelname: 'Details', bindingProperty: 'Details', componentType: 'text' },
    { labelname: 'Visibility', bindingProperty: 'btntext', componentType: 'button' },
      { labelname: 'EDIT', bindingProperty: 'tenantId', componentType: 'icon' },
      ];
      labelsDownload: any = [
        // { labelname: '', bindingProperty: '', componentType: 'checkbox' },
          { labelname: 'Sr. No', bindingProperty: 'id', componentType: 'text' },
          { labelname: 'Name', bindingProperty: 'Name', componentType: 'text' },
          { labelname: 'Valid Till', bindingProperty: 'Valid Till', componentType: 'text' },
          { labelname: 'Status', bindingProperty: 'Status', componentType: 'text' },
          { labelname: 'Mode', bindingProperty: 'Mode', componentType: 'text' },
          { labelname: 'Download', bindingProperty: 'reportLink', componentType: 'icondown',
          noDataMessage: 'No data available' },
          ];
  menuId: number;
  FMReportFilterValues: any = {
    Ids: [],
    Filters: {},
    courseName: [],
    courseTypeId: '',
    menuId: '',
    AcitivityIds: [],
    filterId: '',
  };
  btntext: string;
  constructor(private reportService: ReportsService,
    private router: Router,
    private toast: ToastrService, private spinner: NgxSpinnerService,
    private pagservice: PassService,private routes: ActivatedRoute) {
    // this.displayReportList = this.reportService.displayReportList;
    // this.reportTitle = this.reportService.ReportTitle;
    // this.displayScheduledList = this.reportService.displayScheduledList;
    this.menuId = this.routes.snapshot.params['menuId'] == undefined ? 0 : Number(this.routes.snapshot.params['menuId']);
    this.FMReportFilterValues.menuId = this.menuId;

  }

  ngOnInit() {
    // this.getReportList();
    // this.getScheduledList();
    this.selecttab();
    if(this.menuId==44){
      this.header.title="Course Consumption"
      this.header.breadCrumbList=
        [
          {
            'name': 'Reports',
            'navigationPath': '/pages/reports',
          },
          {
            'name': 'Online Course',
            'navigationPath': '/pages/reports/reports_home',
          },]

    }
  }
  selecttab() {
    if (this.selected == 'Schedule') {
      this.placeholder = 'Search by name and type';
      this.getScheduledList();
      this.displayScheduledList = true;
      this.displayReportList = false;
      this.scheduleTab = true;
      this.DownloadTab = false;
    } else {
      this.placeholder = 'Search by name, category, date and mode';
      this.getReportList();
      this.displayReportList = true;
      this.displayScheduledList = false;
      this.scheduleTab = false;
      this.DownloadTab = true;
    }
  }
  prepareHeaderData() {
    this.headerRow = [];
    this.temData = this.data;
    if (this.displayTableData.length !== 0) {
      this.headerRow = Object.keys(this.displayTableData[0]);
      // this.headerRow = this.headerRow.filter(item => item !== this.ignoreCol);
      if (this.displayScheduledList) {
        this.headerRow = this.headerRow.filter(item => item !== this.ignoreCol);
      }
      console.log('header key rows ======>', this.headerRow);
      this.spinner.hide();
    }
  }
  clear() {
    this.nodata=false;
    this.header.searchtext = '';
    this.searchtext =  this.header.searchtext;
    this.SarchFilter(event, this.searchtext);
  }
  getReportList() {
    this.spinner.show();
    this.reportService.get_reports_download_list().then((res: any) => {
      console.log('Data', res);
      if (res.type) {
        if (res.data.length != 0) {
          this.data = res.data;
          this.tempdata = this.data;
          this.setpage(1);
          // this.prepareReportListData(this.data);
          // this.prepareHeaderData();
        } else {
          this.nodata = true;
          this.data = [];
          this.headerRow = [];
          this.toast.warning('Data Not Avaiable', 'Warning');
          this.spinner.hide();
        }
      }
    }, err => {
      this.spinner.hide();
      console.log(err);
    });
  }

  getScheduledList() {
    // let params = {
    //   menuId: 80,
    //   reportType: 1,
    // }
    this.spinner.show();
    this.reportService.get_report_scheduleList_list().then((res: any) => {
      console.log('Data', res);
      if (res.type) {
        if (res.data.length != 0) {
          this.data = res.data;
          this.tempdata = this.data;
          this.setpage(1);
          // this.prepareScheduledListData(this.data);
          // this.prepareHeaderData();
        } else {
          this.data = [];
          this.nodata = true;
          this.spinner.hide();
          this.headerRow = [];
          this.toast.warning('Data Not Avaiable', 'Warning');
        }
      }
    }, err => {
      this.spinner.hide();
      console.log(err);
    });
  }

  prepareReportListData(data) {
    this.nodata = false;
    if (data.length) {
      this.setpage(1);
    } else {
      this.nodata = true;
    }
    // this.displayTableData = data;
    // if (this.displayTableData.length) {
    //   for (let index = 0; index < this.displayTableData.length; index++) {
    //     this.displayTableData[index] = { id: index + 1, ...this.displayTableData[index] };
    //     // this.displayTableData[index]['Download'] = '';
    //   }
    //   this.setpage(1);
    // }
  }

  setpage(page) {
    this.nodata = false;
    this.pager = this.pagservice.getPager(this.tempdata.length, page, this.pageSize);
    console.log(this.pager);
    if (this.selected == 'Schedule') {
      this.displaySchedule();
      this.prepareHeaderData();
    } else {
      this.displayReport();
      this.prepareHeaderData();
    }
  }
  displayReport() {
    this.displayTableData = this.tempdata.slice(this.pager.startIndex, this.pager.endIndex + 1);
    for (let index = 0; index < this.displayTableData.length; index++) {
      this.displayTableData[index] = { id: index + 1, ...this.displayTableData[index] };
      // this.displayTableData[index]['Download'] = '';
    }
    if (!this.displayTableData.length) {
      this.nodata = true;
    }
    console.log(this.displayTableData,"this.displayTableData");
  }
  displaySchedule() {
    this.displayTableData = this.tempdata.slice(this.pager.startIndex, this.pager.endIndex + 1);
    for (let index = 0; index < this.displayTableData.length; index++) {
      this.displayTableData[index] = { id: index + 1, ...this.displayTableData[index] };
      this.displayTableData[index].isActive==1?this.displayTableData[index].btntext="fa fa-eye":this.displayTableData[index].btntext="fa fa-eye-slash";
      // this.displayTableData[index]['Download'] = '';
      this.displayTableData[index]['Edit'] = '';
    }
    if (!this.displayTableData.length) {
      this.nodata = true;
    }
    console.log(this.displayTableData);
  }
  prepareScheduledListData(data) {
    this.nodata = false;
    if (data.length) {
      this.setpage(1);
    } else {
      this.nodata = true;
    }
    // this.nodata = false;
    // this.setpage
    // this.displayTableData = data;
    // if (this.displayTableData.length) {
    //   for (let index = 0; index < this.displayTableData.length; index++) {
    //     this.displayTableData[index] = { id: index + 1, ...this.displayTableData[index] };
    //     // this.displayTableData[index]['id'] = index + 1;
    //     this.displayTableData[index]['Edit'] = '';
    //     // this.displayTableData[index]['reportLink'] = '';
    //   }
    // }
    // else {
    //   this.nodata = true;
    // }
  }
  editScheduleModel(event) {
    this.scheduleId = event.scheduleId;
    this.scheduleParams =  event.parameter;
    this.showSchedulePopup = true;
  }
  visibilityTableRow(event){
    var param ={
      'scheduleId':event.scheduleId,
       'status':event.isActive==1?0:1
    }


    this.reportService.enableDisableReport(param).then((res: any) => {
      console.log('Data', res);
      if (res.type) {
        for(var i=0 ;i<this.displayTableData.length;i++){
          if(this.displayTableData[i]['scheduleId']==event.scheduleId){
            this.displayTableData[i]['isActive']==1?this.displayTableData[i]['isActive']=0:this.displayTableData[i]['isActive']=1
            this.displayTableData[i].isActive==1?this.displayTableData[i].btntext="fa fa-eye":this.displayTableData[i].btntext="fa fa-eye-slash";
          }
        }
        this.toast.success(res.output[0]['msg'] ,"Success")
        } else {

      }
    }, err => {
      this.spinner.hide();
      console.log(err);
    });
  }
  downloadFile(event) {
    console.log(event);
    // const link = document.createElement('a');
    // link.setAttribute('target', '_blank');
    // link.setAttribute('href', event.reportLink);
    // link.setAttribute('download', event.reportLink);
    // document.body.appendChild(link);
    // link.click();
    // link.remove();

  }
  closedmodel(event) {
    if (event) {
      this.showSchedulePopup = false;
      this.scheduleParams = null;
    }
  }
  close(event) {
    console.log(event);
    this.showSchedulePopup = false;
    this.getScheduledList();
  }
  // searchQuestion(event) {
  //   var val = event.target.value.toLowerCase();
  //   var keys = [];
  //   let data = '';
  //   // filter our data
  //   if (this.temData.length > 0) {
  //     keys = Object.keys(this.temData[0]);
  //   }
  //   var temp = this.temData.filter((d) => {
  //     // keys.forEach(element => {

  //     // });
  //     for (const key of keys) {
  //       if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
  //         return true;
  //       }
  //     }
  //   });

  //   // update the rows
  //   this.data = temp;
  // }

  SarchFilter($event, text){
    const val = text;
    var keys = [];
    let data = '';
    if (this.data.length > 0) {
      keys = Object.keys(this.data[0]);
    }
    var temp = this.data.filter((d) => {

      for (const key of keys) {
        if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
          return true;
        }
      }
    });
    this.tempdata = temp;
    this.setpage(1);
  }

  SarchFiltertest(event, text){
    text = event.target.value;
    const val = text;
    var keys = [];
    let data = '';
    if(val.length>=3 || val.length == 0){
    if (this.data.length > 0) {
      keys = Object.keys(this.data[0]);
    }
    var temp = this.data.filter((d) => {

      for (const key of keys) {
        if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
          return true;
        }
      }
    });
if(temp.length==0){
  this.nodata=true;
}else{
  this.nodata=false
}
    this.tempdata = temp;
    this.setpage(1);
  }
  }

  // SarchFilter($event, text) {
  //   const val = text;
  //   if (this.selected == 'Download') {
  //     const temp = this.data.filter(function (d) {
  //       return String(d.Name).toLowerCase().indexOf(val) !== -1 ||
  //         d['Valid Till'].toLowerCase().indexOf(val) !== -1 ||
  //         d.Category.toLowerCase().indexOf(val) !== -1 ||
  //         d.Mode.toLowerCase().indexOf(val) !== -1 ||
  //         !val
  //     })
  //     console.log(temp);
  //     this.tempdata = temp;
  //     this.setpage(1);
  //     // this.prepareReportListData(temp);
  //     // this.prepareHeaderData();
  //   } else {
  //     const temp = this.data.filter(function (d) {
  //       return String(d.Name).toLowerCase().indexOf(val) !== -1 ||
  //         d.Type.toLowerCase().indexOf(val) !== -1 ||
  //         !val
  //     })
  //     console.log(temp);
  //     this.tempdata = temp;
  //     this.setpage(1);
  //     // this.prepareScheduledListData(temp);
  //     // this.prepareHeaderData();
  //   }

  //   // } else {
  //   //   this.nodata = true;
  //   // }
  // }
  onChangeTab(event) {



    if (event.tabTitle == 'Schedule') {
      this.header  = {
        title:'Report Details',
        btnsSearch: true,
        searchBar: true,
        searchtext: '',
        placeHolder:'Search by name and type',
        dropdownlabel: ' ',
        drplabelshow: false,
        drpName1: '',
        drpName2: ' ',
        drpName3: '',
        drpName1show: false,
        drpName2show: false,
        drpName3show: false,
        btnName1: '',
        btnName2: '',
        btnName3: '',
        btnAdd: '',
        btnName1show: false,
        btnName2show: false,
        btnName3show: false,
        btnBackshow: true,
        btnAddshow: false,
        filter: false,
        showBreadcrumb: true,
        breadCrumbList:[
      {
        'name': 'Reports',
        'navigationPath': '/pages/reports',
      },]
      };
      console.log(event);
      this.selected = event.tabTitle;
      this.searchtext = '';
      this.selecttab();
     }

     if (event.tabTitle == 'Download') {
      this.header  = {
        title:'Report Details',
        btnsSearch: true,
        searchBar: true,
        searchtext: '',
        placeHolder:'Search by name, category, date and mode',
        dropdownlabel: ' ',
        drplabelshow: false,
        drpName1: '',
        drpName2: ' ',
        drpName3: '',
        drpName1show: false,
        drpName2show: false,
        drpName3show: false,
        btnName1: '',
        btnName2: '',
        btnName3: '',
        btnAdd: '',
        btnName1show: false,
        btnName2show: false,
        btnName3show: false,
        btnBackshow: true,
        btnAddshow: false,
        filter: false,
        showBreadcrumb: true,
        breadCrumbList:[
      {
        'name': 'Reports',
        'navigationPath': '/pages/reports',
      },]
      };
      console.log(event);
      this.selected = event.tabTitle;
      this.searchtext = '';
      this.selecttab();
     }


  }
  back() {
    // window.history.back();
    this.router.navigate(['pages/reports']);
  }
}
