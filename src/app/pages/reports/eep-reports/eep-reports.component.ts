import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../app.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ReportsService } from '../reports.service';
import { SuubHeader } from '../../components/models/subheader.model';

@Component({
  selector: 'ngx-eep-reports',
  templateUrl: './eep-reports.component.html',
  styleUrls: ['./eep-reports.component.scss']
})
export class EepReportsComponent  {

  showdata: any = [];
  menuList: any = [];
  reportname: string;
  menuId: number;
  showDownloadList = false;
  header: SuubHeader  = {
    title:'Reaction Report',
    btnsSearch: true,
    searchBar: false,
    dropdownlabel: ' ',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: '',
    btnName3: '',
    btnAdd: '',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: false,
    filter: false,
    showBreadcrumb:true,
    breadCrumbList:[{
      'name': 'Reports',
      'navigationPath': '/pages/reports',
    }]
  };
  constructor(private appService: AppService,
    private router: Router, private routes: ActivatedRoute,
    private service: ReportsService) {
    this.service.setCacheReportFilter(null);
    this.showdata = this.appService.getmenus();
    this.reportname = this.service.ReportTitle;
    this.menuId = this.service.menuId;
    if (this.showdata) {
      for (let i = 0; i < this.showdata.length; i++) {
        if (Number(this.showdata[i].parentMenuId) === this.service.menuId) {
          // console.log(this.menuList);
          this.menuList.push(this.showdata[i]);
          console.log(this.menuList,"menuList")
        }
      }
    }
    if (this.menuId == 80) {
      this.showDownloadList = true;
    }
    // this.menuList = [{"menuName":"Course Consumption", "menuRoute": "course-consumption"}];
  }
  
  gotopages(item, menuId) {
    this.service.setReportname(item.menuName);
    localStorage.setItem('parentMenuId', item.parentMenuId);
    // this.service.menuId = menuId;
    this.router.navigate([item.menuRoute, menuId], { relativeTo: this.routes });
  }
  goToReportList(data){
    // this.service.setReportname(item.menuName);
    if(data == 'Scheduled'){
      this.service.displayScheduledList = true;
      this.service.displayReportList = false;
    }
    if(data == 'Download'){
      this.service.displayReportList = true;
      this.service.displayScheduledList = false;
    }
    this.reportname = this.service.ReportTitle;
    this.menuId = this.service.menuId;
    this.router.navigate( ['../ReportList'], { relativeTo: this.routes } );
  }

  back() {
    this.router.navigate(['/pages/reports']);
  }
  ngOninit(){
    if(this.menuId==135){
      this.header.breadCrumbList=[
        {
          'name': 'Reports',
          'navigationPath': '/pages/reports',
        },
        {
          'name': 'Reaction Report ',
          'navigationPath': '/pages/reports/reports_home',
        },
        ]
    }
  }
}
