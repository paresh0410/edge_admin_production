import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { FlexmonsterPivot } from 'ng-flexmonster';
import { SuubHeader } from '../../../components/models/subheader.model';
import { FMToolbar, FMReportFilter, FMReportEntity } from '../../../../component/fm-report/fm-report';
import { ReportsService } from '../../reports.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ExcelService } from '../../../../service/excel-service';
import { AppService } from '../../../../app.service'
import { ScrollDispatcher } from '@angular/cdk/overlay';
import { Report } from '../../report';
import { Filter } from '../../../../models/filter.modal';

import { webApi } from '../../../../service/webApi';
import { CommonFunctionsService } from "../../../../service/common-functions.service";
import * as _ from "lodash";
@Component({
  selector: 'ngx-eep-report',
  templateUrl: './eep-report.component.html',
  styleUrls: ['./eep-report.component.scss']
})
export class EepReportComponent implements OnInit {

  @ViewChild('pivot') pivot: FlexmonsterPivot;
  @ViewChild('scrollDiv') scrolledElement: ElementRef;
  ReportName: string = 'EEP Nomination Details';
  filterDataReg: any;
  contdata: any;
  header: SuubHeader;
  filtertype: any = {
    primary: 'Survey Wise Filter',
    secondary: 'Data Wise Filter',
    limit: 1
  };
  regRowData = {
    dataSource: {
      data: 'https://cdn.flexmonster.com/reports/report.json',
    },
  };


  regRowDataCount: any;

  pivotReport: any;
  toolbar: FMToolbar = {
    connect: false,
    open: false,
    save: false,
    export: true,
    grid: true,
    charts: true,
    format: true,
    options: true,
    fields: true,
    fullscreen: true,
    report: true,
  };
  FMReadyForWorking: boolean = false;
  FMReport: any = {
    data: [],
    report: {},
    toolbar: this.toolbar,
  };
  FMReportFilterObj: FMReportFilter = {
    main: [],
    sub: [],
  };
  FMReportFilterWorking: boolean = false;
  FMReportList: Array<FMReportEntity> = [];
  courseList: any =  [];
  selected = [];
  readonly headerHeight = 50;
  readonly rowHeight = 50;
  infiniteworking: boolean = false;
  CourseFilterList: any = [];
  courseNamelist: any = [];
  isLoading: boolean = false;
  FMReportFilterValues: any = {
    Ids: [],
    Filters: {},
    courseName: [],
    courseTypeId: '',
    menuId: '',
  };
  // Syntax for reference
  reportFilters: {
    'uniqueName': 'tags',
    'filter': {
      'members': ['tags.[dc3]', 'tags.[demo3]'],
    },
    'sort': 'unsorted'
  };
  ReportFiltersSET: any = {
    FMReport: [],
    FMReportList: [],
    FMReportName: null,
    ReportFilter: {},
  };
  menuId: number;
  exports: boolean = false;
  //courseTypeId: number = CourseType.ONLINE;
  courseTypeId: number = null;

  FilterParam: any = {
    start: 0,
    rows: 20,
    tagIds: null,
    categoryIds: null,
    total: 0,
    courseTypeId: this.courseTypeId
  };
  labels: any = [
		{ labelname: '', bindingProperty: '', componentType: 'checkbox' },
    { labelname: 'Cohort Name', bindingProperty: 'Name', componentType: 'text' },
    { labelname: 'Start Date', bindingProperty: 'startDate', componentType: 'date' },
		{ labelname: 'End Date', bindingProperty: 'endDate', componentType: 'date' },
		{ labelname: 'Enrol Count', bindingProperty: 'nominatedCount', componentType: 'text' },
  ]
  searchtext: string;
  cacheFilters: any;
  showSpinner: boolean = false;

  notiTitle:string="Report Schedule"
  btnName: string = 'Save';
  btnName1: string = 'Save & Show';
  isdata: any;

  constructor(protected service: ReportsService, private router: Router,
    // private toasterService: ToasterService,
    private AppService: AppService, public cdf: ChangeDetectorRef,
    private spinner: NgxSpinnerService,
    private commonFunctionService: CommonFunctionsService,
    private el: ElementRef, private excelservice: ExcelService, private routes: ActivatedRoute,
    private toastr: ToastrService,  private scrollDispatcher: ScrollDispatcher,
    ) {
      // console.log('FMReportlist :- ', this.FMReportList);
    this.contdata = this.AppService.getuserdata();
    this.menuId = this.routes.snapshot.params['menuId'] == undefined ? 0 : Number(this.routes.snapshot.params['menuId']);
    this.filterDataReg = {
      code: null,
      codeRange: {
        to: null,
        from: null,
      },
      name: null,
      invited: null,
      attendance: 'Yes',
      joined: 'Yes',
      doj: null,
      dojRange: {
        to: null,
        from: null,
      },
      inductionLocation: null,
      designation: null,
      department: null,
      grade: null,
      contactDetails: null,
      trainingDate: null,
      trainingDateRange: {
        to: null,
        from: null,
      },
      remarks: null,
      userId: this.contdata.userId,
      roleId: this.contdata.roleId,
    };
    // this.GetCourseConsumptionList(this.FilterParam, (result) => {
    //   this.courseList = result.courseList;
    //   this.FilterParam.total = result.totalCount;
    // })
    // this.getUsersData();
  }

  ngOnInit() {
    // this.GetCourseConsumptionList(this.FilterParam, (result) => {
    //   this.courseList = result.courseList;
    //   this.FilterParam.total = result.totalCount;
    // })
    // this.onScrollDown(0, true);
    this.header= {
      title:'EEP Nomination Details',
      btnsSearch: true,
      searchBar: true,
      searchtext: '',
      dropdownlabel: ' ',
      placeHolder:'Search by survey name',
      drplabelshow: false,
      drpName1: '',
      drpName2: ' ',
      drpName3: '',
      drpName1show: false,
      drpName2show: false,
      drpName3show: false,
      btnName1: '',
      btnName2: 'Schedule',
      btnName3: 'Preview',
      btnAdd: '',
      btnName1show: false,
      btnName2show: true,
      btnName3show: true,
      btnBackshow: true,
      btnAddshow: false,
      filter: false,
      showBreadcrumb:true,
      breadCrumbList:[
        {
          'name': 'Reports',
          'navigationPath': '/pages/reports',
        },
        {
          'name': 'EEP Report',
          'navigationPath': 'pages/reports/eep-reports',
        },
      ],
      filterConfig: {
        primary: false,
        primaryText: this.filtertype.primary,
        secondary: true,
        secondaryText: this.filtertype.secondary,
        showPrimarySelectedFilteredCount: true,
        showSecondarySelectedFilteredCount: true,
        primarySelectedFilteredCount: 0,
        secondarySelectedFilteredCount: 0,
      },
    };
    this.scrollDispatcher.scrolled().subscribe(element =>{
      // console.log('I am scrolling' , x);
      if(element){
      this.onScrollDown(1, false, this.scrolledElement.nativeElement);
      }
    });
  }

  presentToast(type, body) {
    if(type === 'success'){
      this.toastr.success(body, 'Success', {
        closeButton: false
       });
    } else if(type === 'error'){
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else{
      this.toastr.warning(body, 'Warning', {
        closeButton: false
        })
    }
  }

  customizeToolbar(toolbar) {
    // get all tabs
    const tabs = toolbar.getTabs();
    toolbar.getTabs = function () {
        // delete the first tab
        delete tabs[0];
        delete tabs[1];
        return tabs;
    };
  }

  submitFilterAsync(cb) {
    // this.service.getUsers(this.filterDataReg).then(res => {
    //   cb(res);
    // }, err => {
    //   console.log(err);
    // });
  }
  SarchFilter(event, text) {
    console.log(text,"text")
    //this.FilterParam.text = text;
    this.FilterParam.text = event.target.value ;
    this.FilterParam.start = 0;
   // this.FilterParam.menuId = this.menuId
   if(event.target.value.length>=3 || (event.target.value.length == 0 && event.keyCode !==13)){
  //  if(event.keyCode === 13){
    this.spinner.show();
    // this.showSpinner = true;
    this.cdf.detectChanges();
    this.getEEPList(this.FilterParam, (result) => {
      this.spinner.hide();
      // this.showSpinner = false;
    this.cdf.detectChanges();
      this.courseList = result.cohortList;
        this.FilterParam.total = result.totalCount;
        if (this.courseList.length == 0) {
          this.noData = true;
        } else {
          this.noData = false;
        }
    });
  // }
   }
}

SarchFilterTest(event, text) {
  console.log(text,"text")
  this.FilterParam.text = text;
  // this.FilterParam.text = event.target.value ;
  this.FilterParam.start = 0;
 // this.FilterParam.menuId = this.menuId
//  if(event.target.value.length>=3 || event.target.value.length == 0 || event == null){
//  if(event.keyCode === 13){
  this.spinner.show();
  this.showSpinner = true;
  this.cdf.detectChanges();
  this.getEEPList(this.FilterParam, (result) => {
    // this.spinner.hide();
    this.showSpinner = false;
  this.cdf.detectChanges();
    this.courseList = result.cohortList;
      this.FilterParam.total = result.totalCount;
      if (this.courseList.length == 0) {
        this.noData = true;
      } else {
        this.noData = false;
      }
  });
// }
//  }
}
clear() {
// this.searchtext = '';
this.header.searchtext = ''
this.searchtext = this.header.searchtext;
this.noData=false;
this.SarchFilterTest(null, this.searchtext);
}
  onPivotReady(pivot: Flexmonster.Pivot): void {
    console.log('[ready] FlexmonsterPivot', this.pivot);
  }

  onReportComplete(): void {
    this.pivot.flexmonster.off('reportcomplete');
    // this.pivot.flexmonster.setReport({
    //   dataSource: {
    //     dataSourceType: 'json',
    //     filename: 'https://cdn.flexmonster.com/data/data.json'
    //   }
    // });
  }

  onCustomizeCell(cell: Flexmonster.CellBuilder, data: Flexmonster.CellData): void {
    // console.log("[customizeCell] FlexmonsterPivot");
    if (data.isClassicTotalRow) {
      cell.addClass('fm-total-classic-r');
    }
    if (data.isGrandTotalRow) {
      cell.addClass('fm-grand-total-r');
    }
    if (data.isGrandTotalColumn) {
      cell.addClass('fm-grand-total-c');
    }
  }
  ngAfterViewInit() {
    this.submitFilterAsync((result) => {
      if (result.type === true) {
        const data = result['data'][0];
        this.FMReport = {
          data: data,
          report: Report,
          toolbar: this.toolbar,
        };
        this.FMReadyForWorking = true;
        Report.dataSource.data = data;
        // var pivot = new Flexmonster({
        //   licenseKey: this.flexMonsterKey,
        //   container: 'pivot-container',
        //   toolbar: true,
        //   report: Report,
        //   width: '100%',
        //   height: 700,
        //   beforetoolbarcreated: this.customizeToolbar,
        // });
      }
      });
      this.GetReportList( result => {
        this.FMReportList = result;
        console.log('FMReportlist : -', this.FMReportList);
      });
      this.GetFilterInfo( (result: any) => {
        if (result.main) {
          for (const obj in result.main) {
            // this.FMReportFilterObj.main[obj] = [];
            // this.FMReportFilterObj.main[obj]= result.main[obj];
            this.FMReportFilterObj.main.push(result.main[obj]);
            this.bindfilter(result.main[obj], 'primary');
          }
        }
        if (result.sub) {
          for (const obj in result.sub) {
            // this.FMReportFilterObj.sub[obj] = [];
            // this.FMReportFilterObj.sub[obj] = result.sub[obj];
            this.FMReportFilterObj.sub.push(result.sub[obj]);
            this.bindfilter(result.sub[obj], 'secondary');
          }
          this.checkForValidFilter(this.filters.secondary);
        }
        this.FMReportFilterObj = result;
        // this.cacheFilters = this.service.getCacheReportFilter();
        // this.FMReportFilterWorking = true;
        // if (this.cacheFilters && this.cacheFilters.primaryflag) {
        //   this.getSelectedFilter(this.cacheFilters);
        // } else {
        //   this.FMReportFilterValues.Filters = this.populateCourseConsumptionSecondaryFilter(this.cacheFilters);
        //   this.onScrollDown(0, true, this.scrolledElement.nativeElement);
        // }
        this.filtersCache = this.service.getCacheReportFilter();
        this.FMReportFilterWorking = true;
        if (this.filtersCache && this.filtersCache.primary && Object.keys(this.filtersCache.primary).length > 0) {
          this.getSelectedFilter(this.filtersCache.primary);
        } else {
          if (!this.filtersCache) {
            this.filtersCache = {
              primary: {},
              secondary: {},
            };
          }
          this.FMReportFilterValues.Filters = this.populateCourseConsumptionSecondaryFilter(this.filtersCache.secondary);
          this.onScrollDown(0, true, this.scrolledElement.nativeElement);
        }
        this.bindValuesTofilterButtons();
        if (this.filtersCache && this.filtersCache.primary && Object.keys(this.filtersCache.primary).length > 0) {
        this.bindFilterCountToheader('primary', this.filtersCache.primary.filteredCount);
       }
        if (this.filtersCache && this.filtersCache.secondary && Object.keys(this.filtersCache.secondary).length > 0) {
        this.bindFilterCountToheader('secondary', this.filtersCache.secondary.filteredCount);
       }
        console.log('FMReportlist : -', this.FMReportFilterObj);
      });

      this.scrollDispatcher.scrolled().subscribe(x => {
        // console.log('I am scrolling', x);
        this.onScrollDown(1, false, this.scrolledElement.nativeElement);
      });

  }
  GetReportList (cb) {
    const param = {
      menuId: this.menuId || 0,
    };
    this.service.GetReportList(param).then( (res: any) => {
      if (res.type) {
        cb(res.data);
      } else {
        cb([]);
      }
    }, err => {
      console.log(err);
      cb([]);
    });
  }
  GetReport (event) {
    console.log('Report : ', event);
  }
  handleChange(evt: any, value: any) {
    let target = evt.target;
    this.FMReadyForWorking = false;
    if (value) {
      this.FMReport.report = JSON.parse(value.metadata);
    }
    setTimeout( () => {
      this.FMReadyForWorking = true;
      this.cdf.detectChanges();
    }, 500);
  }
  GetFilterInfo (cb) {
    this.FMReportFilterWorking = false;
    const param = {
      courseTypeId: this.courseTypeId,
      menuId : this.menuId,
    };
    this.service.GetFilterInfo(param).then( (res: any) => {
      if (res.type) {
        cb(res.data);
      } else {
        cb([]);
      }
    }, err => {
      console.log(err);
      cb([]);
    });
  }
  noData: boolean = false;
  getEEPList (value, cb) {
    console.log(value,"value")
    // this.spinner.show();
    this.service.getEEPReportList(value).then( (res: any) => {
      if (res.type) {
        // if(res.courseList.length === 0){
        //   this.noData = true;
        //   // this.spinner.hide();
        // }
        // else{
        //   console.log('Course List response', res.courseList);
        //   this.noData = false;
        //   // this.spinner.hide();
        //   cb(res);
        // }
        cb(res);
      } else {
        this.noData = true;
        cb([]);
      }
    }, err => {
      // this.spinner.hide();
      console.log(err);
      cb([]);
    });
  }
  populateCourseConsumptionFilter (obj) {
    const item = {};
    try {
      if (obj) {
        // const primary = obj.primary;
        const primary = obj;
        for (const key in primary) {
          item[key] = null;
          if (primary[key].length > 0) {
            let commastring = null;
            for (let i = 0; i < primary[key].length; i++) {
              if (String(Date.parse(primary[key][i])) === 'NaN') {
                if (commastring) {
                  commastring += ',';
                }
                commastring += primary[key][i]['id'];
              } else {
                if (item[key] == null || item[key].length === 0) {
                  item[key] = [];
                }
                if (primary[key][i]) {
                  item[key].push(primary[key][i]);
                }
              }
            }
            if (commastring) {
              item[key] = commastring;
            }
          }
        }
      }
      return item;
    } catch (e) {
      return item;
    }
  }
  populateCourseConsumptionSecondaryFilter(obj) {
    const item = {};
    try {
      if (obj) {
        // const secondary = obj.secondary;
        const secondary = obj;
        for (const key in secondary) {
          item[key] = null;
          if (secondary[key].length > 0) {
            let commastring = '';
            for (let i = 0; i < secondary[key].length; i++) {
              if (String(Date.parse(secondary[key][i])) === 'NaN') {
                if (commastring) {
                  commastring += ',';
                }
                commastring += secondary[key][i]['itemName'];
              } else {
                if (item[key] == null || item[key].length === 0) {
                  item[key] = [];
                }
                if (secondary[key][i]) {
                  item[key].push(secondary[key][i]);
                }
              }
            }
            if (commastring) {
              item[key] = commastring;
            }
          }
        }
      }
      return item;
    } catch (e) {
      return item;
    }
  }
  getSelectedFilter(event) {
    this.FilterParam.menuId =  this.menuId
    console.log(event,"event");
    // console.log(this.populateCourseConsumptionFilter(event));
    if (event.primaryflag) {
        this.filtersCache.primary = event;
        const filtervalue: any = this.populateCourseConsumptionFilter(event);
        console.log(filtervalue,"filterValue")
        // let param = this.FilterParam;
        this.FilterParam.tagIds = filtervalue.Tags === undefined ? null : filtervalue.Tags;
        this.FilterParam.categoryIds = filtervalue.Category === undefined ? null : filtervalue.Category;
        this.FilterParam.workflowIds = filtervalue.Workflow === undefined ? null : filtervalue.Workflow;
        this.FilterParam.programIds = filtervalue.Program === undefined ? null : filtervalue.Program;
        this.FilterParam.enrolDate = filtervalue['Enrol Date'] === undefined ? [] : filtervalue['Enrol Date'];
        this.FilterParam.rangeDate = filtervalue['Course Date'] === undefined ? [] : filtervalue['Course Date'];
        this.FilterParam.surveyDate = filtervalue['Survey Date'] === undefined ? [] : filtervalue['Survey Date'];

        this.FilterParam.start = 0;
        this.getEEPList(this.FilterParam, (result) => {
          if (result.cohortList.length === 0) {
            this.noData = true;
          }else{
            this.noData = false;
          }
            this.courseList = result.cohortList;
            console.log(result.cohortList,"List")
            this.FilterParam.total = result.totalCount;
        });
    }
    else if (event.secondaryflag) {
      // Passing to FMReport
      // this.FMReportFilterValues.Filters = this.populateCourseConsumptionSecondaryFilter(event);
      this.filtersCache.secondary = event;
    }
    this.FMReportFilterValues.Filters = this.populateCourseConsumptionSecondaryFilter(event);
    this.FMReportFilterValues['secondaryScheduleParams'] = event['secondaryScheduleParams'];
    const cacheFilter = {
      primaryCache: event.primaryCache,
      secondaryCache: event.secondaryCache
    }
    // this.service.setCacheReportFilter(cacheFilter);
    if (event.clear) {
      this.service.setCacheReportFilter(null);
    } else {
      // this.service.setCacheReportFilter(event);
      this.service.setCacheReportFilter(this.filtersCache); // new filter cache
    }
  }

  /**
   * Infinite Scroll
   */
  onScrollDown (offsetY: number, pageload: boolean, element) {
   // this.FilterParam.menuId =  this.menuId

    // console.log('scrolled down!!', offsetY);
    // total height of all rows in the viewport
    // const viewHeight =
    //   this.el.nativeElement.getBoundingClientRect().height - this.headerHeight;

      // console.log("Scroll Height", viewHeight);
    if ((this.FilterParam.start <= this.FilterParam.total && !this.infiniteworking
      && (Math.ceil(element.scrollHeight - element.scrollTop) - element.clientHeight) < 30)
       || String(offsetY) === '0') {
      this.infiniteworking = true;
      this.spinner.show();
      // this.showSpinner = true;
    this.cdf.detectChanges();
      if (!pageload) {
        this.FilterParam.start = this.FilterParam.start + 10;
      }
      this.getEEPList(this.FilterParam, (result) => {
        this.courseList = this.courseList.concat(result.cohortList);
        if(this.courseList.length === 0){
          this.noData = true;
        }
        this.FilterParam.total = result.totalCount;
        this.infiniteworking = false;
        // this.cdf.detectChanges();
        this.spinner.hide();
        // this.showSpinner = false;
        this.cdf.detectChanges();
    });
    } else {
      // this.spinner.hide();
      // console.log('Course List : ', this.courseList)
    }

  }

  /**
   * OnChange Events
   */
  onSelect(event) {
    console.log(event,"event")
   try {
    //  let selected = null;
    // if (event.selected.length > 1) {
    //   selected = event.selected[1];
    // } else if (event.selected.length > 0){
    //   selected = event.selected[0];
    // }
    // if (selected) {
    //   const index = this.CourseFilterList.indexOf(selected.courseId);
    //   if (index === -1) {
    //     this.CourseFilterList.push(selected);
    //   } else {
    //     this.CourseFilterList.splice(index, 1);
    //   }
    // }
    if (event.selected.length > 0) {
      this.CourseFilterList = [];
      this.courseNamelist = [];
      event.selected.forEach(element => {
        if (element) {
          this.CourseFilterList.push(element.id);
          this.courseNamelist.push(element.Name);
        }
      });
      this.FMReportFilterValues.Ids = this.CourseFilterList;
      this.FMReportFilterValues.courseName = this.courseNamelist;
      this.FMReportFilterValues.courseTypeId = this.courseTypeId;
      this.FMReportFilterValues.menuId = this.menuId;
    } else {
      this.CourseFilterList = [];
      this.FMReportFilterValues.Ids = [];
      this.courseNamelist = [];
      this.FMReportFilterValues.courseName = [];
    }
      console.log(this.CourseFilterList);
    } catch (e) {
      console.log(e);
    }

  //  console.log('Select Event', event, this.selected);

  //   this.selected.splice(0, this.selected.length);
  //   this.selected.push(event);
  }

  onSelectRow(event) {
    console.log(event);
    console.log(this.CourseFilterList,"CourseFilterList")
   let allow = true;

    if(this.CourseFilterList.length > 0) {
      for(let i = 0; i< this.CourseFilterList.length; i++) {
        if(this.CourseFilterList[i] == event.id) {
          this.CourseFilterList.splice(i, 1);
          this.courseNamelist.splice(i, 1);
          allow = false;
          break;

        }
      }
    }

    if(allow == true) {
      this.CourseFilterList.push(event.id);
      this.courseNamelist.push(event.Name);

    }
    console.log("courseFilterlist",this.CourseFilterList);
    console.log(this.CourseFilterList)
    this.FMReportFilterValues.Ids = this.CourseFilterList;
    this.FMReportFilterValues.courseName = this.courseNamelist;
    this.FMReportFilterValues.courseTypeId = this.courseTypeId;
    this.FMReportFilterValues.menuId = this.menuId;

    console.log(this.CourseFilterList);
  }

  selectCourseForReport(course){
    this.spinner.show();
    // this.showSpinner = true;
    this.cdf.detectChanges()
    let CourseFilter = [];
    CourseFilter.push(course.id);
    this.FMReportFilterValues.Ids = CourseFilter;
    console.log("Course List ", this.FMReportFilterValues);
    this.GetReports();
  }

  onActivate(event) {
    // console.log('Activate Event', event);
  }
  GetReportCourseConsumption (value, cb) {
    this.service.getEEPReport(value).then( (res: any) => {
      if (res.type) {
        cb(res.data);
      } else {
        cb([]);
      }
    }, err => {
      console.log(err);
      cb([]);
    });

  }
  GetReportCourseConsumptionTimeOut(value, cb){
    let url = webApi.domain + webApi.url.getnominatedList;
    this.commonFunctionService.reportPostRequestTimeOut(url, value).subscribe( (res) =>{
      console.log("res", res);
      cb(res['data']);
    },
    error => {
        console.log('ERROR', error);
        // if(error === 'Timeout Exception'){

        // }
        cb(error);
      });
  }
  GetReportCourseConsumptionDownload (value, cb) {
    this.service.GetReportActivityConsumptionDownload(value).then( (res: any) => {
      if (res.type) {
        cb(res.data);
      } else {
        cb([]);
      }
    }, err => {
      console.log(err);
      cb([]);
    });
  }

  GetReports () {
    // this.showSpinner = true
    // this.cdf.detectChanges();
    console.log(this.FMReportFilterValues,"FMReportFilterValues")
    console.log(this.filtertype,"this.filterType")
    // let courseIds: any;
    let param: any;
  this.spinner.show();
    // this.showSpinner = true;
    this.cdf.detectChanges();
    this.FMReadyForWorking = false;
    if (this.FMReportFilterValues.Ids.length > 0) {
      if (this.FMReportFilterValues.Ids.length > this.filtertype.limit) {
        // alert("You cannot select more than " + this.filtertype.limit);
        this.presentToast('warning', 'You cannot select more than ' + this.filtertype.limit);
        this.spinner.hide();
        // this.showSpinner = false;
        // this.cdf.detectChanges();
        return null;
      } else{
        const courseIds = this.populateString(this.FMReportFilterValues.Ids);

        param = {
          wfIds : courseIds,
         // courseIds : courseIds,
         // courseTypeId : this.courseTypeId,
          filter: this.FMReportFilterValues.Filters
        };
      }

      const reportFilters = this.FMReportFilterValues.Filters; // this.populateFMFilter(this.FMReportFilterValues.Filters);
      this.GetReportCourseConsumptionTimeOut(param, (result) => {
        if (!Array.isArray(result)){
          this.spinner.hide();
          this.cdf.detectChanges();
          // type: 'TimeoutError',
          // const message = "Report taking too much time, can't be Previewed. Please Schedule the report";
          this.toastr.warning(result['message'], 'Warning');
          if(result['type'] === 'TimeoutError'){
            setTimeout(() =>{
              this.Exportreport();
            });
          }

        }else if(result.length !== 0){
          this.cdf.detectChanges();
        // Report['slice']['reportFilters'] = reportFilters;
        Report.dataSource.data = result;
        this.FMReport = {
          data: result,
          report: Report,
          toolbar: this.toolbar,
        };
        this.FMReadyForWorking = true;
        this.ReportFiltersSET = {
          FMReport: this.FMReport,
          FMReportList: this.FMReportList,
          FMReportName: this.ReportName,
          ReportFilter: reportFilters,
        };
        this.service.SET_Report_Filter_Values(this.ReportFiltersSET);
        // this.router.navigate(["pages/reports/report-viewer"]);
        // this.spinner.hide();
        this.showSpinner = false;
        this.cdf.detectChanges();
        this.GoTOReportViewer();
      } else if(result.length === 0){
        this.spinner.hide()
      //  this.spinner.hide();
        this.showSpinner = false;
        this.cdf.detectChanges();
        this.presentToast('warning', 'No data available');
      }
      });

    } else {
      // this.spinner.hide();
      this.showSpinner = false;
      this.cdf.detectChanges();
      // const report: Toast = {
      //   type: 'warning',
      //   title: 'Select a course!',
      //   body: '',
      //   showCloseButton: true,
      //   timeout: 2000,
      // };
      // this.toasterService.pop(report);
      this.presentToast('warning', 'Select a course');

      // alert('select a course');
    }
  }
  DownlaodReports () {
    this.spinner.show();
    // this.showSpinner = true;
    this.cdf.detectChanges();
    this.FMReadyForWorking = false;
    if (this.FMReportFilterValues.Ids.length > 0) {
      if (this.FMReportFilterValues.Ids.length > this.filtertype.limit) {
        // alert("You cannot select more than " + this.filtertype.limit);
        this.presentToast('warning', 'You cannot select more than ' + this.filtertype.limit);
        this.spinner.hide();
        // this.showSpinner = false;
        this.cdf.detectChanges();
        return null;
      }
      const courseIds = this.populateString(this.FMReportFilterValues.Ids);
      const param = {
        courseIds : courseIds,
        courseTypeId: this.courseTypeId
      };
      this.GetReportCourseConsumptionDownload(param, (result) => {
        console.log("activity report data", result);
        if(result.length !== 0){
          this.excelservice.exportAsExcelFile(result, this.ReportName);
          this.spinner.hide();
          // this.showSpinner = false;
          this.cdf.detectChanges();
        } else  if(result.length === 0){
          this.presentToast('warning', 'No data available');
          this.spinner.hide();
          // this.showSpinner = false;
          this.cdf.detectChanges();
        }else {
          this.spinner.hide();
          // this.showSpinner = false;
          this.cdf.detectChanges();
          this.presentToast('error', '');
        }
      })
    }
    else {
      this.presentToast('warning', 'Select a course');
      this.spinner.hide();
      // this.showSpinner = false;
      this.cdf.detectChanges();
    }
  }
  populateString (list = []) {
    if (list.length === 0) {
      return list;
    }
    return list.join(',');
  }

  populateFMFilter(filters) {
    const reportFilters = [];
    try {
      if (filters) {
        for (const filter in filters) {
          const reportFilter = {
            'uniqueName': 'tags',
            'filter': {
              'members': ['tags.[dc3]', 'tags.[demo3]'],
            },
            'sort': 'unsorted'
          };
          if (filter && filters[filter].length > 0) {
            const array = filters[filter];
            const uniqueName = filter; // String(filter).toLowerCase();
            reportFilter['uniqueName'] = uniqueName;
            const memberslist = [];
            array.forEach(element => {
              const member = uniqueName + '.' + '[' + element + ']';
              memberslist.push(member);
            });
            reportFilter['filter']['members'] = memberslist;
            reportFilters.push(reportFilter);
          }
        }
      }
      return reportFilters;
    } catch (e) {
      return reportFilters;
    }
  }
  GoToBack() {
    window.history.back();
  }

  GoTOReportViewer () {
    this.router.navigate(['pages/reports/report-viewer', this.menuId]);
  }
  Exportreport() {
    console.log(this.FMReportFilterValues,"ygygygyt");
    if (this.FMReportFilterValues.courseName.length > 0) {
      if (this.FMReportFilterValues.courseName.length > this.filtertype.limit) {
        this.presentToast('warning', 'You cannot select more than ' + this.filtertype.limit);
        // this.spinner.hide();
        // return null;
      } else {
        const courseName = this.populateString(this.FMReportFilterValues.courseName);
        // this.service.courseName = courseName
        this.exports = true;
        // this.router.navigate(['pages/reports/ReportSchedule']);
      }
    } else {
      this.presentToast('warning', 'Select a course');
      this.spinner.hide();
      // this.showSpinner = false;
      this.cdf.detectChanges();
    }
  }

  closedmodel(data) {
    console.log(data);
    this.exports = false;
    if (data.res == 2) {
      this.router.navigate(['pages/reports/ReportList']);
      // this.FMReportFilterValues = [];
    }
  }

  /**
   * Filter
   */
  filter: any = {
    primary: false,
    secondary: false,
  };
  filters: any = {
    primary: [],
    secondary: [],
  };
  filtersInner: any = {
    primary: [],
    secondary: [],
  };
  filtersCache: any = {
    primary: [],
    secondary: [],
  };
  filtercon: Filter = {
    ascending: false,
    descending: false,
    showDropdown: false,
    dropdownList: [
      { drpName: 'Enrol Date', val: 1 },
      { drpName: 'Created Date', val: 2 },
    ],
  };
  filterSecondaryCon: Filter = {
    ascending: false,
    descending: false,
    showDropdown: false,
    dropdownList: [
      { drpName: 'Enrol Date', val: 1 },
      { drpName: 'Created Date', val: 2 },
    ],
  };
  filterErrorObject = {
    'disabledPreviewAndGetReports': false,
    'disabledPreviewAndGetReportsErrorMessage': '',
  };
  gotoFilter(type) {
    if (type === 'primary') {
      this.filter.secondary = false;
      this.filter.primary = !this.filter.primary;
    } else if (type === 'secondary') {
      this.filter.primary = false;
      this.filter.secondary = !this.filter.secondary;
    }
    // this.filter = !this.filter;
  }
  bindfilter(obj, filterType) {
    let filtername, filterValueName, type, singleSelection, isMandatory, joiner;
    if (obj.length > 0) {
      filtername = obj[0]['filterId'];
      filterValueName = obj[0]['filterValue'];
      type = obj[0]['type'];
      singleSelection = obj[0]['singleSelection'];
      isMandatory = obj[0]['isMandatory'] && obj[0]['isMandatory'] === 1 ? true : false;
      joiner = obj[0]['joiner'];
    }
    const item = {
          count: '',
          value: '',
          tagname: filtername,
          isChecked: false,
          list: obj,
          filterValue: filterValueName,
          type: type,
          filterId: filtername,
          singleSelection: singleSelection,
          isMandatory: isMandatory,
          isError: isMandatory,
          joiner: joiner,
    };
    if (filtername) {
          if (filterType === 'primary') {
            this.filters.primary.push(item);
          } else if (filterType === 'secondary') {
            this.filters.secondary.push(item);
          }
    }
  }
  filteredPrimaryChanged(event) {
    console.log('Filtered Primary Event - ', event);
    if (!event) event = {};
    event.primaryflag = true;
    // if(!event.errorObj.error){
    //   this.getSelectedFilter(event);
    // }else {
    //   // this.bindErrorMessageData(event.errorObj);
    //   this.toastr.warning(event.errorObj.errorMessage, 'Warning');
    // }
    this.getSelectedFilter(event);
    this.bindFilterCountToheader('primary', event['filteredCount']);
  }
  filteredSecondaryChanged(event) {
    console.log('Filtered Secondary Event - ', event);
    if (!event) event = {};
    event.secondaryflag = true;
    this.getSelectedFilter(event);
    this.bindFilterCountToheader('secondary', event['filteredCount']);
    // this.bindErrorMessageData(event.errorObj);
    // if(!event.errorObj.error){
    //   this.getSelectedFilter(event);
    // }else {
    //   this.bindErrorMessageData(event.errorObj);
    //   // this.toastr.warning(event.errorObj.errorMessage, 'Warning');
    // }
  }

  bindErrorMessageData(event){
    this.filterErrorObject.disabledPreviewAndGetReports = event.error;
    this.filterErrorObject.disabledPreviewAndGetReportsErrorMessage = event.message;
  }

  checkForValidFilter(courseTag){
    // let isError = false;
    let errorMessage = '';
    let errorCount = 0;
    courseTag.forEach((value: any, key) => {
      if(value.isMandatory){
            errorCount ++;
            // if(!isError){
            //   errorMessage = 'The field(s) ';
            // }
            // if(isError){
            //   errorMessage += ', ' + value.tagname + ' ';
            // }
            if(errorMessage !== '') {
              errorMessage += ', ' + value.tagname + ' ';
            }else {
              errorMessage += ' '+ value.tagname;
            }
          }
          });
    if(errorCount > 1){
      errorMessage = 'Fields ' + errorMessage + ' are mandetory';
      // this.toast.warning(errorMessage, 'Warning');
    }else if (errorCount === 1) {
      errorMessage = 'The field' + errorMessage + ' is mandetory';
      // this.toast.warning(errorMessage, 'Warning');
    }
    const errorObj = {
      'message' : errorMessage,
      error: errorCount > 0,
    }
    // console.log('Errored Array ===>', this.filterError);
    // return errorObj;
    this.bindErrorMessageData(errorObj);
  }

  sendSaveBtn(val) {
    this.isdata = val;
    setTimeout(() => {
      this.isdata = '';
    }, 2000);
  }

  closeSiderBar() {
    this.exports = false;
  }
  
  bindFilterCountToheader(filterType,value){
    const header = this.header;
    if(filterType=== 'primary'){
      if(value){
        header.filterConfig.primarySelectedFilteredCount = value;
      }else {
        header.filterConfig.primarySelectedFilteredCount = 0;
      }
    }else {
      if(value){
        header.filterConfig.secondarySelectedFilteredCount = value;
      }else {
        header.filterConfig.secondarySelectedFilteredCount = 0;
      }
    }
    this.header = _.cloneDeepWith(header);
  }

  bindValuesTofilterButtons(){
    if(this.FMReportFilterObj['main'] && (this.FMReportFilterObj['main'].length === 0)){
      const headerObj = this.header;
      headerObj.filterConfig.primary = false;
      this.header = _.cloneDeep(headerObj);
    }
    if(this.FMReportFilterObj['sub'] && this.FMReportFilterObj['sub'].length === 0){
      const headerObj = this.header;
      headerObj.filterConfig.secondary = false;
      this.header = _.cloneDeep(headerObj);
    }
  }

}

