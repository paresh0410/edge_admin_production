import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EepReportComponent } from './eep-report.component';

describe('EepReportComponent', () => {
  let component: EepReportComponent;
  let fixture: ComponentFixture<EepReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EepReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EepReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
