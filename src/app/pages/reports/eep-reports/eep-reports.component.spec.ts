import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EepReportsComponent } from './eep-reports.component';

describe('EepReportsComponent', () => {
  let component: EepReportsComponent;
  let fixture: ComponentFixture<EepReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EepReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EepReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
