export let Report = {
    "dataSource": {
        "dataSourceType": "json",
        "data": []
    },
    "slice": {
        "rows": [{
                "uniqueName": "code",
                "sort": "asc"
            },
            {
                "uniqueName": "age",
                "sort": "unsorted"
            },
            {
                "uniqueName": "aopc",
                "sort": "unsorted"
            },
            {
                "uniqueName": "aopn",
                "sort": "unsorted"
            },
            {
                "uniqueName": "aops",
                "sort": "unsorted"
            },
            {
                "uniqueName": "attendance",
                "sort": "unsorted"
            },
            {
                "uniqueName": "attinfodata",
                "sort": "unsorted"
            },
            {
                "uniqueName": "band",
                "sort": "unsorted"
            },
            {
                "uniqueName": "bloodgroup",
                "sort": "unsorted"
            },
            {
                "uniqueName": "business",
                "sort": "unsorted"
            },
            {
                "uniqueName": "ccc",
                "sort": "unsorted"
            },
            {
                "uniqueName": "ccn",
                "sort": "unsorted"
            },
            {
                "uniqueName": "city",
                "sort": "unsorted"
            },
            {
                "uniqueName": "contactDetails",
                "sort": "unsorted"
            },
            {
                "uniqueName": "currenttenuremonth",
                "sort": "unsorted"
            },
            {
                "uniqueName": "currenttenureyear",
                "sort": "unsorted"
            },
            {
                "uniqueName": "department",
                "sort": "unsorted"
            },
            {
                "uniqueName": "designation",
                "sort": "unsorted"
            },
            {
                "uniqueName": "dob",
                "sort": "unsorted"
            },
            {
                "uniqueName": "doj",
                "sort": "unsorted"
            },
            {
                "uniqueName": "email",
                "sort": "unsorted"
            },
            {
                "uniqueName": "empcategory",
                "sort": "unsorted"
            },
            {
                "uniqueName": "empclassification",
                "sort": "unsorted"
            },
            {
                "uniqueName": "employeestatus",
                "sort": "unsorted"
            },
            {
                "uniqueName": "father",
                "sort": "unsorted"
            },
            {
                "uniqueName": "firstname",
                "sort": "unsorted"
            },
            {
                "uniqueName": "gender",
                "sort": "unsorted"
            },
            {
                "uniqueName": "grade",
                "sort": "unsorted"
            },
            {
                "uniqueName": "id",
                "sort": "unsorted"
            },
            {
                "uniqueName": "inductionLocation",
                "sort": "unsorted"
            },
            {
                "uniqueName": "invited",
                "sort": "unsorted"
            },
            {
                "uniqueName": "jobcode",
                "sort": "unsorted"
            },
            {
                "uniqueName": "joined",
                "sort": "unsorted"
            },
            {
                "uniqueName": "lastname",
                "sort": "unsorted"
            },
            {
                "uniqueName": "level",
                "sort": "unsorted"
            },
            {
                "uniqueName": "ljclicks",
                "sort": "unsorted"
            },
            {
                "uniqueName": "locationcode",
                "sort": "unsorted"
            },
            {
                "uniqueName": "logins",
                "sort": "unsorted"
            },
            {
                "uniqueName": "martialstatus",
                "sort": "unsorted"
            },
            {
                "uniqueName": "name",
                "sort": "unsorted"
            },
            {
                "uniqueName": "orgcode",
                "sort": "unsorted"
            },
            {
                "uniqueName": "password",
                "sort": "unsorted"
            },
            {
                "uniqueName": "permanentadd",
                "sort": "unsorted"
            },
            {
                "uniqueName": "permanentcity",
                "sort": "unsorted"
            },
            {
                "uniqueName": "permanentpincode",
                "sort": "unsorted"
            },
            {
                "uniqueName": "permanentstate",
                "sort": "unsorted"
            },
            {
                "uniqueName": "personalemail",
                "sort": "unsorted"
            },
            {
                "uniqueName": "personalmobile",
                "sort": "unsorted"
            },
            {
                "uniqueName": "positioncode",
                "sort": "unsorted"
            },
            {
                "uniqueName": "presentaddress",
                "sort": "unsorted"
            },
            {
                "uniqueName": "presentcity",
                "sort": "unsorted"
            },
            {
                "uniqueName": "presentpincode",
                "sort": "unsorted"
            },
            {
                "uniqueName": "presentstate",
                "sort": "unsorted"
            },
            {
                "uniqueName": "remarks",
                "sort": "unsorted"
            },
            {
                "uniqueName": "reportingmanagercode",
                "sort": "unsorted"
            },
            {
                "uniqueName": "reportingmanagername",
                "sort": "unsorted"
            },
            {
                "uniqueName": "rmemail",
                "sort": "unsorted"
            },
            {
                "uniqueName": "role",
                "sort": "unsorted"
            },
            {
                "uniqueName": "sdepartment",
                "sort": "unsorted"
            },
            {
                "uniqueName": "SkillLevel",
                "sort": "unsorted"
            },
            {
                "uniqueName": "skiplevelmcode",
                "sort": "unsorted"
            },
            {
                "uniqueName": "skiplevelmname",
                "sort": "unsorted"
            },
            {
                "uniqueName": "spousename",
                "sort": "unsorted"
            },
            {
                "uniqueName": "ssdepartment",
                "sort": "unsorted"
            },
            {
                "uniqueName": "state",
                "sort": "unsorted"
            },
            {
                "uniqueName": "tier",
                "sort": "unsorted"
            },
            {
                "uniqueName": "trainingDate",
                "sort": "unsorted"
            },
            {
                "uniqueName": "trainingvenue",
                "sort": "unsorted"
            },
            {
                "uniqueName": "zone",
                "sort": "unsorted"
            }
        ],
        "columns": [{
            "uniqueName": "[Measures]"
        }],
        "measures": [{
            "uniqueName": "userId",
            "aggregation": "sum"
        }],
        "flatOrder": [
            "code",
            "userId",
            "age",
            "aopc",
            "aopn",
            "aops",
            "attendance",
            "attinfodata",
            "band",
            "bloodgroup",
            "business",
            "ccc",
            "ccn",
            "city",
            "contactDetails",
            "currenttenuremonth",
            "currenttenureyear",
            "department",
            "designation",
            "dob",
            "doj",
            "email",
            "empcategory",
            "empclassification",
            "employeestatus",
            "father",
            "firstname",
            "gender",
            "grade",
            "id",
            "inductionLocation",
            "invited",
            "jobcode",
            "joined",
            "lastname",
            "level",
            "ljclicks",
            "locationcode",
            "logins",
            "martialstatus",
            "name",
            "orgcode",
            "password",
            "permanentadd",
            "permanentcity",
            "permanentpincode",
            "permanentstate",
            "personalemail",
            "personalmobile",
            "positioncode",
            "presentaddress",
            "presentcity",
            "presentpincode",
            "presentstate",
            "remarks",
            "reportingmanagercode",
            "reportingmanagername",
            "rmemail",
            "role",
            "sdepartment",
            "SkillLevel",
            "skiplevelmcode",
            "skiplevelmname",
            "spousename",
            "ssdepartment",
            "state",
            "tier",
            "trainingDate",
            "trainingvenue",
            "zone"
        ]
    },
    "options": {
        "grid": {
            "type": "flat"
        }
    }
}