import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReactionHomeComponent } from './reaction-home.component';

describe('ReactionHomeComponent', () => {
  let component: ReactionHomeComponent;
  let fixture: ComponentFixture<ReactionHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReactionHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReactionHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
