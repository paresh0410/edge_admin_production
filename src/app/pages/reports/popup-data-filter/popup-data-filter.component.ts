import { Component, OnInit, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';
import {NgForm} from '@angular/forms';
import { DatePipe } from '@angular/common';
declare var window: any;
export const MY_CUSTOM_FORMATS = {
  fullPickerInput: 'DD-MM-YYYY HH:mm:ss',
  parseInput: 'DD-MM-YYYY HH:mm:ss',
  datePickerInput: 'DD-MM-YYYY',
  timePickerInput: 'LT',
  monthYearLabel: 'MMM YYYY',
  dateA11yLabel: 'LL',
  monthYearA11yLabel: 'MMMM YYYY'
};

@Component({
  selector: 'ngx-popup-data-filter',
  templateUrl: './popup-data-filter.component.html',
  styleUrls: ['./popup-data-filter.component.scss'],
  providers: [DatePipe,
    {provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE]},
    {provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS},]
})
export class PopupDataFilterComponent implements OnInit {
  @ViewChild('filterForm', { read: NgForm }) form: any;
  @Input() items: any[];
  @Input() filter: any[];
  @Input() sfilter: any[];
  @Input() menuId?: any;
  @Input() showSubmittedError: boolean = false;
  @Input() filtertype: any = {
    primary: null,
    secondary: null
  };
  @Input() cacheFilter?: any;
  noDataInMain: boolean;
  noDataInSub: boolean;
  selected_filter_Items: any;
  selected_sfilter_Items: any;
  filterstartworking: boolean = false;
  filterdropdownSettings: any = {};
  sfilterdropdownSettings: any = {};
  dropdownSettings = {
    singleSelection: false,
    text: "Select Countries",
    selectAllText:'Select All',
    unSelectAllText:'UnSelect All',
    enableSearchFilter: true,
    classes:"common-multi",
    badgeShowLimit: 3,
    lazyLoading: true,
  };
  filterJSON:any = {
    primary: {},
    secondary: {}
  };
  filterError = {
    main: [],
    secondary: [],
  };
  // items = [
  //   { Id: 1, Name: 'English' },
  //   { Id: 2, Name: 'Hindi' },
  //   { Id: 3, Name: 'Marathi' },
  // ];
 /**
   * Events
  */
 @Output() onSelectFilter = new EventEmitter<any>();
  constructor(private commonservice: CommonFunctionsService) {
    this.filterstartworking = false;
  }

  ngOnInit() {
    this.filterstartworking = false;
    this.selected_filter_Items = {};
    this.selected_sfilter_Items= {};
    console.log("Filter Data", this.filter);
    if (this.filter.length > 0) {
      // this.createValidationArray();
      for(let i = 0; i < this.filter.length; i++) {
        if (this.filter[i].length > 0) {
          const obj = {
            'Name': this.filter[i][0]['title'],
            'paramName' : this.filter[i][0]['filterId'],
            'type' : this.filter[i][0]['type'],
            'separator' : this.filter[i][0]['joiner'],
            // 'filterId' : this.filter[i][0]['filterId']
            // 'isError': false,
          };
          if(this.filter[i][0]['isMandatory']){
            obj['isMandatory'] = true;
            obj['isError'] = true;
            // this.filterError[i] = obj;
          }else {
            obj['isMandatory'] = false;
            obj['isError'] = false;
            // this.filterError[i] = false;
          }
          this.filterError['main'][i] = obj;
          let setting = this.convert_syncronize_text(this.syncronize_text(this.dropdownSettings));
          this.selected_filter_Items[this.filter[i][0]['title']] = [];
          this.filterdropdownSettings[this.filter[i][0]['title']] = setting;
          setting.text = "Select " + this.filter[i][0]['title'];
          this.filterdropdownSettings[this.filter[i][0]['title']]['text'] = setting.text;
          setting.singleSelection = this.filter[i][0]['singleSelection'] || false;
        }
      }

    } else if(this.filter.length === 0){
      this.noDataInMain = true;
    }
    if (this.sfilter.length > 0) {
      for(let i = 0; i < this.sfilter.length; i++) {
        if (this.sfilter[i].length > 0) {
          const obj = {
            'Name': this.sfilter[i][0]['title'],
            'paramName' : this.sfilter[i][0]['filterId'],
            'type' : this.sfilter[i][0]['type'],
            'separator' : this.sfilter[i][0]['joiner'],
            // 'filterId' : this.filter[i][0]['filterId']
            // 'isError': false,
          };
          if(this.sfilter[i][0]['isMandatory']){
            obj['isMandatory'] = true;
            obj['isError'] = true;
            // this.filterError[i] = obj;
          }else {
            obj['isMandatory'] = false;
            obj['isError'] = false;
            // this.filterError[i] = false;
          }
          this.filterError['secondary'][i] = obj;
          let setting = this.convert_syncronize_text(this.syncronize_text(this.dropdownSettings));
          this.selected_sfilter_Items[this.sfilter[i][0]['title']] = [];
          this.sfilterdropdownSettings[this.sfilter[i][0]['title']] = setting;
          setting.text = "Select " + this.sfilter[i][0]['title'];
          this.sfilterdropdownSettings[this.sfilter[i][0]['title']]['text'] = setting.text;
          setting.singleSelection = this.sfilter[i][0]['singleSelection'] || false;
        }
      }

    } else {
      this.noDataInSub = true;
    }
    console.log('Error Array ==>', this.filterError);
    console.log('Filter -', this.filterdropdownSettings);
    console.log('s Filter -', this.sfilterdropdownSettings);
    if (this.cacheFilter) {
      this.selected_filter_Items = this.cacheFilter['primaryCache'] ? this.cacheFilter['primaryCache'] : {};
      this.selected_sfilter_Items = this.cacheFilter['secondaryCache'] ? this.cacheFilter['secondaryCache'] : {};
      // this.filterJSON.primary = this.selected_filter_Items;
      // this.filterJSON.secondary = this.selected_sfilter_Items;
      this.filterJSON = this.cacheFilter;
    }
    this.filterstartworking = true;
  }



  convert(data) {
    console.log(data);
  }
  onItemSelect(item:any, id, type){
    console.log(item, id, type);
    if (this.filterJSON[type][id]) {
      if (this.filterJSON[type][id].length && this.filterJSON[type][id][0].singleSelection) {
        this.filterJSON[type][id] = [];
        this.filterJSON[type][id].push(item);
      } else {
        this.filterJSON[type][id].push(item);
      }
    } else {
      this.filterJSON[type][id] = [];
      this.filterJSON[type][id].push(item);
    }
    if (type === 'primary') {
      this.filterJSON.primaryflag = true;
      this.filterJSON.secondaryflag = true;
    } else {
      this.filterJSON.primaryflag === true ? true : false;
      this.filterJSON.secondarflag = true;
    }
    console.log(this.filterJSON);
    this.checkForValidFilter();
    if (this.onSelectFilter) {
      this.filterJSON.primaryCache = this.selected_filter_Items;
      this.filterJSON.secondaryCache = this.selected_sfilter_Items;
      this.filterJSON.clear = false;
      this.filterJSON['formValid'] = this.form.valid;
      this.filterJSON['filterStatus'] = this.filterError;
      this.onSelectFilter.emit(this.filterJSON);
    }
  }
  checkForValidFilter(){
    for (let index = 0; index < this.filterError['main'].length; index++) {
      // const element = array[index];
      if(this.filterError['main'][index].isMandatory){
        if(this.filterJSON['primary'][this.filterError['main'][index].Name] &&
        this.filterJSON['primary'][this.filterError['main'][index].Name].length !== 0){
          this.filterError['main'][index].isError = false;
        }else {
          this.filterError['main'][index].isError = true;
        }
      }
    }
    console.log('Errored Array ===>', this.filterError);
  }
  OnItemDeSelect(item:any, id, type){
      console.log(item);
      console.log(this.selected_filter_Items);
      if (type === 'primary') {
        this.filterJSON.primaryflag = true;
        this.filterJSON.secondaryflag = true;
      } else {
        this.filterJSON.primaryflag === true ? true : false;
        this.filterJSON.secondarflag = true;
      }
      if (this.filterJSON[type][id] && this.filterJSON[type][id].length > 0) {
        let index = 0;
        this.filterJSON[type][id].every((value, key) => {
          if (item.id === value.id) {
            index = key;
            return false;
          } else {
            return true;
          }
        })
        this.filterJSON[type][id].splice(index, 1);
        if (this.filterJSON[type][id] && this.filterJSON[type][id].length == 0) {
          delete this.filterJSON[type][id];
        }
      }
      if (this.onSelectFilter) {
        this.filterJSON.primaryCache = this.selected_filter_Items;
        this.filterJSON.secondaryCache = this.selected_sfilter_Items;
        this.filterJSON.clear = false;
        this.filterJSON['formValid'] = this.form.valid;
        this.filterJSON['filterStatus'] = this.filterError;
        this.onSelectFilter.emit(this.filterJSON);
      }
      this.checkForValidFilter();
  }
  onSelectAll(items: any, id, type){
      console.log(items);
      this.filterJSON[type][id] = items;
      if (type === 'primary') {
        this.filterJSON.primaryflag = true;
        this.filterJSON.secondaryflag = true;
      } else {
        // this.filterJSON.primaryflag = false;
        this.filterJSON.primaryflag === true ? true : false;
        this.filterJSON.secondarflag = true;
      }
      this.checkForValidFilter();
      if (this.onSelectFilter) {
        this.filterJSON.primaryCache = this.selected_filter_Items;
        this.filterJSON.secondaryCache = this.selected_sfilter_Items;
        this.filterJSON.clear = false;
        this.filterJSON['formValid'] = this.form.valid;
        // this.filterJSON['paramsObject'] =
        this.filterJSON['filterStatus'] = this.filterError;
        this.onSelectFilter.emit(this.filterJSON);
      }

  }
  onDeSelectAll(items: any, id, type){
      console.log(items);
      this.filterJSON[type][id] = [];
      if (type === 'primary') {
        this.filterJSON.primaryflag = true;
        this.filterJSON.secondaryflag = true;
      } else {
        // this.filterJSON.primaryflag = false;
        this.filterJSON.primaryflag === true ? true : false;
        this.filterJSON.secondarflag = true;
      }
      this.checkForValidFilter();
      if (this.onSelectFilter) {
        this.filterJSON.primaryCache = this.selected_filter_Items;
        this.filterJSON.secondaryCache = this.selected_sfilter_Items;
        this.filterJSON.clear = false;
        this.filterJSON['formValid'] = this.form.valid;
        this.filterJSON['filterStatus'] = this.filterError ? this.filterError : [];
        this.onSelectFilter.emit(this.filterJSON);
      }
  }
  /**
   * Date time input change
   * @param item
   */
  onDateInputChange (event, id, type) {
    console.log('Date time event : ', event);
    if (type === 'primary') {
      this.filterJSON.primaryflag = true;
      this.filterJSON.secondaryflag = true;
    } else {
      // this.filterJSON.primaryflag = false;
      this.filterJSON.primaryflag === true ? true : false;
      this.filterJSON.secondarflag = true;
    }
    this.filterJSON[type][id] = [];
    if (event.value && event.value.length > 0) {
      event.value.forEach( (value, key) => {
        const datetime = this.commonservice.formatDigitDate(value);
        this.filterJSON[type][id].push(datetime);
      })
    }
    this.checkForValidFilter();
    // this.filterJSON[type][id] = event.value;
    if (this.onSelectFilter) {
      this.filterJSON.primaryCache = this.selected_filter_Items;
      this.filterJSON.secondaryCache = this.selected_sfilter_Items;
      this.filterJSON.clear = false;
      this.filterJSON['formValid'] = this.form.valid;
      this.filterJSON['filterStatus'] = this.filterError;
      this.onSelectFilter.emit(this.filterJSON);
    }
  }
  removeSpace(item) {
    try {
      if (item) {
        item = item.split(' ');
        item = item.join('_');
        // console.log(item);
        return item;
      }
    } catch (e) {
      return item;
    }
  }
  syncronize_text (value) {
    try {
      value = JSON.stringify(value);
      return btoa(value);
    } catch (e) {
      console.log('syncronize_text', e.message);
      return value;
    }
  }
  convert_syncronize_text (value) {
    try {
      value = atob(value);
      return JSON.parse(value);
    } catch (e) {
      console.log('convert_syncronize_text', e.message);
      return value;
    }
  }

  clearDateValue( id, type){
    if (type === 'primary') {
      this.filterJSON.primaryflag = true;
      this.filterJSON.secondaryflag = false;
      this.selected_filter_Items[id]['date'] = [];
    } else {
      // this.filterJSON.primaryflag = false;
      this.filterJSON.primaryflag === true ? true : false;
      this.filterJSON.secondarflag = true;
      this.selected_sfilter_Items[id]['date'] = [];
    }
    this.filterJSON[type][id] = []; // event.value;
    if (this.onSelectFilter) {
      this.filterJSON.primaryCache = this.selected_filter_Items;
      this.filterJSON.secondaryCache = this.selected_sfilter_Items;
      this.filterJSON.clear = true;
      this.filterJSON['formValid'] = this.form.valid;
      this.filterJSON['filterStatus'] = this.filterError;
      this.onSelectFilter.emit(this.filterJSON);
    }
  }
  // clearvalue (type) {
  //   if (type === 'primary') {
  //     this.filterJSON.primaryflag = true;
  //     this.filterJSON.secondaryflag = false;
  //     for(let id in this.filterJSON[type]) {
  //       if (this.filterJSON[type][id]['date']) {
  //         this.selected_filter_Items[id]['date'] = [];
  //         this.filterJSON[type][id]['date'] = [];
  //       } else {
  //         this.selected_filter_Items[id] = [];
  //         this.filterJSON[type][id] = [];
  //       }
  //     }
  //   } else {
  //     // this.filterJSON.primaryflag = false;
  //     this.filterJSON.primaryflag === true ? true : false;
  //     this.filterJSON.secondarflag = true;
  //     for(let id in this.filterJSON[type]) {
  //       if (this.filterJSON[type][id]['date']) {
  //         this.selected_sfilter_Items[id]['date'] = [];
  //         this.filterJSON[type][id]['date'] = [];
  //       } else {
  //         this.selected_sfilter_Items[id] = [];
  //         this.filterJSON[type][id] = [];
  //       }
  //     }
  //   }
  //   if (this.onSelectFilter) {
  //     this.filterJSON.primaryCache = this.selected_filter_Items;
  //     this.filterJSON.secondaryCache = this.selected_sfilter_Items;
  //     this.filterJSON.clear = false;
  //     this.onSelectFilter.emit(this.filterJSON);
  //   }
  // }
  onSettings (item) {
    return {
      singleSelection: item.singleSelection || false,
      text: item.title,
      selectAllText:'Select All',
      unSelectAllText:'UnSelect All',
      enableSearchFilter: true,
      classes:"myclass custom-class"
    }
  }

  popupFilterForm(form){
    console.log('Form values ===>',form);
    console.log('Form values ===>',form.controls);
    console.log('Form values ===>',form.valid);
    Object.keys(form.controls).forEach(key => {
      form.controls[key].markAsDirty();
    });
  }
}
