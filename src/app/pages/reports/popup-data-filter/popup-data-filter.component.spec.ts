import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupDataFilterComponent } from './popup-data-filter.component';

describe('PopupDataFilterComponent', () => {
  let component: PopupDataFilterComponent;
  let fixture: ComponentFixture<PopupDataFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupDataFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupDataFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
