import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ThemeModule } from '../../../@theme/theme.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { FilterPipeModule  } from 'ngx-filter-pipe';
import { OrderModule  } from 'ngx-order-pipe';
import { FlexmonsterPivotModule } from 'ng-flexmonster';
import { SharedModule } from '../shared.modules';
import { FmReportComponent } from '../../../component/fm-report/fm-report.component';
import { SideMenuFilterComponent } from '../../../component/side-menu-filter/side-menu-filter.component';
// import { ComponentModule } from '../../component/component.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { ReportsModule } from '../reports.module';

@NgModule({
  imports: [
    ThemeModule,
    NgxPaginationModule,
    FilterPipeModule,
    OrderModule,
    AngularMultiSelectModule,
    FlexmonsterPivotModule,
    NgxDatatableModule,
    SharedModule,
    // ReportsModule
  ],
  declarations: [
    // SideMenuFilterComponent,
    //ReportViewerComponent
  ],
  schemas: [ 
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA 
  ]
})
export class ReportViewerModule { }
