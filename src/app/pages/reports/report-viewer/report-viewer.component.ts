import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ReportsService } from '../reports.service';
import { Report } from '../report';
import { ActivatedRoute, Router } from '@angular/router';
import { FMReportEntity } from '../../../component/fm-report/fm-report';
import { NgxSpinnerService } from 'ngx-spinner';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { NgForm } from '@angular/forms';
import { SuubHeader } from '../../components/models/subheader.model';
import { BrandDetailsService } from '../../../service/brand-details.service';

@Component({
  selector: 'ngx-report-viewer',
  templateUrl: './report-viewer.component.html',
  styleUrls: ['./report-viewer.component.scss'],
  providers: [],
})
export class ReportViewerComponent implements OnInit {
  FMReport: any;
  FMReportList: any = [];
  SelectedReport: any;
  FMReadyForWorking: boolean = false;
  ReportName: string;
  menuId: number;
  TempFMReport: any;
  selectedReportFromList: any;
  openPopUpModal: boolean = false;
  addReport: boolean = true;
  updateReport: boolean = false;
  selected_Tab: any;
  header: SuubHeader;
  disableModalButton: boolean = false;
  // reportname: NgForm;
  // reportSelection: NgForm;
  ReportInfo: FMReportEntity = {
    report_name: null,
    report_id: null,
    metadata: null,
    userid: 0,
    report_map_id: null,
    menuId: null,
  };
  FMReportFilterValues: any = {
    Ids: [],
    Filters: {},
    courseName: [],
    courseTypeId: '',
    menuId: '',
    AcitivityIds: [],
    filterId: '',
  };
  Report_Name_Temp: string = null;
  Selected_Report_Temp: any = 0;
  submitted: boolean = false;
  disableUpdateTab: boolean = false;
  disableRepoName: boolean;
  hideSelectReport: boolean;
  hideReportInput: boolean;
  ReportFilters: any;
  openPopUp: boolean;
  exports: boolean;
  currentBrandData: any;

  notiTitle:string="Report Schedule"
  btnName: string = 'Save';
  btnName1: string = 'Save & Show';
  isdata: any;

  constructor(private reportService: ReportsService, private router: Router,
    // private toasterService: ToasterService, 
    private spinner: NgxSpinnerService,
    public brandService: BrandDetailsService,
    public cdf: ChangeDetectorRef, private routes: ActivatedRoute) {

    this.menuId = this.routes.snapshot.params['menuId'] == undefined ? 0 : Number(this.routes.snapshot.params['menuId']);
    this.FMReportFilterValues.menuId = this.menuId;

  }

  ngOnInit() {

    this.SetReport();
    this.currentBrandData = this.brandService.getCurrentBrandData();
    this.header.title = "Preview";
    console.log("menuid ", this.menuId);
    if (this.menuId === 62) {
      this.header['btnName2show'] = true;
      this.header.breadCrumbList = [
        {
          'name': 'Reports',
          'navigationPath': '/pages/reports',
        },
        {
          'name': 'Workflow',
          'navigationPath': '/pages/reports/reports_home',
        }]
    }
    if (this.menuId === 44) {
      this.header.breadCrumbList = [
        {
          'name': 'Reports',
          'navigationPath': '/pages/reports',
        },
        {
          'name': 'Online Course',
          'navigationPath': '/pages/reports/reports_home',
        },
        {
          'name': 'Course Consumption',
          'navigationPath': '/pages/reports/reports_home/course-consumption/44',
        }
      ]
    }
    if (this.menuId === 47) {
      this.header.breadCrumbList = [
        {
          'name': 'Reports',
          'navigationPath': '/pages/reports',
        },
        {
          'name': 'Online Course',
          'navigationPath': '/pages/reports/reports_home',
        },
        {
          'name': 'Quiz Consumption',
          'navigationPath': '/pages/reports/reports_home/quiz-consumption/47',
        }
      ]
    }

    if (this.menuId === 46) {
      this.header.breadCrumbList = [
        {
          'name': 'Reports',
          'navigationPath': '/pages/reports',
        },
        {
          'name': 'Online Course',
          'navigationPath': '/pages/reports/reports_home',
        },
        {
          'name': 'Activity Consumption',
          'navigationPath': '/pages/reports/reports_home/activity-consumption/46',
        }
      ]
    }
    if (this.ReportName == "Feedback Consumption") {
      this.header.breadCrumbList =
        [{
          'name': 'Reports',
          'navigationPath': '/pages/reports',
        },
        {
          'name': 'Online Course',
          'navigationPath': '/pages/reports/reports_home',
        },
        {
          'name': 'Feedback Consumption',
          'navigationPath': '/pages/reports/reports_home/feedback-consumption/48',
        }
        ]
    }

    if (this.menuId == 108) {
      this.header.breadCrumbList =
        [
          {
            'name': 'Reports',
            'navigationPath': '/pages/reports',
          },
          {
            'name': 'Online Course',
            'navigationPath': '/pages/reports/reports_home',
          },
          {
            'name': 'Quiz Question Response Report',
            'navigationPath': '/pages/reports/reports_home/quiz-consumption/108',
          }
        ]
    }
    if (this.menuId === 119) {
      this.header.breadCrumbList =
        [
          {
            'name': 'Reports',
            'navigationPath': '/pages/reports',
          },
          {
            'name': 'Online Course',
            'navigationPath': '/pages/reports/reports_home',
          },
          {
            'name': 'Quiz Cumulative',
            'navigationPath': '/pages/reports/reports_home/quiz-consumption/119',
          }
        ]
    }

    if (this.menuId === 50) {
      this.header.breadCrumbList = [
        {
          'name': 'Reports',
          'navigationPath': '/pages/reports',
        },
        {
          'name': 'Classroom Course ',
          'navigationPath': '/pages/reports/reports_home',
        },
        {
          'name': 'Employee Attendance',
          'navigationPath': '/pages/reports/reports_home/employee-attendance/50',
        }]
    }
    if (this.menuId === 57) {
      this.header.breadCrumbList = [
        {
          'name': 'Reports',
          'navigationPath': '/pages/reports',
        },
        {
          'name': 'Classroom Course ',
          'navigationPath': '/pages/reports/reports_home',
        },
        {
          'name': 'Batch Consumption',
          'navigationPath': '/pages/reports/reports_home/classroom-reports/57',
        }]
    }
    if (this.menuId === 58) {
      this.header.breadCrumbList = [
        {
          'name': 'Reports',
          'navigationPath': '/pages/reports',
        },
        {
          'name': 'Classroom Course ',
          'navigationPath': '/pages/reports/reports_home',
        },
        {
          'name': 'Activity Consumption',
          'navigationPath': '/pages/reports/reports_home/classroom-reports/58',
        }]
    } if (this.menuId === 59) {
      this.header.breadCrumbList = [
        {
          'name': 'Reports',
          'navigationPath': '/pages/reports',
        },
        {
          'name': 'Classroom Course ',
          'navigationPath': '/pages/reports/reports_home',
        },
        {
          'name': 'Quiz Consumption Report',
          'navigationPath': '/pages/reports/reports_home/classroom-reports/59',
        }]
    }
    if (this.menuId === 60) {
      this.header.breadCrumbList = [
        {
          'name': 'Reports',
          'navigationPath': '/pages/reports',
        },
        {
          'name': 'Classroom Course ',
          'navigationPath': '/pages/reports/reports_home',
        },
        {
          'name': 'Feedback Consumption',
          'navigationPath': '/pages/reports/reports_home/classroom-reports/60',
        }]
    }
    if (this.menuId === 111) {
      this.header.breadCrumbList = [
        {
          'name': 'Reports',
          'navigationPath': '/pages/reports',
        },
        {
          'name': 'Classroom Course ',
          'navigationPath': '/pages/reports/reports_home',
        },
        {
          'name': 'Quiz Queston Response Report',
          'navigationPath': '/pages/reports/reports_home/classroom-reports/111',
        }]
    }
    if (this.menuId === 120) {
      this.header.breadCrumbList = [
        {
          'name': 'Reports',
          'navigationPath': '/pages/reports',
        },
        {
          'name': 'Classroom Course ',
          'navigationPath': '/pages/reports/reports_home',
        },
        {
          'name': 'Quiz Cumulative',
          'navigationPath': '/pages/reports/reports_home/classroom-reports/120',
        }]
    }
    if (this.menuId === 61) {
      this.header.breadCrumbList = [
        {
          'name': 'Reports',
          'navigationPath': '/pages/reports',
        },
        {
          'name': 'Workflow ',
          'navigationPath': '/pages/reports/reports_home',
        },
        {
          'name': 'Workflow Structure',
          'navigationPath': '/pages/reports/reports_home/workflow-reports/61',
        }]
    }
    if (this.menuId === 62) {
      this.header.breadCrumbList = [
        {
          'name': 'Reports',
          'navigationPath': '/pages/reports',
        },
        {
          'name': 'Workflow ',
          'navigationPath': '/pages/reports/reports_home',
        }
      ]
    }
    if (this.menuId === 79) {
      this.header.breadCrumbList = [
        {
          'name': 'Reports',
          'navigationPath': '/pages/reports',
        },
        {
          'name': 'Workflow ',
          'navigationPath': '/pages/reports/reports_home',
        },
        {
          'name': 'Workflow Consumption',
          'navigationPath': '/pages/reports/reports_home/workflow-reports/79',
        }]
    }
    if (this.menuId === 127) {
      this.header.breadCrumbList = [
        {
          'name': 'Reports',
          'navigationPath': '/pages/reports',
        },
        {
          'name': 'Reaction Report ',
          'navigationPath': '/pages/reports/reports_home',
        },
        {
          'name': 'Poll Consumption',
          'navigationPath': '/pages/reports/reports_home/poll-reports/127',
        }]
    }
    if (this.menuId === 126) {
      this.header.breadCrumbList = [
        {
          'name': 'Reports',
          'navigationPath': '/pages/reports',
        },
        {
          'name': 'Reaction Report ',
          'navigationPath': '/pages/reports/reports_home',
        },
        {
          'name': 'Survey Consumption',
          'navigationPath': '/pages/reports/reports_home/survey-reports/126',
        }]
    }
    if (this.menuId === 84 || this.menuId === 91) {
      this.header.breadCrumbList = [
        {
          'name': 'Reports',
          'navigationPath': '/pages/reports',
        },
        {
          'name': 'Train The Trainer',
          'navigationPath': '/pages/reports/reports_home',
        }]
    }
    if (this.menuId === 135) {
      this.header.breadCrumbList = [
        {
          'name': 'Reports',
          'navigationPath': '/pages/reports',
        },
        {
          'name': 'Reaction Report ',
          'navigationPath': '/pages/reports/reports_home',
        },
        {
          'name': 'EEP Nomination Details',
          'navigationPath': '/pages/reports/eep-reports/eep-nomination-report/135',
        }]
    }
   if (this.menuId === 92) {
    this.header.breadCrumbList = [
      {
        'name': 'Reports',
        'navigationPath': '/pages/reports',
      },
      {
        'name': 'Train The Trainer ',
        'navigationPath': '/pages/reports/reports_home',
      },
      {
        'name': 'Workflow Detail',
        'navigationPath': '/pages/reports/reports_home/ttt_reports/92',
      }]
  }
  if (this.menuId === 93) {
    this.header.breadCrumbList = [
      {
        'name': 'Reports',
        'navigationPath': '/pages/reports',
      },
      {
        'name': 'Train The Trainer ',
        'navigationPath': '/pages/reports/reports_home',
      },
      {
        'name': 'Preread Pages',
        'navigationPath': '/pages/reports/reports_home/ttt_reports/93',
      }]
  }
  if (this.menuId === 94) {
    this.header.breadCrumbList = [
      {
        'name': 'Reports',
        'navigationPath': '/pages/reports',
      },
      {
        'name': 'Train The Trainer ',
        'navigationPath': '/pages/reports/reports_home',
      },
      {
        'name': 'Nomination',
        'navigationPath': '/pages/reports/reports_home/ttt_reports/94',
      }]
  }
  if (this.menuId === 95) {
    this.header.breadCrumbList = [
      {
        'name': 'Reports',
        'navigationPath': '/pages/reports',
      },
      {
        'name': 'Train The Trainer ',
        'navigationPath': '/pages/reports/reports_home',
      },
      {
        'name': 'Bh Approval',
        'navigationPath': '/pages/reports/reports_home/ttt_reports/95',
      }]
  }
  if (this.menuId === 96) {
    this.header.breadCrumbList = [
      {
        'name': 'Reports',
        'navigationPath': '/pages/reports',
      },
      {
        'name': 'Train The Trainer ',
        'navigationPath': '/pages/reports/reports_home',
      },
      {
        'name': 'Batch Selection',
        'navigationPath': '/pages/reports/reports_home/ttt_reports/96',
      }]
  }
  if (this.menuId === 97) {
    this.header.breadCrumbList = [
      {
        'name': 'Reports',
        'navigationPath': '/pages/reports',
      },
      {
        'name': 'Train The Trainer ',
        'navigationPath': '/pages/reports/reports_home',
      },
      {
        'name': 'Certificate',
        'navigationPath': '/pages/reports/reports_home/ttt_reports/97',
      }
    ]
  }
  if (this.menuId === 98) {
    this.header.breadCrumbList = [
      {
        'name': 'Reports',
        'navigationPath': '/pages/reports',
      },
      {
        'name': 'Train The Trainer ',
        'navigationPath': '/pages/reports/reports_home',
      },
      {
        'name': 'Webinar',
        'navigationPath': '/pages/reports/reports_home/ttt_reports/98',
      }]
  }
  if (this.menuId === 100) {
    this.header.breadCrumbList = [
      {
        'name': 'Reports',
        'navigationPath': '/pages/reports',
      },
      {
        'name': 'Train The Trainer ',
        'navigationPath': '/pages/reports/reports_home',
      },
      {
        'name': 'On Evaluation Call',
        'navigationPath': '/pages/reports/reports_home/ttt_reports/100',
      }]
  }
  if (this.menuId === 101) {
    this.header.breadCrumbList = [
      {
        'name': 'Reports',
        'navigationPath': '/pages/reports',
      },
      {
        'name': 'Train The Trainer ',
        'navigationPath': '/pages/reports/reports_home',
      },
      {
        'name': 'Viva',
        'navigationPath': '/pages/reports/reports_home/ttt_reports/101',
      }
    ]
  }
  if (this.menuId === 102) {
    this.header.breadCrumbList = [
      {
        'name': 'Reports',
        'navigationPath': '/pages/reports',
      },
      {
        'name': 'Train The Trainer ',
        'navigationPath': '/pages/reports/reports_home',
      },
      {
        'name': 'Mock',
        'navigationPath': '/pages/reports/reports_home/ttt_reports/102',
      }]
  }
  if (this.menuId === 103) {
    this.header.breadCrumbList = [
      {
        'name': 'Reports',
        'navigationPath': '/pages/reports',
      },
      {
        'name': 'Train The Trainer ',
        'navigationPath': '/pages/reports/reports_home',
      },
      {
        'name': 'Final Assessment',
        'navigationPath': '/pages/reports/reports_home/ttt_reports/103',
      }]
  }
  if (this.menuId === 104) {
    this.header.breadCrumbList = [
      {
        'name': 'Reports',
        'navigationPath': '/pages/reports',
      },
      {
        'name': 'Train The Trainer ',
        'navigationPath': '/pages/reports/reports_home',
      },
      {
        'name': 'FG Assessment',
        'navigationPath': '/pages/reports/reports_home/ttt_reports/104',
      }]
  }
  if(this.menuId==159){
    this.header.breadCrumbList = [
      {
        'name': 'Reports',
        'navigationPath': '/pages/reports',
      },
      {
        'name': 'DAM Report',
        'navigationPath': '/pages/reports/reports_home',
      },
      {
        'name': 'Asset Mapping Report',
        'navigationPath': '/pages/reports/reports_home/dam-reports/159',
      }]
  }
  if(this.menuId==161){
    this.header.breadCrumbList = [
      {
        'name': 'Reports',
        'navigationPath': '/pages/reports',
      },
      {
        'name': 'DAM Report',
        'navigationPath': '/pages/reports/reports_home',
      },
      {
        'name': 'Asset Version Mapping Report',
        'navigationPath': '/pages/reports/reports_home/dam-reports/159',
      }]
  }

  }
  SetReport() {
    if (this.menuId === 62) {
      this.openPopUp = false;
    }
    this.header = {
      title: this.ReportName,
      btnsSearch: true,
      searchBar: false,
      catDropdown: true,
      searchtext: '',
      dropdownlabel: '',
      drplabelshow: false,
      optionValue: 'Select Report List',
      dropId: 'report_id',
      // id:'report_id',
      dropName: 'report_name',
      drpName1: '',
      drpName2: ' ',
      drpName3: '',
      drpName1show: false,
      drpName2show: false,
      drpName3show: false,
      btnName1: '',
      btnName2: 'Schedule',
      btnName3: '',
      btnAdd: 'Preview',
      btnName1show: false,
      //btnName2show: true,
      btnName3show: false,
      btnBackshow: true,
      btnAddshow: false,
      filter: false,
      showBreadcrumb: true,
    };
    const report = this.reportService.GET_Report_Filter_Values();
    console.log(report);
    this.FMReportList = report.FMReportList;
    this.ReportName = report.FMReportName;
    if (report.ReportFilter) {
      this.ReportFilters = this.populateFMFilter(report.ReportFilter);
    }
    if (this.FMReportList.length > 0) {
      let selectedreport = this.FMReportList[0];
      this.SelectedReport = String(selectedreport.report_id);
      this.header['id'] = this.SelectedReport
      console.log(this.header['id'])
      if (selectedreport.metadata) {
        selectedreport = JSON.parse(selectedreport.metadata);
      }
      selectedreport.dataSource.data = report.FMReport.data;
      if (selectedreport.slice && this.ReportFilters) {
        selectedreport.slice.reportFilters = this.ReportFilters;
      }
      this.FMReport = {
        data: report.FMReport.data,
        report: selectedreport,
        toolbar: report.FMReport.toolbar,
      };
    } else {
      Report.dataSource.data = report.FMReport.data;
      if (Report.slice && this.ReportFilters) {
        Report.slice['reportFilters'] = this.ReportFilters;
      }
      this.FMReport = {
        data: report.FMReport.data,
        report: Report,
        toolbar: report.FMReport.toolbar,
      };
    }
    this.FMReadyForWorking = true;
  }

  filter_data(reportList) {
    console.log(reportList);
    this.selectedReportFromList = reportList;
  }
  back() {
    if (this.reportService.getDirectRedirect()) {
      this.reportService.setDirectRedirect(false);
      window.history.go(-2);
    } else {
      window.history.back();
    }
  }

  handleChange(evt: any) {
    let target = evt.event.target;
    var id = evt.id
    this.FMReadyForWorking = false;
    if (String(id)) {
      // const report = JSON.parse(value);
      let report = null;
      this.FMReportList.forEach(element => {
        if (id === String(element.report_id)) {
          report = element;
        }
      });
      if (report) {
        // this.SelectedReport = report;
        const selectedreport = JSON.parse(report.metadata);
        if (selectedreport.slice && this.ReportFilters) {
          selectedreport.slice['reportFilters'] = this.ReportFilters;
        }
        this.FMReport.report = selectedreport;
      }
    }
    setTimeout(() => {
      this.FMReadyForWorking = true;
      this.cdf.detectChanges();
    }, 500);
  }
  convertToJsonString(value) {
    if (value) {
      return JSON.stringify(value);
    } else {
      return value;
    }
  }
  /**
   *
   * @param event
   * @metod FMReport component pass data to page
   */
  GetReport(event) {
    console.log('Report : ', event);
    this.TempFMReport = event;
    this.OpenModal();
  }

  /**
   * @description Insert and Update methods
   *
   */
  SaveReport(enteredReport, form_data: NgForm) {
    this.submitted = true;

    if (form_data.valid) {

      this.spinner.show();
      let param = {};
      if (String(this.selected_Tab).toLocaleLowerCase() === 'add new') {

        this.Report_Name_Temp = enteredReport; // pass name of report entered in input field

        param = {
          report_name: this.Report_Name_Temp,
          metadata: JSON.stringify(this.TempFMReport),
          menuId: this.menuId,
        };
        this.reportService.Insert_Report(param).then((result: any) => {
          if (result.type === false) {
            console.log('Report Insert Err : -', result);
            this.CloseModal();
          } else {
            this.GetReportList((result) => {
              this.FMReportList = result;
              this.CloseModal();
            })
          }
        });
        this.spinner.hide();

      } else if (String(this.selected_Tab).toLocaleLowerCase() === 'update') {
        if (this.Selected_Report_Temp == 0) {
          this.spinner.hide();
          // this.CloseModal();
          return form_data.form.controls['reportList'].setErrors({ 'pristine': true });
          // return form_data.invalid;
        }
        this.Selected_Report_Temp = enteredReport;  // pass selected report value

        let report = null;
        if (String(this.Selected_Report_Temp)) {
          this.FMReportList.forEach(element => {
            if (this.Selected_Report_Temp === String(element.report_id)) {
              report = element;
            }
          });
        }
        param = {
          report_id: report.report_id,
          report_name: report.report_name,
          metadata: JSON.stringify(this.TempFMReport),
          menuId: this.menuId,
          isActive: 1,
        };
        this.reportService.Update_Report(param).then((result: any) => {

          if (result.type === false) {

            // tslint:disable-next-line: no-console
            console.log('Report Update Err : -', result);
          } else {
            this.GetReportList((result) => {
              this.FMReportList = result;
              this.CloseModal();
            })
          }
          // this.CloseModal();
        });
        this.spinner.hide();

      }

    }
  }

  /**
   * @description Common methods
   */
  selectedTab(event, type) {
    console.log('event', event);
    console.log('type', type);
    this.selected_Tab = event.tabTitle;

    // console.log(reportname.value);
    // console.log(reportSelection.value);

    // if(String(this.selected_Tab).toLocaleLowerCase() === 'add new'){
    //   if(reportSelection.dirty == false  && reportname.dirty == false){
    //     this.disableUpdateTab = true;
    //     this.hideReportInput = true;
    //     this.hideSelectReport = false;
    //   }
    // }

    // else if(String(this.selected_Tab).toLocaleLowerCase() === 'update'){
    //   if( reportSelection.dirty == true){
    //     this.disableRepoName = true;
    //     this.hideSelectReport = true;
    //     this.hideReportInput = false;
    //   }
    // }
  }

  disableSelectBox(event: Event, is_dirty) {
    console.log(event);
    let Invalue = (<HTMLInputElement>event.target).value;
    if (Invalue != '' && is_dirty == true) {
      this.hideSelectReport = true;
    }
    else {
      this.hideSelectReport = false;
    }
  }

  disableInput(event: Event, is_dirty) {
    console.log(event);
    let Invalue = (<HTMLInputElement>event.target).value;
    if (Invalue != '' && is_dirty == true) {
      this.hideReportInput = true;
    }
    else {
      this.hideReportInput = false;
    }
  }

  OpenModal() {
    console.log(this.selectedReportFromList);
    this.openPopUpModal = true;
  }
  CloseModal() {
    this.openPopUpModal = false;
  }
  Close() {
    this.openPopUp = false;
  }
  Save() {
    this.openPopUpModal = false;
  }

  Exportreport() {
    this.exports = true;
    console.log(this.exports, "exports")
    this.openPopUp = false

  }

  populateFMFilter(filters) {
    const reportFilters = [];
    try {
      if (filters) {
        for (const filter in filters) {
          const reportFilter = {
            'uniqueName': 'tags',
            'filter': {
              'members': ['tags.[dc3]', 'tags.[demo3]'],
            },
            'sort': 'unsorted'
          };
          if (filter && filters[filter].length > 0) {
            const array = filters[filter];
            const uniqueName = filter; // String(filter).toLowerCase();
            reportFilter['uniqueName'] = uniqueName;
            const memberslist = [];
            array.forEach(element => {
              const member = uniqueName + '.' + '[' + element + ']';
              memberslist.push(member);
            });
            reportFilter['filter']['members'] = memberslist;
            reportFilters.push(reportFilter);
          }
        }
      }
      return reportFilters;
    } catch (e) {
      return reportFilters;
    }
  }

  GetReportList(cb) {
    const param = {
      menuId: this.menuId || 0,
    };
    this.reportService.GetReportList(param).then((res: any) => {
      if (res.type) {
        cb(res.data);
      } else {
        cb([]);
      }
    }, err => {
      console.log(err);
      cb([]);
    });
  }

  closedmodel(data) {
    console.log(data);
    this.exports = false;
    if (data.res == 2) {
      this.router.navigate(['pages/reports/ReportList']);
      // this.FMReportFilterValues = [];
    }

  }

  sendSaveBtn(val) {
    this.isdata = val;
    setTimeout(() => {
      this.isdata = '';
    }, 2000);
  }

  closeSiderBar() {
    this.exports = false;
  }

}
