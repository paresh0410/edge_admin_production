import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { ReportsService } from "../reports.service";
import { Report } from "../report";
import { ActivatedRoute, Router } from "@angular/router";
import {
  FMToolbar,
  FMReportEntity,
} from "../../../component/fm-report/fm-report";
import { NgxSpinnerService } from "ngx-spinner";
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { NgForm } from "@angular/forms";
// import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { ToastrService } from "ngx-toastr";
import { ExcelService } from "../../../service/excel-service";
// import { concat } from 'rxjs';
import { SuubHeader } from "../../components/models/subheader.model";
import { CommonFunctionsService } from "../../../service/common-functions.service";
import { webApi } from "../../../service/webApi";
@Component({
  selector: "ngx-pre-onboarding-report",
  templateUrl: "./pre-onboarding-report.component.html",
  styleUrls: ["./pre-onboarding-report.component.scss"],
})
export class PreOnboardingReportComponent implements OnInit {
  FMReport: any = [];
  FMReportList: any = [];
  header: SuubHeader;
  SelectedReport: any;
  FMReadyForWorking: boolean = false;
  ReportName: string;
  menuId: number;
  TempFMReport: any;
  selectedReportFromList: any;
  openPopUpModal: boolean = false;
  addReport: boolean = true;
  updateReport: boolean = false;
  selected_Tab: any;
  disableModalButton: boolean = false;
  openPopUpEmployeeDetailType: boolean = false;
  openPopup: boolean = false;
  // reportname: NgForm;
  // reportSelection: NgForm;
  ReportInfo: FMReportEntity = {
    report_name: null,
    report_id: null,
    metadata: null,
    userid: 0,
    report_map_id: null,
    menuId: null,
  };
  Report_Name_Temp: string = null;
  Selected_Report_Temp: any = 0;
  submitted: boolean = false;
  disableUpdateTab: boolean = false;
  disableRepoName: boolean;
  hideSelectReport: boolean;
  hideReportInput: boolean;
  ReportFilters: any;
  exports: boolean;
  anyOneSelected = true;
  toolbar: FMToolbar = {
    connect: false,
    open: false,
    save: false,
    export: true,
    grid: true,
    charts: true,
    format: true,
    options: true,
    fields: true,
    fullscreen: true,
    report: true,
  };
  FMReportFilterValues: any = {
    Ids: [],
    Filters: {},
    courseName: [],
    courseTypeId: "",
    menuId: "",
    AcitivityIds: [],
    filterId: "",
  };
  emplyeedetailType: any = [];
  courseType: any;
  nodata: boolean = false;

  notiTitle: string = "Report Schedule";
  btnName: string = "Save";
  btnName1: string = "Save & Show";
  isdata: any;

  notiTitleEmp: string = "Select Report";
  btnName1Emp: string = "Schedule";

  constructor(
    private reportService: ReportsService,
    private toastr: ToastrService,
    private router: Router,
    private commonFunctionsService: CommonFunctionsService,
    // private toasterService: ToasterService,
    private spinner: NgxSpinnerService,
    private excelservice: ExcelService,
    public cdf: ChangeDetectorRef,
    private routes: ActivatedRoute
  ) {
    this.menuId =
      this.routes.snapshot.params["menuId"] == undefined
        ? 0
        : Number(this.routes.snapshot.params["menuId"]);
  }

  ngOnInit() {
    this.ReportName = this.reportService.ReportTitle;
    this.FMReportFilterValues.menuId = this.menuId;
    // this.SetReport();
    this.header = {
      title: this.ReportName,
      btnsSearch: true,
      searchBar: false,
      searchtext: "",
      dropdownlabel: " ",
      drplabelshow: false,
      drpName1: "",
      drpName2: " ",
      drpName3: "",
      drpName1show: false,
      drpName2show: false,
      drpName3show: false,
      btnName1: "",
      btnName2: "Schedule",
      btnName3: "",
      btnAdd: "Preview",
      btnName1show: false,
      //btnName2show: true,
      btnName3show: false,
      btnBackshow: true,
      btnAddshow: false,
      filter: false,
      showBreadcrumb: true,
      breadCrumbList: [],
    };

    this.GetReports();
    if (this.menuId == 116 || this.menuId == 119 || this.menuId == 149) {
      this.header.breadCrumbList = [
        {
          name: "Reports",
          navigationPath: "/pages/reports",
        },
        {
          name: "Online Course",
          navigationPath: "/pages/reports/reports_home",
        },
      ];
    } else if (
      this.menuId == 117 ||
      this.menuId == 131 ||
      this.menuId == 132 ||
      this.menuId == 150
    ) {
      this.header.breadCrumbList = [
        {
          name: "Reports",
          navigationPath: "/pages/reports",
        },
        {
          name: "Classroom Course ",
          navigationPath: "/pages/reports/reports_home",
        },
      ];
    } else if (this.menuId == 106 || this.menuId == 107) {
      this.header.breadCrumbList = [
        {
          name: "Reports ",
          navigationPath: "/pages/reports/",
        },
      ];
    } else if (this.menuId == 153 || this.menuId == 154) {
      this.header.breadCrumbList = [
        {
          name: "Reports ",
          navigationPath: "/pages/reports/",
        },
        {
          name: "DAM Reports ",
          navigationPath: "pages/reports/reports_home",
        },
      ];
    }
  }
  showOnlySchedulePopup = false;
  GetReports() {
    this.spinner.show();
    if (this.menuId === 106) {
      this.openPopup = false;
      this.header["btnName2show"] = true;
      this.GetpreonboardingReportTimeOut();
    }
    // if (this.menuId === 129) {
    //   this.openPopup = false
    //   this.header['btnName2show'] = true;
    //   this.GetOnlineConsolidatedReport();
    // }
    // if (this.menuId === 130) {
    //   this.openPopup = false;
    //   this.showOnlySchedulePopup = true;
    //   // this.header['btnName2show'] = true;
    //   // this.GetConsolidatedWorkflowReport();
    // }
    // if (this.menuId === 132) {
    //   this.openPopup = false;
    //   this.showOnlySchedulePopup = true;
    //   // this.header['btnName2show'] = true;
    //   // this.GetConsolidatedAttendanceReport();
    // }
    // if (this.menuId === 131) {
    //   this.openPopup = false;
    //   this.showOnlySchedulePopup = true;
    //   // this.header['btnName2show'] = true;
    //   // this.GetConsolidatedFeedbackReport();
    // }
    if (
      this.menuId === 129 ||
      this.menuId === 130 ||
      this.menuId === 131 ||
      this.menuId === 132 ||
      this.menuId === 141 ||
      this.menuId === 142 ||
      this.menuId === 143 ||
      this.menuId === 144 ||
      this.menuId === 145
    ) {
      this.openPopup = false;
      this.showOnlySchedulePopup = true;
      if (this.menuId == 141) {
        this.FMReportFilterValues.courseTypeId = 1;
      }
      if (this.menuId == 142) {
        this.FMReportFilterValues.courseTypeId = 4;
      }
      if (this.menuId == 143) {
        this.FMReportFilterValues.courseTypeId = 4;
      }
    }
    if (this.menuId === 107) {
      this.getLoginReportsList();
      // this.getLoginReportsListTimeOut();
      // this.getemplyeedetail();
    }
    if (this.menuId === 116) {
      this.openPopup = false;
      this.header["btnName2show"] = true;
      this.ReportName = "Quiz Summary";
      this.header["title"] = this.ReportName;
      this.FMReportFilterValues.courseTypeId = 1;
      this.courseType = 1;
      // this.GetQuizSummaryReport();
      this.GetQuizSummaryReportTimeOut();
    }
    if (this.menuId === 117) {
      this.openPopup = false;
      this.ReportName = "Quiz Summary";
      this.header["title"] = this.ReportName;
      this.header["btnName2show"] = true;
      this.courseType = 4;
      this.FMReportFilterValues.courseTypeId = 4;
      // this.GetQuizSummaryReport();
      this.GetQuizSummaryReportTimeOut();
    }
    if (this.menuId === 149 || this.menuId === 150) {
      this.openPopup = false;
      this.header["btnName2show"] = true;
      this.ReportName = "Like Count";
      this.header["title"] = this.ReportName;
      this.courseType = this.menuId === 149 ? 1 : 4;
      this.FMReportFilterValues.courseTypeId = this.courseType;
      // this.getLikeCountReport();
      this.getLikeCountReportTimeOut();
    }
    if (this.menuId === 153) {
      this.openPopup = false;
      this.header["btnName2show"] = true;
      this.ReportName = "File Details";
      this.header["title"] = this.ReportName;
      this.courseType = "";
      this.FMReportFilterValues.courseTypeId = this.courseType;
      this.getDAMFileDetailsReportTimeOut();
    }
    if (this.menuId === 154) {
      this.openPopup = false;
      this.header["btnName2show"] = true;
      this.ReportName = "File Sharing Details";
      this.header["title"] = this.ReportName;
      this.courseType = "";
      this.FMReportFilterValues.courseTypeId = this.courseType;
      // this.getDAMFileShareReport();
      this.getDAMFileShareReportTimeOut();
    }
    //  else {
    //   this.spinner.hide();
    // }
  }
  // GetpreonboardingReport() {
  //   this.reportService.getonboardingreport({}).then(res => {
  //     console.log(res);
  //     if (res['type'] === true) {
  //       this.FMReport = {
  //         data: res['data'],
  //         report: Report,
  //         toolbar: this.toolbar,
  //       }
  //       this.FMReadyForWorking = true;
  //       console.log(this.FMReport);
  //       this.cdf.detectChanges();
  //     }
  //     this.spinner.hide();
  //   }, err => {
  //     this.spinner.hide();
  //     console.log(err);
  //   });
  // }
  GetpreonboardingReportTimeOut() {
    // const url = `${this.config.FINAL_URL}` + this.url_get_report_preonboarding_report;
    // this.reportService.getonboardingreport({}).then(res => {
    //   console.log(res);
    //   if (res['type'] === true) {
    //     this.FMReport = {
    //       data: res['data'],
    //       report: Report,
    //       toolbar: this.toolbar,
    //     }
    //     this.FMReadyForWorking = true;
    //     console.log(this.FMReport);
    //     this.cdf.detectChanges();
    //   }
    //   this.spinner.hide();
    // }, err => {
    //   this.spinner.hide();
    //   console.log(err);
    // });
    this.getReportsByAPI({}, (result) => {
      if (!Array.isArray(result)) {
        this.spinner.hide();
        this.cdf.detectChanges();
        // type: 'TimeoutError',
        // const message = "Report taking too much time, can't be Previewed. Please Schedule the report";
        this.toastr.warning(result["message"], "Warning");
        if (result["type"] === "TimeoutError") {
          setTimeout(() => {
            this.Exportreport();
          });
        }
      } else if (result.length !== 0) {
        this.FMReport = {
          data: result,
          report: Report,
          toolbar: this.toolbar,
        };
        this.FMReadyForWorking = true;
        console.log(this.FMReport);
        this.spinner.hide();
        this.cdf.detectChanges();
      } else if (result.length === 0) {
        this.spinner.hide();
        // this.showSpinner = false;
        this.cdf.detectChanges();
        this.presentToast("warning", "No data available");
      }
    });
  }

  getReportsByAPI(value, cb) {
    let url = "";
    if (this.menuId === 106) {
      url = webApi.domain + webApi.url.report_preonboarding_report;
    } else if (this.menuId === 116 || this.menuId === 117) {
      url = webApi.domain + webApi.url.report_quiz_summary_report;
    } else if (this.menuId === 149 || this.menuId === 150) {
      url = webApi.domain + webApi.url.report_like_dislike;
    } else if (this.menuId === 153) {
      url = webApi.domain + webApi.url.report_get_dam_file_details;
    } else if (this.menuId === 154) {
      url = webApi.domain + webApi.url.report_get_dam_file_share_details;
    } else if (this.menuId === 107) {
      url = webApi.domain + webApi.url.report_emplyee_consumption;
    } else {
      cb([]);
    }
    this.commonFunctionsService.reportPostRequestTimeOut(url, value).subscribe(
      (res) => {
        console.log("res", res);
        cb(res.data);
      },
      (error) => {
        console.log("ERROR", error);
        // if(error === 'Timeout Exception'){

        // }
        cb(error);
      }
    );
  }

  GetOnlineConsolidatedReport() {
    this.reportService.GetOnlineConsolidatedReport({}).then(
      (res) => {
        console.log(res);
        if (res["type"] === true) {
          this.FMReport = {
            data: res["data"],
            report: Report,
            toolbar: this.toolbar,
          };
          this.FMReadyForWorking = true;
          console.log(this.FMReport);
          this.cdf.detectChanges();
        }
        this.spinner.hide();
      },
      (err) => {
        this.spinner.hide();
        console.log(err);
      }
    );
  }

  GetConsolidatedWorkflowReport() {
    this.reportService.GetConsolidatedWorkflowReport({}).then(
      (res) => {
        console.log(res);
        if (res["type"] === true) {
          this.FMReport = {
            data: res["data"],
            report: Report,
            toolbar: this.toolbar,
          };
          this.FMReadyForWorking = true;
          console.log(this.FMReport);
          this.cdf.detectChanges();
        }
        this.spinner.hide();
      },
      (err) => {
        this.spinner.hide();
        console.log(err);
      }
    );
  }

  GetConsolidatedAttendanceReport() {
    this.reportService.GetConsolidatedAttendanceReport({}).then(
      (res) => {
        console.log(res);
        if (res["type"] === true) {
          this.FMReport = {
            data: res["data"],
            report: Report,
            toolbar: this.toolbar,
          };
          this.FMReadyForWorking = true;
          console.log(this.FMReport);
          this.cdf.detectChanges();
        }
        this.spinner.hide();
      },
      (err) => {
        this.spinner.hide();
        console.log(err);
      }
    );
  }

  GetConsolidatedFeedbackReport() {
    this.reportService.GetConsolidatedFeedbackReport({}).then(
      (res) => {
        console.log(res);
        if (res["type"] === true) {
          this.FMReport = {
            data: res["data"],
            report: Report,
            toolbar: this.toolbar,
          };
          this.FMReadyForWorking = true;
          console.log(this.FMReport);
          this.cdf.detectChanges();
        }
        this.spinner.hide();
      },
      (err) => {
        this.spinner.hide();
        console.log(err);
      }
    );
  }

  getemplyeedetail(data) {
    this.spinner.show();
    // this.reportService
    //   .emplyeedetailreport({
    //     formatIds: data,
    //   })
    //   .then(
    //     (res) => {
    //       console.log(res);
    //       this.closesectioType();
    //       if (res["type"] === true) {
    //         this.FMReport = {
    //           data: res["data"],
    //           report: Report,
    //           toolbar: this.toolbar,
    //         };
    //         this.FMReadyForWorking = true;
    //         console.log(this.FMReport);
    //         this.cdf.detectChanges();
    //       }
    //       this.spinner.hide();
    //     },
    //     (err) => {
    //       this.spinner.hide();
    //       console.log(err);
    //     }
    //   );

   const params = {
       formatIds: data,
    }
    this.getReportsByAPI(params, (result) => {
      if (!Array.isArray(result)) {
        this.spinner.hide();
        this.cdf.detectChanges();
        // type: 'TimeoutError',
        // const message = "Report taking too much time, can't be Previewed. Please Schedule the report";
        this.toastr.warning(result["message"], "Warning");
        if (result["type"] === "TimeoutError") {
          setTimeout(() => {
            this.closesectioType();
            this.Exportreport();
          });
        }
      } else if (result.length !== 0) {
        this.closesectioType();
        this.FMReport = {
          data: result,
          report: Report,
          toolbar: this.toolbar,
        };
        this.FMReadyForWorking = true;
        console.log(this.FMReport);
        this.spinner.hide();
        this.cdf.detectChanges();
      } else if (result.length === 0) {
        this.closesectioType();
        this.spinner.hide();
        // this.showSpinner = false;
        this.cdf.detectChanges();
        this.presentToast("warning", "No data available");
      }
    });
  }

  GetQuizSummaryReport() {
    let param = {
      courseTypeId: this.courseType,
    };
    this.reportService.getQuizSummaryReport(param).then(
      (res) => {
        console.log(res);
        if (res["type"] === true) {
          this.FMReport = {
            data: res["data"],
            report: Report,
            toolbar: this.toolbar,
          };
          this.FMReadyForWorking = true;
          console.log(this.FMReport);
          this.cdf.detectChanges();
        }
        this.spinner.hide();
      },
      (err) => {
        this.spinner.hide();
        console.log(err);
      }
    );
  }

  GetQuizSummaryReportTimeOut() {
    const param = {
      courseTypeId: this.courseType,
    };
    this.getReportsByAPI(param, (result) => {
      if (!Array.isArray(result)) {
        this.spinner.hide();
        this.cdf.detectChanges();
        // type: 'TimeoutError',
        // const message = "Report taking too much time, can't be Previewed. Please Schedule the report";
        this.toastr.warning(result["message"], "Warning");
        if (result["type"] === "TimeoutError") {
          setTimeout(() => {
            this.Exportreport();
          });
        }
      } else if (result.length !== 0) {
        // if (res['type'] === true) {
        this.FMReport = {
          data: result,
          report: Report,
          toolbar: this.toolbar,
        };
        this.FMReadyForWorking = true;
        console.log(this.FMReport);
        this.cdf.detectChanges();
        // }
        this.spinner.hide();
      } else if (result.length === 0) {
        this.spinner.hide();
        // this.showSpinner = false;
        this.cdf.detectChanges();
        this.presentToast("warning", "No data available");
      }
    });
  }
  getLikeCountReportTimeOut() {
    const param = {
      courseTypeId: this.courseType,
    };
    this.getReportsByAPI(param, (result) => {
      if (!Array.isArray(result)) {
        this.spinner.hide();
        this.cdf.detectChanges();
        // type: 'TimeoutError',
        // const message = "Report taking too much time, can't be Previewed. Please Schedule the report";
        this.toastr.warning(result["message"], "Warning");
        if (result["type"] === "TimeoutError") {
          setTimeout(() => {
            this.Exportreport();
          });
        }
      } else if (result.length !== 0) {
        // if (res['type'] === true) {
        this.FMReport = {
          data: result,
          report: Report,
          toolbar: this.toolbar,
        };
        this.FMReadyForWorking = true;
        console.log(this.FMReport);
        this.cdf.detectChanges();
        // }
        this.spinner.hide();
      } else if (result.length === 0) {
        this.spinner.hide();
        // this.showSpinner = false;
        this.cdf.detectChanges();
        this.presentToast("warning", "No data available");
      }
    });
  }

  getLikeCountReport() {
    const param = {
      courseTypeId: this.courseType,
    };
    const url: any = webApi.domain + webApi.url.report_like_dislike;
    this.commonFunctionsService.httpPostRequest(url, param).then(
      (res) => {
        console.log(res);
        if (res["type"] === true) {
          this.FMReport = {
            data: res["data"],
            report: Report,
            toolbar: this.toolbar,
          };
          this.FMReadyForWorking = true;
          console.log(this.FMReport);
          this.cdf.detectChanges();
        }
        this.spinner.hide();
      },
      (err) => {
        this.spinner.hide();
        console.log(err);
      }
    );
  }
  // getLikeCountReport(){
  //   const param = {
  //     courseTypeId: this.courseType,
  //   };
  //   const url: any = webApi.domain + webApi.url.report_like_dislike;

  //   this.commonFunctionsService.reportPostRequest(url,param);
  //   this.commonFunctionsService.reportPostRequest2(url,param);
  // }

  dowmload() {
    this.spinner.show();
    this.reportService.emplyeedetailreport_download({}).then(
      (res) => {
        console.log(res);
        if (res["data"].length !== 0) {
          this.excelservice.exportAsExcelFile(res["data"], this.ReportName);
          this.spinner.hide();
        } else if (res["data"].length === 0) {
          this.presentToast("warning", "No data available");
          this.spinner.hide();
        }
        this.spinner.hide();
      },
      (err) => {
        this.spinner.hide();
        console.log(err);
      }
    );
  }

  /* still not in used */

  SetReport() {
    // const report = this.reportService.GET_Report_Filter_Values();
    this.header["optionValue"] = "Select Report List";
    this.header["dropId"] = "report_id";
    this.header["catDropdown"] = true;
    this.header["dropName"] = "report_name";
    const report = {
      FMReport: this.FMReport,
      FMReportList: this.FMReportList,
      FMReportName: this.ReportName,
      ReportFilter: [],
    };
    // const report = this.reportService.GET_Report_Filter_Values();
    this.FMReportList = report.FMReportList;
    // this.ReportName = report.FMReportName;

    if (report.ReportFilter) {
      this.ReportFilters = this.populateFMFilter(report.ReportFilter);
    }
    if (this.FMReportList.length > 0) {
      let selectedreport = this.FMReportList[0];
      this.SelectedReport = String(selectedreport.report_id);
      if (selectedreport.metadata) {
        selectedreport = JSON.parse(selectedreport.metadata);
      }
      selectedreport.dataSource.data = report.FMReport.data;
      if (selectedreport.slice && this.ReportFilters) {
        selectedreport.slice.reportFilters = this.ReportFilters;
      }
      this.FMReport = {
        data: report.FMReport.data,
        report: selectedreport,
        toolbar: report.FMReport.toolbar,
      };
    } else {
      Report.dataSource.data = report.FMReport.data;
      if (Report.slice && this.ReportFilters) {
        Report.slice["reportFilters"] = this.ReportFilters;
      }
      this.FMReport = {
        data: report.FMReport.data,
        report: Report,
        toolbar: report.FMReport.toolbar,
      };
    }
    this.FMReadyForWorking = true;
  }

  filter_data(reportList) {
    console.log(reportList);
    this.selectedReportFromList = reportList;
  }
  back() {
    if (this.reportService.getDirectRedirect()) {
      this.reportService.setDirectRedirect(false);
      window.history.go(-2);
    } else {
      window.history.back();
    }
  }

  handleChange(evt: any) {
    let target = evt.event.target;
    var id = evt.id;
    this.FMReadyForWorking = false;
    if (String(id)) {
      // const report = JSON.parse(value);
      let report = null;
      this.FMReportList.forEach((element) => {
        if (id === String(element.report_id)) {
          report = element;
        }
      });
      if (report) {
        // this.SelectedReport = report;
        const selectedreport = JSON.parse(report.metadata);
        if (selectedreport.slice && this.ReportFilters) {
          selectedreport.slice["reportFilters"] = this.ReportFilters;
        }
        this.FMReport.report = selectedreport;
      }
    }
    setTimeout(() => {
      this.FMReadyForWorking = true;
      this.cdf.detectChanges();
    }, 500);
  }
  convertToJsonString(value) {
    if (value) {
      return JSON.stringify(value);
    } else {
      return value;
    }
  }
  /**
   *
   * @param event
   * @metod FMReport component pass data to page
   */
  GetReport(event) {
    console.log("Report : ", event);
    this.TempFMReport = event;
    this.OpenModal();
  }

  /**
   * @description Insert and Update methods
   *
   */
  SaveReport(enteredReport, form_data: NgForm) {
    this.submitted = true;

    if (form_data.valid) {
      this.spinner.show();
      let param = {};
      if (String(this.selected_Tab).toLocaleLowerCase() === "add new") {
        this.Report_Name_Temp = enteredReport; // pass name of report entered in input field

        param = {
          report_name: this.Report_Name_Temp,
          metadata: JSON.stringify(this.TempFMReport),
          menuId: this.menuId,
        };
        this.reportService.Insert_Report(param).then((result: any) => {
          if (result.type === false) {
            console.log("Report Insert Err : -", result);
            this.CloseModal();
          } else {
            this.GetReportList((result) => {
              this.FMReportList = result;
              this.CloseModal();
            });
          }
        });
        this.spinner.hide();
      } else if (String(this.selected_Tab).toLocaleLowerCase() === "update") {
        if (this.Selected_Report_Temp == 0) {
          this.spinner.hide();
          // this.CloseModal();
          return form_data.form.controls["reportList"].setErrors({
            pristine: true,
          });
          // return form_data.invalid;
        }
        this.Selected_Report_Temp = enteredReport; // pass selected report value

        let report = null;
        if (String(this.Selected_Report_Temp)) {
          this.FMReportList.forEach((element) => {
            if (this.Selected_Report_Temp === String(element.report_id)) {
              report = element;
            }
          });
        }
        param = {
          report_id: report.report_id,
          report_name: report.report_name,
          metadata: JSON.stringify(this.TempFMReport),
          menuId: this.menuId,
          isActive: 1,
        };
        this.reportService.Update_Report(param).then((result: any) => {
          if (result.type === false) {
            // tslint:disable-next-line: no-console
            console.log("Report Update Err : -", result);
          } else {
            this.GetReportList((result) => {
              this.FMReportList = result;
              this.CloseModal();
            });
          }
          // this.CloseModal();
        });
        this.spinner.hide();
      }
    }
  }

  /**
   * @description Common methods
   */
  selectedTab(event, type) {
    console.log("event", event);
    console.log("type", type);
    this.selected_Tab = event.tabTitle;

    // console.log(reportname.value);
    // console.log(reportSelection.value);

    // if(String(this.selected_Tab).toLocaleLowerCase() === 'add new'){
    //   if(reportSelection.dirty == false  && reportname.dirty == false){
    //     this.disableUpdateTab = true;
    //     this.hideReportInput = true;
    //     this.hideSelectReport = false;
    //   }
    // }

    // else if(String(this.selected_Tab).toLocaleLowerCase() === 'update'){
    //   if( reportSelection.dirty == true){
    //     this.disableRepoName = true;
    //     this.hideSelectReport = true;
    //     this.hideReportInput = false;
    //   }
    // }
  }

  disableSelectBox(event: Event, is_dirty) {
    console.log(event);
    let Invalue = (<HTMLInputElement>event.target).value;
    if (Invalue != "" && is_dirty == true) {
      this.hideSelectReport = true;
    } else {
      this.hideSelectReport = false;
    }
  }

  disableInput(event: Event, is_dirty) {
    console.log(event);
    let Invalue = (<HTMLInputElement>event.target).value;
    if (Invalue != "" && is_dirty == true) {
      this.hideReportInput = true;
    } else {
      this.hideReportInput = false;
    }
  }

  OpenModal() {
    console.log(this.selectedReportFromList);
    this.openPopUpModal = true;
  }
  CloseModal() {
    this.openPopUpModal = false;
  }
  Save() {
    this.openPopUpModal = false;
  }

  populateFMFilter(filters) {
    const reportFilters = [];
    try {
      if (filters) {
        for (const filter in filters) {
          const reportFilter = {
            uniqueName: "tags",
            filter: {
              members: ["tags.[dc3]", "tags.[demo3]"],
            },
            sort: "unsorted",
          };
          if (filter && filters[filter].length > 0) {
            const array = filters[filter];
            const uniqueName = filter; // String(filter).toLowerCase();
            reportFilter["uniqueName"] = uniqueName;
            const memberslist = [];
            array.forEach((element) => {
              const member = uniqueName + "." + "[" + element + "]";
              memberslist.push(member);
            });
            reportFilter["filter"]["members"] = memberslist;
            reportFilters.push(reportFilter);
          }
        }
      }
      return reportFilters;
    } catch (e) {
      return reportFilters;
    }
  }

  GetReportList(cb) {
    const param = {
      menuId: this.menuId || 0,
    };
    this.reportService.GetReportList(param).then(
      (res: any) => {
        if (res.type) {
          cb(res.data);
        } else {
          cb([]);
        }
      },
      (err) => {
        console.log(err);
        cb([]);
      }
    );
  }
  presentToast(type, body) {
    if (type === "success") {
      this.toastr.success(body, "Success", {
        closeButton: false,
      });
    } else if (type === "error") {
      this.toastr.error(
        'Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.',
        "Error",
        {
          timeOut: 0,
          closeButton: true,
        }
      );
    } else {
      this.toastr.warning(body, "Warning", {
        closeButton: false,
      });
    }
  }

  getLoginReportsList() {
    this.reportService.getemployeedetailreportTypeList({}).then(
      (res) => {
        console.log(res);
        if (res["type"] === true) {
          this.emplyeedetailType = res["data"];
          this.emplyeedetailType.forEach((element) => {
            element["selected"] = false;
          });
          this.openPopUpEmployeeDetailType = true;
        }
        this.spinner.hide();
      },
      (err) => {
        this.spinner.hide();
        console.log(err);
      }
    );
  }

  onChange(value, item, index) {
    // console.log('Type: ',type);
    // console.log('item: ',item);
    let tempselected = null;
    this.anyOneSelected = true;

    if (value) {
      this.anyOneSelected = false;
    }

    if (item.id == 1) {
      this.emplyeedetailType.forEach((element) => {
        element.selected = value;
      });
      // anyOneSelected = true;
    } else {
      // this.emplyeedetailType[0].selected = false;
      this.emplyeedetailType[index].selected = value;
      this.emplyeedetailType.forEach((value, key) => {
        if (value.id != 1) {
          if (value.selected === true) {
            this.anyOneSelected = false;
            if (tempselected != false) {
              tempselected = true;
            }
          } else {
            tempselected = false;
          }
        }
      });
      if (tempselected === true) {
        this.emplyeedetailType[0].selected = true;
      }
      // let CheckAllLength = this.emplyeedetailType.length;
      // let count = CheckAllLength - 1;
      // let j = 1;
      // while (count > 0) {
      //     if(this.emplyeedetailType[j].selected){
      //       count--;
      //        j++;
      //     }else{
      //       break;
      //     }
      // }
      // if(j == CheckAllLength){
      //   this.emplyeedetailType[0].selected = true;
      // }
    }
    if (!value) {
      this.emplyeedetailType[0].selected = false;
    }
  }
  getReportEmployee() {
    if (this.emplyeedetailType[0].selected) {
      this.getemplyeedetail(this.emplyeedetailType[0].id);
    } else {
      const concatPipe = this.makeDataCommaSeparated();
      this.getemplyeedetail(concatPipe);
    }
  }

  // makeDataPipeSeparated() {
  //   var concatData = '';
  //   this.emplyeedetailType.forEach(element => {
  //     if (concatData == '') {
  //       // concatData =
  //       if (element.selected) {
  //         concatData = element.id;
  //       }
  //     } else {
  //       if (element.selected) {
  //         concatData = concatData + '|' + element.id;
  //       }
  //       // concatData = concatData+ '|' +element.Id;
  //     }
  //     // if(element.selected){
  //     //   concatData = concatData +
  //     // }
  //   });
  //   console.log('Concated String ==>', concatData);
  //   return concatData;
  // }

  makeDataCommaSeparated() {
    var concatData = "";
    this.emplyeedetailType.forEach((element) => {
      if (concatData == "") {
        // concatData =
        if (element.selected) {
          concatData = element.id;
        }
      } else {
        if (element.selected) {
          concatData = concatData + "," + element.id;
        }
        // concatData = concatData+ '|' +element.Id;
      }
      // if(element.selected){
      //   concatData = concatData +
      // }
    });
    console.log("Concated String ==>", concatData);
    return concatData;
  }
  closesectioType() {
    if (this.menuId === 106 || this.menuId === 116 || this.menuId === 117) {
      this.openPopup = false;
    } else {
      this.openPopUpEmployeeDetailType = false;
      if (this.FMReport["data"] && this.FMReport["data"].length === 0) {
        this.nodata = true;
      }
    }
    // this.nodata = true;
  }

  Exportreport() {
    if (
      this.menuId === 106 ||
      this.menuId === 116 ||
      this.menuId === 117 ||
      this.menuId === 149 ||
      this.menuId === 150
    ) {
      this.openPopup = false;
      this.exports = true;
    } else {
      if (
        this.emplyeedetailType &&
        this.emplyeedetailType.length !== 0 &&
        this.emplyeedetailType[0].selected
      ) {
        // this.getemplyeedetail(this.emplyeedetailType[0].id);
        this.FMReportFilterValues.filterId = this.emplyeedetailType[0].id;
      } else {
        const concatPipe = this.makeDataCommaSeparated();
        // this.getemplyeedetail(concatPipe);
        this.FMReportFilterValues.filterId = concatPipe;
      }
      this.closesectioType();
      this.exports = true;
    }
  }

  closedmodel(data) {
    // if(this.menuId === 106 || this.menuId === 116 ||this.menuId === 117){
    //   this.close(data)
    // }
    // else{
    console.log(data);
    this.exports = false;
    if (data.res === 2) {
      this.router.navigate(["pages/reports/ReportList"]);
      // this.FMReportFilterValues = [];
    } else {
      if (
        this.emplyeedetailType &&
        this.emplyeedetailType.length !== 0 &&
        this.emplyeedetailType[0].selected
      ) {
        this.getemplyeedetail(this.emplyeedetailType[0].id);
      } else {
        const concatPipe = this.makeDataCommaSeparated();
        this.getemplyeedetail(concatPipe);
      }
    }
    // }
  }
  close(data) {
    console.log(data);
    this.exports = false;
    if (data.res == 2) {
      this.router.navigate(["pages/reports/ReportList"]);
      // this.FMReportFilterValues = [];
    }
  }

  closedOnlySchedule(data) {
    if (data.res === 2) {
      this.router.navigate(["pages/reports/ReportList"]);
      // this.FMReportFilterValues = [];
    } else {
      window.history.back();
    }
  }

  getReportSaveFormatList(cb) {
    if (this.menuId === 153 || this.menuId === 154) {
      const param = {
        menuId: this.menuId || 0,
      };
      this.reportService.GetReportList(param).then(
        (res: any) => {
          if (res.type) {
            this.FMReportList = res["data"];
            cb();
            // this.FMReadyForWorking = true;
          } else {
            cb();
            // this.FMReadyForWorking = true;
          }
        },
        (err) => {
          console.log(err);
          this.FMReadyForWorking = true;
        }
      );
    } else {
      this.FMReadyForWorking = true;
    }
  }
  // getDAMFileDetailsReport(){
  //   const param = {
  //     // courseTypeId: this.courseType,
  //   };
  //   const url: any = webApi.domain + webApi.url.report_get_dam_file_details;
  //   this.commonFunctionsService.httpPostRequest(url,param).then(res => {
  //     console.log(res);
  //     if (res['type'] === true) {
  //       this.FMReport = {
  //         data: res['data'],
  //         report: Report,
  //         toolbar: this.toolbar,
  //       }
  //       this.getReportSaveFormatList(() =>{
  //         this.SetReport();
  //       });
  //       // this.FMReadyForWorking = true;
  //       console.log(this.FMReport);
  //       this.cdf.detectChanges();
  //     }
  //     this.spinner.hide();
  //   }, err => {
  //     this.spinner.hide();
  //     console.log(err);
  //   });
  // }

  getDAMFileDetailsReportTimeOut() {
    const param = {
      // courseTypeId: this.courseType,
    };
    const url: any = webApi.domain + webApi.url.report_get_dam_file_details;
    this.getReportsByAPI({}, (result) => {
      if (!Array.isArray(result)) {
        this.spinner.hide();
        this.cdf.detectChanges();
        // type: 'TimeoutError',
        // const message = "Report taking too much time, can't be Previewed. Please Schedule the report";
        this.toastr.warning(result["message"], "Warning");
        if (result["type"] === "TimeoutError") {
          setTimeout(() => {
            this.Exportreport();
          });
        }
      }else if (result.length !== 0) {
        this.FMReport = {
          data: result,
          report: Report,
          toolbar: this.toolbar,
        };
        this.FMReadyForWorking = true;
        this.getReportSaveFormatList(() => {
          this.SetReport();
        });
        console.log(this.FMReport);
        this.spinner.hide();
        this.cdf.detectChanges();
      } else if (result.length === 0) {
        this.spinner.hide();
        // this.showSpinner = false;
        this.cdf.detectChanges();
        this.presentToast("warning", "No data available");
      }
    });
  }

  // getDAMFileShareReport(){
  //   const param = {
  //     // courseTypeId: this.courseType,
  //   };
  //   const url: any = webApi.domain + webApi.url.report_get_dam_file_share_details;
  //   this.commonFunctionsService.httpPostRequest(url,param).then(res => {
  //     console.log(res);
  //     if (res['type'] === true) {
  //       this.FMReport = {
  //         data: res['data'],
  //         report: Report,
  //         toolbar: this.toolbar,
  //       }
  //       // this.FMReadyForWorking = true;
  //       // console.log(this.FMReport);
  //       this.getReportSaveFormatList(() =>{
  //         this.SetReport();
  //       });
  //       this.cdf.detectChanges();
  //     }
  //     this.spinner.hide();
  //   }, err => {
  //     this.spinner.hide();
  //     console.log(err);
  //   });
  // }

  getDAMFileShareReportTimeOut() {
    const param = {
      // courseTypeId: this.courseType,
    };
    const url: any =
      webApi.domain + webApi.url.report_get_dam_file_share_details;
    this.getReportsByAPI(param, (result) => {
      if (!Array.isArray(result)) {
        this.spinner.hide();
        this.cdf.detectChanges();
        // type: 'TimeoutError',
        // const message = "Report taking too much time, can't be Previewed. Please Schedule the report";
        this.toastr.warning(result["message"], "Warning");
        if (result["type"] === "TimeoutError") {
          setTimeout(() => {
            this.Exportreport();
          });
        }
      } else if (result.length !== 0) {
        this.FMReport = {
          data: result,
          report: Report,
          toolbar: this.toolbar,
        };
        this.FMReadyForWorking = true;
        this.getReportSaveFormatList(() => {
          this.SetReport();
        });
        console.log(this.FMReport);
        this.spinner.hide();
        this.cdf.detectChanges();
      } else if (result.length === 0) {
        this.spinner.hide();
        // this.showSpinner = false;
        this.cdf.detectChanges();
        this.presentToast("warning", "No data available");
      }
    });
  }
  sendSaveBtn(val) {
    this.isdata = val;
    setTimeout(() => {
      this.isdata = "";
    }, 2000);
  }

  closeSiderBar() {
    this.exports = false;
  }

  closeOnlySiderBar() {
    window.history.back();
  }
}
