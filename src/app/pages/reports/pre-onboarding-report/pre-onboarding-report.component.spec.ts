import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreOnboardingReportComponent } from './pre-onboarding-report.component';

describe('PreOnboardingReportComponent', () => {
  let component: PreOnboardingReportComponent;
  let fixture: ComponentFixture<PreOnboardingReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreOnboardingReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreOnboardingReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
