import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ThemeModule } from '../../../@theme/theme.module';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
    imports: [
      ThemeModule,
      NgxPaginationModule,
    ],
    declarations: [
      //  CourseConsumptionReportComponent,
    ],
    schemas: [ 
      CUSTOM_ELEMENTS_SCHEMA,
      NO_ERRORS_SCHEMA 
    ]
  })
  export class ClassroomReportModule { }