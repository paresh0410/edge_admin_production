import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkflowReportComponent } from './workflow-report.component';

describe('WorkflowReportComponent', () => {
  let component: WorkflowReportComponent;
  let fixture: ComponentFixture<WorkflowReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkflowReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkflowReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
