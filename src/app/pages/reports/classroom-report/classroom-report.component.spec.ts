import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassroomReportComponent } from './classroom-report.component';

describe('ClassroomReportComponent', () => {
  let component: ClassroomReportComponent;
  let fixture: ComponentFixture<ClassroomReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassroomReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassroomReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
