import {
  Component, OnInit, ViewEncapsulation, ChangeDetectorRef, ElementRef, ViewChild,
  ViewRef, AfterViewInit
} from '@angular/core';
import { NbThemeService, NbColorHelper } from '@nebular/theme';
// import { takeWhile } from 'rxjs/operators/takeWhile' ;
import { PaginatePipe, PaginationControlsDirective, PaginationService } from 'ngx-pagination';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ReportsService } from '../reports.service';
import { AppService } from '../../../app.service';
// declare var Flexmonster;
import * as Flexmonster from 'flexmonster';
import { FlexmonsterPivot } from 'ng-flexmonster';
import { Report } from '../report';
import { FMToolbar, FMReportEntity, FMReportFilterEntity, FMReportFilter } from '../../../component/fm-report/fm-report';
import '../../../../assets/style/style.css';
import { ExcelService } from '../../../service/excel-service';
import { NgxSpinnerService } from 'ngx-spinner';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { SideMenuFilterComponent } from '../../../component/side-menu-filter/side-menu-filter.component';
import { CourseType } from '../../../entity/lovmaster.enum';
import { ToastrService } from 'ngx-toastr';
import { SuubHeader } from '../../components/models/subheader.model';
import { ScrollDispatcher } from '@angular/cdk/scrolling';
import { Filter } from '../../../models/filter.modal';
import * as _ from "lodash";
@Component({
  selector: 'ngx-classroom-report',
  templateUrl: './classroom-report.component.html',
  styleUrls: ['./classroom-report.component.scss'],
  providers: [],
  encapsulation: ViewEncapsulation.None,
})
export class ClassroomReportComponent implements OnInit, AfterViewInit {

  @ViewChild('pivot') pivot: FlexmonsterPivot;
  @ViewChild('scrollDiv') scrolledElement: ElementRef;
  ReportName: string = 'Batch Consumption';
  filterDataReg: any;
  contdata: any;
  header: SuubHeader;
  showSpinner: boolean = false;

  filtertype: any = {
    primary: 'Batch Wise Filter',
    secondary: 'Data Wise Filter',
    limit: 5,
  };
  regRowData = {
    dataSource: {
      data: 'https://cdn.flexmonster.com/reports/report.json',
    },
  };

  regRowDataCount: any;

  pivotReport: any;
  toolbar: FMToolbar = {
    connect: false,
    open: false,
    save: false,
    export: true,
    grid: true,
    charts: true,
    format: true,
    options: true,
    fields: true,
    fullscreen: true,
    report: true,
  };
  FMReadyForWorking: boolean = false;
  FMReport: any = {
    data: [],
    report: {},
    toolbar: this.toolbar,
  };
  FMReportFilterObj: FMReportFilter = {
    main: [],
    sub: [],
  };
  FMReportFilterWorking: boolean = false;
  FMReportList: Array<FMReportEntity> = [];
  courseList: any = [];
  selected = [];
  readonly headerHeight = 50;
  readonly rowHeight = 50;
  infiniteworking: boolean = false;
  CourseFilterList: any = [];
  courseNamelist: any = [];
  isLoading: boolean = false;
  FMReportFilterValues: any = {
    Ids: [],
    Filters: {},
    courseName: [],
    courseTypeId: '',
    menuId: '',
    AcitivityIds: [],
  };
  ActivityFilterList: any = [];
  // Syntax for reference
  reportFilters: {
    'uniqueName': 'tags',
    'filter': {
      'members': ['tags.[dc3]', 'tags.[demo3]'],
    },
    'sort': 'unsorted'
  };
  ReportFiltersSET: any = {
    FMReport: [],
    FMReportList: [],
    FMReportName: null,
    ReportFilter: {}
  };
  menuId: number;
  exports: boolean = false;
  courseTypeId: number = CourseType.CLASSROOM;
  FilterParam: any = {
    start: 0,
    rows: 20,
    tagIds: null,
    categoryIds: null,
    total: 0,
    text: null,
    courseTypeId: this.courseTypeId,
    menuId: '',
  };
  searchtext: string;
  cacheFilters: any;
  // filter
  // filters: any = [];
  // filter: boolean = false;
  filter: any = {
    primary: false,
    secondary: false,
  };
  filters: any = {
    primary: [],
    secondary: [],
  };
  filtersInner: any = {
    primary: [],
    secondary: [],
  };
  filtersCache: any = {
    primary: [],
    secondary: [],
  };
  filtercon: Filter = {
    ascending: false,
    descending: false,
    showDropdown: false,
    dropdownList: [
      { drpName: 'Enrol Date', val: 1 },
      { drpName: 'Created Date', val: 2 },
    ],
  };
  filterSecondaryCon: Filter = {
    ascending: false,
    descending: false,
    showDropdown: false,
    dropdownList: [
      { drpName: 'Enrol Date', val: 1 },
      { drpName: 'Created Date', val: 2 },
    ],
  };
  labelsActivity: any = [
    { labelname: '', bindingProperty: '', componentType: 'checkbox' },
    { labelname: 'Batch Code', bindingProperty: 'courseCode', componentType: 'text' },
    { labelname: 'Batch Name', bindingProperty: 'fullname', componentType: 'text' },
    { labelname: 'Activity Name', bindingProperty: 'activityName', componentType: 'text' },
    { labelname: 'Batch Start Date', bindingProperty: 'startDate', componentType: 'date' },
    { labelname: 'Batch End Date', bindingProperty: 'endDate', componentType: 'date' },
    { labelname: 'Enrol Count', bindingProperty: 'enrolcount', componentType: 'text' },
  ]
  labels: any = [
    { labelname: '', bindingProperty: '', componentType: 'checkbox' },
    { labelname: 'Batch Code', bindingProperty: 'courseCode', componentType: 'text' },
    { labelname: 'Batch Name', bindingProperty: 'fullname', componentType: 'text' },
    { labelname: 'Batch Start Date', bindingProperty: 'startDate', componentType: 'date' },
    { labelname: 'Batch End Date', bindingProperty: 'endDate', componentType: 'date' },
    { labelname: 'Enrol Count', bindingProperty: 'enrolcount', componentType: 'text' },
  ];

  notiTitle:string="Report Schedule"
  btnName: string = 'Save';
  btnName1: string = 'Save & Show';
  isdata: any;

  constructor(protected service: ReportsService, private router: Router,
    // private toasterService: ToasterService,
    private AppService: AppService, public cdf: ChangeDetectorRef,
    private spinner: NgxSpinnerService,
    private el: ElementRef, private excelservice: ExcelService,
    private routes: ActivatedRoute,
    private toastr: ToastrService,
    private scrollDispatcher: ScrollDispatcher,
  ) {
    console.log('FMReportlist :- ', this.FMReportList);
    this.ReportName = this.service.getReportname();
    this.header = {
      title: this.ReportName,
      btnsSearch: true,
      searchBar: true,
      searchtext: '',
      dropdownlabel: ' ',
      placeHolder: 'Search by batch name',
      drplabelshow: false,
      drpName1: '',
      drpName2: ' ',
      drpName3: '',
      drpName1show: false,
      drpName2show: false,
      drpName3show: false,
      btnName1: '',
      btnName2: 'Schedule',
      btnName3: 'Preview',
      btnAdd: '',
      btnName1show: false,
      btnName2show: true,
      btnName3show: true,
      btnBackshow: true,
      btnAddshow: false,
      filter: false,
      showBreadcrumb: true,
      filterConfig: {
        primary: true,
        primaryText: this.filtertype.primary,
        secondary: true,
        secondaryText: this.filtertype.secondary,
        showPrimarySelectedFilteredCount: true,
        showSecondarySelectedFilteredCount: true,
        primarySelectedFilteredCount: 0,
        secondarySelectedFilteredCount: 0,
      },
    };
    this.contdata = this.AppService.getuserdata();
    this.menuId = this.routes.snapshot.params['menuId'] == undefined ? 0 : Number(this.routes.snapshot.params['menuId']);
    console.log('MENUID', this.menuId);
    this.FilterParam.menuId = this.menuId;
    this.filterDataReg = {
      code: null,
      codeRange: {
        to: null,
        from: null,
      },
      name: null,
      invited: null,
      attendance: 'Yes',
      joined: 'Yes',
      doj: null,
      dojRange: {
        to: null,
        from: null,
      },
      inductionLocation: null,
      designation: null,
      department: null,
      grade: null,
      contactDetails: null,
      trainingDate: null,
      trainingDateRange: {
        to: null,
        from: null,
      },
      remarks: null,
      userId: this.contdata ?   this.contdata.userId : null,
      roleId: this.contdata ? this.contdata.roleId : null,
    };
    if(this.menuId==60){
      this.filtertype= {
        primary: 'Feedback Wise Filter',
        secondary: 'Data Wise Filter',
        limit: 1
      };
    }
    // this.GetCourseConsumptionList(this.FilterParam, (result) => {
    //   this.courseList = result.courseList;
    //   this.FilterParam.total = result.totalCount;
    // })
    // this.getUsersData();
  }

  ngOnInit() {
    if (this.menuId == 57 || this.menuId == 58 || this.menuId == 59 || this.menuId == 60 || this.menuId == 111 || this.menuId == 120) {
      this.header.breadCrumbList = [
        {
          'name': 'Reports',
          'navigationPath': '/pages/reports',
        },
        {
          'name': 'Classroom Course ',
          'navigationPath': '/pages/reports/reports_home',
        }];
    }
  }

  customizeToolbar(toolbar) {
    // get all tabs
    const tabs = toolbar.getTabs();
    toolbar.getTabs = function () {
      // delete the first tab
      delete tabs[0];
      delete tabs[1];
      return tabs;
    };
  }

  submitFilterAsync(cb) {

  }

  onPivotReady(pivot: Flexmonster.Pivot): void {
    console.log('[ready] FlexmonsterPivot', this.pivot);
  }

  onReportComplete(): void {
    this.pivot.flexmonster.off('reportcomplete');

  }

  onCustomizeCell(cell: Flexmonster.CellBuilder, data: Flexmonster.CellData): void {
    if (data.isClassicTotalRow) {
      cell.addClass('fm-total-classic-r');
    }
    if (data.isGrandTotalRow) {
      cell.addClass('fm-grand-total-r');
    }
    if (data.isGrandTotalColumn) {
      cell.addClass('fm-grand-total-c');
    }
  }
  ngAfterViewInit() {
    this.submitFilterAsync((result) => {
      if (result.type === true) {
        const data = result['data'][0];
        this.FMReport = {
          data: data,
          report: Report,
          toolbar: this.toolbar,
        };
        this.FMReadyForWorking = true;
        Report.dataSource.data = data;

      }
    });
    this.GetReportList(result => {
      this.FMReportList = result;
      console.log('FMReportlist : -', this.FMReportList);
    });
    this.GetFilterInfo((result: any) => {
      if (result.main) {
        // tslint:disable-next-line:forin
        for (const obj in result.main) {

          this.FMReportFilterObj.main.push(result.main[obj]);
          this.bindfilter(result.main[obj], 'primary');
        }
      }
      if (result.sub) {
        // tslint:disable-next-line:forin
        for (const obj in result.sub) {

          this.FMReportFilterObj.sub.push(result.sub[obj]);
          this.bindfilter(result.sub[obj], 'secondary');
        }
      }
      // this.cacheFilters = this.service.getCacheReportFilter();
      // this.FMReportFilterWorking = true;
      // if (this.cacheFilters && this.cacheFilters.primaryflag) {
      //   this.getSelectedFilter(this.cacheFilters);
      // } else {
      //   this.FMReportFilterValues.Filters = this.populateCourseConsumptionSecondaryFilter(this.cacheFilters);
      //   this.onScrollDown(0, true, this.scrolledElement.nativeElement);
      // }

      /**
       * new filter cache
       */
      this.filtersCache = this.service.getCacheReportFilter();
      this.FMReportFilterWorking = true;
      if (this.filtersCache && this.filtersCache.primary && Object.keys(this.filtersCache.primary).length > 0) {
        this.getSelectedFilter(this.filtersCache.primary);
      } else {
        if (!this.filtersCache) {
          this.filtersCache = {
            primary: {},
            secondary: {},
          };
        }
        this.FMReportFilterValues.Filters = this.populateCourseConsumptionSecondaryFilter(this.filtersCache.secondary);
        this.onScrollDown(0, true, this.scrolledElement.nativeElement);
      }
      console.log('FMReportlist : -', this.FMReportFilterObj);
      this.bindValuesTofilterButtons();
      if (this.filtersCache && this.filtersCache.primary && Object.keys(this.filtersCache.primary).length > 0) {
        this.bindFilterCountToheader('primary', this.filtersCache.primary.filteredCount);
      }
      if (this.filtersCache && this.filtersCache.secondary && Object.keys(this.filtersCache.secondary).length > 0) {
        this.bindFilterCountToheader('secondary', this.filtersCache.secondary.filteredCount);
      }
    });
    this.scrollDispatcher.scrolled().subscribe(x => {
      console.log('I am scrolling', x);
      this.onScrollDown(1, false, this.scrolledElement.nativeElement);
    });
  }
  GetReportList(cb) {
    const param = {
      menuId: this.menuId || 0,
    };
    this.service.GetReportList(param).then((res: any) => {
      if (res.type) {
        cb(res.data);
      } else {
        cb([]);
      }
    }, err => {
      console.log(err);
      cb([]);
    });
  }
  GetReport(event) {
    console.log('Report : ', event);
  }
  handleChange(evt: any, value: any) {
    let target = evt.target;
    this.FMReadyForWorking = false;
    if (value) {
      this.FMReport.report = JSON.parse(value.metadata);
    }
    setTimeout(() => {
      this.FMReadyForWorking = true;
      this.cdf.detectChanges();
    }, 500);
  }
  GetFilterInfo(cb) {
    this.FMReportFilterWorking = false;
    const param = {
      courseTypeId: this.courseTypeId,
      menuId: this.menuId,
    }
    this.service.GetFilterInfo(param).then((res: any) => {
      if (res.type) {
        cb(res.data);
      } else {
        cb([]);
      }
    }, err => {
      console.log(err);
      cb([]);
    });
  }

  noData: boolean = false;
  GetCourseConsumptionList(value, cb) {
    // this.spinner.show();
    this.service.GetQuizCourseConsumption(value).then((res: any) => {
      if (res.type) {
        cb(res);
      } else {
        cb([]);
      }
    }, err => {
      this.spinner.hide();
      // this.showSpinner = false;
      this.cdf.detectChanges();
      console.log(err);
      cb([]);
    });
  }
  populateCourseConsumptionFilter(obj) {
    const item = {};
    try {
      if (obj) {
        // const primary = obj.primary;
        const primary = obj;
        for (const key in primary) {
          item[key] = null;
          if (primary[key].length > 0) {
            let commastring = null;
            for (let i = 0; i < primary[key].length; i++) {
              if (String(Date.parse(primary[key][i])) === 'NaN') {
                if (commastring) {
                  commastring += ',';
                }
                commastring += primary[key][i]['id'];
              } else {
                if (item[key] == null || item[key].length === 0) {
                  item[key] = [];
                }
                if (primary[key][i]) {
                  item[key].push(primary[key][i]);
                }
              }
            }
            if (commastring) {
              item[key] = commastring;
            }
          }
        }
      }
      return item;
    } catch (e) {
      return item;
    }
  }
  populateCourseConsumptionSecondaryFilter(obj) {
    const item = {};
    try {
      if (obj) {
        // const secondary = obj.secondary;
        const secondary = obj;
        for (const key in secondary) {
          item[key] = null;
          if (secondary[key].length > 0) {
            let commastring = '';
            for (let i = 0; i < secondary[key].length; i++) {
              if (String(Date.parse(secondary[key][i])) === 'NaN') {
                if (commastring) {
                  commastring += ',';
                }
                commastring += secondary[key][i]['itemName'];
              } else {
                if (item[key] == null || item[key].length === 0) {
                  item[key] = [];
                }
                if (secondary[key][i]) {
                  item[key].push(secondary[key][i]);
                }
              }
            }
            if (commastring) {
              item[key] = commastring;
            }
          }
        }
      }
      return item;
    } catch (e) {
      return item;
    }
  }
  getSelectedFilter(event) {
    console.log(event);
    if (event.primaryflag) {
      this.filtersCache.primary = event;
      const filtervalue: any = this.populateCourseConsumptionFilter(event); // new filter cache
      // const filtervalue: any = this.populateCourseConsumptionFilter(event.primary);
      this.FilterParam.tagIds = filtervalue.Tags === undefined ? null : filtervalue.Tags;
      this.FilterParam.categoryIds = filtervalue.Category === undefined ? null : filtervalue.Category;
      this.FilterParam.workflowIds = filtervalue.Workflow === undefined ? null : filtervalue.Workflow;
      this.FilterParam.programIds = filtervalue.Program === undefined ? null : filtervalue.Program;
      this.FilterParam.enrolDate = filtervalue['Enrol Date'] === undefined ? [] : filtervalue['Enrol Date'];
      this.FilterParam.rangeDate = filtervalue['Batch Date'] === undefined ? [] : filtervalue['Batch Date'];
      this.FilterParam.start = 0;
      this.FilterParam.text = this.searchtext;
      this.GetCourseConsumptionList(this.FilterParam, (result) => {
        this.courseList = result.courseList;

        this.FilterParam.total = result.totalCount;
        if (this.courseList.length == 0) {
          this.noData = true;
        } else {
          this.noData = false;
        }
      });
    } else if (event.secondaryflag) {
      this.filtersCache.secondary = event;
    }
    this.FMReportFilterValues.Filters = this.populateCourseConsumptionSecondaryFilter(event); // new filter cache
    // this.FMReportFilterValues.Filters = this.populateCourseConsumptionSecondaryFilter(event.secondary);
    this.FMReportFilterValues['secondaryScheduleParams'] = event['secondaryScheduleParams'];
    const cacheFilter = {
      primaryCache: event.primaryCache,
      secondaryCache: event.secondaryCache,
    }
    if (event.clear) {
      this.service.setCacheReportFilter(null);
    } else {
      this.service.setCacheReportFilter(this.filtersCache); // new filter cache
      // this.service.setCacheReportFilter(event);
    }
  }

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }


  SarchFilter($event, text) {
    this.FilterParam.text = text;
    this.FilterParam.start = 0;
    this.spinner.show();
    // this.showSpinner = true;
    // this.cdf.detectChanges();
    this.GetCourseConsumptionList(this.FilterParam, (result) => {
      this.spinner.hide();
      // this.showSpinner = false;
      this.cdf.detectChanges();
      this.courseList = result.courseList;
      this.FilterParam.total = result.totalCount;
      if (this.courseList.length == 0) {
        this.noData = true;
      } else {
        this.noData = false;
      }
    });
  }

  SarchFiltertest(event, text) {
    text = event.target.value;
    this.FilterParam.text = text;
    this.FilterParam.start = 0;
    if (text.length >= 3 || (text.length == 0 && event.keyCode != 13)) {
      // if(event.keyCode === 13){
      // this.spinner.show();
      // this.showSpinner = true;
      this.cdf.detectChanges();
      this.GetCourseConsumptionList(this.FilterParam, (result) => {
        this.spinner.hide();
        // this.showSpinner = false;
        this.cdf.detectChanges();
        this.courseList = result.courseList;
        this.FilterParam.total = result.totalCount;
        if (this.courseList.length === 0) {
          this.noData = true;
        } else {
          this.noData = false;
        }
      });
      // }
    }
  }
  clear() {
    if (this.FilterParam.text) {
    this.noData=false;
      this.header.searchtext = '';
      this.searchtext = this.header.searchtext;
      this.SarchFilter(null, this.searchtext);
    }
  }

  /**
   * Infinite Scroll
   */
  onScrollDown(offsetY: number, firstime: boolean, element) {


    // if ((this.FilterParam.start <= this.FilterParam.total && !this.infiniteworking)
    //     || String(offsetY) === '0')
    if ((this.FilterParam.start <= this.FilterParam.total && !this.infiniteworking
      && (Math.ceil(element.scrollHeight - element.scrollTop) - element.clientHeight) < 30)
      || String(offsetY) === '0') {
      this.infiniteworking = true;
      this.spinner.show();
      // this.showSpinner = true;
      // this.cdf.detectChanges();
      setTimeout(() => {
        if (this.cdf !== null && this.cdf !== undefined &&
          !(this.cdf as ViewRef).destroyed) {
          this.cdf.detectChanges();
        }
      }, 250);
      // this.cdf.detectChanges();
      if (!firstime) {
        this.FilterParam.start = this.FilterParam.start + 10;
      }
      this.GetCourseConsumptionList(this.FilterParam, (result) => {
        this.courseList = this.courseList.concat(result.courseList);
        if (this.courseList.length === 0) {
          this.noData = true;
        }
        this.FilterParam.total = result.totalCount;
        this.infiniteworking = false;
        if (this.courseList.length === 0) {
          this.noData = true;
        } else {
          this.noData = false;
        }
        this.spinner.hide();
        // this.showSpinner = false;
        // this.cdf.detectChanges();
        setTimeout(() => {
          if (this.cdf !== null && this.cdf !== undefined &&
            !(this.cdf as ViewRef).destroyed) {
            this.cdf.detectChanges();
          }
        }, 250);
        // this.cdf.detectChanges();
        // this.cdf.detectChanges();
      });
    } else {
      // this.spinner.hide();
      console.log('Course List : ', this.courseList);
    }
    // const viewHeight =
    //   this.el.nativeElement.getBoundingClientRect().height - this.headerHeight;



  }

  /**
   * OnChange Events
   */
  onSelect(event) {
    // if(this.menuId === 60 || this.menuId === 59){
    try {
      if (event.length > 0) {
        this.CourseFilterList = [];
        this.courseNamelist = [];
        this.ActivityFilterList = [];
        event.forEach(element => {
          if (element) {
            this.CourseFilterList.push(element.courseId);
            this.courseNamelist.push(element.fullname);
            if(element['activityId']){
              this.ActivityFilterList.push(element.activityId);
            }
          }
        });
        this.FMReportFilterValues.Ids = this.CourseFilterList;
        this.FMReportFilterValues.courseName = this.courseNamelist;
        this.FMReportFilterValues.courseTypeId = this.courseTypeId;
        this.FMReportFilterValues.menuId = this.menuId;
        this.FMReportFilterValues.AcitivityIds =  this.ActivityFilterList;
      } else {
        this.CourseFilterList = [];
        this.FMReportFilterValues.Ids = [];
        this.courseNamelist = [];
        this.FMReportFilterValues.courseName = [];
      }

      console.log(this.CourseFilterList);
    } catch (e) {
      console.log(e);
    }
  // }
  }
  onSelectRow(event) {
    console.log(event);
    let allow = true;
    if (this.CourseFilterList.length > 0) {
      for (let i = 0; i < this.CourseFilterList.length; i++) {
        if (this.CourseFilterList[i] === event.courseId) {
          this.CourseFilterList.splice(i, 1);
          this.courseNamelist.splice(i, 1);
          this.ActivityFilterList.splice(i, 1);
          allow = false;
          break;

        }
      }
    }

    if (allow === true) {
      this.CourseFilterList.push(event.courseId);
      this.courseNamelist.push(event.fullname);
      this.ActivityFilterList.push(event.activityId)


    }
    console.log('courseFilterlist', this.CourseFilterList);
    this.FMReportFilterValues.Ids = this.CourseFilterList;
    this.FMReportFilterValues.courseName = this.courseNamelist;
    this.FMReportFilterValues.courseTypeId = this.courseTypeId;
    this.FMReportFilterValues.menuId = this.menuId;
    this.FMReportFilterValues.AcitivityIds = this.ActivityFilterList;
    console.log(this.CourseFilterList);
  }

  selectCourseForReport(course) {
    let CourseFilter = [];
    CourseFilter.push(course.courseId);

    const demoId = [];
    if(course['activityId']){
      demoId.push(course.activityId);
    }
    this.FMReportFilterValues.AcitivityIds = demoId;


    this.FMReportFilterValues.Ids = CourseFilter;

    console.log('Course List ', this.FMReportFilterValues);
    this.GetReports();
  }


  onActivate(event) {
  }
  GetReportCourseConsumption(value, cb) {
    this.service.GetReportCourseConsumption(value).then((res: any) => {
      if (res.type) {
        cb(res.data);
      } else {
        cb([]);
      }
    }, err => {
      console.log(err);
      cb([]);
    });
  }
  GetReportCourseConsumptionDownload(value, cb) {
    this.service.GetClassroomReportByMenuId(value, this.menuId, (res) => {
      if (res.type) {
        cb(res.data);
      } else {
        cb([]);
      }
    });
  }
  GetReports() {
    let param: any;
    // this.showSpinner = true;
    this.spinner.show()
    // this.cdf.detectChanges();
    setTimeout(() => {
      if (this.cdf !== null && this.cdf !== undefined &&
        !(this.cdf as ViewRef).destroyed) {
        this.cdf.detectChanges();
      }
    }, 250);
    // this.spinner.show();
    this.FMReadyForWorking = false;
    if (this.FMReportFilterValues.Ids.length > 0) {

      if (this.menuId === 60 || this.menuId === 111) {
        if (this.FMReportFilterValues.Ids.length > 1) {
          this.spinner.hide();
          // this.showSpinner = false;
          // this.cdf.detectChanges();
          setTimeout(() => {
            if (this.cdf !== null && this.cdf !== undefined &&
              !(this.cdf as ViewRef).destroyed) {
              this.cdf.detectChanges();
            }
          }, 250);
          this.presentToast('warning', 'You cannot select more than one course');
          return;
        }
      }
      if (this.menuId === 120 || this.menuId === 59 || this.menuId === 58 || this.menuId === 57) {
        if (this.FMReportFilterValues.Ids.length > 5) {
          this.spinner.hide();
          // this.showSpinner = false;
          // this.cdf.detectChanges();
          setTimeout(() => {
            if (this.cdf !== null && this.cdf !== undefined &&
              !(this.cdf as ViewRef).destroyed) {
              this.cdf.detectChanges();
            }
          }, 250);
          this.presentToast('warning', 'You cannot select more than five course');
          return;
        }
      }
      const courseIds = this.populateString(this.FMReportFilterValues.Ids);
      const activityId = this.populateString(this.FMReportFilterValues.AcitivityIds);
      param = {
        courseIds: courseIds,
        courseTypeId: this.courseTypeId,
        activityIds: activityId,
        activityId: activityId,
        menuId: this.FMReportFilterValues.menuId,
        filter: this.FMReportFilterValues.Filters
      };

      const reportFilters = this.FMReportFilterValues.Filters;
      // this.populateFMFilter(this.FMReportFilterValues.Filters);
      this.ClassRoomDataByMenuId(param, (result) => {
        if (!Array.isArray(result)){
          this.spinner.hide();
          this.cdf.detectChanges();
          // type: 'TimeoutError',
          // const message = "Report taking too much time, can't be Previewed. Please Schedule the report";
          this.toastr.warning(result['message'], 'Warning');
          if(result['type'] === 'TimeoutError'){
            setTimeout(() =>{
              this.Exportreport();
            });
          }
        }else if (result.length !== 0) {
          Report.dataSource.data = result;
          this.FMReport = {
            data: result,
            report: Report,
            toolbar: this.toolbar,
          };
          this.FMReadyForWorking = true;
          this.ReportFiltersSET = {
            FMReport: this.FMReport,
            FMReportList: this.FMReportList,
            FMReportName: this.ReportName,
            ReportFilter: reportFilters,
          };
          this.service.SET_Report_Filter_Values(this.ReportFiltersSET);
          this.spinner.hide();
          // this.showSpinner = false;
          this.cdf.detectChanges();
          this.GoTOReportViewer();

        } else if (result.length === 0) {
          this.spinner.hide();
          // this.showSpinner = false;
          this.cdf.detectChanges();
          this.presentToast('warning', 'No data available');
        }
      });

    } else {
      this.spinner.hide();
      // this.showSpinner = false;
      this.cdf.detectChanges();
      this.presentToast('warning', 'Select a course');

      // alert('select a course');
    }
  }
  DownlaodReports() {
    this.spinner.show();
    // this.showSpinner = true;
    this.cdf.detectChanges();
    this.FMReadyForWorking = false;
    if (this.FMReportFilterValues.Ids.length > 0) {
      if (this.menuId === 60) {
        if (this.FMReportFilterValues.Ids.length > 1) {
          this.spinner.hide();
          // this.showSpinner = false;
          this.cdf.detectChanges();
          this.presentToast('warning', 'You cannot select more than one course');
          return;
        }
      }
      if (this.FMReportFilterValues.Ids.length > this.filtertype.limit) {
        // alert('You cannot select more than ' + this.filtertype.limit);
        this.presentToast('warning', 'You cannot select more than ' + this.filtertype.limit);
        this.spinner.hide();
        // this.showSpinner = false;
        this.cdf.detectChanges();
        return null;
      }
      const courseIds = this.populateString(this.FMReportFilterValues.Ids);
      const param = {
        courseIds: courseIds,
        courseTypeId: this.courseTypeId
      };
      this.GetReportCourseConsumptionDownload(param, (result) => {
        if (result.length !== 0) {
          this.excelservice.exportAsExcelFile(result, this.ReportName);
          this.spinner.hide();
          // this.showSpinner = false;
          this.cdf.detectChanges();
        } else if (result.length === 0) {
          this.presentToast('warning', 'No data available');
          this.spinner.hide();
          // this.showSpinner = false;
          this.cdf.detectChanges();
        } else {
          this.spinner.hide();
          // this.showSpinner = false;
          this.cdf.detectChanges();
          this.presentToast('error', '');
        }
      })
    }
    else {
      this.presentToast('warning', 'Select a course');
      this.spinner.hide();
      // this.showSpinner = false;
      this.cdf.detectChanges();
    }
  }
  populateString(list = []) {
    if (list.length === 0) {
      return '';
    }
    return list.join(',');
  }

  populateFMFilter(filters) {
    const reportFilters = [];
    try {
      if (filters) {
        for (const filter in filters) {
          const reportFilter = {
            'uniqueName': 'tags',
            'filter': {
              'members': ['tags.[dc3]', 'tags.[demo3]'],
            },
            'sort': 'unsorted'
          };
          if (filter && filters[filter].length > 0) {
            const array = filters[filter];
            const uniqueName = filter; // String(filter).toLowerCase();
            reportFilter['uniqueName'] = uniqueName;
            const memberslist = [];
            array.forEach(element => {
              const member = uniqueName + '.' + '[' + element + ']';
              memberslist.push(member);
            });
            reportFilter['filter']['members'] = memberslist;
            reportFilters.push(reportFilter);
          }
        }
      }
      return reportFilters;
    } catch (e) {
      return reportFilters;
    }
  }
  GoToBack() {
    window.history.back();
  }

  GoTOReportViewer() {
    this.router.navigate(['pages/reports/report-viewer', this.menuId]);
  }

  ClassRoomDataByMenuId(value, cb) {
    this.service.GetClassroomReportByMenuIdTimeOut(value, this.menuId, (res) => {
      if (!Array.isArray(res['data'])){
        cb(res)
      }else if (res['type']) {
        cb(res['data']);
      } else {
        cb([]);
      }
    });
  }

  Exportreport() {
    console.log(this.FMReportFilterValues, 'FMReportFilterValues');
    if (this.FMReportFilterValues.courseName.length > 0) {
      if (this.menuId === 60) {
        if (this.FMReportFilterValues.Ids.length > 1) {
          this.spinner.hide();
          // this.showSpinner = false;
          this.cdf.detectChanges();
          this.presentToast('warning', 'You cannot select more than one course');
          return;
        }
      }
      if (this.FMReportFilterValues.courseName.length > this.filtertype.limit) {
        this.presentToast('warning', 'You cannot select more than ' + this.filtertype.limit);

      } else {
        const courseName = this.populateString(this.FMReportFilterValues.courseName);
        this.exports = true;
      }
    } else {
      this.presentToast('warning', 'Select a course');
      this.spinner.hide();
      // this.showSpinner = false;
      this.cdf.detectChanges();
    }
  }

  closedmodel(data) {
    console.log(data);
    this.exports = false;
    if (data.res === 2) {
      this.router.navigate(['pages/reports/ReportList']);
    }
  }

  /**
   * Filter
   */
  gotoFilter(type) {
    if (type === 'primary') {
      this.filter.secondary = false;
      this.filter.primary = !this.filter.primary;
    } else if (type === 'secondary') {
      this.filter.primary = false;
      this.filter.secondary = !this.filter.secondary;
    }
    // this.filter = !this.filter;
  }
  bindfilter(obj, filterType) {
    let filtername, filterValueName, type, singleSelection, joiner;
    if (obj.length > 0) {
      filtername = obj[0]['filterId'];
      filterValueName = obj[0]['filterValue'];
      type = obj[0]['type'];
      singleSelection = obj[0]['singleSelection'];
      joiner = obj[0]['joiner'];
    }
    const item = {
          count: '',
          value: '',
          tagname: filtername,
          isChecked: false,
          list: obj,
          filterValue: filterValueName,
          type: type,
          filterId: filtername,
          singleSelection: singleSelection,
          joiner: joiner,
        };
        if (filtername) {
          if (filterType === 'primary') {
            this.filters.primary.push(item);
          } else if (filterType === 'secondary') {
            this.filters.secondary.push(item);
          }
        }
  }
  filteredPrimaryChanged(event) {
    console.log('Filtered Primary Event - ', event);
    if (!event) event = {};
    event.primaryflag = true;
    this.getSelectedFilter(event);
    this.bindFilterCountToheader('primary', event['filteredCount']);
  }
  filteredSecondaryChanged(event) {
    console.log('Filtered Secondary Event - ', event);
    if (!event) event = {};
    event.secondaryflag = true;
    this.getSelectedFilter(event);
    this.bindFilterCountToheader('secondary', event['filteredCount']);
  }

  sendSaveBtn(val) {
    this.isdata = val;
    setTimeout(() => {
      this.isdata = '';
    }, 2000);
  }

  closeSiderBar() {
    this.exports = false;
  }

  bindFilterCountToheader(filterType,value){
    const header = this.header;
    if(filterType=== 'primary'){
      if(value){
        header.filterConfig.primarySelectedFilteredCount = value;
      }else {
        header.filterConfig.primarySelectedFilteredCount = 0;
      }
    }else {
      if(value){
        header.filterConfig.secondarySelectedFilteredCount = value;
      }else {
        header.filterConfig.secondarySelectedFilteredCount = 0;
      }
    }
    this.header = _.cloneDeepWith(header);
  }

  bindValuesTofilterButtons(){
    if(this.FMReportFilterObj['main'] && (this.FMReportFilterObj['main'].length === 0)){
      const headerObj = this.header;
      headerObj.filterConfig.primary = false;
      this.header = _.cloneDeep(headerObj);
    }
    if(this.FMReportFilterObj['sub'] && this.FMReportFilterObj['sub'].length === 0){
      const headerObj = this.header;
      headerObj.filterConfig.secondary = false;
      this.header = _.cloneDeep(headerObj);
    }
  }
}
