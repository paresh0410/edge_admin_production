import { Component, OnDestroy, OnInit, ViewEncapsulation, ChangeDetectorRef, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { NbThemeService, NbColorHelper } from '@nebular/theme';
// import { takeWhile } from 'rxjs/operators/takeWhile' ;
import { PaginatePipe, PaginationControlsDirective, PaginationService } from 'ngx-pagination';
import { Router, NavigationStart, Routes, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ReportsService } from '../reports.service';
import { AppService } from '../../../app.service';
// declare var Flexmonster;
import * as Flexmonster from 'flexmonster';
import { FlexmonsterPivot } from 'ng-flexmonster';
import { Report } from '../report';
import { FMToolbar, FMReportEntity, FMReportFilterEntity, FMReportFilter } from '../../../component/fm-report/fm-report';
import '../../../../assets/style/style.css';
import { ExcelService } from '../../../service/excel-service';
import { NgxSpinnerService } from 'ngx-spinner';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { SideMenuFilterComponent } from '../../../component/side-menu-filter/side-menu-filter.component';
import { CourseType } from '../../../entity/lovmaster.enum';
import { ToastrService } from 'ngx-toastr';
import { SuubHeader } from '../../components/models/subheader.model';
import { BrandDetailsService } from '../../../service/brand-details.service';

import { webApi } from '../../../service/webApi';

import { CommonFunctionsService } from "../../../service/common-functions.service";
@Component({
  selector: 'ngx-employee-consumption',
  templateUrl: './employee-consumption.component.html',
  styleUrls: ['./employee-consumption.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class EmployeeConsumptionComponent implements OnInit {
  @ViewChild('pivot') pivot: FlexmonsterPivot;
  ReportName: string ;
  header : SuubHeader = {
    title: '',
    btnsSearch: true,
    searchBar: true,
    searchtext: '',
    placeHolder:'Search by keywords',
    dropdownlabel: ' ',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: 'Schedule',
    btnName3: 'Preview',
    btnAdd: '',
    btnName1show: false,
    btnName2show: true,
    btnName3show: true,
    btnBackshow: true,
    btnAddshow: false,
    filter: false,
    showBreadcrumb:true
    
  };
  contdata: any;
  pivotReport: any;
  courseList: any = [];
  readonly headerHeight = 50;
  readonly rowHeight = 50;
  infiniteworking: boolean = false;
  menuId: number;
  courseTypeId: number = CourseType.ONLINE;
  FilterParam: any = {
    start: 0,
    rows: 10,
    tagIds: null,
    categoryIds: null,
    total: 0,
    courseTypeId: this.courseTypeId,
    searchtext: ""
  };
  FMReportFilterValues: any = {
    Ids: [],
    Filters: {},
  };
  ReportFiltersSET: any = {
    FMReport: [],
    FMReportList: [],
    FMReportName: null,
    ReportFilter: {},
  };
  toolbar: FMToolbar = {
    connect: false,
    open: false,
    save: false,
    export: true,
    grid: true,
    charts: true,
    format: true,
    options: true,
    fields: true,
    fullscreen: true,
    report: true,
  };
  selected = [];
  filtertype: any = {
    primary: 'Course Wise Filter',
    secondary: 'Data Wise Filter',
    limit: 5
  };
  FMReadyForWorking: boolean = false;
  FMReport: any = {
    data: [],
    report: {},
    toolbar: this.toolbar,
  };
  CourseFilterList: any = [];
  courseNamelist: any = [];
  FMReportFilterWorking: boolean = false;
  FMReportList: Array<FMReportEntity> = [];
  searchtext: string;
  exports: boolean = false;
  labels: any = [
		{ labelname: '', bindingProperty: '', componentType: 'checkbox' },
    { labelname: 'User Name', bindingProperty: 'ecn', componentType: 'text' },
    { labelname: 'Full Name', bindingProperty: 'full_name', componentType: 'text' },
    { labelname: 'Email', bindingProperty: 'email', componentType: 'text' },
		{ labelname: 'Enrol Count', bindingProperty: 'enrol_count', componentType: 'text' },
  ];

  notiTitle:string="Report Schedule"
  btnName: string = 'Save';
  btnName1: string = 'Save & Show';
  isdata: any;

  currentBrandData: any;
  setFlag: boolean=true;
  damreport: any;
  constructor(protected service: ReportsService, private router: Router,
    private AppService: AppService, public cdf: ChangeDetectorRef,
    private spinner: NgxSpinnerService,
    private el: ElementRef, private excelservice: ExcelService,
    public brandService: BrandDetailsService,
    private routes: ActivatedRoute,
    private toastr: ToastrService,
    private commonFunctionService: CommonFunctionsService,
  ) {
    this.contdata = this.AppService.getuserdata();


  }
  ngOnInit() {
    this.menuId = this.routes.snapshot.params['menuId'] == undefined ? 0 : Number(this.routes.snapshot.params['menuId']);
if(this.menuId==159||this.menuId==161){
  this.labels= [
		{ labelname: '', bindingProperty: '', componentType: 'checkbox' },
    { labelname: 'Folder Name', bindingProperty: 'CatgeoryName', componentType: 'text' },
    { labelname: 'Asset Count', bindingProperty: 'AssetCount', componentType: 'text' },
    { labelname: 'Course Count', bindingProperty: 'CourseCount', componentType: 'text' },
  ];
  this.setFlag=false
}

    this.currentBrandData = this.brandService.getCurrentBrandData();
    this.header.title=this.currentBrandData.employee+' Learning'
    this.ReportName=this.currentBrandData.employee+' Learning'
    if(this.menuId==115){
      this.header.breadCrumbList=[
        {
          'name': 'Reports',
          'navigationPath': '/pages/reports',
        },
      ];

    }
      if(this.menuId==159){
        this.header.title="Asset Mapping Report"
        this.header.breadCrumbList=[
          {
            'name': 'Reports',
            'navigationPath': '/pages/reports',
          },
          {
            'name': 'DAM Report',
            'navigationPath': '/pages/reports/reports_home',
          }
        ]
      }
      if(this.menuId==161){
        this.header.title="Asset Version Mapping Report"
        this.header.breadCrumbList=[
          {
            'name': 'Reports',
            'navigationPath': '/pages/reports',
          },
          {
            'name': 'DAM Report',
            'navigationPath': '/pages/reports/reports_home',
          }
        ]
      }
      this.onScrollDown(0, true);
  }

  onPivotReady(pivot: Flexmonster.Pivot): void {
    console.log('[ready] FlexmonsterPivot', this.pivot);
  }

  onReportComplete(): void {
    this.pivot.flexmonster.off('reportcomplete');
  }

  noData: boolean = false;
  GetCourseEmployeeList(value, cb) {
    this.service.GetCourseEmployeeList(value).then((res: any) => {
      if (res.type) {
        this.spinner.hide()
        cb(res);
      } else {
        this.spinner.hide()
        this.noData = true;
        cb([]);
      }
    }, err => {
      this.spinner.hide();
      console.log(err);
      cb([]);
    });
  }
  /**
 * Infinite Scroll
 */
  onScrollDown(offsetY: number, pageload: boolean) {
    this.spinner.show();
    if ((this.FilterParam.start <= this.FilterParam.total && !this.infiniteworking) || String(offsetY) === '0') {
      this.infiniteworking = true;
      if (!pageload) {
        this.FilterParam.start = this.FilterParam.start + 10;
      }
      this.GetCourseEmployeeList(this.FilterParam, (result) => {
        this.courseList = this.courseList.concat(result.data);
        this.damreport=this.courseList
        if (this.courseList.length === 0) {
          this.noData = true;
        }
        this.FilterParam.total = result.data.length;
        this.infiniteworking = false;
        this.spinner.hide();
      });
    } else {
      this.spinner.hide();
      console.log('Course List : ', this.courseList);
    }
    const viewHeight =
      this.el.nativeElement.getBoundingClientRect().height - this.headerHeight;
  }

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }
  GetReportEmployeeList(value, cb) {
    if(this.menuId==161){
      this.service.GetReportDamList(value).then((res: any) => {
        if (res.type) {
          cb(res.data);
        } else {
          cb([]);
        }
      }, err => {
        console.log(err);
        cb([]);
      });
    }else{
    this.service.GetReportEmployeeList(value).then((res: any) => {
      if (res.type) {
        cb(res.data);
      } else {
        cb([]);
      }
    }, err => {
      console.log(err);
      cb([]);
    });
  }
  }

  GetReportEmployeeListTimeOut(value, cb){
    let url =  "";
    if(this.menuId == 161){
       url = webApi.domain + webApi.url.report_get_dam_asset_version_details;
    }else if(this.menuId == 159) {
       url = webApi.domain + webApi.url.report_get_dam_asset_mapping;
    }else{
      url = webApi.domain + webApi.url.report_fetch_emp_list;
    }
    this.commonFunctionService.reportPostRequestTimeOut(url, value).subscribe( (res) =>{
        console.log("res", res);
        cb(res.data);
      },
      error => {
          console.log('ERROR', error);
          // if(error === 'Timeout Exception'){

          // }
          cb(error);
        });
  }
  GetReportEmployeeListDownload(value, cb) {
    this.service.GetReportEmployeeListDownload(value).then((res: any) => {
      if (res.type) {
        cb(res.data);
      } else {
        cb([]);
      }
    }, err => {
      console.log(err);
      cb([]);
    });
  }

  SearchFilter($event, text) {
    this.FilterParam.searchtext = text;
    this.FilterParam.start = 0;
    this.spinner.show()
    this.GetCourseEmployeeList(this.FilterParam, (result) => {
      this.courseList = result.data;
      this.FilterParam.total = result.data.length;
      if (this.courseList.length == 0) {
        this.spinner.hide()
        this.noData = true;
      } else {
        this.spinner.hide()
        this.noData = false;
      }
    });
  }
  SarchFiltertest(event, text) {
    text = event.target.value;
    this.FilterParam.searchtext = text;
    this.FilterParam.start = 0;
    if(text.length>=3 || (text.length == 0  && event.keyCode!=13)){
      this.spinner.show()
      if(this.menuId==159||this.menuId==161){
  this.noData = false;
  const damReport=this.damreport;
  const temp = damReport.filter(function (d) {
    return String(d.CatgeoryName).toLowerCase().indexOf(text) !== -1 ||
      // d.description.toLowerCase().indexOf(text) !== -1 ||
      // d.relayDate.toLowerCase().indexOf(text) !== -1 ||
      // d.manager.toLowerCase().indexOf(val) !== -1 ||
      !text
  })
  if(temp.length==0){
    this.noData = true;
    this.spinner.hide()
  }
this.courseList=temp
this.spinner.hide()
}else{
    this.GetCourseEmployeeList(this.FilterParam, (result) => {
      this.courseList = result.data;
      this.FilterParam.total = result.data.length;
      if (this.courseList.length == 0) {
        this.spinner.hide()
        this.noData = true;
      } else {
        this.spinner.hide()
        this.noData = false;
      }
    });
  }
}
  }
  /**
     * OnChange Events
     */

  selectCourseForReport(course) {
    let CourseFilter = [];
    CourseFilter.push(course.id);
    this.FMReportFilterValues.Ids = CourseFilter;
    console.log("Course List ", this.FMReportFilterValues);
    this.GetReports();
  }
  /**
 * OnChange Events
 */
  // onSelect(event) {
  //   try {
  //     if (event.selected.length > 0) {
  //       this.CourseFilterList = [];
  //       this.courseNamelist = [];
  //       event.selected.forEach(element => {
  //         if (element) {
  //           this.CourseFilterList.push(element.id);
  //           this.courseNamelist.push(element.full_name)
  //         }
  //       });
  //       this.FMReportFilterValues.Ids = this.CourseFilterList;
  //       this.FMReportFilterValues.courseName = this.courseNamelist;
  //       this.FMReportFilterValues.courseTypeId = this.courseTypeId;
  //       this.FMReportFilterValues.menuId = this.menuId;
  //     } else {
  //       this.CourseFilterList = [];
  //       this.FMReportFilterValues.Ids = [];
  //       this.FMReportFilterValues.Ids = [];
  //       this.courseNamelist = [];
  //       this.FMReportFilterValues.courseName = [];
  //     }

  //     console.log(this.CourseFilterList);
  //   } catch (e) {
  //     console.log(e);
  //   }
  // }
  onSelectRow(event) {
    console.log(event);
   let allow = true;

    if(this.CourseFilterList.length > 0) {
      for(let i = 0; i< this.CourseFilterList.length; i++) {
        if(this.CourseFilterList[i] == event.id) {
          this.CourseFilterList.splice(i, 1);
          this.courseNamelist.splice(i, 1);
          allow = false;
          break;

        }
      }
    }

    if(allow == true) {
      this.CourseFilterList.push(event.id);
      if(this.menuId==161){
        this.courseNamelist.push(event.CatgeoryName);
      }else{
      this.courseNamelist.push(event.full_name);
      }

    }
    console.log("courseFilterlist",this.CourseFilterList);
    this.FMReportFilterValues.Ids = this.CourseFilterList;
    this.FMReportFilterValues.courseName = this.courseNamelist;
    this.FMReportFilterValues.courseTypeId = this.courseTypeId;
    this.FMReportFilterValues.menuId = this.menuId;

    console.log(this.CourseFilterList);
  }
  onActivate(event) {
    // console.log('Activate Event', event);
  }
  GetReports() {
    let param: any;

    this.spinner.show();
    this.FMReadyForWorking = false;
    if (this.FMReportFilterValues.Ids.length > 0) {
      if (this.FMReportFilterValues.Ids.length > this.filtertype.limit) {
        // alert("You cannot select more than " + this.filtertype.limit);
        this.presentToast('warning', 'You cannot select more than ' + this.filtertype.limit);
        this.spinner.hide();
        return null;
      } else {
        const courseIds = this.populateString(this.FMReportFilterValues.Ids);
        param = {
          courseIds: courseIds,
          catId: courseIds,
          courseTypeId: this.courseTypeId,
          iId: courseIds,
          // iName: this.courseNamelist,
        };
        if (this.menuId === 161) {
          param['iName'] =  this.courseNamelist.join(',');
        }
      }

      const reportFilters = this.FMReportFilterValues.Filters; // this.populateFMFilter(this.FMReportFilterValues.Filters);
      this.GetReportEmployeeListTimeOut(param, (result) => {
        if (!Array.isArray(result)){
          this.spinner.hide();
          this.cdf.detectChanges();
          // type: 'TimeoutError',
          // const message = "Report taking too much time, can't be Previewed. Please Schedule the report";
          this.toastr.warning(result['message'], 'Warning');
          if(result['type'] === 'TimeoutError'){
            setTimeout(() =>{
              this.Exportreport();
            });
          }

        }else if (result.length !== 0) {
          // Report['slice']['reportFilters'] = reportFilters;
          Report.dataSource.data = result;
          this.FMReport = {
            data: result,
            report: Report,
            toolbar: this.toolbar,
          };
          this.FMReadyForWorking = true;
          this.ReportFiltersSET = {
            FMReport: this.FMReport,
            FMReportList: this.FMReportList,
            FMReportName: this.ReportName,
            ReportFilter: reportFilters,
          };
          this.service.SET_Report_Filter_Values(this.ReportFiltersSET);
          // this.router.navigate(["pages/reports/report-viewer"]);
          this.spinner.hide();
          this.GoTOReportViewer();

        } else if (result.length === 0) {
          this.spinner.hide();
          this.presentToast('warning', 'No data available');
        }

      });

    } else {
      this.spinner.hide();
      this.presentToast('warning', 'Select a course');
      // alert('select a course');
    }
  }
  DownlaodReports() {
    this.spinner.show();
    this.FMReadyForWorking = false;
    if (this.FMReportFilterValues.Ids.length > 0) {
      if (this.FMReportFilterValues.Ids.length > this.filtertype.limit) {
        // alert("You cannot select more than " + this.filtertype.limit);
        this.presentToast('warning', 'You cannot select more than ' + this.filtertype.limit);
        this.spinner.hide();
        return null;
      }
      const courseIds = this.populateString(this.FMReportFilterValues.Ids);
      const param = {
        courseIds: courseIds,
        courseTypeId: this.courseTypeId,
      };
      this.GetReportEmployeeListDownload(param, (result) => {
        if (result.length !== 0) {
          this.excelservice.exportAsExcelFile(result, this.ReportName);
          this.spinner.hide();
        } else if (result.length === 0) {
          this.presentToast('warning', 'No data available');
          this.spinner.hide();
        } else {
          this.spinner.hide();
          this.presentToast('error', '');
        }
      })
    }
    else {
      this.presentToast('warning', 'Select a course');
      this.spinner.hide();
    }
  }
  clear() {
    if(this.FilterParam.searchtext){
    this.noData=false;
    this.header.searchtext = ''
    this.searchtext = this.header.searchtext;
    this.SearchFilter(null, this.searchtext);
    }
  }
  populateString(list = []) {
    if (list.length === 0) {
      return list;
    }
    return list.join(',');
  }
  GoToBack() {
    window.history.back();
  }
  GoTOReportViewer() {
    this.router.navigate(['pages/reports/report-viewer', this.menuId]);
  }
  populateString1(list = []) {
    if (list.length === 0) {
      return list;
    }
    return list.join('_');
  }
  Exportreport() {
    console.log(this.FMReportFilterValues);
    if (this.FMReportFilterValues.courseName.length > 0) {
      if (this.FMReportFilterValues.courseName.length > this.filtertype.limit) {
        this.presentToast('warning', 'You cannot select more than ' + this.filtertype.limit);
        // this.spinner.hide();
        // return null;
      } else {
        const courseName = this.populateString1(this.FMReportFilterValues.courseName);
        // this.service.courseName = courseName
        this.exports = true;
        // this.router.navigate(['pages/reports/ReportSchedule']);
      }
    } else {
      this.presentToast('warning', 'Select a course');
      this.spinner.hide();
    }
  }

  closedmodel(data) {
    console.log(data);
    this.exports = false;
    if (data.res == 2) {
      this.router.navigate(['pages/reports/ReportList']);
      // this.FMReportFilterValues = [];
    }
  }

  sendSaveBtn(val) {
    this.isdata = val;
    setTimeout(() => {
      this.isdata = '';
    }, 2000);
  }

  closeSiderBar() {
    this.exports = false;
  }

}
