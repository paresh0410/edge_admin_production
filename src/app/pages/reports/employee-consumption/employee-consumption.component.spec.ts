import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeConsumptionComponent } from './employee-consumption.component';

describe('EmployeeConsumptionComponent', () => {
  let component: EmployeeConsumptionComponent;
  let fixture: ComponentFixture<EmployeeConsumptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeConsumptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeConsumptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
