// import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';
// import { EmployeeConsumptionComponent } from './employee-consumption.component';

import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ThemeModule } from '../../../@theme/theme.module';
import { NgxPaginationModule } from 'ngx-pagination';

// @NgModule({
//   imports: [
//     CommonModule
//   ],
//   declarations: [EmployeeConsumptionComponent]
// })
@NgModule({
  imports: [
    ThemeModule,
    NgxPaginationModule,
  ],
  declarations: [
    //  CourseConsumptionReportComponent,
  ],
  schemas: [ 
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA 
  ]
})
export class EmployeeConsumptionModule { }