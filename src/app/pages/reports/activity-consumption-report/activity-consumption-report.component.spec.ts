import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityConsumptionReportComponent } from './activity-consumption-report.component';

describe('ActivityConsumptionReportComponent', () => {
  let component: ActivityConsumptionReportComponent;
  let fixture: ComponentFixture<ActivityConsumptionReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivityConsumptionReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityConsumptionReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
