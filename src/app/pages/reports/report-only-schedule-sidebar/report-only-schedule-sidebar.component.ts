import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation, OnChanges, SimpleChanges, ViewChild } from '@angular/core';
import { from } from 'rxjs';
import { ReportsService } from './../../reports/reports.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { FMToolbar, FMReportEntity, FMReportFilterEntity, FMReportFilter } from '../../../component/fm-report/fm-report';
import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';
import {NgForm} from '@angular/forms';
import { DatePipe } from '@angular/common';
export const MY_CUSTOM_FORMATS = {
  fullPickerInput: 'DD-MM-YYYY HH:mm:ss',
  parseInput: 'DD-MM-YYYY HH:mm:ss',
  datePickerInput: 'DD-MM-YYYY',
  timePickerInput: 'LT',
  monthYearLabel: 'MMM YYYY',
  dateA11yLabel: 'LL',
  monthYearA11yLabel: 'MMMM YYYY'
};
@Component({
  selector: 'ngx-report-only-schedule-sidebar',
  templateUrl: './report-only-schedule-sidebar.component.html',
  styleUrls: ['./report-only-schedule-sidebar.component.scss'],
  providers: [DatePipe,
    {provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE]},
    {provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS},]
})
export class ReportOnlyScheduleSidebarComponent implements OnInit {

  @ViewChild('reportForm') reportForm: any;

  searchtext: any;
  ReportName = 'Report Schedule';
  reportMode: any = [];
  reporttype: any = [];
  reportfrequency = '';
  chosenItem: any;
  @Input() data?: any;
  @Input() inpdata: any;
  @Input() menuId?: any;
  @Input() scheduleId = 0;
  @Output() closed = new EventEmitter<boolean>();
  @Output() clos = new EventEmitter<boolean>();

  closedmodel: any;
  sectionForm;
  report_Mode: any;
  report_type: any;
  report_days: any;
  report_freq: any;
  courseId: any;
  courseTypeId: any;
  workflowId: any;
  programId: any;
  // reporttype1: any;
  reportDownloadType: any;
  parentmenuId: any;
  stepId: any;
  batchId: any = [];
  settings = {
    text: 'Select Users',
    singleSelection: false,
    classes: 'common-multi',
    primaryKey: 'id',
    labelKey: 'firstname',
    noDataLabel: 'Search Users...',
    enableSearchFilter: true,
    searchBy: ['firstname', 'lastname', 'email'],
    lazyLoading: true,
    badgeShowLimit: 3,
    maxHeight:250,
 };

 selectedUserList = [];
 userList:any = [];
  // scheduleId: any;
  weekdays = [
    { id: '1', name: 'Sunday', checked: false },
    { id: '2', name: 'Monday', checked: false },
    { id: '3', name: 'Tuesday', checked: false },
    { id: '4', name: 'Wednesday', checked: false },
    { id: '5', name: 'Thursday', checked: false },
    { id: '6', name: 'Friday', checked: false },
    { id: '7', name: 'Saturday', checked: false }
  ];
  filterValidForm = false;
  //  Popup Filters
  FMReportFilterObj: FMReportFilter = {
    main: [],
    sub: [],
  };
  filtertype: any = {
    primary: 'Course Wise Filter',
    secondary: 'Data Wise Filter',
    limit: 5
  };
  FMReportFilterValues: any = {
    Ids: [],
    Filters: {},
    courseName: [],
    courseTypeId: '',
    menuId: '',
  };
  cacheFilters: any;
  FMReportFilterWorking: boolean = false;

  constructor(
    private ReportsService: ReportsService, private spinner: NgxSpinnerService,
    private toastr: ToastrService
  ) {
     // this.parentmenuId = this.ReportsService.menuId;
     this.parentmenuId = this.ReportsService.getmenuId();
     console.log(this.menuId);
  }

 // getscheduledropdown
 ngOnInit() {
  // this.scheduleId = this.ReportsService.scheduleId;
  // this.fetchschedule();
  console.log(this.scheduleId);
  // Old Code
  // if(this.menuId === 129){
  //   this.spinner.show();
  //   this.GetFilterInfo((result: any) => {
  //     if (result.main) {
  //       for (const obj in result.main) {
  //         this.FMReportFilterObj.main.push(result.main[obj]);
  //       }
  //       // this.FMReportFilterObj.main.push([]);
  //     }
  //     if (result.sub) {
  //       for (const obj in result.sub) {

  //         this.FMReportFilterObj.sub.push(result.sub[obj]);
  //       }
  //     }
  //     console.log('Filter Object ===>', this.FMReportFilterObj);
  //     this.FMReportFilterValues.Filters = this.populateCourseConsumptionSecondaryFilter(this.cacheFilters)
  //     this.cacheFilters = this.ReportsService.getCacheReportFilter();
  //     this.FMReportFilterWorking = true;
  //     if (this.cacheFilters && this.cacheFilters.primaryflag) {
  //       this.getSelectedFilter(this.cacheFilters);
  //     } else {
  //       this.FMReportFilterValues.Filters = this.populateCourseConsumptionSecondaryFilter(this.cacheFilters);
  //       // this.onScrollDown(0, true, this.scrolledElement.nativeElement);
  //     }
  //     this.getfdropdownlist();
  //   });
  // }else {
  //   this.filterValidForm = true;
  //   this.getfdropdownlist();
  // }

  // New Code

  if(this.menuId === 129 || this.menuId === 131 || this.menuId === 132){
    this.spinner.show();
    this.GetFilterInfo((result: any) => {
      if (result.main) {
        for (const obj in result.main) {
          this.FMReportFilterObj.main.push(result.main[obj]);
        }
        // this.FMReportFilterObj.main.push([]);
      }
      if (result.sub) {
        for (const obj in result.sub) {

          this.FMReportFilterObj.sub.push(result.sub[obj]);
        }
      }
      console.log('Filter Object ===>', this.FMReportFilterObj);
      this.FMReportFilterValues.Filters = this.populateCourseConsumptionSecondaryFilter(this.cacheFilters)
      this.cacheFilters = this.ReportsService.getCacheReportFilter();
      this.FMReportFilterWorking = true;
      if (this.cacheFilters && this.cacheFilters.primaryflag) {
        this.getSelectedFilter(this.cacheFilters);
      } else {
        this.FMReportFilterValues.Filters = this.populateCourseConsumptionSecondaryFilter(this.cacheFilters);
        // this.onScrollDown(0, true, this.scrolledElement.nativeElement);
      }
      this.getfdropdownlist();
    });
  }else {
    this.filterValidForm = true;
    this.getfdropdownlist();
  }

  // if (this.scheduleId !== 0) {
  //   //  this.fetchschedule();
  // } else {
  //   this.courseTypeId = this.data.courseTypeId;
  //   this.stepId = this.data.stepId;
  //   this.searchtext = this.data.courseName.join('_');
  //   this.courseId = this.data.Ids.join(',');
  //   if (this.data.workflowIds) {
  //     this.workflowId = this.data.workflowIds.join(',');
  //   }
  //   if (this.data.programIds) {
  //     this.programId = this.data.programIds.join(',');
  //   }
  //   if (this.data.batchIds) {
  //     this.batchId = this.data.batchIds.join(',');
  //   }
  // }
  if(this.data && this.data['courseTypeId']){
    this.courseTypeId = this.data.courseTypeId;
  }
}
// ngOnChanges() {

// }

ngOnChanges(changes: SimpleChanges): void {
  if (this.inpdata === 'onlysavenshow') {
    this.saveection(this.reportForm, 2);
  }
}

selectedTab(event) {
  console.log(event);
}
getfdropdownlist() {
  let param = {
    data: '',
  }

  this.ReportsService.getscheduledropdown().then(res => {
    console.log(res);
    this.spinner.hide();
    if (res['type'] == true) {
      try {
        this.reportMode = res['report_mode'];
        this.reporttype = res['report_type'];
        this.reportfrequency = res['report_frequency'];
        if(res['selectionCount']){
          // consol
          this.settings['limitSelection'] = parseInt(res['selectionCount'], 10);
        }
        // this.settings = res['']
        if(res['users_list'] && res['users_list'].length !=0){
          this.userList = res['users_list'];
        }
        console.log('Userlist ===>', this.userList);
        for (let i = 0; i < this.reporttype.length; i++) {
          this.reporttype[i].checked = false;
        }
        if (this.scheduleId !== 0) {
          this.fetchschedule();
        }
      } catch (err) {
        this.spinner.hide();
        this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
          timeOut: 0,
          closeButton: true
        });
        console.log(err);
      }
    }
  }, err => {
    this.spinner.hide();
    this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
      timeOut: 0,
      closeButton: true
    });
    console.log(err);
  });
}
closemodel(id) {
  let paramdata: any = {
    res: id,
  }
  this.closed.emit(paramdata);

}
close() {
  this.clos.emit(false);
}
clear() {
  this.searchtext = "";
}
onChangemode(item) {
  console.log(item);
  this.report_Mode = item.LOVValue;
  if (this.report_Mode == 1) {
    this.report_freq = '';
    this.report_days = '';
  }
  console.log(this.report_Mode);
}
onChangetype() {
  console.log(this.report_freq);
  this.report_days = '';
}
changeCheckbox(day, i) {
  if (day) {
    this.weekdays[i].checked = !this.weekdays[i].checked;
  }
}
Checkboxtype(item, i) {
  console.log(item, i);
}
showSubmittedError = false;
saveection(f, id) {
  // this.showSubmittedError = true;
  if (id == 3) {
    this.closemodel(id);
  } else {
    if (f.valid && this.filterValidForm) {
      this.makedata(id);
    } else {
      Object.keys(f.controls).forEach(key => {
        f.controls[key].markAsDirty();
      });
      this.showSubmittedError = true;
    }
  }
}
filterStatus:any;
makedata(id) {
  this.spinner.show();
  if (this.report_days && this.report_days != 'null') {
    let reportsday = this.report_days.join(',');
    if (reportsday) {
      this.report_freq = this.report_freq + '#' + reportsday;
    }
  }
  let reportsend = [];
  let reportSendEmailList = [];
  let reportString = '';
  for (let i = 0; i < this.reporttype.length; i++) {
    if (this.reporttype[i].checked == true) {
      if(this.reporttype[i].LOVValue === '3'){
        for (let index = 0; index < this.selectedUserList.length; index++) {
          reportSendEmailList.push(this.selectedUserList[index]['id']);
        }
      }
      reportsend.push(this.reporttype[i].LOVValue);
    }
  }
  if (reportsend) {
    this.reportDownloadType = reportsend.join(',');
    console.log(this.reportDownloadType);
  }
  if (reportSendEmailList.length !=0) {
    reportString = reportSendEmailList.join(',');
  }

  let params = {
    reportName: this.searchtext,
    scheduleId: this.scheduleId ? this.scheduleId : 0,
    menuId: this.menuId ? this.menuId : '',
    courseIds: this.courseId ? this.courseId : '',
    courseTypeId: this.courseTypeId ? this.courseTypeId : '',
    reportMode: this.report_Mode,
    reportFreq: this.report_freq ? this.report_freq : '',
    parentMenuId: this.parentmenuId ? this.parentmenuId : '',
    reportDownloadType: this.reportDownloadType ? this.reportDownloadType : '',
    workflowIds: this.workflowId ? this.workflowId : '',
    stepId: this.stepId ? this.stepId : '',
    programIds: this.programId ? this.programId : '',
    batchId: this.batchId ? this.batchId : '',
    userIds: reportString,
  };

    // const filtervaluekeys = Object.keys();
    if(this.filterStatus){
      for (let index = 0; index < this.filterStatus['main'].length; index++) {
        // if(filtervalue[filtervaluekeys[index]]){
        //   this.FilterParam[filtervaluekeys[index]] = filtervalue[filtervaluekeys[index]].join(',');
        // }else {
        //   this.FilterParam[filtervaluekeys[index]] = ''
        // }
        const element = this.filterStatus['main'][index];
        const replaced = element['paramName'].split(' ').join('');
        if(this.FilterParam[element['Name']]) {
          if(Array.isArray(this.FilterParam[element['Name']])){
            params[replaced] = this.FilterParam[element['Name']].join(',');
          }else{
            params[replaced] = this.FilterParam[element['Name']];
          }
        }else {
          params[replaced] = '';
        }
      }
      for (let index = 0; index < this.filterStatus['secondary'].length; index++) {
        // if(filtervalue[filtervaluekeys[index]]){
        //   this.FilterParam[filtervaluekeys[index]] = filtervalue[filtervaluekeys[index]].join(',');
        // }else {
        //   this.FilterParam[filtervaluekeys[index]] = ''
        // }
        const element = this.filterStatus['secondary'][index];
        const replaced = element['paramName'].split(' ').join('');
        if(this.FilterParam[element['Name']]) {
          if(Array.isArray(this.FilterParam[element['Name']])){
            params[replaced] = this.FilterParam[element['Name']].join(element['separator']);
          }else{
            params[replaced] = this.FilterParam[element['Name']];
          }
        }else {
          params[replaced] = '';
        }
      }
    }

  // if(this.menuId === 48 || this.menuId  === 60 || this.menuId === 47 ||this.menuId === 59){
  //   params['activityId'] = this.data.AcitivityIds.join(',');
  // }
  params['activityId'] = '';
  if(this.menuId === 107){
    params['filterId'] = this.data['filterId'];
  }
  console.log(params);
  this.ReportsService.add_Edit_schedule_report(params).then(res => {
    this.spinner.hide();
    if (res['type'] === true) {
      try {
        this.toastr.success(res['message'], 'Success', {
          closeButton: false,
        });
        if (this.scheduleId) {
          this.close();
        } else {
          this.closemodel(id);
        }
      }
      catch (err) {
        this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
          timeOut: 0,
          closeButton: true
        });
      }
    } else {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    }
  }, err => {
    this.spinner.hide();
    this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
      timeOut: 0,
      closeButton: true,
    });
  });
}

fetchschedule() {
  let param = {
    scheduleId: this.scheduleId,
  };
  this.ReportsService.fetchschedule_detail(param).then(res => {
    console.log(res);
    if (res['type'] == true) {
      this.prepareDataToBind(res['ScheduleDetail']);
    }
  }, err => {
    console.log(err);
  });
}
prepareDataToBind(data) {
  // this.spinner.show();
  if (data) {
    this.courseId = data.courseIds;
    this.searchtext = data.scheduleReportName;
    this.menuId = data.report_menu_Id;
    this.courseTypeId = data.courseTypeId;
    this.workflowId = data.workflowIds;
    this.stepId = data.stepId;
    this.programId = data.programIds;
    this.batchId = data.batchIds;
    this.report_Mode = String(data.reportMode);
    this.report_freq = String(data.reportFreq);
    if (data.reportFreqParams && data.reportFreqParams.length !== 0){
      this.report_days = data.reportFreqParams.split(',');
    }

    // let report_split = data.reportMode.split('|');
    // if (report_split.length != 0) {
    //   this.report_Mode = report_split[0];
    //   let freq = report_split[1].split('#');
    //   if (freq.length != 0) {
    //     this.report_freq = freq[0];
    //     this.report_days = freq[1].split(',');
    //   }
    // }
    if (data.reportDownloadType) {
      const reportCheckedArray = data.reportDownloadType.split(',');
      if (reportCheckedArray.length != 0) {
        for (let index = 0; index < this.reporttype.length; index++) {
          if (reportCheckedArray.indexOf(this.reporttype[index].LOVValue) > -1) {
            this.reporttype[index].checked = true;
          }
        }
      }
    }
  }
  if(data.userIds && data.userIds.length !== 0){
    const userIdList = data.userIds.split(',');
    // for (let index = 0; index < th.length; index++) {
    //   // const element = array[index];
    //   if(emailList[index] )
    // }
    const matchUserList = [];
     const IdKey = {};
     userIdList.forEach((userId)=> {
      IdKey[userId] = true;
     });

     this.userList.forEach((user,index)=> {
      if(IdKey[user.id]== true){
      matchUserList.push(user);
      }
      });
      this.selectedUserList = matchUserList;
  }
}

onUserSelect(item: any) {
  console.log(item);
  console.log(this.selectedUserList);
}
OnUserDeSelect(item: any){
console.log(item);
console.log(this.selectedUserList);
}

// New Popup Filters
FilterParam: any = {
// start: 0,
// rows: 30,
// tagIds: null,
// categoryIds: null,
// total: 0,
// courseTypeId: this.courseTypeId
// Department: '',
// Business: '',
// SDepartment: '',
// EStatus: '',
// EDate: '',
// Category: '',
// CStatus: '',
};


getSelectedFilter(event) {
console.log(event);


// const cacheFilter = {
//   primaryCache: event.primaryCache,
//   secondaryCache: event.secondaryCache
// }
// if (event.clear) {
//   this.service.setCacheReportFilter(null);
// } else {
//   this.service.setCacheReportFilter(event);
// }
if(event){
  this.filterValidForm = event.formValid;
  if (event.primaryflag) {
    this.FilterParam = this.populateCourseConsumptionFilter(event);
    this.filterStatus =  event.filterStatus;
    // console.log(filtervalue);
    // if(filtervalue){
    //   const filtervaluekeys = Object.keys(filtervalue);
    //   for (let index = 0; index < filtervaluekeys.length; index++) {
    //     if(filtervalue[filtervaluekeys[index]]) {
    //       this.FilterParam[filtervaluekeys[index]] = filtervalue[filtervaluekeys[index]];
    //     }else {
    //       this.FilterParam[filtervaluekeys[index]] = '';
    //     }
    //   }
    // }

    // this.FilterParam['Department'] = filtervalue['Department'] === undefined ? null : filtervalue.Tags;
    // this.FilterParam.categoryIds = filtervalue['Business'] === undefined ? null : filtervalue.Category;
    // this.FilterParam.workflowIds = filtervalue['SDepartment'] === undefined ? null : filtervalue.Workflow;
    // this.FilterParam.programIds = filtervalue['EStatus'] === undefined ? null : filtervalue.Program;
    // this.FilterParam.enrolDate = filtervalue['EDate'] === undefined ? [] : filtervalue['Enrol Date'];
    // this.FilterParam.rangeDate = filtervalue['Category'] === undefined ? [] : filtervalue['Course Date'];
    // this.FilterParam.rangeDate = filtervalue['CStatus'] === undefined ? [] : filtervalue['Course Date'];
    console.log(this.FilterParam);
    // this.GetCourseConsumptionList(this.FilterParam, (result) => {
    //   // this.spinner.hide();
    //   this.showSpinner = false;
    //   this.cdf.detectChanges();
    //   if (result.courseList.length === 0) {
    //     this.noData = true;
    //   } else {
    //     this.courseList = result.courseList;
    //     console.log('one',this.courseList);

    //     this.noData = false;
    //   }
    //   this.FilterParam.total = result.totalCount;
    // });
  } else {
    // this.spinner.hide();
    // this.showSpinner = false;
    // this.cdf.detectChanges();

  }
  this.FMReportFilterValues.Filters = this.populateCourseConsumptionSecondaryFilter(event);
}
}
populateCourseConsumptionFilter(obj) {
const item = {};
try {
  if (obj) {
    const primary = obj.primary;
    for (const key in primary) {
      item[key] = null;
      if (primary[key].length > 0) {
        let commastring = '';
        for (let i = 0; i < primary[key].length; i++) {
          if (String(Date.parse(primary[key][i])) === 'NaN') {
            if (commastring) {
              commastring += ',';
            }
            // commastring += primary[key][i]['id'];
            commastring += primary[key][i]['itemName'];
          } else {
            if (item[key] == null || item[key].length === 0) {
              item[key] = [];
            }
            if (primary[key][i]) {
              item[key].push(primary[key][i]);
            }
          }
        }
        if (commastring) {
          item[key] = commastring;
        }
      }
    }
  }
  return item;
} catch (e) {
  return item;
}
}
populateCourseConsumptionSecondaryFilter(obj) {

const item = {};
try {
  if (obj) {
    const secondary = obj.secondary;
    for (const key in secondary) {
      item[key] = null;
      if (secondary[key].length > 0) {
        let commastring = '';
        for (let i = 0; i < secondary[key].length; i++) {
          if (String(Date.parse(secondary[key][i])) === 'NaN') {
            if (commastring) {
              commastring += ',';
            }
            commastring += secondary[key][i]['itemName'];
          } else {
            if (item[key] == null || item[key].length === 0) {
              item[key] = [];
            }
            if (secondary[key][i]) {
              item[key].push(secondary[key][i]);
            }
          }
        }
        if (commastring) {
          item[key] = commastring;
        }
      }
    }
  }
  return item;
} catch (e) {
  return item;
}
}
GetFilterInfo(cb) {
this.FMReportFilterWorking = false;
let courseTypeId = null;
const parentMenuId =localStorage.getItem('parentMenuId');
if (parentMenuId) {
  if (Number(parentMenuId)  === 80){
    courseTypeId = 1;
  }
  if (Number(parentMenuId)  === 81) {
    courseTypeId = 4;
  }
  if (Number(parentMenuId)  === 82) {
    courseTypeId = null;
  }
}
const param = {
  courseTypeId: courseTypeId,
  menuId: this.menuId,
};
this.ReportsService.GetFilterInfo(param).then((res: any) => {
  if (res.type) {
    cb(res.data);
  } else {
    cb([]);
  }
}, err => {
  this.spinner.hide();
  console.log(err);
  cb([]);
});
}

}
