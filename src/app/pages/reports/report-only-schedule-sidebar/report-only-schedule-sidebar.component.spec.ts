import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportOnlyScheduleSidebarComponent } from './report-only-schedule-sidebar.component';

describe('ReportOnlyScheduleSidebarComponent', () => {
  let component: ReportOnlyScheduleSidebarComponent;
  let fixture: ComponentFixture<ReportOnlyScheduleSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportOnlyScheduleSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportOnlyScheduleSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
