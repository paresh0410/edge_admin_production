import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuizConsumptionReportComponent } from './quiz-consumption-report.component';

describe('QuizConsumptionReportComponent', () => {
  let component: QuizConsumptionReportComponent;
  let fixture: ComponentFixture<QuizConsumptionReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizConsumptionReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizConsumptionReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
