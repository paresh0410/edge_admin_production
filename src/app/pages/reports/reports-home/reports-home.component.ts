
import { Component, OnDestroy, OnInit, ViewEncapsulation, ChangeDetectorRef, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { NbThemeService, NbColorHelper } from '@nebular/theme';
// import { takeWhile } from 'rxjs/operators/takeWhile' ;
import { PaginatePipe, PaginationControlsDirective, PaginationService } from 'ngx-pagination';
import { Router, NavigationStart, Routes, ActivatedRoute } from '@angular/router';
import { AppService } from '../../../app.service';
import { ReportsService } from '../reports.service';
import { SuubHeader } from '../../components/models/subheader.model';

@Component({
  selector: 'ngx-reports-home',
  templateUrl: './reports-home.component.html',
  styleUrls: ['./reports-home.component.scss'],
  providers: [],
  encapsulation: ViewEncapsulation.None,
})
export class ReportsHomeComponent {
  showdata: any = [];
  header: SuubHeader;
  menuList: any = [];
  reportname: string;
  menuId: number;
  showDownloadList = false;
  constructor(private appService: AppService,
    private router: Router, private routes: ActivatedRoute,
    private service: ReportsService) {
    this.service.setCacheReportFilter(null);
    this.showdata = this.appService.getmenus();
    this.reportname = this.service.ReportTitle;
    this.header  = {
      title: '',
      btnsSearch: true,
      searchBar: false,
      dropdownlabel: '',
      drplabelshow: false,
      drpName1: '',
      drpName2: ' ',
      drpName3: '',
      drpName1show: false,
      drpName2show: false,
      drpName3show: false,
      btnName1: '',
      btnName2: '',
      btnName3: '',
      btnAdd: '',
      btnName1show: false,
      btnName2show: false,
      btnName3show: false,
      btnBackshow: true,
      btnAddshow: false,
      filter: false,
      showBreadcrumb:true,
      breadCrumbList:[ {
        'name': 'Reports',
        'navigationPath': '/pages/reports',
      },]
    };
    this.header.title=this.reportname;
    this.menuId = this.service.menuId;
    if(this.menuId==80){
    this.header.title='Online Course';
    }else if(this.menuId==81){
    this.header.title='Classroom Course';
    }else if(this.menuId==82){
      this.header.title='Workflow';
    }else if(this.menuId==83){
    this.header.title='Train The Trainer';
   }else if(this.menuId==86){
    this.header.title='Trainer Automation';
   }else if(this.menuId==152){
     this.header.title="DAM Report"
   }
    if (this.showdata) {
      for (let i = 0; i < this.showdata.length; i++) {
        if (Number(this.showdata[i].parentMenuId) === this.service.menuId) {
          // console.log(this.menuList);
          this.menuList.push(this.showdata[i]);
        }
      }
    }
    if (this.menuId == 80) {
      this.showDownloadList = true;
    }
    // this.menuList = [{"menuName":"Course Consumption", "menuRoute": "course-consumption"}];
  }
  gotopages(item, menuId) {
    this.service.setReportname(item.menuName);
    this.service.ReportTitle = item.menuName;
    localStorage.setItem('parentMenuId', item.parentMenuId);
    // this.service.menuId = menuId;
    this.router.navigate([item.menuRoute, menuId], { relativeTo: this.routes });
  }
  goToReportList(data){
    // this.service.setReportname(item.menuName);
    if(data == 'Scheduled'){
      this.service.displayScheduledList = true;
      this.service.displayReportList = false;
    }
    if(data == 'Download'){
      this.service.displayReportList = true;
      this.service.displayScheduledList = false;
    }
    this.reportname = this.service.ReportTitle;
    this.menuId = this.service.menuId;
    this.router.navigate( ['ReportList'] );
  }

  back() {
    this.router.navigate(['/pages/reports']);
  }
}
