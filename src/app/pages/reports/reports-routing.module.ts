import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { ModuleActivityContainerComponent } from './module-activity-container/module-activity-container.component';
import { ReportsComponent } from './reports.component';

import { WorkflowReportComponent } from './workflow-report/workflow-report.component';
import { ReportsHomeComponent } from './reports-home/reports-home.component';
import { TttReportsComponent } from './ttt-reports/ttt-reports.component';
import { TaReportsComponent } from './ta-reports/ta-reports.component';
import { TaEmpReportsComponent } from './ta-emp-reports/ta-emp-reports.component';
import { PreOnboardingReportComponent } from './pre-onboarding-report/pre-onboarding-report.component';
import { ReportScheduleComponent } from './report-schedule/report-schedule.component';
import { ReportListComponent } from './report-list/report-list.component';
import { ReactionHomeComponent } from './reaction-home/reaction-home.component';
import { SurveyReportComponent } from './reaction-home/survey-report/survey-report.component';
import { PollReportComponent } from './reaction-home/poll-report/poll-report.component';
import { EepReportComponent } from './eep-reports/eep-report/eep-report.component';
import { EepReportsComponent } from './eep-reports/eep-reports.component';
import { EmployeeConsumptionComponent } from './employee-consumption/employee-consumption.component';
import { CourseConsumptionReportComponent } from './course-consumption-report/course-consumption-report.component';
import { ActivityConsumptionReportComponent } from './activity-consumption-report/activity-consumption-report.component';
import { ReportViewerComponent } from './report-viewer/report-viewer.component';
import { QuizConsumptionReportComponent } from './quiz-consumption-report/quiz-consumption-report.component';
import { FeedbackConsumptionReportComponent } from './feedback-consumption-report/feedback-consumption-report.component';
import { EmployeeAttendanceComponent } from './employee-attendance/employee-attendance.component';
import { ClassroomReportComponent } from './classroom-report/classroom-report.component';

const routes: Routes = [
  { path: '', component: ReportsComponent },
  { path: 'reports_home', component: ReportsHomeComponent },
  // {
  //   path: 'reports_home/course-consumption/:menuId',
  //   component: CourseConsumptionReportComponent,
  // },
  { path: 'ReportSchedule', component: ReportScheduleComponent },
  { path: 'ReportList', component: ReportListComponent },
  { path: 'reaction_home', component: ReactionHomeComponent },
  { path: 'eep-reports', component: EepReportsComponent },
  {
    path: 'reaction_home/survey-reports/:menuId',
    component: SurveyReportComponent,
  },
  {
    path: 'reaction_home/poll-reports/:menuId',
    component: PollReportComponent,
  },
  {
    path: 'eep-reports/eep-nomination-report/:menuId',
    component: EepReportComponent,
  },
  {
    path: 'reports_home/workflow-reports/:menuId',
    component: WorkflowReportComponent,
  },
  { path: 'reports_home/ttt_reports/:menuId', component: TttReportsComponent },
  { path: 'reports_home/ta_reports/:menuId', component: TaReportsComponent },
  {
    path: 'reports_home/ta_emp_reports/:menuId',
    component: TaEmpReportsComponent,
  },
  { path: 'pre-onboarding/:menuId', component: PreOnboardingReportComponent },
  { path: 'dam-reports/:menuId', component: PreOnboardingReportComponent },
  {
    path: 'employeeconsumption/:menuId',
    component: EmployeeConsumptionComponent,
  },
  { path: 'report-viewer/:menuId', component: ReportViewerComponent },
  { path: 'reports_home/course-consumption/:menuId', component: CourseConsumptionReportComponent },
  { path: 'reports_home/activity-consumption/:menuId', component: ActivityConsumptionReportComponent },
  { path: 'reports_home/quiz-consumption/:menuId', component: QuizConsumptionReportComponent },
  { path: 'reports_home/feedback-consumption/:menuId', component: FeedbackConsumptionReportComponent },
  { path: 'reports_home/employee-attendance/:menuId', component: EmployeeAttendanceComponent },
  { path: 'reports_home/classroom-reports/:menuId', component: ClassroomReportComponent },
  { path: 'reports_home/dam-reports/:menuId', component: EmployeeConsumptionComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReportsRoutingModule {}
