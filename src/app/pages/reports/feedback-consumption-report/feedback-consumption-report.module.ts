import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ThemeModule } from '../../../@theme/theme.module';
import { NgxPaginationModule } from 'ngx-pagination';
// import { FeedbackConsumptionReportComponent } from '../feedback-consumption-report/feedback-consumption-report.component';

@NgModule({
    imports: [
      ThemeModule,
      NgxPaginationModule,
    ],
    declarations: [
      // FeedbackConsumptionReportComponent,
    ],
    schemas: [ 
      CUSTOM_ELEMENTS_SCHEMA,
      NO_ERRORS_SCHEMA 
    ]
  })
  export class FeedbackConsumptionReportModule { }
  