import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeedbackConsumptionReportComponent } from './feedback-consumption-report.component';

describe('FeedbackConsumptionReportComponent', () => {
  let component: FeedbackConsumptionReportComponent;
  let fixture: ComponentFixture<FeedbackConsumptionReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeedbackConsumptionReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeedbackConsumptionReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
