import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportScheduleSidebarComponent } from './report-schedule-sidebar.component';

describe('ReportScheduleSidebarComponent', () => {
  let component: ReportScheduleSidebarComponent;
  let fixture: ComponentFixture<ReportScheduleSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportScheduleSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportScheduleSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
