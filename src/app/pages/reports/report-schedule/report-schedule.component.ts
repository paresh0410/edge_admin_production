import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation, OnChanges } from '@angular/core';
import { from } from 'rxjs';
import { ReportsService } from './../../reports/reports.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'ngx-report-schedule',
  templateUrl: './report-schedule.component.html',
  styleUrls: ['./report-schedule.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ReportScheduleComponent implements OnInit, OnChanges {
  searchtext: any;
  ReportName = 'Report Schedule';
  reportMode: any = [];
  reporttype: any = [];
  reportfrequency = '';
  chosenItem: any;
  @Input() data?: any;
  @Input() menuId?: any;
  @Input() scheduleId = 0;
  @Input() scheduleParams = null;

  @Output() closed = new EventEmitter<boolean>();
  @Output() clos = new EventEmitter<boolean>();
  closedmodel: any;
  sectionForm;
  report_Mode: any;
  report_type: any;
  report_days: any;
  report_freq: any;
  courseId: any;
  courseTypeId: any;
  workflowId: any;
  programId: any;
  // reporttype1: any;
  reportDownloadType: any;
  parentmenuId: any;
  stepId: any;
  batchId: any = [];
  settings = {
    text: 'Select Users',
    singleSelection: false,
    classes: 'common-multi',
    primaryKey: 'id',
    labelKey: 'firstname',
    noDataLabel: 'Search Users...',
    enableSearchFilter: true,
    searchBy: ['firstname', 'lastname', 'email'],
    lazyLoading: true,
    badgeShowLimit: 3,
    maxHeight:250,
 };

 selectedUserList = [];
 userList:any = [];
  // scheduleId: any;
  weekdays = [
    { id: '1', name: 'Sunday', checked: false },
    { id: '2', name: 'Monday', checked: false },
    { id: '3', name: 'Tuesday', checked: false },
    { id: '4', name: 'Wednesday', checked: false },
    { id: '5', name: 'Thursday', checked: false },
    { id: '6', name: 'Friday', checked: false },
    { id: '7', name: 'Saturday', checked: false }
  ];
  constructor(private ReportsService: ReportsService, private spinner: NgxSpinnerService,
    private toastr: ToastrService) {
    // this.parentmenuId = this.ReportsService.menuId;
    this.parentmenuId = this.ReportsService.getmenuId();
    console.log(this.menuId);
  }
  // getscheduledropdown
  ngOnInit() {
    // this.scheduleId = this.ReportsService.scheduleId;
    // this.fetchschedule();
    console.log(this.scheduleId);
    if (this.scheduleId !== 0) {
      //  this.fetchschedule();
    } else {
      this.courseTypeId = this.data.courseTypeId;
      this.stepId = this.data.stepId;
      this.searchtext = this.data.courseName.join('_');
      this.courseId = this.data.Ids.join(',');
      if (this.data.workflowIds) {
        this.workflowId = this.data.workflowIds.join(',');
      }
      if (this.data.programIds) {
        this.programId = this.data.programIds.join(',');
      }
      if (this.data.batchIds) {
        this.batchId = this.data.batchIds.join(',');
      }
    }
    this.getfdropdownlist();
  }
  ngOnChanges() {

  }
  selectedTab(event) {
    console.log(event);
  }
  getfdropdownlist() {
    let param = {
      data: '',
    }
    this.spinner.show();
    this.ReportsService.getscheduledropdown().then(res => {
      console.log(res);
      this.spinner.hide();
      if (res['type'] == true) {
        try {
          this.reportMode = res['report_mode'];
          this.reporttype = res['report_type'];
          this.reportfrequency = res['report_frequency'];
          if(res['selectionCount']){
            // consol
            this.settings['limitSelection'] = parseInt(res['selectionCount'], 10);
          }
          // this.settings = res['']
          if(res['users_list'] && res['users_list'].length !=0){
            this.userList = res['users_list'];
          }
          console.log('Userlist ===>', this.userList);
          for (let i = 0; i < this.reporttype.length; i++) {
            this.reporttype[i].checked = false;
          }
          if (this.scheduleId !== 0) {
            this.fetchschedule();
          }
        } catch (err) {
          this.spinner.hide();
          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
          console.log(err);
        }
      }
    }, err => {
      this.spinner.hide();
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
      console.log(err);
    });
  }
  closemodel(id) {
    let paramdata: any = {
      res: id,
    }
    this.closed.emit(paramdata);

  }
  close() {
    this.clos.emit(false);
  }
  clear() {
    this.searchtext = "";
  }
  onChangemode(item) {
    console.log(item);
    this.report_Mode = item.LOVValue;
    if (this.report_Mode == 1) {
      this.report_freq = '';
      this.report_days = '';
    }
    console.log(this.report_Mode);
  }
  onChangetype() {
    console.log(this.report_freq);
    this.report_days = '';
  }
  changeCheckbox(day, i) {
    if (day) {
      this.weekdays[i].checked = !this.weekdays[i].checked;
    }
  }
  Checkboxtype(item, i) {
    console.log(item, i);
  }

  saveection(f, id) {
    if (id == 3) {
      this.closemodel(id);
    } else {
      if (f.valid) {
        this.makedata(id);
      } else {
        Object.keys(f.controls).forEach(key => {
          f.controls[key].markAsDirty();
        });
      }
    }
  }
  makedata(id) {
    // this.spinner.show();
    if (this.report_days && this.report_days != 'null') {
      let reportsday = this.report_days.join(',');
      if (reportsday) {
        this.report_freq = this.report_freq + '#' + reportsday;
      }
    }
    let reportsend = [];
    let reportSendEmailList = [];
    let reportString = '';
    for (let i = 0; i < this.reporttype.length; i++) {
      if (this.reporttype[i].checked == true) {
        if(this.reporttype[i].LOVValue === '3'){
          for (let index = 0; index < this.selectedUserList.length; index++) {
            reportSendEmailList.push(this.selectedUserList[index]['id']);
          }
        }
        reportsend.push(this.reporttype[i].LOVValue);
      }
    }
    if (reportsend) {
      this.reportDownloadType = reportsend.join(',');
      console.log(this.reportDownloadType);
    }
    if (reportSendEmailList.length !=0) {
      reportString = reportSendEmailList.join(',');
    }
    let params = {};
    if(!this.scheduleParams){
      params = {
        reportName: this.searchtext,
        scheduleId: this.scheduleId ? this.scheduleId : 0,
        menuId: this.menuId ? this.menuId : '',
        courseIds: this.courseId ? this.courseId : '',
        courseTypeId: this.courseTypeId ? this.courseTypeId : '',
        reportMode: this.report_Mode,
        reportFreq: this.report_freq ? this.report_freq : '',
        parentMenuId: this.parentmenuId ? this.parentmenuId : '',
        reportDownloadType: this.reportDownloadType ? this.reportDownloadType : '',
        workflowIds: this.workflowId ? this.workflowId : '',
        stepId: this.stepId ? this.stepId : '',
        programIds: this.programId ? this.programId : '',
        batchId: this.batchId ? this.batchId : '',
        userIds: reportString,
      };
      if(this.menuId === 48 || this.menuId  === 60 || this.menuId === 47 ||this.menuId === 59 || this.menuId === 108 || this.menuId === 111){
        params['activityId'] = this.data.AcitivityIds.join(',');
      }
      if(this.menuId === 107){
        params['filterId'] = this.data['filterId'];
      }
      if(this.data){
      if(this.data['Filters'] && this.data['secondaryScheduleParams']){
        // const paramsFilterArray = Object.keys(this.data['Filters']);
        for (let index = 0; index < this.data['secondaryScheduleParams'].length; index++) {
          // if(filtervalue[filtervaluekeys[index]]){
          //   this.FilterParam[filtervaluekeys[index]] = filtervalue[filtervaluekeys[index]].join(',');
          // }else {
          //   this.FilterParam[filtervaluekeys[index]] = ''
          // }

          // const element = paramsFilterArray[index];
          // const replaced = element.split(' ').join('');
          // if(this.data['Filters'][paramsFilterArray[index]]) {
          //   if(Array.isArray(this.data['Filters'][paramsFilterArray[index]])){
          //     params[replaced] = this.data['Filters'][paramsFilterArray[index]].join(',');
          //   }else{
          //     params[replaced] = this.data['Filters'][paramsFilterArray[index]];
          //   }
          // }else {
          //   params[replaced] = '';
          // }

          const element = this.data['secondaryScheduleParams'][index];
          const replaced = element['paramName'].split(' ').join('');
          if(this.data['Filters'][element['Name']]) {
            if(Array.isArray(this.data['Filters'][element['Name']])){
              // if(element['type'] === 'date'){
              //   params[replaced] = this.data['Filters'][element['Name']].join('|');
              // }else {
              //   params[replaced] = this.data['Filters'][element['Name']].join(',');
              // }
              params[replaced] = this.data['Filters'][element['Name']].join(element['separator']);
            }else {
              params[replaced] = this.data['Filters'][element['Name']];
            }
          }else {
            params[replaced] = '';
          }

        }
      }
    }
    }else {
      params = this.makeObjectFromString(this.scheduleParams, '|');
      console.log("report params ==>", params);
      // if(params['reportName']){
      //   params['reportName'] = this.searchtext;
      // }
      // if(params['scheduleId']){
      //   params['scheduleId'] = this.scheduleId;
      // }
       params['reportName'] = this.searchtext;
      params['scheduleId'] = this.scheduleId;
      // if(params['reportMode']){
      //   params['reportMode'] = this.report_Mode;
      // }
      params['reportMode'] = this.report_Mode;
      // if(params['reportFreq']){
      //   params['reportFreq'] = this.report_freq;
      // }
      params['reportFreq'] = this.report_freq;
      // if(params['reportDownloadType']){
      //   params['reportDownloadType'] = this.reportDownloadType ? this.reportDownloadType : '',
      // }
      params['reportDownloadType'] = this.reportDownloadType ? this.reportDownloadType : '';
      // if(reportString){
      //   params['userIds'] = reportString;
      // }
      params['userIds'] = reportString;
    }

    this.ReportsService.add_Edit_schedule_report(params).then(res => {
      this.spinner.hide();
      if (res['type'] === true) {
        try {
          this.toastr.success(res['message'], 'Success', {
            closeButton: false,
          });
          if (this.scheduleId) {
            this.close();
          } else {
            this.closemodel(id);
          }
        }
        catch (err) {
          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        }
      } else {
        this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
          timeOut: 0,
          closeButton: true
        });
      }
    }, err => {
      this.spinner.hide();
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true,
      });
    });
  }

  fetchschedule() {
    let param = {
      scheduleId: this.scheduleId,
    };
    this.ReportsService.fetchschedule_detail(param).then(res => {
      console.log(res);
      if (res['type'] == true) {
        this.prepareDataToBind(res['ScheduleDetail']);
      }
    }, err => {
      console.log(err);
    });
  }
  prepareDataToBind(data) {
    // this.spinner.show();
    if (data) {
      this.courseId = data.courseIds;
      this.searchtext = data.scheduleReportName;
      this.menuId = data.report_menu_Id;
      this.courseTypeId = data.courseTypeId;
      this.workflowId = data.workflowIds;
      this.stepId = data.stepId;
      this.programId = data.programIds;
      this.batchId = data.batchIds;
      this.report_Mode = String(data.reportMode);
      this.report_freq = String(data.reportFreq);
      if (data.reportFreqParams && data.reportFreqParams.length !== 0){
        this.report_days = data.reportFreqParams.split(',');
      }

      // let report_split = data.reportMode.split('|');
      // if (report_split.length != 0) {
      //   this.report_Mode = report_split[0];
      //   let freq = report_split[1].split('#');
      //   if (freq.length != 0) {
      //     this.report_freq = freq[0];
      //     this.report_days = freq[1].split(',');
      //   }
      // }
      if (data.reportDownloadType) {
        const reportCheckedArray = data.reportDownloadType.split(',');
        if (reportCheckedArray.length != 0) {
          for (let index = 0; index < this.reporttype.length; index++) {
            if (reportCheckedArray.indexOf(this.reporttype[index].LOVValue) > -1) {
              this.reporttype[index].checked = true;
            }
          }
        }
      }
    }
    if(data.userIds && data.userIds.length !== 0){
      const userIdList = data.userIds.split(',');
      // for (let index = 0; index < th.length; index++) {
      //   // const element = array[index];
      //   if(emailList[index] )
      // }
      const matchUserList = [];
       const IdKey = {};
       userIdList.forEach((userId)=> {
        IdKey[userId] = true;
       });

       this.userList.forEach((user,index)=> {
        if(IdKey[user.id]== true){
        matchUserList.push(user);
        }
        });
        this.selectedUserList = matchUserList;
    }
  }

  onUserSelect(item: any) {
    console.log(item);
    console.log(this.selectedUserList);
}
OnUserDeSelect(item: any){
  console.log(item);
  console.log(this.selectedUserList);
}

makeObjectFromString(string, seperator){
  const splitArray  = String(string).split(seperator);
  // console.log('splitArray object ==>', splitArray);
  const obj = {};

  for(let i= 0 ; i < splitArray.length ; i++){
    const item = splitArray[i];
    if(item ){
      const split2 = String(item).split(":");
      // console.log('split2 object ==>', split2);
      if(split2 && split2.length === 2){
        obj[split2[0]] = split2[1];
      }
    }
    }

  console.log('final object ==>', obj);
  return obj;
}
}

