import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ThemeModule } from '../../../@theme/theme.module';
import { NgxPaginationModule } from 'ngx-pagination';
// import { AddreportscheduleComponent } from './addreportschedule/addreportschedule.component';
// import { ViewreportComponent } from './viewreport/viewreport.component';
// import { QuizConsumptionReportComponent } from '../quiz-consumption-report/quiz-consumption-report.component';
import { DemoMaterialModule } from '../../material-module';
// import { MatRadioModule } from '@angular/material/radio';
@NgModule({
    imports: [
        ThemeModule,
        NgxPaginationModule,
        DemoMaterialModule,
        // MatRadioModule
    ],
    declarations: [
        // QuizConsumptionReportComponent,
        // AddreportscheduleComponent,
        // ViewreportComponent
    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA,
    ]
})
export class ReportscheduleModule { }