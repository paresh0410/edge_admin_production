import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportOnlyScheduleComponent } from './report-only-schedule.component';

describe('ReportOnlyScheduleComponent', () => {
  let component: ReportOnlyScheduleComponent;
  let fixture: ComponentFixture<ReportOnlyScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportOnlyScheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportOnlyScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
