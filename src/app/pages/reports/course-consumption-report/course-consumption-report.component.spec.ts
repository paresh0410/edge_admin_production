import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseConsumptionReportComponent } from './course-consumption-report.component';

describe('CourseConsumptionReportComponent', () => {
  let component: CourseConsumptionReportComponent;
  let fixture: ComponentFixture<CourseConsumptionReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseConsumptionReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseConsumptionReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
