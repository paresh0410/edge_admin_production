import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ThemeModule } from '../../../@theme/theme.module';
import { NgxPaginationModule } from 'ngx-pagination';
// import { CourseConsumptionReportComponent } from './course-consumption-report.component';
// import { ReportScheduleComponent } from './../report-schedule/report-schedule.component';
@NgModule({
  imports: [
    ThemeModule,
    NgxPaginationModule,
  ],
  declarations: [
    //  CourseConsumptionReportComponent,
    // ReportScheduleComponent
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class CourseConsumptionReportModule { }
