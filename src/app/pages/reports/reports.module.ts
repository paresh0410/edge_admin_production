import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { CommonModule } from '@angular/common';
import { ReportsComponent } from './reports.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { FilterPipeModule  } from 'ngx-filter-pipe';
import { DemoMaterialModule } from '../material-module';
// import { FmReportComponent } from '../../component/fm-report/fm-report.component';
import { OrderModule  } from 'ngx-order-pipe';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { ReportsService } from './reports.service';

import { SideMenuFilterComponent } from '../../component/side-menu-filter/side-menu-filter.component';
// import { ReportViewerModule } from '../reports/report-viewer/report-viewer.module';
// import { PreOnboardingReport } from '../reports/pre-onboarding-report/pre-onboarding-report.module';
import { PreOnboardingReportComponent } from '../reports/pre-onboarding-report/pre-onboarding-report.component';
import { CourseConsumptionReportComponent } from './course-consumption-report/course-consumption-report.component';
import { ActivityConsumptionReportComponent } from './activity-consumption-report/activity-consumption-report.component';
import { QuizConsumptionReportComponent } from './quiz-consumption-report/quiz-consumption-report.component';
import { FeedbackConsumptionReportComponent } from './feedback-consumption-report/feedback-consumption-report.component';
import { EmployeeAttendanceComponent } from './employee-attendance/employee-attendance.component';
import { ClassroomReportComponent } from './classroom-report/classroom-report.component';
import { WorkflowReportComponent } from './workflow-report/workflow-report.component';
import { ReportsHomeComponent } from './reports-home/reports-home.component';
import { TttReportsComponent } from './ttt-reports/ttt-reports.component';
import { TaReportsComponent } from './ta-reports/ta-reports.component';
import { TaEmpReportsComponent } from './ta-emp-reports/ta-emp-reports.component';
import {EmployeeConsumptionComponent} from './employee-consumption/employee-consumption.component';
import { ReportScheduleComponent } from './report-schedule/report-schedule.component';
import { ReportListComponent } from './report-list/report-list.component';
import { PassService } from '../../service/passService';
import { SharedModule } from './shared.modules';
import { ComponentModule } from '../../component/component.module';
import { ReactionHomeComponent } from './reaction-home/reaction-home.component';
import { SurveyReportComponent } from './reaction-home/survey-report/survey-report.component';
import { PollReportComponent } from './reaction-home/poll-report/poll-report.component';
import { ReportViewerComponent } from './report-viewer/report-viewer.component';
import { ReportOnlyScheduleComponent } from './report-only-schedule/report-only-schedule.component';
import { PopupDataFilterComponent } from './popup-data-filter/popup-data-filter.component';
// import { EepReportComponent } from './eep-reports/eep-report/eep-report.component';
import { EepReportsComponent } from './eep-reports/eep-reports.component';
import { EepReportComponent } from './eep-reports/eep-report/eep-report.component';
import { ReportScheduleSidebarComponent } from './report-schedule-sidebar/report-schedule-sidebar.component';
import { ReportOnlyScheduleSidebarComponent } from './report-only-schedule-sidebar/report-only-schedule-sidebar.component';
// import { PreOnboardingReportComponent } from './pre-onboarding-report/pre-onboarding-report.component';
// import { ReportViewerComponent } from '../report-viewer.component';
import { ReportsRoutingModule } from './reports-routing.module';

@NgModule({
  imports: [
    ThemeModule,
    CommonModule,
    NgxPaginationModule,
    FilterPipeModule,
    AngularMultiSelectModule,
    FormsModule,
    ReactiveFormsModule,
    OrderModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    NgxDatatableModule,
    // ReportViewerModule,
    // PreOnboardingReportComponent,
    DemoMaterialModule,
    SharedModule,
    ComponentModule,
    ReportsRoutingModule,
  ],
  declarations: [
    ReportsComponent,
    ReportViewerComponent,
    SideMenuFilterComponent,
    ActivityConsumptionReportComponent,
    CourseConsumptionReportComponent,
    QuizConsumptionReportComponent,
    FeedbackConsumptionReportComponent,
    EmployeeAttendanceComponent,
    ClassroomReportComponent,
    WorkflowReportComponent,
    ReportsHomeComponent,
    TttReportsComponent,
    TaReportsComponent,
    TaEmpReportsComponent,
    EmployeeConsumptionComponent,
    ReportScheduleComponent,
    ReportListComponent,
    PreOnboardingReportComponent,
    ReactionHomeComponent,
    SurveyReportComponent,
    PollReportComponent,
    PopupDataFilterComponent,
    ReportOnlyScheduleComponent,
    EepReportComponent,
    EepReportsComponent,
    ReportScheduleSidebarComponent,
    ReportOnlyScheduleSidebarComponent,

    // PreOnboardingReportComponent
  ],
  providers: [ReportsService, PassService],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA ,
  ],
  exports: [
    AngularMultiSelectModule,
    SideMenuFilterComponent,
    FilterPipeModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    // ReportViewerModule,
    PreOnboardingReportComponent,
    SharedModule,
    ReportScheduleComponent,
  ]
})
export class ReportsModule { }
