import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FmReportComponent } from '../../component/fm-report/fm-report.component';
import { ReportScheduleComponent } from './report-schedule/report-schedule.component';


@NgModule({
imports: [
    CommonModule,

],
declarations: [
    FmReportComponent,
    // ReportScheduleComponent
],
exports: [
    FmReportComponent,
    // ReportScheduleComponent
]
})
export class SharedModule {}
