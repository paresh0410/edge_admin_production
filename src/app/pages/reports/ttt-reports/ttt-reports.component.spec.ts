import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TttReportsComponent } from './ttt-reports.component';

describe('TttReportsComponent', () => {
  let component: TttReportsComponent;
  let fixture: ComponentFixture<TttReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TttReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TttReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
