import { Component, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from '../../app.service';
import { ReportsService } from './reports.service';
import { SuubHeader } from '../components/models/subheader.model';
@Component({
  selector: 'ngx-reports',
  styleUrls: ['./reports.component.scss'],
  templateUrl: './reports.component.html',
  providers: [],
  encapsulation: ViewEncapsulation.None
})
export class ReportsComponent {
  showdata: any = [];
  menuList: any = [];
  header: SuubHeader  = {
    title:'Reports',
    showBreadcrumb:true,
    breadCrumbList:[]
  };
  menuName: any;
  cardsCount: boolean = false;
  constructor(private appService: AppService, private router: Router, private routes: ActivatedRoute, private service: ReportsService) {
    this.showdata = this.appService.getmenus();
    this.service.setCacheReportFilter(null);
    if (this.showdata) {
      console.log(this.showdata);
      for (let i = 0; i < this.showdata.length; i++) {
        if (Number(this.showdata[i].parentMenuId) === 43) {
          // console.log(this.menuList);
          this.menuList.push(this.showdata[i]);
        }
      }
    }
    // this.menuList = [{"menuName":"Course Consumption", "menuRoute": "course-consumption"}];
  }
  
  gotopages(item, menuId) {
    console.log(item, ' + ', menuId);
    this.service.ReportTitle = item.menuName;
    // this.service.setReportname(item.menuName);
    this.service.menuId = menuId;
    this.service.setmenuId(menuId);
    if (menuId === 106 || menuId === 107 || menuId === 115) {
      this.router.navigate([item.menuRoute, menuId], { relativeTo: this.routes });
    } else {
      this.router.navigate([item.menuRoute], { relativeTo: this.routes });
    }
  }

  gotoCard(item) {
    console.log(item, ' + ', item.menuId);
    this.service.ReportTitle = item.menuName;
    // this.service.setReportname(item.menuName);
    this.service.menuId = item.menuId;
    this.service.setmenuId(item.menuId);
    if (item.menuId === 106 || item.menuId === 107 || item.menuId === 115 ) {
      this.router.navigate([item.menuRoute, item.menuId], { relativeTo: this.routes });
    } else {
      this.router.navigate([item.menuRoute], { relativeTo: this.routes });
    }
  }
}
