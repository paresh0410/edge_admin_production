import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaEmpReportsComponent } from './ta-emp-reports.component';

describe('TaEmpReportsComponent', () => {
  let component: TaEmpReportsComponent;
  let fixture: ComponentFixture<TaEmpReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaEmpReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaEmpReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
