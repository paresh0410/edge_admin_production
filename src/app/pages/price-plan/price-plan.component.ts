import { Component, OnInit, ChangeDetectorRef  } from '@angular/core';
import { CommonFunctionsService } from '../../service/common-functions.service';
import { webApi } from '../../service/webApi';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { noData } from '../../models/no-data.model';
@Component({
  selector: 'ngx-price-plan',
  templateUrl: './price-plan.component.html',
  styleUrls: ['./price-plan.component.scss']
})
export class PricePlanComponent implements OnInit {

  priceList = [];
  noPlansFound = false;
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No plans available at this time.",
    desc:"",
    titleShow:true,
    btnShow:false,
    descShow:false,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/how-to-add-an-asset-in-the-dam',
  };
  constructor(
    private router: Router,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private activatedRoute: ActivatedRoute,
    private commonFunctionsService: CommonFunctionsService,
    private cdf: ChangeDetectorRef,
  ) {
    var id = document.getElementById('nb-global-spinner');

    id.style.display = 'none';
   }

  ngOnInit() {
    this.fetchPlansList();
  }

  fetchPlansList() {
    const getPlansList =
      webApi.domain + webApi.url.getPlansList;
    const param = {};
    this.spinner.show();
    this.commonFunctionsService
      .httpPostRequest(getPlansList, param)
      .then(
        (rescompData) => {
          this.spinner.hide();
          console.log('rescompData ==>', rescompData);
          if (rescompData && rescompData['type'] === true && rescompData['plans'] && rescompData['plans'].length != 0) {
            // this.currencyData = rescompData['currency'];
            this.priceList = rescompData['plans'];
            for(let i = 0 ; i < this.priceList.length ; i ++){
              this.priceList[i]['featureList'] = [];
              if(this.priceList[i]['fnames'] && this.priceList[i]['fnames'] !== ''){
                this.priceList[i]['featureList'] = this.priceList[i]['fnames'].split(',');
              }
            }
          }else {
              this.noPlansFound = true;
          }
        },
        (error) => {
          this.spinner.hide();
          this.noPlansFound = true;
          this.toastr.warning('Unable to fetch Plans', 'Warning');
        }
      );
  }

  goToSignUp(plan){
    this.router.navigate(['/signUp',plan.id]);
  }

  goToLogin(){
    this.router.navigate(['/login']);
  }
}
