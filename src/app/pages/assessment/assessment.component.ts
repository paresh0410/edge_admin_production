import {Component, OnDestroy, ViewEncapsulation} from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators/takeWhile' ;
import { Router,ActivatedRoute} from '@angular/router';
import { AppService } from '../../app.service';
import { SuubHeader } from '../components/models/subheader.model';

interface CardSettings {
  title: string;
  iconClass: string;
  type: string;
}

@Component({
  selector: 'ngx-assessment',
  styleUrls: ['./assessment.component.scss'],
  templateUrl: './assessment.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class assessmentComponent implements OnDestroy {
  header: SuubHeader  = {
    title: 'Assessment',
    showBreadcrumb:true,
    breadCrumbList:[]
  };


  private alive = true;

  lightCard: CardSettings = {
    title: 'Light',
    iconClass: 'nb-lightbulb',
    type: 'primary',
  };
  rollerShadesCard: CardSettings = {
    title: 'Roller Shades',
    iconClass: 'nb-roller-shades',
    type: 'success',
  };
  wirelessAudioCard: CardSettings = {
    title: 'Wireless Audio',
    iconClass: 'nb-audio',
    type: 'info',
  };
  coffeeMakerCard: CardSettings = {
    title: 'Coffee Maker',
    iconClass: 'nb-coffee-maker',
    type: 'warning',
  };

  statusCards: string;

  commonStatusCardsSet: CardSettings[] = [
    this.lightCard,
    this.rollerShadesCard,
    this.wirelessAudioCard,
    this.coffeeMakerCard,
  ];

  statusCardsByThemes: {
    default: CardSettings[];
    cosmic: CardSettings[];
    corporate: CardSettings[];
  } = {
    default: this.commonStatusCardsSet,
    cosmic: this.commonStatusCardsSet,
    corporate: [
      {
        ...this.lightCard,
        type: 'warning',
      },
      {
        ...this.rollerShadesCard,
        type: 'primary',
      },
      {
        ...this.wirelessAudioCard,
        type: 'danger',
      },
      {
        ...this.coffeeMakerCard,
        type: 'secondary',
      },
    ],
  };
   showdata:any=[];
      employees:boolean=false;
      users:boolean=false;
      courses:boolean=false;
      evaluation:boolean=false;
       competancy:boolean=false;
      courseBundle:boolean=false;
      roleManagement:boolean=false;
      learnData:any = [];
  constructor(private themeService: NbThemeService,public router:Router,public routes:ActivatedRoute,private AppService:AppService) {
    this.themeService.getJsTheme()
      .pipe(takeWhile(() => this.alive))
      .subscribe(theme => {
        this.statusCards = this.statusCardsByThemes[theme.name];
    });
       this.showdata =this.AppService.getmenus();
       if(this.showdata)
       {
         for (let i = 0; i < this.showdata.length; i++)
         {
           if (Number(this.showdata[i].parentMenuId) === 3)
           {
             this.learnData.push(this.showdata[i]);
           }
         }
       }
      // if(this.showdata)
      // {
      //   for(let i=0;i<this.showdata.length;i++)
      //   {
      //     if(this.showdata[i] == 'MEMP')
      //     {
      //       this.employees =true;
      //     }
      //      if(this.showdata[i] == 'MU')
      //     {
      //       this.users =true;
      //     }
      //      if(this.showdata[i] == 'rec')
      //     {
      //       this.courses =true;
      //     }
      //      if(this.showdata[i] == 'training')
      //     {
      //       this.evaluation =true;
      //     }
      //        if(this.showdata[i] == 'rules')
      //     {
      //       this.competancy =true;
      //     }
      //      if(this.showdata[i] == 'MCB')
      //     {
      //       this.courseBundle =true;
      //     }
      //      if(this.showdata[i] == 'training')
      //     {
      //       this.roleManagement =true;
      //     }
      //   }
      // }
  }

  ngOnDestroy() {
    this.alive = false;
  }

  gotoqcategory(){
    this.router.navigate(['qcategory'],{relativeTo:this.routes});
  }

  gotopages(url){
    this.router.navigate([url],{relativeTo:this.routes});
  }
  gotocertificatess(){
    // this.router.navigate(['certificates'],{relativeTo:this.routes});
  }

  gotoquiz(){
    this.router.navigate(['quiz'],{relativeTo:this.routes});
  }

  gotofeedback(){
     this.router.navigate(['feedback'],{relativeTo:this.routes});
  }

  gotosurvey(){
    this.router.navigate(['survey'],{relativeTo:this.routes});
  }
   gotopole(){
    this.router.navigate(['pole'],{relativeTo:this.routes});
  }

  
}
