import { Host, ChangeDetectionStrategy, Component, ChangeDetectorRef, ViewEncapsulation, Directive, forwardRef, Attribute, OnChanges, SimpleChanges, Input, ViewChild, ViewContainerRef, OnInit } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
// import { survey } from '../survey';
// import { surveyService } from '../survey.service';
import { AddeditsurveyService } from '../addEditsurvey.service';
import { NgxSpinnerService } from 'ngx-spinner';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { engageService } from './engage.service';
import { NotificationtemplateServiceService } from '../../../../plan/notification-templates/notification-templates.service'
// import { AddEditsurveyService } from '../../../courses/addEditCourseContent/addEditCourseContent.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
// import { DataSepeartor } from '../../../../../service/data-seperator.enum';
import { DataSeparatorService } from '../../../../../service/data-separator.service';
import { noData } from '../../../../../models/no-data.model';
import { endOfYear } from 'date-fns';
import { webApi } from '../../../../../service/webApi';
import { CommonFunctionsService } from '../../../../../service/common-functions.service';

@Component({
	selector: 'course-engage',
	templateUrl: './engage.html',
	styleUrls: ['./engage.scss'],
	encapsulation: ViewEncapsulation.None
})

export class engageComponent {

	@ViewChild('rulesTable') rulesTable: any;
	@ViewChild(DatatableComponent) tablenoti: DatatableComponent;

	@Input() inpdata: any;
	@Input() searchData: any;

	noDataVal:noData={
		margin:'mt-3',
		imageSrc: '../../../../../assets/images/no-data-bg.svg',
		title:"No Notifications added under survey.",
		desc:"",
		titleShow:true,
		btnShow:true,
		descShow:false,
		btnText:'Learn More',
		btnLink:'https://faq.edgelearning.co.in/kb/reaction-how-to-add-notification-for-a-survey',
	  }
	notificationLabel: any = [
		{ labelname: 'Name', bindingProperty: 'rulename', componentType: 'text' },
		{ labelname: 'Description', bindingProperty: 'description', componentType: 'text' },
		{ labelname: 'Modes', bindingProperty: 'cmodes', componentType: 'text' },
		{ labelname: 'Events', bindingProperty: 'notEvent', componentType: 'text' },
		{ labelname: 'Visibility', bindingProperty: 'btntext', componentType: 'button' },
		{ labelname: 'EDIT', bindingProperty: 'tenantId', componentType: 'icon' },

	]
	// rows = [
	//   { name: 'Dailly Engagement', description: 'This is dailly engagement ', method: 'SMS ,Email,Notification' , modes : 'event' , details : 'on course enrolement'},
	//   { name: 'Monthly Engagement ', description: 'This is monthly engagement', method: 'SMS ,Email'  , modes : 'date' , details : '12/12/2019'},
	//   { name: 'Quaterly Engagement', description: 'This is quaterly engagement', method: 'Email,Notification'  , modes : 'event' , details : 'on course compleation'},
	//   { name: 'halfyearly Engagement', description: 'This is halfyearly engagement', method: 'SMS,Notification'  , modes : 'event' , details : 'on course enrolement'}
	// ];
	// columns = [
	//   { prop: 'name', name: 'Name' },
	//   { prop: 'description', name: 'Description' },
	//   { prop: 'method', name: 'Method'},
	//   { prop: 'modes', name: 'Modes'},
	//   { prop: 'details', name: 'Details'}

	// ];
	rows: any = [];
	columns: any = [];
	engageData = []
	config = {
		height: '200px',
		uploader: {
			insertImageAsBase64URI: true,
		},
		allowResizeX: false,
		allowResizeY: false,
		placeholder: 'Enter minimum 2 maximum 1000 characters',
		limitChars: 1000,
	};
	tagsArray = [];
	init = false;
	selected = [];
	addEngagePopup: boolean = false;
	selectMethod: any;
	showdate: boolean = false;
	showdays: boolean = false;
	enterDays: any;
	SurveyNotification: any = {
		name: '',
		desc: '',
		event: '',
		lnmsg: false,
		lnmsgtemp: '',
		reviewTextSms: false,
		editSmsTemplateData: '',
		lnnot: false,
		lnnottemp: '',
		reviewTextNot: false,
		editNotTemplateData: '',
		lnemail: false,
		lnemailTemp: '',
		reviewTextEmail: false,
		emailSubjectData: '',
		editEmailTemplateData: '',
		smsSubjectData: '',
		NotSubjectData: '',
	};
	addFlag: boolean = false;
	searchText: any;
	searchvalue: any = {
		value: '',
	  };
	notiData: any;
	nodata: boolean=true;
	visibleDropDownData: any;
	selectedNotiId: number = 1;
	visibiltyRes: any;
	notiRuleID: any;
	onselect({ selected }) {
		console.log('select event', selected, this.selected);
		this.selected.splice(0, this.selected.length);
		this.selected.push(...selected);
	}
	userId: any;
	surveyId: any;
	eventsDrop: any = [];
	templateDrop: any = [];
	userData;
	
	notiTitle:string="Add Notifications"
	btnName: string = 'Save';
	action: any;
	constructor(private addEditSurveyService: AddeditsurveyService, private spinner: NgxSpinnerService,
    // private toasterService: ToasterService,
    private engageservice: engageService, public cdf: ChangeDetectorRef,
    private notitempservice: NotificationtemplateServiceService,
    private router: Router, private toastr: ToastrService,
	private commonFunctionService:CommonFunctionsService,
    private http1: HttpClient,
    private dataSeparatorService: DataSeparatorService,
	) {
		this.getHelpContent();
		this.engageData = this.rows;
		if (localStorage.getItem('LoginResData')) {
			this.userData = JSON.parse(localStorage.getItem('LoginResData'));
			console.log('userData', this.userData.data);
			this.userId = this.userData.data.data.id;
			console.log('userId', this.userData.data.data.id);
		}
		if (localStorage.getItem('Notification')) {
			this.notiData = JSON.parse(localStorage.getItem('Notification'));
			console.log('userData', this.notiData);
		  }
		//this.spinner.show();
		this.fetchNotMethodsDropdown();
		this.fetcheventsdropdown();
		this.fetchtemplatedropdown();
		if (this.addEditSurveyService.surveyId) {
			this.surveyId = this.addEditSurveyService.surveyId;
			console.log('Survey Id new for notification:', this.surveyId);
		}


		this.action = this.addEditSurveyService.getDataforAddedit[0];
		console.log('actionOnnotification:', this.action);
		// if (this.action == "EDIT") {
			this.getExistingSurveyNotification();
		// } else {
		// 	this.nodata = true;
		// }

	}

	ngOnChanges(changes: SimpleChanges): void {
		if(this.inpdata === 'notif') {
		  this.addNotification();
		}

		if(this.searchData) {
			this.searchnotification(this.searchData);
		}
	}

	presentToast(type, body) {
		if (type === 'success') {
			this.toastr.success(body, 'Success', {
				closeButton: false
			});
		} else if (type === 'error') {
			this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
				timeOut: 0,
				closeButton: true
			});
		} else {
			this.toastr.warning(body, 'Warning', {
				closeButton: false
			})
		}
	}
	getExistingSurveyNotification() {
		this.spinner.show()
		let param = {
			"surveyId": this.surveyId,
			"tenantId": this.userData.data.data.tenantId,
		}

		this.engageservice.getSurveyNotification(param)
			.then(rescompData => {
				this.spinner.hide();
				var result = rescompData;
				// var SurveyNoti = result.data[0];
				this.SurveyNoti = result['data'][0];
				this.visibleDropDownData=result['data'][1];
				if (result['type'] == true) {
					console.log('ExistingSurveyNotifications:', this.SurveyNoti);
					this.rows = this.SurveyNoti;
					this.rows = [...this.rows];
					

					for(var i=0;i<this.rows.length;i++){
						if(this.rows[i]['visible']==1){
							this.rows[i].btntext="fa fa-eye"
						}else{
							this.rows[i].btntext="fa fa-eye-slash"
						}

					}
					if(this.rows.length==0){
						this.nodata=true;
					}else{
						this.nodata=false;
					}
					console.log('ExistingSurveyNotifications', this.rows);

					this.columns = [
						{ prop: 'rulename', name: 'Name' },
						{ prop: 'description', name: 'Description' },
						{ prop: 'visible', name: 'Visibility'},
						{ prop: 'cmodes', name: 'Modes' },
						{ prop: 'notEvent', name: 'Events' }

					];
					this.cdf.detectChanges();
				} else {
					// var toast: Toast = {
					// 	type: 'error',
					// 	//title: "Server Error!",
					// 	body: "Something went wrong.please try again later.",
					// 	showCloseButton: true,
					// 	timeout: 2000
					// };
					// this.toasterService.pop(toast);
					this.presentToast('error', '');
				}


			}, error => {
				this.spinner.hide();
				// var toast: Toast = {
				// 	type: 'error',
				// 	//title: "Server Error!",
				// 	body: "Something went wrong.please try again later.",
				// 	showCloseButton: true,
				// 	timeout: 2000
				// };
				// this.toasterService.pop(toast);
				this.presentToast('error', '');
			});
	}

	onSelect(data) {
		// if (data.type == "click") {
			this.addFlag = false;
			this.notiTitle='Edit Notifications'
			console.log('rowData', data);
			// var editData = data.row;
			var editData = data;


			this.readyDataforEdit(editData)
			this.addEngagePopup = true;
			this.SurveyNotification = {
				name: editData.rulename,
				desc: editData.description,
				event: editData.notEventId,
				lnmsg: false,
				lnmsgtemp: '',
				reviewTextSms: false,
				editSmsTemplateData: '',
				lnnot: false,
				lnnottemp: '',
				reviewTextNot: false,
				editNotTemplateData: '',
				lnemail: false,
				lnemailTemp: '',
				reviewTextEmail: false,
				emailSubjectData: '',
				editEmailTemplateData: '',
				smsSubjectData: '',
				NotSubjectData: '',
				visible:data.visible
			 }
			this.selectMethod = 2;
			// this.onChange2(data.row.notEventId)
			this.onChange2(data.notEventId)

			for (let i = 0; i < this.modeArr.length; i++) {
				if (this.modeArr[i] == "1" && this.templateArr[i] != null) {
          this.SurveyNotification.lnmsg = true;
          // this.smsMinText = 20;
          // this.smsMaxText = 144;
          this.smsValidCheck(true);
					this.SurveyNotification.reviewTextSms = true;
					this.SurveyNotification.lnmsgtemp = this.templateArr[i];
					this.SurveyNotification.editSmsTemplateData = this.templateNameArr[i];
					this.SurveyNotification.smsSubjectData = this.templateSubjectArr[i];
				}

				if (this.modeArr[i] == "2" && this.templateArr[i] != null) {
					this.SurveyNotification.lnemail = true;
					this.SurveyNotification.reviewTextEmail = true;
					this.SurveyNotification.lnemailTemp = this.templateArr[i];
					this.SurveyNotification.editEmailTemplateData = this.templateNameArr[i];
					this.SurveyNotification.emailSubjectData = this.templateSubjectArr[i];
				}

				if (this.modeArr[i] == "3" && this.templateArr[i] != null) {
					this.SurveyNotification.lnnot = true;
					this.SurveyNotification.reviewTextNot = true;
					this.SurveyNotification.lnnottemp = this.templateArr[i];
					this.SurveyNotification.editNotTemplateData = this.templateNameArr[i];
					this.SurveyNotification.NotSubjectData = this.templateSubjectArr[i];
				}
			}

		// }
	}

	modeArr: any = [];
	templateArr: any = [];
	templateNameArr: any = [];
	templateSubjectArr: any = [];
	// readyDataforEdit(editData) {
	// 	if (editData.modeIds) {
	// 		this.modeArr = editData.modeIds.split("@edge@");
	// 	}
	// 	if (editData.templateIds) {
	// 		this.templateArr = editData.templateIds.split("@edge@");
	// 	}
	// 	if (editData.templates) {
	// 		this.templateNameArr = editData.templates.split("@edge@");
	// 	}
	// 	if (editData.subjects) {
	// 		this.templateSubjectArr = editData.subjects.split("@edge@");
	// 	}
	// 	console.log('this.modeArr', this.modeArr);
	// 	console.log('this.templateArr', this.templateArr);
	// 	console.log('this.templateNameArr', this.templateNameArr);
	// 	console.log('this.templateSubjectArr', this.templateSubjectArr);
	// }
  readyDataforEdit(editData) {
    if (editData.modeIds) {
      this.modeArr = editData.modeIds.split(this.dataSeparatorService.Hash);
    }
    if (editData.templateIds) {
      this.templateArr = editData.templateIds.split(this.dataSeparatorService.Hash);
    }
    if (editData.templates) {
      this.templateNameArr = editData.templates.split(this.dataSeparatorService.Hash);
    }
    if (editData.subjects) {
      this.templateSubjectArr = editData.subjects.split(this.dataSeparatorService.Hash);
    }

    console.log('this.modeArr', this.modeArr);
    console.log('this.templateArr', this.templateArr);
    console.log('this.templateNameArr', this.templateNameArr);
    console.log('this.templateSubjectArr', this.templateSubjectArr);
  }
	methodsDrop: any = [];
	fetchNotMethodsDropdown() {
		let param = {
			"tenantId": this.userData.data.data.tenantId,
		}
		this.engageservice.getNotMethodsDropdown(param)
			.then(rescompData => {

				var result = rescompData;
				var res = result['data'][0];
				if (result['type'] == true) {
					console.log('EventMethodDropdown:', rescompData)
					this.methodsDrop = res;
				} else {
					// this.loader = false;
					// var toast: Toast = {
					// 	type: 'error',
					// 	//title: "Server Error!",
					// 	body: "Something went wrong.please try again later.",
					// 	showCloseButton: true,
					// 	timeout: 2000
					// };
					// this.toasterService.pop(toast);
					this.presentToast('error', '');
				}


			}, error => {
				//this.loader = false;
				// var toast: Toast = {
				// 	type: 'error',
				// 	//title: "Server Error!",
				// 	body: "Something went wrong.please try again later.",
				// 	showCloseButton: true,
				// 	timeout: 2000
				// };
				// this.toasterService.pop(toast);
				this.presentToast('error', '');
			});
	}

	fetcheventsdropdown() {
		let param = {
			"tId": this.userData.data.data.tenantId,
			"aId": 14
		}
		this.engageservice.getNotEventDropdown(param)
			.then(rescompData => {

				var result = rescompData;
				var res = result['data'][0];
				if (result['type'] == true) {
					console.log('EventNotDropdown:', rescompData)
					this.eventsDrop = res;
				} else {
					// this.loader = false;
					// var toast: Toast = {
					// 	type: 'error',
					// 	//title: "Server Error!",
					// 	body: "Something went wrong.please try again later.",
					// 	showCloseButton: true,
					// 	timeout: 2000
					// };
					// this.toasterService.pop(toast);
					this.presentToast('error', '');
				}


			}, error => {
				//this.loader = false;
				// var toast: Toast = {
				// 	type: 'error',
				// 	//title: "Server Error!",
				// 	body: "Something went wrong.please try again later.",
				// 	showCloseButton: true,
				// 	timeout: 2000
				// };
				// this.toasterService.pop(toast);
				this.presentToast('error', '');
			});
	}

	fetchtemplatedropdown() {
		let param = {
			//"nEventId": '1,2,3,4',
			"tid": this.userData.data.data.tenantId,
			"aId": 14
		}
		this.engageservice.getNottemplateDropdown(param)
			.then(rescompData => {

				var result = rescompData;
				var temp = result['data'][0];
				if (result['type'] == true) {
					console.log('TemplateNotDropdown:', rescompData)
					this.templateDrop = temp;
				} else {
					// this.loader = false;
					// var toast: Toast = {
					// 	type: 'error',
					// 	//title: "Server Error!",
					// 	body: "Something went wrong.please try again later.",
					// 	showCloseButton: true,
					// 	timeout: 2000
					// };
					// this.toasterService.pop(toast);
					this.presentToast('error', '');
				}


			}, error => {
				//this.loader = false;
				// var toast: Toast = {
				// 	type: 'error',
				// 	//title: "Server Error!",
				// 	body: "Something went wrong.please try again later.",
				// 	showCloseButton: true,
				// 	timeout: 2000
				// };
				// this.toasterService.pop(toast);
				this.presentToast('error', '');
			});
	}

	addNotification() {
		this.addEngagePopup = true;
		this.init = false;
		this.SurveyNotification = {
			name: '',
			desc: '',
			event: '',
			lnmsg: false,
			lnmsgtemp: '',
			reviewTextSms: false,
			editSmsTemplateData: '',
			lnnot: false,
			lnnottemp: '',
			reviewTextNot: false,
			editNotTemplateData: '',
			lnemail: false,
			lnemailTemp: '',
			reviewTextEmail: false,
			emailSubjectData: '',
			editEmailTemplateData: '',
			smsSubjectData: '',
			NotSubjectData: '',
			visible:1
		};
		this.addFlag = true;
		this.notiTitle="Add Notification"
	}

	closeEngageModal() {
		this.addEngagePopup = false;
		this.rulename='';
	}
	visibilityTableRow(row) {
		this.addEngagePopup = false;
		let value;
		let status;
		let msg;
		if (row.visible == 1) {
		  row.btntext = 'fa fa-eye-slash';
		  value = 'fa fa-eye-slash';
		  row.visible = 0
		  msg = "Disabled Successfully"
		  status = 0;
		} else {
		  status = 1;
		  value = 'fa fa-eye';
		  row.visible = 1;
		  msg = "Enabled Successfully"
		  row.btntext = 'fa fa-eye';
		}
		if(row.notRuleId){
			var notiRuleID = row.notRuleId.split(this.dataSeparatorService.Hash);
			this.notiRuleID=notiRuleID.toString();	
			}
		this.spinner.show();
		var visibilityData = {
		  ruleId: this.notiRuleID,
		  sts: row.visible
		}
		let url = webApi.domain + webApi.url.showHideVisibility;
		this.commonFunctionService.httpPostRequest(url, visibilityData)
		  .then(rescompData => {
			this.spinner.hide();
			this.visibiltyRes = rescompData;
			if (this.visibiltyRes.type == false) {
			  this.presentToast('error', '');
			} else {
			  this.presentToast('success', msg);
			}
		  },
			resUserError => {
			  this.spinner.hide();
			  // this.errorMsg = resUserError;
			});
		console.log('row', row);
	  }

	onChange(event) {
		console.log('selected Method:', event)
		if (event == 1) {
			this.showdate = true;
		}
	}

	templatesms: any = [];
	templatenot: any = [];
	templateemail: any = [];
	onChange2(notEventId) {

		let menuId = null;
		if (localStorage['menuId']) {
			menuId = localStorage.getItem('menuId');
		}
		const param = {
			'tId': this.userData.data.data.tenantId,
			'menuId': this.addEditSurveyService.menuId || menuId,
			'noteventId': notEventId,
			// 'noteventId': 43,
		};
		this.engageservice.getNotificationsTags(param)
			.then(rescompData => {
				if (rescompData && rescompData['data']) {
					let dataResponse = [];
					dataResponse = rescompData['data'];
					if (dataResponse[0].length != 0) {
						this.tagsArray = dataResponse[0];
						if (this.tagsArray.length != 0) {
							setTimeout(() => { this.init = true; }, 450);
						}


					}
				}
				this.spinner.hide();



			}, error => {
				//this.loader = false;
				// var toast: Toast = {
				// 	type: 'error',
				// 	//title: "Server Error!",
				// 	body: "Something went wrong.please try again later.",
				// 	showCloseButton: true,
				// 	timeout: 2000
				// };
				// this.toasterService.pop(toast);
				this.spinner.hide();
			});
		this.templatesms = [];
		this.templatenot = [];
		this.templateemail = [];

		for (let i = 0; i < this.templateDrop.length; i++) {
			if (this.templateDrop[i].notEventId == notEventId) {
				if (this.templateDrop[i].notModeId == 1) {
					let tempObj = {
						id: this.templateDrop[i].templateId,
						name: this.templateDrop[i].templateName,
						desc: this.templateDrop[i].description,
						template: this.templateDrop[i].template,
						subject: this.templateDrop[i].subject,
					}
					this.templatesms.push(tempObj);
					this.SurveyNotification.reviewTextSms = false;
					this.SurveyNotification.lnmsgtemp = '';
					this.SurveyNotification.editSmsTemplateData = '';
				} else if (this.templateDrop[i].notModeId == 2) {
					let tempObj = {
						id: this.templateDrop[i].templateId,
						name: this.templateDrop[i].templateName,
						desc: this.templateDrop[i].description,
						template: this.templateDrop[i].template,
						subject: this.templateDrop[i].subject,
					}
					this.templateemail.push(tempObj);
					this.SurveyNotification.reviewTextEmail = false;

					this.SurveyNotification.editEmailTemplateData = '';
					this.SurveyNotification.lnemailTemp = '';
					this.SurveyNotification.emailSubjectData = '';
				} else if (this.templateDrop[i].notModeId == 3) {
					let tempObj = {
						id: this.templateDrop[i].templateId,
						name: this.templateDrop[i].templateName,
						desc: this.templateDrop[i].description,
						template: this.templateDrop[i].template,
						subject: this.templateDrop[i].subject,
					}
					this.templatenot.push(tempObj);
					this.SurveyNotification.reviewTextNot = false;
					this.SurveyNotification.lnnottemp = '';
					this.SurveyNotification.editNotTemplateData = '';
					this.SurveyNotification.NotSubjectData = '';
				}
			}
		}



	}

	onChangeSelect(event) {
		if (event == 'nDays') {
			this.showdays = true;
		}
	}

	onChangeDropDownSms(event: Event) {
		let SelectedValue = (<HTMLInputElement>event.target).value;
		console.log(event);
		console.log(SelectedValue);

		this.SurveyNotification.reviewTextSms = true;
		for (let i = 0; i < this.templatesms.length; i++) {
			if (SelectedValue == this.templatesms[i].id) {
				this.SurveyNotification.editSmsTemplateData = this.templatesms[i].template;
				this.SurveyNotification.smsSubjectData = this.templatesms[i].subject;
			} else {
				this.SurveyNotification.reviewTextSms = false;
				this.SurveyNotification.editSmsTemplateData = '';
				this.SurveyNotification.smsSubjectData = '';
			}
		}

	}

	onChangeDropDownNot(event: Event) {
		let SelectedValue = (<HTMLInputElement>event.target).value;
		console.log(event);
		console.log(SelectedValue);
		if (SelectedValue) {
			this.SurveyNotification.reviewTextNot = true;
			for (let i = 0; i < this.templatenot.length; i++) {
				if (SelectedValue == this.templatenot[i].id) {
					this.SurveyNotification.editNotTemplateData = this.templatenot[i].template;
					this.SurveyNotification.NotSubjectData = this.templatenot[i].subject;
				}
			}
		} else {
			// this.SurveyNotification.reviewTextEmail = false;
			// this.SurveyNotification.editEmailTemplateData = '';
			// this.SurveyNotification.emailSubjectData = '';
		}

	}

	onChangeDropDownEmail(event: Event) {
		let SelectedValue = (<HTMLInputElement>event.target).value;
		console.log(event);
		console.log(SelectedValue);
		if (SelectedValue) {
			this.SurveyNotification.reviewTextEmail = true;
			for (let i = 0; i < this.templateemail.length; i++) {
				if (SelectedValue == this.templateemail[i].id) {
					this.SurveyNotification.editEmailTemplateData = this.templateemail[i].template;
					this.SurveyNotification.emailSubjectData = this.templateemail[i].subject;
				}
			}
		} else {
			this.SurveyNotification.reviewTextEmail = false;
			this.SurveyNotification.editEmailTemplateData = '';
			this.SurveyNotification.emailSubjectData = '';
		}

	}
	shownoData = false;
	searchArr: any = [];
	searchnotification(event) {
		// const val = event.target.value.toLowerCase();
		var val:any
        if(event !== 'clear'){
         val = event.target.value.toLowerCase();
        }else{
            val = ''
        }
		this.searchArr = [...this.SurveyNoti];
		this.searchText=val;

		if(val.length>=3||val.length==0){

		const temp = this.searchArr.filter(function (d) {
			return String(d.rulename).toLowerCase().indexOf(val) !== -1 ||
				d.description.toLowerCase().indexOf(val) !== -1 ||
				d.modes.toLowerCase().indexOf(val) !== -1 ||
				d.notEvent.toLowerCase().indexOf(val) !== -1 ||
				String(d.enrolmode).toLowerCase().indexOf(val) !== -1 ||
				!val
		});

		// update the rows
		this.rows = [...temp];

		if(this.rows.length == 0) {
			this.shownoData = true;
			this.nodata = true;
		} else {
			this.nodata = false;
		}

		console.log('this.rows',this.rows)
	}
		// Whenever the filter changes, always go back to the first page
		// this.tablenoti.offset = 0;
	}
	clearesearch(){
		if(this.searchText.length>=3){
			this.searchvalue={};
		 this.getExistingSurveyNotification();
		}
		else{
			this.searchvalue={};
		}
	}
	SurveyNoti: any = [];
	proceed: boolean = false;
	warningModal: boolean = false;
	submitSurveyNotification(data, f) {
		if (f.valid) {
			this.spinner.show();
			if (this.addFlag == true) {
				if (this.SurveyNoti.length != 0) {
					for (let i = 0; i < this.SurveyNoti.length; i++) {
						if (data.event == this.SurveyNoti[i].notEventId) {

							this.addEngagePopup = false;
							this.proceed = false;
							this.spinner.hide();
							break;
						} else {
							this.proceed = true;
						}
					}
				} else {
					this.proceed = true;
				}


				if (this.proceed == true) {
					this.submitSurveyNotificationFinal(data);
				} else {
					this.warningModal = true;
				}
			} else {
				this.submitSurveyNotificationFinal(data);
			}



		} else {
			console.log('Please Fill all fields');
			this.presentToast('warning', 'Please Fill all fields');
			// const addEditF: Toast = {
			// 	type: 'error',
			// 	title: 'Unable to update',
			// 	body: 'Please Fill all fields',
			// 	showCloseButton: true,
			// 	timeout: 2000
			// };
			// this.toasterService.pop(addEditF);
			Object.keys(f.controls).forEach(key => {
				f.controls[key].markAsDirty();
			});
		}
	}

	submitSurveyNotificationFinal(data) {
		this.getdatareadyforNotifications(data);
		if(this.rulename){
		if (this.addEditSurveyService.surveyId) {
			this.surveyId = this.addEditSurveyService.surveyId;
			console.log('Survey Id new for notification:', this.surveyId);
		}
		console.log('data', data);
		let param = {
			"areaId": 14,
			"instanceId": this.surveyId,
			"tid": this.userData.data.data.tenantId,
			"userid": this.userId,
			"nEventId": data.event,
			"qOptions": this.rulename,
		}
		this.engageservice.insertUpdateSurveyNotifications(param)
			.then(rescompData => {
				this.spinner.hide();
				var temp = rescompData;
				// console.log('NotificationsSuccess:', rescompData);
				if (this.action == "ADD") {
					if (temp['type'] == true) {
						// var notiInsert: Toast = {
						// 	type: 'success',
						// 	title: "Notifications Inserted!",
						// 	body: "Notifications inserted successfully.",
						// 	showCloseButton: true,
						// 	timeout: 2000
						// };
						// this.toasterService.pop(notiInsert);
						this.presentToast('success', 'Notification Added Successfully');
						this.getExistingSurveyNotification();
						this.cdf.detectChanges();
						// this.router.navigate(['pages/reactions/survey']);

					} else {
						//this.loader = false;
						// var toast: Toast = {
						// 	type: 'error',
						// 	//title: "Server Error!",
						// 	body: "Something went wrong.please try again later.",
						// 	showCloseButton: true,
						// 	timeout: 2000
						// };
						// this.toasterService.pop(toast);
						this.presentToast('error', '');
					}
				} else if (this.action == "EDIT") {
					if (temp['type'] == true) {
						// var notiInsert: Toast = {
						// 	type: 'success',
						// 	title: "Notifications Updated!",
						// 	body: "Notifications updated successfully.",
						// 	showCloseButton: true,
						// 	timeout: 2000
						// };
						// this.toasterService.pop(notiInsert);
						this.presentToast('success', 'Notification Updated Successfully');
						//this.cdf.detectChanges();
						this.getExistingSurveyNotification();
						//this.router.navigate(['pages/reactions/survey']);
					} else {

						// var toast: Toast = {
						// 	type: 'error',
						// 	//title: "Server Error!",
						// 	body: "Something went wrong.please try again later.",
						// 	showCloseButton: true,
						// 	timeout: 2000
						// };
						// this.toasterService.pop(toast);
						this.presentToast('error', '');
						//this.router.navigate(['pages/gamification/ladders']);
					}
				}


			},
				resUserError => {

					this.spinner.hide();
					// var toast: Toast = {
					// 	type: 'error',
					// 	//title: "Server Error!",
					// 	body: "Something went wrong.please try again later.",
					// 	showCloseButton: true,
					// 	timeout: 2000
					// };
					// this.toasterService.pop(toast);
					this.presentToast('error', '');
					//this.router.navigate(['pages/gamification/ladders']);
				});
		this.closeEngageModal();
			}
			else{
				this.toastr.warning('Please select any one of the below checkboxes');
				this.spinner.hide();
			}
	}

	closeWarningModal() {
		this.warningModal = false;
	}


	rulename: any;
	courseIdnotInsert: any = 1;
	getdatareadyforNotifications(notidetails) {

		if (notidetails.lnmsg == true) {
			var modeidsms = 1;
			var templateidsms = notidetails.lnmsgtemp;
			//var rulenamesms = "course" + "_" + this.courseId + "_" + "SMS";
			var rulenamesms = notidetails.name;
			var ruledescsms = notidetails.desc;
			var noteventid = notidetails.event;
			var templatesms = notidetails.editSmsTemplateData;
			// var visible =notidetails.visible
			var subjectsms = '';

			var smsString = rulenamesms + this.dataSeparatorService.Hash + ruledescsms + this.dataSeparatorService.Hash + noteventid + this.dataSeparatorService.Hash + modeidsms + this.dataSeparatorService.Hash + templateidsms + this.dataSeparatorService.Hash + templatesms + this.dataSeparatorService.Hash + this.SurveyNotification.visible + this.dataSeparatorService.Hash + subjectsms;
		} else {
			modeidsms = null;
			templateidsms = null;
		}

		if (notidetails.lnemail == true) {
			var modeidemail = 2;
			var templateidemail = notidetails.lnemailTemp;
			var templateemail = notidetails.editEmailTemplateData;
			//	var rulenameemail = "course" + "_" + this.courseId + "_" + "EMAIL";
			var rulenameemail = notidetails.name;
			var ruledescemail = notidetails.desc;
			var noteventid = notidetails.event;
			// var visible =notidetails.visible
			var subjectEmail = notidetails.emailSubjectData;

			var emailString = rulenameemail + this.dataSeparatorService.Hash + ruledescemail + this.dataSeparatorService.Hash + noteventid + this.dataSeparatorService.Hash + modeidemail + this.dataSeparatorService.Hash + templateidemail + this.dataSeparatorService.Hash + templateemail + this.dataSeparatorService.Hash + this.SurveyNotification.visible + this.dataSeparatorService.Hash + subjectEmail;
		} else {
			modeidemail = null;
			templateidemail = null;
		}

		if (notidetails.lnnot == true) {
			var modeidnot = 3;
			var templateidnot = notidetails.lnnottemp;
			var templatenot = notidetails.editNotTemplateData;
			//	var rulenamenotify = "course" + "_" + this.courseId + "_" + "NOTIFY";
			var rulenamenotify = notidetails.name;
			var ruledescnotify = notidetails.desc;
			var noteventid = notidetails.event;
			// var visible =notidetails.visible
			var subjectnot = '';

			var notifyString = rulenamenotify + this.dataSeparatorService.Hash + ruledescnotify + this.dataSeparatorService.Hash + noteventid + this.dataSeparatorService.Hash + modeidnot + this.dataSeparatorService.Hash + templateidnot + this.dataSeparatorService.Hash + templatenot + this.dataSeparatorService.Hash + this.SurveyNotification.visible + this.dataSeparatorService.Hash + subjectnot;
		} else {
			modeidnot = null;
			templateidnot = null;
		}

		// if (modeidsms == null) {
		// 	this.rulename = emailString + this.dataSeparatorService.Pipe + notifyString;
		// } else if (modeidemail == null) {
		// 	this.rulename = smsString + this.dataSeparatorService.Pipe + notifyString;
		// } else if (modeidnot == null) {
		// 	this.rulename = smsString + this.dataSeparatorService.Pipe + emailString;
		// } else if (modeidsms == null && modeidemail == null) {
		// 	this.rulename = notifyString;
		// } else if (modeidemail == null && modeidnot == null) {
		// 	this.rulename = smsString;
		// } else if (modeidnot == null && modeidsms == null) {
		// 	this.rulename = emailString;
		// } else {
		// 	this.rulename = smsString + this.dataSeparatorService.Pipe + emailString + this.dataSeparatorService.Pipe + notifyString;
		// }
		if (notidetails.lnmsg == true && notidetails.lnemail == true && notidetails.lnnot == true) {
			this.rulename = smsString + this.dataSeparatorService.Pipe + emailString + this.dataSeparatorService.Pipe + notifyString;
		} else if (notidetails.lnemail == true && notidetails.lnnot == true) {
			this.rulename = emailString + this.dataSeparatorService.Pipe + notifyString;
		} else if (notidetails.lnmsg == true && notidetails.lnnot == true) {
			this.rulename = smsString + this.dataSeparatorService.Pipe + notifyString;
		} else if (notidetails.lnmsg == true && notidetails.lnemail == true) {
			this.rulename = smsString + this.dataSeparatorService.Pipe + emailString;
		} else if (notidetails.lnnot == true) {
			this.rulename = notifyString;
		} else if (notidetails.lnmsg == true) {
			this.rulename = smsString;
		} else if (notidetails.lnemail == true) {
			this.rulename = emailString;
		}

		//this.ruledescription = this.rulename;
		console.log('thi.rulename', this.rulename)

	}

	notDetailsModal: boolean = false;
	templateData: any = {};
	getTempData(value, data) {
		//this.addEngagePopup = false;
		this.spinner.show();
		console.log('previewData:', data);
		let param = {
			"tempId": data,
			"tId": this.userData.data.data.tenantId,
		}

		this.notitempservice.getNottepById(param)
			.then(rescompData => {

				if (rescompData != "err") {
					this.spinner.hide();
					var temp = rescompData;
					var data = temp['data'][0];
					var res = data[0];

					if (temp['type'] == true) {
						this.templateData = {
							name: res.tname,
							desc: res.description,
							template: res.template
						}
						console.log('notifytemplateById', this.templateData);

						this.addEngagePopup = false;
						this.notDetailsModal = true;
					} else {
						// var toast: Toast = {
						// 	type: 'error',
						// 	//title: "Server Error!",
						// 	body: "Something went wrong.please try again later.",
						// 	showCloseButton: true,
						// 	timeout: 2000
						// };
						// this.toasterService.pop(toast);
						this.presentToast('error', '');
						//this.addEngagePopup = true;
					}
				} else {
					this.spinner.hide();
					// var toast: Toast = {
					// 	type: 'error',
					// 	//title: "Server Error!",
					// 	body: "Something went wrong.please try again later.",
					// 	showCloseButton: true,
					// 	timeout: 2000
					// };
					// this.toasterService.pop(toast);
					this.presentToast('error', '');
					//this.addEngagePopup = true;
				}

			})
		// resUserError => {
		//   this.spinner.hide();
		//   var toast: Toast = {
		//     type: 'error',
		//     //title: "Server Error!",
		//     body: "Something went wrong.please try again later.",
		//     showCloseButton: true,
		//     timeout: 2000
		//   };
		//   this.toasterService.pop(toast);
		//   this.addEngagePopup = true;
		// });

	}

	closeNotDetailModal() {
		this.notDetailsModal = false;
		this.addEngagePopup = true;
		this.templateData = {};
	}
	// Help Code Start Here //

	helpContent: any;
	getHelpContent() {
		return new Promise(resolve => {
			this.http1
				.get('../../../../../../assets/help-content/addEditCourseContent.json')
				.subscribe(
					data => {
						this.helpContent = data;
						console.log('Help Array', this.helpContent);
					},
					err => {
						resolve('err');
					}
				);
		});
		// return this.helpContent;
	}

	// Help Code Ends Here //
	openNav() {
		document.getElementById('mySidenav').style.width = '170px';
	}
	closeNav() {
		document.getElementById('mySidenav').style.width = '0';
	}
  smsMinText = null;
  smsMaxText = null;
  smsValidCheck(event){
    console.log('Value', event);
   //  console.log('Value', smsValidCheck);
   //  let value = event.target.value;
    if(event){
      this.smsMinText = 20;
      this.smsMaxText = 140;
    }else{
     this.smsMinText = null;
     this.smsMaxText = null;
    }
  }
}

