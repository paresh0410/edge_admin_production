import {
  Component,
  OnInit,
  Directive,
  ViewEncapsulation,
  ChangeDetectorRef,
  forwardRef,
  Attribute,
  OnChanges,
  SimpleChanges,
  Input,
  OnDestroy,
  ViewChild,
  ViewContainerRef,
} from "@angular/core";
import { AddeditsurveyService } from "./addEditsurvey.service";
import {
  NG_VALIDATORS,
  Validator,
  Validators,
  AbstractControl,
  ValidatorFn,
} from "@angular/forms";
import { LocalDataSource } from "ng2-smart-table";
import { Router, NavigationStart, ActivatedRoute } from "@angular/router";
import { OwlDateTimeModule } from "ng-pick-datetime";
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { SortablejsOptions } from "angular-sortablejs";
import { webAPIService } from "../../../../service/webAPIService";
import { webApi } from "../../../../service/webApi";
import { NgxSpinnerService } from "ngx-spinner";
import { AddeditfeedbackService } from "../../feedback/addEditfeedback/addEditfeedback.service";
import { AddEditCourseContentService } from "./../../../plan/courses/addEditCourseContent/addEditCourseContent.service";
import { NbThemeService } from "@nebular/theme";
import { ChartType, ChartOptions } from "chart.js";
import { SingleDataSet, Label } from "ng2-charts";
import * as pluginDataLabels from "chartjs-plugin-datalabels";
import { HttpClient } from "@angular/common/http";
import { ToastrService } from "ngx-toastr";
import { XlsxToJsonService } from "../../../plan/users/uploadusers/xlsx-to-json-service";
import { JsonToXlsxService } from "../../../coaching/participants/bulk-upload-coaching/json-to-xlsx.service";
import { DatePipe } from "@angular/common";
import * as _moment from "moment";
import {
  DateTimeAdapter,
  OWL_DATE_TIME_FORMATS,
  OWL_DATE_TIME_LOCALE,
} from "ng-pick-datetime";
import { MomentDateTimeAdapter } from "ng-pick-datetime-moment";
import { SuubHeader } from "../../../components/models/subheader.model";
import { isTuesday } from "date-fns";
import { CommonFunctionsService } from "../../../../service/common-functions.service";
import { BrandDetailsService } from "../../../../service/brand-details.service";
import { noData } from "../../../../models/no-data.model";
const moment = (_moment as any).default ? (_moment as any).default : _moment;

export const MY_CUSTOM_FORMATS = {
  fullPickerInput: "DD-MM-YYYY h:mm:ss A",
  parseInput: "DD-MM-YYYY h:mm:ss A",
  datePickerInput: "DD-MM-YYYY",
  timePickerInput: "LT",
  monthYearLabel: "MMM YYYY",
  dateA11yLabel: "LL",
  monthYearA11yLabel: "MMMM YYYY",
};
@Component({
  selector: "addEditsurvey",
  templateUrl: "./addEditsurvey.html",
  styleUrls: ["./addEditsurvey.scss"],
  encapsulation: ViewEncapsulation.None,
  providers: [
    DatePipe,
    {
      provide: DateTimeAdapter,
      useClass: MomentDateTimeAdapter,
      deps: [OWL_DATE_TIME_LOCALE],
    },
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS },
  ],
})
export class Addeditsurvey {
  /*******************Pie Chart************************** */
  // Pie
  //title = 'app';
  public pieChartLabels: string[] = ["Option1", "Option2", "Option3"];
  public pieChartData: number[] = [3, 4, 19];
  public pieChartType: string = "pie";
  public pieChartOptions: any = {
    backgroundColor: ["#FF6384", "#4BC0C0", "#FFCE56"],
  };
  // tempo: string;
  backflag = 1;
  searchText: any;
  currentBrandData: any;
  // events on slice click
  public chartClicked(e: any): void {
    console.log(e);
  }
	noDataVal:noData={
		margin:'mt-3',
		imageSrc: '../../../../../assets/images/no-data-bg.svg',
		title:"No enrollment added under survey.",
		desc:"",
		titleShow:true,
		btnShow:true,
		descShow:false,
		btnText:'Learn More',
		btnLink:'https://faq.edgelearning.co.in/kb/reaction-how-to-enrol-survey-to-users',
	  }
  // event on pie chart slice hover
  public chartHovered(e: any): void {
    console.log(e);
  }

  /************************************************************* */
  @ViewChild("fileUpload") fileUpload: any;
  @ViewChild("manualTable") manualTable: any;
  @ViewChild(DatatableComponent) tableDataManual: DatatableComponent;
  @ViewChild("myTable") table: any;

  enrolTab: boolean = false;
  query: string = "";
  public getData;

  datep: any;
  AttemptedUsers: any = [];
  rows: any[] = [];
  rows1: any = [];
  expanded: any = {};
  timeout: any;
  questionarray: any = [];
  Questions: boolean = false;
  Settings: boolean = true;
  Responses: boolean = false;
  Analysis: boolean = false;
  Enrolement: boolean = false;
  Notification: boolean = false;
  questionshow: boolean = false;
  title: any;
  formdata: any = {};
  profileUrl: any = "assets/images/category.jpg";
  errorMsg: any;
  random: any;
  loader: any;
  header: SuubHeader;
  qdata = {
    queaddtype: "",
    quetype: "",
  };
  // rows:any=[];
  selected: any = [];

  questiondata = {
    sqid: "",
    sqname: "",
    smand: "",
    sqtypeid: "",
    qansmin: 1,
    qansmax: 100,
    sqtypeName: "",
    sqdata: [],
  };
  quesTitle:string='Add Question'
	btnName: string = 'Save';
  analyticsData = [
    {
      aqid: 1,
      aqname: "Question 1",
      aqfequency: [
        {
          option: 10,
        },
        {
          option: 20,
        },
        {
          option: 40,
        },
      ],
      aqmean: 35,
      aqsd: "12.472191289246471285",
    },
    {
      aqid: 2,
      aqname: "Question 2",
      aqfequency: [
        {
          option: 10,
        },
        {
          option: 2,
        },
        {
          option: 400,
        },
        {
          option: 90,
        },
      ],
      aqmean: 125,
      aqsd: "162.17505973484332279",
    },
    {
      aqid: 3,
      aqname: "Question 3",
      aqfequency: [
        {
          option: 1,
        },
        {
          option: 199,
        },
        {
          option: 400,
        },
      ],
      aqmean: 200,
      aqsd: "162.89260265586034279",
    },
    {
      aqid: 4,
      aqname: "Question 4",
      aqfequency: [
        {
          option: 10,
        },
        {
          option: 90,
        },
        {
          option: 40,
        },
      ],
      aqmean: 46,
      aqsd: "32.998316455372217806",
    },
    {
      aqid: 5,
      aqname: "Question 5",
      aqfequency: [
        {
          option: 2,
        },
        {
          option: 200,
        },
      ],
      aqmean: 101,
      aqsd: "99",
    },
  ];

  // responseColData = [{
  //   qId: 1,
  //   qName: 'Question 1?'
  // }, {
  //   qId: 2,
  //   qName: 'Question 2?'
  // }, {
  //   qId: 3,
  //   qName: 'Question 3?'
  // }, {
  //   qId: 4,
  //   qName: 'Question 4?'
  // }, {
  //   qId: 5,
  //   qName: 'Question 5?'
  // }, {
  //   qId: 6,
  //   qName: 'Question 6?'
  // }]

  analysisTab = [
    {
      id: 1,
      queName: "Question One",
      tabData: [
        {
          option: "option1",
          count: 3,
        },
        {
          option: "option2",
          count: 4,
        },
        {
          option: "option3",
          count: 19,
        },
      ],
    },
    {
      id: 2,
      queName: "Question Two",
      tabData: [
        {
          option: "option1",
          count: 33,
        },
        {
          option: "option2",
          count: 14,
        },
        {
          option: "option3",
          count: 11,
        },
      ],
    },
    {
      id: 3,
      queName: "Question Three",
      tabData: [
        {
          option: "option1",
          count: 5,
        },
        {
          option: "option2",
          count: 9,
        },
        {
          option: "option3",
          count: 12,
        },
      ],
    },
  ];

  responseData = [
    {
      qId: 1,
      quename: "Question 1",
      udata: [
        {
          uname: "NIKET",
          udate: "16/03/2019",
          uansm: "option",
        },
        {
          uname: "DRUK",
          udate: "16/03/2019",
          uansm: "Agree",
        },
        {
          uname: "Vaibhav",
          udate: "16/03/2019",
          uansm: "Disagree",
        },
      ],
    },
    {
      qId: 2,
      quename: "Question 2",
      udata: [
        {
          uname: "NIKET",
          udate: "16/03/2019",
          uansm: "AREE",
        },
        {
          uname: "DRUK",
          udate: "16/03/2019",
          uansm: "DISAGREE",
        },
        {
          uname: "Vaibhav",
          udate: "16/03/2019",
          uansm: "option",
        },
      ],
    },
  ];

  labels: any = [
    { labelname: "ECN", bindingProperty: "ecn", componentType: "text" },
    {
      labelname: "FULL NAME",
      bindingProperty: "fullname",
      componentType: "text",
    },
    { labelname: "EMAIL", bindingProperty: "emailId", componentType: "text" },
    { labelname: "MOBILE", bindingProperty: "phoneNo", componentType: "text" },
    { labelname: "D.0.E", bindingProperty: "enrolDate", componentType: "text" },
    { labelname: "MODE", bindingProperty: "enrolmode", componentType: "text" },
    {
      labelname: "ACTION",
      bindingProperty: "btntext",
      componentType: "button",
    },
  ];
  labelsData: any = [
    { labelname: "STATUS", bindingProperty: "status", componentType: "text" },
    { labelname: "ECN", bindingProperty: "ecn", componentType: "text" },
    {
      labelname: "FULLNAME",
      bindingProperty: "fullname",
      componentType: "text",
    },
    { labelname: "D.0.E", bindingProperty: "enrolDate", componentType: "text" },
  ];
  columns = [
    {
      prop: "selected",
      name: "",
      sortable: false,
      canAutoResize: false,
      draggable: false,
      resizable: false,
      headerCheckboxable: true,
      checkboxable: true,
      width: 30,
    },
    { prop: "qsname", name: "Name" },
    { prop: "qpoint", name: "Point" },
    { prop: "qtype", name: "Type" },
  ];

  catData: any = undefined;
  temp1 = [];

  /**************settings*****************/
  credits: any = [
    {
      creditAllocId: 0,
      roleId: "",
      creditTypeId: "",
      creditTypeValue: "",
    },
  ];

  surveySettingData: any = {
    sname: "",
    sdesc: "",
    sicon: "",
    surveyicon: "",
    sotime: "",
    sctime: "",
    craditpoints: this.credits,
    prUsers: "",
  };
  userData;
  userId: any;
  addAction: boolean = false;
  surveyId: any;
  surveyQue: any = [];
  /*********************************/
  afterChange: boolean = false;
  QuestionListOptions: SortablejsOptions = {
    group: {
      name: "surveyQue",
    },
    sort: true,
    handle: ".drag-handle",
  };
  tenantId: any;
  action: any;
  file: any = [];
  datasetPreview: any;
  preview: boolean = false;
  resultSheets: any = [];
  result: any = [];
  uploadedData: any = [];
  bulkUploadData: any = [];
  fileUrl: any;
  fileName: any;
  enableUpload: any;
  fileReaded: any;
  addEditModRes: any = [];
  isdata: any;
  searchsub: any;

  private xlsxToJsonService: XlsxToJsonService = new XlsxToJsonService();
  constructor(
    protected addEditsurveyService: AddeditsurveyService,
    private commonFunctionService: CommonFunctionsService,
    // private toasterService: ToasterService,
    private router: Router,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private webApiService: webAPIService,
    private exportService: JsonToXlsxService,
    private datePipe: DatePipe,
    public brandService: BrandDetailsService,
    private courseDataService: AddEditCourseContentService,
    private feedservice: AddeditfeedbackService,
    private theme: NbThemeService,
    public cdf: ChangeDetectorRef,
    private http1: HttpClient,
    private toastr: ToastrService
  ) {
    if (localStorage.getItem("LoginResData")) {
      this.userData = JSON.parse(localStorage.getItem("LoginResData"));
      console.log("userData", this.userData.data);
      this.userId = this.userData.data.data.id;
      this.tenantId = this.userData.data.data.tenantId;
    }
    this.getHelpContent();
    this.getallDrop();
    this.getSurveyUserTypes();

    this.rows = this.responseData;
    this.temp1 = this.rows;
    console.log(
      "this.addEditsurveyService.getDataforAddedit",
      this.addEditsurveyService.getDataforAddedit
    );
    this.action = this.addEditsurveyService.getDataforAddedit[0]
      ? this.addEditsurveyService.getDataforAddedit[0]
      : "ADD";
    console.log("this.action", this.action);
    if (this.addEditsurveyService.getDataforAddedit) {
      if (this.action == "ADD") {
        this.addAction = true;
        let tempo = "Add Survey";
        this.surveySettingData = {
          sid: 0,
          sname: "",
          sdesc: "",
          surveyicon: "",
          sotime: "",
          sctime: "",
          prUsers: 1,
          craditpoints: [
            {
              crid: 0,
              roleId: "",
              pformatid: "",
              bcpoints: "",
              acpoitnts: "",
            },
          ],
        };
        this.datep = new Date();
      } else if (this.action == "EDIT") {
        this.addAction = false;
        let tempo = "Edit Survey";
        this.surveySettingData = {
          sid: this.addEditsurveyService.getDataforAddedit[1].sid,
          sname: this.addEditsurveyService.getDataforAddedit[1].sName,
          sdesc: this.addEditsurveyService.getDataforAddedit[1].sdesc,
          surveyicon: this.addEditsurveyService.getDataforAddedit[1].iconRef,
          sotime: new Date(
            this.addEditsurveyService.getDataforAddedit[1].oDate
          ),
          sctime: new Date(
            this.addEditsurveyService.getDataforAddedit[1].cDate
          ),
          craditpoints: this.addEditsurveyService.getDataforAddedit[1].points,
          prUsers: this.addEditsurveyService.getDataforAddedit[1].recordUsers,
        };

        if (this.addEditsurveyService.surveyId) {
          this.surveyId = this.addEditsurveyService.surveyId;
        }
        const date = new Date();
        if (this.surveySettingData.sotime > date) {
          this.datep = date;
        } else {
          this.datep = this.surveySettingData.sotime;
        }
      }
    }

    console.log("this.surveySettingData:", this.surveySettingData);
    console.log(
      "this.addEditsurveyService.getDataforAddedit111",
      this.addEditsurveyService.getDataforAddedit
    );
    this.QuestionListOptions = {
      onUpdate: (event: any) => {
        this.submitNewSrveyQue();
      },
    };
  }
  ngOnInit() {
    this.currentBrandData = this.brandService.getCurrentBrandData();
    this.fileName = 'Click here to upload an excel file to enrol' + this.currentBrandData.employee.toLowerCase() + 'to this course'
  }

  // ngOnDestroy(): void {
  // 	this.themeSubscription.unsubscribe();
  // }

  /**************************************Pie chart*************************************************** */
  // events
  // public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
  // 	console.log(event, active);
  //   }

  //   public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
  // 	console.log(event, active);
  //   }

  //   changeLabels() {
  // 	const words = ['hen', 'variable', 'embryo', 'instal', 'pleasant', 'physical', 'bomber', 'army', 'add', 'film',
  // 	  'conductor', 'comfortable', 'flourish', 'establish', 'circumstance', 'chimney', 'crack', 'hall', 'energy',
  // 	  'treat', 'window', 'shareholder', 'division', 'disk', 'temptation', 'chord', 'left', 'hospital', 'beef',
  // 	  'patrol', 'satisfied', 'academy', 'acceptance', 'ivory', 'aquarium', 'building', 'store', 'replace', 'language',
  // 	  'redeem', 'honest', 'intention', 'silk', 'opera', 'sleep', 'innocent', 'ignore', 'suite', 'applaud', 'funny'];
  // 	const randomWord = () => words[Math.trunc(Math.random() * words.length)];
  // 	this.pieChartLabels = Array.apply(null, { length: 3 }).map(_ => randomWord());
  //   }
  /***************************************************************************************************** */

  back() {
    if (this.backflag == 2) {
        this.header = {
          title: this.surveySettingData.sname!=""?this.surveySettingData.sname:"Add Survey",
          btnsSearch: true,
          btnName2: "Enrol",
          btnName3: "Bulk Enrol",
          btnName2show: true,
          btnName3show: true,
          btnBackshow: true,
          showBreadcrumb: true,
          breadCrumbList: [
            {
              'name': 'Reaction',
              'navigationPath': '/pages/reactions',
            },
            {
              'name': 'Survey',
              'navigationPath': '/pages/reactions/survey',
            },]
        };
      this.backflag = 1;
      this.showEnrolpage = false;
      this.showBulkpage = false;
      this.file = [];
      this.preview = false;
      this.fileName = 'Click here to upload an excel file to enrol' + this.currentBrandData.employee.toLowerCase() + 'to this course'
      this.enableUpload = false;
      this.allEnrolUser();
    } else {
      window.history.back();
    }
    // this.router.navigate(['/pages/reactions/survey']);
  }

  settingSubmit: boolean = false;
  questionSubmit: boolean = false;
  enrolSubmit: boolean = false;
  notificationSubmit: boolean = false;
  responseSubmit: boolean = false;
  analysisSubmit: boolean = false;

  /*********************tab selection and submit-back button show hide************************/
  selectTab(event) {
    console.log("event", event);
    console.log("event.tabTitle", event.tabTitle);
    if (event.tabTitle == "Settings") {
        this.header = {
          title: this.surveySettingData.sname!=""?this.surveySettingData.sname:"Add Survey",
          btnsSearch: true,
          btnName4: "Save & Next",
          btnName4show: true,
          btnBackshow: true,
          showBreadcrumb: true,
          breadCrumbList: [
            {
              'name': 'Reaction',
              'navigationPath': '/pages/reactions',
            },
            {
              'name': 'Survey',
              'navigationPath': '/pages/reactions/survey',
            },]
        };
      this.backflag = 1;
      this.spinner.show();
      setTimeout(() => {
        this.spinner.hide()
      }, 2000);
      this.Settings = true;
      this.Questions = false;
      this.Enrolement = false;
      this.Notification = false;
    } else if (event.tabTitle == "Questions") {
        this.header = {
          title: this.surveySettingData.sname!=""?this.surveySettingData.sname:"Add Survey",
          btnsSearch: true,
          btnName5: "Add Question",
          btnName5show: true,
          btnBackshow: true,
          showBreadcrumb: true,
          breadCrumbList: [
            {
              'name': 'Reaction',
              'navigationPath': '/pages/reactions',
            },
            {
              'name': 'Survey',
              'navigationPath': '/pages/reactions/survey',
            },]
        };
      this.backflag = 1;
      // this.spinner.show();
      // setTimeout(() => {
      //   this.spinner.hide()
      // }, 2000);
      this.Settings = false;
      this.Questions = true;
      this.Enrolement = false;
      this.Notification = false;
      this.getExistingSurveyQuestion();
      this.getSurveyQuestionType();
    } else if (event.tabTitle == "Enrol") {
        this.header = {
          title: this.surveySettingData.sname!=""?this.surveySettingData.sname:"Add Survey",
          btnsSearch: true,
          btnName2: "Enrol",
          btnName3: "Bulk Enrol",
          btnName2show: true,
          btnName3show: true,
          btnBackshow: true,
          showBreadcrumb: true,
          breadCrumbList: [
            {
              'name': 'Reaction',
              'navigationPath': '/pages/reactions',
            },
            {
              'name': 'Survey',
              'navigationPath': '/pages/reactions/survey',
            },]
        };
      this.backflag = 1;
      // this.spinner.show();
      // setTimeout(() => {
      //   this.spinner.hide()
      // }, 2000);
      this.Settings = false;
      this.Questions = false;
      this.Enrolement = true;
      this.Notification = false;
      this.allEnrolUser();
    } else if (event.tabTitle == "Notification") {
      // this.showEnrolpage = false;
      this.showEnrolpage = false;
      this.showBulkpage = false;
        this.header = {
          title: this.surveySettingData.sname!=""?this.surveySettingData.sname:"Add Survey",
          btnsSearch: true,
          searchBar: true,
          searchtext: '',
          dropdownlabel: '',
          placeHolder:'Search',
          btnName6: 'Add Notification',
          btnName6show: true,
          btnBackshow: true,
          showBreadcrumb: true,
          breadCrumbList: [
            {
              'name': 'Reaction',
              'navigationPath': '/pages/reactions',
            },
            {
              'name': 'Survey',
              'navigationPath': '/pages/reactions/survey',
            },
          ],
        };
        // this.spinner.show();
        // setTimeout(() => {
        //   this.spinner.hide()
        // }, 2000);
      this.backflag = 1;
      this.Settings = false;
      this.Questions = false;
      this.Enrolement = false;
      this.Notification = true;
      this.showEnrolpage = false;
    }
    // else if (event.tabTitle == "Responses") {

    // 	this.settingSubmit = false;
    // 	this.questionSubmit = false;
    // 	this.enrolSubmit = false;
    // 	this.notificationSubmit = false;
    // 	this.responseSubmit = true;
    // 	this.analysisSubmit = false;

    // } else if (event.tabTitle == "Analysis") {

    // 	this.settingSubmit = false;
    // 	this.questionSubmit = false;
    // 	this.enrolSubmit = false;
    // 	this.notificationSubmit = false;
    // 	this.responseSubmit = false;
    // 	this.analysisSubmit = true;

    // }
  }

  /***********************************Survey Settings Start******************************************/
  // surveySettingData:any = {};
  userTypes: any = [];
  getSurveyUserTypes() {
    let param = {
      tenantId: this.userData.data.data.tenantId,
    };

    this.addEditsurveyService.getUserType(param).then(
      (rescompData) => {
        // this.spinner.hide();
        let result = rescompData;

        this.userTypes = result["data"][0];

        console.log("this.userTypes", this.userTypes);
      },
      (resUserError) => {
        // this.spinner.hide();
        this.errorMsg = resUserError;
        // this.notFound = true;
        // this.router.navigate(['**']);
      }
    );
  }

  passparams: any;
  fileUploadRes: any;
  fileres: any;

  presentToast(type, body) {
    if (type === "success") {
      this.toastr.success(body, "Success", {
        closeButton: false,
      });
    } else if (type === "error") {
      this.toastr.error(
        'Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.',
        "Error",
        {
          timeOut: 0,
          closeButton: true,
        }
      );
    } else {
      this.toastr.warning(body, "Warning", {
        closeButton: false,
      });
    }
  }

  settingsubmit(settingData, f) {
    if (f.valid) {
      this.spinner.show();
      // this.makeSurveySettingDataReady();
      var option: string = Array.prototype.map
        .call(this.surveySettingData.craditpoints, function (item) {
          console.log("item", item);
          return Object.values(item).join("|");
        })
        .join("#");

      console.log("settingData", settingData);
      let param = {
        sId: this.addAction ? 0 : this.surveySettingData.sid,
        sName: settingData.sname,
        sdesc: settingData.sdesc,
        oDate: new Date(settingData.sotime),
        cDate: new Date(settingData.sctime),
        // "iRef":'',
        iRef: settingData.surveyicon ? settingData.surveyicon : "",
        //"vis":1,
        tenantId: this.userData.data.data.tenantId,
        userId: this.userId,
        allstr: option,
        rUsers: settingData.prUsers,
      };

      this.passparams = param;

      var fd = new FormData();
      fd.append("content", JSON.stringify(this.passparams));
      fd.append("file", this.surveyImgData);
      console.log("File Data ", fd);

      console.log("Course Data Img", this.surveyImgData);
      console.log("Course Data ", this.passparams);
      let fileUploadUrl = webApi.domain + webApi.url.fileUpload;

      if (this.surveyImgData != undefined) {
        this.webApiService.getService(fileUploadUrl, fd).then(
          (rescompData) => {
            this.spinner.hide();
            var temp: any = rescompData;
            this.fileUploadRes = temp;
            if (temp == "err") {
              // this.notFound = true;
              // var thumbUpload: Toast = {
              // 	type: 'error',
              // 	title: "Survey Thumbnail",
              // 	body: "Unable to upload survey thumbnail.",
              // 	// body: temp.msg,
              // 	showCloseButton: true,
              // 	timeout: 2000
              // };
              // this.toasterService.pop(thumbUpload);
              this.presentToast("error", "");
            } else if (temp.type == false) {
              // var thumbUpload: Toast = {
              // 	type: 'error',
              // 	title: "Survey Thumbnail",
              // 	body: "Unable to upload survey thumbnail.",
              // 	// body: temp.msg,
              // 	showCloseButton: true,
              // 	timeout: 2000
              // };
              // this.toasterService.pop(thumbUpload);
              this.presentToast("error", "");
            } else {
              if (
                this.fileUploadRes.data != null ||
                this.fileUploadRes.fileError != true
              ) {
                this.passparams.iRef = this.fileUploadRes.data.file_url;
                console.log("this.passparams.iRef", this.passparams.iRef);
                this.addEditSurvey(this.passparams);
              } else {
                // var thumbUpload: Toast = {
                // 	type: 'error',
                // 	title: "Survey Thumbnail",
                // 	// body: "Unable to upload course thumbnail.",
                // 	body: this.fileUploadRes.status,
                // 	showCloseButton: true,
                // 	timeout: 2000
                // };
                // this.toasterService.pop(thumbUpload);
                this.presentToast("error", "");
              }
            }
            console.log("File Upload Result", this.fileUploadRes);
            var res = this.fileUploadRes;
            this.fileres = res.data.file_url;
          },
          (resUserError) => {
            //this.loader =false;
            this.spinner.hide();
            this.errorMsg = resUserError;
          }
        );
      } else {
        // this.passparams.iconRef = this.badge.ico;
        this.addEditSurvey(this.passparams);
      }
      console.log("this.passparams", this.passparams);
    } else {
      console.log("Please Fill all fields");
      // const addEditF: Toast = {
      //   type: 'error',
      //   title: 'Unable to update',
      //   body: 'Please Fill all fields',
      //   showCloseButton: true,
      //   timeout: 2000
      // };
      // this.toasterService.pop(addEditF);
      this.presentToast("warning", "Please fill in the required fields");
      Object.keys(f.controls).forEach((key) => {
        f.controls[key].markAsDirty();
      });
    }
  }

  addEditSurvey(param) {
    this.spinner.show();
    console.log("param", param);
    this.addEditsurveyService.addEditsurveySetting(param).then(
      (rescompData) => {
        //this.loader =false;
        this.spinner.hide();
        var temp = rescompData;
        var surveyAddEditRes = temp["data"][0];
        console.log("Survey Add Result ", surveyAddEditRes);
        if (this.action == "ADD") {
          if (temp["type"] == true) {
            console.log("Survey Add Result ", surveyAddEditRes);
            // var certUpdate: Toast = {
            // 	type: 'success',
            // 	title: "Survey Inserted!",
            // 	body: "New survey added successfully.",
            // 	showCloseButton: true,
            // 	timeout: 2000
            // };
            // this.toasterService.pop(certUpdate);
            this.presentToast("success", "Survey added");
            this.surveyId = surveyAddEditRes[0].id;
            console.log("New Survey Id:", this.surveyId);
            this.addEditsurveyService.surveyId = this.surveyId;
            this.surveySettingData.sid = surveyAddEditRes[0].id;
            this.addAction = false;
            // this.router.navigate(['/pages/reactions/survey']);
            const Sdata = {
              tabTitle: "Questions",
            };
            this.selectTab(Sdata);
            this.cdf.detectChanges();
          } else {
            // var certUpdate: Toast = {
            // 	type: 'success',
            // 	title: "Survey Inserted!",
            // 	body: "Unable to add survey.",
            // 	showCloseButton: true,
            // 	timeout: 2000
            // };
            // this.toasterService.pop(certUpdate);
            this.presentToast("error", "");
            // this.router.navigate(['/pages/reactions/survey']);
          }
          this.spinner.hide();
        } else if (this.action == "EDIT") {
          // console.log('Certificate Edit Result ',this.certAddEditRes)
          if (temp["type"] == true) {
            this.presentToast("success", "Survey updated");
            const Sdata = {
              tabTitle: "Questions",
            };
            this.selectTab(Sdata);
            this.cdf.detectChanges();
            // var certUpdate: Toast = {
            // 	type: 'success',
            // 	title: "Survey Updated!",
            // 	body: "Survey updated successfully.",
            // 	showCloseButton: true,
            // 	timeout: 2000
            // };
            // this.toasterService.pop(certUpdate);

            //this.router.navigate(['/pages/assessment/survey'])
            //	this.router.navigate(['/pages/reactions/survey']);
          } else {
            // var certUpdate: Toast = {
            // 	type: 'success',
            // 	title: "Survey Updated!",
            // 	body: "Unable to update survey.",
            // 	showCloseButton: true,
            // 	timeout: 2000
            // };
            // this.toasterService.pop(certUpdate);
            this.presentToast("error", "");
            this.router.navigate(["/pages/reactions/survey"]);
          }
          this.spinner.hide();
        }
      },
      (resUserError) => {
        // this.loader =false;
        this.errorMsg = resUserError;
        this.spinner.hide();
        this.router.navigate(["/pages/reactions/survey"]);
      }
    );
  }

  addPointsList() {
    // let defualtCreditsObjCopy = Object.assign({}, this.defualtCreditsObj);
    let defualtCreditsObj = {
      crid: 0,
      roleId: "",
      pformatid: "",
      bcpoints: "",
      acpoitnts: "",
    };
    // console.log(defualtCreditsObj);
    // this.credits.push(defualtCreditsObj);
    this.surveySettingData.craditpoints.push(defualtCreditsObj);
  }

  removePointsList(i: number) {
    // this.credits.removeAt(i);
    // this.credits.splice(i,1);
    this.surveySettingData.craditpoints.splice(i, 1);
    this.roleSelectedList.splice(i, 1);
    this.selectedRole.splice(i, 1);
    this.addPointsDisableEnable();
    this.disableSelectedRole();
  }

  disableSelectedRole() {
    this.roles.forEach((data, key) => {
      if (this.selectedRole.indexOf(data.name) >= 0) {
        this.roles[key].isSelected = 1;
      } else {
        this.roles[key].isSelected = 0;
      }
    });
    console.log("Selected Disabled", this.roles);
  }

  disableAddCredit: boolean = false;
  addPointsDisableEnable() {
    if (this.roles.length == this.roleSelectedList.length) {
      this.disableAddCredit = true;
    } else {
      this.disableAddCredit = false;
    }
  }
  // roles=[];
  roleSelectedList = [];
  selectedRole = [];
  disableIfLearner: any = false;
  roleTypeSelected(currentIndex, currentItem) {
    console.log("currentItem ", currentItem);
    console.log("this.pointFormat", this.pointFormat);
    if (currentItem.roleId == "8") {
      this.surveySettingData.craditpoints[currentIndex].pformatid = 1;
      this.disableIfLearner = true;
    } else {
      this.disableIfLearner = false;
    }

    for (let i = 0; i < this.roles.length; i++) {
      var role = this.roles[i];
      if (role.id == Number(currentItem.roleId)) {
        this.roleSelectedList[currentIndex] = role;
        if (this.selectedRole.length > 0) {
          this.selectedRole[currentIndex] = role.name;
        } else {
          this.selectedRole.push(role.name);
        }
      }
    }

    this.addPointsDisableEnable();
    this.disableSelectedRole();
  }

  marksTypeSelected(currentEvent, currentIndex, currentItem) {
    console.log("currentItem ", currentItem);
  }

  // getRoleDropdown(){
  //   let param = {
  //     "tenantId":1
  //   }
  //   this.service.getSurveydropdown(param)
  //   .then(rescompData => {
  //    // this.spinner.hide();
  //     var result = rescompData;
  //     console.log('DropdownSurveyRole:',rescompData);
  //     if(result['type'] == true){
  //       this.roles = result['data'][0];
  //     };

  //   },error=>{
  //    // this.spinner.hide();
  //       var toast : Toast = {
  //         type: 'error',
  //         //title: "Server Error!",
  //         body: "Something went wrong.please try again later.",
  //         showCloseButton: true,
  //         timeout: 2000
  //     };
  //     this.toasterService.pop(toast);
  //   });
  // }

  marksType: any = [
    {
      id: 1,
      name: "Points",
    },
    {
      id: 2,
      name: "Percentage",
    },
  ];

  // creditPointsStr = '';
  // makeSurveySettingDataReady(){
  //   if(this.surveySettingData.craditpoints.length > 0){
  //     this.creditPointsStr = '';
  //     for(let i=0; i<this.surveySettingData.craditpoints.length; i++){
  //       var creditPoints = this.surveySettingData.craditpoints[i];
  //       if(this.creditPointsStr != ""){
  //         this.creditPointsStr += "#";
  //       }
  //       if(String(creditPoints.creditAllocId) != "" && String(creditPoints.creditAllocId) != "null"){
  //         this.creditPointsStr += creditPoints.creditAllocId;
  //       }
  //       if(String(creditPoints.roleId) != "" && String(creditPoints.roleId) != "null"){
  //         this.creditPointsStr += '|' + creditPoints.roleId;
  //       }
  //       if(String(creditPoints.creditTypeId) != "" && String(creditPoints.creditTypeId) != "null"){
  //         this.creditPointsStr += '|' + creditPoints.creditTypeId;
  //       }
  //       if(String(creditPoints.creditTypeValue) != "" && String(creditPoints.creditTypeValue) != "null"){
  //         this.creditPointsStr += '|' + creditPoints.creditTypeValue;
  //       }
  //     }
  //     console.log('creditPoints string',this.creditPointsStr);
  //   }
  // }

  surveyImgData: any;
  readUrl(event: any) {
    var validExts = new Array(".png", ".jpg", ".jpeg");
    var fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf("."));
    if (validExts.indexOf(fileExt) < 0) {
      // var toast: Toast = {
      // 	type: 'error',
      // 	title: "Invalid file selected!",
      // 	body: "Valid files are of " + validExts.toString() + " types.",
      // 	showCloseButton: true,
      // 	timeout: 2000
      // };
      // this.toasterService.pop(toast);
      this.presentToast(
        "warning",
        "Valid file types are " + validExts.toString()
      );
    } else {
      if (event.target.files && event.target.files[0]) {
        this.surveyImgData = event.target.files[0];

        var reader = new FileReader();

        reader.onload = (event: ProgressEvent) => {
          this.surveySettingData.surveyicon = (<FileReader>event.target).result;
        };
        reader.readAsDataURL(event.target.files[0]);
      }
    }
  }

  deleteProfile() {
    this.surveySettingData.surveyicon = null;
    this.surveySettingData.categoryPicRefs = undefined;
    //this.surveyImgData = null;
  }

  /********************************Survey Setting End*****************************************/

  /*****************************Survey Questionn Start****************************************/

  submitNewSrveyQue() {
    console.log(this.surveyQue);
    let finalString;
    for (let i = 0; i < this.surveyQue.length; i++) {
      let a = i + 1;
      let str = this.surveyQue[i].qId + "|" + a;

      if (i == 0) {
        finalString = str;
      } else {
        finalString += "#" + str;
      }
    }

    console.log(finalString);
    // return finalString;
    this.setSortOrder(finalString);
  }

  setSortOrder(finalString) {
    this.spinner.show();
    let param = {
      tenantId: this.userData.data.data.tenantId,
      surveyId: this.surveyId,
      allstr: finalString,
    };
    this.addEditsurveyService.ChangeSurveyQuestionOrder(param).then(
      (rescompData) => {
        this.spinner.hide();
        var result = rescompData;

        if (result["type"] == true) {
          // var toast: Toast = {
          // 	type: 'success',
          // 	//title: "Server Error!",
          // 	body: "List updated successfully.",
          // 	showCloseButton: true,
          // 	timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast("success", "List updated");
          this.getExistingSurveyQuestion();
          console.log("ORDER CHANGE SUCCESS:", result);
        }
      },
      (error) => {
        // this.spinner.hide();
        // var toast: Toast = {
        // 	type: 'error',
        // 	//title: "Server Error!",
        // 	body: "Something went wrong.please try again later.",
        // 	showCloseButton: true,
        // 	timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast("error", "");
      }
    );
  }

  noSurveyQuestion: boolean = false;
  getExistingSurveyQuestion() {
    this.spinner.show();
    let param = {
      tenantId: this.userData.data.data.tenantId,
      surveyId: this.surveyId,
    };
    console.log("param", param);
    this.addEditsurveyService.getExistingSurveyQuestions(param).then(
      (rescompData) => {
        this.spinner.hide();
        var result = rescompData;
        var finalRes = result["data"][0];
        if (result["type"] == true) {
          if (finalRes.length == 0) {
            this.noSurveyQuestion = true;
            this.noDataVal={
              margin:'mt-3',
              imageSrc: '../../../../../assets/images/no-data-bg.svg',
              title:"No question added under survey.",
              desc:"",
              titleShow:true,
              btnShow:true,
              descShow:false,
              btnText:'Learn More',
              btnLink:'https://faq.edgelearning.co.in/kb/reaction-how-to-add-questions-to-a-survey',
              }
          } else {
            this.noSurveyQuestion = false;
            this.surveyQue = finalRes;
            console.log("ExistingSurveyQuestions:", this.surveyQue);
            this.cdf.detectChanges();
          }
        }
      },
      (error) => {
        this.spinner.hide();
        // var toast: Toast = {
        // 	type: 'error',
        // 	//title: "Server Error!",
        // 	body: "Something went wrong.please try again later.",
        // 	showCloseButton: true,
        // 	timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast("error", "");
      }
    );
  }
  addqueAction: boolean = false;
  questionTypes: any = [];
  getSurveyQuestionType() {
    let param = {
      tenantId: this.userData.data.data.tenantId,
    };

    this.addEditsurveyService.getSurveyQuestionType(param).then(
      (rescompData) => {
        // this.spinner.hide();
        var result = rescompData;
        console.log("DropdownSurveyQuestionType:", rescompData);
        if (result["type"] == true) {
          this.questionTypes = result["data"][0];
          console.log("DropdownSurveyQuestionType:", this.questionTypes);
        }
      },
      (error) => {
        // this.spinner.hide();
        // var toast: Toast = {
        // 	type: 'error',
        // 	//title: "Server Error!",
        // 	body: "Something went wrong.please try again later.",
        // 	showCloseButton: true,
        // 	timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast("error", "");
      }
    );
  }

  savequestion(data) {
    console.log("data", data);
    // for(let i=0;i<this.formdata.sque.length;i++)
    // {
    //   if(this.formdata.sque[i].sqid==data.sqid)
    //   {
    //     this.formdata.sque.splice(i,1);

    //   }
    // }
    this.formdata.sque.push(data);
    console.log("formdata.sque", this.formdata.sque);

    this.questionshow = false;
    this.questiondata = {
      sqid: "",
      qansmax: 100,
      qansmin: 0,
      sqname: "",
      smand: "",
      sqtypeid: "",
      sqtypeName: "",
      sqdata: [],
    };
  }

  addQuestion() {
    this.questionshow = true;
    this.addqueAction = true;
    this.questiondata = {
      sqid: "",
      qansmax: 100,
      qansmin: 1,
      sqname: "",
      smand: "1",
      sqtypeid: "",
      sqtypeName: "",
      sqdata: [],
    };
  }

  saveQuestion(data, f) {
    if(this.addAction) {
      this.questionshow = false;
      this.presentToast('warning', 'Please save settings first');
      const Sdata = {
        tabTitle: 'Settings',
      }
      this.selectTab(Sdata);
    } else {
    if (f.valid) {
      console.log("data", data);
      let param = {
        queId: this.addqueAction == true ? 0 : data.sqid,
        queName: data.sqname,
        surveyId: this.surveyId,
        isMandatory: data.smand == true || data.smand == 1 ? 1 : 0,
        queTypeId: data.sqtypeid,
        tenantId: this.userData.data.data.tenantId,
        userId: this.userId,
        minVal: data.sqtypeid == 1 ? data.qansmin : 0,
        maxVal: data.sqtypeid == 1 ? data.qansmax : 0,
        labelfirst: data.sqtypeid == 3 ? "Yes" : 0,
        valfirst: data.sqtypeid == 3 ? "1" : 0,
        labelsecond: data.sqtypeid == 3 ? "No" : 0,
        valsecond: data.sqtypeid == 3 ? "0" : 0,
        stars: data.sqtypeid == 4 ? 4 : 0,
        allstr:
          data.sqtypeid == 2
            ? this.getDatareadyforsurveyquestion(data.sqdata)
            : 0,
      };

      console.log("param:", param);

      this.addEditsurveyService.addEditSurveyQuestion(param).then(
        (rescompData) => {
          var temp = rescompData;
          // console.log('NotificationsSuccess:', rescompData);
          if (this.action == "ADD") {
            if (temp["type"] == true) {
              // var notiInsert: Toast = {
              // 	type: 'success',
              // 	title: "Survey Question Inserted!",
              // 	body: "Question inserted successfully.",
              // 	showCloseButton: true,
              // 	timeout: 2000
              // };
              // this.toasterService.pop(notiInsert);
              this.presentToast("success", "Question added");
              console.log("SurveyQueRes:", temp);
              //this.cdf.detectChanges();
              this.getExistingSurveyQuestion();
              this.questiondata = {
                sqid: "",
                qansmax: 100,
                qansmin: 0,
                sqname: "",
                smand: "",
                sqtypeid: "",
                sqtypeName: "",
                sqdata: [],
              };
            } else {
              //this.loader = false;
              // var toast: Toast = {
              // 	type: 'error',
              // 	//title: "Server Error!",
              // 	body: "Something went wrong.please try again later.",
              // 	showCloseButton: true,
              // 	timeout: 2000
              // };
              // this.toasterService.pop(toast);
              this.presentToast("error", "");
              //this.router.navigate(['pages/gamification/ladders']);
            }
          this.spinner.hide();

          } else if (this.action == "EDIT") {
            if (temp["type"] == true) {
              // var notiInsert: Toast = {
              // 	type: 'success',
              // 	title: "Survey Question Updated!",
              // 	body: "Question updated successfully.",
              // 	showCloseButton: true,
              // 	timeout: 2000
              // };
              // this.toasterService.pop(notiInsert);
              this.presentToast("success", "Question updated");
              //this.cdf.detectChanges();
              this.getExistingSurveyQuestion();
              this.questiondata = {
                sqid: "",
                sqname: "",
                smand: "",
                sqtypeid: "",
                qansmin: 0,
                qansmax: 100,
                sqtypeName: "",
                sqdata: [],
              };
            } else {
              // var toast: Toast = {
              // 	type: 'error',
              // 	//title: "Server Error!",
              // 	body: "Something went wrong.please try again later.",
              // 	showCloseButton: true,
              // 	timeout: 2000
              // };
              // this.toasterService.pop(toast);
              this.presentToast("error", "");
              //this.router.navigate(['pages/gamification/ladders']);
            }
          }
          this.spinner.hide();
        },
        (resUserError) => {
          this.spinner.hide();
          // var toast: Toast = {
          // 	type: 'error',
          // 	//title: "Server Error!",
          // 	body: "Something went wrong.please try again later.",
          // 	showCloseButton: true,
          // 	timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast("error", "");
          //this.router.navigate(['pages/gamification/ladders']);
        }
      );
      this.questionshow = false;
    } else {
      console.log("Please Fill all fields");
      // const addEditF: Toast = {
      //   type: 'error',
      //   title: 'Unable to update',
      //   body: 'Please Fill all fields',
      //   showCloseButton: true,
      //   timeout: 2000
      // };
      // this.toasterService.pop(addEditF);
      Object.keys(f.controls).forEach((key) => {
        f.controls[key].markAsDirty();
      });
    }
   }
  }

  getDatareadyforsurveyquestion(data) {
    var allstr;
    for (let i = 0; i < data.length; i++) {
      let a = i + 1;
      var str = data[i].qansmul + "|" + a;

      if (i == 0) {
        allstr = str;
      } else {
        allstr += "#" + str;
      }
    }
    console.log("allstr", allstr);
    return allstr;
  }

  addandswers() {
    var qdata = {};
    this.questiondata.sqdata.push(qdata);
  }

  removeans(ansid) {
    for (let i = 0; i < this.questiondata.sqdata.length; i++) {
      if (ansid == i) {
        this.questiondata.sqdata.splice(i, 1);
      }
    }
  }
  enableDisableQuestionData: any;
  enableDisableQuestionModal: boolean = false;
  enableQuestion: boolean = false;
  queDisableIndex: any;
  disableQuestion(currentIndex, queData, status) {
    this.enableDisableQuestionData = queData;
    console.log("this.enableDisableLadderData", this.enableDisableQuestionData);
    this.enableDisableQuestionModal = true;
    this.queDisableIndex = currentIndex;

    if (this.enableDisableQuestionData.visible == 0) {
      this.enableQuestion = false;
    } else {
      this.enableQuestion = true;
    }
  }

  closeEnableDisableQuestionModal() {
    this.enableDisableQuestionModal = false;
  }

  enableDisableQuestionAction(actionType) {
    if (actionType == true) {
      if (this.enableDisableQuestionData.visible == 1) {
        var status = 0;
        var queData = this.enableDisableQuestionData;
        this.enableDisableQuestion(queData, status);
      } else {
        var status = 1;
        var queData = this.enableDisableQuestionData;
        this.enableDisableQuestion(queData, status);
      }
      this.closeEnableDisableQuestionModal();
    } else {
      this.closeEnableDisableQuestionModal();
    }
  }

  enableDisableQuestion(queData, status) {
    this.spinner.show();
    console.log("queData", queData);
    let param = {
      queId: queData.qId,
      visible: status,
    };
    this.addEditsurveyService
      .UpdateSurveyQuestionStatus(param)

      .then(
        (rescompData) => {
          this.spinner.hide();
          var result = rescompData;
          console.log("UpdateLadderResponse:", rescompData);
          if (result["type"] == true) {
            // var ladderUpdate: Toast = {
            // 	type: 'success',
            // 	title: "Survey Question Updated!!",
            // 	body: "Question updated successfully.",
            // 	showCloseButton: true,
            // 	timeout: 2000
            // };
            // this.toasterService.pop(ladderUpdate);
            this.presentToast("success", "Question updated");
            this.getExistingSurveyQuestion();
          } else {
            // var ladderUpdate: Toast = {
            // 	type: 'error',
            // 	title: "Survey Question Updated!",
            // 	body: "Unable to update question.",
            // 	showCloseButton: true,
            // 	timeout: 2000
            // };
            // this.toasterService.pop(ladderUpdate);
            this.presentToast("error", "");
          }
        },
        (error) => {
          this.spinner.hide();
          // var toast: Toast = {
          // 	type: 'error',
          // 	//title: "Server Error!",
          // 	body: "Something went wrong.please try again later.",
          // 	showCloseButton: true,
          // 	timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast("error", "");
        }
      );
  }

  //editActionQuestion: boolean = false;
  getSurveyQuestionDetails(data) {
    this.addqueAction = false;
    this.quesTitle='Edit Question'
    this.spinner.show();
    console.log("QueData", data);
    let param = {
      tenantId: this.userData.data.data.tenantId,
      queId: data.qId,
      queTypeId: data.qtId,
    };

    this.addEditsurveyService.GetSurveyQuestionDetails(param).then(
      (rescompData) => {
        this.spinner.hide();
        var result = rescompData;
        console.log("QuestionDeatils:", rescompData);
        var qData = result["data"][0][0];

        if (result["type"] == true) {
          console.log("QuestionDeatils:", qData);
          if (data.qtId == 1) {
            this.questiondata = {
              sqid: qData.id,
              sqname: qData.name,
              smand: qData.isMandatory,
              sqtypeid: qData.questionTypeId,
              qansmin: qData.minlen,
              qansmax: qData.maxlen,
              sqtypeName: null,
              sqdata: [],
            };
            this.questionshow = true;
          }

          if (data.qtId == 2) {
            if (qData.optionText) {
              var qOptArr = qData.optionText.split(",");
            }
            //	let qPointsArr = qData.points.split(',');
            let sqDataArr: any = [];
            let sqDataObj: any = {};
            //	console.log('Arrays:', qOptArr, qPointsArr);
            for (let i = 0; i < qOptArr.length; i++) {
              sqDataObj = {
                qansmul: qOptArr[i],
                //	"qansmulpoints": qPointsArr[i]
              };
              sqDataArr.push(sqDataObj);
            }
            console.log("sqDataArr", sqDataArr);

            this.questiondata = {
              sqid: qData.id,
              sqname: qData.name,
              smand: qData.isMandatory,
              sqtypeid: qData.questionTypeId,
              qansmin: null,
              qansmax: null,
              sqtypeName: null,
              sqdata: sqDataArr,
            };
            this.questionshow = true;
          }
          if (data.qtId == 3) {
            this.questiondata = {
              sqid: qData.id,
              sqname: qData.name,
              smand: qData.isMandatory,
              sqtypeid: qData.questionTypeId,
              qansmin: null,
              qansmax: null,
              sqtypeName: null,
              sqdata: [],
            };
            this.questionshow = true;
          }

          if (data.qtId == 4) {
            this.questiondata = {
              sqid: qData.id,
              sqname: qData.name,
              smand: qData.isMandatory,
              sqtypeid: qData.questionTypeId,
              qansmin: null,
              qansmax: null,
              sqtypeName: null,
              sqdata: [],
            };
            this.questionshow = true;
          }
        } else {
          this.spinner.hide();
          // var toast: Toast = {
          // 	type: 'error',
          // 	//title: "Server Error!",
          // 	body: "Something went wrong.please try again later.",
          // 	showCloseButton: true,
          // 	timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast("error", "");
        }
      },
      (error) => {
        this.spinner.hide();
        // var toast: Toast = {
        // 	type: 'error',
        // 	//title: "Server Error!",
        // 	body: "Something went wrong.please try again later.",
        // 	showCloseButton: true,
        // 	timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast("error", "");
      }
    );
  }

  /*****************************Survey Question End********************************************/

  /*****************************Survey Enrol start**************************************/
  showEnrolpage: boolean = false;
  showBulkpage: boolean = false;
  saveEnrol() {
    this.showEnrolpage = true;
    this.showBulkpage = false;
      this.header = {
        title: this.surveySettingData.sname!=""?this.surveySettingData.sname:"Add Survey",
        btnsSearch: true,
        btnName3: "Bulk Enrol",
        btnName3show: true,
        btnBackshow: true,
        showBreadcrumb: true,
        breadCrumbList: [
          {
            'name': 'Reaction',
            'navigationPath': '/pages/reactions',
          },
          {
            'name': 'Survey',
            'navigationPath': '/pages/reactions/survey',
          },]
      };
    this.backflag = 2;
  }
  bulkEnrol() {
    this.showEnrolpage = false;
    this.showBulkpage = true;
      this.header = {
        title: this.surveySettingData.sname!=""?this.surveySettingData.sname:"Add Survey",
        btnsSearch: true,
        btnName2: "Enrol",
        btnName2show: true,
        btnBackshow: true,
        showBreadcrumb: true,
        breadCrumbList: [
          {
            'name': 'Reaction',
            'navigationPath': '/pages/reactions',
          },
          {
            'name': 'Survey',
            'navigationPath': '/pages/reactions/survey',
          },]
      };
    this.backflag = 2;
  }
  backToEnrol() {
    this.showEnrolpage = false;
    this.showBulkpage = false;
    this.file = [];
    this.preview = false;
    this.fileName = 'Click here to upload an excel file to enrol' + this.currentBrandData.employee.toLowerCase() + 'to this course'
    this.enableUpload = false;
    this.allEnrolUser();
  }
  tableData: any;
  temp: any;
  searchvalue: any = {
    value: "",
  };

  clearesearch() {
    if (this.searchText.length >= 3) {
      this.searchvalue = {};
      this.allEnrolUser();
      this.rows = [...this.rows];
    } else {
      this.searchText = ''
      this.searchvalue = {}
    }
  }
  searchEnrolUser(event) {
    const val = event.target.value.toLowerCase();
    this.searchText = val;
    // this.allEnrolUser( this.courseDataService.data.data)

    this.temp = [...this.enrolldata];
    console.log(this.temp);
    // filter our data
    if (val.length >= 3 || val.length == 0) {
      const temp = this.temp.filter(function (d) {
        return (
          String(d.ecn).toLowerCase().indexOf(val) !== -1 ||
          d.fullname.toLowerCase().indexOf(val) !== -1 ||
          d.emailId.toLowerCase().indexOf(val) !== -1 ||
          d.enrolmode.toLowerCase().indexOf(val) !== -1 ||
          d.enrolDate.toLowerCase().indexOf(val) !== -1 ||
          d.phoneNo.toLowerCase().indexOf(val) !== -1 ||
          String(d.enrolmode).toLowerCase().indexOf(val) !== -1 ||
          // d.mode.toLowerCase() === val || !val;
          !val
        );
      });

      // update the rows
      this.rows = [...temp];
      // Whenever the filter changes, always go back to the first page
      this.tableData.offset = 0;
    }
  }

  readfileUrl(event: any) {
    var validExts = new Array(".xlsx", ".xls");
    var fileExt = event.target.files[0].name;
    this.showInvalidExcel = false;
    this.invaliddata = [];
    fileExt = fileExt.substring(fileExt.lastIndexOf("."));
    if (validExts.indexOf(fileExt) < 0) {
      this.presentToast(
        "warning",
        "Valid file types are " + validExts.toString()
      );
      this.cancel();
    } else {
      if (event.target.files && event.target.files[0]) {
        this.fileName = event.target.files[0].name;
        //read file from input
        this.fileReaded = event.target.files[0];

        if (
          this.fileReaded != "" ||
          this.fileReaded != null ||
          this.fileReaded != undefined
        ) {
          this.enableUpload = true;
          this.showInvalidExcel = false;
        }

        this.file = event.target.files[0];

        this.xlsxToJsonService
          .processFileToJson({}, this.file)
          .subscribe((data) => {
            let resultSheets = Object.getOwnPropertyNames(data["sheets"]);
            console.log("File Property Names ", resultSheets);
            let sheetName = resultSheets[0];
            let result = data["sheets"][sheetName];
            console.log("dataSheet", data);

            if (result.length > 0) {
              this.uploadedData = result;
              console.log("this.uploadedData", this.uploadedData);
            } else {
              this.uploadedData = [];
            }
          });

        var reader = new FileReader();
        reader.onload = (event: ProgressEvent) => {
          let fileUrl = (<FileReader>event.target).result;
        };
        reader.readAsDataURL(event.target.files[0]);
      }
    }
  }
  cancel() {
    // this.fileUpload.nativeElement.files = [];
    console.log(this.fileUpload.nativeElement.files);
    this.fileUpload.nativeElement.value = "";
    console.log(this.fileUpload.nativeElement.files);
    this.fileName = 'Click here to upload an excel file to enrol' + this.currentBrandData.employee.toLowerCase() + 'to this course'
    this.enableUpload = false;
    this.file = [];
    this.preview = false;
  }

  uploadenrol() {
    this.spinner.show();

    var fd = new FormData();
    console.log(this.surveySettingData);
    this.surveySettingData.areaId = 14;
    this.surveySettingData.userId = this.userId;
    this.surveySettingData.tenantId = this.tenantId;
    this.surveySettingData.courseId = this.surveySettingData.sid;
    fd.append("content", JSON.stringify(this.surveySettingData));
    fd.append("file", this.file);
    console.log(fd);
    this.courseDataService.TempManEnrolBulk(fd).then(
      (result) => {
        console.log(result);
        // this.loader =false;

        var res = result;
        try {
          if (res["data1"][0]) {
            this.resultdata = res["data1"][0];
            for (let i = 0; i < this.resultdata.length; i++) {
              // this.resultdata[i].enrolDate = this.formdate(this.resultdata[i].enrolDate);
            }

            this.datasetPreview = this.resultdata;
            this.preview = true;
            this.cdf.detectChanges();
            // this.spinner.hide();
          }
        } catch (e) {
          // var courseUpdate: Toast = {
          //   type: 'error',
          //   title: 'Enrol',
          //   body: 'Something went wrong.please try again later.',
          //   showCloseButton: true,
          //   timeout: 2000
          // };

          // this.closeEnableDisableCourseModal();
          // this.toasterService.pop(courseUpdate);
          this.presentToast("error", "");
        }
        this.spinner.hide();
      },
      (resUserError) => {
        // this.loader =false;
        this.spinner.hide();
        this.errorMsg = resUserError;
        // this.closeEnableDisableCourseModal();
      }
    );
  }
  bulkUploadRes: any;
  invaliddata: any = [];
  showInvalidExcel: boolean = false;
  uploadBulkEnrol() {
    if (this.uploadedData.length > 0 && this.uploadedData.length <= 2000) {
      this.spinner.show();

      // const option: string = Array.prototype.map
      //   .call(this.uploadedData, function (item) {
      //     console.log("item", item);
      //     return Object.values(item).join("#");
      //   })
      //   .join("|");

      var option: string = Array.prototype.map
        .call(this.uploadedData, (item) => {
          const array = Object.keys(item);
          let string = '';
          array.forEach(element => {
            if (element === 'EnrolDate') {
              string += this.commonFunctionService.formatDateTimeForBulkUpload(item[element]);
            } else {
              string = item[element] + '#';
            }
          });
          // console.log("item", item);
          // return Object.values(item).join("#");
          return string;
        })
        .join("|");

      const data = {
        wfId: null,
        aId: 14,
        iId: this.surveySettingData.sid,
        upString: option,
      };
      console.log("data", data);
      this.addEditsurveyService.areaBulkEnrol(data).then(
        (rescompData) => {
          // this.loader =false;
          this.spinner.hide();
          var temp: any = rescompData;
          this.bulkUploadRes = temp.data;
          if (temp == "err") {
            this.toastr.error(
              'Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.',
              "Error",
              {
                timeOut: 0,
                closeButton: true,
              }
            );
          } else if (temp.type == false) {
            this.toastr.error(
              'Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.',
              "Error",
              {
                timeOut: 0,
                closeButton: true,
              }
            );
          } else {
            this.enableUpload = false;
            this.fileName = 'Click here to upload an excel file to enrol' + this.currentBrandData.employee.toLowerCase() + 'to this course'
            if (temp["data"].length > 0) {
              if (temp["data"].length === temp["invalidCnt"].invalidCount) {
                this.toastr.warning(
                  "No valid " + this.currentBrandData.employee.toLowerCase() + "s found to enrol.",
                  "Warning",
                  {
                    closeButton: false,
                  }
                );
              } else {
                this.toastr.success("Enrol successfully.", "Success", {
                  closeButton: false,
                });
              }
              this.invaliddata = temp["data"];
              this.showInvalidExcel = true;
            } else {
              this.invaliddata = [];
              this.showInvalidExcel = false;
            }
            // this.backToEnrol()
            this.cdf.detectChanges();
            this.preview = false;
            this.showInvalidExcel = true;
          }
          console.log("Enrol bulk Result ", this.bulkUploadRes);
          this.cdf.detectChanges();
        },
        (resUserError) => {
          this.spinner.hide();
          this.errorMsg = resUserError;
        }
      );
    } else {
      if (this.uploadedData.length > 2000) {
        this.toastr.warning(
          "File Data cannot exceed more than 2000",
          "warning",
          {
            closeButton: true,
          }
        );
      } else {
        this.toastr.warning("No Data Found", "warning", {
          closeButton: true,
        });
      }
    }
  }

  exportToExcelInvalid() {
    this.exportService.exportAsExcelFile(this.invaliddata, "Enrolment Status");
  }

  savebukupload() {
    var data = {
      userId: this.userId,
      coursId: this.surveySettingData.sid,
      areaId: 14,
      tId: this.tenantId,
    };
    // this.loader = true;
    this.spinner.show();
    this.courseDataService.finalManEnrolBulk(data).then(
      (rescompData) => {
        // this.loader =false;
        this.spinner.hide();
        var temp: any = rescompData;
        this.addEditModRes = temp.data[0];
        if (temp == "err") {
          //   var modUpdate: Toast = {
          // 	type: 'error',
          // 	title: 'Enrol',
          // 	body: 'Something went wrong',
          // 	showCloseButton: true,
          // 	timeout: 2000
          //   };
          //   this.toasterService.pop(modUpdate);
          this.presentToast("error", "");
        } else if (temp.type == false) {
          //   var modUpdate: Toast = {
          // 	type: 'error',
          // 	title: 'Enrol',
          // 	body: this.addEditModRes[0].msg,
          // 	showCloseButton: true,
          // 	timeout: 2000
          //   };
          //   this.toasterService.pop(modUpdate);
          this.presentToast("error", "");
        } else {
          //   var modUpdate: Toast = {
          // 	type: 'success',
          // 	title: 'Enrol',
          // 	body: this.addEditModRes[0].msg,
          // 	showCloseButton: true,
          // 	timeout: 2000
          //   };
          //   this.toasterService.pop(modUpdate);
          this.presentToast("success", this.addEditModRes[0].msg);
          this.backToEnrol();
          this.cdf.detectChanges();
          this.preview = false;
        }
        console.log("Enrol bulk Result ", this.addEditModRes);
        this.cdf.detectChanges();
      },
      (resUserError) => {
        this.loader = false;
        this.errorMsg = resUserError;
      }
    );
  }
  enrolldata: any;
  enableCourse: any;
  allEnrolUser() {
    this.rows = [];
    this.spinner.show();
    var data = {
      areaId: 14,
      instanceId: this.surveyId,
      tId: this.userData.data.data.tenantId,
      mode: 0,
    };
    console.log(data);
    this.courseDataService.getallenroluser(data).then((enrolData) => {
      this.spinner.hide();
      this.enrolldata = enrolData["data"];
      this.rows = enrolData["data"];
      this.rows = [...this.rows];
      for (let i = 0; i < this.rows.length; i++) {
        // this.rows[i].Date = new Date(this.rows[i].enrolDate);
        // this.rows[i].enrolDate = this.formdate(this.rows[i].Date);
        if (this.rows[i].visible == 1) {
          this.rows[i].btntext = "fa fa-eye";
        } else {
          this.rows[i].btntext = "fa fa-eye-slash";
        }
      }
      console.log("EnrolledUSer", this.rows);
      if ((this.enrolldata.visible = 1)) {
        this.enableCourse = false;
      } else {
        this.enableCourse = true;
      }
      //this.cdf.detectChanges();
    });
  }
  formdate(date) {
    if (date) {
      var formatted = this.datePipe.transform(date, "dd-MM-yyyy");
      return formatted;
    }
  }
  visibilityTableRow(row) {
    let value;
    let status;

    if (row.visible == 1) {
      row.btntext = "fa fa-eye-slash";
      value = "fa fa-eye-slash";
      row.visible = 0;
      status = 0;
    } else {
      status = 1;
      value = "fa fa-eye";
      row.visible = 1;
      row.btntext = "fa fa-eye";
    }

    for (let i = 0; i < this.rows.length; i++) {
      if (this.rows[i].employeeId == row.employeeId) {
        this.rows[i].btntext = row.btntext;
        this.rows[i].visible = row.visible;
      }
    }
    var visibilityData = {
      employeeId: row.employeeId,
      visible: status,
      courseId: this.surveyId,
      tId: this.userData.data.data.tenantId,
      aId: 14,
    };
    this.courseDataService.disableEnrol(visibilityData).then(
      (result) => {
        console.log(result);
        this.spinner.hide();
        this.resultdata = result;
        if (this.resultdata.type == false) {
          this.presentToast("error", "");
        } else {
          console.log("after", row.visible);
          this.allEnrolUser();
          this.presentToast("success", this.resultdata.data);
        }
      },
      (resUserError) => {
        this.errorMsg = resUserError;
      }
    );

    console.log("row", row);
  }
  resultdata: any;
  // disableCourseVisibility(currentIndex, row, status) {
  // 	this.spinner.show();
  // 	var visibilityData = {
  // 		employeeId: row.employeeId,
  // 		visible: status,
  // 		courseId: this.surveyId,
  // 		tId: this.userData.data.data.tenantId,
  // 		aId: 14,
  // 	}
  // 	this.courseDataService.disableEnrol(visibilityData).then(result => {
  // 		console.log(result);
  // 		this.spinner.hide();
  // 		this.resultdata = result;
  // 		if (this.resultdata.type == false) {
  // 			// var courseUpdate: Toast = {
  // 			// 	type: 'error',
  // 			// 	title: "Survey",
  // 			// 	body: "Unable to update visibility of User.",
  // 			// 	showCloseButton: true,
  // 			// 	timeout: 2000
  // 			// };
  // 			// this.closeEnableDisableCourseModal();
  // 			// this.toasterService.pop(courseUpdate);
  // 			this.presentToast('error', '');
  // 		} else {
  // 			// var courseUpdate: Toast = {
  // 			// 	type: 'success',
  // 			// 	title: "Survey",
  // 			// 	body: this.resultdata.data,
  // 			// 	showCloseButton: true,
  // 			// 	timeout: 2000
  // 			// };
  // 			// row.visible = !row.visible;
  // 			console.log("after", row.visible)
  // 			this.allEnrolUser();
  // 			// this.toasterService.pop(courseUpdate);
  // 			this.presentToast('success', this.resultdata.data);
  // 		}
  // 	},
  // 		resUserError => {
  // 			this.errorMsg = resUserError;
  // 			// this.closeEnableDisableCourseModal();
  // 		});

  // }
  /*************************************************************************************/

  Removeque(sid) {
    for (let i = 0; i < this.formdata.sque.length; i++) {
      if (sid == i) {
        this.formdata.sque.splice(i, 1);
      }
    }
  }

  qtypechange(j) {
    if (this.questiondata.sqdata.length > 0) {
      this.questiondata.sqdata = [];
    }
    if (this.questiondata.sqdata.length == 0) {
      var qdata = {
        // qaid:'',
        // qans:''
      };
      this.questiondata.sqdata.push(qdata);
    }
  }

  // removeans(ansid)
  // {

  //     for(let i=0;i<this.questiondata.sqdata.length;i++)
  //     {
  //       if(ansid ==i)
  //       {
  //         this.questiondata.sqdata.splice(i,1)
  //       }
  //     }

  // }
  // savequestion(data)
  // {
  //   console.log('data',data);
  //   for(let i=0;i<this.formdata.sque.length;i++)
  //   {
  //     if(this.formdata.sque[i].sqid==data.sqid)
  //     {
  //       this.formdata.sque.splice(i,1);

  //     }
  //   }
  //    this.formdata.sque.push(data);

  //   this.questionshow =false;
  //   this.questiondata={
  //               sqid:'',
  //               sqname:'' ,
  //               smand:'',
  //               sqtypeid:'',
  //               sqtypeName:'',
  //               sqdata:[]

  //             };
  // }

  editQuestion(item) {
    console.log("item", item);
    this.questionshow = true;
    this.questiondata = {
      sqid: item.sqid,
      sqname: item.sqname,
      smand: item.smand,
      qansmax: 100,
      qansmin: 0,
      sqtypeid: item.sqtypeid,
      sqtypeName: item.sqtypeName,
      sqdata: item.sqdata,
    };
  }

  save(qaddtype, qtype) {
    var questionarray = this.questionarray;
    this.questionshow = false;
    if (qaddtype == 1) {
      if (this.selected.length > 0) {
        for (let j = 0; j < questionarray.length; j++) {
          for (let i = 0; i < this.selected.length; i++) {
            if (this.selected[i].qsid == questionarray[j].qsid) {
              this.selected.splice(i, 1);
            }
            // this.questionarray.push(this.selected[i])
          }
        }
        for (let i = 0; i < this.selected.length; i++) {
          this.questionarray.push(this.selected[i]);
        }
      }
    } else if (qaddtype == 2) {
      // this.random.qtype=
      this.selected = [];
      this.questionarray.push(this.random);
    }
  }
  Qtypechange(qdata) {
    if (qdata == 2) {
      this.random = {
        qsid: "",
        qsname: "Random",
        qpoint: "",
        qtype: "",
      };
    }
  }

  closeModel() {
    this.questionshow = false;
  }
  // addQuestion()
  // {
  //   this.questionshow =true;

  // }

  Backquestion() {
    this.questionshow = false;
  }
  // onSelect({ selected }) {
  //   console.log('Select Event', selected, this.selected);

  //   this.selected.splice(0, this.selected.length);
  //   this.selected.push(...selected);
  // }
  //  onActivate(event) {
  //   console.log('Activate Event', event);
  // }
  submit() {
    this.router.navigate(["/pages/reactions/survey"]);
    //   var category={
    //     categoryId: this.formdata.categoryId,
    //     name: this.formdata.name,
    //     idnumber: this.formdata.idnumber,
    //     description: this.formdata.description,
    //     descriptionformat:this.formdata.descriptionformat,
    //     parent:this.formdata.parent,
    //     sortorder:this.formdata.sortorder,
    //     coursecount:this.formdata.coursecount,
    //     visible:this.formdata.visible,
    //     visibleold:this.formdata.visibleold,
    //     timemodified:this.formdata.timemodified,
    //     depth:this.formdata.depth,
    //     path:this.formdata.path,
    //     theme:this.formdata.theme
    //   }

    //   this.service.createUpdateCategory(category)
    //   .subscribe(rescompData => {
    //     // this.loader =false;
    //     this.result = rescompData;
    //     console.log('Added Category Result',this.result)
    //     if(this.result.type == true){
    //       if(this.catData != undefined){
    //         this.router.navigate(['/pages/plan/courses/category']);
    //         var catUpdate : Toast = {
    //             type: 'success',
    //             title: "Category updated!",
    //             body: "Category updated successfully.",
    //             showCloseButton: true,
    //             timeout: 2000
    //         };
    //         this.toasterService.pop(catUpdate);
    //       }else{
    //         this.router.navigate(['/pages/plan/courses/category']);
    //         var catCreate : Toast = {
    //             type: 'success',
    //             title: "Category created!",
    //             body: "New category added successfully.",
    //             showCloseButton: true,
    //             timeout: 2000
    //         };
    //         this.toasterService.pop(catCreate);
    //       }
    //     }else{
    //       if(this.catData != undefined){
    //         var catUpdate : Toast = {
    //             type: 'error',
    //             title: "Category not updated!",
    //             body: "Unable to update category.",
    //             showCloseButton: true,
    //             timeout: 2000
    //         };
    //         this.toasterService.pop(catUpdate);
    //       }else{
    //         var catCreate : Toast = {
    //             type: 'error',
    //             title: "Category not created!",
    //             body: "Unable to create category.",
    //             showCloseButton: true,
    //             timeout: 2000
    //         };
    //         this.toasterService.pop(catCreate);
    //       }
    //     }
    //   },
    //   resUserError => {
    //     // this.loader =false;
    //     this.errorMsg = resUserError
    //   });
    // }
  }

  onDetailToggle(event) {
    console.log("Detail Toggled", event);
  }
  onPage(event) {
    // clearTimeout(this.timeout);
    // this.timeout = setTimeout(() => {
    //   // console.log('paged!', event);
    // }, 100);
  }

  onSelect({ selected }) {
    console.log("Selected Event", selected);
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    // console.log('Selected Data',this.selected);
    // this.userData = this.selected;
    // if(this.selected.length > 0){
    //   this.enrollDisabled = false;
    // }else{
    //   this.enrollDisabled = true;
    // }
  }

  onActivate(event) {
    // console.log('Activate Event', event);
    // if(event.type == "click"){
    //   // console.log('Row CLick Event', event);
    //   console.log('Row Data',event.row);
    //   this.showLargeModal();
    // }
  }

  displayCheck(row) {
    // return row.name !== 'Ethel Price';
  }
  toggleExpandRow(row) {
    console.log("Toggled Expand Row!", row);
    this.table.rowDetail.toggleExpandRow(row);
    this.AttemptedUsers = row.rque;
  }

  searchfeedbackUser(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp1.filter(function (d) {
      return d.runame.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }
  roles: any;
  pointFormat: any;
  notFound: boolean = false;
  getallDrop() {
    var datadrop = {
      tId: this.userData.data.data.tenantId,
    };

    this.spinner.show();

    this.feedservice.getallDropdown(datadrop).then(
      (rescompData) => {
        this.spinner.hide();
        this.roles = rescompData["data"].roles;
        this.pointFormat = rescompData["data"].pointFormat;
        if (this.surveySettingData.craditpoints.length > 0) {
          for (let i = 0; i < this.surveySettingData.craditpoints.length; i++) {
            var credit = this.surveySettingData.craditpoints[i];
            this.roleTypeSelected(i, credit);
          }
        }
        console.log("quiz Drop Result", rescompData);
      },
      (resUserError) => {
        this.spinner.hide();
        this.errorMsg = resUserError;
        this.notFound = true;
        // this.router.navigate(['**']);
      }
    );
  }

  // Help Code Start Here //

  helpContent: any;
  getHelpContent() {
    return new Promise((resolve) => {
      this.http1
        .get("../../../../../../assets/help-content/addEditCourseContent.json")
        .subscribe(
          (data) => {
            this.helpContent = data;
            console.log("Help Array", this.helpContent);
          },
          (err) => {
            resolve("err");
          }
        );
    });
    // return this.helpContent;
  }

  passData(val) {
    this.isdata = val;
    // this.eventsSubject.next();
    setTimeout(() => {
      this.isdata = "";
    }, 2000);
  }

  SarchFiltertest(event) {
    this.searchsub = event;
    // console.log('event', event.target.value);
    setTimeout(() => {
      this.searchsub = '';
    }, 2000);
  }

  // Help Code Ends Here //
}
