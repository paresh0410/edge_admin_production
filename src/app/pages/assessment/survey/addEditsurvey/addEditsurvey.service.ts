import {Injectable, Inject} from '@angular/core';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
 import {AppConfig} from '../../../../app.module';
 import { webApi } from '../../../../service/webApi';
 import { HttpClient } from "@angular/common/http";
@Injectable()
export class  AddeditsurveyService {

  public data:any;
  public menuId = null;
  private _url:string = "/api/edge/category/addUpdateCategory" //DevTest
  request: Request;
  surveyId:any;
  getDataforAddedit:any=[];
  private _urlAddeditSurveySetting: string = webApi.domain + webApi.url.addeditsurveysetting;
  private _urlGetroleDropdownSetting: string = webApi.domain + webApi.url.getroledropdownforsurvey;
  private _urlGetSurveyQuestionType:string = webApi.domain + webApi.url.getsurveyquestiontype;
  private _urlAddEditSurveyQuestion:string = webApi.domain + webApi.url.addeditsurveyquestion;
  private _urlGetExistingSurveyQuestion:string = webApi.domain + webApi.url.getexistingsurveyquestions;
  private _urlUpdateSurveyQuestionStatus:string = webApi.domain + webApi.url.changesurveyquestionstatus;
  private _urlGetSurveyQuestionDetails:string = webApi.domain + webApi.url.getsurveyquestiondetails;
  private _urlChangeSurveyQuestionOrder:string = webApi.domain + webApi.url.changesurveyquestionorder;
  private _urlGetUserTypes: string = webApi.domain + webApi.url.getpollusertypes;
  private _urlAreaBulEnrol: string = webApi.domain + webApi.url.area_bulk_enrol;


  constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private _http:Http,private _httpClient:HttpClient){
      //this.busy = this._http.get('...').toPromise();
  }

  createUpdateCategory(category){
    let url:any = `${this.config.FINAL_URL}`+this._url;
    return this._http.post(url,category)
      .map((response:Response) => response.json())
      .catch(this._errorHandler);
  }

  addEditsurveySetting(param){
    return new Promise(resolve => {
      this._httpClient.post(this._urlAddeditSurveySetting, param)
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}

getUserType(param){
  return new Promise(resolve => {
    this._httpClient.post(this._urlGetUserTypes, param)
    .subscribe(data => {
        resolve(data);
    },
    err => {
        resolve('err');
    });
});
}

getSurveydropdown(param){
    return new Promise(resolve => {
      this._httpClient.post(this._urlGetroleDropdownSetting, param)
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}

getSurveyQuestionType(param){
    return new Promise(resolve => {
      this._httpClient.post(this._urlGetSurveyQuestionType, param)
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}

addEditSurveyQuestion(param){
    return new Promise(resolve => {
      this._httpClient.post(this._urlAddEditSurveyQuestion, param)
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}

getExistingSurveyQuestions(param){
    return new Promise(resolve => {
      this._httpClient.post(this._urlGetExistingSurveyQuestion, param)
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}

UpdateSurveyQuestionStatus(param){
    return new Promise(resolve => {
      this._httpClient.post(this._urlUpdateSurveyQuestionStatus, param)
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}

GetSurveyQuestionDetails(param){
  return new Promise(resolve => {
    this._httpClient.post(this._urlGetSurveyQuestionDetails,param)
    .subscribe(data => {
        resolve(data);
    },
    err => {
        resolve('err');
    });
});
}

ChangeSurveyQuestionOrder(param){
  return new Promise(resolve => {
    this._httpClient.post(this._urlChangeSurveyQuestionOrder,param)
    .subscribe(data => {
        resolve(data);
    },
    err => {
        resolve('err');
    });
});
}

areaBulkEnrol(param) {
    return new Promise(resolve => {
        this._httpClient.post(this._urlAreaBulEnrol, param)
            //.map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
                err => {
                    resolve('err');
                });
    });
}

  _errorHandler(error: Response){
    console.error(error);
    return Observable.throw(error || "Server Error")
  }

}
