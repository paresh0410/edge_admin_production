import { Component, ViewEncapsulation,ElementRef, ViewChild } from "@angular/core";
import { LocalDataSource } from "ng2-smart-table";

import {
  Router,
  NavigationStart,
  Routes,
  ActivatedRoute
} from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { surveyService } from "./survey.service";
import { AddeditsurveyService } from "./addEditsurvey/addEditsurvey.service";
// import { questionsService } from './questions/questions.service';
import { BaseChartDirective } from "ng2-charts/ng2-charts";
import { NgxSpinnerService } from "ngx-spinner";
// import { ToasterModule, ToasterService, Toast } from "angular2-toaster";
import { ToastrService } from 'ngx-toastr';
// import { ContentService } from '../content/content.service';
import { DatePipe } from '@angular/common';
import { webApi } from "../../../service/webApi";
import { CommonFunctionsService } from "../../../service/common-functions.service";
import { Card } from "../../../models/card.model";
import { SuubHeader } from "../../components/models/subheader.model";
import { noData } from "../../../models/no-data.model";
@Component({
  selector: "survey",
  styleUrls: ["./survey.scss"],
  // template:'category works'
  templateUrl: "./survey.html",
  providers: [DatePipe],
  encapsulation: ViewEncapsulation.None
})
export class survey {
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  notFound: boolean = false;
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No Survey defined at this time.",
    desc:"Surveys will appear after they are designed by the instructor. Surveys are a set of pre-defined questions aimed to collect information from learners.",
    titleShow:true,
    btnShow:true,
    descShow:true,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/reaction-how-to-create-a-survey',
  }
  cardModify: Card = {
    flag: 'survey',
    titleProp : 'sName',
    discrption: 'sdesc',
    question: 'noofquestion',
    compleCount: 'completedCnt',
    totalCount: 'enrolCnt',
    image: 'iconRef',
    endDate: 'closedate',
    date: 'opendate',
    hoverlable:   true,
    showBottomEnddate : true,
    showBottomQuestion: true,
    showCompletedEnroll : true,
    showBottomdate:   true,
    showImage: true,
    option:   true,
    copyIcon:   true,
    eyeIcon:   true,
    bottomDiv:   true,
    bottomDiscription:   true,
   
    showBottomList:   true,
    bottomTitle:   true,
    btnLabel: 'Edit',
    defaultImage: 'assets/images/survey.png'

  };
  count:number=8
  query: string = "";
  public getData;

  cat: any;
  survey: any = [];

  errorMsg: string;

  // parentcat:any=[]

  // content1:any=[];

  search: any;
  noSurvey: boolean = false;
  skeleton = false;
  Nodatafound: boolean = false;
  rows: any;
  userId: any;
  userData;
  header: SuubHeader  = {
    title:'Survey',
    btnsSearch: true,
    placeHolder:'Search by survey name',
    searchBar: true,
    dropdownlabel: ' ',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: '',
    btnName3: '',
    btnAdd: 'Add Survey',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: true,
    filter: false,
    showBreadcrumb: true,
    breadCrumbList:[
      {
        'name': 'Reaction',
        'navigationPath': '/pages/reactions',
      },
    ]
  };
  currentPage: number;
  str: string;
  title: string;
  //protected service: CCategorydataService,
  constructor(
    protected serveyService: surveyService,
    private commonFunctionService: CommonFunctionsService,
    protected addeditsurveyService: AddeditsurveyService,
    private router: Router,private datePipe: DatePipe,
    // private toasterService: ToasterService, 
    private toastr: ToastrService,
    private spinner: NgxSpinnerService
  ) {
    // this.loader = true;
    if (localStorage.getItem("LoginResData")) {
      this.userData = JSON.parse(localStorage.getItem("LoginResData"));
      console.log("userData", this.userData.data);
      this.userId = this.userData.data.data.id;
    }
    this.getSurveys();
    this.search = {
      sName: ""
    };
    // this.category;
    this.cat = {
      id: ""
    };



    // this.getCategories();
  }

  presentToast(type, body) {
    if(type === 'success'){
      this.toastr.success(body, 'Success', {
        closeButton: false
       });
    } else if(type === 'error'){
      this.noSurvey = true;
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else{
      this.toastr.warning(body, 'Warning', {
        closeButton: false
        })
    }
  }

  getUnique(arr, comp) {
    //store the comparison  values in array
    const unique = arr
      .map(e => e[comp])
      // store the keys of the unique objects
      .map((e, i, final) => final.indexOf(e) === i && i)
      // eliminate the dead keys & return unique objects
      .filter(e => arr[e])
      .map(e => arr[e]);

    return unique;
  }

  getSurveys() {
    // this.spinner.show();
    let param = {
      tenantId: this.userData.data.data.tenantId,
    };
    const _urlFetch:string = webApi.domain + webApi.url.fetchallsurvey;
    this.commonFunctionService.httpPostRequest(_urlFetch,param)
    // this.serveyService.getServey(param)
    .then(
      rescompData => {
        this.spinner.hide();
        this.skeleton = true;
        var result = rescompData;
        console.log("getAllSurveys:", rescompData);
        if (result["type"] == true) {
          if (result["data"][0].length == 0) {
            this.noSurvey = true;
            this.skeleton = true;
          } else {
            var rows = result["data"][0];
            this.skeleton = true;

            if (rows) {
              this.rows = this.getUnique(rows, "sid");
              for (let j = 0; j < this.rows.length; j++) {
                this.rows[j].opendate = this.formdate(this.rows[j].oDate);
                this.rows[j].closedate = this.formdate(this.rows[j].cDate);
                this.rows[j].points = [];
                for (let i = 0; i < rows.length; i++) {
                  if (this.rows[j].sid == rows[i].sid) {
                    if (rows[i].roleId) {
                      var data = {
                        crid: 0,
                        roleId: rows[i].roleId,
                        pformatid: rows[i].dimension,
                        bcpoints: rows[i].bcPoints,
                        acpoitnts: rows[i].acPoints
                      };
                      this.rows[j].points.push(data);
                    }
                  }
                }
              }
            }
            console.log("this.rows", this.rows);
            this.displayArray =[]
            this.addItems(0,this.sum,'push',this.rows);
          }
        } else {
          this.Nodatafound = true;
          this.skeleton = true;
          this.noSurvey = true;
          // var toast: Toast = {
          //   type: "error",
          //   //title: "Server Error!",
          //   body: "Something went wrong.please try again later.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }
      },
      error => {
        this.spinner.hide();
        this.skeleton = true;
        this.Nodatafound = true;
        // var toast: Toast = {
        //   type: "error",
        //   //title: "Server Error!",
        //   body: "Something went wrong.please try again later.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      }
    );
  }
  // getCategories(){
  //   this.categoryService.getCategories()
  //   .subscribe(rescompData => {
  //     this.loader =false;
  //     this.category = rescompData.data[0];
  //     console.log('Category Result',this.category)
  //   },
  //   resUserError=>{
  //     this.loader =false;
  //     this.errorMsg = resUserError;
  //     this.notFound = true;
  //     // this.router.navigate(['**']);
  //   });
  // }

  clear() {
    this.header.searchtext = ''
    this.search = {};
    this.str = '';
    this.currentPage = 1;
    this.getSurveys();
    this.search = {};
  }

  back() {
    this.router.navigate(["/pages/reactions"]);
  }

  content2(data, id) {
    var passData = {
      id: id,
      data: data
    };
    // this.questionsService.data = passData;
    this.router.navigate(["/pages/reactions/survey/add-edit-survey"]);
    // this.passService.data = {};
  }
  delatecategory(ind) {
    for (let i = 0; i < this.survey.length; i++) {
      if (ind == i) {
        this.survey.splice(i, 1);
      }
    }
  }

  addeditsurvey(data, id) {
    console.log(data);
    if (id == 0) {
      var action = "ADD";
      this.addeditsurveyService.surveyId = 0;
      this.addeditsurveyService.getDataforAddedit[0] = action;
      this.addeditsurveyService.getDataforAddedit[1] = "";
      this.router.navigate(["/pages/reactions/survey/add-edit-survey"]);
    } else if (id == 1) {
      var action = "EDIT";
      this.addeditsurveyService.surveyId = data.sid;
      this.addeditsurveyService.getDataforAddedit[0] = action;
      this.addeditsurveyService.getDataforAddedit[1] = data;
      console.log(
        "addeditsurveyService.getDataforAddedit[1]:",
        this.addeditsurveyService.getDataforAddedit[1]
      );

      this.router.navigate(["/pages/reactions/survey/add-edit-survey"]);
    }
  }

  gotoCardaddedit(data) {
    var action = "EDIT";
      this.addeditsurveyService.surveyId = data.sid;
      this.addeditsurveyService.getDataforAddedit[0] = action;
      this.addeditsurveyService.getDataforAddedit[1] = data;
      this.router.navigate(["/pages/reactions/survey/add-edit-survey"]);
  }
  disableCourse: boolean = false;
  disableContent(item, disableStatus) {
    this.disableCourse = !this.disableCourse;
  }
  btnName:string='Yes'
  disableSurvey(currentIndex, surveyData, status) {
    this.enableDisableSurveyData = surveyData;
    console.log("this.enableDisableSurveyData", this.enableDisableSurveyData);
    this.enableDisableSurveyModal = true;
    this.surveyDisableIndex = currentIndex;

    if (this.enableDisableSurveyData.visible == 0) {
      this.enableSurvey = false;
    } else {
      this.enableSurvey = true;
      
    }
  }

  // clickTodisable(surveyData) {
  //   if (surveyData.visible == 1) {
  //     this.enableSurvey = true;
  //     this.title="Disable Survey"
  //     this.enableDisableSurveyData = surveyData;
  //     console.log('this.enableDisableSurveyData', this.enableDisableSurveyData);
  //     this.enableDisableSurveyModal = true;
  //   } else {
  //     this.enableSurvey = false;
  //     this.title="Enable Survey"
  //     this.enableDisableSurveyData = surveyData;
  //     console.log('this.enableDisableSurveyData', this.enableDisableSurveyData);
  //     this.enableDisableSurveyModal = true;
  //   }
  // }
  surveyDisableIndex: any;
  enableSurvey: boolean = false;
  enableDisableSurveyModal: boolean = false;
  enableDisableSurveyData: any;
  // enableDisableSurveyAction(actionType) {
  //   if (actionType == true) {
  //     if (this.enableDisableSurveyData.visible == 1) {
  //       this.enableDisableSurveyData.svisible = 0;
  //       var catData = this.enableDisableSurveyData;
  //       this.enableDisableSurvey(catData);
  //     } else {
  //       this.enableDisableSurveyData.svisible = 1;
  //       var catData = this.enableDisableSurveyData;
  //       this.enableDisableSurvey(catData);
  //     }
  //     this.closeEnableDisableSurveyModal();
  //   } else {
  //     //this.enableDisableCategoryData.visible = 0;
  //     this.closeEnableDisableSurveyModal();
  //   }
  // }

  enableDisableSurvey(data,i) {
    this.spinner.show();
    console.log("data", data);
    let param = {
      surveyId: data.sid,
      svisible: data.visible==0?1:0
    };
    if(this.displayArray[i].visible==0){
      this.displayArray[i].visible=1
    }else{
      this.displayArray[i].visible=0
    }
    const _urlChangeSurveyStatus:string = webApi.domain + webApi.url.changesurveystatus;
    this.commonFunctionService.httpPostRequest(_urlChangeSurveyStatus,param)
    // this.serveyService
    //   .changeServeyStatus(param)

      .then(
        rescompData => {
          this.spinner.hide();
          var result = rescompData;
          console.log("UpdateSurveyResponse:", rescompData);
          if (result["type"] == true) {
            // var catUpdate: Toast = {
            //   type: "success",
            //   title: "Survey Updated!",
            //   body: "Survey updated successfully.",
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(catUpdate);
            // this.presentToast('success', 'Survey updated');
            if(param.svisible==1){      
              this.presentToast( 'success','Survey Enabled Successfully');
             }else{
               this.presentToast('success','Survey Disabled Successfully');
             }
            // this.enableDisableSurveyData.visible = data.svisible;
          } else {
            // var catUpdate: Toast = {
            //   type: "error",
            //   title: "Survey Updated!",
            //   body: "Unable to update survey.",
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(catUpdate);
            this.presentToast('error', '');
          }
        },
        error => {
          this.spinner.hide();
          // var toast: Toast = {
          //   type: "error",
          //   //title: "Server Error!",
          //   body: "Something went wrong.please try again later.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }
      );
  }

  // closeEnableDisableSurveyModal() {
  //   this.enableDisableSurveyModal = false;
  // }

  surveyDup: boolean = false;
  duplicate: any = {
    sName: "",
    sDesc: ""
  };
  dupData: any = {};
  dupSurvey(dupData) {
    this.surveyDup = true;
    this.dupData = dupData;
    console.log("dupData:", dupData);
  }

  copyCard(dupData) {
    this.title='Duplicate Survey'
    this.btnName='Save'
    this.surveyDup = true;
    this.dupData = dupData;
    console.log("dupData:", dupData);
  }
  newSurveyDup: any = {};
  saveDupSurvey(data) {
    console.log("DuplicateData:", data);
    this.newSurveyDup = {
      newSurveyName: data.sName,
      newSurveyDesc: data.sDesc
    };
    this.surveyDup = false;
    console.log("DuplicateData2:", this.newSurveyDup);
    this.DuplicateSurvey();
  }

  DuplicateSurvey() {
    this.spinner.show();
    var option: string = Array.prototype.map
      .call(this.dupData.points, function(item) {
        console.log("item", item);
        return Object.values(item).join("|");
      })
      .join("#");
    let param = {
      surveyId: this.dupData.sid,
      userId: this.userId,
      surveyName: this.newSurveyDup.newSurveyName,
      description: this.newSurveyDup.newSurveyDesc,
      tenantId: this.userData.data.data.tenantId,
    };
    console.log("dupParam", param);
    const _urlCopySurvey:string = webApi.domain + webApi.url.copysurvey;
    this.commonFunctionService.httpPostRequest(_urlCopySurvey,param)
    //this.serveyService.copySurvey(param)
    .then(
      rescompData => {
        this.duplicate = {};
        this.spinner.hide();
        var temp = rescompData;
        var surveyAddEditRes = temp["data"];
        console.log("Survey Add Result ", surveyAddEditRes);
        if (temp["type"] == true) {
          console.log("Survey Add Result ", surveyAddEditRes);
          // var certUpdate: Toast = {
          //   type: "success",
          //   title: "Survey Inserted!",
          //   body: "New survey added successfully.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(certUpdate);
          this.presentToast('success', 'Survey added');
          this.getSurveys();
        } else {
          // var certUpdate: Toast = {
          //   type: "success",
          //   title: "Survey Inserted!",
          //   body: "Unable to add survey.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(certUpdate);
          this.presentToast('error', '');
          this.getSurveys();
        }
        this.spinner.hide();
        this.router.navigate(["/pages/reactions/survey"]);
      },
      resUserError => {
        // this.loader =false;
        this.errorMsg = resUserError;
        this.spinner.hide();
        this.router.navigate(["/pages/reactions/survey"]);
      }
    );
  }

  closeDuplicateModel() {
    this.duplicate = {};
    this.surveyDup = false;
  }
  formdate(date) {

    if (date) {
      // const months = ["JAN", "FEB", "MAR","APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
      // var day = date.getDate();
      // var monthIndex = months[date.getMonth()];
      // var year = date.getFullYear();
      // return day + '_' + monthIndex + '_' +year;
      var formatted = this.datePipe.transform(date, 'dd-MM-yyyy');
      return formatted;
    }
  }

  onSearch(event){
    this.search.sName = event.target.value

  }
  ngOnDestroy(){
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0){
      e1[0].classList.add('with-scroll');
    }
  }
  ngAfterContentChecked() {
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0)
    {
      e1[0].classList.remove('with-scroll');
    }

   }
   conentHeight = '0px';
   offset: number = 100;
   ngOnInit() {
     this.conentHeight = '0px';
     const e = document.getElementsByTagName('nb-layout-column');
     console.log(e[0].clientHeight);
     // this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
     if (e[0].clientHeight > 1300) {
       this.conentHeight = '0px';
       this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
     } else {
       this.conentHeight = String(e[0].clientHeight) + 'px';
     }
   }
 
   sum = 40;
   addItems(startIndex, endIndex, _method, asset) {
     for (let i = startIndex; i < endIndex; ++i) {
       if (asset[i]) {
         this.displayArray[_method](asset[i]);
       } else {
         break;
       }
 
       // console.log("NIKHIL");
     }
   }
   // sum = 10;
   onScroll(event) {
 
     let element = this.myScrollContainer.nativeElement;
     // element.style.height = '500px';
     // element.style.height = '500px';
     // Math.ceil(element.scrollHeight - element.scrollTop) === element.clientHeight
 
     if (element.scrollHeight - element.scrollTop - element.clientHeight < 5) {
       if (this.newArray.length > 0) {
         if (this.newArray.length >= this.sum) {
           const start = this.sum;
           this.sum += 50;
           this.addItems(start, this.sum, 'push', this.newArray);
         } else if ((this.newArray.length - this.sum) > 0) {
           const start = this.sum;
           this.sum += this.newArray.length - this.sum;
           this.addItems(start, this.sum, 'push', this.newArray);
         }
       } else {
         if (this.rows.length >= this.sum) {
           const start = this.sum;
           this.sum += 50;
           this.addItems(start, this.sum, 'push', this.rows);
         } else if ((this.rows.length - this.sum) > 0) {
           const start = this.sum;
           this.sum += this.rows.length - this.sum;
           this.addItems(start, this.sum, 'push', this.rows);
         }
       }
 
 
       console.log('Reached End');
     }
   }
   newArray =[];
   displayArray =[];
    // this function is use to dynamicsearch on table
  searchData(event) {
    this.noDataVal={
      margin:'mt-5',
      imageSrc: '../../../../../assets/images/no-data-bg.svg',
      title:"Sorry we couldn't find any matches please try again",
      desc:".",
      titleShow:true,
      btnShow:false,
      descShow:false,
      btnText:'Learn More',
      btnLink:''
    }
    var temData = this.rows;
    var val = event.target.value.toLowerCase();
    var keys = [];
    if(event.target.value == '')
    {
      this.displayArray =[];
      this.newArray = [];
      this.sum = 50;
      this.addItems(0, this.sum, 'push', this.rows);
    }else{
         // filter our data
         if(val.length>=3||val.length==0){
    // if (temData.length > 0) {
    //   keys = Object.keys(temData[0]);
    // }
    // var temp = temData.filter((d) => {
    //   for (const key of keys) {
    //     if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
    //       return true;
    //     }
    //   }
    // });
    const temp = this.rows.filter(function (d) {
      return String(d.sName).toLowerCase().indexOf(val) !== -1 ||
        d.sdesc.toLowerCase().indexOf(val) !== -1 ||
        // d.relayDate.toLowerCase().indexOf(val) !== -1 ||
        // d.manager.toLowerCase().indexOf(val) !== -1 ||
        !val
    })
    // update the rows
    this.displayArray =[];
    this.newArray = temp;
    this.sum = 50;
    this.addItems(0, this.sum, 'push', this.newArray);
  }
}
}
}
