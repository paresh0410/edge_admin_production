import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MyDatePickerModule } from 'mydatepicker';
import { FilterPipeModule  } from 'ngx-filter-pipe';
import { survey } from './survey.component';
import { surveyService } from './survey.service';
// import { ContentService } from '../content/content.service';
import { AddeditsurveyService } from './addEditsurvey/addEditsurvey.service';
import { NotFoundComponent } from './not-found/not-found.component';
import { ThemeModule } from '../../../@theme/theme.module';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgxContentLoadingModule } from 'ngx-content-loading';
import { ComponentModule } from '../../../component/component.module';
// import { MiscellaneousRoutingModule, routedComponents } from './miscellaneous-routing.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FilterPipeModule,
    ThemeModule,
    NgxSkeletonLoaderModule,
    NgxContentLoadingModule,
    ComponentModule,
  ],
  declarations: [
    survey,
    NotFoundComponent
  ],
  providers: [
    surveyService,
    AddeditsurveyService,
    // qContentService
  ]
})

export class surveyModule {}
