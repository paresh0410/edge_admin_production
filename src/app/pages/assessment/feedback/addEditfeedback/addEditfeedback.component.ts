import { Component, Directive, ViewEncapsulation, forwardRef, Attribute, OnChanges, SimpleChanges, Input, ViewChild, ViewContainerRef } from '@angular/core';
import { AddeditfeedbackService } from './addEditfeedback.service';
// import { NG_VALIDATORS, Validator, Validators, AbstractControl, ValidatorFn } from '@angular/forms';
// import { LocalDataSource } from 'ng2-smart-table';
import { Router, NavigationStart, ActivatedRoute } from '@angular/router';
import { OwlDateTimeModule } from 'ng-pick-datetime';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { SortablejsOptions } from 'angular-sortablejs';
import { NgxSpinnerService } from 'ngx-spinner';
import { webAPIService } from '../../../../service/webAPIService'
import { CommonFunctionsService } from '../../../../service/common-functions.service';
import { webApi } from '../../../../service/webApi'
import { HttpClient } from "@angular/common/http";
import { ToastrService } from 'ngx-toastr';
import * as _moment from 'moment';
import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';
import { SuubHeader } from '../../../components/models/subheader.model';
const moment = (_moment as any).default ? (_moment as any).default : _moment;
import { DataSeparatorService } from '../../../../service/data-separator.service';
import { noData } from '../../../../models/no-data.model';

export const MY_CUSTOM_FORMATS = {
  fullPickerInput: 'DD-MM-YYYY h:mm:ss A',
  parseInput: 'DD-MM-YYYY h:mm:ss A',
  datePickerInput: 'DD-MM-YYYY',
  timePickerInput: 'LT',
  monthYearLabel: 'MMM YYYY',
  dateA11yLabel: 'LL',
  monthYearA11yLabel: 'MMMM YYYY'
};
@Component({
  selector: 'addEditfeedback',
  templateUrl: './addEditfeedback.html',
  styleUrls: ['./addEditfeedback.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    { provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE] },
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS }],
})

export class Addeditfeedback {
  @ViewChild('manualTable') manualTable: any;
  @ViewChild(DatatableComponent) tableDataManual: DatatableComponent;
  @ViewChild('myTable') table: any;
  header: SuubHeader;

  query: string = '';
  public getData;
  QuestionListOptions: SortablejsOptions = {
    group: {
      name: 'questions',

    },
    sort: true,
    // draggable: '.draggable',
    // onEnd: this.onListItemDragEnd.bind(this),
    handle: '.drag-handle',
    // draggable: '.draggable'
  };

  QuestionListOptions1: SortablejsOptions = {
    group: {
      name: 'questions1',

    },
    sort: true,
    // draggable: '.draggable',
    // onEnd: this.onListItemDragEnd.bind(this),
    handle: ".drag-handle"
    // draggable: '.draggable'
  };
  secTitle:string='Add Section'
  quesTitle:string='Add Question'
	btnName: string = 'Save';
  AttemptedUsers: any = [];
  rows: any[] = [];
  rows1: any = [];
  expanded: any = {};
  timeout: any;
  questionarray: any = [];
  Questions: boolean = false;
  Settings: boolean = true;
  Responses: boolean = false;
  Analysis: boolean = false;
  questionshow: boolean = false;
  questionshow1: boolean = false;
  questionshow2: boolean = false;
  noDataVal:noData={
    margin:'',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No section added under feedback.",
    desc:"",
    titleShow:true,
    btnShow:true,
    descShow:false,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/rection-how-to-add-a-question-to-a-feedback',
  }

  title: any;
  formdata: any;
  profileUrl: any = 'assets/images/category.jpg';
  result: any;
  errorMsg: any;
  random: any;

  qdata = {
    queaddtype: '',
    quetype: '',
  }


  questiontype = [];
  // rows:any=[];
  selected: any = [];

  questiondata: any = {
    fqid: '',
    fqname: '',
    fmand: '',
    fqtypeid: '',
    fansmin: '',
    fansmax: '',
    fqtypeName: '',
    fqdata: []

  }


  dragableList = [];
  config = {
    height: '200px',
    uploader: {
      insertImageAsBase64URI: true,
    },
    allowResizeX: false,
    allowResizeY: false,
    placeholder: 'Question Name',
    limitChars: 3000,
    toolbarSticky: false,
  };
  analyticsData = [
    {
      aqid: 1,
      aqname: 'Question 1',
      aqfequency: [{
        option: 10
      }, {
        option: 20
      },
      {
        option: 40
      }],
      aqmean: 35,
      aqsd: '12.472191289246471285'

    },
    {
      aqid: 2,
      aqname: 'Question 2',
      aqfequency: [
        {
          option: 10
        },
        {
          option: 2
        },
        {
          option: 400
        },
        {
          option: 90,
        }
      ],
      aqmean: 125,
      aqsd: '162.17505973484332279'

    },
    {
      aqid: 3,
      aqname: 'Question 3',
      aqfequency: [{
        option: 1
      }, {
        option: 199
      },
      {
        option: 400
      }],
      aqmean: 200,
      aqsd: '162.89260265586034279'

    },
    {
      aqid: 4,
      aqname: 'Question 4',
      aqfequency: [{
        option: 10
      }, {
        option: 90
      },
      {
        option: 40
      }],
      aqmean: 46,
      aqsd: '32.998316455372217806'

    },
    {
      aqid: 5,
      aqname: 'Question 5',
      aqfequency: [{
        option: 2
      }, {
        option: 200
      },
      ],
      aqmean: 101,
      aqsd: '99'

    },
  ]

  responceData = [
    {
      rid: 1,
      runame: 'Vivek',
      rdate: '1/26/2019, 7:45 PM',
      rque: [
        {
          que: 1,
          quename: 'Question 1',
          queoption: 'option option option option ',
          v: ''
        },
        {
          que: 2,
          quename: 'Question 2',
          queoption: 'Agree ',
          queoptid: 1,
        },
        {
          que: 3,
          quename: 'Question 3',
          queoption: 'Disagree',
          queoptid: 3,
        },
      ]

    },
    {
      rid: 2,
      runame: 'Ronit',
      rdate: '1/28/2019, 7:45 PM',
      rque: [
        {
          que: 1,
          quename: 'Question 1',
          queoption: 'option option option option ',
          queoptid: ''
        },
        {
          que: 2,
          quename: 'Question 2',
          queoption: 'Agree ',
          queoptid: 1,
        },
        {
          que: 3,
          quename: 'Question 3',
          queoption: 'Disagree',
          queoptid: 3,
        },
      ]

    },
    {
      rid: 3,
      runame: 'Chetan',
      rdate: '1/20/2019, 7:45 PM',
      rque: [
        {
          que: 1,
          quename: 'Question 1',
          queoption: 'option option option option ',
          queoptid: ''
        },
        {
          que: 2,
          quename: 'Question 2',
          queoption: 'Agree ',
          queoptid: 1,
        },
        {
          que: 3,
          quename: 'Question 3',
          queoption: 'Disagree',
          queoptid: 3,
        },
      ]

    },
    {
      rid: 4,
      runame: 'Bhavesh',
      rdate: '1/19/2019, 7:45 PM',
      rque: [
        {
          que: 1,
          quename: 'Question 1',
          queoption: 'option option option option ',
          queoptid: ''
        },
        {
          que: 2,
          quename: 'Question 2',
          queoption: 'Agree ',
          queoptid: 1,
        },
        {
          que: 3,
          quename: 'Question 3',
          queoption: 'Disagree',
          queoptid: 3,
        },
      ]

    },
  ]




  columns = [
    {
      prop: 'selected',
      name: '',
      sortable: false,
      canAutoResize: false,
      draggable: false,
      resizable: false,
      headerCheckboxable: true,
      checkboxable: true,
      width: 30
    },
    { prop: 'qsname', name: 'Name' },
    { prop: 'qpoint', name: 'Point' },
    { prop: 'qtype', name: 'Type' },
  ];

  catData: any = undefined;
  temp1 = []
  surveyQue: any = [];
  userId: any;
  userlogData: any;
  userData: any;
  issec = false;
  isque = false;
  isSave: boolean = false;
  category: any;
  enableQues: boolean=false;
  item: any;
  noData: boolean=false;
  constructor(private spinner: NgxSpinnerService,
    private http1: HttpClient, private toastr: ToastrService,
    protected service: AddeditfeedbackService, protected webApiService: webAPIService,
    private dataSeparatorService: DataSeparatorService,
    // private toasterService: ToasterService,
    private router: Router,
    private commonFunctionService: CommonFunctionsService,
    private route: ActivatedRoute) {

    this.getHelpContent();

    if (localStorage.getItem('LoginResData')) {
      this.userlogData = JSON.parse(localStorage.getItem('LoginResData'));
      // console.log('userData',this.userData.data);
      this.userData = this.userlogData.data.data;
      this.userId = this.userData.id;
    }
    this.getSurveyQuestionType();
    this.getPollUserTypes();


    // this.rows=this.questions;
    // console.log("Add Category Called");
    var category;
    var id;
    this.rows = this.responceData
    this.temp1 = this.rows;
    this.getallDrop();
    if (this.service.data != undefined) {
      category = this.service.data.data;
      id = this.service.data.id;
      this.catData = this.service.data.data;
    } else {
      id = 0;
    }
    console.log("Service Data:", this.service.data);
    console.log('Category Info', category);
    this.category=category;
    console.log('id', id);

    if (id == 1) {
      this.title = 'Edit Feedback';
      this.formdata = {
        fid: category.fid,
        fname: category.fname,
        fimg: category.fimg,
        fdesc: category.fdesc,
        fotime: category.oDate,
        fctime: category.cDate,
        points: category.points,
        fque: category.fque,
        recordUsers: category.recordUsers
      }

      this.questionarray = this.formdata.qsque;
    } else {
      this.title = 'Add Feedback';
      this.formdata = {

        fid: '',
        fname: '',
        fimg: 'assets/images/category.jpg',
        fdesc: '',
        fotime: '',
        fctime: '',
        recordUsers: '',
        points:
          [
            {
              crid: 0,
              roleId: '',
              pformatid: '',
              bcpoints: '',
              acpoints: '',
            }
          ],
        // flcbpoints:'',
        // flapoints:'',
        // fmcbpoints:'',
        // fmcapoints:'',
        fque: [
          // {
          //   fqid:'',
          //   fqname:'' ,
          //   fmand:'',
          //   fqtypeid:'',
          //   fqtypeName:'',
          //   fqdata:[]

          // }
        ]


      }
    }
    // this.QuestionListOptions = {
    //   onUpdate: (event: any) => {
    //     //this.submitNewSrveyQue();
    //     console.log('main',event)
    //   }
    // };
    //      this.QuestionListOptions1 = {
    //       onRemove: function (/**Event*/evt) {
    //         console.log('onRemove',evt)
    //       },
    // };
    // this.QuestionListOptions1 = {
    //   onStart: function (event) {
    //     event.oldIndex;  // element index within parent
    //     console.log('onStart',event)
    //   },
    // };
    // this.QuestionListOptions1 = {
    //   onEnd: function (/**Event*/evt) {
    //     var itemEl = evt.item;  // dragged HTMLElement
    //     evt.to;    // target list
    //     evt.from;  // previous list
    //     evt.oldIndex;  // element's old index within old parent
    //     evt.newIndex;  // element's new index within new parent
    //     evt.oldDraggableIndex; // element's old index within old parent, only counting draggable elements
    //     evt.newDraggableIndex; // element's new index within new parent, only counting draggable elements
    //     evt.clone // the clone element
    //     evt.pullMode;  // when item is in another sortable: `"clone"` if cloning, `true` if moving
    //     console.log('onEnd',evt)
    //   },
    // };

    // this.getExistingFeedbackQuestion();
  }

  back() {
    this.router.navigate(['/pages/reactions/feedback']);
  }

  readUrl(event: any) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.onload = (event: ProgressEvent) => {
        this.profileUrl = (<FileReader>event.target).result;
      }
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  deleteProfile() {
    this.profileUrl = 'assets/images/category.jpg';
  }



  Removeque(fid) {

    for (let i = 0; i < this.formdata.fque.length; i++) {
      if (fid == i) {
        this.formdata.fque.splice(i, 1)
      }
    }

  }

  addandswers() {
    var qdata = {}
    this.questiondata.fqdata.push(qdata)
  }

  qtypechange(j) {
    if (this.questiondata.fqdata.length > 0) {
      this.questiondata.fqdata = []
    }
    if (this.questiondata.fqdata.length == 0) {
      var qdata = {
        qaid: '',
        qans: ''
      }
      this.questiondata.fqdata.push(qdata)
    }


  }

  removeans(aid) {

    for (let i = 0; i < this.questiondata.fqdata.length; i++) {
      if (aid == i) {
        this.questiondata.fqdata.splice(i, 1)
      }
    }

  }

  userTypes: any = [];
  getPollUserTypes() {
    let param = {
      "tenantId": this.userData.tenantId,
      //iId:this.service.data.data.fid
    }

    this.service.getFeedbackUserType(param)
      .then(rescompData => {
        // this.spinner.hide();
        let result = rescompData;

        this.userTypes = result['data'][0];

        console.log('this.userTypes', this.userTypes)
      },
        resUserError => {
          // this.spinner.hide();
          this.errorMsg = resUserError;
          // this.notFound = true;
          // this.router.navigate(['**']);
        });
  }
  // savequestion(data)
  // {
  //   console.log('data',data);
  //   for(let i=0;i<this.formdata.fque.length;i++)
  //   {
  //     if(this.formdata.fque[i].fqid==data.fqid)
  //     {
  //       this.formdata.fque.splice(i,1);

  //     }
  //   }
  //    this.formdata.fque.push(data);

  //   this.questionshow =false;
  //   this.questiondata={
  //               fqid:'',
  //               fqname:'' ,
  //               fmand:'',
  //               fqtypeid:'',
  //               fqtypeName:'',
  //               fqdata:[]

  //             };
  // }

  // editQuestion(item)
  // {
  //   console.log('item',item)
  //   this.questionshow =true;
  //   this.questiondata={
  //               fqid:item.fqid,
  //               fqname:item.fqname,
  //               fmand:item.fmand,
  //               fqtypeid:item.fqtypeid,
  //               fqtypeName:item.fqtypeName,
  //               fqdata:item.fqdata

  //             };
  // }

  save(qaddtype, qtype) {
    var questionarray = this.questionarray
    this.questionshow = false;
    if (qaddtype == 1) {
      if (this.selected.length > 0) {
        for (let j = 0; j < questionarray.length; j++) {
          for (let i = 0; i < this.selected.length; i++) {
            if (this.selected[i].qsid == questionarray[j].qsid) {
              this.selected.splice(i, 1)
            }
            // this.questionarray.push(this.selected[i])
          }
        }
        for (let i = 0; i < this.selected.length; i++) {
          this.questionarray.push(this.selected[i])
        }

      }
    }
    else
      if (qaddtype == 2) {

        // this.random.qtype=
        this.selected = [];
        this.questionarray.push(this.random)
      }



  }
  // Qtypechange(qdata)
  // {
  //   if(qdata ==2)
  //   {
  //       this.random={
  //          qsid:'',
  //         qsname:'Random',
  //         qpoint:'',
  //         qtype:'',
  //       }
  //   }
  // }

  closeModel() {
    this.questionshow = false;
    this.questionshow1 = false;
    this.questionshow2 = false;
    this.issec = false;
    this.isque = false;
  }


  Backquestion() {
    this.questionshow = false;
  }
  // onSelect({ selected }) {
  //   console.log('Select Event', selected, this.selected);

  //   this.selected.splice(0, this.selected.length);
  //   this.selected.push(...selected);
  // }
  //  onActivate(event) {
  //   console.log('Activate Event', event);
  // }


  onDetailToggle(event) {
    console.log('Detail Toggled', event);
  }
  onPage(event) {
    // clearTimeout(this.timeout);
    // this.timeout = setTimeout(() => {
    //   // console.log('paged!', event);
    // }, 100);
  }

  onSelect({ selected }) {
    console.log('Selected Event', selected);
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    // console.log('Selected Data',this.selected);
    // this.userData = this.selected;
    // if(this.selected.length > 0){
    //   this.enrollDisabled = false;
    // }else{
    //   this.enrollDisabled = true;
    // }
  }

  onActivate(event) {
    // console.log('Activate Event', event);
    // if(event.type == "click"){
    //   // console.log('Row CLick Event', event);
    //   console.log('Row Data',event.row);
    //   this.showLargeModal();
    // }
  }

  displayCheck(row) {
    // return row.name !== 'Ethel Price';
  }
  toggleExpandRow(row) {
    console.log('Toggled Expand Row!', row);
    this.table.rowDetail.toggleExpandRow(row);
    this.AttemptedUsers = row.rque;
  }


  searchfeedbackUser(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp1.filter(function (d) {
      return d.runame.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }
  feedImgData: any;
  readCategoryThumb(event: any) {
    var validExts = new Array(".png", ".jpg", ".jpeg");
    var fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
      // var toast : Toast = {
      //     type: 'error',
      //     title: "Invalid file selected!",
      //     body: "Valid files are of " + validExts.toString() + " types.",
      //     showCloseButton: true,
      //     // tapToDismiss: false,
      //     timeout: 2000
      //     // onHideCallback: () => {
      //     //     this.router.navigate(['/pages/plan/users']);
      //     // }
      // };
      // this.toasterService.pop(toast);

      this.presentToast('warning', 'Valid file types are ' + validExts.toString());
      // this.deleteCourseThumb();
    } else {
      if (event.target.files && event.target.files[0]) {
        this.feedImgData = event.target.files[0];
        this.formdata.qfileName = event.target.files[0].name

        var reader = new FileReader();

        reader.onload = (event: ProgressEvent) => {
          // this.defaultThumb = (<FileReader>event.target).result;
          this.formdata.fimg = (<FileReader>event.target).result;
        }
        reader.readAsDataURL(event.target.files[0]);
      }
    }
  }

  deleteCategoryThumb() {
    // this.defaultThumb = 'assets/images/category.jpg';
    this.formdata.fimg = 'assets/images/category.jpg';
    this.feedImgData = undefined;
    this.formdata.categoryPicRefs = undefined;
    // this.formdata.qfileName=''
  }


  roles: any;
  pointFormat: any;
  notFound: boolean = false;
  getallDrop() {
    var datadrop = {
      tId: this.userData.tenantId,
    }

    this.spinner.show();

    this.service.getallDropdown(datadrop)
      .then(rescompData => {
        this.spinner.hide();
        this.roles = rescompData['data'].roles;
        this.pointFormat = rescompData['data'].pointFormat;
        if (this.formdata.points.length > 0) {
          for (let i = 0; i < this.formdata.points.length; i++) {
            var credit = this.formdata.points[i];
            this.roleTypeSelected(i, credit);
          }
        }
        console.log('quiz Drop Result', rescompData)
      },
        resUserError => {
          this.spinner.hide();
          this.errorMsg = resUserError;
          this.notFound = true;
          // this.router.navigate(['**']);
        });
  }
  removePointsList(i: number) {
    // this.credits.removeAt(i);
    // this.credits.splice(i,1);
    this.formdata.points.splice(i, 1);
    this.roleSelectedList.splice(i, 1);
    this.selectedRole.splice(i, 1);
    this.addPointsDisableEnable();
    this.disableSelectedRole();
  }

  disableAddCredit: boolean = false;
  addPointsDisableEnable() {
    if (this.roles.length == this.roleSelectedList.length) {
      this.disableAddCredit = true;
    } else {
      this.disableAddCredit = false;
    }
  }

  roleSelectedList = [];
  selectedRole = [];
  disableIfLearner: any = false;
  roleTypeSelected(currentIndex, currentItem) {
    console.log('currentItem ', currentItem);


    console.log('currentItem ', currentItem);
    console.log('this.pointFormat', this.pointFormat);
    if (currentItem.roleId == "8") {
      this.formdata.points[currentIndex].pformatid = 1;
      this.disableIfLearner = true;
    } else {
      this.disableIfLearner = false;
    }

    for (let i = 0; i < this.roles.length; i++) {
      var role = this.roles[i];
      if (role.id == Number(currentItem.roleId)) {
        this.roleSelectedList[currentIndex] = role;
        if (this.selectedRole.length > 0) {
          this.selectedRole[currentIndex] = role.name;
        } else {
          this.selectedRole.push(role.name);
        }
      }
    }

    this.addPointsDisableEnable();
    this.disableSelectedRole();
  }

  disableSelectedRole() {
    this.roles.forEach((data, key) => {
      if (this.selectedRole.indexOf(data.name) >= 0) {
        this.roles[key].isSelected = 1;
      } else {
        this.roles[key].isSelected = 0;
      }
    })
    console.log('Selected Disabled', this.roles);
  }

  addPointsList() {
    // let defualtCreditsObjCopy = Object.assign({}, this.defualtCreditsObj);
    let defualtCreditsObj = {
      crid: 0,
      roleId: '',
      pformatid: '',
      bcpoints: '',
      acpoints: '',
    }
    // console.log(defualtCreditsObj);
    // this.credits.push(defualtCreditsObj);
    this.formdata.points.push(defualtCreditsObj);
  }
  fileUploadRes: any;
  categoryAddEditRes: any;
  newFeedID: any;
  addUpdateFeedback(url, category) {
    this.spinner.show();
    this.webApiService.getService(url, category)
      .then(rescompData => {
        this.spinner.hide();
        var temp: any = rescompData;
        this.formdata.fid = temp.fid;
        this.categoryAddEditRes = temp.data;
        if (temp == "err") {
          // this.notFound = true;
          // var catUpdate : Toast = {
          //     type: 'error',
          //     title: "Feedback",
          //     body: "Unable to update Feedback.",
          //     showCloseButton: true,
          //     timeout: 2000
          // };
          // this.toasterService.pop(catUpdate);

          this.presentToast('error', '');
        } else if (temp.type == false) {
          // var catUpdate : Toast = {
          //     type: 'error',
          //     title: "Feedback",
          //     body: this.categoryAddEditRes.msg,
          //     showCloseButton: true,
          //     timeout: 2000
          // };
          this.presentToast('error', '');
        } else {
          // this.router.navigate(['/pages/reactions/feedback']);
          // var catUpdate : Toast = {
          //     type: 'success',
          //     title: "Feedback",
          //     // body: "Unable to update category.",
          //     body: this.categoryAddEditRes.msg,
          //     showCloseButton: true,
          //     timeout: 2000
          // };
          this.Questions = true;
          this.Settings = false;
          // this.toasterService.pop(catUpdate);
          this.presentToast('success', this.categoryAddEditRes.msg);
          this.newFeedID = this.categoryAddEditRes.fid;
        }
        console.log('Feedback AddEdit Result ', this.categoryAddEditRes)
          this.getExistingFeedbackQuestion()
          this.header = {
            title: this.category?this.category.fname:'Add Feedback',
            btnsSearch: true,
            btnName3: 'Update Section',
            btnAdd: 'Add Section',
            btnName3show: true,
            btnBackshow: true,
            btnAddshow: true,
            showBreadcrumb: true,
            breadCrumbList: [
              {
                'name': 'Reaction',
                'navigationPath': '/pages/reactiock',
              },
              {
                'name': 'Feedback',
                'navigationPath': '/pages/reactions/feedback',
              },
            ]
          };

        

      },
        resUserError => {
          this.spinner.hide();
          // this.loader =false;
          this.errorMsg = resUserError;
        });
  }

  submit(f) {
    if (f.valid) {
      this.enableQues=true;
      this.spinner.show();
      // this.makeTagDataReady();

      // if(this.categoryImgData == undefined){


      var feedback = {
        fid: this.formdata.fid,
        fname: this.formdata.fname,
        fdesc: this.formdata.fdesc,
        // categoryPicRef : this.categoryImgData == undefined ? null : this.formdata.categoryPicRef,
        fimg: this.formdata.fimg,
        visible: 1,
        tenantId: this.userData.tenantId,
        points: this.formdata.points,
        usermodified: this.userData.id,
        fotime: this.formdata.fotime,
        fctime: this.formdata.fctime,
        option: '',
        rUsers: this.formdata.recordUsers
        // tagsList : this.formdata.tags,
      }
      var option: string = Array.prototype.map.call(feedback.points, (item) => {
        console.log('item', item)
        return Object.values(item).join(this.dataSeparatorService.Hash)
      }).join("#");
      feedback.option = option
      console.log('category', feedback)
      var fd = new FormData();
      fd.append('content', JSON.stringify(feedback));
      fd.append('file', this.feedImgData);
      console.log('File Data ', fd);

      console.log('Category Data Img', this.feedImgData);
      console.log('Category Data ', feedback);

      let url = webApi.domain + webApi.url.addupdatefeedback;
      let fileUploadUrl = webApi.domain + webApi.url.fileUpload;
      // let fileUploadUrlLocal = 'http://localhost:9845' + webApi.url.fileUpload;
      let param = {
        tId: this.userData.tenantId
      }

      if (this.feedImgData != undefined) {
        this.webApiService.getService(fileUploadUrl, fd)
          .then(rescompData => {
            this.spinner.hide();
            var temp: any = rescompData;
            console.log("response 1", temp);
            this.fileUploadRes = temp;
            // this.formdata.fid =tenp.
            if (temp == "err") {
              // this.notFound = true;
              // var thumbUpload : Toast = {
              //     type: 'error',
              //     title: "Feedback Thumbnail",
              //     body: "Unable to upload Feedback thumbnail.",
              //     // body: temp.msg,
              //     showCloseButton: true,
              //     timeout: 2000
              // };
              // this.toasterService.pop(thumbUpload);
              this.presentToast('error', '');
            } else if (temp.type == false) {
              // var thumbUpload : Toast = {
              //     type: 'error',
              //     title: "Feedback Thumbnail",
              //     body: "Unable to upload Feedback thumbnail.",
              //     // body: temp.msg,
              //     showCloseButton: true,
              //     timeout: 2000
              // };
              // this.toasterService.pop(thumbUpload);
              this.presentToast('error', '');
            }
            else {
              if (this.fileUploadRes.data != null || this.fileUploadRes.fileError != true) {
                feedback.fimg = this.fileUploadRes.data.file_url;
                this.addUpdateFeedback(url, feedback);
              } else {
                // var thumbUpload : Toast = {
                //     type: 'error',
                //     title: "Feedback Thumbnail",
                //     // body: "Unable to upload category thumbnail.",
                //     body: this.fileUploadRes.status,
                //     showCloseButton: true,
                //     timeout: 2000
                // };
                // this.toasterService.pop(thumbUpload);
                this.presentToast('error', '');
              }
            }
            console.log('File Upload Result', this.fileUploadRes)
          },
            resUserError => {
              this.spinner.hide();
              // this.loader =false;
              this.errorMsg = resUserError;
            });
      } else {
        this.addUpdateFeedback(url, feedback);
      }

    } else {
      console.log('Please Fill all fields');
      // const addEditF: Toast = {
      //   type: 'error',
      //   title: 'Unable to update',
      //   body: 'Please Fill all fields',
      //   showCloseButton: true,
      //   timeout: 2000
      // };
      // this.toasterService.pop(addEditF);
      this.enableQues=false;
      this.presentToast('warning', 'Please fill in the required details');
      Object.keys(f.controls).forEach(key => {
        f.controls[key].markAsDirty();
      });
    }
  }

  /*****************************Survey Questionn Start****************************************/
  // sortableOptions:any;
  // sort(oldIndex,newIndex,newArr){
  //   console.log('Newdata:',oldIndex,newIndex,newArr);
  //   console.log('NewSurveyQue:',this.surveyQue)
  // }

  // submitNewSrveyQue() {
  //   console.log(this.surveyQue);
  //   let finalString;
  //   for (let i = 0; i < this.surveyQue.length; i++) {
  //     let a = i + 1;
  //     let str = this.surveyQue[i].qId +this.dataSeparatorService.Hash+ a;

  //     if (i == 0) {
  //       finalString = str;

  //     } else {
  //       finalString +=this.dataSeparatorService.Pipe+ str;
  //     }
  //   }

  //   console.log(finalString)
  //   // return finalString;
  //   this.setSortOrder(finalString);
  // }

  setSortOrder(finalString) {
    this.spinner.show();
    let param = {
      tenantId: this.userData.tenantId,
      feedbackId: this.newFeedID ? this.newFeedID : this.formdata.fid,
      allstr: finalString
    }
    this.service.ChangeFeedbackQuestionOrder(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log("response 1", result);
        if (result['type'] == true) {
          //   var toast : Toast = {
          //     type: 'success',
          //     //title: "Server Error!",
          //     body: "List updated successfully.",
          //     showCloseButton: true,
          //     timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('success', 'List updated');
          this.getExistingFeedbackQuestion();
          console.log('ORDER CHANGE SUCCESS:', result);
        };

      }, error => {
        // this.spinner.hide();
        //    var toast : Toast = {
        //      type: 'error',
        //      //title: "Server Error!",
        //      body: "Something went wrong.please try again later.",
        //      showCloseButton: true,
        //      timeout: 2000
        //  };
        //  this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
  }

  // myCloneImplementation(item){
  //   console.log(item);
  //   let finalString;
  //   for(let i=0;i<item.length;i++){
  //     let a = i+1;
  //     let str = item[i].qId +this.dataSeparatorService.Hash+ a;

  //     if(i == 0){
  //       finalString = str;

  //     }else{
  //       finalString +=this.dataSeparatorService.Pipe+ str;
  //     }
  //   }

  //   console.log(finalString)

  // }
  // surveyQue:any=[];
  getExistingFeedbackQuestion() {
    this.spinner.show();
    let param = {
      "tenantId": this.userData.tenantId,
      "feedbackId": this.newFeedID ? this.newFeedID : this.formdata.fid,
    }
    console.log('param', param);
    this.service.getExistingFeedbackQuestions(param)
      .then(rescompData => {
        var result = rescompData;

        if (result['type'] == true) {
          this.dragableList = result['data'];
          this.noData=false;
          console.log('ExistingSurveyQuestions:', this.dragableList);  
           if(this.dragableList.length==0){
            this.noData=true
          }

        };
     
        this.spinner.hide();
      }, error => {
        this.spinner.hide();
        this.presentToast('error', '');
      });
  }
  addqueAction: boolean = false;
  questionTypes: any = [];
  getSurveyQuestionType() {
    let param = {
      "tenantId": this.userData.tenantId
    }

    this.service.getFeedbackQuestionType(param)
      .then(rescompData => {
        // this.spinner.hide();
        var result = rescompData;
        console.log('DropdownSurveyQuestionType:', rescompData);
        if (result['type'] == true) {
          this.questionTypes = result['data'][0];
          console.log('DropdownSurveyQuestionType:', this.questionTypes);
        };

      }, error => {
        // this.spinner.hide();
        //    var toast : Toast = {
        //      type: 'error',
        //      //title: "Server Error!",
        //      body: "Something went wrong.please try again later.",
        //      showCloseButton: true,
        //      timeout: 2000
        //  };
        //  this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
  }

  deleteSection(data) {
    this.spinner.show();
    console.log(data);
    const param = {
      feedId: this.newFeedID ? this.newFeedID : this.formdata.fid,
      secId: data.secId,
      tId: this.userData.tenantId,
    }
    console.log(param);
    this.service.deleteFeedbackSection(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('delete Section:', rescompData);
        this.questionshow1 = false;
        if (result['type'] == true) {
          console.log('delete Section:', result['data'][0][0]);
          if (result['data'][0][0].cnt == 1) {
            this.presentToast('success', result['data'][0][0].msg);
            this.getExistingFeedbackQuestion();
          } else {
            this.presentToast('warning', result['data'][0][0].msg);
          }
        } else {
          this.presentToast('error', '');
        }
      }, error => {
        this.spinner.hide();
        this.presentToast('error', '');
      });
  }
  sectionData: any = {};
  saveSection(data, val, j, f) {
    console.log(f, "form value")
    if (f.valid) {
      this.issec = true;
      this.spinner.show();
      let theContent = null;
      // if (val == 1) {
      //   const contedit = 'contedit' + j;
      //   theContent = document.getElementById(contedit).innerHTML;
      // }
      console.log(data);

      const param = {
        sId: data.secId ? data.secId : 0,
        fId: this.newFeedID ? this.newFeedID : this.formdata.fid,
        secName: theContent ? theContent : data.secName,
        visible: 1,
        tenantId: this.userData.tenantId,
        userId: this.userId,
      }

      console.log('param', param);
      this.service.insertFeedbackSection(param)
        .then(rescompData => {
          // this.spinner.hide();
          var result = rescompData;
          console.log('Add Feedback Section:', rescompData);
          this.questionshow1 = false;
          if (result['type'] == true) {
            if (this.addSectionF) {
              this.presentToast('success', 'Section added.');
            } else {
              this.presentToast('success', 'Section updated.');
            }
            this.getExistingFeedbackQuestion();
          } else {
            if (this.addSectionF) {
              this.presentToast('error', '');
            } else {
              this.presentToast('error', '');
            }
          }
          this.issec = false;
          this.spinner.hide();
        }, error => {
          this.issec = false;
          this.spinner.hide();
          this.presentToast('error', '');
        });
    } else {
      // this.toastr.warning( 'Please fill in the required fields', 'Warning', {
      //   closeButton: false
      // });

      Object.keys(f.controls).forEach(key => {
        f.controls[key].markAsDirty();
      });
    }
  }


  saveSection1(data, val, j, name) {
    if (name) {
      this.issec = true;
      this.spinner.show();
      let theContent = null;
      // if (val == 1) {
      //   const contedit = 'contedit' + j;
      //   theContent = document.getElementById(contedit).innerHTML;
      // }
      console.log(data);

      const param = {
        sId: data.secId ? data.secId : 0,
        fId: this.newFeedID ? this.newFeedID : this.formdata.fid,
        secName: theContent ? theContent : data.secName,
        visible: 1,
        tenantId: this.userData.tenantId,
        userId: this.userId,
      }

      console.log('param', param);
      this.service.insertFeedbackSection(param)
        .then(rescompData => {
          // this.spinner.hide();
          var result = rescompData;
          console.log('Add Feedback Section:', rescompData);
          this.questionshow1 = false;
          if (result['type'] == true) {
            if (this.addSectionF) {
              this.presentToast('success', 'Section added.');
            } else {
              this.presentToast('success', 'Section updated.');
            }
            this.getExistingFeedbackQuestion();
          } else {
            if (this.addSectionF) {
              this.presentToast('error', '');
            } else {
              this.presentToast('error', '');
            }
          }
          this.issec = false;
          this.spinner.hide();
        }, error => {
          this.issec = false;
          this.spinner.hide();
          this.presentToast('error', '');
        });
    } else {
      this.presentToast('warning', 'Please enter the value')
    }
  }

  deleteQuestion(queData) {
    this.spinner.show();
    console.log('queData', queData);
    const param = {
      feedbackId: this.newFeedID ? this.newFeedID : this.formdata.fid,
      questionId: queData.qId,
      queTypeId: queData.qtId,
      tenantId: this.userData.tenantId,
    }
    console.log(param);
    this.service.deleteFeedbackQuestion(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('delete feedback question:', rescompData);
        if (result['type'] == true) {
          if (result['data'][0].cnt == 0) {
            this.presentToast('warning', result['data'][0].msg);
          } else {
            this.presentToast('success', result['data'][0].msg);
          }
          this.getExistingFeedbackQuestion();
        } else {
          this.presentToast('error', '');
        }
      }, error => {
        this.spinner.hide();
        this.presentToast('error', '');
      });
  }

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }

  addSectionF: boolean = false;
  addsection() {
    this.questionshow1 = true;
    this.addSectionF = true;
    this.sectionData.secName = '';
  }
  sectionId: any;
  questionAdd(item, data, value) {
    this.isSave = false
    console.log('data', data);
    console.log('item', item);
    if (value === 1) {
      this.sectionId = data.secId;
      this.addqueAction = true;
      this.questionshow2 = true;
      this.questiondata = {
        fqid: '',
        fqname: '',
        fmand: true,
        fqtypeid: '',
        fansmin: 1,
        fansmax: 100,
        fqtypeName: '',
        fqdata: []

      }
    } else {
      this.sectionId = item.sectionId;
      console.log('data', data);
      console.log('item', item);

      let param = {
        "tenantId": this.userData.tenantId,
        "queId": item.qId,
        "queTypeId": item.qtId,
      }
      this.service.feedbackgetqueDataForEdit(param)
        .then(rescompData => {
          // this.spinner.hide();
          let result = rescompData;

          let tempData = result['data'][0][0];
          console.log('TempData', tempData)

          this.questiondata = {
            fqid: item.qId,
            fqname: item.qName,
            fmand: item.isMandatory,
            fqtypeid: item.qtId,
            fansmin: item.qtId == 1 ? tempData.minlen : '',
            fansmax: item.qtId == 1 ? tempData.maxlen : '',
            fqtypeName: '',
            fqdata: item.qtId == 2 ? this.getMultDataReady(tempData) : [],
          }
          this.addqueAction = false;
          this.quesTitle='Edit Question'
          this.questionshow2 = true;
          console.log('this.questiondata', this.questiondata)
        },
          resUserError => {
            // this.spinner.hide();
            this.errorMsg = resUserError;
            // this.notFound = true;
            // this.router.navigate(['**']);
          });
    }

  }
  // this is for mult type data ready
  getMultDataReady(tempData) {
    let demo = [];
    let item = tempData.optionText.split('#');
    for (let i = 0; i < item.length; i++) {
      var data = {
        qansmul: item[i],
      };
      demo.push(data);
    };
    return demo;
  }
  // addQuestion()
  // {
  //   this.questionshow2 =true;
  //   this.addqueAction = true;
  //   this.questiondata={
  //     fqid:'',
  //     fansmax:'',
  //     fansmin:'',
  //     fqname:'' ,
  //     fmand:'1',
  //     fqtypeid:'',
  //     fqtypeName:'',
  //     fqdata:[],

  //   };
  // }

  saveQuestion(data, f) {
    if (f.valid) {
      this.isSave = true;
      this.isque = true;
      this.spinner.show();
      console.log('data', data);
      let param = {
        "sId": this.sectionId,
        "queId": data.fqid ? data.fqid : 0,
        "queName": data.fqname,
        "feedbackId": this.newFeedID ? this.newFeedID : this.formdata.fid,
        "isMandatory": data.fmand == true || data.fmand == 1 ? 1 : 0,
        "queTypeId": data.fqtypeid,
        "tenantId": this.userData.tenantId,
        "userId": this.userId,
        "minVal": data.fqtypeid == 1 ? data.fansmin : 0,
        "maxVal": data.fqtypeid == 1 ? data.fansmax : 0,
        "labelfirst": data.fqtypeid == 3 ? 'Yes' : 0,
        "valfirst": data.fqtypeid == 3 ? '1' : 0,
        "labelsecond": data.fqtypeid == 3 ? 'No' : 0,
        "valsecond": data.fqtypeid == 3 ? '0' : 0,
        "stars": data.fqtypeid == 4 ? 4 : 0,
        "allstr": data.fqtypeid == 2 ? this.getDatareadyforsurveyquestion(data.fqdata) : 0
      }
      console.log('param:', param);
      this.service.addEditFeedbackQuestion(param)
        .then(rescompData => {
          this.isSave = true
          this.questionshow2 = false;
          var temp = rescompData;
          console.log('question added:', rescompData);
          if (!this.service.data.id) {
            if (temp['type'] == true) {
              this.presentToast('success', 'Question added');
              console.log('SurveyQueRes:', temp);
              this.questionshow2 = false;
              //this.cdf.detectChanges();
              this.getExistingFeedbackQuestion();
            } else {
              this.presentToast('error', '');
              //this.router.navigate(['pages/gamification/ladders']);
            }
            this.isque = false;
            this.spinner.hide();
          } else if (this.service.data.id) {
            if (temp['type'] == true) {
              this.presentToast('success', 'Question updated');
              //this.cdf.detectChanges();
              this.getExistingFeedbackQuestion();
              this.questiondata = {
                fqid: '',
                fqname: '',
                fmand: '',
                fqtypeid: '',
                fansmin: '',
                fansmax: '',
                fqtypeName: '',
                fqdata: []
              }
            } else {
              // this.toasterService.pop(toast);
              this.presentToast('error', '');
              //this.router.navigate(['pages/gamification/ladders']);
            }
            this.isque = false;
            this.spinner.hide();
          }
        },
          resUserError => {
            this.isque = false;
            this.spinner.hide();
            this.presentToast('error', '');
            //this.router.navigate(['pages/gamification/ladders']);
          });
      this.questionshow = false;

    } else {
      this.isque = false;
      Object.keys(f.controls).forEach(key => {
        f.controls[key].markAsDirty();
      });
    }
  }

  getDatareadyforsurveyquestion(data) {
    var allstr = '';
    for (let i = 0; i < data.length; i++) {
      let a = i + 1;
      var str = data[i].qansmul + this.dataSeparatorService.Hash + 0 + this.dataSeparatorService.Hash + a;

      if (i == 0) {
        allstr = str;
      } else {
        allstr += this.dataSeparatorService.Pipe + str;
      }
    }
    console.log('allstr', allstr);
    return allstr;
  }


  enableDisableQuestionData: any;
  enableDisableQuestionModal: boolean = false;
  enableQuestion: boolean = false;
  queDisableIndex: any;
  disableQuestion(currentIndex, queData, status) {
    this.enableDisableQuestionData = queData;
    console.log('this.enableDisableLadderData', this.enableDisableQuestionData)
    this.enableDisableQuestionModal = true;
    this.queDisableIndex = currentIndex;

    if (this.enableDisableQuestionData.visible == 0) {
      this.enableQuestion = false;
    } else {
      this.enableQuestion = true;
    }
  }

  closeEnableDisableQuestionModal() {
    this.enableDisableQuestionModal = false;
  }

  enableDisableQuestionAction(actionType) {
    if (actionType == true) {
      if (this.enableDisableQuestionData.visible == 1) {
        var status = 0;
        var queData = this.enableDisableQuestionData;
        this.enableDisableQuestion(queData, status);

      } else {
        var status = 1;
        var queData = this.enableDisableQuestionData;
        this.enableDisableQuestion(queData, status);

      }
      this.closeEnableDisableQuestionModal();
    } else {
      this.closeEnableDisableQuestionModal();
    }
  }

  enableDisableQuestion(queData, status) {
    this.spinner.show();
    console.log('queData', queData);
    let param = {
      "queId": queData.qId,
      "visible": status,

    }
    this.service.UpdateFeedbackQuestionStatus(param)

      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('UpdateLadderResponse:', rescompData);
        if (result['type'] == true) {
          //   var ladderUpdate : Toast = {
          //     type: 'success',
          //     title: "Feedback Question Updated!!",
          //     body: "Question updated successfully.",
          //     showCloseButton: true,
          //     timeout: 2000,
          // };
          // this.toasterService.pop(ladderUpdate);
          this.presentToast('success', 'Question updated');
          this.getExistingFeedbackQuestion();
        } else {
          //   var ladderUpdate : Toast = {
          //     type: 'error',
          //     title: "Feedback Question Updated!",
          //     body: "Unable to update Feedback question.",
          //     showCloseButton: true,
          //     timeout: 2000,
          // };
          // this.toasterService.pop(ladderUpdate);
          this.presentToast('error', '');
        }
      }, error => {
        this.spinner.hide();
        //   var toast : Toast = {
        //     type: 'error',
        //     //title: "Server Error!",
        //     body: "Something went wrong.please try again later.",
        //     showCloseButton: true,
        //     timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });

  }


  getSurveyQuestionDetails(data) {
    this.spinner.show();
    console.log('QueData', data);
    let param = {
      "tenantId": this.userData.tenantId,
      "queId": data.qId,
      "queTypeId": data.qtId
    }

    this.service.GetFeedbackQuestionDetails(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('QuestionDeatils:', rescompData);
        var qData = result['data'][0][0];

        if (result['type'] == true) {
          console.log('QuestionDeatils:', qData);
          if (data.qtId == 1) {
            this.questiondata = {
              fqid: qData.id,
              fqname: qData.name,
              fmand: qData.isMandatory,
              fqtypeid: qData.questionTypeId,
              fansmin: qData.minlen,
              fansmax: qData.maxlen,
              fqtypeName: null,
              fqdata: []
            }
            this.questionshow = true;
          }

          if (data.qtId == 2) {
            let qOptArr = qData.optionText.split('#');
            let qPointsArr = qData.points.split('#');
            let sqDataArr: any = [];
            let sqDataObj: any = {};
            console.log('Arrays:', qOptArr, qPointsArr);
            for (let i = 0; i < qOptArr.length; i++) {
              sqDataObj = {
                "qansmul": qOptArr[i],
                "qansmulpoints": qPointsArr[i]
              }
              sqDataArr.push(sqDataObj);
            }
            console.log('sqDataArr', sqDataArr);

            this.questiondata = {
              fqid: qData.id,
              fqname: qData.name,
              fmand: qData.isMandatory,
              fqtypeid: qData.questionTypeId,
              fansmin: null,
              fansmax: null,
              fqtypeName: null,
              fqdata: sqDataArr
            }
            this.questionshow = true;
          }
          if (data.qtId == 3) {
            this.questiondata = {
              fqid: qData.id,
              fqname: qData.name,
              fmand: qData.isMandatory,
              fqtypeid: qData.questionTypeId,
              fansmin: null,
              fansmax: null,
              fqtypeName: null,
              fqdata: []
            }
            this.questionshow = true;
          }

          if (data.qtId == 4) {
            this.questiondata = {
              fqid: qData.id,
              fqname: qData.name,
              fmand: qData.isMandatory,
              fqtypeid: qData.questionTypeId,
              fansmin: null,
              fansmax: null,
              fqtypeName: null,
              fqdata: []
            }
            this.questionshow = true;
          }
        } else {
          this.spinner.hide();
          // var toast : Toast = {
          //   type: 'error',
          //   //title: "Server Error!",
          //   body: "Something went wrong.please try again later.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }
      }, error => {
        this.spinner.hide();
        //   var toast : Toast = {
        //     type: 'error',
        //     //title: "Server Error!",
        //     body: "Something went wrong.please try again later.",
        //     showCloseButton: true,
        //     timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
  }

  /*****************************Feedback Question End********************************************/

  selectTab(item) {
     this.item=item;
    if (item.tabTitle === 'Settings') {
        this.header = {
          title: this.category?this.category.fname:'Add Feedback',
          btnsSearch: true,
          btnName2: 'Save & Next',
          btnName2show: true,
          btnBackshow: true,
          showBreadcrumb: true,
          breadCrumbList: [
            {
              'name': 'Reaction',
              'navigationPath': '/pages/reactions',
            },
            {
              'name': 'Feedback',
              'navigationPath': '/pages/reactions/feedback'
            },
          ]
        };

      
      // this.selectedActInd =undefined;
      this.Questions = false;
      this.Settings = true;
      this.Responses = false;
      this.Analysis = false;
      this.spinner.show();
      setTimeout(() => {
        this.spinner.hide()
      }, 2000);
    } else if (item.tabTitle === 'Questions') {
        this.getExistingFeedbackQuestion();
        this.header = {
          title: this.category?this.category.fname:'Add Feedback',
          btnsSearch: true,
          btnName3: 'Update Section',
          btnAdd: 'Add Section',
          btnName3show: true,
          btnBackshow: true,
          btnAddshow: true,
          showBreadcrumb: true,
          breadCrumbList: [
            {
              'name': 'Reaction',
              'navigationPath': '/pages/reactions',
            },
            {
              'name': 'Feedback',
              'navigationPath': '/pages/reactions/feedback',
            },
          ]
        };
        this.spinner.show();
        setTimeout(() => {
          this.spinner.hide()
        }, 2000);
      // this.selectedActInd =0;
      this.Questions = true;
      this.getExistingFeedbackQuestion()
      this.Settings = false;
      this.Responses = false;
      this.Analysis = false;
    } else if (item.tabTitle === 'Responses') {
      // this.selectedActInd =0;
      this.Questions = false;
      this.Settings = false;
      this.Responses = true;
      this.Analysis = false;
    } else if (item.tabTitle === 'Analysis') {
      // this.selectedActInd =0;
      this.Questions = false;
      this.Settings = false;
      this.Responses = false;
      this.Analysis = true;
    }
  }

  // Help Code Start Here //

  helpContent: any;
  getHelpContent() {
    // return new Promise(resolve => {
    //   this.http1
    //     .get('../../../../../../assets/help-content/addEditCourseContent.json')
    //     .subscribe(
    //       data => {
    //         this.helpContent = data;
    //         console.log('Help Array', this.helpContent);
    //       },
    //       err => {
    //         resolve('err');
    //       }
    //     );
    // });
    // return this.helpContent;
    this.commonFunctionService.getHelpContent().then(
      data => {
        this.helpContent = data;
        console.log('Help Array', this.helpContent);
      },
      err => {
        //resolve('err');
      }
    );

  }

  // Help Code Ends Here //
  // update sort order
  UpdateChnages() {
    let data = [];
    console.log('this.dragableList', this.dragableList);
    for (let i = 0; i < this.dragableList.length; i++) {
      for (let j = 0; j < this.dragableList[i].list.length; j++) {
        if (this.dragableList[i].list[j].qId) {
          let item = {
            secId: this.dragableList[i].secId,
            qId: this.dragableList[i].list[j].qId,
            sOrder: j
          }
          data.push(item);
        }
      }
    }

    const option: string = Array.prototype.map
      .call(data, function (item) {
        console.log("item", item);
        return Object.values(item).join("#");
      })
      .join("|");
    console.log("option", option);

    const tObj = {
      'feedbackId': this.newFeedID ? this.newFeedID : this.formdata.fid,
      'option': option,
      'tId': this.userData.tenantId,
      "userId": this.userId,
    }
    this.service.feedbackUpadateSortOrder(tObj)
      .then(rescompData => {
        this.spinner.hide();
        let temp1: any = rescompData;
        if (temp1['type'] != true) {
          // this.notFound = true;
          // const catUpdate: Toast = {
          //   type: "error",
          //   title: "Question",
          //   body: "Unable to update order.",
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
        } else {
          // this.router.navigate(['/pages/assessment/quiz']);
          // var catUpdate: Toast = {
          //   type: "success",
          //   title: "Question",
          //   // body: "Unable to update category.",
          //   body:'Changes Updated Successfully',
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          this.getExistingFeedbackQuestion();
          //this.getquizQuestion(que);
          // this.closeModel();
          // this.basic= false;
          // this.type=true;
          // this.toasterService.pop(catUpdate);
          this.presentToast('success', 'Changes updated');
        }
        //     console.log("Question AddEdit Result ", this.inert);
      },
        resUserError => {
          this.spinner.hide();
          this.errorMsg = resUserError;
        }
      );
  }

}

