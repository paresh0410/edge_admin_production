import {Injectable, Inject} from '@angular/core';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
 import {AppConfig} from '../../../../app.module';
 import { webApi } from '../../../../service/webApi';
 import { HttpClient } from "@angular/common/http";

@Injectable()
export class  AddeditfeedbackService {

  public data:any;
  menuId = null;
  private _url:string = "/api/edge/category/addUpdateCategory"
  private _urldrop:string="/api/web/getallfeedbackdropdown"
  request: Request;
  private _urlGetroleDropdownSetting: string = webApi.domain + webApi.url.getroledropdownforsurvey;
  private _urlGetFeedbackQuestionType:string = webApi.domain + webApi.url.getfeedbackquestiontype;
  private _urlAddEditFeedbackQuestion:string = webApi.domain + webApi.url.addeditfeedbackquestion;
  private _urlGetExistingFeedbackQuestion:string = webApi.domain + webApi.url.getexistingfeedbackquestions;
  private _urlUpdateFeedbackQuestionStatus:string = webApi.domain + webApi.url.changefeedbackquestionstatus;
  private _urlGetFeedbackQuestionDetails:string = webApi.domain + webApi.url.getfeedbackquestiondetails;
  private _urlChangeFeedbackQuestionOrder:string = webApi.domain + webApi.url.changefeedbackquestionorder;
  private _urlGetFeedbackUserTypes: string = webApi.domain + webApi.url.getfeebackusertypes;
  private _urlInsertFeedbackSection: string = webApi.domain + webApi.url.insertFeedbackSection;
  private _urlDeleteFeedbackSection: string = webApi.domain + webApi.url.deleteFeedbackSection;
  private _urlfeedbackgetqueDataForEdit: string = webApi.domain + webApi.url.feedbackgetqueDataForEdit;
  private _urlfeedbackUpdateSortOrder: string = webApi.domain + webApi.url.feedbackUpadateSortOrder;
  private _urlDeleteFeedbackQuestion: string = webApi.domain + webApi.url.deleteFeedbackQuestion;

  constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private _http: Http,private _httpClient:HttpClient){
      //this.busy = this._http.get('...').toPromise();
  }

  createUpdateCategory(category){
    let url:any = `${this.config.FINAL_URL}`+this._url;
    return this._http.post(url,category)
      .map((response:Response) => response.json())
      .catch(this._errorHandler);
  }



getFeedbackdropdown(param){
    return new Promise(resolve => {
      this._httpClient.post(this._urlGetroleDropdownSetting, param)
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}

getFeedbackUserType(param){
  return new Promise(resolve => {
    this._httpClient.post(this._urlGetFeedbackUserTypes, param)
    .subscribe(data => {
        resolve(data);
    },
    err => {
        resolve('err');
    });
});
}

getFeedbackQuestionType(param){
    return new Promise(resolve => {
      this._httpClient.post(this._urlGetFeedbackQuestionType, param)
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}

addEditFeedbackQuestion(param){
    return new Promise(resolve => {
      this._httpClient.post(this._urlAddEditFeedbackQuestion, param)
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}

getExistingFeedbackQuestions(param){
    return new Promise(resolve => {
      this._httpClient.post(this._urlGetExistingFeedbackQuestion, param)
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}

UpdateFeedbackQuestionStatus(param){
    return new Promise(resolve => {
      this._httpClient.post(this._urlUpdateFeedbackQuestionStatus, param)
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}

GetFeedbackQuestionDetails(param){
  return new Promise(resolve => {
    this._httpClient.post(this._urlGetFeedbackQuestionDetails,param)
    .subscribe(data => {
        resolve(data);
    },
    err => {
        resolve('err');
    });
});
}

ChangeFeedbackQuestionOrder(param){
  return new Promise(resolve => {
    this._httpClient.post(this._urlChangeFeedbackQuestionOrder,param)
    .subscribe(data => {
        resolve(data);
    },
    err => {
        resolve('err');
    });
});
}

  getallDropdown(category){
    let url:any = `${this.config.FINAL_URL}`+this._urldrop;
    // return this._http.post(url,category)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);
    return new Promise(resolve => {
      this._httpClient.post(url, category)
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
    });
  }

  insertFeedbackSection(param) {
    return new Promise(resolve => {
      this._httpClient.post(this._urlInsertFeedbackSection, param)
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}

deleteFeedbackSection(param) {
  return new Promise(resolve => {
    this._httpClient.post(this._urlDeleteFeedbackSection, param)
    .subscribe(data => {
        resolve(data);
    },
    err => {
        resolve('err');
    });
});
}

feedbackgetqueDataForEdit(param){
  return new Promise(resolve => {
    this._httpClient.post(this._urlfeedbackgetqueDataForEdit, param)
    .subscribe(data => {
        resolve(data);
    },
    err => {
        resolve('err');
    });
});
}

feedbackUpadateSortOrder(param){
  return new Promise(resolve => {
    this._httpClient.post(this._urlfeedbackUpdateSortOrder, param)
    .subscribe(data => {
        resolve(data);
    },
    err => {
        resolve('err');
    });
});
}

deleteFeedbackQuestion(param){
  return new Promise(resolve => {
    this._httpClient.post(this._urlDeleteFeedbackQuestion, param)
    .subscribe(data => {
        resolve(data);
    },
    err => {
        resolve('err');
    });
});
}

  _errorHandler(error: Response){
    console.error(error);
    return Observable.throw(error || "Server Error")
  }

}
