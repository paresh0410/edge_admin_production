import { Component, ViewEncapsulation, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

import { Router, NavigationStart, Routes, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { feedbackService } from './feedback.service';
import { AddeditfeedbackService } from './addEditfeedback/addEditfeedback.service';
// import { questionsService } from './questions/questions.service';
import { BaseChartDirective } from 'ng2-charts/ng2-charts';
import { NgxSpinnerService } from 'ngx-spinner';

import { webAPIService } from '../../../service/webAPIService';
import { webApi } from '../../../service/webApi';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { ToastrService } from 'ngx-toastr';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { Card } from '../../../models/card.model';
import { SuubHeader } from '../../components/models/subheader.model';
import { noData } from '../../../models/no-data.model';
// import { ScrollEvent } from 'ngx-scroll-event';

// import { ContentService } from '../content/content.service';

@Component({
  selector: 'feedback',
  styleUrls: ['./feedback.scss'],
  // template:'category works'
  templateUrl: './feedback.html',
  encapsulation: ViewEncapsulation.None,
})
export class feedback implements  AfterViewInit  {
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No Feedback defined at this time.",
    desc:"Feedback will appear after they are designed by the instructor. Feedbacks are a set of pre-defined questions to gather feedback from the learner.",
    titleShow:true,
    btnShow:true,
    descShow:true,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/assessment-how-to-create-a-feedback',
  }
  cardModify: Card =   {
    flag: 'feedback',
    titleProp : 'fname',
    discrption : 'fdesc',
    image: 'fimg',
    hoverlable:   true,
    bottomDiscription:  true,
    showImage: true,
    option:   true,
    eyeIcon:   true,
    bottomDiv:   true,
    bottomTitle:   true,
    btnLabel: 'Edit',
    defaultImage:'assets/images/category.jpg'
  };
  count:number=8
  notFound: boolean = false;
  notFound1: boolean = true;
  throttle = 300;
  scrollDistance = 1;
  scrollUpDistance = 2;
  direction = '';
  query: string = '';
  public getData;
  loginUserdata;
  skeleton =false;

  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  header: SuubHeader  = {
    title:'Feedback',
    btnsSearch: true,
    placeHolder:'Search by feedback',
    searchBar: true,
    dropdownlabel: ' ',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: '',
    btnName3: '',
    btnAdd: 'Add Feedback',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: true,
    filter: false,
    showBreadcrumb: true,
    breadCrumbList:[
      {
        'name': 'Reaction',
        'navigationPath': '/pages/reactions',
      },
    ]
  };

  cat: any;
  feedback: any = []

  errorMsg: string;

  parentcat: any = [{
    id: 0,
    name: 'Parent'
  }, {
    id: 1,
    name: 'Assigned'
  },
  {
    id: 2,
    name: 'Recommended'
  },
  {
    id: 3,
    name: 'Special'
  }]

  content1: any = [];

  search: any;
  loader: any;
  currentPage = 1;
  totalPages: any;
  pagelmt = 12;
  array = [];
  conentHeight: string;
  offset: number = 100;
  str: any = '';
  isworking = false;
  searchText: any;
  enableFeedback: boolean=false;
  enableDisableFeedbackModal: boolean=false;
  enableFeedbackData: any;
  // enableDisableFeedbackModal: boolean;
  //protected service: CCategorydataService,
  constructor(private spinner: NgxSpinnerService, protected categoryService: feedbackService,
    protected addCategoryService: AddeditfeedbackService, private router: Router,private commonFunctionService: CommonFunctionsService,
    // private toasterService: ToasterService,
    protected webApiService: webAPIService,
    private toastr: ToastrService) {
    // this.loader = true;
    this.conentHeight = '0px';
    if (localStorage.getItem('LoginResData')) {
      this.loginUserdata = JSON.parse(localStorage.getItem('LoginResData'));
    }
    this.search = {};
    // this.category;
    this.cat = {
      id: ''
    };

    this.getFeedback(this.str, this.currentPage);
    // this.addItems(0,this.sum , 'push');
    // var e = document.getElementsByTagName('nb-layout-column');console.log(e[0].clientHeight);

  }
  ngAfterViewInit() {
    const e = document.getElementsByTagName('nb-layout-column');
    console.log(e[0].clientHeight);
    if (e[0].clientHeight > 700) {
      this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
    } else {
      this.conentHeight = String(e[0].clientHeight) + 'px';
    }

  }
  addItems(startIndex, endIndex, _method) {
    for (let i = startIndex; i < this.sum; ++i) {
      this.array[_method]([i, ' ', "NIKHIL"]);
      console.log("NIKHIL");
    }
  }
  getUnique(arr, comp) {

    //store the comparison  values in array
    const unique = arr.map(e => e[comp]).
      // store the keys of the unique objects
      map((e, i, final) => final.indexOf(e) === i && i)
      // eliminate the dead keys & return unique objects
      .filter((e) => arr[e]).map(e => arr[e]);

    return unique

  }

  getFeedback(str, pageNo) {
    this.skeleton = false;
    var dataf =
    {
      tId: this.loginUserdata.data.data.tenantId,
      lmt: this.pagelmt,
      pno: pageNo,
      str: str,
    }
    // this.spinner.show();
    const getfeddbackUrl: string = webApi.domain + webApi.url.getfeddback;
    this.commonFunctionService.httpPostRequest(getfeddbackUrl,dataf)
    //this.categoryService.getFeedbacks(dataf)
      .then(rescompData => {
        // this.spinner.hide();
        this.skeleton = true;
        //  this.notFound = true;
        console.log('All Feedback:', rescompData);
        // if (rescompData['data'].length > 0) {
        if (rescompData['type'] === true) {
          this.notFound1 = true;
          this.totalPages = rescompData['data']['totalPages'];
          const list = rescompData['data']['list'];
          if (this.feedback.length > 0 && pageNo > 1) {
            this.feedback = this.feedback.concat(list);
          } else {
            this.feedback = list;
          }
          this.isworking = false;
          console.log(this.feedback);
          this.skeleton = true;
          // feedback=this.
          if (this.feedback) {
            this.feedback = this.getUnique(this.feedback, 'fid')
            for (let j = 0; j < this.feedback.length; j++) {
              this.feedback[j].points = [];
              // for (let i = 0; i < feedback.length; i++) {
              if (this.feedback[j].fid == this.feedback[j].fid) {
                if (this.feedback[j].roleId) {
                  var data =
                  {
                    crid: 0,
                    roleId: this.feedback[j].roleId,
                    pformatid: this.feedback[j].dimension,
                    bcpoints: this.feedback[j].bcPoints,
                    acpoints: this.feedback[j].acPoints,
                  }
                  this.feedback[j].points.push(data);
                }

              }
              // }
            }
            // this.showContent();
          }

          console.log('Category Result', this.feedback);
          if(this.fromSearch && this.feedback.length==0){
          this.notFound1 = false;
            this.noDataVal={
              margin:'mt-5',
              imageSrc: '../../../../../assets/images/no-data-bg.svg',
              title:"Sorry we couldn't find any matches please try again",
              desc:".",
              titleShow:true,
              btnShow:false,
              descShow:false,
              btnText:'Learn More',
              btnLink:''
            }
          }
        }
        else{
          this.notFound1 = false;
        }

        // this.notFound = true;
      },
        resUserError => {
          this.spinner.hide();
          this.skeleton = true;
          this.loader = false;
          this.errorMsg = resUserError;
          this.notFound = false;
          // notFound
          // this.router.navigate(['**']);
        });
  }
  onScroll(event) {
    let element = this.myScrollContainer.nativeElement;
    // element.style.height = '500px';
    // element.style.height = '500px';
    // Math.ceil(element.scrollHeight - element.scrollTop) === element.clientHeight
    if (element.scrollHeight - element.scrollTop - element.clientHeight < 20) {
      if (!this.isworking) {
        this.currentPage++;
        if (this.currentPage <= this.totalPages) {
          this.isworking = true;
          this.skeleton = false;
          this.getFeedback(this.str, this.currentPage);
        }
      }
    }
  }
  clear() {
    if(this.searchText.length>=3){
      this.notFound1=true
    this.header.searchtext = ''
    this.search = {};
    this.str = '';
    this.currentPage = 1;
    this.getFeedback(this.str, this.currentPage);
    this.searchText='';
    }
    else{
    this.search = {};

    }
  }


  sum = 10;
  public handleScroll(events) {
    // console.log('scroll occurred', this.myScrollContainer);

    var element = this.myScrollContainer.nativeElement;
    if (Math.ceil(element.scrollHeight - element.scrollTop) === element.clientHeight) {
      console.log('Reached End');
      const start = this.sum;
      this.sum += 20;
      this.addItems(start, this.sum, 'push');
    }

    // if (event.isReachingBottom) {
    // this.myScrollContainer.nativeElement.onScroll();
    // console.log('scroll occurred', event.originalEvent);
    // if (event.isReachingBottom) {
    //   console.log('End Div');
    //   const start = this.sum;
    //   this.sum += 20;
    //   this.addItems(start, this.sum, 'push');
    // }
    // if (event.isReachingTop) {
    //   console.log('the user is reaching the bottom');
    // }
    // if (event.isWindowEvent) {
    //   console.log('Window End');
    // }

  }


  back() {
    this.router.navigate(['/pages/reactions']);
  }

  content2(data, id) {
    var passData = {
      id: id,
      data: data
    }
    // this.questionsService.data = passData;
    this.router.navigate(['/pages/reactions/feedback/addEditfeedback']);
    // this.passService.data = {};
  }
  delatecategory(ind) {
    for (let i = 0; i < this.feedback.length; i++) {
      if (ind == i) {
        this.feedback.splice(i, 1)
      }
    }
  }

  public addeditccategory(data, id) {
    var data1 = {
      data: data,
      id: id,
    }

    if (data == undefined) {
      this.addCategoryService.data = data1;
      this.router.navigate(['/pages/reactions/feedback/addEditfeedback']);
    } else {
      this.addCategoryService.data = data1;
      this.router.navigate(['/pages/reactions/feedback/addEditfeedback']);
      // this.router.navigate(['/pages/plan/courses/category/addEditCategory',{data:JSON.stringify(data),id:id}]);

    }

  }

  gotoCardaddedit(data) {

    var data1 = {
      data: data,
      id: 1,
    }
    this.addCategoryService.data = data1;
    // this.enableFeedbackData=data1
    this.router.navigate(['/pages/reactions/feedback/addEditfeedback']);
  }
  disableCourse: boolean = false;
  disableContent(item, disableStatus) {
    this.disableCourse = !this.disableCourse;
  }

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }



  visibiltyRes: any;
  disableQcate(i, item) {
    this.spinner.show();
    var visibilityData = {
      id: item.fid,
      visible: item.visible == 1 ? 0 : 1
    }
    let url = webApi.domain + webApi.url.disablefeedback;
    this.webApiService.getService(url, visibilityData)
      .then(rescompData => {
        // this.loader =false;
        this.spinner.hide();
        this.visibiltyRes = rescompData;
        // console.log('Category Visibility Result',this.visibiltyRes);
        if (this.visibiltyRes.type == false) {
          // var catUpdate: Toast = {
          //   type: 'error',
          //   title: "feedback",
          //   body: "Unable to update visibility of feedback.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.closeEnableDisableCategoryModal();
          // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
        } else {
          // var catUpdate: Toast = {
          //   type: 'success',
          //   title: "feedback",
          //   body: this.visibiltyRes.data,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.closeEnableDisableCategoryModal();
          // this.toasterService.pop(catUpdate);
          this.presentToast('success', this.visibiltyRes.data);
          this.disablevaluechnage(i, item)
          // this.getCategories(this.catdata);
        }
      },
        resUserError => {
          // this.loader =false;
          this.spinner.hide();
          if (resUserError.statusText == "Unauthorized") {
            this.router.navigate(['/login']);
          }
          this.errorMsg = resUserError;
          // this.closeEnableDisableCategoryModal();
        });
  }


  clickTodisable(item,i) {
    this.spinner.show();
    var visibilityData = {
      id: item.fid,
      visible: item.visible==0?1:0
    }
    if(this.feedback[i].visible==0){
      this.feedback[i].visible=1
    }else{
      this.feedback[i].visible=0
    }
    let url = webApi.domain + webApi.url.disablefeedback;
    this.webApiService.getService(url, visibilityData)
      .then(rescompData => {
        this.spinner.hide();
        this.visibiltyRes = rescompData;
        if (this.visibiltyRes.type == false) {
          this.presentToast('error', '');
          // this.closeEnableDisableFeedbackModal();
        } else {
          this.presentToast('success', this.visibiltyRes.data);
          // this.closeEnableDisableFeedbackModal();
        }
      },
        resUserError => {
          // this.loader =false;
          this.spinner.hide();
          if (resUserError.statusText == "Unauthorized") {
            this.router.navigate(['/login']);
          }
          // this.closeEnableDisableFeedbackModal();
          this.errorMsg = resUserError;
          // this.closeEnableDisableCategoryModal();
        });
  }
  Disbaledata: any;
  disablevaluechnage(i, item) {
    this.Disbaledata = item;
    this.Disbaledata.visible = this.Disbaledata.visible == 1 ? 0 : 1
    // this.feedback[i].visible=this.feedback[i].visible==1?0:1
  }
  // showContent() {
  //   if (this.feedback.length > 0) {
  //     this.notFound1 = true;
  //     this.skeleton = true;
  //   }
  //   else {
  //     this.notFound1 = false;
  //     this.skeleton = true;

  //   }
  // }
  onScrollDown() {
    // console.log('scrolled down!!', ev);
    // if (this.throttle > this.sum) {
    // // add another 20 items
    // const start = this.sum;
    // this.sum += 20;
    // this.appendItems(start, this.sum);

    //   this.direction = 'down'
    // }
  }
  fromSearch=false
  onsearch(evt: any) {
  //   if (evt.keyCode === 13) {
  // }
  // else{
    console.log(evt);
    this.searchText=evt.target.value;
    if(this.searchText.length>=3||this.searchText.length==0){
    this.fromSearch=true;
    this.str = evt.target.value;
    this.currentPage = 1;
    this.getFeedback(this.str, this.currentPage);
    }
    // else{
    //   this.clear();
    // }
    // if (evt.keyCode === 13) {
    //   this.currentPage = 1;
    //   this.getFeedback(this.str, this.currentPage);
    // }
  }
  ngOnDestroy(){
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0){
      e1[0].classList.add('with-scroll');
    }

  }
  ngAfterContentChecked() {
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0)
    {
      e1[0].classList.remove('with-scroll');
    }

  }
  // closeEnableDisableFeedbackModal() {
  //   this.enableDisableFeedbackModal = false;
  // }
  // enableDisableFeedbackAction(actionType) {
  //   console.log(this.enableFeedbackData);
  //   if (actionType == true) {

  //     if (this.enableFeedbackData.visible == 1) {
  //       this.enableFeedbackData.visible = 0;
  //       var FeedbackData = this.enableFeedbackData;
  //       this.clickTodisable(FeedbackData);
  //     } else {
  //       this.enableFeedbackData.visible = 1;
  //       var FeedbackData = this.enableFeedbackData;
  //       this.clickTodisable(FeedbackData);
  //     }
  //   } else {
  //     this.closeEnableDisableFeedbackModal();
  //   }
  // }
  // clickTodisableFeedback(FeedbackData) {
  //   if (FeedbackData.visible == 1) {
  //     this.enableFeedback = true;
  //     this.enableFeedbackData = FeedbackData;
  //     this.enableDisableFeedbackModal = true;
  //   } else {
  //     this.enableFeedback = false;
  //     this.enableFeedbackData = FeedbackData;
  //     this.enableDisableFeedbackModal = true;
  //   }
  // }

}



