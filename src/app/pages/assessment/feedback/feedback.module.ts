import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MyDatePickerModule } from 'mydatepicker';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { feedback } from './feedback.component';
import { feedbackService } from './feedback.service';
// import { ContentService } from '../content/content.service';
import { AddeditfeedbackService } from './addEditfeedback/addEditfeedback.service';
import { NotFoundComponent } from './not-found/not-found.component';
import { ThemeModule } from '../../../@theme/theme.module';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgxContentLoadingModule } from 'ngx-content-loading';
import { ComponentModule } from '../../../component/component.module';
// import { InfiniteScrollComponent } from '../../../component/infinite-scroll/infinite-scroll-component';
// import { MiscellaneousRoutingModule, routedComponents } from './miscellaneous-routing.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FilterPipeModule,
    ThemeModule,
    NgxSkeletonLoaderModule,
    NgxContentLoadingModule,
    ComponentModule,
  ],
  declarations: [
    feedback,
    NotFoundComponent,
    // InfiniteScrollComponent
  ],
  providers: [
    feedbackService,
    AddeditfeedbackService,
    // qContentService
  ],
  // exports: [InfiniteScrollComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})

export class feedbackModule { }
