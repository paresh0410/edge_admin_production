import { Injectable, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AppConfig } from '../../../app.module';
import { HttpClient } from "@angular/common/http";
import { webApi } from '../../../service/webApi';
@Injectable()
export class feedbackService {
  // private _url: string = '/api/web/getall_feedback';
  private getfeddbackUrl: string = webApi.domain + webApi.url.getfeddback;
  public data: any;

  constructor(
    @Inject('APP_CONFIG_TOKEN') config: AppConfig, private _http: Http, private http1: HttpClient
  ) {}

  // getFeedbacks(data) {
  //   let url: any = `${this.config.FINAL_URL}` + this._url;
  //   // return this._http
  //   //   .post(url, data)
  //   //   .map((response: Response) => response.json())
  //   //   .catch(this._errorHandler);
  //   return new Promise(resolve => {
  //       this._http1.post(url, data)
  //         .subscribe(
  //           data => {
  //             resolve(data);
  //           },
  //           err => {
  //             resolve(err);
  //             // if (err) {
  //             //   this.toastr.error(
  //             //     "Please check server connection",
  //             //     "Server Error!"
  //             //   );
  //             // }
  //           }
  //         );
  //     });
  // }

  getFeedbacks(data) {
    console.log(data);
    return new Promise(resolve => {
      this.http1.post(this.getfeddbackUrl, data)
        //.map(res => res.json())

        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }
  _errorHandler(error: Response) {
    console.error(error);
    return Observable.throw(error || 'Server Error');
  }
}
