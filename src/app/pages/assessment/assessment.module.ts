import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { NgxEchartsModule } from 'ngx-echarts';
import { ThemeModule } from '../../@theme/theme.module';
import { assessmentComponent } from './assessment.component';

import { ReactiveFormsModule } from '@angular/forms';

import { CommonModule } from '@angular/common';
import { SortablejsModule } from 'angular-sortablejs';
import { ComponentModule } from '../../component/component.module';
// import { ScrollEventModule } from 'ngx-scroll-event';

// import { CourseBundleModule } from './courseBundle/courseBundle.module';


@NgModule({
  imports: [
    ThemeModule,
    NgxEchartsModule,
    CommonModule,
    SortablejsModule,
    ReactiveFormsModule,
    ComponentModule,
    // InfiniteScrollModule,
    // CourseBundleModule
  ],
  declarations: [
    assessmentComponent,
    // CourseBundle
  ],
  providers: [
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class assessmentModule { }
