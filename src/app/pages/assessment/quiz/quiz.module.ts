import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MyDatePickerModule } from 'mydatepicker';
import { FilterPipeModule  } from 'ngx-filter-pipe';
import { quiz } from './quiz.component';
import { quizService } from './quiz.service';
// import { ContentService } from '../content/content.service';
import { AddeditquizService } from './addEditquiz/addEditquiz.service';
import { NotFoundComponent } from './not-found/not-found.component';
import { ThemeModule } from '../../../@theme/theme.module';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgxContentLoadingModule } from 'ngx-content-loading';
import { ComponentModule } from '../../../component/component.module';
// import { MiscellaneousRoutingModule, routedComponents } from './miscellaneous-routing.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FilterPipeModule,
    ThemeModule,
    NgxSkeletonLoaderModule,
    NgxContentLoadingModule,
    ComponentModule,
  ],
  declarations: [
    quiz,
    NotFoundComponent
  ],
  providers: [
    quizService,
    AddeditquizService,
    // qContentService
  ]
})

export class quizModule {}
