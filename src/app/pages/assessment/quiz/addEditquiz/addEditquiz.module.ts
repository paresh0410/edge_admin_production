// import { routing }       from './addEditUser.routing';
import { Addeditquiz } from './addEditquiz.component';
import {  AddeditquizService } from './addEditquiz.service';


import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
 import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

//import { AgGridModule } from 'ag-grid-angular';
// import 'ag-grid-enterprise';
import 'ag-grid-community';

import { MyDatePickerModule } from 'mydatepicker';
// import { TruncateModule } from 'ng2-truncate';
// import { TabsModule } from 'ngx-tabs';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { TagInputModule } from 'ngx-chips';
import { NbTabsetModule } from '@nebular/theme';
import { TranslateModule } from '@ngx-translate/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SortablejsModule } from 'angular-sortablejs';
import {ComponentModule } from '../../../../component/component.module';


@NgModule({
  imports: [
    CommonModule,
    // routing,
    FormsModule,
   // AgGridModule.withComponents([]),
    // TabsModule,
    ReactiveFormsModule,
    Ng2SmartTableModule,
    TagInputModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    NbTabsetModule,
    TranslateModule,
    NgxDatatableModule,
    SortablejsModule,
    ComponentModule
  ],
  declarations: [
     Addeditquiz
    // ChartistJs
  ],
  providers: [
     AddeditquizService,
  ],
  schemas: [ 
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA 
  ]
})
export class  AddeditquizModule {}
