import {
  Component,
  Directive,
  ViewEncapsulation,
  forwardRef,
  Attribute,
  OnChanges,
  SimpleChanges,
  Input,
  ViewChild,
  ViewContainerRef,
  ChangeDetectorRef
} from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { AddeditquizService } from "./addEditquiz.service";
import {
  NG_VALIDATORS,
  Validator,
  Validators,
  AbstractControl,
  ValidatorFn
} from "@angular/forms";
import { LocalDataSource } from "ng2-smart-table";
import { Router, NavigationStart, ActivatedRoute } from "@angular/router";
import { OwlDateTimeModule } from "ng-pick-datetime";
// import { ToasterModule, ToasterService, Toast } from "angular2-toaster";
import { ToastrService } from 'ngx-toastr';
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { SortablejsOptions } from "angular-sortablejs";
import { webAPIService } from "../../../../service/webAPIService";
import { webApi } from "../../../../service/webApi";
import { NgxSpinnerService } from "ngx-spinner";
import { SuubHeader } from "../../../components/models/subheader.model";
import { noData } from "../../../../models/no-data.model";

@Component({
  selector: "addEditquiz",
  templateUrl: "./addEditquiz.html",
  styleUrls: ["./addEditquiz.scss"],
  encapsulation: ViewEncapsulation.None
})
export class Addeditquiz {
  @ViewChild("manualTable") manualTable: any;
  @ViewChild(DatatableComponent) tableDataManual: DatatableComponent;
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No section added under quiz.",
    desc:"",
    titleShow:true,
    btnShow:true,
    descShow:false,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/assessment-how-to-add-a-question-under-quiz',
  }

  questionShows: boolean = false;
  query: string = "";
  sectionid: any;
  public getData;

  QuestionListOptions: SortablejsOptions = {
    group: {
      name: "qquestions"
      // pull: 'clone',
      // put: false,
    },
    sort: true,
    // draggable: '.draggable',
    // onEnd: this.onListItemDragEnd.bind(this),
    // put: false,
    onUpdate: () => console.log("this.questions", this.qquestions),
    handle: ".drag-handle"
    // draggable: '.draggable'
    
  };
  QuestionListOptions1: SortablejsOptions = {
    group: "questions1",
    sort: true,
    // draggable: '.draggable',
    // onEnd: this.onListItemDragEnd.bind(this),
    handle: ".drag-handle",
    onUpdate: () => console.log("this.questions", this.qquestions),
    // draggable: '.draggable'
  };

  titleName = "Add Question"
  sectionAdd = "Add Section"
  btnName = "Save"

  // options: SortablejsOptions = {
  //   group: 'test',
  //   onUpdate: () => {
  //     console.log('updated');
  //   },
  //   onAdd: () => {
  //     console.log('added');
  //   },
  //   onRemove: () => {
  //     console.log('removed');
  //   },
  // };

  //     list = [{
  //     name: "Top",
  //     children: [
  //       {
  //         name: "Child",
  //         children: null
  //       }
  //     ]
  //   },
  //   {
  //       name: "Location",
  //       children: [
  //         {
  //           name: "Location 1",
  //           children: null
  //         },
  //         {
  //           name: "Location 2",
  //           children: null
  //         }
  //       ]
  //   }
  // ];
  questionarray: any = [];
  Questions: boolean = false;
  Settings: boolean = true;
  questionshow: boolean;
  title: any;
  formdata: any;
  QuizTitle:string='Add Section'
  QuesTitle:string='Select Question'
  defaultThumb: any = "assets/images/category.jpg";
  profileUrl: any = "assets/images/category.jpg";
  result: any;
  errorMsg: any;
  random: any;
  qOrder: any = [];
  qNav: any = [];
  qComp: any = [];
  qBeh: any = [];
  qScore: any = [];
  qPage: any = [];
  qAttempt: any = [];
  qDelay: any = [];
  qselect: any = [];
  qtype: any = [];
  qcategory: any = [];
  max: any;
  qdata = {
    qselecttype: "",
    quetype: "",
    quecat: "",
    quecnt: "",
  };
  section = {
    sid: 0,
    sname: ""
  };


  qquestions = [];
  questions = [];

  header: SuubHeader;

  addquestiontype = [
    {
      id: 1,
      name: "Pre Made"
    },
    {
      id: 2,
      name: "Random"
    }
  ];
  questiontype = [
    {
      id: 1,
      name: "Multiple"
    },
    {
      id: 2,
      name: "Matching"
    },
    {
      id: 3,
      name: "Short Answer"
    }
  ];


  rows: any = [];
  selected: any = [];
  isSave: boolean = false;
  category: any;

  onchange(event: any) {
    if (event.target.value < 0) {
      event.target.value = 0;
    }
    if (event.target.value > 100) {
      event.target.value = 100;
    }
  }
  labels: any = [
    { labelname: '', bindingProperty: '', componentType: 'checkbox' },
    { labelname: 'Question(s)', bindingProperty: 'questionText', componentType: 'html' },
    { labelname: 'Question Format', bindingProperty: 'qType', componentType: 'text' },
  ]
  columns = [
    {
      prop: "selected",
      name: "",
      sortable: false,
      canAutoResize: false,
      draggable: false,
      resizable: false,
      headerCheckboxable: true,
      checkboxable: true,
      width: 30
    },
    // { prop: "qName", name: "Question(s)" },
    { prop: "questionText", name: "Question(s)" },
    // { prop: 'qpoint', name: 'Point' },
    { prop: "qType", name: "Question Format" }
  ];

  catData: any = undefined;
  loader: any;
  notFound: any;
  quizQue: any;
  sectionshow: boolean = false;
  userLoginData: any;
  userdata: any;
  questionslist: any = [];
  nocnt: boolean = false;
  cnterr: boolean = false;
  constructor(
    private spinner: NgxSpinnerService,
    protected service: AddeditquizService,
    protected webApiService: webAPIService,
    // private toasterService: ToasterService,
    private toastr: ToastrService,
    private router: Router, private cdf: ChangeDetectorRef,
    private route: ActivatedRoute,
    private http1: HttpClient
  ) {
    this.rows = this.questions;
    // console.log("Add Category Called");
    var category;
    var id;
    if (localStorage.getItem("LoginResData")) {
      this.userLoginData = JSON.parse(localStorage.getItem("LoginResData"));
      this.userdata = this.userLoginData.data.data;
      console.log("login data", this.userdata);
    }
    this.getallDrop();
    this.getHelpContent();
    if (this.service.data != undefined) {
      category = this.service.data.data;
      id = this.service.data.id;
      this.catData = this.service.data.data;
    } else {
      id = 0;
    }
    console.log("Service Data:", this.service.data);
    console.log("Category Info", category);
    console.log("id", id);
    this.category=category
    if (id == 1) {
      if (category.feed) {
        var feed = category.feed.split("#");
        var feedData = [];
        for (let i = 0; i < feed.length; i++) {
          var feedobj = feed[i].split(",");
          var data = {
            // qfid:'',
            qfeedmin: feedobj[0],
            qfeedmax: feedobj[1],
            qgrade: feedobj[2],
            qfeedtext: feedobj[3],
            qfeedimg: feedobj[4]
          };
          feedData.push(data);
        }
      }

      // this.title = "";
      this.formdata = {
        qcode: category.qcode,
        qid: category.qid,
        qname: category.qName,
        picRef: category.picRef ? category.picRef : this.defaultThumb,
        qdesc: category.qdesc,
        qinstru: category.inst,
        // qddesc:category.qddesc,
        qotime: category.oDate,
        qctime: category.cDate,
        // qtimel:category.qtimel,
        qetimemin: category.timelimit,
        qorderid: category.qOrder,
        qordername: category.qOrderLabel,
        qnavid: category.nav,
        qnav: category.navLabel,
        qonpageid: category.qPage,
        qonpage: category.qPageLabel,
        qattemptcompid: category.compCrit,
        qattemptcomp: category.compCritLabel,
        qbehavid: category.behav,
        qbehav: category.behavLabel,
        qmutiid: category.att,
        qmult: category.attLabel,
        qscoreid: category.scoreSel,
        qscore: category.scoreSelLabel,
        qdelayid: category.dba,
        qdelay: category.dbaLabel,
        qdelaydays: category.delayTime,
        qenpass: category.qpassword == 1 ? true : false,
        qenpassText: category.qpasswordText,
        passingMarks: category.passingMarks,
        qsaddquetype: category.qsaddquetype,
        qsquetype: category.qsquetype,
        qfeed: feedData ? feedData : [],
        qsque: category.qsque
      };
      var dataq = {
        tId: this.userdata.tenantId,
        qId: this.formdata.qid
      };
      this.getquizQuestion(dataq);
      this.questionarray = this.formdata.qsque;
    } else {
      // this.title = "Add Quiz";
      this.formdata = {
        qcode: "",
        qid: 0,
        qname: "",
        picRef: this.defaultThumb,
        qdesc: "",
        qinstru: "",
        // qddesc:'',
        qotime: "",
        qctime: "",
        // qtimel:'',
        qetimemin: "",
        qorderid: "",
        qordername: "",
        qnavid: "",
        qnav: "",
        qonpageid: "",
        qonpage: "",
        qattemptcompid: "",
        qattemptcomp: "",
        qbehavid: "",
        qbehav: "",
        qmutiid: "",
        qmult: "",
        qscoreid: "",
        qscore: "",
        qenpass: "",
        qenpassText: "",
        passingMarks: "",
        qdelayid: "",
        qdelay: "",
        qdelaydays: "",
        qfeed: [
          {
            // qfid:'',
            qfeedmin: "",
            qfeedmax: "",
            qgrade: "",
            qfeedtext: "",
            qfeedimg: "assets/images/category.jpg",
            qvisibel: 1
          }
        ],
        qsaddquetype: "",
        qsquetype: "",
        qsque: [
          {
            qsid: "",
            qsname: "",
            qpoint: "",
            qtype: ""
          }
        ]
        // qinstru:'',
        // qmutiid:'',
        // qscoreid:'',
        // qattemptcomp:'',
      };
    }

    this.QuestionListOptions = {
      onUpdate: (event: any) => {
        this.submitque();
      },
    };
  }

  getUnique(arr, comp) {
    //store the comparison  values in array
    const unique = arr
      .map(e => e[comp])
      // store the keys of the unique objects
      .map((e, i, final) => final.indexOf(e) === i && i)
      // eliminate the dead keys & return unique objects
      .filter(e => arr[e])
      .map(e => arr[e]);

    return unique;
  }

  back() {
    this.router.navigate(["/pages/assessment/quiz"]);
  }

  cloeSidebar() {
    this.questionshow = false
  }
  closeSection() {
    this.sectionshow = false;
  }
  getallDrop() {
    var datadrop = {
      tId: this.userdata.tenantId
    };

    this.spinner.show();

    this.service.alldropdowm(datadrop).then(
      rescompData => {
        this.spinner.hide();
        this.qOrder = rescompData['data']['qOrder'];
        this.qNav = rescompData['data']['qNav'];
        this.qComp = rescompData['data']['qComp'];
        this.qBeh = rescompData['data']['qBeh'];
        this.qScore = rescompData['data']['qScore'];
        this.qPage = rescompData['data']['qPage'];
        this.qAttempt = rescompData['data']['qAttempt'];
        this.qDelay = rescompData['data']['qDelay'];
        this.qtype = rescompData['data']['qtype'];
        this.qselect = rescompData['data']['qselect'];
        this.qcategory = rescompData['data']['qcategory'];
        console.log("quiz Drop Result", rescompData);
      },
      resUserError => {
        this.spinner.hide();
        this.errorMsg = resUserError;
        this.notFound = true;
        // this.router.navigate(['**']);
      }
    );
  }
  getquizQuestion(dataq) {
    this.spinner.show();
    this.service.quizQuestions(dataq).then(
      rescompData => {
        this.spinner.hide();
        this.quizQue = rescompData['data'][0];
        console.log("quizQue Result", this.quizQue);
        var tempQue = [];
        var tempQueunique = [];
        if (this.quizQue) {
          for (let i = 0; i < this.quizQue.length; i++) {
            var que = {
              sid: this.quizQue[i].sid,
              sname: this.quizQue[i].sName,
              qid: this.quizQue[i].qid,
              sque: []
            };
            tempQue.push(que);
          }
          // let uniq = a => [...new Set(a)];
          tempQueunique = this.getUnique(tempQue, "sid");
          console.log("tempQueunique", tempQueunique);
          for (let j = 0; j < tempQueunique.length; j++) {
            for (let i = 0; i < this.quizQue.length; i++) {
              if (
                tempQueunique[j].sid == this.quizQue[i].sid &&
                this.quizQue[i].qqid
              ) {
                tempQueunique[j].sque.push(this.quizQue[i]);
              }
            }
          }
        }

        for(let i =0; i < tempQueunique.length; i ++) {
          for(let j= 0; j < tempQueunique[i].sque.length; j++) {
            if(tempQueunique[i].sque[j].qSelectLabel == "Random") {
              tempQueunique[i].sque[j].questionText = "A random question will be selected from the question bank when the questionnaire is generated"
            }
          }
        }

        this.qquestions = tempQueunique ? tempQueunique : [];
        console.log("this.questions", this.qquestions);
      },
      resUserError => {
        this.spinner.hide();
        this.errorMsg = resUserError;
        this.notFound = true;
        // this.router.navigate(['**']);
      }
    );
  }

 

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }


  AddFeed() {
    var data = {
      // qfid:'',
      qfeedmin: "",
      qfeedmax: "",
      qgrade: "",
      qfeedtext: "",
      qfeedimg: this.defaultThumb,
      qvisibel: 1
    };
    this.formdata.qfeed.push(data);
  }

  Removefeed(fid) {
    for (let i = 0; i < this.formdata.qfeed.length; i++) {
      if (fid == i) {
        this.formdata.qfeed.splice(i, 1);
      }
    }
  }
  deldata: any;
  Removeque(qqid, qid) {
    var data = {
      qId: qid,
      questionId: qqid
    };

    // for(let i=0;i<this.qquestions.length;i++)
    // {
    //   if(sid ==i)
    //   {
    //     for(let j=0;j<this.qquestions[i].sque.length;j++)
    //     {
    //       if(qid == j )
    //       {
    //          this.qquestions[i].sque[j].splice(j,1)
    //       }
    //     }

    //   }
    // }
    let url = webApi.domain + webApi.url.deletequizquestion;

    this.webApiService.getService(url, data).then(
      rescompData => {
        this.spinner.hide();
        var temp1: any = rescompData;
        this.deldata = temp1.data;
        if (temp1 == "err") {
          // this.notFound = true;
          // var catUpdate: Toast = {
          //   type: "error",
          //   title: "Quiz Question",
          //   body: "Unable to delete Quiz question.",
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
        } else if (temp1.cnt == 0) {
          // var catUpdate: Toast = {
          //   type: "error",
          //   title: "Quiz Question",
          //   body: this.deldata.msg,
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
        } else {
          // this.router.navigate(['/pages/assessment/quiz']);
          // var catUpdate: Toast = {
          //   type: "success",
          //   title: "Quiz Question",
          //   // body: "Unable to update category.",
          //   body: this.deldata.msg,
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          var dataq = {
            tId: this.userdata.tenantId,
            qId: data.qId
          };
          this.getquizQuestion(dataq);
          // this.basic= false;
          // this.type=true;
          // this.toasterService.pop(catUpdate);
          this.presentToast('success', this.deldata.msg);
        }
        console.log("Question AddEdit Result ", this.deldata);
      },
      resUserError => {
        this.spinner.hide();
        this.errorMsg = resUserError;
      }
    );
  }
  delsecdata: any;
  Removesec(sid, qid) {
    var data = {
      qId: qid,
      sId: sid
    };
    this.spinner.show();
    // for(let i=0;i<this.qquestions.length;i++)
    // {
    //   if(sid ==i)
    //   {
    //     for(let j=0;j<this.qquestions[i].sque.length;j++)
    //     {
    //       if(qid == j )
    //       {
    //          this.qquestions[i].sque[j].splice(j,1)
    //       }
    //     }

    //   }
    // }
    let url = webApi.domain + webApi.url.deletequizsection;

    this.webApiService.getService(url, data).then(
      rescompData => {
        this.spinner.hide();
        var temp1: any = rescompData;
        this.delsecdata = temp1.data;
        if (temp1 == "err") {
          // this.notFound = true;
          // var catUpdate: Toast = {
          //   type: "error",
          //   title: "Quiz Section",
          //   body: "Unable to delete Quiz Section.",
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
        } else if (temp1.cnt == 0) {
          // var catUpdate: Toast = {
          //   type: "error",
          //   title: "Quiz Section",
          //   body: this.delsecdata.msg,
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
        } else {
          // this.router.navigate(['/pages/assessment/quiz']);
          // var catUpdate: Toast = {
          //   type: "success",
          //   title: "Quiz Section",
          //   // body: "Unable to update category.",
          //   body: this.delsecdata.msg,
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          var dataq = {
            tId: this.userdata.tenantId,
            qId: data.qId
          };
          this.getquizQuestion(dataq);
          // this.basic= false;
          // this.type=true;
          // this.toasterService.pop(catUpdate);
          this.presentToast('success', this.delsecdata.msg);
        }
        console.log("Question AddEdit Result ", this.deldata);
      },
      resUserError => {
        this.spinner.hide();
        this.errorMsg = resUserError;
      }
    );
  }

  save(qaddtype, qtype) {
    var questionarray = this.questionarray;
    this.questionshow = false;
    if (qaddtype == 1) {
      if (this.selected.length > 0) {
        for (let j = 0; j < questionarray.length; j++) {
          for (let i = 0; i < this.selected.length; i++) {
            if (this.selected[i].qsid == questionarray[j].qsid) {
              this.selected.splice(i, 1);
            }
            // this.questionarray.push(this.selected[i])
          }
        }
        for (let i = 0; i < this.selected.length; i++) {
          this.questionarray.push(this.selected[i]);
        }
      }
    } else if (qaddtype == 2) {
      // this.random.qtype=
      this.selected = [];
      this.questionarray.push(this.random);
    }
  }
  Qtypechange(data) {
    console.log(data);
    this.qdata.quecnt = '';
    this.selected = [];
    console.log(this.qdata);
    if (data == 2) {
      this.random = {
        qsid: "",
        qsname: "Random",
        qpoint: "",
        qtype: ""
      };
      this.questionShows = false;
    }
    if (this.qdata.qselecttype === '2' && this.qdata.quetype && this.qdata.quecat) {
      this.getcount(this.qdata);
    }
  }
  getcount(data) {
    this.spinner.show();
    let param = {
      qType: data.qselecttype,
      qFormat: data.quetype,
      catId: data.quecat,
    }
    this.service.getquizcount(param).then(res => {
      console.log(res);
      if (res['type'] == true) {
        this.max = res['data'][0].qCnt;
        console.log(this.max);
        this.cnterr = false;
        if (this.max == 0 || this.max == null || this.max == undefined) {
          this.nocnt = true;
        } else {
          this.nocnt = false;
        }
      }
      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      console.log(err);
    })
  }
  closeModel() {
    this.qdata = {
      qselecttype: "",
      quetype: "",
      quecat: "",
      quecnt: "",
    };
    this.questions = [];
    this.questionShows = false;
    this.questionshow = false;
    this.nocnt = false;
    this.cnterr = false;
  }
  closesectionModel() {
    this.sectionshow = false;
  }
  addsection() {
    this.sectionshow = true;
  }
  addmodel(sid) {
    this.isSave = false
   
    this.sectionid = sid;

    this.questionshow = true;
  }

  onSelect(selected) {
  
    this.selected = selected;
  }
  onActivate(event) {
    console.log("Activate Event", event);
  }
  quizImgData: any;
  readCategoryThumb(event: any) {
    var validExts = new Array(".png", ".jpg", ".jpeg");
    var fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf("."));
    if (validExts.indexOf(fileExt) < 0) {
     
      this.presentToast('warning', 'Valid file types are ' + validExts.toString());
      // this.deleteCourseThumb();
    } else {
      if (event.target.files && event.target.files[0]) {
        this.quizImgData = event.target.files[0];
        this.formdata.qfileName = event.target.files[0].name;

        var reader = new FileReader();

        reader.onload = (event: ProgressEvent) => {
          // this.defaultThumb = (<FileReader>event.target).result;
          this.formdata.picRef = (<FileReader>event.target).result;
        };
        reader.readAsDataURL(event.target.files[0]);
      }
    }
  }


  clicked = false;

  checkingAll() {
    
  }

 

  deleteCategoryThumb() {
    // this.defaultThumb = 'assets/images/category.jpg';
    this.formdata.picRef = "assets/images/category.jpg";
    this.quizImgData = undefined;
    this.formdata.qfileName = "";
    this.formdata.categoryPicRefs = undefined;
  }

  readfeedimg(event: any, ind) {
    var size = 100000;
    var validExts = new Array("image");
    var fileExt = event.target.files[0].type;
    fileExt = fileExt.substring(0, 5);
    if (size <= event.target.files[0].size) {
      // var toast: Toast = {
      //   type: "error",
      //   title: "file size exceeded!",
      //   body: "files size should be less than 100KB",
      //   showCloseButton: true,
      //   timeout: 4000
      // };
      // this.toasterService.pop(toast);
      this.presentToast('warning', 'File size should be less than 100KB');
      event.target.value = "";
      // this.deleteCourseThumb();
    } else {
      if (validExts.indexOf(fileExt) < 0) {
        // var toast: Toast = {
        //   type: "error",
        //   title: "Invalid file selected!",
        //   body: "Valid files are of " + validExts.toString() + " types.",
        //   showCloseButton: true,
        //   timeout: 4000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('warning', 'Valid file types are ' + validExts.toString());
      } else if (event.target.files && event.target.files[0]) {
        for (let i = 0; i < this.formdata.qfeed.length; i++) {
          if (i == ind) {
            this.formdata.qfeed[i].qffileName = event.target.files[0].name;
            this.formdata.qfeed[i].option = event.target.files[0];
            var reader = new FileReader();

            reader.onload = (event: ProgressEvent) => {
              // this.defaultThumb = (<FileReader>event.target).result;
              this.formdata.qfeed[i].qfeedimg = (<FileReader>(
                event.target
              )).result;
            };
            reader.readAsDataURL(event.target.files[0]);
          }
        }
      }
    }
  }

  cancelfeed(ind) {
    for (let i = 0; i < this.formdata.qfeed.length; i++) {
      if (i == ind) {
        // this.multiple.moption[i].qfileName="No File Chosen"
        this.formdata.qfeed[i].qffileName = "";
        this.formdata.qfeed[i].qfeedimg = "assets/images/category.jpg";
        this.formdata.qfeed[i].option = undefined;
        // this.formdata.qfeed[i].option
      }
    }
  }

  fileUploadRes: any = [];
  submitquizimg(f) {
    console.log(this.formdata.qid,"qid")
    if (f.valid) {
      if(this.checkQuizSetting()){
        this.spinner.show();
        var quiz = {
          qid: this.formdata.qid,
          qcode: this.formdata.qcode,
          qname: this.formdata.qname,
          picRef: this.formdata.picRef,
          qdesc: this.formdata.qdesc,
          qinstru: this.formdata.qinstru,
          // qddesc:category.qddesc,
          qotime: this.formdata.qotime,
          qctime: this.formdata.qctime,
          // qtimel:category.qtimel,
          qetimemin: this.formdata.qetimemin,
          qorderid: this.formdata.qorderid,
          qordername: this.formdata.qordername,
          qnavid: this.formdata.qnavid,
          qnav: this.formdata.qnav,
          qonpageid: this.formdata.qonpageid,
          qonpage: this.formdata.qonpage,
          qattemptcompid: this.formdata.qattemptcompid,
          qattemptcomp: this.formdata.qattemptcomp,
          qbehavid: this.formdata.qbehavid,
          qbehav: this.formdata.qbehav,
          qmutiid: this.formdata.qmutiid,
          qmult: this.formdata.qmult,
          qscoreid: this.formdata.qscoreid,
          qscore: this.formdata.qscore,
          qdelayid: this.formdata.qdelayid,
          qdelay: this.formdata.qdelay,
          qdelaydays: this.formdata.qdelaydays,
          qenpass: this.formdata.qenpass == true ? 1 : 0,
          qenpassText: this.formdata.qenpassText,
          passingMarks: this.formdata.passingMarks,
          qfeed: this.formdata.qfeed,
          visible: 1,
          tenantId: this.userdata.tenantId,
          userId: this.userdata.id
        };

        console.log("category", quiz);
        var fd = new FormData();
        fd.append("content", JSON.stringify(quiz));
        fd.append("file", this.quizImgData);

        console.log("Question file", this.quizImgData);
        console.log("Question Data ", quiz);

        // let url = webApi.domain + webApi.url.addeditqbcategoryquestion;
        let fileUploadUrl = webApi.domain + webApi.url.multifileUpload;
        // let fileUploadUrlLocal = 'http://localhost:9845' + webApi.url.fileUpload;
        if (this.quizImgData != undefined) {
          this.webApiService.getService(fileUploadUrl, fd).then(
            rescompData => {
              console.log(rescompData,"rescompData")
              this.spinner.hide();
              var temp: any = rescompData;
              this.fileUploadRes = rescompData;
              if (temp == "err") {
                // this.notFound = true;
                // var thumbUpload: Toast = {
                //   type: "error",
                //   title: "Quiz",
                //   body: "Unable to upload Question Thumbnail.",
                //   // body: temp.msg,
                //   showCloseButton: true,
                //   timeout: 4000
                // };
                // this.toasterService.pop(thumbUpload);
                this.presentToast('error', '');
              } else if (temp.type == false) {
                // var thumbUpload: Toast = {
                //   type: "error",
                //   title: "Quiz ",
                //   body: "Unable to upload Question Thumbnail",
                //   // body: temp.msg,
                //   showCloseButton: true,
                //   timeout: 4000
                // };
                // this.toasterService.pop(thumbUpload);
                this.presentToast('error', '');
              } else {
                if (this.fileUploadRes.length > 0) {
                  quiz.picRef = this.fileUploadRes[0];
                  this.savemultImageData(quiz);
                } else {
                  // var thumbUpload: Toast = {
                  //   type: "error",
                  //   title: "Quiz",
                  //   // body: "Unable to upload category thumbnail.",
                  //   body: "Unable to upload Question Thumbnail",
                  //   showCloseButton: true,
                  //   timeout: 4000
                  // };
                  // this.toasterService.pop(thumbUpload);
                  this.presentToast('error', '');
                }
              }
              console.log("File Upload Result", this.fileUploadRes);
            },
            resUserError => {
              this.spinner.hide();
              this.errorMsg = resUserError;
            }
          );
        } else {
          this.savemultImageData(quiz);
        }
      }

    } else {
      console.log('Please Fill all fields');
      // const addEditF: Toast = {
      //   type: 'error',
      //   title: 'Unable to update',
      //   body: 'Please Fill all fields',
      //   showCloseButton: true,
      //   timeout: 2000,
      // };
      // this.toasterService.pop(addEditF);
      this.presentToast('warning', 'Please fill in the required fields');
      Object.keys(f.controls).forEach(key => {
        f.controls[key].markAsDirty();
      });
    }
  }

  checkQuizSetting(){
   if(this.formdata.qnavid == 1 && this.formdata.qbehavid == 1){
    this.toastr.warning('Navigation sequential and Behaviour defered not allowed', 'Warning');
    this.onAttemptConditionChange(true);
    this.onAttemptMutliChange(true);
    return false;
   }else {
     return true;
   }
  }
  qbMultiple: any;
  multcontent: any = [];
  mutimgarr: any = [];
  multTemparr: any = [];
  multfileUploadRes: any = [];

  savemultImageData(quiz) {
    console.log(quiz, "quiz")
    this.spinner.show();
    this.mutimgarr = [];
    this.multTemparr = [];
    this.multcontent = [];

    for (let i = 0; i < quiz.qfeed.length; i++) {
      if (quiz.qfeed[i].option) {
        this.mutimgarr.push(quiz.qfeed[i].option);
        this.multTemparr.push(i);
        quiz.qfeed[i].name = quiz.qfeed[i].qffileName;
        quiz.qfeed[i].author = quiz.qid;
        this.multcontent.push(quiz.qfeed[i]);
      }
      quiz.qfeed[i].author = "author";
    }

    // console.log('mutimgarr',this.mutimgarr);
    // console.log('multTemparr',this.multTemparr);
    //  console.log('multcontent',this.multcontent);

    var fd = new FormData();
    fd.append("content", JSON.stringify(this.multcontent));
    for (let i = 0; i < this.mutimgarr.length; i++) {
      fd.append("file", this.mutimgarr[i]);
    }

    // console.log('Question file',this.categoryImgData);
    // console.log('Question Data ',category);

    // let url = webApi.domain + webApi.url.addeditqbcategoryquestion;
    let fileUploadUrl = webApi.domain + webApi.url.multifileUpload;
    // let fileUploadUrlLocal = 'http://localhost:9845' + webApi.url.fileUpload;
    if (this.mutimgarr.length > 0) {
      this.webApiService.getService(fileUploadUrl, fd).then(
        (rescompData: any) => {
          this.spinner.hide();
          var temp: any = rescompData;
          this.multfileUploadRes = rescompData;
          if (temp == "err") {
            // this.notFound = true;
            // var thumbUpload: Toast = {
            //   type: "error",
            //   title: "Quiz",
            //   body: "Unable to upload Quiz Feedback Images.",
            //   // body: temp.msg,
            //   showCloseButton: true,
            //   timeout: 4000
            // };
            // this.toasterService.pop(thumbUpload);
            this.presentToast('error', '');
          } else {
            if (this.multfileUploadRes.length > 0) {
              var imageData = [];
              for (let i = 0; i < this.multfileUploadRes.length; i++) {
                imageData.push(this.multfileUploadRes[i]);
              }
              // var imgData = this.multfileUploadRes.data.file_url;
              this.saveMultiple(quiz, imageData);
            } else {
              // var thumbUpload: Toast = {
              //   type: "error",
              //   title: "Quiz",
              //   // body: "Unable to upload category thumbnail.",
              //   body: "Unable to upload  Quiz Feedback Images",
              //   showCloseButton: true,
              //   timeout: 4000
              // };
              // this.toasterService.pop(thumbUpload);
              this.presentToast('error', '');
            }
          }
          console.log("File Upload Result", this.multfileUploadRes);
        },
        resUserError => {
          this.spinner.hide();
          this.errorMsg = resUserError;
        }
      );
    } else {
      var imgData = [];
      this.saveMultiple(quiz, imgData);
    }
  }
  saveMultiple(quiz, data) {
    this.spinner.show();
    console.log("multiple", quiz);
    console.log("data", data);

    for (let i = 0; i < quiz.qfeed.length; i++) {
      if (quiz.qfeed[i].name) {
        delete quiz.qfeed[i].name;
      }
      if (quiz.qfeed[i].author) {
        delete quiz.qfeed[i].author;
      }

      if (quiz.qfeed[i].option) {
        delete quiz.qfeed[i].option;
      }
      //    if(multiple.moption[i].optionRef)
      // {
      //     multiple.moption[i].optionRef ="";
      // }
      for (let j = 0; j < data.length; j++) {
        if (data[j].includes(quiz.qfeed[i].qffileName)) {
          quiz.qfeed[i].qfeedimg = data[j];
          break;
        }
      }
      if (quiz.qfeed[i].qffileName) {
        delete quiz.qfeed[i].qffileName;
      }
      // quiz.qfeed[i].qtypeid=Number(quiz.moption[i].qtypeid)
    }
    var option: string = Array.prototype.map
      .call(quiz.qfeed, function (item) {
        console.log("item", item);
        return Object.values(item).join("#");
      })
      .join("|");
    quiz.option = option;
    console.log("this.multiple", quiz);
    console.log("this.string", option);

    let url = webApi.domain + webApi.url.addeditquiz;

    this.webApiService.getService(url, quiz).then(
      rescompData => {
        console.log(rescompData, "rescompdata")
        this.spinner.hide();
        var temp1: any = rescompData;
        this.qbMultiple = temp1.data;
        if (temp1 == "err") {
          // this.notFound = true;
          // var catUpdate: Toast = {
          //   type: "error",
          //   title: "Quiz",
          //   body: "Unable to update question settings.",
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
        } else if (temp1.type == false) {
          // var catUpdate: Toast = {
          //   type: "error",
          //   title: "Quiz",
          //   body: this.qbMultiple.msg,
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
        } else {
          //this.router.navigate(["/pages/assessment/quiz"]);
          // var catUpdate: Toast = {
          //   type: "success",
          //   title: "Quiz",
          //   // body: "Unable to update category.",
          //   body: this.qbMultiple.msg,
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          // this.basic= false;
          // this.type=true;
          // this.toasterService.pop(catUpdate);
          this.presentToast('success', this.qbMultiple.msg);
          this.formdata.qid = this.qbMultiple.id;
        }
        console.log("Question AddEdit Result ", this.qbMultiple);
        this.Settings = false;
        this.Questions = true;
        this.header = {
          title:this.category?this.category.qName:"Add Quiz",
          btnsSearch: true,
          btnName3: 'Add Section',
          btnName4: 'Save',
          btnName3show: true,
          btnName4show: true,
          btnBackshow: true,
          showBreadcrumb: true,
          breadCrumbList: [
            {
              'name': 'Assessment',
              'navigationPath': '/pages/assessment',
            },
            {
              'name': 'Quiz',
              'navigationPath': '/pages/assessment/quiz',
            },
          ]
        };
      },
      resUserError => {
        this.spinner.hide();
        this.errorMsg = resUserError;
      }
    );
  }
  qbsection: any;
  disabledSaveSection = false;
  saveection(form,section, id, jid) {
    if(form.valid){
    this.spinner.show();
    this.disabledSaveSection = true;
    this.cdf.detectChanges();
    var theContent = "";
    if (id == 1) {
      var contedit = "contedit" + jid;
      theContent = document.getElementById(contedit).innerHTML;
    }
    var data = {
      sid: section.sid,
      qid: this.qbMultiple
        ? this.qbMultiple.id
          ? this.qbMultiple.id
          : this.formdata.qid
        : this.formdata.qid,
      qname: theContent ? theContent : section.sname,
      visible: 1,
      tId: this.userdata.tenantId,
      userId: this.userdata.id
    };
    let url = webApi.domain + webApi.url.addeditsection;
    if (data.qid) {
      this.webApiService.getService(url, data).then(
        rescompData => {

          var temp1: any = rescompData;
          this.qbsection = temp1.data;
          if (temp1 == "err") {
            // this.notFound = true;
            // var catUpdate: Toast = {
            //   type: "error",
            //   title: "Quiz Question",
            //   body: "Unable to Update Quiz Section.",
            //   showCloseButton: true,
            //   timeout: 4000
            // };
            // this.toasterService.pop(catUpdate);
            this.presentToast('error', '');
          } else if (temp1.type == false) {
            // var catUpdate: Toast = {
            //   type: "error",
            //   title: "Question Section",
            //   body: this.qbsection.msg,
            //   showCloseButton: true,
            //   timeout: 4000
            // };
            // this.toasterService.pop(catUpdate);
            this.presentToast('error', '');
          } else {
            // this.router.navigate(['/pages/assessment/quiz']);
            // var catUpdate: Toast = {
            //   type: "success",
            //   title: "Question Section",
            //   // body: "Unable to update category.",
            //   body: this.qbsection.msg,
            //   showCloseButton: true,
            //   timeout: 4000
            // };
            // this.toasterService.pop(catUpdate);
            this.presentToast('success', this.qbsection.msg);
            var dataq = {
              tId: this.userdata.tenantId,
              qId: data.qid
            };
            this.getquizQuestion(dataq);
            this.sectionshow = false;
            this.section.sname = "";
            // this.spinner.hide();
          }
          this.spinner.hide();
          this.disabledSaveSection = false;
          this.cdf.detectChanges();
          console.log("Question AddEdit Result ", this.qbsection);
        },
        resUserError => {
          this.spinner.hide();
          this.disabledSaveSection = false;
          this.cdf.detectChanges();
          this.errorMsg = resUserError;
        }
      );
    } else {
      // var catUpdate: Toast = {
      //   type: "error",
      //   title: "Quiz Question",
      //   body: "Please fill setting details",
      //   showCloseButton: true,
      //   timeout: 4000
      // };
      // this.toasterService.pop(catUpdate);
      this.presentToast('warning', 'Please fill setting details');
      this.sectionshow = false;
      this.disabledSaveSection = false;
      this.cdf.detectChanges();
    }
  }else {
    Object.keys(form.controls).forEach(key => {
      form.controls[key].markAsDirty();
    });
  }
  }
  searchQue() {
    const catdata = {
      qFormat: this.qdata.quetype,
      catId: this.qdata.quecat,
      tId: this.userdata.tenantId,
    };

    // this.service.getqbdropdowun(catdata)
    // .subscribe(rescompData => {
    //   // this.loader =false;
    //   this.level = rescompData.data[0];
    //   console.log('Category Result',this.category)
    // },
    // resUserError=>{
    //   // this.loader =false;
    //   this.errorMsg = resUserError;
    //   // this.notFound = true;
    //   // this.router.navigate(['**']);
    // });
    this.spinner.show();

    this.service.getcategoryquestion(catdata).then(
      rescompData => {
        // this.loader =false;
        this.spinner.hide();
        this.questionslist = [];
        this.questions = rescompData['data'][0];
        for (let i = 0; i < this.quizQue.length; i++) {
          for (let j = 0; j < this.questions.length; j++) {
            if (this.quizQue[i].questionId == this.questions[j].qid) {
              this.questions.splice(j, 1);
            }
          }
        }
        // for (let i = 0; i < this.quizQue.length; i++) {
        // for (let j = 0; j < this.questions.length; j++) {
        //   if (this.questions[j].visible === 0) {
        //     this.questions.splice(j, 1);
        //   }
        // }
        var i = this.questions.length;
        while (i--) {
          if (this.questions[i].visible == 0) {
            this.questions.splice(i, 1);
          }
        }


        // }
        this.questionslist = this.questions;
        this.questionShows = true;
        // this.tempQuestion=this.questions;
        console.log("Category Result", this.questions);
      },
      resUserError => {
        this.spinner.hide();
        this.loader = false;
        this.errorMsg = resUserError;
        // this.notFound = true;
        // this.router.navigate(['**']);
      }
    );
  }

  inert: any;

  insertque(qdata) {
    // this.isSave = true
    if (qdata.qselecttype == 1) {
      this.insertQuizQue(qdata);
    } else if (qdata.qselecttype == 2) {
      if (qdata.quecnt > this.max || qdata.quecnt < 1) {
        this.cnterr = true;
      } else {
        this.insertQuizQue(qdata);
      }
    }
  }
  insertQuizQue(qdata) {

    var data = [];
    if (qdata.qselecttype == 1) {
      console.log(this.selected);
      if (this.selected.length > 0) {
        for (let i = 0; i < this.selected.length; i++) {
          var item = {
            qSectionId: this.sectionid,
            qSelectId: qdata.qselecttype,
            typeId: qdata.quetype,
            questionId: this.selected[i].qid,
            depQuestionId: 0,
            depQuestionValue: null,
            qOrder: 0,
            visible: 1,
            catId: qdata.quecat,
            quecnt: 0,
          };
          data.push(item);
        }
        // this.selected = [];
        // this.sectionid = '';
        this.addquestions(data);
      } else {
        this.presentToast('warning', 'Please select a question');
      }
    } else if (qdata.qselecttype == 2) {
      item = {
        qSectionId: this.sectionid,
        qSelectId: qdata.qselecttype,
        typeId: qdata.quetype,
        questionId: '',
        depQuestionId: 0,
        depQuestionValue: null,
        qOrder: 0,
        visible: 1,
        catId: qdata.quecat,
        quecnt: qdata.quecnt,
      };
      data.push(item);
      this.addquestions(data);
      // this.sectionid = '';
      // this.selected = [];
    }
    
  }

  addquestions(data) {
    console.log("data", data);
    var option: string = Array.prototype.map
      .call(data, function (item) {
        console.log("item", item);
        return Object.values(item).join("#");
      })
      .join("|");
    console.log("option", option);
    var que = {
      qId: this.qbMultiple
        ? this.qbMultiple.id
          ? this.qbMultiple.id
          : this.formdata.qid
        : this.formdata.qid,
      option: option,
      tId: this.userdata.tenantId,
      userId: this.userdata.id
    };
    console.log(que);
    let url = webApi.domain + webApi.url.addquizquestion;
    this.webApiService.getService(url, que).then(
      rescompData => {
        // this.isSave = true;
        this.spinner.hide();
        var temp1: any = rescompData;
        this.inert = temp1.data;
        if (temp1 == "err") {
          this.presentToast('error', '');
        } else if (temp1.type == false) {

          this.presentToast('error', '');
        } else {

          this.getquizQuestion(que);
          this.closeModel();
          // this.basic= false;
          // this.type=true;
          // this.toasterService.pop(catUpdate);
          this.presentToast('success', this.inert.msg);
          // this.router.navigate(["/pages/assessment/quiz"]);
        }
        this.selected = [];
        console.log("Question AddEdit Result ", this.inert);
      },
      resUserError => {
        this.spinner.hide();
        this.errorMsg = resUserError;
      });
  }


  selectTab(item) {
    if (item.tabTitle === "Settings") {
      this.header = {
        title:this.category?this.category.qName:"Add Quiz",
        btnsSearch: true,
        btnName2: 'Save',
        btnName2show: true,
        btnBackshow: true,
        showBreadcrumb: true,
        breadCrumbList: [
          {
            'name': 'Assessment',
            'navigationPath': '/pages/assessment',
          },
          {
            'name': 'Quiz',
            'navigationPath': '/pages/assessment/quiz',
          },
        ]
      };
      // this.selectedActInd =undefined;
      this.Questions = false;
      this.Settings = true;
      this.spinner.show();
      setTimeout(() => {
        this.spinner.hide()
      }, 2000);
    } else if (item.tabTitle === "Questions") {
      // this.selectedActInd =0;
      this.Settings = false;
      this.Questions = true;
      this.header = {
        title:this.category?this.category.qName:"Add Quiz",
        btnsSearch: true,
        btnName3: 'Add Section',
        btnName4: 'Save',
        btnName3show: true,
        btnName4show: true,
        btnBackshow: true,
        showBreadcrumb: true,
        breadCrumbList: [
          {
            'name': 'Assessment',
            'navigationPath': '/pages/assessment',
          },
          {
            'name': 'Quiz',
            'navigationPath': '/pages/assessment/quiz',
          },
        ]
      };
      this.spinner.show();
      setTimeout(() => {
        this.spinner.hide()
      }, 2000);
    }
  }

  submitque() {
    this.spinner.show();
    var Data = [];
    for (let i = 0; i < this.qquestions.length; i++) {
      for (let j = 0; j < this.qquestions[i].sque.length; j++) {
        var item = {
          qqid: this.qquestions[i].sque[j].qqid,
          qSectionId: this.qquestions[i].sid,
          qSelectId: this.qquestions[i].sque[j].qSelectId,
          typeId: this.qquestions[i].sque[j].qTypeId,
          questionId: this.qquestions[i].sque[j].questionId,
          depQuestionId: 0,
          depQuestionValue: null,
          qOrder: j,
          visible: 1,
          points: this.qquestions[i].sque[j].points,
          // mand:this.qquestions[i].sque[j]
        };
        Data.push(item);
      }
    }

    var option: string = Array.prototype.map
      .call(Data, function (item) {
        console.log("item", item);
        return Object.values(item).join("#");
      })
      .join("|");
    console.log("option", option);
    var que = {
      qId: this.qbMultiple
        ? this.qbMultiple.id
          ? this.qbMultiple.id
          : this.formdata.qid
        : this.formdata.qid,
      option: option,
      tId: this.userdata.tenantId,
      userId: this.userdata.id
    };

    let url = webApi.domain + webApi.url.updatequizquestion;

    this.webApiService.getService(url, que).then(
      rescompData => {
        this.spinner.hide();
        var temp1: any = rescompData;
        this.inert = temp1.data;
        if (temp1 == "err") {
          // this.notFound = true;
          // var catUpdate: Toast = {
          //   type: "error",
          //   title: "Question",
          //   body: "Unable to add questions.",
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
        } else if (temp1.type == false) {
          // var catUpdate: Toast = {
          //   type: "error",
          //   title: "Question",
          //   body: this.inert.msg,
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
        } else {
          // this.router.navigate(['/pages/assessment/quiz']);
          // var catUpdate: Toast = {
          //   type: "success",
          //   title: "Question",
          //   // body: "Unable to update category.",
          //   body: this.inert.msg,
          //   showCloseButton: true,
          //   timeout: 4000
          // };

          this.getquizQuestion(que);
          // this.closeModel();
          // this.basic= false;
          // this.type=true;
          // this.toasterService.pop(catUpdate);
          this.presentToast('success', this.inert.msg);
        }
        console.log("Question AddEdit Result ", this.inert);
      },
      resUserError => {
        this.spinner.hide();
        this.errorMsg = resUserError;
      }
    );
  }

  passchange() {
    this.formdata.qenpassText = "";
  }

  // Help Code Start Here //

  helpContent: any;
  getHelpContent() {
    return new Promise(resolve => {
      this.http1
        .get("../../../../../../assets/help-content/addEditCourseContent.json")
        .subscribe(
          data => {
            this.helpContent = data;
            console.log("Help Array", this.helpContent);
          },
          err => {
            resolve("err");
          }
        );
    });
    // return this.helpContent;
  }
  temp: any;
  // Help Code Ends Here //
  searchquestion(event) {
    const val = event.target.value.toLowerCase();
    console.log(val);

    this.cdf.detectChanges();
    if(val.length>=3||val.length==0){
    this.temp = [...this.questions]
    const temp = this.temp.filter(function (d) {
      return String(d.questionText).toLowerCase().indexOf(val) !== -1 ||
        d.qType.toLowerCase().indexOf(val) !== -1 ||
        // d.emailId.toLowerCase().indexOf(val) !== -1 ||
        // d.enrolmode.toLowerCase().indexOf(val) !== -1 ||
        // d.enrolDate.toLowerCase().indexOf(val) !== -1 ||
        // d.phoneNo.toLowerCase().indexOf(val) !== -1 ||
        // String(d.enrolmode).toLowerCase().indexOf(val) !== -1 ||
        !val
    });

    // update the rows
    this.questionslist = [...temp];
    // Whenever the filter changes, always go back to the first page
    this.tableDataManual.offset = 0;
  }

  }

  onAttemptConditionChange(event){
    if(event){
      if(this.formdata.qattemptcompid == 1){
        this.formdata.passingMarks = null;
        // this.formdata.qmutiid = 1;
        // this.formdata.qscoreid = 3;
        // this.formdata.qdelayid = null;
      }
    }
  }
  disableDeferedBehaviour = false;
  onNavigationDropDownChange(event){
    if(event){

      if(this.formdata.qnavid == 1){
        this.disableDeferedBehaviour = true;
      }else {
        this.disableDeferedBehaviour = false;
      }
    }

  }

  onAttemptMutliChange(event){
    if(event){
      if(this.formdata.qmutiid == 1){
        this.formdata.qscoreid = 3;
        this.formdata.qdelayid = '';
        this.formdata.qdelaydays = '';
      }
    }
  }
}
