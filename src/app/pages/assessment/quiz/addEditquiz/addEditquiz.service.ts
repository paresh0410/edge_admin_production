import { Injectable, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AppConfig } from '../../../../app.module';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AddeditquizService {
  public data: any;
  private _url: string = '/api/edge/category/addUpdateCategory'; //DevTest
  private _url_drop: string = '/api/web/getallquizdropdown';
  private _url_quiz_que: string = '/api/web/getall_quiz_question';
  private _url_que: string = '/api/web/get_category_question';
  private _urlquizcnt: string = '/api/web/get_random_question_cnt';
  request: Request;

  constructor(
    @Inject('APP_CONFIG_TOKEN') private config: AppConfig,
    private _http: Http, private _http1: HttpClient
  ) {
    //this.busy = this._http.get('...').toPromise();
  }

  alldropdowm(category) {
    let url: any = `${this.config.FINAL_URL}` + this._url_drop;
    // return this._http.post(url,category)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);

    return new Promise(resolve => {
      this._http1
        .post(url, category)
        //.map(res => res.json())
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            resolve('err');
          }
        );
    });
  }
  quizQuestions(category) {
    let url: any = `${this.config.FINAL_URL}` + this._url_quiz_que;
    // return this._http
    //   .post(url, category)
    //   .map((response: Response) => response.json())
    //   .catch(this._errorHandler);
    return new Promise(resolve => {
      this._http1
        .post(url, category)
        //.map(res => res.json())
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            resolve('err');
          }
        );
    });
  }
  getcategoryquestion(data1) {
    let url: any = `${this.config.FINAL_URL}` + this._url_que;
    //   return this._http1
    //     .post(url, data)
    //     .map((response: Response) => response.json())
    //     .catch(this._errorHandler);
    // }
    return new Promise(resolve => {
      this._http1
        .post(url, data1)
        //.map(res => res.json())
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            resolve('err');
          }
        );
    });
  }
  getquizcount(data1) {
    let url: any = `${this.config.FINAL_URL}` + this._urlquizcnt;
    //   return this._http1
    //     .post(url, data)
    //     .map((response: Response) => response.json())
    //     .catch(this._errorHandler);
    // }
    return new Promise(resolve => {
      this._http1
        .post(url, data1)
        //.map(res => res.json())
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            resolve('err');
          }
        );
    });
  }
  _errorHandler(error: Response) {
    console.error(error);
    return Observable.throw(error || 'Server Error');
  }
}
