import { Component, ViewEncapsulation, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { Router, NavigationStart, Routes, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { quizService } from './quiz.service';
import { AddeditquizService } from './addEditquiz/addEditquiz.service';
// import { questionsService } from './questions/questions.service';
import { BaseChartDirective } from 'ng2-charts/ng2-charts';
import { NgxSpinnerService } from 'ngx-spinner';
import { webAPIService } from '../../../service/webAPIService';
import { webApi } from '../../../service/webApi';
import { ToastrService } from 'ngx-toastr';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { Card } from '../../../models/card.model';
import { SuubHeader } from '../../components/models/subheader.model';
import { noData } from '../../../models/no-data.model';

// import { ContentService } from '../content/content.service';

@Component({
  selector: 'quiz',
  styleUrls: ['./quiz.scss'],
  // template:'category works'
  templateUrl: './quiz.html',
  encapsulation: ViewEncapsulation.None,
})
export class quiz implements AfterViewInit {
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No Quiz defined at this time.",
    desc:"Quizzes will appear after they are designed by the instructor. Quizzes are a set of pre-defined or random questions to assess the learner.",
    titleShow:true,
    btnShow:true,
    descShow:true,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/assessment-how-to-create-a-quiz',
  }
  header: SuubHeader  = {
    title:'Quiz',
    btnsSearch: true,
    placeHolder:'Search',
    searchBar: true,
    dropdownlabel: ' ',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: '',
    btnName3: '',
    btnAdd: 'Add Quiz',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: true,
    filter: false,
    showBreadcrumb: true,
    breadCrumbList:[
      {
        'name': 'Assessment',
        'navigationPath': '/pages/assessment',
      },
    ]
  };
  count:number=8
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  notFound: boolean = false;
  query: string = '';
  public getData;
  loginUserdata;
  cardModify: Card = {
    flag: 'Quiz',
    titleProp : 'qName',
    discrption: 'qdesc',
    question: 'qcnt',
    image: 'picRef',
    maxpoints: 'maxscore',
    maxAttemp: 'attLabel',
    hoverlable:   true,
    showBottomQuestion: true,
    showImage: true,
    option:   true,
    showBottomMaximumPoint:   true,
    showBottomMaximumAttemp: true,
    eyeIcon:   true,
    bottomDiv:   true,
    bottomDiscription:   true,
    showBottomList:   true,
    bottomTitle:   true,
    btnLabel: 'Edit',
    customCard: 'quizCard customCard',
    defaultImage: 'assets/images/category.jpg'

  };
  cat: any;
  quiz: any = [];
  duplicate = {
    dname: '',
    ddesc: ''
  }
  errorMsg: string;
  skeleton = false;
  parentcat: any = [
    {
      id: 0,
      name: 'Parent'
    }, {
      id: 1,
      name: 'Assigned'
    },
    {
      id: 2,
      name: 'Recommended'
    },
    {
      id: 3,
      name: 'Special'
    }]
  content1: any = [];
  search: any;
  loader: any;
  QuizDup: boolean = false;
  currentPage = 1;
  totalPages: any;
  pagelmt = 12;
  conentHeight: string;
  offset: number = 300;
  str: any = '';
  isworking = false;
  title: string;
  enable: boolean;
  enableDisableModal: boolean;
  fromSearch: boolean=false;
  //protected service: CCategorydataService,
  constructor(private spinner: NgxSpinnerService, protected categoryService: quizService,private commonFunctionService: CommonFunctionsService,
    protected addCategoryService: AddeditquizService, private router: Router,
    // private toasterService: ToasterService, 
    protected webApiService: webAPIService,
    private toastr: ToastrService, ) {
    // this.loader = true;
    this.conentHeight = '0px';
    if (localStorage.getItem('LoginResData')) {
      this.loginUserdata = JSON.parse(localStorage.getItem('LoginResData'));

    }
    this.search = {};
    // this.category;
    this.cat = {
      id: ''
    };




    this.getquiz(this.str, this.currentPage);
  }
  ngAfterViewInit() {
    this.conentHeight = '0px';
    const e = document.getElementsByTagName('nb-layout-column');
    console.log(e[0].clientHeight);
    // this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
    if (e[0].clientHeight > 1300) {
      this.conentHeight = '0px';
      this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
    } else {
      this.conentHeight = String(e[0].clientHeight) + 'px';
    }
  }
  getquiz(str, pageNo) {
    this.skeleton = false;
    var dataq = {
      tId: this.loginUserdata.data.data.tenantId,
      lmt: this.pagelmt,
      pno: pageNo,
      str: str,
    };
    // this.spinner.show();
    const getquizcategoryUrl: string = webApi.domain + webApi.url.getquiz;
    this.commonFunctionService.httpPostRequest(getquizcategoryUrl,dataq)
    //this.categoryService.getquiz(dataq)
      .then(rescompData => {
        // this.spinner.hide();
        this.skeleton = true;
        if (rescompData['type'] === true) {
          this.notFound = false;
          this.totalPages = rescompData['data']['totalPages'];
          const list = rescompData['data']['list'];
          this.skeleton = true;
          if (this.quiz.length > 0 && pageNo > 1) {
            this.quiz = this.quiz.concat(list);
            this.skeleton = true;
          } else {
            this.quiz = list;
            this.skeleton = true;
          }
          this.isworking = false;
          if(!this.quiz.length){
          this.notFound = true;
          } 
          if(this.fromSearch &&!this.quiz.length ){
          this.noDataVal={
            margin:'mt-5',
            imageSrc: '../../../../../assets/images/no-data-bg.svg',
            title:"Sorry we couldn't find any matches please try again",
            desc:".",
            titleShow:true,
            btnShow:false,
            descShow:false,
            btnText:'Learn More',
            btnLink:''
          }
        }
        }
        else{
          this.notFound = true;
          this.skeleton = true;
        }
        // this.quiz = rescompData['data'][0];
        console.log('Category Result', this.quiz);
   
      },
        resUserError => {
          this.spinner.hide();
          this.skeleton = true;
          this.errorMsg = resUserError;
          this.notFound = true;
          // this.router.navigate(['**']);
        });
  }


  clear() {
    if(this.str.length>=3){
      this.notFound=false
    this.header.searchtext = ''
    this.search = {};
    this.str = '';
    this.currentPage = 1;
    this.getquiz(this.str, this.currentPage);
    }
    else{
      this.search={};
    }
  }

  back() {
    this.router.navigate(['/pages/assessment']);
  }
  ngOnDestroy(){
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0){
      e1[0].classList.add('with-scroll');
    }
  }
  ngAfterContentChecked() {
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0)
    {
      e1[0].classList.remove('with-scroll');
    }

   }

  content2(data, id) {
    var passData = {
      id: id,
      data: data
    }
    // this.questionsService.data = passData;
    this.router.navigate(['/pages/assessment/quiz/addeditquiz']);
    // this.passService.data = {};
  }
  delatecategory(ind) {
    for (let i = 0; i < this.quiz.length; i++) {
      if (ind == i) {
        this.quiz.splice(i, 1)
      }
    }
  }

  public addeditccategory(data, id) {
    var data1 = {
      data: data,
      id: id
    }

    if (data == undefined) {
      this.addCategoryService.data = data1;
      this.router.navigate(['/pages/assessment/quiz/addEditquiz']);
    } else {
      this.addCategoryService.data = data1;
      this.router.navigate(['/pages/assessment/quiz/addEditquiz']);
      // this.router.navigate(['/pages/plan/courses/category/addEditCategory',{data:JSON.stringify(data),id:id}]);

    }

  }

  gotoCardaddedit(data) {
    var data1 = {
      data: data,
      id: 1
    }
    this.addCategoryService.data = data1;
    this.router.navigate(['/pages/assessment/quiz/addEditquiz']);
  }
  disableCourse: boolean = false;
  disableContent(item, disableStatus) {
    this.disableCourse = !this.disableCourse;
  }

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }

  visibiltyRes: any;
  disablequize(i, item) {
    this.spinner.show();
    var visibilityData = {
      id: item.qid,
      visible: item.visible == 1 ? 0 : 1
    }
    let url = webApi.domain + webApi.url.disablequiz;
    this.webApiService.getService(url, visibilityData)
      .then(rescompData => {
        // this.loader =false;
        this.spinner.hide();
        this.visibiltyRes = rescompData;
        // console.log('Category Visibility Result',this.visibiltyRes);
        if (this.visibiltyRes.type == false) {
          // var catUpdate : Toast = {
          //     type: 'error',
          //     title: "Category",
          //     body: "Unable to update visibility of Quiz.",
          //     showCloseButton: true,
          //     timeout: 4000
          // };
          // // this.closeEnableDisableCategoryModal();
          // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
        } else {
          // var catUpdate : Toast = {
          //     type: 'success',
          //     title: "Quiz",
          //     body: this.visibiltyRes.data,
          //     showCloseButton: true,
          //     timeout: 4000
          // };
          // // this.closeEnableDisableCategoryModal();
          // this.toasterService.pop(catUpdate);
          this.presentToast('success', this.visibiltyRes.data);
          this.disablevaluechnage(i, item)
          // this.getCategories(this.catdata);
        }
      },
        resUserError => {
          // this.loader =false;
          this.spinner.hide();
          if (resUserError.statusText == "Unauthorized") {
            this.router.navigate(['/login']);
          }
          this.errorMsg = resUserError;
          // this.closeEnableDisableCategoryModal();
        });
  }

  clickTodisable(item,i) {
    // this.spinner.show();
    var visibilityData = {
      id: item.qid,
      visible: item.visible==0?1:0,
    }

    if(this.quiz[i].visible == 1) {
      this.quiz[i].visible = 0
    } else {
      this.quiz[i].visible = 1
    }	
    // if(item.visible == 0) {
    //   item.visible = 1;
    // } else {
    //   item.visible = 0;
    // }
    let url = webApi.domain + webApi.url.disablequiz;
    this.webApiService.getService(url, visibilityData)
      .then(rescompData => {
     
        // this.spinner.hide();
        this.visibiltyRes = rescompData;
        
        if (this.visibiltyRes.type == false) {
         
          this.presentToast('error', '');
        } else {
          
          this.presentToast('success', this.visibiltyRes.data);
          
        }
      },
        resUserError => {
          // this.loader =false;
          this.spinner.hide();
          if (resUserError.statusText == "Unauthorized") {
            this.router.navigate(['/login']);
          }
          this.errorMsg = resUserError;
          // this.closeEnableDisableCategoryModal();
        });
        this.closeEnableDisableModal();
  }
  Disbaledata: any;
  disablevaluechnage(i, item) {
    this.Disbaledata = item;
    this.Disbaledata.visible = this.Disbaledata.visible == 1 ? 0 : 1
    //this.quiz[i].visible=this.quiz[i].visible==1?0:1
  }


  closeDuplicateModel() {
    this.QuizDup = false;
  }

  quizData = [];
  dupquiz(item) {
    this.QuizDup = true;
    this.quizData = item;
  }

  saveDupquiz(duplicate) {

  }
  onScroll(event) {
    let element = this.myScrollContainer.nativeElement;
    // element.style.height = '500px';
    // element.style.height = '500px';
    // Math.ceil(element.scrollHeight - element.scrollTop) === element.clientHeight
    if (element.scrollHeight - element.scrollTop - element.clientHeight < 5) {
      if (!this.isworking) {
        this.currentPage++;
        if (this.currentPage <= this.totalPages) {
          this.isworking = true;
          this.skeleton = false;
          this.getquiz(this.str, this.currentPage);
        }
      }
    }

  }
  onsearch(evt: any) {
    console.log(evt);
    // if(evt.target.value != ''){
    this.str = evt.target.value;
    if(this.str.length>=3 ||this.str.length==0){
    this.currentPage = 1;
    this.fromSearch=true;
    this.getquiz(this.str, this.currentPage);
    }
    // if (evt.keyCode === 13) {
    //   this.currentPage = 1;
    //   this.getquiz(this.str, this.currentPage);
    // }else{
      // this.clear();
    // }

  }
  btnName:string='Yes'
  clickToDisableQuiz(item,i) {

    if(this.quiz[i].visible == 1) {
      this.quiz[i].visible = 0
    } else {
      this.quiz[i].visible = 1
    }	

    // this.clickTodisable(item);

  
      // if (item.visible == 1 ) {
      // this.title="Disable Quiz"
      //   this.enable = true;
      //   this.Disbaledata = item;
      //   this.enableDisableModal = true;
      // } else {
      // this.title="Enable Quiz"
      //   this.enable = false;
      //   this.Disbaledata = item;
      //   this.enableDisableModal = true;
  
      // }
  }
  enableDisableQuizAction(actionType) {
    if (actionType == true) {
  
      if (this.Disbaledata.visible == 1) {
        var data = this.Disbaledata;
        // this.clickTodisable(data);
      } else {
        var data = this.Disbaledata;
        // this.clickTodisable(data);
      }
    } else {
      this.closeEnableDisableModal();
    }
  }
  closeEnableDisableModal() {
    this.enableDisableModal = false;
  }
}



