import { Injectable, Inject } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AppConfig } from '../../../app.module';
import { HttpClient } from '@angular/common/http';
import { webApi } from '../../../service/webApi'
@Injectable()
export class quizService {
  // private _url: string = '/api/web/getall_quiz';
  private getquizcategoryUrl: string = webApi.domain + webApi.url.getquiz;
  public data: any;

  constructor(
    @Inject('APP_CONFIG_TOKEN') private config: AppConfig,
    private _http: Http,
    public http1: HttpClient
  ) {}

  // getquiz(data) {
  //   return new Promise(resolve => {
  //     this.http1.post(`${this.config.FINAL_URL}` + this._url, data).subscribe(
  //       data => {
  //         resolve(data);
  //       },
  //       err => {
  //         resolve('err');
  //       }
  //     );
  //   });
  // }
  getquiz(data) {
    return new Promise(resolve => {
      this.http1.post(this.getquizcategoryUrl, data).subscribe(
        data => {
          resolve(data);
        },
        err => {
          resolve('err');
        }
      );
    });
  }
  _errorHandler(error: Response) {
    console.error(error);
    return Observable.throw(error || 'Server Error');
  }
}
