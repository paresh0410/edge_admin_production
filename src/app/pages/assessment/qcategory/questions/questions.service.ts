import {Injectable, Inject} from '@angular/core';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
 import {AppConfig} from '../../../../app.module';
 import { HttpClient } from '@angular/common/http';

@Injectable()
export class  questionsService {

  public data:any;
  private _url:string = "/api/web/getallqbdropdown" 
  private _url_que:string = "/api/web/get_category_question"
  //DevTest
  request: Request;
  
  constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private _http: Http, private _http1: HttpClient){
      //this.busy = this._http.get('...').toPromise();
  }

  // getqbdropdowun(data){
  //   let url:any = `${this.config.FINAL_URL}`+this._url;
  //   return this._http.post(url,data)
  //     .map((response:Response) => response.json())
  //     .catch(this._errorHandler);
  // }

   getcategoryquestion(data){
    let url:any = `${this.config.FINAL_URL}`+this._url_que;
    // return this._http.post(url,data)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);
    return new Promise(resolve => {
      this._http1
        .post(url, data)
        //.map(res => res.json())
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            resolve('err');
          }
        );
    });
  }

  _errorHandler(error: Response){
    console.error(error);
    return Observable.throw(error || "Server Error")
  }

}
