import { Component, Directive, ViewEncapsulation, forwardRef, Attribute, OnChanges, SimpleChanges, Input, ViewChild, ViewContainerRef, ChangeDetectorRef } from '@angular/core';
import { questionsService } from './questions.service';
import { addEditquestionService } from '../addEditquestion/addEditquestion.service';
import { NG_VALIDATORS, Validator, Validators, AbstractControl, ValidatorFn, FormGroupDirective } from '@angular/forms';
import { LocalDataSource } from 'ng2-smart-table';
import { Router, NavigationStart, ActivatedRoute } from '@angular/router';
import { webAPIService } from '../../../../service/webAPIService'
import { webApi } from '../../../../service/webApi'
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { PassService } from '../../../../service/passService';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { SuubHeader } from '../../../components/models/subheader.model';
import { noData } from '../../../../models/no-data.model';
import { HttpClient } from '@angular/common/http';
import { DataSeparatorService } from '../../../../service/data-separator.service';

@Component({
  selector: 'questions',
  templateUrl: './questions.html',
  styleUrls: ['./questions.scss'],
  encapsulation: ViewEncapsulation.None
})

export class questions {
  enableUpload: any = false;
  @ViewChild("fileUpload") fileUpload: any;
  @ViewChild("multupload") multupload: any;
  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('shortform') shortform: FormGroupDirective;
  @ViewChild('sequenceform') sequenceform: FormGroupDirective;
  @ViewChild('matchingform') matchingform: FormGroupDirective;
  @ViewChild('multipleform') multipleform: FormGroupDirective;




  sidebarForm = false;
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No question added under category.",
    desc:"",
    titleShow:true,
    btnShow:true,
    descShow:false,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/assessment-how-to-add-question-to-a-question-bank-category',
  }

  query: string = '';
  public getData;

  title: any;
  formdata: any;
  profileUrl: any = 'assets/images/cat3.png';
  result: any;
  errorMsg: any;
  selected: any = [];
  parentcat: any = [{
    id: 0,
    name: 'Parent'
  },
  {
    id: 1,
    name: 'Assigned'
  },
  {
    id: 2,
    name: 'Recommended'
  },
  {
    id: 3,
    name: 'Special'
  },]

  visibility: any = [{
    id: 1,
    name: 'Show'
  },
  {
    id: 2,
    name: 'Hide'
  }]
  columnsque = [
    { prop: 'qname', name: 'Question Name' },
    { prop: 'qtype', name: 'Question Type' },
    { prop: 'qinstru', name: 'Instructions ' },
    { prop: 'qfeed', name: 'General Feedback' },
  ];
  questions: any = [];
  pager: any;
  labels: any = [
    { labelname: 'Question', bindingProperty: 'questionText', componentType: 'html' },
    { labelname: 'Question Format', bindingProperty: 'qType', componentType: 'html' },
    { labelname: 'Instructions', bindingProperty: 'instructions', componentType: 'html' },
    { labelname: 'Visibility', bindingProperty: 'btntext', componentType: 'button' },
    { labelname: 'Duplicate', bindingProperty: 'qType', componentType: 'icondup' },
  ]
  header: SuubHeader = {
    title:'',
    btnsSearch: true,
    searchBar: true,
    catDropdown:true,
    placeHolder:"Search by Question name",
    optionValue : 'All Categories',
    dropId:'id',
    dropName:'catName',
    btnAdd: 'Add Question',
    btnBackshow: true,
    btnAddshow: false,
    showBreadcrumb: true,
    breadCrumbList:[
      {
        'name': 'Assessment',
        'navigationPath': '/pages/assessment',
      },
      {
        'name': 'Question Bank',
        'navigationPath': '/pages/assessment/qcategory',
      },]
  };

  loginUserdata;
  catData: any = {
    qFormat: 0,
    catId: 0,
    // tId : this.loginUserdata.data.data.tenantId,
  }
  category: any;
  tempQuestion: any = [];
  rows: any = [];
  loader: any;
  catshowname: any;
  temData: any;
  pageSize: number = 10;
  displayTableData: any;
  searchTags: any;
  nodata: boolean;
  qbformat: any;



  short: any
  multiple: any;
  sequence: any;
  matching: any;
  settingsTagDrop: any;
  qbtype: any;
  qblevel: any;
  typeId: any;
  catID: any;
  catAddId: any;
  formatID: any;
  userdata: any;
  tagList: any;
  tempTags: any;
  selectedTags: any = [];
  qbcategory: any;
  tempname:any;
  selectedActInd: any;
  activeTypeName: any;
  headers: any;
  titleName: any;
  btnName = "Save"
  customCss="inner-form-zero";
  categoryImgData: any;
  fileUploadRes: any;
  qbMultiple: any;
  qbShort: any;
  shortCount: any = 0;
  mutimgarr: any = [];
  multTemparr: any = [];
  multcontent: any = [];
  multfileUploadRes: any = [];
  multCount: any = 0;
 qbSequence: any;
  squCount: any = 0;
  qbmatching: any;
  matchcontent: any = [];
  matchfileUploadRes: any = [];
  matchimgarr: any = [];
  matchTemparr: any = [];
  matchCount: any = 0;
  tempCheckedIndex: any = 0;
  // addUpdateCategoryque: any;


padding="p-0"




  constructor(private spinner: NgxSpinnerService, protected service: questionsService,
    private pagservice: PassService,
    protected services: addEditquestionService,
    public cdf: ChangeDetectorRef,
    private http1: HttpClient,
    protected addEditquestionService: addEditquestionService, private toastr: ToastrService,
    // private toasterService: ToasterService,
    private router: Router, private route: ActivatedRoute,
    private dataSeparatorService: DataSeparatorService,
    protected webApiService: webAPIService) {
    this.loader = true;
    this.getHelpContent();
    this.settingsTagDrop = {
      text: 'Select Tags ',
      singleSelection: false,
      classes: 'common-multi',
      primaryKey: 'id',
      labelKey: 'name',
      noDataLabel: 'Search Tags...',
      enableSearchFilter: true,
      lazyLoading: true,
      searchBy: ['name'],
      badgeShowLimit: 1,
      maxHeight:190,
    };


    // this.short = {
    //   min: "",
    //   max: "",
    //   score: null,
    //   penelty: null,
    //   visible: 1,
    // };

    // this.multiple = {
    //   mvalue: "1",
    //   moption: [
    //     {
    //       manswer: "",
    //       mpoint: "",
    //       mipoint: 0,
    //       manfeed: "",
    //       mopvalue: false,
    //       qtypeid: 1,
    //       optionRef: "",
    //       qfileName: "No File Chosen",
    //       perct: 0
    //     }
    //   ]
    // };
    // this.sequence = {
    //   soption: [
    //     {
    //       sanswer: "",
    //       sindex: "",
    //       spoint: "",
    //       sipoint: 0,
    //       perct: 0
    //     }
    //   ]
    // };
    // this.matching = {
    //   moption: [
    //     {
    //       qtypeid: 1,
    //       optionText: "",
    //       optionRef: "",
    //       optionMatch: "",
    //       mpoint: "",
    //       mipoint: 0,
    //       qfileName: "No File Chosen",
    //       perct: 0
    //     }
    //   ]
    // };


    // console.log("Add Category Called");
    var category;
    var id;
    if (localStorage.getItem('LoginResData')) {
      this.loginUserdata = JSON.parse(localStorage.getItem('LoginResData'));
    }
    this.header['id']= this.catData.catId

    var catdata = {
      tId: this.loginUserdata.data.data.tenantId
    };






    if (this.service.data != undefined) {
      category = this.service.data.data;
      this.parentcat = this.service.data.catdata


      this.catData = {
        qFormat: 0,
        catId: category.id,
        tId: this.loginUserdata.data.data.tenantId,
      }
      this.services.getqbdropdowun(catdata).then(
        rescompData => {
          this.qbtype = rescompData['data'].qbtype;
          this.qblevel = rescompData['data'].qblevel;
          this.qbformat = rescompData['data'].qbformat;
          this.qbcategory = rescompData['data'].qbcategory;
          console.log( " this.qbformat =============================>",this.qbformat);


          this.header  = {
            title:'',
            btnsSearch: true,
            searchBar: true,
            catDropdown:true,
            placeHolder:"Search by Question name",
            optionValue : 'All Categories',
            dropId:'id',
            dropName:'catName',
            btnAdd: 'Add Question',
            btnBackshow: true,
            btnAddshow: false,
            showBreadcrumb: true,
            dropdownlist:  this.qbformat,
            drplabelshow: true,
            dropdownlabel: "Add Question",
            breadCrumbList:[
              {
                'name': 'Assessment',
                'navigationPath': '/pages/assessment',
              },
              {
                'name': 'Question Bank',
                'navigationPath': '/pages/assessment/qcategory',
              },

            ]
          };
          this.header.title=this.catshowname;


          this.header['id'] = category.id
        },
        resUserError => {
          this.spinner.hide();
          this.errorMsg = resUserError;



        }
      );

      this.spinner.show();

      this.service.getcategoryquestion(this.catData)
        .then(rescompData => {
          // this.loader =false;
          this.spinner.hide();
          this.questions = rescompData['data'][0];
          for (let i = 0; i < this.questions.length; i++) {
            if (this.questions[i].visible == 1) {
              this.questions[i].btntext = 'fa fa-eye';
            } else {
              this.questions[i].btntext = 'fa fa-eye-slash';
            }
          }
          if(this.questions.length==0){
            this.nodata=true;
          }else{
            this.nodata=false;
          }
          this.temData = this.questions
          this.tempQuestion = this.questions;
          console.log('Category Result', this.questions)
        },
          resUserError => {
            this.spinner.hide();
            this.loader = false;
            this.errorMsg = resUserError;
            // this.notFound = true;
            // this.router.navigate(['**']);
          });

      this.category = category;
      this.catshowname = this.category.catName
      id = this.service.data.id;
      // this.catData = this.service.data.data;

      // this.questions=this.category.cque;
      // this.tempQuestion=this.category.cque
    } else {
      id = 0;
      this.category = {
        catName: ''
      };


      this.catshowname = this.category.catName
      // this.category.catName="";
      this.back();
      // this.router.navigate(['/pages/dashboard']);
    }
    console.log("Service Data:", this.service.data);
    console.log('Category Info', category);
    console.log('id', id);
    this.header.title=this.catshowname;
  }

  showlist() {
    this.service.getcategoryquestion(this.catData)
    .then(rescompData => {
      // this.loader =false;
      this.spinner.hide();
      this.questions = rescompData['data'][0];
      for (let i = 0; i < this.questions.length; i++) {
        if (this.questions[i].visible == 1) {
          this.questions[i].btntext = 'fa fa-eye';
        } else {
          this.questions[i].btntext = 'fa fa-eye-slash';
        }
      }
      if(this.questions.length==0){
        this.nodata=true;
      }else{
        this.nodata=false;
      }
      this.temData = this.questions
      this.tempQuestion = this.questions;
      console.log('Category Result', this.questions)
    },
      resUserError => {
        this.spinner.hide();
        this.loader = false;
        this.errorMsg = resUserError;
        // this.notFound = true;
        // this.router.navigate(['**']);
      });



  }

  showList() {

  }
  questionType: any;
  selectedChoices(value) {
    this.questionType = value.name;
    this.selectedActInd = value.id;
    this.addeditquestion(0,this.category);
    // this.sidebarForm = true;
    console.log('value==========================================>',this.selectedActInd)

  }

  cloeSidebar() {
    this.formatID = 0;
    this.selectedTags = [];
    this.sidebarForm = false;
    this.type=false
  }



  addeditquestion(id, data) {

    if(id === 0) {
      this.titleName = "Add Question";
    } else if(id=== 1) {
      this.titleName = "Edit Question";
    } else {
      this.titleName = "Copy Question";
    }
    var data1 = {
      data: data,
      id: id,
      cdata: this.catData.catId
    }

     if (data == undefined) {
      this.addEditquestionService.data = data1;
      // this.router.navigate(['/pages/assessment/qcategory/addEditquestion']);
    } else {
      this.addEditquestionService.data = data1;
      // this.router.navigate(['/pages/assessment/qcategory/addEditquestion']);

    }

    var categoryque;
    if (data != undefined) {
      categoryque = this.services.data.data;
      id = this.services.data.id;
      this.typeId=id;
      this.catID = this.services.data.data.cid;
      // this.catData = this.services.data.data;
      this.catAddId = this.services.data.cdata;
      this.formatID = data.qFormat;
      if(this.formatID == 1) {
        this.questionType = 'Choice'
      } else if (this.formatID == 2) {
        this.questionType = 'Matching'
      }else if (this.formatID == 3) {
        this.questionType = 'Sequence'
      }else if(this.formatID == 4) {
        this.questionType = 'Short'
      }
    } else {
      id = 0;
    }
    this.typeId=this.services.data.id
    console.log("Service Data:", this.services.data);
    console.log("categoryque Info", categoryque);
    console.log("id", id);
    this.getallTagList(id, categoryque);




  }

  back() {
    this.router.navigate(['/pages/assessment/qcategory']);
  }
  onSelect({ selected }) {
    console.log('Select Manual Event', selected, this.selected);

    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);

    // if(this.selectedManual.length == 1){
    //   this.enableShowRuleUsers = true;
    // }else{
    //   this.enableShowRuleUsers = false;
    // }
  }

  onActivate(event) {
    console.log('Activate Event', event);
    if (event.type == 'click') {
      if (event.cellIndex < 4) {
        // this.addeditquestion(1, event.row)
      }
    }
  }
ngOnInit(){
  this.spinner.show();
  setTimeout(() => {
    this.spinner.hide()
  }, 2000);
}
  searchQuestion(event) {
    var val = event.target.value.toLowerCase();
    this.searchTags=val
    // filter our data
    if(val.length>=3 || val.length==0){
    var temp = this.tempQuestion.filter(function (d) {
      return String(d.questionText).toLowerCase().indexOf(val) !== -1 ||
        // String(d.qtype).toLowerCase().indexOf(val) !== -1 ||
        // String(d.qinstru).toLowerCase() === val ||
        // String(d.qfeed).toLowerCase().indexOf(val) !== -1 ||
        !val;
    });
if(temp.length==0){
  this.nodata=true
  this.noDataVal={
    margin:'mt-5',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"Sorry we couldn't find any matches please try again",
    desc:".",
    titleShow:true,
    btnShow:false,
    descShow:false,
    btnText:'Learn More',
    btnLink:''
  }
}
    // update the rows
    this.questions = temp;
    // Whenever the filter changes, always go back to the first page
    this.questions.offset = 0;
  }
}

  clear(){
    this.nodata=false
    this.questions = this.tempQuestion
    this.questions.offset = 0;
  }
  doropserch(Data) {
    console.log(Data,"catData")
    var catId = Data.id
    var catData: any = {
      qFormat: 0,
      catId: catId,
      tId : this.loginUserdata.data.data.tenantId,
    }
    for (let i = 0; i < this.parentcat.length; i++) {
      if (catData.catId == this.parentcat[i].id) {
      // if (catId == this.parentcat[i].id) {
        this.catshowname = this.parentcat[i].catName;

        console.log('parentcat', this.parentcat);
      } else if (catData.catId == 0) {
        this.catshowname = 'All Categories';
        console.log('parentcat', this.parentcat);
      }
    }
    this.header.title=this.catshowname;
    this.spinner.show();
    this.service.getcategoryquestion(catData)
      .then(rescompData => {
        // this.loader =false;
        this.spinner.hide();
        this.questions = rescompData['data'][0];
        this.tempQuestion = this.questions;
        for (let i = 0; i < this.questions.length; i++) {
          if (this.questions[i].visible == 1) {
            this.questions[i].btntext = 'fa fa-eye';
          } else {
            this.questions[i].btntext = 'fa fa-eye-slash';
          }
        }
        if(this.questions.length==0){
          this.nodata=true;
        }else{
          this.nodata=false;
        }
        console.log('Category Result', this.questions)
      },
        resUserError => {
          this.spinner.hide();
          this.loader = false;
          this.errorMsg = resUserError;
          // this.notFound = true;
          // this.router.navigate(['**']);
        });

  }

  // disableQuecat:boolean = false;
  // disabledQuecat(item,disableStatus){
  //   this.disableQuecat = !this.disableQuecat;
  // }
  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }
  visibiltyRes: any;
  visibilityTableRow(row) {
    let value;
    let status;

    if (row.visible == 1) {
      row.btntext = 'fa fa-eye-slash';
      value = 'fa fa-eye-slash';
      row.visible = 0
      status = 0;
    } else {
      status = 1;
      value = 'fa fa-eye';
      row.visible = 1;
      row.btntext = 'fa fa-eye';
    }

    // for(let i =0; i < this.questions.length; i++) {
    //   if(this.questions[i].employeeId == row.employeeId) {
    //   this.questions[i].btntext = row.btntext;
    //   this.questions[i].visible = row.visible
    //   }
    // }
    this.spinner.show();
    var visibilityData = {
      id: row.qid,
      visible: row.visible
    }
    let url = webApi.domain + webApi.url.disablequestion;
    this.webApiService.getService(url, visibilityData)
      .then(rescompData => {
        this.spinner.hide();
        this.visibiltyRes = rescompData;
        if (this.visibiltyRes.type == false) {
          this.presentToast('error', '');
        } else {
          this.presentToast('success', this.visibiltyRes.data);
          this.disablevaluechnage(row)
        }
      },
        resUserError => {
          this.spinner.hide();
          if (resUserError.statusText == "Unauthorized") {
            this.router.navigate(['/login']);
          }
          this.errorMsg = resUserError;
        });

    console.log('row', row);
  }


  disabledQuecat(item) {
    console.log(item);

    this.spinner.show();
    var visibilityData = {
      id: item.qid,
      visible: item.visible == 1 ? 0 : 1
    }
    let url = webApi.domain + webApi.url.disablequestion;
    this.webApiService.getService(url, visibilityData)
      .then(rescompData => {
        // this.loader =false;
        this.spinner.hide();
        this.visibiltyRes = rescompData;
        // console.log('Category Visibility Result',this.visibiltyRes);
        if (this.visibiltyRes.type == false) {
          // var catUpdate : Toast = {
          //     type: 'error',
          //     title: "Category",
          //     body: "Unable to update visibility of category question .",
          //     showCloseButton: true,
          //     timeout: 4000
          // };
          // this.closeEnableDisableCategoryModal();
          // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
        } else {
          // var catUpdate : Toast = {
          //     type: 'success',
          //     title: "Question",
          //     body: this.visibiltyRes.data,
          //     showCloseButton: true,
          //     timeout: 4000
          // };
          // this.closeEnableDisableCategoryModal();
          // this.toasterService.pop(catUpdate);
          this.presentToast('success', this.visibiltyRes.data);
          this.disablevaluechnage(item)
          // this.getCategories(this.catdata);
        }
      },
        resUserError => {
          // this.loader =false;
          this.spinner.hide();
          if (resUserError.statusText == "Unauthorized") {
            this.router.navigate(['/login']);
          }
          this.errorMsg = resUserError;
          // this.closeEnableDisableCategoryModal();
        });
  }

  disablevaluechnage(item) {
    // for (let i = 0; i < this.questions.length; i++) {
    //   if (this.questions[i].qid == item.qid) {
    //     this.questions[i].visible = this.questions[i].visible == 1 ? 0 : 1
    //   }
    // }
    console.log("this.questions==================>", this.questions);
  }

  setPage(page) {
    this.pager = this.pagservice.getPager(this.temData.length, page, this.pageSize);
    //console.log(this.pager.pages,"THIS.PAGER");
    this.dispalyData()
  }

  dispalyData() {
    this.displayTableData = this.temData.slice(this.pager.startIndex, this.pager.endIndex + 1);
    console.log(this.displayTableData, "this.displayTable")
    this.questions = this.displayTableData

  }


  helpContent: any;
  getHelpContent() {
    return new Promise(resolve => {
      this.http1
        .get("../../../../../../assets/help-content/addEditCourseContent.json")
        .subscribe(
          data => {
            this.helpContent = data;
            console.log("Help Array", this.helpContent);
          },
          err => {
            resolve("err");
          }
        );
    });
    // return this.helpContent;
  }




  submits() {

    this.router.navigate(['/pages/assessment/qcategory']);
    //   var category={
    //     categoryId: this.formdata.categoryId,
    //     name: this.formdata.name,
    //     idnumber: this.formdata.idnumber,
    //     description: this.formdata.description,
    //     descriptionformat:this.formdata.descriptionformat,
    //     parent:this.formdata.parent,
    //     sortorder:this.formdata.sortorder,
    //     coursecount:this.formdata.coursecount,
    //     visible:this.formdata.visible,
    //     visibleold:this.formdata.visibleold,
    //     timemodified:this.formdata.timemodified,
    //     depth:this.formdata.depth,
    //     path:this.formdata.path,
    //     theme:this.formdata.theme
    //   }

    //   this.service.createUpdateCategory(category)
    //   .subscribe(rescompData => {
    //     // this.loader =false;
    //     this.result = rescompData;
    //     console.log('Added Category Result',this.result)
    //     if(this.result.type == true){
    //       if(this.catData != undefined){
    //         this.router.navigate(['/pages/plan/courses/category']);
    //         var catUpdate : Toast = {
    //             type: 'success',
    //             title: "Category updated!",
    //             body: "Category updated successfully.",
    //             showCloseButton: true,
    //             timeout: 4000
    //         };
    //         this.toasterService.pop(catUpdate);
    //       }else{
    //         this.router.navigate(['/pages/plan/courses/category']);
    //         var catCreate : Toast = {
    //             type: 'success',
    //             title: "Category created!",
    //             body: "New category added successfully.",
    //             showCloseButton: true,
    //             timeout: 4000
    //         };
    //         this.toasterService.pop(catCreate);
    //       }
    //     }else{
    //       if(this.catData != undefined){
    //         var catUpdate : Toast = {
    //             type: 'error',
    //             title: "Category not updated!",
    //             body: "Unable to update category.",
    //             showCloseButton: true,
    //             timeout: 4000
    //         };
    //         this.toasterService.pop(catUpdate);
    //       }else{
    //         var catCreate : Toast = {
    //             type: 'error',
    //             title: "Category not created!",
    //             body: "Unable to create category.",
    //             showCloseButton: true,
    //             timeout: 4000
    //         };
    //         this.toasterService.pop(catCreate);
    //       }
    //     }
    //   },
    //   resUserError => {
    //     // this.loader =false;
    //     this.errorMsg = resUserError
    //   });
    // }

  }






  getallTagList(id, categoryque) {
    this.spinner.show();
    let url = webApi.domain + webApi.url.getAllTagsComan;
    let param = {
      tenantId: this.loginUserdata.data.data.tenantId
    };
    this.webApiService.getService(url, param)
      .then(rescompData => {
        // this.loader =false;
        this.spinner.hide();
        var temp: any = rescompData;
        this.tagList = temp.data;
        this.tempTags = [... this.tagList];
        this.makeCategoryDataReady(id, categoryque);
        // console.log('Visibility Dropdown',this.visibility);
      },
        resUserError => {
          // this.loader =false;
          this.show = false;
          this.spinner.hide();
          this.errorMsg = resUserError;
        });
  }



  show: boolean = false;
  makeCategoryDataReady(id, categoryque) {
    this.spinner.show();
      var tagIds = [];
      if (id == 1) {
        this.title = "Edit Question";
        this.formdata = {
          cid: categoryque.cid,
          cname: categoryque.catName,
          qinsrtu: categoryque.instructions,
          qid: categoryque.qid,
          qname: categoryque.qName,
          qfeed: categoryque.feedback,
          qtext: categoryque.questionText,
          qFormat: categoryque.qFormat,
          qcontent: categoryque.contentRef,
          qtypeid: categoryque.qTypeid,
          qlevelid: categoryque.qLevelid,
          qvisible: categoryque.visible,
          qtenantId: this.loginUserdata.data.data.tenantId,
          qtag:
            categoryque.tags == ""
              ? categoryque.tags
              : categoryque.tags.split(","),
          qfileName: categoryque.contentRef
            ? categoryque.contentRef.substring(
              categoryque.contentRef.lastIndexOf("/") + 1
            )
            : categoryque.contentRef
        };
        this.getAllBAsicData();

        if (categoryque.tagIds) {
          tagIds = categoryque.tagIds.split(',');
          if (tagIds.length > 0) {
            this.tempTags.forEach((tag) => {
              tagIds.forEach((tagId) => {
                if (tag.id == tagId) {
                  this.selectedTags.push(tag);
                }
              });
            });
          }
        }

       } else if (id == 2) {
          this.title = "Copy Question";
          this.formdata = {
            cid: categoryque.cid,
            cname: categoryque.catName,
            qinsrtu: categoryque.instructions,
            qid:categoryque.qid,
            qname: categoryque.qName,
            qfeed: categoryque.feedback,
            qtext: categoryque.questionText,
            qFormat: categoryque.qFormat,
            qcontent: categoryque.contentRef,
            qtypeid: categoryque.qTypeid,
            qlevelid: categoryque.qLevelid,
            qvisible: categoryque.visible,
            qtenantId: this.loginUserdata.data.data.tenantId,
            qtag:
              categoryque.tags == ""
                ? categoryque.tags
                : categoryque.tags.split(","),
            qfileName: categoryque.contentRef
              ? categoryque.contentRef.substring(
                categoryque.contentRef.lastIndexOf("/") + 1
              )
              : categoryque.contentRef
          };
          this.getAllBAsicData();
          tagIds = categoryque.tagIds.split(',');
          if (tagIds.length > 0) {
            this.tempTags.forEach((tag) => {
              tagIds.forEach((tagId) => {
                if (tag.id == tagId) {
                  this.selectedTags.push(tag);
                }
              });
            });
          }

        }

       else {
        this.title = "Add Question";
        this.formdata = {
          cid: this.catAddId ? this.catAddId : "",
          cname: "",
          qinsrtu: "",
          qid: 0,
          qname: "",
          qfeed: "",
          qtext: "",
          qFormat: "",
          qcontent: "",
          qtypeid: "",
          qlevelid: "",
          qvisible: 1,
          qtenantId: this.loginUserdata.data.data.tenantId,
          qfileName: "You can drag and drop files here to add them."
        };

        this.short = {
          min: "",
          max: "",
          score: null,
          penelty: null,
          visible: 1,
        };

        this.multiple = {
          mvalue: "1",
          moption: [
            {
              manswer: "",
              mpoint: "",
              mipoint: 0,
              manfeed: "",
              mopvalue: false,
              qtypeid: 1,
              optionRef: "",
              qfileName: "No File Chosen",
              perct: 0
            }
          ]
        };
        this.sequence = {
          soption: [
            {
              sanswer: "",
              sindex: "",
              spoint: "",
              sipoint: 0,
              perct: 0
            }
          ]
        };
        this.matching = {
          moption: [
            {
              qtypeid: 1,
              optionText: "",
              optionRef: "",
              optionMatch: "",
              mpoint: "",
              mipoint: 0,
              qfileName: "No File Chosen",
              perct: 0
            }
          ]
        };

      }
      this.show = true;

      for(let i=0;i < this.qbcategory.length; i++) {
        if(this.qbcategory[i].id  == this.formdata.cid){
            this.tempname = this.qbcategory[i].name
        }

        console.log('this.tempname===================================>',this.tempname)
      }
      this.sidebarForm = true;
      this.spinner.hide();

  }




  getAllBAsicData(){
    if (this.formatID == 1) {
      this.selectedActInd = 1;
      var mulData = {
        qId: this.formdata.qid,
        tId: this.loginUserdata.data.data.tenantId
      };
      this.spinner.show();
      this.services.getmultipleData(mulData).then(
        rescompData => {
          this.spinner.hide();
          var multipleData = rescompData['data'][0] ? rescompData['data'][0] : [];
          console.log(" this.multiple", this.multiple);
          if (multipleData.length > 0) {
            var mvaluetemp = multipleData[0].choiceType;
            for (let i = 0; i < multipleData.length; i++) {
              delete multipleData[i].choiceType;
              delete multipleData[i].id;
              delete multipleData[i].questionId;
              delete multipleData[i].isShuffle;
              multipleData[i].qtypeid = Number(multipleData[i].qtypeid);
              if (mvaluetemp == 1) {
                multipleData[i].mopvalue = multipleData[i].mopvalue;
              } else {
                multipleData[i].mopvalue =
                  multipleData[i].mopvalue == 1 ? true : false;
              }
              multipleData[i].qfileName = multipleData[i].optionRef
                ? multipleData[i].optionRef.substring(
                  multipleData[i].optionRef.lastIndexOf("/") + 1
                )
                : "No file Choosen";
              // multipleData[i].Content=multipleData[i].optionRef?multipleData[i].optionRef.substring(multipleData[i].optionRef.lastIndexOf("/")+1):'No file Choosen';
            }

            this.multiple = {
              mvalue: String(mvaluetemp),
              moption: multipleData
            };
          } else {
            this.multiple = {
              mvalue: "1",
              moption: [
                {
                  manswer: "",
                  mpoint: "",
                  mipoint: 0,
                  manfeed: "",
                  mopvalue: false,
                  qtypeid: 1,
                  optionRef: "",
                  qfileName: "No File Chosen",
                  perct: 0
                }
              ]
            };
          }

          // this.selectedActInd = 0;
          this.activeTypeName = "Choice";
          this.headers={
            title: this.title,
           btnsSearch: true,
           btnName2: 'Save',
           btnName2show: true,
           btnBackshow: true,
           showBreadcrumb:true,
           breadCrumbList:[
             {
               'name': 'Assessment',
               'navigationPath': '/pages/assessment',
             },
             {
             'name': 'Question Bank',
             'navigationPath': '/pages/assessment/qcategory',
           },
           {
            // 'name': this.formdata.cname,
            'name': this.tempname,

            'navigationPath': '/pages/assessment/qcategory/questions',
           }
         ]
         };

        },
        resUserError => {
          this.spinner.hide();
          this.errorMsg = resUserError;
          // this.notFound = true;
          // this.router.navigate(['**']);
        }
      );
    }
    if (this.formatID == 2) {
      this.selectedActInd =2
      var mulData = {
        qId: this.formdata.qid,
        tId: this.loginUserdata.data.data.tenantId,
      };
      this.spinner.show();
      this.services.getMatchingData(mulData).then(
        rescompData => {
          this.spinner.hide();
          var matchingData = rescompData['data'][0] ? rescompData['data'][0] : [];
          console.log(" this.multiple", this.multiple);
          if (matchingData.length > 0) {
            for (let i = 0; i < matchingData.length; i++) {
              delete matchingData[i].id;
              delete matchingData[i].questionId;

              matchingData[i].qfileName = matchingData[i].optionRef
                ? matchingData[i].optionRef.substring(
                  matchingData[i].optionRef.lastIndexOf("/") + 1
                )
                : "No file Choosen";
              // multipleData[i].Content=multipleData[i].optionRef?multipleData[i].optionRef.substring(multipleData[i].optionRef.lastIndexOf("/")+1):'No file Choosen';
            }
            console.log("matchingData===========>",matchingData)

            this.matching = {
              moption: matchingData
            };
          } else {
            this.matching = {
              moption: [
                {
                  qtypeid: 1,
                  optionText: "",
                  optionRef: "",
                  optionMatch: "",
                  mpoint: "",
                  mipoint: 0,
                  qfileName: "No File Chosen",
                  perct: 0
                }
              ]
            };
          }

          // this.selectedActInd = 1;
          this.activeTypeName = "Matching";
          this.headers={
            title: this.title,
           btnsSearch: true,
           btnName2: 'Save',
           btnName2show: true,
           btnBackshow: true,
           showBreadcrumb:true,
           breadCrumbList:[
             {
               'name': 'Assessment',
               'navigationPath': '/pages/assessment',
             },
             {
             'name': 'Question Bank',
             'navigationPath': '/pages/assessment/qcategory',
           },
           {
            //  'name': this.formdata.cname,
            'name': this.tempname,
             'navigationPath': '/pages/assessment/qcategory/questions',
           }
         ]
         };
          //   console.log('this.qbtype',this.qbtype);
          //     console.log('this.qblevel',this.qblevel);
          //       console.log('this.qbformat',this.qbformat);
          //         console.log('this.qbcategory',this.qbcategory);
        },
        resUserError => {
          this.spinner.hide();
          this.errorMsg = resUserError;
          // this.notFound = true;
          // this.router.navigate(['**']);
        }
      );
    }

    if (this.formatID == 3) {
      this.selectedActInd =3;
      var mulData = {
        qId: this.formdata.qid,
        tId: this.loginUserdata.data.data.tenantId,
      };
      this.spinner.show();
      this.services.getsequenceData(mulData).then(
        rescompData => {
          this.spinner.hide();
          var SequenceData = rescompData['data'][0] ? rescompData['data'][0] : [];
          console.log(" this.multiple", this.multiple);
          if (SequenceData.length > 0) {
            var mvaluetemp = SequenceData[0].choiceType;
            for (let i = 0; i < SequenceData.length; i++) {
              delete SequenceData[i].id;
              delete SequenceData[i].questionId;
            }

            this.sequence = {
              soption: SequenceData
            };
          } else {
            this.sequence = {
              soption: [
                {
                  sanswer: "",
                  sindex: "",
                  spoint: "",
                  sipoint: 0,
                  perct: 0
                }
              ]
            };
          }

          // this.selectedActInd = 2;
          this.activeTypeName = "Sequence";
          this.headers={
            title: this.title,
           btnsSearch: true,
           btnName2: 'Save',
           btnName2show: true,
           btnBackshow: true,
           showBreadcrumb:true,
           breadCrumbList:[
             {
               'name': 'Assessment',
               'navigationPath': '/pages/assessment',
             },
             {
             'name': 'Question Bank',
             'navigationPath': '/pages/assessment/qcategory',
           },
           {
            //  'name': this.formdata.cname,
            'name': this.tempname,
             'navigationPath': '/pages/assessment/qcategory/questions',
           }
         ]
         };
          //   console.log('this.qbtype',this.qbtype);
          //     console.log('this.qblevel',this.qblevel);
          //       console.log('this.qbformat',this.qbformat);
          //         console.log('this.qbcategory',this.qbcategory);
        },
        resUserError => {
          this.spinner.hide();
          this.errorMsg = resUserError;
          // this.notFound = true;
          // this.router.navigate(['**']);
        }
      );
    }
    if (this.formatID == 4) {
      this.selectedActInd = 4
      var mulData = {
        qId: this.formdata.qid,
        tId: this.loginUserdata.data.data.tenantId
      };
      this.spinner.show();
      this.services.getshortData(mulData).then(
        rescompData => {
          this.spinner.hide();
          var shortData = rescompData['data'][0] ? rescompData['data'][0] : [];
          console.log(" this.multiple", shortData);
          if (shortData.length > 0) {
            // var mvaluetemp = shortData[0].isCase;
            // for (let i = 0; i < shortData.length; i++) {
            //   delete shortData[i].id;
            //   delete shortData[i].questionId;
            // }
            var sData = shortData[0];
            this.short = {
              min: sData.min,
              max: sData.max,
              score: null,
              penelty: null,
            };
          } else {
            this.short = {
              min: "",
              max: "",
              score: null,
              penelty: null,
            };
          }

          // this.selectedActInd = 3;
          this.headers={
            title: this.title,
           btnsSearch: true,
           btnName2: 'Save',
           btnName2show: true,
           btnBackshow: true,
           showBreadcrumb:true,
           breadCrumbList:[
             {
               'name': 'Assessment',
               'navigationPath': '/pages/assessment',
             },
             {
             'name': 'Question Bank',
             'navigationPath': '/pages/assessment/qcategory',
           },
           {
            //  'name': this.formdata.cname,
            'name': this.tempname,
             'navigationPath': '/pages/assessment/qcategory/questions',
           }
         ]
         };
          this.activeTypeName = "Short";
          //   console.log('this.qbtype',this.qbtype);
          //     console.log('this.qblevel',this.qblevel);
          //       console.log('this.qbformat',this.qbformat);
          //         console.log('this.qbcategory',this.qbcategory);
        },
        resUserError => {
          this.spinner.hide();
          this.errorMsg = resUserError;
          // this.notFound = true;
          // this.router.navigate(['**']);
        }
      );
    }
  }
  type = false;
  basic = true;
  selectTab(item) {
    console.log('this.formdata ====================>',this.formdata);
    if (item.tabTitle === "Basic Details") {
      // this.selectedActInd =undefined;
      this.type = false;
      this.basic = true;
      // for(let i=0;i < this.qbcategory.length; i++) {
      //   if(this.qbcategory[i].id  == this.formdata.cid){
      //       let name = this.qbcategory[i].name
      //   }
      // }
      console.log('name', name)

     this.headers={
       title: this.title,
      btnsSearch: true,
      btnName2: 'Save',
      btnName2show: true,
      btnBackshow: true,
      showBreadcrumb:true,
      breadCrumbList:[
        {
          'name': 'Assessment',
          'navigationPath': '/pages/assessment',
        },
        {
        'name': 'Question Bank',
        'navigationPath': '/pages/assessment/qcategory',
      },
      {
        'name': this.tempname,
        'navigationPath': '/pages/assessment/qcategory/questions',
      }]
    };
    } else if (item.tabTitle === "Question Type") {

     if(this.selectedActInd == 1)  {
      this.activeTypeName = "Choice";
      this.headers={
        title: this.title,
       btnsSearch: true,
       btnName2: 'Save',
       btnName2show: true,
       btnBackshow: true,
       showBreadcrumb:true,
       breadCrumbList:[
         {
           'name': 'Assessment',
           'navigationPath': '/pages/assessment',
         },
         {
         'name': 'Question Bank',
         'navigationPath': '/pages/assessment/qcategory',
       },
       {
        // 'name': this.formdata.cname,
        'name': this.tempname,

        'navigationPath': '/pages/assessment/qcategory/questions',
       }
     ]
     };
     this.spinner.show();
     setTimeout(() => {
       this.spinner.hide()
     }, 2000);
     }

     if(this.selectedActInd == 2) {
      this.activeTypeName = "Matching";
      this.headers={
        title: this.title,
       btnsSearch: true,
       btnName2: 'Save',
       btnName2show: true,
       btnBackshow: true,
       showBreadcrumb:true,
       breadCrumbList:[
         {
           'name': 'Assessment',
           'navigationPath': '/pages/assessment',
         },
         {
         'name': 'Question Bank',
         'navigationPath': '/pages/assessment/qcategory',
       },
       {
        //  'name': this.formdata.cname,
        'name': this.tempname,
         'navigationPath': '/pages/assessment/qcategory/questions',
       }
     ]
     };
     this.spinner.show();
     setTimeout(() => {
       this.spinner.hide()
     }, 2000);
     }


     if(this.selectedActInd == 3){
      this.activeTypeName = "Sequence";
      this.headers={
        title: this.title,
       btnsSearch: true,
       btnName2: 'Save',
       btnName2show: true,
       btnBackshow: true,
       showBreadcrumb:true,
       breadCrumbList:[
         {
           'name': 'Assessment',
           'navigationPath': '/pages/assessment',
         },
         {
         'name': 'Question Bank',
         'navigationPath': '/pages/assessment/qcategory',
       },
       {
        //  'name': this.formdata.cname,
        'name': this.tempname,
         'navigationPath': '/pages/assessment/qcategory/questions',
       }
     ]
     };
     this.spinner.show();
     setTimeout(() => {
       this.spinner.hide()
     }, 2000);
     }

     if(this.selectedActInd == 4) {
      this.headers={
        title: this.title,
       btnsSearch: true,
       btnName2: 'Save',
       btnName2show: true,
       btnBackshow: true,
       showBreadcrumb:true,
       breadCrumbList:[
         {
           'name': 'Assessment',
           'navigationPath': '/pages/assessment',
         },
         {
         'name': 'Question Bank',
         'navigationPath': '/pages/assessment/qcategory',
       },
       {
        //  'name': this.formdata.cname,
        'name': this.tempname,
         'navigationPath': '/pages/assessment/qcategory/questions',
       }
     ]
     };
     this.spinner.show();
     setTimeout(() => {
       this.spinner.hide()
     }, 2000);
      this.activeTypeName = "Short";
     }
      // this.selectedActInd =0;
      this.basic = false;
      this.type = true;
    }
  }

  changetab(f) {

    let item: any = {}
    item.tabTitle = "Question Type"
    if (f.valid) {
      this.selectTab(item)
    }  else {

      this.presentToast('warning', 'Please fill in the required fields');
      Object.keys(f.controls).forEach(key => {
        f.controls[key].markAsDirty();
      });
    }
  }

  submit(f) {
    if (f.valid) {
      this.loader = true;
      if (this.selectedTags.length > 0) {
        this.makeTagDataReady(this.selectedTags);
        // this.formdata.tags = this.formattedTags;
      }
      var category = {
        qid: this.formdata.qid,
        cid: this.formdata.cid,
        qname: this.formdata.qname,
        qtypeid: this.formdata.qtypeid,
        qFormat: this.formdata.qFormat,
        qtext: this.formdata.qtext,
        qcontent: this.formdata.qcontent,
        qinsrtu: this.formdata.qinsrtu,
        qfeed: this.formdata.qfeed,
        qlevelid: this.formdata.qlevelid,
        qvisible: this.formdata.qvisible,
        qtenantId: this.formdata.qtenantId,
        qtag: this.formdata.tags,
        userId: this.loginUserdata.data.data.id
        // tagsList : this.formdata.tags,
      };
      if (this.typeId == 2) {
        category.qid = 0;
        this.categoryImgData = undefined;
      }

      console.log("category", category);
      var fd = new FormData();
      fd.append("content", JSON.stringify(category));
      fd.append("file", this.categoryImgData);

      console.log("Question file", this.categoryImgData);
      console.log("Question Data ", category);

      let url = webApi.domain + webApi.url.addeditqbcategoryquestion;
      let fileUploadUrl = webApi.domain + webApi.url.fileUpload;
      // let fileUploadUrlLocal = 'http://localhost:9845' + webApi.url.fileUpload;
      if (this.categoryImgData != undefined) {
        this.spinner.show();
        this.webApiService.getService(fileUploadUrl, fd).then(
          rescompData => {
            this.spinner.hide();
            var temp: any = rescompData;
            this.fileUploadRes = temp;
            if (temp == "err") {
              // this.notFound = true;
              // var thumbUpload: Toast = {
              //   type: "error",
              //   title: "Question file",
              //   body: "Unable to upload Question file.",
              //   // body: temp.msg,
              //   showCloseButton: true,
              //   timeout: 4000
              // };
              // this.toasterService.pop(thumbUpload);
              this.presentToast('error', '');
            } else if (temp.type == false) {
              // var thumbUpload: Toast = {
              //   type: "error",
              //   title: "Question file",
              //   body: "Unable to upload Question file.",
              //   // body: temp.msg,
              //   showCloseButton: true,
              //   timeout: 4000
              // };
              // this.toasterService.pop(thumbUpload);
              this.presentToast('error', '');
            } else {
              if (
                this.fileUploadRes.data != null ||
                this.fileUploadRes.fileError != true
              ) {
                category.qcontent = this.fileUploadRes.data.file_url;
                this.addUpdateCategoryque(url, category);
              } else {
                // var thumbUpload: Toast = {
                //   type: "error",
                //   title: "Question file",
                //   // body: "Unable to upload category thumbnail.",
                //   body: this.fileUploadRes.status,
                //   showCloseButton: true,
                //   timeout: 4000
                // };
                // this.toasterService.pop(thumbUpload);
                this.presentToast('error', '');
              }
            }
            console.log("File Upload Result", this.fileUploadRes);
          },
          resUserError => {
            this.spinner.hide();
            this.loader = false;
            this.errorMsg = resUserError;
          }
        );
      } else {
        this.addUpdateCategoryque(url, category);
      }
    } else {
      // console.log('Please Fill all fields');
      // const addEditF: Toast = {
      //   type: 'error',
      //   title: 'Unable to update',
      //   body: 'Please Fill all fields',
      //   showCloseButton: true,
      //   timeout: 2000
      // };
      // this.toasterService.pop(addEditF);
      this.presentToast('warning', 'Please fill in the required fields');
      Object.keys(f.controls).forEach(key => {
        f.controls[key].markAsDirty();
      });
    }
  }

  makeTagDataReady(tagsData) {
    this.formdata.tags = ''
    tagsData.forEach((tag) => {
      if (this.formdata.tags == '') {
        this.formdata.tags = tag.id;
      } else {
        this.formdata.tags = this.formdata.tags + '|' + tag.id;
      }
      console.log('this.formdata.tags', this.formdata.tags);
    });
    // var tagsData = this.formdata.tags;
    // var tagsString = '';
    // if (tagsData) {
    //   for (let i = 0; i < tagsData.length; i++) {
    //     var tag = tagsData[i];
    //     if (tagsString != "") {
    //       tagsString += "|";
    //     }
    //     if (String(tag) != "" && String(tag) != "null") {
    //       tagsString += tag;
    //     }
    //   }
    //   this.formdata.tags1 = tagsString;
    // }
  }


  categoryqueAddEditRes: any;
  addUpdateCategoryque(url, category) {
    this.spinner.show();
    this.webApiService.getService(url, category).then(
      rescompData => {
        this.spinner.hide();
        var temp: any = rescompData;
        this.categoryqueAddEditRes = temp.data;
        if (temp == "err") {
          // this.notFound = true;
          // var catUpdate: Toast = {
          //   type: "error",
          //   title: "Question",
          //   body: "Unable to update Question.",
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
        } else if (temp.type == false) {
          // var catUpdate: Toast = {
          //   type: "error",
          //   title: "Question",
          //   body: this.categoryqueAddEditRes.msg,
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
        } else {
          // this.router.navigate(['/pages/assessment/qcategory']);
          // var catUpdate: Toast = {
          //   type: "success",
          //   title: "Question",
          //   // body: "Unable to update category.",
          //   body: this.categoryqueAddEditRes.msg,
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          if (this.catID == 2) {
            if (this.formdata.qFormat == 1) {
              this.savemultImageData(this.formdata);
            } else if (this.formdata.qFormat == 2) {
              this.savematchImageData(this.formdata);
              this.savemultImageData(this.formdata);
            } else if (this.formdata.qFormat == 3) {
              this.saveSequence(this.formdata);
            } else if (this.formdata.qFormat == 4) {
              this.saveShort(this.formdata);
            }
          }
          this.basic = false;
          this.type = true;
          // this.toasterService.pop(catUpdate);
          this.presentToast('success', this.categoryqueAddEditRes.msg);
          if(this.selectedActInd == 4) {
            this.saveShort(this.shortform.form);
          }
          if(this.selectedActInd == 3) {
            this.saveSequence(this.sequenceform.form);
          }
          if(this.selectedActInd == 2) {
            this.savematchImageData(this.matchingform.form);
          }
          if(this.selectedActInd == 1) {
            this.savemultImageData(this.multipleform.form);
          }




        }
        console.log("Question AddEdit Result ", this.categoryqueAddEditRes);
      },
      resUserError => {
        this.spinner.hide();
        this.errorMsg = resUserError;
      }
    );
  }







  savematchImageData(f) {

    f.value.optionRef0 = this.matching.moption[0].optionRef
    if(f.valid){
    this.matchimgarr = [];
    this.matchTemparr = [];
    this.matchcontent = [];
    var matching = {
      moption: this.matching.moption,
      qId: this.categoryqueAddEditRes
        ? this.categoryqueAddEditRes.id
          ? this.categoryqueAddEditRes.id
          : this.formdata.qid
        : this.formdata.qid,
      userId: this.loginUserdata.data.data.id,
      tId: this.loginUserdata.data.data.tenantId,
      option: ""
    };

    if (matching.qId) {
      for (let i = 0; i < matching.moption.length; i++) {
        if (matching.moption[i].contentRef) {
          this.matchimgarr.push(matching.moption[i].optionRef);
          this.matchTemparr.push(i);
          matching.moption[i].name = matching.moption[i].qfileName;
          matching.moption[i].author = matching.qId;
          this.matchcontent.push(matching.moption[i]);
        }
        matching.moption[i].author = "author";
      }

      console.log("mutimgarr", this.matchimgarr);
      console.log("multTemparr", this.matchTemparr);
      console.log("multcontent", this.matchcontent);

      var fd = new FormData();
      fd.append("content", JSON.stringify(this.matchcontent));
      for (let i = 0; i < this.matchimgarr.length; i++) {
        fd.append("file", this.matchimgarr[i]);
      }

      // console.log('Question file',this.categoryImgData);
      // console.log('Question Data ',category);

      // let url = webApi.domain + webApi.url.addeditqbcategoryquestion;
      let fileUploadUrl = webApi.domain + webApi.url.multifileUpload;
      // let fileUploadUrlLocal = 'http://localhost:9845' + webApi.url.fileUpload;
      if (this.matchimgarr.length > 0) {
        this.spinner.show();
        this.webApiService.getService(fileUploadUrl, fd).then(
          (rescompData: any) => {
            this.spinner.hide();
            var temp: any = rescompData;
            this.matchfileUploadRes = rescompData;
            if (temp == "err") {
              // this.notFound = true;
              // var thumbUpload: Toast = {
              //   type: "error",
              //   title: "Question type",
              //   body: "Unable to upload Answer Images.",
              //   // body: temp.msg,
              //   showCloseButton: true,
              //   timeout: 4000
              // };
              // this.toasterService.pop(thumbUpload);
              this.presentToast('error', '');
            } else {
              if (this.matchfileUploadRes.length > 0) {
                var imageData = [];
                for (let i = 0; i < this.matchfileUploadRes.length; i++) {
                  imageData.push(this.matchfileUploadRes[i]);
                }
                // var imgData = this.multfileUploadRes.data.file_url;

                this.saveMatching(matching, imageData);
              } else {
                // var thumbUpload: Toast = {
                //   type: "error",
                //   title: "Question file",
                //   // body: "Unable to upload category thumbnail.",
                //   body: "Unable to upload Answer Images",
                //   showCloseButton: true,
                //   timeout: 4000
                // };
                // this.toasterService.pop(thumbUpload);
                this.presentToast('error', '');
              }
            }
            console.log("File Upload Result", this.multfileUploadRes);
          },
          resUserError => {
            this.spinner.hide();
            this.errorMsg = resUserError;
          }
        );
      } else {
        var imgData = [];
        this.saveMatching(matching, imgData);
      }
    } else {
      // var thumbUpload: Toast = {
      //   type: "error",
      //   title: "Add-Question",
      //   // body: "Unable to upload category thumbnail.",
      //   body: "Kindly fill Basic Datails",
      //   showCloseButton: true,
      //   timeout: 4000
      // };
      this.basic = true;
      this.type = false;
      // this.toasterService.pop(thumbUpload);
      this.presentToast('warning', 'Please fill the basic details');
    }
  }else{
    Object.keys(f.controls).forEach(key => {
      f.controls[key].markAsDirty();
    });
  }
  }

  saveMatching(matching, data) {
    console.log("multiple", matching);
    console.log("data", data);
    this.matchCount = 0;
    for (let i = 0; i < matching.moption.length; i++) {
      this.matchCount += Number(matching.moption[i].mpoint);
    }

    for (let i = 0; i < matching.moption.length; i++) {
      matching.moption[i].perct =
        (matching.moption[i].mpoint * 100) / this.matchCount;
      matching.moption[i].mpoint = matching.moption[i].mpoint
        ? matching.moption[i].mpoint
        : 0;
      matching.moption[i].mipoint = matching.moption[i].mipoint
        ? matching.moption[i].mipoint
        : 0;

      if (matching.moption[i].name) {
        delete matching.moption[i].name;
      }
      if (matching.moption[i].author) {
        delete matching.moption[i].author;
      }

      if (matching.moption[i].contentRef) {
        delete matching.moption[i].contentRef;
      }
      //    if(multiple.moption[i].optionRef)
      // {
      //     multiple.moption[i].optionRef ="";
      // }
      for (let j = 0; j < data.length; j++) {
        const imageName = matching.moption[i].qfileName.replace(/ /g, "_")
        if (decodeURI(data[j]).includes(imageName)) {
          matching.moption[i].optionRef = data[j];
          break;
        }
      }
      if (matching.moption[i].qfileName) {
        delete matching.moption[i].qfileName;
      }
      matching.moption[i].qtypeid = Number(matching.moption[i].qtypeid);
      // matching.moption[i].optionMatch = this.sanitizeData(matching.moption[i].optionMatch);
      // matching.moption[i].manfeed = this.sanitizeData(multiple.moption[i].manfeed);
    }
    var option: string = Array.prototype.map
      .call(matching.moption,(item) => {
        console.log("item", item);
        return Object.values(item).join(this.dataSeparatorService.Hash);
      })
      .join(this.dataSeparatorService.Pipe);
    matching.option = option;
    console.log("this.multiple", matching);
    console.log("this.string", option);

    let url = webApi.domain + webApi.url.addeditqbquestionmatching;
    this.spinner.show();
    this.webApiService.getService(url, matching).then(
      rescompData => {
        this.spinner.hide();
        var temp1: any = rescompData;
        this.qbmatching = temp1.data;
        if (temp1 == "err") {
          // this.notFound = true;
          // var catUpdate: Toast = {
          //   type: "error",
          //   title: "Question",
          //   body: "Unable to update question Matching type.",
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
        } else if (temp1.type == false) {
          // var catUpdate: Toast = {
          //   type: "error",
          //   title: "Question",
          //   body: this.qbmatching.msg,
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
        } else {
          // this.router.navigate(["/pages/assessment/qcategory/questions"]);
          // var catUpdate: Toast = {
          //   type: "success",
          //   title: "Question",
          //   // body: "Unable to update category.",
          //   body: this.qbmatching.msg,
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          this.basic = false;
          this.type = true;

          this.showlist();
          this.selectedTags = [];
          this.formatID = 0;
          this.cloeSidebar()
          // this.toasterService.pop(catUpdate);
          this.presentToast('success', this.qbmatching.msg);
        }
        console.log("Question AddEdit Result ", this.qbmatching);
      },
      resUserError => {
        this.spinner.hide();
        this.loader = false;
        this.errorMsg = resUserError;
      }
    );
  }


  savemultImageData(f) {
    console.log(f);
    console.log(f.valid);
    if (f.valid) {
      this.mutimgarr = [];
      this.multTemparr = [];
      this.multcontent = [];
      var multiple = {
        qformatId: this.formdata.qFormat ? this.formdata.qFormat : 1,
        mvalue: this.multiple.mvalue,
        moption: this.multiple.moption,
        qId: this.categoryqueAddEditRes
          ? this.categoryqueAddEditRes.id
            ? this.categoryqueAddEditRes.id
            : this.formdata.qid
          : this.formdata.qid,
        userId: this.loginUserdata.data.data.id,
        tId: this.loginUserdata.data.data.tenantId,
        option: ""
      };

      if (multiple.qId) {
        for (let i = 0; i < multiple.moption.length; i++) {
          if (multiple.moption[i].contentRef) {
            this.mutimgarr.push(multiple.moption[i].optionRef);
            this.multTemparr.push(i);
            multiple.moption[i].name = multiple.moption[i].qfileName;
            multiple.moption[i].author = multiple.qId;
            this.multcontent.push(multiple.moption[i]);
          }
          multiple.moption[i].author = "author";
        }

        console.log("mutimgarr", this.mutimgarr);
        console.log("multTemparr", this.multTemparr);
        console.log("multcontent", this.multcontent);

        var fd = new FormData();
        fd.append("content", JSON.stringify(this.multcontent));
        for (let i = 0; i < this.mutimgarr.length; i++) {
          fd.append("file", this.mutimgarr[i]);
        }

        // console.log('Question file',this.categoryImgData);
        // console.log('Question Data ',category);

        // let url = webApi.domain + webApi.url.addeditqbcategoryquestion;
        let fileUploadUrl = webApi.domain + webApi.url.multifileUpload;
        // let fileUploadUrlLocal = 'http://localhost:9845' + webApi.url.fileUpload;
        if (this.mutimgarr.length > 0) {
          this.spinner.show();
          this.webApiService.getService(fileUploadUrl, fd).then(
            (rescompData: any) => {
              this.spinner.hide();
              var temp: any = rescompData;
              this.multfileUploadRes = rescompData;
              if (temp == "err") {
                // this.notFound = true;
                // var thumbUpload: Toast = {
                //   type: "error",
                //   title: "Question type",
                //   body: "Unable to upload Answer Images.",
                //   // body: temp.msg,
                //   showCloseButton: true,
                //   timeout: 4000
                // };
                // this.toasterService.pop(thumbUpload);
                this.presentToast('error', '');
              } else {
                if (this.multfileUploadRes.length > 0) {
                  var imageData = [];
                  for (let i = 0; i < this.multfileUploadRes.length; i++) {
                    imageData.push(this.multfileUploadRes[i]);
                  }

                  // var imgData = this.multfileUploadRes.data.file_url;
                  this.saveMultiple(multiple, imageData);
                } else {
                  // var thumbUpload: Toast = {
                  //   type: "error",
                  //   title: "Question file",
                  //   // body: "Unable to upload category thumbnail.",
                  //   body: "Unable to upload Answer Images",
                  //   showCloseButton: true,
                  //   timeout: 4000
                  // };
                  // this.toasterService.pop(thumbUpload);
                  this.presentToast('error', '');
                }
              }
              console.log("File Upload Result", this.multfileUploadRes);
            },
            resUserError => {
              this.spinner.hide();
              this.errorMsg = resUserError;
            }
          );
        } else {
          var imgData = [];
          this.saveMultiple(multiple, imgData);
        }
      } else {
        // var thumbUpload: Toast = {
        //   type: "error",
        //   title: "Add-Question",
        //   // body: "Unable to upload category thumbnail.",
        //   body: "Kindly fill Basic Datails",
        //   showCloseButton: true,
        //   timeout: 4000
        // };
        this.basic = true;
        this.type = false;
        // this.toasterService.pop(thumbUpload);
        this.presentToast('error', '');
      }
    } else {
      Object.keys(f.controls).forEach(key => {
        f.controls[key].markAsDirty();
      });
    }
  }
  saveMultiple(multiple, data) {
    console.log("multiple", multiple);
    console.log("data", data);

    if(this.checkValidationForCorrectAns(multiple.moption))
    {
      this.multCount = 0;
      for (let i = 0; i < multiple.moption.length; i++) {
        this.multCount += Number(multiple.moption[i].mpoint);
        multiple.moption[i].perct =
          (multiple.moption[i].mpoint * 100) / this.multCount;
      }

      for (let i = 0; i < multiple.moption.length; i++) {
        multiple.moption[i].mpoint = multiple.moption[i].mpoint
          ? multiple.moption[i].mpoint
          : 0;
        multiple.moption[i].perct =
          (multiple.moption[i].mpoint * 100) / this.multCount;
        multiple.moption[i].mipoint = multiple.moption[i].mipoint
          ? multiple.moption[i].mipoint
          : 0;
        multiple.moption[i].mopvalue =
          multiple.moption[i].mopvalue == ""
            ? 0
            : multiple.moption[i].mopvalue == true
              ? 1
              : multiple.moption[i].mopvalue;
        if (multiple.moption[i].name) {
          delete multiple.moption[i].name;
        }
        if (multiple.moption[i].author) {
          delete multiple.moption[i].author;
        }

        if (multiple.moption[i].contentRef) {
          delete multiple.moption[i].contentRef;
        }
        //    if(multiple.moption[i].optionRef)
        // {
        //     multiple.moption[i].optionRef ="";
        // }
        for (let j = 0; j < data.length; j++) {
          const imageName = multiple.moption[i].qfileName.replace(/ /g, "_")
          if (decodeURI(data[j]).includes(imageName)) {
            multiple.moption[i].optionRef = data[j];
            break;
          }
        }
        if (multiple.moption[i].qfileName) {
          delete multiple.moption[i].qfileName;
        }
        multiple.moption[i].qtypeid = Number(multiple.moption[i].qtypeid);
        // if(multiple.moption[i])
        // multiple.moption[i].manswer = this.sanitizeData(multiple.moption[i].manswer);
        // multiple.moption[i].manfeed = this.sanitizeData(multiple.moption[i].manfeed);
      }
      var option: string = Array.prototype.map
        .call(multiple.moption, (item) => {
          console.log("item", item);
          return Object.values(item).join(this.dataSeparatorService.Hash);
        })
        .join(this.dataSeparatorService.Pipe);
      multiple.option = option;
      console.log("this.multiple", multiple);
      console.log("this.string", option);

      let url = webApi.domain + webApi.url.addeditqbquestionmultiple;
      this.spinner.show();
      this.webApiService.getService(url, multiple).then(
        rescompData => {
          this.spinner.hide();
          var temp1: any = rescompData;
          this.qbMultiple = temp1.data;
          if (temp1 == "err") {
            // this.notFound = true;
            // var catUpdate: Toast = {
            //   type: "error",
            //   title: "Question",
            //   body: "Unable to update question Multiple type.",
            //   showCloseButton: true,
            //   timeout: 4000
            // };
            // this.toasterService.pop(catUpdate);
            this.presentToast('error', '');
          } else if (temp1.type == false) {
            // var catUpdate: Toast = {
            //   type: "error",
            //   title: "Question",
            //   body: this.qbMultiple.msg,
            //   showCloseButton: true,
            //   timeout: 4000
            // };
            // this.toasterService.pop(catUpdate);
            this.presentToast('error', '');
          } else {
            // this.router.navigate(["/pages/assessment/qcategory/questions"]);
            // var catUpdate: Toast = {
            //   type: "success",
            //   title: "Question",
            //   // body: "Unable to update category.",
            //   body: this.qbMultiple.msg,
            //   showCloseButton: true,
            //   timeout: 4000
            // };
            this.basic = false;
            this.type = true;
            this.showlist();

                  this.selectedTags = [];
                  this.formatID = 0;
                  this.cloeSidebar();
            // this.toasterService.pop(catUpdate);
            this.presentToast('success', this.qbMultiple.msg);
          }
          console.log("Question AddEdit Result ", this.qbMultiple);
        },
        resUserError => {
          this.spinner.hide();
          this.errorMsg = resUserError;
        }
      );
    }else{
      this.toastr.info('Please select valid answer for quiz', 'Info');
    }

  }


  saveShort(f) {
    console.log("formObj",f)
    if(f.valid&&f.value.min<=f.value.max){
    var short = {
      qformatId: this.formdata.qFormat ? this.formdata.qFormat : 1,
      min: this.short.min,
      max: this.short.max,
      score: null,
      penelty: null,
      qId: this.categoryqueAddEditRes
        ? this.categoryqueAddEditRes.id
          ? this.categoryqueAddEditRes.id
          : this.formdata.qid
        : this.formdata.qid,
      userId:this.loginUserdata.data.data.id,
      tId: this.loginUserdata.data.data.tenantId,
      visible: 1,
    };

    // for (let i = 0; i < short.soption.length; i++) {
    //   this.shortCount += Number(short.soption[i].score);
    // }
    // for (let i = 0; i < short.soption.length; i++) {
    //   short.soption[i].perct = (short.soption[i].score * 100) / this.shortCount;
    //   short.soption[i].isCase = short.scase;
    // }

    // var option: string = Array.prototype.map
    //   .call(short.soption, function (item) {
    //     return Object.values(item).join("#");
    //   })
    //   .join("|");
    // short.option = option;
    // console.log("this.multiple", short);
    // console.log("this.string", option);

    let url = webApi.domain + webApi.url.addeditqbquestionshort;
    this.spinner.show();
    this.webApiService.getService(url, short).then(
      rescompData => {
        this.spinner.hide();
        var temp1: any = rescompData;
        this.qbShort = temp1.data;
        if (temp1 == "err") {
          // this.notFound = true;
          // var catUpdate: Toast = {
          //   type: "error",
          //   title: "Question",
          //   body: "Unable to update question short type.",
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
        } else if (temp1.type == false) {
          // var catUpdate: Toast = {
          //   type: "error",
          //   title: "Question",
          //   body: this.qbShort.msg,
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
        } else {
          // this.router.navigate(["/pages/assessment/qcategory/questions"]);
          // var catUpdate: Toast = {
          //   type: "success",
          //   title: "Question",
          //   // body: "Unable to update category.",
          //   body: this.qbShort.msg,
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          this.basic = false;
          this.type = true;
          // this.toasterService.pop(catUpdate);
          this.presentToast('success', this.qbShort.msg);
          this.showlist();

                  this.selectedTags = [];
                  this.formatID = 0;
                  this.cloeSidebar();
        }
        console.log("Question AddEdit Result ", this.qbShort);
      },
      resUserError => {
        this.spinner.hide();
        this.errorMsg = resUserError;
      }
    );
    }else{
      Object.keys(f.controls).forEach(key => {
        f.controls[key].markAsDirty();
      });
    }
  }

  saveSequence(f) {
    if(f.valid){
      this.squCount = 0;
    var sequence = {
      soption: this.sequence.soption,
      qId: this.categoryqueAddEditRes
        ? this.categoryqueAddEditRes.id
          ? this.categoryqueAddEditRes.id
          : this.formdata.qid
        : this.formdata.qid,
      userId: this.loginUserdata.data.data.id,
      tId: this.loginUserdata.data.data.tenantId,
      option: ""
    };
    for (let i = 0; i < sequence.soption.length; i++) {
      this.squCount += Number(sequence.soption[i].spoint);
    }
    for (let i = 0; i < sequence.soption.length; i++) {
      sequence.soption[i].perct =
        (sequence.soption[i].spoint * 100) / this.squCount;
      sequence.soption[i].sindex = i;
      // sequence.soption[i].sanswer = this.sanitizeData(sequence.soption[i].sanswer);
    }

    var option: string = Array.prototype.map
      .call(sequence.soption, (item) => {
        return Object.values(item).join(this.dataSeparatorService.Hash);
      })
      .join(this.dataSeparatorService.Pipe);
    sequence.option = option;
    console.log("this.multiple", sequence);
    console.log("this.string", option);

    let url = webApi.domain + webApi.url.addeditqbquestionsequence;
    this.spinner.show();
    this.webApiService.getService(url, sequence).then(
      rescompData => {
        this.spinner.hide();
        var temp1: any = rescompData;
        this.qbSequence = temp1.data;
        if (temp1 == "err") {
          // this.notFound = true;
          // var catUpdate: Toast = {
          //   type: "error",
          //   title: "Question",
          //   body: "Unable to update question Sequence type.",
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          this.presentToast('error', '');
          // this.toasterService.pop(catUpdate);

        } else if (temp1.type == false) {
          // var catUpdate: Toast = {
          //   type: "error",
          //   title: "Question",
          //   body: this.qbSequence.msg,
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
        } else {
          // this.router.navigate(["/pages/assessment/qcategory/questions"]);
          // var catUpdate: Toast = {
          //   type: "success",
          //   title: "Question",
          //   // body: "Unable to update category.",
          //   body: this.qbSequence.msg,
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          this.basic = false;
          this.type = true;
          // this.toasterService.pop(catUpdate);
          this.presentToast('success', this.qbSequence.msg);
          this.showlist();

                  this.selectedTags = [];
                  this.formatID = 0;
                  this.cloeSidebar();
        }
        console.log("Question AddEdit Result ", this.qbSequence);
      },
      resUserError => {
        this.spinner.hide();
        this.errorMsg = resUserError;
      }
    );
    }else{
      Object.keys(f.controls).forEach(key => {
        f.controls[key].markAsDirty();
      });
    }
  }


  checkValidationForCorrectAns(data) {
    if(data.length !=0) {
      // data.forEach(element => {
      //   if(element.mopvalue == true || element.mopvalue == 1) {
      //     return true;
      //   }
      // });
      for (let index = 0; index < data.length; index++) {
        if(data[index].mopvalue == true || data[index].mopvalue == 1) {
              return true;
            }
      }
    }
      return false;
  }


  addmulti() {
    // for(let i=0;i<this.multiple.moption.length;i++){
    //   if(this.multiple.moption[i].mipoint && this.multiple.moption[i].mpoint && this.multiple.moption[i].mpoint )
    // }

    if(this.multipleform.form.valid) {
      var option = {
        manswer: "",
        mpoint: "",
        mipoint: 0,
        manfeed: "",
        mopvalue: false,
        qtypeid: 1,
        optionRef: "",
        qfileName: "No File Chosen",
        perct: 0
      };
      this.multiple.moption.push(option);
      if (this.multiple.length > 0) {
        this.multiple.moption.forEach((data, key) => {
          if (this.tempCheckedIndex == key) {
            data.mopvalue = true;
          } else {
            data.mopvalue = false;
          }
        });
      }
    } else {
      this.presentToast('warning', 'Please fill in the required fields');
      Object.keys(this.multipleform.form.controls).forEach(key => {
        this.multipleform.form.controls[key].markAsDirty();
      });
    }

  }

  radiobtn(event, ind) {
    if(event){
      for (let i = 0; i < this.multiple.moption.length; i++) {
        if (ind != i) {
          this.multiple.moption[i].mopvalue = false;
          // this.multiple.moption[i].mipoint='';
          this.multiple.moption[i].mpoint = "";
          // this.multiple.moption[i].isSelected=0;
        } else {
          // this.multiple.moption[i].isSelected=1;
          this.multiple.moption[i].mopvalue = true;
        }
      }
    } else {
      this.multiple.moption[ind].mopvalue = false;
    }
    // this.multiple.moption.forEach((item, key)=>{
    //   item.mopvalue = false;
    // });
    // this.multiple.moption[ind].mopvalue =true;
    //    this.target == event.target.checked;
    this.tempCheckedIndex = ind;
    this.cdf.detectChanges();
  }

  removemulti(id) {
    for (let i = 0; i < this.multiple.moption.length; i++) {
      if (i == id) {
        this.multiple.moption.splice(i, 1);
      }
    }
  }

  addanswer(f) {
    if(f.valid){
    var option = {
      qtypeid: 1,
      optionText: "",
      optionRef: "",
      optionMatch: "",
      mpoint: "",
      mipoint: 0,
      qfileName: "No File Chosen",
      perct: 0
    };
    this.matching.moption.push(option);
  }else{
      Object.keys(f.controls).forEach(key => {
        f.controls[key].markAsDirty();
      });
  }
  }

  addsequence() {
    if(this.sequenceform.form.valid) {
      var option = {
        sanswer: "",
        sindex: "",
        spoint: "",
        sipoint: 0,
        perct: 0
      };
      this.sequence.soption.push(option);
    } else {
      this.presentToast('warning', 'Please fill in the required fields');
      Object.keys(this.sequenceform.form.controls).forEach(key => {
        this.sequenceform.form.controls[key].markAsDirty();
      });
    }

  }
  removesequence(id) {
    for (let i = 0; i < this.sequence.soption.length; i++) {
      if (i == id) {
        this.sequence.soption.splice(i, 1);
      }
    }
  }

  remove(id) {
    for (let i = 0; i < this.matching.moption.length; i++) {
      if (i == id) {
        this.matching.moption.splice(i, 1);
      }
    }
  }

  qtypechange() {
    this.formdata.qtext = "";
    this.formdata.content = "";
    this.formdata.qfileName = "You can drag and drop files here to add them.";
  }

  onTagsSelect(item: any) {
    console.log(item);
    console.log(this.selectedTags);
  }

  OnTagDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedTags);
  }

  onTagSearch(evt: any) {
    console.log(evt.target.value);
    const val = evt.target.value;
    this.tagList = [];
    const temp = this.tempTags.filter(function (d) {
      return (
        String(d.name)
          .toLowerCase()
          .indexOf(val) !== -1 ||
        !val
      );
    });

    // update the rows
    this.tagList = temp;
    console.log('filtered Tag LIst', this.tagList);
  }


  save(categoryForm) {
    if(this.selectedActInd == 4) {
        if(categoryForm.valid && this.shortform.form.valid && this.short.max >= this.short.min) {
        this.submit(categoryForm);
        // this.saveShort(this.shortform.form);
      } else {
        this.presentToast('warning', 'Please fill in the required fields');
        Object.keys(categoryForm.controls).forEach(key => {
          categoryForm.controls[key].markAsDirty();
        });
        Object.keys(this.shortform.form.controls).forEach(key => {
          this.shortform.form.controls[key].markAsDirty();
        });
      }

    } else if(this.selectedActInd == 3) {
      if(categoryForm.valid && this.sequenceform.form.valid) {
        // this.saveSequence(this.sequenceform.form);
        this.submit(categoryForm);
      } else {
        this.presentToast('warning', 'Please fill in the required fields');
        Object.keys(categoryForm.controls).forEach(key => {
          categoryForm.controls[key].markAsDirty();
        });
        Object.keys(this.sequenceform.form.controls).forEach(key => {
          this.sequenceform.form.controls[key].markAsDirty();
        });
      }

    } else if(this.selectedActInd == 2) {

      if(categoryForm.valid && this.matchingform.form.valid) {
        // this.savematchImageData(this.matchingform.form);
         this.submit(categoryForm);
      } else {
        this.presentToast('warning', 'Please fill in the required fields');
        Object.keys(categoryForm.controls).forEach(key => {
          categoryForm.controls[key].markAsDirty();
        });
        Object.keys(this.matchingform.form.controls).forEach(key => {
          this.matchingform.form.controls[key].markAsDirty();
        });
      }

    }
     else {
      if(categoryForm.valid && this.multipleform.form.valid) {
        // this.savemultImageData(this.multipleform.form);
        this.submit(categoryForm);
      } else {
        this.presentToast('warning', 'Please fill in the required fields');
        Object.keys(categoryForm.controls).forEach(key => {
          categoryForm.controls[key].markAsDirty();
        });
        Object.keys(this.multipleform.form.controls).forEach(key => {
          this.multipleform.form.controls[key].markAsDirty();
        });
      }

    }
  }


  readquestion(event: any) {
    var size = 10000000;
    this.formdata.qfileType = event.target.files[0].type;
    var validExts = new Array("image", "video", "audio");
    var fileExt = event.target.files[0].type;
    fileExt = fileExt.substring(0, 5);
    // var fileExt = event.target.files[0].name;
    // fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (size <= event.target.files[0].size) {
      // var toast: Toast = {
      //   type: "error",
      //   title: "file size exceeded!",
      //   body: "files size should be less than 10MB",
      //   showCloseButton: true,
      //   // tapToDismiss: false,
      //   timeout: 4000
      //   // onHideCallback: () => {
      //   //     this.router.navigate(['/pages/plan/users']);
      //   // }
      // };
      // this.toasterService.pop(toast);
      this.presentToast('warning', 'File size should be less than 10MB');
      // this.deleteCourseThumb();
    } else {
      if (validExts.indexOf(fileExt) < 0) {
        // var toast: Toast = {
        //   type: "error",
        //   title: "Invalid file selected!",
        //   body: "Valid files are of " + validExts.toString() + " types.",
        //   showCloseButton: true,
        //   // tapToDismiss: false,
        //   timeout: 4000
        //   // onHideCallback: () => {
        //   //     this.router.navigate(['/pages/plan/users']);
        //   // }
        // };
        // this.toasterService.pop(toast);
        this.presentToast('warning', 'Valid file types are ' + validExts.toString());
      } else if (event.target.files && event.target.files[0]) {
        this.categoryImgData = event.target.files[0];
        this.formdata.qfileName = event.target.files[0].name;
        var reader = new FileReader();

        reader.onload = (event: ProgressEvent) => {
          // this.defaultThumb = (<FileReader>event.target).result;
          this.formdata.contentRef = (<FileReader>event.target).result;
          // this.formdata.filename=event.target.files[0].name
        };
        reader.readAsDataURL(event.target.files[0]);
      }
    }
  }

  cancel() {
    // this.fileUpload.nativeElement.files = [];
    console.log(this.fileUpload.nativeElement.files);
    this.fileUpload.nativeElement.value = "";
    console.log(this.fileUpload.nativeElement.files);
    this.formdata.qfileName = "You can drag and drop files here to add them.";
    this.enableUpload = false;
  }


  onMultiple() {
    for (let i = 0; i < this.multiple.moption.length; i++) {
      this.multiple.moption[i].mopvalue = "";
      this.multiple.moption[i].mipoint = 0;
      this.multiple.moption[i].mpoint = "";
    }
  }

  choicetypechange(mid) {
    for (let i = 0; i < this.multiple.moption.length; i++) {
      if (i == mid) {
        this.multiple.moption[i].optionRef = "";
        this.multiple.moption[i].manswer = "";
        this.multiple.moption[i].qfileName = "No File Chosen";
      }
    }
  }

  cancelmulti(ind) {
    for (let i = 0; i < this.multiple.moption.length; i++) {
      if (i == ind) {
        this.multiple.moption[i].qfileName = "No File Chosen";
        this.multiple.moption[i].optionRef = "";
        this.multiple.moption[i].contentRef = undefined;
      }
    }
  }

  readmultimg(event: any, ind) {
    var size = 100000;
    var validExts = new Array("image");
    var fileExt = event.target.files[0].type;
    fileExt = fileExt.substring(0, 5);
    if (size <= event.target.files[0].size) {
      // var toast: Toast = {
      //   type: "error",
      //   title: "file size exceeded!",
      //   body: "files size should be less than 100KB",
      //   showCloseButton: true,
      //   timeout: 4000
      // };
      // this.toasterService.pop(toast);
      this.presentToast('warning', 'File size should be less than 100KB');
      event.target.value = "";
      // this.deleteCourseThumb();
    } else {
      if (validExts.indexOf(fileExt) < 0) {
        // var toast: Toast = {
        //   type: "error",
        //   title: "Invalid file selected!",
        //   body: "Valid files are of " + validExts.toString() + " types.",
        //   showCloseButton: true,
        //   timeout: 4000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('warning', 'Valid file types are ' + validExts.toString());
      } else if (event.target.files && event.target.files[0]) {
        for (let i = 0; i < this.multiple.moption.length; i++) {
          if (i == ind) {
            this.multiple.moption[i].qfileName = event.target.files[0].name
              ? event.target.files[0].name
              : "No file Chosen";
            this.multiple.moption[i].optionRef = event.target.files[0];
          }
        }
      }
    }
  }

  matchtypechange(mid) {
    for (let i = 0; i < this.matching.moption.length; i++) {
      if (i == mid) {
        this.matching.moption.optionText = "";
        this.matching.moption.optionRef = "";
        this.matching.moption[i].qfileName = "No File Chosen";
      }
    }
  }

  cancelmatch(ind) {
    for (let i = 0; i < this.matching.moption.length; i++) {
      if (i == ind) {
        this.matching.moption[i].qfileName = "No File Chosen";
        this.matching.moption[i].optionRef = "";
        this.matching.moption[i].contentRef = undefined;
      }
    }
  }

  readmatchimg(event: any, ind, qTypeid) {
    var size = 100000;
    var validExts = new Array("image");
    var fileExt = event.target.files[0].type;
    fileExt = fileExt.substring(0, 5);
    if (size <= event.target.files[0].size) {
      // var toast: Toast = {
      //   type: "error",
      //   title: "file size exceeded!",
      //   body: "files size should be less than 100KB",
      //   showCloseButton: true,
      //   timeout: 4000
      // };
      // this.toasterService.pop(toast);
      this.presentToast('warning', 'File size should be less than 100KB');
      event.target.value = "";
      // this.deleteCourseThumb();
    } else {
      if (validExts.indexOf(fileExt) < 0) {
        // var toast: Toast = {
        //   type: "error",
        //   title: "Invalid file selected!",
        //   body: "Valid files are of " + validExts.toString() + " types.",
        //   showCloseButton: true,
        //   timeout: 4000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('warning', 'Valid file types are ' + validExts.toString());
      } else if (event.target.files && event.target.files[0]) {
        for (let i = 0; i < this.matching.moption.length; i++) {
          if (i == ind) {
            this.matching.moption[i].qfileName = event.target.files[0].name
              ? event.target.files[0].name
              : "No file Chosen";
            this.matching.moption[i].optionRef = event.target.files[0];
          }
        }
      }
    }
  }

}














