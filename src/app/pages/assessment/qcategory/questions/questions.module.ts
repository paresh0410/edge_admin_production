// import { routing }       from './addEditUser.routing';
import { questions } from './questions.component';
import {  questionsService } from './questions.service';


import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

//import { AgGridModule } from 'ag-grid-angular';
// import 'ag-grid-enterprise';
import 'ag-grid-community';

import { MyDatePickerModule } from 'mydatepicker';
// import { TruncateModule } from 'ng2-truncate';
// import { TabsModule } from 'ngx-tabs';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ComponentModule } from '../../../../component/component.module';
import { NbTabsetModule } from '@nebular/theme';
import { TagInputModule } from 'ngx-chips';
import { SortablejsModule } from 'angular-sortablejs';
import { JoditAngularModule } from 'jodit-angular';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';


@NgModule({
  imports: [
    CommonModule,
    // routing,
    FormsModule,
   // AgGridModule.withComponents([]),
    // TabsModule,
    ReactiveFormsModule,
    Ng2SmartTableModule,
    NgxDatatableModule,
    NbTabsetModule,
    NgxSpinnerModule,
    ComponentModule,
    TagInputModule,
    SortablejsModule,
    JoditAngularModule,
    AngularMultiSelectModule,
  ],
  declarations: [
     questions
    // ChartistJs
  ],
  providers: [
     questionsService,
  ],
  schemas: [ 
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA 
  ]
})
export class  questionsModule {}
