import { NgModule, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MyDatePickerModule } from 'mydatepicker';
import { FilterPipeModule  } from 'ngx-filter-pipe';
import { qCategory } from './qcategory.component';
import { qCategoryService } from './qcategory.service';
// import { ContentService } from '../content/content.service';
import { qAddeditccategoryService } from './qaddEditCategory/qaddEditCategory.service';
import { NotFoundComponent } from './not-found/not-found.component';
import { ThemeModule } from '../../../@theme/theme.module';
import { UploadQuestionComponent } from './upload-question/upload-question.component';
import { JsonToXlsxService } from './upload-question/json-to-xlsx-service';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgxContentLoadingModule } from 'ngx-content-loading';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ComponentModule } from '../../../component/component.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';

// import { MiscellaneousRoutingModule, routedComponents } from './miscellaneous-routing.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FilterPipeModule,
    ThemeModule,
    NgxDatatableModule,
    NgxSkeletonLoaderModule,
    NgxContentLoadingModule,
    NgxSpinnerModule,
    ComponentModule,
    AngularMultiSelectModule
  ],
  declarations: [
    qCategory,
    NotFoundComponent,
    UploadQuestionComponent
  ],
  providers: [
    qCategoryService,
    qAddeditccategoryService,
    JsonToXlsxService
    // qContentService
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})

export class qCategoryModule {}
