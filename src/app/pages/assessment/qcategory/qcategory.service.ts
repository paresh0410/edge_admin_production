import { Injectable, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AppConfig } from '../../../app.module';
import { HttpClient } from '@angular/common/http';
import { webApi } from '../../../service/webApi';
@Injectable()
export class qCategoryService {
  // private _url: string = '/api/web/getall_qbcat';
  private _url: string = '/api/web/getall_qbcat_demo';
  private getquizcategoryUrl: string = webApi.domain + webApi.url. getquizcategory;

  public data: any;

  constructor(
    @Inject('APP_CONFIG_TOKEN') private config: AppConfig,
    private _http: Http, private http1: HttpClient
  ) { }

  getquizCategories(data) {
    console.log(data);
    return new Promise(resolve => {
      this.http1.post(this.getquizcategoryUrl, data)
        //.map(res => res.json())

        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });

  }

  _errorHandler(error: Response) {
    console.error(error);
    return Observable.throw(error || 'Server Error');
  }
}
