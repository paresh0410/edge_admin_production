import { Component, ViewEncapsulation, ViewChild, ElementRef, AfterViewInit} from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { Router, NavigationStart, Routes, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { qCategoryService } from './qcategory.service';
import { qAddeditccategoryService } from './qaddEditCategory/qaddEditCategory.service';
import { questionsService } from './questions/questions.service';
import { BaseChartDirective } from 'ng2-charts/ng2-charts';
import { NgxSpinnerService } from 'ngx-spinner';
import { webAPIService } from '../../../service/webAPIService';
import { webApi } from '../../../service/webApi';
import { ToastrService } from 'ngx-toastr';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { Card } from '../../../models/card.model';
import { SuubHeader } from '../../components/models/subheader.model';
import { noData } from '../../../models/no-data.model';
import { HttpClient } from '@angular/common/http';
import { addEditquestionService } from './addEditquestion/addEditquestion.service';

// import { ContentService } from '../content/content.service';

@Component({
  selector: 'qcategory',
  styleUrls: ['./qcategory.scss'],
  // template:'category works'
  templateUrl: './qcategory.html',
  encapsulation: ViewEncapsulation.None,
})
export class qCategory implements AfterViewInit{
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No Question Bank Categories at this time.",
    desc:"Question Bank Categories will appear after they are added by the instructor. Question Bank Categories are classification for managing the question bank",
    titleShow:true,
    btnShow:true,
    descShow:true,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/assessment-how-to-create-a-question-bank-category',
  }

  header: SuubHeader  = {
    title:'Question Bank',
    btnsSearch: true,
    placeHolder:'Search by question bank category',
    searchBar: true,
    searchtext: '',
    dropdownlabel: ' ',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: 'Upload',
    btnName3: '',
    btnAdd: 'Add Category',
    btnName1show: false,
    btnName2show: true,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: true,
    filter: false,
    showBreadcrumb: true,
    
    breadCrumbList:[
      {
        'name': 'Assessment',
        'navigationPath': '/pages/assessment',
      },
    ]
  };
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  notFound: boolean = false;

  query: string = '';
  public getData;
  item: any;
  cat: any;
  loginUserdata : any;
  skeleton = false;

  errorMsg: string;

  cardModify: Card = {
    flag: 'questionbank',
    titleProp : 'catName',
    discrption: 'description',
    question: 'qcnt',
    image: 'picRef',
    hoverlable:   true,
    showBottomQuestion: true,
    showImage: true,
    option:   true,
    editIcon: true,
    eyeIcon:   true,
    bottomDiv:   true,
    bottomDiscription:   true,
    showBottomList:   true,
    bottomTitle:   true,
    btnLabel: 'Details',
    defaultImage: 'assets/images/category.jpg'
  };


  parentcat: any = [{
    id: 0,
    name: 'Parent'
  }, {
    id: 1,
    name: 'Assigned'
  },
  {
    id: 2,
    name: 'Recommended'
  },
  {
    id: 3,
    name: 'Special'
  }]
  currentPage = 1;
  totalPages: any;
  pagelmt = 12;
  content1: any = [];
  category: any = [];
  search: any;
  loader: any;
  catdata: any;
  conentHeight: string;
  offset: number = 0;
  str: any = '';
  isworking = false;
  searchText: any;
  enable: boolean;
  title: string;
  qCategory: any;
  enableDisableModal: boolean;
  sidebarForm = false;
  helpContent: any;
  formdata:any;
  qbformat: any;
  tempTags:any =[];
  tagList:any = [];
  selectedTags:any = [];
  userdata:any;
  settingsTagDrop ={};
  titleName = "Add Category"
  btnName = "Save"
  defaultThumb : any = 'assets/images/category.jpg';
  fromSearch: boolean=false;
  count:number=8
  //protected service: CCategorydataService,
  constructor(private spinner: NgxSpinnerService,  protected service: addEditquestionService,protected categoryService: qCategoryService,private commonFunctionService: CommonFunctionsService,
    protected addCategoryService: qAddeditccategoryService, private toastr: ToastrService,private http1: HttpClient,
    protected questionsService: questionsService, private router: Router,
    // private toasterService: ToasterService, 
    protected webApiService: webAPIService) {
    // this.loader = true;
    // this.spinner.show()

   

    if (localStorage.getItem('LoginResData')) {
      this.loginUserdata = JSON.parse(localStorage.getItem('LoginResData'));
    }

    var catdata = {
      tId: this.loginUserdata.data.data.tenantId
    };
    

   
    this.service.getqbdropdowun(catdata).then(
      rescompData => {
       
        this.qbformat = rescompData['data'].qbformat;
        console.log( " this.qbformat =============================>",this.qbformat);
      },
      resUserError => {
        this.spinner.hide();
        this.errorMsg = resUserError;
       
      }
    );




    this.getHelpContent();
    // console.log("Add Category Called");
    var category;
    var id;
    
    this.settingsTagDrop = {
      text: 'Select Tags',
      singleSelection: false,
      classes: 'common-multi',
      primaryKey: 'id',
      labelKey: 'name',
      noDataLabel: 'Search Tags...',
      enableSearchFilter: true,
      lazyLoading: true,
      searchBy: ['name'],
      maxHeight:250,
    };
    // if(this.service.data != undefined){
    //   category = this.service.data.data;
    //   id = this.service.data.id;
    //   // this.catData = this.service.data.data;
    // }else{
    //   id = 0;
    // }
     

  
    this.spinner.hide()
   
    console.log('Category Info',category);
    console.log('id',id);



   
    this.conentHeight = '0px';
    this.search = {};
    // this.category;
    this.cat = {
      id: ''
    };

  
    this.getCategories(this.str, this.currentPage);
    this.spinner.hide()
  }
  ngAfterViewInit() {
    
    const e = document.getElementsByTagName('nb-layout-column');
    console.log(e[0].clientHeight);
    this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
  }
 
ngOnInit(){
  // id = 0;
  // category = undefined;
 this.getallTagList(0,undefined);
}
  
 
  getCategories(str, pageNo) {
    // this.spinner.show();
    this.skeleton = false;
    const catdata = {
      tId: this.loginUserdata.data.data.tenantId,
      lmt: this.pagelmt,
      pno: pageNo,
      str: str,
    }
    const getquizcategoryUrl: string = webApi.domain + webApi.url. getquizcategory;
    this.commonFunctionService.httpPostRequest(getquizcategoryUrl,catdata)
    //this.categoryService.getquizCategories(catdata)
      .then(rescompData => {
        this.spinner.hide();
        this.skeleton = true;
        if (rescompData['type'] === true) {
          this.totalPages = rescompData['data']['totalPages'];
          const list = rescompData['data']['list'];
          this.skeleton = true;
          this.notFound = false;
          if (this.category.length > 0 && pageNo > 1) {
            this.category = this.category.concat(list);
            this.skeleton = true;
          this.notFound = false;
          } else {
            this.category = list;
            this.skeleton = true;
          }
          if(!list.length){
            this.notFound = true;
            } 
          if(this.fromSearch && this.category.length==0){
            this.noDataVal={
              margin:'mt-5',
              imageSrc: '../../../../../assets/images/no-data-bg.svg',
              title:"Sorry we couldn't find any matches please try again",
              desc:".",
              titleShow:true,
              btnShow:false,
              descShow:false,
              btnText:'Learn More',
              btnLink:''
            }
          }
          this.isworking = false;
        }
   
        else{
          this.notFound = true;
          this.skeleton = true;
        }
        // this.category = rescompData['data'][0];
        console.log('Category Result', this.category);
      },
        resUserError => {
          // this.loader =false;
          // this.spinner.hide();
          this.skeleton = true;
          this.errorMsg = resUserError;
          this.notFound = true;
          // this.router.navigate(['**']);
        });
  }

  
  cloeSidebar() {
    this.sidebarForm = false;
  }

  back() {
    this.router.navigate(['/pages/assessment']);
  }
  ngOnDestroy(){
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0){
      e1[0].classList.add('with-scroll');
    }
  }
  ngAfterContentChecked() {
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0)
    {
      e1[0].classList.remove('with-scroll');
    }

   }
  content2(data, id) {
    var passData = {
      id: id,
      data: data,
      catdata: this.category
    }
    this.questionsService.data = passData;
    this.router.navigate(['/pages/assessment/qcategory/questions']);
    // this.passService.data = {};
  }

  gotoCardQuestion(data) {
    var passData = {
      id: 1,
      data: data,
      catdata: this.category
    }
    this.questionsService.data = passData;
    this.router.navigate(['/pages/assessment/qcategory/questions']);
  }

  gotoCardaddedit(data) {
    // var data1 = {
    //   data: data,
    //   id: 1
    // }
    this.titleName="Edit Category"
    const id =1

    this.makeCategoryDataReady(id, data);
    // this.addCategoryService.data = data1;
    // this.router.navigate(['/pages/assessment/qcategory/qaddEditCategory']);
  }

  public addeditccategory(data, id) {



    this.sidebarForm = true

    let idpass = 0
    
    this.selectedTags = []
    this.makeCategoryDataReady(idpass, undefined);

    // this.getallTagList(id,data);

    // this.loader==true;
    // var data1 = {
    //   data: data,
    //   id: id
    // }

    // if (data == undefined) {
    //   this.loader=false;
    //   this.addCategoryService.data = data1;
    //   this.router.navigate(['/pages/assessment/qcategory/qaddEditCategory']);
    // } else {
    //   this.loader=true;
    //   this.addCategoryService.data = data1;
    //   this.router.navigate(['/pages/assessment/qcategory/qaddEditCategory']);

    // }

  }


  delatecategory(ind) {
    for (let i = 0; i < this.category.length; i++) {
      if (ind == i) {
        this.category.splice(i, 1)
      }
    }
  }

 

 
  visibiltyRes: any;
  disableQcate(i, item) {
    this.spinner.show();
    var visibilityData = {
      id: item.id,
      visible: item.visible == 1 ? 0 : 1
    }
    let url = webApi.domain + webApi.url.disablequestionbank;
    this.webApiService.getService(url, visibilityData)
      .then(rescompData => {
        // this.loader =false;
        this.spinner.hide();
        this.visibiltyRes = rescompData;
        // console.log('Category Visibility Result',this.visibiltyRes);
        if (this.visibiltyRes.type == false) {
          // var catUpdate : Toast = {
          //     type: 'error',
          //     title: "Category",
          //     body: "Unable to update visibility of question category.",
          //     showCloseButton: true,
          //     timeout: 4000
          // };
          // // this.closeEnableDisableCategoryModal();
          // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
        } else {
          // var catUpdate : Toast = {
          //     type: 'success',
          //     title: "Category",
          //     body: this.visibiltyRes.data,
          //     showCloseButton: true,
          //     timeout: 4000
          // };
          // // this.closeEnableDisableCategoryModal();
          // this.toasterService.pop(catUpdate);
          this.presentToast('success', this.visibiltyRes.data);
          this.disablevaluechnage(i, item)
          // this.getCategories(this.catdata);
        }
      },
        resUserError => {
          // this.loader =false;
          this.spinner.hide();
          if (resUserError.statusText == "Unauthorized") {
            this.router.navigate(['/login']);
          }
          this.errorMsg = resUserError;
          // this.closeEnableDisableCategoryModal();
        });
  }



  clickTodisable(item) {
    // this.spinner.show();
    var visibilityData = {
      id: item.id,
      visible: item.visible
    }

    // if(item.visible == 1) {
    //   item.visible = 0;
    // } else {
    //   item.visible =1;
    // }
    let url = webApi.domain + webApi.url.disablequestionbank;
    this.webApiService.getService(url, visibilityData)
      .then(rescompData => {
        // this.loader =false;
        // this.spinner.hide();
        this.visibiltyRes = rescompData;
       
        if (this.visibiltyRes.type == false) {
          
          this.presentToast('error', '');
        } else {
          
          this.presentToast('success', this.visibiltyRes.data);
          
         this.closeEnableDisableModal();
        }
      },
        resUserError => {
          // this.loader =false;
          this.spinner.hide();
          if (resUserError.statusText == "Unauthorized") {
            this.router.navigate(['/login']);
          }
          this.errorMsg = resUserError;
          // this.closeEnableDisableCategoryModal();
        });
  }
  Disbaledata: any;
  disablevaluechnage(i, item) {
    this.Disbaledata = item;
    this.Disbaledata.visible = this.Disbaledata.visible == 1 ? 0 : 1
    //this.category[i].visible=this.category[i].visible==1?0:1
  }

  onScroll(event) {
    let element = this.myScrollContainer.nativeElement;
    // element.style.height = '500px';
    // element.style.height = '500px';
    // Math.ceil(element.scrollHeight - element.scrollTop) === element.clientHeight
    if (element.scrollHeight - element.scrollTop - element.clientHeight < 20) {
      if (!this.isworking) {
        this.currentPage++;
        if (this.currentPage <= this.totalPages) {
          this.isworking = true;
          this.skeleton = false;
          this.getCategories(this.str, this.currentPage);
        }
      }
    }
  }
  onsearch(evt: any) {
    if(evt.target.value != ''){
    console.log(evt);
    this.str = evt.target.value;
    if (evt.keyCode === 13) {
      this.currentPage = 1;
      this.getCategories(this.str, this.currentPage);
     }
    } else {
    this.clear();
    }
  }


  clear() {
    if(this.searchText.length>=3){
    this.header.searchtext = ''
    this.search = {};
    this.str = '';
    this.currentPage = 1;
    this.searchText='';
    this.getCategories(this.str, this.currentPage);
    this.notFound=false
    }
  
  else{
    this.search = {};

  }
}

  onsearchTest(evt: any) {
    this.searchText=evt.target.value;
    if(this.searchText.length>=3||this.searchText.length==0){
    // if(evt.target.value != ''){
    console.log(evt);
    this.header.searchtext = evt.target.value;
    this.currentPage=1 ;
    this.fromSearch=true;
    this.getCategories( this.header.searchtext, this.currentPage);
    // if (evt.keyCode === 13) {
    //   this.currentPage = 1;
    //   this.getCategories(this.str, this.currentPage);
    //  }
    // } else {
    // this.clear();
    // }
  }
  }

  uploadQuestion(){
    this.router.navigate(['/pages/assessment/qcategory/upload_questions']);
  }
  clickTodisableCategory(catData,i) {

    if(this.category[i].visible == 1) {
      this.category[i].visible = 0
    } else {
      this.category[i].visible = 1
    }	
      // if (catData.visible == 1 ) {
      //   this.enable = true;
      //   this.qCategory = catData;
      //   this.enableDisableModal = true;
      // } else {
      //   this.enable = false;
      //   this.qCategory = catData;
      //   this.enableDisableModal = true;
  
      // }

      this.clickTodisable(catData);
  }
  enableDisableCategoryAction(actionType) {
    if (actionType == true) {
  
      if (this.qCategory.visible == 1 ) {
        // this.qCategory.visible = 0;
        var catData = this.qCategory;
        this.clickTodisable(catData);
      } else {
        // this.qCategory.visible = 1;
        var catData = this.qCategory;
        this.clickTodisable(catData);
      }
    } else {
      this.closeEnableDisableModal();
    }
  }
  closeEnableDisableModal() {
    this.enableDisableModal = false;
  }



  getallTagList(id,category) {
    this.tagList=[];
    this.tempTags=[]
    // this.spinner.show();
    let url = webApi.domain + webApi.url.getAllTagsComan;
    let param = {
      tenantId : this.loginUserdata.data.data.tenantId,
    };
    this.webApiService.getService(url, param)
      .then(rescompData => {
        // this.loader =false;
        this.spinner.hide();
        var temp: any = rescompData;
        this.tagList = temp.data;
        this.tempTags = this.tagList;
        this.makeCategoryDataReady(id, category);
        // console.log('Visibility Dropdown',this.visibility);
      },
        resUserError => {
          // this.loader =false;
          this.show =false;
          this.spinner.hide();
          this.errorMsg = resUserError;
        });
  }
  
  show:boolean =false;
  makeCategoryDataReady(id,category){
    this.selectedTags=[];
    // this.spinner.show()
        if(id==1){
          // this.title = 'Edit Category';
          // this.header['title'] = this.title
          this.formdata={
            id: category.id,
            cname: category.catName,
            cdesc: category.description,
            ctags:category.tags=='' ? category.tags:category.tags.split(',') ,
            picRef:category.picRef ? category.picRef : this.defaultThumb,
            visible : category.visible,
            tenantId :category.tenantId,
          }
          
          if(category.tagIds)
          {
            var tagIds =category.tagIds.split(',');
            if(tagIds.length > 0){
              this.tempTags.forEach((tag) => {
                tagIds.forEach((tagId)=>{
                  if (tag.id == tagId ) {
                    this.selectedTags.push(tag);
                  }
                });
              });
              }

          }
          this.sidebarForm = true
          console.log(this.formdata)
        }else{
          // this.title = 'Add Category';
          // this.header['title'] = this.title
          this.formdata={
            id: 0,
            cname: '',
            cdesc: '',
            ctags:'',
            picRef:this.defaultThumb,
            visible : 1,
            tenantId :  this.loginUserdata.data.data.tenantId,
          }
        }

        // this.selectedTags = []
        this.show =true;
        this.spinner.hide()
      }
    
 


  presentToast(type, body) {
    if(type === 'success'){
      this.toastr.success(body, 'Success', {
        closeButton: false
       });
    } else if(type === 'error'){
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else{
      this.toastr.warning(body, 'Warning', {
        closeButton: false
        })
    }
  }

  // readUrl(event:any) {
  //    if (event.target.files && event.target.files[0]) {
  //            var reader = new FileReader();

  //            reader.onload = (event: ProgressEvent) => {
  //           this.profileUrl = (<FileReader>event.target).result;
  //        }
  //       reader.readAsDataURL(event.target.files[0]);
  //    }
  // }

    categoryImgData:any;
  readCategoryThumb(event:any) {
    var validExts = new Array(".png", ".jpg", ".jpeg");
    var fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if(validExts.indexOf(fileExt) < 0){
        // var toast : Toast = {
        //     type: 'error',
        //     title: "Invalid file selected!",
        //     body: "Valid files are of " + validExts.toString() + " types.",
        //     showCloseButton: true,
        //     // tapToDismiss: false, 
        //     timeout: 4000
            // onHideCallback: () => {
            //     this.router.navigate(['/pages/plan/users']);
            // }
        // };
        // this.toasterService.pop(toast);
      this.presentToast('warning', 'Valid file types are ' + validExts.toString());
        // this.deleteCourseThumb();
    }else{
      if (event.target.files && event.target.files[0]) {
              this.categoryImgData = event.target.files[0];

              var reader = new FileReader();

              reader.onload = (event: ProgressEvent) => {
              // this.defaultThumb = (<FileReader>event.target).result;
              this.formdata.picRef = (<FileReader>event.target).result;
          }
          reader.readAsDataURL(event.target.files[0]);
      }
    }
  }

    deleteCategoryThumb() {
      // this.defaultThumb = 'assets/images/category.jpg';
      this.formdata.picRef = 'assets/images/category.jpg';
      this.categoryImgData = undefined;
      this.formdata.categoryPicRefs =undefined;
  }


  // deleteProfile() {
  //     this.profileUrl = 'assets/images/category.jpg';
  // }
  
fileUploadRes:any;
  categoryAddEditRes:any;
  
  addUpdateCategory(url,category){
    this.spinner.show();
    this.webApiService.getService(url,category)
        .then(rescompData => { 
          this.spinner.hide();
          var temp:any = rescompData;
          this.categoryAddEditRes = temp.data;
          if(temp == "err"){
            // this.notFound = true;
            // var catUpdate : Toast = {
            //     type: 'error',
            //     title: "Category",
            //     body: "Unable to update category.",
            //     showCloseButton: true,
            //     timeout: 4000
            // };
            // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
          }else if(temp.type == false){
            // var catUpdate : Toast = {
            //     type: 'error',
            //     title: "Category",
            //     body: this.categoryAddEditRes.msg,
            //     showCloseButton: true,
            //     timeout: 4000
            // };
            // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
          }else{
            this.sidebarForm = false;
            this.selectedTags = []
            this.formdata={
              id: 0,
              cname: '',
              cdesc: '',
              ctags:'',
              picRef:this.defaultThumb,
              visible : 1,
              tenantId :  this.loginUserdata.data.data.tenantId,
            }

            this.getCategories(this.str, this.currentPage);
            // this.router.navigate(['/pages/assessment/qcategory']);
            // var catUpdate : Toast = {
            //     type: 'success',
            //     title: "Category",
            //     // body: "Unable to update category.",
            //     body: this.categoryAddEditRes.msg,
            //     showCloseButton: true,
            //     timeout: 4000
            // };
            // this.toasterService.pop(catUpdate);
          this.presentToast('success', this.categoryAddEditRes.msg);
          }
          console.log('Category AddEdit Result ',this.categoryAddEditRes)
        },
        resUserError=>{
                this.spinner.hide();
          this.loader =false;
          this.errorMsg = resUserError;
        });
  }
  makeTagDataReady(tagsData) {
    this.formdata.tags  =''
     tagsData.forEach((tag)=>{
      if(this.formdata.tags  == '')
      {
        this.formdata.tags  = tag.id;
      }else
      {
        this.formdata.tags = this.formdata.tags +'|' + tag.id;
      }
      console.log('this.formdata.tags',this.formdata.tags);
     });
    // var tagsData = this.formdata.tags;
    // var tagsString = '';
    // if (tagsData) {
    //   for (let i = 0; i < tagsData.length; i++) {
    //     var tag = tagsData[i];
    //     if (tagsString != "") {
    //       tagsString += "|";
    //     }
    //     if (String(tag) != "" && String(tag) != "null") {
    //       tagsString += tag;
    //     }
    //   }
    //   this.formdata.tags1 = tagsString;
    // }
  }

  submit(f){
    if(f.valid){
      this.spinner.show();
      if (this.selectedTags.length > 0) {
        this.makeTagDataReady(this.selectedTags);
         // this.formdata.tags = this.formattedTags;
       }
      var category={
        categoryId : this.formdata.id,
        categoryName : this.formdata.cname,
        categoryCode :'',
        description : this.formdata.cdesc,
        // categoryPicRef : this.categoryImgData == undefined ? null : this.formdata.categoryPicRef,
        categoryPicRef : this.formdata.picRef,
        visible : this.formdata.visible,
        tenantId : this.formdata.tenantId,
        tags : this.formdata.tags ,
        userId:this.loginUserdata.data.data.id,
        // tagsList : this.formdata.tags,
      }
  
  console.log('category',category)
      var fd = new FormData();
      fd.append('content',JSON.stringify(category));
      fd.append('file',this.categoryImgData);
      console.log('File Data ',fd);
  
      console.log('Category Data Img',this.categoryImgData);
      console.log('Category Data ',category);
      
      let url = webApi.domain + webApi.url.addEditquestionCategory;
      let fileUploadUrl = webApi.domain + webApi.url.fileUpload;
      // let fileUploadUrlLocal = 'http://localhost:9845' + webApi.url.fileUpload;
      let param = {
        tId: 1
      } 
  
      if(this.categoryImgData != undefined){
        this.webApiService.getService(fileUploadUrl,fd)
          .then(rescompData => { 
            this.spinner.hide();
            var temp:any = rescompData;
            this.fileUploadRes = temp;
            if(temp == "err"){
              
          this.presentToast('error', '');
            }else if(temp.type == false){
              
              this.presentToast('error', '');
            }
            else{
              if(this.fileUploadRes.data != null || this.fileUploadRes.fileError != true){
                category.categoryPicRef = this.fileUploadRes.data.file_url;
                this.addUpdateCategory(url,category);
              }else{
               
                 this.presentToast('error', '');
              }
            }
            console.log('File Upload Result',this.fileUploadRes)
          },
          resUserError=>{
                  this.spinner.hide();
            this.loader =false;
            this.errorMsg = resUserError;
          });
      }else{
        this.addUpdateCategory(url,category);
      }
    }  else{
      console.log('Please Fill all fields');
      // const addEditF: Toast = {
      //   type: 'error',
      //   title: 'Unable to update',
      //   body: 'Please Fill all fields',
      //   showCloseButton: true,
      //   timeout: 2000
      // };
      // this.toasterService.pop(addEditF);
      this.presentToast('warning', 'Please fill in the required fields');
      Object.keys( f.controls).forEach(key => {
        f.controls[key].markAsDirty();
        });
    }
}
   // Tag cganges

   onTagsSelect(item: any) {
    console.log(item);
    console.log(this.selectedTags);
  }
  OnTagDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedTags);
  }
  onTagSearch(evt: any) {
    console.log(evt.target.value);
    const val = evt.target.value;
    this.tagList = [];
    const temp = this.tempTags.filter(function(d) {
      return (
        String(d.name)
          .toLowerCase()
          .indexOf(val) !== -1 ||
        !val
      );
    });
  
    // update the rows
    this.tagList = temp;
    console.log('filtered Tag LIst',this.tagList);
  }
  

       // Help Code Start Here //

       
       getHelpContent() {
         return new Promise(resolve => {
           this.http1
             .get("../../../../../../assets/help-content/addEditCourseContent.json")
             .subscribe(
               data => {
                 this.helpContent = data;
                 console.log("Help Array", this.helpContent);
               },
               err => {
                 resolve("err");
               }
             );
         });
         // return this.helpContent;
       }
     
       // Help Code Ends Here //

  
}




