// import { routing }       from './addEditUser.routing';
import { addEditquestion } from './addEditquestion.component';
import { addEditquestionService } from './addEditquestion.service';


import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NbTabsetModule } from '@nebular/theme';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { NgxSpinnerModule } from "ngx-spinner";
import { ComponentModule } from '../../../../component/component.module';
//import { AgGridModule } from 'ag-grid-angular';
// import 'ag-grid-enterprise';
import 'ag-grid-community';

import { MyDatePickerModule } from 'mydatepicker';
// import { TruncateModule } from 'ng2-truncate';
// import { TabsModule } from 'ngx-tabs';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { QuillModule } from 'ngx-quill'
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import { TagInputModule } from 'ngx-chips';
import { SortablejsModule } from 'angular-sortablejs';

import { JoditAngularModule } from 'jodit-angular';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';


@NgModule({
  imports: [
    CommonModule,
    // routing,
    FormsModule,
    NgxSkeletonLoaderModule,
    // AgGridModule.withComponents([]),
    // TabsModule,
    ReactiveFormsModule,
    Ng2SmartTableModule,
    NbTabsetModule,
    QuillModule,
    TagInputModule,
    SortablejsModule,
    JoditAngularModule,
    AngularMultiSelectModule,
    NgxSpinnerModule,
    ComponentModule
  ],
  declarations: [
    addEditquestion,
    // ChartistJs
  ],
  providers: [
    addEditquestionService,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA,
  ],
})
export class addEditquestionModule { }
