import {
  Component,
  Directive,
  ChangeDetectorRef,
  ViewEncapsulation,
  forwardRef,
  Attribute,
  OnChanges,
  SimpleChanges,
  Input,
  ViewChild,
  ViewContainerRef,
  ElementRef
} from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { addEditquestionService } from "./addEditquestion.service";
import {
  NG_VALIDATORS,
  Validator,
  Validators,
  AbstractControl,
  ValidatorFn
} from "@angular/forms";
import { LocalDataSource } from "ng2-smart-table";
import { Router, NavigationStart, ActivatedRoute } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
// import { ToasterModule, ToasterService, Toast } from "angular2-toaster";
import { webAPIService } from "../../../../service/webAPIService";
import { webApi } from "../../../../service/webApi";
import { SortablejsOptions } from "angular-sortablejs";
import { NgxSpinnerService } from "ngx-spinner";
import { DataSeparatorService } from '../../../../service/data-separator.service';
import { SuubHeader } from "../../../components/models/subheader.model";

@Component({
  selector: "addEditquestion",
  templateUrl: "./addEditquestion.html",
  styleUrls: ["./addEditquestion.scss"],
  encapsulation: ViewEncapsulation.None
})
export class addEditquestion {
  @ViewChild("fileUpload") fileUpload: any;
  @ViewChild("multupload") multupload: any;

  query: string = "";
  public getData;
  QuestionListOptions: SortablejsOptions = {
    group: {
      name: "sequence.soption"
    },
    sort: true,
    // draggable: '.draggable',
    // onEnd: this.onListItemDragEnd.bind(this),
    handle: ".drag-handle"
    // draggable: '.draggable'
  };
  header: SuubHeader  = {
    title: '',
    btnsSearch: true,
    btnName1show: false,
    btnName2show: false,
    showBreadcrumb:true,
    breadCrumbList:[]
  };
  mutimgarr: any = [];
  multTemparr: any = [];
  target: any;
  title: any;
  formdata: any;
  profileUrl: any = "assets/images/cat3.png";
  result: any;
  errorMsg: any;

  fileUrl: any;
  fileName: any = "You can drag and drop files here to add them.";
  fileIcon: any = "assets/img/app/profile/avatar4.png";
  enableUpload: any = false;
  fileReaded: any;
  matching: any = {
    moption: [
      {
        qtypeid: 1,
        optionText: "",
        optionRef: "",
        optionMatch: "",
        mpoint: "",
        mipoint: 0,
        perct: 0
      }
    ]
  };
  sequence: any = {
    soption: [
      {
        sanswer: "",
        sindex: "",
        spoint: "",
        sipoint: 0,
        perct: 0
      }
    ]
  };
  multiple: any = {
    mvalue: "1",
    mshuffle: "",
    moption: [
      {
        manswer: "",
        mpoint: "",
        mipoint: 0,
        manfeed: "",
        mopvalue: false,
        qtypeid: 1,
        optionRef: "",
        qfileName: "No File Chosen",
        perct: 0
      }
    ]
  };

  short: any = {
    min: "",
    max: "",
    score: null,
    penelty: null,
    feedback: "",
    visible: 1,
  };
  parentcat: any = [
    {
      id: 1,
      name: "category 1"
    },
    {
      id: 2,
      name: "category 2"
    },
    {
      id: 3,
      name: "category 3"
    },
    {
      id: 4,
      name: "category 4"
    },
    {
      id: 5,
      name: "category 5"
    },
    {
      id: 6,
      name: "category 6"
    }
    // {
    //   id:7,
    //   name:'category 7'
    // },
    // {
    //   id:8,
    //   name:'category 8'
    // },
  ];

  selectedActivityType: any = {
    id: "",
    name: ""
  };

  visibility: any = [
    {
      id: 1,
      name: "Show"
    },
    {
      id: 2,
      name: "Hide"
    }
  ];

  level: any = [
    {
      id: 1,
      name: "Beginner"
    },
    {
      id: 2,
      name: "Intermediate"
    },
    {
      id: 3,
      name: "Advanced"
    }
  ];

  basic: boolean = true;
  type: boolean = false;
  loader: any;
  tempname: any;
  catData: any = undefined;
  tempCheckedIndex: any = 0;
  qbtype: any;
  qblevel: any;
  qbformat: any;
  qbcategory: any;
  catID: any;
  formatID: any;
  catAddId: any;
  userLoginData: any;
  userdata: any;
  config = {
    height: '200px',
    uploader: {
      insertImageAsBase64URI: true,
    },
    allowResizeX: false,
    allowResizeY: false,
    placeholder: 'Maximum 3000 characters',
    limitChars: 3000,
  };
  config1 = {
    height: '200px',
    uploader: {
      insertImageAsBase64URI: true,
    },
    allowResizeX: false,
    allowResizeY: false,
    placeholder: 'Maximum 3000 charecters',
    limitChars: 3000,
  };
  config2 = {
    height: '200px',
    minHeight: 200,
    uploader: {
      insertImageAsBase64URI: true,
    },
    allowResizeX: false,
    allowResizeY: false,
    placeholder: 'Answer',
    limitChars: 3000,
  };
  config3 = {
    height: '200px',
    minHeight: 200,
    uploader: {
      insertImageAsBase64URI: true,
    },
    allowResizeX: false,
    allowResizeY: false,
    placeholder: 'Feedback',
    limitChars: 3000,
  };

  settingsTagDrop = {};
  tempTags: any = [];
  tagList: any = [];
  selectedTags: any = [];
  typeId: any;
  constructor(
    public cdf: ChangeDetectorRef,
    private spinner: NgxSpinnerService,
    protected service: addEditquestionService,
    protected webApiService: webAPIService,
    // private toasterService: ToasterService,
    private router: Router,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private http1: HttpClient,
    private dataSeparatorService: DataSeparatorService,
  ) {
    this.spinner.show();
    this.getHelpContent();
   // this.spinner.hide();
    // console.log("Add Category Called");
    var categoryque;
    var id;
    if (localStorage.getItem("LoginResData")) {
      this.userLoginData = JSON.parse(localStorage.getItem("LoginResData"));
      this.userdata = this.userLoginData.data.data;
      console.log("login data", this.userdata);
    }
    this.settingsTagDrop = {
      text: 'Select Tags ',
      singleSelection: false,
      classes: 'myclass custom-class smallHeight',
      primaryKey: 'id',
      labelKey: 'name',
      noDataLabel: 'Search Tags...',
      enableSearchFilter: true,
      lazyLoading: true,
      searchBy: ['name'],
      badgeShowLimit: 5,
      maxHeight:250,
    };
    var catdata = {
      tId: this.userdata.tenantId
    };

    this.spinner.show();
    this.service.getqbdropdowun(catdata).then(
      rescompData => {
        this.spinner.hide();
        this.qbtype = rescompData['data'].qbtype;
        this.qblevel = rescompData['data'].qblevel;
        this.qbformat = rescompData['data'].qbformat;
        this.qbcategory = rescompData['data'].qbcategory;
        console.log("QB dropdown Result", rescompData['data']);
        
        
        //   console.log('this.qbtype',this.qbtype);
        //     console.log('this.qblevel',this.qblevel);
        //       console.log('this.qbformat',this.qbformat);
        //         console.log('this.qbcategory',this.qbcategory);
      },
      resUserError => {
        this.spinner.hide();
        this.errorMsg = resUserError;
        // this.notFound = true;
        // this.router.navigate(['**']);
      }
    );

    if (this.service.data != undefined) {
      categoryque = this.service.data.data;
      id = this.service.data.id;
      this.typeId=id;
      this.catID = this.service.data.data.cid;
      this.catData = this.service.data.data;
      this.catAddId = this.service.data.cdata;
      this.formatID = categoryque.qFormat;
    } else {
      id = 0;
    }
    this.typeId=this.service.data.id
    console.log("Service Data:", this.service.data);
    console.log("categoryque Info", categoryque);
    console.log("id", id);
    this.getallTagList(id, categoryque);

  }

  ngOnInit(){
  this.spinner.show()
  }
  
  getallTagList(id, categoryque) {
    this.spinner.show();
    let url = webApi.domain + webApi.url.getAllTagsComan;
    let param = {
      tenantId: this.userdata.tenantId
    };
    this.webApiService.getService(url, param)
      .then(rescompData => {
        // this.loader =false;
        this.spinner.hide();
        var temp: any = rescompData;
        this.tagList = temp.data;
        this.tempTags = [... this.tagList];
        this.makeCategoryDataReady(id, categoryque);
        // console.log('Visibility Dropdown',this.visibility);
      },
        resUserError => {
          // this.loader =false;
          this.show = false;
          this.spinner.hide();
          this.errorMsg = resUserError;
        });
  }

  show: boolean = false;
  makeCategoryDataReady(id, categoryque) {
    this.spinner.show();
      var tagIds = [];
      if (id == 1) {
        this.title = "Edit Question";
        this.formdata = {
          cid: categoryque.cid,
          cname: categoryque.catName,
          qinsrtu: categoryque.instructions,
          qid: categoryque.qid,
          qname: categoryque.qName,
          qfeed: categoryque.feedback,
          qtext: categoryque.questionText,
          qFormat: categoryque.qFormat,
          qcontent: categoryque.contentRef,
          qtypeid: categoryque.qTypeid,
          qlevelid: categoryque.qLevelid,
          qvisible: categoryque.visible,
          qtenantId: categoryque.tenantId,
          qtag:
            categoryque.tags == ""
              ? categoryque.tags
              : categoryque.tags.split(","),
          qfileName: categoryque.contentRef
            ? categoryque.contentRef.substring(
              categoryque.contentRef.lastIndexOf("/") + 1
            )
            : categoryque.contentRef
        };
        if (categoryque.tagIds) {
          tagIds = categoryque.tagIds.split(',');
          if (tagIds.length > 0) {
            this.tempTags.forEach((tag) => {
              tagIds.forEach((tagId) => {
                if (tag.id == tagId) {
                  this.selectedTags.push(tag);
                }
              });
            });
          }
        }
        this.getAllBAsicData();

       } else if (id == 2) {
          this.title = "Copy Question";
          this.formdata = {
            cid: categoryque.cid,
            cname: categoryque.catName,
            qinsrtu: categoryque.instructions,
            qid:categoryque.qid,
            qname: categoryque.qName,
            qfeed: categoryque.feedback,
            qtext: categoryque.questionText,
            qFormat: categoryque.qFormat,
            qcontent: categoryque.contentRef,
            qtypeid: categoryque.qTypeid,
            qlevelid: categoryque.qLevelid,
            qvisible: categoryque.visible,
            qtenantId: categoryque.tenantId,
            qtag:
              categoryque.tags == ""
                ? categoryque.tags
                : categoryque.tags.split(","),
            qfileName: categoryque.contentRef
              ? categoryque.contentRef.substring(
                categoryque.contentRef.lastIndexOf("/") + 1
              )
              : categoryque.contentRef
          };
          tagIds = categoryque.tagIds.split(',');
          if (tagIds.length > 0) {
            this.tempTags.forEach((tag) => {
              tagIds.forEach((tagId) => {
                if (tag.id == tagId) {
                  this.selectedTags.push(tag);
                }
              });
            });
          }
          this.getAllBAsicData();
        }

       else {
        this.title = "Add Question";
        this.formdata = {
          cid: this.catAddId ? this.catAddId : "",
          cname: "",
          qinsrtu: "",
          qid: 0,
          qname: "",
          qfeed: "",
          qtext: "",
          qFormat: "",
          qcontent: "",
          qtypeid: "",
          qlevelid: "",
          qvisible: 1,
          qtenantId: this.userdata.tenantId,
          qfileName: "You can drag and drop files here to add them."
        };

        this.short = {
          min: "",
          max: "",
          score: null,
          penelty: null,
          visible: 1,
        };

        this.multiple = {
          mvalue: "1",
          moption: [
            {
              manswer: "",
              mpoint: "",
              mipoint: 0,
              manfeed: "",
              mopvalue: false,
              qtypeid: 1,
              optionRef: "",
              qfileName: "No File Chosen",
              perct: 0
            }
          ]
        };
        this.sequence = {
          soption: [
            {
              sanswer: "",
              sindex: "",
              spoint: "",
              sipoint: 0,
              perct: 0
            }
          ]
        };
        this.matching = {
          moption: [
            {
              qtypeid: 1,
              optionText: "",
              optionRef: "",
              optionMatch: "",
              mpoint: "",
              mipoint: 0,
              qfileName: "No File Chosen",
              perct: 0
            }
          ]
        };

      }
      this.show = true;

      for(let i=0;i < this.qbcategory.length; i++) {
        if(this.qbcategory[i].id  == this.formdata.cid){
            this.tempname = this.qbcategory[i].name
        }

        console.log('this.tempname===================================>',this.tempname)
      }
      this.spinner.hide();

  }
getAllBAsicData(){
  if (this.formdata.qFormat == 1) {
    var mulData = {
      qId: this.formdata.qid,
      tId: this.userdata.tenantId
    };
    this.spinner.show();
    this.service.getmultipleData(mulData).then(
      rescompData => {
        this.spinner.hide();
        var multipleData = rescompData['data'][0] ? rescompData['data'][0] : [];
        console.log(" this.multiple", this.multiple);
        if (multipleData.length > 0) {
          var mvaluetemp = multipleData[0].choiceType;
          for (let i = 0; i < multipleData.length; i++) {
            delete multipleData[i].choiceType;
            delete multipleData[i].id;
            delete multipleData[i].questionId;
            delete multipleData[i].isShuffle;
            multipleData[i].qtypeid = Number(multipleData[i].qtypeid);
            if (mvaluetemp == 1) {
              multipleData[i].mopvalue = multipleData[i].mopvalue;
            } else {
              multipleData[i].mopvalue =
                multipleData[i].mopvalue == 1 ? true : false;
            }
            multipleData[i].qfileName = multipleData[i].optionRef
              ? multipleData[i].optionRef.substring(
                multipleData[i].optionRef.lastIndexOf("/") + 1
              )
              : "No file Choosen";
            // multipleData[i].Content=multipleData[i].optionRef?multipleData[i].optionRef.substring(multipleData[i].optionRef.lastIndexOf("/")+1):'No file Choosen';
          }

          this.multiple = {
            mvalue: String(mvaluetemp),
            moption: multipleData
          };
        } else {
          this.multiple = {
            mvalue: "1",
            moption: [
              {
                manswer: "",
                mpoint: "",
                mipoint: 0,
                manfeed: "",
                mopvalue: false,
                qtypeid: 1,
                optionRef: "",
                qfileName: "No File Chosen",
                perct: 0
              }
            ]
          };
        }

        this.selectedActInd = 0;
        this.activeTypeName = "Choice";
        this.header={ 
          title: this.title,
         btnsSearch: true,
         btnName2: 'Save',
         btnName2show: true,
         btnBackshow: true,
         showBreadcrumb:true,
         breadCrumbList:[
           {
             'name': 'Assessment',
             'navigationPath': '/pages/assessment',
           },
           {
           'name': 'Question Bank',
           'navigationPath': '/pages/assessment/qcategory',
         },
         {
          // 'name': this.formdata.cname,
          'name': this.tempname,

          'navigationPath': '/pages/assessment/qcategory/questions',
         }
       ]
       };
        //   console.log('this.qbtype',this.qbtype);
        //     console.log('this.qblevel',this.qblevel);
        //       console.log('this.qbformat',this.qbformat);
        //         console.log('this.qbcategory',this.qbcategory);
      },
      resUserError => {
        this.spinner.hide();
        this.errorMsg = resUserError;
        // this.notFound = true;
        // this.router.navigate(['**']);
      }
    );
  }
  if (this.formdata.qFormat == 2) {
    var mulData = {
      qId: this.formdata.qid,
      tId: this.userdata.tenantId,
    };
    this.spinner.show();
    this.service.getMatchingData(mulData).then(
      rescompData => {
        this.spinner.hide();
        var matchingData = rescompData['data'][0] ? rescompData['data'][0] : [];
        console.log(" this.multiple", this.multiple);
        if (matchingData.length > 0) {
          for (let i = 0; i < matchingData.length; i++) {
            delete matchingData[i].id;
            delete matchingData[i].questionId;

            matchingData[i].qfileName = matchingData[i].optionRef
              ? matchingData[i].optionRef.substring(
                matchingData[i].optionRef.lastIndexOf("/") + 1
              )
              : "No file Choosen";
            // multipleData[i].Content=multipleData[i].optionRef?multipleData[i].optionRef.substring(multipleData[i].optionRef.lastIndexOf("/")+1):'No file Choosen';
          }
          console.log("matchingData===========>",matchingData)

          this.matching = {
            moption: matchingData
          };
        } else {
          this.matching = {
            moption: [
              {
                qtypeid: 1,
                optionText: "",
                optionRef: "",
                optionMatch: "",
                mpoint: "",
                mipoint: 0,
                qfileName: "No File Chosen",
                perct: 0
              }
            ]
          };
        }

        this.selectedActInd = 1;
        this.activeTypeName = "Matching";
        this.header={ 
          title: this.title,
         btnsSearch: true,
         btnName2: 'Save',
         btnName2show: true,
         btnBackshow: true,
         showBreadcrumb:true,
         breadCrumbList:[
           {
             'name': 'Assessment',
             'navigationPath': '/pages/assessment',
           },
           {
           'name': 'Question Bank',
           'navigationPath': '/pages/assessment/qcategory',
         },
         {
          //  'name': this.formdata.cname,
          'name': this.tempname,
           'navigationPath': '/pages/assessment/qcategory/questions',
         }
       ]
       };
        //   console.log('this.qbtype',this.qbtype);
        //     console.log('this.qblevel',this.qblevel);
        //       console.log('this.qbformat',this.qbformat);
        //         console.log('this.qbcategory',this.qbcategory);
      },
      resUserError => {
        this.spinner.hide();
        this.errorMsg = resUserError;
        // this.notFound = true;
        // this.router.navigate(['**']);
      }
    );
  }

  if (this.formdata.qFormat == 3) {
    var mulData = {
      qId: this.formdata.qid,
      tId: this.userdata.tenantId,
    };
    this.spinner.show();
    this.service.getsequenceData(mulData).then(
      rescompData => {
        this.spinner.hide();
        var SequenceData = rescompData['data'][0] ? rescompData['data'][0] : [];
        console.log(" this.multiple", this.multiple);
        if (SequenceData.length > 0) {
          var mvaluetemp = SequenceData[0].choiceType;
          for (let i = 0; i < SequenceData.length; i++) {
            delete SequenceData[i].id;
            delete SequenceData[i].questionId;
          }

          this.sequence = {
            soption: SequenceData
          };
        } else {
          this.sequence = {
            soption: [
              {
                sanswer: "",
                sindex: "",
                spoint: "",
                sipoint: 0,
                perct: 0
              }
            ]
          };
        }

        this.selectedActInd = 2;
        this.activeTypeName = "Sequence";
        this.header={ 
          title: this.title,
         btnsSearch: true,
         btnName2: 'Save',
         btnName2show: true,
         btnBackshow: true,
         showBreadcrumb:true,
         breadCrumbList:[
           {
             'name': 'Assessment',
             'navigationPath': '/pages/assessment',
           },
           {
           'name': 'Question Bank',
           'navigationPath': '/pages/assessment/qcategory',
         },
         {
          //  'name': this.formdata.cname,
          'name': this.tempname,
           'navigationPath': '/pages/assessment/qcategory/questions',
         }
       ]
       };
        //   console.log('this.qbtype',this.qbtype);
        //     console.log('this.qblevel',this.qblevel);
        //       console.log('this.qbformat',this.qbformat);
        //         console.log('this.qbcategory',this.qbcategory);
      },
      resUserError => {
        this.spinner.hide();
        this.errorMsg = resUserError;
        // this.notFound = true;
        // this.router.navigate(['**']);
      }
    );
  }
  if (this.formdata.qFormat == 4) {
    var mulData = {
      qId: this.formdata.qid,
      tId: this.userdata.tenantId
    };
    this.spinner.show();
    this.service.getshortData(mulData).then(
      rescompData => {
        this.spinner.hide();
        var shortData = rescompData['data'][0] ? rescompData['data'][0] : [];
        console.log(" this.multiple", shortData);
        if (shortData.length > 0) {
          // var mvaluetemp = shortData[0].isCase;
          // for (let i = 0; i < shortData.length; i++) {
          //   delete shortData[i].id;
          //   delete shortData[i].questionId;
          // }
          var sData = shortData[0];
          this.short = {
            min: sData.min,
            max: sData.max,
            score: null,
            penelty: null,
          };
        } else {
          this.short = {
            min: "",
            max: "",
            score: null,
            penelty: null,
          };
        }

        this.selectedActInd = 3;
        this.header={ 
          title: this.title,
         btnsSearch: true,
         btnName2: 'Save',
         btnName2show: true,
         btnBackshow: true,
         showBreadcrumb:true,
         breadCrumbList:[
           {
             'name': 'Assessment',
             'navigationPath': '/pages/assessment',
           },
           {
           'name': 'Question Bank',
           'navigationPath': '/pages/assessment/qcategory',
         },
         {
          //  'name': this.formdata.cname,
          'name': this.tempname,
           'navigationPath': '/pages/assessment/qcategory/questions',
         }
       ]
       };
        this.activeTypeName = "Short";
        //   console.log('this.qbtype',this.qbtype);
        //     console.log('this.qblevel',this.qblevel);
        //       console.log('this.qbformat',this.qbformat);
        //         console.log('this.qbcategory',this.qbcategory);
      },
      resUserError => {
        this.spinner.hide();
        this.errorMsg = resUserError;
        // this.notFound = true;
        // this.router.navigate(['**']);
      }
    );
  }
}
  backtomain() {
    this.router.navigate(["/pages/assessment/qcategory"]);
  }
  back() {
    this.router.navigate(["/pages/assessment/qcategory/questions"]);
  }

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }

  categoryImgData: any;
  readquestion(event: any) {
    var size = 10000000;
    this.formdata.qfileType = event.target.files[0].type;
    var validExts = new Array("image", "video", "audio");
    var fileExt = event.target.files[0].type;
    fileExt = fileExt.substring(0, 5);
    // var fileExt = event.target.files[0].name;
    // fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (size <= event.target.files[0].size) {
      // var toast: Toast = {
      //   type: "error",
      //   title: "file size exceeded!",
      //   body: "files size should be less than 10MB",
      //   showCloseButton: true,
      //   // tapToDismiss: false,
      //   timeout: 4000
      //   // onHideCallback: () => {
      //   //     this.router.navigate(['/pages/plan/users']);
      //   // }
      // };
      // this.toasterService.pop(toast);
      this.presentToast('warning', 'File size should be less than 10MB');
      // this.deleteCourseThumb();
    } else {
      if (validExts.indexOf(fileExt) < 0) {
        // var toast: Toast = {
        //   type: "error",
        //   title: "Invalid file selected!",
        //   body: "Valid files are of " + validExts.toString() + " types.",
        //   showCloseButton: true,
        //   // tapToDismiss: false,
        //   timeout: 4000
        //   // onHideCallback: () => {
        //   //     this.router.navigate(['/pages/plan/users']);
        //   // }
        // };
        // this.toasterService.pop(toast);
        this.presentToast('warning', 'Valid file types are ' + validExts.toString());
      } else if (event.target.files && event.target.files[0]) {
        this.categoryImgData = event.target.files[0];
        this.formdata.qfileName = event.target.files[0].name;
        var reader = new FileReader();

        reader.onload = (event: ProgressEvent) => {
          // this.defaultThumb = (<FileReader>event.target).result;
          this.formdata.contentRef = (<FileReader>event.target).result;
          // this.formdata.filename=event.target.files[0].name
        };
        reader.readAsDataURL(event.target.files[0]);
      }
    }
  }

  cancel() {
    // this.fileUpload.nativeElement.files = [];
    console.log(this.fileUpload.nativeElement.files);
    this.fileUpload.nativeElement.value = "";
    console.log(this.fileUpload.nativeElement.files);
    this.formdata.qfileName = "You can drag and drop files here to add them.";
    this.enableUpload = false;
  }

  // deleteProfile() {
  //     this.profileUrl = 'assets/images/cat3.png';
  // }

  activeItemId: any;
  activeItemName: any;
  activeItem: any;
  activeIndex: any;
  activeActTypeId: any;
  activeTypeIndex: any;
  activeType: any;
  activeTypeId: any = 1;
  activeTypeName: any = "Choice";

  setActiveItem(currentIndex, currentItem) {
    this.activeIndex = currentIndex;
    this.activeItem = currentItem;
    this.activeItemId = currentItem.id;
    this.activeItemName = currentItem.name;
  }

  setActiveType(currentIndex, currentItem) {
    this.activeTypeIndex = currentIndex;
    this.activeType = currentItem;
    this.activeTypeId = currentItem.id;
    this.activeTypeName = currentItem.name;
  }

  multipleCheck(ind) {
    // for(let i=0;i<this.multiple.moption.length;i++)
    // {
    //     if(ind == i)
    //     {
    //       this.multiple.moption[i].mopvalue =!this.multiple.moption[i].mopvalue;
    //     }
    // }
  }

  qtypechange() {
    this.formdata.qtext = "";
    this.formdata.content = "";
    this.formdata.qfileName = "You can drag and drop files here to add them.";
  }

  @ViewChild("activityForm") activityForm: ElementRef;
  selectedActInd: any = 0;

  activateActivityTab(event, activity, currentIndex) {
    console.log(currentIndex);
    this.activeTypeName = activity.name;
    this.selectedActInd = currentIndex;
    console.log(this.selectedActInd);
  }

  fileUploadRes: any;
  categoryqueAddEditRes: any;
  addUpdateCategoryque(url, category) {
    this.spinner.show();
    this.webApiService.getService(url, category).then(
      rescompData => {
        this.spinner.hide();
        var temp: any = rescompData;
        this.categoryqueAddEditRes = temp.data;
        if (temp == "err") {
          // this.notFound = true;
          // var catUpdate: Toast = {
          //   type: "error",
          //   title: "Question",
          //   body: "Unable to update Question.",
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
        } else if (temp.type == false) {
          // var catUpdate: Toast = {
          //   type: "error",
          //   title: "Question",
          //   body: this.categoryqueAddEditRes.msg,
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
        } else {
          // this.router.navigate(['/pages/assessment/qcategory']);
          // var catUpdate: Toast = {
          //   type: "success",
          //   title: "Question",
          //   // body: "Unable to update category.",
          //   body: this.categoryqueAddEditRes.msg,
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          if (this.catID == 2) {
            if (this.formdata.qFormat == 1) {
              this.savemultImageData(this.formdata);
            } else if (this.formdata.qFormat == 2) {
              this.savematchImageData(this.formdata);
              this.savemultImageData(this.formdata);
            } else if (this.formdata.qFormat == 3) {
              this.saveSequence(this.formdata);
            } else if (this.formdata.qFormat == 4) {
              this.saveShort(this.formdata);
            }
          }
          this.basic = false;
          this.type = true;
          // this.toasterService.pop(catUpdate);
          this.presentToast('success', this.categoryqueAddEditRes.msg);
        }
        console.log("Question AddEdit Result ", this.categoryqueAddEditRes);
      },
      resUserError => {
        this.spinner.hide();
        this.errorMsg = resUserError;
      }
    );
  }

  makeTagDataReady(tagsData) {
    this.formdata.tags = ''
    tagsData.forEach((tag) => {
      if (this.formdata.tags == '') {
        this.formdata.tags = tag.id;
      } else {
        this.formdata.tags = this.formdata.tags + '|' + tag.id;
      }
      console.log('this.formdata.tags', this.formdata.tags);
    });
    // var tagsData = this.formdata.tags;
    // var tagsString = '';
    // if (tagsData) {
    //   for (let i = 0; i < tagsData.length; i++) {
    //     var tag = tagsData[i];
    //     if (tagsString != "") {
    //       tagsString += "|";
    //     }
    //     if (String(tag) != "" && String(tag) != "null") {
    //       tagsString += tag;
    //     }
    //   }
    //   this.formdata.tags1 = tagsString;
    // }
  }
  submit(f) {
    if (f.valid) {
      this.loader = true;
      if (this.selectedTags.length > 0) {
        this.makeTagDataReady(this.selectedTags);
        // this.formdata.tags = this.formattedTags;
      }
      var category = {
        qid: this.formdata.qid,
        cid: this.formdata.cid,
        qname: this.formdata.qname,
        qtypeid: this.formdata.qtypeid,
        qFormat: this.formdata.qFormat,
        qtext: this.formdata.qtext,
        qcontent: this.formdata.qcontent,
        qinsrtu: this.formdata.qinsrtu,
        qfeed: this.formdata.qfeed,
        qlevelid: this.formdata.qlevelid,
        qvisible: this.formdata.qvisible,
        qtenantId: this.formdata.qtenantId,
        qtag: this.formdata.tags,
        userId: this.userdata.id
        // tagsList : this.formdata.tags,
      };
      if (this.typeId == 2) {
        category.qid = 0;
        this.categoryImgData = undefined;
      }

      console.log("category", category);
      var fd = new FormData();
      fd.append("content", JSON.stringify(category));
      fd.append("file", this.categoryImgData);

      console.log("Question file", this.categoryImgData);
      console.log("Question Data ", category);

      let url = webApi.domain + webApi.url.addeditqbcategoryquestion;
      let fileUploadUrl = webApi.domain + webApi.url.fileUpload;
      // let fileUploadUrlLocal = 'http://localhost:9845' + webApi.url.fileUpload;
      if (this.categoryImgData != undefined) {
        this.spinner.show();
        this.webApiService.getService(fileUploadUrl, fd).then(
          rescompData => {
            this.spinner.hide();
            var temp: any = rescompData;
            this.fileUploadRes = temp;
            if (temp == "err") {
              // this.notFound = true;
              // var thumbUpload: Toast = {
              //   type: "error",
              //   title: "Question file",
              //   body: "Unable to upload Question file.",
              //   // body: temp.msg,
              //   showCloseButton: true,
              //   timeout: 4000
              // };
              // this.toasterService.pop(thumbUpload);
              this.presentToast('error', '');
            } else if (temp.type == false) {
              // var thumbUpload: Toast = {
              //   type: "error",
              //   title: "Question file",
              //   body: "Unable to upload Question file.",
              //   // body: temp.msg,
              //   showCloseButton: true,
              //   timeout: 4000
              // };
              // this.toasterService.pop(thumbUpload);
              this.presentToast('error', '');
            } else {
              if (
                this.fileUploadRes.data != null ||
                this.fileUploadRes.fileError != true
              ) {
                category.qcontent = this.fileUploadRes.data.file_url;
                this.addUpdateCategoryque(url, category);
              } else {
                // var thumbUpload: Toast = {
                //   type: "error",
                //   title: "Question file",
                //   // body: "Unable to upload category thumbnail.",
                //   body: this.fileUploadRes.status,
                //   showCloseButton: true,
                //   timeout: 4000
                // };
                // this.toasterService.pop(thumbUpload);
                this.presentToast('error', '');
              }
            }
            console.log("File Upload Result", this.fileUploadRes);
          },
          resUserError => {
            this.spinner.hide();
            this.loader = false;
            this.errorMsg = resUserError;
          }
        );
      } else {
        this.addUpdateCategoryque(url, category);
      }
    } else {
      // console.log('Please Fill all fields');
      // const addEditF: Toast = {
      //   type: 'error',
      //   title: 'Unable to update',
      //   body: 'Please Fill all fields',
      //   showCloseButton: true,
      //   timeout: 2000
      // };
      // this.toasterService.pop(addEditF);
      this.presentToast('warning', 'Please fill in the required fields');
      Object.keys(f.controls).forEach(key => {
        f.controls[key].markAsDirty();
      });
    }
  }

  /**********MULTIPLE TYPE************/

  addmulti() {
    // for(let i=0;i<this.multiple.moption.length;i++){
    //   if(this.multiple.moption[i].mipoint && this.multiple.moption[i].mpoint && this.multiple.moption[i].mpoint )
    // }

    var option = {
      manswer: "",
      mpoint: "",
      mipoint: 0,
      manfeed: "",
      mopvalue: false,
      qtypeid: 1,
      optionRef: "",
      qfileName: "No File Chosen",
      perct: 0
    };
    this.multiple.moption.push(option);
    if (this.multiple.length > 0) {
      this.multiple.moption.forEach((data, key) => {
        if (this.tempCheckedIndex == key) {
          data.mopvalue = true;
        } else {
          data.mopvalue = false;
        }
      });
    }
  }

  removemulti(id) {
    for (let i = 0; i < this.multiple.moption.length; i++) {
      if (i == id) {
        this.multiple.moption.splice(i, 1);
      }
    }
  }

  onMultiple() {
    for (let i = 0; i < this.multiple.moption.length; i++) {
      this.multiple.moption[i].mopvalue = "";
      this.multiple.moption[i].mipoint = 0;
      this.multiple.moption[i].mpoint = "";
    }
  }

  radiobtn(event, ind) {
    if(event){
      for (let i = 0; i < this.multiple.moption.length; i++) {
        if (ind != i) {
          this.multiple.moption[i].mopvalue = false;
          // this.multiple.moption[i].mipoint='';
          this.multiple.moption[i].mpoint = "";
          // this.multiple.moption[i].isSelected=0;
        } else {
          // this.multiple.moption[i].isSelected=1;
          this.multiple.moption[i].mopvalue = true;
        }
      }
    } else {
      this.multiple.moption[ind].mopvalue = false;
    }
    // this.multiple.moption.forEach((item, key)=>{
    //   item.mopvalue = false;
    // });
    // this.multiple.moption[ind].mopvalue =true;
    //    this.target == event.target.checked;
    this.tempCheckedIndex = ind;
    this.cdf.detectChanges();
  }

  choicetypechange(mid) {
    for (let i = 0; i < this.multiple.moption.length; i++) {
      if (i == mid) {
        this.multiple.moption[i].optionRef = "";
        this.multiple.moption[i].manswer = "";
        this.multiple.moption[i].qfileName = "No File Chosen";
      }
    }
  }

  readmultimg(event: any, ind) {
    var size = 100000;
    var validExts = new Array("image");
    var fileExt = event.target.files[0].type;
    fileExt = fileExt.substring(0, 5);
    if (size <= event.target.files[0].size) {
      // var toast: Toast = {
      //   type: "error",
      //   title: "file size exceeded!",
      //   body: "files size should be less than 100KB",
      //   showCloseButton: true,
      //   timeout: 4000
      // };
      // this.toasterService.pop(toast);
      this.presentToast('warning', 'File size should be less than 100KB');
      event.target.value = "";
      // this.deleteCourseThumb();
    } else {
      if (validExts.indexOf(fileExt) < 0) {
        // var toast: Toast = {
        //   type: "error",
        //   title: "Invalid file selected!",
        //   body: "Valid files are of " + validExts.toString() + " types.",
        //   showCloseButton: true,
        //   timeout: 4000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('warning', 'Valid file types are ' + validExts.toString());
      } else if (event.target.files && event.target.files[0]) {
        for (let i = 0; i < this.multiple.moption.length; i++) {
          if (i == ind) {
            this.multiple.moption[i].qfileName = event.target.files[0].name
              ? event.target.files[0].name
              : "No file Chosen";
            this.multiple.moption[i].optionRef = event.target.files[0];
          }
        }
      }
    }
  }

  cancelmulti(ind) {
    for (let i = 0; i < this.multiple.moption.length; i++) {
      if (i == ind) {
        this.multiple.moption[i].qfileName = "No File Chosen";
        this.multiple.moption[i].optionRef = "";
        this.multiple.moption[i].contentRef = undefined;
      }
    }
  }
  qbMultiple: any;
  multcontent: any = [];
  multfileUploadRes: any = [];
  multCount: any = 0;
  savemultImageData(f) {
    console.log(f);
    console.log(f.valid);
    if (f.valid) {
      this.mutimgarr = [];
      this.multTemparr = [];
      this.multcontent = [];
      var multiple = {
        qformatId: this.formdata.qFormat ? this.formdata.qFormat : 1,
        mvalue: this.multiple.mvalue,
        moption: this.multiple.moption,
        qId: this.categoryqueAddEditRes
          ? this.categoryqueAddEditRes.id
            ? this.categoryqueAddEditRes.id
            : this.formdata.qid
          : this.formdata.qid,
        userId: this.userdata.id,
        tId: this.userdata.tenantId,
        option: ""
      };

      if (multiple.qId) {
        for (let i = 0; i < multiple.moption.length; i++) {
          if (multiple.moption[i].contentRef) {
            this.mutimgarr.push(multiple.moption[i].optionRef);
            this.multTemparr.push(i);
            multiple.moption[i].name = multiple.moption[i].qfileName;
            multiple.moption[i].author = multiple.qId;
            this.multcontent.push(multiple.moption[i]);
          }
          multiple.moption[i].author = "author";
        }

        console.log("mutimgarr", this.mutimgarr);
        console.log("multTemparr", this.multTemparr);
        console.log("multcontent", this.multcontent);

        var fd = new FormData();
        fd.append("content", JSON.stringify(this.multcontent));
        for (let i = 0; i < this.mutimgarr.length; i++) {
          fd.append("file", this.mutimgarr[i]);
        }

        // console.log('Question file',this.categoryImgData);
        // console.log('Question Data ',category);

        // let url = webApi.domain + webApi.url.addeditqbcategoryquestion;
        let fileUploadUrl = webApi.domain + webApi.url.multifileUpload;
        // let fileUploadUrlLocal = 'http://localhost:9845' + webApi.url.fileUpload;
        if (this.mutimgarr.length > 0) {
          this.spinner.show();
          this.webApiService.getService(fileUploadUrl, fd).then(
            (rescompData: any) => {
              this.spinner.hide();
              var temp: any = rescompData;
              this.multfileUploadRes = rescompData;
              if (temp == "err") {
                // this.notFound = true;
                // var thumbUpload: Toast = {
                //   type: "error",
                //   title: "Question type",
                //   body: "Unable to upload Answer Images.",
                //   // body: temp.msg,
                //   showCloseButton: true,
                //   timeout: 4000
                // };
                // this.toasterService.pop(thumbUpload);
                this.presentToast('error', '');
              } else {
                if (this.multfileUploadRes.length > 0) {
                  var imageData = [];
                  for (let i = 0; i < this.multfileUploadRes.length; i++) {
                    imageData.push(this.multfileUploadRes[i]);
                  }
                  // var imgData = this.multfileUploadRes.data.file_url;
                  this.saveMultiple(multiple, imageData);
                } else {
                  // var thumbUpload: Toast = {
                  //   type: "error",
                  //   title: "Question file",
                  //   // body: "Unable to upload category thumbnail.",
                  //   body: "Unable to upload Answer Images",
                  //   showCloseButton: true,
                  //   timeout: 4000
                  // };
                  // this.toasterService.pop(thumbUpload);
                  this.presentToast('error', '');
                }
              }
              console.log("File Upload Result", this.multfileUploadRes);
            },
            resUserError => {
              this.spinner.hide();
              this.errorMsg = resUserError;
            }
          );
        } else {
          var imgData = [];
          this.saveMultiple(multiple, imgData);
        }
      } else {
        // var thumbUpload: Toast = {
        //   type: "error",
        //   title: "Add-Question",
        //   // body: "Unable to upload category thumbnail.",
        //   body: "Kindly fill Basic Datails",
        //   showCloseButton: true,
        //   timeout: 4000
        // };
        this.basic = true;
        this.type = false;
        // this.toasterService.pop(thumbUpload);
        this.presentToast('error', '');
      }
    } else {
      Object.keys(f.controls).forEach(key => {
        f.controls[key].markAsDirty();
      });
    }
  }
  saveMultiple(multiple, data) {
    console.log("multiple", multiple);
    console.log("data", data);

    if(this.checkValidationForCorrectAns(multiple.moption))
    {
      for (let i = 0; i < multiple.moption.length; i++) {
        this.multCount += Number(multiple.moption[i].mpoint);
        multiple.moption[i].perct =
          (multiple.moption[i].mpoint * 100) / this.multCount;
      }

      for (let i = 0; i < multiple.moption.length; i++) {
        multiple.moption[i].mpoint = multiple.moption[i].mpoint
          ? multiple.moption[i].mpoint
          : 0;
        multiple.moption[i].perct =
          (multiple.moption[i].mpoint * 100) / this.multCount;
        multiple.moption[i].mipoint = multiple.moption[i].mipoint
          ? multiple.moption[i].mipoint
          : 0;
        multiple.moption[i].mopvalue =
          multiple.moption[i].mopvalue == ""
            ? 0
            : multiple.moption[i].mopvalue == true
              ? 1
              : multiple.moption[i].mopvalue;
        if (multiple.moption[i].name) {
          delete multiple.moption[i].name;
        }
        if (multiple.moption[i].author) {
          delete multiple.moption[i].author;
        }

        if (multiple.moption[i].contentRef) {
          delete multiple.moption[i].contentRef;
        }
        //    if(multiple.moption[i].optionRef)
        // {
        //     multiple.moption[i].optionRef ="";
        // }
        for (let j = 0; j < data.length; j++) {
          if (data[j].includes(multiple.moption[i].qfileName)) {
            multiple.moption[i].optionRef = data[j];
            break;
          }
        }
        if (multiple.moption[i].qfileName) {
          delete multiple.moption[i].qfileName;
        }
        multiple.moption[i].qtypeid = Number(multiple.moption[i].qtypeid);
        // if(multiple.moption[i])
        // multiple.moption[i].manswer = this.sanitizeData(multiple.moption[i].manswer);
        // multiple.moption[i].manfeed = this.sanitizeData(multiple.moption[i].manfeed);
      }
      var option: string = Array.prototype.map
        .call(multiple.moption, (item) => {
          console.log("item", item);
          return Object.values(item).join(this.dataSeparatorService.Hash);
        })
        .join(this.dataSeparatorService.Pipe);
      multiple.option = option;
      console.log("this.multiple", multiple);
      console.log("this.string", option);

      let url = webApi.domain + webApi.url.addeditqbquestionmultiple;
      this.spinner.show();
      this.webApiService.getService(url, multiple).then(
        rescompData => {
          this.spinner.hide();
          var temp1: any = rescompData;
          this.qbMultiple = temp1.data;
          if (temp1 == "err") {
            // this.notFound = true;
            // var catUpdate: Toast = {
            //   type: "error",
            //   title: "Question",
            //   body: "Unable to update question Multiple type.",
            //   showCloseButton: true,
            //   timeout: 4000
            // };
            // this.toasterService.pop(catUpdate);
            this.presentToast('error', '');
          } else if (temp1.type == false) {
            // var catUpdate: Toast = {
            //   type: "error",
            //   title: "Question",
            //   body: this.qbMultiple.msg,
            //   showCloseButton: true,
            //   timeout: 4000
            // };
            // this.toasterService.pop(catUpdate);
            this.presentToast('error', '');
          } else {
            this.router.navigate(["/pages/assessment/qcategory/questions"]);
            // var catUpdate: Toast = {
            //   type: "success",
            //   title: "Question",
            //   // body: "Unable to update category.",
            //   body: this.qbMultiple.msg,
            //   showCloseButton: true,
            //   timeout: 4000
            // };
            this.basic = false;
            this.type = true;
            // this.toasterService.pop(catUpdate);
            this.presentToast('success', this.qbMultiple.msg);
          }
          console.log("Question AddEdit Result ", this.qbMultiple);
        },
        resUserError => {
          this.spinner.hide();
          this.errorMsg = resUserError;
        }
      );
    }else{
      this.toastr.info('Please select valid answer for quiz', 'Info');
    }

  }
  /**********MATCHING TYPE*************/
  cancelmatch(ind) {
    for (let i = 0; i < this.matching.moption.length; i++) {
      if (i == ind) {
        this.matching.moption[i].qfileName = "No File Chosen";
        this.matching.moption[i].optionRef = "";
        this.matching.moption[i].contentRef = undefined;
      }
    }
  }

  matchtypechange(mid) {
    for (let i = 0; i < this.matching.moption.length; i++) {
      if (i == mid) {
        this.matching.moption.optionText = "";
        this.matching.moption.optionRef = "";
        this.matching.moption[i].qfileName = "No File Chosen";
      }
    }
  }

  remove(id) {
    for (let i = 0; i < this.matching.moption.length; i++) {
      if (i == id) {
        this.matching.moption.splice(i, 1);
      }
    }
  }
  addanswer(f) {
    if(f.valid){
    var option = {
      qtypeid: 1,
      optionText: "",
      optionRef: "",
      optionMatch: "",
      mpoint: "",
      mipoint: 0,
      qfileName: "No File Chosen",
      perct: 0
    };
    this.matching.moption.push(option);
  }else{
      Object.keys(f.controls).forEach(key => {
        f.controls[key].markAsDirty();
      });
  }
  }

  readmatchimg(event: any, ind, qTypeid) {
    var size = 100000;
    var validExts = new Array("image");
    var fileExt = event.target.files[0].type;
    fileExt = fileExt.substring(0, 5);
    if (size <= event.target.files[0].size) {
      // var toast: Toast = {
      //   type: "error",
      //   title: "file size exceeded!",
      //   body: "files size should be less than 100KB",
      //   showCloseButton: true,
      //   timeout: 4000
      // };
      // this.toasterService.pop(toast);
      this.presentToast('warning', 'File size should be less than 100KB');
      event.target.value = "";
      // this.deleteCourseThumb();
    } else {
      if (validExts.indexOf(fileExt) < 0) {
        // var toast: Toast = {
        //   type: "error",
        //   title: "Invalid file selected!",
        //   body: "Valid files are of " + validExts.toString() + " types.",
        //   showCloseButton: true,
        //   timeout: 4000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('warning', 'Valid file types are ' + validExts.toString());
      } else if (event.target.files && event.target.files[0]) {
        for (let i = 0; i < this.matching.moption.length; i++) {
          if (i == ind) {
            this.matching.moption[i].qfileName = event.target.files[0].name
              ? event.target.files[0].name
              : "No file Chosen";
            this.matching.moption[i].optionRef = event.target.files[0];
          }
        }
      }
    }
  }

  //   cancelmatch(ind){
  //   for(let i =0;i<this.matching.moption.length;i++)
  //             {
  //               if(i == ind)
  //               {
  //                   this.matching.moption[i].qfileName="No File Chosen"
  //                   this.matching.moption[i].optionRef=""
  //               }

  //             }
  // }
  qbmatching: any;
  matchcontent: any = [];
  matchfileUploadRes: any = [];
  matchimgarr: any = [];
  matchTemparr: any = [];

  savematchImageData(f) {
    f.form.value.optionRef0 = this.matching.moption[0].optionRef
    if(f.valid){
    this.matchimgarr = [];
    this.matchTemparr = [];
    this.matchcontent = [];
    var matching = {
      moption: this.matching.moption,
      qId: this.categoryqueAddEditRes
        ? this.categoryqueAddEditRes.id
          ? this.categoryqueAddEditRes.id
          : this.formdata.qid
        : this.formdata.qid,
      userId: this.userdata.id,
      tId: this.userdata.tenantId,
      option: ""
    };

    if (matching.qId) {
      for (let i = 0; i < matching.moption.length; i++) {
        if (matching.moption[i].contentRef) {
          this.matchimgarr.push(matching.moption[i].optionRef);
          this.matchTemparr.push(i);
          matching.moption[i].name = matching.moption[i].qfileName;
          matching.moption[i].author = matching.qId;
          this.matchcontent.push(matching.moption[i]);
        }
        matching.moption[i].author = "author";
      }

      console.log("mutimgarr", this.matchimgarr);
      console.log("multTemparr", this.matchTemparr);
      console.log("multcontent", this.matchcontent);

      var fd = new FormData();
      fd.append("content", JSON.stringify(this.matchcontent));
      for (let i = 0; i < this.matchimgarr.length; i++) {
        fd.append("file", this.matchimgarr[i]);
      }

      // console.log('Question file',this.categoryImgData);
      // console.log('Question Data ',category);

      // let url = webApi.domain + webApi.url.addeditqbcategoryquestion;
      let fileUploadUrl = webApi.domain + webApi.url.multifileUpload;
      // let fileUploadUrlLocal = 'http://localhost:9845' + webApi.url.fileUpload;
      if (this.matchimgarr.length > 0) {
        this.spinner.show();
        this.webApiService.getService(fileUploadUrl, fd).then(
          (rescompData: any) => {
            this.spinner.hide();
            var temp: any = rescompData;
            this.matchfileUploadRes = rescompData;
            if (temp == "err") {
              // this.notFound = true;
              // var thumbUpload: Toast = {
              //   type: "error",
              //   title: "Question type",
              //   body: "Unable to upload Answer Images.",
              //   // body: temp.msg,
              //   showCloseButton: true,
              //   timeout: 4000
              // };
              // this.toasterService.pop(thumbUpload);
              this.presentToast('error', '');
            } else {
              if (this.matchfileUploadRes.length > 0) {
                var imageData = [];
                for (let i = 0; i < this.matchfileUploadRes.length; i++) {
                  imageData.push(this.matchfileUploadRes[i]);
                }
                // var imgData = this.multfileUploadRes.data.file_url;
                this.saveMatching(matching, imageData);
              } else {
                // var thumbUpload: Toast = {
                //   type: "error",
                //   title: "Question file",
                //   // body: "Unable to upload category thumbnail.",
                //   body: "Unable to upload Answer Images",
                //   showCloseButton: true,
                //   timeout: 4000
                // };
                // this.toasterService.pop(thumbUpload);
                this.presentToast('error', '');
              }
            }
            console.log("File Upload Result", this.multfileUploadRes);
          },
          resUserError => {
            this.spinner.hide();
            this.errorMsg = resUserError;
          }
        );
      } else {
        var imgData = [];
        this.saveMatching(matching, imgData);
      }
    } else {
      // var thumbUpload: Toast = {
      //   type: "error",
      //   title: "Add-Question",
      //   // body: "Unable to upload category thumbnail.",
      //   body: "Kindly fill Basic Datails",
      //   showCloseButton: true,
      //   timeout: 4000
      // };
      this.basic = true;
      this.type = false;
      // this.toasterService.pop(thumbUpload);
      this.presentToast('warning', 'Please fill the basic details');
    }
  }else{
    Object.keys(f.controls).forEach(key => {
      f.controls[key].markAsDirty();
    });
  }
  }

  matchCount: any = 0;
  saveMatching(matching, data) {
    console.log("multiple", matching);
    console.log("data", data);

    for (let i = 0; i < matching.moption.length; i++) {
      this.matchCount += Number(matching.moption[i].mpoint);
    }

    for (let i = 0; i < matching.moption.length; i++) {
      matching.moption[i].perct =
        (matching.moption[i].mpoint * 100) / this.matchCount;
      matching.moption[i].mpoint = matching.moption[i].mpoint
        ? matching.moption[i].mpoint
        : 0;
      matching.moption[i].mipoint = matching.moption[i].mipoint
        ? matching.moption[i].mipoint
        : 0;

      if (matching.moption[i].name) {
        delete matching.moption[i].name;
      }
      if (matching.moption[i].author) {
        delete matching.moption[i].author;
      }

      if (matching.moption[i].contentRef) {
        delete matching.moption[i].contentRef;
      }
      //    if(multiple.moption[i].optionRef)
      // {
      //     multiple.moption[i].optionRef ="";
      // }
      for (let j = 0; j < data.length; j++) {
        if (data[j].includes(matching.moption[i].qfileName)) {
          matching.moption[i].optionRef = data[j];
          break;
        }
      }
      if (matching.moption[i].qfileName) {
        delete matching.moption[i].qfileName;
      }
      matching.moption[i].qtypeid = Number(matching.moption[i].qtypeid);
      // matching.moption[i].optionMatch = this.sanitizeData(matching.moption[i].optionMatch);
      // matching.moption[i].manfeed = this.sanitizeData(multiple.moption[i].manfeed);
    }
    var option: string = Array.prototype.map
      .call(matching.moption,(item) => {
        console.log("item", item);
        return Object.values(item).join(this.dataSeparatorService.Hash);
      })
      .join(this.dataSeparatorService.Pipe);
    matching.option = option;
    console.log("this.multiple", matching);
    console.log("this.string", option);

    let url = webApi.domain + webApi.url.addeditqbquestionmatching;
    this.spinner.show();
    this.webApiService.getService(url, matching).then(
      rescompData => {
        this.spinner.hide();
        var temp1: any = rescompData;
        this.qbmatching = temp1.data;
        if (temp1 == "err") {
          // this.notFound = true;
          // var catUpdate: Toast = {
          //   type: "error",
          //   title: "Question",
          //   body: "Unable to update question Matching type.",
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
        } else if (temp1.type == false) {
          // var catUpdate: Toast = {
          //   type: "error",
          //   title: "Question",
          //   body: this.qbmatching.msg,
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
        } else {
          this.router.navigate(["/pages/assessment/qcategory/questions"]);
          // var catUpdate: Toast = {
          //   type: "success",
          //   title: "Question",
          //   // body: "Unable to update category.",
          //   body: this.qbmatching.msg,
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          this.basic = false;
          this.type = true;
          // this.toasterService.pop(catUpdate);
          this.presentToast('success', this.qbmatching.msg);
        }
        console.log("Question AddEdit Result ", this.qbmatching);
      },
      resUserError => {
        this.spinner.hide();
        this.loader = false;
        this.errorMsg = resUserError;
      }
    );
  }

  /**********SHORT TYPE*************/

  // addshort() {
  //   var option = {
  //     qoption: "",
  //     isCase: "",
  //     score: "",
  //     penelty: 0,
  //     feedback: "",
  //     visible: 1,
  //     perct: 0
  //   };
  //   this.short.soption.push(option);
  // }
  // removeshort(id) {
  //   for (let i = 0; i < this.short.soption.length; i++) {
  //     if (i == id) {
  //       this.short.soption.splice(i, 1);
  //     }
  //   }
  // }

  qbShort: any;
  shortCount: any = 0;
  saveShort(f) {
    console.log("formObj",f)
    if(f.valid&&f.value.min<=f.value.max){
    var short = {
      qformatId: this.formdata.qFormat ? this.formdata.qFormat : 1,
      min: this.short.min,
      max: this.short.max,
      score: null,
      penelty: null,
      qId: this.categoryqueAddEditRes
        ? this.categoryqueAddEditRes.id
          ? this.categoryqueAddEditRes.id
          : this.formdata.qid
        : this.formdata.qid,
      userId: this.userdata.id,
      tId: this.userdata.tenantId,
      visible: 1,
    };

    // for (let i = 0; i < short.soption.length; i++) {
    //   this.shortCount += Number(short.soption[i].score);
    // }
    // for (let i = 0; i < short.soption.length; i++) {
    //   short.soption[i].perct = (short.soption[i].score * 100) / this.shortCount;
    //   short.soption[i].isCase = short.scase;
    // }

    // var option: string = Array.prototype.map
    //   .call(short.soption, function (item) {
    //     return Object.values(item).join("#");
    //   })
    //   .join("|");
    // short.option = option;
    // console.log("this.multiple", short);
    // console.log("this.string", option);

    let url = webApi.domain + webApi.url.addeditqbquestionshort;
    this.spinner.show();
    this.webApiService.getService(url, short).then(
      rescompData => {
        this.spinner.hide();
        var temp1: any = rescompData;
        this.qbShort = temp1.data;
        if (temp1 == "err") {
          // this.notFound = true;
          // var catUpdate: Toast = {
          //   type: "error",
          //   title: "Question",
          //   body: "Unable to update question short type.",
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
        } else if (temp1.type == false) {
          // var catUpdate: Toast = {
          //   type: "error",
          //   title: "Question",
          //   body: this.qbShort.msg,
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
        } else {
          this.router.navigate(["/pages/assessment/qcategory/questions"]);
          // var catUpdate: Toast = {
          //   type: "success",
          //   title: "Question",
          //   // body: "Unable to update category.",
          //   body: this.qbShort.msg,
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          this.basic = false;
          this.type = true;
          // this.toasterService.pop(catUpdate);
          this.presentToast('success', this.qbShort.msg);
        }
        console.log("Question AddEdit Result ", this.qbShort);
      },
      resUserError => {
        this.spinner.hide();
        this.errorMsg = resUserError;
      }
    );
    }else{
      Object.keys(f.controls).forEach(key => {
        f.controls[key].markAsDirty();
      });
    }
  }

  /*******SEQUENCE TYPE*********/

  addsequence() {
    var option = {
      sanswer: "",
      sindex: "",
      spoint: "",
      sipoint: 0,
      perct: 0
    };
    this.sequence.soption.push(option);
  }
  removesequence(id) {
    for (let i = 0; i < this.sequence.soption.length; i++) {
      if (i == id) {
        this.sequence.soption.splice(i, 1);
      }
    }
  }

  qbSequence: any;
  squCount: any = 0;
  saveSequence(f) {
    if(f.valid){
    var sequence = {
      soption: this.sequence.soption,
      qId: this.categoryqueAddEditRes
        ? this.categoryqueAddEditRes.id
          ? this.categoryqueAddEditRes.id
          : this.formdata.qid
        : this.formdata.qid,
      userId: this.userdata.id,
      tId: this.userdata.tenantId,
      option: ""
    };
    for (let i = 0; i < sequence.soption.length; i++) {
      this.squCount += Number(sequence.soption[i].spoint);
    }
    for (let i = 0; i < sequence.soption.length; i++) {
      sequence.soption[i].perct =
        (sequence.soption[i].spoint * 100) / this.squCount;
      sequence.soption[i].sindex = i;
      // sequence.soption[i].sanswer = this.sanitizeData(sequence.soption[i].sanswer);
    }

    var option: string = Array.prototype.map
      .call(sequence.soption, (item) => {
        return Object.values(item).join(this.dataSeparatorService.Hash);
      })
      .join(this.dataSeparatorService.Pipe);
    sequence.option = option;
    console.log("this.multiple", sequence);
    console.log("this.string", option);

    let url = webApi.domain + webApi.url.addeditqbquestionsequence;
    this.spinner.show();
    this.webApiService.getService(url, sequence).then(
      rescompData => {
        this.spinner.hide();
        var temp1: any = rescompData;
        this.qbSequence = temp1.data;
        if (temp1 == "err") {
          // this.notFound = true;
          // var catUpdate: Toast = {
          //   type: "error",
          //   title: "Question",
          //   body: "Unable to update question Sequence type.",
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          this.presentToast('error', '');
          // this.toasterService.pop(catUpdate);

        } else if (temp1.type == false) {
          // var catUpdate: Toast = {
          //   type: "error",
          //   title: "Question",
          //   body: this.qbSequence.msg,
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
        } else {
          this.router.navigate(["/pages/assessment/qcategory/questions"]);
          // var catUpdate: Toast = {
          //   type: "success",
          //   title: "Question",
          //   // body: "Unable to update category.",
          //   body: this.qbSequence.msg,
          //   showCloseButton: true,
          //   timeout: 4000
          // };
          this.basic = false;
          this.type = true;
          // this.toasterService.pop(catUpdate);
          this.presentToast('success', this.qbSequence.msg);
        }
        console.log("Question AddEdit Result ", this.qbSequence);
      },
      resUserError => {
        this.spinner.hide();
        this.errorMsg = resUserError;
      }
    );
    }else{
      Object.keys(f.controls).forEach(key => {
        f.controls[key].markAsDirty();
      });
    }
  }

  selectTab(item) {
    console.log('this.formdata ====================>',this.formdata);
    if (item.tabTitle === "Basic Details") {
      // this.selectedActInd =undefined;
      this.type = false;
      this.basic = true;
      // for(let i=0;i < this.qbcategory.length; i++) {
      //   if(this.qbcategory[i].id  == this.formdata.cid){
      //       let name = this.qbcategory[i].name
      //   }
      // }

      console.log('name', name)
      
     this.header={ 
       title: this.title,
      btnsSearch: true,
      btnName2: 'Save',
      btnName2show: true,
      btnBackshow: true,
      showBreadcrumb:true,
      breadCrumbList:[
        {
          'name': 'Assessment',
          'navigationPath': '/pages/assessment',
        },
        {
        'name': 'Question Bank',
        'navigationPath': '/pages/assessment/qcategory',
      },
      {
        'name': this.tempname,
        'navigationPath': '/pages/assessment/qcategory/questions',
      }]
    };
    } else if (item.tabTitle === "Question Type") {
      // this.selectedActInd =0;
      this.basic = false;
      this.type = true;
    }
  }

  // Tag cganges

  onTagsSelect(item: any) {
    console.log(item);
    console.log(this.selectedTags);
  }
  OnTagDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedTags);
  }
  onTagSearch(evt: any) {
    console.log(evt.target.value);
    const val = evt.target.value;
    this.tagList = [];
    const temp = this.tempTags.filter(function (d) {
      return (
        String(d.name)
          .toLowerCase()
          .indexOf(val) !== -1 ||
        !val
      );
    });

    // update the rows
    this.tagList = temp;
    console.log('filtered Tag LIst', this.tagList);
  }
  // Help Code Start Here //

  helpContent: any;
  getHelpContent() {
    return new Promise(resolve => {
      this.http1
        .get("../../../../../../assets/help-content/addEditCourseContent.json")
        .subscribe(
          data => {
            this.helpContent = data;
            console.log("Help Array", this.helpContent);
          },
          err => {
            resolve("err");
          }
        );
    });
    // return this.helpContent;
  }

  // Help Code Ends Here //

  // Check validation for correct answer

  checkValidationForCorrectAns(data) {
    if(data.length !=0) {
      // data.forEach(element => {
      //   if(element.mopvalue == true || element.mopvalue == 1) {
      //     return true;
      //   }
      // });
      for (let index = 0; index < data.length; index++) {
        if(data[index].mopvalue == true || data[index].mopvalue == 1) {
              return true;
            }
      }
    }
      return false;
  }


  // hexToRGB(h) {
  //   let r:any = 0, g:any = 0, b:any = 0;
  //   // 3 digits
  //   if (h.length == 4) {
  //     r = "0x" + h[1] + h[1];
  //     g = "0x" + h[2] + h[2];
  //     b = "0x" + h[3] + h[3];
  //   // 6 digits
  //   } else if (h.length == 7) {
  //     r = "0x" + h[1] + h[2];
  //     g = "0x" + h[3] + h[4];
  //     b = "0x" + h[5] + h[6];
  //   }
  //   return "rgb("+ +r + "," + +g + "," + +b + ")";
  //  }

  //  sanitizeData(data) {
  //   const rgbHex = /#([0-9A-F][0-9A-F])([0-9A-F][0-9A-F])([0-9A-F][0-9A-F])/gim;
  //   const rgbHex3char = /#([0-9A-F][0-9A-F])([0-9A-F])/gim;
  //   data = data.replace(rgbHex,(m, r, g, b) => {
  //   /*return 'rgb(' + parseInt(r,16) + ','
  //                 + parseInt(g,16) + ','
  //                 + parseInt(b,16) + ')';*/
  //                 return this.hexToRGB(m);
  //   });
  //   data = data.replace(rgbHex3char,(m, r, g, b) => {
  //     /*return 'rgb(' + parseInt(r,16) + ','
  //                   + parseInt(g,16) + ','
  //                   + parseInt(b,16) + ')';*/
  //                   return this.hexToRGB(m);
  //     });
  //   return data;
  //  }
}
