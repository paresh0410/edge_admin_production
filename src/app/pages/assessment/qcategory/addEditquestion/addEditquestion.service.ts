import {Injectable, Inject} from '@angular/core';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
 import {AppConfig} from '../../../../app.module';
 import { HttpClient } from '@angular/common/http';

@Injectable()
export class  addEditquestionService {

  public data:any;
    private _url:string = "/api/web/getallqbdropdown" 
  private _url_mul:string = "/api/web/get_category_question_multiple" //DevTest
   private _url_match:string = "/api/web/get_category_question_matching" //DevTest
  private _url_seq:string="/api/web/get_category_question_sequence"
  private _url_sho:string="/api/web/get_category_question_short"
  request: Request;
  
  constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private _http: Http, private _http1: HttpClient){
      //this.busy = this._http.get('...').toPromise();
  }

  getmultipleData(category){
    let url:any = `${this.config.FINAL_URL}`+this._url_mul;
    // return this._http.post(url,category)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);
    return new Promise(resolve => {
      this._http1
        .post(url, category)
        //.map(res => res.json())
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            resolve('err');
          }
        );
    });
  }
    getMatchingData(category){
    let url:any = `${this.config.FINAL_URL}`+this._url_match;
    // return this._http.post(url,category)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);
    return new Promise(resolve => {
      this._http1
        .post(url, category)
        //.map(res => res.json())
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            resolve('err');
          }
        );
    });
  }
   getsequenceData(category){
    let url:any = `${this.config.FINAL_URL}`+this._url_seq;
    // return this._http.post(url,category)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);
    return new Promise(resolve => {
      this._http1
        .post(url, category)
        //.map(res => res.json())
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            resolve('err');
          }
        );
    });
  }
     getshortData(category){
    let url:any = `${this.config.FINAL_URL}`+this._url_sho;
    // return this._http.post(url,category)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);
    return new Promise(resolve => {
      this._http1
        .post(url, category)
        //.map(res => res.json())
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            resolve('err');
          }
        );
    });
  }
    getqbdropdowun(data){
    let url:any = `${this.config.FINAL_URL}`+this._url;
    // return this._http.post(url,data)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);
    return new Promise(resolve => {
      this._http1
        .post(url, data)
        //.map(res => res.json())
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            resolve('err');
          }
        );
    });
  }


  _errorHandler(error: Response){
    console.error(error);
    return Observable.throw(error || "Server Error")
  }

}
