// import { routing }       from './addEditUser.routing';
import { qAddeditccategory } from './qaddEditCategory.component';
import {  qAddeditccategoryService } from './qaddEditCategory.service';


import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';

//import { AgGridModule } from 'ag-grid-angular';
// import 'ag-grid-enterprise';
import 'ag-grid-community';

import { MyDatePickerModule } from 'mydatepicker';
// import { TruncateModule } from 'ng2-truncate';
// import { TabsModule } from 'ngx-tabs';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { TagInputModule } from 'ngx-chips';
import { ComponentModule } from '../../../../component/component.module';


@NgModule({
  imports: [
    CommonModule,
    // routing,
    FormsModule,
   // AgGridModule.withComponents([]),
    // TabsModule,
    ReactiveFormsModule,
    Ng2SmartTableModule,
    TagInputModule,
    AngularMultiSelectModule,
    ComponentModule
  ],
  declarations: [
    qAddeditccategory,
    // ChartistJs
  ],
  providers: [
     qAddeditccategoryService,
  ],
  schemas: [ 
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA 
  ]
})
export class  qAddeditccategoryModule {}
