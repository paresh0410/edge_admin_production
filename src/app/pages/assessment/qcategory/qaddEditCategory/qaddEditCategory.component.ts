import { Component,Directive,ViewEncapsulation,forwardRef,Attribute,OnChanges,
  SimpleChanges,Input, ViewChild, ViewContainerRef } from '@angular/core';
import { qAddeditccategoryService } from './qaddEditCategory.service';
import { NG_VALIDATORS,Validator,Validators,AbstractControl,ValidatorFn } from '@angular/forms';
import { LocalDataSource } from 'ng2-smart-table';
import { Router, NavigationStart, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { webAPIService } from '../../../../service/webAPIService';
import { webApi } from '../../../../service/webApi'
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpClient } from "@angular/common/http";
import { SuubHeader } from '../../../components/models/subheader.model';

@Component({
  selector: 'qaddeditccategory',
  templateUrl: './qaddEditCategory.html',
  styleUrls: ['./qaddEditCategory.scss'],
  encapsulation: ViewEncapsulation.None
})

export class  qAddeditccategory {
  header: SuubHeader  = {
    title: '',
    btnsSearch: true,
    searchBar: false,
    dropdownlabel: ' ',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: 'Save',
    btnName2: '',
    btnName3: '',
    btnAdd: '',
    btnName1show: true,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: false,
    filter: false,
    showBreadcrumb:true,
    breadCrumbList:[
      {
        'name': 'Assessment',
        'navigationPath': '/pages/assessment',
      },
      {
      'name': 'Question Bank Category',
      'navigationPath': '/pages/assessment/qcategory',
    }]
  };

  query: string = '';
  public getData;
  defaultThumb : any = 'assets/images/category.jpg';
  title:any;
  formdata:any;
  profileUrl : any = 'assets/images/category.jpg';
  result:any;  
  errorMsg:any;
  loader:any;
  parentcat:any=[{
    id:0,
    name:'Parent'
  },
  {
    id:1,
    name:'Assigned'
  },
  {
    id:2,
    name:'Recommended'
  },
  {
    id:3,
    name:'Special'
  },]

  visibility:any=[{
    id:1,
    name:'Show'
  },
  {
    id:2,
    name:'Hide'
  }]

  settingsTagDrop ={};
  tempTags:any =[];
  tagList:any = [];
  selectedTags:any = [];

  catData: any = undefined;
    userLoginData:any;
    userdata:any;
  constructor(private spinner: NgxSpinnerService,protected service:qAddeditccategoryService,
    //  private toasterService: ToasterService,
     protected webApiService:webAPIService, private router:Router, 
     private toastr: ToastrService, private route :ActivatedRoute, private http1: HttpClient){
       this.spinner.show()
      this.getHelpContent();
    // console.log("Add Category Called");
    var category;
    var id;
      if (localStorage.getItem('LoginResData')) {
      this.userLoginData = JSON.parse(localStorage.getItem('LoginResData'));
      this.userdata = this.userLoginData.data.data;
      console.log("login data", this.userdata);
    }
    this.settingsTagDrop = {
      text: 'Select Tags',
      singleSelection: false,
      classes: 'myclass custom-class',
      primaryKey: 'id',
      labelKey: 'name',
      noDataLabel: 'Search Tags...',
      enableSearchFilter: true,
      lazyloading: true,
      searchBy: ['name'],
      maxHeight:250,
    };
    if(this.service.data != undefined){
      category = this.service.data.data;
      id = this.service.data.id;
      this.catData = this.service.data.data;
    }else{
      id = 0;
    }
    
    this.getallTagList(id,category);
    console.log(this.title,"title")
    this.header['title'] = category?category.catName:"Add Category"
    this.spinner.hide()
    console.log("Service Data:",this.service.data);
    console.log('Category Info',category);
    console.log('id',id);
    
 
  }
  

  getallTagList(id,category) {
    this.spinner.show();
    let url = webApi.domain + webApi.url.getAllTagsComan;
    let param = {
      tenantId :this.userdata.tenantId
    };
    this.webApiService.getService(url, param)
      .then(rescompData => {
        // this.loader =false;
        this.spinner.hide();
        var temp: any = rescompData;
        this.tagList = temp.data;
        this.tempTags = [... this.tagList];
        this.makeCategoryDataReady(id, category);
        // console.log('Visibility Dropdown',this.visibility);
      },
        resUserError => {
          // this.loader =false;
          this.show =false;
          this.spinner.hide();
          this.errorMsg = resUserError;
        });
  }
  
  show:boolean =false;
  makeCategoryDataReady(id,category){
    this.spinner.show()
        if(id==1){
          // this.title = 'Edit Category';
          // this.header['title'] = this.title
          this.formdata={
            id: category.id,
            cname: category.catName,
            cdesc: category.description,
            ctags:category.tags=='' ? category.tags:category.tags.split(',') ,
            picRef:category.picRef ? category.picRef : this.defaultThumb,
            visible : category.visible,
            tenantId :category.tenantId,
          }
          if(category.tagIds)
          {
            var tagIds =category.tagIds.split(',');
            if(tagIds.length > 0){
              this.tempTags.forEach((tag) => {
                tagIds.forEach((tagId)=>{
                  if (tag.id == tagId ) {
                    this.selectedTags.push(tag);
                  }
                });
              });
              }
        }
          console.log(this.formdata)
        }else{
          // this.title = 'Add Category';
          // this.header['title'] = this.title
          this.formdata={
            id: 0,
            cname: '',
            cdesc: '',
            ctags:'',
            picRef:this.defaultThumb,
            visible : 1,
            tenantId : this.userdata.tenantId,
          }
        }
        this.show =true;
        this.spinner.hide()
      }
    
  back(){
    this.router.navigate(['/pages/assessment/qcategory']);
  }


  presentToast(type, body) {
    if(type === 'success'){
      this.toastr.success(body, 'Success', {
        closeButton: false
       });
    } else if(type === 'error'){
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else{
      this.toastr.warning(body, 'Warning', {
        closeButton: false
        })
    }
  }

  // readUrl(event:any) {
  //    if (event.target.files && event.target.files[0]) {
  //            var reader = new FileReader();

  //            reader.onload = (event: ProgressEvent) => {
  //           this.profileUrl = (<FileReader>event.target).result;
  //        }
  //       reader.readAsDataURL(event.target.files[0]);
  //    }
  // }

    categoryImgData:any;
  readCategoryThumb(event:any) {
    var validExts = new Array(".png", ".jpg", ".jpeg");
    var fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if(validExts.indexOf(fileExt) < 0){
        // var toast : Toast = {
        //     type: 'error',
        //     title: "Invalid file selected!",
        //     body: "Valid files are of " + validExts.toString() + " types.",
        //     showCloseButton: true,
        //     // tapToDismiss: false, 
        //     timeout: 4000
            // onHideCallback: () => {
            //     this.router.navigate(['/pages/plan/users']);
            // }
        // };
        // this.toasterService.pop(toast);
      this.presentToast('warning', 'Valid file types are ' + validExts.toString());
        // this.deleteCourseThumb();
    }else{
      if (event.target.files && event.target.files[0]) {
              this.categoryImgData = event.target.files[0];

              var reader = new FileReader();

              reader.onload = (event: ProgressEvent) => {
              // this.defaultThumb = (<FileReader>event.target).result;
              this.formdata.picRef = (<FileReader>event.target).result;
          }
          reader.readAsDataURL(event.target.files[0]);
      }
    }
  }

    deleteCategoryThumb() {
      // this.defaultThumb = 'assets/images/category.jpg';
      this.formdata.picRef = 'assets/images/category.jpg';
      this.categoryImgData = undefined;
      this.formdata.categoryPicRefs =undefined;
  }


  // deleteProfile() {
  //     this.profileUrl = 'assets/images/category.jpg';
  // }
  
fileUploadRes:any;
  categoryAddEditRes:any;
  
  addUpdateCategory(url,category){
    this.spinner.show();
    this.webApiService.getService(url,category)
        .then(rescompData => { 
          this.spinner.hide();
          var temp:any = rescompData;
          this.categoryAddEditRes = temp.data;
          if(temp == "err"){
            // this.notFound = true;
            // var catUpdate : Toast = {
            //     type: 'error',
            //     title: "Category",
            //     body: "Unable to update category.",
            //     showCloseButton: true,
            //     timeout: 4000
            // };
            // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
          }else if(temp.type == false){
            // var catUpdate : Toast = {
            //     type: 'error',
            //     title: "Category",
            //     body: this.categoryAddEditRes.msg,
            //     showCloseButton: true,
            //     timeout: 4000
            // };
            // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
          }else{
            this.router.navigate(['/pages/assessment/qcategory']);
            // var catUpdate : Toast = {
            //     type: 'success',
            //     title: "Category",
            //     // body: "Unable to update category.",
            //     body: this.categoryAddEditRes.msg,
            //     showCloseButton: true,
            //     timeout: 4000
            // };
            // this.toasterService.pop(catUpdate);
          this.presentToast('success', this.categoryAddEditRes.msg);
          }
          console.log('Category AddEdit Result ',this.categoryAddEditRes)
        },
        resUserError=>{
                this.spinner.hide();
          this.loader =false;
          this.errorMsg = resUserError;
        });
  }
  makeTagDataReady(tagsData) {
    this.formdata.tags  =''
     tagsData.forEach((tag)=>{
      if(this.formdata.tags  == '')
      {
        this.formdata.tags  = tag.id;
      }else
      {
        this.formdata.tags = this.formdata.tags +'|' + tag.id;
      }
      console.log('this.formdata.tags',this.formdata.tags);
     });
    // var tagsData = this.formdata.tags;
    // var tagsString = '';
    // if (tagsData) {
    //   for (let i = 0; i < tagsData.length; i++) {
    //     var tag = tagsData[i];
    //     if (tagsString != "") {
    //       tagsString += "|";
    //     }
    //     if (String(tag) != "" && String(tag) != "null") {
    //       tagsString += tag;
    //     }
    //   }
    //   this.formdata.tags1 = tagsString;
    // }
  }

  submit(f){
    if(f.valid){
      this.spinner.show();
      if (this.selectedTags.length > 0) {
        this.makeTagDataReady(this.selectedTags);
         // this.formdata.tags = this.formattedTags;
       }
      var category={
        categoryId : this.formdata.id,
        categoryName : this.formdata.cname,
        categoryCode :'',
        description : this.formdata.cdesc,
        // categoryPicRef : this.categoryImgData == undefined ? null : this.formdata.categoryPicRef,
        categoryPicRef : this.formdata.picRef,
        visible : this.formdata.visible,
        tenantId : this.formdata.tenantId,
        tags : this.formdata.tags ,
        userId:this.userdata.id,
        // tagsList : this.formdata.tags,
      }
  
  console.log('category',category)
      var fd = new FormData();
      fd.append('content',JSON.stringify(category));
      fd.append('file',this.categoryImgData);
      console.log('File Data ',fd);
  
      console.log('Category Data Img',this.categoryImgData);
      console.log('Category Data ',category);
      
      let url = webApi.domain + webApi.url.addEditquestionCategory;
      let fileUploadUrl = webApi.domain + webApi.url.fileUpload;
      // let fileUploadUrlLocal = 'http://localhost:9845' + webApi.url.fileUpload;
      let param = {
        tId: 1
      } 
  
      if(this.categoryImgData != undefined){
        this.webApiService.getService(fileUploadUrl,fd)
          .then(rescompData => { 
            this.spinner.hide();
            var temp:any = rescompData;
            this.fileUploadRes = temp;
            if(temp == "err"){
              // this.notFound = true;
              // var thumbUpload : Toast = {
              //     type: 'error',
              //     title: "Category Thumbnail",
              //     body: "Unable to upload category thumbnail.",
              //     // body: temp.msg,
              //     showCloseButton: true,
              //     timeout: 4000
              // };
              // this.toasterService.pop(thumbUpload);
          this.presentToast('error', '');
            }else if(temp.type == false){
              // var thumbUpload : Toast = {
              //     type: 'error',
              //     title: "Category Thumbnail",
              //     body: "Unable to upload category thumbnail.",
              //     // body: temp.msg,
              //     showCloseButton: true,
              //     timeout: 4000
              // };
              // this.toasterService.pop(thumbUpload);
              this.presentToast('error', '');
            }
            else{
              if(this.fileUploadRes.data != null || this.fileUploadRes.fileError != true){
                category.categoryPicRef = this.fileUploadRes.data.file_url;
                this.addUpdateCategory(url,category);
              }else{
                // var thumbUpload : Toast = {
                //     type: 'error',
                //     title: "Category Thumbnail",
                //     // body: "Unable to upload category thumbnail.",
                //     body: this.fileUploadRes.status,
                //     showCloseButton: true,
                //     timeout: 4000
                // };
                // this.toasterService.pop(thumbUpload);
                 this.presentToast('error', '');
              }
            }
            console.log('File Upload Result',this.fileUploadRes)
          },
          resUserError=>{
                  this.spinner.hide();
            this.loader =false;
            this.errorMsg = resUserError;
          });
      }else{
        this.addUpdateCategory(url,category);
      }
    }  else{
      console.log('Please Fill all fields');
      // const addEditF: Toast = {
      //   type: 'error',
      //   title: 'Unable to update',
      //   body: 'Please Fill all fields',
      //   showCloseButton: true,
      //   timeout: 2000
      // };
      // this.toasterService.pop(addEditF);
      this.presentToast('warning', 'Please fill in the required fields');
      Object.keys( f.controls).forEach(key => {
        f.controls[key].markAsDirty();
        });
    }
}
   // Tag cganges

   onTagsSelect(item: any) {
    console.log(item);
    console.log(this.selectedTags);
  }
  OnTagDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedTags);
  }
  onTagSearch(evt: any) {
    console.log(evt.target.value);
    const val = evt.target.value;
    this.tagList = [];
    const temp = this.tempTags.filter(function(d) {
      return (
        String(d.name)
          .toLowerCase()
          .indexOf(val) !== -1 ||
        !val
      );
    });
  
    // update the rows
    this.tagList = temp;
    console.log('filtered Tag LIst',this.tagList);
  }
  

       // Help Code Start Here //

       helpContent: any;
       getHelpContent() {
         return new Promise(resolve => {
           this.http1
             .get("../../../../../../assets/help-content/addEditCourseContent.json")
             .subscribe(
               data => {
                 this.helpContent = data;
                 console.log("Help Array", this.helpContent);
               },
               err => {
                 resolve("err");
               }
             );
         });
         // return this.helpContent;
       }
     
       // Help Code Ends Here //

}
