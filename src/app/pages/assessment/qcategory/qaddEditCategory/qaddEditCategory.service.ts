import {Injectable, Inject} from '@angular/core';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
 import {AppConfig} from '../../../../app.module';

@Injectable()
export class  qAddeditccategoryService {

  public data:any;
  private _url:string = "/api/edge/category/addUpdateCategory" //DevTest
  request: Request;
  
  constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private _http: Http){
      //this.busy = this._http.get('...').toPromise();
  }

  createUpdateCategory(category){
    let url:any = `${this.config.FINAL_URL}`+this._url;
    return this._http.post(url,category)
      .map((response:Response) => response.json())
      .catch(this._errorHandler);
  }

  _errorHandler(error: Response){
    console.error(error);
    return Observable.throw(error || "Server Error")
  }

}
