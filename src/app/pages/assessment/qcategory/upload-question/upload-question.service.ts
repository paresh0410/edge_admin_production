import { Injectable } from '@angular/core';
import { webApi } from '../../../../service/webApi';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class UploadQuestionService {
  private _urlUploadQuestion= webApi.domain + webApi.url.uploadBulkQuizQuestion;
  private _urlUploadQuestionDropdown= webApi.domain + webApi.url.uploadBulkQuizQuestionType;
  // private _urlgetAllpartnersListUP = webApi.domain + webApi.url.getAllpartnersListUP;
  constructor(private _httpClient: HttpClient) { }
  submitBulkQuestion(param) {
    return new Promise(resolve => {
        this._httpClient.post(this._urlUploadQuestion, param)
            //.map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
                err => {
                    resolve('err');
                });
    }); 
  }

  getBulkQuestionDropdown(param) {
    return new Promise(resolve => {
        this._httpClient.post(this._urlUploadQuestionDropdown, param)
            //.map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
                err => {
                    resolve('err');
                });
    });
    
}
}
