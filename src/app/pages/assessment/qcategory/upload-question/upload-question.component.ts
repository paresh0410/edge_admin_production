import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { XlsxToJsonService } from './xlsx-to-json-service';
import { JsonToXlsxService } from './json-to-xlsx-service';
import { NgxSpinnerService } from 'ngx-spinner';
import { UploadQuestionService } from './upload-question.service';
import { Router } from '@angular/router';
import { id } from '@swimlane/ngx-datatable/release/utils';
import { SuubHeader } from '../../../components/models/subheader.model';
@Component({
  selector: 'ngx-upload-question',
  templateUrl: './upload-question.component.html',
  styleUrls: ['./upload-question.component.scss']
})
export class UploadQuestionComponent implements OnInit {
  disabledPopup = false;
  title = 'Upload Question';
  enableUpload: boolean = false;
  fileReaded: any;
  rows = [];
  selected = [];
  templateExelLink: any = 'assets/images/Asset_Upload_Template.xlsx';

  loader: any;
  questionTypeArray = [
    // {id:1 , name: 'Single',isChecked: false},
    // {id:2 , name: 'Multiple', isChecked: false},
  ];
  selectedQuestionType: any;

  @ViewChild('fileUpload') fileUpload: any;
  fileName: String = 'You can drag and drop file here to add questions';
  fileIcon: any = 'assets/img/app/profile/avatar4.png';
  selectFileTitle: any = 'No file chosen';
  @ViewChild('myTable') table: any;
  validdata: any = [];
  invaliddata: any = [];
  // validDataFinal: any = [];
  // statusData: any = [];
  public result: any;
  fileUrl: any;
  bulkUploadQuestion: any = null;
  private xlsxToJsonService: XlsxToJsonService = new XlsxToJsonService();
  uploadedData: any;
  resultSheets: any = [];
  tenantId: any;
  userId: any;
  hideReason = false;
  uploadsheetLink = '';
  // questionTypeFlag = false;
  submitDisabled = false;
  // uploadDetailsOne: any = [
  //   { assetName: "B2B sales", date: '12/04/2019', category: 'Abc', format: '.xls', channel: 'akdf', language: 'english', status: 'english2'},
  //   { assetName: "B2B sales", date: '12/04/2019', category: 'Abc', format: '.xls', channel: 'akdf', language: 'english', status: 'englis23h' },
  //   { assetName: "B2B sales", date: '12/04/2019', category: 'Abc', format: '.xls', channel: 'akdf', language: 'english', status: 'english232' },
  //   { assetName: "B2B sales", date: '12/04/2019', category: 'Abc', format: '.xls', channel: 'akdf', language: 'english' ,status: 'english31' },
  //   { assetName: "B2B sales", date: '12/04/2019', category: 'Abc', format: '.xls', channel: 'akdf', language: 'english' , status: 'english3' },
  //   { assetName: "B2B sales", date: '12/04/2019', category: 'Abc', format: '.xls', channel: 'akdf', language: 'english', status: 'english32'  },
  //   { assetName: "B2B sales", date: '12/04/2019', category: 'Abc', format: '.xls', channel: 'akdf', language: 'english', status: 'english22'  },
  // ]
  header: SuubHeader
  constructor(private toastr: ToastrService,
    protected exportService: JsonToXlsxService,
    private spinner: NgxSpinnerService,
    private cdf: ChangeDetectorRef,
    private uploadQuestion: UploadQuestionService, public router: Router) {
    if (localStorage.getItem('LoginResData')) {
      const userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', userData.data);
      this.userId = userData.data.data.id;
      console.log('userId', userData.data.data.id);
      this.tenantId = userData.data.data.tenantId;
    }
  }

  ngOnInit() {
    const param = {
      'ltypeId': 8,
      'tId': this.tenantId,
    };
    this.uploadQuestion.getBulkQuestionDropdown(param).then(res => {
      let data = res['data'];
      // this.statusData =  JSON.parse(data);
      // console.log('this.statusData:', this.statusData);
      console.log('data:', data);
      if (data) {
        this.questionTypeArray = data[0];
        if (data.length > 0) {
          this.selectedQuestionType = this.questionTypeArray[0];

          this.uploadsheetLink = 'assets/images/Template_single-multichoice_choice.xlsx';
          this.title = 'Single / Multichoice Upload';
          this.header  = {
            title:'Single / Multichoice Upload',
            btnsSearch: true,
            quesDrop:true,
            dropId:'id',
            dropName:'name',
            btnBackshow: true,
            showBreadcrumb: true,
            btnNameupload: true,
            uploadsheetLink : 'assets/images/Template_single-multichoice_choice.xlsx',
            breadCrumbList:[
              {
                'name': 'Assessment',
                'navigationPath': '/pages/assessment',
              },
              {
                'name': 'Question Bank Category',
                'navigationPath': '/pages/assessment/qcategory',
              },]
            }
        }
        else {
          this.toastr.error('error', 'Question Types not found.');
        }
      }

      // this.questionTypeFlag = true;
    }).catch(
      err => {
        // this.questionTypeFlag = true;
        this.toastr.error('Error', 'Question Types not found. Please check server');
      }
    );

    
  }

  closesectionModel() {
    this.disabledPopup = false;
    window.history.back();
  }

  onChange(type) {
    console.log('data ===>', type);
    this.selectedQuestionType = type;
    this.changeLinkandTitle();
    setTimeout(() => {
      this.disabledPopup = false;
    }, 200);

  }

  changeLinkandTitle() {
    if (this.selectedQuestionType.id == '1') {
      this.uploadsheetLink = 'assets/images/Template_single-multichoice_choice.xlsx';
      this.title = 'Single / Multichoice Upload';
      this.header  = {
        title:'Single / Multichoice Upload',
        btnsSearch: true,
        quesDrop:true,
        dropId:'id',
        dropName:'name',
        btnBackshow: true,
        showBreadcrumb: true,
        btnNameupload: true,
            uploadsheetLink : 'assets/images/Template_single-multichoice_choice.xlsx',
        breadCrumbList:[
          {
            'name': 'Assessment',
            'navigationPath': '/pages/assessment',
          },
          {
            'name': 'Question Bank Category',
            'navigationPath': '/pages/assessment/qcategory',
          },]
      };
    } else if (this.selectedQuestionType.id == '2') {
      this.uploadsheetLink = 'assets/images/Template_matching_choice.xlsx';
      this.title = 'Matching Question Upload';
      this.header  = {
        title:'Matching Question Upload',
        btnsSearch: true,
        searchBar: false,
        quesDrop:true,
        dropId:'id',
        dropName:'name',
        btnBackshow: true,
        showBreadcrumb: true,
        btnNameupload: true,
        uploadsheetLink : 'assets/images/Template_matching_choice.xlsx',
        breadCrumbList:[
          {
            'name': 'Assessment',
            'navigationPath': '/pages/assessment',
          },
          {
            'name': 'Question Bank Category',
            'navigationPath': '/pages/assessment/qcategory',
          },]
      };
    }
    else if (this.selectedQuestionType.id == '3') {
      this.uploadsheetLink = 'assets/images/Template_sequnce_question.xlsx';
      this.title = 'Sequence Question Upload';
      this.header  = {
        title:'Sequence Question Upload',
        btnsSearch: true,
        quesDrop:true,
        dropId:'id',
        dropName:'name',
        btnBackshow: true,
         showBreadcrumb: true,
         btnNameupload: true,
        uploadsheetLink :'assets/images/Template_sequnce_question.xlsx',
        breadCrumbList:[
          {
            'name': 'Assessment',
            'navigationPath': '/pages/assessment',
          },
          {
            'name': 'Question Bank Category',
            'navigationPath': '/pages/assessment/qcategory',
          },]
      };
    }
    else {
      this.uploadsheetLink = 'assets/images/Template_short_questions.xlsx';
      this.title = 'Short Question Upload';
      this.header  = {
        title:'Short Question Upload',
        btnsSearch: true,
        quesDrop:true,
        dropId:'id',
        dropName:'name',
        btnBackshow: true,
         showBreadcrumb: true,
         btnNameupload: true,
         uploadsheetLink :'assets/images/Template_short_questions.xlsx',
        breadCrumbList:[
          {
            'name': 'Assessment',
            'navigationPath': '/pages/assessment',
          },
          {
            'name': 'Question Bank Category',
            'navigationPath': '/pages/assessment/qcategory',
          },]
      };
    }
  }


  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }


  selectQuestionType(data) {
    this.cancelFile();
    this.selectedQuestionType = data;
    console.log('Uploaded Data value passed', data);
    this.changeLinkandTitle();
  }

  disableUploadButton: boolean = true;
  readFileUrl(event: any) {
    this.disableUploadButton = false;
    this.uploadedData = [];
    this.result = [];
    const validExts = new Array('.xlsx', '.xls');
    let fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
      // var toast: Toast = {
      //   type: 'error',
      //   title: 'Invalid file selected!',
      //   body: 'Valid files are of ' + validExts.toString() + 'types.',
      //   showCloseButton: true,
      //   // tapToDismiss: false,
      //   timeout: 2000,
      // };
      // this.toasterService.pop(toast);
      this.presentToast('warning', 'Valid file types are ' + validExts.toString());
      this.cancelFile();
    } else {
      // return true;
      if (event.target.files && event.target.files[0]) {
        this.fileName = event.target.files[0].name;
        // read file from input
        this.fileReaded = event.target.files[0];

        if (this.fileReaded != '' || this.fileReaded != null || this.fileReaded != undefined) {
          this.enableUpload = true;
        }

        let file = event.target.files[0];
        this.bulkUploadQuestion = event.target.files[0];
        console.log('this.bulkUploadAssetDataFileRead', this.bulkUploadQuestion);

        this.xlsxToJsonService.processFileToJson({}, file).subscribe(data => {
          this.resultSheets = Object.getOwnPropertyNames(data['sheets']);
          console.log('File Property Names ', this.resultSheets);
          if (this.resultSheets.length > 0) {
            let sheetName = this.resultSheets[0];
            this.result = data['sheets'][sheetName];
            console.log('dataSheet', data);

            if (this.result.length > 0) {
              this.uploadedData = this.result;
            }
          }
          else {
            this.toastr.warning('File is empty', 'Warning');
          }
        });
        var reader = new FileReader();
        reader.onload = (event: ProgressEvent) => {
          this.fileUrl = (<FileReader>event.target).result;
          /// console.log(this.fileUrl);
        };
        reader.readAsDataURL(event.target.files[0]);
      }
    }
  }
  showInvalidExcel: boolean = false;
  showTable: boolean = false;

  uploadSheet() {

    this.disableUploadButton = false;
    this.submitDisabled = false;
    // this.statusData =[];
    // this.showTable = true;
    // var numbers = /^[0-9]+$/;
    this.hideReason = false;
    // console.log('this.uploadedData', this.uploadedData);
    if (this.uploadedData.length > 0) {
      // this.AllEmployess.some((item,i) => {
      if (this.selectedQuestionType.id === '1') {
        this.checkForChoice();
      }
      if (this.selectedQuestionType.id === '2') {
        this.checkForMatching();
      }
      if (this.selectedQuestionType.id === '3') {
        this.checkForSequence();
      }
      if (this.selectedQuestionType.id === '4') {
        this.checkForShort();
      }
      // this.showTable = true;
      console.log('this.uploadedData', this.uploadedData);
    }



  }

  checkForChoice() {
    this.uploadedData.forEach((data, j) => {
      // console.log("J ==>", j);
      var isValid = true;
      if (this.isEmptyString(data['Category name'])) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Category name is required';
        isValid = false;
        return;
      }
      if (this.isEmptyString(data.IsCorrect)) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'IsCorrect is required';
        isValid = false;
        return;
      }
      if (this.isBinary(data.IsCorrect)) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'IsCorrect option is invalid.  Please enter 0 or 1';
        isValid = false;
        return;
      }
      if (this.isEmptyString(data.IsShuffle)) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'IsShuffle is required';
        isValid = false;
        return;
      }

      if (this.isBinary(data.IsShuffle)) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'IsShuffle is should be either 1 or 0';
        isValid = false;
        return;
      }

      if (this.isEmptyString(data.Level)) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Level is required';
        isValid = false;
        return;
      }
      if (this.isEmptyString(data.OptionChoiceType)) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'OptionChoiceType is required';
        isValid = false;
        return;
      }
      if (this.isEmptyString(data.Penality)) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Penality is required';
        isValid = false;
        return;
      }
      if (this.isScore(data.Penality)) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Penality should be a number and should be between 0 and 99';
        isValid = false;
        return;
      }
      if (this.isEmptyString(data.QueOption) && this.isEmptyString(data.QueOptionRef)) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'QueOption or QueOptionRef is required';
        isValid = false;
        return;
      }
      if (this.isEmptyString(data['Question name'])) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Question name is required';
        isValid = false;
        return;
      }
      if (this.isEmptyString(data['Question text']) && this.isEmptyString(data['ContentRef'])) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Question text or ContentRef is required';
        isValid = false;
        return;
      }
      if (this.isEmptyString(data['Score'])) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Score is required';
        isValid = false;
        return;
      }
      if (this.isScore(data['Score'])) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Score should be a number and should be between 0 and 99';
        isValid = false;
        return;
      }
      if (this.isEmptyString(data['Visible'])) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Visible field is required';
        isValid = false;
        return;
      }

      if (this.isBinary(data['Visible'])) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Visible field is invalid.  Please enter 0 or 1.';
        isValid = false;
        return;
      }
      if (this.isEmptyString(data['Tags'])) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Tags is required.';
        isValid = false;
        return;
      }

      if (isValid) {
        this.uploadedData[j].status = 'Valid';
        this.uploadedData[j].reason = '-';
        return;
      }
    });
    this.sortValidInvalidUploadSheet();
  }

  checkForMatching() {
    console.log('checkForMatching');
    this.uploadedData.forEach((data, j) => {
      // console.log("J ==>", j);
      var isValid = true;
      if (this.isEmptyString(data['Category name'])) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Category name is required';
        isValid = false;
        return;
      }
      if (this.isEmptyString(data.Level)) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Level is required';
        isValid = false;
        return;
      }
      if (this.isEmptyString(data.Penality)) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Penality is required';
        isValid = false;
        return;
      }
      if (this.isScore(data.Penality)) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Penality should be a number and should be between 0 and 99';
        isValid = false;
        return;
      }

      if (this.isEmptyString(data.QueOption) && this.isEmptyString(data.QueOptionRef)) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'QueOption is required';
        isValid = false;
        return;
      }

      if (this.isEmptyString(data.QueOptionMatch)) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'QueOptionMatch is required';
        isValid = false;
        return;
      }

      if (this.isEmptyString(data['Question name'])) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Question name is required';
        isValid = false;
        return;
      }
      if (this.isEmptyString(data['Question text']) && this.isEmptyString(data['ContentRef'])) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Question text or Content Ref is required';
        isValid = false;
        return;
      }
      if (this.isEmptyString(data['Score'])) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Score is required';
        isValid = false;
        return;
      }
      if (this.isScore(data['Score'])) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Score should be a number and should be between 0 and 99';
        isValid = false;
        return;
      }
      if (this.isEmptyString(data['Visible'])) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Visible field is invalid.  Please enter 0 or 1.';
        isValid = false;
        return;
      }

      if (this.isBinary(data['Visible'])) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Visible should should be either 1 or 0';
        isValid = false;
        return;
      }
      if (this.isEmptyString(data['Tags'])) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Tags is required.';
        isValid = false;
        return;
      }

      if (isValid) {
        this.uploadedData[j].status = 'Valid';
        this.uploadedData[j].reason = '-';
        return;
      }
    });
    this.sortValidInvalidUploadSheet();
  }

  checkForSequence() {
    this.uploadedData.forEach((data, j) => {
      // console.log("J ==>", j);
      var isValid = true;
      if (this.isEmptyString(data['Category name'])) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Category name is required';
        isValid = false;
        return;
      }
      if (this.isEmptyString(data.Level)) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Level is required';
        isValid = false;
        return;
      }
      if (this.isEmptyString(data.Penality)) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Penality is required';
        isValid = false;
        return;
      }
      if (this.isScore(data.Penality)) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Penality should be a number and should be between 0 and 99';
        isValid = false;
        return;
      }
      if (this.isEmptyString(data.QueOption)) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'QueOption is required';
        isValid = false;
        return;
      }

      if (this.isEmptyString(data['Question name'])) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Question name is required';
        isValid = false;
        return;
      }
      if (this.isEmptyString(data['Question text']) && this.isEmptyString(data['ContentRef'])) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Question text or ContentRef is required';
        isValid = false;
        return;
      }
      if (this.isEmptyString(data['Score'])) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Score is required';
        isValid = false;
        return;
      }
      if (this.isScore(data['Score'])) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Score should be a number and should be between 0 and 99';
        isValid = false;
        return;
      }
      if (this.isEmptyString(data['SequenceNo'])) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'SequenceNo is required';
        isValid = false;
        return;
      }
      if (this.isScore(data['SequenceNo'])) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'SequenceNo should be a number and should be between 0 and 99';
        isValid = false;
        return;
      }


      if (this.isEmptyString(data['Visible'])) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Visible field is required';
        isValid = false;
        return;
      }

      if (this.isBinary(data['Visible'])) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Visible field is invalid.  Please enter 0 or 1.';
        isValid = false;
        return;
      }
      if (this.isEmptyString(data['Tags'])) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Tags is required.';
        isValid = false;
        return;
      }

      if (isValid) {
        this.uploadedData[j].status = 'Valid';
        this.uploadedData[j].reason = '-';
        return;
      }
    });
    this.sortValidInvalidUploadSheet();
  }

  checkForShort() {
    this.uploadedData.forEach((data, j) => {
      // console.log("J ==>", j);
      var isValid = true;
      if (this.isEmptyString(data['Category name'])) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Category name is required';
        isValid = false;
        return;
      }
      if (this.isEmptyString(data.Level)) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Level is required';
        isValid = false;
        return;
      }
      if (this.isEmptyString(data.MaxLength)) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'MaxLength is required';
        isValid = false;
        return;
      }
      if (this.isNumber(data.MaxLength)) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'MaxLength should be number';
        isValid = false;
        return;
      }
      if (this.isMax(data.MaxLength)) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'MaxLength should be less 1000';
        isValid = false;
        return;
      }
      if (this.isEmptyString(data.MinLength)) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'MinLength is required';
        isValid = false;
        return;
      }
      if (this.isNumber(data.MinLength)) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'MinLength should be a number';
        isValid = false;
        return;
      }
      if (this.isMin(data.MinLength)) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'MinLength should be greater than 1';
        isValid = false;
        return;
      }
      if (this.checkForMinMax(data.MinLength, data.MaxLength)) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'MinLength and MaxLength should not be equal';
        isValid = false;
        return;
      }
      if (this.isEmptyString(data['Question name'])) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Question name is required';
        isValid = false;
        return;
      }
      if (this.isEmptyString(data['Question text'])) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Question text is required';
        isValid = false;
        return;
      }
      if (this.isEmptyString(data['Visible'])) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Visible field is required';
        isValid = false;
        return;
      }

      if (this.isBinary(data['Visible'])) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Visible field is invalid.  Please enter 0 or 1.';
        isValid = false;
        return;
      }
      if (this.isEmptyString(data['Tags'])) {
        this.uploadedData[j].status = 'Invalid';
        this.uploadedData[j].reason = 'Tags is required.';
        isValid = false;
        return;
      }

      if (isValid) {
        this.uploadedData[j].status = 'Valid';
        this.uploadedData[j].reason = '-';
        return;
      }
    });
    this.sortValidInvalidUploadSheet();
  }

  sortValidInvalidUploadSheet() {
    this.validdata = [];
    this.invaliddata = [];
    // this.validDataFinal = [];
    this.uploadedData.forEach(element => {
      if (element.status === 'Valid') {
        this.validdata.push(element);
      } else {
        this.invaliddata.push(element);
      }
    });
    console.log('ValidData', this.validdata);
    console.log('Invalid Data', this.invaliddata);
    if (this.invaliddata.length > 0) {
      this.showInvalidExcel = true;
    } else {
      this.showInvalidExcel = false;
    }
    this.showTable = true;
    this.cdf.detectChanges();
  }

  submit() {
    this.uploadedData = [];
    const param = {
      'option': JSON.stringify(this.validdata),
      'tId': this.tenantId,
      'qFormatId': parseInt(this.selectedQuestionType.id, 10),
      'userModid': this.userId,
    };
    this.spinner.show();
    this.uploadQuestion.submitBulkQuestion(param).then(res => {
      // let data = res['data'];
      if (res['type'] == true) {
        if (res['data']) {
          this.uploadedData = res['data'];
          console.log('this.statusData:', this.uploadedData);
        }
        this.hideReason = false;
        // console.log('AllEdgeEmployessDAM:', this.statusData);
        if (this.uploadedData.length > 0) {
          // this.invaliddata.forEach(item => {
          //   this.uploadedData.push(item);
          // });
          // this.invaliddata =[];
          // this.validData =[];
          // this.uploadedData.forEach(element => {
          //   if (element.status === 'Invalid') {
          //     this.invaliddata.push(element);
          //   } else {
          //     this.validDataFinal.push(element);
          //   }
          // });
          this.sortValidInvalidUploadSheet();
          // if (this.invaliddata.length > 0) {
          //   this.showInvalidExcel = true;
          // }
          // this.showTable = true;
          // this.cdf.detectChanges();
        }
        this.spinner.hide();
        this.submitDisabled = true;
        this.presentToast('success', 'File uploaded successfully')
        // this.toastr.success('Question Added successfully', 'Success');
      } else {
        this.presentToast('error', 'File uploaded successfully')
      }

    }).catch(err => {
      this.submitDisabled = false;
      this.spinner.hide();
      this.toastr.error('Please Contact Site Adminstrator', 'Error');
      this.presentToast('error', 'File uploaded successfully')
    });

  }


  exportToExcelInvalid() {
    this.exportService.exportAsExcelFile(this.invaliddata, 'Invalid Data')
  }

  back() {
    // this.router.navigate(['../'], { relativeTo: this.routes });
    window.history.back();
  }

  cancelFile() {
    // this.bulkUploadAssetData = null;
    if (this.fileUpload) {
      if (this.fileUpload.nativeElement) {
        this.fileUpload.nativeElement.value = null;
      }
    }
    this.enableUpload = false;
    this.showTable = false;
    this.showInvalidExcel = false;
    this.fileName = 'You can drag and drop file here to add questions';
    this.selectFileTitle = 'No file chosen';
    this.disableUploadButton = true;
    this.cdf.detectChanges();
  }

  isBinary(value: string | number): boolean {
    // var z1 = /^[0-9]*$/;
    // if (z1.test(value.toString())) { 

    // }
    // else{
    //   return true;
    // }
    if (parseInt(value.toString()) == 0 || parseInt(value.toString()) == 1) {
      return false;
    }
    else {
      return true;
    }
  }
  isScore(value: string | number): boolean {
    var z1 = /^[0-9]*$/;
    if (z1.test(value.toString())) {
      let convertedInt = parseInt(value.toString());
      if (convertedInt >= 0 && convertedInt < 100) {
        return false;
      }
    }
    else {
      return true;
    }
  }

  isEmptyString(value): boolean {
    return (value == null || value === '');
  }
  isNumber(value: string | number): boolean {
    var z1 = /^[0-9]*$/;
    if (z1.test(value.toString())) {
      return false;
    }
    else {
      return true;
    }
  }
  isMin(value: string | number): boolean {
    let convertedInt = parseInt(value.toString());
    if (convertedInt > 0) {
      return false;
    }
    else {
      return true;
    }
  }
  isMax(value: string | number): boolean {
    let convertedInt = parseInt(value.toString());
    if (convertedInt < 1000) {
      return false;
    }
    else {
      return true;
    }
  }
  checkForMinMax(min: number, max: number) {
    let minNum = parseInt(min.toString());
    let maxNum = parseInt(max.toString());
    if (minNum != maxNum) {
      return false;
    }
    else {
      return true;
    }
  }

}
