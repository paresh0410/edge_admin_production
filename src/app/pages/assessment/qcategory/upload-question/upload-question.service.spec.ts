import { TestBed } from '@angular/core/testing';

import { UploadQuestionService } from './upload-question.service';

describe('UploadQuestionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UploadQuestionService = TestBed.get(UploadQuestionService);
    expect(service).toBeTruthy();
  });
});
