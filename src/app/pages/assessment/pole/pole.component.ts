import { Component, ViewEncapsulation,ElementRef, ViewChild  } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { ToastrService } from 'ngx-toastr';
import { Router, NavigationStart, Routes, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { poleService } from './pole.service';
import { AddeditpoleService } from './addEditpole/addEditpole.service';
// import { questionsService } from './questions/questions.service';
import { BaseChartDirective } from 'ng2-charts/ng2-charts';
import { NgxSpinnerService } from 'ngx-spinner';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { DatePipe } from '@angular/common';
import { webApi } from '../../../service/webApi';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { Card } from '../../../models/card.model';
import { SuubHeader } from '../../components/models/subheader.model';
import { noData } from '../../../models/no-data.model';
// import { ContentService } from '../content/content.service';

@Component({
  selector: 'pole',
  styleUrls: ['./pole.scss'],
  // template:'category works'
  templateUrl: './pole.html',
  encapsulation: ViewEncapsulation.None,
  providers: [DatePipe],
})
export class pole {
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  notFound: boolean = true;
  skeleton = false;
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No Polls defined at this time.",
    desc:"Polls will appear after they are designed by the instructor. Polls help the organisation to get consensus on any topic or situation or person.",
    titleShow:true,
    btnShow:true,
    descShow:true,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/reaction-how-to-add-a-poll',
  }
  query: string = '';
  public getData;
  loginUserdata;
  cat: any;
  poll: any = [];
  count:number=8
  cardModify: Card =   {
    flag: 'poll',
    titleProp : 'name',
    discrption: 'description',
    endDate: 'cDate',
    compleCount: 'compCount',
    totalCount: 'totalCount',
    date: 'oDate',
    image:'iconRef',
    hoverlable:   true,
    showBottomEnddate : true,
    showBottomdate:   true,
    showImage: true,
    bottomDiscription:   true,
    showBottomList:   true,
    showCompletedEnroll : true,
    option:   true,
    eyeIcon:   true,
    bottomDiv:   true,
    bottomTitle:   true,
    btnLabel: 'Edit',
    defaultImage:'assets/images/survey.png'
  };
  header: SuubHeader  = {
    title:'Poll',
    btnsSearch: true,
    placeHolder:'Search by poll name',
    searchBar: true,
    dropdownlabel: ' ',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: '',
    btnName3: '',
    btnAdd: 'Add Poll',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: true,
    filter: false,
    showBreadcrumb: true,
    breadCrumbList:[
      {
        'name': 'Reaction',
        'navigationPath': '/pages/reactions',
      },
    ]
  };
  errorMsg: string;

  // parentcat:any=[]

  // content1:any=[];

  search: any;
  currentPage: number;
  str: string;
  enablePole: boolean;
  // enableDisableTrainerModal: boolean=false;
  enablePoleData: any;
  enableDisablePoleModal: boolean=false;
  // loader:any;
  //protected service: CCategorydataService,
  constructor(private poleservice: poleService, private addPollService: AddeditpoleService, private datePipe: DatePipe,
    private router: Router,
    //  private toasterService: ToasterService,
     private spinner: NgxSpinnerService,
    private commonFunctionService: CommonFunctionsService,
    private toastr: ToastrService) {
    // this.loader = true;
    if (localStorage.getItem('LoginResData')) {
      this.loginUserdata = JSON.parse(localStorage.getItem('LoginResData'));
    }
    this.getPolls();
    this.search = {
      name: ''
    };
    // this.category;
    this.cat = {
      id: ''
    };


    // this.getCategories();
  }

  // getCategories(){
  //   this.categoryService.getCategories()
  //   .subscribe(rescompData => { 
  //     this.loader =false;
  //     this.category = rescompData.data[0];
  //     console.log('Category Result',this.category)
  //   },
  //   resUserError=>{
  //     this.loader =false;
  //     this.errorMsg = resUserError;
  //     this.notFound = true;
  //     // this.router.navigate(['**']);
  //   });
  // }
  noPoll: boolean = false;
  rowsData: any;

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }


  getPolls() {
    // this.spinner.show();
    let param = {
      'tenantId': this.loginUserdata.data.data.tenantId,
    }
    const _urlFetch:string = webApi.domain + webApi.url.getallpoll;
    this.commonFunctionService.httpPostRequest(_urlFetch,param)
    //this.poleservice.getPoll(param)
      .then(rescompData => {
        this.spinner.hide();
        this.skeleton = true;
        var result = rescompData;
        console.log('getAllpolls:', rescompData);
        if (result['type'] == true) {
          var rows = result['data'][0];
          this.poll = rows;
          this.skeleton = true;
          if (rows) {
            this.poll = this.getUnique(rows, 'id')
            for (let j = 0; j < this.poll.length; j++) {
              this.poll[j].oDate = this.formdate(this.poll[j].openDate);
              this.poll[j].cDate = this.formdate(this.poll[j].closingDate);
              this.poll[j].points = [];
              for (let i = 0; i < rows.length; i++) {
                if (this.poll[j].id == rows[i].id) {
                  if (rows[i].roleId) {
                    var data =
                    {
                      crid: 0,
                      roleId: rows[i].roleId,
                      pformatid: rows[i].dimension,
                      bcpoints: rows[i].bcPoints,
                      acpoitnts: rows[i].acPoints,
                    }
                    this.poll[j].points.push(data)
                  }

                }
              }
            }
          }
          this.displayArray = []
          this.addItems(0,this.sum,'push',this.poll);
          console.log('this.poll', this.poll);
          this.skeleton = true;

        } else {
          // this.notFound = true;
          // var toast: Toast = {
          //   type: 'error',
          //   //title: "Server Error!",
          //   body: "Something went wrong.please try again later.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          this.notFound = false;
          this.presentToast('error', '');
        }


      }, error => {
        this.spinner.hide();
        this.skeleton = true;
        this.notFound = false;
        // var toast: Toast = {
        //   type: 'error',
        //   //title: "Server Error!",
        //   body: "Something went wrong.please try again later.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
  }

  getUnique(arr, comp) {

    //store the comparison  values in array
    const unique = arr.map(e => e[comp]).
      // store the keys of the unique objects
      map((e, i, final) => final.indexOf(e) === i && i)
      // eliminate the dead keys & return unique objects
      .filter((e) => arr[e]).map(e => arr[e]);

    return unique

  }
  formdate(date) {

    if (date) {
      // const months = ["JAN", "FEB", "MAR","APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
      // var day = date.getDate();
      // var monthIndex = months[date.getMonth()];
      // var year = date.getFullYear();
      // return day + '_' + monthIndex + '_' +year;
      var formatted = this.datePipe.transform(date, 'dd-MM-yyyy');
      return formatted;
    }
  }
  clear() {
    if(this.str.length>=3){
    this.search = {};
    this.header.searchtext = ''
    this.search = {};
    this.str = '';
    this.currentPage = 1;
    this.getPolls();
    }else {
      this.search={}
    }
  };

  back() {
    this.router.navigate(['/pages/reactions']);
  }

  content2(data, id) {
    var passData = {
      id: id,
      data: data
    }
    // this.questionsService.data = passData;
    this.router.navigate(['/pages/reactions/poll/add-edit-poll']);

  }

  public addeditpoll(data) {


    if (data == '') {
      var action = "ADD";
      this.addPollService.pollId = 0;
      this.addPollService.getDataforAddedit[0] = action;
      this.addPollService.getDataforAddedit[1] = null;
      this.router.navigate(['/pages/reactions/poll/add-edit-poll']);
    } else {
      var action = "EDIT";
      this.addPollService.pollId = data.id;
      this.addPollService.getDataforAddedit[0] = action;
      this.addPollService.getDataforAddedit[1] = data;
      this.router.navigate(['/pages/reactions/poll/add-edit-poll']);
      console.log('addPollService.getDataforAddedit[1]:', this.addPollService.getDataforAddedit[1]);

    }

  }

  gotoCardaddedit(data) {
    var action = "EDIT";
    this.addPollService.pollId = data.id;
    this.addPollService.getDataforAddedit[0] = action;
    this.addPollService.getDataforAddedit[1] = data;
    this.enablePoleData=data;
    this.router.navigate(['/pages/reactions/poll/add-edit-poll']);
    console.log('addPollService.getDataforAddedit[1]:', this.addPollService.getDataforAddedit[1]);
  }
  //     disableCourse:boolean = false;
  // disableContent(item,disableStatus){
  //   this.disableCourse = !this.disableCourse;
  // }

  disablePoll(data, status) {
    console.log('dataForChange:', data)
    if (data.visible == 1) {
      var pvisible = 0;
    } else if (data.visible == 0) {
      pvisible = 1;
    }

    this.spinner.show();
    console.log('data', data);
    let param = {
      "pId": data.id,
      "pvisible": pvisible,
      "tenantId": this.loginUserdata.data.data.tenantId,

    }
    const _urlChangePollStatus:string = webApi.domain + webApi.url.changepollstatus;
    this.commonFunctionService.httpPostRequest(_urlChangePollStatus,param)

    //this.poleservice.changePollStatus(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('UpdatePollResponse:', rescompData);
        if (result['type'] == true) {
          // var catUpdate: Toast = {
          //   type: 'success',
          //   title: "Poll Updated!",
          //   body: "Poll updated successfully.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('success', 'Poll updated');
          this.getPolls();
        } else {
          // var catUpdate: Toast = {
          //   type: 'error',
          //   title: "Poll Updated!",
          //   body: "Unable to update poll.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
        }
      }, error => {
        this.spinner.hide();
        // var toast: Toast = {
        //   type: 'error',
        //   //title: "Server Error!",
        //   body: "Something went wrong.please try again later.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
  }

  clickTodisable(data,i) {
    console.log('dataForChange:', data)
    // if (data.visible == 1) {
    //   var pvisible = 0;
    //   data.visible = 0;
    // } else if (data.visible == 0) {
    //   pvisible = 1;
    //   data.visible = 1;
    // }
  
    this.spinner.show();
    console.log('data', data);
    let param = {
      "pId": data.id,
      "pvisible": data.visible==0?1:0 ,
      "tenantId": this.loginUserdata.data.data.tenantId,

    }
    if(this.displayArray[i].visible==0){
      this.displayArray[i].visible=1
    }else{
      this.displayArray[i].visible=0
    }
    const _urlChangePollStatus:string = webApi.domain + webApi.url.changepollstatus;
    this.commonFunctionService.httpPostRequest(_urlChangePollStatus,param)

      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('UpdatePollResponse:', rescompData);
        if (result['type'] == true) {
          
          // this.presentToast('success', 'Poll updated');
          if(param.pvisible==1){      
           this.toastr.success( 'Poll Enabled Successfully','Success');
          }else{
            this.toastr.success('Poll Disabled Successfully','Success');
          }
          // this.getPolls();
        //  this.enableDisablePoleModal = false;
        // this.closeEnableDisablePoleModal();
        } else {
          
          this.presentToast('error', '');
        }
      }, error => {
        this.spinner.hide();
        this.enableDisablePoleModal = false;
        this.presentToast('error', '');
        // this.closeEnableDisablePoleModal();
      });
  }
  onSearch(event){
    this.search.name = event.target.value

  }
  ngOnDestroy(){
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0){
      e1[0].classList.add('with-scroll');
    }
  }
  ngAfterContentChecked() {
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0)
    {
      e1[0].classList.remove('with-scroll');
    }

   }
   conentHeight = '0px';
   offset: number = 100;
   ngOnInit() {
     this.conentHeight = '0px';
     const e = document.getElementsByTagName('nb-layout-column');
     console.log(e[0].clientHeight);
     // this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
     if (e[0].clientHeight > 1300) {
       this.conentHeight = '0px';
       this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
     } else {
       this.conentHeight = String(e[0].clientHeight) + 'px';
     }
   }
 
   sum = 40;
   addItems(startIndex, endIndex, _method, asset) {
     for (let i = startIndex; i < endIndex; ++i) {
       if (asset[i]) {
         this.displayArray[_method](asset[i]);
       } else {
         break;
       }
 
       // console.log("NIKHIL");
     }
   }
   // sum = 10;
   onScroll(event) {
 
     let element = this.myScrollContainer.nativeElement;
     // element.style.height = '500px';
     // element.style.height = '500px';
     // Math.ceil(element.scrollHeight - element.scrollTop) === element.clientHeight
 
     if (element.scrollHeight - element.scrollTop - element.clientHeight < 5) {
       if (this.newArray.length > 0) {
         if (this.newArray.length >= this.sum) {
           const start = this.sum;
           this.sum += 50;
           this.addItems(start, this.sum, 'push', this.newArray);
         } else if ((this.newArray.length - this.sum) > 0) {
           const start = this.sum;
           this.sum += this.newArray.length - this.sum;
           this.addItems(start, this.sum, 'push', this.newArray);
         }
       } else {
         if (this.poll.length >= this.sum) {
           const start = this.sum;
           this.sum += 50;
           this.addItems(start, this.sum, 'push', this.poll);
         } else if ((this.poll.length - this.sum) > 0) {
           const start = this.sum;
           this.sum += this.poll.length - this.sum;
           this.addItems(start, this.sum, 'push', this.poll);
         }
       }
 
 
       console.log('Reached End');
     }
   }
   newArray =[];
   displayArray =[];
   fromSearch=false
    // this function is use to dynamicsearch on table
  searchData(event) {
      // this.notFound1 = false;
        this.noDataVal={
          margin:'mt-5',
          imageSrc: '../../../../../assets/images/no-data-bg.svg',
          title:"Sorry we couldn't find any matches please try again",
          desc:".",
          titleShow:true,
          btnShow:false,
          descShow:false,
          btnText:'Learn More',
          btnLink:''
        }
    var temData = this.poll;
    var val = event.target.value.toLowerCase();
    this.str=val;
    var keys = [];
    if(event.target.value == '')
    {
      this.displayArray =[];
      this.newArray = [];
      this.sum = 50;
      this.addItems(0, this.sum, 'push', this.poll);
    }else{
         // filter our data
         if(val.length>=3||val.length==0){
    // if (temData.length > 0) {
    //   keys = Object.keys(temData[0]);
    // }
    // var temp = temData.filter((d) => {
    //   for (const key of keys) {
    //     if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
    //       return true;
    //     }
    //   }
    // });
    const temp = this.poll.filter(function (d) {
      return String(d.name).toLowerCase().indexOf(val) !== -1 ||
        d.description.toLowerCase().indexOf(val) !== -1 ||
        // d.relayDate.toLowerCase().indexOf(val) !== -1 ||
        // d.manager.toLowerCase().indexOf(val) !== -1 ||
        !val
    })
    // update the rows
    this.displayArray =[];
    this.newArray = temp;
    this.sum = 50;
    this.addItems(0, this.sum, 'push', this.newArray);
  }
}
  }
  // closeEnableDisablePoleModal() {
  //   this.enableDisablePoleModal = false;
  // }
  // enableDisablePoleAction(actionType) {
  //   console.log(this.enablePoleData);
  //   if (actionType == true) {
  //     if (this.enablePoleData.pvisible == 1) {
  //       this.enablePoleData.pvisible = 0;
  //       var poleData = this.enablePoleData;
  //       this.clickTodisable(poleData);
  //     } else {
  //       this.enablePoleData.pvisible = 1;
  //       var poleData = this.enablePoleData;
  //       this.clickTodisable(poleData);
  //     }
  //   } else {
  //     this.closeEnableDisablePoleModal();
  //   }
  // }
  // clickTodisablePole(poleData) {
  //   if (poleData.visible == 1) {
  //     this.enablePole = true;
  //     this.enablePoleData = poleData;
  //     this.enableDisablePoleModal = true;
  //   } else {
  //     this.enablePole = false;
  //     this.enablePoleData = poleData;
  //     this.enableDisablePoleModal = true;
  //   }
  // }
}



