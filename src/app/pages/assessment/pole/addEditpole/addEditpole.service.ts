import {Injectable, Inject} from '@angular/core';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
 import {AppConfig} from '../../../../app.module';
 import { webApi } from '../../../../service/webApi';
 import { HttpClient } from "@angular/common/http";

@Injectable()
export class  AddeditpoleService {

  pollId:any;
  menuId = null;
  getDataforAddedit:any=[];
  private _urlAddeditPollSetting: string = webApi.domain + webApi.url.addeditpoll;
  private _urlGetPollUserTypes: string = webApi.domain + webApi.url.getpollusertypes;
  private _urlGetSurveyQuestionType:string = webApi.domain + webApi.url.getsurveyquestiontype;
  private _urlAddEditPollQuestion:string = webApi.domain + webApi.url.addeditpollquestion;
  private _urlGetExistingPollQuestion:string = webApi.domain + webApi.url.getpollexistingquestions;
  private _urlUpdateSurveyQuestionStatus:string = webApi.domain + webApi.url.changesurveyquestionstatus;
  private _urlGetPollQuestionDetails:string = webApi.domain + webApi.url.getpollquestiondetails;
  private _urlChangeSurveyQuestionOrder:string = webApi.domain + webApi.url.changesurveyquestionorder;
  private _urlAreaBulEnrol: string = webApi.domain + webApi.url.area_bulk_enrol;

  constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private _http: Http,private _httpClient:HttpClient){
      //this.busy = this._http.get('...').toPromise();
  }



  addEditPollSetting(param){
    return new Promise(resolve => {
      this._httpClient.post(this._urlAddeditPollSetting, param)
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}

getPollUserType(param){
  return new Promise(resolve => {
    this._httpClient.post(this._urlGetPollUserTypes, param)
    .subscribe(data => {
        resolve(data);
    },
    err => {
        resolve('err');
    });
});
}

getSurveyQuestionType(param){
  return new Promise(resolve => {
    this._httpClient.post(this._urlGetSurveyQuestionType, param)
    .subscribe(data => {
        resolve(data);
    },
    err => {
        resolve('err');
    });
});
}

addEditPollQuestion(param){
  return new Promise(resolve => {
    this._httpClient.post(this._urlAddEditPollQuestion, param)
    .subscribe(data => {
        resolve(data);
    },
    err => {
        resolve('err');
    });
});
}

getExistingPollQuestions(param){
  return new Promise(resolve => {
    this._httpClient.post(this._urlGetExistingPollQuestion, param)
    .subscribe(data => {
        resolve(data);
    },
    err => {
        resolve('err');
    });
});
}

UpdateSurveyQuestionStatus(param){
  return new Promise(resolve => {
    this._httpClient.post(this._urlUpdateSurveyQuestionStatus, param)
    .subscribe(data => {
        resolve(data);
    },
    err => {
        resolve('err');
    });
});
}

GetPollQuestionDetails(param){
return new Promise(resolve => {
  this._httpClient.post(this._urlGetPollQuestionDetails,param)
  .subscribe(data => {
      resolve(data);
  },
  err => {
      resolve('err');
  });
});
}

ChangeSurveyQuestionOrder(param){
return new Promise(resolve => {
  this._httpClient.post(this._urlChangeSurveyQuestionOrder,param)
  .subscribe(data => {
      resolve(data);
  },
  err => {
      resolve('err');
  });
});
}

areaBulkEnrol(param) {
  return new Promise(resolve => {
      this._httpClient.post(this._urlAreaBulEnrol, param)
          //.map(res => res.json())
          .subscribe(data => {
              resolve(data);
          },
              err => {
                  resolve('err');
              });
  });
}

  _errorHandler(error: Response){
    console.error(error);
    return Observable.throw(error || "Server Error")
  }

}
