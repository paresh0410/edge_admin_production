import {Injectable, Inject} from '@angular/core';
import {Http, Response, Headers, RequestOptions, Request} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppConfig} from '../../../../../app.module';
import {webApi} from '../../../../../service/webApi';
import { HttpClient } from "@angular/common/http";

@Injectable()

export class engageService {

  public data: any;
  request: Request;

  private _url:string = "";
  private _urlfetchnoteventdropdown = webApi.domain + webApi.url.getnotevent;
  private _urlfetchnottemplatedropdown = webApi.domain + webApi.url.dropdownnotifytemplate;
  private _urlInsertUpdateSurveyNotification: string = webApi.domain + webApi.url.insertupdateladdernotification;
  private _urlGetAllPollNotification: string = webApi.domain + webApi.url.getpollnotification;
  private _urlGetNotificationTags: string = webApi.domain + webApi.url.getNotificationTags;

  constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private _http: Http,private _httpClient:HttpClient){
      //this.busy = this._http.get('...').toPromise();
  }

  getData() {
     let url:any = `${this.config.FINAL_URL}`+this._url;
     // let url:any = `${this.config.DEV_URL}`+this._urlFilter;
     let headers = new Headers({ 'Content-Type': 'application/json' });
     let options = new RequestOptions({ headers: headers });
     //let body = JSON.stringify(user);
     return this._http.post(url, options ).map((res: Response) => res.json());
  }

  getNotEventDropdown(param) {
       return new Promise(resolve => {
         this._httpClient.post(this._urlfetchnoteventdropdown, param)
         //.map(res => res.json())
         .subscribe(data => {
             resolve(data);
         },
         err => {
             resolve('err');
         });
     });
}

getNottemplateDropdown(param){
   return new Promise(resolve => {
      this._httpClient.post(this._urlfetchnottemplatedropdown, param)
      //.map(res => res.json())
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}

getPollNotification(param){
    return new Promise(resolve => {
       this._httpClient.post(this._urlGetAllPollNotification, param)
       //.map(res => res.json())
       .subscribe(data => {
           resolve(data);
       },
       err => {
           resolve('err');
       });
   });
 }

insertUpdateSurveyNotifications(param) {
    // let url: any = this._urlInsertUpdateCourseNotification;
    // return this._http.post(url, param)
    //    .map((response: Response) => response.json())
    //    .catch(this._errorHandler);

    return new Promise(resolve => {
       this._httpClient.post(this._urlInsertUpdateSurveyNotification, param)
       //.map(res => res.json())
       .subscribe(data => {
           resolve(data);
       },
       err => {
           resolve('err');
       });
   });
 }
  _errorHandler(error: Response){
     console.error(error);
     return Observable.throw(error || "Server Error")
  }

  getNotificationsTags(param) {
    // let url:any = this._urlGetCourseNotification;
    // return this._http.post(url,param)
    //     .map((response:Response)=>response.json())
    //     .catch(this._errorHandler);

    return new Promise(resolve => {
       this._httpClient.post(this._urlGetNotificationTags, param)
       //.map(res => res.json())
       .subscribe(data => {
           resolve(data);
       },
       err => {
           resolve('err');
       });
   });
  }

}
