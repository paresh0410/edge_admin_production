// import { routing }       from './addEditUser.routing';
import { Addeditpole } from './addEditpole.component';
import {  AddeditpoleService } from './addEditpole.service';
import { ComponentModule } from '../../../../component/component.module';

import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
 import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
 import { JoditAngularModule } from 'jodit-angular';
//import { AgGridModule } from 'ag-grid-angular';
// import 'ag-grid-enterprise';
import 'ag-grid-community';

import { MyDatePickerModule } from 'mydatepicker';
// import { TruncateModule } from 'ng2-truncate';
// import { TabsModule } from 'ngx-tabs';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { TagInputModule } from 'ngx-chips';
import { NbTabsetModule } from '@nebular/theme';
import { TranslateModule } from '@ngx-translate/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SortablejsModule } from 'angular-sortablejs';
import { engageComponent } from './engage/engage.component';
import { engageService } from './engage/engage.service';


import { enrolmentComponent } from './enrolment/enrolment.component';
import { enrolService } from './enrolment/enrolment.service';

import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { QuillModule } from 'ngx-quill'
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'


@NgModule({
  imports: [
    CommonModule,
    // routing,
    FormsModule,
   // AgGridModule.withComponents([]),
    // TabsModule,
    ReactiveFormsModule,
    Ng2SmartTableModule,
    TagInputModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    NbTabsetModule,
    TranslateModule,
    NgxDatatableModule,
    SortablejsModule,
        BsDatepickerModule.forRoot(),
    DatepickerModule.forRoot(),
    AngularMultiSelectModule,
    QuillModule,
    JoditAngularModule,
    ComponentModule
  ],
  declarations: [
     Addeditpole,
      enrolmentComponent,
      engageComponent
  ],
  providers: [
     AddeditpoleService,
     engageService,
     enrolService
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class  AddeditpoleModule {}
