import { Component, Directive, ViewEncapsulation, forwardRef, Attribute, OnChanges, SimpleChanges, Input, ViewChild, ViewContainerRef, ChangeDetectorRef } from '@angular/core';
import { AddeditpoleService } from './addEditpole.service';
import { NG_VALIDATORS, Validator, Validators, AbstractControl, ValidatorFn } from '@angular/forms';
import { LocalDataSource } from 'ng2-smart-table';
import { Router, NavigationStart, ActivatedRoute } from '@angular/router';
import { OwlDateTimeModule } from 'ng-pick-datetime';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { SortablejsOptions } from 'angular-sortablejs';
import { AddeditfeedbackService } from '../../feedback/addEditfeedback/addEditfeedback.service';
import { webApi } from '../../../../service/webApi';
import { webAPIService } from '../../../../service/webAPIService';
import { NgxSpinnerService } from 'ngx-spinner';
import { AddEditCourseContentService } from './../../../plan/courses/addEditCourseContent/addEditCourseContent.service'
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { XlsxToJsonService } from "../../../plan/users/uploadusers/xlsx-to-json-service";
import { JsonToXlsxService } from '../../../coaching/participants/bulk-upload-coaching/json-to-xlsx.service';

import { DatePipe } from '@angular/common';
import * as _moment from 'moment';
import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';
import { SuubHeader } from '../../../components/models/subheader.model';
import { CommonFunctionsService } from '../../../../service/common-functions.service';
import { BrandDetailsService } from '../../../../service/brand-details.service';
import { noData } from '../../../../models/no-data.model';
const moment = (_moment as any).default ? (_moment as any).default : _moment;

export const MY_CUSTOM_FORMATS = {
  fullPickerInput: 'DD-MM-YYYY h:mm:ss A',
  parseInput: 'DD-MM-YYYY h:mm:ss A',
  datePickerInput: 'DD-MM-YYYY',
  timePickerInput: 'LT',
  monthYearLabel: 'MMM YYYY',
  dateA11yLabel: 'LL',
  monthYearA11yLabel: 'MMMM YYYY'
};
@Component({
  selector: 'addEditpole',
  templateUrl: './addEditpole.html',
  styleUrls: ['./addEditpole.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [DatePipe,
    { provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE] },
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS }],
})

export class Addeditpole {
  @ViewChild('fileUpload') fileUpload: any;
  @ViewChild('manualTable') manualTable: any;
  @ViewChild(DatatableComponent) tableDataManual: DatatableComponent;
  @ViewChild('myTable') table: any;
  noDataVal:noData={
		margin:'mt-3',
		imageSrc: '../../../../../assets/images/no-data-bg.svg',
		title:"No Enrollment added under Poll.",
		desc:"",
		titleShow:true,
		btnShow:true,
		descShow:false,
		btnText:'Learn More',
		btnLink:'https://faq.edgelearning.co.in/kb/PLJB520FTAVzdfZc',
	  }
  query: string = '';
  public getData;
  // QuestionListOptions: SortablejsOptions = {
  //   group: {
  //     name: 'formdata.pque',

  //   },
  //      sort: true,
  //   handle: '.drag-handle',
  // };
  AttemptedUsers: any = [];
  rows: any[] = [];
  rows1: any = [];
  expanded: any = {};
  timeout: any;

  questionarray: any = [];
  Questions: boolean = false;
  Settings: boolean = true;
  Responses: boolean = false;
  Analysis: boolean = false;
  Enrolement: boolean = false;
  Notification: boolean = false;
  questionshow: boolean = false;
  title: any;
  formdata: any = {};
  profileUrl: any = 'assets/images/category.jpg';
  // result: any;
  errorMsg: any;
  random: any;
  temp1: any;
  header: SuubHeader;

  loginUserdata = JSON.parse(localStorage.getItem('LoginResData'));

  qdata = {
    queaddtype: '',
    quetype: '',
  }


  questiontype = [
    {
      id: 1,
      name: 'Long Text'
    },
    {
      id: 2,
      name: 'Multiple'
    },
  ]
  // rows:any=[];
  selected: any = [];

  questiondata = {
    pqid: '',
    pqname: '',
    pmand: '',
    pqtypeid: '',
    qansmin: '',
    qansmax: '',
    pqtypeName: '',
    pqdata: []

  }

  analyticsData = [
    {
      aqid: 1,
      aqname: 'Question 1',
      aqfequency: [{
        option: 10
      }, {
        option: 20
      },
      {
        option: 40
      }],
      aqmean: 35,
      aqsd: '12.472191289246471285'

    },
    {
      aqid: 2,
      aqname: 'Question 2',
      aqfequency: [
        {
          option: 10
        },
        {
          option: 2
        },
        {
          option: 400
        },
        {
          option: 90,
        }
      ],
      aqmean: 125,
      aqsd: '162.17505973484332279'

    },
    {
      aqid: 3,
      aqname: 'Question 3',
      aqfequency: [{
        option: 1
      }, {
        option: 199
      },
      {
        option: 400
      }],
      aqmean: 200,
      aqsd: '162.89260265586034279'

    },
    {
      aqid: 4,
      aqname: 'Question 4',
      aqfequency: [{
        option: 10
      }, {
        option: 90
      },
      {
        option: 40
      }],
      aqmean: 46,
      aqsd: '32.998316455372217806'

    },
    {
      aqid: 5,
      aqname: 'Question 5',
      aqfequency: [{
        option: 2
      }, {
        option: 200
      },
      ],
      aqmean: 101,
      aqsd: '99'

    },
  ]

  responceData = [
    {
      rid: 1,
      runame: 'Vivek',
      rdate: '1/26/2019, 7:45 PM',
      rque: [
        {
          que: 1,
          quename: 'Question 1',
          queoption: 'option option option option ',
          v: ''
        },
        {
          que: 2,
          quename: 'Question 2',
          queoption: 'Agree ',
          queoptid: 1,
        },
        {
          que: 3,
          quename: 'Question 3',
          queoption: 'Disagree',
          queoptid: 3,
        },
      ]

    },
    {
      rid: 2,
      runame: 'Ronit',
      rdate: '1/28/2019, 7:45 PM',
      rque: [
        {
          que: 1,
          quename: 'Question 1',
          queoption: 'option option option option ',
          queoptid: ''
        },
        {
          que: 2,
          quename: 'Question 2',
          queoption: 'Agree ',
          queoptid: 1,
        },
        {
          que: 3,
          quename: 'Question 3',
          queoption: 'Disagree',
          queoptid: 3,
        },
      ]

    },
    {
      rid: 3,
      runame: 'Chetan',
      rdate: '1/20/2019, 7:45 PM',
      rque: [
        {
          que: 1,
          quename: 'Question 1',
          queoption: 'option option option option ',
          queoptid: ''
        },
        {
          que: 2,
          quename: 'Question 2',
          queoption: 'Agree ',
          queoptid: 1,
        },
        {
          que: 3,
          quename: 'Question 3',
          queoption: 'Disagree',
          queoptid: 3,
        },
      ]

    },
    {
      rid: 4,
      runame: 'Bhavesh',
      rdate: '1/19/2019, 7:45 PM',
      rque: [
        {
          que: 1,
          quename: 'Question 1',
          queoption: 'option option option option ',
          queoptid: ''
        },
        {
          que: 2,
          quename: 'Question 2',
          queoption: 'Agree ',
          queoptid: 1,
        },
        {
          que: 3,
          quename: 'Question 3',
          queoption: 'Disagree',
          queoptid: 3,
        },
      ]

    },
  ]


  labels: any = [
    { labelname: 'ECN', bindingProperty: 'ecn', componentType: 'text' },
    { labelname: 'FULL NAME', bindingProperty: 'fullname', componentType: 'text' },
    { labelname: 'EMAIL', bindingProperty: 'emailId', componentType: 'text' },
    { labelname: 'MOBILE', bindingProperty: 'phoneNo', componentType: 'text' },
    { labelname: 'D.0.E', bindingProperty: 'enrolDate', componentType: 'text' },
    { labelname: 'MODE', bindingProperty: 'enrolmode', componentType: 'text' },
    { labelname: 'ACTION', bindingProperty: 'btntext', componentType: 'button' },
  ];
  labelsPreview: any = [
    { labelname: 'STATUS', bindingProperty: 'status', componentType: 'text' },
    { labelname: 'ECN', bindingProperty: 'ecn', componentType: 'text' },
    { labelname: 'FULL NAME', bindingProperty: 'fullname', componentType: 'text' },
    { labelname: 'D.0.E', bindingProperty: 'enrolDate', componentType: 'text' },
  ];

  columns = [
    {
      prop: 'selected',
      name: '',
      sortable: false,
      canAutoResize: false,
      draggable: false,
      resizable: false,
      headerCheckboxable: true,
      checkboxable: true,
      width: 30
    },
    { prop: 'qpname', name: 'Name' },
    { prop: 'qpoint', name: 'Point' },
    { prop: 'qtype', name: 'Type' },
  ];
  /*****************************Settings************************ */
  credits: any = [
    {
      crid: 0,
      roleId: '',
      pformatid: '',
      bcpoints: '',
      acpoitnts: '',
    }
  ];

  pollSettingData: any = {
    pname: '',
    pdesc: '',
    picon: '',
    iconRef: '',
    prUsers: '',
    psResult: '',
    potime: '',
    pctime: '',
    craditpoints: this.credits,

  }
  /***************************************************** */


  addAction: boolean = false;
  userId: any;
  getDataForAddEdit: any = [];
  pollId: any;
  file: any = [];
  datasetPreview: any;
  preview: boolean = false;
  resultSheets: any = [];
  result: any = [];
  uploadedData: any = [];
  bulkUploadData: any = [];
  fileUrl: any;
  fileName: any;
  enableUpload: any;
  fileReaded: any;
  addEditModRes: any = [];
  tenantId: any;
  content: any = [];
  loader: any;
  backflag = 1;
  datep: any;
  isdata: any;
  searchsub: any;

  private xlsxToJsonService: XlsxToJsonService = new XlsxToJsonService();
  searchText: any;
  currentBrandData: any;
  constructor(protected service: AddeditpoleService,
    private commonFunctionService: CommonFunctionsService,
    // private toasterService: ToasterService,
    public cdf: ChangeDetectorRef, private router: Router, private route: ActivatedRoute,
    private feedservice: AddeditfeedbackService, private webApiService: webAPIService, private datePipe: DatePipe,
    public brandService: BrandDetailsService,
    private spinner: NgxSpinnerService, private courseDataService: AddEditCourseContentService,
    private http1: HttpClient, private toastr: ToastrService, private exportService: JsonToXlsxService) {
    this.getHelpContent();
    this.settingSubmit = true;
    if (this.service.pollId) {
      this.pollId = this.service.pollId;
    }
    if (localStorage.getItem('LoginResData')) {
      var userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', userData.data);
      this.userId = userData.data.data.id;
      this.tenantId = userData.data.data.tenantId;
    }

    this.getallDrop();
    this.getPollUserTypes();
    this.getDataForAddEdit = this.service.getDataforAddedit;
    this.content = this.getDataForAddEdit[1];
    console.log("POLLDETAIL", this.content);
    if (this.content) {
      this.pollId = this.content.id;
    }
    console.log('this.getDataForAddEdit', this.getDataForAddEdit);
    if (this.service.getDataforAddedit) {
      if (this.service.getDataforAddedit[0] == 'ADD') {
        this.addAction = true;
        this.pollSettingData = {
          pid: 0,
          pname: '',
          pdesc: '',
          iconRef: '',
          potime: '',
          pctime: '',
          prUsers: 1,
          psResult: '',
          craditpoints: [{
            crid: 0,
            roleId: '',
            pformatid: '',
            bcpoints: '',
            acpoitnts: '',
          }],

        }
        this.datep = new Date();
      } else if (this.service.getDataforAddedit[0] == 'EDIT') {
        this.addAction = false;
        this.pollSettingData = {
          pid: this.service.getDataforAddedit[1].id,
          pname: this.service.getDataforAddedit[1].name,
          pdesc: this.service.getDataforAddedit[1].description,
          iconRef: this.service.getDataforAddedit[1].iconRef,
          potime: new Date(this.service.getDataforAddedit[1].openDate),
          pctime: new Date(this.service.getDataforAddedit[1].closingDate),
          prUsers: this.service.getDataforAddedit[1].recordUsers,
          psResult: this.service.getDataforAddedit[1].showResults,
          craditpoints: this.service.getDataforAddedit[1].points,

        };
        const date = new Date();
        if (this.pollSettingData.potime > date) {
          this.datep = date;
        } else {
          this.datep = this.pollSettingData.potime;
        }
      }
    }

    // this.QuestionListOptions = {
    //   onUpdate: (event: any) => {
    //     this.submitNewSrveyQue();
    //   }
    // };

    // this.getExistingPollQuestion();
    //this.getSurveyQuestionType();


  }

  back() {
    if (this.backflag == 2) {
        this.header = {
          title: this.content?this.content.name:'Add Poll',
          btnsSearch: true,
          btnName2: 'Enrol',
          btnName3: 'Bulk Enrol',
          btnName2show: true,
          btnName3show: true,
          btnBackshow: true,
          showBreadcrumb: true,
          breadCrumbList: [
            {
              'name': 'Reaction',
              'navigationPath': '/pages/reactions',
            },
            {
              'name': 'Poll',
              'navigationPath': '/pages/reactions/poll',
            },]
        };
      this.showEnrolpage = false;
      this.showBulkpage = false;
      this.file = [];
      this.preview = false;
      this.fileName = 'Click here to upload an excel file to enrol ' + this.currentBrandData.employee.toLowerCase() + ' to this course'
      this.enableUpload = false;
      this.allEnrolUser();
    } else {
      window.history.back();
    }
    this.backflag = 1;

    // this.router.navigate(['/pages/reactions/poll']);
  }

  settingSubmit: boolean = false;
  questionSubmit: boolean = false;
  enrolSubmit: boolean = false;
  notificationSubmit: boolean = false;
  responseSubmit: boolean = false;
  analysisSubmit: boolean = false;

  SettingsTab: boolean;
  QuestionsTab: boolean;
  NotificationTab: boolean;
  EnrolementTab: boolean;
  /*********************tab selection and submit-back button show hide************************/
  selectTab(event) {
    console.log('event', event);
    console.log('event.tabTitle', event.tabTitle);
    if (event.tabTitle == 'Settings') {
        this.header = {
          title: this.content?this.content.name:'Add Poll',
          btnsSearch: true,
          btnName4: 'Save',
          btnName4show: true,
          btnBackshow: true,
          showBreadcrumb: true,
          breadCrumbList: [
            {
              'name': 'Reaction',
              'navigationPath': '/pages/reactions',
            },
            {
              'name': 'Poll',
              'navigationPath': '/pages/reactions/poll',
            },]
        };
      this.backflag = 1;
      this.SettingsTab = true;
      this.QuestionsTab = false;
      this.NotificationTab = false;
      this.EnrolementTab = false;
      this.spinner.show();
      setTimeout(() => {
        this.spinner.hide()
      }, 2000);
      // this.settingSubmit = true;
      // this.questionSubmit = false;
      // this.enrolSubmit = false;
      // this.notificationSubmit = false;
      // this.responseSubmit = false;
      // this.analysisSubmit = false;
    } else if (event.tabTitle == 'Questions') {
        this.header = {
          title: this.content?this.content.name:'Add poll',
          btnsSearch: true,
          btnName5: 'Add New Option',
          btnName5show: true,
          btnBackshow: true,
          showBreadcrumb: true,
          breadCrumbList: [
            {
              'name': 'Reaction',
              'navigationPath': '/pages/reactions',
            },
            {
              'name': 'Poll',
              'navigationPath': '/pages/reactions/poll',
            },]
        };
      this.backflag = 1;
      this.spinner.show();
      setTimeout(() => {
        this.spinner.hide()
      }, 2000);
      // this.settingSubmit = false;
      // this.questionSubmit = true;
      // this.enrolSubmit = false;
      // this.notificationSubmit = false;
      // this.responseSubmit = false;
      // this.analysisSubmit = false;
      this.SettingsTab = false;
      this.QuestionsTab = true;
      this.getExistingPollQuestion();
      this.NotificationTab = false;
      this.EnrolementTab = false;
    } else if (event.tabTitle == 'Enrol') {
        this.header = {
          title: this.content?this.content.name:'Add Poll',
          btnsSearch: true,
          btnName2: 'Enrol',
          btnName3: 'Bulk Enrol',
          btnName2show: true,
          btnName3show: true,
          btnBackshow: true,
          showBreadcrumb: true,
          breadCrumbList: [
            {
              'name': 'Reaction',
              'navigationPath': '/pages/reactions',
            },
            {
              'name': 'Poll',
              'navigationPath': '/pages/reactions/poll',
            },]
        };
        this.spinner.show();
        setTimeout(() => {
          this.spinner.hide()
        }, 2000);
      // this.settingSubmit = false;
      // this.questionSubmit = false;
      // this.enrolSubmit = true;
      // this.notificationSubmit = false;
      // this.responseSubmit = false;
      // this.analysisSubmit = false;
      this.backflag = 1;
      this.SettingsTab = false;
      this.QuestionsTab = false;
      this.NotificationTab = false;
      this.EnrolementTab = true;
      this.allEnrolUser();

    } else if (event.tabTitle == 'Notification') {
        this.header = {
          title: this.content?this.content.name:'Add Poll',
          btnsSearch: true,
          searchBar: true,
          searchtext: '',
          dropdownlabel: '',
          placeHolder:'Search',
          btnName6: 'Add Notification',
          btnName6show: true,
          btnBackshow: true,
          showBreadcrumb: true,
          breadCrumbList: [
            {
              'name': 'Reaction',
              'navigationPath': '/pages/reactions',
            },
            {
              'name': 'Poll',
              'navigationPath': '/pages/reactions/poll',
            },]
        };
        this.spinner.show();
        setTimeout(() => {
          this.spinner.hide()
        }, 2000);
      // this.settingSubmit = false;
      // this.questionSubmit = false;
      // this.enrolSubmit = false;
      // this.notificationSubmit = true;
      // this.responseSubmit = false;
      // this.analysisSubmit = false;
      this.backflag = 1;
      this.SettingsTab = false;
      this.QuestionsTab = false;
      this.NotificationTab = true;
      this.EnrolementTab = false;

    } else if (event.tabTitle == 'Responses') {

      this.settingSubmit = false;
      this.questionSubmit = false;
      this.enrolSubmit = false;
      this.notificationSubmit = false;
      this.responseSubmit = true;
      this.analysisSubmit = false;

    } else if (event.tabTitle == 'Analysis') {

      this.settingSubmit = false;
      this.questionSubmit = false;
      this.enrolSubmit = false;
      this.notificationSubmit = false;
      this.responseSubmit = false;
      this.analysisSubmit = true;

    }
    console.log('QuestionsTab', this.QuestionsTab);
  }



  Removeque(pid) {

    for (let i = 0; i < this.formdata.pque.length; i++) {
      if (pid == i) {
        this.formdata.pque.splice(i, 1)
      }
    }

  }


  qtypechange(j) {
    if (this.questiondata.pqdata.length > 0) {
      this.questiondata.pqdata = []
    }
    if (this.questiondata.pqdata.length == 0) {
      var qdata = {
        qaid: '',
        qans: ''
      }
      this.questiondata.pqdata.push(qdata)
    }


  }




  editQuestion(item) {
    console.log('item', item)
    this.questionshow = true;
    this.questiondata = {
      pqid: item.pqid,
      pqname: item.pqname,
      pmand: item.pmand,
      qansmax: '',
      qansmin: '',
      pqtypeid: item.pqtypeid,
      pqtypeName: item.pqtypeName,
      pqdata: item.pqdata

    };
  }

  save(qaddtype, qtype) {
    var questionarray = this.questionarray
    this.questionshow = false;
    if (qaddtype == 1) {
      if (this.selected.length > 0) {
        for (let j = 0; j < questionarray.length; j++) {
          for (let i = 0; i < this.selected.length; i++) {
            if (this.selected[i].qpid == questionarray[j].qpid) {
              this.selected.splice(i, 1)
            }
            // this.questionarray.push(this.selected[i])
          }
        }
        for (let i = 0; i < this.selected.length; i++) {
          this.questionarray.push(this.selected[i])
        }

      }
    }
    else
      if (qaddtype == 2) {

        // this.random.qtype=
        this.selected = [];
        this.questionarray.push(this.random)
      }



  }
  Qtypechange(qdata) {
    if (qdata == 2) {
      this.random = {
        qpid: '',
        qpname: 'Random',
        qpoint: '',
        qtype: '',
      }
    }
  }

  closeModel() {
    this.questionshow = false;
  }


  Backquestion() {
    this.questionshow = false;
    this.router.navigate(['/pages/reactions/poll']);
  }

  submit() {
    this.router.navigate(['/pages/reactions/poll']);
  }

  onDetailToggle(event) {
    console.log('Detail Toggled', event);
  }
  onPage(event) {
    // clearTimeout(this.timeout);
    // this.timeout = setTimeout(() => {
    //   // console.log('paged!', event);
    // }, 100);
  }

  onSelect({ selected }) {
    console.log('Selected Event', selected);
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    // console.log('Selected Data',this.selected);
    // this.userData = this.selected;
    // if(this.selected.length > 0){
    //   this.enrollDisabled = false;
    // }else{
    //   this.enrollDisabled = true;
    // }
  }

  onActivate(event) {
    // console.log('Activate Event', event);
    // if(event.type == 'click'){
    //   // console.log('Row CLick Event', event);
    //   console.log('Row Data',event.row);
    //   this.showLargeModal();
    // }
  }

  displayCheck(row) {
    // return row.name !== 'Ethel Price';
  }
  toggleExpandRow(row) {
    console.log('Toggled Expand Row!', row);
    this.table.rowDetail.toggleExpandRow(row);
    this.AttemptedUsers = row.rque;
  }


  searchfeedbackUser(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp1.filter(function (d) {
      return d.runame.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  /*********************************ollSettings*********************************** */
  roles: any = [];
  pointFormat: any = [];
  getallDrop() {
    var datadrop = {
      tId: this.loginUserdata.data.data.tenantId,
    }

    //this.spinner.show();

    this.feedservice.getallDropdown(datadrop)
      .then(rescompData => {
        // this.spinner.hide();
        console.log('rescompDataALLDrops:', rescompData)
        this.roles = rescompData['data'].roles;
        this.pointFormat = rescompData['data'].pointFormat;
        if (this.pollSettingData.craditpoints.length > 0) {
          for (let i = 0; i < this.pollSettingData.craditpoints.length; i++) {
            var credit = this.pollSettingData.craditpoints[i];
            this.roleTypeSelected(i, credit);
          }
        }
        console.log('quiz Drop Result', rescompData)
      },
        resUserError => {
          // this.spinner.hide();
          this.errorMsg = resUserError;
          // this.notFound = true;
          // this.router.navigate(['**']);
        });
  }

  userTypes: any = [];
  getPollUserTypes() {
    let param = {
      'tenantId': this.loginUserdata.data.data.tenantId
    }

    this.service.getPollUserType(param)
      .then(rescompData => {
        // this.spinner.hide();
        let result = rescompData;

        this.userTypes = result['data'][0];

        console.log('this.userTypes', this.userTypes)
      },
        resUserError => {
          // this.spinner.hide();
          this.errorMsg = resUserError;
          // this.notFound = true;
          // this.router.navigate(['**']);
        });
  }

  roleSelectedList = [];
  selectedRole = [];
  newPointFormat: any = [];
  item: any;
  disableIfLearner: boolean = false;
  roleTypeSelected(currentIndex, currentItem) {
    console.log('currentItem ', currentItem);
    console.log('this.pointFormat', this.pointFormat);
    if (currentItem.roleId == '8') {
      this.pollSettingData.craditpoints[currentIndex].pformatid = 1;
      this.disableIfLearner = true;
    } else {
      this.disableIfLearner = false;
    }
    // for(let i = 0;i<this.pointFormat.length;i++){
    //   if(currentItem.roleId == '8' &&  this.pointFormat[i].id == 2){
    //     this.pointFormat[i].isSelected = 1;
    //   }else{
    //     this.pointFormat[i].isSelected = 0;
    //   }
    // }
    for (let i = 0; i < this.roles.length; i++) {
      var role = this.roles[i];
      if (role.id == Number(currentItem.roleId)) {
        this.roleSelectedList[currentIndex] = role;
        if (this.selectedRole.length > 0) {
          this.selectedRole[currentIndex] = role.name;
        } else {
          this.selectedRole.push(role.name);
        }
      }
    }

    this.addPointsDisableEnable();
    this.disableSelectedRole();
  }

  pointFormatTypeSelected(index, item) {
    // for(let i = 0;i<this.pointFormat.length;i++){
    this.pointFormat[index].isSelected = 0;
    // if(item.roleid='5'){
    //   this.pointFormat[index].isSelected = 0;
    // }else{
    //   this.pointFormat[index].isSelected = 1;
    // }

    // }
  }

  disableAddCredit: boolean = false;
  addPointsDisableEnable() {
    if (this.roles.length == this.roleSelectedList.length) {
      this.disableAddCredit = true;
    } else {
      this.disableAddCredit = false;
    }
  }

  disableSelectedRole() {
    this.roles.forEach((data, key) => {
      if (this.selectedRole.indexOf(data.name) >= 0) {
        this.roles[key].isSelected = 1;
      } else {
        this.roles[key].isSelected = 0;
      }
    })
    console.log('Selected Disabled', this.roles);
  }

  addPointsList() {
    // let defualtCreditsObjCopy = Object.assign({}, this.defualtCreditsObj);
    let defualtCreditsObj = {
      crid: 0,
      roleId: '',
      pformatid: '',
      bcpoints: '',
      acpoitnts: '',
    }
    // console.log(defualtCreditsObj);
    // this.credits.push(defualtCreditsObj);
    this.pollSettingData.craditpoints.push(defualtCreditsObj);
  }

  removePointsList(i: number) {
    // this.credits.removeAt(i);
    // this.credits.splice(i,1);
    this.pollSettingData.craditpoints.splice(i, 1);
    this.roleSelectedList.splice(i, 1);
    this.selectedRole.splice(i, 1);
    this.addPointsDisableEnable();
    this.disableSelectedRole();
  }

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }

  passparams: any;
  fileUploadRes: any;
  fileres: any;
  settingsubmit(settingData, f) {
    if (f.valid) {
      this.spinner.show();
      // this.spinner.show();
      // this.makeSurveySettingDataReady();
      var option: string = Array.prototype.map.call(this.pollSettingData.craditpoints, function (item) {
        console.log('item', item)
        return Object.values(item).join('|')
      }).join('#');

      console.log('settingData', settingData);
      let param = {
        'pId': this.addAction ? 0 : this.pollSettingData.pid,
        'pName': settingData.pname,
        'pdesc': settingData.pdesc,
        'oDate': new Date(settingData.potime),
        'cDate': new Date(settingData.pctime),
        // 'iRef': '',
        'iRef': settingData.iconRef ? settingData.iconRef : '',
        'rUsers': settingData.prUsers,
        'sResult': settingData.psResult == true || settingData.psResult == 1 ? 1 : 0,
        //'vis':1,
        'tenantId': this.loginUserdata.data.data.tenantId,
        'userId': this.userId,
        'allstr': option
      }
      console.log('settingDataparam', param);

      this.passparams = param;

      var fd = new FormData();
      fd.append('content', JSON.stringify(this.passparams));
      fd.append('file', this.pollImgData);
      console.log('File Data ', fd);

      console.log('Course Data Img', this.pollImgData);
      console.log('Course Data ', this.passparams);
      let fileUploadUrl = webApi.domain + webApi.url.fileUpload;

      if (this.pollImgData != undefined) {
        this.webApiService.getService(fileUploadUrl, fd)
          .then(rescompData => {
            this.spinner.hide();
            var temp: any = rescompData;
            this.fileUploadRes = temp;
            if (temp == 'err') {
              // this.notFound = true;
              // var thumbUpload: Toast = {
              //   type: 'error',
              //   title: 'Poll Thumbnail',
              //   body: 'Unable to upload poll thumbnail.',
              //   // body: temp.msg,
              //   showCloseButton: true,
              //   timeout: 2000
              // };
              // this.toasterService.pop(thumbUpload);
              this.presentToast('error', '');

            } else if (temp.type == false) {
              // var thumbUpload: Toast = {
              //   type: 'error',
              //   title: 'Poll Thumbnail',
              //   body: 'Unable to upload poll thumbnail.',
              //   // body: temp.msg,
              //   showCloseButton: true,
              //   timeout: 2000
              // };
              // this.toasterService.pop(thumbUpload);
              this.presentToast('error', '');
            }
            else {
              if (this.fileUploadRes.data != null || this.fileUploadRes.fileError != true) {
                this.passparams.iRef = this.fileUploadRes.data.file_url;
                console.log('this.passparams.iRef', this.passparams.iRef);
                this.addEditPoll(this.passparams);
              } else {
                // var thumbUpload: Toast = {
                //   type: 'error',
                //   title: 'Poll Thumbnail',
                //   // body: 'Unable to upload course thumbnail.',
                //   body: this.fileUploadRes.status,
                //   showCloseButton: true,
                //   timeout: 2000
                // };
                // this.toasterService.pop(thumbUpload);
                this.presentToast('error', '');
              }
            }
            console.log('File Upload Result', this.fileUploadRes);
            var res = this.fileUploadRes;
            this.fileres = res.data.file_url;
          },
            resUserError => {
              //this.loader =false;
              this.spinner.hide();
              this.errorMsg = resUserError;
            });
      } else {
        // this.passparams.iconRef = this.badge.ico;
        this.addEditPoll(this.passparams);
      }
      console.log('this.passparams', this.passparams);

    } else {
      console.log('Please Fill all fields');
      // const addEditF: Toast = {
      //   type: 'error',
      //   title: 'Unable to update',
      //   body: 'Please Fill all fields',
      //   showCloseButton: true,
      //   timeout: 2000
      // };
      // this.toasterService.pop(addEditF);
      this.presentToast('warning', 'Please fill in the required fields');
      Object.keys(f.controls).forEach(key => {
        f.controls[key].markAsDirty();
      });
    }

  }

  pollImgData: any;
  readUrl(event: any) {
    var validExts = new Array('.png', '.jpg', '.jpeg');
    var fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
      // var toast: Toast = {
      //   type: 'error',
      //   title: 'Invalid file selected!',
      //   body: 'Valid files are of ' + validExts.toString() + ' types.',
      //   showCloseButton: true,
      //   timeout: 2000
      // };
      // this.toasterService.pop(toast);
      this.presentToast('warning', 'Valid file types are ' + validExts.toString());
    } else {
      if (event.target.files && event.target.files[0]) {
        this.pollImgData = event.target.files[0];

        var reader = new FileReader();

        reader.onload = (event: ProgressEvent) => {
          this.pollSettingData.iconRef = (<FileReader>event.target).result;
        }
        reader.readAsDataURL(event.target.files[0]);
      }
    }
  }

  deleteProfile() {
    this.pollSettingData.iconRef = null;
    this.pollSettingData.categoryPicRefs = undefined;
  }

  addEditPoll(passData) {
    console.log('param', passData);
    this.service.addEditPollSetting(passData)
      .then(rescompData => {
        //this.loader =false;
        this.spinner.hide();
        var temp = rescompData;
        var pollAddEditRes = temp['data'][0];
        console.log('Poll Add Result ', pollAddEditRes);
        if (temp['type'] == true) {
          if (!this.pollId) {
            this.pollId = pollAddEditRes[0].id;
            this.pollSettingData.pid = pollAddEditRes[0].id;
          }
          this.addAction = false;
          console.log('New Poll Id:', this.pollId, this.pollSettingData.pid);
          this.service.pollId = this.pollId;
          this.presentToast('success', pollAddEditRes[0].msg);
          // this.router.navigate(['/pages/reactions/poll']);
          const Sdata = {
            tabTitle: 'Questions',
          }
          this.selectTab(Sdata);
          this.cdf.detectChanges();
        } else {
          this.presentToast('error', '');
        }
        // if (this.service.getDataforAddedit[0] == 'ADD') {
        //   if (temp['type'] == true) {
        //     console.log('Poll Add Result ', pollAddEditRes)
        //     // var certUpdate: Toast = {
        //     //   type: 'success',
        //     //   title: 'Poll Inserted!',
        //     //   body: 'New poll added successfully.',
        //     //   showCloseButton: true,
        //     //   timeout: 2000
        //     // };
        //     // this.toasterService.pop(certUpdate);
        //     this.presentToast('success', 'Poll added');
        //     this.pollId = pollAddEditRes[0].id;
        //     this.pollSettingData.pid = pollAddEditRes[0].id;
        //     this.addAction = false;
        //     console.log('New Poll Id:', this.pollId);
        //     this.service.pollId = this.pollId;
        //     const Sdata = {
        // 			tabTitle: 'Questions',
        // 		}
        // 		this.selectTab(Sdata);
        // 		this.cdf.detectChanges();
        //     // this.router.navigate(['/pages/reactions/poll']);

        //   } else {
        //     // var certUpdate: Toast = {
        //     //   type: 'success',
        //     //   title: 'Poll Inserted!',
        //     //   body: 'Unable to add poll.',
        //     //   showCloseButton: true,
        //     //   timeout: 2000
        //     // };
        //     // this.toasterService.pop(certUpdate);
        //     this.presentToast('success', 'Poll added');
        //     this.router.navigate(['/pages/reactions/poll']);
        //   }
        //   this.spinner.hide();

        // } else if (this.service.getDataforAddedit[0] == 'EDIT') {
        //   // console.log('Certificate Edit Result ',this.certAddEditRes)
        //   if (temp['type'] == true) {
        //     // var certUpdate: Toast = {
        //     //   type: 'success',
        //     //   title: 'Poll Updated!',
        //     //   body: 'Poll updated successfully.',
        //     //   showCloseButton: true,
        //     //   timeout: 2000
        //     // };
        //     // this.toasterService.pop(certUpdate);
        //     this.presentToast('success', 'Poll updated');
        //     this.router.navigate(['/pages/reactions/poll'])
        //   } else {
        //     // var certUpdate: Toast = {
        //     //   type: 'success',
        //     //   title: 'Survey Updated!',
        //     //   body: 'Unable to update pole.',
        //     //   showCloseButton: true,
        //     //   timeout: 2000
        //     // };
        //     // this.toasterService.pop(certUpdate);
        //     this.presentToast('success', 'Poll updated');
        //     this.router.navigate(['/pages/reactions/poll'])
        //   }
        //   this.spinner.hide();

        // }

      },
        resUserError => {
          // this.loader =false;
          this.errorMsg = resUserError;
          this.spinner.hide();
          this.presentToast('error', '');
          // this.router.navigate(['/pages/reactions/poll'])
        });
  }
  /********************************************************************************************* */

  /*****************************Survey Enrol start**************************************/
  showEnrolpage: boolean = false;
  showBulkpage: boolean = false;
  saveEnrol() {
    this.showEnrolpage = true;
    this.showBulkpage = false;
      this.header = {
        title: this.content?this.content.name:'Add Poll',
        btnsSearch: true,
        btnName3: 'Bulk Enrol',
        btnName3show: true,
        btnBackshow: true,
        showBreadcrumb: true,
        breadCrumbList: [
          {
            'name': 'Reaction',
            'navigationPath': '/pages/reactions',
          },
          {
            'name': 'Poll',
            'navigationPath': '/pages/reactions/poll',
          },]
      };
    this.backflag = 2;
  }
  bulkEnrol() {
    this.showEnrolpage = false;
    this.showBulkpage = true;
      this.header = {
        title: this.content?this.content.name:'Add Poll',
        btnsSearch: true,
        btnName2: 'Enrol',
        btnName1show: false,
        btnName2show: true,
        btnName3show: false,
        btnBackshow: true,
        showBreadcrumb: true,
        breadCrumbList: [
          {
            'name': 'Reaction',
            'navigationPath': '/pages/reactions',
          },
          {
            'name': 'Poll',
            'navigationPath': '/pages/reactions/poll',
          },]
      };
    this.backflag = 2;
  }
  backToEnrol() {
    this.showEnrolpage = false;
    this.showBulkpage = false;
    this.file = [];
    this.preview = false;
    this.fileName = 'Click here to upload an excel file to enrol ' + this.currentBrandData.employee.toLowerCase() + ' to this course'
    this.enableUpload = false;
    this.allEnrolUser();
  }
  tableData: any;
  temp: any;
  searchvalue: any = {
    value: ''
  };
  ngOnInit() {
    this.currentBrandData = this.brandService.getCurrentBrandData();
    this.fileName = 'Click here to upload an excel file to enrol ' + this.currentBrandData.employee.toLowerCase() + ' to this course'
  }
  clearesearch() {
    if (this.searchText.length >= 3) {
      this.searchvalue = { value: '' };
      // this.searchEnrolUser(this.searchvalue);
      this.allEnrolUser();
    }
    else {
      this.searchvalue = {};
    }
  }
  searchEnrolUser(event) {
    const val = event.target.value.toLowerCase();

    // this.allEnrolUser( this.courseDataService.data.data)
    this.searchText = val;
    this.temp = [...this.enrolldata];
    console.log(this.temp);
    // filter our data
    if (val.length >= 3 || val.length == 0) {
      const temp = this.temp.filter(function (d) {
        return String(d.ecn).toLowerCase().indexOf(val) !== -1 ||
          d.fullname.toLowerCase().indexOf(val) !== -1 ||
          d.emailId.toLowerCase().indexOf(val) !== -1 ||
          d.enrolmode.toLowerCase().indexOf(val) !== -1 ||
          d.enrolDate.toLowerCase().indexOf(val) !== -1 ||
          d.phoneNo.toLowerCase().indexOf(val) !== -1 ||
          String(d.enrolmode).toLowerCase().indexOf(val) !== -1 ||
          // d.mode.toLowerCase() === val || !val;
          !val
      });

      // update the rows
      this.rows = [...temp];
    }
    // Whenever the filter changes, always go back to the first page
    this.tableData.offset = 0;
  }

  enrolldata: any;
  enableCourse: any;
  allEnrolUser() {
    this.spinner.show();
    var data = {
      areaId: 16,
      instanceId: this.pollId,
      tId: 1,
      mode: 0
    }
    console.log(data);
    this.courseDataService.getallenroluser(data).then(enrolData => {
      this.spinner.hide();
      this.enrolldata = enrolData['data'];
      this.rows = enrolData['data'];
      this.rows = [...this.rows];
      for (let i = 0; i < this.rows.length; i++) {
        // this.rows[i].Date = new Date(this.rows[i].enrolDate);
        // this.rows[i].enrolDate = this.formdate(this.rows[i].Date);
        if (this.rows[i].visible == 1) {
          this.rows[i].btntext = 'fa fa-eye';
        } else {
          this.rows[i].btntext = 'fa fa-eye-slash';
        }
      }
      console.log('EnrolledUSer', this.rows);
      if (this.enrolldata.visible = 1) {
        this.enableCourse = false;
      } else {
        this.enableCourse = true;
      }
      //this.cdf.detectChanges();


    })
  }
  formdate(date) {

    if (date) {

      var formatted = this.datePipe.transform(date, 'dd-MM-yyyy');
      return formatted;
    }
  }
  readfileUrl(event: any) {
    var validExts = new Array('.xlsx', '.xls');
    var fileExt = event.target.files[0].name;
    this.showInvalidExcel = false;
    this.invaliddata = [];
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
      // alert('Invalid file selected, valid files are of ' +
      //          validExts.toString() + ' types.');
      // var toast: Toast = {
      //   type: 'error',
      //   title: 'Invalid file selected!',
      //   body: 'Valid files are of ' + validExts.toString() + ' types.',
      //   showCloseButton: true,
      //   // tapToDismiss: false,
      //   timeout: 2000
      //   // onHideCallback: () => {
      //   //     this.router.navigate(['/pages/plan/users']);
      //   // }
      // };
      // this.toasterService.pop(toast);
      this.presentToast('warning', 'Valid file types are ' + validExts.toString());
      // setTimeout(data => {
      //   this.router.navigate(['/pages/users']);
      // },1000);
      // return false;
      this.cancel();
    } else {
      // return true;
      if (event.target.files && event.target.files[0]) {
        this.fileName = event.target.files[0].name;
        //read file from input
        this.fileReaded = event.target.files[0];

        if (this.fileReaded != '' || this.fileReaded != null || this.fileReaded != undefined) {
          this.enableUpload = true;
          this.showInvalidExcel = false;
        }

        this.file = event.target.files[0];

        this.xlsxToJsonService
          .processFileToJson({}, this.file)
          .subscribe(data => {
            let resultSheets = Object.getOwnPropertyNames(data["sheets"]);
            console.log("File Property Names ", resultSheets);
            let sheetName = resultSheets[0];
            let result = data["sheets"][sheetName];
            console.log("dataSheet", data);

            if (result.length > 0) {
              this.uploadedData = result;
              console.log("this.uploadedData", this.uploadedData);
            } else {
              this.uploadedData = [];
            }
          });

        var reader = new FileReader();
        reader.onload = (event: ProgressEvent) => {
          let fileUrl = (<FileReader>event.target).result;
          // event.setAttribute('data-title', this.fileName);
          // console.log(this.fileUrl);
        }
        reader.readAsDataURL(event.target.files[0]);
      }
    }
  }
  cancel() {
    // this.fileUpload.nativeElement.files = [];
    console.log(this.fileUpload.nativeElement.files);
    this.fileUpload.nativeElement.value = '';
    console.log(this.fileUpload.nativeElement.files);
    this.fileName = 'Click here to upload an excel file to enrol ' + this.currentBrandData.employee.toLowerCase() + ' to this course'
    this.enableUpload = false;
    this.file = [];
    this.preview = false;
  }
  uploadenrol() {
    this.spinner.show();


    var fd = new FormData();
    console.log(this.content);
    this.content.areaId = 16;
    this.content.userId = this.userId;
    this.content.tenantId = this.tenantId;
    this.content.courseId = this.content.id;
    fd.append('content', JSON.stringify(this.content));
    fd.append('file', this.file);
    console.log(fd);
    this.courseDataService.TempManEnrolBulk(fd).then(result => {
      console.log(result);
      // this.loader =false;

      var res = result;
      try {
        if (res['data1'][0]) {
          this.resultdata = res['data1'][0];
          for (let i = 0; i < this.resultdata.length; i++) {
            // this.resultdata[i].enrolDate = this.formdate(this.resultdata[i].enrolDate);
          }

          this.datasetPreview = this.resultdata;
          this.preview = true;
          this.cdf.detectChanges();
          // this.spinner.hide();
        }
      } catch (e) {

        // var courseUpdate: Toast = {
        //   type: 'error',
        //   title: 'Enrol',
        //   body: 'Something went wrong.please try again later.',
        //   showCloseButton: true,
        //   timeout: 2000
        // };

        // this.closeEnableDisableCourseModal();
        // this.toasterService.pop(courseUpdate);
        this.presentToast('error', '');
      }
      this.spinner.hide();

    },
      resUserError => {
        // this.loader =false;
        this.spinner.hide();
        this.errorMsg = resUserError;
        // this.closeEnableDisableCourseModal();
      });
  }

  uploadBulEnrol() {
    if (this.uploadedData.length > 0 && this.uploadedData.length <= 2000) {
      this.spinner.show();

      // const option: string = Array.prototype.map
      //   .call(this.uploadedData, function (item) {
      //     console.log("item", item);
      //     return Object.values(item).join("#");
      //   })
      //   .join("|");

      // New Function as per requirement.

      var option: string = Array.prototype.map
        .call(this.uploadedData, (item) => {
          const array = Object.keys(item);
          let string = '';
          array.forEach(element => {
            if (element === 'EnrolDate') {
              string += this.commonFunctionService.formatDateTimeForBulkUpload(item[element]);
            } else {
              string = item[element] + '#';
            }
          });
          // console.log("item", item);
          // return Object.values(item).join("#");
          return string;
        })
        .join("|");

      const data = {
        wfId: null,
        aId: 16,
        iId: this.pollId,
        upString: option
      };
      console.log("data", data);
      this.service.areaBulkEnrol(data).then(
        rescompData => {
          // this.loader =false;
          this.spinner.hide();
          var temp: any = rescompData;
          this.bulkUploadRes = temp.data;
          if (temp == "err") {
            this.toastr.error(
              'Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.',
              "Error",
              {
                timeOut: 0,
                closeButton: true
              }
            );
          } else if (temp.type == false) {
            this.toastr.error(
              'Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.',
              "Error",
              {
                timeOut: 0,
                closeButton: true
              }
            );
          } else {

            this.enableUpload = false;
            this.fileName = 'Click here to upload an excel file to enrol ' + this.currentBrandData.employee.toLowerCase() + ' to this course'
            if (temp["data"].length > 0) {
              if (temp["data"].length === temp["invalidCnt"].invalidCount) {
                this.toastr.warning(
                  "No valid " + this.currentBrandData.employee.toLowerCase() + "s found to enrol.",
                  "Warning",
                  {
                    closeButton: false
                  }
                );
              } else {
                this.toastr.success("Enrol successfully.", "Success", {
                  closeButton: false
                });
              }
              this.invaliddata = temp["data"];
              this.showInvalidExcel = true;
            } else {
              this.invaliddata = [];
              this.showInvalidExcel = false;
            }
            // this.backToEnrol()
            this.cdf.detectChanges();
            this.preview = false;
            this.showInvalidExcel = true;
          }
          console.log("Enrol bulk Result ", this.bulkUploadRes);
          this.cdf.detectChanges();
        },
        resUserError => {
          this.spinner.hide();
          this.errorMsg = resUserError;
        }
      );
    } else {
      if (this.uploadedData.length > 2000) {
        this.toastr.warning('File Data cannot exceed more than 2000', 'warning', {
          closeButton: true
        });
      }
      else {
        this.toastr.warning('No Data Found', 'warning', {
          closeButton: true
        });
      }
    }
  }
  invaliddata: any = [];
  bulkUploadRes: any;
  showInvalidExcel: boolean = false;
  exportToExcelInvalid() {
    this.exportService.exportAsExcelFile(this.invaliddata, 'Enrolment Status')
  }
  savebukupload() {
    var data = {
      userId: this.userId,
      coursId: this.content.id,
      areaId: 16,
      tId: this.tenantId,
    }
    // this.loader = true;
    this.spinner.show();
    this.courseDataService.finalManEnrolBulk(data)
      .then(rescompData => {
        // this.loader =false;
        this.spinner.hide();
        var temp: any = rescompData;
        this.addEditModRes = temp.data[0];
        if (temp == 'err') {
          // var modUpdate: Toast = {
          //   type: 'error',
          //   title: 'Enrol',
          //   body: 'Something went wrong',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(modUpdate);
          this.presentToast('error', '');
        } else if (temp.type == false) {
          // var modUpdate: Toast = {
          //   type: 'error',
          //   title: 'Enrol',
          //   body: this.addEditModRes[0].msg,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(modUpdate);
          this.presentToast('error', '');
        } else {
          // var modUpdate: Toast = {
          //   type: 'success',
          //   title: 'Enrol',
          //   body: this.addEditModRes[0].msg,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(modUpdate);
          this.presentToast('success', this.addEditModRes[0].msg);
          this.backToEnrol()
          this.cdf.detectChanges();
          this.preview = false;

        }
        console.log('Enrol bulk Result ', this.addEditModRes);
        this.cdf.detectChanges();
      },
        resUserError => {
          this.loader = false;
          this.errorMsg = resUserError;
        });
  }
  visibilityTableRow(row) {
    let value;
    let status;

    if (row.visible == 1) {
      row.btntext = 'fa fa-eye-slash';
      value = 'fa fa-eye-slash';
      row.visible = 0
      status = 0;
    } else {
      status = 1;
      value = 'fa fa-eye';
      row.visible = 1;
      row.btntext = 'fa fa-eye';
    }

    for (let i = 0; i < this.rows.length; i++) {
      if (this.rows[i].employeeId == row.employeeId) {
        this.rows[i].btntext = row.btntext;
        this.rows[i].visible = row.visible
      }
    }
    var visibilityData = {
      employeeId: row.employeeId,
      visible: status,
      courseId: this.content.id,
      tId: this.tenantId,
      aId: 16,
    }
    this.courseDataService.disableEnrol(visibilityData).then(result => {
      console.log(result);
      this.spinner.hide();
      this.resultdata = result;
      if (this.resultdata.type == false) {

        this.presentToast('error', '');
      } else {

        console.log('after', row.visible)
        this.allEnrolUser();

        this.presentToast('success', this.resultdata.data);
      }
    },
      resUserError => {
        this.errorMsg = resUserError;
      });

    console.log('row', row);
  }
  resultdata: any;
  disableCourseVisibility(currentIndex, row, status) {
    this.spinner.show();
    var visibilityData = {
      employeeId: row.employeeId,
      visible: status,
      courseId: this.content.id,
      tId: this.tenantId,
      aId: 16,
    }
    this.courseDataService.disableEnrol(visibilityData).then(result => {
      console.log(result);
      this.spinner.hide();
      this.resultdata = result;
      if (this.resultdata.type == false) {
        // var courseUpdate: Toast = {
        //   type: 'error',
        //   title: 'Poll',
        //   body: 'Unable to update visibility of User.',
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.closeEnableDisableCourseModal();
        // this.toasterService.pop(courseUpdate);
        this.presentToast('error', '');
      } else {
        // var courseUpdate: Toast = {
        //   type: 'success',
        //   title: 'Poll',
        //   body: this.resultdata.data,
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // row.visible = !row.visible;
        console.log('after', row.visible)
        this.allEnrolUser();
        // this.toasterService.pop(courseUpdate);
        this.presentToast('success', this.resultdata.data);
      }
    },
      resUserError => {
        this.errorMsg = resUserError;
        // this.closeEnableDisableCourseModal();
      });


  }
  /*************************************************************************************/

  /*****************************Poll Questionn Start****************************************/

  //  surveyQue:any=[];
  //  submitNewSrveyQue(){
  //   console.log(this.surveyQue);
  //   let finalString;
  //   for(let i=0;i<this.surveyQue.length;i++){
  //     let a = i+1;
  //     let str = this.surveyQue[i].qId + '|' + a;

  //     if(i == 0){
  //       finalString = str;

  //     }else{
  //       finalString += '#' + str;
  //     }
  //   }

  //   console.log(finalString)
  //   // return finalString;
  //   this.setSortOrder(finalString);
  // }

  // setSortOrder(finalString){
  //   this.spinner.show();
  //   let param = {
  //     'tenantId':1,
  //     'surveyId':this.pollId,
  //     'allstr':finalString
  //   }
  //   this.service.ChangeSurveyQuestionOrder(param)
  //   .then(rescompData => {
  //     this.spinner.hide();
  //      var result = rescompData;

  //      if(result['type'] == true){
  //         var toast : Toast = {
  //           type: 'success',
  //           //title: 'Server Error!',
  //           body: 'List updated successfully.',
  //           showCloseButton: true,
  //           timeout: 2000
  //       };
  //       this.toasterService.pop(toast);
  //       this.getExistingPollQuestion();
  //        console.log('ORDER CHANGE SUCCESS:',result);
  //      };

  //    },error=>{
  //     // this.spinner.hide();
  //        var toast : Toast = {
  //          type: 'error',
  //          //title: 'Server Error!',
  //          body: 'Something went wrong.please try again later.',
  //          showCloseButton: true,
  //          timeout: 2000
  //      };
  //      this.toasterService.pop(toast);
  //    });
  // }


  noSurveyQuestion: boolean = false;
  surveyQue: any = [];
  getExistingPollQuestion() {
    this.spinner.show();
    let param = {
      'tenantId': this.loginUserdata.data.data.tenantId,
      'pollId': this.pollId,
    }
    console.log('param', param);
    this.service.getExistingPollQuestions(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('ExistingPollQuestion:', rescompData);
        if (result['type'] == true) {
          if (result['data'][0].length == 0) {
            this.noSurveyQuestion = true;
            //this.getPollQuestionDetails(this.surveyQue[0]);
            // var qOptArr = [];
            var sqDataObj = {};
            var sqDataArr = [];

            for (let i = 0; i < 2; i++) {
              sqDataObj = {
                'qansmul': '',
                'qansmulpoints': ''
              }
              sqDataArr.push(sqDataObj);
            }
            console.log('sqDataArr', sqDataArr);
            this.questiondata = {
              pqid: '',
              pqname: '',
              pmand: '',
              pqtypeid: '',
              qansmin: null,
              qansmax: null,
              pqtypeName: null,
              pqdata: sqDataArr
            }
          } else {
            this.noSurveyQuestion = false;
            this.surveyQue = result['data'][0];

            console.log('getExistingPollQuestions:', this.surveyQue);
            this.getPollQuestionDetails(this.surveyQue[0]);
          }

        };

      }, error => {
        this.spinner.hide();
        // var toast: Toast = {
        //   type: 'error',
        //   //title: 'Server Error!',
        //   body: 'Something went wrong.please try again later.',
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });

  }
  addqueAction: boolean = false;
  questionTypes: any = [];
  getSurveyQuestionType() {
    let param = {
      'tenantId': this.loginUserdata.data.data.tenantId
    }

    this.service.getSurveyQuestionType(param)
      .then(rescompData => {
        // this.spinner.hide();
        var result = rescompData;
        console.log('DropdownSurveyQuestionType:', rescompData);
        if (result['type'] == true) {
          this.questionTypes = result['data'][0];
          this.questionTypes.forEach(element => {
            if (element.id == 2) {
              element.isShow = 0;
            } else {
              element.isShow = 1;
            }
          });
          console.log('DropdownSurveyQuestionType:', this.questionTypes);
        };

      }, error => {
        // this.spinner.hide();
        // var toast: Toast = {
        //   type: 'error',
        //   //title: 'Server Error!',
        //   body: 'Something went wrong.please try again later.',
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
  }

  // savequestion(data) {
  //   console.log('data', data);
  //   // for(let i=0;i<this.formdata.sque.length;i++)
  //   // {
  //   //   if(this.formdata.sque[i].sqid==data.sqid)
  //   //   {
  //   //     this.formdata.sque.splice(i,1);

  //   //   }
  //   // }
  //   this.formdata.sque.push(data);
  //   console.log('formdata.sque', this.formdata.sque);

  //   this.questionshow = false;
  //   this.questiondata = {
  //     pqid: '',
  //     qansmax: '',
  //     qansmin: '',
  //     pqname: '',
  //     pmand: '',
  //     pqtypeid: '',
  //     pqtypeName: '',
  //     pqdata: []

  //   };
  // }

  addQuestion() {
    this.questionshow = true;
    this.addqueAction = true
  }

  saveQuestion(data, f) {

    if(this.addAction) {
      this.presentToast('warning', 'Please save settings first');
      const Sdata = {
        tabTitle: 'Settings',
      }
      this.selectTab(Sdata);
    } else {

    if (f.valid) {
      console.log('data', data);
      let param = {
        // 'queId':this.addqueAction == true ? 0 : data.pqid,
        // 'queName':data.pqname,
        // 'pollId':this.pollId,
        // 'isMandatory':data.pmand == true || data.pmand == 1 ? 1 : 0,
        // 'queTypeId':data.pqtypeid,
        // 'tenantId':1,
        // 'userId':this.userId,
        // 'minVal':data.pqtypeid == 1 ? data.pansmin : 0,
        // 'maxVal':data.pqtypeid == 1 ? data.pansmax : 0,
        // 'labelfirst':data.pqtypeid == 3 ? 'Yes' : 0,
        // 'valfirst':data.pqtypeid == 3 ? '1' : 0,
        // 'labelsecond':data.pqtypeid == 3 ? 'No' : 0,
        // 'valsecond':data.pqtypeid == 3 ? '0' : 0,
        // 'allstr':data.pqtypeid == 2 ? this.getDatareadyforpollquestion(data.pqdata) : 0

        'queId': this.addqueAction == true ? 0 : data.pqid,
        'queName': data.pqname,
        'pollId': this.pollId ? this.pollId : this.pollSettingData.pid,
        'isMandatory': 1,
        'queTypeId': 2,
        'tenantId': this.loginUserdata.data.data.tenantId,
        'userId': this.userId,
        'minVal': 0,
        'maxVal': 0,
        'labelfirst': 0,
        'valfirst': 0,
        'labelsecond': 0,
        'valsecond': 0,
        'allstr': this.getDatareadyforpollquestion(data.pqdata)
      }

      console.log('param:', param);

      this.service.addEditPollQuestion(param)
        .then(rescompData => {
          this.spinner.hide();
          var temp = rescompData;
          // console.log('NotificationsSuccess:', rescompData);

          if (this.service.getDataforAddedit[0] == 'ADD') {
            if (temp['type'] == true) {
              // var notiInsert: Toast = {
              //   type: 'success',
              //   title: 'Poll Question Inserted!',
              //   body: 'Question inserted successfully.',
              //   showCloseButton: true,
              //   timeout: 2000
              // };
              // this.toasterService.pop(notiInsert);
              this.presentToast('success', 'Question added');
              console.log('SurveyQueRes:', temp);
              //this.cdf.detectChanges();
              this.back();
              // this.getExistingPollQuestion();
              this.questiondata = {
                pqid: '',
                pqname: '',
                pmand: '',
                pqtypeid: '',
                qansmin: '',
                qansmax: '',
                pqtypeName: '',
                pqdata: []

              }

            } else {
              //this.loader = false;
              // var toast: Toast = {
              //   type: 'error',
              //   //title: 'Server Error!',
              //   body: 'Something went wrong.please try again later.',
              //   showCloseButton: true,
              //   timeout: 2000
              // };
              // this.toasterService.pop(toast);
              this.presentToast('error', '');
              //this.router.navigate(['pages/gamification/ladders']);
            }
          } else if (this.service.getDataforAddedit[0] == 'EDIT') {
            if (temp['type'] == true) {
              // var notiInsert: Toast = {
              //   type: 'success',
              //   title: 'Poll Question Updated!',
              //   body: 'Question updated successfully.',
              //   showCloseButton: true,
              //   timeout: 2000
              // };
              // this.toasterService.pop(notiInsert);
              this.presentToast('success', 'Question updated');
              //this.cdf.detectChanges();
              this.getExistingPollQuestion();
              this.questiondata = {
                pqid: '',
                pqname: '',
                pmand: '',
                pqtypeid: '',
                qansmin: '',
                qansmax: '',
                pqtypeName: '',
                pqdata: []

              }
            } else {

              // var toast: Toast = {
              //   type: 'error',
              //   //title: 'Server Error!',
              //   body: 'Something went wrong.please try again later.',
              //   showCloseButton: true,
              //   timeout: 2000
              // };
              // this.toasterService.pop(toast);
              this.presentToast('error', '');
              //this.router.navigate(['pages/gamification/ladders']);
            }
          }


        },
          resUserError => {

            this.spinner.hide();
            // var toast: Toast = {
            //   type: 'error',
            //   //title: 'Server Error!',
            //   body: 'Something went wrong.please try again later.',
            //   showCloseButton: true,
            //   timeout: 2000
            // };
            // this.toasterService.pop(toast);
            this.presentToast('error', '');
            //this.router.navigate(['pages/gamification/ladders']);
          });
      this.questionshow = false;
    } else {
      console.log('Please Fill all fields');
    console.log('this.questiondata.pqdata', this.questiondata.pqdata);
      // const addEditF: Toast = {
      //   type: 'error',
      //   title: 'Unable to update',
      //   body: 'Please Fill all fields',
      //   showCloseButton: true,
      //   timeout: 2000
      // };
      // this.toasterService.pop(addEditF);
      this.presentToast('warning', 'Please fill in the required fields');
      Object.keys(f.controls).forEach(key => {
        f.controls[key].markAsDirty();
      });
    }
    }
  }

  getDatareadyforpollquestion(data) {
    var allstr;
    for (let i = 0; i < data.length; i++) {
      let a = i + 1;
      var str = data[i].qansmul + '|' + 0 + '|' + a;

      if (i == 0) {
        allstr = str;
      } else {
        allstr += '#' + str;
      }
    }
    console.log('allstr', allstr);
    return allstr;
  }

  addandswers() {
    var qdata = {}
    this.questiondata.pqdata.push(qdata)
    console.log('this.questiondata.pqdata', this.questiondata.pqdata);
  }

  removeans(ansid) {

    for (let i = 0; i < this.questiondata.pqdata.length; i++) {
      if (ansid == i) {
        this.questiondata.pqdata.splice(i, 1)
      }
    }

  }
  // enableDisableQuestionData:any;
  // enableDisableQuestionModal:boolean = false;
  // enableQuestion:boolean=false;
  // queDisableIndex:any;
  // disableQuestion(currentIndex,queData,status){
  //   this.enableDisableQuestionData = queData;
  //   console.log('this.enableDisableLadderData',this.enableDisableQuestionData)
  //   this.enableDisableQuestionModal = true;
  //   this.queDisableIndex = currentIndex;

  //   if(this.enableDisableQuestionData.visible == 0){
  //     this.enableQuestion = false;
  //   }else{
  //     this.enableQuestion = true;
  //   }
  // }

  // closeEnableDisableQuestionModal(){
  //   this.enableDisableQuestionModal = false;
  // }

  // enableDisableQuestionAction(actionType){
  //   if(actionType == true){
  //     if(this.enableDisableQuestionData.visible == 1){
  //       var status = 0;
  //       var queData = this.enableDisableQuestionData;
  //       this.enableDisableQuestion(queData,status);

  //     }else{
  //       var status = 1;
  //       var queData = this.enableDisableQuestionData;
  //       this.enableDisableQuestion(queData,status);

  //     }
  //     this.closeEnableDisableQuestionModal();
  //   }else{
  //     this.closeEnableDisableQuestionModal();
  //   }
  // }

  // enableDisableQuestion(queData,status){
  //   this.spinner.show();
  //   console.log('queData',queData);
  //   let param = {
  //     'queId':queData.qId,
  //     'visible':status,

  //   }
  //   this.service.UpdateSurveyQuestionStatus(param)

  //   .then(rescompData => {
  //     this.spinner.hide();
  //     var result = rescompData;
  //     console.log('UpdateLadderResponse:',rescompData);
  //     if(result['type'] == true){
  //       var ladderUpdate : Toast = {
  //         type: 'success',
  //         title: 'Survey Question Updated!!',
  //         body: 'Question updated successfully.',
  //         showCloseButton: true,
  //         timeout: 2000
  //     };
  //     this.toasterService.pop(ladderUpdate);
  //     this.getExistingPollQuestion();
  // }else{
  //         var ladderUpdate : Toast = {
  //           type: 'error',
  //           title: 'Survey Question Updated!',
  //           body: 'Unable to update question.',
  //           showCloseButton: true,
  //           timeout: 2000
  //       };
  //       this.toasterService.pop(ladderUpdate);
  //     }
  //   },error=>{
  //     this.spinner.hide();
  //     var toast : Toast = {
  //       type: 'error',
  //       //title: 'Server Error!',
  //       body: 'Something went wrong.please try again later.',
  //       showCloseButton: true,
  //       timeout: 2000
  //   };
  //   this.toasterService.pop(toast);
  // });

  // }

  editActionQuestion: boolean = false;
  getPollQuestionDetails(data) {
    this.addqueAction = false;
    this.editActionQuestion = true;
    this.spinner.show();
    console.log('QueData', data);
    let param = {
      'tenantId': this.loginUserdata.data.data.tenantId,
      'queId': data.qId,
      // 'queTypeId':data.qtId
    }

    this.service.GetPollQuestionDetails(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('QuestionDeatils:', rescompData);
        var qData = result['data'][0][0];

        if (result['type'] == true) {
          console.log('QuestionDeatils:', qData);
          if (data.qtId == 1) {
            this.questiondata = {
              pqid: qData.id,
              pqname: qData.name,
              pmand: qData.isMandatory,
              pqtypeid: qData.questionTypeId,
              qansmin: qData.minlen,
              qansmax: qData.maxlen,
              pqtypeName: null,
              pqdata: []
            }
            //this.questionshow=true;
          }

          if (data.qtId == 2) {
            let qOptArr = qData.optionText.split(',');
            let qPointsArr = qData.points.split(',');
            let sqDataArr: any = [];
            let sqDataObj: any = {};
            console.log('Arrays:', qOptArr, qPointsArr);
            for (let i = 0; i < qOptArr.length; i++) {
              sqDataObj = {
                'qansmul': qOptArr[i],
                'qansmulpoints': qPointsArr[i],
              };
              sqDataArr.push(sqDataObj);
            }
            console.log('sqDataArr', sqDataArr);
            this.questiondata = {
              pqid: qData.id,
              pqname: qData.name,
              pmand: qData.isMandatory,
              pqtypeid: qData.questionTypeId,
              qansmin: null,
              qansmax: null,
              pqtypeName: null,
              pqdata: sqDataArr
            };
            //this.questionshow=true;
          }
          if (data.qtId == 3) {
            this.questiondata = {
              pqid: qData.id,
              pqname: qData.name,
              pmand: qData.isMandatory,
              pqtypeid: qData.questionTypeId,
              qansmin: null,
              qansmax: null,
              pqtypeName: null,
              pqdata: []
            }
            //this.questionshow=true;
          }
        } else {
          this.spinner.hide();
          // var toast: Toast = {
          //   type: 'error',
          //   //title: 'Server Error!',
          //   body: 'Something went wrong.please try again later.',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }
      }, error => {
        this.spinner.hide();
        // var toast: Toast = {
        //   type: 'error',
        //   //title: 'Server Error!',
        //   body: 'Something went wrong.please try again later.',
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
  }

  /*****************************Survey Question End********************************************/

  // Help Code Start Here //

  helpContent: any;
  getHelpContent() {
    return new Promise(resolve => {
      this.http1
        .get('../../../../../../assets/help-content/addEditCourseContent.json')
        .subscribe(
          data => {
            this.helpContent = data;
            console.log('Help Array', this.helpContent);
          },
          err => {
            resolve('err');
          });
    });
    // return this.helpContent;
  }

  passData(val) {
    this.isdata = val;
    // this.eventsSubject.next();
    setTimeout(() => {
      this.isdata = '';
    }, 2000);
  }

  SarchFiltertest(event) {
    this.searchsub = event;
    console.log('event', event.target.value);
    setTimeout(() => {
      this.searchsub = '';
    }, 2000);
  }
  // Help Code Ends Here //
}
