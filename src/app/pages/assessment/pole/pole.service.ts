import {Injectable,Inject} from '@angular/core';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppConfig} from '../../../app.module';
import { webApi } from '../../../service/webApi';
import { HttpClient } from "@angular/common/http";

@Injectable()
export class poleService{
    private _url:string = "/api/edge/category/getCategory"
    public data:any;
    private _urlFetch:string = webApi.domain + webApi.url.getallpoll;
    private _urlChangePollStatus:string = webApi.domain + webApi.url.changepollstatus;

    constructor(@Inject('APP_CONFIG_TOKEN') private config:AppConfig,private _http:Http,private _httpClient:HttpClient){

    }
    
    getCategories(){
        let url:any = `${this.config.FINAL_URL}`+this._url;
        return this._http.post(url,{})
            .map((response:Response)=>response.json())
            .catch(this._errorHandler);
    }

    changePollStatus(param){
            return new Promise(resolve => {
            this._httpClient.post(this._urlChangePollStatus, param)
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve('err');
            });
        });
    }

    getPoll(param){
        return new Promise(resolve => {
        this._httpClient.post(this._urlFetch, param)
        .subscribe(data => {
            resolve(data);
        },
        err => {
            resolve('err');
        });
    });
}

    
    _errorHandler(error: Response){
        console.error(error);
        return Observable.throw(error || "Server Error")
      }
}