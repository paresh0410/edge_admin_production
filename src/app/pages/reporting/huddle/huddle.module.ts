import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';

import { HuddleComponent } from './huddle.component';



import { ChartsModule } from 'ng2-charts';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ChartsModule
  ],
  declarations: [
    HuddleComponent
  ],
  providers: [
    
  ]
})
export class HuddleModule {}
