import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';

import { MarketingComponent } from './marketing.component';



import { ChartsModule } from 'ng2-charts';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ChartsModule
  ],
  declarations: [
    MarketingComponent
  ],
  providers: [
    
  ]
})
export class MarketingModule {}
