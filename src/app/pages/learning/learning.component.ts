import { Component, OnDestroy, OnInit } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators/takeWhile';
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from '../../app.service';
import { ContentService } from '../plan/courses/content/content.service';
import { AddEditCourseContentService } from '../plan/courses/addEditCourseContent/addEditCourseContent.service';
import { AddEditPreonCourseContentService } from '../plan/preoncourses/addEditPreonCourseContent/addEditPreonCourseContent.service';
import { SuubHeader } from '../components/models/subheader.model';
import { PreonContentService } from '../plan/preoncourses/preoncontent/preoncontent.service';
interface CardSettings {
  title: string;
  iconClass: string;
  type: string;
}

@Component({
  selector: 'ngx-learning',
  styleUrls: ['./learning.component.scss'],
  templateUrl: './learning.component.html',
})
export class LearningComponentMain implements OnDestroy {

  header: SuubHeader  = {
    title: 'Learning',
    showBreadcrumb:true,
    breadCrumbList:[]
  };
  private alive = true;

  lightCard: CardSettings = {
    title: 'Light',
    iconClass: 'nb-lightbulb',
    type: 'primary',
  };
  rollerShadesCard: CardSettings = {
    title: 'Roller Shades',
    iconClass: 'nb-roller-shades',
    type: 'success',
  };
  wirelessAudioCard: CardSettings = {
    title: 'Wireless Audio',
    iconClass: 'nb-audio',
    type: 'info',
  };
  coffeeMakerCard: CardSettings = {
    title: 'Coffee Maker',
    iconClass: 'nb-coffee-maker',
    type: 'warning',
  };

  statusCards: string;

  commonStatusCardsSet: CardSettings[] = [
    this.lightCard,
    this.rollerShadesCard,
    this.wirelessAudioCard,
    this.coffeeMakerCard,
  ];

  statusCardsByThemes: {
    default: CardSettings[];
    cosmic: CardSettings[];
    corporate: CardSettings[];
  } = {
      default: this.commonStatusCardsSet,
      cosmic: this.commonStatusCardsSet,
      corporate: [
        {
          ...this.lightCard,
          type: 'warning',
        },
        {
          ...this.rollerShadesCard,
          type: 'primary',
        },
        {
          ...this.wirelessAudioCard,
          type: 'danger',
        },
        {
          ...this.coffeeMakerCard,
          type: 'secondary',
        },
      ],
    };
  showdata: any = [];
  employees: boolean = false;
  users: boolean = false;
  courses: boolean = false;
  evaluation: boolean = false;
  competancy: boolean = false;
  courseBundle: boolean = false;
  roleManagement: boolean = false;
  learnData: any = [];
  constructor(private themeService: NbThemeService, public router: Router, public routes: ActivatedRoute, private AppService: AppService,
    public passService: PreonContentService,public contentservice: ContentService, public addEditCourseContentService: AddEditCourseContentService ,
    public preonaddEditCourseContentService: AddEditPreonCourseContentService) {

    // this.contentservice.data.data = undefined;
    // console.log(this.contentservice.data.data);
    this.themeService.getJsTheme()
      .pipe(takeWhile(() => this.alive))
      .subscribe(theme => {
        this.statusCards = this.statusCardsByThemes[theme.name];
      });
    this.showdata = this.AppService.getmenus();
   this.passService.page = ''
      console.log(this.passService.page,"learning")
      localStorage.removeItem('cat')
    // if(this.showdata)
    // {
    //   for(let i=0;i<this.showdata.length;i++)
    //   {
    //     if(this.showdata[i] == 'MEMP')
    //     {
    //       this.employees =true;
    //     }
    //      if(this.showdata[i] == 'MU')
    //     {
    //       this.users =true;
    //     }
    //      if(this.showdata[i] == 'MC')
    //     {
    //       this.courses =true;
    //     }
    //      if(this.showdata[i] == 'training')
    //     {
    //       this.evaluation =true;
    //     }
    //        if(this.showdata[i] == 'rules')
    //     {
    //       this.competancy =true;
    //     }
    //      if(this.showdata[i] == 'MCB')
    //     {
    //       this.courseBundle =true;
    //     }
    //      if(this.showdata[i] == 'training')
    //     {
    //       this.roleManagement =true;
    //     }
    //   }
    //}
    // }

    this.contentservice.parentCatId = null
    this.contentservice.countLevel = 0
    this.addEditCourseContentService.previousBreadCrumb = null
    this.addEditCourseContentService.breadcrumbArray = null
    this.addEditCourseContentService.breadtitle = null
    if (this.showdata) {
      for (let i = 0; i < this.showdata.length; i++) {
        if (Number(this.showdata[i].parentMenuId) === 2) {
          this.learnData.push(this.showdata[i]);
        }
      }
    }
  }
  ngOninit() {
    // this.contentservice.data.data = undefined;
    // console.log(this.contentservice.data.data);
  }
  ngOnDestroy() {
    this.alive = false;
  }


  gotopages(url, item) {
    console.log('Menu Item ===>', item);
    if (url == '/pages/plan/courses/content') {
      var passData = {
        id: 0,
        data: undefined
      }
      this.contentservice.data = passData;
    }
    if (item.menuId){
      this.addEditCourseContentService.menuId = item.menuId;
      this.preonaddEditCourseContentService.menuId = item.menuId;
      localStorage.setItem('menuId', item.menuId);
    }
    this.router.navigate([url], { relativeTo: this.routes });
  }
  // gotousers() {
  //   this.router.navigate(['users'], { relativeTo: this.routes });
  // }

  // gotoemployees() {
  //   this.router.navigate(['employees'], { relativeTo: this.routes });
  // }

  // gotocourses() {
  //   this.router.navigate(['/pages/plan/courses/content'], { relativeTo: this.routes });
  // }
  // gotocategory() {
  //   this.router.navigate(['/pages/plan/courses/category'], { relativeTo: this.routes });
  // }

  // gotocoursebundle() {
  //   this.router.navigate(['/pages/plan/courseBundle'], { relativeTo: this.routes });
  // }

  // gotocoursesBlended() {
  //   this.router.navigate(['blended-home'], { relativeTo: this.routes });
  // }

  // gotoroleManagement() {
  //   this.router.navigate(['roleManagement'], { relativeTo: this.routes });
  // }


}
