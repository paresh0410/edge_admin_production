import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { NgxEchartsModule } from 'ngx-echarts';
import { ThemeModule } from '../../@theme/theme.module';
import { LearningComponentMain } from './learning.component';
import { ReactiveFormsModule } from '@angular/forms';
// import { UsersService } from './users/users.service';
// import { EmployeesService } from './employees/employees.service';
// import { CourseBundleService } from './courseBundle/courseBundle.service';
// import { CourseBundle } from './courseBundle/courseBundle';
//import { ContentService } from './courses/content/content.service';
import { CommonModule } from '@angular/common';
import { SortablejsModule } from 'angular-sortablejs';
import { ComponentModule } from '../../component/component.module';

// import { CourseBundleModule } from './courseBundle/courseBundle.module';


@NgModule({
  imports: [
    ThemeModule,
    NgxEchartsModule,
    CommonModule,
    SortablejsModule,
    ReactiveFormsModule,
    ComponentModule,
    // CourseBundleModule
  ],
  declarations: [
    LearningComponentMain,
    // CourseBundle
  ],
  providers: [
    // UsersService,
    // CourseBundleService,
    // ContentService
  ],
  schemas: [ 
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA 
  ]
})
export class LearningMainModule { }
