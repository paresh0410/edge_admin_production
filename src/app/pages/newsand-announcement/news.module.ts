import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CommonModule } from '@angular/common';
import { NewsandAnnouncementComponent } from './newsand-announcement.component';
import { EnrollNewsComponent } from './enrollNews/enrollNews.component';
import { NbTabsetModule } from '@nebular/theme';
import { BasicDetailsComponent } from './enrollNews/basic-details/basic-details.component';
import { EnrolComponent } from './enrollNews/enrol/enrol.component';
import { FormsModule } from '@angular/forms';
import { NewsServices } from './newsand-annoucement.services';
import { EnrolAnouncementComponent } from './enrollNews/enrol-anouncement/enrol-anouncement.component';
import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { SortablejsModule } from 'angular-sortablejs';
import { TagInputModule } from 'ngx-chips';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { JoditAngularModule } from 'jodit-angular';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgxContentLoadingModule } from 'ngx-content-loading';
import { ComponentModule } from '../../component/component.module';
import { NewsRoutingModule } from './news-routing.module';

@NgModule({
  imports: [
    CommonModule,
    NgxDatatableModule,
    NbTabsetModule,
    FormsModule,
    DatepickerModule,
    BsDatepickerModule,
    AngularMultiSelectModule,
    NgxMyDatePickerModule.forRoot(),
    SortablejsModule,
    TagInputModule,
    TooltipModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    JoditAngularModule,
    NgxSkeletonLoaderModule,
    NgxContentLoadingModule,
    ComponentModule,
    NewsRoutingModule,
  ],
  declarations: [
    NewsandAnnouncementComponent,
    EnrollNewsComponent,
    BasicDetailsComponent,
    EnrolComponent,
    EnrolAnouncementComponent,
  ],
  providers: [
    NewsServices,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA,
  ],
})

export class NewsModule {

}
