import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NewsServices } from './newsand-annoucement.services';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CommonFunctionsService } from '../../service/common-functions.service';
import { webApi } from '../../service/webApi';
import { Card } from '../../models/card.model';
import { SuubHeader } from '../components/models/subheader.model';
import { noData } from '../../models/no-data.model';


@Component({
  selector: 'ngx-newsand-announcement',
  templateUrl: './newsand-announcement.component.html',
  styleUrls: ['./newsand-announcement.component.scss']
})

export class NewsandAnnouncementComponent implements OnInit {
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No news available at this time.",
    desc:"",
    titleShow:true,
    btnShow:true,
    descShow:false,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/news',
  }
  news: any = [];
  labelCard: any;
  loginUserdata: any;
  noCategory: boolean = false;
  skeleton = false;
  newslist: any;
  flag: any;
  searchtext: string;
  cardModify: Card =  {
    flag: 'newsAnnoucement',
    titleProp: 'name',
    // image: 'assets/images/badgeNew.png',
    discrption: 'description',
    showImage:   true,
    hoverlable:   true,
    bottomDiv:   true,
    bottomDiscription: true,
    bottomTitle:   true,
    showBottomList:   false,
    showBottomdate:   false,
    showBottomEmployee:   false,
    showBottomManager:   false,
    showBottomCategory:   false,
    showBottomMaximumPoint:   false,
    customCard:  '',
    eyeIcon: true,
    option: true,
    btnLabel: 'Edit',
    defaultImage:'assets/images/badgeNew.png'
  };
  header: SuubHeader  = {
    title:'News',
    btnsSearch: true,
    searchBar: true,
    placeHolder:'Search by News title',
    btnAdd: 'Add News',
    btnAddshow: true,
    showBreadcrumb:true,
    breadCrumbList:[]
  };
  count:number=8
  search: any;
  constructor(public router: Router, public routes: ActivatedRoute, private newsService: NewsServices,
    private spinner: NgxSpinnerService,
    // private toasterService: ToasterService,
    private toastr: ToastrService, private commonFunctionService: CommonFunctionsService) {
    if (localStorage.getItem('LoginResData')) {
      this.loginUserdata = JSON.parse(localStorage.getItem('LoginResData'));
    }
    this.labelCard = 'Edit';
    this.flag = 'news';
    this.getAllAnnouncement();
  }
  ngOnDestroy(){
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0){
      e1[0].classList.add('with-scroll');
    }
  }
  ngAfterContentChecked() {
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0)
    {
      e1[0].classList.remove('with-scroll');
    }

   }
  gotoEnroll() {
    // if (id === 1) {
      const action = 'ADD';
      this.newsService.addEditNewData[0] = action;
      this.newsService.addEditNewData[1] = null;
      this.router.navigate(['add-edit-news'], { relativeTo: this.routes });
    // } else {
    //   const action = 'EDIT';
    //   this.newsService.addEditNewData[0] = action;
    //   this.newsService.addEditNewData[1] = item;
    //   this.router.navigate(['add-edit-news'], { relativeTo: this.routes });
    // }
  }

  gotoEnrollCard(item) {
      const action = 'EDIT';
      this.newsService.addEditNewData[0] = action;
      this.newsService.addEditNewData[1] = item;
      this.router.navigate(['add-edit-news'], { relativeTo: this.routes });
  }

  conentHeight = '0px';
  offset: number = 100;
  ngOnInit() {
    this.conentHeight = '0px';
    const e = document.getElementsByTagName('nb-layout-column');
    console.log(e[0].clientHeight);
    // this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
    if (e[0].clientHeight > 1300) {
      this.conentHeight = '0px';
      this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
    } else {
      this.conentHeight = String(e[0].clientHeight) + 'px';
    }
  }
  sum = 40;
  displayArray=[];
  addItems(startIndex, endIndex, _method, asset) {
    for (let i = startIndex; i < endIndex; ++i) {
      if (asset[i]) {
        this.displayArray[_method](asset[i]);
      } else {
        break;
      }

      // console.log("NIKHIL");
    }
  }
  newArray = [];

  onScroll(event) {

    let element = this.myScrollContainer.nativeElement;
    // element.style.height = '500px';
    // element.style.height = '500px';
    // Math.ceil(element.scrollHeight - element.scrollTop) === element.clientHeight

    // if (element.scrollHeight - element.scrollTop - element.clientHeight < 5) {
    if((Math.ceil(element.scrollHeight - element.scrollTop) - element.clientHeight) < 30){
      if (this.newArray.length > 0) {
        if (this.newArray.length >= this.sum) {
          const start = this.sum;
          this.sum +=20;
          this.addItems(start, this.sum, 'push', this.newArray);
        } else if ((this.newArray.length - this.sum) > 0) {
          const start = this.sum;
          this.sum += this.newArray.length - this.sum;
          this.addItems(start, this.sum, 'push', this.newArray);
        }
      } else {
        if (this.newslist.length >= this.sum) {
          const start = this.sum;
          this.sum += 20;
          this.addItems(start, this.sum, 'push', this.newslist);
        } else if ((this.newslist.length - this.sum) > 0) {
          const start = this.sum;
          this.sum += this.newslist.length - this.sum;
          this.addItems(start, this.sum, 'push', this.newslist);
        }
      }


      console.log('Reached End');
    }
  }
  noNews: boolean = false;
  noDataFound: boolean = false;
  getAllAnnouncement() {
    const param = {
      'tId': this.loginUserdata.data.data.tenantId,
    }
    const _urlGetAllAnn: string = webApi.domain + webApi.url.get_all_announcement;
    this.commonFunctionService.httpPostRequest(_urlGetAllAnn,param)
    // this.newsService.getAllAnn(param)
      .then(rescompData => {
        // this.spinner.hide();
        this.skeleton = true;
        const result = rescompData;
        console.log('getAnn:', rescompData);
        if (result['type'] === true) {
          if (result['data'].length === 0) {
            this.noNews = true;
          } else {
            this.noNews = false;
            this.news = result['data'];
            for (let i = 0; i < this.news.length; i++) {
              this.news[i].name = this.coverthtmltotext(this.news[i].name);
              this.news[i].description = this.coverthtmltotext(this.news[i].description);
              console.log("NEWs=>", this.news);
              this.newslist = this.news;

              // for(let i = 0; i < this.newslist.length; i++) {
              //   this.newslist[i].title = this.newslist[i].name;
              //   this.newslist[i].image = 'assets/images/badgeNew.png';
              // }


            }
            for (let i = 0; i < this.newslist.length; i++) {
              if (this.newslist[i].tags) {
                this.newslist[i].tags = this.newslist[i].tags.split(',');
              }
            }
            this.addItems(0, this.sum, 'push', this.newslist);

          }
        } else {
          // this.spinner.hide();
          this.skeleton = true;
          this.noDataFound = true;
          this.presentToast('error', '');
        }
      }, error => {
        this.spinner.hide();
        this.skeleton = true;
        this.noDataFound = true;
        this.presentToast('error', '');
      });
  }

  coverthtmltotext(data) {
    if(data){
    var htmlString = data;
    var stripedHtml = htmlString.replace(/<[^>]+>/g, '');
    console.log(stripedHtml);
    return (stripedHtml);
    }
  }
  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }

  SarchFilter(event, searchtext) {
    this.header.searchtext = event.target.value
    searchtext= event.target.value;
    console.log(searchtext);
    const val = searchtext.toLowerCase();
    if(val.length>=3 || val.length == 0){
    this.displayArray=[];
    const tempcc = this.news.filter(function (d) {
      return String(d.name).toLowerCase().indexOf(val) !== -1 ||
        String(d.description).toLowerCase().indexOf(val) !== -1 ||
        // d.relayDate.toLowerCase().indexOf(val) !== -1 ||
        // d.manager.toLowerCase().indexOf(val) !== -1 ||
        !val
    })
    console.log(tempcc);
    // this.newslist = tempcc;
    this.newArray = tempcc;
    this.sum=50;
    this.addItems(0, this.sum, 'push', this.newArray);
    if (!this.newArray.length) {
      this.noNews = true;
      this.noDataVal={
        margin:'mt-5',
        imageSrc: '../../../../../assets/images/no-data-bg.svg',
        title:"Sorry we couldn't find any matches please try again",
        desc:".",
        titleShow:true,
        btnShow:false,
        descShow:false,
        btnText:'Learn More',
        btnLink:''
      }
    } else {
      this.noNews = false;
    }
  }
  }
  clear() {
    // this.searchtext = "";
    if(this.header.searchtext){
      this.noNews=false
    this.displayArray = []
    this.header.searchtext = '';
    this.newArray = this.news;
    this.sum=50;
    this.addItems(0, this.sum, 'push', this.newArray);
    // this.sum=50;
    // this.addItems(0, this.sum, 'push', this.newslist);
    // this.getAllAnnouncement()
    // this.searchtext = this.header.searchtext;
    // this.SarchFilter(event, this.searchtext);
    }
  }
  back() {
    window.history.back();
  }
  onSearch(event){
    this.search.name = event.target.value

  }

  title = "";
  enableDisableModal = false;
  enableCourse = true;
  enableDisableNewsData:any ;
  clickTodisable(item) {
    console.log('clickTodisable')
    if (item.visible == 1) {
      this.title = "Disable News";
      this.enableDisableNewsData = item;
      this.enableDisableModal = true;
    } else {
      this.title = "Enable News";
      this.enableDisableNewsData = item;
      this.enableDisableModal = true;
    }

    // this.visibleStatus(courseData);
  }

  enableDisableNews(item) {
    this.spinner.show();
    // this.showSpinner = true
    let visibilityData = {
      annId: item.id,
    };
    if(Number (item.visible) === 1){
      // item.visible = 0;
      visibilityData['status'] = 0;
    }else {
      // item.visible = 1;
      visibilityData['status'] = 1;
    }
    const enable_disable_news_announcement : string = webApi.domain+ webApi.url.enable_disable_news_announcement;
    // webApi.url.enable_disable_news_announcement;
    this.commonFunctionService
      .httpPostRequest(enable_disable_news_announcement, visibilityData)
      //this.contentservice.disableCourse(visibilityData)
      .then(
        (rescompData) => {
          this.spinner.hide();
          console.log("Course Visibility Result", rescompData);
          if (rescompData["type"] == false) {
            this.toastr.error(
              'Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.',
              "Error",
              {
                timeOut: 0,
                closeButton: true,
              }
            );
          } else {


            if(Number (item.visible) === 1){
              item.visible = 0;
            }else {
              item.visible = 1;
            }
            this.toastr.success(rescompData["data"]['msg'], "Success", {
              closeButton: false,
            });
          }
          // this.closeEnableDisableModal();
        },
        (resUserError) => {
          this.toastr.error(
            'Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.',
            "Error",
            {
              timeOut: 0,
              closeButton: true,
            }
          );
          this.spinner.hide();
          // this.closeEnableDisableModal();
        }
      );
  }
  closeEnableDisableModal() {
    this.enableDisableModal = false;
    this.enableDisableNewsData = null;
  }


}
