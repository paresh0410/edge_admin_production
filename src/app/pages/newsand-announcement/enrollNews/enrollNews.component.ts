import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation, ViewChild, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { FormlyValidationMessage } from '@ngx-formly/core/lib/templates/formly.validation-message';
import { Router, ActivatedRoute } from '@angular/router';
import { NewsServices } from './../newsand-annoucement.services';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { DatePipe } from '@angular/common';
import { XlsxToJsonService } from '../../plan/users/uploadusers/xlsx-to-json-service';
import { JsonToXlsxService } from '../../coaching/participants/bulk-upload-coaching/json-to-xlsx.service';
import { SuubHeader } from '../../components/models/subheader.model';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { BrandDetailsService } from '../../../service/brand-details.service';
@Component({
  selector: 'ngx-enroll',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './enrollNews.component.html',
  styleUrls: ['./enrollNews.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [DatePipe]
})
export class EnrollNewsComponent implements OnInit {
  @Output() saveOut = new EventEmitter<any>();

  @ViewChild('fileUpload') fileUpload: any;
  // Button Enabling Function
  ButtonAvailabilty = false;
  tabstrue = true;
  content: any = [];
  addEditModRes: any;
  fileUploadRes: any;
  showEnrolpage: boolean = false;
  showBulkpage: boolean = false;
  getDataForAddEdit: any;
  addAction: boolean = false;
  fileName: any;
  fileReaded: any;
  enableUpload: boolean = false;
  file: any = [];
  datasetPreview: any;
  preview: boolean = false;
  uId: any;
  tenantId: any;
  demoData: any = [];
  resultdata: any = [];
  selected: any = [];
  rows: any = [];
  temp = [];
  result: any;
  errorMsg: any;
  backflag = 1;
  header: SuubHeader;
  // header: SuubHeader  = {
  //   title:'Add News',
  //   btnsSearch: true,
  //   searchBar: true,
  //   dropdownlabel: ' ',
  //   drplabelshow: false,
  //   drpName1: '',
  //   drpName2: ' ',
  //   drpName3: '',
  //   drpName1show: false,
  //   drpName2show: false,
  //   drpName3show: false,
  //   btnName1: '',
  //   btnName2: 'Enrol',
  //   btnName3: 'Bulk Enrol',
  //   btnAdd: '',
  //   btnName1show: false,
  //   btnName2show: true,
  //   btnName3show: true,
  //   btnBackshow: true,
  //   btnAddshow: true,
  //   filter: false,

  // };
  labels: any = [
    { labelname: 'STATUS', bindingProperty: 'status', componentType: 'text' },
    { labelname: 'ECN', bindingProperty: 'name', componentType: 'text' },
    { labelname: 'FULLNAME', bindingProperty: 'fullname', componentType: 'text' },
    { labelname: 'ENROL Date', bindingProperty: 'enrolDate', componentType: 'text' },

  ];
  isdata: any;

  private xlsxToJsonService: XlsxToJsonService = new XlsxToJsonService();
  currentBrandData: any;
  //Enrol: boolean;
  constructor(public router: Router, public routes: ActivatedRoute, private newsService: NewsServices,
    public brandService: BrandDetailsService,
    private commonFunctionService: CommonFunctionsService,
    private toastr: ToastrService, private spinner: NgxSpinnerService, private exportService: JsonToXlsxService,
    public cdf: ChangeDetectorRef, private datePipe: DatePipe) {
    var userData = JSON.parse(localStorage.getItem('LoginResData'));
    console.log('userData', userData.data);
    this.uId = userData.data.data.id;
    this.tenantId = userData.data.data.tenantId;
    console.log(this.newsService.addEditNewData, "addEdit news")
    if (this.newsService.addEditNewData) {
      this.getDataForAddEdit = this.newsService.addEditNewData;
      this.content = this.getDataForAddEdit[1];
      if (this.content) {
        this.content.courseId = this.content.id;
      }
    }
    if (this.getDataForAddEdit[0] == "ADD") {
      this.addAction = true;

    } else {
      this.addAction = false;
    }
  }
  selectedTab(tabEvent) {
    console.log('tab Selected', tabEvent);

    if (tabEvent.tabTitle == 'Basic Details') {
      this.Details = true;
      this.Enrol = false;
      this.backflag = 1;
        this.header = {
          title:this.content?this.content.name: 'Add News',
          btnsSearch: true,
          btnBackshow: true,
          showBreadcrumb: true,
          breadCrumbList: [
            {
              'name': 'News',
              'navigationPath': '/pages/news',
            },]
        };
        this.cdf.detectChanges();
    }
    else {
      this.Details = false;
      this.Enrol = true;
        this.header = {
          title:this.content?this.content.name: 'Add News',
          btnsSearch: true,
          btnName2: 'Enrol',
          btnName3: 'Bulk Enrol',
          btnName2show: true,
          btnName3show: true,
          btnBackshow: true,
          showBreadcrumb: true,
          breadCrumbList: [
            {
              'name': 'News',
              'navigationPath': '/pages/news',
            },]
        };
        this.cdf.detectChanges();
      
      // this.Details==false;
      this.backflag = 1;
    }
  }

  Details: boolean;
  Enrol: boolean;
  ButtonEnableEnroll(event) {
    console.log(this.newsService.addEditNewData, "ngx-enroll")
    // console.log(event.tabTitle);
    if (event.tabTitle === 'Enrol') {
      this.Enrol = true;
      this.Details = false;
      this.ButtonAvailabilty = true;
        this.header = {
          title:this.content?this.content.name: 'Add News',
          btnsSearch: true,
          btnName2: 'Enrol',
          btnName3: 'Bulk Enrol',
          btnName2show: true,
          btnName3show: true,
          btnBackshow: true,
          showBreadcrumb: true,
          breadCrumbList: [
            {
              'name': 'News',
              'navigationPath': '/pages/news',
            },]
        };
        this.cdf.detectChanges();
        this.spinner.show();
        setTimeout(() => {
          this.spinner.hide()
        }, 2000);
      // this.Details==false;
      this.backflag = 1;
    } else {
      this.Enrol = false;
      this.Details = true;
      this.ButtonAvailabilty = false;
      this.backflag = 1;
        this.header = {
          title:this.content?this.content.name: 'Add News',
          btnsSearch: true,
          btnName6: 'Save',
          btnName6show: true,
          btnBackshow: true,
          showBreadcrumb: true,
          breadCrumbList: [
            {
              'name': 'News',
              'navigationPath': '/pages/news',
            },]
        };
        this.cdf.detectChanges();
        this.spinner.show();
        setTimeout(() => {
          this.spinner.hide()
        }, 2000);

    }
  }
  ngOnInit() {
    this.currentBrandData = this.brandService.getCurrentBrandData();
    this.fileName = 'Click here to upload an excel file to enrol ' + this.currentBrandData.employee.toLowerCase() + ' to this course'
  }
  enrolpage() {
     this.ButtonAvailabilty = true;
      this.header = {
        title:this.content?this.content.name: 'Add News',
        btnsSearch: true,
        btnName3: 'Bulk Enrol',
        btnName3show: true,
        btnBackshow: true,
        showBreadcrumb: true,
        breadCrumbList: [
          {
            'name': 'News',
              'navigationPath': '/pages/news',
          },]
      };
    this.backflag = 2;
    this.showEnrolpage = true;
    this.showBulkpage = false;
    this.tabstrue = true;
  }
  bulkEnrol() {
    this.showEnrolpage = false;
    this.showBulkpage = true;
    this.tabstrue = true;
      // this.ButtonAvailabilty = true;
      this.header = {
        title:this.content?this.content.name: 'Add News',
        btnsSearch: true,
        btnName2: 'Enrol',
        btnName2show: true,
        btnBackshow: true,
        showBreadcrumb: true,
        breadCrumbList: [
          {
            'name': 'News',
              'navigationPath': '/pages/news',
          },]
      };
    this.backflag = 2;
  }
  back() {
    if (this.backflag == 2) {
        this.header = {
          title:this.content?this.content.name: 'Add News',
          btnsSearch: true,
          btnName2: 'Enrol',
          btnName3: 'Bulk Enrol',
          btnName2show: true,
          btnName3show: true,
          btnBackshow: true,
          showBreadcrumb: true,
          breadCrumbList: [
            {
              'name': 'News',
              'navigationPath': '/pages/news',
            },]
        };
      
      this.backflag = 1;
      this.ButtonAvailabilty = true;
      this.showEnrolpage = false;
      this.showBulkpage = false;
      this.preview = false;
      this.tabstrue = false;
    }
    else {
      this.showEnrolpage = false;
      this.tabstrue = false;
      this.router.navigate(['../'], { relativeTo: this.routes });
    }
  }
  backtoenrol() {
    this.ButtonAvailabilty = true;
    this.showEnrolpage = false;
    this.showBulkpage = false;
    this.preview = false;
    this.tabstrue = false;
  }

  bulkUploadData: any = [];
  uploadedData: any = [];
  showInvalidExcel: boolean = false;
  readUrl(event: any) {

    var validExts = new Array('.xlsx', '.xls');
    var fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
      this.toastr.warning('Valid file types are ' + validExts.toString(), 'Warning', {
        closeButton: false
      });

      this.cancel();
    }
    else {
      // return true;
      if (event.target.files && event.target.files[0]) {
        this.fileName = event.target.files[0].name;
        //read file from input
        this.fileReaded = event.target.files[0];

        if (this.fileReaded != '' || this.fileReaded != null || this.fileReaded != undefined) {
          this.enableUpload = true;
          this.showInvalidExcel = false;
        }

        this.file = event.target.files[0];

        this.bulkUploadData = event.target.files[0];
        console.log('this.bulkUploadData', this.bulkUploadData);

        this.xlsxToJsonService.processFileToJson({}, this.file).subscribe(data => {
          const resultSheets = Object.getOwnPropertyNames(data['sheets']);
          console.log('File Property Names ', resultSheets);
          let sheetName = resultSheets[0];
          this.result = data['sheets'][sheetName];
          console.log('dataSheet', data);
          console.log('this.result', this.result);

          if (this.result.length > 0) {
            this.uploadedData = this.result;
          }
          console.log('this.uploadedData', this.uploadedData);
        });

        var reader = new FileReader();
        reader.onload = (event: ProgressEvent) => {
          let fileUrl = (<FileReader>event.target).result;
          // event.setAttribute('data-title', this.fileName);
          // console.log(this.fileUrl);
        }
        reader.readAsDataURL(event.target.files[0]);
      }
    }

    // if(event.target.files[0].name.split('.')[event.target.files[0].name.split('.').length-1] === 'xlsx'){
    //     let file = event.target.files[0];
    // } else if(event.target.files[0].name.split('.')[event.target.files[0].name.split('.').length-1] === 'xls'){
    //     let file = event.target.files[0];
    // }else{
    //   alert('Invalid file selected, valid files are of ' +
    //          validExts.toString() + ' types.');
    // }
  }
  cancel() {
    // this.fileUpload.nativeElement.files = [];
    console.log(this.fileUpload.nativeElement.files);
    this.fileUpload.nativeElement.value = '';
    console.log(this.fileUpload.nativeElement.files);
    this.fileName = 'Click here to upload an excel file to enrol ' + this.currentBrandData.employee.toLowerCase() + ' to this course'
    this.enableUpload = false;
    this.file = [];
    this.preview = false;
  }
  save() {
    this.saveOut.emit();
  }


  uploadfile() {
    this.spinner.show();
    this.content.areaId = 30;
    this.content.tenantId = this.tenantId;
    this.content.userId = this.uId;
    console.log(this.content);
    var fd = new FormData();
    fd.append('content', JSON.stringify(this.content));
    fd.append('file', this.file);

    this.newsService.TempManEnrolBulk(fd).then(result => {
      console.log(result);
      // this.loader =false;

      var res = result;
      try {
        if (res['data1'][0]) {
          this.resultdata = res['data1'][0];
          for (let i = 0; i < this.resultdata.length; i++) {
            this.resultdata[i].enrolDate = this.formatDate(this.resultdata[i].enrolDate);
          }

          this.datasetPreview = this.resultdata;
          this.preview = true;
          this.cdf.detectChanges();
          // this.spinner.hide();
        }
      } catch (e) {

        this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
          timeOut: 0,
          closeButton: true
        });
      }
      this.spinner.hide();

    },
      resUserError => {
        // this.loader =false;
        this.spinner.hide();
        this.errorMsg = resUserError;
        // this.closeEnableDisableCourseModal();
      });
  }

  formatDate(date) {
    var d = new Date(date)
    var formatted = this.datePipe.transform(d, 'dd-MMM-yyyy');
    return formatted;
  }

  clearPreviewData() {
    this.datasetPreview = [];
    this.cancel();
  }
  savebukupload1() {
    var data = {
      userId: this.content.userId,
      coursId: this.content.courseId,
      areaId: 30,
      tId: this.tenantId,
    }
    // this.loader = true;
    this.spinner.show();
    this.newsService.finalManEnrolBulk(data)
      .then(rescompData => {
        // this.loader =false;
        this.spinner.hide();
        var temp: any = rescompData;
        this.addEditModRes = temp.data[0];
        this.result = "true";
        if (temp == 'err') {
          // var modUpdate: Toast = {
          //   type: 'error',
          //   title: 'Enrol',
          //   body: 'Something went wrong',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(modUpdate);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        } else if (temp.type == false) {
          // var modUpdate: Toast = {
          //   type: 'error',
          //   title: 'Enrol',
          //   body: this.addEditModRes[0].msg,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(modUpdate);

          this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
            timeOut: 0,
            closeButton: true
          });
        } else {
          // var modUpdate: Toast = {
          //   type: 'success',
          //   title: 'Enrol',
          //   body: this.addEditModRes[0].msg,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(modUpdate);

          this.toastr.success(this.addEditModRes[0].msg, 'Success', {
            closeButton: false
          });
          this.showEnrolpage = false;
          this.showBulkpage = false;
          this.preview = false;
          this.enableUpload = false;
          // this.result = "true";
          // this.allEnrolUser(this.content);
        }
        console.log('Enrol bulk Result ', this.addEditModRes);
        this.cdf.detectChanges();
      },
        resUserError => {
          // this.loader = false;
          this.errorMsg = resUserError;
        });
  }

  invaliddata: any = [];
  exportToExcelInvalid() {
    this.exportService.exportAsExcelFile(this.invaliddata, 'Enrolment Status');
  }

  savebukupload() {
    // console.log(this.content.id,"content id")
    console.log(this.newsService.addEditNewData, "new")
    //new change
    var temp: any = []
    temp = this.newsService.addEditNewData[1]
    this.content = this.newsService.addEditNewData[1]

    //  console.log(temp,"temp")
    // this.content.id = temp.id
    //end new change

    // this.loader = true;
    this.invaliddata = [];
    if (this.uploadedData.length > 0 && this.uploadedData.length <= 2000) {
      this.spinner.show();
      // var option: string = Array.prototype.map
      //   .call(this.uploadedData, function (item) {
      //     console.log("item", item);
      //     return Object.values(item).join("#");
      //   })
      //   .join("|");

      // New Function as per requirement.

      var option: string = Array.prototype.map
        .call(this.uploadedData, (item) => {
          const array = Object.keys(item);
          let string = '';
          array.forEach(element => {
            if (element === 'EnrolDate') {
              string += this.commonFunctionService.formatDateTimeForBulkUpload(item[element]);
            } else {
              string = item[element] + '#';
            }
          });
          // console.log("item", item);
          // return Object.values(item).join("#");
          return string;
        })
        .join("|");

      //  console.log('this.courseDataService.workflowId', this.courseDataService.workflowId);

      const data = {
        wfId: null,
        aId: 30,
        iId: this.content.id,

        // userId: this.content.userId,
        upString: option
      }
      console.log('data', data);
      this.newsService.areaBulkEnrol(data)
        .then(rescompData => {
          // this.loader =false;
          this.spinner.hide();
          var temp: any = rescompData;
          this.addEditModRes = temp.data;
          if (temp == 'err') {
            this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
              timeOut: 0,
              closeButton: true
            });
          } else if (temp.type == false) {
            this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
              timeOut: 0,
              closeButton: true
            });
          } else {
            this.enableUpload = false;
            this.fileName = 'Click here to upload an excel file to enrol ' + this.currentBrandData.employee.toLowerCase() + ' to this course'
            if (temp['data'].length > 0) {
              if (temp['data'].length === temp['invalidCnt'].invalidCount) {
                this.toastr.warning('No valid ' + this.currentBrandData.employee.toLowerCase() + 's found to enrol.', 'Warning', {
                  closeButton: false
                });
              } else {
                this.toastr.success('Enrol successfully.', 'Success', {
                  closeButton: false
                });
              }
              this.datasetPreview = temp['data'];
              this.invaliddata = temp['data'];
              this.showInvalidExcel = true;

              // this.invaliddata = temp['data'];
              // if (this.uploadedData.length == this.invaliddata.length) {
              //   this.toastr.warning('No valid employees found to enrol.', 'Warning', {
              //     closeButton: false
              //     });
              // } else {
              //   this.toastr.success('Enrol successfully.', 'Success', {
              //     closeButton: false
              //     });
              // }
            } else {
              this.invaliddata = [];
              this.showInvalidExcel = false;
            }




            // this.backToEnrol()
            this.cdf.detectChanges();
            // this.preview = false;
            // this.showInvalidExcel = true;

          }
          console.log('Enrol bulk Result ', this.addEditModRes);
          this.cdf.detectChanges();
        },
          resUserError => {
            //this.loader = false;
            this.errorMsg = resUserError;
          });
    } else {
      if (this.uploadedData.length > 2000) {
        this.toastr.warning('File Data cannot exceed more than 2000', 'warning', {
          closeButton: true
        });
      }
      else {
        this.toastr.warning('No Data Found', 'warning', {
          closeButton: true
        });
      }
    }

  }

  passData(val) {
    this.isdata = val;
    // this.eventsSubject.next();
    setTimeout(() => {
      this.isdata = '';
      this.cdf.detectChanges();
    }, 2000);
  }
}
