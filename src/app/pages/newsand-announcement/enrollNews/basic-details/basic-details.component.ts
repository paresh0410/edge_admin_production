import { Component, OnInit, ChangeDetectorRef, Input, OnChanges,SimpleChanges, ViewChild } from '@angular/core';
import { NewsServices } from '../../newsand-annoucement.services';
import { NgxSpinnerService } from 'ngx-spinner';
// import { ToasterService, Toast } from 'angular2-toaster';
import { Router, ActivatedRoute } from '@angular/router';
import { ArrayBase } from '@syncfusion/ej2-ng-base';
import { webAPIService } from './../../../../service/webAPIService';
import { webApi } from './../../../../service/webApi';
import { NullTemplateVisitor } from '@angular/compiler';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
import { CommonFunctionsService } from '../../../../service/common-functions.service';
import { DataSeparatorService } from '../../../../service/data-separator.service';
import {EnrollNewsComponent} from '../enrollNews.component';

@Component({
  selector: 'ngx-basic-details',
  templateUrl: './basic-details.component.html',
  styleUrls: ['./basic-details.component.scss']
})
export class BasicDetailsComponent implements OnInit, OnChanges {

	@Input() inpdata: any;
  @ViewChild('formData') formval: any;
  addOrEditValue: any = [];
  attachment: any = null;
  announcementDetails: any = {};
  enableUpload: any;
  fileName: any = 'You can drag and drop files here to add them.';
  fileReaded: any;
  fileUpload: any;
  fileUrl: any;
  notificationData: any = [];
  noNotifications: boolean = false;
  notify: any;
  selectFileTitle: any;
  uploadedData: any;
  init = false;
  loginUserdata: any;
  getDataForAddEdit: any = [];
  msg: any;
  name: any;
  detail: any;
  config = {
    // height: '100',
    uploader: {
      insertImageAsBase64URI: true,
    },
    allowResizeX: false,
    allowResizeY: false,
    placeholder: 'Min 2 and Max 1000 characters',
    limitChars: 1000,
  };
  tagsArray = [];

  constructor(
    private newsService: NewsServices,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    public router: Router,
    public routes: ActivatedRoute,
    private cdf: ChangeDetectorRef,
    protected webApiService: webAPIService,
    // private toastr: ToastrService,
    private http1: HttpClient,
    private EnrollNewsComponent:EnrollNewsComponent,
    private commonFunctionService: CommonFunctionsService,
    private dataSeparatorService: DataSeparatorService ) {
      this.getHelpContent();

    if (localStorage.getItem('LoginResData')) {
      this.loginUserdata = JSON.parse(localStorage.getItem('LoginResData'));
    }
    if (this.newsService.addEditNewData) {
      this.getDataForAddEdit = this.newsService.addEditNewData;
    }
    console.log('Seperator Pipe ==>', this.dataSeparatorService.Pipe);
    console.log('Seperator Hash ==>', this.dataSeparatorService.Hash);
    console.log('Seperator Dollar ==>', this.dataSeparatorService.Dollar);
    // this.prepareData();
  }


  addAction: boolean = false;
  prepareData() {
    //this.spinner.show();
    if (this.getDataForAddEdit[0] === 'ADD') {
      this.addAction = true;
      this.basicDetails = {
        NotificationTypeName: null,
        NotificationTypeText: null,
        AppNotificationCheckBox: false,
        AppNotificationSubject: null,
        AppNotificationDescrition: null,
        SMSChecked: false,
        SMSText: null,
        EmailCheckBox: false,
        EmailSubject: null,
        EmailDescrition: null,
        attchRef: null,
        id: 0,
      };
    } else if (this.getDataForAddEdit[0] === 'EDIT') {
      this.addAction = false;
      this.getNotifyById(this.getDataForAddEdit[1]);
    }
  }

  ngOnInit() {
    this.getTagsData();
  }

  ngOnChanges(changes: SimpleChanges): void {
		if(this.inpdata === 'news') {
      // this.submitDetail(this.basicDetails);
      this.validate();
		}
	}

  // Object to store form data
  basicDetails: any = {
    AppNotificationCheckBox: false,
    EmailCheckBox: false,
    // EmailCheckBox: false,
    SMSChecked: false,
  };

  // function to retrieve form data and sending it to the database

  cancelFile() {
    // this.fileUpload.nativeElement.files = [];
    this.attachment = null;
    // console.log(this.fileUpload.nativeElement.files);
    if (this.fileUpload) {
      if (this.fileUpload.nativeElement) {
        this.fileUpload.nativeElement.value = null;
      }
    }

    // console.log(this.fileUpload.nativeElement.files);
    this.fileName = 'You can drag and drop files here to add them.';
    this.selectFileTitle = 'No file chosen';
    // this.enableUpload = false;
  }

  getdatareadyforNotifications(basicDetailsData) {
    // tslint:disable-next-line: no-console
    console.log(basicDetailsData);

    let qoptionString: string = '';

    if (basicDetailsData.AppNotificationCheckBox) {
      const appNotifiString =
        'Announcement__App' +
        this.dataSeparatorService.Hash+
        'Announcement_App' +
        this.dataSeparatorService.Hash +
        3 +
        this.dataSeparatorService.Hash +
        basicDetailsData.AppNotificationSubject +
        this.dataSeparatorService.Hash +
        basicDetailsData.AppNotificationDescrition +
        this.dataSeparatorService.Hash +
        1 +
        this.dataSeparatorService.Pipe
        ;

      qoptionString += appNotifiString;
      // console.log(appNotifiString);
    }
    if (basicDetailsData.SMSChecked) {
      const smsString =
        'Announcement_Sms' +
        this.dataSeparatorService.Hash +
        'Announcement_Sms' +
        this.dataSeparatorService.Hash +
        1 +
        this.dataSeparatorService.Hash +
        'null' +
        this.dataSeparatorService.Hash +
        basicDetailsData.SMSText +
        this.dataSeparatorService.Hash +
        1 +
        this.dataSeparatorService.Pipe;

      qoptionString += smsString;
      // console.log(smsString);
    }
    if (basicDetailsData.EmailCheckBox) {
      const emailString =
      // this.dataSeparatorService.Pipe +
        'Announcement_Email' +
        this.dataSeparatorService.Hash +
        'Announcement_Email' +
        this.dataSeparatorService.Hash +
        2 +
        this.dataSeparatorService.Hash +
        basicDetailsData.EmailSubject +
        this.dataSeparatorService.Hash +
        basicDetailsData.EmailDescrition +
        this.dataSeparatorService.Hash +
        1 +
        this.dataSeparatorService.Pipe;
      qoptionString += emailString;
      // console.log(emailString);
    }
    console.log(qoptionString.length);
    console.log(qoptionString);
    return qoptionString;
  }

  getNotifyById(data) {
    this.spinner.show();
    console.log('data', data);
    const param = {
      tId: this.loginUserdata.data.data.tenantId,
      nId: data.id
    };
    const _urlGetAllNotify: string = webApi.domain + webApi.url.get_all_notify_by_id;
    this.commonFunctionService.httpPostRequest(_urlGetAllNotify,param)
    // this.newsService.getAllNotify(param)
    .then(
      rescompData => {
        this.spinner.hide();
        const result = rescompData;
        // console.log('getAnnNotify:', rescompData);
        if (result['type'] === true) {
          if (result['data'].length === 0) {
            this.noNotifications = true;
          } else {
            this.notify = result['data'];
            // console.log('this.notify', this.notify);
            this.ResponseDataOfServer(this.notify, data);
          }
          setTimeout(() => {
            this.cdf.detectChanges();
          }, 2000);

        } else {
          this.spinner.hide();
          this.presentToast('error', '');
        }
      },
      error => {
        this.spinner.hide();
        this.presentToast('error', '');
      },
    );
  }

  presentToast(type, body) {
		if(type === 'success'){
		  this.toastr.success(body, 'Success', {
			closeButton: false
		   });
		} else if(type === 'error'){
		  this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
			timeOut: 0,
			closeButton: true
		  });
		} else{
		  this.toastr.warning(body, 'Warning', {
			closeButton: false
			})
		}
	  }

  readFileUrl(event: any) {
    const validExts = new Array('.png', '.jpg', '.jpeg', '.xlsx', '.xls');
    let fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
      // const toast: Toast = {
      //   type: 'error',
      //   title: 'Invalid file selected!',
      //   body: 'Valid files are of ' + validExts.toString() + 'types.',
      //   showCloseButton: true,
      //   // tapToDismiss: false,
      //   timeout: 4000,
      // };
      // this.toastr.pop(toast);
      this.presentToast('warning', 'Valid file types are ' + validExts.toString());
      this.cancelFile();
    } else {
      // return true;
      if (event.target.files && event.target.files[0]) {
        this.fileName = event.target.files[0].name;
        // read file from input
        this.fileReaded = event.target.files[0];

        if (
          this.fileReaded != '' ||
          this.fileReaded != null ||
          this.fileReaded != undefined
        ) {
          this.enableUpload = true;
        }

        let file = event.target.files[0];
        this.attachment = event.target.files[0];
        console.log('this.attachment', this.attachment);

        var reader = new FileReader();
        reader.onload = (event: ProgressEvent) => {
          this.fileUrl = (<FileReader>event.target).result;
          /// console.log(this.fileUrl);
        };
        reader.readAsDataURL(event.target.files[0]);
      }
    }
  }

  ResponseDataOfServer(ArrayOfDatta, notificationTypeData) {
    this.spinner.show();
    console.log('array data > ', ArrayOfDatta);
    console.log('data > ', notificationTypeData);

    if (this.getDataForAddEdit[1].attachRef) {
      console.log(
        'fileNamexx',
        this.getDataForAddEdit[1].attachRef.lastIndexOf('/') + 1
      );
      this.fileName = this.getDataForAddEdit[1].attachRef.substr(
        this.getDataForAddEdit[1].attachRef.lastIndexOf('/') + 1
      );
      console.log('fileName', this.fileName);
    }

    this.basicDetails.id = notificationTypeData.id;
    this.basicDetails.NotificationTypeName = notificationTypeData.name;
    this.basicDetails.NotificationTypeText = notificationTypeData.description;

    for (const data of ArrayOfDatta) {
      if (data.modeIds == 1) {
        this.basicDetails.SMSChecked = true;
        // this.smsValidCheck(true);
        // this.basicDetails.AppNotificationSubject = data.subject;
        this.basicDetails.SMSText = data.templates;
        this.setUnsetValidation('sms', true);
      }
      if (data.modeIds == 3) {
        this.basicDetails.AppNotificationCheckBox = true;
        this.basicDetails.AppNotificationSubject = data.subject;
        this.basicDetails.AppNotificationDescrition = data.templates;
        this.setUnsetValidation('app', true);
      }
      if (data.modeIds == 2) {
        this.basicDetails.EmailCheckBox = true;
        this.basicDetails.EmailSubject = data.subject;
        this.basicDetails.EmailDescrition = data.templates;
        this.setUnsetValidation('email', true);
      }
    }
    this.cdf.detectChanges();
    this.spinner.hide();
  }

  passParams: any;
  fileUploadRes: any;
  fileres: any;
  errorMsg: any;

  basicDetailsObjectArray: any = {
    SMSText: false,
    AppNotificationSubject: false,
    AppNotificationDescrition: false,
    EmailSubject: false,
    EmailDescrition: false,
    NotificationTypeName: false
  };

  validate() {
    // if((this.formval.form.valid && this.basicDetails.SMSChecked && this.basicDetails.EmailCheckBox &&
    //   this.basicDetails.EmailSubject && this.basicDetails.EmailDescrition))
    // {
    //   this.submitDetail(this.basicDetails);


    // }
    // else{
    // if (this.basicDetails.EmailCheckBox) {
    //   if (!this.basicDetails.EmailSubject || !this.basicDetails.EmailDescrition) {
    //     this.toastr.warning('Please fill required email details');
    //   } else {
    //     // this.submitDetail(this.basicDetails);
    //   }
    // } else if (this.basicDetails.SMSChecked) {
    //   if (!this.basicDetails.SMSText) {
    //     this.toastr.warning('Please fill required sms details');
    //   } else {
    //     // this.submitDetail(this.basicDetails);
    //   }
    // }
    // else if(!this.formval.form.valid && !this.basicDetails.SMSChecked && !this.basicDetails.SMSChecked) {
    //   this.toastr.warning('Please fill required details');
    //   return;
    // }
    // else {
    //   // this.toastr.warning('Please fill required details');

    // }
    // }
    if (this.basicDetails.SMSChecked || this.basicDetails.EmailCheckBox || this.basicDetails.AppNotificationCheckBox)
    {
        if(this.formval.valid){
          this.submitDetail(this.formval);
        }else {
          Object.keys(this.formval.controls).forEach(key => {
            this.formval.controls[key].markAsDirty();
          });
        }
    }else {
      this.toastr.warning('Please select at least one option.');
      Object.keys(this.formval.controls).forEach(key => {
        this.formval.controls[key].markAsDirty();
      });
    }
  }

  submitDetail(f) {
    console.log(f);
    // this.name = inputData.AppNotificationSubject;
    // this.detail = inputData.AppNotificationDescrition;
    // this.spinner.show();
    console.log(f);
    const qOptionsStr = this.getdatareadyforNotifications(this.basicDetails);

    const param = {
      annId: this.addAction ? 0 : this.basicDetails.id,
      // annName: inputData.NotificationTypeName,
      // annDetails: inputData.NotificationTypeText,
      annName: this.basicDetails.NotificationTypeName,
      annDetails: this.detail,
      annAttach: this.basicDetails.EmailCheckBox ? f.attachRef : null,
      annRelayDate: NullTemplateVisitor,
      annIconRef: null,
      tId: this.loginUserdata.data.data.tenantId,
      userId: this.loginUserdata.data.data.id,
      qOptions: qOptionsStr,
    };
    console.log('param', param);
    this.passParams = param;



    // if (this.attachment) {
       // var fd = new FormData();
    // fd.append('content', JSON.stringify(this.passParams));
    // fd.append('file', this.attachment);
    // console.log('File Data ', fd);

    // console.log('Asset File', this.attachment);
    // console.log('Asset Data ', this.passParams);
    // let fileUploadUrl = webApi.domain + webApi.url.fileUpload;

    //   this.webApiService.getService(fileUploadUrl, fd).then(
    //     rescompData => {
    //       var temp: any = rescompData;
    //       this.fileUploadRes = temp;
    //       console.log('rescompData', this.fileUploadRes);
    //       this.attachment = null;
    //       if (temp === 'err') {
    //         // this.notFound = true;
    //         // var thumbUpload: Toast = {
    //         //   type: 'error',
    //         //   title: 'Attachment',
    //         //   body: 'Unable to upload attachment.',
    //         //   // body: temp.msg,
    //         //   showCloseButton: true,
    //         //   timeout: 4000,
    //         // };
    //         // this.toastr.pop(thumbUpload);
    //         this.presentToast('error', '');
    //         this.spinner.hide();
    //       } else if (temp.type === false) {
    //         // var thumbUpload: Toast = {
    //         //   type: 'error',
    //         //   title: 'Attachment',
    //         //   body: 'Unable to upload attachment.',
    //         //   // body: temp.msg,
    //         //   showCloseButton: true,
    //         //   timeout: 4000,
    //         // };
    //         // this.toastr.pop(thumbUpload);
    //   this.presentToast('error', '');
    //         this.spinner.hide();
    //       } else {
    //         if (
    //           this.fileUploadRes.data != null ||
    //           this.fileUploadRes.fileError != true
    //         ) {
    //           this.passParams.annAttach = this.fileUploadRes.data.file_url;
    //           console.log(
    //             'this.passparams.annAttach',
    //             this.passParams.annAttach
    //           );
    //           this.spinner.hide();
    //           // this.addEditDetails(this.passParams);
    //         } else {
    //           // var thumbUpload: Toast = {
    //           //   type: 'error',
    //           //   title: 'Attachment',
    //           //   // body: 'Unable to upload course thumbnail.',
    //           //   body: this.fileUploadRes.status,
    //           //   showCloseButton: true,
    //           //   timeout: 4000,
    //           // };
    //           // this.toastr.pop(thumbUpload);
    //           this.presentToast('error', '');
    //           this.spinner.hide();
    //         }
    //       }
    //       console.log('File Upload Result', this.fileUploadRes);
    //       var res = this.fileUploadRes;
    //       this.fileres = res.data.file_url;
    //     },
    //     resUserError => {
    //       this.errorMsg = resUserError;
    //       console.log('File upload this.errorMsg', this.errorMsg);
    //       this.spinner.hide();
    //     }
    //   );
    // } else {
    //   this.spinner.hide();
    //   this.addEditDetails(this.passParams);
    //   // const DataTab = {
    //     const DataTab = {
    //       tabTitle: 'Enrol',
    //     }
    //     this.EnrollNewsComponent.ButtonEnableEnroll(DataTab);
    // }
    this.addEditDetails(param);
  }

  addEditDetails(param) {
    console.log(param);
    // if (param.annId == 0) {
    //   this.msg = 'News added.';
    // } else {
    //   this.msg = 'Announcement updated';
    // }
    this.spinner.show();
    const _urlNewsAddEdit: string = webApi.domain + webApi.url.add_edit_news_announcement;
    this.commonFunctionService.httpPostRequest(_urlNewsAddEdit,param)
    // this.newsService.AddEditNews(param)
    .then(
      rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('UpdateAssetResponse:', rescompData);
        if (result['type'] === true) {
          // var catUpdate: Toast = {
          //   type: 'success',
          //   title: 'Announcement',
          //   body: this.msg,
          //   showCloseButton: true,
          //   timeout: 4000,
          // };
          this.addAction  = false;
          if(result['data'] && result['data'].length !=0){
            if(result['data'][0]['id']){
              this.basicDetails.id = result['data'][0]['id'];
              const array = ['Edit', { 'id' : result['data'][0]['id']}]
              this.newsService.addEditNewData = array;
              }
            this.msg = result['data'][0]['msg'];
          }

          this.spinner.hide();
          // this.toastr.pop(catUpdate);
          this.presentToast('success', this.msg);
          // event.tabTitle === 'Enrol'
          const DataTab = {
            tabTitle: 'Enrol',
          };
          this.EnrollNewsComponent.ButtonEnableEnroll(DataTab);
          // this.router.navigate(['../'], { relativeTo: this.routes });

        } else {
          // var catUpdate: Toast = {
          //   type: 'error',
          //   title: 'Announcement',
          //   body: 'Unable to update Announcement.',
          //   showCloseButton: true,
          //   timeout: 4000,
          // };
          // this.toastr.pop(catUpdate);
          this.presentToast('error', '');
        }

      },
      error => {
        this.spinner.hide();
        // const toast: Toast = {
        //   type: 'error',
        //   // title: 'Server Error!',
        //   body: 'Something went wrong.please try again later.',
        //   showCloseButton: true,
        //   timeout: 4000,
        // };
        // this.toastr.pop(toast);
        this.presentToast('error', '');
      },
    );
  }

  // Help Code Start Here //

  helpContent: any;
  getHelpContent() {
    return new Promise(resolve => {
      this.http1
        .get('../../../../../../assets/help-content/addEditCourseContent.json')
        .subscribe(
          data => {
            this.helpContent = data;
            console.log('Help Array', this.helpContent);
            this.cdf.detectChanges();
            this.prepareData();
          },
          err => {
            resolve('err');
          }
        );
    });
    // return this.helpContent;
  }

  // Help Code Ends Here //
  getTagsData(){
    let menuId;
    if (localStorage['menuId']) {
      menuId = localStorage.getItem('menuId');
    }
      const param = {
        'tId': this.loginUserdata.data.data.tenantId,
        'menuId': menuId,
        'noteventId': 40,
        // 'noteventId': 43,
      };
      const _urlGetNotificationTags: string = webApi.domain + webApi.url.getNotificationTags;
      this.commonFunctionService.httpPostRequest(_urlGetNotificationTags,param)
      // this.newsService.getNotificationsTags(param)
      .then(
        (rescompData) => {
          if (rescompData && rescompData['data'])
          {
            let dataResponse = [];
            dataResponse = rescompData['data'];
            if(dataResponse[0].length !== 0){
              this.tagsArray = dataResponse[0];
              if (this.tagsArray.length !== 0) {
                setTimeout(() => { this.init = true; }, 450);
              }
              this.cdf.detectChanges();
            }
          }
          this.spinner.hide();
        },
        error => {
          this.spinner.hide();
          // const toast: Toast = {
          //   type: 'error',
          //   // title: 'Server Error!',
          //   body: 'Something went wrong.please try again later.',
          //   showCloseButton: true,
          //   timeout: 4000,
          // };
          // this.toastr.pop(toast);
          this.presentToast('error', '');
        },
      );
  }
  openNav() {
    document.getElementById('mySidenav').style.width = '170px';
  }
  closeNav() {
    document.getElementById('mySidenav').style.width = '0';
  }

  appNotification = {
    notificationSubjectMin : null,
    notificationSubjectMax: null,
    appNotificationDescritionMin: null,
  };
  smsNotification = {
    min : null,
    max : null,
  };
  emailNotification = {
    emailSubjectMin: null,
    emailSubjectMax: null,
    // emailBodyMin:
  };
  setUnsetValidation(notificationName, setUnsetFlag){
    if (notificationName === 'app' ) {
      if (setUnsetFlag){
      this.appNotification = {
        notificationSubjectMin : 2,
        notificationSubjectMax: 200,
        appNotificationDescritionMin: 2,
      };
    //   Object.keys(this.formval.controls).forEach(control => {
    //     if(control !== 'newsName'){
    //       this.formval.controls[control].markAsPristine();
    //     }
    // });

    }else {
        this.appNotification = {
          notificationSubjectMin : null,
          notificationSubjectMax: null,
          appNotificationDescritionMin: null,
        };
      }
    }
    if (notificationName === 'sms' ) {
      if (setUnsetFlag) {
      this.smsNotification = {
        min : 2,
        max : 144,
      };
    //   Object.keys(this.formval.controls).forEach(control => {
    //     if(control !== 'newsName'){
    //       this.formval.controls[control].markAsPristine();
    //     }
    // });
    }else {
      this.smsNotification = {
        min : null,
        max : null,
      };
      }
    }
    if (notificationName === 'email' ){
      if (setUnsetFlag) {
      this.emailNotification = {
        emailSubjectMin: 2,
        emailSubjectMax: 1000,
        // emailBodyMin:
      };
    //   Object.keys(this.formval.controls).forEach(control => {
    //     if(control !== 'newsName'){
    //       this.formval.controls[control].markAsPristine();
    //     }
    // });
    }else {
      this.emailNotification = {
        emailSubjectMin: null,
        emailSubjectMax: null,
        // emailBodyMin:
      };
      }
    }
  }
}
