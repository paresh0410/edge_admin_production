import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnrollNewsComponent } from './enrollNews.component';

describe('EnrollNewsComponent', () => {
  let component: EnrollNewsComponent;
  let fixture: ComponentFixture<EnrollNewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnrollNewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnrollNewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
