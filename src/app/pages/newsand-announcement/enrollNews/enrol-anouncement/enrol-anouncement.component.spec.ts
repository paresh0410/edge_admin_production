import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnrolAnouncementComponent } from './enrol-anouncement.component';

describe('EnrolAnouncementComponent', () => {
  let component: EnrolAnouncementComponent;
  let fixture: ComponentFixture<EnrolAnouncementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnrolAnouncementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnrolAnouncementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
