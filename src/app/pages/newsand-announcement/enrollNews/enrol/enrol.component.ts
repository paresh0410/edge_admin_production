import { Component, OnInit, ViewChild, ChangeDetectorRef, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { NewsServices } from '../../newsand-annoucement.services';
import { EnrolanounccmnetService } from '../enrol-anouncement/enrol-anouncement.service';
import { AddEditCourseContentService } from '../../../plan/courses/addEditCourseContent/addEditCourseContent.service';
import { DatePipe } from '@angular/common';
// import { ToasterService, Toast } from 'angular2-toaster';
import { ToastrService } from 'ngx-toastr';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { CommonFunctionsService } from '../../../../service/common-functions.service';
import { webApi } from '../../../../service/webApi';
import { noData } from '../../../../models/no-data.model';
@Component({
  selector: 'ngx-enrol',
  templateUrl: './enrol.component.html',
  styleUrls: ['./enrol.component.scss']
})
export class EnrolComponent implements OnInit {
  @ViewChild('myTable') table: any;
  @ViewChild(DatatableComponent) tableData: DatatableComponent;
  @Input() data;
  errorMsg: any;
  // rows = [];
  selected = [];
  rows: any = [];
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No enrollment available at this time.",
    desc:"",
    titleShow:true,
    btnShow:true,
    descShow:false,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/news-enrol',
  }
  labels: any = [
		{ labelname: 'EMP CODE', bindingProperty: 'ecn', componentType: 'text' },
		{ labelname: 'FULL NAME', bindingProperty: 'fullname', componentType: 'text' },
		{ labelname: 'EMAIL', bindingProperty: 'emailId', componentType: 'text' },
		{ labelname: 'MOBILE', bindingProperty: 'phoneNo', componentType: 'text' },
		{ labelname: 'D.0.E', bindingProperty: 'enrolDate', componentType: 'text' },
		{ labelname: 'MODE', bindingProperty: 'enrolmode', componentType: 'text' },
		{ labelname: 'ACTION', bindingProperty: 'btntext', componentType: 'button' },
		// { labelname: 'Action', bindingProperty: 'action', componentType: 'select' },
		// { labelname: 'Action', bindingProperty: 'action', componentType: 'checkbox' },

	  ];
	  // rows: any = [
		// { 'empcode': 'ecn', 'ullname': 'fullname', 'email':'emailId','number':'phoneNo','date':'enrolDate','mode':'enrolmode','button' :'btntext' },
	  // ];
  team: any = [
    {
      "id": 1,
      "Name": "John Cena",
      "Subject": "English",
      "Date": "16 April, 2019",
      "Employees": "10/100",
      "Type": [
        {
          "type": "mobile",
          "ClassName": "fas fa-mobile subIcon",
        },
        {
          "type": "email",
          "ClassName": "fas fa-envelope subIcon",
        },
        {
          "type": "sms",
          "ClassName": "fas fa-comment-alt subIcon",
        },
      ],
    },
    {
      "id": 1,
      "Name": "John Cena",
      "Subject": "English",
      "Date": "16 April, 2019",
      "Employees": "10/100",
      "Type": [
        {
          "type": "mobile",
          "ClassName": "fas fa-mobile subIcon",
        },
        {
          "type": "email",
          "ClassName": "fas fa-envelope subIcon",
        },
        {
          "type": "sms",
          "ClassName": "fas fa-comment-alt subIcon",
        },
      ],
    },
    {
      "id": 1,
      "Name": "John Cena",
      "Subject": "English",
      "Date": "16 April, 2019",
      "Employees": "10/100",
      "Type": [
        {
          "type": "mobile",
          "ClassName": "fas fa-mobile subIcon",
        },
        {
          "type": "email",
          "ClassName": "fas fa-envelope subIcon",
        },
        {
          "type": "sms",
          "ClassName": "fas fa-comment-alt subIcon",
        },
      ],
    },
    {
      "id": 1,
      "Name": "John Cena",
      "Subject": "English",
      "Date": "16 April, 2019",
      "Employees": "10/100",
      "Type": [
        {
          "type": "mobile",
          "ClassName": "fas fa-mobile subIcon",
        },
        {
          "type": "email",
          "ClassName": "fas fa-envelope subIcon",
        },
        {
          "type": "sms",
          "ClassName": "fas fa-comment-alt subIcon",
        },
      ],
    },
    {
      "id": 1,
      "Name": "John Cena",
      "Subject": "English",
      "Date": "16 April, 2019",
      "Employees": "10/100",
      "Type": [
        {
          "type": "mobile",
          "ClassName": "fas fa-mobile subIcon",
        },
        {
          "type": "email",
          "ClassName": "fas fa-envelope subIcon",
        },
        {
          "type": "sms",
          "ClassName": "fas fa-comment-alt subIcon",
        },
      ],
    },
    {
      "id": 1,
      "Name": "John Cena",
      "Subject": "English",
      "Date": "16 April, 2019",
      "Employees": "10/100",
      "Type": [
        {
          "type": "mobile",
          "ClassName": "fas fa-mobile subIcon",
        },
        {
          "type": "email",
          "ClassName": "fas fa-envelope subIcon",
        },
        {
          "type": "sms",
          "ClassName": "fas fa-comment-alt subIcon",
        },
      ],
    },


  ];
  enableCourse: boolean = false;
  enrolldata: any = [];
  serviceData: any;
  userLoginData: any = [];
  userdata: any = [];
  resultdata: any = [];
  getdata: any;
  temp: any;
  searchvalue: any = {
    value: '',
    value1: ''
  };

  constructor(public router: Router, public routes: ActivatedRoute, private spinner: NgxSpinnerService,
    public newsandanouncmnetservice: NewsServices, public addEditCourseService: AddEditCourseContentService,
    private datePipe: DatePipe, public cdf: ChangeDetectorRef,
    // public toasterService: ToasterService,
    private commonFunctionService: CommonFunctionsService,
    private toastr: ToastrService) {
    this.getdata = this.data;
    if (this.getdata) {
      this.allenroluser();
    }
    // this.serviceData = this.newsandanouncmnetservice.newsid;

    this.serviceData = this.newsandanouncmnetservice.addEditNewData;
    console.log("serviceData", this.serviceData);

    // this.rows = this.team;
  }
  ngOnInit() {
    if (localStorage.getItem('LoginResData')) {
      this.userLoginData = JSON.parse(localStorage.getItem('LoginResData'));
      this.userdata = this.userLoginData.data.data;
      console.log("login data", this.userdata);
    }
    if (this.serviceData[1]) {
      this.allenroluser();
    }
  }
  allenroluser() {
    this.spinner.show();
    const data = {
      areaId: 30,
      instanceId: this.serviceData[1].id,
      tId: this.userdata.tenantId,
      mode: 0,
    };
    console.log(data);
    const llErnroledusers:string = webApi.domain + webApi.url.getAllEnrolUser;
    this.commonFunctionService.httpPostRequest(llErnroledusers,data)
    // this.addEditCourseService.getallenroluser(data)
    .then(enrolData => {
      this.spinner.hide();
      this.enrolldata = enrolData['data'];
      this.rows = enrolData['data'];
      this.rows = [...this.rows];
      for (let i = 0; i < this.rows.length; i++) {
        // this.rows[i].Date = new Date(this.rows[i].enrolDate);
        // this.rows[i].enrolDate = this.formdate(this.rows[i].Date);
        if(this.rows[i].visible == 1) {
					this.rows[i].btntext = 'fa fa-eye';
				  } else {
					this.rows[i].btntext = 'fa fa-eye-slash';
				  }
      }
      console.log("EnrolledUSer", this.rows);
      if (this.enrolldata.visible = 1) {
        this.enableCourse = false;
      } else {
        this.enableCourse = true;
      }
      this.cdf.detectChanges();
    });

  }

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }

  visibilityTableRow(row) {
		let value;
		let status;

		if (row.visible == 1) {
		  row.btntext = 'fa fa-eye-slash';
		  value  = 'fa fa-eye-slash';
		  row.visible = 0
		  status = 0;
		} else {
		  status = 1;
		  value  = 'fa fa-eye';
		  row.visible = 1;
		  row.btntext = 'fa fa-eye';
		}

		for(let i =0; i < this.rows.length; i++) {
		  if(this.rows[i].employeeId == row.employeeId) {
			this.rows[i].btntext = row.btntext;
			this.rows[i].visible = row.visible
		  }
		}
    var visibilityData = {
      employeeId: row.employeeId,
      visible: status,
      courseId: this.serviceData[1].id,
      tId: this.userdata.tenantId,
      aId: 30,
    }
    const _urlDisableEnrol: string = webApi.domain + webApi.url.disableEnrol;
    this.commonFunctionService.httpPostRequest(_urlDisableEnrol,visibilityData)
    .then(result => {
      console.log(result);
      this.spinner.hide();
      this.resultdata = result;
      if (this.resultdata.type == false) {

        this.presentToast('error', '');
      } else {
        console.log("after", row.visible)
        this.allenroluser();
        this.presentToast('success', this.resultdata.data);
      }
    },
      resUserError => {
        this.errorMsg = resUserError;
        this.spinner.hide();
      });
		console.log('row', row);
	  }
  disableCourseVisibility(currentIndex, row, status) {
    this.spinner.show();
    var visibilityData = {
      employeeId: row.employeeId,
      visible: status,
      courseId: this.serviceData[1].id,
      tId: this.userdata.tenantId,
      aId: 30,
    }
    const _urlDisableEnrol: string = webApi.domain + webApi.url.disableEnrol;
    this.commonFunctionService.httpPostRequest(_urlDisableEnrol,visibilityData)
    // this.addEditCourseService.disableEnrol(visibilityData)
    .then(result => {
      console.log(result);
      this.spinner.hide();
      // this.loader = false;
      this.resultdata = result;
      if (this.resultdata.type == false) {
        // var courseUpdate: Toast = {
        //   type: 'error',
        //   title: "Course",
        //   body: "Unable to update visibility of User.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.closeEnableDisableCourseModal();
        // this.toasterService.pop(courseUpdate);
        this.presentToast('error', '');
      } else {
        // var courseUpdate: Toast = {
        //   type: 'success',
        //   title: "Course",
        //   body: this.resultdata.data,
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // row.visible = !row.visible;
        console.log("after", row.visible)
        this.allenroluser();
        // this.toasterService.pop(courseUpdate);
        this.presentToast('success', this.resultdata.data);
      }
    },
      resUserError => {
        // this.loader = false;
        this.errorMsg = resUserError;
        this.spinner.hide();
        // this.closeEnableDisableCourseModal();
      });
  }

  formdate(date) {

    if (date) {
      const formatted = this.datePipe.transform(date, 'dd-MM-yyyy');
      return formatted;
    }
  }
  clearesearch(){
    if(this.searchvalue.value){
    this.searchvalue = {};
    this.allenroluser();
  }
  }
  searchenrol(event) {
    const val = event.target.value.toLowerCase();
    // this.allEnrolUser(this.addEditCourseService.data.data);
    // this.cdf.detectChanges();
    this.temp = [...this.enrolldata]
    if(val.length>=3 || val.length == 0){
    const temp = this.temp.filter(function (d) {
      return String(d.ecn).toLowerCase().indexOf(val) !== -1 ||
        d.fullname.toLowerCase().indexOf(val) !== -1 ||
        d.emailId.toLowerCase().indexOf(val) !== -1 ||
        d.enrolmode.toLowerCase().indexOf(val) !== -1 ||
        d.enrolDate.toLowerCase().indexOf(val) !== -1 ||
        d.phoneNo.toLowerCase().indexOf(val) !== -1 ||
        String(d.enrolmode).toLowerCase().indexOf(val) !== -1 ||
        !val
    });

    // update the rows
    this.rows = [...temp];
    // Whenever the filter changes, always go back to the first page
    this.tableData.offset = 0;
  }
  }
}
