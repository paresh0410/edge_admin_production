import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsandAnnoucementIntermediateComponent } from './newsand-annoucement-intermediate.component';

describe('NewsandAnnoucementIntermediateComponent', () => {
  let component: NewsandAnnoucementIntermediateComponent;
  let fixture: ComponentFixture<NewsandAnnoucementIntermediateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsandAnnoucementIntermediateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsandAnnoucementIntermediateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
