import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsandAnnouncementComponent } from './newsand-announcement.component';

describe('NewsandAnnouncementComponent', () => {
  let component: NewsandAnnouncementComponent;
  let fixture: ComponentFixture<NewsandAnnouncementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsandAnnouncementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsandAnnouncementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
