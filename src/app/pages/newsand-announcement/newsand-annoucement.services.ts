import { Injectable, Inject } from '@angular/core';
import {Http, Response, Headers, RequestOptions, Request} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AppConfig } from '../../app.module';
import { webApi } from '../../service/webApi';
import { HttpClient } from "@angular/common/http";
import { AuthenticationService } from '../../service/authentication.service';

@Injectable()
export class NewsServices {

  addEditNewData: any = [];
  private _urlNewsAddEdit: string = webApi.domain + webApi.url.add_edit_news_announcement;
  private _urlGetAllAnn: string = webApi.domain + webApi.url.get_all_announcement;
  private _urlGetAllNotify: string = webApi.domain + webApi.url.get_all_notify_by_id;
  private _urlrempbulk:string= webApi.domain + webApi.url.tempSaveBulkManEnrol;
  private _urlfinalbulk:string= webApi.domain + webApi.url.saveBulkManEnrol;
  private _urlGetNotificationTags: string = webApi.domain + webApi.url.getNotificationTags;
  private _urlAreaBulEnrol: string = webApi.domain + webApi.url.area_bulk_enrol;
  constructor(@Inject('APP_CONFIG_TOKEN') private config: AppConfig, private _http: Http, private _httpClient: HttpClient,
  private authenticationService: AuthenticationService) {
    //this.busy = this._http.get('...').toPromise();
  }

  AddEditNews(param) {
    return new Promise(resolve => {
      this._httpClient.post(this._urlNewsAddEdit, param)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  getAllNotify(param) {
    return new Promise(resolve => {
      this._httpClient.post(this._urlGetAllNotify, param)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  getAllAnn(param) {
    return new Promise(resolve => {
      this._httpClient.post(this._urlGetAllAnn, param)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }

  TempManEnrolBulk(item){
    // return this._http.post(this._urlCheckCourseCode,courseData)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);
    // add authorization header with jwt token
        let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
        // let headers = new Headers({ 'Authorization': 'Bearer ' + 'myToken' });
        let options = new RequestOptions({ headers: headers });
    return new Promise(resolve => {
      this._http.post(this._urlrempbulk, item, options)
    .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve('err');
            });
      });

  }

  finalManEnrolBulk(item){
    // return this._http.post(this._urlCheckCourseCode,courseData)
    //   .map((response:Response) => response.json())
    //   .catch(this._errorHandler);
       let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
        // let headers = new Headers({ 'Authorization': 'Bearer ' + 'myToken' });
        let options = new RequestOptions({ headers: headers });
    return new Promise(resolve => {
      this._http.post(this._urlfinalbulk, item, options)
    .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve('err');
            });
      });

  }
  // new code end //


  _errorHandler(error: Response) {
    console.error(error);
    return Observable.throw(error || 'Server Error');
  }
  getNotificationsTags(param) {
    // let url:any = this._urlGetCourseNotification;
    // return this._http.post(url,param)
    //     .map((response:Response)=>response.json())
    //     .catch(this._errorHandler);

    return new Promise(resolve => {
       this._httpClient.post(this._urlGetNotificationTags, param)
       //.map(res => res.json())
       .subscribe(data => {
           resolve(data);
       },
       err => {
           resolve('err');
       });
   });
  }

  areaBulkEnrol(param) {
    return new Promise(resolve => {
        this._httpClient.post(this._urlAreaBulEnrol, param)
            //.map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
                err => {
                    resolve('err');
                });
    });
}
}
