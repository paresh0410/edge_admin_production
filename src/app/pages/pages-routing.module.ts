// import { ReportsComponent } from './reports/reports.component';
// import { IntermediateReport } from './reports/intermediatereports/intermediatereports';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { Login } from './login/login.component';

// import { Evaluate } from './evaluate/evaluate.component';
// import { EvaluateReporting } from './evaluate/evaluate-reporting/evaluateReporting';

import { Intermediate } from './plan/intermediate/intermediate';
import { Category } from './plan/courses/category/category.component';
import { Content } from './plan/courses/content/content';

// import { Addeditccategory } from './plan/courses/category/addEditCategory/addEditCategory.component';
import { AddEditCategoryContent } from './plan/courses/addEditCategoryContent/addEditCategoryContent.component';
// import { Addeditcontent } from './plan/courses/content/addEditContent/addEditContent';

import { AddEditCourseContent } from './plan/courses/addEditCourseContent/addEditCourseContent';
// import { AddEditBlendCourseContent } from './plan/blended-courses/addEditCourseContent/addEditCourseContent';
// import { Addeditcontent } from './plan/courses/addEditContent/addEditContent';
// preon
import { PreonCategory } from './plan/preoncourses/Preoncategory/Preoncategory.component';
import { PreonContent } from './plan/preoncourses/preoncontent/preoncontent';
import { AddEditPreonCategoryContent } from './plan/preoncourses/addEditPreonCategoryContent/addEditPreonCategoryContent.component';
import { AddEditPreonCourseContent } from './plan/preoncourses/addEditPreonCourseContent/addEditPreonCourseContent';
import { CourseBundle } from './plan/courseBundle/courseBundle';
import { Users } from './plan/users/users.component';
import { AddeditusersComponent } from './plan/users/addeditusers/addeditusers.component';
// import { UploadusersComponent } from './plan/users/uploadusers/uploadusers.component'
import { UploadusersComponent } from './plan/users/uploadusers/uploadusers.component';
import { Employees } from './plan/employees/employees.component';
import { AddeditemployeesComponent } from './plan/employees/addeditemployees/addeditemployees.component';
import { UploademployeesComponent } from './plan/employees/uploademployees/uploademployees.component'

import { Courses } from './plan/courses/courses.component';
import { pCourses } from './plan/preoncourses/courses.component';
import { PlanComponent } from './plan/plan.component';
import { LearningComponentMain } from './learning/learning.component';
import { roleManagement } from './plan/roleManagement/roleManagement.component';
import { addeditrole } from './plan/roleManagement/addeditrole/addeditrole.component';
// import { IntermediateEnroll } from './enroll/intermediateEnroll/intermediateEnroll';

// import { Category } from './enroll/courses/category/category.component';
// import { Content } from './enroll/courses/content/content';
// import { Addeditccategory } from './enroll/courses/category/addEditCategory/addEditCategory.component';
// import { Courses } from './enroll/courses/courses.component';
// import { Addeditcontent } from './enroll/courses/content/addEditContent/addEditContent';

// import { Users } from './enroll/users/users.component';
// import { AddeditusersComponent } from './enroll/users/addeditusers/addeditusers.component';
// import { UploadusersComponent } from './enroll/users/uploadusers/uploadusers.component';

// import { ManualEnroll } from './enroll/manualEnroll/manualEnroll';
// import { Rules } from './enroll/rules/rules';

// import { EnrollComponent } from './enroll/enroll.component';
// import { SelfComponent } from './enroll/self/self.component';
// import { RegulatoryComponent } from './enroll/regulatory/regulatory.component';

// import { ReportingComponent } from './evaluate/reporting/reporting.component'
// import { LearningComponent } from './evaluate/reporting/learning/learning.component';
// import { MarketingComponent } from './evaluate/reporting/marketing/marketing.component';
// import { HuddleComponent } from './evaluate/reporting/huddle/huddle.component';
// import { PivotComponent } from './evaluate/reporting/learning/pivot/pivot.component';
// import { EmployeeInductionReportComponent } from './evaluate/reporting/learning/employee-induction-report/employee-induction-report.component';
// import { EmployeeLoginReportComponent } from './evaluate/reporting/learning/employee-login-report/employee-login-report.component';
// import { UserActivityReportComponent } from './evaluate/reporting/learning/user-activity-report/user-activity-report.component';
// import { CourseCompletionInitialReportDateComponent } from './evaluate/reporting/learning/course-completion-report-date/course-completion-initial-report-date.component';
// import { CourseCompletionReportDateComponent } from './evaluate/reporting/learning/course-completion-report-date/course-completion-report-date.component';
// import { QuizCompletionReportComponent } from './evaluate/reporting/learning/quiz-completion-report/quiz-completion-report.component';
// import { CourseModuleCompletionReportComponent } from './evaluate/reporting/learning/course-module-completion-report/course-module-completion-report.component';
// import { QuizCompletionInitialReportComponent } from './evaluate/reporting/learning/quiz-completion-report/quiz-completion-initial-report.component';
// import { FeedbackReportComponent } from './evaluate/reporting/learning/feedback-report/feedback-report.component';
// import { FeedbackInitialReportComponent } from './evaluate/reporting/learning/feedback-report/feedback-initial-report.component';
// import { BuddyCallingReportComponent } from './evaluate/reporting/learning/buddy-calling-report/buddy-calling-report.component';
// import { CourseCompletionInitialReportPercentageComponent } from './evaluate/reporting/learning/course-completion-report-percentage/course-completion-initial-report-percentage.component';
// import { CourseCompletionReportPercentageComponent } from './evaluate/reporting/learning/course-completion-report-percentage/course-completion-report-percentage.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';
// import { slikgridDemo } from './slikgridDemo/slikgridDemo.component';
// import { CourseModuleCompletionInitialReportComponent } from './evaluate/reporting/learning/course-module-completion-report/course-module-completion-initial-report.component';
//import { LearningdataComponent } from './evaluate/learning/learningdata/learningdata.component';
import { GamificationComponent } from './gamification/gamification.component';
import { Badges } from './gamification/badges/badges.component';
import { intermediategame } from './gamification/intermediategame/intermediategame';
import { CertificateComponent } from './gamification/certificate/certificate.component';
import { AddEditCertificateComponent } from './gamification/certificate/add-edit-certificate/add-edit-certificate.component';
import { ladders } from './gamification/ladders/ladders.component';
import { socialSharing } from './gamification/socialSharing/socialSharing.component';
import { addeditbadges } from './gamification/badges/addeditbadges/addeditbadges.component';
//import { addeditcertificates } from './gamification/certificates/addeditcertificates/addeditcertificates.component';
import { addeditladders } from './gamification/ladders/addeditladders/addeditladders.component';
import { addedisocialSharing } from './gamification/socialSharing/addedisocialSharing/addedisocialSharing.component';

import { assessmentComponent } from './assessment/assessment.component';
import { qCategory } from './assessment/qcategory/qcategory.component';
import { UploadQuestionComponent } from '../pages/assessment/qcategory/upload-question/upload-question.component';
import { qAddeditccategory } from './assessment/qcategory/qaddEditCategory/qaddEditCategory.component';
import { questions } from './assessment/qcategory/questions/questions.component';
import { addEditquestion } from './assessment/qcategory/addEditquestion/addEditquestion.component';
import { intermediateasses } from './assessment/intermediateasses/intermediateasses';
import { quiz } from './assessment/quiz/quiz.component';
import { Addeditquiz } from './assessment/quiz/addEditquiz/addEditquiz.component';
import { feedback } from './assessment/feedback/feedback.component';
import { Addeditfeedback } from './assessment/feedback/addEditfeedback/addEditfeedback.component';

import { survey } from './assessment/survey/survey.component';
import { Addeditsurvey } from './assessment/survey/addEditsurvey/addEditsurvey.component';

import { pole } from './assessment/pole/pole.component';
import { Addeditpole } from './assessment/pole/addEditpole/addEditpole.component';

// import { CourseSummeryReportComponent } from './evaluate/reporting/learning/course-summery-report/course-summery-report.component';
// import { CourseSummeryInitialReportComponent } from './evaluate/reporting/learning/course-summery-report/course-summery-initial-report.component';

import { NotificationTemplatesComponent } from './plan/notification-templates/notification-templates.component';
import { AddEditNotificationTemplateComponent } from './plan/notification-templates/add-edit-notification-template/add-edit-notification-template.component';

import { BadgeCategoryComponent } from './gamification/badge-category/badge-category.component'

// import { LearningReports } from './evaluate1/evaluate1Basic/learningReports/learningReports.component';
/******************************Coaching Call**************************** */
// import { CoachingComponent } from './coaching/coaching.component';
// import { TeamComponent } from './coaching/team/team.component';
// import { CallMappingComponent } from './coaching/call-mapping/call-mapping.component';
// import { IntermediateCoach } from './coaching/intermediatecoach/intermediatecoach';
// import { from } from 'rxjs';

// import { AddEditTeamComponent } from './coaching/team/add-edit-team/add-edit-team.component';
// import { ParticipantsComponent } from './coaching/participants/participants.component';
// import { CalendarComponent } from './coaching/calendar/calendar.component';
// import { CallDetailComponent } from './coaching/call-detail/call-detail.component';

// import { PartnerComponent } from './coaching/partner/partner.component';
// import { AddPartnerComponent } from './coaching/partner/add-partner/add-partner.component';
// import { BulkUploadCoachingComponent } from './coaching/participants/bulk-upload-coaching/bulk-upload-coaching.component';
// import { AttributeSkillComponent } from './coaching/attribute-skill/attribute-skill.component';
// import { AddAttributeSkillComponent } from './coaching/attribute-skill/add-attribute-skill/add-attribute-skill.component';
// import { AttributeMasterComponent } from './coaching/attribute-master/attribute-master.component';
// import { CallsComponent } from './coaching/calls/calls.component';
// import { AddParticipantsComponent } from './coaching/participants/add-participants/add-participants.component';
// import { AddAttributeMasterComponent } from './coaching/attribute-master/add-attribute-master/add-attribute-master.component';
// import { CourseDetailComponent } from './coaching/call-detail/content/course-detail/course-detail.component'
// import { MasterComponent } from './coaching/master/master.component';
// import { FeedackTemplateComponent } from './coaching/feedack-template/feedack-template.component';
// import { AssessmentTemplateComponent } from './coaching/assessment-template/assessment-template.component';
// import { ObservationTemplateComponent } from './coaching/observation-template/observation-template.component';

/**************************************Call Coaching End*****************************************/
import { BlendedCoursesComponent } from './plan/blended-courses/blended-courses.component';
import { AddEditBlendCourseContent } from './plan/blended-courses/addEditCourseContent/addEditCourseContent';
import { BlendedHomeComponent } from './plan/blended-courses/blended-home/blended-home.component';
import { BlendedCategoryComponent } from './plan/blended-courses/blended-category/blended-category.component';
import { BlendedProgramComponent } from './plan/blended-courses/blended-program/blended-program.component';
import { AddEditCategoryBlended } from './plan/blended-courses/blended-category/addEditCategoryBlended';
import { AddEditProgramBlended } from './plan/blended-courses/blended-program/addEditProgramBlended/addEditProgramBlended.component';
import { TrainerComponent } from './plan/trainer/trainer.component';
import { AddTrainerComponent } from './plan/trainer/add-trainer/add-trainer.component';
import { NominationInstanceComponent } from './plan/blended-courses/nomination-instance/nomination-instance.component';
import { AddEditNominationComponent } from './plan/blended-courses/nomination-instance/add-edit-nomination/add-edit-nomination.component';



/************************************************Task Manager**********************************************/

// import { TaskManagerComponent } from './task-manager/task-manager.component';
import { ReactionsComponent } from './reactions/reactions.component';

/************************************************Task Manager**********************************************/


// /*********************************DAM************************************ */
// import { DamComponent } from './dam/dam.component';
// import { AddAssetComponent } from './dam/asset/add-asset/add-asset.component';
// import { ShareAssetComponent } from './dam/share-asset/share-asset.component';
// import { ApproveAssetComponent } from './dam/approve-asset/approve-asset.component';
// import { AssetReviewPolicyComponent } from './dam/asset-review-policy/asset-review-policy.component';
// import { IntermediateDamComponent } from './dam/intermediate-dam/intermediate-dam.component';
// import { AssetComponent } from './dam/asset/asset.component';
// import { AssetVersionComponent } from './dam/asset-version/asset-version.component';
// // import { AssetCategoryComponent } from './dam/asset-category/asset-category.component';
// // import { AddEditAssetCategoryComponent} from './dam/asset-category/add-edit-asset-category/add-edit-asset-category.component'
// //import { AssetComponent } from './dam/asset/asset.component'
// import { AssetCategoriesComponent } from './dam/asset-categories/asset-categories.component';
// import { BulkUploadComponent } from './dam/bulk-upload/bulk-upload.component';
// import { NewAssetComponent } from './dam/new-asset/new-asset.component';
// import { FolderFileComponent } from './dam/folder-file/folder-file.component';
// import { FolderUploadComponent } from './dam/folder-upload/folder-upload.component';

//////Content Upload//////
import { ContentUploadComponent } from './content-upload/content-upload.component';
////////////////////////



/*********************************News************************************ */
// import { NewsandAnnouncementComponent } from './newsand-announcement/newsand-announcement.component';
// import { NewsandAnnoucementIntermediateComponent } from './newsand-announcement/newsand-annoucement-intermediate/newsand-annoucement-intermediate.component';
// import { EnrollNewsComponent } from './newsand-announcement/enrollNews/enrollNews.component';
// import { BasicDetailsComponent } from './newsand-announcement/enrollNews/basic-details/basic-details.component';
// import { EnrolComponent } from './newsand-announcement/enrollNews/enrol/enrol.component';

/*********************************Report************************************ */
// import { CourseConsumptionReportComponent } from './reports/course-consumption-report/course-consumption-report.component';
// import { ActivityConsumptionReportComponent } from './reports/activity-consumption-report/activity-consumption-report.component';
// import { ReportViewerComponent } from './reports/report-viewer/report-viewer.component';
// import { ReactionHomeComponent } from './reports/reaction-home/reaction-home.component';
// import { SurveyReportComponent } from './reports/reaction-home/survey-report/survey-report.component';
// import { PollReportComponent } from './reports/reaction-home/poll-report/poll-report.component';
// import { WorkflowReportComponent } from './reports/workflow-report/workflow-report.component';
// import { ReportsHomeComponent } from './reports/reports-home/reports-home.component';
// import { TttReportsComponent } from './reports/ttt-reports/ttt-reports.component';
// import { TaReportsComponent } from './reports/ta-reports/ta-reports.component';
// import { TaEmpReportsComponent } from './reports/ta-emp-reports/ta-emp-reports.component';
// import { PreOnboardingReportComponent } from './reports/pre-onboarding-report/pre-onboarding-report.component';
// import { QuizConsumptionReportComponent } from './reports/quiz-consumption-report/quiz-consumption-report.component';
// import { FeedbackConsumptionReportComponent } from './reports/feedback-consumption-report/feedback-consumption-report.component';
// import { EmployeeAttendanceComponent } from './reports/employee-attendance/employee-attendance.component';
// import { ClassroomReportComponent } from './reports/classroom-report/classroom-report.component';
// import { EepReportComponent } from './reports/eep-reports/eep-report/eep-report.component';
// import { EepReportsComponent } from './reports/eep-reports/eep-reports.component';
// import { EmployeeConsumptionComponent } from './reports/employee-consumption/employee-consumption.component';

// import { ReportScheduleComponent } from './reports/report-schedule/report-schedule.component';
// import { ReportListComponent } from './reports/report-list/report-list.component';

/*EEP */
import { EepWorkflowComponent } from './plan/blended-courses/eep-workflow/eep-workflow.component';
import { AddEditEepWorkflowComponent } from './plan/blended-courses/eep-workflow/add-edit-eep-workflow/add-edit-eep-workflow.component';



// import { IntermediateToolsComponent } from './tools/intermediate-tools/intermediate-tools.component';
// import { AddEditContentComponent } from './tools/add-edit-content/add-edit-content.component';
// import { ContentDetailsComponent } from './tools/add-edit-content/content-details/content-details.component';
// import { enrolmentComponent } from './plan/courses/addEditCourseContent/enrolment/enrolment.component';
// import { EneroltoolsComponent } from './tools/add-edit-content/eneroltools/eneroltools.component';



import { UnEnrolEmployessComponent } from './plan/un-enrol-employess/un-enrol-employess.component';

// TA Dashbords

// import { TrainerDashboardComponent } from './trainer/trainer-dashboard/trainer-dashboard.component';
// import { Level1Component } from './trainer/level1/level1.component';
// import { Level2Component } from './trainer/level2/level2.component';
// import { TrainerDetailsComponent } from './trainer/trainer-details/trainer-details.component';
// import { AttendanceDataComponent } from './trainer/attendance-data/attendance-data.component';
// import { FeedbackDataComponent } from './trainer/feedback-data/feedback-data.component';
// import { AssessmentDataComponent } from './trainer/assessment-data/assessment-data.component';

import { LocationComponent } from './plan/location/location.component';
import { VenueComponent } from './plan/venue/venue.component';
import { AddVenueComponent } from './plan/venue/add-venue/add-venue.component';
import { PartnerListComponent } from './plan/partner-list/partner-list.component';



import { TagsComponent } from './plan/tags/tags.component';
import { OpenApiComponent } from './plan/open-api/open-api.component';
import { InstitutesComponent } from './plan/institutes/institutes.component';
import { GbhMappingComponent } from './plan/gbh-mapping/gbh-mapping.component';



import { ThemeChangeComponent } from './theme-change/theme-change.component';
//import { AddapiComponent } from './plan/open-api/addapi/addapi.component';
// import { ToolsComponent } from './tools/tools.component';

import { RecordRtcComponent } from './record-rtc/record-rtc.component';




import { DiscountComponent } from './plan/discount/discount.component';
import { PricingComponent } from './plan/pricing/pricing.component';


import { PreonboardingComponent } from './preonboarding/preonboarding.component';
import { BulkBatchComponent } from './plan/blended-courses/bulk-batch/bulk-batch.component';
// import { BatchUploadComponent } from './plan/blended-courses/batch-upload/batch-upload.component';

// import { TrainerDashboardModule } from './trainer/trainer-dashboard.module';

const routes: Routes = [
  // {
  //   path: 'login',
  //   //loadChildren: 'app/pages/login/login.module#LoginModule',
  //   component:Login
  // },
  {
    path: '',
    component: PagesComponent,
    //loadChildren: 'app/pages/pages.module#PagesModule',
    children: [{
      path: 'dashboard',
      component: DashboardComponent,
    },
    {
      path: 'record-rtc',
      component: RecordRtcComponent,
    },
    {
      path: 'content-upload',
      component: ContentUploadComponent,
    },
    // {
    //   path: 'users',
    //   component: Users,
    // },
    {
      path: 'learning',
      component: LearningComponentMain,
    },
    {
      path: 'plan',
      component: PlanComponent,
    },
    { path: 'blended-home',
      component: BlendedHomeComponent ,
    },
    { path: 'nomination-instance',
     component: NominationInstanceComponent ,
    },
    { path: 'eep-workflow',
      component: EepWorkflowComponent,
    },
    { path: 'preonboarding',
      component: PreonboardingComponent,
    },
    {
      path: '',
      component: Intermediate,
      //redirectTo: 'dashboard',
      children: [
        { path: 'plan/discount', component: DiscountComponent },
        { path: 'plan/pricing', component: PricingComponent },
        { path: 'plan/theme', component: ThemeChangeComponent },
        { path: 'plan/users', component: Users },
        { path: 'plan/users/add-edit-users', component: AddeditusersComponent },
        { path: 'plan/users/upload-users', component: UploadusersComponent },
        { path: 'plan/employees', component: Employees },
        { path: 'plan/employees/add-edit-employees', component: AddeditemployeesComponent },
        { path: 'plan/employees/upload-employees', component: UploademployeesComponent },
        { path: 'plan/enrol-unenrol/:menuId', component: UnEnrolEmployessComponent },
        { path: 'plan/location', component: LocationComponent },
        { path: 'plan/tags', component: TagsComponent },
        { path: 'plan/partnerList', component: PartnerListComponent },
        { path: 'plan/venue', component: VenueComponent },
        { path: 'plan/venue/addVenue', component: AddVenueComponent },
        { path: 'plan/courses', component: Courses },
        { path: 'plan/courses/category', component: Category },
        { path: 'plan/courses/addEditCategoryContent', component: AddEditCategoryContent },
        { path: 'plan/courses/content', component: Content },
        { path: 'plan/courses/addEditCourseContent', component: AddEditCourseContent },
        { path: 'preonboarding/preoncourses', component: pCourses },
        { path: 'preonboarding/preoncourses/Preoncategory', component: PreonCategory },
        { path: 'preonboarding/preoncourses/addEditPreonCategoryContent', component: AddEditPreonCategoryContent },
        { path: 'preonboarding/preoncourses/preoncontent', component: PreonContent },
        { path: 'preonboarding/preoncourses/addEditPreonCourseContent', component: AddEditPreonCourseContent },
        { path: 'plan/courseBundle', component: CourseBundle },
        { path: 'plan/roleManagement', component: roleManagement },
        { path: 'plan/roleManagement/addeditrole', component: addeditrole },
        { path: 'plan/notification-templates', component: NotificationTemplatesComponent },
        { path: 'plan/notification-templates/add-edit-notification-template', component: AddEditNotificationTemplateComponent },
        { path: 'plan/trainer', component: TrainerComponent },
        { path: 'plan/openApi',component: OpenApiComponent},
        { path: 'plan/Institutes', component: InstitutesComponent},
        { path: 'plan/gbh-mapping', component: GbhMappingComponent},
        //{ path: 'paln/open-Api/addApi', component:AddapiComponent},
        { path: 'plan/trainer/add-edit-trainer', component: AddTrainerComponent },
        // { path: 'learning/blended-home', component: BlendedHomeComponent },
        { path: 'blended-home/blended-courses', component: BlendedCoursesComponent },
        { path: 'blended-home/blended-courses/addEditCourseContent', component: AddEditBlendCourseContent },
        { path: 'blended-home/blended-category', component: BlendedCategoryComponent },
        { path: 'blended-home/blended-category/add-edit-blended-category', component: AddEditCategoryBlended },
        { path: 'blended-home/bulk-batch', component: BulkBatchComponent },
        { path: 'blended-home/blended-programs', component: BlendedProgramComponent },
        { path: 'blended-home/blended-programs/add-edit-program', component: AddEditProgramBlended },
        // { path: 'nomination-instance', component: NominationInstanceComponent },
        { path: 'nomination-instance/add-edit-nomination', component: AddEditNominationComponent },
        // { path: 'eep-workflow', component: EepWorkflowComponent },
        { path: 'eep-workflow/add-edit-eep-workflow', component: AddEditEepWorkflowComponent }
      ]
    },
    // {
    //   path: 'enroll',
    //   component: EnrollComponent,
    // },
    // {
    //   path:'',
    //   component:IntermediateEnroll,
    //    //redirectTo: 'dashboard',
    //   children:[
    //     {path:'enroll/users',component:Users},
    //     {path:'enroll/users/add-edit-users',component:AddeditusersComponent},
    //     {path:'enroll/users/upload-users',component:UploadusersComponent},
    //     {path:'enroll/courses',component:Courses},
    //     {path:'enroll/courses/category',component:Category},
    //     // {path:'enroll/courses/category/addEditCategory',component:Addeditccategory},
    //     // {path:'enroll/courses/content/addEditContent',component:Addeditcontent},
    //     {path:'enroll/courses/content',component:Content},
    //     {path:'enroll/manualEnroll',component:ManualEnroll},
    //     {path:'enroll/rules',component:Rules},
    //     {path:'enroll/self',component:SelfComponent},
    //     {path:'enroll/regulatory', component: RegulatoryComponent}
    //   ]
    // },
    // {
    //   path: 'evaluate',
    //   component: Evaluate,
    // },
    // {
    //   path: '',
    //   component: EvaluateReporting,
    //   //redirectTo: 'dashboard',
    //   children: [
    //     {
    //       path: 'evaluate/reporting',
    //       component: ReportingComponent,
    //     },
    //     { path: 'evaluate/reporting/learning', component: LearningComponent },
    //     { path: 'evaluate/reporting/marketing', component: MarketingComponent },
    //     { path: 'evaluate/reporting/huddle', component: HuddleComponent },
    //     { path: 'evaluate/reporting/learning/pivot', component: PivotComponent },
    //     { path: 'evaluate/reporting/learning/employee-induction-report', component: EmployeeInductionReportComponent },
    //     { path: 'evaluate/reporting/learning/employee-login-report', component: EmployeeLoginReportComponent },
    //     { path: 'evaluate/reporting/learning/user-activity-report', component: UserActivityReportComponent },
    //     { path: 'evaluate/reporting/learning/course-completion-initial-report-date', component: CourseCompletionInitialReportDateComponent },
    //     { path: 'evaluate/reporting/learning/course-completion-initial-report-date/course-completion-report-date', component: CourseCompletionReportDateComponent },
    //     { path: 'evaluate/reporting/learning/quiz-completion-initial-report/quiz-completion-report', component: QuizCompletionReportComponent },
    //     { path: 'evaluate/reporting/learning/quiz-completion-initial-report', component: QuizCompletionInitialReportComponent },
    //     { path: 'evaluate/reporting/learning/feedback-initial-report/feedback-report', component: FeedbackReportComponent },
    //     { path: 'evaluate/reporting/learning/feedback-initial-report', component: FeedbackInitialReportComponent },
    //     { path: 'evaluate/reporting/learning/buddy-calling-report', component: BuddyCallingReportComponent },
    //     { path: 'evaluate/reporting/learning/course-completion-initial-report-percentage', component: CourseCompletionInitialReportPercentageComponent },
    //     { path: 'evaluate/reporting/learning/course-completion-initial-report-percentage/course-completion-report-percentage', component: CourseCompletionReportPercentageComponent },
    //     { path: 'evaluate/reporting/learning/course-module-completion-initial-report', component: CourseModuleCompletionInitialReportComponent },
    //     { path: 'evaluate/reporting/learning/course-module-completion-initial-report/course-module-completion-report', component: CourseModuleCompletionReportComponent },
    //     { path: 'evaluate/reporting/learning/course-summery-initial-report', component: CourseSummeryInitialReportComponent },
    //     { path: 'evaluate/reporting/learning/course-summery-initial-report/course-summery-report', component: CourseSummeryReportComponent },
    //   ]

    // },
    {
      path: 'gamification',
      component: GamificationComponent,
    },
    {
      path: '',
      component: intermediategame,
      //redirectTo: 'dashboard',
      children: [
        { path: 'gamification/badges_category/badges', component: Badges },
        { path: 'gamification/certificate', component: CertificateComponent },
        { path: 'gamification/leaderboards', component: ladders },
        { path: 'gamification/socialSharing', component: socialSharing },
        { path: 'gamification/badges_category/badges/addeditbadges', component: addeditbadges },
        { path: 'gamification/certificate/add-edit-certificate', component: AddEditCertificateComponent },
        { path: 'gamification/leaderboards/add-edit-leaderboards', component: addeditladders },
        { path: 'gamification/socialSharing/addedisocialSharing', component: addedisocialSharing },
        { path: 'gamification/badges_category', component: BadgeCategoryComponent },
      ]

    },
    {
      path: 'assessment',
      component: assessmentComponent,
    },
    {
      path: 'tools',
      loadChildren : './tools/tools.module#ToolsModule',
      data: { preload: false },
      // loadChildren: () => import('./lazy/lazy.module').then(m => m.LazyModule)
      // loadChildren: './module-activity-module/module-activity.module#ModuleActivityModule',
    },
    // {
    //   path: 'tools',
    //   component: ToolsComponent,
    // },
    // {
    //   path: '',
    //   component: IntermediateToolsComponent,
    //   //redirectTo: 'dashboard',
    //   children: [
    //     { path: 'tools/content', component: ContentComponent },
    //     //{ path: 'tools/section', component: SectionComponent },
    //     { path: 'tools/content/add-edit-content', component: AddEditContentComponent },




    //   ]
    // },
    {
      path: 'dam',
      loadChildren : './dam/dam.module#DamModule',
      data: { preload: false },
    },
    // {
    //   path: '',
    //   component: IntermediateDamComponent,
    //   //redirectTo: 'dashboard',
    //   children: [
    //     { path: 'dam/asset-category', component: AssetCategoriesComponent },
    //     { path: 'dam/assets', component: NewAssetComponent },
    //     { path: 'dam/folders', component: FolderFileComponent },
    //     { path: 'dam/asset-category/asset/add-edit-asset', component: AddAssetComponent },//Add Asset >asset-cateory>asset
    //     { path: 'dam/assets/add-edit-asset', component: AddAssetComponent },//Add Asset >asset-cateory>asset
    //     { path: 'dam/add-asset', component: AddAssetComponent },//Add Asset Direct
    //     { path: 'dam/asset/share-asset', component: ShareAssetComponent },
    //     { path: 'dam/assets/share-asset', component: ShareAssetComponent },
    //     { path: 'dam/asset/asset-review-policy', component: AssetReviewPolicyComponent },
    //     { path: 'dam/assets/asset-review-policy', component: AssetReviewPolicyComponent },
    //     { path: 'dam/asset/approve-asset', component: ApproveAssetComponent },
    //     { path: 'dam/asset-category/asset', component: AssetComponent },//Asset from category
    //     { path: 'dam/asset', component: AssetComponent },//Direct Asset
    //     { path: 'dam/asset-category/newasset', component: NewAssetComponent },//Asset from category
    //     { path: 'dam/assets', component: NewAssetComponent },//Direct Asset
    //     { path: 'dam/folders', component: FolderFileComponent },

    //     { path: 'dam/asset-category/asset/asset-version', component: AssetVersionComponent },
    //     { path: 'dam/assets/asset-version', component: AssetVersionComponent },
    //     { path: 'dam/bulk-upload', component: BulkUploadComponent },
    //     { path: 'dam/bulk-upload/file-upload', component: FolderUploadComponent },
    //     { path: 'dam/assets/file-upload', component: FolderUploadComponent },
    //     // {path : 'dam/asset-category',component : AssetCategoryComponent },
    //     // {path : 'dam/asset-category/add-edit-asset-category',component : AddEditAssetCategoryComponent}
    //   ]

    // },

    // News and Annoucement Component

    {
      path: 'news',
      loadChildren : './newsand-announcement/news.module#NewsModule',
      data: { preload: false }
      // loadChildren: () => import('./lazy/lazy.module').then(m => m.LazyModule)
      // loadChildren: './module-activity-module/module-activity.module#ModuleActivityModule',
    },
    // {
    //   path: 'news',
    //   component: NewsandAnnouncementComponent,
    // },
    // {
    //   path: '',
    //   component: NewsandAnnoucementIntermediateComponent,
    //   //redirectTo: 'dashboard',
    //   children: [
    //     { path: 'news/add-edit-news', component: EnrollNewsComponent },
    //     // {path : 'news/add-edit/basic-details', component:BasicDetailsComponent},
    //     // {path : 'news/add-edit/enrol', component:EnrolComponent},
    //   ]

    // },
    // {
    //   path: 'EnrollNews',
    //   component: EnrollNewsComponent,
    // },


    {
      path: '',
      component: intermediateasses,
      //redirectTo: 'dashboard',
      children: [
        { path: 'assessment/qcategory', component: qCategory },
        { path: 'assessment/qcategory/qaddEditCategory', component: qAddeditccategory },
        { path: 'assessment/qcategory/questions', component: questions },
        { path: 'assessment/qcategory/addEditquestion', component: addEditquestion },
        { path: 'assessment/qcategory/upload_questions', component: UploadQuestionComponent },
        { path: 'assessment/quiz', component: quiz },
        { path: 'assessment/quiz/addEditquiz', component: Addeditquiz },
        { path: 'reactions/feedback', component: feedback },
        { path: 'reactions/feedback/addEditfeedback', component: Addeditfeedback },
        { path: 'reactions/survey', component: survey },
        { path: 'reactions/survey/add-edit-survey', component: Addeditsurvey },
        { path: 'reactions/poll', component: pole },
        { path: 'reactions/poll/add-edit-poll', component: Addeditpole },
        // {path : 'gamification/badges/addeditbadges',component:addeditbadges},
        // {path : 'gamification/certificates/addeditcertificates',component:addeditcertificates},
        // {path : 'gamification/ladders/addeditladders',component:addeditladders},
        // {path : 'gamification/socialSharing/addedisocialSharing',component:addedisocialSharing},
      ]

    },
    {
      path: 'reactions',
      component: ReactionsComponent,
    },
    /***************************Coaching Call************************* */

    {
      path: 'coaching',
      loadChildren : './coaching/coaching.module#CoachingModule',
      data: { preload: false }
      // loadChildren: () => import('./lazy/lazy.module').then(m => m.LazyModule)
      // loadChildren: './module-activity-module/module-activity.module#ModuleActivityModule',
    },

    // {
    //   path: 'coaching',
    //   component: CoachingComponent,
    // },
    // {
    //   path: '',
    //   component: IntermediateCoach,
    //   children: [
    //     { path: 'coaching/team', component: TeamComponent },
    //     { path: 'coaching/feedback-template', component: FeedackTemplateComponent },
    //     { path: 'coaching/assessment', component: AssessmentTemplateComponent },
    //     { path: 'coaching/observation', component: ObservationTemplateComponent },
    //     { path: 'coaching/call-mapping', component: CallMappingComponent },
    //     { path: 'coaching/team/add-edit-team', component: AddEditTeamComponent },
    //     { path: 'coaching/participants', component: ParticipantsComponent },
    //     { path: 'coaching/participants/calendar', component: CalendarComponent },
    //     { path: 'coaching/participants/calendar/call-detail', component: CallDetailComponent },
    //     { path: 'coaching/participants/add-edit-participants', component: AddParticipantsComponent },
    //     { path: 'coaching/participants/bulk-upload-coaching', component: BulkUploadCoachingComponent },
    //     { path: 'coaching/partner', component: PartnerComponent },
    //     { path: 'coaching/partner/add-partner', component: AddPartnerComponent },
    //     { path: 'coaching/attribute-skills', component: AttributeSkillComponent },
    //     { path: 'coaching/attribute-skills/add-attribute-skill', component: AddAttributeSkillComponent },
    //     { path: 'coaching/attribute-master', component: AttributeMasterComponent },
    //     { path: 'coaching/attribute-master/add-attribute-master', component: AddAttributeMasterComponent },
    //     { path: 'coaching/calls', component: CallsComponent },
    //     { path: 'coaching/participants/calendar/call-detail/content/course-details', component: CourseDetailComponent },
    //     { path: 'coaching/master', component: MasterComponent }
    //   ]
    // },

    // {
    //   path: 'task-manager',
    //   component: TaskManagerComponent,
    // },

    {
      path: 'reports',
      loadChildren : './reports/reports.module#ReportsModule',
      data: { preload: false }
    },

    // {
    //   path: 'reports',
    //   component: ReportsComponent,
    // },
    // {
    //   path: '',
    //   component: IntermediateReport,
    //   children: [
    //     { path: 'reports/reports_home', component: ReportsHomeComponent },
    //     { path: 'reports/reports_home/course-consumption/:menuId', component: CourseConsumptionReportComponent },
    //     { path: 'reports/reports_home/activity-consumption/:menuId', component: ActivityConsumptionReportComponent },
    //     { path: 'reports/reports_home/quiz-consumption/:menuId', component: QuizConsumptionReportComponent },
    //     { path: 'reports/reports_home/feedback-consumption/:menuId', component: FeedbackConsumptionReportComponent },
    //     { path: 'reports/reports_home/employee-attendance/:menuId', component: EmployeeAttendanceComponent },
    //     { path: 'reports/reports_home/classroom-reports/:menuId', component: ClassroomReportComponent },
    //     { path: 'reports/report-viewer/:menuId', component: ReportViewerComponent },
    //     { path: 'reports/reports_home/workflow-reports/:menuId', component: WorkflowReportComponent },
    //     { path: 'reports/reports_home/ttt_reports/:menuId', component: TttReportsComponent },
    //     { path: 'reports/reports_home/ta_reports/:menuId', component: TaReportsComponent },
    //     { path: 'reports/reports_home/ta_emp_reports/:menuId', component: TaEmpReportsComponent },
    //     { path: 'reports/pre-onboarding/:menuId', component: PreOnboardingReportComponent },
    //     { path: 'reports/employeeconsumption/:menuId', component: EmployeeConsumptionComponent },
    //     { path: 'reports/ReportSchedule', component: ReportScheduleComponent },
    //     { path: 'reports/ReportList', component: ReportListComponent},
    //     { path: 'reports/reaction_home', component: ReactionHomeComponent },
    //     { path: 'reports/eep-reports', component: EepReportsComponent },
    //     { path: 'reports/reaction_home/survey-reports/:menuId', component: SurveyReportComponent },
    //     { path: 'reports/reaction_home/poll-reports/:menuId', component: PollReportComponent },
    //     { path: 'reports/eep-reports/eep-nomination-report/:menuId', component: EepReportComponent },

    //   ]
    // },
    {
      path: 'trainerDashbord',
      loadChildren : './trainer/trainer-dashboard.module#TrainerDashboardModule',
      data: { preload: false }
    },

    // {
    //   path: 'trainerDashbord/level-1',
    //   component: Level1Component,
    // },
    // {
    //   path: 'trainerDashbord/level-2',
    //   component: Level2Component,
    // },
    // {
    //   path: 'trainerDashbord/feedback',
    //   component: FeedbackDataComponent
    // },
    // {
    //   path: 'trainerDashbord/attendance',
    //   component: AttendanceDataComponent
    // },
    // {
    //   path: 'trainerDashbord/assessment',
    //   component: AssessmentDataComponent
    // },
    {
      path: '',
      redirectTo: 'login',
      pathMatch: 'full',
    },
    // { path: 'trainerDashbord/trainer-details', component: TrainerDetailsComponent },
    {
      path: '**',
      component: NotFoundComponent,
    }],
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
