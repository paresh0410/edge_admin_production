import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ChartsModule } from 'ng2-charts';
import { PagesRoutingModule } from './pages-routing.module'
import { PagesComponent } from './pages.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { PlanModule } from './plan/plan.module';
import { LearningMainModule } from './learning/learning.module';
import { DemoMaterialModule } from './material-module';
import{MatDialogModule}from '@angular/material';
import { ModalDirective } from 'ngx-bootstrap';
import {ComponentModule} from '../component/component.module';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

//import {TableModalComponent} from '../../../component/table-modal/table-modal.component';
// import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
// import {BreadcrumbsModule} from "ng6-breadcrumlearningbs";
// import { Users } from './plan/users/users.component';
// import { Courses } from './plan/courses/courses.component';
// import { Category } from './plan/courses/category/category.component';
// import { Content } from './plan/courses/content/content';

// import { Addeditccategory } from './plan/courses/category/addEditCategory/addEditCategory.component';
// import { AddeditccategoryService } from './plan/courses/category/addEditCategory/addEditCategory.service';

import { AddEditCategoryContent } from './plan/courses/addEditCategoryContent/addEditCategoryContent.component';
import { AddEditCategoryContentService } from './plan/courses/addEditCategoryContent/addEditCategoryContent.service'
import { AddEditCategoryContentModule } from './plan/courses/addEditCategoryContent/addEditCategoryContent.module'

// import { Addeditcontent } from './plan/courses/addEditContent/addEditContent';
// import { addEditContentService } from './plan/courses/addEditContent/addEditContent.service'


import { AddEditCourseContent } from './plan/courses/addEditCourseContent/addEditCourseContent';
import { AddEditCourseContentService } from './plan/courses/addEditCourseContent/addEditCourseContent.service'
import { AddEditCourseContentModule } from './plan/courses/addEditCourseContent/addEditCourseContent.module';
//// preon course
import { PreonCategoryModule  } from './plan/preoncourses/Preoncategory/Preoncategory.module';
import { PreonContentModule  } from './plan/preoncourses/preoncontent/preoncontent.module';
// import {PreonNotFoundComponent} from '../pages/plan/Preoncourses/Preoncategory/not-found/not-found.component';
//import { AddEditPreonCategoryContent } from './plan/preoncourses/addEditPreonCategoryContent/addEditPreonCategoryContent.component';
//import { AddEditPreonCategoryContentService } from './plan/preoncourses/addEditPreonCategoryContent/addEditPreonCategoryContent.service'
 import { AddEditPreonCategoryContentModule } from './plan/preoncourses/addEditPreonCategoryContent/addEditPreonCategoryContent.module'
//import { AddEditPreonCourseContent } from './plan/preoncourses/addEditPreonCourseContent/addEditPreonCourseContent';
//import { AddEditPreonCourseContentService } from './plan/preoncourses/addEditPreonCourseContent/addEditPreonCourseContent.service'
import { AddEditPreonCourseContentModule } from './plan/preoncourses/addEditPreonCourseContent/addEditPreonCourseContent.module';

import { EmployeesModule } from './plan/employees/employees.module';
import { AddeditemployeesModule } from './plan/employees/addeditemployees/addeditemployees.module';
import { UploadEmployeesModule } from './plan/employees/uploademployees/uploademployees.module';

import { CourseBundle } from './plan/courseBundle/courseBundle';
import { AddeditusersComponent } from './plan/users/addeditusers/addeditusers.component';
import { UsersModule } from './plan/users/users.module';
import { roleManagementModule } from './plan/roleManagement/roleManagement.module';
import { CourseBundleModule } from './plan/courseBundle/courseBundle.module';
import { addeditroleModule } from './plan/roleManagement/addeditrole/addeditrole.module'
import { AddeditusersModule } from './plan/users/addeditusers/addeditusers.module';
import { UploadUsersModule } from './plan/users/uploadusers/uploadusers.module';
import { Intermediate } from './plan/intermediate/intermediate';
import { IntermediateReport } from './reports/intermediatereports/intermediatereports';
// import { EnrollModule } from './enroll/enroll.module';
import { pCoursesModule } from './plan/preoncourses/courses.module'
import { CategoryModule  } from './plan/courses/category/category.module';
import { ContentModule  } from './plan/courses/content/content.module';
import {NotFoundComponent} from '../pages/plan/courses/category/not-found/not-found.component';
//import { NgxSummernoteModule } from 'ngx-summernote';

// import { Courses } from './enroll/courses/courses.component';
// import { Category } from './enroll/courses/category/category.component';
// import { Addeditccategory } from './enroll/courses/category/addEditCategory/addEditCategory.component';
// import { AddeditccategoryService } from './enroll/courses/category/addEditCategory/addEditCategory.service';
// import { Addeditcontent } from './enroll/courses/content/addEditContent/addEditContent';
// import { addEditContentService } from './enroll/courses/content/addEditContent/addEditContent.service'
// import { Content } from './enroll/courses/content/content';

// import { AddeditusersComponent } from './enroll/users/addeditusers/addeditusers.component';
// import { UsersModule } from './enroll/users/users.module'
// import { AddeditusersModule } from './enroll/users/addeditusers/addeditusers.module';
// import { UploadUsersModule } from './enroll/users/uploadusers/uploadusers.module';

// import { ManualEnroll } from './enroll/manualEnroll/manualEnroll';
// import { ManualEnrollModule } from './enroll/manualEnroll/manualEnroll.module';
// import { RulesModule } from './enroll/rules/rules.module';
// import { Rules } from './enroll/rules/rules';
// import { IntermediateEnroll } from './enroll/intermediateEnroll/intermediateEnroll';

import { LoginModule } from './login/login.module';
// import { PrivacyModule } from './privacy/privacy.module';
import { EvaluateModule } from './evaluate/evaluate.module';
import { EvaluateReporting } from './evaluate/evaluate-reporting/evaluateReporting';
// import { EvaluateModule1 } from './evaluate1/evaluate1.module';
// import { Evaluate1BasicModule } from './evaluate1/evaluate1Basic/evaluate1Basic.module';
import { ReportingComponent } from './evaluate/reporting/reporting.component';
import { LearningComponent } from './evaluate/reporting/learning/learning.component';
import { MarketingComponent } from './evaluate/reporting/marketing/marketing.component';
import { HuddleComponent } from './evaluate/reporting/huddle/huddle.component';
import { PivotComponent } from './evaluate/reporting/learning/pivot/pivot.component';
// import { EmployeeInductionReportComponent } from './evaluate/reporting/learning/employee-induction-report/employee-induction-report.component';
// import { EmployeeLoginReportComponent } from './evaluate/reporting/learning/employee-login-report/employee-login-report.component';
import { EmployeeLoginReportService } from './evaluate/reporting/learning/employee-login-report/employee-login-report.service'
// import { UserActivityReportComponent } from './evaluate/reporting/learning/user-activity-report/user-activity-report.component';
import { UserActivityReportService } from './evaluate/reporting/learning/user-activity-report/user-activity-report.service';
// import { CourseCompletionReportDateComponent} from './evaluate/reporting/learning/course-completion-report-date/course-completion-report-date.component';
import { CourseCompletionReportDateService } from './evaluate/reporting/learning/course-completion-report-date/course-completion-report-date.service'
// import { QuizCompletionReportComponent } from './evaluate/reporting/learning/quiz-completion-report/quiz-completion-report.component';
import { QuizCompletionReportService } from './evaluate/reporting/learning/quiz-completion-report/quiz-completion-report.service'
import { QuizCompletionInitialReportService } from './evaluate/reporting/learning/quiz-completion-report/quiz-completion-initial-report.service'
// import { FeedbackReportComponent } from './evaluate/reporting/learning/feedback-report/feedback-report.component';
// import { BuddyCallingReportComponent } from './evaluate/reporting/learning/buddy-calling-report/buddy-calling-report.component';
import { CourseCompletionReportPercentageComponent} from './evaluate/reporting/learning/course-completion-report-percentage/course-completion-report-percentage.component';
// import { ECommerceModule } from './e-commerce/e-commerce.module';
import { ThemeModule } from '../@theme/theme.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
// import { slikgridDemoModule } from './slikgridDemo/slikgridDemo.module';
// import { EmployeeInductionReportModule } from './evaluate/reporting/learning/employee-induction-report/employee-induction-report.module';
// import { EmployeeLoginReportModule } from './evaluate/reporting/learning/employee-login-report/employee-login-report.module';
// import { FeedbackReportModule } from './evaluate/reporting/learning/feedback-report/feedback-report.module';
// import { FeedbackInitialReportModule } from './evaluate/reporting/learning/feedback-report/feedback-initial-report.module';
// import { UserActivityReportModule } from './evaluate/reporting/learning/user-activity-report/user-activity-report.module';
// import { CourseCompletionInitialReportDateModule } from './evaluate/reporting/learning/course-completion-report-date/course-completion-initial-report-date.module';
// import { CourseCompletionReportDateModule } from './evaluate/reporting/learning/course-completion-report-date/course-completion-report-date.module';
// import { QuizCompletionReportModule } from './evaluate/reporting/learning/quiz-completion-report/quiz-completion-report.module';
// import { QuizCompletionInitialReportModule } from './evaluate/reporting/learning/quiz-completion-report/quiz-completion-initial-report.module';
// import { BuddyCallingReportModule } from './evaluate/reporting/learning/buddy-calling-report/buddy-calling-report.module';
// import { CourseCompletionInitialReportPercentageModule } from './evaluate/reporting/learning/course-completion-report-percentage/course-completion-initial-report-percentage.module';
// import { CourseCompletionReportPercentageModule } from './evaluate/reporting/learning/course-completion-report-percentage/course-completion-report-percentage.module';
// import { ModuleCompletionReportModule } from './evaluate/reporting/learning/course-module-completion-report/course-module-completion-report.module';
// import {CourseModuleCompletionInitialReportModule} from './evaluate/reporting/learning/course-module-completion-report/course-module-completion-initial-report.module'
// import { CourseSummeryReportModule } from './evaluate/reporting/learning/course-summery-report/course-summery-report.module';
// import {CourseSummeryInitialReportModule} from './evaluate/reporting/learning/course-summery-report/course-summery-initial-report.module';

import { NgxPaginationModule } from 'ngx-pagination';
import { FilterPipeModule  } from 'ngx-filter-pipe';
import { OrderModule  } from 'ngx-order-pipe';
import { TableModule } from 'ngx-easy-table';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import {TabsModule} from "ngx-tabs";
//import { SelfComponent } from './enroll/self/self.component'
import { CourseModuleCompletionInitialReportService } from './evaluate/reporting/learning/course-module-completion-report/course-module-completion-initial-report.service';
// import { ToastrModule } from 'ngx-toastr';
// import { detailsComponent } from './plan/courses/addEditCourseContent/details/details.component';

// import { LoginModule} from './login/login.module'
import { intermediategame } from './gamification/intermediategame/intermediategame';
import { GamificationModule } from './gamification/gamification.module';
import { BadgesModule } from './gamification/badges/badges.module';
import { CertificateModule  } from './gamification/certificate/certificate.module';
import { laddersModule } from './gamification/ladders/ladders.module';
import { socialSharingModule } from './gamification/socialSharing/socialSharing.module';
import { addeditbadgesModule } from './gamification/badges/addeditbadges/addeditbadges.module';
//import { addeditcertificatesModule } from './gamification/certificates/addeditcertificates/addeditcertificates.module';
import { addeditladdersModule } from './gamification/ladders/addeditladders/addeditladders.module';
import { addedisocialSharingModule } from './gamification/socialSharing/addedisocialSharing/addedisocialSharing.module';
import { qCategoryModule } from './assessment/qcategory/qcategory.module';
import { qAddeditccategoryModule } from './assessment/qcategory/qaddEditCategory/qaddEditCategory.module';
import { questionsModule } from './assessment/qcategory/questions/questions.module';
import { assessmentModule } from './assessment/assessment.module';
import { intermediateasses } from './assessment/intermediateasses/intermediateasses';
import { addEditquestionModule } from './assessment/qcategory/addEditquestion/addEditquestion.module';
import { quizModule } from './assessment/quiz/quiz.module';
import { AddeditquizModule } from './assessment/quiz/addEditquiz/addEditquiz.module';
import { feedbackModule } from './assessment/feedback/feedback.module';
import { AddeditfeedbackModule } from './assessment/feedback/addEditfeedback/addEditfeedback.module';
import { surveyModule } from './assessment/survey/survey.module';
import { AddeditsurveyModule } from './assessment/survey/addEditsurvey/addEditsurvey.module';
import { poleModule } from './assessment/pole/pole.module';
import { AddeditpoleModule } from './assessment/pole/addEditpole/addEditpole.module';
import { ReactionsModule } from './reactions/reactions.module'

import { BadgesCategoryModule } from './gamification/badge-category/badge-category.module'

import { HttpConfigInterceptor } from '../interceptor/interceptor';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

/***************************Caoaching Call************************************ */
import { IntermediateCoach } from './coaching/intermediatecoach/intermediatecoach';
// import { CoachingModule } from './coaching/coaching.module';


import { BlendedCoursesModule } from '../pages/plan/blended-courses/blended-courses.module';
// import { TaskManagerModule } from './task-manager/task-manager.module';
import { AddEditBlendCourseContentModule } from './plan/blended-courses/addEditCourseContent/addEditCourseContent.module';

import { DatePipe } from '@angular/common';
import { UserChatService } from '../service/user-chat-service';
import { ModalViewerComponent } from '../component/modal-viewer/modal-viewer.component';
import { FeedackTemplateComponent } from '../pages/coaching/feedack-template/feedack-template.component';
/************************************DAM**************************************** */
//import { DamComponent } from './dam/dam.component'
// import { DamModule } from './dam/dam.module';
import { IntermediateDamComponent} from './dam/intermediate-dam/intermediate-dam.component';
import { ContentUploadComponent } from './content-upload/content-upload.component';
// import { NewsModule } from './newsand-announcement/news.module';

/************************************News**************************************** */
// import { IntermediateDamComponent} from './dam/intermediate-dam/intermediate-dam.component';
import { NewsandAnnoucementIntermediateComponent } from './newsand-announcement/newsand-annoucement-intermediate/newsand-annoucement-intermediate.component'
/*PRod BUild */
import{ComponentsComponent} from './components/components.component';
// import{TreeComponent} from './components/tree/tree.component';
import{NotificationsComponent} from './components/notifications/notifications.component';
import { YoutubePlayerModule } from 'ngx-youtube-player';

// import { ReportsModule } from './reports/reports.module';
// import { CourseConsumptionReportModule } from './reports/course-consumption-report/course-cosumpiton-report.module';
// import { ActivityConsumptionReportModule } from './reports/activity-consumption-report/activity-consumption-report.module';

// import { QuizConsumptionReportModule } from './reports/quiz-consumption-report/quiz-consumption-report.module';
// import { FeedbackConsumptionReportModule } from './reports/feedback-consumption-report/feedback-consumption-report.module';
// import { ReportViewerModule } from './reports/report-viewer/report-viewer.module';
import { ExcelService } from '../service/excel-service';
// import { ToolsModule } from './tools/tools.module';
import { IntermediateToolsComponent } from './tools/intermediate-tools/intermediate-tools.component';
//import { ToolsComponent } from './tools/tools.component';
// import {InfiniteScrollModule} from 'ngx-infinite-scroll';
// import { TrainerDashboardModule } from './trainer/trainer-dashboard.module';
import { PopupComponent } from './plan/blended-courses/eep-workflow/add-edit-eep-workflow/popup/popup.component';

// ==================================== Setting ========================================
import { ThemeChangeComponent } from './theme-change/theme-change.component';
import { ColorPickerModule } from 'ngx-color-picker';
import { RecordRtcComponent } from './record-rtc/record-rtc.component'
import { DiscountComponent } from './plan/discount/discount.component';
import { PricingComponent } from './plan/pricing/pricing.component';
import { PreonboardingComponent } from './preonboarding/preonboarding.component';
// import { ContextMenuModule } from 'ngx-contextmenu';
// import { GridstackModule } from 'libria-gridstack';
const PAGES_COMPONENTS = [
  PagesComponent,
];

@NgModule({
  imports: [
    ColorPickerModule,
    pCoursesModule,
    PreonCategoryModule,
    PreonContentModule,
  AddEditBlendCourseContentModule,
   AddEditPreonCategoryContentModule,
   AddEditPreonCourseContentModule,
  addEditquestionModule,
    PagesRoutingModule,
    ThemeModule,
    ComponentModule,
    DashboardModule,
    PlanModule,
    // EnrollModule,
    UsersModule,
    CourseBundleModule,
    // ManualEnrollModule,
    // RulesModule,
    CategoryModule,
    ContentModule,
    AddEditCourseContentModule,
    AddEditCategoryContentModule,
    // CourseSummeryReportModule,
    // CourseSummeryInitialReportModule,
    BlendedCoursesModule,
    YoutubePlayerModule,
   // NgxSummernoteModule,
    // BreadcrumbsModule,
    // LoginModule,
    EvaluateModule,
    // ECommerceModule,
    MiscellaneousModule,
    TableModule,
    ChartsModule,
    NgxPaginationModule,
    FilterPipeModule,
    OrderModule,
    NgxMyDatePickerModule,
    TabsModule,
    // slikgridDemoModule,
    // EmployeeInductionReportModule,
    // EmployeeLoginReportModule,
    // FeedbackReportModule,
    // UserActivityReportModule,
    // CourseCompletionReportDateModule,
    // CourseCompletionInitialReportDateModule,
    // QuizCompletionReportModule,
    // QuizCompletionInitialReportModule,
    // BuddyCallingReportModule,
    // CourseCompletionReportPercentageModule,
    // CourseCompletionInitialReportPercentageModule,
    AddeditusersModule,
    // FeedbackInitialReportModule,
    UploadUsersModule,
    // ModuleCompletionReportModule,
    // CourseModuleCompletionInitialReportModule,
    roleManagementModule,
    addeditroleModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    EmployeesModule,
    AddeditemployeesModule,
    UploadEmployeesModule,
    LearningMainModule,
    GamificationModule,
    BadgesModule,
    CertificateModule ,
    laddersModule,
    socialSharingModule,
    addeditbadgesModule,
    ReactionsModule,
   // addeditcertificatesModule,
    addedisocialSharingModule,
    addeditladdersModule,
    assessmentModule,
    qCategoryModule,
    qAddeditccategoryModule,
    questionsModule,
    quizModule,
    AddeditquizModule,
    feedbackModule,
    AddeditfeedbackModule,
    surveyModule,
    AddeditsurveyModule,
    poleModule,
    AddeditpoleModule,
    // NgxSkeletonLoaderModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    BadgesCategoryModule,
    // ToastrModule.forRoot({
    //   timeOut: 4000,
    //   preventDuplicates: true,
    //   enableHtml: true,
    // }),
    // CoachingModule , // coaching call module
    //IntermediateCoach,    // coaching call module
    // TaskManagerModule,
    DemoMaterialModule,
    // DamModule,
    // NewsModule,
    // ReportsModule,
    // ToolsModule,
    // PrivacyModule,
    // InfiniteScrollModule,
    // CourseConsumptionReportModule,
    // ActivityConsumptionReportModule,
    // QuizConsumptionReportModule,
    // FeedbackConsumptionReportModule,
    // ReportViewerModule
    // TrainerDashboardModule,
    // ComponentModule,
    // GridstackModule.forRoot(),
    // TrainerDashboardModule,
    ComponentModule,
    // ContextMenuModule.forRoot(),
    OwlDateTimeModule,
     OwlNativeDateTimeModule
  ],
  entryComponents: [
    //  PopupComponent,
  ],
  declarations: [
    ...PAGES_COMPONENTS,
    ReportingComponent,
    LearningComponent,
    MarketingComponent,
    HuddleComponent,
    PivotComponent,
    intermediateasses,
    // PreonNotFoundComponent,
    // AddEditPreonCategoryContent,
    // AddEditPreonCourseContent,
    //SelfComponent,
   //AddeditusersComponent,
    //EmployeeInductionReportComponent,
    //EmployeeLoginReportComponent,
    //UserActivityReportComponent,
    //CourseCompletionReportDateComponent,
    //QuizCompletionReportComponent,
    //FeedbackReportComponent,
    //BuddyCallingReportComponent,
   //CourseCompletionReportPercentageComponent,
    EvaluateReporting,
    Intermediate,
    // IntermediateEnroll,
    intermediategame,
    IntermediateDamComponent,
    IntermediateToolsComponent,

    // Courses,
    //ModalViewerComponent,

    // Category,
    // Content,
    // Addeditccategory,
    // Addeditcontent,
    // detailsComponent,
    IntermediateCoach,
    ContentUploadComponent,
    NewsandAnnoucementIntermediateComponent,
    //DamComponent
    ComponentsComponent,
    // TreeComponent,
    NotificationsComponent,
    IntermediateReport,
    NotFoundComponent,
    ThemeChangeComponent,
    RecordRtcComponent,
    DiscountComponent,
    PricingComponent,
    PreonboardingComponent,
  ],
  providers: [
     // AddeditccategoryService,
     // addEditContentService,
     { provide : HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi:true},
     EmployeeLoginReportService,
     UserActivityReportService,
     CourseCompletionReportDateService,
     QuizCompletionReportService,
     QuizCompletionInitialReportService,
     CourseModuleCompletionInitialReportService,
     DatePipe,
     UserChatService,
     ExcelService,
    //  AddEditPreonCategoryContentService,
    //  AddEditPreonCourseContentService,
  ],
  exports: [
    // MatDialogModule,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})

export class PagesModule {

}
