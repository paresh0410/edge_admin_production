import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
	selector: 'ngx-logout',
	template: 'logout',
	// styleUrls: ['./login.component.scss']
})
export class LogoutComponent {

	constructor(private router: Router) {
		this.logout();
		
	}
	
  	logout() {
		console.log('Logout called');
		localStorage.clear();
		sessionStorage.clear();
		window.localStorage.clear();
		this.router.navigate(['login']);
  	}
}