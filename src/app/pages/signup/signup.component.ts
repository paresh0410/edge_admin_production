import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute
  //  NavigationStart
   } from "@angular/router";
import {Md5} from 'ts-md5/dist/md5';
import { webApi } from '../../service/webApi';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CommonFunctionsService } from '../../service/common-functions.service';
import { BrandDetailsService } from '../../service/brand-details.service';

@Component({
  selector: 'ngx-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  planId = null;
  show = false;
  currentBrandData: any;
  constructor(
    private router: Router,
    public brandService: BrandDetailsService,
    private route:ActivatedRoute,
    private spinnerNgx: NgxSpinnerService,
    private toastr: ToastrService,
    private commonFunctionsService: CommonFunctionsService,
  ) {
    this.currentBrandData = this.brandService.getCurrentBrandData();
    var id = document.getElementById("nb-global-spinner");

    id.style.display = "none";
    this.planId = this.route.snapshot.paramMap.get('priceId');
    // New Routing
    // this.route.queryParams
    //   .subscribe(params => {
    //     console.log(params); // {order: "popular"}

    //     // this.order = params.order;
    //     // console.log(this.order); // popular
    //   });
  }

  ngOnInit() {
  }

  signin: boolean = false;
  signUpDetails = {
    'firstName': '',
    'lastName':'',
    'email': '',
    'phone': '',
    'password': '',
    'confirmPassword': '',
    'termsAndCondition': false,
    'organization':'',
  }
  signIn() {
    this.router.navigate(["../login"]);
    // this.signin = !this.signin;
    // if(form){
    //   form.reset();
    // }
    // this.signUpDetails = {
    //   'firstName': '',
    //   'lastName':'',
    //   'email': '',
    //   'phone': '',
    //   'password': '',
    //   'confirmPassword': '',
    //   'termsAndCondition': false,
    //   'organization':'',
    // };
    // this.mailSent = false;
    // this.count = 0;
  }

  sendVerificationLink(signupForm){
    if(signupForm.valid){
      console.log('Signup Data ==>', this.signUpDetails);
      console.log('Signup Data ==>', Md5.hashStr(this.signUpDetails.email));

      this.insertDataAndSendMail();
    }else {
    Object.keys(signupForm.controls).forEach(key => {
      signupForm.controls[key].markAsTouched();
    });
    console.log('Please fill all fields ==>', this.signUpDetails);
   }

  }

  mailSent = false;
  mailSentMessage = '';
  resendParameter = {};
  insertDataAndSendMail(){
    this.mailSent = false;
    this.mailSentMessage = '';
    this.resendParameter = {};
    const insertSignUpData = webApi.domain + webApi.url.insertSignUpData;
    this.spinnerNgx.show();
    const param = {
      firstName: this.signUpDetails.firstName,
      lastName: this.signUpDetails.lastName,
      userName: this.signUpDetails.email,
      phone: this.signUpDetails.phone,
      emailId: this.signUpDetails.email,
      uniqueId: Md5.hashStr(this.signUpDetails.email),
      password: this.signUpDetails.password,
      organization: this.signUpDetails.organization,
      link : 'domain-map/' + Md5.hashStr(this.signUpDetails.email),
      platform: 1,
      planId: this.planId,
    };
    // this.spinner.show();
    this.commonFunctionsService
      .httpPostRequest(insertSignUpData, param)
      // this.serveyService.getServey(param)
      .then(
        (rescompData) => {
          // this.spinner.hide();
          this.spinnerNgx.hide();
          if(rescompData['type'] === true){
            this.mailSent = true;
            this.mailSentMessage = rescompData['message'];
            this.toastr.success(rescompData['message'], 'Success');
            param['shortLink'] = rescompData['link'];
            this.resendParameter = param;
          }else {
            if(rescompData['data'] && rescompData['data']['flag'] == 0){
              this.toastr.warning(rescompData['data']['msg'], 'Warning');
              this.router.navigate(['../plans']);
            }
          }

          // this.router.navigate(['domain-map']);
          // console.log('addEditDiscountData ==>', rescompData);
          // if(rescompData && rescompData['type'] === true){
          //   if(rescompData && rescompData['data']){
          //   this.toastr.success(rescompData['data'][0].msg , 'Success');
          //   this.spinnerNgx.hide();
          // }else {
          //   this.toastr.warning('Something went wrong.', 'Warning');
          // }
        },
        (error) => {
          this.spinnerNgx.hide();
          this.toastr.warning('Something went wrong.', 'Warning');
        }
      );
  }

  count: number = 0;
  maxResendCount: number = 3;
  resendEmail(){
    if (this.count < this.maxResendCount) {
      this.mailSent = false;
      const resendVerificationMail = webApi.domain + webApi.url.resendVerificationMail;
      this.spinnerNgx.show();
      // this.spinner.show();
      this.commonFunctionsService
        .httpPostRequest(resendVerificationMail, this.resendParameter)
        // this.serveyService.getServey(param)
        .then(
          (rescompData) => {
            this.spinnerNgx.hide();
            if(rescompData['type'] === true){
              this.mailSent = true;
              this.mailSentMessage = rescompData['message'];
              this.toastr.success(rescompData['message'], 'Success');
              this.count ++;
            }
          },
          (error) => {
            this.spinnerNgx.hide();
            this.toastr.warning('Something went wrong.', 'Warning');
          }
        );
    }else {
      this.toastr.warning('Maximum resend count reached', 'Warning');
    }
  }
  passwordEye() {
    this.show = !this.show;
  }

}
