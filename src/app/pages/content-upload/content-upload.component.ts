import { Component, OnInit } from '@angular/core';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { webAPIService } from '../../service/webAPIService';
import { webApi } from '../../service/webApi';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'ngx-content-upload',
  templateUrl: './content-upload.component.html',
  styleUrls: ['./content-upload.component.scss']
})
export class ContentUploadComponent implements OnInit {

  constructor(
    // private toasterService: ToasterService,
    protected webApiService:webAPIService,private spinner: NgxSpinnerService) { }

  ngOnInit() {
  }

  assetFileData:any;
  fileUpload:any;
  fileName:any;
  selectFileTitle:any;
  enableUpload:boolean=false;
  fileReaded:any;
  fileUrl:any;
  addEditAssetForm : any = {};
  cancelFile(){
    // this.fileUpload.nativeElement.files = [];
    this.assetFileData = null;
    // console.log(this.fileUpload.nativeElement.files);
    if(this.fileUpload){
      if(this.fileUpload.nativeElement){
        this.fileUpload.nativeElement.value = "";
      }
    }

    // console.log(this.fileUpload.nativeElement.files);
    this.fileName = 'You can drag and drop files here to add them.';
    this.selectFileTitle = "No file chosen";
    this.enableUpload = false;
  }

  activityFileData:any=null;
  readFileUrl(event:any) {
    var size = 10000000000000000000000000000000;
  
    var validExts = new Array("image", "video", "audio", "pdf","rar", "zip", "7z");
    
    var fileType = event.target.files[0].type;
    // fileType = fileType.substring(0,5);

    // var validExts = new Array(".png", ".jpg", ".jpeg",);
    // var fileExt = event.target.files[0].name;
    // fileExt = fileExt.substring(fileExt.lastIndexOf('.'));

    var fileExt = false;
    for(let i=0; i<validExts.length; i++){
      if(fileType.includes(validExts[i])){
        // return true;
        fileExt = true;
        break;
      }
    }

    // if(validExts.indexOf(fileType) < 0) {
    if(!fileExt) {
      // var toast : Toast = {
      //   type: 'error',
      //   title: "Invalid file selected!",
      //   body: "Valid files are of " + validExts.toString() + " types.",
      //   showCloseButton: true,
      //   timeout: 2000
      // };
      // this.toasterService.pop(toast);
      this.cancelFile();
    } else {
      if(size <= event.target.files[0].size){
        // var toast : Toast = {
        //     type: 'error',
        //     title: "File size exceeded!",
        //     body: "File size should be less than 100MB",
        //     showCloseButton: true,
        //     timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.cancelFile();
      } else {
        if (event.target.files && event.target.files[0]) {
          this.fileName = event.target.files[0].name;
          //read file from input
          this.fileReaded = event.target.files[0];
          this.assetFileData = event.target.files[0];

          if(this.fileReaded != '' || this.fileReaded != null || this.fileReaded != undefined){
            this.enableUpload = true;
          }
          let file = event.target.files[0];
          var reader = new FileReader();
          reader.onload = (event: ProgressEvent) => {
            this.fileUrl = (<FileReader>event.target).result;
          }
          reader.readAsDataURL(event.target.files[0]);
        }
      }
    }
  }
  errorMsg:any;
  passParams:any;
  fileUploadRes:any;
  fileres:any;
  submit(){
       // this.submitted = true;
       this.spinner.show();
        //console.log('data',data);
        //var tagStr = this.getTagDataReady(data.customTags);
       // console.log('tagStr',tagStr);

        let param = {
          "assetsRef":null,
        }
        this.passParams = param;
        console.log('this.passParams',this.passParams);
        var fd = new FormData();
        fd.append('content',JSON.stringify(this.passParams));
        fd.append('file',this.assetFileData);
        console.log('File Data ',fd);
    
        console.log('Asset File',this.assetFileData);
        console.log('Asset Data ',this.passParams);
        let fileUploadUrl = webApi.domain + webApi.url.fileUpload;
    
        if(this.assetFileData){
          this.webApiService.getService(fileUploadUrl,fd)
            .then(rescompData => { 
              var temp:any = rescompData;
              this.fileUploadRes = temp;
              console.log('rescompData',this.fileUploadRes)
              this.assetFileData = null;
              if(temp == "err"){
                // this.notFound = true;
                // var thumbUpload : Toast = {
                //     type: 'error',
                //     title: "Asset File",
                //     body: "Unable to upload Asset File.",
                //     // body: temp.msg,
                //     showCloseButton: true,
                //     timeout: 2000
                // };
                // this.toasterService.pop(thumbUpload);
                this.spinner.hide();
              }else if(temp.type == false){
                // var thumbUpload : Toast = {
                //     type: 'error',
                //     title: "Asset File",
                //     body: "Unable to upload Asset File.",
                //     // body: temp.msg,
                //     showCloseButton: true,
                //     timeout: 2000
                // };
                // this.toasterService.pop(thumbUpload);
                this.spinner.hide();
              }
              else{
                if(this.fileUploadRes.data != null || this.fileUploadRes.fileError != true){
                  this.passParams.assetsRef = this.fileUploadRes.data.file_url;
                  console.log('this.passparams.assetsRef',this.passParams.assetsRef);
                //   var thumbUpload : Toast = {
                //     type: 'success',
                //     title: "Asset File",
                //     body: "SUCCESSFULLY UPLOADED.",
                //     // body: temp.msg,
                //     showCloseButton: true,
                //     timeout: 2000
                // };
                // this.toasterService.pop(thumbUpload);
                this.spinner.hide();
                  //this.addEditAsset(this.passParams);
                }else{
                  // var thumbUpload : Toast = {
                  //     type: 'error',
                  //     title: "Asset File",
                  //     // body: "Unable to upload course thumbnail.",
                  //     body: this.fileUploadRes.status,
                  //     showCloseButton: true,
                  //     timeout: 2000
                  // };
                  // this.toasterService.pop(thumbUpload);
                 this.spinner.hide();
                }
              }
              console.log('File Upload Result',this.fileUploadRes);
              var res = this.fileUploadRes;
              this.fileres = res.data.file_url;
            },
            resUserError=>{
              this.errorMsg = resUserError;
              console.log('File upload this.errorMsg', this.errorMsg);
              this.spinner.hide();
            });
        }else{
        this.spinner.hide();
        // this.addEditAsset(this.passParams);
        }
  }



}
