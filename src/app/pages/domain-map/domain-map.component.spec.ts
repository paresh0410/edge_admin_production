import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DomainMapComponent } from './domain-map.component';

describe('DomainMapComponent', () => {
  let component: DomainMapComponent;
  let fixture: ComponentFixture<DomainMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DomainMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DomainMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
