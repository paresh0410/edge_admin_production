import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { CommonFunctionsService } from '../../service/common-functions.service';
import { webApi } from '../../service/webApi';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'ngx-domain-map',
  templateUrl: './domain-map.component.html',
  styleUrls: ['./domain-map.component.scss'],
})
export class DomainMapComponent implements OnInit {
  domainMap = {
    domainName: '',
    country: [],
    // 'currency': [],
  };
  domainNameString = '';
  // currencyData = [];
  countryData = [];
  queryParams: any;
  uniqueId:any;
  dropdownSettings = {
    badgeShowLimit: 1,
    lazyLoading: true,
    showCheckbox: true,
    escapeToClose: false,
    singleSelection: true,
    enableSearchFilter: true,
    text: 'Select From Below',
    selectAllText: 'Select All',
    noDataLabel: 'No data found',
    unSelectAllText: 'UnSelect All',
    classes: 'doamin-multi custom-class-example',
    primaryKey: 'id',
    labelKey: 'name',
    limitSelection: 1,
  };
  domainNameCheck = '';
  constructor(
    private router: Router,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private activatedRoute: ActivatedRoute,
    private commonFunctionsService: CommonFunctionsService,
    private cdf: ChangeDetectorRef,
  ) {}

  ngOnInit() {

    let domainURL = window.location.hostname;
      // let domainURL = 'devadmin.edgelearning.co.in';
      if(domainURL === 'localhost'){
        this.domainNameString = '.edgelearning.co.in';
      }else {
        if(domainURL){
          domainURL =  domainURL.substring(domainURL.indexOf('.')+1);
        }
        this.domainNameString = '.' + domainURL;
      }
    var id = document.getElementById('nb-global-spinner');

    id.style.display = 'none';
    // this.activatedRoute.params.subscribe((params) => {
    //   console.log('Nav params ===>', params);
    //   this.queryParams = params;
    //   // this.verifyCode();
    //   if (
    //     !this.queryParams &&
    //     !this.queryParams['uniqueId'] &&
    //     this.queryParams['uniqueId'] === null
    //   ) {
    //     this.toastr.warning('Invalid verfication link', 'Warning');
    //     this.router.navigate(['../login']);
    //   }
    //   this.fetchDomainDropDowns();
    // });

    this.uniqueId = this.activatedRoute.snapshot.paramMap.get('uniqueId');
    console.log('Nav params ===>', this.uniqueId );
    if (
          !this.uniqueId &&
          this.uniqueId === null
        ) {
          this.toastr.warning('Invalid verfication link', 'Warning');
          this.router.navigate(['../login']);
        }else {
          this.fetchDomainDropDowns();
        }
  }

  fetchDomainDropDowns() {
    const getSignUpDropdownList =
      webApi.domain + webApi.url.getSignUpDropdownList;
    const param = {};
    // this.spinner.show();
    this.commonFunctionsService
      .httpPostRequest(getSignUpDropdownList, param)
      .then(
        (rescompData) => {
          this.spinner.hide();
          // console.log('rescompData ==>', rescompData);
          if (rescompData && rescompData['type'] === true) {
            // this.currencyData = rescompData['currency'];
            this.countryData = rescompData['country'];
          }
        },
        (error) => {
          this.spinner.hide();

          this.toastr.warning('Unable to fetch country dropdown.', 'Warning');
        }
      );
  }

  submit(form) {
    console.log('Unique Id : ==>', this.queryParams);
    if (form.invalid) {
      Object.keys(form.controls).forEach((key) => {
        form.controls[key].markAsTouched();
      });
      return null;
    }
    if (this.domainNameCheck === 'Inused') {
      this.toastr.warning('Domain name already taken', 'Warning');
    } else if (this.domainNameCheck === '') {
      this.checkIfDomainNameIsAvailable((error, response) => {
        if (this.domainNameCheck === 'available') {
          this.submitFormData();
        }
      });
    }else {
      this.submitFormData();
    }
  }

  checkIfDomainNameIsAvailable(cb) {
    this.domainNameCheck = '';
    if (this.domainMap.domainName && this.domainMap.domainName.length > 2) {
      const checkIfDomainNameIsAvailable =
        webApi.domain + webApi.url.checkIfDomainNameIsAvailable;
      const param = {
        subDomainName: this.domainMap.domainName,
      };
      this.commonFunctionsService
        .httpPostRequest(checkIfDomainNameIsAvailable, param)
        // this.serveyService.getServey(param)
        .then(
          (rescompData) => {
            // this.spinner.hide();


            // if(rescompData['data']){
            //   this.toastr.warning('Invalid verfication link', 'Warning');
            //   this.router.navigate(['../login']);
            //   return null;
            // }
            if (rescompData['type'] && rescompData['data'] !== null) {
              if (Number(rescompData['data']) === 1) {
                this.domainNameCheck = 'available';
                // this.toastr.success('Domain name is available', 'success');
              } else {
                this.domainNameCheck = 'Inused';
                // this.toastr.warning('Domain name already taken', 'warning');
              }
            } else {
              this.domainNameCheck = '';
              this.toastr.warning('Something went wrong', 'Warning');
            }
            cb(null, rescompData);
          },
          (error) => {
            cb(error, null);
            this.spinner.hide();
            this.toastr.warning('Something went wrong.', 'Warning');
          }
        );
    } else {
      this.spinner.hide();
      this.toastr.warning('Atleast 3 charecter are required', 'Warning');
    }
    // console.log('Search string =>', );
  }

  checkIfDomainName() {
    this.spinner.show();
    this.checkIfDomainNameIsAvailable(() => {
      this.spinner.hide();
    });
  }

  submitFormData() {
    const getSignUpDropdownList =
      webApi.domain + webApi.url.createSubDomain;
    const param = {
      // uniqueId: this.queryParams.uniqueId,
      uniqueId: this.uniqueId,
      country: this.domainMap.country[0].id,
      subDomainName: this.domainMap.domainName,
    };
    console.log('param', param);
    this.spinner.show();
    this.cdf.detectChanges();
    this.commonFunctionsService
    .httpPostRequest(getSignUpDropdownList, param)
    // this.serveyService.getServey(param)
    .then(
      (rescompData) => {
        this.spinner.hide();
        this.cdf.detectChanges();
          if(rescompData['type']){
            if(rescompData['alreadyExist']){
              this.toastr.warning('Current email is already associated with domain', 'Warning');
            }else {
              this.toastr.success(rescompData['message'], 'Success');
             this.router.navigate(['../'], { relativeTo: this.activatedRoute });
            }

          }else {
            this.toastr.warning('Unable to create subdomain', 'Warning');
          }
      },
      (error) => {
        this.spinner.hide();
        this.cdf.detectChanges();
        this.toastr.warning('Something went wrong.', 'Warning');
      }
    );
  }
}
