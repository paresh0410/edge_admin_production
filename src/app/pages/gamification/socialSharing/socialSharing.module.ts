import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { NgxEchartsModule } from 'ngx-echarts';
 import { ThemeModule } from '../../../@theme/theme.module';
import { socialSharing } from './socialSharing.component';
import { ReactiveFormsModule } from '@angular/forms';
import {socialSharingService } from './socialSharing.service';
// import { CourseBundleService } from './courseBundle/courseBundle.service';
// import { CourseBundle } from './courseBundle/courseBundle';
// import { ContentService } from './courses/content/content.service';
import { CommonModule } from '@angular/common';
import { SortablejsModule } from 'angular-sortablejs';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

// import { CourseBundleModule } from './courseBundle/courseBundle.module';


@NgModule({
  imports: [
     ThemeModule,
    NgxEchartsModule,
    CommonModule,
    SortablejsModule,
    ReactiveFormsModule,
    NgxDatatableModule
    // CourseBundleModule
  ],
  declarations: [
    socialSharing,
    // CourseBundle
  ],
  providers: [
    socialSharingService,
    // CourseBundleService,
    // ContentService
  ],
  schemas: [ 
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA 
  ]
})
export class socialSharingModule { }
