import {Injectable, Inject} from '@angular/core';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppConfig} from '../../../app.module';
import { webApi } from '../../../service/webApi';
import { HttpClient } from "@angular/common/http";

@Injectable()
export class CertificateService {

  private _urlFetch:string = webApi.domain + webApi.url.getallcertificates;
  private _urlUpdateStatus = webApi.domain + webApi.url.changecertificatestatus;
  private _urlfetchdropdownscourses: string = webApi.domain + webApi.url.fetchddcoursedisplay;

  constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private _http: Http,private _httpClient:HttpClient){

  }

  getCertificates(param){
    // let url:any = this._urlFetch;
    // return this._http.post(url,param)
    //     .map((response:Response)=>response.json())
    //     .catch(this._errorHandler);
    return new Promise(resolve => {
      this._httpClient.post(this._urlFetch, param)
      //.map(res => res.json())
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}

updateCertStatus(param){
    // let url:any = this._urlUpdateStatus;
    // return this._http.post(url,param)
    //     .map((response:Response)=>response.json())
    //     .catch(this._errorHandler);
    return new Promise(resolve => {
      this._httpClient.post(this._urlUpdateStatus, param)
      //.map(res => res.json())
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}

fetchdropdownsCourses(param) {
  // let url: any = this._urlfetchdropdownscourses;
  // return this._http.post(url, param)
  //   .map((response: Response) => response.json())
  //   .catch(this._errorHandler);

    return new Promise(resolve => {
      this._httpClient.post(this._urlfetchdropdownscourses, param)
      //.map(res => res.json())
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}


_errorHandler(error: Response){
  console.error(error);
  return Observable.throw(error || "Server Error")
}


}
