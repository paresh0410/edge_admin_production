import { Component, OnInit ,ElementRef, ViewChild} from '@angular/core';
import { CertificateService } from './certificate.service';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, NavigationStart, ActivatedRoute } from '@angular/router';
import { AddEditCertificateService } from './add-edit-certificate/add-edit-certificate.service';
import { ToastrService } from 'ngx-toastr';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { webApi } from '../../../service/webApi';
import { Card } from '../../../models/card.model';
import { SuubHeader } from '../../components/models/subheader.model';
import { noData } from '../../../models/no-data.model';

@Component({
  selector: 'ngx-certificate',
  templateUrl: './certificate.component.html',
  styleUrls: ['./certificate.component.scss']
})
export class CertificateComponent implements OnInit {
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No Certificates at this time.",
    desc:"Certificates will appear after they are added by the instructor. Certificates are rewarded to learners on successfully completing a course",
    titleShow:true,
    btnShow:true,
    descShow:true,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/EAk2ERNFQi8HU8SO',
  }
  cardModify: Card =   {
    flag: 'certificates',
    titleProp : 'certName',
    discrption: 'description',
    image: 'certIcon',
    hoverlable:   true,
    showImage: true,
    option:   true,
    eyeIcon:   true,
    bottomDiv:   true,
    bottomTitle:   true,
    bottomDiscription:   true,
    btnLabel: 'Edit',
    defaultImage: 'assets/images/courseicon.jpg'
  };
  count:number=8
  header: SuubHeader  = {
    title:'Certificate',
    btnsSearch: true,
    placeHolder:'Search by certificate name',
    searchBar: true,
    dropdownlabel: ' ',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: '',
    btnName3: '',
    btnAdd: 'Add Certificate',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: true,
    filter: false,
    showBreadcrumb: true,
    breadCrumbList:[
      {
        'name': 'Gamification',
        'navigationPath': '/pages/gamification',
      },]
  };
  Nodatafound: boolean = false;
  nocertificates: boolean = false;
  certificates: any = [];
  enableDisableCertModal: boolean = false;
  enableDisableCertData: any;
  enableCert: boolean = false;
  certDisableIndex: any;
  tenantId;
  userData;
  skeleton = false;
  searchtext: any;
  rows: any;
  btnName: string='Yes';
  title: string;
  constructor(private certificateService: CertificateService,
    // private toasterService: ToasterService, 
    private spinner: NgxSpinnerService,
    private router: Router, private addeditcertservice: AddEditCertificateService, private toastr: ToastrService,
    private commonFunctionService: CommonFunctionsService,) {
    if (localStorage.getItem('LoginResData')) {
      this.userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', this.userData.data);
      // this.userId = this.userData.data.data.id;
      this.tenantId = this.userData.data.data.tenantId;
    }
    this.fetchCertificates();
    this.fetchdropdowns();

  }

  // ngOnInit() {
  // }

  back() {
    // window.history.back();
    this.router.navigate(['/pages/gamification']);
  }

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }

  fetchCertificates() {
    this.spinner.show();
    let param = {
      'tenantId': this.tenantId,
      'courseId': 0
    }
    const _urlFetch:string = webApi.domain + webApi.url.getallcertificates;
    this.commonFunctionService.httpPostRequest(_urlFetch,param)
    //this.certificateService.getCertificates(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('getBadgeResponse:', rescompData);
        if (result['type'] == true) {
          if (result['data'][0].length == 0) {
            this.skeleton = true;
            this.nocertificates = true;
          } else {
            this.rows = result['data'][0];
            this.certificates = this.rows;
            this.displayArray =[]
            this.addItems(0,this.sum,'push',this.certificates);
            this.nocertificates = false;
            this.skeleton = true;
          }
          console.log('this.certificates', this.certificates);
        } else {
          this.skeleton = true;
          this.Nodatafound = true;
          // var toast: Toast = {
          //   type: 'error',
          //   //title: 'Server Error!',
          //   body: 'Something went wrong.please try again later.',
          //   showCloseButton: true,
          //   timeout: 4000,
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }
        this.skeleton = true;

      }, error => {
        // this.spinner.hide();
        this.skeleton = true;
        this.Nodatafound = true;
        // var toast: Toast = {
        //   type: 'error',
        //   //title: 'Server Error!',
        //   body: 'Something went wrong.please try again later.',
        //   showCloseButton: true,
        //   timeout: 4000,
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
  }

  disableCertificate(currentIndex, certData, status) {
    this.enableDisableCertData = certData;
    console.log('this.enableDisableCertData', this.enableDisableCertData)
    this.enableDisableCertModal = true;
    this.certDisableIndex = currentIndex;

    if (this.enableDisableCertData.visible == 0) {
      this.enableCert = false;
    } else {
      this.enableCert = true;
    }

  }

  clickTodisable(certData) {
   

    if (certData.visible == 1) {
      this.title='Disable Certificate'
      this.enableCert = true;
      this.enableDisableCertData = certData;
      console.log('this.enableDisableCertData', this.enableDisableCertData);
      this.enableDisableCertModal = true;
    } else {
      this.title='Enable Certificate'
      this.enableCert = false;
      this.enableDisableCertData = certData;
      console.log('this.enableDisableCertData', this.enableDisableCertData);
      this.enableDisableCertModal = true;
    }
  }


  enableDisableCategoryAction(actionType) {
    if (actionType == true) {
      if (this.enableDisableCertData.visible == 1) {
        this.enableDisableCertData.cvisible = 0;
        var certData = this.enableDisableCertData;
        this.enableDisableCert(certData);

      } else {
        this.enableDisableCertData.cvisible = 1;
        var certData = this.enableDisableCertData;
        this.enableDisableCert(certData);

      }
      this.closeEnableDisableCertModal();
    } else {
      this.closeEnableDisableCertModal();
    }
  }

  closeEnableDisableCertModal() {
    this.enableDisableCertModal = false;
  }

  enableDisableCert(certData) {
    this.spinner.show();
    console.log('certData', certData);
    let param = {
      'certId': certData.id,
      'visible': certData.cvisible,

    }
    const _urlUpdateStatus = webApi.domain + webApi.url.changecertificatestatus;
    this.commonFunctionService.httpPostRequest(_urlUpdateStatus,param)
    //this.certificateService.updateCertStatus(param)

      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('UpdateCertResponse:', rescompData);
        if (result['type'] == true) {
          // var certUpdate: Toast = {
          //   type: 'success',
          //   title: 'Certificate',
          //   body: 'Certificate has been updated successfully.',
          //   showCloseButton: true,
          //   timeout: 4000,
          // };
          // this.toasterService.pop(certUpdate);
          this.presentToast('success', 'Certificate updated');
          this.enableDisableCertData.visible = certData.cvisible;
        } else {
          // var certUpdate: Toast = {
          //   type: 'error',
          //   title: 'Certificate',
          //   body: 'Unable to update certificate.',
          //   showCloseButton: true,
          //   timeout: 4000,
          // };
          // this.toasterService.pop(certUpdate);
          this.presentToast('error', '');
        }
      }, error => {
        this.spinner.hide();
        // var toast: Toast = {
        //   type: 'error',
        //   //title: 'Server Error!',
        //   body: 'Something went wrong.please try again later.',
        //   showCloseButton: true,
        //   timeout: 4000,
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
  }

  gotoaddedit(data, value) {
    if (value == 0) {
      var action = 'ADD';
      this.addeditcertservice.addEditData[0] = action;
      this.addeditcertservice.addEditData[1] = '';
      this.addeditcertservice.addEditData[2] = this.courses;
      this.router.navigate(['/pages/gamification/certificate/add-edit-certificate']);
    } else if (value == 1) {
      console.log('data', data);
      var action = 'EDIT';
      this.addeditcertservice.addEditData[0] = action;
      this.addeditcertservice.addEditData[1] = data;
      this.addeditcertservice.addEditData[2] = this.courses;
      this.router.navigate(['/pages/gamification/certificate/add-edit-certificate']);
    }
  }

  gotoCardaddedit(data) {
    console.log('data', data);
      var action = 'EDIT';
      this.addeditcertservice.addEditData[0] = action;
      this.addeditcertservice.addEditData[1] = data;
      this.addeditcertservice.addEditData[2] = this.courses;
      this.router.navigate(['/pages/gamification/certificate/add-edit-certificate']);
  }

  dropdisplay: any = [];
  dropcourse: any = [];
  display: any = [];
  courses: any = [];
  fetchdropdowns() {
    let param = {
      'tenantId': this.tenantId,
      'courseId': 0
    }
    const _urlfetchdropdownscourses: string = webApi.domain + webApi.url.fetchddcoursedisplay;
    this.commonFunctionService.httpPostRequest(_urlfetchdropdownscourses,param)
    //this.certificateService.fetchdropdownsCourses(param)
      .then(rescompData => {

        var result = rescompData;
        console.log('getCourseDrop:', rescompData);
        if (result['type'] == true) {
          this.dropdisplay = result['data'][0];

          this.dropdisplay.forEach(element => {
            this.display.push(element);
          });
          console.log('this.display', this.display);


          this.dropcourse = result['data'][1];
          for (let i = 0; i < this.dropcourse.length; i++) {
            var tempObj = {
              id: this.dropcourse[i].courseId,
              itemName: this.dropcourse[i].fullname
            }
            this.courses.push(tempObj);
          }
          console.log('this.display', this.display);
          console.log('this.courses', this.courses);
        } else {
          // var toast: Toast = {
          //   type: 'error',
          //   //title: 'Server Error!',
          //   body: 'Something went wrong.please try again later.',
          //   showCloseButton: true,
          //   timeout: 4000,
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }


      }, error => {
        //   var toast : Toast = {
        //     type: 'error',
        //     //title: 'Server Error!',
        //     body: 'Something went wrong.please try again later.',
        //     showCloseButton: true,
        //     timeout: 4000
        // };
        // this.toasterService.pop(toast);
      });
  }
  // SarchFilter(event, searchtext) {
  //   searchtext= event.target.value;
  //   console.log(searchtext);
  //   const val = searchtext.toLowerCase();
  //   const tempcc = this.rows.filter(function (d) {
  //     return String(d.certName).toLowerCase().indexOf(val) !== -1 ||
  //       d.description.toLowerCase().indexOf(val) !== -1 ||
  //       // d.relayDate.toLowerCase().indexOf(val) !== -1 ||
  //       // d.manager.toLowerCase().indexOf(val) !== -1 ||
  //       !val
  //   })
  //   console.log(tempcc);
  //   this.certificates = tempcc;
  //   if (!this.certificates.length) {
  //     this.nocertificates = true;
  //   } else {
  //     this.nocertificates = false;
  //   }
  // }
  clear() {
    if(this.header.searchtext){
    this.Nodatafound = false
    // this.searchtext = "";
    this.header.searchtext = '';
    this.searchtext = this.header.searchtext;
    this.fetchCertificates();
    }
  }
  ngOnDestroy(){
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0){
      e1[0].classList.add('with-scroll');
    }
  }
  ngAfterContentChecked() {
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0)
    {
      e1[0].classList.remove('with-scroll');
    }

   }
   conentHeight = '0px';
   offset: number = 100;
   ngOnInit() {
     this.conentHeight = '0px';
     const e = document.getElementsByTagName('nb-layout-column');
     console.log(e[0].clientHeight);
     // this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
     if (e[0].clientHeight > 1300) {
       this.conentHeight = '0px';
       this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
     } else {
       this.conentHeight = String(e[0].clientHeight) + 'px';
     }
   }
 
   sum = 40;
   addItems(startIndex, endIndex, _method, asset) {
     for (let i = startIndex; i < endIndex; ++i) {
       if (asset[i]) {
         this.displayArray[_method](asset[i]);
       } else {
         break;
       }
 
       // console.log("NIKHIL");
     }
   }
   // sum = 10;
   onScroll(event) {
 
     let element = this.myScrollContainer.nativeElement;
     // element.style.height = '500px';
     // element.style.height = '500px';
     // Math.ceil(element.scrollHeight - element.scrollTop) === element.clientHeight
 
     if (element.scrollHeight - element.scrollTop - element.clientHeight < 5) {
       if (this.newArray.length > 0) {
         if (this.newArray.length >= this.sum) {
           const start = this.sum;
           this.sum += 50;
           this.addItems(start, this.sum, 'push', this.newArray);
         } else if ((this.newArray.length - this.sum) > 0) {
           const start = this.sum;
           this.sum += this.newArray.length - this.sum;
           this.addItems(start, this.sum, 'push', this.newArray);
         }
       } else {
         if (this.certificates.length >= this.sum) {
           const start = this.sum;
           this.sum += 50;
           this.addItems(start, this.sum, 'push', this.certificates);
         } else if ((this.certificates.length - this.sum) > 0) {
           const start = this.sum;
           this.sum += this.certificates.length - this.sum;
           this.addItems(start, this.sum, 'push', this.certificates);
         }
       }
 
 
       console.log('Reached End');
     }
   }
   newArray =[];
   displayArray =[];
    // this function is use to dynamicsearch on table
  searchData(event) {
    var temData = this.certificates;
    var val = event.target.value.toLowerCase();
    var keys = [];
    if(event.target.value == '')
    {
      this.Nodatafound = false
      this.displayArray =[];
      this.newArray = [];
      this.sum = 50;
      this.addItems(0, this.sum, 'push', this.certificates);
    }else if(val.length>=3){
         // filter our data
    // if (temData.length > 0) {
    //   keys = Object.keys(temData[0]);
    // }
    // var temp = temData.filter((d) => {
    //   for (const key of keys) {
    //     if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
    //       return true;
    //     }
    //   }
    // });
    const temp = this.certificates.filter(function (d) {
      return String(d.certName).toLowerCase().indexOf(val) !== -1 ||
        d.description.toLowerCase().indexOf(val) !== -1 ||
        // d.relayDate.toLowerCase().indexOf(val) !== -1 ||
        // d.manager.toLowerCase().indexOf(val) !== -1 ||
        !val
    })
    // update the rows
    this.displayArray =[];
    this.newArray = temp;
    this.sum = 50;
    this.addItems(0, this.sum, 'push', this.newArray);
  }
  if(this.displayArray.length == 0){
    this.Nodatafound = true
    this.noDataVal={
      margin:'mt-5',
      imageSrc: '../../../../../assets/images/no-data-bg.svg',
      title:"Sorry we couldn't find any matches please try again",
      desc:".",
      titleShow:true,
      btnShow:false,
      descShow:false,
      btnText:'Learn More',
      btnLink:''
    }
  }
  else{
    this.Nodatafound = false
  }
}
}
