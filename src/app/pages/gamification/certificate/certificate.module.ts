import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { NgxEchartsModule } from 'ngx-echarts';
 import { ThemeModule } from '../../../@theme/theme.module';
import { CertificateComponent } from './certificate.component';
import { CertificateService } from './certificate.service'
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { SortablejsModule } from 'angular-sortablejs';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AddEditCertificateComponent } from './add-edit-certificate/add-edit-certificate.component';
import { AddEditCertificateService } from './add-edit-certificate/add-edit-certificate.service';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { JoditAngularModule } from 'jodit-angular';
import { QuillModule } from 'ngx-quill'
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgxContentLoadingModule } from 'ngx-content-loading';
import { ComponentModule } from '../../../component/component.module';

// import { CourseBundleModule } from './courseBundle/courseBundle.module';


@NgModule({
  imports: [
     ThemeModule,
    NgxEchartsModule,
    CommonModule,
    ComponentModule,
    SortablejsModule,
    ReactiveFormsModule,
    AngularMultiSelectModule,
    QuillModule,
    JoditAngularModule,
    NgxSkeletonLoaderModule,
    NgxContentLoadingModule
  ],
  declarations: [
    CertificateComponent,
    AddEditCertificateComponent
  ],
  providers: [
    CertificateService,
    AddEditCertificateService
  ],
  schemas: [ 
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA 
  ]
})
export class CertificateModule { }
