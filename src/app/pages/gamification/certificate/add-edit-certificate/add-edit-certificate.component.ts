import { Component, OnInit } from '@angular/core';
import { AddEditCertificateService } from './add-edit-certificate.service';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { Router, NavigationStart, ActivatedRoute } from '@angular/router';
import { webAPIService } from '../../../../service/webAPIService'
import { webApi } from '../../../../service/webApi';
import { NgxSpinnerService } from 'ngx-spinner';
import { DomSanitizer } from '@angular/platform-browser';
import { FormGroup, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { CommonFunctionsService } from '../../../../service/common-functions.service';
import { SuubHeader } from '../../../components/models/subheader.model';

@Component({
  selector: 'ngx-add-edit-certificate',
  templateUrl: './add-edit-certificate.component.html',
  styleUrls: ['./add-edit-certificate.component.scss']
})
export class AddEditCertificateComponent implements OnInit {
  form: FormGroup;
  config = {
    height: '500px',
    uploader: {
      insertImageAsBase64URI: true
    }
  };
  data: any;
  editorDisabled = false;
  certData: any = {}
  getDataForAddEdit: any = [];
  dropdownListCourses: any = [];
  dropdownSettingsDept = {};
  selectedItems = [];
  selectedItemsCert: any = [];
  userId: any;
  tId;
  addAction: boolean = false;
  header: SuubHeader  = {
    title:'',
    btnsSearch: true,
    btnAdd: 'Save',
    btnBackshow: true,
    btnAddshow: true,
    showBreadcrumb: true,
  };
  certificateData: any;

  get sanitizedHtml() {
    return this.sanitizer.bypassSecurityTrustHtml(this.form.get('html').value);
  }
  constructor(private addeditcertservice: AddEditCertificateService, 
    // private toasterService: ToasterService, 
    private router: Router, protected webApiService: webAPIService, private spinner: NgxSpinnerService, 
    private sanitizer: DomSanitizer, private http1: HttpClient,  private toastr: ToastrService,private commonFunctionService: CommonFunctionsService) {
    this.getHelpContent();

    //     var editor = new Jodit('#editor', {
    //   'uploader': {
    //     'insertImageAsBase64URI': true
    //   }
    // });
    if (localStorage.getItem('LoginResData')) {
      var userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', userData.data);
      this.userId = userData.data.data.id;
      this.tId = userData.data.data.tenantId;
    }
    this.selectedItems = [];
    this.selectedItemsCert = [];
    this.dropdownSettingsDept = {
      singleSelection: false,
      text: 'Select Courses',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      // badgeShowLimit: 2,
      classes: 'myclass custom-class'
    };
    this.form = new FormGroup({
      html: new FormControl()
    });
    console.log('this.addeditcertservice.addEditData', this.addeditcertservice.addEditData);
    this.getDataForAddEdit = this.addeditcertservice.addEditData;
    this.certificateData = this.getDataForAddEdit[1];
    this.dropdownListCourses = this.getDataForAddEdit[2];


    if (this.getDataForAddEdit[0] == 'ADD') {
      this.addAction = true;
      this.certData = {
        id: '',
        name: '',
        desc: '',
        content: '',
        certicon: '',
        courses: [],
      }

    } else if (this.getDataForAddEdit[0] == 'EDIT') {
      this.addAction = false;
      if (this.getDataForAddEdit[1].cIds == null) {
        this.certData = {
          id: this.getDataForAddEdit[1].id,
          name: this.getDataForAddEdit[1].certName,
          desc: this.getDataForAddEdit[1].description,
          content: this.getDataForAddEdit[1].certContent,
          certicon: this.getDataForAddEdit[1].certIcon,
          courses: this.getDataForAddEdit[1].courses,
        }
      } else {
        var courseArr = this.getDataForAddEdit[1].cIds.split(',');
        for (let i = 0; i < this.dropdownListCourses.length; i++) {
          for (let j = 0; j < courseArr.length; j++) {
            if (courseArr[j] == this.dropdownListCourses[i].id) {
              this.selectedItemsCert.push(this.dropdownListCourses[i]);
            }
          }

        }
        this.certData = {
          id: this.getDataForAddEdit[1].id,
          name: this.getDataForAddEdit[1].certName,
          desc: this.getDataForAddEdit[1].description,
          content: this.getDataForAddEdit[1].certContent,
          certicon: this.getDataForAddEdit[1].certIcon,
          courses: this.selectedItemsCert
        }
      }


    }
  }

  ngOnInit() {
    this.header.title=this.certificateData!=""?this.certificateData.certName:"Add Certificate"
    this.header.breadCrumbList=[
      {
        'name': 'Gamification',
        'navigationPath': '/pages/gamification',
      },
      {
        'name': 'Certificate',
        'navigationPath': '/pages/gamification/certificate',
      },]

  }

  presentToast(type, body) {
    if(type === 'success'){
      this.toastr.success(body, 'Success', {
        closeButton: false
       });
    } else if(type === 'error'){
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else{
      this.toastr.warning(body, 'Warning', {
        closeButton: false
        })
    }
  }

  certImgData: any;
  readCertThumb(event: any) {
    var validExts = new Array('.png', '.jpg', '.jpeg');
    var fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
      // var toast: Toast = {
      //   type: 'error',
      //   title: 'Invalid file selected!',
      //   body: 'Valid files are of ' + validExts.toString() + ' types.',
      //   showCloseButton: true,
      //   timeout: 4000,
      // };
      // this.toasterService.pop(toast);
      this.presentToast('warning', 'Valid file types are ' + validExts.toString());
    } else {
      if (event.target.files && event.target.files[0]) {
        this.certImgData = event.target.files[0];

        var reader = new FileReader();

        reader.onload = (event: ProgressEvent) => {
          this.certData.certicon = (<FileReader>event.target).result;
        }
        reader.readAsDataURL(event.target.files[0]);
      }
    }
  }


  deleteCategoryThumb() {
    this.certData.certicon = 'assets/images/category.jpg';
    this.certImgData = undefined;
  }

  ondeptSelect(item: any) {
    console.log(item);
  }
  OndeptDeSelect(item: any) {
    console.log(item)
  }
  ondeptSelectAll(items: any) {
    console.log(items);

  }
  ondeptDeSelectAll(items: any) {
    console.log(items);

  }

  back() {
    // this.router.navigate(['pages/gamification/certificate']);
    window.history.back();
  }

  onEditorBlured(quill) {
    console.log('editor blur!', quill);
  }

  onEditorFocused(quill) {
    console.log('editor focus!', quill);
  }

  onEditorCreated(quill) {
    console.log('quill is ready! this is current quill instance object', quill);
  }

  onContentChanged({ quill, html, text }) {
    console.log('quill content is changed!', quill, html, text);
  }

  passparams: any;
  fileUploadRes: any;
  certAddEditRes: any;
  errorMsg: any;
  fileres: any;
  submit(certData, f) {
    if (f.valid) {
      this.spinner.show();
      //this.makeCourseDataReady();
      console.log('certData', certData);
      if (this.getDataForAddEdit[0] == 'ADD') {
        let param = {
          'cId': 0,
          'cname': certData.name,
          'cIcon': '',
          'cDescription': certData.desc,
          'cContent': certData.content,
          'cuserId': this.userId,
          // 'dispname':this.formattedCourse,
          'ctId': this.tId,
        }
        this.passparams = param;
      } else if (this.getDataForAddEdit[0] == 'EDIT') {
        let param = {
          'cId': certData.id,
          'cname': certData.name,
          'cIcon': certData.certicon?certData.certicon:null,
          // 'cIcon': '',
          'cDescription': certData.desc,
          'cContent': certData.content,
          'cuserId': this.userId,
          // 'dispname':this.formattedCourse,
          'ctId': this.tId,
        }
        this.passparams = param;
      }

      var fd = new FormData();
      fd.append('content', JSON.stringify(this.passparams));
      fd.append('file', this.certImgData);
      console.log('File Data ', fd);

      console.log('Course Data Img', this.certImgData);
      console.log('Course Data ', this.passparams);
      let fileUploadUrl = webApi.domain + webApi.url.fileUpload;

      if (this.certImgData) {
        this.webApiService.getService(fileUploadUrl, fd)
          .then(rescompData => {
            this.spinner.hide();
            var temp: any = rescompData;
            this.fileUploadRes = temp;
            this.certImgData = null;
            if (temp === 'err') {
              // this.notFound = true;
              // var thumbUpload: Toast = {
              //   type: 'error',
              //   title: 'Certificate',
              //   body: 'Unable to upload certificate thumbnail.',
              //   // body: temp.msg,
              //   showCloseButton: true,
              //   timeout: 4000,
              // };
              this.spinner.hide();
              // this.toasterService.pop(thumbUpload);
          this.presentToast('error', '');
            } else if (temp.type === false) {
              // var thumbUpload: Toast = {
              //   type: 'error',
              //   title: 'Certificate',
              //   body: 'Unable to upload certificate thumbnail.',
              //   // body: temp.msg,
              //   showCloseButton: true,
              //   timeout: 4000,
              // };
              this.spinner.hide();
              // this.toasterService.pop(thumbUpload);
          this.presentToast('error', '');
            } else {
              if (this.fileUploadRes.data != null || this.fileUploadRes.fileError != true) {
                this.passparams.cIcon = this.fileUploadRes.data.file_url;
                console.log('this.passparams.iconRef', this.passparams.iconRef);
                this.spinner.hide();
                this.addEditCertificate(this.passparams);
              } else {
                // var thumbUpload: Toast = {
                //   type: 'error',
                //   title: 'Certificate',
                //   // body: 'Unable to upload course thumbnail.',
                //   body: this.fileUploadRes.status,
                //   showCloseButton: true,
                //   timeout: 4000,
                // };
                this.spinner.hide();
                // this.toasterService.pop(thumbUpload);
          this.presentToast('error', '');
              }
            }
            console.log('File Upload Result', this.fileUploadRes);
            var res = this.fileUploadRes;
            this.fileres = res.data.file_url;
          },
            resUserError => {
              //this.loader =false;
              this.spinner.hide();
              this.errorMsg = resUserError;
            });
      } else {
        // this.passparams.iconRef = this.badge.ico;
        this.spinner.hide();
        this.addEditCertificate(this.passparams);
      }
    } else {
      console.log('Please Fill all fields');
      // const addEditF: Toast = {
      //   type: 'error',
      //   title: 'Unable to update',
      //   body: 'Please Fill all fields',
      //   showCloseButton: true,
      //   timeout: 4000,
      // };
      // this.toasterService.pop(addEditF);
      this.presentToast('warning', 'Please fill in the required fields');
      Object.keys(f.controls).forEach(key => {
        f.controls[key].markAsDirty();
      });
    }

  }


  addEditCertificate(params) {
    this.spinner.show();
    const _urlAddEdit: string = webApi.domain + webApi.url.addeditcertificate;
    this.commonFunctionService.httpPostRequest(_urlAddEdit,params)
    // this.addeditcertservice.addEditCertificates(params)
      .then(rescompData => {
        //this.loader =false;
        this.spinner.hide();
        var temp = rescompData;
        this.certAddEditRes = temp['data'];
        if (this.getDataForAddEdit[0] == 'ADD') {
          if (temp['type'] == true) {
            console.log('Certificate Add Result ', this.certAddEditRes)
            // var certUpdate: Toast = {
            //   type: 'success',
            //   title: 'Certificate',
            //   body: 'New certificate has been added successfully.',
            //   showCloseButton: true,
            //   timeout: 4000,
            // };
            // this.toasterService.pop(certUpdate);
          this.presentToast('success', 'New certificate added');
          } else {
            // var certUpdate: Toast = {
            //   type: 'error',
            //   title: 'Certificate',
            //   body: 'Unable to add certificate.',
            //   showCloseButton: true,
            //   timeout: 4000,
            // };
            // this.toasterService.pop(certUpdate);
          this.presentToast('error', '');
          }
          this.spinner.hide();
          this.router.navigate(['/pages/gamification/certificate']);
        } else if (this.getDataForAddEdit[0] == 'EDIT') {
          console.log('Certificate Edit Result ', this.certAddEditRes)
          if (temp['type'] == true) {
            // var certUpdate: Toast = {
            //   type: 'success',
            //   title: 'Certificate',
            //   body: 'Certificate has been updated successfully.',
            //   showCloseButton: true,
            //   timeout: 4000,
            // };
            // this.toasterService.pop(certUpdate);
          this.presentToast('success', 'Certificate updated');
          } else {
            // var certUpdate: Toast = {
            //   type: 'error',
            //   title: 'Certificate',
            //   body: 'Unable to update certificate.',
            //   showCloseButton: true,
            //   timeout: 4000,
            // };
            // this.toasterService.pop(certUpdate);
          this.presentToast('error', '');
          }
          this.spinner.hide();
          this.router.navigate(['/pages/gamification/certificate'])
        }

      },
        resUserError => {
          // this.loader =false;
          this.errorMsg = resUserError;
          this.spinner.hide();
          this.router.navigate(['/pages/gamification/certificate'])
        });
  }

  enableEditor() {
    this.editorDisabled = false;
  }

  disableEditor() {
    this.editorDisabled = true;
  }

  onBlur() {
    console.log('Blur');
  }
  // formattedCourse:any;
  //   makeCourseDataReady(){
  //     var courseData = this.certData.courses;
  //     if(courseData){
  //       if(courseData.length > 0){
  //         var courseString = '';
  //         for(let i=0; i < courseData.length; i++){
  //           var course = courseData[i];
  //           if(courseString != ''){
  //             courseString += '|';
  //           }
  //           if(course.id){
  //             if(String(course.id) != '' && String(course.id) != 'null'){
  //               courseString += course.id;
  //             } 
  //           }else{
  //             if(String(course) != '' && String(course) != 'null'){
  //               courseString += course;
  //             } 
  //           }  
  //         }
  //         this.formattedCourse = courseString;
  //       }
  //     }

  //   }

  // Help Code Start Here //

  helpContent: any;
  getHelpContent() {
    return new Promise(resolve => {
      this.http1
        .get('../../../../../../assets/help-content/addEditCourseContent.json')
        .subscribe(
          data => {
            this.helpContent = data;
            console.log('Help Array', this.helpContent);
          },
          err => {
            resolve('err');
          }
        );
    });
    // return this.helpContent;
  }

  // Help Code Ends Here //

}
