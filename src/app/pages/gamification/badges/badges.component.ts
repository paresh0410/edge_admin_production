import { Component, ViewEncapsulation, ChangeDetectorRef ,ElementRef, ViewChild } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators/takeWhile';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute, Routes } from '@angular/router';
import { FormGroup, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';
import { BadgesService } from './badges.service';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { NgxSpinnerService } from 'ngx-spinner';
import { webAPIService } from '../../../service/webAPIService'
import { webApi } from '../../../service/webApi'
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { Card } from '../../../models/card.model';
import { SuubHeader } from '../../components/models/subheader.model';
import { noData } from '../../../models/no-data.model';

@Component({
  selector: 'ngx-badges',
  styleUrls: ['./badges.component.scss'],
  templateUrl: './badges.component.html',
  encapsulation: ViewEncapsulation.None
})
export class Badges {
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  cardModify: Card =   {
    flag: 'badges',
    titleProp : 'badgeName',
    discrption: 'description',
    image: 'badgeIcon',
    hoverlable:   true,
    showImage: true,
    option:   true,
    eyeIcon:   true,
    bottomDiv:   true,
    bottomTitle:   true,
    bottomDiscription:   true,
    btnLabel: 'Edit',
    defaultImage: 'assets/images/badgeNew.png'
  };
  count:number=8
  badge: any = {
    name: '',
    desc: '',
    ico: '',
    tags: '',
    visible: ''
  }
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No badges added under a category.",
    desc:"",
    titleShow:true,
    btnShow:true,
    descShow:false,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/EAk2ERNFQi8HU8SO',
  }
  header: SuubHeader  = {
    title:'',
    btnsSearch: true,
    searchBar: true,
    placeHolder:"Search by badge name",
    btnAdd: 'Add Badges',
    btnBackshow: true,
    btnAddshow: true,
    showBreadcrumb: true,
    breadCrumbList:[
      {  'name': 'Gamification',
      'navigationPath': '/pages/gamification',},
      {
        'name': 'Badges Category',
        'navigationPath': '/pages/gamification/badges_category',
      },]
  };

  rows: any[];
  selected = [];
  addPopup: boolean =false;
  img: boolean = false;
  Add: boolean = false;
  Nodatafound: boolean = false;

  courseIconUrl: any = 'assets/images/profile1.png';

  columns = [
    { prop: 'name', name: 'Name' },
    { prop: 'category', name: 'Category' },
    { prop: 'desc', name: 'Description' },
    { prop: 'ico', name: 'Icon' },
    { prop: 'tags', name: 'Tags' }
  ];

  items = ['Javascript', 'Typescript'];
  tags = ['@item'];
  badgeTitle:string='Add Badge'
	btnName: string = 'Save';
  enableDisableCategoryData: any;
  enableCat: boolean = false;
  catDisableIndex: any;
  loader: any;
  enableDisableBadgeModal: boolean = false;
  fileexterror: boolean = false;
  userId: any;
  nobadges: boolean = false;
  catId: any;
  tenantId;
  catName: any
  skeleton = false;
  settingsTagDrop = {};
  tempTags: any = [];
  tagList: any = [];
  selectedTags: any = [];
  show: boolean = false;
  searchtext: string;
  badgelist: any;
  title: string;
  fromSearch: boolean=false;
  constructor(public router: Router, private bagdeservice: BadgesService, 
    // private toasterService: ToasterService,
    protected webApiService: webAPIService, private spinner: NgxSpinnerService, private http1: HttpClient,
    private cdf: ChangeDetectorRef, private toastr: ToastrService, private commonFunctionService: CommonFunctionsService ) {
    this.getHelpContent();
    this.spinner.show();
    if (this.bagdeservice.categoryId && this.bagdeservice.catName) {
      this.catId = this.bagdeservice.categoryId;
      this.catName = this.bagdeservice.catName;
    }
    if (localStorage.getItem('LoginResData')) {
      var userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', userData.data);
      this.userId = userData.data.data.id;
      this.tenantId = userData.data.data.tenantId;
    }
    this.settingsTagDrop = {
      text: 'Select Tags',
      singleSelection: false,
      classes: 'common-multi',
      primaryKey: 'id',
      labelKey: 'name',
      noDataLabel: 'Search Tags...',
      enableSearchFilter: true,
      searchBy: ['name'],
      maxHeight:250,
      lazyLoading: true,
    };


    this.fetchBadges();
    this.getallTagList();

  }
  clear() {
    // this.searchtext = "";
    this.Nodatafound=false
    this.header.searchtext = '';
    this.searchtext = this.header.searchtext;
    this.fetchBadges();
  }

  getallTagList() {
    this.spinner.show();
    let url = webApi.domain + webApi.url.getAllTagsComan;
    let param = {
      tenantId: this.tenantId
    };
    this.webApiService.getService(url, param)
      .then(rescompData => {
        // this.loader =false;
        this.spinner.hide();
        var temp: any = rescompData;
        this.tagList = temp.data;
        this.tempTags = [... this.tagList];
        // console.log('Visibility Dropdown',this.visibility);
      },
        resUserError => {
          // this.loader =false;
          this.show = false;
          this.spinner.hide();
          this.errorMsg = resUserError;
        });
  }


  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }

  fetchBadges() {
    let param = {
      'courseId': 0,
      'tenantId': this.tenantId,
      'catId': this.catId
    }
    const _urlFetch:string = webApi.domain + webApi.url.fetchBadges;
    this.commonFunctionService.httpPostRequest(_urlFetch,param)
    // this.bagdeservice.getBadges(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('getBadgeResponse:', rescompData);
        if (result['type'] == true) {
          if (result['data'][0].length == 0) {
            this.Nodatafound = true;
            this.noDataVal={
              margin:'mt-3',
              imageSrc: '../../../../../assets/images/no-data-bg.svg',
              title:"No badges added under a category.",
              desc:"",
              titleShow:true,
              btnShow:true,
              descShow:false,
              btnText:'Learn More',
              btnLink:'https://faq.edgelearning.co.in/kb/EAk2ERNFQi8HU8SO',
            }
          } else {
            this.rows = result['data'][0];
            this.badgelist = this.rows;
            this.displayArray = []
            this.addItems(0,this.sum,'push',this.badgelist)
            console.log('this.rows', this.rows);
            this.Nodatafound = false;
            // this.Nodatafound = false
            this.cdf.detectChanges();
          }
          this.skeleton = true;
        } else {
          this.Nodatafound = true;
          this.skeleton = true;
          // var toast: Toast = {
          //   type: 'error',
          //   //title: 'Server Error!',
          //   body: 'Something went wrong.please try again later.',
          //   showCloseButton: true,
          //   timeout: 4000,
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }


      }, error => {
        this.spinner.hide();
        this.skeleton = true;
        this.Nodatafound = true;
        // var toast: Toast = {
        //   type: 'error',
        //   //title: 'Server Error!',
        //   body: 'Something went wrong.please try again later.',
        //   showCloseButton: true,
        //   timeout: 4000,
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });

  }

  // ngOnInit() {
  //   this.addPopup = false;
  // }

  onSelect({ selected }) {
    console.log('Select Event', selected, this.selected);

    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }


  popupModal(item, id) {
    this.selectedTags = [];
    if (id == 0) {
      this.addPopup = true;
      this.Add = true;
      this.badgeTitle='Add Badge'
      this.badge = {
        id: 0,
        name: '',
        desc: '',
        ico: './assets/images/badgeNew.png',
        tags: '',
        visible: '',
      }
    } else if (id == 1) {
      console.log('item', item);
      if (item.badgeIcon == '' || item.badgeIcon == null || item.badgeIcon == undefined) {
        item.badgeIcon = './assets/images/badgeNew.png';
      }
      if (item.tagIds) {
        var tagIds = item.tagIds.split(',');
        if (tagIds.length > 0) {
          this.tempTags.forEach((tag) => {
            tagIds.forEach((tagId) => {
              if (tag.id == tagId) {
                this.selectedTags.push(tag);
              }
            });
          });
        }
      }
      if (item.tags == '') {
        this.addPopup = true;
        this.Add = false;
        this.badgeTitle='Edit Badge'
        this.badge = {
          id: item.badgeId,
          name: item.badgeName,
          desc: item.description,
          ico: item.badgeIcon,
          tags: item.tags,
          visible: item.visible,
        }
      } else {
        var tagsArr = item.tags.split(',');

        this.addPopup = true;
        this.Add = false;
        this.badgeTitle='Edit Badge'
        this.badge = {
          id: item.badgeId,
          name: item.badgeName,
          desc: item.description,
          ico: item.badgeIcon,
          tags: tagsArr,
          visible: item.visible,
        }
      }



    }

  }

  gotoCardaddedit(item) {
    this.selectedTags = [];
    console.log('item', item);
      if (item.badgeIcon == '' || item.badgeIcon == null || item.badgeIcon == undefined) {
        item.badgeIcon = './assets/images/badgeNew.png';
      }
      if (item.tagIds) {
        var tagIds = item.tagIds.split(',');
        if (tagIds.length > 0) {
          this.tempTags.forEach((tag) => {
            tagIds.forEach((tagId) => {
              if (tag.id == tagId) {
                this.selectedTags.push(tag);
              }
            });
          });
        }
      }
      if (item.tags == '') {
        this.addPopup = true;
        this.Add = false;
        this.badgeTitle='Edit Badge'
        this.badge = {
          id: item.badgeId,
          name: item.badgeName,
          desc: item.description,
          ico: item.badgeIcon,
          tags: item.tags,
          visible: item.visible,
        }
      } else {
        var tagsArr = item.tags.split(',');

        this.addPopup = true;
        this.badgeTitle='Edit Badge'
        this.Add = false;
        this.badge = {
          id: item.badgeId,
          name: item.badgeName,
          desc: item.description,
          ico: item.badgeIcon,
          tags: tagsArr,
          visible: item.visible,
        }
      }
 }

  back() {
    window.history.back();
    // this.router.navigate(['/pages/gamification/badges_category']);
  }

  closeModal() {
    this.addPopup = false;
  }

  badgeImgData: any;
  readBadgeThumb(event: any) {
    this.fileexterror = false;
    var validExts = new Array('.png', '.jpg', '.jpeg');
    var fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
      this.fileexterror = true;
    } else {
      if (event.target.files && event.target.files[0]) {
        this.badgeImgData = event.target.files[0];

        var reader = new FileReader();

        reader.onload = (event: ProgressEvent) => {
          // this.defaultThumb = (<FileReader>event.target).result;
          //this.formdata.coursePicRef = null;
          this.badge.ico = (<FileReader>event.target).result;
          // this.cdf.detectChanges();
        }
        reader.readAsDataURL(event.target.files[0]);
      }
    }
  }

  deleteBadgeThumb() {
    // this.defaultThumb = 'assets/images/category.jpg';
    this.badge.ico = null;
    this.badgeImgData = null;
  }

  passparams: any;
  fileres: any;

  Submit(item, f) {
    console.log(item,"itrm")
    if (f.valid) {
      this.spinner.show();
      this.closeModal();
      if (this.selectedTags.length > 0) {
        this.makeTagDataReady(this.selectedTags);
        // this.formdata.tags = this.formattedTags;
      }
      // this.closeModal();
      // this.makeTagDataReady();
      // if(item.id == undefined){
      //   let param = {
      //     'cId': 0,
      //     'bname':item.name,
      //     'description':item.description,
      //     'iconRef':'',
      //     'visible':'1',
      //     'tId':1,
      //     'tags':this.formattedTags,
      //     'userId':this.userId,
      //     'catId':this.catId

      //     // this.formdata.tags1

      //   }

      //   this.passparams =param;
      //   console.log('param:',this.passparams);

      // }else{

      //   let param = {

      //     'cId': item.id,
      //     'bname':item.name,
      //     'description':item.desc,
      //     'iconRef':'',
      //     'visible':item.visible,
      //     'tId':1,
      //     'tags':this.formattedTags,
      //     'userId':this.userId,
      //     'catId':this.catId

      //   }

      //   this.passparams = param;
      //   console.log('param:',this.passparams);

      // }

      let param = {
        'cId': item.id ? item.id : 0,
        'bname': item.name,
        'description': item.desc,
        'iconRef': item.ico?item.ico:null,
        'visible': '1',
        'tId': this.tenantId,
        'tags': this.badge.tags,
        'userId': this.userId,
        'catId': this.catId

        // this.formdata.tags1

      }

      this.passparams = param;

      var fd = new FormData();
      fd.append('content', JSON.stringify(this.passparams));
      fd.append('file', this.badgeImgData);
      console.log('File Data ', fd);

      console.log('Course Data Img', this.badgeImgData);
      console.log('Course Data ', this.passparams);
      let fileUploadUrl = webApi.domain + webApi.url.fileUpload;

      if (this.badgeImgData) {
        this.spinner.show();
        this.webApiService.getService(fileUploadUrl, fd)
          .then(rescompData => {
            var temp: any = rescompData;
            this.fileUploadRes = temp;
            this.badgeImgData = null;
            if (temp === 'err') {
              // this.notFound = true;
              //               var thumbUpload: Toast = {
              //                 type: 'error',
              //                 title: 'Badge',
              //                 body: 'Unable to upload Badge thumbnail.',
              //                 // body: temp.msg,
              //                 showCloseButton: true,
              //                 timeout: 4000
              // ,              };
              this.spinner.hide();
              // this.toasterService.pop(thumbUpload);
              this.presentToast('error', '');

            } else if (temp.type === false) {
              // var thumbUpload: Toast = {
              //   type: 'error',
              //   title: 'Badge',
              //   body: 'Unable to upload Badge thumbnail.',
              //   // body: temp.msg,
              //   showCloseButton: true,
              //   timeout: 4000,
              // };
              this.spinner.hide();
              // this.toasterService.pop(thumbUpload);
              this.presentToast('error', '');
              // this.fetchBadges();
            }
            else {
              if (this.fileUploadRes.data != null || this.fileUploadRes.fileError != true) {
                this.passparams.iconRef = this.fileUploadRes.data.file_url;
                console.log('this.passparams.iconRef', this.passparams.iconRef);
                this.spinner.hide();
                this.addUpdateBadge(this.passparams);
              } else {
                // var thumbUpload: Toast = {
                //   type: 'error',
                //   title: 'Badge',
                //   // body: 'Unable to upload course thumbnail.',
                //   body: this.fileUploadRes.status,
                //   showCloseButton: true,
                //   timeout: 4000,
                // };
                this.spinner.hide();
                // this.toasterService.pop(thumbUpload);
                this.presentToast('error', '');
                this.fetchBadges();
                // this.cdf.detectChanges();
              }
            }
            console.log('File Upload Result', this.fileUploadRes);
            var res = this.fileUploadRes;
            this.fileres = res.data.file_url;
          },
            resUserError => {
              this.spinner.hide();
              this.errorMsg = resUserError;
            });
      } else {
        // this.passparams.iconRef = this.badge.ico;
        this.spinner.hide();
        this.addUpdateBadge(this.passparams);
      }

    } else {
      console.log('Please Fill all fields');
      // const addEditF: Toast = {
      //   type: 'error',
      //   title: 'Unable to update',
      //   body: 'Please Fill all fields',
      //   showCloseButton: true,
      //   timeout: 2000
      // };
      // this.toasterService.pop(addEditF);
      Object.keys(f.controls).forEach(key => {
        f.controls[key].markAsDirty();
      });
    }
  }

  fileUploadRes: any;
  badgeAddEditRes: any;
  errorMsg: any;

  addUpdateBadge(badge) {
    this.spinner.show();
    const _urlInsertUpdate:string = webApi.domain + webApi.url.insertupdatebadges;
    this.commonFunctionService.httpPostRequest(_urlInsertUpdate,badge)
    //  this.bagdeservice.insertUpdateBadges(badge)
      .then(rescompData => {

        this.spinner.hide();

        var temp = rescompData;
        this.badgeAddEditRes = temp['data'];
        console.log('Badge Add Result ', this.badgeAddEditRes)
        if (this.badge.id === 0) {
          if (temp['type'] === true) {
            // this.closeEnableDisableBadgeModal();
            // this.fetchBadges();
            // console.log('Badge Add Result ',this.badgeAddEditRes)
            // var catUpdate: Toast = {
            //   type: 'success',
            //   title: 'Badge',
            //   body: 'New Badge has been added successfully.',
            //   showCloseButton: true,
            //   timeout: 4000,
            // };
            // this.toasterService.pop(catUpdate);
            this.presentToast('success', 'New badge added');
            this.displayArray =[];
            this.newArray =[];
            this.fetchBadges();
            // this.cdf.detectChanges();
          } else {
            //  this.closeEnableDisableBadgeModal();
            // this.fetchBadges();
            // var catUpdate: Toast = {
            //   type: 'error',
            //   title: 'Badge ',
            //   body: 'Unable to add Badge.',
            //   showCloseButton: true,
            //   timeout: 4000,
            // };
            // this.toasterService.pop(catUpdate);
            this.presentToast('error', '');
            this.displayArray =[];
            this.newArray =[];
            this.fetchBadges();
            // this.cdf.detectChanges();
          }
        } else {
          console.log('Badge Edit Result ', this.badgeAddEditRes)
          if (temp['type'] == true) {
            // this.closeEnableDisableBadgeModal();
            this.displayArray =[];
            this.newArray =[];
            this.fetchBadges();
            // var catUpdate: Toast = {
            //   type: 'success',
            //   title: 'Badge',
            //   body: 'Badge has been updated successfully.',
            //   showCloseButton: true,
            //   timeout: 4000,
            // };
            // this.toasterService.pop(catUpdate);
            this.presentToast('success', 'Badge updated');
            // this.fetchBadges();
            // this.cdf.detectChanges();
          } else {
            //this.closeEnableDisableBadgeModal();
            this.displayArray =[];
            this.newArray =[];
            this.fetchBadges();
            // this.cdf.detectChanges();
            // var catUpdate: Toast = {
            //   type: 'error',
            //   title: 'Badge',
            //   body: 'Unable to update Badge.',
            //   showCloseButton: true,
            //   timeout: 4000,
            // };
            // this.toasterService.pop(catUpdate);
            this.presentToast('error', '');
            // this.fetchBadges();
          }
        }

      },
        resUserError => {
          this.spinner.hide();
          this.errorMsg = resUserError;
          // var toast: Toast = {
          //   type: 'error',
          //   body: 'Something went wrong.please try again later.',
          //   showCloseButton: true,
          //   timeout: 4000,
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        });
  }

  // formattedTags: any;
  // makeTagDataReady() {
  //   var tagsData = this.badge.tags;
  //   if (tagsData.length > 0) {
  //     var tagsString = '';
  //     for (let i = 0; i < tagsData.length; i++) {
  //       var tag = tagsData[i];
  //       if (tagsString != '') {
  //         tagsString += '|';
  //       }
  //       if (tag.value) {
  //         if (String(tag.value) != '' && String(tag.value) != 'null') {
  //           tagsString += tag.value;
  //         }
  //       } else {
  //         if (String(tag) != '' && String(tag) != 'null') {
  //           tagsString += tag;
  //         }
  //       }
  //     }
  //     this.formattedTags = tagsString;
  //   }
  // }

  disableBadge(currentIndex, badgeData, status) {
    this.enableDisableCategoryData = badgeData;
    console.log('this.enableDisableCategoryData', this.enableDisableCategoryData)
    this.enableDisableBadgeModal = true;
    this.catDisableIndex = currentIndex;

    if (this.enableDisableCategoryData.visible == 0) {
      this.enableCat = false;
    } else {
      this.enableCat = true;
    }

  }
  visibleStatus : any;
  clickTodisable(badgeData) {
    this.btnName='Yes'
    if (badgeData.visible == 1) {
      this.title='Disable Badge'
      this.enableCat = false;
      this.enableDisableCategoryData = badgeData;
      console.log('this.enableDisableCategoryData', this.enableDisableCategoryData);
      this.enableDisableBadgeModal = true;
    } else {
      this.enableCat = true;
      this.title='Enable Badge'
      this.enableDisableCategoryData = badgeData;
      console.log('this.enableDisableCategoryData', this.enableDisableCategoryData);
      this.enableDisableBadgeModal = true;
    }

  }

  enableDisableCategoryAction(actionType) {
    if (actionType == true) {
      if (this.enableDisableCategoryData.visible == 1) {
        this.enableDisableCategoryData.bvisible = 0;
        var catData = this.enableDisableCategoryData;
        this.enableDisableBadge(catData);

      } else {
        this.enableDisableCategoryData.bvisible = 1;
        var catData = this.enableDisableCategoryData;
        this.enableDisableBadge(catData);

      }
      this.closeEnableDisableBadgeModal();
    } else {
      //this.enableDisableCategoryData.visible = 0;
      this.closeEnableDisableBadgeModal();
    }
  }

  enableDisableBadge(catData) {
    this.spinner.show();
    console.log('catData', catData);
    let param = {
      'badgeId': catData.badgeId,
      'visible': catData.bvisible,

    }
    const _urlUpdateBadgeStatus:string = webApi.domain + webApi.url.changebadgestatus;
    this.commonFunctionService.httpPostRequest(_urlUpdateBadgeStatus,param)
    //this.bagdeservice.UpdateBadgesStatus(param)


      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('UpdateBadgeResponse:', rescompData);
        if (result['type'] == true) {
          // var catUpdate: Toast = {
          //   type: 'success',
          //   title: 'Badge',
          //   body: 'Badge has been updated successfully.',
          //   showCloseButton: true,
          //   timeout: 4000,
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('success', 'Badge updated');
          this.enableDisableCategoryData.visible = catData.bvisible;
        } else {
          // var catUpdate: Toast = {
          //   type: 'error',
          //   title: 'Badge',
          //   body: 'Unable to update Badge.',
          //   showCloseButton: true,
          //   timeout: 4000,
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
        }
      }, error => {
        this.spinner.hide();
        // var toast: Toast = {
        //   type: 'error',
        //   //title: 'Server Error!',
        //   body: 'Something went wrong.please try again later.',
        //   showCloseButton: true,
        //   timeout: 4000,
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });

  }
  SarchFilter(event, searchtext) {
    searchtext= event.target.value;
    console.log(searchtext);
    const val = searchtext.toLowerCase();
    if(searchtext.length>=3||searchtext.length==0){
    const tempcc = this.rows.filter(function (d) {
      return String(d.badgeName).toLowerCase().indexOf(val) !== -1 ||
        // d.description.toLowerCase().indexOf(val) !== -1 ||
        // d.relayDate.toLowerCase().indexOf(val) !== -1 ||
        // d.manager.toLowerCase().indexOf(val) !== -1 ||
        !val
    })
    console.log(tempcc);
    this.badgelist = tempcc;
    if (!tempcc.length) {
      this.Nodatafound = true;
      this.noDataVal={
        margin:'mt-5',
        imageSrc: '../../../../../assets/images/no-data-bg.svg',
        title:"Sorry we couldn't find any matches please try again",
        desc:".",
        titleShow:true,
        btnShow:false,
        descShow:false,
        btnText:'Learn More',
        btnLink:''
      }
    } else {
      this.Nodatafound = false;
    }
  }
  
  }

  closeEnableDisableBadgeModal() {
    // console.log('Course content',course);
    this.enableDisableBadgeModal = false;

  }

  // Help Code Start Here //

  helpContent: any;
  getHelpContent() {
    return new Promise(resolve => {
      this.http1
        .get('../../../../../../assets/help-content/addEditCourseContent.json')
        .subscribe(
          data => {
            this.helpContent = data;
            console.log('Help Array', this.helpContent);
          },
          err => {
            resolve('err');
          }
        );
    });
    // return this.helpContent;
  }

  // Help Code Ends Here //

  makeTagDataReady(tagsData) {
    this.badge.tags = ''
    tagsData.forEach((tag) => {
      if (this.badge.tags == '') {
        this.badge.tags = tag.id;
      } else {
        this.badge.tags = this.badge.tags + '|' + tag.id;
      }
      console.log('this.badge.tags', this.badge.tags);
    });
    // var tagsData = this.formdata.tags;
    // var tagsString = '';
    // if (tagsData) {
    //   for (let i = 0; i < tagsData.length; i++) {
    //     var tag = tagsData[i];
    //     if (tagsString != "") {
    //       tagsString += "|";
    //     }
    //     if (String(tag) != "" && String(tag) != "null") {
    //       tagsString += tag;
    //     }
    //   }
    //   this.formdata.tags1 = tagsString;
    // }
  }
  onTagsSelect(item: any) {
    console.log(item);
    console.log(this.selectedTags);
  }
  OnTagDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedTags);
  }
  ngOnDestroy(){
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0){
      e1[0].classList.add('with-scroll');
    }
  }
  ngAfterContentChecked() {
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0)
    {
      e1[0].classList.remove('with-scroll');
    }

   }
   conentHeight = '0px';
   offset: number = 100;
   ngOnInit() {
     this.header.title=this.catName;
     this.conentHeight = '0px';
     const e = document.getElementsByTagName('nb-layout-column');
     console.log(e[0].clientHeight);
     // this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
     if (e[0].clientHeight > 1300) {
       this.conentHeight = '0px';
       this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
     } else {
       this.conentHeight = String(e[0].clientHeight) + 'px';
     }
   }
 
   sum = 40;
   addItems(startIndex, endIndex, _method, asset) {
     for (let i = startIndex; i < endIndex; ++i) {
       if (asset[i]) {
         this.displayArray[_method](asset[i]);
       } else {
         break;
       }
 
       // console.log("NIKHIL");
     }
     if(this.displayArray.length==0&&this.fromSearch){
      this.Nodatafound = true
      this.noDataVal={
        margin:'mt-5',
        imageSrc: '../../../../../assets/images/no-data-bg.svg',
        title:"Sorry we couldn't find any matches please try again",
        desc:".",
        titleShow:true,
        btnShow:false,
        descShow:false,
        btnText:'Learn More',
        btnLink:''
      }
      }else{
        this.Nodatafound = false
      }
   }
   // sum = 10;
   onScroll(event) {
 
     let element = this.myScrollContainer.nativeElement;
     // element.style.height = '500px';
     // element.style.height = '500px';
     // Math.ceil(element.scrollHeight - element.scrollTop) === element.clientHeight
 
     if (element.scrollHeight - element.scrollTop - element.clientHeight < 5) {
       if (this.newArray.length > 0) {
         if (this.newArray.length >= this.sum) {
           const start = this.sum;
           this.sum += 50;
           this.addItems(start, this.sum, 'push', this.newArray);
         } else if ((this.newArray.length - this.sum) > 0) {
           const start = this.sum;
           this.sum += this.newArray.length - this.sum;
           this.addItems(start, this.sum, 'push', this.newArray);
         }
       } else {
         if (this.badgelist.length >= this.sum) {
           const start = this.sum;
           this.sum += 50;
           this.addItems(start, this.sum, 'push', this.badgelist);
         } else if ((this.badgelist.length - this.sum) > 0) {
           const start = this.sum;
           this.sum += this.badgelist.length - this.sum;
           this.addItems(start, this.sum, 'push', this.badgelist);
         }
       }
 
 
       console.log('Reached End');
     }
   }
   newArray =[];
   displayArray =[];
    // this function is use to dynamicsearch on table
  searchData(event) {
    this.fromSearch=true
    var temData = this.badgelist;
    var val = event.target.value.toLowerCase();
    var keys = [];
    if(event.target.value == '')
    {
      this.displayArray =[];
      this.newArray = [];
      this.sum = 50;
      this.addItems(0, this.sum, 'push', this.badgelist);
      // if(this.badgelist.length==0){
      // this.Nodatafound = false
      // }else{
      //   this.Nodatafound = true
      // }
    }else if(val.length >=3){
         // filter our data
         const tempcc = this.badgelist.filter(function (d) {
          return String(d.badgeName).toLowerCase().indexOf(val) !== -1 ||
            // d.description.toLowerCase().indexOf(val) !== -1 ||
            // d.relayDate.toLowerCase().indexOf(val) !== -1 ||
            // d.manager.toLowerCase().indexOf(val) !== -1 ||
            !val
        })
    // if (temData.length > 0) {
    //   keys = Object.keys(temData[0]);
    // }
    // var temp = temData.filter((d) => {
    //   for (const key of keys) {
    //     if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
    //       return true;
    //     }
    //   }
    // });

    // update the rows
    this.displayArray =[];
    this.newArray = tempcc;
    this.sum = 50;
    this.addItems(0, this.sum, 'push', this.newArray);
    // if(this.newArray.length==0){
    //   this.Nodatafound = true;
    // }
    // else{
    //   this.Nodatafound = false
    // }
  }
}
}
