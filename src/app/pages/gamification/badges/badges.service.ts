import {Injectable, Inject} from '@angular/core';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppConfig} from '../../../app.module';
import { webApi } from '../../../service/webApi';
import { HttpClient } from "@angular/common/http";

@Injectable()
export class BadgesService {


  categoryId:any;
  catName:any;

  private _urlFetch:string = webApi.domain + webApi.url.fetchBadges;
  private _urlInsertUpdate:string = webApi.domain + webApi.url.insertupdatebadges;
  private _urlUpdateBadgeStatus:string = webApi.domain + webApi.url.changebadgestatus;

   //busy: Promise<any>;
  //private _url:string = "https://api.myjson.com/bins/hcnt6"
  // private _url:string = "http://52.33.226.70:2000/api/v1/arte/userMaster/getUserTableData"
  // private _url1:string = "/api/dashboard/course_completion"
  // private _url2:string = "/api/dashboard/course_grades"
  // private _url3:string = "/api/dashboard/user_details"

  constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private _http: Http,private _httpClient:HttpClient){
      //this.busy = this._http.get('...').toPromise();
  }

  getBadges(param){
    // let url:any = this._urlFetch;
    // return this._http.post(url,param)
    //     .map((response:Response)=>response.json())
    //     .catch(this._errorHandler);
    return new Promise(resolve => {
      this._httpClient.post(this._urlFetch, param)
      //.map(res => res.json())
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}

insertUpdateBadges(param){
  // let url:any = this._urlInsertUpdate;
  // return this._http.post(url,param)
  //     .map((response:Response)=>response.json())
  //     .catch(this._errorHandler);
    return new Promise(resolve => {
      this._httpClient.post(this._urlInsertUpdate, param)
      //.map(res => res.json())
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}

UpdateBadgesStatus(param){
  // let url:any = this._urlUpdateBadgeStatus;
  // return this._http.post(url,param)
  //     .map((response:Response)=>response.json())
  //     .catch(this._errorHandler);
    return new Promise(resolve => {
      this._httpClient.post(this._urlUpdateBadgeStatus, param)
      //.map(res => res.json())
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}

_errorHandler(error: Response){
  console.error(error);
  return Observable.throw(error || "Server Error")
}


}
