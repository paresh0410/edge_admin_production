import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { NgxEchartsModule } from 'ngx-echarts';
 import { ThemeModule } from '../../../@theme/theme.module';
import { Badges } from './badges.component';
import { ReactiveFormsModule } from '@angular/forms';
import {BadgesService } from './badges.service';
// import { CourseBundleService } from './courseBundle/courseBundle.service';
// import { CourseBundle } from './courseBundle/courseBundle';
// import { ContentService } from './courses/content/content.service';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { CommonModule } from '@angular/common';
import { SortablejsModule } from 'angular-sortablejs';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { TagInputModule } from 'ngx-chips';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgxContentLoadingModule } from 'ngx-content-loading';
import { ComponentModule } from '../../../component/component.module';



// import { CourseBundleModule } from './courseBundle/courseBundle.module';


@NgModule({
  imports: [
     ThemeModule,
    NgxEchartsModule,
    CommonModule,
    SortablejsModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    TagInputModule,
    NgxSkeletonLoaderModule,
    NgxContentLoadingModule,
    AngularMultiSelectModule,
    ComponentModule,
    // CourseBundleModule
  ],
  declarations: [
    Badges,
    // CourseBundle
  ],
  providers: [
    BadgesService,
    // CourseBundleService,
    // ContentService
  ],
  schemas: [ 
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA 
  ]
})
export class BadgesModule { }
