import {Component, OnDestroy, ViewEncapsulation} from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators/takeWhile' ;
import { Router,ActivatedRoute} from '@angular/router';
import { AppService } from '../../app.service';
import { SuubHeader } from '../components/models/subheader.model';

interface CardSettings {
  title: string;
  iconClass: string;
  type: string;
}

@Component({
  selector: 'ngx-gamification',
  styleUrls: ['./gamification.component.scss'],
  templateUrl: './gamification.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class GamificationComponent implements OnDestroy {

  private alive = true;

  lightCard: CardSettings = {
    title: 'Light',
    iconClass: 'nb-lightbulb',
    type: 'primary',
  };
  rollerShadesCard: CardSettings = {
    title: 'Roller Shades',
    iconClass: 'nb-roller-shades',
    type: 'success',
  };
  wirelessAudioCard: CardSettings = {
    title: 'Wireless Audio',
    iconClass: 'nb-audio',
    type: 'info',
  };
  coffeeMakerCard: CardSettings = {
    title: 'Coffee Maker',
    iconClass: 'nb-coffee-maker',
    type: 'warning',
  };

  statusCards: string;

  commonStatusCardsSet: CardSettings[] = [
    this.lightCard,
    this.rollerShadesCard,
    this.wirelessAudioCard,
    this.coffeeMakerCard,
  ];

  statusCardsByThemes: {
    default: CardSettings[];
    cosmic: CardSettings[];
    corporate: CardSettings[];
  } = {
    default: this.commonStatusCardsSet,
    cosmic: this.commonStatusCardsSet,
    corporate: [
      {
        ...this.lightCard,
        type: 'warning',
      },
      {
        ...this.rollerShadesCard,
        type: 'primary',
      },
      {
        ...this.wirelessAudioCard,
        type: 'danger',
      },
      {
        ...this.coffeeMakerCard,
        type: 'secondary',
      },
    ],
  };
  header: SuubHeader  = {
    title:'Gamification',
    showBreadcrumb:true,
    breadCrumbList:[]
  };
   showdata:any=[];
      employees:boolean=false;
      users:boolean=false;
      courses:boolean=false;
      evaluation:boolean=false;
       competancy:boolean=false;
      courseBundle:boolean=false;
      roleManagement:boolean=false;
      learnData:any=[];
    constructor(private themeService: NbThemeService,public router:Router,public routes:ActivatedRoute,private AppService:AppService) {
    this.themeService.getJsTheme()
      .pipe(takeWhile(() => this.alive))
      .subscribe(theme => {
        this.statusCards = this.statusCardsByThemes[theme.name];
    });
       this.showdata = this.AppService.getmenus();

       
      if(this.showdata)
      {
          for (let i = 0; i < this.showdata.length; i++)
          {
            if (Number(this.showdata[i].parentMenuId) === 33)
            {
              console.log("MEnu ====>",this.learnData);
              this.learnData.push(this.showdata[i]);
            }
          }
      }
  }

  ngOnDestroy() {
    this.alive = false;
  }
  gotopages(url){
    this.router.navigate([url],{relativeTo:this.routes});
  }

  gotobadges(){
    this.router.navigate(['badges'],{relativeTo:this.routes});
  }

  gotobadgescategory(){
    this.router.navigate(['badges_category'],{relativeTo: this.routes});
  }

    gotocertificatess(){
    this.router.navigate(['certificate'],{relativeTo: this.routes});
  }

  gotoladders(){
    this.router.navigate(['leaderboards'], {relativeTo: this.routes});
  }

  gotosocialsharing(){
    // this.router.navigate(['socialSharing'],{relativeTo:this.routes});
  }

  gotoroleManagement(){
    this.router.navigate(['roleManagement'],{relativeTo:this.routes});
  }

  
}
