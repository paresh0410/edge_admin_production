import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { NgxEchartsModule } from 'ngx-echarts';
import { ThemeModule } from '../../@theme/theme.module';
import { GamificationComponent } from './gamification.component';
import { ReactiveFormsModule } from '@angular/forms';

import { CommonModule } from '@angular/common';
import { SortablejsModule } from 'angular-sortablejs';
import { CertificateComponent } from './certificate/certificate.component';
import { TemplateComponent } from './template/template.component';
import { BadgeCategoryComponent } from './badge-category/badge-category.component';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgxContentLoadingModule } from 'ngx-content-loading';
import { ComponentModule } from '../../component/component.module';

// import { CourseBundleModule } from './courseBundle/courseBundle.module';


@NgModule({
  imports: [
    ThemeModule,
    NgxEchartsModule,
    CommonModule,
    SortablejsModule,
    ReactiveFormsModule,
    NgxSkeletonLoaderModule,
    NgxContentLoadingModule,
    ComponentModule,

    
    // CourseBundleModule
  ],
  declarations: [
    GamificationComponent,
    TemplateComponent,
    BadgeCategoryComponent,
    // CourseBundle
  ],
  providers: [
  ],
  schemas: [ 
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA 
  ]
})
export class GamificationModule { }
