import { Component, OnDestroy, ViewEncapsulation } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators/takeWhile';
import { NG_VALIDATORS, Validator, Validators, AbstractControl, ValidatorFn, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { addeditladdersService } from './addeditladders.service';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { BadgesService } from './../../badges/badges.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { CertificateService } from '../../certificate/certificate.service';
import { NotificationtemplateServiceService } from '../../../plan/notification-templates/notification-templates.service'
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { SuubHeader } from '../../../components/models/subheader.model';

interface CardSettings {
	title: string;
	iconClass: string;
	type: string;
}

@Component({
	selector: 'ngx-addeditladders',
	styleUrls: ['./addeditladders.component.scss'],
	templateUrl: './addeditladders.component.html',
	encapsulation: ViewEncapsulation.None
})
export class addeditladders {
	badgeId: any;
	private alive = true;

	lightCard: CardSettings = {
		title: 'Light',
		iconClass: 'nb-lightbulb',
		type: 'primary',
	};
	rollerShadesCard: CardSettings = {
		title: 'Roller Shades',
		iconClass: 'nb-roller-shades',
		type: 'success',
	};
	wirelessAudioCard: CardSettings = {
		title: 'Wireless Audio',
		iconClass: 'nb-audio',
		type: 'info',
	};
	coffeeMakerCard: CardSettings = {
		title: 'Coffee Maker',
		iconClass: 'nb-coffee-maker',
		type: 'warning',
	};

	statusCards: string;

	commonStatusCardsSet: CardSettings[] = [
		this.lightCard,
		this.rollerShadesCard,
		this.wirelessAudioCard,
		this.coffeeMakerCard,
	];

	form: FormGroup = new FormGroup({});

	statusCardsByThemes: {
		default: CardSettings[];
		cosmic: CardSettings[];
		corporate: CardSettings[];
	} = {
			default: this.commonStatusCardsSet,
			cosmic: this.commonStatusCardsSet,
			corporate: [
				{
					...this.lightCard,
					type: 'warning',
				},
				{
					...this.rollerShadesCard,
					type: 'primary',
				},
				{
					...this.wirelessAudioCard,
					type: 'danger',
				},
				{
					...this.coffeeMakerCard,
					type: 'secondary',
				},
			],
		};

	formdata =
		{
			lid: '',
			lname: '',
			lcount: '',
			lformula: '',
			lbasep: '',
			lmulti: '',
			lcourse: [],
			lstatus: '',
			ldisplay: '',
			lmintime: '',
			lmaxtime: '',
			//	lenablenot: '',
			lnmsg: '',
			lnmsgtemp: '',
			lnnot: '',
			lnnottemp: '',
			lnemail: '',
			lnemailTemp: '',
		}
	formdataDetails =
		{
			lid: '',
			lname: '',
			//  lcount:'',
			lformula: '',
			lbasep: '',
			lmulti: '',
			lcourse: [],
			lstatus: '',
			ldisplay: '',
			lmintime: '',
			lmaxtime: '',
			// lenablenot:'',
			// lnmsg:'',
			// lnmsgtemp:'',
			// lnnot:'',
			// lnnottemp:'',
			// lnemail:'',
			// lnemailTemp:'',
		}

	formdataSteps = {
		lcount: '',
	}

	// formdataNotifications = {
	// 	lenablenot: '',
	// 	lnmsg: '',
	// 	lnmsgtemp: '',
	// 	lnnot: '',
	// 	lnnottemp: '',
	// 	lnemail: '',
	// 	lnemailTemp: ''
	// }

	formdataNotifications: any = {};


	detailsTab: boolean = true;
	stepsTab: boolean = false;
	notificationTab: boolean = false;
	header: SuubHeader;

	level =
		{
			lid: '',
			points: '',
			desc: '',
			badge: '',
			certi: '',
			imgsrc: '',
			name: '',
			cnt: ''

		}

	lcountData: any = [];
	lcountData2: any = [];

	templatenot: any = [];
	templatesms: any = [];
	templateemail: any = [];

	display = [];
	courses = [];
	badgeData: any = [];
	certData: any = [];
	//CertData:any = [];
	dropdownListDept = [];
	selectedItemsDept = [];
	dropdownSettingsDept = {};
	badge: any;
	certificate: any;

	showEnrolpage: boolean = false;
	badgeshow: boolean = false;
	certshow: boolean = false;
	contdata: any;
	contdata2: any;
	indx: any;
	addAction: any;

	loader: any;
	DetailsSubmit: boolean = false;
	StepsSubmit: boolean = false;
	NotificationSubmit: boolean = false;
	getdateforaddedit: any = [];
	mymodel: any;

	badgeDiv: boolean = true;
	certDiv: boolean = true;
	warning: boolean = false;
	formulaChange: boolean = false;
	userId: any;
	tenantId;
	lid: any;
	leaderData: any;
	constructor(private themeService: NbThemeService, public router: Router,
		// private toasterService: ToasterService,
		public routes: ActivatedRoute, private addeditladdersservice: addeditladdersService,
		private bagdeservice: BadgesService, private spinner: NgxSpinnerService, private certificateservice: CertificateService,
		private notitempservice: NotificationtemplateServiceService, private http1: HttpClient, private toastr: ToastrService) {
		this.getHelpContent();
		//this.fetchdropdowns();
		if (localStorage.getItem('LoginResData')) {
			var userData = JSON.parse(localStorage.getItem('LoginResData'));
			console.log('userData', userData.data);
			this.userId = userData.data.data.id;
			this.tenantId = userData.data.data.tenantId;
		}
		this.fetchBadges();
		this.fetchCertificates();
		// this.badgeData = this.badges;
		// this.themeService.getJsTheme()
		// 	.pipe(takeWhile(() => this.alive))
		// 	.subscribe(theme => {
		// 		this.statusCards = this.statusCardsByThemes[theme.name];
		// 	});

		this.selectedItemsDept = [];
		this.dropdownSettingsDept = {
			singleSelection: false,
			text: 'Select Courses',
			selectAllText: 'Select All',
			unSelectAllText: 'UnSelect All',
			enableSearchFilter: true,
			// badgeShowLimit: 2,
			classes: 'myclass custom-class'
		};
		/*********************************aditya************************/


		this.getdateforaddedit = this.addeditladdersservice.addeditLadderData;
		console.log('this.getdateforaddedit', this.getdateforaddedit);
		this.leaderData=this.getdateforaddedit[1];
		this.courses = this.getdateforaddedit[2];
		this.display = this.getdateforaddedit[3];
		this.dropdownListDept = this.courses;
		if (this.getdateforaddedit[0] == 'ADD') {

			this.formdataDetails.lid = '',
				this.formdataDetails.lname = '',
				this.formdataDetails.lformula = '0',
				this.formdataDetails.lbasep = '',
				this.formdataDetails.lmulti = '',
				this.formdataDetails.lcourse = [],
				this.formdataDetails.lstatus = '',
				this.formdataDetails.ldisplay = '',
				this.formdataDetails.lmintime = '',
				this.formdataDetails.lmaxtime = ''
			this.addAction = true;

		} else if (this.getdateforaddedit[0] == 'EDIT') {


			if (this.getdateforaddedit[1].courseIds == null) {
				this.formdataDetails.lid = this.getdateforaddedit[1].ladderId,
					this.formdataDetails.lname = this.getdateforaddedit[1].ladderName,
					this.formdataDetails.lformula = this.getdateforaddedit[1].isFormula.toString(),
					this.formdataDetails.lbasep = this.getdateforaddedit[1].basePoints,
					this.formdataDetails.lmulti = this.getdateforaddedit[1].multiplier,
					this.formdataDetails.lcourse = this.getdateforaddedit[1].courseIds,
					this.formdataDetails.lstatus = 'Yes',
					this.formdataDetails.ldisplay = this.getdateforaddedit[1].display,
					this.formdataDetails.lmintime = this.getdateforaddedit[1].timebetweenActions,
					this.formdataDetails.lmaxtime = this.getdateforaddedit[1].maxActions
				this.addAction = false;
			} else {

				var courseArr = this.getdateforaddedit[1].courseIds.split(',');
				var courseNameArr
				for (let i = 0; i < this.courses.length; i++) {
					for (let j = 0; j < courseArr.length; j++) {
						if (courseArr[j] == this.courses[i].id) {
							this.selectedItemsDept.push(this.courses[i]);
						}
					}

				}
				console.log('courseNameArr', courseNameArr)

				this.formdataDetails.lid = this.getdateforaddedit[1].ladderId,
					this.formdataDetails.lname = this.getdateforaddedit[1].ladderName,
					this.formdataDetails.lformula = this.getdateforaddedit[1].isFormula.toString(),
					this.formdataDetails.lbasep = this.getdateforaddedit[1].basePoints,
					this.formdataDetails.lmulti = this.getdateforaddedit[1].multiplier,
					this.formdataDetails.lcourse = this.selectedItemsDept,
					this.formdataDetails.lstatus = 'Yes',
					this.formdataDetails.ldisplay = this.getdateforaddedit[1].display,
					this.formdataDetails.lmintime = this.getdateforaddedit[1].timebetweenActions,
					this.formdataDetails.lmaxtime = this.getdateforaddedit[1].maxActions

			}



		}
		console.log('this.formdataDetails', this.formdataDetails);
		this.fetchnotifytemplate();


	}

	points: any;
	mainCount: any;
	submit2(item) {
		// var cnt = Number(count);
		var basepoint = Number(item.lbasep)
		console.log('ITEM--->', item);
		console.log('BASEPOINT--->', basepoint);
		if (this.lcountData.length == 0) {
			var tempId = 0;
		} else {
			tempId = this.lcountData.length;
		}
		var multi = item.lmulti;
		var basep = item.lbasep;
		var level = {
			id: tempId,
			points: '',
			topoint: 0,
			desc: '',
			badge: '',
			certi: '',
			// certid:  0,
			bimgsrc: '',
			bname: '',
			cimgsrc: '',
			cname: '',
		}
		this.lcountData.push(level);
		var tempLength = this.lcountData.length - 1;
		if (tempLength == 0) {
			this.lcountData[tempLength].points = basep;
		} else {
			if (item.lformula == '1') {
				this.lcountData[tempLength].points = (this.lcountData[tempLength - 1].points * multi);
			} else {
				this.lcountData[tempLength].points = '';
			}

			// if(this.lcountData[tempLength].bimgsrc == ''){
			// 	this.badgeDiv = true;
			// }
		}
		console.log('DATA===>', this.lcountData);
	}


	// submit(item, count) {
	// 	count = count + 1;
	// 	//this.lcountData =[];
	// 	if (count) {
	// 		for (let i = 0; i < count; i++) {
	// 			var multi = item.lmulti;
	// 			var basep = item.lbasep;
	// 			if (item.lformula == 1) {
	// 				if (i == 0) {
	// 					var points = basep
	// 				} else {
	// 					points = ((points) * (multi))
	// 				}

	// 				var level =
	// 				{
	// 					points: points,
	// 					topoint: 0,
	// 					desc: '',
	// 					badge: '',
	// 					certi: '',
	// 					certid:0,
	// 					imgsrc: count,
	// 					name: '',

	// 				}
	// 				this.lcountData.push(level)
	// 			} else {
	// 				if (i == 0) {
	// 					var points = basep
	// 				} else {
	// 					points = ''
	// 				}
	// 				level =
	// 					{
	// 						points: points,
	// 						topoint: 0,
	// 						desc: '',
	// 						badge: '',
	// 						certi: '',
	// 						certid:0,
	// 						imgsrc: count,
	// 						name: '',

	// 					}
	// 				this.mainCount = level.imgsrc
	// 				this.lcountData.push(level)
	// 			}
	// 		}
	// 	}
	// }



	// onclick(rid, ind, bId) {
	// 	if (rid == 1) {
	// 		this.badgeshow = true;
	// 		this.badgeData.currentid = ind;
	// 		if(bId != undefined || bId != null){
	// 			this.badgeData.badgeId = bId;
	// 		}
	// 		this.certshow = false;
	// 	} else if (rid == 2) {
	// 		this.certshow = true;
	// 		this.certData.currentid=ind;
	// 		if(bId != undefined || bId != null){
	// 			this.certData.certId = bId;
	// 		}
	// 		this.badgeshow = false;
	// 	}
	// 	this.indx = ind
	// }


	badgeCheck: boolean = true;
	certCheck: boolean = true;
	closeModel(rid) {
		if (rid == 1) {
			this.badgeshow = false;
			this.badgeCheck = false;
		}
		if (rid == 2) {
			this.certshow = false;
			this.certCheck = false;
		}
	}
	onCheckBoxselecClick(item, rid) {
		this.contdata = {};
		this.lcountData[this.badgeData.currentid].imgsrc = item.badgeIcon;
		this.lcountData[this.badgeData.currentid].badgeid = item.badgeId;
		this.lcountData[this.badgeData.currentid].name = item.badgeName;
		//this.lcountData[this.badgeData.currentid].certid = 0;
		this.lcountData[this.badgeData.currentid].topoint = 0;

		this.contdata = item;
		console.log('this.contdata', this.contdata);
		console.log('this.lcountData', this.lcountData);

		// if (rid == 1) {

		// 	this.badgeshow = true;
		// 	this.certshow = false;

		// }
		// if (rid == 2) {
		// 	this.certshow = true;
		// 	this.badgeshow = false;
		// }
	}

	save(rid) {
		this.badgeData.currentid = null;
		if (rid == 1) {

			// for (let i = 0; i < this.lcountData.length; i++) {
			// 	if (i == this.badgeData.currentid) {

			// 		this.lcountData[this.badgeData.currentid].badge=
			// 	}
			// }
			//   this.level = {
			//   points:'',
			//   desc:'',
			//   badge:'',
			//   certi:'',
			//   imgsrc:this.contdata.bimgsrc,
			//   name:this.contdata.bname,
			// };
			this.badgeshow = false;
			//  this.badge=true;
			// this.certificate =true;
		}
		if (rid == 2) {

			// for (let i = 0; i < this.lcountData.length; i++) {
			// 	if (i == this.indx) {
			// 		this.lcountData[i].imgsrc = this.contdata.cimgsrc;
			// 		this.lcountData[i].name = this.contdata.cname;

			// 	}
			// }


			this.certshow = false;

			// this.indx = '';

		}
	}

	savelevel(data) {
		console.log('data', data)
	}

	/*****************************************************************************/

	ondeptSelect(item: any) {
		console.log(item);

	}
	OndeptDeSelect(item: any) {
		console.log(item)
	}
	ondeptSelectAll(items: any) {
		console.log(items);

	}
	ondeptDeSelectAll(items: any) {
		console.log(items);

	}

	back() {
		this.router.navigate(['pages/gamification/leaderboards']);
	}


	selectTab(event) {
		console.log('event', event);
		console.log('event.tabTitle', event.tabTitle);
		if (event.tabTitle == 'Details') {
				this.header = {
					title: this.leaderData?this.leaderData.ladderName:'Add Leaderboard',
					btnsSearch: true,
					btnName2: 'Save',
					btnName2show: true,
					btnBackshow: true,
					showBreadcrumb: true,
					breadCrumbList: [
						{
							'name': 'Gamification',
							'navigationPath': '/pages/gamification',
						},
						{
							'name': 'Leaderboard',
							'navigationPath': '/pages/gamification/leaderboards',
						}, ]
				};
				this.spinner.show();
				setTimeout(() => {
				  this.spinner.hide()
				}, 2000);
			this.DetailsSubmit = true;
			this.StepsSubmit = false;
			this.NotificationSubmit = false;
		} else if (event.tabTitle == 'Steps') {
				this.header = {
					title: this.leaderData?this.leaderData.ladderName:'Add Leaderboard',
					btnsSearch: true,
					btnName3: 'Save',
					btnName3show: true,
					btnBackshow: true,
					showBreadcrumb: true,
					breadCrumbList: [
						{
							'name': 'Gamification',
							'navigationPath': '/pages/gamification',
						},
						{
							'name': 'Leaderboard',
							'navigationPath': '/pages/gamification/leaderboards',
						}, ]
				};
				this.spinner.show();
				setTimeout(() => {
				  this.spinner.hide()
				}, 2000);
			this.DetailsSubmit = false;
			this.StepsSubmit = true;
			this.NotificationSubmit = false;
			this.fetchLadderSteps();

		} else if (event.tabTitle == 'Notification') {
				this.header = {
					title: this.leaderData?this.leaderData.ladderName:'Add Leaderboard',
					btnsSearch: true,
					btnName4: 'Save',
					btnName4show: true,
					btnBackshow: true,
					showBreadcrumb: true,
					breadCrumbList: [
						{
							'name': 'Gamification',
							'navigationPath': '/pages/gamification',
						},
						{
							'name': 'Leaderboard',
							'navigationPath': '/pages/gamification/leaderboards',
						}, ]
				};
				this.spinner.show();
				setTimeout(() => {
				  this.spinner.hide()
				}, 2000);
			this.DetailsSubmit = false;
			this.StepsSubmit = false;
			this.NotificationSubmit = true;
			this.fetchLaddernotifications()
		}
	}

	presentToast(type, body) {
		if (type === 'success') {
			this.toastr.success(body, 'Success', {
				closeButton: false
			});
		} else if (type === 'error') {
			this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
				timeOut: 0,
				closeButton: true
			});
		} else {
			this.toastr.warning(body, 'Warning', {
				closeButton: false
			})
		}
	}

	passparams: any;
	ladderIdStepsInsert: any;
	ladderIdEdit: any;
	forsubmit2: any
	submitDetails(formdataDetails, f) {
		if (f.valid) {
			this.spinner.show();
			this.forsubmit2 = formdataDetails;
			if (formdataDetails.lcourse == null) {

				this.formattedTags = null;
			} else {
				this.makeLadderDataReady(formdataDetails);
			}
			console.log('formdataDetails', formdataDetails);
			if (formdataDetails.lformula == 0) {
				formdataDetails.lmulti = 0
			}

			if (this.getdateforaddedit[0] == 'ADD') {
				let param = {
					'lid': this.lid ? this.lid : 0,
					'lname': formdataDetails.lname,
					'steps': 0,
					'isFormula': formdataDetails.lformula,
					'multiplier': formdataDetails.lmulti,
					'basePoints': formdataDetails.lbasep,
					'visible': 1,
					'display': formdataDetails.ldisplay,
					'timebetweenActions': formdataDetails.lmintime,
					'maxActions': formdataDetails.lmaxtime,
					'tenantId': this.tenantId,
					'userId': this.userId,
					'courses': this.formattedTags

				}
				this.passparams = param;
			} else if (this.getdateforaddedit[0] == 'EDIT') {
				let param = {
					'lid': this.getdateforaddedit[1].ladderId,
					'lname': formdataDetails.lname,
					'steps': 0,
					'isFormula': formdataDetails.lformula,
					'multiplier': formdataDetails.lmulti,
					'basePoints': formdataDetails.lbasep,
					'visible': 1,
					'display': formdataDetails.ldisplay,
					'timebetweenActions': formdataDetails.lmintime,
					'maxActions': formdataDetails.lmaxtime,
					'tenantId': this.tenantId,
					'userId': this.userId,
					'courses': this.formattedTags
				}

				this.passparams = param;
			}
			this.addeditladdersservice.insertUpdateLadders(this.passparams)
				.then(rescompData => {
					this.spinner.hide();

					var temp = rescompData;
					console.log('ladderdetailsResponse', rescompData);
					if (this.getdateforaddedit[0] == 'ADD') {
						this.lid = rescompData['data'][0][0]['last_inserted_ladder_id']
						if (temp['type'] == true) {
							// var ladderUpdate: Toast = {
							// 	type: 'success',
							// 	title: 'Ladder',
							// 	body: 'New Ladder has been added successfully.',
							// 	showCloseButton: true,
							// 	timeout: 4000,
							// };
							// this.toasterService.pop(ladderUpdate);
							this.presentToast('success', 'Ladder added');
							this.submit2(formdataDetails);
							this.detailsTab = false;
							this.stepsTab = true;
							this.notificationTab = false;
							this.DetailsSubmit = false;
							this.StepsSubmit = true;
							this.NotificationSubmit = false;
							//this.router.navigate(['pages/gamification/ladders']);
							var lid = temp['data'][0];
							this.ladderIdStepsInsert = lid[0].last_inserted_ladder_id;
							console.log('ladderId', this.ladderIdStepsInsert);

						} else {
							// var ladderUpdate: Toast = {
							// 	type: 'success',
							// 	title: 'Ladder',
							// 	body: 'Unable to add ladder.',
							// 	showCloseButton: true,
							// 	timeout: 4000,
							// };
							// this.toasterService.pop(ladderUpdate);
							this.presentToast('error', '');
							this.router.navigate(['pages/gamification/leaderboards']);
						}
					} else if (this.getdateforaddedit[0] == 'EDIT') {
						if (temp['type'] == true) {
							// var ladderUpdate: Toast = {
							// 	type: 'success',
							// 	title: 'Ladder',
							// 	body: 'Ladder has been updated successfully.',
							// 	showCloseButton: true,
							// 	timeout: 4000,
							// };
							// this.toasterService.pop(ladderUpdate);
							this.presentToast('success', 'Ladder updated');
							this.submit2(formdataDetails);
							this.detailsTab = false;
							this.stepsTab = true;
							this.notificationTab = false;
							this.DetailsSubmit = false;
							this.StepsSubmit = true;
							this.NotificationSubmit = false;
							//this.router.navigate(['pages/gamification/ladders']);
							this.ladderIdStepsInsert = this.getdateforaddedit[1].ladderId;
							console.log('ladderIdEdit', this.ladderIdStepsInsert);

							if (this.formulaChange == false) {
								this.fetchLadderSteps();
							}


						} else {
							// var ladderUpdate: Toast = {
							// 	type: 'success',
							// 	title: 'Ladder',
							// 	body: 'Unable to update ladder.',
							// 	showCloseButton: true,
							// 	timeout: 4000,
							// };
							// this.toasterService.pop(ladderUpdate);
							this.presentToast('error', '');
							this.router.navigate(['pages/gamification/leaderboards']);
						}
					}
				},
					resUserError => {
						this.spinner.hide();
						//this.errorMsg = resUserError;
					});
		} else {
			console.log('Please Fill all fields');
			// const addEditF: Toast = {
			// 	type: 'error',
			// 	title: 'Unable to update',
			// 	body: 'Please Fill all fields',
			// 	showCloseButton: true,
			// 	timeout: 4000,
			// };
			// this.toasterService.pop(addEditF);
			this.presentToast('warning', 'Please fill in the required fields');
			Object.keys(f.controls).forEach(key => {
				f.controls[key].markAsDirty();
			});
		}
	}

	formattedTags: any;
	makeLadderDataReady(formdataDetails) {
		var ladderData = formdataDetails.lcourse;
		if (ladderData.length > 0) {
			var ladderString = '';
			for (let i = 0; i < ladderData.length; i++) {
				var lad = ladderData[i];
				if (ladderString != '') {
					ladderString += '|';
				}
				if (lad.id) {
					if (String(lad.id) != '' && String(lad.id) != 'null') {
						ladderString += lad.id;
					}
				} else {
					if (String(lad) != '' && String(lad) != 'null') {
						ladderString += lad;
					}
				}
			}
			this.formattedTags = ladderString;
		}
	}

	// gototab2() {
	// 	this.detailsTab = false;
	// 	this.stepsTab = true;
	// 	this.notificationTab = false;
	// }



	dropdisplay: any;
	dropcourse: any;
	fetchdropdowns() {
		let param = {
			'tenantId': this.tenantId,
		}

		this.addeditladdersservice.fetchdropdowns(param)
			.then(rescompData => {

				var result = rescompData;
				console.log('getdropdowns:', rescompData);
				if (result['type'] == true) {
					this.dropdisplay = result['data'][0];

					this.dropdisplay.forEach(element => {
						this.display.push(element);
					});
					console.log('this.display', this.display);


					this.dropcourse = result['data'][1];
					for (let i = 0; i < this.dropcourse.length; i++) {
						// this.courses[i].id = this.dropcourse[i].courseId,
						// this.courses[i].itemName = this.dropcourse[i].fullname
						var tempObj = {
							id: this.dropcourse[i].courseId,
							itemName: this.dropcourse[i].fullname
						}
						this.courses.push(tempObj);
					}
					console.log('this.display', this.display);
					console.log('this.dropcourse', this.dropcourse);
				} else {
					// var toast: Toast = {
					// 	type: 'error',
					// 	// title: 'Server Error!',
					// 	body: 'Something went wrong.please try again later.',
					// 	showCloseButton: true,
					// 	timeout: 4000,
					// };
					// this.toasterService.pop(toast);
					this.presentToast('error', '');
				}


			}, error => {
				// var toast: Toast = {
				// 	type: 'error',
				// 	// title: 'Server Error!',
				// 	body: 'Something went wrong.please try again later.',
				// 	showCloseButton: true,
				// 	timeout: 4000,
				// };
				// this.toasterService.pop(toast);
				this.presentToast('error', '');
			});
	}


	//badges:any=[];
	fetchBadges() {
		let param = {
			'tenantId': this.tenantId,
			'courseId': 0,
			'catId': 0
		}
		this.bagdeservice.getBadges(param)
			.then(rescompData => {

				var result = rescompData;
				console.log('getBadgeResponse:', rescompData);
				if (result['type'] == true) {
					this.badgeData = result['data'][0];
					for (let i = 0; i < this.badgeData.length; i++) {
						if (this.badgeData[i].badgeIcon == '' || this.badgeData[i].badgeIcon == null || this.badgeData[i].badgeIcon == undefined) {
							this.badgeData[i].badgeIcon = 'assets/images/badgeNew.png';
						}
					}
					console.log('this.rows', this.badgeData);
				} else {
					// var toast: Toast = {
					// 	type: 'error',
					// 	// title: 'Server Error!',
					// 	body: 'Something went wrong.please try again later.',
					// 	showCloseButton: true,
					// 	timeout: 4000,
					// };
					// this.toasterService.pop(toast);
					this.presentToast('error', '');
				}


			}, error => {
				// var toast: Toast = {
				// 	type: 'error',
				// 	// title: 'Server Error!',
				// 	body: 'Something went wrong.please try again later.',
				// 	showCloseButton: true,
				// 	timeout: 4000,
				// };
				// this.toasterService.pop(toast);
				this.presentToast('error', '');
			});

	}

	fetchCertificates() {
		let param = {
			'tenantId': this.tenantId,
			'courseId': 0
		}
		this.certificateservice.getCertificates(param)
			.then(rescompData => {
				var result = rescompData;
				if (result['type'] == true) {
					this.certData = rescompData['data'][0];
					console.log(' this.certData', this.certData);
				} else {
					// var toast: Toast = {
					// 	type: 'error',
					// 	// title: 'Server Error!',
					// 	body: 'Something went wrong.please try again later.',
					// 	showCloseButton: true,
					// 	timeout: 4000,
					// };
					// this.toasterService.pop(toast);
					this.presentToast('error', '');
				}

			},
				resUserError => {
					// this.loader =false;
					// var toast: Toast = {
					// 	type: 'error',
					// 	// title: 'Server Error!',
					// 	body: 'Something went wrong.please try again later.',
					// 	showCloseButton: true,
					// 	timeout: 4000,
					// };
					// this.toasterService.pop(toast);
					this.presentToast('error', '');
				});

	}


	badgeIdstep: any
	onCheckBoxselecClickBadge(badgId) {
		this.badgeIdstep = badgId;
		console.log('this.badgeIdstep', this.badgeIdstep);
	}

	certIdstep: any
	onCheckBoxselecClickCert(certId) {
		this.certIdstep = certId;
		console.log('this.certIdstep', this.certIdstep);
	}

	desc: any;
	descdata(desc, j) {
		console.log('desc', this.lcountData[j].desc);
	}

	select(num) {
		if (num == 1) {
			this.closeModel(num);
		} else if (num == 2) {
			this.closeModel(num);
		}
		this
	}

	deleteSteps(stepid) {
		this.lcountData.splice(stepid, 1);
	}

	submitSteps(f) {
		if (f.valid) {
			this.spinner.show();
			var allstr = this.getDatareadyforSteps(this.lcountData);
			console.log('AllString--->', allstr);
			let param = {
				'lId': this.ladderIdStepsInsert,
				'allstr': allstr,
				'tId': this.tenantId,
				'userId': this.userId
			}

			this.addeditladdersservice.insertUpdateLaddersSteps(param)
				.then(rescompData => {
					this.spinner.hide();
					var temp = rescompData;
					console.log('ladderstepsResponse', rescompData);
					if (this.getdateforaddedit[0] == 'ADD') {
						if (temp['type'] == true) {
							// var ladderUpdate: Toast = {
							// 	type: 'success',
							// 	title: 'Step',
							// 	body: 'New Step has been added successfully.',
							// 	showCloseButton: true,
							// 	timeout: 4000,
							// };
							// this.toasterService.pop(ladderUpdate);
							this.presentToast('success', 'A new step added');
							this.detailsTab = false;
							this.stepsTab = false;
							this.notificationTab = true;
							this.DetailsSubmit = false;
							this.StepsSubmit = false;
							this.NotificationSubmit = true;

							this.lcountData = [];

						} else {
							// var ladderUpdate: Toast = {
							// 	type: 'error',
							// 	title: 'Step',
							// 	body: 'Unable to add step.',
							// 	showCloseButton: true,
							// 	timeout: 4000,
							// };
							// this.toasterService.pop(ladderUpdate);
							this.presentToast('error', '');
							this.router.navigate(['pages/gamification/leaderboards']);
						}
					} else if (this.getdateforaddedit[0] == 'EDIT') {
						if (temp['type'] == true) {
							// var ladderUpdate: Toast = {
							// 	type: 'success',
							// 	title: 'Step',
							// 	body: 'Step has been updated successfully.',
							// 	showCloseButton: true,
							// 	timeout: 4000,
							// };
							// this.toasterService.pop(ladderUpdate);
							this.presentToast('success', 'Step updated');
							this.detailsTab = false;
							this.stepsTab = false;
							this.notificationTab = true;
							this.DetailsSubmit = false;
							this.StepsSubmit = false;
							this.NotificationSubmit = true;
							this.lcountData = [];
							this.fetchLaddernotifications();

						} else {
							// var ladderUpdate: Toast = {
							// 	type: 'error',
							// 	title: 'Step',
							// 	body: 'Unable to update step.',
							// 	showCloseButton: true,
							// 	timeout: 4000,
							// };
							// this.toasterService.pop(ladderUpdate);
							this.presentToast('error', '');
							this.router.navigate(['pages/gamification/leaderboards']);
						}
					}



				},
					resUserError => {
						this.spinner.hide();
						//this.errorMsg = resUserError;
					});
		} else {
			console.log('Please Fill all fields');
			// const addEditF: Toast = {
			// 	type: 'error',
			// 	title: 'Unable to update',
			// 	body: 'Please Fill all fields',
			// 	showCloseButton: true,
			// 	timeout: 4000,
			// };
			// this.toasterService.pop(addEditF);
			this.presentToast('warning', 'Please fill in the required fields');
			Object.keys(f.controls).forEach(key => {
				f.controls[key].markAsDirty();
			});
		}
	}

	allstr: any;
	getDatareadyforSteps(stepsArray) {
		var allstr
		for (let i = 0; i < stepsArray.length; i++) {
			if (stepsArray[i].badgeid == null) {
				stepsArray[i].badgeid = 0;
			}

			if (stepsArray[i].certid == null) {
				stepsArray[i].certid = 0;
			}
			var str = stepsArray[i].points + '|' + stepsArray[i].topoint + '|' + stepsArray[i].desc + '|' + stepsArray[i].badgeid + '|' + stepsArray[i].certid;

			if (i == 0) {
				allstr = str;
			} else {
				allstr += '#' + str;
			}
		}

		return allstr;
	}

	/****************Steps******************************** */


	fetchLadderSteps() {
		let param = {
			'ladderId': this.ladderIdStepsInsert,
			'tenantId': this.tenantId,
		}
		if (!param.ladderId) {
			param.ladderId = this.getdateforaddedit[1].ladderId;
		}
		this.addeditladdersservice.fetchladdersteps(param)
			.then(rescompData => {
				var temp = rescompData;
				var res: any = temp['data'][0];
				if (temp['data'][0].length == 0) {
					//this.submit2(this.forsubmit2)
				} else {
					if (temp['type'] == true) {
						console.log('FetchLadderStepsForEdit:', rescompData);
						this.lcountData = [];
						res.forEach(element => {
							var level = {
								id: element.lsId,
								points: element.fromPoints,
								topoint: 0,
								desc: element.ladderDesc,
								badgeid: element.badgeId,
								bimgsrc: element.imgsrc,
								bname: element.badgeName,
								badge: true,
								certid: element.certId,
								cimgsrc: element.cimgsrc,
								cname: element.certName,
								certi: true
							}
							this.lcountData.push(level);
						});

					} else {
						// var toast: Toast = {
						// 	type: 'error',
						// 	//title: 'Server Error!',
						// 	body: 'Something went wrong.please try again later.',
						// 	showCloseButton: true,
						// 	timeout: 4000,
						// };
						// this.toasterService.pop(toast);
						this.presentToast('error', '');
						this.router.navigate(['pages/gamification/leaderboards']);
						console.log('FetchLadderStepsForEdit:', rescompData);
					}
				}


			},
				resUserError => {
					// var toast: Toast = {
					// 	type: 'error',
					// 	//title: 'Server Error!',
					// 	body: 'Something went wrong.please try again later.',
					// 	showCloseButton: true,
					// 	timeout: 4000,
					// };
					// this.toasterService.pop(toast);
					this.presentToast('error', '');
					this.router.navigate(['pages/gamification/leaderboards']);
				});
	}



	fetchLaddernotifications() {
		let param = {
			'lId': this.ladderIdStepsInsert,
			'tid': this.tenantId,
		}
		if (!param.lId) {
			param.lId = this.getdateforaddedit[1].ladderId
		}
		console.log(param);
		this.addeditladdersservice.fetchladdernotifications(param)
			.then(rescompData => {

				var temp = rescompData;
				var temp1: any = temp['data'][0];
				//var arr: any = temp.data[0];
				console.log('notifytemplate', rescompData);

				if (temp['type'] == true) {
					this.formdataNotifications.lnemail = false;
					this.formdataNotifications.lnmsg = false;
					this.formdataNotifications.lnnot = false;
					//this.formdataNotifications.lenablenot = false;
					if (temp1) {

						for (let i = 0; i < temp1.length; i++) {
							if (temp1[i].id == 2 && temp1[i].templateId != null) {
								this.formdataNotifications.lnemailTemp = temp1[i].templateId;
								this.formdataNotifications.lnemail = true;
								this.formdataNotifications.reviewemail = true;
								this.formdataNotifications.editEmailTemplateData = temp1[i].template;
								break;
							} else {
								this.formdataNotifications.lnemail = false;
							}
						}

						for (let i = 0; i < temp1.length; i++) {
							if (temp1[i].id == 1 && temp1[i].templateId != null) {
								this.formdataNotifications.lnmsgtemp = temp1[i].templateId;
								this.formdataNotifications.lnmsg = true;
								this.formdataNotifications.reviewTextSms = true;
								this.formdataNotifications.editSmsTemplateData = temp1[i].template;
								break;
							} else {
								this.formdataNotifications.lnmsg = false;
								// this.formdataNotifications.reviewTextSms = false;
							}
						}

						for (let i = 0; i < temp1.length; i++) {
							if (temp1[i].id == 3 && temp1[i].templateId != null) {
								this.formdataNotifications.lnnottemp = temp1[i].templateId;
								this.formdataNotifications.lnnot = true;
								this.formdataNotifications.reviewnot = true;
								this.formdataNotifications.editNotTemplateData = temp1[i].template;
								break;
							} else {
								this.formdataNotifications.lnnot = false;
							}
						}




					} else {
						//this.formdataNotifications.lenablenot = false;
					}


				} else {
					this.loader = false;
					// var toast: Toast = {
					// 	type: 'error',
					// 	//title: 'Server Error!',
					// 	body: 'Something went wrong.please try again later.',
					// 	showCloseButton: true,
					// 	timeout: 4000,
					// };
					// this.toasterService.pop(toast);
					this.presentToast('error', '');
					this.router.navigate(['pages/gamification/leaderboards']);
				}


			},

				resUserError => {
					this.loader = false;
					this.loader = false;
					// var toast: Toast = {
					// 	type: 'error',
					// 	//title: 'Server Error!',
					// 	body: 'Something went wrong.please try again later.',
					// 	showCloseButton: true,
					// 	timeout: 4000,
					// };
					// this.toasterService.pop(toast);
					this.presentToast('error', '');
					this.router.navigate(['pages/gamification/leaderboards']);
				});
	}


	/********************Notify****************** */
	fetchnotifytemplate() {

		let param = {
			//'nEventId': '5',
			'tid': this.tenantId,
			'aId': 11
		}

		this.addeditladdersservice.fetchdropdownsnotify(param)
			.then(rescompData => {

				var temp = rescompData;
				var arr: any = temp['data'][0];
				console.log('notifytemplate', rescompData);

				if (temp['type'] == true) {
					for (let i = 0; i < arr.length; i++) {
						if (arr[i].notModeId == 1) {
							var tempObj = {
								id: arr[i].templateId,
								name: arr[i].templateName,
								description: arr[i].description,
								template: arr[i].template,
							}
							this.templatesms.push(tempObj)
						} else if (arr[i].notModeId == 2) {
							var tempObj = {
								id: arr[i].templateId,
								name: arr[i].templateName,
								description: arr[i].description,
								template: arr[i].template,
							}
							this.templateemail.push(tempObj)
						} else if (arr[i].notModeId == 3) {
							var tempObj = {
								id: arr[i].templateId,
								name: arr[i].templateName,
								description: arr[i].description,
								template: arr[i].template,
							}
							this.templatenot.push(tempObj)
						}
					}

					console.log('this.templatesms', this.templatesms);
					console.log('this.templateemail', this.templateemail);
					console.log('this.templatenot', this.templatenot);

				} else {
					// var toast: Toast = {
					// 	type: 'error',
					// 	//title: 'Server Error!',
					// 	body: 'Something went wrong.please try again later.',
					// 	showCloseButton: true,
					// 	timeout: 4000,
					// };
					// this.toasterService.pop(toast);
					this.presentToast('error', '');
					this.router.navigate(['pages/gamification/leaderboards']);
				}





			},
				resUserError => {
					// var toast: Toast = {
					// 	type: 'error',
					// 	//title: 'Server Error!',
					// 	body: 'Something went wrong.please try again later.',
					// 	showCloseButton: true,
					// 	timeout: 4000,
					// };
					// this.toasterService.pop(toast);
					this.presentToast('error', '');
					this.router.navigate(['pages/gamification/leaderboards']);
				});

	}

	submitNotifications(notidetails, f) {
		if (f.valid) {
			this.spinner.show();
			this.getdatareadyforNotifications(notidetails);
			console.log('NotificationDetails:', notidetails);

			let param = {
				'areaId': 11,
				'instanceId': this.ladderIdStepsInsert,
				'tid': this.tenantId,
				'userId': this.userId,
				'nEventId': 5,
				'qOptions': this.rulename
			}
			if (!param.instanceId) {
				param.instanceId = this.getdateforaddedit[1].ladderId
			}
			console.log(param);
			this.addeditladdersservice.insertUpdateLaddersNotifications(param)
				.then(rescompData => {
					this.spinner.hide();
					var temp = rescompData;
					console.log('NotificationsSuccess:', rescompData);
					if (this.getdateforaddedit[0] == 'ADD') {
						if (temp['type'] == true) {
							// var notiInsert: Toast = {
							// 	type: 'success',
							// 	title: 'Notifications',
							// 	body: 'New Notifications has been inserted successfully.',
							// 	showCloseButton: true,
							// 	timeout: 4000,
							// };
							// this.toasterService.pop(notiInsert);
							this.presentToast('success', 'A new notification added');
							this.router.navigate(['pages/gamification/leaderboards']);
						} else {
							// var toast: Toast = {
							// 	type: 'error',
							// 	// title: 'Server Error!',
							// 	body: 'Something went wrong.please try again later.',
							// 	showCloseButton: true,
							// 	timeout: 4000,
							// };
							// this.toasterService.pop(toast);
							this.presentToast('error', '');
							this.router.navigate(['pages/gamification/leaderboards']);
						}
					} else if (this.getdateforaddedit[0] == 'EDIT') {
						if (temp['type'] == true) {
							// var notiInsert: Toast = {
							// 	type: 'success',
							// 	title: 'Notifications',
							// 	body: 'Notifications has been updated successfully.',
							// 	showCloseButton: true,
							// 	timeout: 4000,
							// };
							// this.toasterService.pop(notiInsert);
							this.presentToast('success', 'Notification updated');
							this.router.navigate(['pages/gamification/leaderboards']);
						} else {
							// var toast: Toast = {
							// 	type: 'error',
							// 	// title: 'Server Error!',
							// 	body: 'Something went wrong.please try again later.',
							// 	showCloseButton: true,
							// 	timeout: 4000,
							// };
							// this.toasterService.pop(toast);
							this.presentToast('error', '');
							this.router.navigate(['pages/gamification/leaderboards']);
						}
					}
				},
					resUserError => {
						this.spinner.hide();
						// var toast: Toast = {
						// 	type: 'error',
						// 	// title: 'Server Error!',
						// 	body: 'Something went wrong.please try again later.',
						// 	showCloseButton: true,
						// 	timeout: 4000,
						// };
						// this.toasterService.pop(toast);
						this.presentToast('error', '');
						this.router.navigate(['pages/gamification/leaderboards']);
					});
		} else {
			console.log('Please Fill all fields');
			// const addEditF: Toast = {
			// 	type: 'error',
			// 	title: 'Unable to update',
			// 	body: 'Please Fill all fields',
			// 	showCloseButton: true,
			// 	timeout: 4000,
			// };
			// this.toasterService.pop(addEditF);
			this.presentToast('warning', 'Please fill in the required fields');
			Object.keys(f.controls).forEach(key => {
				f.controls[key].markAsDirty();
			});
		}
	}

	rulename: any;
	ruledescription: any;
	notModeId: any;
	getdatareadyforNotifications(notidetails) {
		console.log(notidetails);
		if (!this.ladderIdStepsInsert) {
			this.ladderIdStepsInsert = this.getdateforaddedit[1].ladderId;
		}
		if (notidetails.lnmsg == true) {
			var modeidsms = 1;
			var templateidsms = notidetails.lnmsgtemp;
			var rulenamesms = 'ladder' + '_' + this.ladderIdStepsInsert + '_' + 'SMS';
			var ruledescsms = rulenamesms;
			var noteventid = 5;
			var templatemsg = this.formdataNotifications.editSmsTemplateData;

			var smsString = rulenamesms + '#' + ruledescsms + '#' + noteventid + '#' + modeidsms + '#' + templateidsms + '#' + templatemsg + '#' + 1;
		} else {
			modeidsms = null;
			templateidsms = null;
		}

		if (notidetails.lnemail == true) {
			var modeidemail = 2;
			var templateidemail = notidetails.lnemailTemp;
			var rulenameemail = 'ladder' + '_' + this.ladderIdStepsInsert + '_' + 'EMAIL';
			var ruledescemail = rulenameemail;
			var noteventid = 5;
			var templateemail = this.formdataNotifications.editEmailTemplateData;
			var emailString = rulenameemail + '#' + ruledescemail + '#' + noteventid + '#' + modeidemail + '#' + templateidemail + '#' + templateemail + '#' + 1;
		} else {
			modeidemail = null;
			templateidemail = null;
		}

		if (notidetails.lnnot == true) {
			var modeidnot = 3;
			var templateidnot = notidetails.lnnottemp;
			var rulenamenotify = 'ladder' + '_' + this.ladderIdStepsInsert + '_' + 'NOTIFY';
			var ruledescnotify = rulenamenotify;
			var noteventid = 5;
			var templatenotify = this.formdataNotifications.editNotTemplateData;
			var notifyString = rulenamenotify + '#' + ruledescnotify + '#' + noteventid + '#' + modeidnot + '#' + templateidnot + '#' + templatenotify + '#' + 1;
		} else {
			modeidnot = null;
			templateidnot = null;
		}

		if (modeidsms == null) {
			this.rulename = emailString + '|' + notifyString;
		} else if (modeidemail == null) {
			this.rulename = smsString + '|' + notifyString;
		} else if (modeidnot == null) {
			this.rulename = smsString + '|' + emailString;
		} else if (modeidsms == null && modeidemail == null) {
			this.rulename = notifyString;
		} else if (modeidemail == null && modeidnot == null) {
			this.rulename = smsString;
		} else if (modeidnot == null && modeidsms == null) {
			this.rulename = emailString;
		} else {
			this.rulename = smsString + '|' + emailString + '|' + notifyString;
		}

		//this.ruledescription = this.rulename;


	}

	notDetailsModal: boolean = false;
	templateData: any = {};
	getTempData(value, data) {
		this.spinner.show();
		console.log('previewData:', data);
		let param = {
			'tempId': data
		}

		this.notitempservice.getNottepById(param)
			.then(rescompData => {
				this.spinner.hide();
				var temp = rescompData;
				var data = temp['data'][0];
				var res = data[0];

				if (temp['type'] == true) {
					this.templateData = {
						name: res.tname,
						desc: res.description,
						template: res.template
					}
					console.log('notifytemplateById', res);

				} else {
					// var toast: Toast = {
					// 	type: 'error',
					// 	//title: 'Server Error!',
					// 	body: 'Something went wrong.please try again later.',
					// 	showCloseButton: true,
					// 	timeout: 4000,
					// };
					// this.toasterService.pop(toast);
					this.presentToast('error', '');
				}
			},
				resUserError => {
					this.spinner.hide();
					// var toast: Toast = {
					// 	type: 'error',
					// 	//title: 'Server Error!',
					// 	body: 'Something went wrong.please try again later.',
					// 	showCloseButton: true,
					// 	timeout: 4000,
					// };
					// this.toasterService.pop(toast);
					this.presentToast('error', '');
				});
		this.notDetailsModal = true;
	}

	closeNotDetailModal() {
		this.notDetailsModal = false;
		this.templateData = {};
	}

	/***************************stepsNew****************************** */
	browse(rid, ind) {
		if (rid == 1) {
			this.badgeData.currentid = ind;
			this.badgeshow = true;
			this.certshow = false;
		} else if (rid == 2) {
			this.certData.currentid = ind;
			this.certshow = true;
			this.badgeshow = false;
		}
	}

	activeSelectedBadgeId: any;
	condataBadge: any;
	setActiveSelectedBadge(currentIndex, currentBadge) {

		this.condataBadge = {};
		this.activeSelectedBadgeId = currentBadge.badgeId;
		this.condataBadge = currentBadge;

	}

	saveBadge(rid) {

		this.contdata = {};
		this.lcountData[this.badgeData.currentid].bimgsrc = this.condataBadge.badgeIcon;
		this.lcountData[this.badgeData.currentid].badgeid = this.condataBadge.badgeId;
		this.lcountData[this.badgeData.currentid].bname = this.condataBadge.badgeName;
		// this.lcountData[this.badgeData.currentid].certid = 0;
		// this.lcountData[this.badgeData.currentid].topoint = 0;

		this.contdata = this.condataBadge;
		console.log('this.contdata', this.contdata);
		console.log('this.lcountData', this.lcountData);

		this.badgeshow = false;
		this.badgeDiv = false;
	}

	removeBadge(index) {
		//  this.lcountData[this.badgeData.currentid] ={};
		this.lcountData[index].bimgsrc = '';
		this.lcountData[index].badgeid = '';
		this.lcountData[index].bname = '';
		this.badgeDiv = true;
	}


	activeSelectedCertId: any;
	condataCert: any;
	setActiveSelectedCert(currentIndex, currentCert) {

		this.condataCert = {};
		this.activeSelectedCertId = currentCert.id;
		this.condataCert = currentCert;

	}

	saveCert(rid) {

		this.contdata2 = {};
		this.lcountData[this.certData.currentid].cimgsrc = this.condataCert.certIcon;
		this.lcountData[this.certData.currentid].certid = this.condataCert.id;
		this.lcountData[this.certData.currentid].cname = this.condataCert.certName;
		// this.lcountData[this.badgeData.currentid].certid = 0;
		// this.lcountData[this.badgeData.currentid].topoint = 0;

		this.contdata2 = this.condataCert;
		console.log('this.contdata', this.contdata);
		console.log('this.lcountData', this.lcountData);

		this.certshow = false;
		this.certDiv = false;
	}

	removeCert(index) {
		this.lcountData[index].cimgsrc = '';
		this.lcountData[index].certid = '';
		this.lcountData[index].cname = '';
		this.certDiv = true;
	}

	tempData: any;
	checkPreviousvalueNo(data) {
		this.tempData = data;
		console.log('New Value:', data);
		if (this.getdateforaddedit[1].isFormula.toString()) {
			if (data != this.getdateforaddedit[1].isFormula.toString()) {
				console.log('Value not matched.');
				this.warning = true;
			}
		}
	}

	checkPreviousvalueYes(data) {
		this.tempData = data;
		console.log('New Value:', data);
		if (this.getdateforaddedit[1].isFormula.toString()) {
			if (data != this.getdateforaddedit[1].isFormula.toString()) {
				console.log('Value not matched.');
				this.warning = true;
			}
		}

	}

	WarningYes() {
		this.warning = false;
		this.formulaChange = true;
	}

	WarningNo() {
		this.warning = false;
		if (this.tempData == 0) {
			this.formdataDetails.lformula = '1';
		} else if (this.tempData == 1) {
			this.formdataDetails.lformula = '0';
		}
	}

	closeWarningModel() {
		this.warning = false;
	}

	onChangeDropDownSms($event) {
		let SelectedValue = (<HTMLInputElement>event.target).value;
		console.log(event);
		console.log(SelectedValue);

		this.formdataNotifications.reviewTextSms = true;
		for (let i = 0; i < this.templatesms.length; i++) {
			console.log(this.templatesms);
			if (SelectedValue == this.templatesms[i].id) {
				this.formdataNotifications.editSmsTemplateData = this.templatesms[i].template;
			}
		}
	}
	onChangeDropDownnot($event) {
		let SelectedValue = (<HTMLInputElement>event.target).value;
		console.log(event);
		console.log(SelectedValue);

		this.formdataNotifications.reviewnot = true;
		for (let i = 0; i < this.templatenot.length; i++) {
			if (SelectedValue == this.templatenot[i].id) {
				this.formdataNotifications.editNotTemplateData = this.templatenot[i].template;
			}
		}
	}
	onChangeDropDownemail($event) {
		let SelectedValue = (<HTMLInputElement>event.target).value;
		console.log(event);
		console.log(SelectedValue);

		this.formdataNotifications.reviewemail = true;
		for (let i = 0; i < this.templateemail.length; i++) {
			if (SelectedValue == this.templateemail[i].id) {
				this.formdataNotifications.editEmailTemplateData = this.templateemail[i].template;
			}
		}
	}

	// Help Code Start Here //

	helpContent: any;
	getHelpContent() {
		return new Promise(resolve => {
			this.http1
				.get('../../../../../../assets/help-content/addEditCourseContent.json')
				.subscribe(
					data => {
						this.helpContent = data;
						console.log('Help Array', this.helpContent);
					},
					err => {
						resolve('err');
					}
				);
		});
		// return this.helpContent;
	}

	// Help Code Ends Here //
}
