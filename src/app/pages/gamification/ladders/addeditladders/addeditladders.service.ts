import { Injectable, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AppConfig } from '../../../../app.module';
import { webApi } from '../../../../service/webApi';
import { HttpClient } from "@angular/common/http";

@Injectable()
export class addeditladdersService {

	private _urlInsertUpdate: string = webApi.domain + webApi.url.insertupdateladders;
	private _urlfetchdropdowns: string = webApi.domain + webApi.url.fetchddcoursedisplay;
	private _urlInsertUpdateLadderSteps: string = webApi.domain + webApi.url.insertupdateladdersteps;
	private _urlfetchnotifytemplatedropdown: string = webApi.domain + webApi.url.dropdownnotifytemplate;
	private _urlInsertUpdateLadderNotification: string = webApi.domain + webApi.url.insertupdateladdernotification;
	private _urlgetLadderSteps: string = webApi.domain + webApi.url.fetchLaddersSteps;
	private _urlgetLadderNotifications: string = webApi.domain + webApi.url.fetchLaddersNotifications;

	addeditLadderData: any = [];
	constructor(@Inject('APP_CONFIG_TOKEN') private config: AppConfig, private _http: Http,private _httpClient:HttpClient) {
		//this.busy = this._http.get('...').toPromise();
	}

	insertUpdateLadders(param) {
		// let url: any = this._urlInsertUpdate;
		// return this._http.post(url, param)
		// 	.map((response: Response) => response.json())
		// 	.catch(this._errorHandler);
		return new Promise(resolve => {
			this._httpClient.post(this._urlInsertUpdate, param)
			//.map(res => res.json())
			.subscribe(data => {
				resolve(data);
			},
			err => {
				resolve('err');
			});
		});
	}

	insertUpdateLaddersSteps(param) {
		// let url: any = this._urlInsertUpdateLadderSteps;
		// return this._http.post(url, param)
		// 	.map((response: Response) => response.json())
		// 	.catch(this._errorHandler);

		return new Promise(resolve => {
			this._httpClient.post(this._urlInsertUpdateLadderSteps, param)
			//.map(res => res.json())
			.subscribe(data => {
				resolve(data);
			},
			err => {
				resolve('err');
			});
		});
	}

	insertUpdateLaddersNotifications(param) {
		// let url: any = this._urlInsertUpdateLadderNotification;
		// return this._http.post(url, param)
		// 	.map((response: Response) => response.json())
		// 	.catch(this._errorHandler);

		return new Promise(resolve => {
			this._httpClient.post(this._urlInsertUpdateLadderNotification, param)
			//.map(res => res.json())
			.subscribe(data => {
				resolve(data);
			},
			err => {
				resolve('err');
			});
		});
	}

	fetchdropdowns(param) {
		// let url: any = this._urlfetchdropdowns;
		// return this._http.post(url, param)
		// 	.map((response: Response) => response.json())
		// 	.catch(this._errorHandler);

		return new Promise(resolve => {
			this._httpClient.post(this._urlfetchdropdowns, param)
			//.map(res => res.json())
			.subscribe(data => {
				resolve(data);
			},
			err => {
				resolve('err');
			});
		});
	}

	fetchdropdownsnotify(param) {
		// let url: any = this._urlfetchnotifytemplatedropdown;
		// return this._http.post(url, param)
		// 	.map((response: Response) => response.json())
		// 	.catch(this._errorHandler);

		return new Promise(resolve => {
			this._httpClient.post(this._urlfetchnotifytemplatedropdown, param)
			//.map(res => res.json())
			.subscribe(data => {
				resolve(data);
			},
			err => {
				resolve('err');
			});
		});
	}

	fetchladdersteps(param) {
		// let url: any = this._urlgetLadderSteps;
		// return this._http.post(url, param)
		// 	.map((response: Response) => response.json())
		// 	.catch(this._errorHandler);

		return new Promise(resolve => {
			this._httpClient.post(this._urlgetLadderSteps, param)
			//.map(res => res.json())
			.subscribe(data => {
				resolve(data);
			},
			err => {
				resolve('err');
			});
		});
	}

	fetchladdernotifications(param) {
		// let url: any = this. _urlgetLadderNotifications;
		// return this._http.post(url, param)
		// 	.map((response: Response) => response.json())
		// 	.catch(this._errorHandler);

		return new Promise(resolve => {
			this._httpClient.post(this. _urlgetLadderNotifications, param)
			//.map(res => res.json())
			.subscribe(data => {
				resolve(data);
			},
			err => {
				resolve('err');
			});
		});
	}

	_errorHandler(error: Response) {
		console.error(error);
		return Observable.throw(error || "Server Error")
	}


}
