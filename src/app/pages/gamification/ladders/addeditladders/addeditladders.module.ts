import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { NgxEchartsModule } from 'ngx-echarts';
 import { ThemeModule } from '../../../../@theme/theme.module';
import { addeditladders } from './addeditladders.component';
import { ReactiveFormsModule } from '@angular/forms';
import {addeditladdersService } from './addeditladders.service';
// import { CourseBundleService } from './courseBundle/courseBundle.service';
// import { CourseBundle } from './courseBundle/courseBundle';
// import { ContentService } from './courses/content/content.service';
import { CommonModule } from '@angular/common';
import { SortablejsModule } from 'angular-sortablejs';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import{ComponentsRoutingModule}from '../../../components/components-routing.module';
import { SuubHeader } from '../../../components/models/subheader.model';

// import { CourseBundleModule } from './courseBundle/courseBundle.module';
import { QuillModule } from 'ngx-quill';
import 'quill/dist/quill.core.css';
import 'quill/dist/quill.snow.css';
import { ComponentModule } from '../../../../component/component.module';

@NgModule({
  imports: [
     ThemeModule,
    NgxEchartsModule,
    CommonModule,
    SortablejsModule,
    ReactiveFormsModule,
    AngularMultiSelectModule,
    // CourseBundleModule
    QuillModule,
    ComponentsRoutingModule,
    ComponentModule
  ],
  declarations: [
    addeditladders,
    // CourseBundle
  ],
  providers: [
    addeditladdersService,
    // CourseBundleService,
    // ContentService
  ],
  schemas: [ 
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA 
  ]
})
export class addeditladdersModule { }
