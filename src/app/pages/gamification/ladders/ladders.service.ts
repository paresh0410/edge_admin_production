import {Injectable, Inject} from '@angular/core';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppConfig} from '../../../app.module';
import { webApi } from '../../../service/webApi';
import { HttpClient } from "@angular/common/http";

@Injectable()
export class laddersService {

  private _urlFetch:string = webApi.domain + webApi.url.fetchLadders;
  private _urlUpdateLadderStatus:string = webApi.domain + webApi.url.changeladderstatus;

  constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private _http: Http,private _httpClient:HttpClient){
      //this.busy = this._http.get('...').toPromise();
  }

  getLadders(param){
    // let url:any = this._urlFetch;
    // return this._http.post(url,param)
    //     .map((response:Response)=>response.json())
    //     .catch(this._errorHandler);

    return new Promise(resolve => {
      this._httpClient.post(this._urlFetch, param)
      //.map(res => res.json())
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}

UpdateLadderStatus(param){
  // let url:any = this._urlUpdateLadderStatus;
  // return this._http.post(url,param)
  //     .map((response:Response)=>response.json())
  //     .catch(this._errorHandler);
  return new Promise(resolve => {
    this._httpClient.post(this._urlUpdateLadderStatus, param)
    //.map(res => res.json())
    .subscribe(data => {
        resolve(data);
    },
    err => {
        resolve('err');
    });
});
}

_errorHandler(error: Response){
  console.error(error);
  return Observable.throw(error || "Server Error")
}



}
