import { Component, OnDestroy,ElementRef, ViewChild } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators/takeWhile';
import { Router, ActivatedRoute } from '@angular/router';
import { laddersService } from './ladders.service';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { addeditladdersService } from './addeditladders/addeditladders.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { webApi } from '../../../service/webApi';
import { Card } from '../../../models/card.model';
import { SuubHeader } from '../../components/models/subheader.model';
import { noData } from '../../../models/no-data.model';

interface CardSettings {
  title: string;
  iconClass: string;
  type: string;
}

@Component({
  selector: 'ngx-ladders',
  styleUrls: ['./ladders.component.scss'],
  templateUrl: './ladders.component.html',
})
export class ladders {
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  private alive = true;
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No Leaderboards at this time.",
    desc:"Leaderboards will appear after they are added by the admin. A leaderboard is a set of rules which ranks participants when they earn points after participating in any learning activity.",
    titleShow:true,
    btnShow:false,
    descShow:true,
    btnText:'Learn More',
    btnLink:'',
  }
  count:number=8
  cardModify: Card =   {
    flag: 'leaderboard',
    titleProp : 'ladderName',
    image: 'certIcon',
    hoverlable:   true,
    showImage: true,
    option:   true,
    eyeIcon:   true,
    bottomDiv:   true,
    bottomTitle:   true,
    btnLabel: 'Edit',
    defaultImage:'assets/images/courseicon.jpg'
  };
  header: SuubHeader  = {
    title:'Leaderboard',
    btnsSearch: true,
    searchBar: false,
    dropdownlabel: ' ',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: '',
    btnName3: '',
    btnAdd: 'Add Leaderboard',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: true,
    filter: false,
    showBreadcrumb: true,
    breadCrumbList:[
      {
        'name': 'Gamification',
        'navigationPath': '/pages/gamification',
      },]
  };
  // rows: any[] = [
  //   {
  //     id:1,
  //     name:'Admin'
  //   },
  //   {
  //     id:2,
  //     name:'Manager'
  //   },
  //   {
  //     id:3,
  //     name:'Student'
  //   },
  // ];
  ladderData = [
    {
      id: 1,
      name: 'ladder 1',
      des: 'Get all the information related to your ladder here'
    },
    {
      id: 2,
      name: 'ladder 2',
      des: 'Get all the information related to your ladder here'
    },
    //   {
    //   id:3,
    //   name:'ladder 3',
    //   des:'Get all the information related to your ladder here'
    // },
    //   {
    //   id:4,
    //   name:'ladder 4',
    //   des:'Get all the information related to your ladder here'
    // },
  ]

  rows: any[];

  selected = [];
  errorMsg: any;
  Nodatafound: boolean = false;
  enableDisableLadderData: any;
  enableDisableLadderModal: boolean = false;
  ladderDisableIndex: any;
  userLoginData: any;
  enableLadder: boolean = false;
  noladders: boolean = false;
  skeleton = false;
  data: any = [];
  userData;
  tenantId;
  title: string;
  btnName: string="Yes";
  constructor(private themeService: NbThemeService, public router: Router,
    public routes: ActivatedRoute,
    //  private toasterService: ToasterService,
    private ladderservice: laddersService, private addeditladderservice: addeditladdersService,
    private spinner: NgxSpinnerService, private toastr: ToastrService,
    private commonFunctionService: CommonFunctionsService,) {

    if (localStorage.getItem('LoginResData')) {
      this.userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', this.userData.data);
      // this.userId = this.userData.data.data.id;
      this.tenantId = this.userData.data.data.tenantId;
    }
    this.fetchLadders();
    this.fetchdropdowns();
  }

  presentToast(type, body) {
    if(type === 'success'){
      this.toastr.success(body, 'Success', {
        closeButton: false
       });
    } else if(type === 'error'){
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else{
      this.toastr.warning(body, 'Warning', {
        closeButton: false
        })
    }
  }

  back() {
    this.router.navigate(['/pages/gamification']);
  }

  fetchLadders() {
    // this.spinner.show();
    let param = {
      'tenantId': this.tenantId,
      'courseId': 0
    }
    const _urlFetch:string = webApi.domain + webApi.url.fetchLadders;
    this.commonFunctionService.httpPostRequest(_urlFetch,param)

    //this.ladderservice.getLadders(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('getladderResponse:', rescompData);
        if (result['type'] == true) {
          if (result['data'][0].length == 0) {
            this.skeleton = true;
            this.noladders = true;
          } else {
            this.rows = result['data'][0];
            this.addItems(0,this.sum,'push',this.rows);
          }
          console.log('this.rows', this.rows);
        } else {
          this.skeleton = true;
          this.Nodatafound = true;
          // this.spinner.hide();
          // var toast: Toast = {
          //   type: 'error',
          //   // title: 'Server Error!',
          //   body: 'Something went wrong.please try again later.',
          //   showCloseButton: true,
          //   timeout: 4000,
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }
        this.skeleton = true;

      }, error => {
        // this.spinner.hide();
        this.skeleton = true;
        this.Nodatafound = true;
        // var toast: Toast = {
        //   type: 'error',
        //   // title: 'Server Error!',
        //   body: 'Something went wrong.please try again later.',
        //   showCloseButton: true,
        //   timeout: 4000,
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });

  }


  disableLadder(currentIndex, ladderData, status) {
    this.enableDisableLadderData = ladderData;
    console.log('this.enableDisableLadderData', this.enableDisableLadderData)
    this.enableDisableLadderModal = true;
    this.ladderDisableIndex = currentIndex;
    if (this.enableDisableLadderData.visible == 0) {
      this.enableLadder = false;
    } else {
      this.enableLadder = true;
    }
  }

  clickTodisable(ladderData) {
    if (ladderData.visible == 1) {
      this.enableLadder = true;
      this.title='Disable leaderboard'
      this.enableDisableLadderData = ladderData;
      console.log('this.enableDisableLadderData', this.enableDisableLadderData);
      this.enableDisableLadderModal = true;
    } else {
      this.enableLadder = false;
      this.title='Enable leaderboard'
      this.enableDisableLadderData = ladderData;
      console.log('this.enableDisableLadderData', this.enableDisableLadderData);
      this.enableDisableLadderModal = true;
    }
  }

  enableDisableLadderAction(actionType) {
    if (actionType == true) {
      if (this.enableDisableLadderData.visible == 1) {
        var status = 0;
        var ladderData = this.enableDisableLadderData;
        this.enableDisableLadder(ladderData, status);

      } else {
        var status = 1;
        var ladderData = this.enableDisableLadderData;
        this.enableDisableLadder(ladderData, status);

      }
      this.closeEnableDisableLadderModal();
    } else {
      this.closeEnableDisableLadderModal();
    }
  }


  enableDisableLadder(ladderData, status) {
    this.spinner.show();
    console.log('ladderData', ladderData);
    let param = {
      'ladderId': ladderData.ladderId,
      'visible': status,

    }
    const _urlUpdateLadderStatus:string = webApi.domain + webApi.url.changeladderstatus;
    this.commonFunctionService.httpPostRequest(_urlUpdateLadderStatus,param)
    //this.ladderservice.UpdateLadderStatus(param)

      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('UpdateLadderResponse:', rescompData);
        if (result['type'] === true) {
          // var ladderUpdate: Toast = {
          //   type: 'success',
          //   title: 'Ladder',
          //   body: 'Ladder has been updated successfully.',
          //   showCloseButton: true,
          //   timeout: 4000,
          // };
          // this.toasterService.pop(ladderUpdate);
          this.presentToast('success', 'Ladder updated');
          this.fetchLadders();
        } else {
          // var ladderUpdate: Toast = {
          //   type: 'error',
          //   title: 'Ladder',
          //   body: 'Unable to update ladder.',
          //   showCloseButton: true,
          //   timeout: 4000,
          // };
          // this.toasterService.pop(ladderUpdate);
          this.presentToast('error', '');
        }
      }, error => {
        this.spinner.hide();
        // var toast: Toast = {
        //   type: 'error',
        //   // title: 'Server Error!',
        //   body: 'Something went wrong.please try again later.',
        //   showCloseButton: true,
        //   timeout: 4000,
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });

  }

  closeEnableDisableLadderModal() {
    this.enableDisableLadderModal = false;
  }

  gotoaddeditLadders() {

    // if (item == '') {
      var action = 'ADD';
      this.addeditladderservice.addeditLadderData[0] = action
      this.addeditladderservice.addeditLadderData[1] = null;
      this.addeditladderservice.addeditLadderData[2] = this.courses;
      this.addeditladderservice.addeditLadderData[3] = this.display;
      this.router.navigate(['/pages/gamification/leaderboards/add-edit-leaderboards']);
    // }
    //  else {
    //   var action = 'EDIT';
    //   this.addeditladderservice.addeditLadderData[0] = action
    //   this.addeditladderservice.addeditLadderData[1] = item;
    //   this.addeditladderservice.addeditLadderData[2] = this.courses;
    //   this.addeditladderservice.addeditLadderData[3] = this.display;
    //   this.router.navigate(['/pages/gamification/leaderboards/add-edit-leaderboards']);
    // }


  }

  gotoCardaddedit(item) {
    var action = 'EDIT';
    this.addeditladderservice.addeditLadderData[0] = action
    this.addeditladderservice.addeditLadderData[1] = item;
    this.addeditladderservice.addeditLadderData[2] = this.courses;
    this.addeditladderservice.addeditLadderData[3] = this.display;
    this.router.navigate(['/pages/gamification/leaderboards/add-edit-leaderboards']);
  }

  dropdisplay: any = [];
  dropcourse: any = [];
  display: any = [];
  courses: any = [];
  fetchdropdowns() {
    let param = {
      'tenantId': this.tenantId,
      'courseId': 0
    }
    const _urlfetchdropdowns: string = webApi.domain + webApi.url.fetchddcoursedisplay;
    this.commonFunctionService.httpPostRequest(_urlfetchdropdowns,param)
    //this.addeditladderservice.fetchdropdowns(param)
      .then(rescompData => {

        var result = rescompData;
        console.log('getdropdowns:', rescompData);
        if (result['type'] == true) {
          this.dropdisplay = result['data'][0];

          this.dropdisplay.forEach(element => {
            this.display.push(element);
          });
          console.log('this.display', this.display);


          this.dropcourse = result['data'][1];
          for (let i = 0; i < this.dropcourse.length; i++) {
            // this.courses[i].id = this.dropcourse[i].courseId,
            // this.courses[i].itemName = this.dropcourse[i].fullname
            var tempObj = {
              id: this.dropcourse[i].courseId,
              itemName: this.dropcourse[i].fullname,
            }
            this.courses.push(tempObj);
          }
          console.log('this.display', this.display);
          console.log('this.courses', this.courses);
        } else {
          // var toast: Toast = {
          //   type: 'error',
          //   // title: 'Server Error!',
          //   body: 'Something went wrong.please try again later.',
          //   showCloseButton: true,
          //   timeout: 4000,
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }


      }, error => {
        //   var toast : Toast = {
        //     type: 'error',
        //     //title: 'Server Error!',
        //     body: 'Something went wrong.please try again later.',
        //     showCloseButton: true,
        //     timeout: 4000
        // };
        // this.toasterService.pop(toast);
      });
  }
  ngOnDestroy(){
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0){
      e1[0].classList.add('with-scroll');
    }
  }
  ngAfterContentChecked() {
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0)
    {
      e1[0].classList.remove('with-scroll');
    }

   }
   conentHeight = '0px';
   offset: number = 100;
   ngOnInit() {
     this.conentHeight = '0px';
     const e = document.getElementsByTagName('nb-layout-column');
     console.log(e[0].clientHeight);
     // this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
     if (e[0].clientHeight > 1300) {
       this.conentHeight = '0px';
       this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
     } else {
       this.conentHeight = String(e[0].clientHeight) + 'px';
     }
   }
 
   sum = 40;
   addItems(startIndex, endIndex, _method, asset) {
     for (let i = startIndex; i < endIndex; ++i) {
       if (asset[i]) {
         this.displayArray[_method](asset[i]);
       } else {
         break;
       }
 
       // console.log("NIKHIL");
     }
   }
   // sum = 10;
   onScroll(event) {
 
     let element = this.myScrollContainer.nativeElement;
     // element.style.height = '500px';
     // element.style.height = '500px';
     // Math.ceil(element.scrollHeight - element.scrollTop) === element.clientHeight
 
     if (element.scrollHeight - element.scrollTop - element.clientHeight < 5) {
       if (this.newArray.length > 0) {
         if (this.newArray.length >= this.sum) {
           const start = this.sum;
           this.sum += 50;
           this.addItems(start, this.sum, 'push', this.newArray);
         } else if ((this.newArray.length - this.sum) > 0) {
           const start = this.sum;
           this.sum += this.newArray.length - this.sum;
           this.addItems(start, this.sum, 'push', this.newArray);
         }
       } else {
         if (this.rows.length >= this.sum) {
           const start = this.sum;
           this.sum += 50;
           this.addItems(start, this.sum, 'push', this.rows);
         } else if ((this.rows.length - this.sum) > 0) {
           const start = this.sum;
           this.sum += this.rows.length - this.sum;
           this.addItems(start, this.sum, 'push', this.rows);
         }
       }
 
 
       console.log('Reached End');
     }
   }
   newArray =[];
   displayArray =[];
    // this function is use to dynamicsearch on table
  searchData(event) {
    var temData = this.rows;
    var val = event.target.value.toLowerCase();
    var keys = [];
    if(event.target.value == '')
    {
      this.displayArray =[];
      this.newArray = [];
      this.sum = 50;
      this.addItems(0, this.sum, 'push', this.rows);
    }else{
         // filter our data
    if (temData.length > 0) {
      keys = Object.keys(temData[0]);
    }
    var temp = temData.filter((d) => {
      for (const key of keys) {
        if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
          return true;
        }
      }
    });

    // update the rows
    this.displayArray =[];
    this.newArray = temp;
    this.sum = 50;
    this.addItems(0, this.sum, 'push', this.newArray);
  }
}


}
