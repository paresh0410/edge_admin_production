import { Component, OnInit, ChangeDetectorRef,ElementRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Routes } from '@angular/router';
import { BadgesCategoryService } from './badge-cateory.service';
import { NgxSpinnerService } from 'ngx-spinner';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { webAPIService } from '../../../service/webAPIService';
import { webApi } from '../../../service/webApi';
import { BadgesService } from '../badges/badges.service'
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { Card } from '../../../models/card.model';
import { assessmentComponent } from '../../assessment/assessment.component';
import { SuubHeader } from '../../components/models/subheader.model';
import { noData } from '../../../models/no-data.model';

@Component({
  selector: 'ngx-badge-category',
  templateUrl: './badge-category.component.html',
  styleUrls: ['./badge-category.component.scss']
})
export class BadgeCategoryComponent implements OnInit {
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  popupModal: boolean = false;
  enableDisableBadgeCatModal: boolean = false;
  cardModify: Card =   {
    flag: 'budgetCategory',
    titleProp : 'cname',
    image: 'picRef',
    discrption: 'description',
    hoverlable:   true,
    showImage: true,
    option:   true,
    eyeIcon:   true,
    editIcon:   true,
    bottomDiv:   true,
    bottomTitle:   true,
    bottomDiscription:   true,
    btnLabel: 'Details',
    defaultImage: 'assets/images/survey.png'
  };
  count:number=8
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No Badge Categories at this time.",
    desc:"Categories will appear after they are added by the instructor. Categories are classification for managing the badges",
    titleShow:true,
    btnShow:true,
    descShow:true,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/gamification-badge-category',
  }
  header: SuubHeader  = {
    title:'Badges category',
    btnsSearch: true,
    placeHolder:'Search by category name',
    searchBar: true,
    dropdownlabel: ' ',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: '',
    btnName3: '',
    btnAdd: 'Add Category',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: true,
    filter: false,
    showBreadcrumb: true,
    breadCrumbList:[
      {
        'name': 'Gamification',
        'navigationPath': '/pages/gamification',
      },]
  };
  badgeTitle:string='Add Category'
	btnName: string = 'Save';
  noCategory: boolean = false;
  noDataFound: boolean = false;
  addAction: boolean = false;
  editAction: boolean = false;
  skeleton = false;
  rows: any;
  category: any = {};
  userId: any;
  enableCat: boolean = false;
  tenantId;
  searchtext: string;
  badgecategory: any;
  search: any;
  str: string;
  currentPage: number;
  title: string;
  constructor(public router: Router, private badgecategoryservice: BadgesCategoryService,
    private spinner: NgxSpinnerService, private toastr: ToastrService,
    // private toasterService: ToasterService, 
    private routes: ActivatedRoute,
    protected webApiService: webAPIService, private badgeservice: BadgesService,
    private commonFunctionService: CommonFunctionsService,
    private http1: HttpClient, private cdf: ChangeDetectorRef) {
    this.getHelpContent();
    if (localStorage.getItem('LoginResData')) {
      var userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', userData.data);
      this.userId = userData.data.data.id;
      this.tenantId = userData.data.data.tenantId;
    }
    this.fetchBadgeCateory();
  }

  // ngOnInit() {
  // }

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }

  fetchBadgeCateory() {
    // this.spinner.show();
    this.displayArray =[];
    let param = {
      'tenantId': this.tenantId,
    }
    const _urlFetch:string = webApi.domain + webApi.url.fetchbadgecategories;
    this.commonFunctionService.httpPostRequest(_urlFetch,param)
    //this.badgecategoryservice.getBadgesCategory(param)
      .then(rescompData => {
        // this.spinner.hide();
        var result = rescompData;
        console.log('getBadgeCategoryResponse:', rescompData);
        if (result['type'] == true) {
          if (result['data'][0].length === 0) {
            this.skeleton = true;
            this.noCategory = true;
          } else {
            this.rows = result['data'][0],
            this.badgecategory = result['data'][0];
            this.addItems(0,this.sum,'push',this.badgecategory);
            this.noCategory = false;
              console.log('this.rows', this.rows);
          }

        } else {
          this.spinner.hide();
          this.skeleton = false;
          this.noDataFound = true;
          // var toast: Toast = {
          //   type: 'error',
          //   //title: 'Server Error!',
          //   body: 'Something went wrong.please try again later.',
          //   showCloseButton: true,
          //   timeout: 4000,
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }

        this.skeleton = true;
      }, error => {
        this.spinner.hide();
        this.skeleton = true;
        this.noDataFound = true;
        // var toast: Toast = {
        //   type: 'error',
        //   //title: 'Server Error!',
        //   body: 'Something went wrong.please try again later.',
        //   showCloseButton: true,
        //   timeout: 4000,
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });

  }
  clear() {
    if(this.header.searchtext){
    // this.search = {};
    // this.header.searchtext = ''
    // this.search = {};
    // this.str = '';
    // this.currentPage = 1;
    this.noCategory=false
    this.header.searchtext = '';
    this.searchtext = this.header.searchtext;
    this.fetchBadgeCateory();
    }
  }

  back() {
    window.history.back();
    // this.router.navigate(['/pages/gamification']);
  }

  closeModal() {
    this.popupModal = false;
  }
  gotoaddedit(data, value) {
    console.log(data)
    if (value == 0) {
      this.category = {};
      this.addAction = true;
      this.popupModal = true;
    } else if (value == 1) {
      this.addAction = false;
      this.popupModal = true;
      this.category = {
        id: data.id,
        name: data.cname,
        desc: data.description,
        picRef: data.picRef
      }
    }

  }

  gotoCardaddedit(data) {
    this.addAction = false;
    this.badgeTitle='Edit Category'
    this.popupModal = true;
    this.category = {
      id: data.id,
      name: data.cname,
      desc: data.description,
      picRef: data.picRef
  }
 }


  closeEnableDisableBadgeCatModal() {
    this.enableDisableBadgeCatModal = false;
  }

  catName: any;
  catIdStatus: any;
  catVisibleStatus: any;
  disableCategory(value, data) {
    console.log('data', data);
    if (value == 1) {
      this.title=''
      this.enableCat = false;
      this.catName = data.cname;
      this.enableDisableBadgeCatModal = true;
      this.catIdStatus = data.id;
      this.catVisibleStatus = 0;
      // this.changeBadgeStatus(data.id,visible);
    } else if (value == 2) {
      this.enableCat = true;
      this.catName = data.cname;
      this.enableDisableBadgeCatModal = true;
      this.catIdStatus = data.id;
      this.catVisibleStatus = 1;
      //this.changeBadgeStatus(data.id,visible);
    }

  }
  visibleStatus : any;
  clickTodisable(data) {
    this.btnName='Yes'
    if (data.visible == 1) {
      this.title='Disable Category'
      this.enableCat = false;
      this.catName = data.cname;
      this.enableDisableBadgeCatModal = true;
      this.catIdStatus = data.id;
      this.catVisibleStatus = 0;
      // this.changeBadgeStatus(data.id,visible);
    } else if (data.visible == 0) {
      this.title='Enable Category'
      this.enableCat = true;
      this.catName = data.cname;
      this.enableDisableBadgeCatModal = true;
      this.catIdStatus = data.id;
      this.catVisibleStatus = 1;
      //this.changeBadgeStatus(data.id,visible);
    }
    this.visibleStatus = this.catVisibleStatus;


  }

  enableDisableBadgeCategoryAction(value) {
    
    if (value == 1) {
      this.changeBadgeStatus(this.catIdStatus, this.catVisibleStatus);
      this.closeEnableDisableBadgeCatModal();
    } else if (value == 2) {
      this.closeEnableDisableBadgeCatModal();
      this.catIdStatus = null,
      this.catVisibleStatus = null
    }
  }

  changeBadgeStatus(cid, status) {
    this.spinner.show();
    let param = {
      'cId': cid,
      'cvisible': status
    }
    const  _urlChangeBadgeCategoryStatus:string = webApi.domain + webApi.url.changebadgecategorystatus;
    this.commonFunctionService.httpPostRequest(_urlChangeBadgeCategoryStatus,param)
    //this.badgecategoryservice.changeBadgesCategoryStatus(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('UpdateBadgeCategoryStatusResponse:', rescompData);
        if (result['type'] == true) {
          // var catUpdate: Toast = {
          //   type: 'success',
          //   title: 'Badge',
          //   body: 'Badge has been updated successfully.',
          //   showCloseButton: true,
          //   timeout: 4000,
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('success', 'Badge updated successfully');
          this.fetchBadgeCateory();
        } else {
          // var catUpdate: Toast = {
          //   type: 'error',
          //   title: 'Badge',
          //   body: 'Unable to update Badge.',
          //   showCloseButton: true,
          //   timeout: 4000,
          // };
          // this.toasterService.pop(catUpdate);
          this.presentToast('error', '');
        }
      }, error => {
        this.spinner.hide();
        // var toast: Toast = {
        //   type: 'error',
        //   //title: 'Server Error!',
        //   body: 'Something went wrong.please try again later.',
        //   showCloseButton: true,
        //   timeout: 4000,
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
  }

  badgeCatImgData: any;
  fileexterror: boolean = false;
  readBadgeCategoryThumb(event: any) {
    this.fileexterror = false;
    var validExts = new Array('.PNG','.png', '.jpg', '.jpeg');
    var fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
      this.fileexterror = true;
    } else {
      if (event.target.files && event.target.files[0]) {
        this.badgeCatImgData = event.target.files[0];

        var reader = new FileReader();

        reader.onload = (event: ProgressEvent) => {
          // this.defaultThumb = (<FileReader>event.target).result;
          //this.formdata.coursePicRef = null;
          this.category.picRef = (<FileReader>event.target).result;
          // this.cdf.detectChanges();
        }
        reader.readAsDataURL(event.target.files[0]);
      }
    }
  }

  deleteBadgeCategoryThumb() {
    this.badgeCatImgData = null;
    this.category.picRef = null;
  }

  passParams: any;
  fileres: any;
  fileUploadRes: any;
  errorMsg: any;
  Submit(data, f) {
    if (f.valid) {
      this.spinner.show();
      console.log('Data:', data);
      let param = {
        'cId': this.addAction ? 0 : this.category.id,
        'cname': data.name,
        'cdescription': data.desc,
        // 'picRef':this.badgeCatImgData ? this.badgeCatImgData : null,
        'picRef': data.picRef?data.picRef:null,
        'cvisible': 1,
        'tId': this.tenantId,
        'userId': this.userId,
      }
      this.passParams = param;
      console.log('param', param);

      var fd = new FormData();
      fd.append('content', JSON.stringify(this.passParams));
      fd.append('file', this.badgeCatImgData);
      console.log('File Data ', fd);
      console.log('Course Data Img', this.badgeCatImgData);
      console.log('Course Data ', this.passParams);
      let fileUploadUrl = webApi.domain + webApi.url.fileUpload;

      if (this.badgeCatImgData) {
        this.webApiService.getService(fileUploadUrl, fd)
          .then(rescompData => {
            var temp: any = rescompData;
            this.fileUploadRes = temp;
            console.log('rescompData', this.fileUploadRes)
            this.badgeCatImgData = null;
            if (temp === 'err') {
              // this.notFound = true;
              // var thumbUpload: Toast = {
              //   type: 'error',
              //   title: 'Badge',
              //   body: 'Unable to upload Badge Category thumbnail.',
              //   // body: temp.msg,
              //   showCloseButton: true,
              //   timeout: 4000,
              // };
              // this.toasterService.pop(thumbUpload);
              this.presentToast('error', '');
              this.spinner.hide();
            } else if (temp.type === false) {
              // var thumbUpload: Toast = {
              //   type: 'error',
              //   title: 'Badge',
              //   body: 'Unable to upload Badge Category thumbnail.',
              //   // body: temp.msg,
              //   showCloseButton: true,
              //   timeout: 4000,
              // };
              // this.toasterService.pop(thumbUpload);
              this.presentToast('error', '');
              this.spinner.hide();
            } else {
              if (this.fileUploadRes.data != null || this.fileUploadRes.fileError != true) {
                this.passParams.picRef = this.fileUploadRes.data.file_url;
                console.log('this.passparams.picRef', this.passParams.picRef);
                this.spinner.hide();
                this.addUpdateBadge(this.passParams);
              } else {
                // var thumbUpload: Toast = {
                //   type: 'error',
                //   title: 'Badge',
                //   // body: 'Unable to upload course thumbnail.',
                //   body: this.fileUploadRes.status,
                //   showCloseButton: true,
                //   timeout: 4000,
                // };
                // this.toasterService.pop(thumbUpload);
                this.presentToast('error', '');
                this.spinner.hide();
              }
            }
            console.log('File Upload Result', this.fileUploadRes);
            var res = this.fileUploadRes;
            this.fileres = res.data.file_url;
          },
            resUserError => {
              this.errorMsg = resUserError;
              console.log('File upload this.errorMsg', this.errorMsg);
            });
      } else {
        // this.passparams.iconRef = this.badge.ico;
        this.spinner.hide();
        this.addUpdateBadge(this.passParams);
      }


    } else {
      console.log('Please Fill all fields');
      // const addEditF: Toast = {
      //   type: 'error',
      //   title: 'Unable to update',
      //   body: 'Please Fill all fields',
      //   showCloseButton: true,
      //   timeout: 4000
      // };
      // this.toasterService.pop(addEditF);
      Object.keys(f.controls).forEach(key => {
        f.controls[key].markAsDirty();
      });
    }
  }

  badgeCatAddEditRes: any;
  addUpdateBadge(params) {
    this.closeModal();
    console.log('Newparams', params);
    this.spinner.show();
    const _urlInsertUpdateBadgeCategory:string = webApi.domain + webApi.url.insertupdatebadgecategory;
    this.commonFunctionService.httpPostRequest(_urlInsertUpdateBadgeCategory,params)
    //this.badgecategoryservice.insertUpdateBadgesCategory(params)
      .then(rescompData => {

        this.spinner.hide();

        var temp = rescompData;
        this.badgeCatAddEditRes = temp['data'];
        if (params.cId === 0) {
          if (temp['type'] === true) {
            // this.closeEnableDisableBadgeModal();
            this.displayArray =[];
            this.newArray =[];
            this.fetchBadgeCateory();
            this.cdf.detectChanges();

            console.log('Badge Category Add Result ', this.badgeCatAddEditRes);
            // var catUpdate: Toast = {
            //   type: 'success',
            //   title: 'Badge',
            //   body: 'New Badge Category has been added successfully.',
            //   showCloseButton: true,
            //   timeout: 4000,
            // };
            // this.toasterService.pop(catUpdate);
            this.presentToast('success', 'New badge category added successfully');

          } else {
            //  this.closeEnableDisableBadgeModal();
            this.fetchBadgeCateory();
            this.cdf.detectChanges();

            // var catUpdate: Toast = {
            //   type: 'error',
            //   title: 'Badge',
            //   body: 'Unable to add Badge Category.',
            //   showCloseButton: true,
            //   timeout: 4000,
            // };
            // this.toasterService.pop(catUpdate);
            this.presentToast('error', '');
          }
        } else {
          console.log('Badge Category Edit Result ', this.badgeCatAddEditRes)
          if (temp['type'] === true) {
            // this.closeEnableDisableBadgeModal();
            this.fetchBadgeCateory();
            this.cdf.detectChanges();
            // var catUpdate: Toast = {
            //   type: 'success',
            //   title: 'Badge',
            //   body: 'Badge category has been updated successfully.',
            //   showCloseButton: true,
            //   timeout: 4000,
            // };
            // this.toasterService.pop(catUpdate);
            this.presentToast('success', 'Badge category updated');
          } else {
            //this.closeEnableDisableBadgeModal();
            this.fetchBadgeCateory();
            // var catUpdate: Toast = {
            //   type: 'error',
            //   title: 'Badge',
            //   body: 'Unable to update Badge category.',
            //   showCloseButton: true,
            //   timeout: 4000,
            // };
            // this.toasterService.pop(catUpdate);
            this.presentToast('error', '');
          }
        }

      },
        resUserError => {
          this.spinner.hide();
          this.errorMsg = resUserError;
        });
  }

  gotoBadges(item) {
    console.log('id', item.id);
    this.badgeservice.categoryId = item.id;
    this.badgeservice.catName = item.cname;
    // this.router.navigate(['/pages/gamification/badges-category/badges']);
    this.router.navigate(['badges'], { relativeTo: this.routes });

  }

  gotoCardBadges(item) {
    console.log('id', item.id);
    this.badgeservice.categoryId = item.id;
    this.badgeservice.catName = item.cname;
    // this.router.navigate(['/pages/gamification/badges-category/badges']);
    this.router.navigate(['badges'], { relativeTo: this.routes });
  }

  SarchFilter(event, searchtext) {
    searchtext= event.target.value;
    console.log(searchtext);
    const val = searchtext.toLowerCase();
    const tempcc = this.rows.filter(function (d) {
      return String(d.cname).toLowerCase().indexOf(val) !== -1 ||
        d.description.toLowerCase().indexOf(val) !== -1 ||
        // d.relayDate.toLowerCase().indexOf(val) !== -1 ||
        // d.manager.toLowerCase().indexOf(val) !== -1 ||
        !val
    })
    console.log(tempcc);
    this.badgecategory = tempcc;
    if (!this.badgecategory.length) {
      this.noCategory = true;
    } else {
      this.noCategory = false;
    }
  }

  // Help Code Start Here //

  helpContent: any;
  getHelpContent() {
    return new Promise(resolve => {
      this.http1
        .get('../../../../../../assets/help-content/addEditCourseContent.json')
        .subscribe(
          data => {
            this.helpContent = data;
            console.log('Help Array', this.helpContent);
          },
          err => {
            resolve('err');
          }
        );
    });
    // return this.helpContent;
  }
  // onSearch(event){
  //   this.search.cname = event.target.value
  //   this.header.searchtext = '';
  //   this.searchtext = this.header.searchtext;

  // }
  // Help Code Ends Here //
  ngOnDestroy(){
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0){
      e1[0].classList.add('with-scroll');
    }
  }
  ngAfterContentChecked() {
    let e1 = document.getElementsByTagName('nb-layout');
    if(e1 && e1.length !=0)
    {
      e1[0].classList.remove('with-scroll');
    }

   }
   conentHeight = '0px';
   offset: number = 100;
   ngOnInit() {
     this.conentHeight = '0px';
     const e = document.getElementsByTagName('nb-layout-column');
     console.log(e[0].clientHeight);
     // this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
     if (e[0].clientHeight > 1300) {
       this.conentHeight = '0px';
       this.conentHeight = String(e[0].clientHeight - this.offset) + 'px';
     } else {
       this.conentHeight = String(e[0].clientHeight) + 'px';
     }
   }
 
   sum = 40;
   addItems(startIndex, endIndex, _method, asset) {
     for (let i = startIndex; i < endIndex; ++i) {
       if (asset[i]) {
         this.displayArray[_method](asset[i]);
       } else {
         break;
       }
 
       // console.log("NIKHIL");
     }
   }
   // sum = 10;
   onScroll(event) {
 
     let element = this.myScrollContainer.nativeElement;
     // element.style.height = '500px';
     // element.style.height = '500px';
     // Math.ceil(element.scrollHeight - element.scrollTop) === element.clientHeight
 
     if (element.scrollHeight - element.scrollTop - element.clientHeight < 5) {
       if (this.newArray.length > 0) {
         if (this.newArray.length >= this.sum) {
           const start = this.sum;
           this.sum += 50;
           this.addItems(start, this.sum, 'push', this.newArray);
         } else if ((this.newArray.length - this.sum) > 0) {
           const start = this.sum;
           this.sum += this.newArray.length - this.sum;
           this.addItems(start, this.sum, 'push', this.newArray);
         }
       } else {
         if (this.badgecategory.length >= this.sum) {
           const start = this.sum;
           this.sum += 50;
           this.addItems(start, this.sum, 'push', this.badgecategory);
         } else if ((this.badgecategory.length - this.sum) > 0) {
           const start = this.sum;
           this.sum += this.badgecategory.length - this.sum;
           this.addItems(start, this.sum, 'push', this.badgecategory);
         }
       }
 
 
       console.log('Reached End');
     }
   }
   newArray =[];
   displayArray =[];
    // this function is use to dynamicsearch on table
  searchData(event) {
    var temData = this.badgecategory;
    var val = event.target.value.toLowerCase();
    var keys = [];
    if(event.target.value == '')
    {
      this.noCategory = false;
      this.displayArray =[];
      this.newArray = [];
      this.sum = 50;
      this.addItems(0, this.sum, 'push', this.badgecategory);
    }else if(val.length>=3||val.length==0){
         // filter our data
    // if (temData.length > 0) {
    //   keys = Object.keys(temData[0]);
    // }
    // var temp = temData.filter((d) => {
    //   for (const key of keys) {
    //     if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
    //       return true;
    //     }
    //   }
    // });
    const temp = this.badgecategory.filter(function (d) {
      return String(d.cname).toLowerCase().indexOf(val) !== -1 ||
        d.description.toLowerCase().indexOf(val) !== -1 ||
        // d.relayDate.toLowerCase().indexOf(val) !== -1 ||
        // d.manager.toLowerCase().indexOf(val) !== -1 ||
        !val
    })
    // update the rows
    this.displayArray =[];
    this.newArray = temp;
    this.sum = 50;
    this.addItems(0, this.sum, 'push', this.newArray);
    if (!this.newArray.length) {
      this.noCategory = true;
      this.noDataVal={
        margin:'mt-5',
        imageSrc: '../../../../../assets/images/no-data-bg.svg',
        title:"Sorry we couldn't find any matches please try again",
        desc:".",
        titleShow:true,
        btnShow:false,
        descShow:false,
        btnText:'Learn More',
        btnLink:''
      }
    } else {
      this.noCategory = false;
    }
  }
}

}
