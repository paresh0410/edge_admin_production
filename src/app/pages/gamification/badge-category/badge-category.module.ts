import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ThemeModule } from '../../../@theme/theme.module';
import { ReactiveFormsModule } from '@angular/forms';
import { BadgesCategoryService } from './badge-cateory.service';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgxContentLoadingModule } from 'ngx-content-loading';
import { ComponentModule } from '../../../component/component.module';


@NgModule({
    imports: [
       ThemeModule,
        ReactiveFormsModule,
        NgxSkeletonLoaderModule,
        NgxContentLoadingModule,
        ComponentModule,
    ],
    declarations: [
    ],
    providers: [
        BadgesCategoryService,
      // CourseBundleService,
      // ContentService
    ],
    schemas: [ 
      CUSTOM_ELEMENTS_SCHEMA,
      NO_ERRORS_SCHEMA 
    ]
  })
  export class BadgesCategoryModule { }