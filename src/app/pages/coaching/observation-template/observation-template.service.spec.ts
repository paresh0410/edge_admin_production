import { TestBed } from '@angular/core/testing';

import { ObservationTemplateService } from './observation-template.service';

describe('ObservationTemplateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ObservationTemplateService = TestBed.get(ObservationTemplateService);
    expect(service).toBeTruthy();
  });
});
