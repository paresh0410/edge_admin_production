import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { SortablejsOptions } from "angular-sortablejs";
import { ObservationTemplateService } from './observation-template.service';
import { NgxSpinnerService } from 'ngx-spinner';
// import { ToasterService } from 'angular2-toaster';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
import { assessmenttemplateservice } from '../assessment-template/assessmnet-template.service';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { webApi } from '../../../service/webApi';
import { SuubHeader } from '../../components/models/subheader.model';
import { noData } from '../../../models/no-data.model';

@Component({
  selector: 'ngx-observation-template',
  templateUrl: './observation-template.component.html',
  styleUrls: ['./observation-template.component.scss']
})
export class ObservationTemplateComponent implements OnInit {
  spectrListF: any;
  enableDeleteModal: boolean=false;
  delData: any;
  btnName: string = 'Submit';
  title:string='Add Pre-Assessment Template';
  pressedOnce: boolean=true;
  constructor(private router: Router,
    private observationService: ObservationTemplateService,
    private spinner: NgxSpinnerService, public assessservice: assessmenttemplateservice,
    // private toasterService: ToasterService,
     private toastr: ToastrService, private http1: HttpClient,private commonFunctionService: CommonFunctionsService,
  ) {
    this.getHelpContent();
    this.tenantId = this.observationService.tenantId;
    this.settingsAreaDrop = {
      text: "Select Evaluation area",
      // singleSelection: true,
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      classes: "common-multi",
      primaryKey: "areaId",
      labelKey: "areaName",
      noDataLabel: "Search Area Evaluation...",
      enableSearchFilter: true,
      searchBy: ['areaId', 'areaName'],
      maxHeight:250,
      lazyLoading: true,
    };

    this.settingsCompenDrop = {
      text: "Select Competency",
      // singleSelection: true,
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      classes: "common-multi",
      primaryKey: "compId",
      labelKey: "compName",
      noDataLabel: "Search Competency...",
      enableSearchFilter: true,
      searchBy: ['compId', 'compName'],
      maxHeight:250,
      lazyLoading: true,
    };

    this.settingsSpectrDrop = {
      text: "Select Spectr",
      // singleSelection: true,
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      classes: "common-multi",
      primaryKey: "spectrId",
      labelKey: "spectrName",
      noDataLabel: "Search Spectr...",
      enableSearchFilter: true,
      searchBy: ['spectrId', 'spectrName'],
      maxHeight:250,
      lazyLoading: true,
    };

  }

  @ViewChild('observationForm') SecForm: NgForm;
  showObservTemp: boolean = false;
  questions = [];

  deleteIcon: boolean = true;

  ObservationEmpRows: any = [];
  allObservationEmployees: any[];
  tenantId: any = [];
  observation: any = [];
  noObservation: boolean = false;
  settingsCompenDrop = {};
  settingsAreaDrop = {};

  settingsSpectrDrop = {};
  spectrList: any = [];
  spectrListDrop: any = [];
  selectedSpectr: any = [];
  selectedCompetency: any = [];
  competencyList: any = [];
  areaList: any = [];
  areaListF: any = [];
  competencyListF: any = [];
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No pre-assessment templates are available at this time.",
    desc:"Pre-assessment templates will appear after they are added by the admin . The template will provide a blueprint to the coach to conduct assessment of the coachee",
    titleShow:true,
    btnShow:true,
    descShow:true,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/cc-call-add-pre-assessment',
  }
  header: SuubHeader  = {
    title:'Pre-Assessment',
    btnsSearch: true,
    searchBar: false,
    dropdownlabel: ' ',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: '',
    btnName3: '',
    btnAdd: 'Add Pre-Assessment',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: true,
    filter: false,
    showBreadcrumb: true,
    breadCrumbList:[
      {
        'name': 'Coaching',
        'navigationPath': '/pages/coaching',
      },]
  };
  //dropdown data

  spectrArr: any = [];
  compNameArr: any = [];
  areaNameArr: any = [];
  selectedArea: any = [];

  spectrId: any = [];
  spectrName: any = [];
  compName: any = [];
  compId: any = [];
  areaId: any = [];
  areaName: any = [];
  assessNames: any = [];
  observationList: any = [
    {
      "specterName": "spec 1",
      "competencyName": "comp 1",
      "evaluationAreaName": "evalu 1",

    },
    {
      "specterName": "spec 2",
      "competencyName": "comp 2",
      "evaluationAreaName": "evalu 2",

    },
    {
      "specterName": "spec 3",
      "competencyName": "comp 3",
      "evaluationAreaName": "evalu 3",

    },
  ]
  userDetails: any = [];
  labels: any = [
    { labelname: 'Spectr', bindingProperty: 'spectrNames', componentType: 'text' },
    { labelname: 'Competency', bindingProperty: 'compNames', componentType: 'text' },
    { labelname: 'Evaluation Area', bindingProperty: 'evalAreaNames', componentType: 'text' },
    { labelname: 'Action', bindingProperty: 'btntext', componentType: 'button' },
    { labelname: 'Delete', bindingProperty: 'btndelete', componentType: 'delete' }
  ]
  ngOnInit() {
    this.userDetails = JSON.parse(localStorage.getItem('LoginResData'));
    // this.tenantId = this.observationService.tenantId;
    this.getAllObsevations();
    this.getDropDown();

  }
  dropdownSpectors: any = [];

  getDropDown() {
    this.spinner.show();
    const param = {
      tId: this.tenantId
    }
    const _urlDropdownObservation: string = webApi.domain + webApi.url.getDropdownObservation;
    this.commonFunctionService.httpPostRequest(_urlDropdownObservation,param)
    // this.observationService.dropdownObservation(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('getAllDropdown:', rescompData);
        if (result['type'] == true) {
          console.log('getAllDropdown:', rescompData);
          this.spectrList = result['data'][0];
          this.spectrListF = result['data'][0];
          this.competencyList = result['data'][1];
          this.competencyListF = result['data'][1];
          this.areaList = result['data'][2];
          this.areaListF = result['data'][2];
        } else {
          // var toast: any = {
          //   type: 'error',
          //   //title: 'Server Error!',
          //   body: 'Something went wrong.please try again later.',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.toastr.warning('Warning', 'Something went wrong.please try again later.');
        }


      }, error => {
        this.spinner.hide();
        this.presentToast('error', '');
      });
    // console.log("dropdownLIst", this.dropSpectrList);

  }
  onSpectrSelect(item: any) {
    console.log(item);
    console.log(this.selectedSpectr);

  }
  OnSpectrDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedSpectr);
  }
  onSelectAllSpectr(item: any) {
    console.log(item);
    console.log(this.selectedSpectr);
  }
  onDeSelectAllSpectr(item: any) {
    console.log(item);
    console.log(this.selectedSpectr);
  }

  onSpectrSearch(evt: any) {
    console.log(evt.target.value);
    const val = evt.target.value;
    if (val) {
      const tuser = this.spectrList.filter(function (d) {
        return String(d.spectrId).toLowerCase().indexOf(val) !== -1 ||
          d.spectrName.toLowerCase().indexOf(val) !== -1 || !val;
      });

      // // update the rows
      this.spectrList = tuser;
      console.log("spctrList", this.spectrList);
    } else {
      this.spectrList = this.spectrListF;
    }
  }


  onCompenSelect(item: any) {
    console.log(item);
    console.log(this.selectedCompetency);
  }

  OnCompenDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedCompetency);
  }

  onSelectAllCompen(item: any) {
    console.log(item);
    console.log(this.selectedCompetency);
  }
  onDeSelectAllCompen(item: any) {
    console.log(item);
    console.log(this.selectedCompetency);
  }
  onCompenSearch(evt: any) {
    console.log(evt.target.value);
    const val = evt.target.value;
    if (val) {
      const tuser = this.competencyList.filter(function (d) {
        return String(d.compId).toLowerCase().indexOf(val) !== -1 ||
          d.compName.toLowerCase().indexOf(val) !== -1 || !val;
      });

      // // update the rows
      this.competencyList = tuser;
      console.log("spctrList", this.competencyList);
    } else {
      this.competencyList = this.competencyListF;
    }
  }

  onAreaSelect(item: any) {
    console.log(item);
    console.log(this.selectedArea);
  }

  onAreaDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedArea);
  }
  onSelectAllarea(item: any) {
    console.log(item);
    console.log(this.selectedArea);
  }
  onDeSelectAllarea(item: any) {
    console.log(item);
    console.log(this.selectedArea);
  }
  onAreaSearch(evt: any) {
    console.log(evt.target.value);
    const val = evt.target.value;
    if (val) {
      const tuser = this.areaList.filter(function (d) {
        return String(d.areaId).toLowerCase().indexOf(val) !== -1 ||
          d.areaName.toLowerCase().indexOf(val) !== -1 || !val;
      });

      // // update the rows
      this.areaList = tuser;
      console.log("areaList", this.areaList);
    } else {
      this.areaList = this.areaListF;
    }
  }

  QuestionListOptions: SortablejsOptions = {
    group: {
      name: "questions"
      // pull: 'clone',
      // put: false,
    },
    sort: true,
    // draggable: '.draggable',
    // onEnd: this.onListItemDragEnd.bind(this),
    // put: false,
    handle: ".drag-handle"
    // draggable: '.draggable'
  };
  QuestionListOptions1: SortablejsOptions = {
    group: "questions1",
    sort: true,
    // draggable: '.draggable',
    // onEnd: this.onListItemDragEnd.bind(this),
    handle: ".drag-handle"
    // draggable: '.draggable'
  };

  addObservation() {
    this.selectedArea = [];
    this.selectedCompetency = [];
    this.selectedSpectr = [];
    this.areaId=''
    this.areaName=''
    this.spectrId=''
    this.spectrName=''
    this.compId=''
    this.compName=''
    this.showObservTemp = true;
  }

  closeObservationTemp() {
    this.showObservTemp = false;
    this.selectedArea = [];
    this.selectedCompetency = [];
    this.selectedSpectr = [];
  }

  back() {
    this.router.navigate(["/pages/coaching"]);
  }


  // ObservationEmpRows: any =[];
  // allObservationEmployees: any [];
  // tenantId : any = [];
  // noObservation: boolean = false;



  getAllObsevations() {
    this.spinner.show();
    this.ObservationEmpRows = [];
    this.allObservationEmployees = [];
    let param = {
      tId: this.tenantId,
    }
    const _urlFetchAllObservation:string = webApi.domain + webApi.url.getObservationTemplate;
    this.commonFunctionService.httpPostRequest(_urlFetchAllObservation,param)
    // this.observationService.getAllObservation(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('getAllObservationCC:', rescompData);
        if (result['type'] == true) {
          if (result['data'][0].length == 0) {
            this.noObservation = true;
          } else {
            this.observation = result['data'][0];
            this.noObservation = false;
            for (let i = 0; i < this.observation.length; i++) {
              if(this.observation[i].visible == 1 ) {
                this.observation[i].btntext = 'fa fa-eye';
              } else {
                this.observation[i].btntext = 'fa fa-eye-slash';
              }
              if(this.observation[i].isDelete ==0){
                this.observation[i].btndelete = '';
              }
              else{
                this.observation[i].btndelete = 'fa fa-trash';
              }
            }
            console.log('this.observation', this.observation);

            if (this.observation.delete === 1) {
              this.deleteIcon = true;
            }

            // console.log('this.nominatedEmpRows:', this.ObservationEmpRows);
            // console.log('this.allNominatedEmployees',this.allObservationEmployees);
          }

        } else {

          this.presentToast('error', '');
        }


      }, error => {
        this.noObservation = true;
        this.presentToast('error', '');

      });
  }


  saveObservation(form) {
    if(form.valid&&this.pressedOnce){
      this.pressedOnce=false
    for (let i = 0; i < this.selectedSpectr.length; i++) {
      if (this.spectrId != '') {
        this.spectrId += ','
      }
      if (String(this.selectedSpectr[i].spectrId) != '' && String(this.selectedSpectr[i].spectrId) != 'null'
        && String(this.selectedSpectr[i].spectrId) != undefined) {
        this.spectrId += this.selectedSpectr[i].spectrId;
      }
      if (this.spectrName != '') {
        this.spectrName += ','
      }
      if (String(this.selectedSpectr[i].spectrName) != '' && String(this.selectedSpectr[i].spectrName) != 'null'
        && String(this.selectedSpectr[i].spectrName) != undefined) {
        this.spectrName += this.selectedSpectr[i].spectrName;
      }
    }
    for (let i = 0; i < this.selectedCompetency.length; i++) {
      if (this.compId != '') {
        this.compId += ','
      }
      if (String(this.selectedCompetency[i].compId) != '' && String(this.selectedCompetency[i].compId) != 'null') {
        this.compId += this.selectedCompetency[i].compId;
      }
      if (this.compName != '') {
        this.compName += ','
      }
      if (String(this.selectedCompetency[i].compName) != '' && String(this.selectedCompetency[i].compName) != 'null') {
        this.compName += this.selectedCompetency[i].compName;
      }
    }
    for (let i = 0; i < this.selectedArea.length; i++) {
      if (this.areaId != '') {
        this.areaId += ','
      }
      if (String(this.selectedArea[i].areaId) != '' && String(this.selectedArea[i].areaId) != 'null') {
        this.areaId += this.selectedArea[i].areaId;
      }
      if (this.areaName != '') {
        this.areaName += ',';
      }
      if (String(this.selectedArea[i].areaName) != '' && String(this.selectedArea[i].areaName) != 'null') {
        this.areaName += this.selectedArea[i].areaName;
      }
    }

    let param = {
      specIds: this.spectrId,
      specNames: this.spectrName,
      comIds: this.compId,
      comNames: this.compName,
      evArIds: this.areaId,
      evArNames: this.areaName,
      tId: this.tenantId,
      userId: this.userDetails.data.data.id,
    };
    const  _urlInsertObservation: string = webApi.domain + webApi.url.insertDropdownObservation;
    this.commonFunctionService.httpPostRequest(_urlInsertObservation,param)
    // this.observationService.insertObservation(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('InsertObservation:', rescompData);
        if (result['type'] == true) {
          this.presentToast('success', 'Observation saved');
        this.pressedOnce=true
          this.showObservTemp = false;
          this.getAllObsevations();
        } else {

          this.presentToast('error', '');
        }


      }, error => {

        this.presentToast('error', '');

      });
    }else{
      Object.keys(form.controls).forEach(key => {
        form.controls[key].markAsDirty();
      });
    }
  }

  delsecdata: any;

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }
  visibilityTableRow(row) {
    let value;
    let status;

    if (row.visible == 1) {
      row.btntext = 'fa fa-eye-slash';
      value  = 'fa fa-eye-slash';
      row.visible = 0
      status = 0;
    } else {
      status = 1;
      value  = 'fa fa-eye';
      row.visible = 1;
      row.btntext = 'fa fa-eye';
    }

    console.log(row, status);
    const param = {
      flag: 'pre',
      tmpId: row.otId,
      stat: status
    }
    this.spinner.show();
    const enableassessment: string = webApi.domain + webApi.url.enabledisble;
    this.commonFunctionService.httpPostRequest(enableassessment,param)
    // this.assessservice.enabledisableassessment(param)
    .then(res => {
      console.log(res);
      try {
        if (res['type'] == true) {
          this.presentToast('success', res['data'][0][0]['msg']);
          this.getAllObsevations();
        }
      } catch (e) {
        console.log(e);
      }
      this.spinner.hide();
    }, err => {
      console.log(err);
      this.spinner.hide();
      this.presentToast('error', 'Something went wrong, please try again');
    });
  
  }
  // Disableassessment(i, data, status) {
  //   console.log(i, data, status);
  //   const param = {
  //     flag: 'pre',
  //     tmpId: data.otId,
  //     stat: status
  //   }
  //   this.spinner.show();
  //   const enableassessment: string = webApi.domain + webApi.url.enabledisble;
  //   this.commonFunctionService.httpPostRequest(enableassessment,param)
  //   // this.assessservice.enabledisableassessment(param)
  //   .then(res => {
  //     console.log(res);
  //     try {
  //       if (res['type'] == true) {
  //         this.presentToast('success', res['data'][0][0]['msg']);
  //         this.getAllObsevations();
  //       }
  //     } catch (e) {
  //       console.log(e);
  //     }
  //     this.spinner.hide();
  //   }, err => {
  //     console.log(err);
  //     this.spinner.hide();
  //     this.presentToast('error', 'Something went wrong, please try again');
  //   });
  // }

  Removesec(data) {
    var param = {
      otId: data.otId,
      tId: this.tenantId,
    };
    const _urlDeleteObservation: string = webApi.domain + webApi.url.deleteObservationtemplate;
    this.commonFunctionService.httpPostRequest(_urlDeleteObservation,param)
    // this.observationService.deleteObservation(param)
      .then(
        rescompData => {
          this.spinner.hide();
          if (rescompData['type'] === true) {
            console.log('remove object', rescompData);
            this.presentToast('success', rescompData['data'][0][0]['msg']);
            this.getAllObsevations();
            this.closeDeleteModal();

          }
        },
        resUserError => {
          this.spinner.hide();
         this.closeDeleteModal();
        }
      );

    this.getAllObsevations();
  }

  // Help Code Start Here //

  helpContent: any;
  getHelpContent() {
    return new Promise(resolve => {
      this.http1
        .get('../../../../../../assets/help-content/addEditCourseContent.json')
        .subscribe(
          data => {
            this.helpContent = data;
            console.log('Help Array', this.helpContent);
          },
          err => {
            resolve('err');
          }
        );
    });
    // return this.helpContent;
  }
  closeDeleteModal() {
    this.enableDeleteModal = false;
  }
  enableDelaction(actionType) {
    console.log(this.delData);
    if (actionType == true) {  
      this.Removesec(this.delData)
    } else {
      this.closeDeleteModal();
    }
  }
  clickToDelete(delData) {
      this.delData = delData;
      this.enableDeleteModal = true;
  }
  // Help Code Ends Here //
}
