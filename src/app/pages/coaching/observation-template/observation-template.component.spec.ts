import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObservationTemplateComponent } from './observation-template.component';

describe('ObservationTemplateComponent', () => {
  let component: ObservationTemplateComponent;
  let fixture: ComponentFixture<ObservationTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObservationTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObservationTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
