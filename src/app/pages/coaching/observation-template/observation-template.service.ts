import { Injectable , Inject } from '@angular/core';
import { webApi } from '../../../service/webApi';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ObservationTemplateService {



  private _urlFetchAllObservation:string = webApi.domain + webApi.url.getObservationTemplate;
  private _urlDeleteObservation: string = webApi.domain + webApi.url.deleteObservationtemplate;
  private _urlDropdownObservation: string = webApi.domain + webApi.url.getDropdownObservation;
  private _urlInsertObservation: string = webApi.domain + webApi.url.insertDropdownObservation;

  
  

  userData;
  tenantId;

  constructor(private _httpClient: HttpClient) {
    if (localStorage.getItem('LoginResData')) {
      this.userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', this.userData.data);
      // this.userId = userData.data.data.id;
    }
    this.tenantId = this.userData.data.data.tenantId;
   }


  getAllObservation(param){
    return new Promise(resolve => {
      this._httpClient.post(this._urlFetchAllObservation, param)
      //.map(res => res.json())
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}

  deleteObservation(param) {
    return new Promise(resolve => {
      this._httpClient.post(this._urlDeleteObservation,param)
      .subscribe(data => {
        resolve(data);
      },
      err => {
        resolve('err');
      });
    });
  }

  dropdownObservation(param) {
    return new Promise(resolve => {
      this._httpClient.post(this._urlDropdownObservation,param)
      .subscribe(data => {
        resolve(data);
      },
      err => {
        resolve('err');
      });
    });
  }

  insertObservation(param){
    return new Promise(resolve => {
      this._httpClient.post(this._urlInsertObservation, param)
      //.map(res => res.json())
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}
  

}
