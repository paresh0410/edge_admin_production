import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, NavigationStart, Routes, ActivatedRoute } from '@angular/router';
import { AppService } from '../../app.service';
import { masterservice } from './master/master.service';
import { SuubHeader } from '../components/models/subheader.model';


@Component({
  selector: 'ngx-coaching',
  templateUrl: './coaching.component.html',
  styleUrls: ['./coaching.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CoachingComponent implements OnInit {
  showdata: any = [];
  learnData: any = [];
  header: SuubHeader  = {
    title:'Coaching',
    showBreadcrumb:true,
    breadCrumbList:[]
  };
  constructor(private router: Router, public routes: ActivatedRoute, private AppService: AppService,
    private master: masterservice) {
    this.showdata = this.AppService.getmenus();
    if (this.showdata) {
      for (let i = 0; i < this.showdata.length; i++) {
        if (Number(this.showdata[i].parentMenuId) === 7) {
          this.learnData.push(this.showdata[i]);
        }
        //console.log(this.learnData);
      }
    }
  }

  ngOnInit() {
  }
  gotopages(url, data) {
    this.master.data = data;
    this.router.navigate([url], { relativeTo: this.routes });
  }
  gototeam() {
    this.router.navigate(['team'], { relativeTo: this.routes });
  }

  gotoCallMapping() {
    this.router.navigate(['call-mapping'], { relativeTo: this.routes });
  }

  gotoCallParticipants() {
    this.router.navigate(['participants'], { relativeTo: this.routes });
  }

  gotoAttrMaster() {
    this.router.navigate(['attribute-master'], { relativeTo: this.routes });
  }

  gotoAttriSkills() {
    this.router.navigate(['attribute-skills'], { relativeTo: this.routes });
  }

  gotoPartner() {
    this.router.navigate(['partner'], { relativeTo: this.routes });
  }

  gotoCalls() {
    this.router.navigate(['calls'], { relativeTo: this.routes });
  }

  gotoFeedback() {
    this.router.navigate(['feedback-template'], { relativeTo: this.routes });
  }

  gotoAssessment() {
    this.router.navigate(['assessment'], { relativeTo: this.routes });
  }

  gotoObservation() {
    this.router.navigate(['observation'], { relativeTo: this.routes });
  }
  gotomaster(data) {
    console.log(data);
    this.master.data = data;
    this.router.navigate(['master'], { relativeTo: this.routes });
  }
}
