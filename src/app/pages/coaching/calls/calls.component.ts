import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'ngx-calls',
  templateUrl: './calls.component.html',
  styleUrls: ['./calls.component.scss']
})
export class CallsComponent implements OnInit {
  rows = [];
  selected = [];

  coachingcalls = [
  {
  	"participantName":'John Cena',
  	"date":'19-Nov-2018',
  	"business":'Business1',
  	"department":'department4',
  	"manager":'Steve'
  },
  {
  	"participantName":'Ramsey Cummings',
  	"date":'22-Sep-2018',
  	"business":'Business3',
  	"department":'department1',
  	"manager":'Jack Harris'
  },
  {
  	"participantName":'Jack Harris',
  	"date":'11-Nov-2018',
  	"business":'Business2',
  	"department":'department2',
  	"manager":'Steve'
  },
  {
  	"participantName":'Stefanie Huff',
  	"date":'04-Nov-2018',
  	"business":'Business1',
  	"department":'department4',
  	"manager":'Steve'
  },
  {
  	"participantName":'Mabel David',
  	"date":'06-Feb-2018',
  	"business":'Business1',
  	"department":'department4',
  	"manager":'Steve'
  },
  {
  	"participantName":'Frank Bradford',
  	"date":'07-Aug-2018',
  	"business":'Business1',
  	"department":'department5',
  	"manager":'Steve'
  },
  {
  	"participantName":'Forbes Levine',
  	"date":'19-Jun-2018',
  	"business":'Business6',
  	"department":'department4',
  	"manager":'Steve'
  },
  {
  	"participantName":'Santiago Mcclain',
  	"date":'20-Jul-2018',
  	"business":'Business5',
  	"department":'department2',
  	"manager":'Leigh Beasley'
  },
  {
  	"participantName":'Merritt Booker',
  	"date":'14-Mar-2018',
  	"business":'Business3',
  	"department":'department2',
  	"manager":'Merritt Booker'
  },
  {
  	"participantName":'Oconnor Wade',
  	"date":'25-Apr-2018',
  	"business":'Business3',
  	"department":'department4',
  	"manager":'Santiago Mcclain'
  }];

  constructor(private router:Router,private routes:ActivatedRoute) { 
    this.rows = this.coachingcalls;
  }

  ngOnInit() {
  }
  onSelect({ selected }) {
    console.log('Select Event', selected, this.selected);

    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }

  gotoBack(){
    this.router.navigate(['../../coaching'],{relativeTo:this.routes});
	}

}
