import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CommonModule } from '@angular/common';
import { NbTabsetModule } from '@nebular/theme';
import { FormsModule } from '@angular/forms';
import { Ng5SliderModule } from 'ng5-slider';


import { CallDetailComponent } from './call-detail.component';
import { ContentComponent } from './content/content.component';
import { ChatComponent } from './chat/chat.component';
import { ChatService } from './chat/chat.service';
import { FeedbackComponent } from './feedback/feedback.component';
import { AssessmentComponent } from './assessment/assessment.component';
import { CallDetailService } from './call-detail.service';
import { ContentService } from './content/content.service';
import { AssessmentService } from './assessment/assessment.service'
import { FeedbackCCService } from './feedback/feedback.service';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

import { DocumentRef } from '@agm/core/utils/browser-globals';
// import { ToastrService,ToastrModule } from 'ngx-toastr';
import { CourseDetailComponent } from './content/course-detail/course-detail.component';
import {ModalViewerComponent} from '../../../component/modal-viewer/modal-viewer.component';
import { BarRatingModule } from "ngx-bar-rating";
import { ObservationComponent } from './observation/observation.component';
import { StarRatingModule } from 'angular-star-rating';
import { FilterPipeModule  } from 'ngx-filter-pipe';
import{ComponentModule }from '../../../component/component.module'
@NgModule({
    imports: [
        CommonModule,
        NgxDatatableModule,
        NbTabsetModule,
        FormsModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        StarRatingModule.forRoot(),
        // ToastrModule.forRoot(),
        Ng5SliderModule,
        BarRatingModule,
        FilterPipeModule,
        ComponentModule
    ],

    declarations: [

        CallDetailComponent,
        ContentComponent,
        ChatComponent,
        FeedbackComponent,
        AssessmentComponent,
        CourseDetailComponent,
        ModalViewerComponent,
        ObservationComponent,
    ],

    providers: [ 
        CallDetailService,
        ContentService,
        FeedbackCCService,
        AssessmentService,
        ChatService
    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
     ]
      
})
      
export class CallDetailModule{
      
      }