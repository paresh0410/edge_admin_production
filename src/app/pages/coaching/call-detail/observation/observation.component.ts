import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { CallDetailService } from '../call-detail.service';
import { webApi } from '../../../../service/webApi';
import { CommonFunctionsService } from '../../../../service/common-functions.service';

@Component({
  selector: 'ngx-observation',
  templateUrl: './observation.component.html',
  styleUrls: ['./observation.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ObservationComponent implements OnInit {
  userData: any = [];
  callId: any = [];
  obsqueans: any = [];
  obs: boolean = false;
  constructor(public CallDetailService: CallDetailService,private commonFunctionService: CommonFunctionsService) { }

  ngOnInit() {
    this.callId = this.CallDetailService.callId;
    this.userData = JSON.parse(localStorage.getItem('LoginResData'));
    this.observationdata();
  }
  observationdata() {
    const param = {
      // tId: this.userData.data.data.tenantId,
      // calId: this.callId,
      tId: this.userData.data.data.tenantId,
      calId: this.callId,
    };
    const _observationurl: string = webApi.domain + webApi.url.getobservation;
    this.commonFunctionService.httpPostRequest(_observationurl,param)
    // this.CallDetailService.observation(param)
    .then(res => {
      console.log(res);
      if (res['type'] === true) {
        try {
          for (let i = 0; i < res['data'].length; i++) {
            if (res['data'][i].score === null) {
              res['data'][i].score = 0;
            }
            this.obsqueans = res['data'];
            this.obs = true;
          } 
        } catch (e) {
          console.log(e);
         }
      }
    }, err => {
      console.log(err);
    });
  }
}
