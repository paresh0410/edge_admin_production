import {Injectable, Inject} from '@angular/core';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppConfig} from '../../../../app.module';
import { webApi } from '../../../../service/webApi';
import { HttpClient } from "@angular/common/http";

@Injectable()
export class AssessmentService {

    

        private _urlGetAssessmentTemplate : string = webApi.domain + webApi.url.getAssessmentTemplate;
        private _urlAddEditAssessmentDetails : string = webApi.domain + webApi.url.addeditassessmentdetails;
        

  constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private _http: Http,private _httpClient:HttpClient){
      //this.busy = this._http.get('...').toPromise();
  }

  getAssessmentTemplate(param){
    return new Promise(resolve => {
      this._httpClient.post(this._urlGetAssessmentTemplate, param)
      //.map(res => res.json())
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}

addEditAssessmentDetails(param){
    return new Promise(resolve => {
      this._httpClient.post(this._urlAddEditAssessmentDetails, param)
      //.map(res => res.json())
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}





_errorHandler(error: Response){
  console.error(error);
  return Observable.throw(error || "Server Error")
}


}
