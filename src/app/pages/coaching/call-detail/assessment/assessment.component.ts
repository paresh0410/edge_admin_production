import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { Router, NavigationStart, Routes, ActivatedRoute } from '@angular/router';
import { CallDetailService } from './../call-detail.service';
import { AssessmentService } from './assessment.service';
import { Options } from 'ng5-slider';
import { ToastrService } from 'ngx-toastr';
import { webApi } from '../../../../service/webApi';
import { CommonFunctionsService } from '../../../../service/common-functions.service';

@Component({
  selector: 'ngx-assessment',
  templateUrl: './assessment.component.html',
  styleUrls: ['./assessment.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AssessmentComponent implements OnInit {

  value: number = 2;
  options: Options = {
    floor: 0,
    ceil: 5,
    step: 1,
    showTicks: true,
    showTicksValues: true
  };

  personList: any = [];
  tenantId: any;
  empId: any;
  callId: any;
  userId: any;
  assessmentTemplate: any = [];
  constructor(private spinner: NgxSpinnerService, private router: Router, public routes: ActivatedRoute,
    // private toasterService: ToasterService, 
    private callDetailService: CallDetailService,
     private assessmentservice: AssessmentService, private toastr: ToastrService,private commonFunctionService: CommonFunctionsService,) {
    this.tenantId = this.callDetailService.tenantId;
    this.empId = this.callDetailService.empId;
    this.callId = this.callDetailService.callId;
    if (localStorage.getItem('LoginResData')) {
      var userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', userData.data);
      this.userId = userData.data.data.id;
    }

    this.getAssessmenttemplate();
  }

  ngOnInit() {
  }

  presentToast(type, body) {
    if(type === 'success'){
      this.toastr.success(body, 'Success', {
        closeButton: false
       });
    } else if(type === 'error'){
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else{
      this.toastr.warning(body, 'Warning', {
        closeButton: false
        })
    }
  }

  getAssessmenttemplate() {
    this.spinner.show();
    let param = {
      "tId": this.tenantId,
      "busiId": 1,
      "calId": this.callId
    }
    const _urlGetAssessmentTemplate : string = webApi.domain + webApi.url.getAssessmentTemplate;
    this.commonFunctionService.httpPostRequest(_urlGetAssessmentTemplate,param)
    // this.assessmentservice.getAssessmentTemplate(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('assess:', rescompData);
        if (result['type'] == true) {
          if (result['data'].length === 0) {
          } else {
            try {
              for (let i = 0; i < result['data'].length; i++) {
                this.assessmentTemplate = result['data'];
                for(let j = 0; j<this.assessmentTemplate[i].areaSubAssess.length; j++) {
                  if(this.assessmentTemplate[i].areaSubAssess[j].score === null) {
                    this.assessmentTemplate[i].areaSubAssess[j].score = 0;
                  }
                  if(this.assessmentTemplate[i].areaSubAssess[j].remarks === 'null') {
                    this.assessmentTemplate[i].areaSubAssess[j].remarks = 'NA';
                  }
                }
                console.log('this.assessmentTemplate', this.assessmentTemplate);
              }
            } catch{ }
          }

        } else {
          // var toast: Toast = {
          //   type: 'error',
          //   //title: "Server Error!",
          //   body: "Something went wrong.please try again later.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
        this.presentToast('error', '');
        }
      }, error => {
        this.spinner.hide();
        // var toast: Toast = {
        //   type: 'error',
        //   //title: "Server Error!",
        //   body: "Something went wrong.please try again later.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
  }

  submit(data) {
    this.spinner.show();
    console.log('data', data);
    var newDataArr = [];
    for (let i = 0; i < data.length; i++) {
      for (let j = 0; j < data[i].areaSubAssess.length; j++) {
        var newDataObj = {
          assessName: data[i].areaSubAssess[j].assessName,
          atId: data[i].areaSubAssess[j].atId,
          compId: data[i].areaSubAssess[j].compId,
          compName: data[i].areaSubAssess[j].compName,
          remarks: data[i].areaSubAssess[j].remarks,
          score: data[i].areaSubAssess[j].score,
          spectrId: data[i].areaSubAssess[j].spectrId,
          spectrName: data[i].areaSubAssess[j].spectrName,
          areaId: data[i].areaId,
          areaName: data[i].areaName
        }
        newDataArr.push(newDataObj);
      }
    }
    console.log('newDataArr', newDataArr);
    var allstr = this.getDataReadyForFeedbackCC(newDataArr);

    let param = {
      "callId": this.callId,
      "userId": this.userId,
      "tId": this.tenantId,
      "allstr": allstr
    }
    const _urlAddEditAssessmentDetails : string = webApi.domain + webApi.url.addeditassessmentdetails;
    this.commonFunctionService.httpPostRequest(_urlAddEditAssessmentDetails,param)
    // this.assessmentservice.addEditAssessmentDetails(param)
      .then(rescompData => {

        this.spinner.hide();

        var res = rescompData;
        console.log('Assessment Add Edit CC:', res);
        if (res['type'] == true) {
          // var toast: Toast = {
          //   type: 'success',
          //   body: "Assessment inserted successfully.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
        this.presentToast('success', 'Assessment added');
        } else {
          //this.getAssessmenttemplate();
          // var toast: Toast = {
          //   type: 'error',
          //   body: "Unable to insert assessment. please try again later.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }


      },
        resUserError => {
          this.spinner.hide();
          // this.getAssessmenttemplate();
          //this.errorMsg = resUserError;
          // var toast: Toast = {
          //   type: 'error',
          //   body: "Something went wrong.please try again later.",
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
        this.presentToast('error', '');
        });
  }

  getDataReadyForFeedbackCC(newDataArr) {
    console.log('newDataArrReady', newDataArr);
    var spectrId;
    var spectrName;
    var compId;
    var compName;
    var evalAreaId;
    var evalAreaName;
    var assessId;
    var assessName;
    var score;
    var remarks;
    var atId;
    var Finalline;
    for (let i = 0; i < newDataArr.length; i++) {

      if (newDataArr[i].areaId == null) {
        newDataArr[i].areaId = '';
      }
      if (newDataArr[i].areaName == null) {
        newDataArr[i].areaName = '';
      }

      spectrId = newDataArr[i].spectrId;
      spectrName = newDataArr[i].spectrName;
      compId = newDataArr[i].compId;
      compName = newDataArr[i].compName;
      evalAreaId = newDataArr[i].areaId;
      evalAreaName = newDataArr[i].areaName;
      assessName = newDataArr[i].assessName;
      score = newDataArr[i].score;
      remarks = newDataArr[i].remarks;
      atId = newDataArr[i].atId;
      let line = spectrId + "|" + spectrName + "|" + compId + "|" + compName + "|" + evalAreaId + "|" + evalAreaName
        + "|" + assessName + "|" + score + "|" + remarks + "|" + atId;
      if (i == 0) {
        Finalline = line;
      } else {
        Finalline += "#" + line;
      }

    }
    return Finalline;
    // console.log('Finalline',Finalline);
  }


}
