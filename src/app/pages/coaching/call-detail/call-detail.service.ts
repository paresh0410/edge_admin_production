import { Injectable, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AppConfig } from '../../../app.module';
import { webApi } from '../../../service/webApi';
import { HttpClient } from "@angular/common/http";

@Injectable()
export class CallDetailService {

  callId: any;
  empId: any;
  tenantId: any;
  coachId: any;
  empName:any;
  loginUserdata;
  // CallDetail: any = {};
  //   private _urlAddEditEmployee:string = webApi.domain + webApi.url.addeditnominatedemployee;
  private _observationurl: string = webApi.domain + webApi.url.getobservation;
  private _getNote: string = webApi.domain + webApi.url.getCallNote;
  constructor(@Inject('APP_CONFIG_TOKEN') private config: AppConfig, 
  private http: Http, private _httpClient: HttpClient) {
    //this.busy = this._http.get('...').toPromise();
    if (localStorage.getItem('LoginResData')) {
      this.loginUserdata = JSON.parse(localStorage.getItem('LoginResData'));
    }
    this.tenantId = this.loginUserdata.data.data.tenantId;
  }

    observation(param){
      return new Promise(resolve => {
        this._httpClient.post(this._observationurl, param)
        //.map(res => res.json())
        .subscribe(data => {
            resolve(data);
        },
        err => {
            resolve('err');
        });
    });
  }

  getCallNote(param){
    return new Promise(resolve => {
      this._httpClient.post(this._getNote, param)
      //.map(res => res.json())
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}


  _errorHandler(error: Response) {
    console.error(error);
    return Observable.throw(error || "Server Error")
  }

  // setCallDetail() {

  // }
  // getCallDetail(){
  //   return this.CallDetail
  // }

}
