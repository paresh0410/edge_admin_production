import { Component, OnInit } from '@angular/core';
import { CallDetailService } from './../call-detail.service';
import { ContentService } from './content.service';
import { NgxSpinnerService } from 'ngx-spinner';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { ToastrService } from 'ngx-toastr';
import {
  Router,
  NavigationStart,
  Routes,
  ActivatedRoute,
} from '@angular/router';
import { webApi } from '../../../../service/webApi';
import { CommonFunctionsService } from '../../../../service/common-functions.service';

@Component({
  selector: 'ngx-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {
  modal: boolean;
  courses: any;
  name: string;
  id: number;
  contentDataArray: any = [];
  contentArray: any = [];

  callArray: any = [];

  allCourse: any = [];

  tenantId: any;
  callId: any;
  empId: any;
  userId: any;
  condataCourse: any;
  couresshow: boolean = false;
  courseSelected: boolean = false;
  searchcourse:any;
  constructor(
    private callDetailService: CallDetailService,
    private contentService: ContentService,
    private spinner: NgxSpinnerService,
    private router: Router,
    public routes: ActivatedRoute,
    // private toasterService: ToasterService,
    private toastr: ToastrService,
    private commonFunctionService: CommonFunctionsService
  ) {
    this.searchcourse = {
      // fullname
    }
    //this.tenantId = this.callDetailService.tenantId;
    this.empId = this.callDetailService.empId;
    this.callId = this.callDetailService.callId;
    if (localStorage.getItem('LoginResData')) {
      const userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', userData.data);
      this.userId = userData.data.data.id;
      this.tenantId = userData.data.data.tenantId;
    }
    this.getAllCoursesCC();
    this.getAllEnrolCourseModules();
    // this.ActiveTab = this.contentArray[0].id;
    // this.contentDataArray = this.contentArray[0].list;
  }

  coursesCC: any[];
  getAllCoursesCC() {
    const param = {
      tId: this.tenantId,
    };
    const _urlGetAllCourses: string = webApi.domain + webApi.url.getallcoursescc;
    this.commonFunctionService.httpPostRequest(_urlGetAllCourses,param)
    // this.contentService.getAllCoursesCC(param)
    .then(
      rescompData => {
        this.spinner.hide();
        const result = rescompData;
        console.log('getAllEmployessCC:', rescompData);
        if (result['type'] === true) {
          if (result['data'][0].length === 0) {
            // this.noEmployees = true;
          } else {
            this.coursesCC = result['data'][0];
            console.log('this.coursesCC', this.coursesCC);
            // this.participantsservice.allEmployess = this.allEmployees;
            // console.log('this.participantsservice.getAllEmployees',this.participantsservice.getAllEmployees);
          }
        } else {
		  this.presentToast('error', '');
        }
      },
      error => {
        this.spinner.hide();
		this.presentToast('error', '');
      },
    );
  }

  ngOnInit() {
    // this.modal = false;
  }

  openModal() {
    this.couresshow = true;
  }

  closeModal() {
    this.couresshow = false;
  }

  selectItem(item) {
    if (!item.isSelected) {
      item.isSelected = true;
    } else {
      item.isSelected = false;
    }
  }

  // addSelected(){
  //   // this.allCourse.forEach(obj => {

  //   // });
  // }

  activeSelectedCourseId: any;
  setActiveSelectedCourse(currentIndex, currentCourse) {
    console.log('currentCourseOld:', currentCourse);

    this.condataCourse = {};
    this.activeSelectedCourseId = currentCourse.courseId;
    this.condataCourse = currentCourse;
    console.log('currentCourseNew:', this.condataCourse);
  }
  courseCCObj: any = {};
  saveCourse() {
    this.spinner.show();
    this.courseCCObj = {
      courseId: this.condataCourse.courseId,
      coursePicRef: this.condataCourse.coursePicRef,
      fullname: this.condataCourse.fullname,
      summary: this.condataCourse.summary,
    };
    console.log('Selected Course:', this.condataCourse);
    this.couresshow = false;
    // this.badge = false;

    const param = {
      emId: 1,
      aId: 20,
      courseId: this.condataCourse.courseId,
      eId: this.empId,
      tId: this.tenantId,
      actv: 1,
      dId: 0,
      callId: this.callId,
      userId: this.userId,
    };
    const _urlEnrolCourseForCCContent: string = webApi.domain + webApi.url.enrolcourseforcccontent;
    this.commonFunctionService.httpPostRequest(_urlEnrolCourseForCCContent,param)
    // this.contentService.enrollCourseForCCContent(param)
    .then(
      rescompData => {
        this.spinner.hide();

        const res = rescompData;
        console.log('Selected Course:', res);
        if (res['type'] === true) {
          this.presentToast('success', 'Course enrolled');
          this.courseSelected = true;
          this.activeSelectedCourseId = null;
          this.getAllEnrolCourseModules();
        } else {
          this.presentToast('error', ' ');
        }
      },
      resUserError => {
        this.spinner.hide();
        this.presentToast('error', ' ');
      },
    );
  }

  courseAvailable: boolean = false;
  enrolId: any;
  courseData: any = {};
  getAllEnrolCourseModules() {
    const param = {
      callId: this.callId,
      tenantId: this.tenantId,
      areaId: 20,
    };
  console.log('getAllEnrolCourseModulesparam', param);
  const _urlGetAllEnrollCourseModule: string = webApi.domain + webApi.url.getallenrolcoursemodule;
  this.commonFunctionService.httpPostRequest(_urlGetAllEnrollCourseModule,param)
    // this.contentService.getAllEnrolCourseModule(param)
    .then(
      rescompData => {
        this.spinner.hide();
		const res = rescompData;
        console.log('ModulesCC:', res);
        if (res['type'] === true) {
          const Finres: any = res;
          if (Finres.data.length === 0) {
            this.courseAvailable = false;
          } else {
            this.courseAvailable = true;
            this.contentArray = Finres.data;
            // for (let i = 0; i < this.contentArray.length; i++) {
            //   const tempModule: any = i + 1;
            //   this.contentArray[i].id = i;
            //   this.contentArray[i].moduleName = 'Module' + ' ' + tempModule;
            // }
            this.ActiveTab = this.contentArray[0].id;
            this.contentDataArray = this.contentArray[0].list;
            this.enrolId = this.contentDataArray[0].enrolId;
            this.courseData = {
              courseName : this.contentArray[0].list[0].courseName,
              courseDesc : this.contentArray[0].list[0].cDescription,
            }
            console.log('enrolId', this.enrolId);
            console.log('this.contentDataArray', this.contentDataArray);
            // for (let i = 0; i < this.contentDataArray.length; i++) {
            //   const tempActivity: any = i + 1;
            //   this.contentDataArray[i].img = 'assets/images/open-book-leaf.jpg';
            //   this.contentDataArray[i].activityName =
            //     'Activity' + ' ' + tempActivity;
            // }
            console.log('CD===>', this.contentArray);
            this.spinner.hide();
          //   console.log('ModulesCC:', res);
          // console.log('currentCourseNew:', this.condataCourse);
          //   this.ActiveTab = this.contentArray[0].id;
          //   this.contentDataArray = this.contentArray[0].list;
          }
        } else {
          this.presentToast('error', ' ');
        }
      },
      resUserError => {
        this.spinner.hide();
        this.presentToast('error', ' ');
	 },
    );
  }
  ActiveTab: any;

  tabChanged(data) {
   // this.spinner.show();
    console.log('DATA--->', data);
    this.ActiveTab = data.id;
    this.contentDataArray = this.contentArray[data.id].list;
    // for (let i = 0; i < this.contentDataArray.length; i++) {
    //   const ind: any = i + 1;
    //   this.contentDataArray[i].img = 'assets/images/open-book-leaf.jpg';
    //   this.contentDataArray[i].activityName = 'Activity' + ' ' + ind;
    // }
   // this.spinner.hide();
  }

  romoveCourseModules(data) {
    console.log('data', data);
    const param = {
      tId: this.tenantId,
      cId: this.callId,
    };
    const _urlRemoveEnrolCourse: string = webApi.domain + webApi.url.removeenrolcoursecc;
    this.commonFunctionService.httpPostRequest(_urlRemoveEnrolCourse,param)
    // this.contentService.removeEnrolCourse(param)
    .then(
      rescompData => {
        this.spinner.hide();
        const result = rescompData;
        console.log('Remove Course Result', rescompData);
        if (result['type'] === true) {
          this.presentToast('success', 'Course removed.');
          this.enrolId = null;
          this.courseAvailable = false;
        } else {
          this.presentToast('error', ' ');
        }
      },
      error => {
        this.spinner.hide();
        this.presentToast('error', '');
      },
    );
  }

  goToCourseDetails(data) {
    this.router.navigate(['content/course-details'], {
      relativeTo: this.routes,
    });
  }

  presentToast(type, body) {
    if(type === 'success'){
      this.toastr.success(body, 'Success', {
        closeButton: false
       });
    } else if(type === 'error'){
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else{
      this.toastr.warning(body, 'Warning', {
        closeButton: false
        })
    }
  }
}
