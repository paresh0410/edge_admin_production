import {Injectable, Inject} from '@angular/core';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppConfig} from '../../../../app.module';
import { webApi } from '../../../../service/webApi';
import { HttpClient } from "@angular/common/http";

@Injectable()
export class ChatService {

    private _urlChatList: string = webApi.domain + webApi.url.getChatListCC;

    constructor(@Inject ('APP_CONFIG_TOKEN') private config: AppConfig, private _http: Http,
    private _httpClient: HttpClient) {
    }

    getChatList(param){
        return new Promise(resolve => {
          this._httpClient.post(this._urlChatList, param)
          .subscribe(data => {
              resolve(data);
          },
          err => {
              resolve('err');
          });
      });
    }

    _errorHandler(error: Response){
        console.error(error);
        return Observable.throw(error || "Server Error")
      }
}