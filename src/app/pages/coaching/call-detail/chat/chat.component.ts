import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { ChatService } from './chat.service';
import { CallDetailService } from './../call-detail.service';
import { NgxSpinnerService } from 'ngx-spinner';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { ToastrService } from 'ngx-toastr';
import { webApi } from '../../../../service/webApi';
import { CommonFunctionsService } from '../../../../service/common-functions.service';

@Component({
  selector: 'ngx-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
})
export class ChatComponent implements OnInit {
  tenantId: any;
  empId: any;
  callId: any;
  trainerId: any;

  playRecording: boolean = false;
  @ViewChild('videoPlayer') videoplayer: ElementRef;

  recordings: any = [];

  modal_title: any;
  videoLink: any;
  constructor(public chatService: ChatService,
    private callDetailService: CallDetailService,
    private spinner: NgxSpinnerService,
    // private toasterService: ToasterService,
    private toastr: ToastrService,private commonFunctionService: CommonFunctionsService) {
      this.empId = this.callDetailService.empId;
      this.callId = this.callDetailService.callId;
      this.trainerId = this.callDetailService.coachId;
      if (localStorage.getItem('LoginResData')) {
        const userData = JSON.parse(localStorage.getItem('LoginResData'));
        this.tenantId = userData.data.data.tenantId;
      }

  }

  ngOnInit() {
    this.getChatList();
  }
  chatList: any[];
  noChatAvailable: boolean = false;
  getChatList() {
    this.chatList = [];
    this.recordings = [];
    const param = {
      'tId': this.tenantId,
      'aId': 21,
      'iId': this.callId,
      // 'iId': 69,
    };
    console.log('Chat param',param)
    const _urlChatList: string = webApi.domain + webApi.url.getChatListCC;
    this.commonFunctionService.httpPostRequest(_urlChatList,param)
    // this.chatService.getChatList(param)
    .then(rescompData => {
        this.spinner.hide();
        const res = rescompData;
        console.log('chatList:', res);
        if (res['type'] === true) {
          if (res['data'][0].length === 0) {
            this.noChatAvailable = true;
            console.log('noChatAvailable', this.noChatAvailable);
          } else {
            this.chatList = res['data'][0];
            console.log('chatList', this.chatList);
            this.noChatAvailable = false;
            console.log('noChatAvailable', this.noChatAvailable);
          }

          if (res['data'][1].length > 0) {
            this.recordings = res['data'][1];
            console.log('this.recordings', this.recordings);
          }
        } else {
          this.presentToast('error', ' ');
        }
      },
      resUserError => {
        this.spinner.hide();
        this.presentToast('error', ' ');
	    },
    );
  }

  playRecordedVideo(item, i){
    this.playRecording = true;
    this.modal_title = i;
    this.videoLink = item.link;
  }

  closeModal(){
    this.playRecording = false;
  }


toggleVideo(event: any) {
    this.videoplayer.nativeElement.play();
}

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false,
       });
    } else if (type === 'error') {
      this.toastr.error(
        'Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 
        'Error', {
        timeOut: 0,
        closeButton: true,
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false,
        });
    }
  }
}
