import { Component, OnInit ,ViewEncapsulation} from '@angular/core';
declare var window: any;
import { FeedbackCCService } from './feedback.service';
import { NgxSpinnerService } from 'ngx-spinner';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { ToastrService } from 'ngx-toastr';
import { CallDetailService } from './../call-detail.service'
import { webApi } from '../../../../service/webApi';
import { CommonFunctionsService } from '../../../../service/common-functions.service';
// import { ViewEncapsulation } from '@angular/compiler/src/core';

@Component({
  selector: 'ngx-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class FeedbackComponent implements OnInit {

  myInnerHeight: any = window.innerHeight - 140;

  modal: boolean;
  id: any;
//   managerFeedback:any=[{
//     feedback:'Lorem ipsum dolor sit amet,consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, qunostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.'
//   }];

//  coachFeedback:any=[{
//     feedback:'Lorem ipsum dolor sit amet,consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, qunostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.'
//   }];

//  participantFeedback:any=[{
//     feedback:'Lorem ipsum dolor sit amet,consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, qunostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.'
//   }];

  users:any = [
    {
      id: 1,
      name: "Manager",
      feedback: [
        {
          feedbackDesc:"Lorem ipsum dolor sit amet,consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, qunostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat"
        },
        {
          feedbackDesc:"kabads Lorem ipsum dolor sit amet,consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, qunostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat"
        }
      ],
    },
    {
      id: 2,
      name: "Coach",
      feedback: [
        {
          feedbackDesc:"Lorem ipsum dolor sit amet,consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, qunostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat"
        }
      ],
    },
    {
      id: 3,
      name: "Participant",
      feedback: [
        {
          feedbackDesc:"Lorem ipsum dolor sit amet,consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, qunostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat"
        }
      ],
    }
  ];

  tenantId:any;
  feedbacktemplateArr:any=[];
  feedbackData:any={
    text:'',
    date : ''
  };
  callId :any;
  userId : any;
  userData: any;
  constructor(private feedbackccservice:FeedbackCCService, private spinner: NgxSpinnerService,
    // private toasterService: ToasterService,
     private calldetailservice:CallDetailService,
    private toastr: ToastrService,private commonFunctionService: CommonFunctionsService,) {
    this.tenantId = this.feedbackccservice.tenantId;
    this.callId = this.calldetailservice.callId;
    if(localStorage.getItem('LoginResData')){
      this.userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', this.userData.data);
      this.userId = this.userData.data.data.id;
    }
    this.getFeebackTemplate();
   }

  ngOnInit() {
    this.modal = false;
  }
  presentToast(type, body) {
    if(type === 'success'){
      this.toastr.success(body, 'Success', {
        closeButton: false
       });
    } else if(type === 'error'){
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else{
      this.toastr.warning(body, 'Warning', {
        closeButton: false
        })
    }
  }


  getFeebackTemplate(){
    let param = {
      "tId":this.tenantId,
      "roleId":8,
      "calId":this.callId
    }
    console.log('getFeebackTemplateparam',param);
    const _urlGetFeebackTemplate : string = webApi.domain + webApi.url.getfeedbacktemplate;
    this.commonFunctionService.httpPostRequest(_urlGetFeebackTemplate,param)
    // this.feedbackccservice.getFeedbackTemplate(param)
    .then(rescompData => {

      this.spinner.hide();

      var res = rescompData;
      console.log('Feed:', res);
      if (res['type'] == true) {
        // var toast: Toast = {
        //   type: 'success',
        //   body: "Course enrolled to course.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        
        this.feedbacktemplateArr = res['data'];
        console.log('feedbacktemplateArr CC:', this.feedbacktemplateArr);
      
      } else {
        // var toast: Toast = {
        //   type: 'error',
        //   body: "Something went wrong.please try again later.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      }


    },
      resUserError => {
        this.spinner.hide();
        //this.errorMsg = resUserError;
        // var toast: Toast = {
        //   type: 'error',
        //   body: "Something went wrong.please try again later.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
  }

  formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

  submit(data){
    var feedbackArrObjToPush : any = {}
    var feedbackArrToPush : any = [];
    console.log('FeedbackData:',data);
    for(let i=0;i<data.length;i++){
      for(let j=0;j<data[i].section.length;j++){
        for(let k=0;k<data[i].section[j].subsection.length;k++){
          if(data[i].section[j].subsection[k].dataType == "datetime"){
                data[i].section[j].subsection[k].value=this.formatDate(data[i].section[j].subsection[k].value);
         }
          feedbackArrObjToPush = {
            role : data[i].role,
            section : data[i].section[j].section,
            subSection : data[i].section[j].subsection[k].subsection_name,
            dataType : data[i].section[j].subsection[k].dataType,
            roleId : data[i].section[j].subsection[k].roleId,
            feedback : data[i].section[j].subsection[k].value,
            ftId : data[i].section[j].subsection[k].ftId
          }
          feedbackArrToPush.push(feedbackArrObjToPush);
        }
      }
    }

    console.log('feedbackArrToPush',feedbackArrToPush);
    var allstr=this.getDataReadyForFeedbackCC(feedbackArrToPush);
    console.log('allstr',allstr)
    // for(let i = 0;i<data.length;i++){
    //   for(let j=0;j<data[i].list.length;j++){
    //     if(data[i].list[j].subSection.length == 0){
    //       data[i].list[j].subSection = null;
    //     }
    //     if(data[i].list[j].dataType == "datetime"){
    //       data[i].list[j].value=new Date(data[i].list[j].value);
    //     }
    //     feedbackArrToPush.push(data[i].list[j]);
    //   }
    // }
    // console.log('feedbackArrToPush:',feedbackArrToPush);
    // var allstr=this.getDataReadyForFeedbackCC(feedbackArrToPush);
    // console.log('allstr',allstr);

    // let param = {
    //   "callId":this.callId,
    //   "userId":this.userId,
    //   "tId":this.tenantId,
    //   "allstr":allstr
    // }
    let param = {
      "callId":this.callId,
      "userId":this.userId,
      "tId":this.userData.data.data.tenantId,
      "allstr":allstr
    }
    console.log('param:',param)
    const _urlAddEditFeedbackDetails : string = webApi.domain + webApi.url.addeditfeedbackdetails;
    this.commonFunctionService.httpPostRequest(_urlAddEditFeedbackDetails,param)
    // this.feedbackccservice.addEditFeedbackDetails(param)
    .then(rescompData => {

      this.spinner.hide();

      var res = rescompData;
      console.log('Feedback Add Edit CC:', res);
      if (res['type'] == true) {
        // var toast: Toast = {
        //   type: 'success',
        //   body: "Feedback inserted successfully.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('success', 'Feedback added');
      } else {
        // var toast: Toast = {
        //   type: 'error',
        //   body: "Unable to insert feedback.please try again later.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      }


    },
      resUserError => {
        this.spinner.hide();
        //this.errorMsg = resUserError;
        // var toast: Toast = {
        //   type: 'error',
        //   body: "Something went wrong.please try again later.",
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(toast);
        this.presentToast('error', '');
      });
  }

  getDataReadyForFeedbackCC(feedbackArr){
    var section;
    var subSection;
    var roleId;
    var dataType;
    var feedback;
    var ftId;
    var Finalline;
    for(let i=0;i<feedbackArr.length;i++){

        if(feedbackArr[i].subSection == null){
          feedbackArr[i].subSection = '';
        }
        section = feedbackArr[i].section;
        subSection = feedbackArr[i].subSection;
        feedback = feedbackArr[i].feedback;
        dataType = feedbackArr[i].dataType;
        roleId = feedbackArr[i].roleId;
        ftId = feedbackArr[i].ftId;
        const line = section + "|" + subSection + "|" + feedback + "|" + dataType  + "|" + roleId  + "|" + ftId;
      if(i === 0) {
        Finalline = line;
      }else {
        Finalline  += "#" + line;
      }
    }
    return Finalline;
    //console.log('Finalline',Finalline);
  }

  openModal(index){
    this.modal =true;
    this.id = index;
  }

  close(){
    this.modal= false;
  }

 value:any;


  addToArray(value){
    this.modal = false;
        let feedobj={ 
          feedbackDesc: value
        };
        this.users[this.id].feedback.push(feedobj);
        return value = " ";
  }

}
