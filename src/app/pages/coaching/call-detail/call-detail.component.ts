import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { DomSanitizer, SafeHtml} from '@angular/platform-browser';
import { Router, NavigationStart, ActivatedRoute } from '@angular/router';
import { CallDetailService } from './call-detail.service';
import { ToastrService } from 'ngx-toastr';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { webApi } from '../../../service/webApi';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { SuubHeader } from '../../components/models/subheader.model';
@Component({
  selector: 'ngx-call-detail',
  templateUrl: './call-detail.component.html',
  styleUrls: ['./call-detail.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CallDetailComponent implements OnInit {

  contentTab: boolean = true;
  chatTab: boolean = false;
  feedbackTab: boolean = false;
  assessTab: boolean = false;
  obsTab: boolean = false;
  notesTab: boolean = false;
  notes_not_found:boolean = false;
  header: SuubHeader = {
    title: '',
    btnsSearch: true,
    btnBackshow: true,
    showBreadcrumb: true,
    breadCrumbList: []
  };
  notesHtmlStr : SafeHtml = '<p>Hello, your notes will get displayed here. :)</p><ul><li> jhabbdsjbcjadsbsjfbjsad</li><li>jhbdabshfbjabsdjfbajsbdjabsdj</li></ul>';

  constructor(private router: Router, private route: ActivatedRoute, 
    private service: CallDetailService,
    protected sanitizer: DomSanitizer,
    // private toasterService: ToasterService,
     private toastr: ToastrService,private commonFunctionService: CommonFunctionsService) { }

  ngOnInit() {
    this.getCallNote();
  }

  selectedTab(tabEvent) {
    console.log('tab Selected', tabEvent);

    if (tabEvent.tabTitle === 'Content') {
      this.contentTab = true;
      this.chatTab = false;
      this.feedbackTab = false;
      this.assessTab = false;
      this.obsTab = false;
      this.notesTab = false;
      this.header={
        title: 'Call -'+this.service.callId,
        btnsSearch: true,
        btnBackshow: true,
        showBreadcrumb: true,
        breadCrumbList: [
          {
            'name': 'Coaching',
            'navigationPath': '/pages/coaching',
          },
          {
            'name': 'Nominations',
            'navigationPath': '/pages/coaching/participants',
          },
          {
            'name': this.service.empName,
            'navigationPath': '/pages/coaching/participants/calendar',
          }
        ]
      }
    } else if (tabEvent.tabTitle === 'Chat') {
      this.contentTab = false;
      this.chatTab = true;
      this.feedbackTab = false;
      this.assessTab = false;
      this.obsTab = false;
      this.notesTab = false;
      this.header={
        title: 'Call -'+this.service.callId,
        btnsSearch: true,
        btnBackshow: true,
        showBreadcrumb: true,
        breadCrumbList: [
          {
            'name': 'Coaching',
            'navigationPath': '/pages/coaching',
          },
          {
            'name': 'Nominations',
            'navigationPath': '/pages/coaching/participants',
          },
          {
            'name': this.service.empName,
            'navigationPath': '/pages/coaching/participants/calendar',
          }
        ]
      }
    } else if (tabEvent.tabTitle === 'Feedback') {
      this.contentTab = false;
      this.chatTab = false;
      this.feedbackTab = true;
      this.assessTab = false;
      this.obsTab = false;
      this.notesTab = false;
      this.header={
        title: 'Call -'+this.service.callId,
        btnsSearch: true,
        btnBackshow: true,
        showBreadcrumb: true,
        breadCrumbList: [
          {
            'name': 'Coaching',
            'navigationPath': '/pages/coaching',
          },
          {
            'name': 'Nominations',
            'navigationPath': '/pages/coaching/participants',
          },
          {
            'name': this.service.empName,
            'navigationPath': '/pages/coaching/participants/calendar',
          }
        ]
      }
    } else if (tabEvent.tabTitle === 'Assessment') {
      this.contentTab = false;
      this.chatTab = false;
      this.feedbackTab = false;
      this.assessTab = true;
      this.obsTab = false;
      this.notesTab = false;
      this.header={
        title: 'Call -'+this.service.callId,
        btnsSearch: true,
        btnBackshow: true,
        showBreadcrumb: true,
        breadCrumbList: [
          {
            'name': 'Coaching',
            'navigationPath': '/pages/coaching',
          },
          {
            'name': 'Nominations',
            'navigationPath': '/pages/coaching/participants',
          },
          {
            'name': this.service.empName,
            'navigationPath': '/pages/coaching/participants/calendar',
          }
        ]
      }
    } else if (tabEvent.tabTitle === 'Pre-Assessment') {
      this.contentTab = false;
      this.chatTab = false;
      this.feedbackTab = false;
      this.assessTab = false;
      this.obsTab = true;
      this.notesTab = false;
      this.header={
        title: 'Call -'+this.service.callId,
        btnsSearch: true,
        btnBackshow: true,
        showBreadcrumb: true,
        breadCrumbList: [
          {
            'name': 'Coaching',
            'navigationPath': '/pages/coaching',
          },
          {
            'name': 'Nominations',
            'navigationPath': '/pages/coaching/participants',
          },
          {
            'name': this.service.empName,
            'navigationPath': '/pages/coaching/participants/calendar',
          }
        ]
      }
    } else if (tabEvent.tabTitle === 'Notepad') {
      this.contentTab = false;
      this.chatTab = false;
      this.feedbackTab = false;
      this.assessTab = false;
      this.obsTab = false;
      this.notesTab = true;
      this.header={
        title: 'Call -'+this.service.callId,
        btnsSearch: true,
        btnBackshow: true,
        showBreadcrumb: true,
        breadCrumbList: [
          {
            'name': 'Coaching',
            'navigationPath': '/pages/coaching',
          },
          {
            'name': 'Nominations',
            'navigationPath': '/pages/coaching/participants',
          },
          {
            'name': this.service.empName,
            'navigationPath': '/pages/coaching/participants/calendar',
          }
        ]
      }
    }
  }

  back() {
    console.log('back pressed.');
   // this.router.navigate(['../calendar']);
    window.history.back();
  }

  getCallNote() {
    const param = {
      cId: this.service.callId,
      tId: this.service.tenantId,
    };
    console.log('note param', param);
    const _getNote: string = webApi.domain + webApi.url.getCallNote;
    this.commonFunctionService.httpPostRequest(_getNote,param)
    // this.service.getCallNote(param)
    .then(
      rescompData => {
       // this.spinner.hide();
        const result = rescompData;
        console.log('call note:', rescompData);
        if (result['type'] === true) {
          if (rescompData['data'].length > 0) {
            this.notes_not_found = true;
            this.notesHtmlStr = this.sanitizer.bypassSecurityTrustHtml(rescompData['data'][0].note);
          } else {
            this.notesHtmlStr = this.sanitizer.bypassSecurityTrustHtml('');
           
          }
        } else {
		      this.presentToast('error', '');
        }
      },
      error => {
       // this.spinner.hide();
		      this.presentToast('error', '');
      },
    );
  }

  presentToast(type, body) {
    if(type === 'success'){
      this.toastr.success(body, 'Success', {
        closeButton: false
       });
    } else if(type === 'error'){
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else{
      this.toastr.warning(body, 'Warning', {
        closeButton: false
        })
    }
  }
}
