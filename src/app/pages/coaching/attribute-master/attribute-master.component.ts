import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'ngx-attribute-master',
  templateUrl: './attribute-master.component.html',
  styleUrls: ['./attribute-master.component.scss']
})
export class AttributeMasterComponent implements OnInit {
  personList: Array<any> = [
    { id: 1, 
      name: 'Self',
      skill: [
          {
            id:1,
               name:'Personal Gravitas',
               desc:'Is able to carry himself with an air of seriousness that commands respect.',
               PI: 0,
           },{
            id:2,
               name:'Motivation',
               desc:'Is able to set goals for himself - personal and professional',
               PI: 0,
           },{
            id:3,
               name:'Pride',
               desc:'Is able to display organizational citizenship',
               PI: 0,
           },{
            id:4,
               name:'Aspirations',
               desc:'Is confident, assertive and focused',
               PI: 0,
           },{
            id:5,
               name:'Interpersonal Effectivness',
               desc:'Is open and approachable',
               PI: 0,
           }
           ],
      // skill1: 'Personal Gravitas',
      // skill2: 'Motivation',
      // skill3: 'Pride',
      // skill4: 'Aspirations',
      // skill5: 'Interpersonal Effectivness',
      FS5: 0, 
      FS: 0, 
      FGD: 0,
      PIAvg: 0,
      WeiAvg: 0 
    },
    { id: 2, 
      name: 'Team', 
      skill: [
          {
            id:1,
               name:'Delegating and Empowering',
               desc:'Is able to bring disparate group together to deliver results',
               PI: 0,
           },{
            id:2,
               name:'Team Synergies',
               desc:'Is able to avoid “taking sides” when mediating a conflict',
               PI: 0,
           },{
            id:3,
               name:'Cooaching And FeedBack',
               desc:' Is able to give meaningful and timely feedback which is individualized to fit the specific person and situation',
               PI: 0,
           },{
            id:4,
               name:'Recognition',
               desc:'Is able to identify and address the intrinsic as well as extrinsic motivators of team members',
               PI: 0,
           }
           ],
      // skill1: 'Personal Gravitas',
      // skill2: 'Motivation',
      // skill3: 'Pride',
      // skill4: 'Aspirations',
      // skill5: 'Interpersonal Effectivness',
      FS5: 0, 
      FS: 0, 
      FGD: 0,
      PIAvg: 0,
      WeiAvg: 0  
    },
    { id: 3, 
      name: 'Business', 
      skill: [
          {
            id:1,
               name:'Business Acumen',
               desc:' Is able to make decisions based on facts and not intuition',
               PI: 0,
           },{
            id:2,
               name:'Execution Excellence',
               desc:'Is able to adhere to processes and workflows',
               PI: 0,
           },{
            id:3,
               name:'Strategic Conventions',
               desc:'Is able to have creative and challenging conversations with team members',
               PI: 0,
           },{
            id:4,
               name:'Customer/Stakeholer Engagement',
               desc:'Is able to build and sustain customer stakeholder relationships',
               PI: 0,
           }
           ],
      // skill1: 'Personal Gravitas',
      // skill2: 'Motivation',
      // skill3: 'Pride',
      // skill4: 'Aspirations',
      // skill5: 'Interpersonal Effectivness',
      FS5: 0, 
      FS: 0, 
      FGD: 0,
      PIAvg: 0,
      WeiAvg: 0 
    },
    { id: 4, 
      name: 'Enviornment', 
      skill: [
          {
            id:1,
               name:'Proactive',
               desc:'Is able to to display willingness and flexibility to take on a broader role at work',
               PI: 0,
           },{
            id:2,
               name:'Open to Change',
               desc:'Is able to explore ideas which run counter to the status quo',
               PI: 0,
           },{
            id:3,
               name:'Driving Change',
               desc:'Is able to create an environment of collaboration and trust amongst various teams towards change',
               PI: 0,
           },{
            id:4,
               name:'Willingness to Risk',
               desc:'Is able to take risk and move out of comfort zone',
               PI: 0,
           },{
            id:5,
               name:'Innovation enablement',
               desc:'Is able to generate new ideas and ways of working that enhance productivity / efficiency',
               PI: 0,
           }
           ],
      // skill1: 'Personal Gravitas',
      // skill2: 'Motivation',
      // skill3: 'Pride',
      // skill4: 'Aspirations',
      // skill5: 'Interpersonal Effectivness',
      FS5: 0, 
      FS: 0, 
      FGD: 0,
      PIAvg: 0,
      WeiAvg: 0 
    },
    // { id: 5, name: 'Elisa Gallagher', age: 31, companyName: 'Portica', country: 'United Kingdom', city: 'London' },
  ];
  
  constructor(private router:Router,private routes:ActivatedRoute) { }

  ngOnInit() {
  }
  
  gotoAddEdit(){
    this.router.navigate(['add-attribute-master'],{relativeTo:this.routes});
  }

  gotoBack(){
    this.router.navigate(['../../coaching'],{relativeTo:this.routes});
	}

}
