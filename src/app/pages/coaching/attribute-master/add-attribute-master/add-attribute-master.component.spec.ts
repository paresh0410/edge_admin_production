import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAttributeMasterComponent } from './add-attribute-master.component';

describe('AddAttributeMasterComponent', () => {
  let component: AddAttributeMasterComponent;
  let fixture: ComponentFixture<AddAttributeMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAttributeMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAttributeMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
