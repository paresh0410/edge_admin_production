import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormGroupDirective } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'ngx-add-attribute-master',
  templateUrl: './add-attribute-master.component.html',
  styleUrls: ['./add-attribute-master.component.scss']
})
export class AddAttributeMasterComponent implements OnInit {
  registerForm: FormGroup;
	submitted = false;
	status:any=[
    	{
    		id:1,
    		label:'Active',
    	},
    	{
    		id:2,
    		label:'Inactive',
    	}
    ];

     addEditAttr:any=[
		[
			{
		    	attribute:'',
		    	skill:'',
		    	description:'',
		    	status:1,
		    	display:true,
		    }
		]
    ];
    addSkillVar:any={
    	attribute:'',
    	skill:'',
    	description:'',
    	status:1,
    	display:false,
    };
    addAttrVar:any={
    	attribute:'',
    	skill:'',
    	description:'',
    	status:1,
    	display:true,
    };

  constructor(private formBuilder: FormBuilder, private router:Router,private routes:ActivatedRoute) { }

  ngOnInit() {
  	this.registerForm = this.formBuilder.group({
            attribute: ['', Validators.required],
            skill: ['', Validators.required],
            description: ['', Validators.required],
            status: ['', Validators.required],
        });
  }

   addSkill(index){
    	this.addEditAttr[index].push(this.addSkillVar)
    }

    addAttr(){
    	var array=[];
    	array.push(this.addAttrVar);
    	this.addEditAttr.push(array);
    }

    gotoBack(){
      this.router.navigate(['../../attribute-master'],{relativeTo:this.routes})
    }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }

        alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value))
    }

    gotoAddEdit() {}

}
