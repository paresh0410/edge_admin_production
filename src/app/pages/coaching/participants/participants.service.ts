import {Injectable, Inject} from '@angular/core';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppConfig} from '../../../app.module';
import { webApi } from '../../../service/webApi';
import { HttpClient } from "@angular/common/http";

@Injectable()
export class ParticipantsService {

  data : any= [
    {
        'username': 'Ramsey Cumg',
        'business': 'Business4',
        'department': 'Department1',
        'manager': 'Hallie Mack',
        'level': '7',
        'date' : '',
        'visible' : '',

    },
    {
        'username': 'Stefanie Huff',
        'business': 'Bussiness1',
        'department': 'Department1',
        'manager': 'Hallie Mack',
        'level': '7',
        'date' : '',
        'visible' : '',
    },
    {
        'username': 'Mabel David',
        'business': 'Bussiness1',
        'department': 'Department1',
        'manager': 'Hallie Mack',
        'level':'7',
        'date' : '',
        'visible' : ''
    },
    {
        'username': 'Forbes Levine',
        'business': 'Bussiness1',
        'department': 'Department2',
        'manager': 'Hallie Mack',
        'level':'7',
        'date' : '',
        'visible' : ''
    },
    {
        'username': 'Sango Mclain',
        'business': 'Bussiness2',
        'department': 'Department2',
        'manager': 'Hallie Mack',
        'level':'7',
        'date' : '',
        'visible' : ''
    },
    {
        'username': 'Merritt Booker',
        'business': 'Bussiness2',
        'department': 'Department2',
        'manager': 'Hallie Mack',
        'level':'7',
        'date' : '',
        'visible' : ''
    }
    ];



    tenantId ;
    getAllCalls: boolean = false;
    userData;
    allEmployess:any = [];
    addEditParticipants : any = [];
  private _urlFetchAllParticipants: string = webApi.domain + webApi.url.getallemployeecc;
  private _urlFetchAllNominatedEmployee: string = webApi.domain + webApi.url.getallnominatedemployees;
  private _urlChangeParticipantStatus: string = webApi.domain + webApi.url.changeparticipantstatus;


  constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private _http: Http,private _httpClient:HttpClient){
      //this.busy = this._http.get('...').toPromise();
      if (localStorage.getItem('LoginResData')) {
        this.userData = JSON.parse(localStorage.getItem('LoginResData'));
        console.log('userData', this.userData.data);
        // this.userId = userData.data.data.id;
      }
      this.tenantId = this.userData.data.data.tenantId;
  }


  //data pull from json

  get_data(){
    //  return this.httpClient.get(this.baseUrl + '/courses');
    return this.data;
  
  }

  getAllEmployees(param){
    return new Promise(resolve => {
      this._httpClient.post(this._urlFetchAllParticipants, param)
      //.map(res => res.json())
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}

getAllNominatedEmployees(param){
    return new Promise(resolve => {
      this._httpClient.post(this._urlFetchAllNominatedEmployee, param)
      //.map(res => res.json())
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}

changeParticipantStatus(param){
  return new Promise(resolve => {
    this._httpClient.post(this._urlChangeParticipantStatus, param)
    //.map(res => res.json())
    .subscribe(data => {
        resolve(data);
    },
    err => {
        resolve('err');
    });
});
}


_errorHandler(error: Response){
  console.error(error);
  return Observable.throw(error || "Server Error")
}




}
