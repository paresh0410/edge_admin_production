import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CommonModule } from '@angular/common';

import { ReactiveFormsModule, FormsModule} from '@angular/forms';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';


import { ParticipantsComponent } from './participants.component';
import { AddParticipantsComponent } from './add-participants/add-participants.component';
import { MyDatePickerModule } from 'mydatepicker';
import { ParticipantsService } from './participants.service';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { AddParticipantsService } from './add-participants/add-participants.service'
import { FilterPipeModule } from 'ngx-filter-pipe';
import { BulkUploadCoachingComponent } from './bulk-upload-coaching/bulk-upload-coaching.component';
import { BulkUploadCoachingService } from './bulk-upload-coaching/bulk-upload-coaching.service';
import {JsonToXlsxService} from './bulk-upload-coaching/json-to-xlsx.service';
import {ComponentModule} from '../../../component/component.module';


@NgModule({
    imports: [
        CommonModule,
        NgxDatatableModule,
        FormsModule,
        ReactiveFormsModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        MyDatePickerModule,
        AngularMultiSelectModule,
        FilterPipeModule,
        ComponentModule

        
    ],

    declarations: [
        ParticipantsComponent,
        AddParticipantsComponent,
        BulkUploadCoachingComponent,
    ],

    providers: [ 
        ParticipantsService,
        AddParticipantsService,
        BulkUploadCoachingService,
        JsonToXlsxService,
    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
     ]
      
})
      
export class ParticipantModule{
      
      }