import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkUploadCoachingComponent } from './bulk-upload-coaching.component';

describe('BulkUploadComponent', () => {
  let component: BulkUploadCoachingComponent;
  let fixture: ComponentFixture<BulkUploadCoachingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BulkUploadCoachingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkUploadCoachingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
