import { Component, OnInit, ViewChild } from '@angular/core';
// import { ToasterService, Toast } from 'angular2-toaster';
import { NgxSpinnerService } from 'ngx-spinner';
import { webAPIService } from '../../../../service/webAPIService';
// import { webApi } from '../../../service/webApi';
import { DatePipe } from '@angular/common';
import { BulkUploadCoachingService } from './bulk-upload-coaching.service';
import { Router, ActivatedRoute } from '@angular/router';
import { XlsxToJsonService } from '../../../plan/users/uploadusers/xlsx-to-json-service';
import { JsonToXlsxService } from './json-to-xlsx.service'
import { NULL_EXPR } from '@angular/compiler/src/output/output_ast';
import { Switch } from '@syncfusion/ej2-buttons';
import { ToastrService } from 'ngx-toastr';
import { SuubHeader } from '../../../components/models/subheader.model';
import { BrandDetailsService } from '../../../../service/brand-details.service';
import { CommonFunctionsService } from '../../../../service/common-functions.service';
import { dataBound } from '@syncfusion/ej2-ng-grids';

@Component({
  selector: 'ngx-bulk-upload-coaching',
  templateUrl: './bulk-upload-coaching.component.html',
  styleUrls: ['./bulk-upload-coaching.component.scss']
})
export class BulkUploadCoachingComponent implements OnInit {
  @ViewChild('myTable') table: any;
  private xlsxToJsonService: XlsxToJsonService = new XlsxToJsonService();
  rows = [];
  selected = [];
  templateExelLink: any = 'assets/images/Asset_Upload_Template.xlsx';
  /*
    uploadDetailsOne: any = [
      { assetName: "B2B sales", date: '12/04/2019', category: 'Abc', format: '.xls', channel: 'akdf', language: 'english' },
      { assetName: "B2B sales", date: '12/04/2019', category: 'Abc', format: '.xls', channel: 'akdf', language: 'english' },
      { assetName: "B2B sales", date: '12/04/2019', category: 'Abc', format: '.xls', channel: 'akdf', language: 'english' },
      { assetName: "B2B sales", date: '12/04/2019', category: 'Abc', format: '.xls', channel: 'akdf', language: 'english' },
      { assetName: "B2B sales", date: '12/04/2019', category: 'Abc', format: '.xls', channel: 'akdf', language: 'english' },
      { assetName: "B2B sales", date: '12/04/2019', category: 'Abc', format: '.xls', channel: 'akdf', language: 'english' },
      { assetName: "B2B sales", date: '12/04/2019', category: 'Abc', format: '.xls', channel: 'akdf', language: 'english' },
    ]
    */
  userId: any;
  AllEmployess: any = [];
  Allpartners: any = [];
  allNominatedEmp: any = [];
  AllAssetDropdown: any = [];
  resultSheets: any = [];
  result: any = [];
  uploadedData: any = [];
  damapproval: any = [];
  lang: any = [];
  fileformatf: any = [];
  filechannel: any = [];
  cat: any = [];
  validdata: any = [];
  invaliddata: any = [];
  tenantId: any = 1;
  header: SuubHeader  = {
    title:'Bulk Upload',
    btnsSearch: true,
    searchBar: false,
    dropdownlabel: ' ',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: '',
    btnName3: '',
    btnAdd: '',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: false,
    filter: false,
    showBreadcrumb: true,
    breadCrumbList:[
      {
        'name': 'Coaching',
        'navigationPath': '/pages/coaching',
      },
      {
        'name': 'Nominations',
        'navigationPath': '/pages/coaching/participants',
      },]
  };
  currentBrandData: any;
  // dateFormatNew = /^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/g.;

  constructor(
    // private toasterService: ToasterService, 
    private spinner: NgxSpinnerService, public router: Router,
    public routes: ActivatedRoute, protected webApiService: webAPIService, private datePipe: DatePipe,
    public brandService: BrandDetailsService,
    private bulkUploadService: BulkUploadCoachingService, private exportService: JsonToXlsxService,
    private commonFunctionService: CommonFunctionsService,
    private toastr: ToastrService, ) {
    // this.rows = this.uploadDetailsOne;
    if (localStorage.getItem('LoginResData')) {
      const userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', userData.data);
      this.userId = userData.data.data.id;
      console.log('userId', userData.data.data.id);
      this.tenantId = userData.data.data.tenantId;
    }

    this.getAllEmployees();
    this.getAllparterns();
    this.getAllNominatedEmployees();
    // this.getAllAssetDropdown();
  }

  ngOnInit() {
    this.currentBrandData = this.brandService.getCurrentBrandData();
    this.fileName='Click here to upload an excel file to enrol '+ this.currentBrandData.employee.toLowerCase() +' to this course'
  }

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }

  bulkUploadAssetData: any = null;
  fileReaded: any;
  enableUpload: boolean = false;
  fileUrl: any;


  fileUpload: any;
  fileName: String = 'You can drag and drop files here to add them.';
  fileIcon: any = 'assets/img/app/profile/avatar4.png';
  selectFileTitle: any = 'No file chosen';
  cancelFile() {
    this.bulkUploadAssetData = null;
    if (this.fileUpload) {
      if (this.fileUpload.nativeElement) {
        this.fileUpload.nativeElement.value = null;
      }
    }
    this.fileName = 'You can drag and drop files here to add them.';
    this.selectFileTitle = 'No file chosen';
  }

  passParams: any;
  assetFileData: any;
  fileUploadRes: any;
  fileres: any;
  errorMsg: any;
  uploadedFile: any;
  submit() {
    if (this.uploadedData.length > 0 && this.uploadedData.length <= 2000) {
      this.spinner.show();
      // const option: string = Array.prototype.map
      //   .call(this.uploadedData, function (item) {
      //     console.log('item', item);
      //     return Object.values(item).join('#');
      //   })
      //   .join('|');

      //New Requirement

      var option: string = Array.prototype.map
      .call(this.uploadedData, (item) => {
        const array = Object.keys(item);
        let string = '';
        array.forEach(element => {
          if (element === 'nomination Date') {
            string += this.commonFunctionService.formatDateTimeForBulkUpload(item[element]);
          } else {
            string += item[element] + '#';
          }
        });
        // console.log("item", item);
        // return Object.values(item).join("#");
        return string;
      })
      .join("|");


      const param = {
        'tId': this.tenantId,
        'userId': this.userId,
        'option': option
      };
      console.log('params', param);
      this.bulkUploadService.insertAssetBulkUpload(param)
        .then(rescompData => {
          this.spinner.hide();
          const result = rescompData;
          console.log('Bulk Upload assets:', rescompData);
          if (result['type'] == true) {
            // const toast: Toast = {
            //   type: 'success',
            //   title: 'Bulk Upload.',
            //   body: 'Uploaded successfully.',
            //   showCloseButton: true,
            //   timeout: 2000,
            // };
            if (result['data'].length > 0) {
              this.invaliddata = result['data'];
              this.showInvalidExcel = true;
              if (this.uploadedData.length == this.invaliddata.length) {
                this.toastr.warning('No valid '+this.currentBrandData.employee+'s found to enrol.', 'Warning', {
                  closeButton: false
                  });
              }
              else if(this.uploadedData.length > this.invaliddata.length){
                this.toastr.success('Enrol successfully.', 'Success', {
                  closeButton: false
                  });
                  // this.invaliddata = [];
                  // this.showInvalidExcel = false;
              }
            } else if(result['data'].length == 0) {
                this.toastr.success('Enrol successfully.', 'Success', {
                  closeButton: false
                  });
              }
              
             else {
              this.invaliddata = [];
              this.showInvalidExcel = false;
            }
           
            this.spinner.hide();
            // this.toasterService.pop(toast);
         //   this.presentToast('success', 'Participants uploaded')
           // this.back();
          } else {
            // var toast: Toast = {
            //   type: 'error',
            //   title: 'Bulk Upload.',
            //   body: 'Unable to upload.',
            //   showCloseButton: true,
            //   timeout: 2000,
            // };
            this.spinner.hide();
            // this.toasterService.pop(toast);
            this.presentToast('error', '')
          }
  
        }, error => {
          this.spinner.hide();
          // var toast: Toast = {
          //   type: 'error',
          //   //title: "Server Error!",
          //   body: 'Something went wrong.please try again later.',
          //   showCloseButton: true,
          //   timeout: 2000,
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '')
        });
    } else {
      if(this.uploadedData.length > 2000)
      this.toastr.warning('File Data cannot exceed more than 2000', 'warning', {
        closeButton: true
      });
      
      else{
      this.toastr.warning('No Data Found', 'warning', {
        closeButton: true
      });
      }
    }
    
  }

  back() {
    this.router.navigate(['../'], { relativeTo: this.routes });
  }

  empobj: any = {}
  getAllEmployees() {
    const param = {
      'srcStr': null,
      'tenantId': this.tenantId,
    };
    this.bulkUploadService.getAllEmpListUP(param)
      .then(rescompData => {
        this.AllEmployess = rescompData['data'][0];
        if (this.AllEmployess.length > 0) {
          this.AllEmployess.forEach((element, i) => {
            this.empobj[element.code] = i + 1;
          });
          console.log('empobj:', this.empobj);
        }
        console.log('AllEmployess:', this.AllEmployess);
      });
  }
  partObj: any = {};
  getAllparterns() {
    const param = {
      'tId': this.tenantId,
    };
    this.bulkUploadService.getAllpartnersListUP(param).then(res => {
      this.Allpartners = res['data'][0];
      if (this.Allpartners.length > 0) {
        this.Allpartners.forEach((element, i) => {
          this.partObj[element.partnerName] = i + 1;
        });
        console.log('partObj:', this.partObj);
      }
      console.log('AllEdgeEmployessDAM:', this.Allpartners);
    });
  }
  nomiObj: any = {};
  getAllNominatedEmployees() {
    const param = {
      'tenantId': this.tenantId,
    };
    this.bulkUploadService.getAllNominatedEmployees(param).then(res => {
      this.allNominatedEmp = res['data'][0];
      if (this.allNominatedEmp.length > 0) {
        this.allNominatedEmp.forEach((element, i) => {
          this.nomiObj[element.code] = i + 1;
        });
        console.log('nomiObj:', this.nomiObj);
      }
      console.log('AllEdgeEmployessDAM:', this.allNominatedEmp);
    });
  }


  readFileUrl(event: any) {
    this.uploadedData = [];
    this.result = [];
    const validExts = new Array('.xlsx', '.xls');
    let fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
      this.presentToast('warning', 'Valid file types are ' + validExts.toString());
      this.cancelFile();
    } else {
      // return true;
      if (event.target.files && event.target.files[0]) {
        this.fileName = event.target.files[0].name;
        // read file from input
        this.fileReaded = event.target.files[0];

        if (this.fileReaded != '' || this.fileReaded != null || this.fileReaded != undefined) {
          this.enableUpload = true;
        }

        let file = event.target.files[0];
        this.bulkUploadAssetData = event.target.files[0];
        console.log('this.bulkUploadAssetDataFileRead', this.bulkUploadAssetData);

        this.xlsxToJsonService.processFileToJson({}, file).subscribe(data => {
          this.resultSheets = Object.getOwnPropertyNames(data['sheets']);
          console.log('File Property Names ', this.resultSheets);
          let sheetName = this.resultSheets[0];
          this.result = data['sheets'][sheetName];
          console.log('dataSheet', data);

          if (this.result.length > 0) {
            this.uploadedData = this.result;
          }
        });
        var reader = new FileReader();
        reader.onload = (event: ProgressEvent) => {
          this.fileUrl = (<FileReader>event.target).result;
          /// console.log(this.fileUrl);
        };
        reader.readAsDataURL(event.target.files[0]);
      }
    }
  }
  uploadSheet() {
    if (this.uploadedData.length > 0) {
      this.validdata = [];
      this.invaliddata = [];
      console.log('this.AllEmployess', this.AllEmployess);
      console.log('this.Allpartners', this.Allpartners);
      console.log('this.uploadedData', this.uploadedData);
      if (this.AllEmployess.length > 1) {
        // this.AllEmployess.some((item,i) => {
        this.uploadedData.forEach((data, j) => {
          if (!data.ecn) {
            this.uploadedData[j].status = 'Invalid';
            this.uploadedData[j].reason = 'ECN is required';
            return;
          } else if (!this.empobj[data.ecn]) {
            this.uploadedData[j].status = 'Invalid';
            this.uploadedData[j].reason = 'Invalid ECN';
            return;
          } else if (this.nomiObj[data.ecn]) {
            this.uploadedData[j].status = 'Invalid';
            this.uploadedData[j].reason = 'User already nominated';
            return;
          } else if (!data.partner) {
            this.uploadedData[j].status = 'Invalid';
            this.uploadedData[j].reason = 'Partner is required';
            return;
          } else if (!this.partObj[data.partner]) {
            this.uploadedData[j].status = 'Invalid';
            this.uploadedData[j].reason = 'Invalid Partner';
            return;
          } else if (!data.assessment) {
            this.uploadedData[j].status = 'Invalid';
            this.uploadedData[j].reason = 'Assessment is required ';
            return;
          } else if (!data.date) {
            this.uploadedData[j].status = 'Invalid';
            this.uploadedData[j].reason = 'Date is required ';
            return;
          } else if (!(/^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/g.test(data.date))) {
            this.uploadedData[j].status = 'Invalid';
            this.uploadedData[j].reason = 'Invalid date format.Use DD/MM/YYYY date format';
            return;
          } else {
            this.uploadedData[j].status = 'Valid';
            this.uploadedData[j].reason = '';
            return;
          }
        });
        console.log('this.uploadedData', this.uploadedData);
      }
      if (this.uploadedData.length > 0) {
        this.uploadedData.forEach(element => {
          if (element.status === 'Valid') {
            delete element.status;
            delete element.reason;
            element.date = element.date.split("/").reverse().join("-")
            this.validdata.push(element);
          } else {
            this.invaliddata.push(element);
          }
        });
      }
      this.spinner.hide();
      if (this.invaliddata.length > 0) {
        this.showInvalidExcel = true;
      }
      console.log('this.invaliddata', this.invaliddata);
      console.log('this.validdata', this.validdata);
    } else {
      this.presentToast('warning', 'Please Select a file first');
    }
  }

  showInvalidExcel: boolean = false;

  exportToExcelInvalid() {
    this.exportService.exportAsExcelFile(this.invaliddata, 'Invalid Data')
  }
}
