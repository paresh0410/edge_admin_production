import {Injectable, Inject} from '@angular/core';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppConfig} from '../../../../app.module';
import { webApi } from '../../../../service/webApi';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AddParticipantsService {

    allEmployess:any = [];

  private _urlAddEditEmployee: string = webApi.domain + webApi.url.addeditnominatedemployee;
  private _urlGetAllPartners: string = webApi.domain + webApi.url.getAllPartners;

  constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private _http: Http,private _httpClient:HttpClient){
      //this.busy = this._http.get('...').toPromise();
  }

  addEditNominatedEmployees(param) {
    return new Promise(resolve => {
      this._httpClient.post(this._urlAddEditEmployee, param)
      //.map(res => res.json())
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}

getAllPartners(param) {
  return new Promise(resolve => {
    this._httpClient.post(this._urlGetAllPartners, param)
    //.map(res => res.json())
    .subscribe(data => {
        resolve(data);
    },
    err => {
        resolve('err');
    });
});
}


_errorHandler(error: Response){
  console.error(error);
  return Observable.throw(error || "Server Error")
}


}
