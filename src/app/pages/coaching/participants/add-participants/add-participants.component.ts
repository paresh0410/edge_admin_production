import { Component, OnInit, ViewEncapsulation, ChangeDetectorRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';
import { ParticipantsService } from './../participants.service';
// import { ToasterService, Toast } from 'angular2-toaster';
import { NgxSpinnerService } from 'ngx-spinner';
import { AddParticipantsService } from './add-participants.service';
import { webApi } from '../../../../service/webApi';
import { webAPIService } from '../../../../service/webAPIService';
import { HttpClient } from '@angular/common/http';
import { FileSaverService } from 'ngx-filesaver';
import { ToastrService } from 'ngx-toastr';
import * as _moment from 'moment';
import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';
import { SuubHeader } from '../../../components/models/subheader.model';
import { BrandDetailsService } from '../../../../service/brand-details.service';
const moment = (_moment as any).default ? (_moment as any).default : _moment;

export const MY_CUSTOM_FORMATS = {
  fullPickerInput: 'DD-MM-YYYY HH:mm:ss',
  parseInput: 'DD-MM-YYYY HH:mm:ss',
  datePickerInput: 'DD-MM-YYYY',
  timePickerInput: 'LT',
  monthYearLabel: 'MMM YYYY',
  dateA11yLabel: 'LL',
  monthYearA11yLabel: 'MMMM YYYY'
};
@Component({
  selector: 'ngx-add-participants',
  templateUrl: './add-participants.component.html',
  styleUrls: ['./add-participants.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    { provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE] },
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS }],
})
export class AddParticipantsComponent implements OnInit {
  // formControlValue:any;
  selected: boolean = true;
  selectFileTitle: any = 'No file chosen';
  fileUrl: any;
  enableUpload: boolean = false;
  fileReaded: any;
  fileUpload: any;
  fileName: String = 'You can drag and drop files here to add them.';
  registerForm: FormGroup;
  submitted = false;
  userData: any = {};
  header: SuubHeader  = {
    title:'',
    btnsSearch: true,
    btnName1: 'Save',
    btnName1show: true,
    btnBackshow: true,
    showBreadcrumb: true,
    breadCrumbList:[
      {
        'name': 'Coaching',
        'navigationPath': '/pages/coaching',
      },
      {
        'name': 'Nominations',
        'navigationPath': '/pages/coaching/participants',
      },]
  }
  partners: any = [];
  allEmployeesAuto: any = [];
  allEmployeeAutoDrop: any = [];
  selectedCreator: any = [];
  settingsCreatorDrop = {};
  usersList: any = [];
  userId: any;
  getDataForAddEdit: any = [];
  addAction: boolean = false;
  tenantId: any;
  disable: boolean;
  currentBrandData: any;
  constructor(private formBuilder: FormBuilder, private router: Router, private routes: ActivatedRoute,
    private participantsservice: ParticipantsService, private spinner: NgxSpinnerService,
    // private toasterService: ToasterService, 
    private addparticipantservice: AddParticipantsService,
    private webApiService: webAPIService, private _httpClient: HttpClient, private toastr: ToastrService,
    private _FileSaverService: FileSaverService, private cdf: ChangeDetectorRef, private http1: HttpClient,
    public brandService: BrandDetailsService,
  ) {
    // this.getHelpContent();
    this.allEmployeesAuto = this.participantsservice.allEmployess;
    console.log('this.allEmployeesAuto', this.allEmployeesAuto);
    this.prepareEmployeeDropdown();
    this.settingsCreatorDrop = {
      text: 'Search & select employee',
      singleSelection: true,
      classes: 'common-multi',
      primaryKey: 'empId',
      labelKey: 'empName',
      noDataLabel: 'Search & select employee ...',
      enableSearchFilter: true,
      searchBy: ['empCode', 'empName'],
      lazyLoading: true,
      maxHeight:250,
    };

    if (localStorage.getItem('LoginResData')) {
      const userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', userData.data);
      this.userId = userData.data.data.id;
      this.tenantId = userData.data.data.tenantId;
    }

    this.getDataForAddEdit = this.participantsservice.addEditParticipants;
    console.log('this.getDataForAddEdit', this.getDataForAddEdit);

    this.getAllPartners();
    // this.getDataReady();
  }

  ngOnInit() {
    this.header.title=this.getDataForAddEdit[1]?this.getDataForAddEdit[1].name:"Add Paricipants"
    this.registerForm = this.formBuilder.group({
      business: ['', Validators.required],
      department: ['', Validators.required],
      location: ['', Validators.required],
      zone: ['', Validators.required],
      selectedCreator: ['', Validators.required],
      date: ['', Validators.required],
      partner: ['', Validators.required],
    });
    this.currentBrandData = this.brandService.getCurrentBrandData();
    if(this.currentBrandData.employee=="Employee"){
      this.getHelpContent();
    }else{
      this.getContent();
    }
  }

  getDataReady() {
    if (this.getDataForAddEdit[0] === 'ADD') {
      this.addAction = true;
      this.disable = false;
      this.userData = {
        nomEmpId: '',
        business: '',
        department: '',
        location: '',
        zone: '',
        date: '',
        partner: '',
      };
    } else {
      this.addAction = false;
      this.disable = true;
      if (this.getDataForAddEdit[1].preAssessRef) {
        console.log('fileNamexx', this.getDataForAddEdit[1].preAssessRef.lastIndexOf('/') + 1);
        this.fileName = this.getDataForAddEdit[1].preAssessRef
          .substr(this.getDataForAddEdit[1].preAssessRef.lastIndexOf('/') + 1);
        console.log('fileName', this.fileName);
      }
      this.userData = {
        nomEmpId: this.getDataForAddEdit[1].nomEmpId,
        business: this.getDataForAddEdit[1].business,
        department: this.getDataForAddEdit[1].department,
        location: this.getDataForAddEdit[1].location,
        zone: this.getDataForAddEdit[1].zone,
        date: new Date(this.getDataForAddEdit[1].nomDate),
        preAssessRef: this.getDataForAddEdit[1].preAssessRef,
        partner: this.getDataForAddEdit[1].partnerId,

      }
      console.log('this.userData', this.userData);


      //  this.selectedCreator = [];

      const empObj = {
        empId: this.getDataForAddEdit[1].nomEmpId,
        empCode: this.getDataForAddEdit[1].code,
        empName: this.getDataForAddEdit[1].name,
      };
      this.selectedCreator.push(empObj);
      this.allEmployeeAutoDrop.push(empObj);
      // for ( let i = 0; i < this.allEmployeesAuto.length; i++) {
      //   if (this.allEmployeesAuto[i].id ==  this.userData.nomEmpId) {
      //     let allEmployeeAutoDropObj = {};
      //     allEmployeeAutoDropObj = {
      //         empId : this.allEmployeesAuto[i].id,
      //         empName : this.allEmployeesAuto[i].name,
      //       };
      //     this.selectedCreator.push(allEmployeeAutoDropObj);
      //     break;
      //   }
      // }
      console.log('this.allEmployeeAutoDrop', this.allEmployeeAutoDrop);
    }
  }

  getAllPartners() {
    const param = {
      'tId': this.tenantId,
    };
    this.addparticipantservice.getAllPartners(param)
      .then(rescompData => {
        const result = rescompData;
        console.log('partner Dropdown', result);
        if (result['type'] === true) {
          this.partners = result['data'][0];
          this.getDataReady();
        } else {
          this.presentToast('error', ' ');
        }
      },
        resUserError => {
          this.presentToast('error', '');
        });

  }

  readFileUrl(event: any) {
    const size = 100000000;
    const validExts = new Array('pdf');
    const fileType = event.target.files[0].type;

    let fileExt = false;
    for (let i = 0; i < validExts.length; i++) {
      if (fileType.includes(validExts[i])) {
        fileExt = true;
        break;
      }
    }
    if (!fileExt) {
      this.presentToast('warning', 'Valid file types are  ' + validExts.toString());
      this.cancelFile();
    } else {
      if (size <= event.target.files[0].size) {

        this.presentToast('warning', 'File size should be less than 100 MB.');
        this.cancelFile();
      } else {
        if (event.target.files && event.target.files[0]) {
          this.fileName = event.target.files[0].name;
          //read file from input
          this.fileReaded = event.target.files[0];
          this.assessmentFileData = event.target.files[0];
          console.log('assementFile', this.assessmentFileData);
          this.selected = false;
          if (this.fileReaded != '' || this.fileReaded != null || this.fileReaded != undefined) {
            this.enableUpload = true;
          }
          //let file = event.target.files[0];
          const reader = new FileReader();
          reader.onload = (event: ProgressEvent) => {
            this.fileUrl = (<FileReader>event.target).result;
          };
          reader.readAsDataURL(event.target.files[0]);
        }
      }
    }
  }

  cancelFile() {
    // this.fileUpload.nativeElement.files = [];
    this.assessmentFileData = null;
    // console.log(this.fileUpload.nativeElement.files);
    if (this.fileUpload) {
      if (this.fileUpload.nativeElement) {
        this.fileUpload.nativeElement.value = '';
      }
    }
  }

  noEmployees: boolean = false;
  allEmployees: any = [];
  getAllEmployeesCC(str) {

    this.spinner.show();
    const param = {
      'srcStr': str,
      'tenantId': this.tenantId,
    };
    this.participantsservice.getAllEmployees(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('getAllEmployessCC:', rescompData);
        if (result['type'] === true) {
          if (result['data'][0].length === 0) {
            this.noEmployees = true;
          } else {
            this.allEmployees = result['data'][0];
            console.log('this.allEmployees', this.allEmployees);
            this.prepareEmployeeDropdown();
            //this.participantsservice.allEmployess = this.allEmployees;
            console.log('this.participantsservice.getAllEmployees', this.participantsservice.getAllEmployees);
          }
        } else {
          this.presentToast('error', '');
        }
      }, error => {
        this.spinner.hide();
        this.presentToast('error', '');
      });

  }







  // gotoCalendar() {

  // }
  prepareEmployeeDropdown() {
    this.allEmployeeAutoDrop = [];
    this.allEmployees.forEach(element => {
      let allEmployeeAutoDropObj = {};
      allEmployeeAutoDropObj = {
        empId: element.id,
        empCode: element.code,
        empName: element.name,
      },
        this.allEmployeeAutoDrop.push(allEmployeeAutoDropObj);

    });
    console.log('this.allEmployeeAutoDrop', this.allEmployeeAutoDrop);
  }


  onCreatorSelect(item: any) {
    console.log(item);
    console.log(this.selectedCreator);
    console.log('this.allEmployee', this.allEmployees);
    this.allEmployees.forEach(element => {
      if (item.empId == element.id) {
        this.userData = {
          business: element.business,
          department: element.department,
          level: element.level,
          location: element.location,
          zone: element.zone,

        };
      }
    });
    console.log('this.userData', this.userData);
  }
  OnCreatorDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedCreator);
  }

  onCreatorSearch(evt: any) {
    console.log(evt.target.value);
    const val = evt.target.value;
    if (val.length > 3) {
      this.getAllEmployeesCC(val);
    }
    //   this.usersList = [];
    //   const tuser = this.allEmployeeAutoDrop.filter(function(d) {
    //   return String(d.empId).toLowerCase().indexOf(val) !== -1 ||
    //   d.empName.toLowerCase().indexOf(val) !== -1 || !val;
    // });

    // // update the rows
    // this.usersList = tuser;
  }

  gotoBack() {
    this.router.navigate(['../../participants'], { relativeTo: this.routes });
  }

  // convenience getter for easy access to form fields
  //  get f() { return this.registerForm.controls; }

  //  onSubmit() {
  //      this.submitted = true;

  //      // stop here if form is invalid
  //      if (this.registerForm.invalid) {
  //          return;
  //      }

  //      alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value))
  //  }



  assessmentFileData: any = null;



  fileUploadRes: any;
  @ViewChild('f') editParticipants: NgForm;

 
   
  submit(userData) {
    console.log('userData', userData);
    this.submitted = true;
    console.log(this.editParticipants.form.status);

    if(this.editParticipants.form.status == "INVALID") {
      this.presentToast('warning', 'Form not valid.');
      return;
    }
    this.spinner.show();


    const participantData = {
      'pId': this.addAction ? 0 : this.getDataForAddEdit[1].id,
      'empId': this.selectedCreator[0].empId,
      'nomDate': this.formatDate(userData.date),
      'preAssess': userData.preAssessRef,
      'partId': userData.partner,
      'tId': this.tenantId,
      'userId': this.userId,
    };

    console.log('participantData', participantData);

    const fd = new FormData();
    fd.append('content', JSON.stringify(participantData));
    fd.append('file', this.assessmentFileData);
    console.log('File Data ', fd);

    console.log('Nominee Data Img', this.assessmentFileData);
    console.log('Nominee Data ', participantData);

    const fileUploadUrl = webApi.domain + webApi.url.fileUpload;

    if (this.assessmentFileData) {
      this.webApiService.getService(fileUploadUrl, fd)
        .then(rescompData => {
          const uploadRes: any = rescompData;
          console.log('file upload res:', rescompData);
          this.fileUploadRes = uploadRes;
          this.spinner.hide();
          if (uploadRes === 'err') {
            this.presentToast('error', '');
          } else if (uploadRes.type === false) {
            this.presentToast('error', '');
          } else {
            if (this.fileUploadRes.data != null || this.fileUploadRes.fileError != true) {
              participantData.preAssess = this.fileUploadRes.data.file_url;
              this.addEditParticipants(participantData);
            } else {
              this.presentToast('error', this.fileUploadRes.status);
            }
          }
          console.log('File Upload Result', this.fileUploadRes);
        },
          resUserError => {
            this.spinner.hide();
          });
    } else {
      this.spinner.hide();
      this.addEditParticipants(participantData);
    }
  }


  participantAddEditRes: any;
  addEditParticipants(data) {
    this.spinner.show();
    console.log('data', data);
    this.addparticipantservice.addEditNominatedEmployees(data)
      .then(rescompData => {
        this.spinner.hide();
        const result = rescompData;
        this.participantAddEditRes = result['data'];
        console.log('Participant Add Edit Result ', this.participantAddEditRes)
        if (this.getDataForAddEdit[0] === 'ADD') {
          if (result['type'] === true) {
            this.presentToast('success', 'Nomination added');
            this.gotoBack();

          } else {
            this.presentToast('error', '');
          }
        } else {
          console.log('Participant Edit Result ', this.participantAddEditRes);
          if (result['type'] === true) {
            this.presentToast('success', 'Nomination updated');
            this.gotoBack();
          } else {
            this.presentToast('error', '');
          }
        }
      },
        resUserError => {
          this.spinner.hide();
          this.presentToast('error', ' ');
        });
  }

  formatDate(date) {
    let d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }


  // Help Code Start Here //

  helpContent: any;
  getHelpContent() {
    return new Promise(resolve => {
      this.http1
        .get('../../../../../../assets/help-content/addEditCourseContent.json')
        .subscribe(
          data => {
            this.helpContent = data;
            console.log('Help Array', this.helpContent);
          },
          err => {
            resolve('err');
          }
        );
    });
    // return this.helpContent;
  }

  getContent() {
    return new Promise(resolve => {
      this.http1
        .get('../../../../../../assets/help-content/help_content.json')
        .subscribe(
          data => {
            this.helpContent = data;
            console.log('Help Array', this.helpContent);
          },
          err => {
            resolve('err');
          }
        );
    });
  }
  // Help Code Ends Here //

}
