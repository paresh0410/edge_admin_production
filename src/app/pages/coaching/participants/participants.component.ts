import { Component, OnInit, ChangeDetectorRef, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ParticipantsService } from './participants.service';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { NgxSpinnerService } from 'ngx-spinner';
import { CalendarService } from '../../coaching/calendar/calendar.service';
import { DatePipe } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { webApi } from '../../../service/webApi';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { SuubHeader } from '../../components/models/subheader.model';
import { noData } from '../../../models/no-data.model';

@Component({
  selector: 'ngx-participants',
  templateUrl: './participants.component.html',
  styleUrls: ['./participants.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ParticipantsComponent implements OnInit {
  @ViewChild('myTable') table: any;
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No nominations are available at this time.",
    desc:"Nominations will appear when a learner is nominated for a coaching assignment by the admin. Nominations will be able to participate in the coaching sessions scheduled by the coach",
    titleShow:true,
    btnShow:true,
    descShow:true,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/cc-nomination',
  }
  rows = [];
  selected = [];
  searchParticipant: any;
  searchtext:string;
  //kv start

  column: any = [];
  isDesc: any = [];
  nominees = [];
  search = {
    name: '',
    business: '',
    department: '',
    manager: '',

  };
  header: SuubHeader  = {
    title:'Nominations',
    btnsSearch: true,
    searchBar: true,
    dropdownlabel: ' ',
    placeHolder:'Search by nominee fields',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: 'Bulk Upload',
    btnName3: '',
    btnAdd: 'Add Nomination',
    btnName1show: false,
    btnName2show: true,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: true,
    filter: false,
    showBreadcrumb: true,
    breadCrumbList:[
      {
        'name': 'Coaching',
        'navigationPath': '/pages/coaching',
      },]
  };
  //kv end

  labels: any = [
		{ labelname: 'ECN NO.', bindingProperty: 'code', componentType: 'text' },
    { labelname: 'FULL NAME', bindingProperty: 'name', componentType: 'text' },
    { labelname: 'BUSINESS', bindingProperty: 'business', componentType: 'text' },
		{ labelname: 'DEPARTMENT', bindingProperty: 'department', componentType: 'text' },
		{ labelname: 'MANAGER', bindingProperty: 'manager', componentType: 'text' },
    { labelname: 'EDIT', bindingProperty: 'tenantId', componentType: 'icon' },
  ]




  tenantId: any;
  arrow: boolean;
  nomineelist: any = [];
  constructor(private router: Router, private routes: ActivatedRoute, private participantsservice: ParticipantsService,
    private datePipe: DatePipe, private spinner: NgxSpinnerService, 
    // private toasterService: ToasterService,
    private cdf: ChangeDetectorRef, private calendarService: CalendarService, private toastr: ToastrService,private commonFunctionService: CommonFunctionsService,) {
    // this.rows=this.data;
    this.searchParticipant = {}
  }

  ngOnInit() {
    this.tenantId = this.participantsservice.tenantId;
    // this.getAllEmployeesCC('testuser4');
    this.getAllNominatedEmployee();
    //this.allNominatedEmployees = this.data;
    this.spinner.show();
    setTimeout(() => {
      this.spinner.hide()
    }, 2000);
    //kv
    //this.course = this.participantsservice.get_data();
  }

  //kv start
  clearBtn() {
    this.search.name = '';
    this.search.business = '';
    this.search.department = '';
    this.search.manager = '';
  }


  clear() {
    // this.searchParticipant = '';
    this.noNominatedEmployees=false
    this.header.searchtext = '';
    this.searchtext = this.header.searchtext;
    this.getAllNominatedEmployee();
  }

  sort(property) {
    this.isDesc = !this.isDesc; // change the direction
    this.column = property;
    const direction = this.isDesc ? 1 : -1;


    this.nominees.sort(function (a, b) {
      if (a[property] < b[property]) {
        return -1 * direction;
      } else if (a[property] > b[property]) {
        return 1 * direction;
      } else {
        return 0;
      }
    });
  }

  invert() {
    this.arrow = !this.arrow;
  }

  onEdit() {
    this.router.navigate(['']);
  }

  //kv end
  addeditParticipantAdd(data) {
  
      const action = 'ADD';
      this.participantsservice.addEditParticipants[0] = action;
      this.participantsservice.addEditParticipants[1] = data;
      this.router.navigate(['add-edit-participants'], { relativeTo: this.routes });
  }

  addeditParticipant(data, value) {
    if (value === 0) {
      const action = 'ADD';
      this.participantsservice.addEditParticipants[0] = action;
      this.participantsservice.addEditParticipants[1] = null;
      this.router.navigate(['add-edit-participants'], { relativeTo: this.routes });
    } else {
      const action = 'EDIT';
      this.participantsservice.addEditParticipants[0] = action;
      this.participantsservice.addEditParticipants[1] = data;
      this.router.navigate(['add-edit-participants'], { relativeTo: this.routes });
    }
  }

  onSelect({ selected }) {
    console.log('Select Event', selected, this.selected);

    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }

  gotouserprofile(item) {
    console.log('RowData:', item);
    this.participantsservice.getAllCalls = false;
    this.calendarService.empId = item.empId;
    this.calendarService.participantData = item;
    this.router.navigate(['calendar'], { relativeTo: this.routes });
  }

  gotoCalendar() {
    this.router.navigate(['calendar'], { relativeTo: this.routes });
    this.participantsservice.getAllCalls = true;
    this.calendarService.participantData = {};
  }
  // this is for redirect to bulk upload
  gotoBulkUpload() {
    this.router.navigate(['bulk-upload-coaching'], { relativeTo: this.routes });
  }

  goback() {
    this.router.navigate(['../../participants'], { relativeTo: this.routes });
  }

  gotoBack() {
    this.router.navigate(['../../coaching'], { relativeTo: this.routes });
  }

  noEmployees: boolean = false;
  allEmployees: any = [];
  // getAllEmployeesCC(evt) {
  //   this.spinner.show();
  //     const param = {
  //         'srcStr' : str,
  //         'tenantId': this.tenantId,
  //     };
  //     this.participantsservice.getAllEmployees(param)
  //     .then(rescompData => {
  //       this.spinner.hide();
  //       var result = rescompData;
  //       console.log('getAllEmployessCC:', rescompData);
  //       if (result['type'] === true) {
  //         if (result['data'][0].length === 0) {
  //           this.noEmployees = true;
  //         }else {
  //           this.allEmployees = result['data'][0];
  //           console.log('this.allEmployees',this.allEmployees);
  //           this.participantsservice.allEmployess = this.allEmployees;
  //           console.log('this.participantsservice.getAllEmployees',this.participantsservice.getAllEmployees);
  //         }
  //       }else {
  //         this.presentToast('error', '', 'Something went wrong.please try again later.');
  //       }
  //     }, error => {
  //       this.spinner.hide();
  //       this.presentToast('error', '', 'Something went wrong.please try again later.');
  //     });
  // }

  noNominatedEmployees: boolean = false;
  allNominatedEmployees: any = [];
  nominatedEmpRows: any = [];
  getAllNominatedEmployee() {
    this.nominatedEmpRows = [];
    this.allNominatedEmployees = [];
    const param = {
      'tenantId': this.tenantId,
    };
    const _urlFetchAllNominatedEmployee: string = webApi.domain + webApi.url.getallnominatedemployees;
    this.commonFunctionService.httpPostRequest(_urlFetchAllNominatedEmployee,param)
    // this.participantsservice.getAllNominatedEmployees(param)
      .then(rescompData => {
        this.spinner.hide();
        const result = rescompData;
        console.log('getAllNominatedEmployessCC:', rescompData);
        if (result['type'] === true) {
          if (result['data'][0].length === 0) {
            this.noNominatedEmployees = true;
          } else {
            this.noNominatedEmployees = false;
            this.nomineelist = result['data'][0];
            this.nominees = this.nomineelist;
            console.log('this.nominees', this.nominees);
          }
        } else {
          this.presentToast('error', '');
        }
      }, error => {
        this.spinner.hide();
        this.presentToast('error', '');
      });
  }


  disableParticipant(item, value) {
    this.spinner.show();
    console.log('item', item);

    const param = {
      'tenantId': this.tenantId,
      'nomId': item.nomId,
      'pvisible': value == 0 ? 0 : 1,
    }
    console.log(param);
    const _urlFetchAllParticipants: string = webApi.domain + webApi.url.getallemployeecc;
    this.commonFunctionService.httpPostRequest(_urlFetchAllParticipants,param)
    // this.participantsservice.changeParticipantStatus(param)
      .then(rescompData => {
        this.spinner.hide();
        const result = rescompData;
        console.log('getAllNominatedEmployessCC:', rescompData);
        if (result['type'] === true) {
          this.presentToast('success', 'Nomination updated');
          this.getAllNominatedEmployee();
        } else {
          this.presentToast('error', '');
        }
      }, error => {
        this.spinner.hide();
        this.presentToast('error', '');
      });
  }

  formatDate(date) {
    const nDate = new Date(date);
    const formatted = this.datePipe.transform(nDate, 'dd-MMM-yy');
    return formatted;
  }
  detailsTab: false;
  selectedTab(event) {
    console.log("Data =====>", event);
  }

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }
  SarchFilter(event, searchtext) {
    console.log(searchtext);
    searchtext=event.target.value
    if(searchtext.length>=3||searchtext.length==0){
      this.noNominatedEmployees=false
    const val = searchtext.toLowerCase();
    const tempcc = this.nomineelist.filter(function (d) {
      return String(d.code).toLowerCase().indexOf(val) !== -1 ||
        d.name.toLowerCase().indexOf(val) !== -1 ||
        d.business.toLowerCase().indexOf(val) !== -1 ||
        d.department.toLowerCase().indexOf(val) !== -1 ||
        d.manager.toLowerCase().indexOf(val) !== -1 ||
        !val
    })
    console.log(tempcc);
    this.nominees = tempcc;
    if(!tempcc.length){
      this.noNominatedEmployees=true
      this.noDataVal={
        margin:'mt-5',
        imageSrc: '../../../../../assets/images/no-data-bg.svg',
        title:"Sorry we couldn't find any matches please try again",
        desc:".",
        titleShow:true,
        btnShow:false,
        descShow:false,
        btnText:'Learn More',
        btnLink:''
      }
    }
  }
  else{
    searchtext={}

  }
  }
}
