import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { SortablejsOptions } from 'angular-sortablejs';
import { assessmenttemplateservice } from './assessmnet-template.service';
// import { ToasterService, Toast } from 'angular2-toaster';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { webApi } from '../../../service/webApi';
import { SuubHeader } from '../../components/models/subheader.model';
import { noData } from '../../../models/no-data.model';

@Component({
  selector: 'ngx-assessment-template',
  templateUrl: './assessment-template.component.html',
  styleUrls: ['./assessment-template.component.scss']
})
export class AssessmentTemplateComponent implements OnInit {
  userDetails: any = [];
  nobusiness: boolean = false;
  spectrList: any = [];
  competencyList: any = [];
  areaList: any = [];
  settingsAreaDrop: any = [];
  settingsCompenDrop: any = [];
  settingsSpectrDrop: any = [];
  selectedCompetency: any = [];
  selectedSpectr: any = [];
  selectedArea: any = [];
  businessId: any = [];
  spectrId: any = [];
  spectrName: any = [];
  compName: any = [];
  compId: any = [];
  areaId: any = [];
  areaName: any = [];
  assessNames: any = [];
  noAssessment: boolean = false;
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No assessment templates are available at this time.",
    desc:"Assessment templates will appear after they are added by the admin . The template will provide a blueprint to the coach to conduct assessment of the coachee",
    titleShow:true,
    btnShow:true,
    descShow:true,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/cc-call-add-assessment',
  }
  header: SuubHeader  = {
    title:'Assessment',
    btnsSearch: true,
    searchBar: false,
    dropdownlabel: ' ',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: '',
    btnName3: '',
    btnAdd: 'Add Assessment',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: true,
    filter: false,
    showBreadcrumb: true,
    breadCrumbList:[
      {
        'name': 'Coaching',
        'navigationPath': '/pages/coaching',
      },]
  };
btnName: string = 'Submit';
title:string='Add Assessment Template';
  labels: any = [
    { labelname: 'Spectr', bindingProperty: 'spectrNames', componentType: 'text' },
    { labelname: 'Competency', bindingProperty: 'compNames', componentType: 'text' },
    { labelname: 'Evaluation Area', bindingProperty: 'evalAreaName', componentType: 'text' },
    { labelname: 'Assess Name', bindingProperty: 'assessNames', componentType: 'text' },
    { labelname: 'Action', bindingProperty: 'btntext', componentType: 'button' },
    { labelname: 'Delete', bindingProperty: 'btndelete', componentType: 'delete' }
  ]
  enableDisableFeedModal: boolean=false;
  delData: any;
  enableDeleteModal: boolean;
  pressedOnce: boolean=true;
  constructor(private router: Router, public assessservice: assessmenttemplateservice,
    // public toasterService: ToasterService, 
    private spinner: NgxSpinnerService, private toastr: ToastrService, private http1: HttpClient,
    private commonFunctionService: CommonFunctionsService) {
    this.getHelpContent();
    this.userDetails = JSON.parse(localStorage.getItem('LoginResData'));

    this.settingsAreaDrop = {
      text: 'Select Evaluation Area',
      // singleSelection: true,
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      classes: 'common-multi',
      primaryKey: 'areaId',
      labelKey: 'areaName',
      noDataLabel: 'Search Evaluation Area...',
      enableSearchFilter: true,
      searchBy: ['areaId', 'areaName'],
      maxHeight:250,
      lazyLoading: true,
    };

    this.settingsCompenDrop = {
      text: 'Select Competency',
      // singleSelection: true,
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      classes: 'common-multi',
      primaryKey: 'compId',
      labelKey: 'compName',
      noDataLabel: 'Search Competency...',
      enableSearchFilter: true,
      searchBy: ['compId', 'compName']
    };


    this.settingsSpectrDrop = {
      text: 'Select Spectr',
      // singleSelection: true,
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      classes: 'common-multi',
      primaryKey: 'spectrId',
      labelKey: 'spectrName',
      noDataLabel: 'Search Spectr...',
      enableSearchFilter: true,
      searchBy: ['spectrId', 'spectrName'],
      maxHeight:250,
      lazyLoading: true,
    };

  }

  busininesTemp: boolean = false;
  questions = [];

  showTemp: boolean = false;

  businessData: any = [];

  business: any = [];
  @ViewChild('subsectionForm') SecForm: NgForm;

  ngOnInit() {
    this.getBusiness();
    this.getdropdownlist();
  }

  // Help Code Start Here //

  helpContent: any;
  getHelpContent() {
    return new Promise(resolve => {
      this.http1
        .get('../../../../../../assets/help-content/addEditCourseContent.json')
        .subscribe(
          data => {
            this.helpContent = data;
            console.log('Help Array', this.helpContent);
          },
          err => {
            resolve('err');
          }
        );
    });
    // return this.helpContent;
  }

  // Help Code Ends Here //

  onSpectrSearch(eve: any) {
    console.log(eve.target.value);
    let val = eve.target.value;
    if (val) {
      // this.usersList = [];
      const fSpectr = this.spectrList.filter(function (d) {
        return String(d.spectrId).toLowerCase().indexOf(val) !== -1 ||
          d.spectrName.toLowerCase().indexOf(val) !== -1 || !val;
      });

      // update the rows
      this.spectrList = fSpectr;
    } else {
      this.spectrList = this.spectrListF;
    }
  }
  onCompenSearch(eve: any) {
    console.log(eve.target.value);
    let val = eve.target.value;
    if (val) {
      // this.usersList = [];
      const fComp = this.competencyList.filter(function (d) {
        return String(d.compId).toLowerCase().indexOf(val) !== -1 ||
          d.compName.toLowerCase().indexOf(val) !== -1 || !val;
      });

      // update the rows
      this.competencyList = fComp;
    } else {
      this.competencyList = this.competencyListF;
    }
  }
  onAreaSearch(eve: any) {
    console.log(eve.target.value);
    let val = eve.target.value;
    if (val) {
      // this.usersList = [];
      const fArea = this.areaList.filter(function (d) {
        return String(d.areaId).toLowerCase().indexOf(val) !== -1 ||
          d.areaName.toLowerCase().indexOf(val) !== -1 || !val;
      });

      // update the rows
      this.areaList = fArea;
    } else {
      this.areaList = this.areaListF;
    }
  }
  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }

  getBusiness() {
    this.spinner.show();
    const param = {
      tId: this.userDetails.data.data.tenantId,
    };
    const business_dropdown: string = webApi.domain + webApi.url.getdropdownbusiness;
    this.commonFunctionService.httpPostRequest(business_dropdown,param)
    // this.assessservice.getbusinessdropdown(param)
    .then(res => {
      console.log(res);
      this.spinner.hide();
      if (res['type'] == true) {
        try {
          if (res['data'].length > 0) {
            this.business = res['data'];
            this.businessId = this.business[0].businessId;
            this.getaassessmenttemplate(this.businessId);
            this.noAssessment = false;
          } else if (res['data'].length === 0) {
            this.noAssessment = true;
          }
        } catch{ }
      }
    }, err => {
      this.nobusiness = true;
      console.log(err);
    });
  }
  spectrListF: any = [];
  competencyListF: any = [];
  areaListF: any = [];
  getdropdownlist() {
    this.spinner.show();
    const param = {
      tId: this.userDetails.data.data.tenantId,
    };
    const getdropddown: string = webApi.domain + webApi.url.getDropdownObservation;
    this.commonFunctionService.httpPostRequest(getdropddown,param)
    // this.assessservice.getdropdown(param)
    .then(res => {
      console.log(res);
      this.spinner.hide();
      if (res['type'] === true) {
        try {
          this.spectrList = res['data'][0];
          this.competencyList = res['data'][1];
          this.areaList = res['data'][2];

          this.spectrListF = res['data'][0];
          this.competencyListF = res['data'][1];
          this.areaListF = res['data'][2];
          console.log('this.spectrList', this.spectrList);
          console.log('this.competencyList', this.competencyList);
          console.log('this.areaList', this.areaList);
        } catch (e) {
          console.log(e);
        }
      }
    }, err => {
      console.log(err);
    });
  }
  searchTemp(id: number) {
    console.log(id);
    this.businessId = id;
    this.getaassessmenttemplate(id);
  }

  getaassessmenttemplate(id) {
    this.businessData = [];
    this.showTemp = true;
    const param = {
      tId: this.userDetails.data.data.tenantId,
      busiId: id,
    };
    const asseessment_template: string = webApi.domain + webApi.url.getasseessmenttemplate;
    this.commonFunctionService.httpPostRequest(asseessment_template,param)
    // this.assessservice.getassessment(param)
    .then(res => {
      console.log(res);
      
   
      if (res['type'] === true) {
        try {
          if (res['data'].length > 0) {
            this.businessData = res['data'];
            for (let i = 0; i < this.businessData.length; i++) {
              if(this.businessData[i].visible == 1 ) {
                this.businessData[i].btntext = 'fa fa-eye';
              } else {
                this.businessData[i].btntext = 'fa fa-eye-slash';
              }
              if(this.businessData[i].isDelete ==0){
                this.businessData[i].btndelete = '';
              }
              else{
                this.businessData[i].btndelete = 'fa fa-trash';
              }
            }
            console.log("businessssssssssssdata",this.businessData)
            this.noAssessment = false;
          } else if (res['data'].length === 0) {
            this.noAssessment = true;
          }
        } catch{ }
      }
    }, err => {
      this.nobusiness = true;
      console.log(err);
    })
  }
  addBusinessTemp() {
    this.selectedArea = [];
    this.selectedCompetency = [];
    this.selectedSpectr = [];
    this.assessNames=[];
    this.areaId=''
    this.areaName=''
    this.spectrId=''
    this.spectrName=''
    this.compId=''
    this.compName=''
    this.busininesTemp = true;
  }



  closeBusinessTemp() {
    this.busininesTemp = false;
    this.selectedArea = [];
    this.selectedCompetency = [];
    this.selectedSpectr = [];
  }
  back() {
    this.router.navigate(['/pages/coaching']);
  }
  //////spectr//////////
  onSpectrSelect(item: any) {
    console.log(item);
    console.log(this.selectedSpectr);
  }
  OnSpectrDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedSpectr);
  }
  onSelectAllspestr(items: any) {
    console.log(items);
  }
  onDeSelectAllspestr(items: any) {
    console.log(items);
  }

  //////Competency///////////

  onCompenSelect(item: any) {
    console.log(item);
    // console.log(this.selectedItems);
  }
  OnCompenDeSelect(item: any) {
    console.log(item);
    // console.log(this.selectedItems);
  }
  onSelectAllCompen(items: any) {
    console.log(items);
  }
  onDeSelectAllCompen(items: any) {
    console.log(items);
  }

  /////Area/////
  onAreaSelect(item: any) {
    console.log(item);
    // console.log(this.selectedItems);
  }
  OnAreaDeSelect(item: any) {
    console.log(item);
    // console.log(this.selectedItems);
  }
  onSelectAllArea(items: any) {
    console.log(items);
  }
  onDeSelectAllArea(items: any) {
    console.log(items);
  }

  saveSection(form) {
    console.log("form value",form)
    if(form.valid &&this.pressedOnce){
      this.pressedOnce=false;
    // if (this.selectedSpectr.length > 0 && this.selectedCompetency.length
    //   && this.selectedArea.length > 0) {
    for (let i = 0; i < this.selectedSpectr.length; i++) {
      if (this.spectrId != '') {
        this.spectrId += ','
      }
      if (String(this.selectedSpectr[i].spectrId) != '' && String(this.selectedSpectr[i].spectrId) != 'null'
        && String(this.selectedSpectr[i].spectrId) != undefined) {
        this.spectrId += this.selectedSpectr[i].spectrId;
      }
      if (this.spectrName != '') {
        this.spectrName += ','
      }
      if (String(this.selectedSpectr[i].spectrName) != '' && String(this.selectedSpectr[i].spectrName) != 'null'
        && String(this.selectedSpectr[i].spectrName) != undefined) {
        this.spectrName += this.selectedSpectr[i].spectrName;
      }
    }
    for (let i = 0; i < this.selectedCompetency.length; i++) {
      if (this.compId != '') {
        this.compId += ','
      }
      if (String(this.selectedCompetency[i].compId) != '' && String(this.selectedCompetency[i].compId) != 'null') {
        this.compId += this.selectedCompetency[i].compId;
      }
      if (this.compName != '') {
        this.compName += ','
      }
      if (String(this.selectedCompetency[i].compName) != '' && String(this.selectedCompetency[i].compName) != 'null') {
        this.compName += this.selectedCompetency[i].compName;
      }
    }
    for (let i = 0; i < this.selectedArea.length; i++) {
      if (this.areaId != '') {
        this.areaId += ','
      }
      if (String(this.selectedArea[i].areaId) != '' && String(this.selectedArea[i].areaId) != 'null') {
        this.areaId += this.selectedArea[i].areaId;
      }
      if (this.areaName != '') {
        this.areaName += ',';
      }
      if (String(this.selectedArea[i].areaName) != '' && String(this.selectedArea[i].areaName) != 'null') {
        this.areaName += this.selectedArea[i].areaName;
      }
    }
    const param = {
      busiId: this.businessId,
      specId: this.spectrId,
      specNames: this.spectrName,
      comId: this.compId,
      comNames: this.compName,
      evArId: this.areaId,
      evArName: this.areaName,
      assName: this.assessNames,
      tId: this.userDetails.data.data.tenantId,
      userId: this.userDetails.data.data.id,
    };
    console.log(param);
    const insertassessment: string = webApi.domain + webApi.url.insertassessmentemplate;
    this.commonFunctionService.httpPostRequest(insertassessment,param)
    // this.assessservice.inserttemplate(param)
    .then(res => {
      console.log(res);
      if (res['type'] === true) {
        this.busininesTemp = false;
        // var courseUpdate: Toast = {
        //   type: 'success',
        //   title: 'Assessment',
        //   body: 'You have successfully added.',
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // // this.closeEnableDisableCourseModal();
        // this.toasterService.pop(courseUpdate);
        this.presentToast('success', 'Assessment added');
        this.pressedOnce=true
        this.getaassessmenttemplate(this.businessId);
      }
    }, err => {
      console.log(err);
    });
    // } else {
    //   var courseUpdate: Toast = {
    //     type: 'error',
    //     title: 'Assessment',
    //     body: 'Please select all the fields.',
    //     showCloseButton: true,
    //     timeout: 2000
    //   };
    //   // this.closeEnableDisableCourseModal();
    //   this.toasterService.pop(courseUpdate);
    // }
   }else {
      Object.keys(form.controls).forEach(key => {
        form.controls[key].markAsDirty();
      });
    }
  }
   
  visibilityTableRow(row) {
    let value;
    let status;

    if (row.visible == 1) {
      row.btntext = 'fa fa-eye-slash';
      value  = 'fa fa-eye-slash';
      row.visible = 0
      status = 0;
    } else {
      status = 1;
      value  = 'fa fa-eye';
      row.visible = 1;
      row.btntext = 'fa fa-eye';
    }

    for(let i =0; i < this.businessData.length; i++) {
      if(this.businessData[i].employeeId == row.employeeId) {
        this.businessData[i].btntext = row.btntext;
        this.businessData[i].visible = row.visible
      }
    }
    console.log(row, status);
    const param = {
      flag: 'assess',
      tmpId: row.atId,
      stat: status
    }
    this.spinner.show();
    const enableassessment: string = webApi.domain + webApi.url.enabledisble;
    this.commonFunctionService.httpPostRequest(enableassessment,param)
    // this.assessservice.enabledisableassessment(param)
    .then(res => {
      console.log(res);
      try {
        if (res['type'] == true) {
          this.presentToast('success',  res['data'][0][0]['msg']);
          this.getaassessmenttemplate(this.businessId);
        }
      } catch (e) {
        console.log(e);
      }
      this.spinner.hide();
    }, err => {
      console.log(err);
      this.spinner.hide();
      this.presentToast('error', 'Something went wrong, please try again');
    });

  }
  // Disableassessment(i, data, status) {
  //   console.log(i, data, status);
  //   const param = {
  //     flag: 'assess',
  //     tmpId: data.atId,
  //     stat: status
  //   }
  //   this.spinner.show();
  //   const enableassessment: string = webApi.domain + webApi.url.enabledisble;
  //   this.commonFunctionService.httpPostRequest(enableassessment,param)
  //   // this.assessservice.enabledisableassessment(param)
  //   .then(res => {
  //     console.log(res);
  //     try {
  //       if (res['type'] == true) {
  //         this.presentToast('success',  res['data'][0][0]['msg']);
  //         this.getaassessmenttemplate(this.businessId);
  //       }
  //     } catch (e) {
  //       console.log(e);
  //     }
  //     this.spinner.hide();
  //   }, err => {
  //     console.log(err);
  //     this.spinner.hide();
  //     this.presentToast('error', 'Something went wrong, please try again');
  //   });
  // }

  Removeque(data) {
    console.log(data);
    const param = {
      atId: data.atId,
      tId: this.userDetails.data.data.tenantId,
    };
    this.spinner.show();
    const deletassessment: string = webApi.domain + webApi.url.deletassessmenttemplate;
    this.commonFunctionService.httpPostRequest(deletassessment,param)
    // this.assessservice.deletassess(param)
    .then(res => {
      console.log(res);
      if (res['type'] === true) {
        // var courseUpdate: Toast = {
        //   type: 'success',
        //   title: 'Assessment',
        //   body: 'You have successfully deleted.',
        //   showCloseButton: true,
        //   timeout: 2000,
        // };
        // // this.closeEnableDisableCourseModal();
        // this.toasterService.pop(courseUpdate);
        this.presentToast('success', res['data'][0][0]['msg']);
      this.closeDeleteModal();
        // this.businessData.splice(i, 0);
        this.getaassessmenttemplate(this.businessId);
      }
      this.spinner.hide();
    }, err => {
      this.closeDeleteModal();
      this.spinner.hide();
      console.log(err);
    });
  }
  QuestionListOptions: SortablejsOptions = {
    group: {
      name: 'questions',
      // pull: 'clone',
      // put: false,
    },
    sort: false,
    // draggable: '.draggable',
    // onEnd: this.onListItemDragEnd.bind(this),
    // put: false,
    handle: '.drag-handle',
    // draggable: '.draggable'
  };
  QuestionListOptions1: SortablejsOptions = {
    group: 'questions1',
    sort: true,
    // draggable: '.draggable',
    // onEnd: this.onListItemDragEnd.bind(this),
    handle: '.drag-handle',
    // draggable: '.draggable'
  };
  item = false;

  closeDeleteModal() {
    this.enableDeleteModal = false;
  }
  enableDelaction(actionType) {
    console.log(this.delData);
    if (actionType == true) {  
      this.Removeque(this.delData)
    } else {
      this.closeDeleteModal();
    }
  }
  clickToDelete(delData) {
      this.delData = delData;
      this.enableDeleteModal = true;
  }
}
