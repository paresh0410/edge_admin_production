import { Injectable, Inject } from '@angular/core';
import { webApi } from '../../../service/webApi';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from "@angular/common/http";

@Injectable({
    providedIn: 'root'
})

export class assessmenttemplateservice {


    private business_dropdown: string = webApi.domain + webApi.url.getdropdownbusiness;
    private asseessment_template: string = webApi.domain + webApi.url.getasseessmenttemplate;
    private getdropddown: string = webApi.domain + webApi.url.getDropdownObservation;
    private insertassessment: string = webApi.domain + webApi.url.insertassessmentemplate;
    private deletassessment: string = webApi.domain + webApi.url.deletassessmenttemplate;
    private enableassessment: string = webApi.domain + webApi.url.enabledisble;

    constructor(private _httpClient: HttpClient) {
    }
    getbusinessdropdown(param) {
        return new Promise(resolve => {
            this._httpClient.post(this.business_dropdown, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    getassessment(param) {
        return new Promise(resolve => {
            this._httpClient.post(this.asseessment_template, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    getdropdown(param) {
        return new Promise(resolve => {
            this._httpClient.post(this.getdropddown, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    inserttemplate(param) {
        return new Promise(resolve => {
            this._httpClient.post(this.insertassessment, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    deletassess(param) {
        return new Promise(resolve => {
            this._httpClient.post(this.deletassessment, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    enabledisableassessment(param) {
        return new Promise(resolve => {
            this._httpClient.post(this.enableassessment, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }
}