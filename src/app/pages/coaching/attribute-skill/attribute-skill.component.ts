import { Component, OnInit , ViewEncapsulation} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'ngx-attribute-skill',
  templateUrl: './attribute-skill.component.html',
  styleUrls: ['./attribute-skill.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AttributeSkillComponent implements OnInit {
	rows:any=[];

	deptList:any=[
    {
		"business":"Collections",
		"dept":"Rural Collection-RTL",
		"subdept":"Rural Collection-RTL",
		"bands":"E02"
    },
    {
		"business":"Rural Lending",
		"dept":"Rural Consumer Durable",
		"subdept":"Sales",
		"bands":"E04A"
    },
    {
		"business":"Credit Operations",
		"dept":"Consumer Operations",
		"subdept":"Customer Support - Prime / Emerging",
		"bands":"E02"
    },
    {
		"business":"Rural Lending",
		"dept":"Rural Gold Loans",
		"subdept":"Collections",
		"bands":"E01"
    },
    {
		"business":"Capital Market & Corporate Finance",
		"dept":"COE",
		"subdept":"Product",
		"bands":"E03"
    },
    {
		"business":"PLCS - Growth",
		"dept":"PLCS - Growth",
		"subdept":"Sales",
		"bands":"E02"
    },
    {
		"business":"Collections",
		"dept":"Consumer - GCL",
		"subdept":"Consumer - GCL",
		"bands":"E01"
    },
    {
		"business":"Professional Loans",
		"dept":"Direct",
		"subdept":"Sales",
		"bands":"E02"
    },
    {
		"business":"Finance & Accounts",
		"dept":"Finance Control & PMO",
		"subdept":"Finance Control & PMO",
		"bands":"E03"
    },
    {
		"business":"NSB",
		"dept":"Future Group Co-Branded Card",
		"subdept":"Sales",
		"bands":"E02"
    }
  ];
  constructor(private router:Router,private routes:ActivatedRoute) {
    this.rows=this.deptList;
   }

  ngOnInit() {
	}

	gotoAddEdit(){
		this.router.navigate(['add-attribute-skill'],{relativeTo:this.routes});
	}
	
	gotoBack(){
    this.router.navigate(['../../coaching'],{relativeTo:this.routes});
	}

}
