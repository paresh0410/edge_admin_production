import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAttributeSkillComponent } from './add-attribute-skill.component';

describe('AddAttributeSkillComponent', () => {
  let component: AddAttributeSkillComponent;
  let fixture: ComponentFixture<AddAttributeSkillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAttributeSkillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAttributeSkillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
