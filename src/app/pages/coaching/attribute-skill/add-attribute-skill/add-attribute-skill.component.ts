import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'ngx-add-attribute-skill',
  templateUrl: './add-attribute-skill.component.html',
  styleUrls: ['./add-attribute-skill.component.scss']
})
export class AddAttributeSkillComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;
  countryForm:any;
  editField: string;
  DBM:any={};
  status:any=[
    {
      id:1,
      label:'Active',
    },
    {
      id:2,
      label:'Inactive',
    }
  ];
  addEditAttr:any=[
  [
    {
        attribute:'',
        skill:'',
        description:'',
        status:1,
        display:true,
      }
  ]
  ];
  addSkillVar:any={
    attribute:'',
    skill:'',
    description:'',
    status:1,
    display:false,
  };
  addAttrVar:any={
    attribute:'',
    skill:'',
    description:'',
    status:1,
    display:true,
  };

  constructor(private formBuilder: FormBuilder, private router:Router,private routes:ActivatedRoute) { }

  ngOnInit() {
      this.registerForm = this.formBuilder.group({
          business: ['', Validators.required],
          department: ['', Validators.required],
          subdepartment: ['', Validators.required],
          band: ['', Validators.required],
          attribute: ['', Validators.required],
          skill: ['', Validators.required],
          description: ['', Validators.required],
          status: ['', Validators.required],
      });
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  onSubmit() {
      this.submitted = true;

      // stop here if form is invalid
      if (this.registerForm.invalid) {
          return;
      }

      alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value))
  }

  goBack(){
    this.router.navigate(['../../attribute-skills'],{relativeTo:this.routes})
  }

  addSkill(index){
    this.addEditAttr[index].push(this.addSkillVar)
  }

  addAttr(){
    var array=[];
    array.push(this.addAttrVar);
    this.addEditAttr.push(array);
  }


}
