import { Component, OnInit, ViewEncapsulation, ViewChild, ChangeDetectorRef } from '@angular/core';
import { masterservice } from './master.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Route, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
// import { ToasterService } from 'angular2-toaster';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
import { webApi } from '../../../service/webApi';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { t } from '@angular/core/src/render3';
import { SuubHeader } from '../../components/models/subheader.model';
import { noData } from '../../../models/no-data.model';

@Component({
  selector: 'ngx-master',
  templateUrl: './master.component.html',
  styleUrls: ['./master.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MasterComponent implements OnInit {
  masterdata: any = [];
  mastertitle:any;
  userDetails: any = [];
  tenantId: any;
  param: any = [];
  addeditdata: any = [];
  masterlist: any = [];
  sectionshow: boolean = false;
  questions: boolean = true;
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../assets/images/no-data-bg.svg',
    title:"No evaluation line items are available at this time.",
    desc:"Evaluation line items will appear after they are added by the admin . The line items will create a guideline for the coach to create a template for assessing the coachee",
    titleShow:true,
    btnShow:true,
    descShow:true,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/cc-call-evaluationarea'
}
btnName: string = 'Submit';
  editData: any = [];
  fname: any;
  msg: any;
  areaWeight: any;
  header: SuubHeader  = {
    title:'Spectr',
    btnsSearch: true,
    searchBar: true,
    dropdownlabel: ' ',
    placeHolder:'Search by spectr name',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: '',
    btnName3: '',
    btnAdd: 'Add',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: true,
    filter: false,
    showBreadcrumb: true,
    breadCrumbList:[
      {
        'name': 'Coaching',
        'navigationPath': '/pages/coaching',
      },]
  };
  labels: any = [
		{ labelname: 'ID', bindingProperty: 'sr_no', componentType: 'text' },
		{ labelname: 'NAME', bindingProperty: 'Name', componentType: 'text' },
		{ labelname: 'ACTION', bindingProperty: 'btntext', componentType: 'button' },
    { labelname: 'EDIT', bindingProperty: 'tenantId', componentType: 'icon' },
  ]
  labelsEa: any = [
		{ labelname: 'ID', bindingProperty: 'sr_no', componentType: 'text' },
    { labelname: 'NAME', bindingProperty: 'Name', componentType: 'text' },
    { labelname: 'Weightage', bindingProperty: 'weightage', componentType: 'text' },
		{ labelname: 'ACTION', bindingProperty: 'btntext', componentType: 'button' },
    { labelname: 'EDIT', bindingProperty: 'tenantId', componentType: 'icon' },
  ]
  noMaster: boolean = false;
  masterduplicate: any;
  enable: boolean;
  title: string;
  rowData: any;
  enableDisableModal: boolean;
  constructor(public master: masterservice, private spinner: NgxSpinnerService, private router: Router,
    // private toasterService: ToasterService,
    private cdf: ChangeDetectorRef,
     private toastr: ToastrService, private http1: HttpClient,
     private commonFunctionService: CommonFunctionsService,
    ) {
      this.getHelpContent();
  }

  @ViewChild('subsectionForm') SecForm: NgForm;

  onSubmitSec() {
    console.log(this.SecForm);
  }

  ngOnInit() {
    this.userDetails = JSON.parse(localStorage.getItem('LoginResData'));
    this.param = {
      tId: this.userDetails.data.data.tenantId,
    };
    this.masterdata = this.master.data;
    console.log('IdPage', this.masterdata);
    this.list();
  }

  back() {
    this.router.navigate(['../pages/coaching']);
  }
  list() {
    this.masterlist = [];
    if (this.masterdata === 'SPECTR') {
      this.mastertitle = 'Add Spectr';
      this.header.title='Spectr'
      this.header.placeHolder='Search by spectr name',
      this.spectrlist();
    } else if (this.masterdata === 'COMP') {
      this.header.title='Competencies'
      this.header.placeHolder='Search by Competencies name'
      this.mastertitle = 'Add Competencies';
      this.completencieslist();
    } else {
      this.header.title='Evalution Area'
      this.header.placeHolder='Search by Evalution name'
      this.mastertitle = 'Add Evalution Area';
      this.evalarea();
    }
  }
  spectrlist() {
    this.spinner.show();
    this.cdf.detectChanges();
    console.log(this.param);
    const spectrlisturl: string = webApi.domain + webApi.url.getspectr;
    this.commonFunctionService.httpPostRequest(spectrlisturl,this.param)
    // this.master.getAllspectr(this.param)
    .then(res => {
      var result = res;
      console.log('spctr list', result);
      if (result['type'] === true)  {
        if (result['data'].length == 0) {
          this.noMaster = true;
          this.noDataVal={
            margin:'mt-3',
            imageSrc: '../../../../assets/images/no-data-bg.svg',
            title:"No assessment line items are available at this time.",
            desc:"Assessment line items will appear after they are added by the admin . The line items will create a guideline for the coach to create a template for assessing the coachee",
            titleShow:true,
            btnShow:true,
            descShow:true,
            btnText:'Learn More',
            btnLink:'https://faq.edgelearning.co.in/kb/cc-call-spectr',
          }
        }else {
          this.noMaster = false;
          this.binddata(res['data']);
        }
        this.spinner.hide();
      }
    }, err => {
      this.noMaster = true;
      this.spinner.hide();
      console.log(err);
    });
  }

  completencieslist() {
    this.spinner.show();
    this.cdf.detectChanges();
    const competencieslisturl: string = webApi.domain + webApi.url.getcompetency;
    this.commonFunctionService.httpPostRequest(competencieslisturl,this.param)
    // this.master.getAllcompetencies(this.param)
    .then(res => {
      var result = res;
      console.log('competency list', result);
      if (result['type'] === true)  {
        if (result['data'].length == 0) {
          this.noMaster = true;
          this.noDataVal={
            margin:'mt-3',
            imageSrc: '../../../../assets/images/no-data-bg.svg',
            title:"No competency line items are available at this time.",
            desc:"Competency line items will appear after they are added by the admin . The line items will create a guideline for the coach to create a template for assessing the coachee",
            titleShow:true,
            btnShow:true,
            descShow:true,
            btnText:'Learn More',
            btnLink:'https://faq.edgelearning.co.in/kb/cc-call-competency',
          }
        }else {
          this.noMaster = false;
          this.binddata(res['data']);
        }
        this.spinner.hide();
      }
    }, err => {
      this.spinner.hide();
      this.noMaster = true;
      console.log(err);
    });
  }
  evalarea() {
    this.spinner.show();
    this.cdf.detectChanges();
    const getevalutionareaurl: string = webApi.domain + webApi.url.getevalutionarea;
    this.commonFunctionService.httpPostRequest(getevalutionareaurl,this.param)
    // this.master.getAllevalarea(this.param)
    .then(res => {
      var result = res;
      console.log('evaluation list', result);
      if (result['type'] === true)  {
        if (result['data'].length == 0) {
          this.noMaster = true;
          this.noDataVal={
            margin:'mt-3',
            imageSrc: '../../../../assets/images/no-data-bg.svg',
            title:"No evaluation line items are available at this time.",
            desc:"Evaluation line items will appear after they are added by the admin . The line items will create a guideline for the coach to create a template for assessing the coachee",
            titleShow:true,
            btnShow:true,
            descShow:true,
            btnText:'Learn More',
            btnLink:'https://faq.edgelearning.co.in/kb/cc-call-evaluationarea',
          }
        }else {
          this.noMaster = false;
          this.binddata(res['data']);
        }
        this.spinner.hide();
      }
    }, err => {
      this.noMaster = true;
      this.spinner.hide();
      console.log(err);
    });
  }

  binddata(data) {
    try {
      this.masterlist = data;
      this.masterduplicate = this.masterlist
      for (let i = 0; i < this.masterlist.length; i++) {
        this.masterlist[i].sr_no = i + 1;
        if(this.masterlist[i].visible == 1) {
          this.masterlist[i].btntext = 'fa fa-eye';
          } else {
          this.masterlist[i].btntext = 'fa fa-eye-slash';
          }
      }
    } catch{ }
  }
  visibilityTableRow(row) {
    let value;
    let status;

    if (row.visible == 1) {
      row.btntext = 'fa fa-eye-slash';
      value  = 'fa fa-eye-slash';
      row.visible = 0
      status = 0;
    } else {
      status = 1;
      value  = 'fa fa-eye';
      row.visible = 1;
      row.btntext = 'fa fa-eye';
    }
    this.addeditdata = {
      visible: status,
      tId: this.userDetails.data.data.tenantId,
      userId: this.userDetails.data.data.id,
      Id: row.id,
      Name: row.Name,
      weightage: row.weightage,
    };
    console.log('param',this.addeditdata);
    if (status === 1) {
      this.msg = 'Enabled' + ' ' + this.mastertitle;
    } else {
      this.msg = 'Disabled' + ' ' + this.mastertitle;
    }
    this.result();
    console.log('row', row);
  }
  // disableVisibility(index, data, status) {
  //   this.addeditdata = {
  //     visible: status,
  //     tId: this.userDetails.data.data.tenantId,
  //     userId: this.userDetails.data.data.id,
  //     Id: data.id,
  //     Name: data.Name,
  //     weightage: data.weightage,
  //   };
  //   console.log('param',this.addeditdata);
  //   if (status === 1) {
  //     this.msg = 'Enabled' + ' ' + this.mastertitle;
  //   } else {
  //     this.msg = 'Disabled' + ' ' + this.mastertitle;
  //   }
  //   this.result();
  // }

  result() {
    this.spinner.show();
    this.cdf.detectChanges();
    this.updatedata(this.addeditdata, response => {
      console.log('dataMaster',response);
      if (response.type === true) {
        this.list();
        this.sectionshow = false;
        this.presentToast('success', this.msg);
      } else {
        this.spinner.hide();
        this.presentToast('error', '');
      }
    });
  }

  updatedata(addeditdata, cb) {
    if (this.masterdata === 'SPECTR') {
      const  addeditspectraurl: string = webApi.domain + webApi.url.addeditspectr;
      this.commonFunctionService.httpPostRequest(addeditspectraurl,addeditdata)
      // this.master.editSectr(addeditdata)
      .then(res => {
        cb(res);
      }, err => {
        this.presentToast('error', '');
        console.log(err);
      });
    } else if (this.masterdata === 'COMP') {
      const addeditcompetenciesurl: string = webApi.domain + webApi.url.addeditcompetencies;
      this.commonFunctionService.httpPostRequest(addeditcompetenciesurl,addeditdata)
      // this.master.editCompetency(addeditdata)
      .then(res => {
        cb(res);
      }, err => {
        this.presentToast('error', '');
        console.log(err);
      });
    } else {
      const addeditevalareaurl: string = webApi.domain + webApi.url.addeditevalutionarea;
      this.commonFunctionService.httpPostRequest(addeditevalareaurl,addeditdata)
      // this.master.editAreaEva(addeditdata)
      .then(res => {
        cb(res);
      }, err => {
        this.presentToast('error', '');
        console.log(err);
      });
    }
  }

  showTemplate(data) {
    console.log(data,"data")
    this.editData.Name = ''
    // this.editData = data ? data : [];
    // if (value === 0) {
      // this.editData.id = 0;
      // this.editData.visible = 1;
    // }
    if(data != null){
      console.log("if")
      this.editData.id = data.id;
      this.editData.visible = 1;
      this.editData.Name = data.Name
    }else {
      console.log("else")
      this.editData.id = 0;
      this.editData.visible = 1;
    }
    this.sectionshow = true;
    console.log('editdata', this.editData);
  }
  closesectionModel() {
    this.sectionshow = false;
  }
  presentToast(type, body) {
    if(type === 'success'){
      this.toastr.success(body, 'Success', {
        closeButton: false
       });
    } else if(type === 'error'){
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else{
      this.toastr.warning(body, 'Warning', {
        closeButton: false
        })
    }
  }
  saveTemp(form) {
    if(form.valid){
    this.addeditdata = {
      Id: this.editData.id,
      visible: this.editData.visible,
      Name: this.editData.Name,
      tId: this.userDetails.data.data.tenantId,
      userId: this.userDetails.data.data.id,
      weightage: this.editData.weightage,
    };
    if (this.addeditdata.Id === 0) {
      this.msg = 'You have successfully added' + ' ' + this.mastertitle;
    } else {
      this.msg = 'You have successfully updated' + ' ' + this.mastertitle;
    }
    this.result();
  }else{
    Object.keys(form.controls).forEach(key => {
      form.controls[key].markAsDirty();
    });
  }
  }


  SarchFilter(event, searchtext) {

    console.log(searchtext);
    searchtext=event.target.value
    if(searchtext.length>=3||searchtext.length==0){
      this.noMaster=false
    const val = searchtext.toLowerCase();
    const tempcc = this.masterduplicate.filter(function (d) {
      return  d.Name.toLowerCase().indexOf(val) !== -1 ||
        !val
    })
    console.log(tempcc);
    if(!tempcc.length){
      this.noMaster=true
      this.noDataVal={
        margin:'mt-5',
        imageSrc: '../../../../../assets/images/no-data-bg.svg',
        title:"Sorry we couldn't find any matches please try again",
        desc:".",
        titleShow:true,
        btnShow:false,
        descShow:false,
        btnText:'Learn More',
        btnLink:''
      }
    }
    this.masterlist = tempcc;
  }else{
    searchtext={}
  }
  }

  clear() {
    this.noMaster=false
    // this.searchParticipant = '';
    this.header.searchtext = '';
    this.masterlist = this.masterduplicate
    // this.searchtext = this.header.searchtext;
    // this.getAllNominatedEmployee();
  }



   // Help Code Start Here //

   helpContent: any;
   getHelpContent() {
     return new Promise(resolve => {
       this.http1
         .get('../../../../../../assets/help-content/addEditCourseContent.json')
         .subscribe(
           data => {
             this.helpContent = data;
             console.log('Help Array', this.helpContent);
           },
           err => {
             resolve('err');
           }
         );
     });
     // return this.helpContent;
   }

   // Help Code Ends Here //

}
