import { Injectable, Inject } from '@angular/core';
import { webApi } from '../../../service/webApi';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from "@angular/common/http";

@Injectable({
    providedIn: 'root'
})

export class masterservice {
    data: any = [];
    private spectrlisturl: string = webApi.domain + webApi.url.getspectr;
    private competencieslisturl: string = webApi.domain + webApi.url.getcompetency;
    private getevalutionareaurl: string = webApi.domain + webApi.url.getevalutionarea;
    private addeditspectraurl: string = webApi.domain + webApi.url.addeditspectr;
    private addeditcompetenciesurl: string = webApi.domain + webApi.url.addeditcompetencies;
    private addeditevalareaurl: string = webApi.domain + webApi.url.addeditevalutionarea;
    constructor(private _httpClient: HttpClient) { }

    getAllspectr(param) {
        return new Promise(resolve => {
            this._httpClient.post(this.spectrlisturl, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    getAllcompetencies(param) {
        return new Promise(resolve => {
            this._httpClient.post(this.competencieslisturl, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    getAllevalarea(param) {
        return new Promise(resolve => {
            this._httpClient.post(this.getevalutionareaurl, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    editSectr(param) {
        return new Promise(resolve => {
            this._httpClient.post(this.addeditspectraurl, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }
     editCompetency(param) {
        return new Promise(resolve => {
            this._httpClient.post(this.addeditcompetenciesurl, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
     }

     editAreaEva(param) {   
        return new Promise(resolve => {
            this._httpClient.post(this.addeditevalareaurl, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });

     }
}