import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CallMappingComponent } from './call-mapping.component';

describe('CallMappingComponent', () => {
  let component: CallMappingComponent;
  let fixture: ComponentFixture<CallMappingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CallMappingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CallMappingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
