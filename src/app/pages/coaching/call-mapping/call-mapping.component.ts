import { Component, OnInit, ViewChild,ViewEncapsulation } from '@angular/core';
import { Router, NavigationStart, Routes, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'ngx-call-mapping',
  templateUrl: './call-mapping.component.html',
  styleUrls: ['./call-mapping.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CallMappingComponent implements OnInit {

  @ViewChild('myTable') table: any;

  expanded: any = {};
  timeout: any;
  rows = [];
  selected = [];
  state:any = false;
  coach:any;

  coaches = [
    {
      id:1,
      name:'Coach1'
    },{
      id:2,
      name:'Coach2'
    },{
      id:3,
      name:'Coach3'
    },{
      id:4,
      name:'Coach4'
    },{
      id:5,
      name:'Coach5'
    },
  ]

  coachingcalls = [
  {
    "id":1,
  	"participantName":'John Cena',
  	"coach":'Unassigned',
  	"business":'Business1',
  	"department":'department4',
  	"manager":'Steve',
  	"zone":"South",
  	"location":"Tamilnadu",
  },
  {
    "id":2,
  	"participantName":'Ramsey Cummings',
  	"coach":'Unassigned',
  	"business":'Business3',
  	"department":'department1',
  	"manager":'Jack Harris',
  	"zone":"East",
  	"location":"Odisha",
  },
  {
    "id":3,
  	"participantName":'Jack Harris',
  	"coach":'Unassigned',
  	"business":'Business2',
  	"department":'department2',
  	"manager":'Steve',
  	"zone":"North",
  	"location":"Uttar Pradesh",
  },
  {
    "id":4,
  	"participantName":'Stefanie Huff',
  	"coach":'Unassigned',
  	"business":'Business1',
  	"department":'department4',
  	"manager":'Steve',
  	"zone":"Central",
  	"location":"Madhya Pradesh",
  },
  {
    "id":5,
  	"participantName":'Mabel David',
  	"coach":'Unassigned',
  	"business":'Business1',
  	"department":'department4',
  	"manager":'Steve',
  	"zone":"North",
  	"location":"Hariyana",
  },
  {
    "id":6,
  	"participantName":'Frank Bradford',
  	"coach":'Unassigned',
  	"business":'Business1',
  	"department":'department5',
  	"manager":'Steve',
  	"zone":"South",
  	"location":"Kerala",
  },
  {
    "id":7,
  	"participantName":'Forbes Levine',
  	"coach":'Unassigned',
  	"business":'Business6',
  	"department":'department4',
  	"manager":'Steve',
  	"zone":"West",
  	"location":"Goa",
  },
  {
    "id":8,
  	"participantName":'Santiago Mcclain',
  	"coach":'Unassigned',
  	"business":'Business5',
  	"department":'department2',
  	"manager":'Leigh Beasley',
  	"zone":"East",
  	"location":"Bengal",
  },
  {
    "id":9,
  	"participantName":'Merritt Booker',
  	"coach":'Unassigned',
  	"business":'Business3',
  	"department":'department2',
  	"manager":'Merritt Booker',
  	"zone":"West",
  	"location":"Maharashtra",
  },
  {
    "id":10,
  	"participantName":'Oconnor Wade',
  	"coach":'Unassigned',
  	"business":'Business3',
  	"department":'department4',
  	"manager":'Santiago Mcclain',
  	"zone":"North",
  	"location":"Delhi",
  }]


  constructor(private router:Router,public routes:ActivatedRoute) {
    this.rows = this.coachingcalls;
   }

  ngOnInit() {
  }


  onSelect({ selected }) {
    console.log('Select Event', selected, this.selected);
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    if(this.selected.length>0){
      this.state = true;
    }else{
      this.state = false;
    }
  }

  gotoBack(){
    this.router.navigate(['../'],{relativeTo:this.routes})
  }

}
