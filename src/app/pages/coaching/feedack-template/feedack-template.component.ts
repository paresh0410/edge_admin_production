import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { SortablejsOptions } from "angular-sortablejs";
import { NgForm } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
// import { ToasterService } from 'angular2-toaster';
import { FeedackTemplateService } from './feedack-template.service';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
import { assessmenttemplateservice } from '../assessment-template/assessmnet-template.service';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { webApi } from '../../../service/webApi';
import { SuubHeader } from '../../components/models/subheader.model';
import { noData } from '../../../models/no-data.model';

@Component({
  selector: 'ngx-feedack-template',
  templateUrl: './feedack-template.component.html',
  styleUrls: ['./feedack-template.component.scss']
})
export class FeedackTemplateComponent implements OnInit {
  isSave: boolean = false;
  enableDeleteModal: boolean;
  delData: any;
  btnName: string = 'Submit';
  title:string='Add Section';
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No feedback templates are available at this time.",
    desc:"Feedback templates will appear after they are added by the admin . A feedback template allow the coach and learner to provide online feedback about the session",
    titleShow:true,
    btnShow:true,
    descShow:true,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/cc-add-feedback',
  }
  constructor(private router: Router,
    private spinner: NgxSpinnerService,
    // private toasterService: ToasterService, 
    private assessservice: assessmenttemplateservice,
    private feedbackService: FeedackTemplateService,
    private toastr: ToastrService, private http1: HttpClient,
    private commonFunctionService: CommonFunctionsService,
  ) {
    this.spinner.show()
    this.getHelpContent();
    this.tenantId = this.feedbackService.tenantId;
    this.userDetails = JSON.parse(localStorage.getItem('LoginResData'));
    this.getAllFeedback();
    this.getFeedbackDropdown();
  }

  tenantId: any = [];
  userDetails: any = [];

  @ViewChild('subsectionForm') SecForm: NgForm;

  onSubmitSec() {
    console.log(this.SecForm);
  }

  role: any = [
    {
      "id": 5,
      "name": "manager"
    },
    {
      "id": 8,
      "name": "learner"
    },
    {
      "id": 7,
      "name": "trainer"
    }
  ];

  datatype: any = [
    {
      "id": 1,
      "name": "text"
    },
    {
      "id": 2,
      "name": "datatime"
    },
  ];



  sectionList: any = [
    {
      "id": 1,
      "name": "section1",
      "role": "manager",
      "subSection": [{
        "subsecName": "subsec1",
        "datatype": "text",
        "minLength": 2,
        "maxLength": 5,
      },
      {
        "subsecName": "subsec2",
        "datatype": "text",
        "minLength": 2,
        "maxLength": 5,
      },
      {
        "subsecName": "subsec3",
        "datatype": "datetime",
        "minLength": null,
        "maxLength": null,
      }
      ],
    },


    {
      "id": 2,
      "name": "section2",
      "role": "trainer",
      "subSection": [{
        "subsecName": "subsec2",
        "datatype": "datetime",
        "minLength": null,
        "maxLength": null,
      },
      {
        "subsecName": "subsec3",
        "datatype": "text",
        "minLength": 2,
        "maxLength": 5,
      }
      ]
    },
    {
      "id": 3,
      "name": "section3",
      "role": "learner",
      "subSection": [{
        "subsecName": "subsec1",
        "datatype": "datetime",
        "minLength": null,
        "maxLength": null,
      }]
    },
    {
      "id": 4,
      "name": "section4",
      "role": "manager"
    }
  ]


  feedback: any = [];
  noFeedback: boolean = false;
  sectionshow: boolean = false;
  questions = [];
  qquestions = [];
  subsectionshow: boolean = false;
  header: SuubHeader  = {
    title:'Feedback Template',
    btnsSearch: true,
    searchBar: false,
    dropdownlabel: ' ',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: '',
    btnName3: '',
    btnAdd: 'Add Feedback',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: true,
    filter: false,
    showBreadcrumb: true,
    breadCrumbList:[
      {
        'name': 'Coaching',
        'navigationPath': '/pages/coaching',
      },]
  };
  ngOnInit() {
    this.spinner.show();
    setTimeout(() => {
      this.spinner.hide()
    }, 2000);
  }

  QuestionListOptions: SortablejsOptions = {
    group: {
      name: "questions"
      // pull: 'clone',
      // put: false,
    },
    sort: true,
    // draggable: '.draggable',
    // onEnd: this.onListItemDragEnd.bind(this),
    // put: false,
    handle: ".drag-handle"
    // draggable: '.draggable'
  };
  QuestionListOptions1: SortablejsOptions = {
    group: "questions1",
    sort: true,
    // draggable: '.draggable',
    // onEnd: this.onListItemDragEnd.bind(this),
    handle: ".drag-handle"
    // draggable: '.draggable'
  };

  roleList: any = [];
  datatypeList: any = [];
  showMax: boolean = false;
  labels: any = [
    { labelname: 'Section', bindingProperty: 'section', componentType: 'text' },
    { labelname: 'Role', bindingProperty: 'roleName', componentType: 'text' },
    { labelname: 'Feedback Question', bindingProperty: 'subSection', componentType: 'text' },
    { labelname: 'Datatype', bindingProperty: 'dataType', componentType: 'text' },
    { labelname: 'Action', bindingProperty: 'btntext', componentType: 'button' },
    { labelname: 'Delete', bindingProperty: 'btndelete', componentType: 'delete' }
  ]
  searchTemp(event) {
    const name = event.target.value;
    console.log('event', event);
    console.log('name', name);
    if (name == 'text') {
      this.showMax = true;
    } else {
      this.showMax = false;
    }
  }


  getFeedbackDropdown() {
    // this.spinner.show();
    const param = {
      'tId': this.tenantId,
    }
    const _urlDropdownFeedback: string = webApi.domain + webApi.url.dropdownFeedback;
    this.commonFunctionService.httpPostRequest(_urlDropdownFeedback,param)
    // this.feedbackService.dropdownFeedback(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('dropDown:', rescompData);
        if (result['type'] == true) {
          console.log('getAllDropdown:', rescompData);
          this.datatypeList = result['data'][0];
          this.roleList = result['data'][1];

          console.log('roleList', this.roleList);
          console.log('datdaList', this.datatypeList);

        } else {
          // var toast: any = {
          //   type: 'error',
          //   //title: 'Server Error!',
          //   body: 'Something went wrong.please try again later.',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(toast);
          this.presentToast('error', '');
        }


      }, error => {
        this.spinner.hide();
        this.presentToast('error', '');
      });;
  }

  back() {
    this.router.navigate(["/pages/coaching"]);
  }

  feedbackTemplate: any = {
    section: null,
    subSection: null,
    min: null,
    max: null,
    tempTypeId: '',
    roleCode: '',
  }

  addsection() {
    this.feedbackTemplate = {
      section: null,
      subSection: null,
      min: null,
      max: null,
      tempTypeId: '',
      roleCode: '',
    }
    this.sectionshow = true;

  }

  closesectionModel() {
    this.sectionshow = false;
    this.showMax = false;
  }

  closesubsectionModel() {
    this.subsectionshow = false;
  }

  addsubsection() {
    this.subsectionshow = true;
  }


  getAllFeedback() {
    // this.spinner.show();
    let param = {
      'tId': this.tenantId
    }
    const _urlFetchFeedback: string = webApi.domain + webApi.url.getFeedbackTemplate;
    this.commonFunctionService.httpPostRequest(_urlFetchFeedback,param)
    // this.feedbackService.getAllFeedback(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('getAllFeedback:', rescompData);
        if (result['type'] == true) {
          if (result['data'][0].length == 0) {
            this.noFeedback = true;
          } else {
            this.feedback = result['data'][0];
            for (let i = 0; i < this.feedback.length; i++) {
              if(this.feedback[i].visible == 1 ) {
                this.feedback[i].btntext = 'fa fa-eye';
              } else {
                this.feedback[i].btntext = 'fa fa-eye-slash';
              }
              if(this.feedback[i].isDelete ==0){
                this.feedback[i].btndelete = '';
              }
              else{
                this.feedback[i].btndelete = 'fa fa-trash';
              }
            }
            this.noFeedback = false;
            console.log('this.feedback', this.feedback);



            // console.log('this.nominatedEmpRows:', this.ObservationEmpRows);
            // console.log('this.allNominatedEmployees',this.allObservationEmployees);
          }

        } else {
          this.noFeedback = true;
          this.presentToast('error', '');
        }


      }, error => {
        this.noFeedback = true;
        this.presentToast('error', '');

      });
  }
  visibilityTableRow(row) {
    let value;
    let status;

    if (row.visible == 1) {
      row.btntext = 'fa fa-eye-slash';
      value  = 'fa fa-eye-slash';
      row.visible = 0
      status = 0;
    } else {
      status = 1;
      value  = 'fa fa-eye';
      row.visible = 1;
      row.btntext = 'fa fa-eye';
    }
    console.log(row, status);
    const param = {
      flag: 'feed',
      tmpId: row.ftId,
      stat: status
    }
    this.spinner.show();
    this.assessservice.enabledisableassessment(param).then(res => {
      console.log(res);
      try {
        if (res['type'] == true) {
          this.presentToast('success', res['data'][0][0]['msg']);
          this.getAllFeedback();
        }
      } catch (e) {
        console.log(e);
      }
      this.spinner.hide();
    }, err => {
      console.log(err);
      this.spinner.hide();
      this.presentToast('error', 'Something went wrong, please try again');
    });
  }
  // Disableassessment(i, data, status) {
  //   console.log(i, data, status);
  //   const param = {
  //     flag: 'feed',
  //     tmpId: data.ftId,
  //     stat: status
  //   }
  //   this.spinner.show();
  //   this.assessservice.enabledisableassessment(param).then(res => {
  //     console.log(res);
  //     try {
  //       if (res['type'] == true) {
  //         this.presentToast('success', res['data'][0][0]['msg']);
  //         this.getAllFeedback();
  //       }
  //     } catch (e) {
  //       console.log(e);
  //     }
  //     this.spinner.hide();
  //   }, err => {
  //     console.log(err);
  //     this.spinner.hide();
  //     this.presentToast('error', 'Something went wrong, please try again');
  //   });
  // }

  removeFedback(data) {

    var param = {
      ftId: data.ftId,
      tId: this.tenantId,
    };
    this.spinner.show();
    const _urlDeleteFeedback: string = webApi.domain + webApi.url.deleteFeedbackTemplate;
    this.commonFunctionService.httpPostRequest(_urlDeleteFeedback,param)
    // this.feedbackService.deleteFeedback(param)
      .then(
        rescompData => {
          this.spinner.hide();
          if (rescompData['type'] === true) {
            console.log('remove object', rescompData);
            this.presentToast('success', rescompData['data'][0][0]['msg']);
            this.getAllFeedback();
            this.closeDeleteModal();
          }
        },
        resUserError => {
          this.closeDeleteModal();
          this.spinner.hide();
        }
      );

    this.getAllFeedback();


  }

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }

  saveFeedback(data) {
    this.isSave = true
    this.spinner.show();
    const param = {
      rolId: data.roleCode,
      sec: data.section,
      subSec: data.subSection,
      dType: data.tempTypeId,
      tMin: data.min ? data.min : null,
      tMax: data.max ? data.max : null,
      tId: this.tenantId,
      userId: this.userDetails.data.data.id,
    };

    console.log('param', param)
    const _urlInsertFeedback: string = webApi.domain + webApi.url.insertFeedbackTemplate;
    this.commonFunctionService.httpPostRequest(_urlInsertFeedback,param)
    // this.feedbackService.insertFeedback(param)
      .then(rescompData => {
        this.spinner.hide();
        this.isSave = false
        var result = rescompData;
        console.log('InsertFeedback:', rescompData);
        if (result['type'] == true) {
          this.presentToast('success', 'Feedback saved.');
          this.sectionshow = false;
          this.showMax=false
          this.getAllFeedback();
        } else {
          this.presentToast('error', '');
        }
      }, error => {
        this.presentToast('error', '');
      });
  }
  // Help Code Start Here //

  helpContent: any;
  getHelpContent() {
    return new Promise(resolve => {
      this.http1
        .get('../../../../../../assets/help-content/addEditCourseContent.json')
        .subscribe(
          data => {
            this.helpContent = data;
            console.log('Help Array', this.helpContent);
          },
          err => {
            resolve('err');
          }
        );
    });
    // return this.helpContent;
  }
  closeDeleteModal() {
    this.enableDeleteModal = false;
  }
  enableDelaction(actionType) {
    console.log(this.delData);
    if (actionType == true) {  
      this.removeFedback(this.delData)
    } else {
      this.closeDeleteModal();
    }
  }
  clickToDelete(delData) {
      this.delData = delData;
      this.enableDeleteModal = true;
  }

  // Help Code Ends Here //
}
