import { TestBed } from '@angular/core/testing';

import { FeedackTemplateService } from './feedack-template.service';

describe('FeedackTemplateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FeedackTemplateService = TestBed.get(FeedackTemplateService);
    expect(service).toBeTruthy();
  });
});
