import { Injectable , Inject } from '@angular/core';
import { webApi } from '../../../service/webApi';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class FeedackTemplateService {

  private _urlFetchFeedback: string = webApi.domain + webApi.url.getFeedbackTemplate;
  private _urlDeleteFeedback: string = webApi.domain + webApi.url.deleteFeedbackTemplate;
  private _urlInsertFeedback: string = webApi.domain + webApi.url.insertFeedbackTemplate;
  private _urlDropdownFeedback: string = webApi.domain + webApi.url.dropdownFeedback;

    

  

  constructor(private _httpClient: HttpClient) { 
    if (localStorage.getItem('LoginResData')) {
      this.userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', this.userData.data);
      // this.userId = userData.data.data.id;
    }
    this.tenantId = this.userData.data.data.tenantId;
  }

  userData;
  tenantId;


  getAllFeedback(param) {
    return new Promise(resolve => {
      this._httpClient.post(this._urlFetchFeedback, param)
      //.map(res => res.json())
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
  }

  deleteFeedback(param) {
    return new Promise(resolve => {
      this._httpClient.post(this._urlDeleteFeedback, param)
      //.map(res => res.json())
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
  }

  
  insertFeedback(param) {
    return new Promise(resolve => {
      this._httpClient.post(this._urlInsertFeedback, param)
      //.map(res => res.json())
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
  }


  dropdownFeedback(param) {
    return new Promise(resolve => {
      this._httpClient.post(this._urlDropdownFeedback, param)
      //.map(res => res.json())
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });

  }
}

