import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeedackTemplateComponent } from './feedack-template.component';

describe('FeedackTemplateComponent', () => {
  let component: FeedackTemplateComponent;
  let fixture: ComponentFixture<FeedackTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeedackTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeedackTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
