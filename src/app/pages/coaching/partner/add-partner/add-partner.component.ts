import { Component, OnInit , ViewEncapsulation} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AddEditPartnerService } from './add-partner.service';

@Component({
  selector: 'ngx-add-partner',
  templateUrl: './add-partner.component.html',
  styleUrls: ['./add-partner.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AddPartnerComponent implements OnInit {
  roles = ['Partner', 'Consultant', 'Client']
	statuss = ['Active','Inactive']
	member : any = {
    id : 0,
    name : "",
    designation : "",
    mobile : "",
    email : "",
    dob : "",
    doj : "",
    qualification : "",
    cost : "",
    role : "",
    status : "",
};
	// myOptions: INgxMyDpOptions = {
  //       // other options...
  //       dateFormat: 'dd.mm.yyyy',
  //   };
  addPartner:boolean = true;
	registerForm: FormGroup;
  	    submitted = false;

  constructor(private formBuilder: FormBuilder,private router:Router,private routes:ActivatedRoute,private addeditpartner:AddEditPartnerService) {
   console.log(this.member);
  	 	if(this.addeditpartner.editPartner.id != undefined){
         this.member = this.addeditpartner.editPartner;
      }
    	if(this.member.id!= 0){
      		// console.log('newTeamMember',DATA);
      		this.addPartner= false;
      		console.log('this.member',this.member);
      		this.member.name = this.addeditpartner.editPartner.partner;
      		//this.member.business = this.addeditpartner.editPartner.partner;
     			this.member.designation = this.addeditpartner.editPartner.department;
     			this.member.mobile = 9869330761;
     			this.member.email = 'abc@gmail.com'
      		this.member.dob = {date: {year: 2018, month: 11, day: 1},
								epoc: 1541010600,
								formatted: "01.11.2018",}
      		this.member.doj = {date: {year: 2018, month: 11, day: 1},
								epoc: 1541010600,
								formatted: "01.11.2018",}
      		this.member.qualification = 'Graduate';
      		this.member.cost = 100000;
      		this.member.role = 'Partner';
      		this.member.status='Active'
      	}
    }

  ngOnInit() {
  	 this.registerForm = this.formBuilder.group({
            name: ['', Validators.required],
            dob: [null, Validators.required],
            designation: ['', Validators.required],
            qualification: ['', Validators.required],
            doj: [null, Validators.required],
            mobile: ['', [Validators.required,Validators.minLength(10),Validators.maxLength(10)]],
            email: ['', Validators.required],
            cost: ['', Validators.required],
            role: ['', Validators.required],
            status: ['', Validators.required]
        });
  }

  	get f() { return this.registerForm.controls; }

  	onSubmit(item) {
        this.submitted = true;
        // console.log('new member',item);
        
        // stop here if form is invalid
        if (this.registerForm.invalid) {
        	
            return;

        }else{
        	//var i1 = JSON.stringify(item);
        	console.log('newPartner',item);
        	this.addeditpartner.newPartner = item;
            this.router.navigate(['../../partner'],{relativeTo:this.routes})

        }

        // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value))
    }

     setDate(): void {
        // Set today date using the patchValue function
        let date = new Date();
        this.registerForm.patchValue({dob: {
        date: {
            year: date.getFullYear(),
            month: date.getMonth() + 1,
            day: date.getDate()}
        }});
    }

    clearDate(): void {
        // Clear the date using the patchValue function
        this.registerForm.patchValue({dob: null});
    }

    gotoBack(){
    	 this.router.navigate(['../../partner'],{relativeTo:this.routes})
    	}

}
