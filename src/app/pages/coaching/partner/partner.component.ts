import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AddEditPartnerService } from './add-partner/add-partner.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'ngx-partner',
  templateUrl: './partner.component.html',
  styleUrls: ['./partner.component.scss']
})
export class PartnerComponent implements OnInit {
  registerForm: FormGroup;
  countryForm:any;
  submitted = false;
  addForm:boolean=false;
  addPartner : any = {};
  rows: any = [];
  rows1:any =[];
  expanded: any = {};
  timeout: any;
  state:any;
  index:any;
   newMember:any;
    pushnewMember:any ={
    name: "",
    designation: "",
    mobile:"",
    email:"",
    status:""
  };

	data:any=[
    {
      "id":1,
      "partner":"Tony Stark",
        "business": "Finatics",
        "department": "Developer",
        "level":"7",
    },{
      "id":2,
      "partner":"Peter Parker",
        "business": "Finatics",
        "department": "Team Leader",
        "level":"7",
    },{
      "id":3,
      "partner":"Bruce Banner",
        "business": "Mobeserv",
        "department": "Developer",
        "level":"7",
    },{
      "id":4,
      "partner": "Steve Rogers",
        "business": "Bajaj",
        "department": "Developer",
        "level":"7",
    },{
      "id":5,
      "partner":"Bruce Wayne",
        "business": "Mobeserv",
        "department": "Developer",
        "level":"7",
    },{
      "id":6,
      "partner":"Richard Castle",
        "business": "Bajaj",
        "department": "Developer",
        "level":"7",
    },{
      "id":7,
      "partner":"Kate Beckett",
        "business": "Bajaj",
        "department": "Developer",
        "level":"7",
    },{
      "id":8,
      "partner":"Natalia Romanov",
        "business": "Mindspace",
        "department": "Developer",
        "level":"7",
    },
  ];

  constructor(private formBuilder: FormBuilder,private router:Router,private routes:ActivatedRoute,private addeditpartner:AddEditPartnerService) {
    this.newMember = this.addeditpartner.newPartner;
    console.log('partNew',this.newMember)
  if(this.newMember!= null){
      //console.log('newTeamMember',DATA);
      //this.newMember = JSON.parse(DATA);
      console.log('this.newMember',this.newMember);
      if(this.newMember.id == 0){
          this.pushnewMember.partner = this.newMember.name;
          this.pushnewMember.business = this.newMember.qualification;
          this.pushnewMember.department = this.newMember.cost;
          this.pushnewMember.level = this.newMember.mobile;
          // this.pushnewMember.designation = this.newMember.designation;
          // this.pushnewMember.mobile = this.newMember.mobile;
          // this.pushnewMember.email = this.newMember.email;
          // this.pushnewMember.status = this.newMember.status;
          this.data.push(this.pushnewMember);
      }else{
          for(var i=0;i<this.data.length;i++){
              if(this.data[i].id == this.newMember.id){
                  this.data[i].partner = this.newMember.name;
                  this.data[i].department = this.newMember.designation;
                  this.data[i].level = this.newMember.mobile;
                  // this.data[i].email = this.newMember.email;
                  // this.data[i].status = this.newMember.status;

              }
          }
      }
  }
  
  // this.fetch((data) => {
  //   this.rows = data;
  //  });
  this.rows=this.data;

   }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      business: ['', Validators.required],
      department: ['', Validators.required],
      partner: ['', Validators.required],
      level: ['', Validators.required],
  });
  this.countryForm = this.formBuilder.group({
   countryControl: ['Canada']
});
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {
      this.submitted = true;

      // stop here if form is invalid
      if (this.registerForm.invalid) {
          return;
      }

      alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value))
  }

  // gotoaddpartner(){
  //   this.addeditpartner.editPartner = {};
  //     this.router.navigate(['add-partner'],{relativeTo:this.routes})
  // }

   editPartner(item,id){
  this.addeditpartner.editPartner = this.data[item];
  console.log('editPartner',this.addeditpartner.editPartner)
  this.router.navigate(['add-partner'],{relativeTo:this.routes})
  //console.log('editing1',i2)

}

  gotoAddPartner(){
    this.addeditpartner.editPartner = {};
    this.router.navigate(['add-partner'],{relativeTo:this.routes});
  }

  gotoBack(){
    this.router.navigate(['/pages/coaching'],{relativeTo:this.routes});
  }
  

}
