import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ngx-modal-viewer',
  templateUrl: './modal-viewer.component.html',
  styleUrls: ['./modal-viewer.component.scss']
})
export class ModalViewerComponent implements OnInit {

 @Input() modalInfo:any = {
    title: '',
    subtitle: '',
    message: '',
    event_first: '',
    event_second: ''
 };

 @Output() acceptEvent = new EventEmitter<string>();
 @Output() rejectEvent = new EventEmitter<string>();
 @Output() closeEvent = new EventEmitter<string>();
  
  constructor() { }

  ngOnInit() {
  }
  /**
   * @event - Accept event
   */
  accepted() {
    this.acceptEvent.emit('true');
  }

  /**
   * @event - Rejected Event
   */
  rejected() {
    this.rejectEvent.emit('false');
  }

  /**
   * @event - Close modal Popup
   */
  close() {
    this.closeEvent.emit('');
  }

}
