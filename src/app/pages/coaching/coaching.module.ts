import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CommonModule } from '@angular/common';
import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { FormsModule } from '@angular/forms';
import { CallDetailModule } from './call-detail/call-detail.module';
import { ParticipantModule } from './participants/participants.module';


import { TeamComponent } from './team/team.component';
import { CallMappingComponent } from './call-mapping/call-mapping.component';
import { CoachingComponent } from './coaching.component';
import { AddEditTeamComponent } from './team/add-edit-team/add-edit-team.component';
import { AddEditTeamService } from './team/add-edit-team/add-edit-team.service';
import { ReactiveFormsModule } from '@angular/forms';
// import { ParticipantsComponent } from './participants/participants.component';
import { CalendarComponent } from './calendar/calendar.component';
import { CalendarService } from './calendar/calendar.service'
import { PartnerComponent } from './partner/partner.component';
import { AttributeSkillComponent } from './attribute-skill/attribute-skill.component';
import { AttributeMasterComponent } from './attribute-master/attribute-master.component';
import { CallsComponent } from './calls/calls.component';
import { AddAttributeMasterComponent } from './attribute-master/add-attribute-master/add-attribute-master.component';
import { AddAttributeSkillComponent } from './attribute-skill/add-attribute-skill/add-attribute-skill.component';
import { AddPartnerComponent } from './partner/add-partner/add-partner.component';
import { AddEditPartnerService } from './partner/add-partner/add-partner.service';

import { DemoMaterialModule } from '.././material-module';
import { FlatpickrModule } from 'angularx-flatpickr';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { from } from 'rxjs';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import {ModalViewerComponent} from './component/modal-viewer/modal-viewer.component';
import { FeedackTemplateComponent } from './feedack-template/feedack-template.component';

import { SortablejsModule } from 'angular-sortablejs';
import { AssessmentTemplateComponent } from './assessment-template/assessment-template.component';
import { ObservationTemplateComponent } from './observation-template/observation-template.component';
import { ObservationTemplateService } from './observation-template/observation-template.service';
import { MasterComponent } from './master/master.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { ComponentModule } from '../../component/component.module';
import { CoachingRoutingModule } from './coaching-routing.module';

@NgModule({
  imports: [
    CommonModule,
    NgxDatatableModule,
    BsDatepickerModule.forRoot(),
    DatepickerModule.forRoot(),
    FormsModule,
    ParticipantModule,
    ReactiveFormsModule,
    CallDetailModule,
    SortablejsModule,
    DemoMaterialModule,
    AngularMultiSelectModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    ComponentModule,
    FlatpickrModule.forRoot(),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
    ComponentModule,
    CoachingRoutingModule,
],
declarations: [
    CoachingComponent,
    TeamComponent,
    CallMappingComponent,
    AddEditTeamComponent,
    // ParticipantsComponent,
    CalendarComponent,
    PartnerComponent,
    AttributeSkillComponent,
    AttributeMasterComponent,
    CallsComponent,
    AddAttributeMasterComponent,
    AddAttributeSkillComponent,
    AddPartnerComponent,
    ModalViewerComponent,
    FeedackTemplateComponent,
    AssessmentTemplateComponent,
    ObservationTemplateComponent,
    MasterComponent,
],
providers: [
  AddEditTeamService,
  AddEditPartnerService,
  CalendarService,
  ObservationTemplateService
],
schemas: [

]
})

export class CoachingModule{

}
