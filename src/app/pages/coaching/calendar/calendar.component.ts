import { Router, ActivatedRoute } from '@angular/router';
import { Component, ViewChild, TemplateRef, ViewEncapsulation, OnInit, ChangeDetectorRef } from '@angular/core';
import { startOfDay, endOfDay, subDays, addDays, endOfMonth, isSameDay, isSameMonth, addHours } from 'date-fns';
import { Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarView } from 'angular-calendar';
import { CalendarService } from './calendar.service';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { NgxSpinnerService } from 'ngx-spinner';
import { CallDetailService } from '../call-detail/call-detail.service';
import { ParticipantsService } from '../participants/participants.service';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
import * as _moment from 'moment';
import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';
import { webApi } from '../../../service/webApi';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { SuubHeader } from '../../components/models/subheader.model';
import { noData } from '../../../models/no-data.model';
import { BrandDetailsService } from '../../../service/brand-details.service';

const moment = (_moment as any).default ? (_moment as any).default : _moment;
export const MY_CUSTOM_FORMATS = {
  fullPickerInput: 'DD-MM-YYYY HH:mm:ss',
  parseInput: 'DD-MM-YYYY HH:mm:ss',
  datePickerInput: 'DD-MM-YYYY',
  timePickerInput: 'LT',
  monthYearLabel: 'MMM YYYY',
  dateA11yLabel: 'LL',
  monthYearA11yLabel: 'MMMM YYYY',
};

export declare enum CalendarEventTimesChangedEventType {
  Drag = "drag",
  Drop = "drop",
  Resize = "resize"
}
export interface CalendarEventTimesChangedEvent1<MetaType = any> {
  type: CalendarEventTimesChangedEventType;
  event: CalendarEvent1<MetaType>;
  newStart: Date;
  newEnd?: Date;
  allDay?: boolean;
}
export interface EventColor {
  primary: string;
  secondary: string;
}
export interface EventAction {
  label: string;
  cssClass?: string;
  onClick({ event }: {
    event: CalendarEvent1;
  }): any;
}
export interface CalendarEvent1<MetaType = any> {
  callId?: string | number;
  empId?: any;
  empName?: any;
  trainerId?: any;
  trainerName?: any;
  managerEmpId?: any;
  managerName?: any;
  id?: string | number;
  callStartDate?: Date;
  callEndDate?: Date;
  callDateTime?: Date;
  title: string;
  color?: EventColor;
  actions?: EventAction[];
  allDay?: boolean;
  cssClass?: string;
  resizable?: {
    beforeStart?: boolean;
    afterEnd?: boolean;
  };
  draggable?: boolean;
  meta?: MetaType;
}

const colors: any = {
  red: {
    primary: '#f52929',
    secondary: '#FAE3E3',
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF',
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA',
  },
};

@Component({
  selector: 'ngx-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    { provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE] },
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS }]
})
export class CalendarComponent implements OnInit {
  eventsList: any = {
    // startDate: null,
    // endDate: null,
    // cDateTime: null,
    // selectedCoach: [],
    // selectedEmployee : [],
  };
  eventsBucket: any;
  addEventModal: boolean = false;
  allCoach: any = [];
  allNominatesEmployee: any = [];
  public startMinDate: any = new Date();
  public abc: any = new Date();
  endDateMins: Date;
  endMaxDates: Date;

  @ViewChild('modalContent') modalContent: TemplateRef<any>;

  view: CalendarView = CalendarView.Month;

  CalendarView = CalendarView;

  viewDate: Date = new Date();

  modalData: {
    action: string;
    event: CalendarEvent;
  };

  // actions: CalendarEventAction[] = [
  //   {
  //     label: '<i class="fa fa-fw fa-pencil"></i>',
  //     onClick: ({ event }: { event: CalendarEvent1 }): void => {
  //       this.handleEvent('Edited', event);
  //     }
  //   },
  //   {
  //     label: '<i class="fa fa-fw fa-times"></i>',
  //     onClick: ({ event }: { event: CalendarEvent1 }): void => {
  //       this.events = this.events.filter(iEvent => iEvent !== event);
  //       this.handleEvent('Deleted', event);
  //     }
  //   }
  // ];
  noDataVal:noData={
    margin:'mt-3 w-100',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No call added under a nomination.",
    desc:"",
    titleShow:true,
    btnShow:true,
    descShow:false,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/cc-call',
  }
  refresh: Subject<any> = new Subject();
  openevent: any;
  events: CalendarEvent1[] = [];
  activeDayIsOpen: boolean = true;
  eventlist: any = [];
  empId: any;
  tenantId: any;
  userId: any;
  participantData: any;
  settingsEmployeeDrop: any = {};
  settingsCoachDrop: any = {};
  selectedEmployee: any = [];
  selectedCoach: any = [];
  fetchAllCalls: boolean = false;
  disabledEmpDrop: boolean = false;
  todaydate = new Date();
  findate: any;
  confirmmsg: boolean = false;
  ddata: any;
  allCoachAutoDrop: any = [];
  // selectedCoach:any;
  coachList: any = [];
  eenddate: any;
  helpContent: any;
  header: SuubHeader  = {
    title: '',
    btnsSearch:true,
    btnBackshow:true,
    showBreadcrumb:true,
    breadCrumbList:[
      {
        'name': 'Coaching',
        'navigationPath': '/pages/coaching',
      },
      {
        'name': 'Nominations',
        'navigationPath': '/pages/coaching/participants',
      },
    ]
  };
  title: string;
  currentBrandData: any;
  employee: any;
  constructor(private router: Router, private routes: ActivatedRoute, private modal: NgbModal,
    private calendarService: CalendarService, private spinner: NgxSpinnerService,
    // private toasterService: ToasterService,
    private calldetailservice: CallDetailService,  public brandService: BrandDetailsService,
    private participantservice: ParticipantsService, private cdf: ChangeDetectorRef,
    private toastr: ToastrService, private http1: HttpClient,private commonFunctionService: CommonFunctionsService,
  ) {
    this.getHelpContent();
    if (localStorage.getItem('LoginResData')) {
      const userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', userData.data);
      this.userId = userData.data.data.id;
      this.tenantId = userData.data.data.tenantId;
    }
    // this.getAllNominatedEmployee('testuser4');
    if (this.participantservice.getAllCalls) {
      this.disabledEmpDrop = false;
      this.fetchAllCalls = this.participantservice.getAllCalls;
      this.selectedCoach = [];
      this.selectedEmployee = [];
    }

    console.log('this.fetchAllCalls', this.fetchAllCalls);

    this.settingsEmployeeDrop = {
      text: 'Select Employee',
      singleSelection: true,
      classes: 'common-multi',
      primaryKey: 'empId',
      labelKey: 'empName',
      noDataLabel: 'Search Employee...',
      enableSearchFilter: true,
      searchBy: ['empCode', 'empName'],
      maxHeight:250,
      lazyLoading: true,
    };

    this.settingsCoachDrop = {
      text: 'Select Trainer',
      singleSelection: true,
      classes: 'common-multi',
      primaryKey: 'trainerId',
      labelKey: 'trainerName',
      noDataLabel: 'Search trainer...',
      enableSearchFilter: true,
      searchBy: ['trainerId', 'trainerName'],
      maxHeight:250,
      lazyLoading: true,
    };
    console.log('this.calendarService.participantData', this.calendarService.participantData);
    console.log(Object.keys(this.calendarService.participantData).length);
    if (Object.keys(this.calendarService.participantData).length !== 0) {
      this.participantData = this.calendarService.participantData;
      const empObj = {
        empId: this.participantData.nomEmpId,
        empCode: this.participantData.code,
        empName: this.participantData.name,
      };
      let nomsdate = new Date(this.participantData.nomDate);
      console.log(nomsdate);
      if (this.startMinDate < nomsdate) {
        this.startMinDate = nomsdate;
        this.startMinDate.setDate(this.startMinDate.getDate() - 1);
      } else {
        this.startMinDate.setDate(this.startMinDate.getDate() - 1);
        console.log(this.startMinDate);
      }

      this.selectedEmployee.push(empObj);
      this.disabledEmpDrop = true;
    } else {
      this.disabledEmpDrop = false;
      this.selectedCoach = [];
      this.selectedEmployee = [];
      this.gotoParticipant();
    }
    this.getTrainer();
    this.getCallsById();
    this.header.title=this.participantData.name;
  }

  ngOnInit() {
    this.currentBrandData = this.brandService.getCurrentBrandData();
    this.employee=this.currentBrandData.employee
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    console.log(date);
    var startDate1 = new Date(
      date.getFullYear(),
      date.getMonth(),
      1
    );
    var endDate1 = new Date(
      date.getFullYear(),
      date.getMonth() + 1,
      0
    );
    if (new Date(date) >= startDate1 &&
      new Date(date) <= endDate1
    ) {
      this.viewDate = date;
      this.closeOpenMonthViewDay();
    }
    this.eventlist = events;
    if (isSameMonth(date, this.viewDate)) {
      this.viewDate = date;
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        // this.activeDayIsOpen = false;;
        this.openevent = true;
      } else {
        this.openevent = false;
        // this.activeDayIsOpen = true;
      }
      this.cdf.detectChanges();
    }
  }

  closeOpenMonthViewDay() {
    this.eventlist = [];
    console.log(this.viewDate);
    if (this.viewDate) {
      this.liveevent(this.events, this.viewDate);
    }
  }

  handleEvent(action: string, event: CalendarEvent1): void {
    //this.modalData = { event, action };
    //this.modal.open(this.modalContent, { size: 'lg' });
    console.log('event', event);
    console.log('action', action);
  }

  addEvent(data): void {
    this.eventsList = [];
    this.selectedCoach = [];
    //this.callFromService = false;
    console.log('data', data);
    if (data === null) {
      this.addAction = true;
      this.title="Add Call"
    } else {
      if(data && data.isEditable === 0){
        this.toastr.warning('You cannot edit past or current calls' , 'Warning');
        return null;
      }
      this.addAction = false;
      this.title="Edit Call"
      const selectedEmployee = [];
      const selectedTrainer = [];

      const trainObj = {
        trainerId: data.trainerId,
        trainerName: data.trainerName,
      };
      selectedTrainer.push(trainObj);

      const empObj = {
        empId: data.empId,
        empName: data.empName,
      };
      selectedEmployee.push(empObj);

      this.selectedCoach = selectedTrainer;
      this.selectedEmployee = selectedEmployee;
      // this.startMinDate = data.callStartDateF;
      this.eenddate = data.callStartDateF;
      this.eventsList = {
        callId: data.callId,
        startDate: data.callStartDate,
        endDate: data.callEndDate,
        // selectedCoach : selectedTrainer,
        // selectedEmployee : selectedEmployee,
        cDateTime: new Date(data.callDateTimeF),
      };
      console.log(this.eventsList);
      this.startMinDate = new Date(this.eventsList.startDate);
      this.endDateMins = new Date(this.eventsList.startDate);
      this.eenddate = new Date(this.eventsList.startDate);
      this.startMinDate.setDate(this.startMinDate.getDate() - 1);
      console.log(this.startMinDate);
    }
    this.addEventModal = true;
  }

  gotoCallDetail(data) {
    console.log('Event:', data);
    this.calldetailservice.empId = data.empId;
    this.calldetailservice.coachId = data.trainerId;
    this.calldetailservice.callId = data.callId;
    this.calldetailservice.empName = data.empName;
    this.router.navigate(['call-detail'], { relativeTo: this.routes });
  }

  gotoparticipants() {
    window.history.back();
    // this.router.navigate(['../../participants'], { relativeTo: this.routes });
  }

  closeModal() {
    this.addEventModal = false;
    //this.addAction = false;
  }

  // saveEvent(event) {
  //   console.log(event);
  //   this.eventsBucket = {
  //     startDate: event.startDate,
  //     endDate: event.endDate,
  //     coach: event.selectedCoach[0].trainerName,
  //     employee: event.selectedEmployee[0].empName,
  //   };
  //   console.log("Event saved", this.eventsBucket);
  //   this.eventsList.push(this.eventsBucket);
  //   this.refresh.next();
  //   this.addEventModal = false;
  // }
btnName='Save'
  addAction: boolean = false;
  callFromService: boolean = false;
  noCalls: boolean = true;
  getCallsById() {
    this.spinner.show();
    const param = {
      'empId': this.fetchAllCalls ? 0 : this.participantData.nomEmpId,
      'tId': this.tenantId,
    };
    const _urlGetCallById: string = webApi.domain + webApi.url.getcallsbyempid;
    this.commonFunctionService.httpPostRequest(_urlGetCallById,param)
    // this.calendarService.getCallsById(param)
      .then(rescompData => {
        this.spinner.hide();
        //const res = rescompData;
        const finalRes = rescompData['data'][0];
        // this.events = finalRes;
        console.log('AllCalls:', finalRes);
        if (rescompData['type'] === true) {
          // this.events = [];
          if (finalRes.length === 0) {
            this.addAction = true;
             this.title="Add Call"
            this.noCalls = true;
          } else {
            this.noCalls = false;
            this.callFromService = true;
            finalRes.forEach(element => {
              element.start = new Date(element.callStartDateF);
              element.callEndDate = new Date(element.callEndDateF);
              element.calldate = this.formateddate(element.callDateTimeF);
              // element.callStartDate = this.formatDisplayDate(element.callStartDateF);
              // element.callEndDate = this.formatDisplayDate(element.callEndDateF);
              // element.callDateTime = this.formatDisplayDate(element.callDateTimeF) + ' ' + this.formatDisplayDateTime(element.callDateTimeF);
            });
            this.events = finalRes;
            // var todaydate = new Date();
            this.findate = this.todaydate;
            this.liveevent(finalRes, this.findate);
            console.log('events', this.events);
            this.addAction = false;
            this.title="Edit Call"
          }
          console.log('rescompData', rescompData);
        } else {
          this.presentToast('error', ' ');
        }
      },
        resUserError => {
          this.spinner.hide();
          this.presentToast('error', ' ');
        });
  }

  liveevent(eventsfordisaply, todaydate) {
    this.eventlist = [];
    console.log(eventsfordisaply);
    var currentdate = new Date(todaydate);
    var startDate = new Date(
      currentdate.getFullYear(),
      currentdate.getMonth(),
      1
    );
    var endDate = new Date(
      currentdate.getFullYear(),
      currentdate.getMonth() + 1,
      0
    );
    for (let i = 0; i < eventsfordisaply.length; i++) {
      if (
        // this.formdate(eventsfordisaply[i].eventDate) ==
        // this.formdate(currentdate)
        new Date(eventsfordisaply[i].callStartDateF) >= startDate &&
        new Date(eventsfordisaply[i].callStartDateF) <= endDate
      ) {
        // this.eventlist = eventsfordisaply;
        this.eventlist.push(eventsfordisaply[i]);
        if (this.eventlist.length === 0) {
          this.noCalls = true;
        }
      }
      this.cdf.detectChanges();
    }
  }
  deleteCall(data) {
    this.ddata = data;
    this.confirmmsg = true;
  }
  closemodel(text) {
    console.log(text);
    if (text == 1) {
      this.confirmmsg = false;
    } else {
      this.delCall(this.ddata);
    }

  }
  delCall(data) {
    this.confirmmsg = true;
    const param = {
      'cId': data.callId,
      'tId': this.tenantId,
    };
    console.log(data);
    const  _urlDeleteCallById: string = webApi.domain + webApi.url.deletecallbyid;
    this.commonFunctionService.httpPostRequest(_urlDeleteCallById,param)
    // this.calendarService.deleteCallById(param)
      .then(rescompData => {
        this.spinner.hide();
        const res = rescompData;
        if (res['type'] === true) {
          this.removeevnetlist(data);
          this.removeevents(data);
          console.log('rescompData', rescompData);
          this.presentToast('success', 'Call deleted successfully.');
          this.selectedCoach = [];
          this.confirmmsg = false;
          // this.getCallsById();
        } else {
          this.presentToast('error', '');
        }
      },
        resUserError => {
          this.spinner.hide();
          this.presentToast('error', '');
        });
  }
  removeevnetlist(data) {
    for (let i = 0; i < this.eventlist.length; i++) {
      if (data.callId == this.eventlist[i].callId) {
        this.eventlist.splice(i, 1);
      }
    }
  }
  removeevents(data) {
    for (let i = 0; i < this.events.length; i++) {
      if (data.callId == this.events[i].callId) {
        this.events.splice(i, 1);
      }
    }
    this.cdf.detectChanges();
  }
  saveCall(form, data) {
    console.log('Form ===>',form);
    if(form && form.valid){
      this.spinner.show();
      console.log('callDataToPush:', form, data);
      data.startDate = this.formatSendDate(data.startDate);
      data.endDate = this.formatSendDate(data.endDate);
      data.cDateTime = this.formatSendDateTime(data.cDateTime);
        const param = {
          'cId': this.addAction ? 0 : data.callId,
          'empId': this.selectedEmployee[0].empId,
          'sDate': data.startDate,
          'cDate': data.endDate,
          'cDateTime': data.cDateTime,
          'tId': this.tenantId,
          'userId': this.userId,
          'trainerId': this.selectedCoach[0].trainerId,
        };
        this.todaydate = param.sDate;
        this.viewDate = param.sDate;
        console.log('param', param);
        const _urlAddEditCall: string = webApi.domain + webApi.url.addeditcall;
        this.commonFunctionService.httpPostRequest(_urlAddEditCall,param)
        // this.calendarService.addEditCall(param)
          .then(rescompData => {
            this.addEventModal = false;
            this.spinner.hide();
            const result = rescompData;
            if (this.addAction) {
            this.spinner.hide();
              if (result['type'] === true) {
                this.presentToast('success', 'Call added successfully.');
                this.getCallsById();
                this.closeModal()
              } else {
                this.presentToast('error', '');
              }
            } else {
              if (result['type'] === true) {
                this.presentToast('success', 'Call updated successfully.');
            this.spinner.hide();
                this.getCallsById();
                this.closeModal()
              } else {
                this.presentToast('error', '');
              }
            }
          },
            resUserError => {
              this.addEventModal = false;
              this.presentToast('error', '');
            });
      
    }else {
      Object.keys(form.controls).forEach(key => {
        form.controls[key].markAsDirty();
      });
    }
   

  }
  ///////////////////////////////////Trainer and Nominated Employee??????????????????????????
  getTrainer() {
    const param = {
      'partnerId': this.fetchAllCalls ? 1 : this.participantData.partnerId,
      'tId': this.tenantId,
    };
    const _urlGetTrainerByPartner: string = webApi.domain + webApi.url.getTrainerByPartner;
    this.commonFunctionService.httpPostRequest(_urlGetTrainerByPartner,param)
    // this.calendarService.getTrainerPartner(param)
      .then(rescompData => {
        this.spinner.hide();
        const res = rescompData;
        console.log('All Coach', rescompData);
        if (res['type'] === true) {
          this.allCoach = res['data'][0];
          console.log('All Coach', this.allCoach);
          this.prepareCoachDropdown();
        } else {
          this.presentToast('error', '');
        }
      },
        resUserError => {
          this.spinner.hide();
          this.presentToast('error', '');
        });
  }

  getAllNominatedEmployee(evt) {
    this.allNominatesEmployee = [];
    console.log(evt.target.value);
    const str = evt.target.value;
    const param = {
      'srcStr': str,
      'tenantId': this.tenantId,
    };
    const _urlFetchAllNominatedEmployee: string = webApi.domain + webApi.url.getallnominatedemployees;
    this.commonFunctionService.httpPostRequest(_urlFetchAllNominatedEmployee,param)
    // this.participantservice.getAllNominatedEmployees(param)
      .then(rescompData => {
        this.spinner.hide();
        const res = rescompData;
        if (res['type'] === true) {
          this.allNominatesEmployee = res['data'][0];
          console.log('All Nominated Employees:', this.allNominatesEmployee);
          this.prepareEmployeeDropdown();
        } else {
          this.presentToast('error', '');
        }
      },
        resUserError => {
          this.spinner.hide();
          this.presentToast('error', '');
        });
  }
  allEmployeeAutoDrop: any = [];
  //selectedEmployee:any;
  usersList: any = [];
  prepareEmployeeDropdown() {
    this.allEmployeeAutoDrop = [];
    this.allNominatesEmployee.forEach(element => {
      let allEmployeeAutoDropObj = {};
      allEmployeeAutoDropObj = {
        empId: element.nomEmpId,
        empCode: element.code,
        empName: element.name,
      };
      this.allEmployeeAutoDrop.push(allEmployeeAutoDropObj);

    });
    console.log('this.allEmployeeAutoDrop', this.allEmployeeAutoDrop);
  }

  onCreatorSelect(item: any) {
    console.log(item);
    // console.log(this.selectedEmployee);
  }
  OnCreatorDeSelect(item: any) {
    console.log(item);
    // console.log(this.selectedEmployee);
  }

  //   onCreatorSearch(evt: any) {
  //     console.log(evt.target.value);
  //     const val = evt.target.value;
  //     this.usersList = [];
  //     const tuser = this.allEmployeeAutoDrop.filter(function(d) {
  //     return String(d.empId).toLowerCase().indexOf(val) !== -1 ||
  //     d.empName.toLowerCase().indexOf(val) !== -1 || !val;
  //   });

  //   // update the rows
  //   this.usersList = tuser;
  // }

  onCreatorSearch(evt: any) {
    console.log(evt.target.value);
    const val = evt.target.value;
    this.usersList = [];
    const tuser = this.allEmployeeAutoDrop.filter(function (d) {
      return String(d.empId).toLowerCase().indexOf(val) !== -1 ||
        d.empName.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.usersList = tuser;
  }

  prepareCoachDropdown() {
    this.allCoach.forEach(element => {
      let allCoachAutoDropObj = {};
      allCoachAutoDropObj = {
        trainerId: element.trainerId,
        trainerName: element.trainerName,
      };
      this.allCoachAutoDrop.push(allCoachAutoDropObj);

    });
    console.log('this.allCoachAutoDrop', this.allCoachAutoDrop);
  }

  onCoachSelect(item: any) {
    console.log(item);
    //console.log(this.selectedCoach);
  }
  OnCoachDeSelect(item: any) {
    console.log(item);
    //console.log(this.selectedCoach);
  }

  onCoachSearch(evt: any) {
    console.log(evt.target.value);
    const val = evt.target.value;
    this.coachList = [];
    const tcoach = this.allCoachAutoDrop.filter(function (d) {
      return String(d.trainerId).toLowerCase().indexOf(val) !== -1 ||
        d.trainerName.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.coachList = tcoach;
  }

  gotoParticipant() {
    this.router.navigate(['../participants']);
  }

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false
      });
    } else if (type === 'error') {
      this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false
      })
    }
  }

  endMinDate(date) {
    console.log(date);
    console.log(this.eventsList.startDate);

    this.endDateMins = new Date(date);
    // this.endDateMins = date;
    this.eenddate = date;
    console.log("endDateMins", this.endDateMins);
    console.log("endDateMins", new Date());
    if (this.endDateMins.getDate() == new Date().getDate()) {
      this.endDateMins = new Date();
    }
    // this.endDateMins.setDate(this.endDateMins.getDate() + 1);
    console.log(this.endDateMins);
    if (this.eventsList.endDate) {
      if (this.eventsList.endDate < this.eventsList.startDate) {
        this.eventsList.endDate = '';
      }
    }
    this.eventlist.cDateTime = '';
  }

  endMaxDate(date) {
    this.endMaxDates = new Date(date);
    this.endMaxDates.setHours(23, 59);
    if (this.eventsList.endDate < this.eventsList.cDateTime) {
      this.eventsList.cDateTime = '';
    }
    // this.eventlist.cDateTime = '';
    console.log(this.endMaxDates);
    // this.endDateMins.setDate(this.endDateMins.getDate() + 1);
  }

  // Help Code Start Here //

  formatSendDate(date) {
    const d = new Date(date);
    // const formatted = this.datepipe.transform(d, 'yyyy-MM-dd');
    const formattedTime = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
    return formattedTime;
  }
  formatSendDateTime(Dtime) {
    const t = new Date(Dtime);
    // const formattedDateTime = this.datepipe.transform(t, 'yyyy-MM-dd h:mm');
    const year = t.getFullYear() + '-' + (t.getMonth() + 1) + '-' + t.getDate();
    const time = ' ' + t.getHours() + ':' + t.getMinutes() + ':' + t.getSeconds();
    const formattedDateTime = year + time;
    return formattedDateTime;
  }

  formatDisplayDate(dateTime) {
    if (dateTime) {
      const dateTimeData = new Date(dateTime);
      console.log(dateTimeData);
      const formattedTime = dateTimeData.getDate() + "-" + (dateTimeData.getMonth() + 1) + "-" + dateTimeData.getFullYear();
      return formattedTime;
    }
  }

  formatDisplayDateTime(dateTime) {
    const dateTimeData = new Date(dateTime);
    let hours = dateTimeData.getHours();
    let minutes = dateTimeData.getMinutes();
    let ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    let minutes2 = minutes < 10 ? '0' + minutes : minutes;
    let strTime = hours + ':' + minutes2 + ' ' + ampm;
    return strTime;
    // const seconds = new Date()
  }
  formateddate(date) {
    console.log(date);
    date = date.split(" ");
    console.log(date);
    const formatteddate = date[0];
    const formattedTime = date[1];
    console.log(formattedTime);

    const currentTime = formattedTime.split(":");

    var hours = currentTime[0];
    var minutes = currentTime[1];
    var second = currentTime[2];

    if (minutes < 10)
      minutes = "0" + minutes;

    var suffix = "AM";
    if (hours >= 12) {
      suffix = "PM";
      hours = hours - 12;
    }
    if (hours == 0) {
      hours = 12;
    }
    var current_time = hours + ":" + minutes + ":" + second + " " + suffix;

    var calldate = formatteddate + " " + current_time
    console.log("date currenttime", calldate);
    return calldate;
  }
  getHelpContent() {
    return new Promise(resolve => {
      this.http1
        .get('../../../../../../assets/help-content/addEditCourseContent.json')
        .subscribe(
          data => {
            this.helpContent = data;
            console.log('Help Array', this.helpContent);
          },
          err => {
            resolve('err');
          }
        );
    });
    // return this.helpContent;
  }

  // Help Code Ends Here //

}
