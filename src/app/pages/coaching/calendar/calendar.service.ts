import {Injectable, Inject} from '@angular/core';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppConfig} from '../../../app.module';
import { webApi } from '../../../service/webApi';
import { HttpClient } from "@angular/common/http";

@Injectable()
export class CalendarService {
  loginUserdata;
  tenantId ;
  empId: any;
  participantData: any = {};

  private _urlAddEditCall: string = webApi.domain + webApi.url.addeditcall;
  private _urlGetCallById: string = webApi.domain + webApi.url.getcallsbyempid;
  private _urlDeleteCallById: string = webApi.domain + webApi.url.deletecallbyid;
  private _urlGetTrainerByPartner: string = webApi.domain + webApi.url.getTrainerByPartner;

  constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private _http: Http,private _httpClient:HttpClient){
      //this.busy = this._http.get('...').toPromise();
      if(localStorage.getItem('LoginResData')){
        this.loginUserdata = JSON.parse(localStorage.getItem('LoginResData'));
   
       }
    this.tenantId = this.loginUserdata.data.data.tenantId;

  }

  addEditCall(param){
    return new Promise(resolve => {
      this._httpClient.post(this._urlAddEditCall, param)
      //.map(res => res.json())
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}

getCallsById(param){
    return new Promise(resolve => {
      this._httpClient.post(this._urlGetCallById, param)
      //.map(res => res.json())
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve('err');
      });
  });
}


getTrainerPartner(param){
  return new Promise(resolve => {
    this._httpClient.post(this._urlGetTrainerByPartner, param)
    //.map(res => res.json())
    .subscribe(data => {
        resolve(data);
    },
    err => {
        resolve('err');
    });
});
}

deleteCallById(param){
  return new Promise(resolve => {
    this._httpClient.post(this._urlDeleteCallById, param)
    //.map(res => res.json())
    .subscribe(data => {
        resolve(data);
    },
    err => {
        resolve('err');
    });
});
}


_errorHandler(error: Response){
  console.error(error);
  return Observable.throw(error || "Server Error")
}


}
