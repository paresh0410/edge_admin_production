import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router, NavigationStart, Routes, ActivatedRoute } from '@angular/router';
import { AddEditTeamService } from './add-edit-team/add-edit-team.service';


@Component({
  selector: 'ngx-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TeamComponent implements OnInit {

  @ViewChild('myTable') table: any;
  rows = [];
  selected = [];

  
  team:any=[
    {
        "id": 1,
        "name": "Ramsey Cummings",
        "designation": "Business Support Manager",
        "qualification":"Abc",
        "doj": "02/18/2019",
        "cost": "",
        "emprole":"",
        "mobile":9876543210,
        "email":"abc@gmail.com",
        "status":"Active",

    },
    {
        "id": 2,
        "name": "Stefanie Huff",
        "designation": "Partner",
        "mobile":9876543210,
        "email":"qwer@gmail.com",
        "status":"Active",
        "qualification":"Abc",
        "doj": "",
        "cost": "",
        "emprole":"",
    },
    {
        "id": 3,
        "name": "Mabel David",
        "designation": "Consultant",
        "mobile":5564421852,
        "email":"asdfvg@gmail.com",
        "status":"Active",
        "qualification":"Abc",
        "doj": "",
        "cost": "",
        "emprole":"",
    },
    {
        "id": 4,
        "name": "Frank Bradford",
        "designation": "Business Support Manager",
        "mobile":9900887766,
        "email":"asde@gmail.com",
        "status":"Active",
        "qualification":"Abc",
        "doj": "",
        "cost": "",
        "emprole":"",
    },
    {
        "id": 5,
        "name": "Forbes Levine",
        "designation": "Partner",
        "mobile":9876543210,
        "email":"abqwec@gmail.com",
        "status":"Active",
        "qualification":"Abc",
        "doj": "",
        "cost": "",
        "emprole":"",
    },
    {
        "id": 6,
        "name": "Santiago Mcclain",
        "designation": "Consultant",
        "mobile":9876543210,
        "email":"bvh@gmail.com",
        "status":"Active",
        "qualification":"Abc",
        "doj": "",
        "cost": "",
        "emprole":"",
    },
    {
        "id": 7,
        "name": "Merritt Booker",
        "designation": "Business Support Manager",
        "mobile":9876543210,
        "email":"acg@gmail.com",
        "status":"Active",
        "qualification":"Abc",
        "doj": "",
        "cost": "",
        "emprole":"",
    },
    {
        "id": 8,
        "name": "Oconnor Wade",
        "designation": "Partner",
        "mobile":9876543210,
        "email":"jkl@gmail.com",
        "status":"Active",
        "qualification":"Abc",
        "doj": "",
        "cost": "",
        "emprole":"",
    },
    {
        "id": 9,
        "name": "Leigh Beasley",
        "designation": "Business Support Manager",
        "mobile":9876543210,
        "email":"lmn@gmail.com",
        "status":"Active",
        "qualification":"Abc",
        "doj": "",
        "cost": "",
        "emprole":"",
    },
    {
        "id": 10,
        "name": "Johns Wood",
        "designation": "Consultant",
        "mobile":9876543210,
        "email":"xyz@gmail.com",
        "status":"Active",
        "qualification":"Abc",
        "doj": "",
        "cost": "",
        "emprole":"",
    },
    {
        "id": 11,
        "name": "Thompson Hays",
        "designation": "Partnerr",
        "mobile":8897564123,
        "email":"def@gmail.com",
        "status":"Active",
        "qualification":"Abc",
        "doj": "",
        "cost": "",
        "emprole":"",
    },
    {
        "id": 12,
        "name": "Stefanie Huff",
        "designation": "Partner",
        "mobile":9876543210,
        "email":"qwer@gmail.com",
        "status":"Active",
        "qualification":"Abc",
        "doj": "",
        "cost": "",
        "emprole":"",
    },
    {
        "id": 13,
        "name": "Mabel David",
        "designation": "Consultant",
        "mobile":5564421852,
        "email":"asdfvg@gmail.com",
        "status":"Active",
        "qualification":"Abc",
        "doj": "",
        "cost": "",
        "emprole":"",
    }];


  constructor(private router:Router,public routes:ActivatedRoute, private addeditservice: AddEditTeamService ) { 
    this.rows=this.team;
  }

  ngOnInit() {
  }


  gotoAddEditTeam(data, value){
    console.log(data);
    if(value == 0){
        var action = "ADD";
        this.addeditservice.addeditteam[0]= action;
        this.addeditservice.addeditteam[1]= '';
        this.router.navigate(['add-edit-team'],{relativeTo:this.routes});
    }
    else if( value == 1){
        var action = "EDIT";
        this.addeditservice.addeditteam[0]= action;
        this.addeditservice.addeditteam[1]= data;
        this.router.navigate(['add-edit-team'],{relativeTo:this.routes});
    }
  }

  onSelect({ selected }) {
    console.log('Select Event', selected, this.selected);

    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }
  gotoBack(){
    this.router.navigate(['../'],{relativeTo:this.routes})
  }

}
