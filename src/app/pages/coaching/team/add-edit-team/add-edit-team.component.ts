import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
// import {INgxMyDpOptions} from 'ngx-mydatepicker';
import { Router, ActivatedRoute } from '@angular/router';
import { AddEditTeamService } from './add-edit-team.service'

@Component({
  selector: 'ngx-add-edit-team',
  templateUrl: './add-edit-team.component.html',
  styleUrls: ['./add-edit-team.component.scss'],
  encapsulation: ViewEncapsulation.None

})
export class AddEditTeamComponent implements OnInit {

  roles = ['Partner', 'Consultant', 'Client']
	statuss = ['Active','Inactive']
	member : any = {};


	registerForm: FormGroup;
        submitted = false;
        
  getDataForEdit:any = [];
  addAction:boolean=false;
  constructor(private router:Router,public routes:ActivatedRoute,private formBuilder: FormBuilder,private addeditservice : AddEditTeamService) { 
    this.getDataForEdit = this.addeditservice.addeditteam;
    console.log('this.getDataForEdit',this.getDataForEdit);

    if(this.getDataForEdit[0]=="ADD"){
      this.addAction=true;
      this.member.name = '';
      this.member.dob ='';
      this.member.designation = '';
      this.member.qualification = '';
      this.member.doj ='';
      this.member.mobile = '';
      this.member.email ='';
      this.member.cost = '';
      this.member.emprole = this.roles[0];
      this.member.status = this.statuss[0];

    }else if(this.getDataForEdit[0]=="EDIT"){
      this.addAction = false;
      this.member.name = this.getDataForEdit[1].name;
      this.member.dob = this.getDataForEdit[1].dob;
      this.member.designation = this.getDataForEdit[1].designation;
      this.member.qualification = this.getDataForEdit[1].qualification;
      this.member.doj = this.getDataForEdit[1].doj;
      this.member.mobile = this.getDataForEdit[1].mobile;
      this.member.email = this.getDataForEdit[1].email;
      this.member.cost = this.getDataForEdit[1].cost;
      this.member.emprole = this.getDataForEdit[1].emprole;
      this.member.status = this.getDataForEdit[1].status;
    }
  }

  ngOnInit() {

    this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      dob: [null, Validators.required],
      designation: ['', Validators.required],
      qualification: ['', Validators.required],
      doj: [null, Validators.required],
      mobile: ['', [Validators.required,Validators.minLength(10),Validators.maxLength(10)]],
      email: ['', Validators.required],
      cost: ['', Validators.required],
      emprole: ['', Validators.required],
      status: ['', Validators.required]
  });

}
get f() { return this.registerForm.controls; }

// onSubmit(item) {
//   this.submitted = true;
  
  
//   // stop here if form is invalid
//   if (this.registerForm.invalid) {
    
//       return;

//   }else{
//     // this.addeditmemberservice.newMember=item;
//     // this.router.navigate(['../../team'],{relativeTo:this.routes});
//   }

// }

// setDate(): void {
//   let date = new Date();
//   this.registerForm.patchValue({dob: {
//   date: {
//       year: date.getFullYear(),
//       month: date.getMonth() + 1,
//       day: date.getDate()}
//   }});
// }

// clearDate(): void {
//   // Clear the date using the patchValue function
//   this.registerForm.patchValue({dob: null});
// }

gotoTeam(){
 this.router.navigate(['../../team'],{relativeTo:this.routes});
}

onSubmit(data){

  this.submitted = true;

  console.log(data);
this.member ={
  name : data.name,
  dob: data.dob,
  designation: data.designation,
  qualification: data.qualification,
  doj: data.doj,
  mobile: data.mobile,
  email: data.email,
  cost: data.cost,
  emprole: data.emprole,
  status: data.status
}

  // stop here if form is invalid
  if (this.registerForm.invalid) {
    
    return;
}

}

  }

