import { TestBed } from '@angular/core/testing';

import { AddEditTeamService } from './add-edit-team.service';

describe('AddEditTeamService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddEditTeamService = TestBed.get(AddEditTeamService);
    expect(service).toBeTruthy();
  });
});
