import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CoachingComponent } from './coaching.component';
import { TeamComponent } from './team/team.component';
import { CallMappingComponent } from './call-mapping/call-mapping.component';
// import { IntermediateCoach } from './intermediatecoach/intermediatecoach';

import { AddEditTeamComponent } from './team/add-edit-team/add-edit-team.component';
import { ParticipantsComponent } from './participants/participants.component';
import { CalendarComponent } from './calendar/calendar.component';
import { CallDetailComponent } from './call-detail/call-detail.component';

import { PartnerComponent } from './partner/partner.component';
import { AddPartnerComponent } from './partner/add-partner/add-partner.component';
import { BulkUploadCoachingComponent } from './participants/bulk-upload-coaching/bulk-upload-coaching.component';
import { AttributeSkillComponent } from './attribute-skill/attribute-skill.component';
import { AddAttributeSkillComponent } from './attribute-skill/add-attribute-skill/add-attribute-skill.component';
import { AttributeMasterComponent } from './attribute-master/attribute-master.component';
import { CallsComponent } from './calls/calls.component';
import { AddParticipantsComponent } from './participants/add-participants/add-participants.component';
import { AddAttributeMasterComponent } from './attribute-master/add-attribute-master/add-attribute-master.component';
import { CourseDetailComponent } from './call-detail/content/course-detail/course-detail.component'
import { MasterComponent } from './master/master.component';
import { FeedackTemplateComponent } from './feedack-template/feedack-template.component';
import { AssessmentTemplateComponent } from './assessment-template/assessment-template.component';
import { ObservationTemplateComponent } from './observation-template/observation-template.component';

const routes: Routes = [
  {path: '', component: CoachingComponent},
    { path: 'team', component: TeamComponent },
    { path: 'feedback-template', component: FeedackTemplateComponent },
    { path: 'assessment', component: AssessmentTemplateComponent },
    { path: 'observation', component: ObservationTemplateComponent },
    { path: 'call-mapping', component: CallMappingComponent },
    { path: 'team/add-edit-team', component: AddEditTeamComponent },
    { path: 'participants', component: ParticipantsComponent },
    { path: 'participants/calendar', component: CalendarComponent },
    { path: 'participants/calendar/call-detail', component: CallDetailComponent },
    { path: 'participants/add-edit-participants', component: AddParticipantsComponent },
    { path: 'participants/bulk-upload-coaching', component: BulkUploadCoachingComponent },
    { path: 'partner', component: PartnerComponent },
    { path: 'partner/add-partner', component: AddPartnerComponent },
    { path: 'attribute-skills', component: AttributeSkillComponent },
    { path: 'attribute-skills/add-attribute-skill', component: AddAttributeSkillComponent },
    { path: 'attribute-master', component: AttributeMasterComponent },
    { path: 'attribute-master/add-attribute-master', component: AddAttributeMasterComponent },
    { path: 'calls', component: CallsComponent },
    { path: 'participants/calendar/call-detail/content/course-details', component: CourseDetailComponent },
    { path: 'master', component: MasterComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoachingRoutingModule { }
