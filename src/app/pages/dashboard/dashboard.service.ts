
import {throwError as observableThrowError, Observable} from 'rxjs';
import {Injectable, Inject} from '@angular/core';
import {Http,Response} from '@angular/http';
import {AppConfig} from '../../app.module';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { HttpClientModule, HttpErrorResponse } from '@angular/common/http';

import { webAPIService } from '../../service/webAPIService';
import { webApi } from '../../service/webApi';


@Injectable()
export class DashboardService {
  private url: string = webApi.domain + webApi.url.get_dashboard_data;

  constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private _http: Http,private httpClient: HttpClientModule){
  }

  getDashboardData(url, param) {
    return new Promise(resolve => {
      this._http.post(url, param)
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            resolve(err);
            // if (err) {
            //   this.toastr.error(
            //     "Please check server connection",
            //     "Server Error!"
            //   );
            // }
          }
        );
    });
  }

  // getDashboardData(url, param) {
  //   return new Promise(resolve => {
  //     this.http
  //       .post(url, param)
  //       // .map(res => res.json())
  //       .subscribe(
  //         data => {
  //           resolve(data);
  //         },
  //         err => {
  //           resolve(err);
  //           if (err) {
  //             this.toastr.error(
  //               "Please check server connection",
  //               "Server Error!"
  //             );
  //           }
  //         }
  //       );
  //   });
  // }


  _errorHandler(error: Response){
    console.error(error);
    return observableThrowError(error || "Server Error")
  }
  public saveWidget(widgets) {
    localStorage.setItem('widgets', JSON.stringify(widgets));
    // this._widgetConfigurations.next(configs);
  }
  public getWidget() {
      return localStorage.getItem('widgets');
      // this._widgetConfigurations.next(configs);
  }
}


 // getcompletion(user){
  //   let url:any = `${this.config.FINAL_URL}`+this._url1;
  //   return this._http.post(url,user)
  //     .map((response:Response) => response.json())
  //     .catch(this._errorHandler);
  // }
  //  getgrades(){
  //   let url:any = `${this.config.FINAL_URL}`+this._url2;
  //   return this._http.post(url,{})
  //     .map((response:Response) => response.json())
  //     .catch(this._errorHandler);
  // }
  //  getusers(){
  //   let url:any = `${this.config.FINAL_URL}`+this._url3;
  //   return this._http.post(url,{})
  //     .map((response:Response) => response.json())
  //     .catch(this._errorHandler);
  // }