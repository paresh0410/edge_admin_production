import { Component, OnDestroy, OnInit, Input, ViewEncapsulation, ChangeDetectorRef } from "@angular/core";
import { NbThemeService, NbColorHelper } from "@nebular/theme";
import { NumberSymbol } from '@angular/common';

import { ConfigService } from "./config-service";
import {
  Router,
  ActivatedRoute
} from "@angular/router";
import { DashboardService } from "./dashboard.service";
import { BaseChartDirective } from "ng2-charts/ng2-charts";
import "../../../assets/style/style.scss";
import { webApi } from "../../service/webApi";
import { webAPIService } from "../../service/webAPIService";

interface CardSettings {
  title: string;
  iconClass: string;
  type: string;
}

@Component({
  selector: "ngx-dashboard",
  styleUrls: ["./dashboard.component.scss"],
  templateUrl: "./dashboard.component.html",
  providers: [ConfigService, DashboardService],
  encapsulation: ViewEncapsulation.None
})

export class DashboardComponent implements OnInit, OnDestroy {
  options: any;
  themeSubscription: any;

  /********************** Stacked Bar **********************/
  multi: any;

  completeCourseData: any = [];

  view: any[] = [700, 300];
  @Input() noBarWhenZero: boolean = false;
  // barchart data
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  // showGridLines = false;
  showLegend = true;
  legendTitle = "";
  showXAxisLabel = false;
  showYAxisLabel = false;
  skeleton = false;
  colorScheme = {
    // domain: ["#5a98d8", "#076ba1", "#C7B42C", "#AAAAAA"]
    domain : [],
  };

  year: any[] = [
    {
      'year': '2014-2015',
    },
    {
      'year': '2014-2015',
    },
    {
      'year': '2014-2015',
    },
    {
      'year': '2014-2015',
    },
    {
      'year': '2014-2015',
    },
    {
      'year': '2014-2015',
    },
    {
      'year': '2014-2015',
    },
    {
      'year': '2014-2015',
    },
    {
      'year': '2014-2015',
    },
    {
      'year': '2014-2015',
    },
    {
      'year': '2014-2015',
    },
  ];

  //   view : any;
  //   onResize(event) {
  //     this.view = [event.target.innerWidth / 2, 300];
  // }
  /********************** Stacked Bar **********************/

  /*********************** Most popular course ******************************/
  mostPopularCourses: any = [];

  active_user: any;
  Total_Course_Completion_Percentage: any;
  learning_Hours: any;
  total_courses: any;

  line_chart_data: any;
  multi_line_chart: any = [];
  loginUserdata: any = [];
  yearArray: any = [];

  d = new Date();
  fromYear = this.d.getFullYear();
  toYear = this.fromYear + 1;
  public widgetList = widgetList;
  widgetArray: any = [
    {
      wname: "ngx-active-user",
      icon: "fa fa-users",
      name: "Active users",
    },
    {
      wname: "ngx-total-user",
      icon: "fas fa-user-circle",
      name: "Active users for this month",
    },
    {
      wname: "ngx-active-user",
      icon: "fas fa-book",
      name: "Total Courses",
    },
    {
      wname: "ngx-total-user",
      icon: "fas fa-percentage",
      name: "Courses Completed",
    },
  ];
  popupConfig = {
    enabled: true,
    width: '300px !important',
  }; 

  constructor(
    private theme: NbThemeService,
    private themeService: NbThemeService,
    protected service: DashboardService,
    private router: Router,
    protected webApiService: webAPIService,
    public routes: ActivatedRoute,
    public cdf: ChangeDetectorRef
  ) {
    this.loginUserdata = JSON.parse(localStorage.getItem("LoginResData"));
    // this.view = [innerWidth / 1, 400];

    var years: any = [
      {
        fromYear: this.fromYear,
        toYear: this.toYear,
      },
    ];
    let fromyr = this.fromYear;
    let toyr = this.toYear;
    for (var i = 0; i < 9; i++) {
      years.push({ fromYear: --fromyr, toYear: --toyr });
    }
    console.log('current year : ' + this.fromYear + ' - to year' + this.toYear);
    this.yearArray = years;
    console.log('Years array', years);
    const themecl = JSON.parse(localStorage.getItem("theme"));
    this.colorScheme.domain = ["#5a98d8", themecl.commonColor , "#C7B42C", "#AAAAAA"];

  }

  dataTable: any = [];

  errorMsg: any;
  dashData: any = [];
  dashboard_data_array: any = [];
  completeCourseDataForBarChart: any = [];

  showCalender = false;
  ngOnInit() {
    this.getDashData(this.fromYear, this.toYear);
    this.onLoad();
  }
  ngOnDestroy(): void {
    if (this.themeSubscription) {
      this.themeSubscription.unsubscribe();
    }
  }

  getYear(data) {
    // this.fromYear = data.fromYear;
    // this.toYear = data.toYear;
    this.fromYear = data.fromYear;
    this.toYear = data.toYear;
    console.log('dates clicked: ' + data.fromYear + ' , ' + data.toYear);
    this.getDashData(data.fromYear, data.toYear);
    this.showCalender = false;
  }
  showBarChart: boolean = false;

  showLineChart: boolean = false;
  yScaleMin_line = 0;
  xScaleMin_line = 0;
  autoScale_line = false;
  showXAxis_line = true;
  showYAxis_line = true;
  // gradient_line = false;
  showGridLines_line = true;
  showLegend_line = true;
  legendTitle_line = "";
  showXAxisLabel_line = false;
  showYAxisLabel_line = false;
  showData = false;
  showDataLabel = true;

  getDashData(fromYr, toYr) {
    this.showLineChart = false;
    this.showBarChart = false;
    this.multi = [];
    let url = webApi.domain + webApi.url.get_dashboard_data;
    let param = {
      tId: this.loginUserdata.data.data.tenantId,
      frYear: fromYr,     // current year initially
      toYear: toYr,       // to year
    };
    // console.log('dates passed as param: ' + fromYr + ' , ' + toYr);

    this.webApiService.getService(url, param).then(
      rescompData => {

        this.dashData = rescompData["data"];    // dashboard data recieved 
        this.showData = false;
        // console.log("Dashboard data here >> ", this.dashData);

        try {
          if (this.dashData) {
            this.active_user = this.dashData.active_user;     //active user card
            this.total_courses = this.dashData.total_courses;   // total courses card
            this.Total_Course_Completion_Percentage = this.dashData.Total_Course_Completion_Percentage;
            // total course completed in percent card

            this.learning_Hours = this.dashData.learning_Hours;   // total users card
            this.completeCourseDataForBarChart = this.dashData['Enrolments_vs_completion'];
            // enrolment vs completion data seperated from dashboard data

            this.mostPopularCourses = this.dashData['Popular_courses_by_enrolment'];
            // popular courses seperated from dashboard data
            // console.log('popular course data', this.mostPopularCourses);   // most popular courses array

            this.dataTable = this.dashData['total_vs_active_users'];
            // total vs active users array seperated from dashboard data

            // console.log(this.completeCourseData);
            if (this.completeCourseDataForBarChart.length !== 0) {
              // if(this.multi.length === 0){

              //setInterval((function(){
              this.get_barChart(this.completeCourseDataForBarChart);

              this.showBarChart = true;
              //this.cdf.detectChanges();
              //}).bind(this), 1000);

              // } else {
              //   this.showBarChart = false;
              // }
            } else {
              this.showBarChart = false;
            }
            // this.data = {
            //   datasets: [],
            //   labels: [],
            //   xLabels: [],
            //   yLabels: [],
            // };

            // this.data = { };
            this.get_LineChart_data();  // call line chart function after creating barchart chart data
            console.log("bar chart data", this.multi);
          }
          this.skeleton = true;
        }
        catch (e) {
          this.skeleton = true;
          console.log(e);
        }

      },
      resUserError => {
        this.errorMsg = resUserError;
        this.showData = true;
        this.skeleton = true;
      }
    );
  }

  get_barChart(data) {
    data.forEach((element) => {
      const barchart_data = {};
      // if (element['faMonth']) {
      barchart_data['name'] = element['faMonth'];     //add month in array object
      barchart_data['series'] = [
        {
          name: 'Completed courses',                  //add 1st object of course completed
          value: element['courseComp'],
        },
        {
          name: 'Enrolled Courses',                   //add 2nd object of course enrolled
          value: element['enrolments'],
        },
      ];
      this.multi.push(barchart_data);                 //push created object into an array
      // } 
    });
  }



  get_LineChart_data() {
    this.line_chart_data = {};
    let labels: any = [];
    let activeUsers: any = [];
    let totalUsers: any = [];
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {
      const colors: any = config.variables;
      const chartjs: any = config.variables.chartjs;
      if (this.dataTable.length !== 0) {
        this.dataTable.forEach(element => {
          if (element['faMonth']) {
            labels.push(element['faMonth']);
            activeUsers.push(element['activeUsers']);
            totalUsers.push(element['totalUsers']);
          }
        });

        // console.log('this.labels',this.labels);
        // console.log('this.activeUsers',this.activeUsers);
        // console.log('this.totalUsers',this.totalUsers);

        this.line_chart_data = {
          labels: labels,
          datasets: [
            {
              data: activeUsers,
              label: "Active Users",
              backgroundColor: NbColorHelper.hexToRgbA(colors.primary, 0.1),
              borderColor: colors.primary
            },
            {
              data: totalUsers,
              label: "Total Users",
              backgroundColor: NbColorHelper.hexToRgbA(colors.danger, 0.1),
              borderColor: colors.danger
            }
          ]
        };
        console.log('data Line Chart===>', this.line_chart_data);
        this.options = {
          responsive: true,
          maintainAspectRatio: false,
          scales: {
            xAxes: [
              {
                gridLines: {
                  display: false,
                  color: chartjs.axisLineColor
                },
                ticks: {
                  fontColor: chartjs.textColor
                }
              }
            ],
            yAxes: [
              {
                gridLines: {
                  display: false,
                  color: chartjs.axisLineColor
                },
                ticks: {
                  fontColor: chartjs.textColor
                }
              }
            ]
          },
          legend: {
            labels: {
              fontColor: chartjs.textColor
            }
          }
        };
        this.showLineChart = true;
      }
      else {
        this.showLineChart = false;
      }
    });

  }


  /************************************************************************************** */

  // configuration;
  // columns = [
  //   { key: 'phone', title: 'Phone' },
  //   { key: 'age', title: 'Age' },
  //   { key: 'company', title: 'Company' },
  //   { key: 'name', title: 'Name' },
  //   { key: 'isActive', title: 'STATUS' },
  // ];

  // p:any;
  // q:any;
  // columnsCopy = [];

  // checked = new Set(['phone', 'age', 'company', 'name', 'isActive']);

  // data = [{
  //   phone: '+1 (934) 551-2224',
  //   age: 20,
  //   address: { street: 'North street', number: 12 },
  //   company: 'ZILLANET',
  //   name: 'Valentine Webb',
  //   isActive: false,
  // }, {
  //   phone: '+1 (948) 460-3627',
  //   age: 31,
  //   address: { street: 'South street', number: 12 },
  //   company: 'KNOWLYSIS',
  //   name: 'Heidi Duncan',
  //   isActive: true,
  // }];

  // newData:any = [];

  // private alive = true;

  // lightCard: CardSettings = {
  //   title: 'Light',
  //   iconClass: 'nb-lightbulb',
  //   type: 'primary',
  // };
  // rollerShadesCard: CardSettings = {
  //   title: 'Roller Shades',
  //   iconClass: 'nb-roller-shades',
  //   type: 'success',
  // };
  // wirelessAudioCard: CardSettings = {
  //   title: 'Wireless Audio',
  //   iconClass: 'nb-audio',
  //   type: 'info',
  // };
  // coffeeMakerCard: CardSettings = {
  //   title: 'Coffee Maker',
  //   iconClass: 'nb-coffee-maker',
  //   type: 'warning',
  // };

  // statusCards: string;

  // commonStatusCardsSet: CardSettings[] = [
  //   this.lightCard,
  //   this.rollerShadesCard,
  //   this.wirelessAudioCard,
  //   this.coffeeMakerCard,
  // ];

  // statusCardsByThemes: {
  //   default: CardSettings[];
  //   cosmic: CardSettings[];
  //   corporate: CardSettings[];
  // } = {
  //   default: this.commonStatusCardsSet,
  //   cosmic: this.commonStatusCardsSet,
  //   corporate: [
  //     {
  //       ...this.lightCard,
  //       type: 'warning',
  //     },
  //     {
  //       ...this.rollerShadesCard,
  //       type: 'primary',
  //     },
  //     {
  //       ...this.wirelessAudioCard,
  //       type: 'danger',
  //     },
  //     {
  //       ...this.coffeeMakerCard,
  //       type: 'secondary',
  //     },
  //   ],
  // };

  //   query: string = '';
  //   public getData;

  //   userTableData:any = [];
  //   compleation:any=[];
  //   grades:any=[];
  //   users:any=[];

  //   source: LocalDataSource = new LocalDataSource();
  //   isDesc :any ;
  //   column :any ;
  //   search :any = {};
  //   search1 :any = {};
  //   search2 :any = {};

  //   errorMsg: string;
  //   loader :any;
  //   charts:any;
  //   doughnutChartLabels :any;
  //   doughnutChartData : any;
  //   doughnutChartType:any;

  //   // this.loader = true;

  //   this.themeService.getJsTheme()
  //     .pipe(takeWhile(() => this.alive))
  //     .subscribe(theme => {
  //       this.statusCards = this.statusCardsByThemes[theme.name];
  //   });

  //   // this.columns = this.columns;
  //   this.columnsCopy = this.columns;
  //   this.newData = this.data;

  //   var user={
  //     userid:1
  //   }
  //   // this.service.getcompletion(user)
  //   // .subscribe(rescompData => {
  //   //   this.loader =false;
  //   //   this.compleation = rescompData.data[0];
  //   //   console.log(this.compleation , 'this.compleation')
  //   // },
  //   // resUserError => {
  //   //   this.loader =false;
  //   //   this.errorMsg = resUserError
  //   // });

  //   // this.service.getgrades()
  //   // .subscribe(resgradeData => {
  //   //   this.loader =false;
  //   //   this.grades = resgradeData.data[0];
  //   //   for(let i = 0; i < this.grades.length; i++)
  //   //      {
  //   //        this.grades[i].alldata=[];
  //   //       var setData=[];

  //   //        setData.push(this.grades[i].g25)
  //   //        setData.push(this.grades[i].g50)
  //   //        setData.push(this.grades[i].g100)

  //   //        this.grades[i].alldata=setData;
  //   //    }
  //   //   console.log(this.grades , 'this.grades')
  //   // },
  //   // resUserError => {
  //   //   this.loader =false;
  //   //   this.errorMsg = resUserError
  //   // });

  //   //  this.service.getusers()
  //   // .subscribe(resUserData => {
  //   //   this.loader =false;
  //   //   this.users = resUserData.data[0];
  //   //   console.log(this.users , 'this.users')
  //   // },
  //   // resUserError => {
  //   //   this.loader =false;
  //   //   this.errorMsg = resUserError
  //   // });

  //   this.doughnutChartLabels= ['<25', '<50', '<100'];
  //   this.doughnutChartData=  this.newData;
  //   this.doughnutChartType = 'doughnut';
  // }

  // ngOnInit() {
  //   this.configuration = ConfigService.config;
  //   this.data = this.data;
  // }

  // ngOnDestroy() {
  //   this.alive = false;
  // }

  // toggle(name: string, value: boolean): void {
  //   value ? this.checked.add(name) : this.checked.delete(name);
  //   this.exportDataGenrate(name,value);
  //   this.columns = this.columnsCopy.filter(column => this.checked.has(column.key));
  //   // for(var i=0; i<this.data.length; i++){}
  //     // if(name == this.data[i][name] && value == false){
  //     // }
  //     //this.newData.push(this.data[i]);
  // }

  // // exportToExcel() {
  // //   try {
  // //     json2excel({
  // //       data: this.data,
  // //       name: 'user-info-data',
  // //       formateDate: 'yyyy/mm/dd'
  // //     });
  // //   } catch (e) {
  // //     console.error('export error');
  // //   }
  // // }

  // // exportToText() {
  // //   var a = document.createElement("a");
  // //   var file = new Blob([this.data], { type: 'text/plain' });
  // //   a.href = URL.createObjectURL(file);
  // //   a.download = 'json.txt';
  // //   a.click();
  // // }

  // exportDataGenrate(name,value){
  //   value ? this.checked.add(name) : this.checked.delete(name);
  //   this.data = this.newData.filter(column => this.checked.has(column.name));
  // }
  // exportToCSV() {
  //   const data = this.newData;
  //   let csvContent = 'data:text/csv;charset=utf-8,';
  //   let dataString = '';
  //   const x: Array<any> = [];
  //   const keys = Object.keys(this.data[0]);
  //   data.forEach((row, index) => {
  //     x[index] = [];
  //     keys.forEach((i) => {
  //       if (row.hasOwnProperty(i)) {
  //         if (typeof row[i] === 'object') {
  //           row[i] = 'Object'; // so far just change object to "Object" string
  //         }
  //         x[index].push(row[i]);
  //       }
  //     });
  //   });

  //   csvContent += keys + '\n';
  //   x.forEach((row, index) => {
  //     dataString = row.join(',');
  //     csvContent += index < data.length ? dataString + '\n' : dataString;
  //   });
  //   const encodedUri = encodeURI(csvContent);
  //   const link = document.createElement('a');
  //   link.setAttribute('href', encodedUri);
  //   link.setAttribute('download', 'my_data.csv');

  //   link.click();
  // }

  // // newData:any=[];
  // setData:any=[];

  //    comp(i)
  //   {
  //       var styles={
  //           'width':i +'%',
  //           'height':'20px',
  //           'background-color':'#51c9e8'
  //       }
  //       return styles
  //   }

  //  incomp(i)
  //   {
  //       var styles={
  //           'width':i +'%',
  //           'height':'20px',
  //           'background-color':'#efc5c5'
  //       }
  //       return styles
  //   }

  // g100(i)
  //   {
  //       var styles={
  //           'width':i +'%',
  //           'height':'14px',
  //           'background-color':'#51c9e8',
  //           'margin': '2px 0px'
  //       }
  //       return styles
  //   }

  //   g25(i)
  //   {
  //       var styles={
  //           'width':i +'%',
  //           'height':'14px',
  //           'background-color':'#e87051',
  //           'margin': '2px 0px'
  //       }
  //       return styles
  //   }
  //  g50(i)
  //   {
  //       var styles={
  //           'width':i +'%',
  //           'height':'14px',
  //           'background-color':'#e8bc51',
  //           'margin-bottom': '2px'
  //       }
  //       return styles
  //   }

  // // events
  // public chartClicked(e:any):void {
  //   console.log(e);
  // }

  // public chartHovered(e:any):void {
  //   console.log(e);
  // }

  // Clear(){
  //   this.search = {};
  // }
  // Clear1(){
  //     this.search1 = {};
  // }
  // Clear2(){
  //     this.search2 = {};
  // }

  // key: string; //set default
  //    reverse: boolean = false;
  //    sort(key){
  //      this.key = key;
  //      this.reverse = !this.reverse;
  //    }

  //   onDeleteConfirm(event) {
  //       if (window.confirm('Are you sure you want to delete?')) {
  //         event.confirm.resolve();
  //       } else {
  //         event.confirm.reject();
  //       }
  //   }

  getCalender() {
    this.showCalender = true;
  }
  closeModal() {
    this.showCalender = false;
  }

  tempFunction() {
    this.router.navigate(['/CategoryComponentCompon/ent'], { relativeTo: this.routes });
  }

  /**
   * Grid Stack
   */
  public widgets: {
    x?: number,
    y?: number,
    width?: number,
    height?: number,
    id: NumberSymbol,
    widgetId: any
}[] = [];

  public onWidgetGridChangeNew($event: any) {
    // let index;
    // $event.map((element, key) => {
    //     if (element._temporary) {
    //         const widgetId = element.widgetId || 'app-widget1';
    //         this.addWidgetDyanamic(widgetId);
    //         index = key;
    //     }
    // });
    // if (String(index) != undefined && String(index) == '') {
    //     $event.splice(index, 1);
    // }
    const newConfigs = this.widgets.map(wc => {
        // tslint:disable-next-line:radix
        const id = wc.id;
        const item = $event.find(i => String(i.id).toLowerCase() === String(id).toLowerCase());
        if (!item) {
            // return wc;
            // if ($event.length > this.widgets.length) {
            //     const newItem = $event[0];
            //     const widgetId = newItem.widgetId || 'app-widget1';
            //     this.addWidgetDyanamic(widgetId);
            // }
            // item = newItem;
            return wc;
        }

        return {
            ...wc,
            x: item.x,
            y: item.y,
            width: item.width,
            height: item.height,
            id: wc.id,
            widgetId: wc.widgetId
        };
    });

    this.service.saveWidget(newConfigs);
}
public onLoad() {
  this.widgets = [];
  const item = this.service.getWidget();
  const localWidget = item != null ? JSON.parse(item) : [];
  if (localWidget.length > 0) {
      dynamicWidgetId = localWidget.length;
      localWidget.forEach((value, key) => {
          this.widgets.push({
              width: 1,
              height: 1,
              id: value.id,
              widgetId: value.widgetId,
              x: value.x,
              y: value.y
          });
      });
  } else {
      dynamicWidgetId = 0;
  }
}
public onAdded(event) {
  console.log('Added - ', event);
  if (event && event.length > 0 && event[0]._temporary) {
      const widgetId = event[0].el.context.dataset.gsId;
      this.addWidgetDyanamic(widgetId);
      // this.cdf.detectChanges();
      event[0]._grid.batchUpdate();
  }
}
public removeWidget(item) {
  const widgetIndex = this.widgets.findIndex(wc => wc.id == item.id);
  this.widgets.splice(widgetIndex, 1);
}
public addWidgetDyanamic(id) {
  const element = this.getWidget(id);
  this.widgets.push({
      width: element.width,
      height: element.height,
      id: dynamicWidgetId++,
      widgetId: id
  });
}
public getWidget(widgetId) {
  return this.widgetList.find(element => element.widgetId === widgetId);
}
}
const widgetList = [{
  widgetId: 'ngx-active-user',
  id: 1,
  width: 1,
  height: 2,
  enabled: true,
  x: 0
}, {
  widgetId: 'ngx-total-user',
  id: 2,
  width: 1,
  height: 2,
  enabled: true,
  x: 1
}];

let dynamicWidgetId = widgetList.length + 1;
