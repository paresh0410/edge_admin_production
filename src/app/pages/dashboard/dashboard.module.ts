import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxEchartsModule } from 'ngx-echarts';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ChartModule } from 'angular2-chartjs';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { ThemeModule } from '../../@theme/theme.module';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { DashboardComponent } from './dashboard.component';
// import { DashboardService } from './dashboard.service';
import { ComponentModule } from '../../component/component.module';
// import { StatusCardComponent } from './status-card/status-card.component';
// import { ContactsComponent } from './contacts/contacts.component';
// import { RoomsComponent } from './rooms/rooms.component';
// import { RoomSelectorComponent } from './rooms/room-selector/room-selector.component';
// import { TemperatureComponent } from './temperature/temperature.component';
// import { TemperatureDraggerComponent } from './temperature/temperature-dragger/temperature-dragger.component';
// import { TeamComponent } from './team/team.component';
// import { KittenComponent } from './kitten/kitten.component';
// import { SecurityCamerasComponent } from './security-cameras/security-cameras.component';
// import { ElectricityComponent } from './electricity/electricity.component';
// import { ElectricityChartComponent } from './electricity/electricity-chart/electricity-chart.component';
// import { WeatherComponent } from './weather/weather.component';
// import { SolarComponent } from './solar/solar.component';
// import { PlayerComponent } from './rooms/player/player.component';
// import { TrafficComponent } from './traffic/traffic.component';
// import { TrafficChartComponent } from './traffic/traffic-chart.component';

import { TableModule } from 'ngx-easy-table';

import { ChartsModule } from 'ng2-charts';
import { NgxPaginationModule } from 'ngx-pagination';
import { FilterPipeModule  } from 'ngx-filter-pipe';
import { OrderModule  } from 'ngx-order-pipe';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { NgxContentLoadingModule } from 'ngx-content-loading';
// import {BrowserModule} from '@angular/platform-browser';
// import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


@NgModule({
  imports: [
    CommonModule,
    ThemeModule,
    // BrowserModule,
    // BrowserAnimationsModule,
    NgxChartsModule,
    NgxEchartsModule,
    NgxChartsModule,
    ChartModule,
    TableModule,
    ChartsModule,
    NgxPaginationModule,
    FilterPipeModule,
    OrderModule,
    NgxMyDatePickerModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    NgxSkeletonLoaderModule,
    NgxContentLoadingModule,
    ComponentModule,
    // GridstackModule,
  ],
  declarations: [
    DashboardComponent,
    // StatusCardComponent,
    // TemperatureDraggerComponent,
    // ContactsComponent,
    // RoomSelectorComponent,
    // TemperatureComponent,
    // RoomsComponent,
    // TeamComponent,
    // KittenComponent,
    // SecurityCamerasComponent,
    // ElectricityComponent,
    // ElectricityChartComponent,
    // WeatherComponent,
    // PlayerComponent,
    // SolarComponent,
    // TrafficComponent,
    // TrafficChartComponent,
  ],
  schemas: [ 
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA 
  ]
})
export class DashboardModule { }
