import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DigitalAssetComponent } from './digital-asset.component';

describe('DigitalAssetComponent', () => {
  let component: DigitalAssetComponent;
  let fixture: ComponentFixture<DigitalAssetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DigitalAssetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DigitalAssetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
