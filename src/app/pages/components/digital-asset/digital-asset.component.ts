import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { ContextMenuComponent } from 'ngx-contextmenu';

@Component({
  selector: 'app-digital-asset',
  templateUrl: './digital-asset.component.html',
  styleUrls: ['./digital-asset.component.scss']
})
export class DigitalAssetComponent implements OnInit {
  // @ViewChild('ContextMenuComponent') public basicMenu: ContextMenuComponent;
  fileFolder: boolean = true;
  activeFolder: boolean = false;
  activeTab: boolean = false;
  treeFolder: boolean = false;
  view: boolean = false;
  @Input() damTabs;  

  // tabs: any = [
  //   {
  //     name: "All Asset",
  //   },
  //   {
  //     name: "My Asset",
  //   },
  //   {
  //     name: "Share with me",
  //   },
  // ]
  tree: any = [
    {
      name: "Market",
      children: [
        {
          name: "equity",
          children: []
        },
        {
          name: "commodities",
          children: [
            {
              name: "F&o",
              children: [
                {
                  name: "lorem",
                  children: []
                },
                {
                  name: "ipsum",
                  children: []
                },
              ]
            }
          ]
        },
      ]
    },
    {
      name: "category",
      children: [
        {
          name: "Admin",
          children: []
        },
        {
          name: "portal",
          children: [
            {
              name: "app",
              children: [
                {
                  name: "android",
                  children: []
                },
                {
                  name: "ios",
                  children: []
                },
              ]
            }
          ]
        },
      ]
    },
    {
      name: "Web Application Framework",
      children: [
        {
          name: "Ruby on Rails",
          children: [
            {
              name: "Ruby",
              children: [
                {
                  name: "Python",
                  children: [
                    {
                      name: "JavaScript",
                      children: [
                        {
                          name: "C#"
                        },
                        {
                          name: "C ++"
                        },
                      ]
                    },
                  ]
                },
                {
                  name: "Django "
                },
              ]
            }
          ]
        },
        {
          name: "Django",
          children: [
            {
              name: "Python"
            },
            {
              name: "Django "
            },
          ]
        },
        {
          name: "Angular",
          children: [
            {
              name: "JavaScript"
            },
          ]
        },
        {
          name: "ASP.NET",
          children: [
            {
              name: "C#"
            },
          ]
        },
        {
          name: "Metor"
        },
        {
          name: "Laravel"
        },
        {
          name: "Express"
        },
      ]
    },
  ]
  folder: any = [
    {
      name: "upper management discussion",
    },
    {
      name: "digital Manager discussion",
    },
    {
      name: "company discussion asfjh kj",
    },
    {
      name: "product jhsaf kjas discussion",
    },
    {
      name: "team flow kwekjr jj 12398 ",
    },
    {
      name: "online 12498b489",
    },
    {
      name: "timesheet klajsd 83274b hg",
    },
  ]
  details: any = [
    {
      name: "Details",
    },
    {
      name: "Activity",
    },
  ]
  context: any = [
    {
      name: "View Details",
      icon: "fas fa-info-circle",
    },
    {
      name: "Edit Folder",
      icon: "fa fa-folder",
    },
    {
      name: "Disable",
      icon: "fa fa-eye-slash",
    },
  ]
  constructor() { 
    console.log(this.damTabs,"tabs")
  }

  ngOnInit() {
  }

  rightClick(event) {
    console.log(event);
  }

  singleClick(item) {
    this.activeFolder = !this.activeFolder;
  }
  
  doubleClick() {
    this.view = !this.view;
  }

  showFolder(item) {
    item['expand'] = !item['expand'];
  }

  tabActive() {
    this.activeTab = !this.activeTab;
  }

}
