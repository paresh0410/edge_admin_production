import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy, OnChanges, SimpleChanges } from '@angular/core';
import { SuubHeader } from '../models/subheader.model';
import { PreonContentService } from '../../plan/preoncourses/preoncontent/preoncontent.service';
import {
  Router,
} from '@angular/router';
@Component({
  selector: 'app-subheader',
  templateUrl: './subheader.component.html',
  styleUrls: ['./subheader.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SubheaderComponent implements OnInit {
  @Input() isAdd: boolean = true;
  @Input() user: boolean = true;
  @Input() isSave = true;
  @Input() parentcat;
  @Output() btntwo = new EventEmitter<any>();
  @Output() btnthree = new EventEmitter<any>();
  @Output() btnfour = new EventEmitter<any>();
  @Output() btnfive = new EventEmitter<any>();
  @Output() btnsix = new EventEmitter<any>();
  @Output() btnseven = new EventEmitter<any>();
  @Output() btneight = new EventEmitter<any>();
  @Output() btnine = new EventEmitter<any>();
  @Output() btnten = new EventEmitter<any>();
  @Output() btneleven = new EventEmitter<any>();
  @Output() btnone = new EventEmitter<any>();
  @Output() addbtn = new EventEmitter<any>();
  @Output() inputsearch = new EventEmitter<any>();
  @Output() drpdown1 = new EventEmitter<any>();
  @Output() drpdown2 = new EventEmitter<any>();
  @Output() backClick = new EventEmitter<any>();
  @Output() searchClick = new EventEmitter<any>();
  @Output() dropdClick = new EventEmitter<any>();
  @Output() drop_1 = new EventEmitter<any>();
  @Output() drop_2 = new EventEmitter<any>();
  @Output() Clear = new EventEmitter<any>();
  @Output() dropsearch = new EventEmitter<any>();
  @Output() selectdropdown = new EventEmitter<any>();
  @Output() comeBack = new EventEmitter<any>();
  @Output() sameComp = new EventEmitter<any>();
  @Output() filterClick = new EventEmitter<any>();
  @Output() filterReportSecondary = new EventEmitter<any>();
  @Output() filterReportPrimary = new EventEmitter<any>();

  @Input() headerConfig: SuubHeader = {
    title: '',
    // suubheader: '',
    searchtext: '',
    placeHolder: '',
    btnsSearch: false,
    searchBar: false,
    catDropdown: false,
    quesDrop: false,
    // showSearch: false,
    id: '',
    optionValue: '',
    dropId: '',
    dropName: '',
    dropdownlabel: '',
    drplabelshow: false,
    drpName1: '',
    drpName2: '',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: '',
    btnName3: '',
    btnName4: '',
    btnName5: '',
    btnName6: '',
    btnName7: '',
    btnName8: '',
    btnName9: '',
    btnName10: '',
    btnName11: '',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnName4show: false,
    btnName5show: false,
    btnName6show: false,
    btnName7show: false,
    btnName8show: false,
    btnName9show: false,
    btnName10show: false,
    btnName11show: false,
    btnBackshow: false,
    isSame: false,
    filter: false,
    dropdownlist: [],
    showBreadcrumb: false,
    breadCrumbList: [],
    filterConfig: {
      primary: false,
      primaryText: '',
      secondary: false,
      secondaryText: '',
      defaultText: 'Filter',
  },
  };
  showSearch: boolean = false;
  showlist: boolean = false;
  page: any = '';
  isShow: boolean = false
  showlist1: boolean = false;

  constructor(private passService: PreonContentService, private router: Router) { }

  ngOnInit() {

  }

  dropdown() {
    console.log("dropdown is clicked")
    this.showlist = !this.showlist;
    // this.showlist1 = false
    this.dropdClick.emit();
  }
  drop1() {
    console.log("dropdown is clicked")
    this.showlist1 = !this.showlist1;
    // this.showlist = false

    // this.dropdClick.emit();
  }
  out(){
    // this.showlist1 = false
    this.showlist = false
  }
  out1(){
    this.showlist1 = false

  }
  dropdown_1(){
    this.drop_1.emit()
  }
  dropdown_2(){
    this.drop_2.emit()

  }
  search() {
    this.showSearch = true;
    this.searchClick.emit();
  }

  back(back: any) {
    this.backClick.emit(back)
  }

  dropdown1() {
    this.drpdown1.emit();
  }

  dropdown2() {
    this.drpdown2.emit();
  }

  inputSearch(event) {
    this.inputsearch.emit(event)
  }

  add() {
    this.addbtn.emit()
  }

  btn1() {
    this.btnone.emit()
  }

  btn2() {
    this.btntwo.emit()
  }
  btn3() {
    this.btnthree.emit()
  }
  btn4() {
    this.btnfour.emit()
  }
  btn5() {
    this.btnfive.emit()
  }

  btn6() {
    this.btnsix.emit()
  }
  btn7() {
    this.btnseven.emit()
  }
  btn8() {
    this.btneight.emit()
  }
  btn9() {
    this.btnine.emit()
  }
  btn10() {
    this.btnten.emit()
  }
  btn11() {
    this.btneleven.emit()
  }

  selectList(list) {
    this.selectdropdown.emit(list);
  }

  clear() {
    var event: any = {}
    // console.log(this.headerConfig.searchtext, "search Text");
    this.showSearch = false;
    this.Clear.emit();
    this.headerConfig.searchtext = '';

  }
  doropserch(event, id) {
    var param = {
      event: event,
      id: id
    }

    this.dropsearch.emit(param)
  }
  questypeSelect(data) {
    this.dropsearch.emit(data)
  }
  ngOnChanges(changes: SimpleChanges): void {
    // console.log('changes ====================>>>>>>>>>>>', changes);

  }

  goToPage(data, index) {
    console.log('breadcrumb data ==>', data);
    this.router.navigate([data.navigationPath]);
    if (this.headerConfig.isSame || data.sameComp == true) {
      // this.back([data.navigationPath]);
      data.id['index'] = index;
      console.log("index", data.id['index'])
      this.comeBack.emit(data.id);
    }
    if (data.isSame == true || data.sameComp == false) {
      this.sameComp.emit();
    }
  }

  openfilter() {
    this.filterClick.emit();
  }
  openPrimaryFilter() {
    this.filterReportPrimary.emit();
  }
  openSecondaryFilter() {
    this.filterReportSecondary.emit();
  }
}
