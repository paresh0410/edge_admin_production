import {
  Component,
  OnInit,
  Input,
  EventEmitter,
  Output,
  ViewChild,
  OnChanges,
  SimpleChanges,
} from "@angular/core";
import { AssetService } from "../../dam/asset/asset.service";
import { Router } from "@angular/router";
import { FolderFileComponent } from "../../dam/folder-file/folder-file.component";

@Component({
  selector: "app-tree",
  templateUrl: "./tree.component.html",
  styleUrls: ["./tree.component.scss"],
})
export class TreeComponent implements OnInit, OnChanges {
  @Input() trees;
  @Input() selectedItem;
  @Input() inputTab;

  @Input() valueChanged;
  @Input() showCurrentSelected = false;

  @Input() showCheckedBox = false;

  @Output() getFolders = new EventEmitter<any>();
  @Output() openMainFolder = new EventEmitter<any>();
  @Output() bread = new EventEmitter<any>();

  @Output() getCheckedFolder = new EventEmitter<any>();

  @ViewChild("Input") file: FolderFileComponent;

  demTree: any = [];

  constructor(private assetservice: AssetService, private router: Router) {}

  ngOnInit() {
    if(this.trees){
      for (let i = 0; i < this.trees.length; i++) {
        this.trees[i]["children"] = [];
        this.trees[i]["showActive"] = false;
        // this.trees[i]["checkBoxChecked"] = false;
      }
      if (this.assetservice.tree) {
        this.demTree = this.assetservice.tree;
      }

      if (this.assetservice.ref) {
        this.openFolder(this.assetservice.ref);
      }
    }
  }

  folderClicked = null;
  openFolder(folder) {
    if(this.assetservice.tree){
      this.demTree = this.assetservice.tree
    }
    var demo = [];
    folder["expand"] = !folder["expand"];
    this.folderClicked = folder;
    console.log(folder, "folder");
    for (let i = 0; i < this.demTree.length; i++) {

      if (folder.id == this.demTree[i].parentCatId) {
        demo.push(this.demTree[i]);
      }
    }
    if (demo.length > 0) {
      folder["children"] = demo;
    }
    console.log("folder === ", folder);
  }
  getFolder(tree) {
    console.log(tree, "fghfgd");
    // this.assetservice.mySubject.next(tree);

    // this.file.getFolders(tree)
    // this.router.navigate[]
    // for (let i = 0; i < this.trees.length; i++) {
    //   if(this.trees[i].id === tree.id){
    //     this.trees[i]["showActive"] = true;
    //   }else {
    //      this.trees[i]["showActive"] = true;
    //   }
    // }
    // for (let i = 0; i < this.demTree.length; i++) {

    //   if (tree.id == this.demTree[i].id) {
    //     // demo.push(this.demTree[i]);
    //     this.demTree[i]["showActive"] = true;
    //   }else {
    //     this.demTree[i]["showActive"] = true;
    //   }
    // }

    //new code

    // var param = {
    //   tree :tree,
    //   tabItem:this.inputTab
    // }
    // end new code
    this.getFolders.emit(tree);
  }
  open_branch(node) {
    console.log("Node ==>", node);

    // this.demTree[0].children[0].expand = !this.demTree[0].children[0].expand;
    // this.demTree[0].expand = ! this.demTree[0].expand;
    // if(node.parentCatId){
    //   const obj = this.getObject(node);
    //   obj.expand = !obj.expand;
    //   this.getObject(obj);
    // }
    // const obj = this.getObject(this.demTree);
    // for (let i = 0; i < this.demTree.length; i++) {
    //   const obj = this.deepSearch(this.demTree[i], 'parentCatId', (k, v) => v === '123');
    //   console.log('obj', obj);
    // }


    // var item = findDeep(this.demTree,node)
    // console.log(item,"item")
    // this.openFolder(findDeep(this.demTree,node))
    // }

    // console.log( "deepSearch ==>", this.deepSearch(this.demTree, 'id', (k, v) => v === node));

    // // console.log(findDeep(this.demTree, '596'));
    // // console.log(this.findNestedObj(this.demTree, 'id', '599'));
    this.array = [];
    this.openDeepyNestedFolder(node);
    if(this.array.length !== 0){
      this.array.reverse();
      for(let i = 0; i < this.array.length ; i++){
        if(this.array[i].parentCatId){
          this.openFolderDirectly(this.array[i]);
        }else{
          this.findInTreeArray(this.array[i]);
        }

        this.array[i]['expand'] = true;

      }
    }
    console.log('Final array',  this.array);
    if(this.array){
    this.bread.emit(this.array)
    }
    // this.assetservice.breadcrumbArray = this.array
    console.log('Final array',  this.trees);
  }
  ngOnChanges(changes: SimpleChanges) {
    // console.log(changes);
    // if(this.assetservice.tabActive == 1){
    if (this.valueChanged) {
      if (this.assetservice.tree) {
        this.demTree = this.assetservice.tree;

      }
      this.open_branch(this.valueChanged);

    }
  // }
  }

array = []
  openDeepyNestedFolder(node){
    const findDeep = function(data, activity) {
      return data.filter(function(e) {
        if(e.id == activity) {
          console.log(e); return e;}
        else if(e.items) return findDeep(e.items, activity)
      })
    }

    // console.log("findDeep =>",findDeep(this.demTree, node));
    let item = findDeep(this.demTree, node);
    if(item.length>0){
      node = item[0].parentCatId;
     this.array.push(item[0]);
      console.log("array =>", this.array);
      if(node){
        this.openDeepyNestedFolder(node);
      }else {
        console.log("findDeep =>", true,  this.array);
      }

    }else {
      console.log("findDeep =>", true,  this.array);
    }
  }

  openFolderDirectly(folder) {
    var demo = [];
    folder["expand"] = true;
    this.folderClicked = folder;
    console.log(folder, "folder");
    for (let i = 0; i < this.demTree.length; i++) {
      if (folder.id == this.demTree[i].id) {
        var j = i;
      }
      if (folder.id == this.demTree[i].parentCatId) {
        demo.push(this.demTree[i]);
      }
    }
    if (demo.length > 0) {
      folder["children"] = demo;
    }
    console.log("folder === ", folder);
  }
  findInTreeArray(event){
    if(event){
      for(let i = 0 ; i < this.trees.length; i++){
        if(this.trees[i].id === event.id){
          this.trees[i]['expand'] = true;
          break;
        }
      }
      this.openFolderDirectly(event);
      // this.getFolders(event,)
    }
  }

  // checkAllChild(folder){
  //   if(this.assetservice.tree){
  //     this.demTree = this.assetservice.tree
  //   }
  //   var demo = [];
  //   folder["expand"] = !folder["expand"];
  //   folder["checkBoxChecked"] = !folder["checkBoxChecked"];
  //   this.folderClicked = folder;
  //   console.log(folder, "folder");
  //   for (let i = 0; i < this.demTree.length; i++) {

  //     if (folder.id == this.demTree[i].parentCatId) {
  //       this.demTree[i]["checkBoxChecked"] = folder["checkBoxChecked"];
  //       demo.push(this.demTree[i]);
  //     }
  //   }
  //   if (demo.length > 0) {
  //     folder["children"] = demo;
  //   }
  //   console.log("folder === ", folder);
  //   this.getCheckedFolder.emit(folder);
  // }
}
