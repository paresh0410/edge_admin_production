import { Component, OnInit , Compiler} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Router, NavigationStart } from '@angular/router';
//import { UtilityService } from '../../../shared/services/utility.service';

@Component({
  selector: 'add-service-modal',
  styleUrls: ['./default-modal.component.scss'],
  templateUrl: './default-modal.component.html'
})

export class DefaultModal implements OnInit {

  modalHeader: string;
  modalContent: string = `New randomly generated password has been sent to your registered email-id 
  Please enter that password to login and do not forgot to change your password after login.`;

  constructor(private activeModal: NgbActiveModal, private _compiler: Compiler, private router: Router) {
  }

  ngOnInit() {}

  closeModal() {
    // this.activeModal.close();
    // this.router.navigate(['../../login']);
    this.activeModal.close();
    // localStorage.clear(); 
    // sessionStorage.clear();
    // window.localStorage.clear();
    // this._compiler.clearCache();
    // this.utility.logout();
    // this.router.navigate(['login'], { replaceUrl: false });
  }
}
