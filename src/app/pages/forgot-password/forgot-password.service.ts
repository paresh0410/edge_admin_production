import {Injectable, Inject} from '@angular/core';
import {Http, Response, Headers, RequestOptions, Request} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppConfig} from '../../app.module';

@Injectable()
export class ForgotpassService {

   //busy: Promise<any>;
  //private _url:string = "https://api.myjson.com/bins/hcnt6"
  private _url:string = "/api/bfl/NewUser/token"
  private _urlToken:string = "/api/bfl/NewUser/forgotpassword"
  // service="moodle_mobile_app"
  request: Request;

  constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private _http: Http){
      //this.busy = this._http.get('...').toPromise();
  }

  //@LoadingIndicator()

  forgotPass(user) {
    // let Str = "username="+user.username+"&password="+user.password+"&service="+user.service;
    let url:any = `${this.config.FINAL_URL}`+this._url;
    let headers = new Headers({ 'Content-Type':  'application/json' });
    let options = new RequestOptions({ headers: headers });
    //let body = JSON.stringify(user);
    return this._http.post(url, user, options ).map((res: Response) => res.json());
  }

  tokenPass(user) {
    let url:any = `${this.config.FINAL_URL}`+this._urlToken;
    let headers = new Headers({ 'Content-Type':  'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this._http.post(url, user, options ).map((res: Response) => res.json());
  }

  _errorHandler(error: Response){
    console.error(error);
    return Observable.throw(error || "Server Error")
  }

  public data: any;
}
