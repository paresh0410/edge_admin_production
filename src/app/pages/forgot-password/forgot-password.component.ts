



import {Component,Input,ElementRef, OnInit ,ViewChild,ViewContainerRef } from '@angular/core';
import {FormGroup, AbstractControl, FormBuilder, Validators} from '@angular/forms';
//import {EmailValidator, EqualPasswordsValidator} from '../../theme/validators';
import { Router, NavigationStart } from '@angular/router';
//import {AuthService} from '../../shared/services/auth.service';
import * as myGlobals from '../../app.module';
//import { BaImageLoaderService, BaThemePreloader, BaThemeSpinner } from '../../theme/services';
import {Http, Headers, Response,RequestOptions} from '@angular/http';
import {Observable} from 'rxjs';
import {AppConfig} from '../../app.module';
import "rxjs/add/operator/do";
import "rxjs/add/operator/map";
//import {GlobalState} from '../../global.state';
//import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Compiler } from '@angular/core';
import { ForgotpassService } from './forgot-password.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DefaultModal } from './default-model/default-modal.component';
//import { UtilityService } from '../../../../shared/services/utility.service';
// import { Ng2PopupComponent, Ng2MessagePopupComponent } from 'ng2-popup';

// import {ModalModule} from "ng2-modal";

// import {Popup} from 'ng2-opd-popup';

@Component({
  selector: 'ngx-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})

export class ForgotPasswordComponent {

	showPassVal:boolean;
  
  constructor(fb:FormBuilder, private router: Router, vRef: ViewContainerRef, private _compiler: Compiler,
    private modalService: NgbModal,protected service1: ForgotpassService) {
    
    this.showPassVal = true;

  }

  gotologin(){
  	this.router.navigate(['login']);
  }

  lgModalShow() {
    const activeModal = this.modalService.open(DefaultModal, {size: 'sm'});
    activeModal.componentInstance.modalHeader = 'Forgot password';
  }

  userForgotData:any = [];
  error:any;
  errorMessage:any;
  errorMsg:any;
  
  forgotData1:any = {
      ecn:"",
      // email: "",
  }

  ecnData:any = {};

  service:any ="moodle_mobile_app";

  forgotPass() {
  	this.error = false;
    //this._spinner.show();
    let forgotData = {
        ecn: this.forgotData1.ecn,
        // email: this.forgotData1.email,
     }
    console.log('User Data ',forgotData);
    this.ecnData = forgotData;
    localStorage.setItem('LoginData', JSON.stringify(forgotData));
    this.service1.forgotPass(forgotData).subscribe(
       data => {
          this.userForgotData = data;
          console.log('user Forgot Data', this.userForgotData);
          localStorage.setItem('ForgotResData', JSON.stringify(this.userForgotData));
          if(data.error || this.userForgotData.data1 == "No match found"){
            //this._spinner.hide();
            this.error = true;
            this.errorMessage = 'Invalid ECN / EMP ID';
          }else{
            //this._spinner.hide();
            // this.lgModalShow();
            this.showPassVal = false;
            // this.router.navigate(['pages/login']);
          }
          // this._spinner.hide();
          // this.router.navigate(['pages/dashboard']);
       },
       error => {
         //this._spinner.hide();
         console.error("Error recovering user!");
         this.errorMsg = error;
         this.error = true;
         this.errorMessage = 'Error recovering user!';
         //return Observable.throw(error);
       }
    );
  }

	error1:any;
  	errorMessage1:any;
  	errorMsg1:any;

	userTokenData:any = [];
	tokenData1:any = {
	  token:"",
	  // email: "",
	}

  	tokenVal() {
    	this.error1 = false;
	    //this._spinner.show();
	    let tokenData = {
	        ecn: this.ecnData.ecn,
	        // email: this.forgotData1.email,
	     }
	    console.log('User Data ',tokenData);
	    localStorage.setItem('LoginData', JSON.stringify(tokenData));
	    if(this.userForgotData.data1 == this.tokenData1.token){
	    	this.service1.tokenPass(tokenData).subscribe(
		       data => {
		          this.userTokenData = data;
		          console.log('user token Data', this.userTokenData);
		          localStorage.setItem('tokenData', JSON.stringify(this.userTokenData));
		          if(data.error){
		            //this._spinner.hide();
		            this.error1 = true;
		            this.errorMessage1 = 'Invalid Token & ECN';
		          }else{
		            //this._spinner.hide();
		            this.lgModalShow();
		            this._compiler.clearCache();
          			//this.utility.logout();
         			this.router.navigate(['login'], { replaceUrl: false });
		            // this.showPassVal = false;
		            // this.router.navigate(['pages/login']);
		          }
		       },
		       error => {
		         //this._spinner.hide();
		         console.error("Error recovering user!");
		         this.errorMsg1 = error;
		         this.error1 = true;
		         this.errorMessage1 = 'Error recovering user!';
	       }
	    );
	}else{
		//this._spinner.hide();
        this.error1 = true;
        this.errorMessage1 = 'Invalid Token';
	}    
  }

  cancel(){
    this.router.navigate(['../']);
  }
}  

