import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import * as RecordRTC from 'recordrtc';
import { SuubHeader } from '../components/models/subheader.model';
@Component({
  selector: 'record-rtc',
  templateUrl: './record-rtc.component.html',
  styleUrls: ['./record-rtc.component.scss']
})
export class RecordRtcComponent implements AfterViewInit {
  @ViewChild('video') video: any
  recordRTC: any;
  stream: MediaStream;
  header: SuubHeader;
 
  constructor() { }

  ngOnInit() {
    this.header = {
      title: "Record Video",
      showBreadcrumb : true,
      btnsSearch: true,
      btnName1:'Record',
      btnName1show:true,
      btnName2:'Stop',
      btnName2show:true,
      btnName3:'Download',
      btnName3show:true,
      btnBackshow: true,
      breadCrumbList:[
        {
          'name': 'Settings',
          'navigationPath': '/pages/plan',
          }
      ]
   
    };
  }

  back() {
    window.history.back();
  }

  ngAfterViewInit(){
    console.log('video ' + this.video);
    let video: HTMLVideoElement = this.video.nativeElement;
    video.muted = false;
    video.controls = true;
    video.autoplay = false;
  }

  toggleControls() {
    let video: HTMLVideoElement = this.video.nativeElement;
    video.muted = !video.muted;
    video.controls = !video.controls;
    video.autoplay = !video.autoplay;
  }

  startRecording() {

    navigator.mediaDevices.getUserMedia({
      audio: true, 
      video: { 
          facingMode: "user", 
          width: { exact: 1280 },
          height: { exact: 720 }, 
      }})
      .then(this.successCallback.bind(this), this.errorCallback.bind(this));
}

  successCallback(stream: MediaStream) {
    var options = {
      mimeType: 'video/webm', // or video/webm\;codecs=h264 or video/webm\;codecs=vp9
      // audioBitsPerSecond: 128000,
      // videoBitsPerSecond: 128000,
      bitsPerSecond: 51200000 // if this line is provided, skip above two
    };
    this.stream = stream;
    this.recordRTC = RecordRTC(stream, options);
    this.recordRTC.startRecording();
    let video: HTMLVideoElement = this.video.nativeElement;
    video.srcObject = stream;
    this.toggleControls();
  }

  errorCallback() {
    alert(this);
  }

  stopRecording() {
    let recordRTC = this.recordRTC;
    recordRTC.stopRecording(this.processVideo.bind(this));
    let stream = this.stream;
    stream.getAudioTracks().forEach(track => track.stop());
    stream.getVideoTracks().forEach(track => track.stop());
  }

  processVideo(audioVideoWebMURL) {
 
    let recordRTC = this.recordRTC;
    this.video.nativeElement.srcObject = null;
    this.video.nativeElement.src = audioVideoWebMURL;
    this.toggleControls();
    var recordedBlob = recordRTC.getBlob();
    recordRTC.getDataURL(function (dataURL) { });
  }

  download() {
    if (this.recordRTC) {
      this.recordRTC.save('video.mp4');
    } else {
      alert('Unable to download!');
    }

  }
}
