import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntermediateToolsComponent } from './intermediate-tools.component';

describe('IntermediateToolsComponent', () => {
  let component: IntermediateToolsComponent;
  let fixture: ComponentFixture<IntermediateToolsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntermediateToolsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntermediateToolsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
