import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { ModuleActivityContainerComponent } from './module-activity-container/module-activity-container.component';
import { ToolsComponent } from './tools.component';
// import { ToolsComponent } from './tools/tools.component';
import { ContentComponent } from './content/content.component';
import { AddEditContentComponent } from './add-edit-content/add-edit-content.component';
const routes: Routes = [
  {path: '', component: ToolsComponent},
  { path: 'content', component: ContentComponent },
  { path: 'content/add-edit-content', component: AddEditContentComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ToolsRoutingModule { }
