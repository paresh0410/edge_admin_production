import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
//import { SectionComponent } from './section/section.component';
import { ContentComponent } from './content/content.component';
// import { IntermediateToolsComponent } from './intermediate-tools/intermediate-tools.component';
import { FormsModule } from '@angular/forms';
import { ToolsService } from './tools.service';
import { IconPickerModule } from 'ngx-icon-picker';

import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { FilterPipeModule  } from 'ngx-filter-pipe';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AddEditContentComponent } from './add-edit-content/add-edit-content.component';
import { ContentDetailsComponent } from './add-edit-content/content-details/content-details.component';
import { EneroltoolsComponent } from './add-edit-content/eneroltools/eneroltools.component';
import { enrolService } from './add-edit-content/eneroltools/enrol/enrolment.service';
import { enrolmentComponent } from './add-edit-content/eneroltools/enrol/enrolment.component';
import { NbTabsetModule } from '@nebular/theme';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgxContentLoadingModule } from 'ngx-content-loading';
import { ComponentModule } from '../../component/component.module';
import { ToolsRoutingModule } from './tools-routing.module';
import { ToolsComponent } from './tools.component';
@NgModule({
  imports: [
    CommonModule,
    AngularMultiSelectModule,
    FormsModule,
    FilterPipeModule,
    NgxDatatableModule,
    ComponentModule,
    IconPickerModule,
    NbTabsetModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    NgxSkeletonLoaderModule,
    NgxContentLoadingModule,
    ComponentModule.forRoot(),
    BsDatepickerModule.forRoot(),
    DatepickerModule.forRoot(),
    ToolsRoutingModule,
  ],
  declarations: [
    ToolsComponent,
   // SectionComponent,
    ContentComponent,
    EneroltoolsComponent,
    enrolmentComponent,
    AddEditContentComponent,
    ContentDetailsComponent,
  ],
  providers: [
    ToolsService,
    enrolService,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA,
  ],
})
export class ToolsModule { }
