import { Component, OnInit, ChangeDetectorRef, ViewChild, Input, OnChanges,SimpleChanges } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToolsService } from '../../tools.service';
// import { ToasterService } from 'angular2-toaster';
import { webAPIService } from '../../../../service/webAPIService';
import { AuthenticationService } from '../../../../service/authentication.service';
import { AddEditContentComponent } from '../../add-edit-content/add-edit-content.component';
import { ToastrService } from 'ngx-toastr';
import { noData } from '../../../../models/no-data.model';

@Component({
  selector: 'ngx-content-details',
  templateUrl: './content-details.component.html',
  styleUrls: ['./content-details.component.scss']
})
export class ContentDetailsComponent implements OnInit {

  @ViewChild("contentFormValidation") formval: any;
  @Input() inpdata: any;

  parentSectionId;
  content: any;
  contentData: any = {};
  browseContentShow: boolean = false;
  browse: boolean  = false;
  textBox: boolean = false;
  addAction: boolean;
  allContentDropdown: any = [];
  noDataFound: boolean = false;
  fileUrl: any;
  enableUpload: any = false;
  fileReaded: any;
  selected: boolean = true;
  errorMsg: any;
  tenantId;
  userId;
  assessmentFileData: any = null;
  fileUploadRes: any;
  fileName: string = 'Upload File.';
  showContent: boolean = false;
  srchContentResult: any = [];
  msg: any;
  title='Select a content'
  btnName='Select'
  contentSect='Enter the Content of the Section'
  description='Enter description for section'
contentTooltip='Enter the name of the Content'
contentLink='Enter link of content'
noDataVal:noData={
  margin:'mt-3',
  imageSrc: '../../../../../assets/images/no-data-bg.svg',
  title:'',
  desc:'',
  titleShow:true,
  btnShow:false,
  descShow:false,
  btnText:'Learn More',
  btnLink:'https://faq.edgelearning.co.in/kb/add-location',
}
  constructor( private spinner: NgxSpinnerService,
              private toolsService: ToolsService,
              private toasterService: ToastrService,
              private webApiService: webAPIService,
              private cdf: ChangeDetectorRef,
              private authservice: AuthenticationService,
              private addEditConComp: AddEditContentComponent,
              private toastr: ToastrService,

  ) {
    this.parentSectionId = this.toolsService.parentSecId;
    console.log(this.parentSectionId,"parent")
    if (localStorage.getItem('LoginResData')) {
      const userData = JSON.parse(localStorage.getItem('LoginResData'));
      this.userId = userData.data.data.id;
    }
   }

  ngOnInit() {
    this.parentSectionId = this.toolsService.parentSecId;
    console.log(this.parentSectionId,"parent ng Oninit")

    this.addAction = this.toolsService.addActions;
    console.log(this.addAction,"this.addAction")
    if (!this.addAction) {
      if (this.toolsService.contentDetails.contentType == 8) {
        this.activityContentSelected = false;
        this.browse = false;
        this.textBox = true;

      } else {
        this.activityContentSelected = true;
        this.browse = true;
        this.textBox = false;
        this.enableUpload = true;
      }
    }
    this.tenantId = this.toolsService.tenantId;
    this.getContentDropdowns();
    this.contentData = this.toolsService.contentDetails;
    console.log(this.contentData.id,"this.contentData")
    this.contentData.link = this.toolsService.contentDetails.assetRef;
    this.getTokenAndUserData();
  }

  ngOnChanges(changes: SimpleChanges): void {
		if(this.inpdata === 'details') {
		  this.submit(this.contentData)
		}
	}


  contentSelect(event: any) {
    console.log('value', event.target.value);
    if (event.target.value == 8) {
      this.textBox = true;
      this.browse = false
      this.content = event.target.value;
      this.activityContentSelected = false;
    } else {
      this.browse = true;
      this.textBox = false;
      this.content = event.target.value;
    }
  }

  browseContent() {
    this.browseContentShow = true;
  }

  closeActivityContentModel() {
    this.browseContentShow = false;
     this.srchContentResult = [];
     if (!this.addAction) {
      this.contentData.contentType = '';
      this.contentData.assetRef = null;
      this.contentData.assetName = null;
      this.contentData.description = null;
      this.activityContentSelected = false;
     }
  }

  submit(data) {
    console.log('userData', data);
    if(!this.formval.form.valid) {
      this.toasterService.warning('Please fill required details');
      return;
    } else {
      if (data.contentType == 8) {
        this.spinner.show();
        const param = {
          'secId': this.parentSectionId,
          'conId': data.id,
          'conName': data.contentName,
          'conDesc': data.contentDesc,
          'assetName': null,
          'assetDesc': null,
          'assetRef': data.link,
          'conType': data.contentType,
          'vis': data.visible,
          'userId': this.tenantId,
          'tId': this.userId,
        }
        console.log('submitData', param);
  
        this.addEditContentSectionTool(param);
  
        // if(this.enableUpload === false) {
        //   this.presentToast('error', '');
        // } else {
  
        // }
  
      } else {
        if (this.activityContentSelected) {
          this.spinner.show();
          const param = {
            'secId': this.parentSectionId,
            'conId': data.id,
            'conName': data.contentName,
            'conDesc': data.contentDesc,
            'assetName': data.assetName,
            'assetDesc': data.description,
            'assetRef': data.assetRef,
            'conType': data.contentType,
            'vis': data.visible,
            'userId': this.tenantId,
            'tId': this.userId,
          }
          console.log('submitData', param);
  
          if(this.enableUpload === false) {
            this.presentToast('error', '');
          } else {
            this.addEditContentSectionTool(param);
          }
  
  
        } else {
          this.presentToast('warning', 'select assets for content');
        }
      }
    }

  }

  getContentDropdowns() {
    this.spinner.show();
    const param = {
      'tId': this.tenantId,
    };
    this.toolsService.getContentDropdown(param)
      .then(rescompData => {
        this.spinner.hide();
        var result = rescompData;
        console.log('contentDropdown:', rescompData);
        if (result['type'] === true) {
          if (result['data'][0].length === 0) {
            // this.presentToast('warning', result['data'][0].msg);

            this.noDataFound = true;

          } else {
           // this.presentToast('success', result['data'][0].msg);
            this.allContentDropdown = result['data'][0];

            console.log('this.allContentDropdown', this.allContentDropdown);

          }

        } else {

          this.presentToast('error', '');
        }


      }, error => {
        this.spinner.hide();

        this.presentToast('error', '');
     });
  }

  addEditContentSectionTool(params) {
    console.log('paramNew', params);
    this.spinner.show();
    this.toolsService.addEditContentSection(params)
      .then(rescompData => {

        this.spinner.hide();

        const temp = rescompData;
        const result = temp['data'];
        console.log('content add edit:', result);
        if (temp['type'] === true) {
          if (this.addAction) {
            this.contentData.id = result[0][0].conId;
            this.presentToast('success', result[0][0].msg);
          } else {
            this.presentToast('success', result[0][0].msg);
          }
          // this.addEditConComp.EnrolTab = true;
          // this.addEditConComp.contentTab = false;
          const DataTab = {
            tabTitle: 'Enrol',
          }
          this.addEditConComp.selectedTab(DataTab);
          this.toolsService.conId = result[0][0].conId;
          console.log(this.toolsService.conId);
        } else {
          this.presentToast('error', '');
        }


      },
        resUserError => {
          this.spinner.hide();
          this.presentToast('error', '');
          this.errorMsg = resUserError;
        });
  }

  presentToast(type, body) {
    if(type === 'success'){
      this.toastr.success(body, 'Success', {
        closeButton: false,
       });
    } else if(type === 'error'){
      // tslint:disable-next-line: max-line-length
      this.toastr.error('Please contact site administrator and <div (click)="giveFeedback()"> click here <div> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true,
      });
    } else{
      this.toastr.warning(body, 'Warning', {
        closeButton: false,
        });
    }
  }

  GlobalDamToken = null;
  dataDetail: string = null;

  getTokenAndUserData() {

    const token = this.authservice.getToken();
    let result = null;
    if (token) {
      this.toolsService.generatingToken(null).then(
        rescompData => {
        //  this.loader = false;
          result = rescompData;
          this.GlobalDamToken = result['token'];
          console.log(this.GlobalDamToken);
          console.log("Response token Data with activity Result", result);
          this.cdf.detectChanges();
          if (this.GlobalDamToken != null) {
            this.noDataVal.title = 'Search content...';
          } else {
            this.noDataVal.title = 'You Have no access to the Dam Asset';
          }
        },
        resUserError => {
         // this.loader = false;
          this.errorMsg = resUserError;
        }
      );
    }
  }


  contentSearchQry: string = "";


  searchActivityContent(conType, srchStr) {
    console.log('conType', conType);
    console.log("contentSearchQry ", srchStr);
    const param = {
      searchString: srchStr,
      tId: this.tenantId,
      userId: this.userId,
      token: this.GlobalDamToken,
      assetType: conType,
    };
    console.log('search con param', param);
    if (srchStr.length > 3 && this.GlobalDamToken != null) {
      this.spinner.show();
      this.toolsService.ApprovedDamHanshakeData(param).then(
        rescompData => {
          this.spinner.hide();
          if (rescompData) {
            this.srchContentResult = rescompData['AssetData'];
            if (this.srchContentResult.length == 0 && rescompData['type'] === false) {
              this.noDataVal.title = 'You Have no access to the Dam Asset';
            } else {
              this.noDataVal.title = 'No Asset Available';
            }

          } else {
            this.srchContentResult = [];
            this.noDataVal.title = 'No Asset Available';

          }

          console.log("Search Content Result ", rescompData);
          this.cdf.detectChanges();
        },
        resUserError => {
          // this.loader =false;
          this.spinner.hide();
          this.errorMsg = resUserError;
        }
      );
    }
  }

  selectedActivityContent(currentIndex, content) {
    console.log("Selected content ", content);
  }

  activeSelectedContentIndex: any;
  activeSelectedContent: any;
  activeSelectedContentId: any;
  activeSelectedContentName: any;

  setActiveSelectedContent(currentIndex, currentContent) {
    this.activeSelectedContentIndex = currentIndex;
    this.activeSelectedContent = currentContent;
    this.activeSelectedContentId = currentContent.id;
    this.activeSelectedContentName = currentContent.assetName;
  }
  activityContentSelected: boolean = false;
  saveSelectedActivityContent() {
    console.log("Selected activity content ", this.activeSelectedContent);
    this.contentData.assetName = this.activeSelectedContent.assetName;
    this.contentData.description = this.activeSelectedContent.description;
    this.contentData.assetRef = this.activeSelectedContent.assetRef;
    this.activityContentSelected = true;

    this.browseContentShow = false;

    this.enableUpload = true;
  }

  removeSelectedActivityContent() {
    this.contentData.name = null;

    this.activityContentSelected = false;
    this.activeSelectedContentId = null;
    this.enableUpload = false;
  }

}
