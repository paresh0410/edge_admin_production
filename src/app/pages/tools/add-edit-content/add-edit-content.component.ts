import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SuubHeader } from '../../components/models/subheader.model';
import { ToolsService } from '../tools.service';
// import { NONAME } from 'dns';

@Component({
  selector: 'ngx-add-edit-content',
  templateUrl: './add-edit-content.component.html',
  styleUrls: ['./add-edit-content.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AddEditContentComponent implements OnInit {


  contentTab: boolean = true;
  EnrolTab: boolean = false;
  parentSectionId;
  userId;

  header: SuubHeader = {
    title: '',
    btnsSearch: true,
    searchBar: false,
    dropdownlabel: '',
    drplabelshow: false,
    drpName1: '',
    drpName2: ' ',
    drpName3: '',
    drpName1show: false,
    drpName2show: false,
    drpName3show: false,
    btnName1: '',
    btnName2: '',
    btnName3: '',
    btnAdd: '',
    btnName1show: false,
    btnName2show: false,
    btnName3show: false,
    btnBackshow: true,
    btnAddshow: false,
    filter: false,
    showBreadcrumb: true,
    breadCrumbList: [
      {
        'name': 'Tools',
        'navigationPath': '/pages/tools',
      }, {
        'name': 'Content',
        'navigationPath': '/pages/tools/content',
      },]
  };
  isdata: any;
  nav: any;

  constructor(private router: Router,
    private route: ActivatedRoute,    private toolsService: ToolsService,) {

      console.log(this.toolsService.nav)
      this.nav=this.toolsService.nav
  }
  ngOnInit() {
  }
  backToTools(data){
    this.toolsService.fromContent = true;
    this.nav = this.nav.slice(0, data.index);
    this.toolsService.parentSecId=data.id
    this.toolsService.fromContent = true;
    this.toolsService.navObj={
      'routeFrom':'content',
      'title':'',
      // 'titleContent':this.contentData.contentName,
      'index':data.index,
      'section':this.toolsService.nav[data.index].name,
      'data':this.toolsService.nav[data.index].id
      }
    this.toolsService.nav=this.nav
    this.router.navigate(['../pages/tools']);

  }
  selectedTab(tabEvent) {
    console.log('tab Selected', tabEvent);

    if (tabEvent.tabTitle === 'Details') {
      this.header = {
        title: this.toolsService.navObj.titleContent,
        btnsSearch: true,
        searchBar: false,
        searchtext: '',
        dropdownlabel: ' ',
        drplabelshow: false,
        drpName1: '',
        drpName2: ' ',
        drpName3: '',
        drpName1show: false,
        drpName2show: false,
        drpName3show: false,
        btnName1: '',
        btnName2: '',
        btnName3: '',
        btnName4: 'Save & Next',
        btnAdd: '',
        btnName1show: false,
        btnName2show: false,
        btnName3show: false,
        btnName4show: true,
        btnBackshow: true,
        btnAddshow: false,
        filter: false,
        showBreadcrumb: true,
        breadCrumbList: this.nav
      };
      this.contentTab = true;
      this.EnrolTab = false;
    } else if (tabEvent.tabTitle === 'Enrol') {
      this.header = {
        title: this.toolsService.navObj.titleContent,
        btnsSearch: true,
        searchBar: false,
        searchtext: '',
        dropdownlabel: ' ',
        drplabelshow: false,
        drpName1: '',
        drpName2: ' ',
        drpName3: '',
        drpName1show: false,
        drpName2show: false,
        drpName3show: false,
        btnName1: '',
        btnName2: 'Enrol',
        btnName3: 'Bulk Enrol',
        btnName4: '',
        btnName5: 'Back',
        btnAdd: '',
        btnName1show: false,
        btnName2show: true,
        btnName3show: true,
        btnName4show: false,
        btnName5show: false,
        btnBackshow: true,
        btnAddshow: false,
        filter: false,
        showBreadcrumb: true,
        breadCrumbList: this.nav
        // breadCrumbList: [
        //   {
        //     'name': 'Tools',
        //     'navigationPath': '/pages/tools',
        //   }, {
        //     'name': 'Content',
        //     'navigationPath': '/pages/tools/content',
        //   },]
      };
      this.contentTab = false;
      this.EnrolTab = true;
      console.log(this.EnrolTab, "EnrolTab")
    }
  }

  backContent() {
    console.log('back pressed.');
    // this.router.navigate(['../calendar']);
    if (this.EnrolTab === true) {
      if (this.header.btnName2show === false || this.header.btnName3show === false) {
        this.passData('oneback');
      // this.toolsService.fromContent=true
      // this.toolsService.navObj={
      //   'routeFrom':'content',
      //   'title':'',
      //   // 'titleContent':this.contentData.contentName,
      //   'index':this.nav.length,
      //   'section':this.nav[this.nav.length-1].name,
      //   'data':''
      //   }
      // this.nav = this.nav.slice(0, this.nav.length-1);
      // this.toolsService.nav=this.nav
      } else {
        window.history.back();
        this.toolsService.fromContent=true
        this.toolsService.navObj={
          'routeFrom':'content',
          'title':'',
          // 'titleContent':this.contentData.contentName,
          'index':this.nav.length,
          'section':this.nav[this.nav.length-1].name,
          'data':''
          }
        this.nav = this.nav.slice(0, this.nav.length-1);
        this.toolsService.nav=this.nav
      }
    } else {
      this.toolsService.fromContent=true
      this.toolsService.navObj={
        'routeFrom':'content',
        'title':'',
        // 'titleContent':this.contentData.contentName,
        'index':this.nav.length,
        'section':this.nav[this.nav.length-1].name,
        'data':''
        }
      this.nav = this.nav.slice(0, this.nav.length-1);
      this.toolsService.nav=this.nav
      window.history.back();
    }
  }

  passData(val) {
    this.isdata = val;
    // this.eventsSubject.next();
    if (val === 'enrol') {
      this.header = {
        title: this.toolsService.navObj.titleContent,
        btnsSearch: true,
        searchBar: false,
        searchtext: '',
        dropdownlabel: ' ',
        drplabelshow: false,
        drpName1: '',
        drpName2: ' ',
        drpName3: '',
        drpName1show: false,
        drpName2show: false,
        drpName3show: false,
        btnName1: '',
        btnName2: 'Enrol',
        btnName3: 'Bulk Enrol',
        btnAdd: '',
        btnName1show: false,
        btnName2show: false,
        btnName3show: true,
        btnBackshow: true,
        btnAddshow: false,
        filter: false,
        showBreadcrumb: true,
        breadCrumbList: this.toolsService.nav

        // breadCrumbList: [

        //   {
        //     'name': 'Tools',
        //     'navigationPath': '/pages/tools',
        //   }, {
        //     'name': 'Content',
        //     'navigationPath': '/pages/tools/content',
        //   },]
      };
    } else if (val === 'bulken') {
      this.header = {
        title: this.toolsService.navObj.titleContent,
        btnsSearch: true,
        searchBar: false,
        searchtext: '',
        dropdownlabel: ' ',
        drplabelshow: false,
        drpName1: '',
        drpName2: ' ',
        drpName3: '',
        drpName1show: false,
        drpName2show: false,
        drpName3show: false,
        btnName1: '',
        btnName2: 'Enrol',
        btnName3: 'Bulk Enrol',
        btnAdd: '',
        btnName1show: false,
        btnName2show: true,
        btnName3show: false,
        btnBackshow: true,
        btnAddshow: false,
        filter: false,
        showBreadcrumb: true,
        breadCrumbList: this.toolsService.nav
   
      };
    } else if (val === 'oneback') {
      this.header = {
        title: this.toolsService.navObj.titleContent,
        btnsSearch: true,
        searchBar: false,
        searchtext: '',
        dropdownlabel: ' ',
        drplabelshow: false,
        drpName1: '',
        drpName2: ' ',
        drpName3: '',
        drpName1show: false,
        drpName2show: false,
        drpName3show: false,
        btnName1: '',
        btnName2: 'Enrol',
        btnName3: 'Bulk Enrol',
        btnName4: '',
        btnName5: 'Back',
        btnAdd: '',
        btnName1show: false,
        btnName2show: true,
        btnName3show: true,
        btnName4show: false,
        btnName5show: false,
        btnBackshow: true,
        btnAddshow: false,
        filter: false,
        showBreadcrumb: true,
        breadCrumbList: this.nav

      };
    }
    setTimeout(() => {
      this.isdata = '';
    }, 2000);
  }


}
