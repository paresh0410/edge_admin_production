import {
  Component,
  OnInit,
  ChangeDetectorRef,
  ViewChild,
  ViewEncapsulation,
  Input,
  OnChanges,SimpleChanges
} from "@angular/core";
import { DatePipe } from "@angular/common";
import { Router } from "@angular/router";
import { DatatableComponent } from "@swimlane/ngx-datatable";
// import { ToasterService, Toast } from "angular2-toaster";
import { NgxSpinnerService } from "ngx-spinner";
import { ToolsService } from "../../tools.service";
import { AddEditCourseContentService } from "../../../plan/courses/addEditCourseContent/addEditCourseContent.service";
import { XlsxToJsonService } from "../../../plan/users/uploadusers/xlsx-to-json-service";
import { ToastrService } from "ngx-toastr";
import { JsonToXlsxService } from "../../../coaching/participants/bulk-upload-coaching/json-to-xlsx.service";
import { CommonFunctionsService } from "../../../../service/common-functions.service";
import { BrandDetailsService } from "../../../../service/brand-details.service";
import { noData } from "../../../../models/no-data.model";
@Component({
  selector: "ngx-eneroltools",
  templateUrl: "./eneroltools.component.html",
  styleUrls: ["./eneroltools.component.scss"],
  providers: [DatePipe],
  encapsulation: ViewEncapsulation.None
})
export class EneroltoolsComponent implements OnInit {
  @ViewChild("fileUpload") fileUpload: any;
  @ViewChild("myTable") table: any;
  @ViewChild(DatatableComponent) tableData: DatatableComponent;
	@Input() inpdata: any;
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No enrollment added.",
    desc:"",
    titleShow:true,
    btnShow:true,
    descShow:false,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/tools-content-enrol',
  }
  userData: any = [];
  content: any = [];
  enrolldata: any = [];
  rows: any = [];
  selected: any = [];
  temp = [];
  enableCourse: boolean = false;
  showEnrolpage: boolean = false;
  showBulkpage: boolean = false;
  fileName: any = "You can drag and drop files here to add them.";
  fileReaded: any;
  enableUpload: any;
  file: any = [];
  loader: any;
  datasetPreview: any;
  timeout: any;
  preview: boolean = false;
  bulkUploadRes: any = [];
  resultdata: any = [];
  labels: any = [
		{ labelname: 'EMP CODE', bindingProperty: 'ecn', componentType: 'text' },
		{ labelname: 'FULL NAME', bindingProperty: 'fullname', componentType: 'text' },
		{ labelname: 'EMAIL', bindingProperty: 'emailId', componentType: 'text' },
		{ labelname: 'MOBILE', bindingProperty: 'phoneNo', componentType: 'text' },
		{ labelname: 'D.0.E', bindingProperty: 'enrolDate', componentType: 'text' },
		{ labelname: 'MODE', bindingProperty: 'enrolmode', componentType: 'text' },
		{ labelname: 'ACTION', bindingProperty: 'btntext', componentType: 'button' },
		// { labelname: 'Action', bindingProperty: 'action', componentType: 'select' },
		// { labelname: 'Action', bindingProperty: 'action', componentType: 'checkbox' },

	  ];
	  // rows: any = [
		// { 'empcode': 'ecn', 'Fullname': 'fullname', 'email':'emailId','number':'phoneNo','date':'enrolDate','mode':'enrolmode','button' :'btntext' },
	  // ];
  columns = [
    { prop: "ecn", name: "EMP CODE" },
    { prop: "fullname", name: "FULLNAME" },
    { prop: "gender", name: "GENDER" },
    { prop: "doj", name: "DOJ" },
    { prop: "department", name: "DEPARTMENT" },
    { prop: "mode", name: "MODE" }
  ];
  searchvalue: any = {
    value: ""
  };
  errorMsg: any;
  private xlsxToJsonService: XlsxToJsonService = new XlsxToJsonService();
  currentBrandData: any;
  nodata: boolean;
  constructor(
    public toolservice: ToolsService,
    private commonFunctionService: CommonFunctionsService,
    public courseDataService: AddEditCourseContentService,
    public cdf: ChangeDetectorRef,
    public datePipe: DatePipe,
    private router: Router,
    // public toasterService: ToasterService,
    public spinner: NgxSpinnerService,
    public brandService: BrandDetailsService,
    private exportService: JsonToXlsxService,
    private toastr: ToastrService
  ) {
    this.userData = JSON.parse(localStorage.getItem("LoginResData"));
  }

  ngOnInit() {
    this.currentBrandData = this.brandService.getCurrentBrandData();
    console.log(
      "this.toolservice.contentDetails",
      this.toolservice.contentDetails
    );
    this.content = this.toolservice.contentDetails;
    console.log("this.content", this.content);
    this.allEnrolUser();
  }

  ngOnChanges(changes: SimpleChanges): void {
		if(this.inpdata === 'enrol') {
		  this.saveEnrol();
		} else if(this.inpdata === 'bulken') {
      this.bulkEnrol();
    } else if (this.inpdata === 'oneback') {
      this.backToEnrol();
    }
	}

  clearesearch() {
    this.searchvalue = {};
    this.temp = [...this.enrolldata];
    this.rows = this.temp

  }
  allEnrolUser() {
    this.spinner.show();
    const param = {
      areaId: 33,
      instanceId: this.toolservice.conId,
      tId: this.userData.data.data.tenantId,
      mode: 0
    };
    console.log(param);
    this.courseDataService.getallenroluser(param).then(enrolData => {
     this.spinner.hide();
      console.log(enrolData);
      this.enrolldata = enrolData["data"];
      this.rows = enrolData["data"];
      this.rows = [...this.rows];
      if(enrolData['data'].length==0){
        this.nodata=true;
      }else{
        this.nodata=false;
      }
      for (let i = 0; i < this.rows.length; i++) {
        // this.rows[i].Date = new Date(this.rows[i].enrolDate);
        // this.rows[i].enrolDate = this.formdate(this.rows[i].Date);
        if(this.rows[i].visible == 1) {
					this.rows[i].btntext = 'fa fa-eye';
				  } else {
					this.rows[i].btntext = 'fa fa-eye-slash';
				  }
      }

      console.log("EnrolledUSer", this.rows);
      if ((this.enrolldata.visible = 1)) {
        this.enableCourse = false;
      } else {
        this.enableCourse = true;
      }
      this.cdf.detectChanges();
    });
  }
  formdate(date) {
    if (date) {
      var formatted = this.datePipe.transform(date, "dd-MM-yyyy");
      return formatted;
    }
  }
  onEnrolDataClickEvent(event) {
    // if (event.type == 'click' && event.cellIndex != 0) {
    //     // Do something when you click on row cell other than checkbox.
    //     console.log('Activate Event', event);
    // }
    // const checkboxCellIndex = 1;
    if (event.type === "checkbox") {
      // Stop event propagation and let onSelect() work
      console.log("Checkbox Selected", event);
      event.event.stopPropagation();
    } else if (event.type === "click" && event.cellIndex != 0) {
      // Do somethings when you click on row cell other than checkbox
      console.log("Row Clicked", event.row); /// <--- object is in the event row variable
    }
  }

  onPageEnrolData(event) {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      console.log("paged!", event);
    }, 100);
  }
  saveEnrol() {
    this.showEnrolpage = true;
    this.showBulkpage = false;
    this.allEnrolUser();
  }

  bulkEnrol() {
    this.showEnrolpage = false;
    this.showBulkpage = true;
  }
  back() {
    this.router.navigate(["../pages/tools/content"]);
    // window.history.back();
  }
  backToEnrol() {
    this.showEnrolpage = false;
    this.showBulkpage = false;
    this.file = [];
    this.preview = false;
    this.fileName = "You can drag and drop files here to add them.";
    this.enableUpload = false;
    this.allEnrolUser();
  }

  uploadedData: any = [];
  readUrl(event: any) {
    var validExts = new Array(".xlsx", ".xls");
    var fileExt = event.target.files[0].name;
    this.invaliddata = [];
    fileExt = fileExt.substring(fileExt.lastIndexOf("."));
    if (validExts.indexOf(fileExt) < 0) {
      // alert('Invalid file selected, valid files are of ' +
      //          validExts.toString() + ' types.');
      // var toast: Toast = {
      //   type: "error",
      //   title: "Invalid file selected!",
      //   body: "Valid files are of " + validExts.toString() + " types.",
      //   showCloseButton: true,
      //   // tapToDismiss: false,
      //   timeout: 2000
      //   // onHideCallback: () => {
      //   //     this.router.navigate(['/pages/plan/users']);
      //   // }
      // };
      // this.toastr.success(toast);
      // setTimeout(data => {
      //   this.router.navigate(['/pages/users']);
      // },1000);
      // return false;
      this.cancel();
    } else {
      // return true;
      if (event.target.files && event.target.files[0]) {
        this.fileName = event.target.files[0].name;
        //read file from input
        this.fileReaded = event.target.files[0];

        if (
          this.fileReaded != "" ||
          this.fileReaded != null ||
          this.fileReaded != undefined
        ) {
          this.enableUpload = true;
          this.showInvalidExcel = false;
        }

        this.file = event.target.files[0];

        this.xlsxToJsonService
          .processFileToJson({}, this.file)
          .subscribe(data => {
            let resultSheets = Object.getOwnPropertyNames(data["sheets"]);
            console.log("File Property Names ", resultSheets);
            let sheetName = resultSheets[0];
            let result = data["sheets"][sheetName];
            console.log("dataSheet", data);

            if (result.length > 0) {
              this.uploadedData = result;
              console.log("this.uploadedData", this.uploadedData);
            } else {
              this.uploadedData = [];
            }
          });
        var reader = new FileReader();
        reader.onload = (event: ProgressEvent) => {
          let fileUrl = (<FileReader>event.target).result;
          // event.setAttribute('data-title', this.fileName);
          // console.log(this.fileUrl);
        };
        reader.readAsDataURL(event.target.files[0]);
      }
    }

    // if(event.target.files[0].name.split('.')[event.target.files[0].name.split('.').length-1] === 'xlsx'){
    //     let file = event.target.files[0];
    // } else if(event.target.files[0].name.split('.')[event.target.files[0].name.split('.').length-1] === 'xls'){
    //     let file = event.target.files[0];
    // }else{
    //   alert('Invalid file selected, valid files are of ' +
    //          validExts.toString() + ' types.');
    // }
  }
  cancel() {
    // this.fileUpload.nativeElement.files = [];
    console.log(this.fileUpload.nativeElement.files);
    this.fileUpload.nativeElement.value = "";
    console.log(this.fileUpload.nativeElement.files);
    this.fileName = "You can drag and drop files here to add them.";
    this.enableUpload = false;
    this.file = [];
    this.preview = false;
  }

  invaliddata: any = [];
  showInvalidExcel: boolean = false;
  uploadfile() {
    if (this.uploadedData.length > 0 && this.uploadedData.length <= 2000) {
      this.spinner.show();

      // const option: string = Array.prototype.map
      //   .call(this.uploadedData, function(item) {
      //     console.log("item", item);
      //     return Object.values(item).join("#");
      //   })
      //   .join("|");

      // New Function as per requirement.

      var option: string = Array.prototype.map
      .call(this.uploadedData , (item) => {
        const array = Object.keys(item);
        let string = '';
        array.forEach(element => {
          if(element === 'EnrolDate'){
            string += this.commonFunctionService.formatDateTimeForBulkUpload(item[element]);
          }else {
            string = item[element] + '#';
          }
        });
        // console.log("item", item);
        // return Object.values(item).join("#");
        return string;
      })
      .join("|");

      const data = {
        wfId: null,
        aId: 33,
        iId: this.content.id,
        upString: option
      };
      console.log("data", data);
      this.toolservice.areaBulkEnrol(data).then(
        rescompData => {
          // this.loader =false;
          this.spinner.hide();
          var temp: any = rescompData;
          this.bulkUploadRes = temp.data;
          if (temp == "err") {
            this.toastr.error(
              'Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.',
              "Error",
              {
                timeOut: 0,
                closeButton: true
              }
            );
          } else if (temp.type == false) {
            this.toastr.error(
              'Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.',
              "Error",
              {
                timeOut: 0,
                closeButton: true
              }
            );
          } else {
            //if (temp['data'].length > 0) {
            // this.invaliddata = temp['data'];
            // this.showInvalidExcel = true;

            // this.invaliddata = temp['data'];
            // if (this.uploadedData.length == this.invaliddata.length) {
            //   this.toastr.warning('No valid employees found to enrol.', 'Warning', {
            //     closeButton: false
            //     });
            // } else {
            //   this.toastr.success('Enrol successfully.', 'Success', {
            //     closeButton: false
            //     });
            // }
            this.enableUpload = false;
            this.fileName =
              "Click here to upload an excel file to enrol "+ this.currentBrandData.employee.toLowerCase()+" to this course";
            if (temp["data"].length > 0) {
              if (temp["data"].length === temp["invalidCnt"].invalidCount) {
                this.toastr.warning(
                  "No valid "+ this.currentBrandData.employee.toLowerCase()+"s found to enrol.",
                  "Warning",
                  {
                    closeButton: false
                  }
                );
              } else {
                this.toastr.success("Enrol successfully.", "Success", {
                  closeButton: false
                });
              }
              this.invaliddata = temp["data"];
              this.showInvalidExcel = true;
            } else {
              this.invaliddata = [];
              this.showInvalidExcel = false;
            }
            // this.backToEnrol()
            this.cdf.detectChanges();
            this.preview = false;
            this.showInvalidExcel = true;
          }
          console.log("Enrol bulk Result ", this.bulkUploadRes);
          this.cdf.detectChanges();
        },
        resUserError => {
          this.spinner.hide();
          this.errorMsg = resUserError;
        }
      );
      // let fd = new FormData();
      // fd.append('content', JSON.stringify(this.content));
      // fd.append('file', this.file);

      // this.courseDataService.TempManEnrolBulk(fd).then(result => {
      //   console.log(result);
      //   // this.loader =false;

      //   var res = result;
      //   try {
      //     if (res['data1'][0]) {
      //       this.resultdata = res['data1'][0];
      //       for (let i = 0; i < this.resultdata.length; i++) {
      //         this.resultdata[i].enrolDate = this.formatDate(this.resultdata[i].enrolDate);
      //       }

      //       this.datasetPreview = this.resultdata;
      //       this.preview = true;
      //       this.cdf.detectChanges();
      //       // this.spinner.hide();
      //     }
      //   } catch (e) {

      //     this.presentToast('error', '');
      //   }
      //   this.spinner.hide();

      // },
      // resUserError => {
      //   // this.loader =false;
      //   this.spinner.hide();
      //   this.errorMsg = resUserError;
      //   // this.closeEnableDisableCourseModal();
      // });
    } else {

      if(this.uploadedData.length > 2000){
        this.toastr.warning('File Data cannot exceed more than 2000', 'warning', {
          closeButton: true
        });
        }
        else{
        this.toastr.warning('No Data Found', 'warning', {
          closeButton: true
        });
      }
    }
  }
  savebukupload() {
    var data = {
      userId: this.userData.userId,
      coursId: this.content.id
    };
    // this.loader = true;
    this.spinner.show();
    this.courseDataService.finalManEnrolBulk(data).then(
      rescompData => {
        // this.loader =false;
        this.spinner.hide();
        var temp: any = rescompData;
        this.bulkUploadRes = temp.data[0];
        if (temp == "err") {
          this.presentToast("error", "");
        } else if (temp.type == false) {
          this.presentToast("error", "");
        } else {
          this.presentToast("success", temp["data"].msg);
          this.backToEnrol();
          this.cdf.detectChanges();
          this.preview = false;
        }
        console.log("Enrol bulk Result ", this.bulkUploadRes);
        this.cdf.detectChanges();
      },
      resUserError => {
        this.loader = false;
        this.errorMsg = resUserError;
      }
    );
  }

  exportToExcelInvalid() {
    this.exportService.exportAsExcelFile(this.invaliddata, "Enrolment Status");
  }

  formatDate(date) {
    var d = new Date(date);
    var formatted = this.datePipe.transform(d, "dd-MMM-yyyy");
    return formatted;
  }

  presentToast(type, body) {
    if (type === "success") {
      this.toastr.success(body, "Success", {
        closeButton: false
      });
    } else if (type === "error") {
      // tslint:disable-next-line: max-line-length
      this.toastr.error(
        'Please contact site administrator and <div (click)="giveFeedback()"> click here <div> to leave your feedback.',
        "Error",
        {
          timeOut: 0,
          closeButton: true
        }
      );
    } else {
      this.toastr.warning(body, "Warning", {
        closeButton: false
      });
    }
  }
  visibilityTableRow(row) {
		let value;
		let status;

		if (row.visible == 1) {
		  row.btntext = 'fa fa-eye-slash';
		  value  = 'fa fa-eye-slash';
		  row.visible = 0
		  status = 0;
		} else {
		  status = 1;
		  value  = 'fa fa-eye';
		  row.visible = 1;
		  row.btntext = 'fa fa-eye';
		}

		for(let i =0; i < this.rows.length; i++) {
		  if(this.rows[i].employeeId == row.employeeId) {
			this.rows[i].btntext = row.btntext;
			this.rows[i].visible = row.visible
		  }
		}
    var visibilityData = {
			employeeId: row.employeeId,
			visible: status,
			courseId: this.content.id,
			tId: this.userData.data.data.tenantId,
			aId: 33,
		  }
		  this.courseDataService.disableEnrol(visibilityData).then(result => {
      console.log(result);
      this.spinner.hide();
      this.resultdata = result;
      if (this.resultdata.type == false) {
        this.presentToast('error', '');
      } else {

        console.log('after', row.visible)
        this.allEnrolUser();
        this.presentToast('success', this.resultdata.data);
      }
    },
      resUserError => {
        this.errorMsg = resUserError;
        // this.closeEnableDisableCourseModal();
      });
		console.log('row', row);
	  }
  disableCourseVisibility(currentIndex, row, status) {
    this.spinner.show();
    var visibilityData = {
			employeeId: row.employeeId,
			visible: status,
			courseId: this.content.id,
			tId: this.userData.data.data.tenantId,
			aId: 33,
		  }
		  this.courseDataService.disableEnrol(visibilityData).then(result => {
      console.log(result);
      this.spinner.hide();
      this.resultdata = result;
      if (this.resultdata.type == false) {
        // var courseUpdate: Toast = {
        //   type: 'error',
        //   title: 'Poll',
        //   body: 'Unable to update visibility of User.',
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.closeEnableDisableCourseModal();
        // this.toasterService.pop(courseUpdate);
        this.presentToast('error', '');
      } else {
        // var courseUpdate: Toast = {
        //   type: 'success',
        //   title: 'Poll',
        //   body: this.resultdata.data,
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // row.visible = !row.visible;
        console.log('after', row.visible)
        this.allEnrolUser();
        // this.toasterService.pop(courseUpdate);
        this.presentToast('success', this.resultdata.data);
      }
    },
      resUserError => {
        this.errorMsg = resUserError;
        // this.closeEnableDisableCourseModal();
      });
  }

  searchEnrolUser(event){
    const val = event.target.value.toLowerCase();
    if(val.length>=3 || (val.length == 0  && event.keyCode!=13))
    {
    // this.allEnrolUser( this.courseDataService.data.data)

    this.temp = [...this.enrolldata];
    console.log(this.temp);
    // filter our data
    const temp = this.temp.filter(function (d) {
      return String(d.ecn).toLowerCase().indexOf(val) !== -1 ||
        d.fullname.toLowerCase().indexOf(val) !== -1 ||
        d.emailId.toLowerCase().indexOf(val) !== -1 ||
        d.enrolmode.toLowerCase().indexOf(val) !== -1 ||
        d.enrolDate.toLowerCase().indexOf(val) !== -1 ||
        d.phoneNo.toLowerCase().indexOf(val) !== -1 ||
        String(d.enrolmode).toLowerCase().indexOf(val) !== -1 ||
        // d.mode.toLowerCase() === val || !val;
        !val
    });

    // update the rows
    this.rows = [...temp];

  }
}
}
