import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EneroltoolsComponent } from './eneroltools.component';

describe('EneroltoolsComponent', () => {
  let component: EneroltoolsComponent;
  let fixture: ComponentFixture<EneroltoolsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EneroltoolsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EneroltoolsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
