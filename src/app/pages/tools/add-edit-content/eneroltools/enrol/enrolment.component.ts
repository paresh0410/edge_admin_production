import { ChangeDetectorRef, Component, ViewEncapsulation, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
// import { AddEditCourseContent } from '../addEditCourseContent';
import { FormGroup, FormArray, FormBuilder, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { enrolService } from './enrolment.service';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { ToastrService } from 'ngx-toastr';

import * as _moment from "moment";
import {
  DateTimeAdapter,
  OWL_DATE_TIME_FORMATS,
  OWL_DATE_TIME_LOCALE,
} from "ng-pick-datetime";
import { MomentDateTimeAdapter } from "ng-pick-datetime-moment";
const moment = (_moment as any).default ? (_moment as any).default : _moment;

//import { AddEditCourseContent } from '../../../plan/courses/addEditCourseContent/addEditCourseContent';

import { ToolsService } from '../../../tools.service';
import { AddEditCourseContentService } from '../../../../plan/courses/addEditCourseContent/addEditCourseContent.service';
import { EnrolmentConfig } from '../../../../../models/enrolment.model';
import * as _ from "lodash";

export const MY_CUSTOM_FORMATS = {
  fullPickerInput: "DD-MM-YYYY",
  parseInput: "DD-MM-YYYY",
  datePickerInput: "DD-MM-YYYY",
  timePickerInput: "LT",
  monthYearLabel: "MMM YYYY",
  dateA11yLabel: "LL",
  monthYearA11yLabel: "MMMM YYYY",
};
@Component({
  selector: 'course-enrolment',
  templateUrl: './enrolment.html',
  styleUrls: ['./enrolment.scss'],
  providers: [DatePipe,
    {
      provide: DateTimeAdapter,
      useClass: MomentDateTimeAdapter,
      deps: [OWL_DATE_TIME_LOCALE],
    },
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS },],
  encapsulation: ViewEncapsulation.None,
})

export class enrolmentComponent {


  colorTheme = 'theme-dark-blue';

  bsConfig: Partial<BsDatepickerConfig>

  @ViewChild('myTable') table: any;
  @ViewChild(DatatableComponent) tableData: DatatableComponent;

  @ViewChild('manualTable') manualTable: any;
  @ViewChild(DatatableComponent) tableDataManual: DatatableComponent;

  @ViewChild('rulesTable') rulesTable: any;
  @ViewChild(DatatableComponent) tableDataRules: DatatableComponent;

  @ViewChild('regTable') regTable: any;
  @ViewChild(DatatableComponent) tableDataReg: DatatableComponent;

  @ViewChild('selfTable') selfTable: any;
  @ViewChild(DatatableComponent) tableDataSelf: DatatableComponent;

  usersList: any = [];
  // selectedUsers = [];
  settingsUsersSelDrop = {};
  settingsprofileSelDrop = {};
  content: any = [];
  dropdownListUsers: any;
  selectedItemsUsers: any;
  dropdownSettingsUsers: any;
  demoData: any = [];

  selected: any = [];
  rows: any = [];
  temp = [];
  minDate = new Date();
  nextDay: any;
  selectedUsers: any = [];
  rowsUsers: any = [];
  tempUsers = [];

  selectedManual: any = [];
  rowsManual: any = [];
  tempManual = [];
  rowsEnrolRule: any = [];
  selectedRules: any = [];
  rowsRules: any = [];
  tempRules = [];

  selectedReg: any = [];
  rowsReg: any = [];
  tempReg = [];
  enrolldata: any = [];
  enrollruledata: any = [];
  enrollselfdata: any = [];
  enrollregdata: any = [];
  selectedSelf: any = [];
  rowsSelf: any = [];
  tempSelf = [];

  // columns = [
  //  	{ prop: 'name' },
  //  	{ name: 'Company' },
  //  	{ name: 'Gender' }
  // ];
  columnsManual = [
    {
      prop: 'selected',
      name: '',
      sortable: false,
      canAutoResize: false,
      draggable: false,
      resizable: false,
      headerCheckboxable: true,
      checkboxable: true,
      width: 30
    },
    { prop: 'ecn', name: 'EMP CODE' },
    { prop: 'fullname', name: 'FULLNAME' },
    { prop: 'gender', name: 'GENDER' },
    { prop: 'doj', name: 'DOJ' },
    { prop: 'department', name: 'DEPARTMENT' },
    { prop: 'mode', name: 'MODE' }
  ];

  columnsRules = [
    {
      prop: 'selected',
      name: '',
      sortable: false,
      canAutoResize: false,
      draggable: false,
      resizable: false,
      headerCheckboxable: true,
      checkboxable: true,
      width: 30
    },
    { prop: 'usersCount', name: 'APPLICABLE USERS' },
    { prop: 'name', name: 'RULE NAME' },
    { prop: 'description', name: 'DESCRIPTION' }
    // { prop: 'dimension', name: 'DIMENSION' },
    // { prop: 'field', name: 'FIELD' },
    // { prop: 'value', name: 'VALUES' }
  ];

  columnsReg = [
    {
      prop: 'selected',
      name: '',
      sortable: false,
      canAutoResize: false,
      draggable: false,
      resizable: false,
      headerCheckboxable: true,
      checkboxable: true,
      width: 30
    },
    { prop: 'enrolDate', name: 'ENROL DATE' },
    { prop: 'dueDays', name: 'DUE DATE (in Days)' },
    { prop: 'reminder', name: 'REMINDER (in Days)' }
  ];

  columnsSelf = [
    {
      prop: 'selected',
      name: '',
      sortable: false,
      canAutoResize: false,
      draggable: false,
      resizable: false,
      headerCheckboxable: true,
      checkboxable: true,
      width: 30
    },
    { prop: 'ecn', name: 'EMP CODE' },
    { prop: 'fullname', name: 'FULLNAME' },
    { prop: 'gender', name: 'GENDER' },
    { prop: 'doj', name: 'DOJ' },
    { prop: 'department', name: 'DEPARTMENT' },
    { prop: 'status', name: 'STATUS' }
  ];

  showEnrolpage: boolean = false;
  enableCourse: boolean = false;

  reviewCheck: any = {
    value1: false,
    value2: false,
    value3: false,
  }

  enrolment: any = {
    manual: true,
    rule: false,
    regulatory: false,
    self: false
  };
  resultdata: any = [];
  showAddRuleModal: boolean = false;
  showAddRegulatoryModal: boolean = false;
  showAddSelfModal: boolean = false;
  enableDisableCourseModal: boolean = false;
  // showAddRuleModal:boolean = false;
  //   showAddRegulatoryModal:boolean = false;
  //   showAddSelfModal:boolean = false;

  ruleType: any = [{
    ruleTypeId: 1,
    ruleTypeName: 'Profile Fields',
  }];

  ruleApplicType: any = [];

  prospectivType: any = []

  ruleSubType: any = [{
    ruleTypeId: 1,
    ruleSubTypeId: 1,
    ruleSubTypeName: 'Username',
  }, {
    ruleTypeId: 1,
    ruleSubTypeId: 2,
    ruleSubTypeName: 'Department',
  }, {
    ruleTypeId: 2,
    ruleSubTypeId: 1,
    ruleSubTypeName: 'Doj',
  }, {
    ruleTypeId: 2,
    ruleSubTypeId: 2,
    ruleSubTypeName: 'Custom date',
  }];


  prospectiveData: any = {
    id: '',
    usersCount: '',
    name: '',
    description: '',
    type: '',
    subType: '',
    value: '',
    prospname: ''
  };
  selfFieldsData: any = {
    enrolSelfId: '',
    sid: '',
    maxCount: '',
    cid: '',
    tid: '',
    userId: '',
    profiles: [],
  };

  searchvalue: any = {
    value: '',
    value1: ''
  };

  ruleData: any = {
    id: 0,
    usersCount: '',
    name: '',
    description: '',
    type: '',
    subType: '',
    value: '',
    prospName: '',
  };

  formdata: any = {
    id: '',
    shortname: '',
    name: '',
    datatype: '',
    selected: ''
  }

  strArrayType: any = [[]];
  selectedFilterOption = [];
  strArrayPar: any = [];
  datarule: any;
  menutypeid: any;
  public addRulesForm: FormGroup;
  controlList: any = [{ datatype: '' }];
  controlFlag: any = false;

  profileFields: any = [];
  errorMsg: any;
  loader: any;

  enabledata: any = [];
  enableuser: any = [];

  private ValueId: number = 0;
  strArrayTypePar: any = [];
  // selectedFilterOption = [];
  selectedRule: any = [];
  // ruleType:any
  selectedRuleType: any = {
    id: ''
  };

  profileFieldSelected: boolean = false;

  prospectivemodeul: boolean = false;
  itemList = [];
  selectedItems = [];
  settings = {};
  strArraySkilllevel: any = [];
  openfilter: any;
  menuType = [];
  datetimeType = [];
  textType = [];
  textareaType = [];
  msg: any;
  msg1: any;
  msg2: any;
  regiD: any;
  regenId: any = '';
  strArrayTypeSelfFields: any = [[]];
  selectedFilterOptionSelf = [];
  strArrayParSelfFields: any = [];

  public addSelfFieldsForm: FormGroup;
  controlListSelfFields: any = [{ datatype: '' }];
  controlFlagSelfFields: any = false;

  profileFieldsSelf: any = [];

  selfType: any = [];

  selfFeildType: any = [{
    selfTypeId: 1,
    selfTypeName: 'Profile Fields',
  }];
  // selfFieldsData:any;
  regularprofiles: any;
  formdataSelf: any = {
    id: '',
    shortname: '',
    name: '',
    datatype: '',
    selected: ''
  }

  // ruleType:any
  selectedSelfFieldsType: any = {
    id: ''
  };

  selfProfileFieldSelected: boolean = false;
  private selfFieldValueId: number = 0;
  strArrayTypeParSelfFields: any = [];
  // selectedFilterOption = [];
  selectedSelfFields: any = [];

  regData: any = {
    id: '',
    enrolDate: '',
    dueDays: '',
    reminder: ''
  }

  public regulatoryForm: FormGroup;

  strArrayTypeRegFilter: any = [[]];
  selectedFilterOptionRegFilter = [];
  strArrayParRegFilter: any = [];
  public addRegFilterForm: FormGroup;
  controlListRegFilter: any = [{ datatype: '' }];
  controlFlagRegFilter: any = false;
  profileFieldsRegFilter: any = [];
  regFilterProfileFieldSelected: boolean = false;
  private regFilterValueId: number = 0;
  strArrayTypeParRegFilter: any = [];
  // selectedFilterOption = [];
  selectedRegFilter: any = [];

  dueDaysArr: any = [{ id: 1, name: 1 },
  { id: 2, name: 2 },
  { id: 3, name: 3 },
  { id: 4, name: 4 },
  { id: 5, name: 5 },
  { id: 6, naAme: 6 },
  { id: 7, name: 7 },
  { id: 8, name: 8 },
  { id: 9, name: 9 },
  { id: 10, name: 10 },
  { id: 11, name: 11 },
  { id: 12, name: 12 },
  { id: 13, name: 13 },
  { id: 14, name: 14 },
  { id: 15, name: 15 },
  { id: 16, name: 16 },
  { id: 17, name: 17 },
  { id: 18, name: 18 },
  { id: 19, name: 19 },
  { id: 20, name: 20 },
  { id: 21, name: 21 },
  { id: 22, name: 22 },
  { id: 23, name: 23 },
  { id: 24, name: 24 },
  { id: 25, name: 25 },
  { id: 26, name: 26 },
  { id: 27, name: 27 },
  { id: 28, name: 28 },
  { id: 29, name: 29 },
  { id: 30, name: 30 }];
  selectedfiltervalue: any = [];
  selectedsettingvalue: any = [];
  selectedrulevalue: any = [];
  regularData: any;
  remDaysArr: any = this.dueDaysArr;
  userLoginData: any;
  toppings = new FormControl();
  toppingList: string[] = ['Extra cheese', 'Mushroom', 'Onion', 'Pepperoni', 'Sausage', 'Tomato'];
  userids = '';
  userdata: any;
  selftypefield: any = [];
  enrolldatarule: any = [];
  contentdata: any = [];
  areaId = 33;
  labelsRule: any = [
    { labelname: 'RULE NAME', bindingProperty: 'rulename', componentType: 'text' },
    { labelname: 'DESCRIPTION', bindingProperty: 'description', componentType: 'text' },
    { labelname: 'APPLICABLE USERS', bindingProperty: 'noOfEmp', componentType: 'text' },
    { labelname: 'ACTION', bindingProperty: 'btntext', componentType: 'button' },
    { labelname: 'EDIT', bindingProperty: 'tenantId', componentType: 'icon' },
    ];
    labelsManual: any = [
      { labelname: 'ECN', bindingProperty: 'ecn', componentType: 'text' },
      { labelname: 'FULL NAME', bindingProperty: 'fullname', componentType: 'text' },
      { labelname: 'EMAIL', bindingProperty: 'emailId', componentType: 'text' },
      { labelname: 'MOBILE', bindingProperty: 'phoneNo', componentType: 'text' },
      { labelname: 'D.0.E', bindingProperty: 'enrolDate', componentType: 'text' },
      { labelname: 'MODE', bindingProperty: 'enrolmode', componentType: 'text' },
      { labelname: 'ACTION', bindingProperty: 'btntext', componentType: 'button' },
      ];
      labelsPreview: any = [
        { labelname: 'ECN', bindingProperty: 'ecn', componentType: 'text' },
        { labelname: 'FULL NAME', bindingProperty: 'fullname', componentType: 'text' },
        { labelname: 'EMAIL', bindingProperty: 'emailId', componentType: 'text' },
        { labelname: 'MOBILE', bindingProperty: 'phoneNo', componentType: 'text' },
        { labelname: 'D.0.E', bindingProperty: 'enrolDate', componentType: 'text' },
        { labelname: 'MODE', bindingProperty: 'enrolmode', componentType: 'text' },
        ];
  isSave: boolean= false;

  // New Changes

  labels4: any = [
    { labelname: "ECN", bindingProperty: "ecn", componentType: "text" },
    {
      labelname: "FULL NAME",
      bindingProperty: "fullname",
      componentType: "text",
    },
    { labelname: "EMAIL", bindingProperty: "emailId", componentType: "text" },
    { labelname: "MOBILE", bindingProperty: "phoneNo", componentType: "text" },
    { labelname: "Date Of Enrolment", bindingProperty: "enrolDate", componentType: "text" },
    { labelname: "MODE", bindingProperty: "enrolmode", componentType: "text" },
    // {
    //   labelname: "ACTION",
    //   bindingProperty: "btntext",
    //   componentType: "button",
    // },
  ];

  config: EnrolmentConfig = {
    manulEnrolmentData: {
      show: true,
      data: [...this.rowsManual],
      labels: this.labelsManual,
      userList: [...this.tempUsers],
      tabTitle: "Manual",
      identifer: "manual",
      helpContent: [],
      selectedUsers: this.selectedUsers,
    },
    ruleBasedEnrolmentData: {
      show: true,
      data: [...this.rowsRules],
      labels: this.labelsRule,
      tabTitle: "Rule Based",
      identifer: "ruleBased",
      ruleData: this.ruleData,
      ruleApplicType: [...this.ruleApplicType],
      prospectivType: [...this.prospectivType],
      profileFields: [...this.profileFields],
      controlList: this.controlList,
      profileFieldSelected: this.profileFieldSelected,
      helpContent: [],
      rowsEnrolRule : this.rowsEnrolRule,
      showEnroleduserPopup : false,
      enrolUserPopupTableLabel: this.labels4,
      showAddRuleModal: this.showAddRuleModal,
    },
    selfEnrolmentData: {
      show: false,
      data: [...this.rowsSelf],
      labels: [],
      tabTitle: "Self",
      identifer: "self",
      helpContent: [],
      showAddSelfModal: this.showAddSelfModal,
      selfFeildType: this.selfFeildType,
      selfFieldsData: this.selfFieldsData,
      selfType: this.selfType,
      isFetchingSettings: false,
      profileFieldsSelf: this.profileFieldsSelf,
      controlList: this.controlList,
    },
    regulatoryEnrolmentData: {
      show: false,
      data: [...this.rowsReg],
      labels: [],
      tabTitle: "Regulatory",
      identifer: "regulatory",
      helpContent: [],
      controlList: this.controlList,
      profileFieldsRegFilter: this.profileFieldsRegFilter,
      showAddRegulatoryFilterModal: false,
      regFilterProfileFieldSelected: this.regFilterProfileFieldSelected,
      regularData: this.regularData,
    },
    priceBasedEnrolmentData: {
      show: false,
      data: [],
      labels: [],
      tabTitle: "Pricing",
      identifer: "pricing",
      helpContent: [],
      courseId: null,
      currencyTypeDropDown: [],
      discountListDropdownList: [],
      addEditPriceForm: [],
      showSidebar: false,
    },
  };

  constructor(private _fb: FormBuilder,
    private toast: ToastrService,
    private http: HttpClient,
    private addEditCourseService: AddEditCourseContentService,
    private enrolService: enrolService, public cdf: ChangeDetectorRef,
    private spinner: NgxSpinnerService, public toolsservice: ToolsService,
    private router: Router, private datePipe: DatePipe) {
    this.settings = {
      text: 'Select ',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      classes: 'myclass custom-class',
      primaryKey: 'id',
      labelKey: 'name',
      enableSearchFilter: true,
      lazyLoading: true,
      badgeShowLimit: 2,
      maxHeight: 150
    };
    var day = new Date();
    this.nextDay = new Date(day);
    this.nextDay.setDate(day.getDate() + 1);

    if (localStorage.getItem('LoginResData')) {
      this.userLoginData = JSON.parse(localStorage.getItem('LoginResData'));
      this.userdata = this.userLoginData.data.data;
      console.log('login data', this.userdata);
    }
    this.contentdata = this.toolsservice.contentdata;
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });

    this.addRulesForm = new FormGroup({
      FilterOpt: new FormControl(),
      Value1: new FormControl(),
      Value2: new FormControl(),
    });

    this.addSelfFieldsForm = new FormGroup({
      FilterOpt: new FormControl(),
      Value1: new FormControl(),
      Value2: new FormControl(),
    });

    this.addRegFilterForm = new FormGroup({
      FilterOpt: new FormControl(),
      Value1: new FormControl(),
      Value2: new FormControl(),
    });

    this.regulatoryForm = new FormGroup({
      enrolDate: new FormControl(),
      dueDays: new FormControl(),
      remDays: new FormControl(),
    });

    this.settingsUsersSelDrop = {
      text: 'Select Users',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      classes: 'myclass custom-class',
      primaryKey: 'ecn',
      labelKey: 'fullname',
      noDataLabel: 'Search Users...',
      enableSearchFilter: true,
      searchBy: ['ecn', 'fullname'],
      lazyLoading: true,
      badgeShowLimit: 3,
      maxHeight:250,
    };

    this.fetchManual((data) => {
      // cache our list
      this.tempManual = [...data];
      this.rowsManual = data;
    });

    this.fetchUnEnrolledUsers((data) => {
      // cache our list
      this.tempUsers = [...data];
      // this.rowsUsers = data;
    });

    this.fetchRules((data) => {
      // cache our list
      this.tempRules = [...data];
      this.rowsRules = data;
      // this.rowsRules.push(this.tempRuleData);
    });

    this.fetchReg((data) => {
      // cache our list
      this.tempReg = [...data];
      this.rowsReg = data;
      // this.rowsRules.push(this.tempRuleData);
    });

    this.fetchSelf((data) => {
      // cache our list
      this.tempSelf = [...data];
      this.rowsSelf = data;
      // this.rowsRules.push(this.tempRuleData);
    });

    this.getUserProfileFields();
  }

  ngOnInit() {
    this.getHelpContent()
    this.ruledropdownmenu();
    this.getUserProfileFields();
    this.addRulesForm = this._fb.group({
      rules: this._fb.array([
        // this.initRules(),
      ]),
    });

    this.addSelfFieldsForm = this._fb.group({
      fields: this._fb.array([
        // this.initRules(),
      ]),
    });

    this.addRegFilterForm = this._fb.group({
      filters: this._fb.array([
        // this.initRegFilter(),
      ]),
    });

    this.regulatoryForm = this._fb.group({
      fields: this._fb.array([
        this.initRegForm(),
        this.initRegForm(),
        this.initRegForm(),
      ]),
    });

    this.makeCourseDataReady();

  }

  selfdatafield() {
    let data = {
      lovtype: 15,
      tId: this.userdata.tenantId,
    };
    this.enrolService.getselfdropdownlist(data).then(res => {
      console.log(res);
      this.selftypefield = res;
      this.selfType = this.selftypefield.data[0];
      console.log(this.selfType);
    });
  }
  ruledropdownmenu() {
    this.enrolService.dropdown().then(res => {
      console.log(res['data']);
      this.ruleApplicType = res['data'][0];
      this.prospectivType = res['data'][1];
      console.log('dropdown', this.ruleApplicType);
      console.log('dropdown', this.prospectivType);
      this.config.ruleBasedEnrolmentData.ruleApplicType = [
        ...this.ruleApplicType,
      ];
      this.config.ruleBasedEnrolmentData.prospectivType = [
        ...this.prospectivType,
      ];
    });
  }

  makeCourseDataReady() {
    this.content;
    if (this.addEditCourseService.data) {
      this.content = this.addEditCourseService.data.data;
      if (this.content == undefined) {
        this.content = {
          courseId: this.addEditCourseService.data.courseId,
        };
        console.log('content', this.content);
      }
      console.log('content', this.content);
      this.allEnrolUser();
      this.selfdatafield();
    } else {
      this.showEnrolpage = !this.showEnrolpage;
    }
    this.cdf.detectChanges();
  }

  /*------------------Rule list -----------*/
  allruleList() {
    this.spinner.show();
    var data = {
      areaId: this.areaId,
      instanceId: this.toolsservice.conId,
      tId: this.userdata.tenantId,
    };
    console.log(data);
    this.enrolService.getallrule(data).then(enrolData => {
      console.log(enrolData);
      this.spinner.hide();
      this.enrollruledata = enrolData['data'];
      this.rowsRules = enrolData['data'];
      this.rowsRules = [...this.rowsRules];
      for (let i = 0; i < this.rowsRules.length; i++) {
        if(this.rowsRules[i].visible == 1) {
          this.rowsRules[i].btntext = 'fa fa-eye';
          } else {
          this.rowsRules[i].btntext = 'fa fa-eye-slash';
          }
        }
      console.log('RULE', this.rowsRules);
      this.passDataToChild();
      this.cdf.detectChanges();
    });
  }

  /*------------------------Disable rule------------*/
  visibilityTableRowRule(row) {
		let value;
		let status;

		if (row.visible == 1) {
		  row.btntext = 'fa fa-eye-slash';
		  value  = 'fa fa-eye-slash';
		  row.visible = 0
		  status = 0;
		} else {
		  status = 1;
		  value  = 'fa fa-eye';
		  row.visible = 1;
		  row.btntext = 'fa fa-eye';
		}

		for(let i =0; i < this.rowsRules.length; i++) {
		  if(this.rowsRules[i].employeeId == row.employeeId) {
			this.rowsRules[i].btntext = row.btntext;
			this.rowsRules[i].visible = row.visible
		  }
    }
    var visibilityData = {
      enrolRuleId: row.enrolRuleId,
      visible: status
    };
    console.log(visibilityData);
    this.enrolService.disableRule(visibilityData).then(result => {
      this.spinner.hide();
      console.log(result);
      this.loader = false;
      this.resultdata = result;
      if (this.resultdata.type == false) {
        this.toast.error('Unable to update visibility of Rule.', 'Error');
      } else {

        console.log('after', row.visible);
        this.allruleList();
        this.toast.success(this.resultdata.data, 'Success');

      }
      this.cdf.detectChanges();
    },
      resUserError => {
        this.loader = false;
        this.errorMsg = resUserError;
        this.spinner.show();
      });

    console.log('row', row);
    this.passDataToChild();
	  }
  // disableRuleVisibility(currentIndex, row, status) {
  //   this.spinner.show();
  //   var visibilityData = {
  //     enrolRuleId: row.enrolRuleId,
  //     visible: status
  //   };
  //   console.log(visibilityData);
  //   this.enrolService.disableRule(visibilityData).then(result => {
  //     this.spinner.hide();
  //     console.log(result);
  //     this.loader = false;
  //     this.resultdata = result;
  //     if (this.resultdata.type == false) {
  //       // var courseUpdate: Toast = {
  //       //   type: 'error',
  //       //   title: 'Course',
  //       //   body: 'Unable to update visibility of Rule.',
  //       //   showCloseButton: true,
  //       //   timeout: 2000
  //       // };
  //       // // this.closeEnableDisableCourseModal();
  //       // this.toasterService.pop(courseUpdate);
  //       this.toast.error('Unable to update visibility of Rule.', 'Error');
  //     } else {
  //       // var courseUpdate: Toast = {
  //       //   type: 'success',
  //       //   title: 'Course',
  //       //   body: this.resultdata.data,
  //       //   showCloseButton: true,
  //       //   timeout: 2000
  //       // };
  //       // row.visible = !row.visible;
  //       console.log('after', row.visible);
  //       this.allruleList();
  //       this.toast.success(this.resultdata.data, 'Success');
  //       // this.toasterService.pop(courseUpdate);
  //     }
  //     this.cdf.detectChanges();
  //   },
  //     resUserError => {
  //       this.loader = false;
  //       this.errorMsg = resUserError;
  //       this.spinner.show();
  //       // this.closeEnableDisableCourseModal();
  //     });
  // }

  /*----------------Rule add ---------*/
  saveRule(f) {

    // this.loader =true;
    // console.log('Events',event);
    if (f.valid) {
      this.isSave = true;
      let rules: any = this.addRulesForm.value.rules;
      this.ruleData.rules = rules;
      console.log('Rule data', this.ruleData);

      this.makeRuleDataready(this.ruleData);
    }
    else {
      console.log('Please Fill all fields');
      Object.keys(f.controls).forEach(key => {
        f.controls[key].markAsDirty();
      });
    }
    this.isSave = false;

  }

  /*---------------Rule Edit----------*/
  editrule(row) {
    console.log(row);
    this.openRuleModal(row, 1);
    // this.openRulemodel23(row);
  }

  /*--------------- enrolled User (manual)----------------------*/
  allEnrolUser() {
    this.spinner.show();
    var data = {
      areaId: this.areaId,
      instanceId: this.toolsservice.conId,
      tId: this.userdata.tenantId,
      mode: 1
    }
    console.log(data);
    this.addEditCourseService.getallenroluser(data).then(enrolData => {
      this.spinner.hide();
      this.enrolldata = enrolData['data'];
      this.rowsManual = enrolData['data'];
      this.rowsManual = [...this.rowsManual];
      for (let i = 0; i < this.rowsManual.length; i++) {
        // this.rowsManual[i].Date = new Date(this.rowsManual[i].enrolDate);
        // this.rowsManual[i].enrolDate = this.formdate(this.rowsManual[i].Date);
        if(this.rowsManual[i].visible == 1) {
          this.rowsManual[i].btntext = 'fa fa-eye';
          } else {
          this.rowsManual[i].btntext = 'fa fa-eye-slash';
          }
      }
      console.log('EnrolledUSer', this.rowsManual);
      if (this.enrolldata.visible = 1) {
        this.enableCourse = false;
      } else {
        this.enableCourse = true;
      }
      this.cdf.detectChanges();
      this.passDataToChild();
    });

  }
  /*--------------- Unenrolled User (manual)----------------------*/

  allUNEnrolUser(evt) {
    const data = {
      courseId: this.toolsservice.conId,
      tId: this.userdata.tenantId,
      searchStr: evt,
      aId: 33,
    }
    // console.log(data);
    if (evt.length > 3) {
      this.enrolService.getallunenroluser(data).then(enrolData => {
        console.log(enrolData);
        this.enrolldata = enrolData['data'];
        // this.usersList = enrolData.data;
        this.tempUsers = enrolData['data'];
        this.tempUsers = [...this.tempUsers];
        // if(this.enrolldata.length == 0){
        // 	this.showEnrolpage = !this.showEnrolpage;
        // }
        console.log('EnrolledUSer ', this.tempUsers);
      })
      this.cdf.detectChanges();
    }
  }

  fetchAllUnEnrolUsersAsync(params: any, cb) {
    const data = {
      courseId: this.toolsservice.conId,
      tId: this.userdata.tenantId,
      searchStr: params,
      aId: 33,
    };
    this.enrolService.getallunenroluser(data).then(res => {
      cb(res);
    }, err => {
      console.log(err);
    });
  }

  /*-----------------disable manual user-------------*/
  visibilityTableRowManual(row) {
		let value;
		let status;

		if (row.visible == 1) {
		  row.btntext = 'fa fa-eye-slash';
		  value  = 'fa fa-eye-slash';
		  row.visible = 0
		  status = 0;
		} else {
		  status = 1;
		  value  = 'fa fa-eye';
		  row.visible = 1;
		  row.btntext = 'fa fa-eye';
		}

		for(let i =0; i < this.rowsManual.length; i++) {
		  if(this.rowsManual[i].employeeId == row.employeeId) {
			this.rowsManual[i].btntext = row.btntext;
			this.rowsManual[i].visible = row.visible
		  }
      var visibilityData = {
        employeeId: row.employeeId,
        visible: status,
        courseId: this.toolsservice.conId,
        tId: this.userdata.tenantId,
        aId: 33,
      }
      this.addEditCourseService.disableEnrol(visibilityData).then(result => {
        console.log(result);
        this.spinner.hide();
        this.loader = false;
        this.resultdata = result;
        if (this.resultdata.type == false) {

          this.toast.error('Unable to update visibility of User.', 'Error');

        } else {

          console.log('after', row.visible)
          this.allEnrolUser();
          this.toast.success(this.resultdata.data, 'Success');
        }
      },
        resUserError => {
          this.loader = false;
          this.errorMsg = resUserError;
          this.spinner.show();
          // this.closeEnableDisableCourseModal();
        });

		console.log('row', row);
    }
  }
  // disableCourseVisibility(currentIndex, row, status) {
  //   this.spinner.show();
  //   var visibilityData = {
  //     employeeId: row.employeeId,
  //     visible: status,
  //     courseId: this.toolsservice.conId,
  //     tId: this.userdata.tenantId,
  //     aId: 33,
  //   }
  //   this.addEditCourseService.disableEnrol(visibilityData).then(result => {
  //     console.log(result);
  //     this.spinner.hide();
  //     this.loader = false;
  //     this.resultdata = result;
  //     if (this.resultdata.type == false) {
  //       // var courseUpdate: Toast = {
  //       //   type: 'error',
  //       //   title: 'Course',
  //       //   body: 'Unable to update visibility of User.',
  //       //   showCloseButton: true,
  //       //   timeout: 2000
  //       // };
  //       this.toast.error('Unable to update visibility of User.', 'Error');
  //       // this.closeEnableDisableCourseModal();
  //       // this.toasterService.pop(courseUpdate);
  //     } else {
  //       // var courseUpdate: Toast = {
  //       //   type: 'success',
  //       //   title: 'Course',
  //       //   body: this.resultdata.data,
  //       //   showCloseButton: true,
  //       //   timeout: 2000
  //       // };
  //       // row.visible = !row.visible;
  //       console.log('after', row.visible)
  //       this.allEnrolUser();
  //       this.toast.success(this.resultdata.data, 'Success');
  //       // this.toasterService.pop(courseUpdate);
  //     }
  //   },
  //     resUserError => {
  //       this.loader = false;
  //       this.errorMsg = resUserError;
  //       this.spinner.show();
  //       // this.closeEnableDisableCourseModal();
  //     });
  // }

  enableDisableCourseAction(actionType) {
    // if(actionType == true){
    //   if(this.enabledata == 1){
    //     this.enabledata = 0;
    //     // var courseData = this.content[this.courseDisableIndex];
    //     this.enableDisableCourse(this.enableuser);
    //   }else{
    //     this.enabledata = 1;
    //     // var courseData = this.content[this.courseDisableIndex];
    //     this.enableDisableCourse(this.enableuser);
    //   }
    // }else{
    //   this.closeEnableDisableCourseModal();
    // }
  }

  /*-----------------add enroluser-----------------*/

  manEnrolUser() {
    console.log('Selected user ', this.selectedUsers);


    if (this.selectedUsers.length > 0) {
      this.userids = '';
      this.spinner.show();
      for (let i = 0; i < this.selectedUsers.length; i++) {
        var user = this.selectedUsers[i];
        // this.userids = id;
        if (this.userids != '') {
          this.userids += '|';
        }
        if (String(user.id) != '' && String(user.id) != 'null') {
          this.userids += user.id;
        }
        console.log('abc', this.userids);

      }
      var data = {
        empIds: this.userids,
        tId: this.userdata.tenantId,
        courseId: this.toolsservice.conId,
        modeId: 1,
        visible: 1,
        areaId: this.areaId,
        uId: this.userdata.id,
      }
      console.log('Selected data', data);
      this.enrolService.addenroluser(data).then(Response => {
        console.log(Response);
        this.spinner.hide();
        if (Response['type'] == true) {
          // var enrolUsersToast: Toast = {
          //   type: 'success',
          //   title: 'Course',
          //   body: 'User(s) enrolled successfully.',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(enrolUsersToast);
          this.toast.success('User(s) enrolled successfully.', 'Success');
          this.allEnrolUser();
          // this.selectedUsers.visible = 1;
          // for(let i=0; i< this.selectedUsers.length; i++){
          //   this.rowsManual.push(this.selectedUsers[i]);
          // }
        } else {
          // var enrolUsersToast: Toast = {
          //   type: 'error',
          //   title: 'Course',
          //   body: 'Unable to enrol user(s) at this time, Please try again later.',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(ensaaaaaaaaaaaaaaaarolUsersToast);
          this.toast.error('Unable to enrol user(s) at this time, Please try again later.', 'Error');
        }
      })

    } else {
      // var courseUpdate: Toast = {
      //   type: 'error',
      //   title: 'Course',
      //   body: 'Please select a User.',
      //   showCloseButton: true,
      //   timeout: 2000
      // };
      // // this.closeEnableDisableCourseModal();
      // this.toasterService.pop(courseUpdate);
      this.toast.info('Please select a User.', 'Info');
    }

    if (this.tempManual.length > 0) {
      for (let j = 0; j < this.tempManual.length; j++) {
        var user = this.tempManual[j];
        for (let i = 0; i < this.selectedUsers.length; i++) {
          if (user.ecn == this.selectedUsers[i].ecn) {
            this.tempManual.splice(j, 1);
          }
        }
      }
    }


    this.selectedUsers = [];
    this.tempUsers = [];
    this.tempUsers = [...this.tempUsers];
    this.usersList = [];
    this.rowsManual = [...this.rowsManual];
    this.tempManual = this.rowsManual;
    console.log('Updated enrolled users ', this.rowsManual);
    this.passDataToChild();
  }

  initRegFilter() {
    return this._fb.group({
      FilterOpt: [''],
      Value1: [''],
      Value2: ['']
    });
  }

  prepareRegFilter() {
    this.selectedFilterOptionRegFilter = [];
    this.controlListRegFilter = [{ datatype: '' }];
    this.clearRegFilter();
    this.disableSelectRegFilter();
    this.addRegFilterForm = this._fb.group({
      filters: this._fb.array([
        this.initRegFilter(),
      ]),
    });
  }

  addRegulatoryFilter() {

    let defualtRegsObj = {
      enrolProfileId: 0,
      field: '',
      fieldValues: ''
    };
    this.regularData.profiles.push(defualtRegsObj);
    this.controlFlag = true;
    this.controlList.push(0);
    this.strArrayType.push([]);
    this.passDataToChild();
    this.cdf.detectChanges();
  }

  removeRegFilter(currentIndex) {

    this.regularData.profiles.splice(currentIndex, 1);
    this.selectedFilterOption.splice(currentIndex, 1);
    this.disableSelectedRuleFieldType();
    this.controlFlag = true;
    this.controlList.splice(currentIndex, 1);
    this.strArrayType.splice(currentIndex, 1);
    this.passDataToChild();
  }

  clearRegFilter() {
    const arr = <FormArray>this.addRegFilterForm.controls.filters;
    arr.controls = [];
    this.addRegFilterForm.reset({
      FilterOpt: [''],
      Value1: [''],
      Value2: ['']
    })
  }

  regFilterTypeSelected() {
  }

  callTypeRegFilter(id: any, index: any, selectedField) {
    if (this.strArrayTypeRegFilter[index]) {
      this.strArrayTypeRegFilter[index] = [];
    }
    this.passDataToChild();

    this.regFilterValueId = parseInt((id.srcElement || id.target).value);
    //this.disableSelect();

    // this.controlList[index] = this.ValueId;
    for (let i = 0; i < this.profileFieldsRegFilter.length; i++) {
      if (this.profileFieldsRegFilter[i].id == this.regFilterValueId) {
        // this.selectedRuleType = this.profileFields[i].datatype;

        this.controlListRegFilter[index] = this.profileFieldsRegFilter[i];

        this.strArrayTypeRegFilter[index].push(this.profileFieldsRegFilter[i]);
        if (this.selectedFilterOptionRegFilter.length > 0) {
          this.selectedFilterOptionRegFilter[index] = this.profileFieldsRegFilter[i].shortname;
        } else {
          this.selectedFilterOptionRegFilter.push(this.profileFieldsRegFilter[i].shortname);
        }
      }
    }
    this.disableSelectRegFilter();
    this.passDataToChild();
  }

  disableSelectRegFilter() {
    this.profileFieldsRegFilter.forEach((data, key) => {
      if (this.selectedFilterOptionRegFilter.indexOf(data.shortname) >= 0) {
        this.profileFieldsRegFilter[key].selected = 'true';
      } else {
        this.profileFieldsRegFilter[key].selected = 'false';
      }
    })
    console.log('Selected Disabled', this.strArrayParRegFilter);
  }

  showAddRegulatoryFilterModal: boolean = false;
  openRegulatoryFilterModal() {
    // this.showAddRegulatoryFilterModal = true;
    this.filterdata();
    this.passDataToChild();
  }
  filterdata() {
    var data = {
      courseId: this.content.courseId,
      TId: this.userdata.tenantId,
      eModeId: 3,
      eSetId: this.regiD
    }
    if (data.eSetId == '' || data.eSetId == null || data.eSetId == undefined) {
      data.eSetId = 0;
    }
    console.log(data);

    this.enrolService.getfilter(data).then(res => {
      this.regularprofiles = res['data'][0];
      console.log(this.regularprofiles);
      this.regulatorydata(this.regularprofiles);
      this.cdf.detectChanges();
    });
  }
  regulatorydata(data) {
    console.log(data);
    var allstring = '';
    if (data == '' || data == undefined || data == null) {
      this.regularData = {
        modeId: '',
        aId: '',
        cid: '',
        profiles: [],
        tid: '',
        createrId: ''
      };
      this.addRegulatoryFilter();
      this.msg1 = 'You have successfully added a filter';
    } else {

      this.regularData = {
        modeId: '',
        aId: '',
        cid: '',
        profiles: this.regularprofiles,
        tid: '',
        createrId: '',
      };
      this.msg1 = 'You have successfully update a filter'
      if (this.regularData.profiles.length > 0) {
        for (let i = 0; i < this.regularData.profiles.length; i++) {
          let rule = this.regularData.profiles[i];
          this.callRuleFieldType(rule.field, i, 1);
          console.log(this.datarule);
          if (this.datarule == 'datetime') {
            var array = this.regularData.profiles[i].fieldValues.split('$');
            this.regularData.profiles[i].fieldValues = [new Date(array[0]), new Date(array[1])];
            // this.regularData.profiles[i].fieldValues = new Date(this.regularData.profiles[i].fieldValues);
          }
          if (this.datarule == 'menu') {
            this.databind(this.regularData.profiles[i], i);
          }

        }
        this.profileFieldSelected = true;
      }

      console.log('Edit regulatory data ', this.regularData);
    }
    this.showAddRegulatoryFilterModal = true;
  }
  databind(alldata, i) {
    var array = alldata.fieldValues.split(',');
    let newarray = []
    let data = {
      lovtype: this.menutypeid,
      tId: this.userdata.tenantId
    }
    this.enrolService.getprofileFieldDropdown(data).then(res => {
      console.log(res);
      if (res['type'] == true) {
        let strArraySkilllevel = res['data'][0];
        this.strArraySkilllevel = [...strArraySkilllevel];
        // this.itemList = this.strArraySkilllevel;
        // // this.itemList = this.controlList[i].subtype;
        // console.log(this.itemList);
        this.profileFields[i].subtype = this.strArraySkilllevel;
        // this.cdf.detectChanges();
        array.forEach(element => {
          strArraySkilllevel.forEach(dataitem => {
            if (dataitem.name == element)
              newarray.push(dataitem);
          });
        });
      }
      this.cdf.detectChanges();
    });
    this.selectedfiltervalue[i] = newarray;
  }
  closeRegulatoryFilterModal() {
    this.showAddRegulatoryFilterModal = false;
    this.regularData = {};

    this.disableSelectRegFilter();
    // this.prepareRegFilter();
    this.passDataToChild();
  }

  selectedDueDate: any;
  callTypeRegDueDays(id, curIndex, field) {

    console.log('final rem days ', id, curIndex, field);
  }

  initRegForm() {
    return this._fb.group({
      enrolDate: [''],
      // dueDays: [''],
      // remDays: ['']
    });
  }

  addRegForm() {
    const control = <FormArray>this.regulatoryForm.controls['fields'];
    control.push(this.initRegForm());
  }

  removeRegForm(i: number) {
    const control = <FormArray>this.regulatoryForm.controls['fields'];
    control.removeAt(i);
  }

  makeRulesReady() {
    for (let i = 0; i < this.profileFields.length; i++) {
      let field = this.profileFields[i];
      if (field.datatype == 'menu') {
        this.menuType.push(field);
      }
      if (field.datatype == 'datetime') {
        this.datetimeType.push(field);
      }
      if (field.datatype == 'text') {
        this.textType.push(field);
      }
      if (field.datatype == 'textarea') {
        this.textareaType.push(field);
      }
    }
  }

  onItemSelect(item: any) {
    console.log(item);
    console.log(this.selectedUsers);
  }
  OnItemDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedUsers);
  }
  onSelectAll(items: any) {
    console.log(items);
  }
  onDeSelectAll(items: any) {
    console.log(items);
  }


  /*---------------- list of regulatory----------*/
  allregulatorylist(content) {
    this.spinner.show();
    var data = {
      courseId: this.toolsservice.conId,
      tId: this.userdata.tenantId,
    }
    console.log(data);
    this.enrolService.getallregulatory(data).then(res => {
      console.log(res);
      this.spinner.hide();
      this.enrollregdata = res['data'];
      this.rowsReg = res['data'];

      for (var i = 0; i < this.rowsReg.length; i++) {
        // this.rowsReg[i].enrolDate = this.formdate(this.rowsReg[i].enrolDate);
        this.regiD = this.rowsReg[0].enrolRegId;
      }
      this.rowsReg = [...this.rowsReg];
      console.log(this.rowsReg);
      this.cdf.detectChanges();

    });
    this.rowsReg;

  }

  /*-------------- Disable Regulatory------------*/
  disableregulatoryVisibility(currentIndex, row, status) {
    this.spinner.show();
    var visibilityData = {
      enrolRegDtId: row.enrolRegDtId,
      visible: status
    }
    console.log(visibilityData);
    this.enrolService.disableregulatory(visibilityData).then(result => {
      console.log(result);
      this.spinner.hide();
      this.loader = false;
      this.resultdata = result;
      if (this.resultdata.type == false) {
        // var courseUpdate: Toast = {
        //   type: 'error',
        //   title: 'Course',
        //   body: 'Unable to update visibility of Rule.',
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // // this.closeEnableDisableCourseModal();
        // this.toasterService.pop(courseUpdate);
        this.toast.error('Unable to update visibility of Rule.', 'Error');

      } else {
        // var courseUpdate: Toast = {
        //   type: 'success',
        //   title: 'Course',
        //   body: this.resultdata.data,
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // row.visible = !row.visible;
        console.log('after', row.visible)
        this.allregulatorylist(this.addEditCourseService.data.data);
        // this.toasterService.pop(courseUpdate);
        this.toast.success(this.resultdata.data, 'Success');
      }

    },
      resUserError => {
        this.loader = false;
        this.errorMsg = resUserError;
        this.spinner.hide();
        // this.closeEnableDisableCourseModal();
      });
    this.cdf.detectChanges();
  }
  /* --------------Add regulatory Date------------ */
  saveReg(data) {
    console.log(data);
    // console.log('Regulatory data',this.regData);
    // this.rowsReg.push();
    if (data.enrolDate != '') {
      this.makeRegDataReady(data);
    } else {
      // var courseUpdate: Toast = {
      //   type: 'error',
      //   title: 'Course',
      //   body: 'Please select a Date',
      //   showCloseButton: true,
      //   timeout: 2000
      // };
      // this.toasterService.pop(courseUpdate);
      this.toast.info('Please select a Date', 'Info');

    }
  }
  clearesearch() {

    this.searchvalue = {};
    this.rowsManual = [...this.enrolldata]
    this.rowsRules = [...this.enrollruledata];
    this.rowsEnrolRule = [...this.enrolldatarule]
    this.passDataToChild();

  }
  onSearch(evt: any) {
    console.log(evt.target.value);
    // this.allUNEnrolUser(evt.target.value);
    const val = evt.target.value;
    if (val.length >= 3) {
      this.fetchAllUnEnrolUsersAsync(val, (enrolData) => {
        if (enrolData.type === true) {
          //this.enrolldata = enrolData['data'];
          this.tempUsers = enrolData['data'];
          this.tempUsers = [...this.tempUsers];
          console.log('EnrolledUSer ', this.tempUsers);

          const temp = this.tempUsers.filter(function (d) {
            return String(d.ecn).toLowerCase().indexOf(val) !== -1 ||
              d.fullname.toLowerCase().indexOf(val) !== -1 || !val;
          });

          // update the rows
          this.usersList = temp;
          this.cdf.detectChanges();
        } else {
          console.log('Error getting EnrolledUSer ', enrolData);
          this.tempUsers = [];
          this.tempUsers = [...this.tempUsers];
          this.usersList = [];
          this.cdf.detectChanges();
        }
      });
    } else {
      // this.subscription.unsubscribe();
    }
    this.passDataToChild();
  }

  getUserProfileFields() {
    this.enrolService.getProfileFields()
      .then(rescompData => {
        console.log('profile', rescompData);
        // this.loader =false;
        this.profileFields = rescompData['data'][0];

        for (let i = 0; i < this.profileFields.length; i++) {
          this.profileFields[i].subtype = [];
        }
        this.profileFieldsSelf = rescompData['data'][0];
        this.profileFieldsRegFilter = rescompData['data'][0];
        // this.topic = rescompData.data[0];
        console.log('User profile fields', this.profileFields);
      },
        resUserError => {
          // this.loader =false;
          this.errorMsg = resUserError;
        });
  }

  fetchUnEnrolledUsers(cb) {
    // const req = new XMLHttpRequest();
    // req.open('GET', `assets/data/unEnroledUsers.json`);

    // req.onload = () => {
    //   cb(JSON.parse(req.response));
    // };

    // req.send();
  }

  fetchRules(cb) {
    // const req = new XMLHttpRequest();
    // req.open('GET', `assets/data/rules.json`);

    // req.onload = () => {
    //   cb(JSON.parse(req.response));
    // };

    // req.send();
  }

  fetchManual(cb) {
    // const req = new XMLHttpRequest();
    // // req.open('GET', `assets/data/company.json`);
    // req.open('GET', `assets/data/enroledUsers.json`);
    // // req.open('GET', `assets/data/100k.json`);

    // req.onload = () => {
    //   cb(JSON.parse(req.response));
    // };

    // req.send();
  }

  fetchReg(cb) {
    // const req = new XMLHttpRequest();
    // // req.open('GET', `assets/data/company.json`);
    // req.open('GET', `assets/data/regulatory.json`);
    // // req.open('GET', `assets/data/100k.json`);

    // req.onload = () => {
    //   cb(JSON.parse(req.response));
    // };

    // req.send();
  }

  fetchSelf(cb) {
    // const req = new XMLHttpRequest();
    // // req.open('GET', `assets/data/company.json`);
    // req.open('GET', `assets/data/self.json`);
    // // req.open('GET', `assets/data/100k.json`);

    // req.onload = () => {
    //   cb(JSON.parse(req.response));
    // };

    // req.send();
  }

  onSelect({ selected }) {
    console.log('Select Event', selected, this.selected);

    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }

  toggleExpandRow(row) {
    console.log('Toggled Expand Row!', row);
    this.table.rowDetail.toggleExpandRow(row);
  }

  onDetailToggle(event) {
    console.log('Detail Toggled', event);
  }

  onActivate(event) {
    // console.log('Activate Event', event);
    if (event.type === 'checkbox') {
      // Stop event propagation and let onSelect() work
      console.log('Checkbox Selected', event);
      event.event.stopPropagation();
    } else if (event.type === 'click' && event.cellIndex != 0) {
      // Do somethings when you click on row cell other than checkbox
      console.log('Row Clicked', event.row); /// <--- object is in the event row variable
    }
  }

  selectTab(tabEvent) {
    console.log('tab Selected', tabEvent);
  }

  searchManEnrol(event) {
    const val = event.target.value.toLowerCase();
    if(val.length>=3 || (val.length == 0  && event.keyCode!=13))
    {
    // this.allEnrolUser(this.addEditCourseService.data.data);
    // this.cdf.detectChanges();
    this.temp = [...this.enrolldata]
    const temp = this.temp.filter(function (d) {
      return String(d.ecn).toLowerCase().indexOf(val) !== -1 ||
        d.fullname.toLowerCase().indexOf(val) !== -1 ||
        d.emailId.toLowerCase().indexOf(val) !== -1 ||
        d.enrolmode.toLowerCase().indexOf(val) !== -1 ||
        d.enrolDate.toLowerCase().indexOf(val) !== -1 ||
        d.phoneNo.toLowerCase().indexOf(val) !== -1 ||
        String(d.enrolmode).toLowerCase().indexOf(val) !== -1 ||
        !val
    });

    // update the rows
    this.rowsManual = [...temp];
    // Whenever the filter changes, always go back to the first page
    // this.tableDataManual.offset = 0;
    this.passDataToChild();
  }
}
  searchEnrolRule(event) {
    const val = event.target.value.toLowerCase();
    if(val.length>=3 || (val.length == 0  && event.keyCode!=13))
    {
    // this.allEnrolUser(this.addEditCourseService.data.data);
    // this.cdf.detectChanges();
    this.temp = [...this.enrolldatarule]
    const temp = this.temp.filter(function (d) {
      return String(d.ecn).toLowerCase().indexOf(val) !== -1 ||
        d.fullname.toLowerCase().indexOf(val) !== -1 ||
        d.emailId.toLowerCase().indexOf(val) !== -1 ||
        d.enrolmode.toLowerCase().indexOf(val) !== -1 ||
        d.enrolDate.toLowerCase().indexOf(val) !== -1 ||
        d.phoneNo.toLowerCase().indexOf(val) !== -1 ||
        String(d.enrolmode).toLowerCase().indexOf(val) !== -1 ||
        !val
    });

    // update the rows
    this.rowsEnrolRule = [...temp];
    // Whenever the filter changes, always go back to the first page
    // this.tableDataManual.offset = 0;
  }
}
  searchRuleEnrol(event) {
    const val = event.target.value.toLowerCase();
    if(val.length>=3 || (val.length == 0  && event.keyCode!=13))
    {
    // this.allruleList(this.addEditCourseService.data.data);

    this.tempRules = [...this.enrollruledata];


    const temp = this.tempRules.filter(function (d) {
      return String(d.noOfEmp).toLowerCase().indexOf(val) !== -1 ||
        String(d.rulename).toLowerCase().indexOf(val) !== -1 ||
        String(d.description).toLowerCase().indexOf(val) !== -1 ||
        // String(d.dimension).toLowerCase().indexOf(val) !== -1 ||
        // String(d.field).toLowerCase().indexOf(val) !== -1 ||
        // String(d.value).toLowerCase().indexOf(val) !== -1 ||
        !val;
    });

    // update the rows
    this.rowsRules = [...temp];
    // Whenever the filter changes, always go back to the first page
    // this.tableDataRules.offset = 0;
    this.passDataToChild();
  }
}

  searchRegEnrol(event) {
    const val = event.target.value.toLowerCase();
    // this.allregulatorylist(this.addEditCourseService.data.data);

    this.tempReg = [...this.enrollregdata];
    // filter our data
    const temp = this.tempReg.filter(function (d) {
      return d.enrolDate.toLowerCase().indexOf(val) !== -1 ||
        // String(d.dueDays).toLowerCase() === val ||
        // String(d.reminder).toLowerCase().indexOf(val) !== -1 ||
        !val;
    });

    // update the rows
    this.rowsReg = [...temp];
    // Whenever the filter changes, always go back to the first page
    this.tableDataReg.offset = 0;
  }

  searchSelfEnrol(event) {
    const val = event.target.value.toLowerCase();
    // this.selfenrolledUser(this.addEditCourseService.data.data);

    this.tempReg = [...this.enrollselfdata];
    // filter our data
    const temp = this.tempSelf.filter(function (d) {
      return String(d.ecn).toLowerCase().indexOf(val) !== -1 ||
        d.fullname.toLowerCase().indexOf(val) !== -1 ||
        d.emailId.toLowerCase().indexOf(val) !== -1 ||
        d.enrolmode.toLowerCase().indexOf(val) !== -1 ||
        d.enrolDate.toLowerCase().indexOf(val) !== -1 ||
        d.phoneNo.toLowerCase().indexOf(val) !== -1 ||
        // d.department.toLowerCase().indexOf(val) !== -1 ||
        // d.status.toLowerCase().indexOf(val) !== -1 ||
        !val;
    });

    // update the rows
    this.rowsSelf = [...temp];
    this.passDataToChild();
    // Whenever the filter changes, always go back to the first page
    this.tableDataSelf.offset = 0;
  }

  enrolUser() {
    this.showEnrolpage = !this.showEnrolpage;
  }

  onCheckBoxClick(event, ruletype) {
    this.searchvalue ={}
    console.log('Ruletype', ruletype);
    this.clearRuleType();

    if (event) {
      switch (ruletype) {
        case 'manual':
          // code...
          this.enrolment.manual = true;
          this.allEnrolUser();
          break;
        case 'rule':
          // code...
          this.enrolment.rule = true;
          this.allruleList();
          break;
        case 'self':
          this.enrolment.self = true;
          this.selfenrolledUser(this.content);
          // code...
          break;
        case 'regulatory':
          this.enrolment.regulatory = true;
          this.allregulatorylist(this.content);
          // code...
          break;
        default:
          // code...
          this.enrolment.manual = true;
          this.allEnrolUser();
          break;
      }
    }

    //  if(event == false){
    //      this.reviewCheck = {};
    //  }else{

    // //  	if(this.enrolment.manual == true){
    // //   	this.enrolment.regulatory = false;
    // // this.enrolment.rule = false;
    // // this.enrolment.self = false;
    // //   }else if(this.enrolment.regulatory == true){
    // //   	this.enrolment.manual = false;
    // // this.enrolment.rule = false;
    // // this.enrolment.self = false;
    // //   }else if(this.enrolment.rule == true){
    // //   	this.enrolment.manual = false;
    // // this.enrolment.regulatory = false;
    // // this.enrolment.self = false;
    // //   }else if(this.enrolment.self == true){
    // //   	this.enrolment.manual = false;
    // // this.enrolment.regulatory = false;
    // // this.enrolment.rule = false;
    // //   }
    //  }
    // console.log('$event',$event);
    //console.log('courseReviewCheck',courseReviewCheck);
  }
  clearRuleType() {
    this.enrolment = {
      manual: false,
      rule: false,
      regulatory: false,
      self: false
    };
  }

  setActiveItem(index, item) {
    console.log('Selected Module', item, index);
  }

  openRule() {
    // this.parent_Comp.openRuleModal();
    this.showAddRuleModal = this.addEditCourseService.showRule;
  }

  openRegulatory() {
    // this.parent_Comp.openRegulatoryModal();
    this.showAddRegulatoryModal = this.addEditCourseService.showRegulatory;
  }
  /*-------------Open Self----------*/
  openSelf() {
    // this.parent_Comp.openSelfModal();
    this.showAddSelfModal = this.addEditCourseService.showSelf;
    this.self();
  }
  self() {
    this.spinner.show();
    var data = {
      courseId: this.content.courseId,
      tId: this.userdata.tenantId,
    }
    console.log(data);
    this.enrolService.getfechsetting(data).then(res => {
      this.spinner.hide();
      console.log(res);
    });
  }
  // closeRule(){
  // 	this.parent_Comp.closeRuleModal();
  // 	this.showAddRuleModal = this.addEditCourseService.showRule;

  // }


  onSelectManual({ selected }) {
    console.log('Select Manual Event', selected, this.selectedManual);

    this.selectedManual.splice(0, this.selectedManual.length);
    this.selectedManual.push(...selected);

    // if(this.selectedManual.length == 1){
    // 	this.enableShowRuleUsers = true;
    // }else{
    // 	this.enableShowRuleUsers = false;
    // }
  }

  onActivateManual(event) {
    // console.log('Activate Event', event);
    if (event.type === 'checkbox') {
      // Stop event propagation and let onSelect() work
      console.log('Checkbox Selected', event);
      event.event.stopPropagation();
    } else if (event.type === 'click' && event.cellIndex != 0) {
      // Do somethings when you click on row cell other than checkbox
      console.log('Row Clicked', event.row); /// <--- object is in the event row variable
    }
  }

  deleteManual(selectedRow) {
    console.log('Manual Current', selectedRow);
    for (let i = 0; i < this.rowsManual.length; i++) {
      var row = this.rowsManual[i];
      if (selectedRow.ecn == row.ecn) {
        this.rowsManual.splice(i, 1);
        this.rowsManual = [...this.rowsManual];
      }
    }
    this.tempManual = this.rowsManual;
    // this.tableData.offset = 0;
  }

  rules: any[];
  selectedRuleToEdit: any = {
    profiles: []
  };
  openRuleModal(rowData, id) {
    // this.searchvalue ={}
    // this.selectedrulevalue = '';
    this.showAddRuleModal = true;

    // this.courseDataService.showRule = this.showAddRuleModal;
    if (id == 0) {
      // this.addRulesForm.reset();
      this.ruleData = {
        id: 0,
        usersCount: '',
        name: '',
        description: '',
        type: '',
        prospName: '',
        profiles: [],
        visible: '',
        value: '',

      };
      this.selectedrulevalue = [];
      this.msg = 'You have successfully added a Rule';
    this.passDataToChild();
    } else {
      this.ruleData = {
        id: rowData.enrolRuleId,
        usersCount: rowData.noOfEmp,
        name: rowData.rulename,
        description: rowData.description,
        type: rowData.ruleAppType,
        prospName: rowData.ruleAppEvent,
        profiles: rowData.profiles,
        visible: rowData.visible,
        value: new Date(rowData.ruleAppDate)
      };
      this.msg = 'You have successfully update a Rule'
      if (this.ruleData.profiles.length > 0) {
        for (let i = 0; i < this.ruleData.profiles.length; i++) {
          let rule = this.ruleData.profiles[i];
          console.log(rule);
          console.log(this.profileFields);
          //   if(this.controlList[i].datatype == 'datetime'){
          //     rule.fieldValues = new Date(rule.fieldValues);
          // }
          this.callRuleFieldType(rule.field, i, 1);
          console.log(this.datarule);
          if (this.datarule == 'datetime') {
            var array = this.ruleData.profiles[i].fieldValues.split('$');
            this.ruleData.profiles[i].fieldValues = [new Date(array[0]), new Date(array[1])];
            // this.ruleData.profiles[i].fieldValues = new Date(this.ruleData.profiles[i].fieldValues);
          }
          if (this.datarule == 'menu') {
            this.databindrule(this.ruleData.profiles[i], i);
            // rule[i] = this.ruleData.profiles[i].fieldValues.split(',');
            //   rule[i].fieldValues= this.createstring(this.selectedrulevalue[i])
          }
          console.log(rule.fieldValues);
        }
        this.profileFieldSelected = true;
        this.passDataToChild();
      }

      console.log('Edit rule data ', this.ruleData);
    }
  }
  databindrule(alldata, i) {
    var array = alldata.fieldValues.split(',');
    let newarray = []
    let data = {
      lovtype: this.menutypeid,
      tId: this.userdata.tenantId
    }
    this.enrolService.getprofileFieldDropdown(data).then(res => {
      console.log(res);
      if (res['type'] == true) {
        let strArraySkilllevel = res['data'][0];

        if (strArraySkilllevel && strArraySkilllevel.length > 0) {
          this.make_selected_dropdown_data_ready(alldata, strArraySkilllevel, i);
          this.cdf.detectChanges();
        } else {
          this.enrolService.getprofileFieldDropdown(data).then(res => {
            console.log(res);
            if (res['type'] == true) {
              let strArraySkilllevel = res['data'][0];
              this.profileFields[i].subtype = strArraySkilllevel;
              this.make_selected_dropdown_data_ready(alldata, strArraySkilllevel, i);
            }
            this.cdf.detectChanges();
          });
        }
      }
    });
    this.selectedrulevalue[i] = newarray;
    this.passDataToChild();
  }
  make_selected_dropdown_data_ready(alldata, strArraySkilllevel, i) {
    var array = alldata.fieldValues.split(',');
    let newarray = []
    strArraySkilllevel.forEach(dataitem => {
      array.forEach(element => {
        if (dataitem.name == element)
          newarray.push(dataitem);
      });
    });
    alldata.fieldValuesArr = [];
    if (newarray && newarray.length > 0) {
      alldata.fieldValuesArr = newarray;
      alldata.fieldValues = '';
    }
    if (i == this.selfFieldsData.profiles.length - 1) {
      // this.isFetchingSettings = false;
    }
    this.cdf.detectChanges();
    this.passDataToChild();
  }
  /* --------------Add regulatory Filter------------ */

  saveRegFilter(item, f) {
    if (f.valid || item.name !== '') {
      console.log(item);
      // console.log(this.addRegFilterForm);
      //var filter = this.addRegFilterForm.value.filters;
      this.makefilter(item);
    } else {
      console.log('Please Fill all fields');
      Object.keys(f.controls).forEach(key => {
        f.controls[key].markAsDirty();
      });
    }
  }
  createstring(data) {
    var str = '';
    for (var i = 0; i < data.length; i++) {
      if (i == 0) {
        str = data[i].name;
        // str = data[i].id;
      } else {
        str = str + ',' + data[i].name;
        // str = str + ',' + data[i].id;
      }
    }
    return str;
  }
  makefilter(data) {
    var allstring = '';
    console.log(data.profiles);
    var filter = data.profiles;
    if (filter.length > 0) {
      for (var i = 0; i < filter.length; i++) {
        console.log(this.controlList);
        if (this.controlList[i].datatype == 'datetime') {
          var fromdate = this.formatDateReady(filter[i].fieldValues[0]);
          var todate = this.formatDateReady(filter[i].fieldValues[1]);
          // rule.fieldValues = this.formatDateReady(rule.fieldValues);
          filter[i].fieldValues = fromdate + '$' + todate;
          console.log(filter[i].fieldValues);
        }
        if (this.controlList[i].datatype == 'menu') {
          console.log(this.selectedfiltervalue[i]);
          filter[i].fieldValues = this.createstring(this.selectedfiltervalue[i]);
        }
        if (i == 0) {
          allstring = filter[i].field + '|' + filter[i].fieldValues + '|1';
        } else {
          allstring += '#' + filter[i].field + '|' + filter[i].fieldValues + '|1';
        }
        console.log(allstring);
      }
      if (this.regiD == '' || this.regiD == null || this.regiD == undefined) {
        this.regiD = 0;
      }
      var newRegulatoryData = {
        modeId: 3,
        aId: this.regiD,
        cid: this.content.courseId,
        allstr: allstring,
        tid: this.userdata.tenantId,
        createrId: this.userdata.id,
        // createrId: this.userLoginData.username
      };

      console.log('Final rule data', newRegulatoryData);
      this.enrolService.regulatory_filterforcourse(newRegulatoryData).then(res => {
        console.log(res);
        if (res['type'] = true) {
          // var courseUpdate: Toast = {
          //   type: 'success',
          //   title: 'Course',
          //   body: this.msg1,
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // // this.addregulatorynew();
          // this.toasterService.pop(courseUpdate);

          this.toast.success(this.msg1, 'success');
        } else {
          // var courseUpdate: Toast = {
          //   type: 'error',
          //   title: 'Course',
          //   body: 'Unable to add a filter',
          //   showCloseButton: true,
          //   timeout: 2000
          // };
          // this.toasterService.pop(courseUpdate);
          this.toast.error('Unable to add a filter', 'Error');
        }
        this.cdf.detectChanges();
      });
    }
    this.closeRegulatoryFilterModal();
  }


  /******************* add edit rule new start*/
  ruleFieldTypeSelected(i, item) {
    console.log(item);
  }

  async callRuleFieldType(id: any, index: any, status) {
    this.profileFieldSelected = false;
    this.passDataToChild();
    console.log(id + index);
    console.log(this.ruleData.profiles);
    if (status == 2) {
      this.ruleData.profiles[index].fieldValues = '';
      this.ruleData.profiles[index].fieldValuesArr = [];
    }
    if (this.strArrayType[index]) {
      this.strArrayType[index] = [];
    }

    if (id.srcElement == undefined || id.target == undefined) {
      this.ValueId = id;
    } else {
      this.ValueId = parseInt((id.srcElement || id.target).value);
    }
    for (let i = 0; i < this.profileFields.length; i++) {
      if (this.profileFields[i].id == this.ValueId) {

        this.controlList[index] = this.profileFields[i];
        // this.strArrayType[index].push(this.profileFields[i]);
        // if (this.selectedFilterOption.length > 0) {
        this.selectedFilterOption[index] = this.profileFields[i].shortname;
        this.datarule = this.profileFields[i].datatype;
        this.menutypeid = this.profileFields[i].menuTypeId;
        // this.ruleData.profiles[index].fieldValues = '';
        // this.ruleData.profiles[index].fieldValuesArr = [];
        // console.log(this.menutypeid)
        /*---------profile menu list-----------*/
        if (this.menutypeid != '' && this.menutypeid != null && this.menutypeid != undefined) {
          var data = {
            lovtype: this.menutypeid,
            tId: this.userdata.tenantId
          }
          this.enrolService.getprofileFieldDropdown(data).then(res => {
            this.strArraySkilllevel = res['data'][0];
            this.strArraySkilllevel = [...this.strArraySkilllevel];
            this.itemList = this.strArraySkilllevel;
            // this.itemList = this.controlList[i].subtype;
            console.log(this.itemList);
            this.profileFields[i].subtype = this.strArraySkilllevel;
            this.cdf.detectChanges();
            this.passDataToChild();
          })
        }
        console.log(this.profileFields);
        this.profileFieldSelected = true;
        this.passDataToChild();
        //  this.datatype.push(this.ruleData.profiles[i]);
        // } else {
        //   this.selectedFilterOption.push(this.profileFields[i].shortname);
        //   console.log(this.selectedFilterOption);
        // }
        this.cdf.detectChanges();
      }
    }
    this.passDataToChild();
    this.disableSelectedRuleFieldType();
  }

  disableSelectedRuleFieldType() {
    console.log(this.selectedFilterOption,"selectedFilterOption")
    this.profileFields.forEach((data, key) => {
      console.log(this.selectedFilterOption.indexOf(data.shortname),"shortName")
      if (this.selectedFilterOption.indexOf(data.shortname) >= 0) {
        this.profileFields[key].selected = 'true';
      } else {
        this.profileFields[key].selected = 'false';
      }
    })
    console.log(this.profileFields,"profilefields")
    console.log('Selected Disabled', this.strArrayPar);
  }

  addRuleList() {
    if (this.ruleData.profiles.length == 0) {
      this.profileFieldSelected = true;
    }
    let defualtRulesObj = {
      enrolProfileId: 0,
      field: '',
      fieldValues: ''
    }
    this.ruleData.profiles.push(defualtRulesObj);
    this.controlFlag = true;
    this.controlList.push(0);
    console.log(this.ruleData.profiles);
    this.disableSelectedRuleFieldType();
    this.passDataToChild();
  }

  removeRuleList(currentIndex) {
    this.profileFieldSelected = false;
    this.ruleData.profiles.splice(currentIndex, 1);
    this.selectedFilterOption.splice(currentIndex, 1);
    this.disableSelectedRuleFieldType();
    this.controlFlag = true;
    this.controlList.splice(currentIndex, 1);
    this.strArrayType.splice(currentIndex, 1);
    this.cdf.detectChanges();
    console.log(this.ruleData.profiles);
    this.profileFieldSelected = true;
  }
  /******************* add edit rule new end*/

  setUpForm(rules: any[]) {
    return new FormGroup({
      rules: new FormArray(rules.map((rule) => this.createRule(rule)))
    });
  }

  get rulesFormArray() {
    return (this.addRulesForm.get('rules') as FormArray);
  }

  createRule(rule: any) {
    return new FormGroup({
      FilterOpt: new FormControl(rule.FilterOpt || ''),
      Value1: new FormControl(rule.Value1 || ''),
      Value2: new FormControl(rule.Value2 || '')
    })
  }

  closeRuleModal() {
    this.showAddRuleModal = false;
    // this.courseDataService.showRule = this.showAddRuleModal;
    this.ruleData = {};
    this.ruleData.profiles = [];
    this.formdata = {};
    this.clearRule();
    this.profileFieldSelected = false;
    for (let i = 0; i < this.profileFields.length; i++) {
      this.profileFields[i].selected = false;
    }
    if(!this.isSave){
    this.selectedFilterOption = []
    }

    this.disableSelectedRuleFieldType();
    this.passDataToChild();
    this.allEnrolUserruleList(this.content);
   // this.isSave = false;
  }

  clearRule() {
    this.addRulesForm.reset({
      FilterOpt: [''],
      Value1: [''],
      Value2: ['']
    })
  }

  initRules() {
    return this._fb.group({
      FilterOpt: [''],
      Value1: [''],
      Value2: ['']
    });
  }

  addRule() {
    if (this.ruleData.type == 1) {
      this.profileFieldSelected = true;
      // this.ruleData.type = 'Profile Fields';
    } else if (this.ruleData.type == 2) {
      this.profileFieldSelected = false;
      // this.ruleData.type = 'Others';
    }
    const control = <FormArray>this.addRulesForm.controls['rules'];
    control.push(this.initRules());
    console.log(this.addRulesForm.controls['rules']);
    this.controlFlag = true;
    this.controlList.push(0);
    this.strArrayType.push([]);

  }

  removeRule(i: number) {
    const control = <FormArray>this.addRulesForm.controls['rules'];
    control.removeAt(i);
    // this.enableSelect(this.strArrayType[i]);
    this.selectedFilterOption.splice(i, 1);
    this.disableSelect();
    this.controlFlag = true;
    this.controlList.splice(i, 1);
    this.strArrayType.splice(i, 1);
  }

  disableSelect() {
    this.profileFields.forEach((data, key) => {
      if (this.selectedFilterOption.indexOf(data.shortname) >= 0) {
        this.profileFields[key].selected = 'true';
      } else {
        this.profileFields[key].selected = 'false';
      }
    })
    console.log('Selected Disabled', this.strArrayPar);
  }


  ruleTypeSelected($event, i) {
    console.log(this.ruleData);

  }
  prospTypeSelected() {
    console.log(this.ruleData);
    // if(this.ruleData.)
  }
  callType(id: any, index: any) {
    if (this.strArrayType[index]) {
      this.strArrayType[index] = [];
    }

    if (id.srcElement == undefined || id.target == undefined) {
      this.ValueId = id;
    } else {
      this.ValueId = parseInt((id.srcElement || id.target).value);
    }
    //this.disableSelect();

    // this.controlList[index] = this.ValueId;
    for (let i = 0; i < this.profileFields.length; i++) {
      if (this.profileFields[i].id == this.ValueId) {
        // this.selectedRuleType = this.profileFields[i].datatype;

        this.controlList[index] = this.profileFields[i];

        this.strArrayType[index].push(this.profileFields[i]);
        if (this.selectedFilterOption.length > 0) {
          this.selectedFilterOption[index] = this.profileFields[i].shortname;
        } else {
          this.selectedFilterOption.push(this.profileFields[i].shortname);
        }
      }
    }
    this.disableSelect();
  }

  makeRuleDataready(ruleData) {
    this.spinner.show();
    // var rules = ruleData.rules;
    var rules = ruleData.profiles;
    var field = '';
    var value = '';
    var allstring = '';
    console.log('rules', rules)
    if (rules.length > 0) {
      for (var i = 0; i < rules.length; i++) {
        var rule = rules[i];
        // if(dimension != ''){
        //   dimension += '|';
        // }
        // if(String(parameter.type) != '' && String(parameter.type) != 'null'){
        //   dimension += parameter.type;
        // }
        if (this.controlList[i].datatype == 'datetime') {
          var fromdate = this.formatDateReady(rule.fieldValues[0]);
          var todate = this.formatDateReady(rule.fieldValues[1]);
          // rule.fieldValues = this.formatDateReady(rule.fieldValues);
          rule.fieldValues = fromdate + '$' + todate;
          console.log(rule.fieldValues);
        }
        if (this.controlList[i].datatype == 'menu') {
          console.log(rule.fieldValuesArr);
          rules[i].fieldValues = this.createstring(rule.fieldValuesArr);
        }

        if (i == 0) {
          allstring = rule.field + '|' + rule.fieldValues + '|1';
        }
        else {
          allstring += '#' + rule.field + '|' + rule.fieldValues + '|1';
        }
        console.log(allstring);
      }

    } else {
      // field = this.ruleData.prospName;
      // value = this.formatDateReady(this.ruleData.value);
      // if(value == 'undefined'){
      //   value = '';
      // }
      // allstring = field+ '|' + value + '|' + 1;
    }
    var ruleDimension;
    for (let r = 0; r < this.ruleType.length; r++) {
      if (this.ruleType[r].ruleTypeId == ruleData.type) {
        ruleDimension = this.ruleType[r].ruleTypeName;
        console.log(this.ruleType, ' +++ ', ruleDimension)
      }
    }

    // field : field,
    // value : value,
    // usersCount: 0,
    var visibleRule;
    console.log(this.ruleData);
    if (this.ruleData.id == '') {
      visibleRule = 1;
    }
    else {
      visibleRule = this.ruleData.visible;
    }
    console.log('visibleRule', visibleRule);
    // if(this.ruleData.value != ''){
    //    var appData =  this.formatDateReady(this.ruleData.value);
    // }
    var roledata = 0;
    if (ruleData.id != '' && ruleData.id != null && ruleData.id != undefined) {
      roledata = ruleData.id;
    }
    console.log(ruleData.type);
    // if(ruleData.type==2){
    //   allstring=null;
    // }
    if (this.ruleData.value != '' || this.ruleData.value != undefined || this.ruleData.value != null) {
      this.ruleData.value = this.formatDateReady(this.ruleData.value);
    }
    var newRuleData = {
      rId: roledata,
      rname: ruleData.name,
      rdescription: ruleData.description,
      appType: ruleData.type,
      appevent: this.ruleData.prospName,
      appDate: this.ruleData.value,
      cid: this.toolsservice.conId,
      tid: this.userdata.tenantId,
      userId: this.userdata.id,
      allstr: allstring,
      visible: visibleRule,
      areaId: this.areaId,
    };
    console.log('Final rule data', newRuleData);
    this.enrolService.Addruleforcourse(newRuleData).then(res => {
      this.spinner.hide();
      console.log(res);
      this.loader = false;
      this.resultdata = res;
      if (this.resultdata.type == false) {
        // var courseUpdate: Toast = {
        //   type: 'error',
        //   title: 'Course',
        //   body: 'Unable to add a Rule.',
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // // this.closeEnableDisableCourseModal();
        // this.toasterService.pop(courseUpdate);
        this.toast.error('Unable to add a Rule.', 'Error');

      } else {

        // var courseUpdate: Toast = {
        //   type: 'success',
        //   title: 'Course',
        //   body: this.msg,
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // row.visible = !row.visible;
        this.newruleadd();
        this.allruleList();
        // this.toasterService.pop(courseUpdate);
        this.toast.success(this.msg, 'Success');
      }
    });
    this.closeRuleModal();
  }


  enableShowRuleUsers: boolean = false;
  onSelectRules({ selected }) {
    console.log('Select Rules Event', selected, this.selectedRules);

    this.selectedRules.splice(0, this.selectedRules.length);
    this.selectedRules.push(...selected);

    if (this.selectedRules.length == 1) {
      this.enableShowRuleUsers = true;
    } else {
      this.enableShowRuleUsers = false;
    }
  }

  onActivateRules(event) {
    // console.log('Activate Event', event);
    if (event.type === 'checkbox') {
      // Stop event propagation and let onSelect() work
      console.log('Checkbox Selected', event);
      event.event.stopPropagation();
    } else if (event.type === 'click' && event.cellIndex != 0) {
      // Do somethings when you click on row cell other than checkbox
      console.log('Row Clicked', event.row); /// <--- object is in the event row variable
    }
  }

  deleteRules(selectedRow) {
    console.log('Rules Current', selectedRow);
    for (let i = 0; i < this.rowsRules.length; i++) {
      var row = this.rowsRules[i];
      if (selectedRow.id == row.id) {
        this.rowsRules.splice(i, 1);
        this.rowsRules = [...this.rowsRules];
      }
    }
    this.tempRules = this.rowsRules;
    // this.tableData.offset = 0;
  }

  showRuleUsersModal: boolean = false;
  ruleUsersModelTitle: any = '';

  viewRuleUsers(rowData) {
    console.log(rowData);
    // this.ruleUsersModelTitle = this.selectedRules[0].name;
    this.ruleUsersModelTitle = rowData.name;
    this.showRuleUsersModal = true;
    this.passDataToChild();
    this.allEnrolUserruleList(rowData);
  }

  /*********** enrol user by rules ******************/
  allEnrolUserruleList(content) {
    this.spinner.show();
    var data = {
      areaId: this.areaId,
      instanceId: this.toolsservice.conId,
      tId: this.userdata.tenantId,
      ruleId:content.enrolRuleId,
      mode: 2,
    };
    this.addEditCourseService.getallenroluser(data).then(enrolData => {
      this.spinner.hide();
      this.enrolldatarule = enrolData['data'];
      this.rowsEnrolRule = enrolData['data'];
      this.rowsEnrolRule = [...this.rowsEnrolRule];
      // for (let i = 0; i < this.rowsEnrolRule.length; i++) {
      //   this.rowsEnrolRule[i].Date = new Date(this.rowsEnrolRule[i].enrolDate);
      //   this.rowsEnrolRule[i].enrolDate = this.formdate(this.rowsEnrolRule[i].Date);
      // }
      console.log('EnrolledUSer', this.rowsEnrolRule);
      if (this.enrolldata.visible = 1) {
        this.enableCourse = false;
      } else {
        this.enableCourse = true;
      }
      this.passDataToChild()
      this.cdf.detectChanges();
    });
  }
  closeRuleUsersModal() {
    this.showRuleUsersModal = false;
    this.ruleUsersModelTitle = '';
  }

  openRegulatoryModal() {
    this.showAddRegulatoryModal = true;
    // this.courseDataService.showRegulatory = this.showAddRegulatoryModal;

  }

  onSelectReg({ selected }) {
    console.log('Select Reg Event', selected, this.selectedReg);

    this.selectedReg.splice(0, this.selectedReg.length);
    this.selectedReg.push(...selected);

    // if(this.selectedManual.length == 1){
    // 	this.enableShowRuleUsers = true;
    // }else{
    // 	this.enableShowRuleUsers = false;
    // }
  }

  onActivateReg(event) {
    // console.log('Activate Event', event);
    if (event.type === 'checkbox') {
      // Stop event propagation and let onSelect() work
      console.log('Checkbox Selected', event);
      event.event.stopPropagation();
    } else if (event.type === 'click' && event.cellIndex != 0) {
      // Do somethings when you click on row cell other than checkbox
      console.log('Row Clicked', event.row); /// <--- object is in the event row variable
    }
  }

  deleteReg(selectedRow) {
    console.log('Rules Current', selectedRow);
    for (let i = 0; i < this.rowsReg.length; i++) {
      var row = this.rowsReg[i];
      if (selectedRow.id == row.id) {
        this.rowsReg.splice(i, 1);
        this.rowsReg = [...this.rowsReg];
      }
    }
    this.tempReg = this.rowsReg;
    // this.tableData.offset = 0;
  }

  closeRegulatoryModal() {
    this.showAddRegulatoryModal = false;
   this.profileFieldSelected = false;
    for (let i = 0; i < this.strArrayTypeRegFilter.length; i++) {
      this.profileFields[i].selected = false;
    }
    this.disableSelectRegFilter();
    // this.disableSelectedRuleFieldType();
  }

  makeRegDataReady(data) {
    this.spinner.show();
    console.log(data)
    var enrolDt: any = this.formatDateReady(data.enrolDate);
    // if(this.regData.enrolDate != null || this.regData.enrolDate != undefined || this.regData.enrolDate != ''){
    // 	var enrolDt:any = this.regData.enrolDate;
    // }
    var roledata = 0;
    console.log(this.regiD);
    if (this.regiD != '' && this.regiD != null && this.regiD != undefined) {
      roledata = this.regiD;
    }
    else {
      roledata = 0;
    }


    // if(this.regenId == '' && this.regenId==null && this.regenId==undefined){
    //   this.regenId = 0;
    // }
    console.log(this.regenId);

    var regDataDinal = {
      rId: roledata,
      rDtId: 0,
      enDate: enrolDt,
      cid: this.content.courseId,
      tid: this.userdata.tenantId,
      userId: this.userdata.id,
      visible: 1
    }

    console.log('Regulatory Final data', regDataDinal);
    this.enrolService.Addregulatoryforcourse(regDataDinal).then(result => {
      console.log(result);
      this.spinner.hide();
      if (result['type'] == true) {
        // var courseUpdate: Toast = {
        //   type: 'success',
        //   title: 'Course',
        //   body: 'You have successfully added Enrol date.',
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        this.regData = {};
        // this.closeEnableDisableCourseModal();
        // this.toasterService.pop(courseUpdate);
        this.toast.success('You have successfully added Enrol date.', 'Success');

      } else {
        // var courseUpdate: Toast = {
        //   type: 'error',
        //   title: 'Course',
        //   body: 'Unable to update added Enrol date.',
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // // this.closeEnableDisableCourseModal();
        // this.toasterService.pop(courseUpdate);
        this.toast.error('Unable to update added Enrol date.', 'Error');
      }

    })
    this.allregulatorylist(this.content);


    // this.closeRegulatoryModal();
  }

  clearRegData() {
    this.regData = {
      id: '',
      enrolDate: '',
      dueDays: '',
      reminder: ''
    };
  }

  /*--------------- enrolled User (self)----------------------*/
  selfenrolledUser(content) {
    this.spinner.show();
    var data = {
      areaId: this.areaId,
      instanceId: this.toolsservice.conId,
      tId: this.userdata.tenantId,
      mode: 4,
    };
    console.log(data);
    this.addEditCourseService.getallenroluser(data).then(enrolData => {
      this.spinner.hide();
      console.log(enrolData);
      this.enrollselfdata = enrolData['data'];
      this.rowsSelf = enrolData['data'];
      this.rowsSelf = [...this.rowsSelf];
      if (this.enrolldata.length == 0) {
        this.showEnrolpage = !this.showEnrolpage;
      }
      console.log('SELF USER', this.rowsSelf);
      this.cdf.detectChanges();
    }, resUserError => {
      console.log(resUserError);
      if (resUserError.statusText == 'Unauthorized') {
        this.router.navigate(['/login']);
      }
      // this.loader = false;
      this.spinner.hide();
      this.errorMsg = resUserError
    });

  }

  /*--------------disable self user--------------*/
  disableselfVisibility(currentIndex, row, status) {
    this.spinner.show();
    var visibilityData = {
      employeeId: row.employeeId,
      visible: status,
      courseId: this.toolsservice.conId,
      tId: this.userdata.tenantId,
      aId: 33,
    }
    this.addEditCourseService.disableEnrol(visibilityData).then(result => {
      console.log(result);
      this.spinner.hide();
      this.loader = false;
      this.resultdata = result;
      if (this.resultdata.type == false) {
        // var courseUpdate: Toast = {
        //   type: 'error',
        //   title: 'Course',
        //   body: 'Unable to update visibility of User.',
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // // this.closeEnableDisableCourseModal();
        // this.toasterService.pop(courseUpdate);
        this.toast.error('Unable to update visibility of User.', 'Error');
      } else {
        // var courseUpdate: Toast = {
        //   type: 'success',
        //   title: 'Course',
        //   body: this.resultdata.data,
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // row.visible = !row.visible;
        console.log('after', row.visible)
        this.allEnrolUser();
        // this.toasterService.pop(courseUpdate);
        this.toast.success(this.resultdata.data, 'Success');
      }
    },
      resUserError => {
        this.loader = false;
        this.errorMsg = resUserError;
        // this.closeEnableDisableCourseModal();
      });
  }

  /*--------open setting for self ------*/
  openSelfModal() {
    this.showAddSelfModal = true;

    this.fetchsetting();
    // this.courseDataService.showRule = this.showAddRuleModal;
  }


  fetchsetting() {
    //  this.selfFieldsData =[];
    this.spinner.show();
    var data = {
      courseId: this.content.courseId,
      tId: this.userdata.tenantId,
    };
    this.enrolService.getfechsetting(data).then(res => {
      console.log('fetch setting', res);
      this.spinner.hide();
      this.selfFieldsData = res['data'][0];
      if (this.selfFieldsData) {
        this.selfFieldsData = {
          id: this.selfFieldsData.enrolSelfId,
          sid: this.selfFieldsData.selfType,
          maxCount: this.selfFieldsData.maxEnrolments,
          type: 1,
          cid: this.selfFieldsData.cid,
          tid: this.userdata.tid,
          userId: this.selfFieldsData.userId,
          profiles: this.selfFieldsData.profiles,
        };
        this.msg2 = 'You have successfully update setting';
        if (this.selfFieldsData.profiles.length > 0) {
          for (let i = 0; i < this.selfFieldsData.profiles.length; i++) {
            let rule = this.selfFieldsData.profiles[i];
            //   if(this.controlList[i].datatype == 'datetime'){
            //     rule.fieldValues = new Date(rule.fieldValues);
            // }
            this.callRuleFieldType(rule.field, i, 1);
            if (this.datarule == 'datetime') {
              var array = this.selfFieldsData.profiles[i].fieldValues.split('$');
              this.selfFieldsData.profiles[i].fieldValues = [new Date(array[0]), new Date(array[1])];
              // this.selfFieldsData.profiles[i].fieldValues = new Date(this.selfFieldsData.profiles[i].fieldValues);
            }
            if (this.controlList[i].datatype == 'menu') {
              this.databindself(this.selfFieldsData.profiles[i], i);
              console.log(this.selectedsettingvalue[i]);
              //   this.selfFieldsData[i].fieldValues= this.createstring(this.selectedsettingvalue[i])
            }
          }
          // this.profileFieldSelected = true;
        }

        console.log('Edit self data ', this.selfFieldsData);
        this.cdf.detectChanges();

      } else {
        this.selfFieldsData = {
          id: 0,
          sid: '',
          maxCount: '',
          cid: '',
          tid: '',
          userId: '',
          profiles: [],
        };
        this.msg2 = 'You have successfully added setting';
        console.log(this.selfFieldsData);
      }

      console.log(this.selfFieldsData.enrolSelfId);
    });
  }

  databindself(alldata, i) {
    var array = alldata.fieldValues.split(',');
    let newarray = []
    let data = {
      lovtype: this.menutypeid,
      tId: this.userdata.tenantId
    }
    this.enrolService.getprofileFieldDropdown(data).then(res => {
      console.log(res);
      if (res['type'] == true) {
        let strArraySkilllevel = res['data'][0];
        this.strArraySkilllevel = [...strArraySkilllevel];
        // this.itemList = this.strArraySkilllevel;
        // // this.itemList = this.controlList[i].subtype;
        // console.log(this.itemList);
        this.profileFields[i].subtype = this.strArraySkilllevel;
        // this.cdf.detectChanges();
        array.forEach(element => {
          strArraySkilllevel.forEach(dataitem => {
            if (dataitem.name == element)
              newarray.push(dataitem);
          });
        });
      }
      this.cdf.detectChanges();
    });
    this.selectedsettingvalue[i] = newarray;
  }

  onSelectSelf({ selected }) {
    console.log('Select Reg Event', selected, this.selectedSelf);

    this.selectedSelf.splice(0, this.selectedSelf.length);
    this.selectedSelf.push(...selected);
  }

  onActivateSelf(event) {
    // console.log('Activate Event', event);
    if (event.type === 'checkbox') {
      // Stop event propagation and let onSelect() work
      console.log('Checkbox Selected', event);
      event.event.stopPropagation();
    } else if (event.type === 'click' && event.cellIndex != 0) {
      // Do somethings when you click on row cell other than checkbox
      console.log('Row Clicked', event.row); /// <--- object is in the event row variable
    }
  }

  deleteSelf(selectedRow) {
    console.log('Self Current', selectedRow);
    for (let i = 0; i < this.rowsSelf.length; i++) {
      var row = this.rowsSelf[i];
      if (selectedRow.ecn == row.ecn) {
        this.rowsSelf.splice(i, 1);
        this.rowsSelf = [...this.rowsSelf];
      }
    }
    this.tempSelf = this.rowsSelf;
    // this.tableData.offset = 0;
  }


  closeSelfModal() {
    this.showAddSelfModal = false;
    // this.courseDataService.showRule = this.showAddRuleModal;
    this.selfFieldsData = {};
    this.formdataSelf = {};
    this.clearSelfFields();
    this.selfProfileFieldSelected = false;
  }

  initSelfFields() {
    return this._fb.group({
      FilterOpt: [''],
      Value1: [''],
      Value2: ['']
    });
  }

  addSelfFields() {
    if (this.selfFieldsData.profiles.length == 0) {
      this.selfProfileFieldSelected = true;
    }
    let defualselfObj = {
      enrolProfileId: 0,
      field: '',
      fieldValues: ''
    }
    this.selfFieldsData.profiles.push(defualselfObj);
    this.controlFlag = true;
    this.controlList.push(0);
    this.strArrayType.push([]);
  }
  //   if(this.selfFieldsData.type == 1){
  //     this.selfProfileFieldSelected = true;
  //   }
  //   const control = <FormArray>this.addSelfFieldsForm.controls['fields'];
  //   control.push(this.initSelfFields());
  //   console.log(this.addSelfFieldsForm.controls['fields']);
  //   this.controlFlagSelfFields = true;
  //   this.controlListSelfFields.push(0);
  //   this.strArrayTypeSelfFields.push([]);

  // }

  removeSelfFields(currentIndex) {
    // const control = <FormArray>this.addSelfFieldsForm.controls['fields'];
    // control.removeAt(i);
    // // this.enableSelect(this.strArrayType[i]);
    // this.selectedFilterOptionSelf.splice(i, 1);
    // this.disableSelectSelfFields();
    // this.controlFlagSelfFields = true;
    // this.controlListSelfFields.splice(i,1);
    // this.strArrayTypeSelfFields.splice(i,1);
    this.selfFieldsData.profiles.splice(currentIndex, 1);
    this.selectedFilterOption.splice(currentIndex, 1);
    // this.disableSelectedRuleFieldType();
    this.disableSelectSelfFields();
    this.controlFlag = true;
    this.controlList.splice(currentIndex, 1);
    this.strArrayType.splice(currentIndex, 1);
  }

  disableSelectSelfFields() {
    this.profileFieldsSelf.forEach((data, key) => {
      if (this.selectedFilterOptionSelf.indexOf(data.shortname) >= 0) {
        this.profileFieldsSelf[key].selected = 'true';
      } else {
        this.profileFieldsSelf[key].selected = 'false';
      }
    })
    console.log('Selected Disabled', this.strArrayPar);
  }

  clearSelfFields() {
    const arr = <FormArray>this.addSelfFieldsForm.controls.fields;
    arr.controls = [];
    this.addSelfFieldsForm.reset({
      FilterOpt: [''],
      Value1: [''],
      Value2: ['']
    })
  }

  selfFieldTypeSelected() {

  }

  callTypeSelfFields(id: any, index: any, selectedField) {
    if (this.strArrayTypeSelfFields[index]) {
      this.strArrayTypeSelfFields[index] = [];
    }

    this.selfFieldValueId = parseInt((id.srcElement || id.target).value);
    //this.disableSelect();

    // this.controlList[index] = this.ValueId;
    for (let i = 0; i < this.profileFieldsSelf.length; i++) {
      if (this.profileFieldsSelf[i].id == this.selfFieldValueId) {
        // this.selectedRuleType = this.profileFields[i].datatype;

        this.controlListSelfFields[index] = this.profileFieldsSelf[i];

        this.strArrayTypeSelfFields[index].push(this.profileFieldsSelf[i]);
        if (this.selectedFilterOptionSelf.length > 0) {
          this.selectedFilterOptionSelf[index] = this.profileFieldsSelf[i].shortname;
        } else {
          this.selectedFilterOptionSelf.push(this.profileFieldsSelf[i].shortname);
        }
      }
    }
    this.disableSelectSelfFields();
  }

  makeSelfDataReady(selfFieldsData) {

    var fields = selfFieldsData.profiles;
    var field = '';
    var value = '';
    var allstring = '';
    if (fields.length > 0) {
      for (var i = 0; i < fields.length; i++) {
        var selffield = fields[i];
        // if(dimension != ''){
        //   dimension += '|';
        // }
        // if(String(parameter.type) != '' && String(parameter.type) != 'null'){
        //   dimension += parameter.type;
        // }
        if (this.controlList[i].datatype == 'datetime') {
          var fromdate = this.formatDateReady(selffield.fieldValues[0]);
          var todate = this.formatDateReady(selffield.fieldValues[1]);
          // rule.fieldValues = this.formatDateReady(rule.fieldValues);
          selffield.fieldValues = fromdate + '$' + todate;
          console.log(selffield.fieldValues);
          // selffield.fieldValues = this.formatDateReady(selffield.fieldValues);
        }
        if (this.controlList[i].datatype == 'menu') {
          console.log(this.selectedsettingvalue[i]);
          fields[i].fieldValues = this.createstring(this.selectedsettingvalue[i])
        }
        if (i == 0) {
          allstring = selffield.field + '|' + selffield.fieldValues + '|1';
        }
        else {
          allstring += '#' + selffield.field + '|' + selffield.fieldValues + '|1';
        }
        console.log(allstring);
        //     if(field != ''){
        //       field += ',';
        //     }
        //     if(String(selffield.FilterOpt) != '' && String(selffield.FilterOpt) != 'null'){
        //     	for(let j=0; j< this.profileFieldsSelf.length; j++){
        //     		var profileFieldsSelf = this.profileFieldsSelf[j];
        //     		if(selffield.FilterOpt == profileFieldsSelf.id){
        //       			field += profileFieldsSelf.name;
        //     		}
        //     	}
        //       // field += rule.FilterOpt;
        //     }

        //     if(selffield.Value1.formatted == '' || selffield.Value1.formatted == undefined || selffield.Value1.formatted == null){
        //       if(value != ''){
        //         value += ',';
        //       }
        //       if(String(selffield.Value1) != '' && String(selffield.Value1) != 'null'){
        //         value += selffield.Value1;
        //       }
        //     }else{
        //       if(value != ''){
        //         value += ',';
        //       }
        //       if(String(selffield.Value1.formatted) != '' && String(selffield.Value1.formatted) != 'null'){
        //         value += selffield.Value1.formatted;
        //       }
        //     }

      }
    }


    for (let r = 0; r < this.selfType.length; r++) {
      if (this.selfType[r].idd == selfFieldsData.id) {
      }
    }
    for (let r = 0; r < this.selfFeildType.length; r++) {
      if (this.selfFeildType[r].selfTypeId == selfFieldsData.type) {
      }
    }
    var selfdataid = 0;
    if (selfFieldsData.id != '' && selfFieldsData.id != null && selfFieldsData.id != undefined) {
      selfdataid = selfFieldsData.id;
    }
    var newSelfData = {
      rId: selfdataid,
      sType: selfFieldsData.sid,
      maxEnrolments: selfFieldsData.maxCount,
      cid: this.content.courseId,
      tid: this.userdata.tenantId,
      userId: this.userdata.id,
      allstr: allstring,
      areaId: this.areaId,
    }

    console.log('Final self data', newSelfData);

    this.enrolService.addselfsetting(newSelfData).then(res => {
      console.log(res);
      if (res['type'] == false) {
        // var courseUpdate: Toast = {
        //   type: 'error',
        //   title: 'Course',
        //   body: 'Unable to update the setting.',
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // // this.closeEnableDisableCourseModal();
        // this.toasterService.pop(courseUpdate);
        this.toast.error('Unable to update the setting.', 'Error');

      } else {
        this.newsetting(res['data'][0])
        // var courseUpdate: Toast = {
        //   type: 'success',
        //   title: 'Course',
        //   body: this.msg2,
        //   showCloseButton: true,
        //   timeout: 2000
        // };
        // this.toasterService.pop(courseUpdate);
        this.toast.success(this.msg2, 'Success');
      }
    })

    this.closeSelfModal();
  }
  newsetting(res) {
    console.log(res);
    var data = {
      inst: this.content.courseId,
      setId: res.setId,
      tenId: this.userdata.tenantId,
      mgrId: this.content.creatorId
    }
    console.log(data);
    this.enrolService.addselfsetting_new(data).then(res => {
      console.log(res);
    })
  }
  newruleadd() {
    var data = {
      areaId: this.areaId,
      corsId: this.toolsservice.conId,
      tenId: this.userdata.tenantId,
    }
    console.log(data);
    this.enrolService.Addruleforcourse_new_enrolmnet(data).then(res => {
      console.log(res);
    })
  }
  // addregulatorynew() {
  //   var data = {
  //     corsId: this.content.courseId,
  //     tenId: this.content.tenantId,
  //   }
  //   console.log(data);
  //   this.enrolService.Addregulatoryforcourse_new_enrol(data).then(res => {
  //     console.log(res);
  //   })
  // }
  saveSelfFields(item, f) {
    if (f.valid) {
      console.log(item)
      // this.loader =true;
      // console.log(this.addRulesForm.value);
      let selfFields: any = item;
      this.selfFieldsData.fields = selfFields;
      console.log('Self fields data final', selfFields);

      this.makeSelfDataReady(selfFields);
    } else {
      console.log('Please Fill all fields');
      Object.keys(f.controls).forEach(key => {
        f.controls[key].markAsDirty();
      });
    }

  }
  formatDateReady(date) {
    if (date) {
      date = new Date(date);
      var day = date.getDate();
      var monthIndex = ('0' + (date.getMonth() + 1)).slice(-2);
      var year = date.getFullYear();

      return year + '-' + monthIndex + '-' + day;
    }
  }

  formdate(date) {

    if (date) {
      // const months = ['JAN', 'FEB', 'MAR','APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
      // var day = date.getDate();
      // var monthIndex = months[date.getMonth()];
      // var year = date.getFullYear();
      // return day + '_' + monthIndex + '_' +year;
      var formatted = this.datePipe.transform(date, 'dd-MM-yyyy');
      return formatted;
    }
  }

  // ngOnDestroy() {

  //   if (this.addEditCourseService.data.data) {
  //     this.parent_Comp.allEnrolUser(this.addEditCourseService.data.data);
  //   } else {
  //     this.parent_Comp.allEnrolUser(this.addEditCourseService.data);
  //   }
  //   // this.contentservice.data =demo;
  //   console.log('Service destroy')
  // }

  helpContent: any;
  getHelpContent() {
    return new Promise(resolve => {

      this.http.get('../../../../../../assets/help-content/addEditCourseContent.json').subscribe(
        data => {
          this.helpContent = data;
          console.log('Help Array', this.helpContent);
        },
        () => {
          resolve('err');
        },
      );
    });
    // return this.helpContent;
  }

  // new enrol ui functions

  performActionOnData(event) {
    console.log("Event ==>", event);
    if (event) {
      // console.log('args ==>', event.argument.join(','));
      // console.log('args ==>', [...event.argument]);
      switch (event.action) {
        // Tab Events
        case "manual":
          // code...
          // this.enrolment.manual = true;
          this.allEnrolUser();
          break;
        case "ruleBased":
          // code...
          // this.enrolment.rule = true;
          this.allruleList();
          break;
        case "self":
          // this.enrolment.self = true;
          this.selfenrolledUser(this.content);
          // code...
          break;
        case "regulatory":
          // this.enrolment.regulatory = true;
          // this.allregulatorylist(this.content);
          // code...
          break;
        case "pricing":
            // this.enrolment.regulatory = true;
            // this.getPriceList();
            // code...
            break;

        // Manual Enrol Events
        case "searchManEnrol":
          this.searchManEnrol(event.argument[0]);
          break;
        case "clearesearch":
          this.clearesearch();
          break;
        case "onSearch":
          this.onSearch(event.argument[0]);
          break;
          ////
        case "manEnrolUser": 
          this.selectedUsers = event.argument[0];
          this.manEnrolUser();
          break;
          ////
        case "visibilityTableRow":
          this.visibilityTableRowManual(event.argument[0]);
          break;

        // Rule Enrolment
        case "searchRuleEnrol":
          this.searchRuleEnrol(event.argument[0]);
          break;
        case "clearRuleEnrol":
          this.clearRuleEnrol();
          break;
        case "addEditRule":
          this.openRuleModal(event.argument[0], event.argument[1]);
          break;
          ////
        case "visibilityTableRow1":
          this.visibilityTableRowRule(event.argument[0]);
          break;
        case "viewRuleUsers":
          this.viewRuleUsers(event.argument[0]);
          break;
        ////
        case "saveRule":
          // this.selectedUsers = event.argument[0];
          this.ruleData = event.argument[2];
          // this.saveRule(event.argument[0], event.argument[1]);
          this.saveRule(event.argument[1]);
          break;
          ////
        case "ruleTypeSelected":
          this.ruleData = event.argument[1];
          this.ruleTypeSelected(event.argument[0], null);
          break;
          ////
        case "prospTypeSelected":
          this.ruleData = event.argument[1];
          this.prospTypeSelected();
          break;
          ////
        case "removeRuleList":
          // this.selectedUsers = event.argument[0];
          this.ruleData = event.argument[1];
          this.removeRuleList(event.argument[0]);
          break;
          ////
        case "ruleFieldTypeSelected":
          // this.selectedUsers = event.argument[0];
          this.ruleData = event.argument[3];
          this.ruleFieldTypeSelected(event.argument[1], event.argument[2]);
          break;
        case "callRuleFieldType":
          // this.selectedUsers = event.argument[0];
          this.ruleData = event.argument[3];
          this.callRuleFieldType(
            event.argument[0],
            event.argument[1],
            event.argument[2]
          );
          break;
        case "onItemSelectRule":
          // this.selectedUsers = event.argument[0];
          this.ruleData = event.argument[3];
          this.onItemSelectRule(
            event.argument[0],
            event.argument[1],
            event.argument[2]
          );
          break;
        case "onSelectAllRule":
          // this.selectedUsers = event.argument[0];
          this.ruleData = event.argument[3];
          this.onSelectAllRule(
            event.argument[0],
            event.argument[1],
            event.argument[2]
          );
          break;
        case "OnItemDeSelectRule":
          // this.selectedUsers = event.argument[0];
          this.ruleData = event.argument[3];
          this.OnItemDeSelectRule(
            event.argument[0],
            event.argument[1],
            event.argument[2]
          );
          break;
        case "onDeSelectAllRule":
          // this.selectedUsers = event.argument[0];
          this.ruleData = event.argument[3];
          this.onDeSelectAllRule(
            event.argument[0],
            event.argument[1],
            event.argument[2]
          );
          break;
        case "addRuleList":
          // this.selectedUsers = event.argument[0];
          this.addRuleList();
          break;
        case "closePopup":
            // this.selectedUsers = event.argument[0];
            this.closeRuleModal();
            break;
        case "updateFormValuesRules":
              // this.selectedUsers = event.argument[0];
              // this.closeRuleModal();
              this.ruleData = event.argument[0];
              break;
              ////
        case "closeRuleEnrolPopup":
                // this.selectedUsers = event.argument[0];
                // this.closeRuleModal();
               this.closeRuleUsersModal();
                break;

        // Regulatory
        case "searchRuleEnrol":
          this.searchRuleEnrol(event.argument[0]);
          break;
        case "clearRuleEnrol":
          this.clearRuleEnrol();
          break;
        // case "addEditRule":
        //   this.openRuleModal(event.argument[0], event.argument[1]);
        //   break;
        ////
        case "regulatoryVisiblityChange":
          // this.visibilityTableRow2(event.argument[0]);
          break;
          ////
        case "saveReg":
          // this.saveReg(event.argument[0]);
          break;
          ////
        case "saveRegFilter":
            this.regularData = event.argument[2];
            // this.saveRegFilter(event.argument[0],event.argument[1]);
            break;
          ////
        case "closeRegulatoryFilterModal":
          // this.selectedUsers = event.argument[0];
          this.closeRegulatoryFilterModal();
          break;
        case "removeRegFilter":
            // this.selectedUsers = event.argument[0];
            this.removeRegFilter(event.argument[0]);
            break;
        case "regFilterTypeSelected":
          this.regularData = event.argument[3];
          this.ruleTypeSelected(event.argument[0], null);
          break;
          ////
        case "callTypeRegFilter":
          this.regularData = event.argument[3];
          this.callTypeRegFilter(event.argument[0], event.argument[1],event.argument[2]);
          break;
          ////
        case "onItemSelectRegulatory":
            // this.selectedUsers = event.argument[0];
            this.regularData = event.argument[3];
            this.onItemSelectRule(
              event.argument[0],
              event.argument[1],
              event.argument[2]
            );
            break;
          case "onSelectAllRegulatory":
            // this.selectedUsers = event.argument[0];
            this.regularData = event.argument[3];
            this.onSelectAllRule(
              event.argument[0],
              event.argument[1],
              event.argument[2]
            );
            break;
          case "OnItemDeSelectRegulatory":
            // this.selectedUsers = event.argument[0];
            this.regularData = event.argument[3];
            this.OnItemDeSelectRule(
              event.argument[0],
              event.argument[1],
              event.argument[2]
            );
            break;
          case "onDeSelectAllRegulatory":
            // this.selectedUsers = event.argument[0];
            this.regularData = event.argument[3];
            this.onDeSelectAllRule(
              event.argument[0],
              event.argument[1],
              event.argument[2]
            );
            break;
          case "onSelectAllRegulatory":
              // this.selectedUsers = event.argument[0];
              this.regularData = event.argument[3];
              this.onSelectAllRule(
                event.argument[0],
                event.argument[1],
                event.argument[2]
              );
              break;
        case "openRegulatoryFilterModal":
                // this.selectedUsers = event.argument[0];
               this.openRegulatoryFilterModal();
                break;

        case "addRegulatoryFilter":
          // this.selectedUsers = event.argument[0];
          this.addRegulatoryFilter();
          break;
       case "updateFormValuesRegulatory":
            // this.selectedUsers = event.argument[0];
            // this.closeRuleModal();
            this.regularData = event.argument[0];
            break;
        // Self

        case "clearself":
          // this.selectedUsers = event.argument[0];
          this.clearself();
          break;
        case "searchSelfEnrol":
          // this.selectedUsers = event.argument[0];
          this.searchSelfEnrol(event.argument[0]);
          break;
        case "openSelfModal":
          this.openSelfModal();
          break;
          ////
        case "changeVisibilitySelf":
          // this.visibilityTableRow3(event.argument[0]);
          break;
        case "onItemSelectSelf":
          // this.selectedUsers = event.argument[0];
          this.selfFieldsData = event.argument[3];
          this.onItemSelectSelf(
            event.argument[0],
            event.argument[1],
            event.argument[2],
          );
          break;
        case "OnItemDeSelectSelf":
          // this.selectedUsers = event.argument[0];
          this.selfFieldsData = event.argument[3];
          this.OnItemDeSelectSelf(
            event.argument[0],
            event.argument[1],
            event.argument[2]
          );
          break;
       case "onSelectAllSelf":
            // this.selectedUsers = event.argument[0];
            this.selfFieldsData = event.argument[3];
            this.onSelectAllSelf(
              event.argument[0],
              event.argument[1],
              event.argument[2]
            );
            break;
      case "onDeSelectAllSelf":
              // this.selectedUsers = event.argument[0];
              this.selfFieldsData = event.argument[3];
              this.onSelectAllSelf(
                event.argument[0],
                event.argument[1],
                event.argument[2]
              );
              break;
              ////
        case "saveSelfFields":
          // this.selectedUsers = event.argument[0];
          this.selfFieldsData = event.argument[0];
          // this.saveSelfFields(event.argument[0], event.argument[1]);
          break;
          ////
        case "closeSelfModal":
            // this.selectedUsers = event.argument[0];
            // this.closeSelfModal();
            break;
            ////
        case "selfFieldTypeSelected":
              // this.selectedUsers = event.argument[0];
              // this.selfFieldsData = event.argument[3];
              // this.selfFieldTypeSelected(event.argument[0], event.argument[1],event.argument[2]);
              break;
              ////
        case "callTypeSelfFields":
                // this.selectedUsers = event.argument[0];
                this.selfFieldsData = event.argument[3];
                // this.callTypeSelfFields(event.argument[0], event.argument[1],event.argument[2]);
                break;
              ////
        case "addSelfFields":
                  // this.selectedUsers = event.argument[0];
                  // this.addSelfFields();
                  break;
            ////
        case 'removeSelfFields':
                // this.removeSelfFields(event.argument[0], event.argument[1],event.argument[2]);
                break;
        case "updateFormValuesSelf":
                  // this.selectedUsers = event.argument[0];
                  // this.closeRuleModal();
                  this.selfFieldsData = event.argument[0];
                  break;

        // Price
        ////
        case 'createUpdatePrice': 
        // this.addEditPriceForm = event.argument[0];
                                  // this.createUpdatePrice(event.argument[0]);
                                  break;
        ////
        case 'searchBar': 
        // this.searchOnPriceList(event.argument[0]);
                                  break;
        ////
        case 'bindValueToAddEditForm': 
        // this.bindValueToAddEditForm(event.argument[0],event.argument[1]);
                                      break;
        ////
        case 'clearSearch': 
        // this.clearSearch();
                                      break;
        ////
        case 'closeSidebar': 
        // this.closeSidebar();
                                      break;
      }
    }
  }

  passDataToChild() {
    this.config.manulEnrolmentData.userList = [...this.tempUsers];
    this.config.manulEnrolmentData.data = [...this.rowsManual];
    this.config.manulEnrolmentData.selectedUsers = [...this.selectedUsers];

    this.config.ruleBasedEnrolmentData.data = [...this.rowsRules];

    this.config.regulatoryEnrolmentData.data = [...this.rowsReg];

    this.config.ruleBasedEnrolmentData.helpContent = _.clone(this.helpContent);
    this.config.ruleBasedEnrolmentData.profileFieldSelected = _.clone(
      this.profileFieldSelected
    );
    this.config.ruleBasedEnrolmentData.profileFields = _.cloneDeep[this.profileFields]
    this.config.ruleBasedEnrolmentData.ruleData = _.cloneDeep(this.ruleData);
    this.config.ruleBasedEnrolmentData.controlList = _.cloneDeep(this.controlList);
    this.config.ruleBasedEnrolmentData.rowsEnrolRule = [...this.rowsEnrolRule];
    this.config.ruleBasedEnrolmentData.enrolUserPopupTableLabel = [this.labels4];
    this.config.ruleBasedEnrolmentData.showEnroleduserPopup =_.clone(this.showRuleUsersModal);
    this.config.ruleBasedEnrolmentData.showAddRuleModal = _.clone(this.showAddRuleModal);
    this.config.ruleBasedEnrolmentData.profileFields = _.cloneDeep(
      this.profileFields
    );

    this.config.regulatoryEnrolmentData.showAddRegulatoryFilterModal = _.clone(this.showAddRegulatoryFilterModal)
    // this.config.ruleBasedEnrolmentData.ruleData.profiles = _.clone(this.ruleData.profiles);

    this.config.regulatoryEnrolmentData.helpContent = _.clone(this.helpContent);
    this.config.regulatoryEnrolmentData.regFilterProfileFieldSelected = _.clone(this.regFilterProfileFieldSelected);
    this.config.regulatoryEnrolmentData.profileFieldsRegFilter = [...this.profileFieldsRegFilter];
    this.config.regulatoryEnrolmentData.controlList = [...this.controlList];
    this.config.regulatoryEnrolmentData.regularData =  _.clone(this.regularData);

    this.config.selfEnrolmentData.helpContent = _.clone(this.helpContent);
    this.config.selfEnrolmentData.showAddSelfModal = _.clone(this.showAddSelfModal);
    this.config.selfEnrolmentData.selfFieldsData = _.cloneDeep(this.selfFieldsData);
    this.config.selfEnrolmentData.selfType = _.cloneDeep(this.selfType);
    // this.config.selfEnrolmentData.isFetchingSettings = _.clone(this.isFetchingSettings);
    this.config.selfEnrolmentData.controlList = _.clone(this.controlList);
    this.config.selfEnrolmentData.profileFieldsSelf = _.clone(this.profileFieldsSelf);
    this.config.selfEnrolmentData.selfFeildType = _.clone(this.selfFeildType);
    this.config.selfEnrolmentData.data = _.clone(this.rowsSelf);
    // Pricing

    // this.config.priceBasedEnrolmentData.data = _.clone(this.tempDisplayPriceList);
    // this.config.priceBasedEnrolmentData.currencyTypeDropDown = _.clone(this.currencyTypeDropDown);
    // this.config.priceBasedEnrolmentData.discountListDropdownList = _.clone(this.discountListDropdownList);
    // this.config.priceBasedEnrolmentData.addEditPriceForm = _.clone(this.addEditPriceForm);
    // this.config.priceBasedEnrolmentData.showSidebar = _.clone(this.showPriceSidebar);
    // this.config.priceBasedEnrolmentData.labels =  _.clone(this.labelsPrice);
    // console.log("this.profileFieldSelected", this.profileFieldSelected);
    console.log(
      "this.config",
      this.config
    );
    this.cdf.detectChanges();
  }

  clearRuleEnrol() {
    // if (this.searchText.length >= 3) {
    //   this.searchvalue = {};
    //   this.allruleList(this.content);
    // } else {
    //   this.searchvalue = {};
    // }
    this.allruleList();
  }

  onItemSelectSelf(currentEvent: any, currentIndex: any, currentItem: any) {
    console.log(
      "currentEvent, currentIndex, currentItem",
      currentEvent,
      currentIndex,
      currentItem
    );
  }
  OnItemDeSelectSelf(currentEvent: any, currentIndex: any, currentItem: any) {
    console.log(
      "currentEvent, currentIndex, currentItem",
      currentEvent,
      currentIndex,
      currentItem
    );
  }
  onSelectAllSelf(currentEvent: any, currentIndex: any, currentItem: any) {
    console.log(
      "currentEvent, currentIndex, currentItem",
      currentEvent,
      currentIndex,
      currentItem
    );
  }
  onDeSelectAllSelf(currentEvent: any, currentIndex: any, currentItem: any) {
    console.log(
      "currentEvent, currentIndex, currentItem",
      currentEvent,
      currentIndex,
      currentItem
    );
  }
  onItemSelectRule(currentEvent: any, currentIndex: any, currentItem: any) {
    console.log(
      "currentEvent, currentIndex, currentItem",
      currentEvent,
      currentIndex,
      currentItem
    );
  }
  OnItemDeSelectRule(currentEvent: any, currentIndex: any, currentItem: any) {
    console.log(
      "currentEvent, currentIndex, currentItem",
      currentEvent,
      currentIndex,
      currentItem
    );
  }
  onSelectAllRule(currentEvent: any, currentIndex: any, currentItem: any) {
    console.log(
      "currentEvent, currentIndex, currentItem",
      currentEvent,
      currentIndex,
      currentItem
    );
  }
  onDeSelectAllRule(currentEvent: any, currentIndex: any, currentItem: any) {
    console.log(
      "currentEvent, currentIndex, currentItem",
      currentEvent,
      currentIndex,
      currentItem
    );
  }
  clearself() {
    // if (this.searchText.length >= 3) {
    //   this.searchvalue = {};

    // } else {
    //   this.searchvalue = {};
    // }
    this.selfenrolledUser(this.content);
  }
  
}
