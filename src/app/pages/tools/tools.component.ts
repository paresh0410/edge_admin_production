import { Component, OnInit, ViewChild, ViewEncapsulation, ChangeDetectorRef, SimpleChanges, DoCheck, AfterViewInit, OnChanges, SimpleChange, Output, EventEmitter } from '@angular/core';
import { Router, Route, ActivatedRoute } from '@angular/router';
import { NgForm, FormControl } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ParticipantsService } from '../coaching/participants/participants.service';
import { ToolsService } from './tools.service';
import { Toast, ToastrService } from 'ngx-toastr';
// import { ToasterService } from 'angular2-toaster';
import { DateAdapter } from 'angular-calendar';
import { dataBound } from '@syncfusion/ej2-grids';
import { Card } from '../../models/card.model';
import { SuubHeader } from '../components/models/subheader.model';
import { noData } from '../../models/no-data.model';

@Component({
  selector: 'ngx-tools',
  templateUrl: './tools.component.html',
  styleUrls: ['./tools.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ToolsComponent implements OnInit{
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No section added.",
    desc:"",
    titleShow:true,
    btnShow:true,
    descShow:false,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/tools-section',
  }
  skeleton = false;
  labelCard: any;
  title:string='Add Section'
  btnName:string='Save'
  tooltipName:string='Enter the name of the Section'
  tooltipDesc:string='Enter description for section'
  tooltipCont:string='Enter the Content of the Section'
  tooltipIcon:string='Enter the Icon of the Section'
  cardModify: Card =   {
    flag: 'tool',
    titleProp: 'sectionName',
    sectionIcon: 'sectionIcon',
    discrption: 'sectionDesc',
    showIcon: true,
    hoverlable: true,
    option: true,
    eyeIcon: true,
    editIcon: true,
    bottomDiv: true,
    bottomTitle: true,
    bottomDiscription: true,
    btnLabel: 'Details',
    customCard: 'toolCard',
  };
  count:number=8;
  header: SuubHeader ;
  enableDisableToolModal: boolean=false;
  enableTool: boolean;
  // enableDisableTrainerModal: boolean;
  enableToolData: any;
  nav=[
    {
      'sameComp':true,
      'id':{},
      'name' : 'Tools',
      'navigationPath':'/pages/tools'
    },
  ];
  secName: any;
  insidesec: number=1;
  index: any;
  previousdata: any;
  menuId: any;
  // breadcrum: any;
  constructor(private router: Router,
    private routes: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private toolsService: ToolsService,
    // private toasterService: ToasterService,
    private cdf: ChangeDetectorRef,
    private toastr: ToastrService) {
    this.parentSectionId = null;
    if (localStorage.getItem('LoginResData')) {
      var userData = JSON.parse(localStorage.getItem('LoginResData'));
      console.log('userData', userData.data);
      this.userId = userData.data.data.id;
    }
    if(localStorage.getItem('menuId')){
      var userData = JSON.parse(localStorage.getItem('menuId'));
      console.log('userData', userData);
      this.menuId=userData;
    }
    this.flag = 'section';
    this.labelCard = 'Details'

    //  this.fallbackIcon = this.toolsService.alltoolsSection.sectionIcon;
    //  console.log('this.fallbackIcon', this.fallbackIcon);

  }
  @ViewChild('categoryFormValidation') conForm: NgForm;
  @Output() breadcrum  = new EventEmitter<any>();


  secIcons = new FormControl();
  fallbackIcon: any = null;
  icon: string = '';
  secIcon: string = null;
  ///////////////////////////

  showSection: boolean = false;
  addAction: boolean;
  tenantId: any;
  sectionForm: any = [];
  allSection: any = [];
  errorMsg: any;
  userId: any;
  msg: any;
  visibleStatus: boolean = true;
  sectionValue: any;
  secVisibleStatus: any;
  flag: any;
  dropdownActivity: any = [];
  noDataFound: boolean = false;
  parentSectionId: any = null;
  oldParentSecId: any = null;
  Content: any = [
    {
      'id': 1,
      'name': 'Section',
    },
    {
      'id': 2,
      'name': 'Activity',
    }
  ]

  section: any = [
    {
      'id': 1,
      'Name': 'Videos',
      'Description': 'Content in the Video',
      'Content': 'Section',
      'visible': true,
      'categoryPicRef': 'https://bhaveshedgetest.s3.ap-south-1.amazonaws.com/book.jpg',
    },
    {
      'id': 2,
      'Name': 'Audio',
      'Description': 'Content in the Audio',
      'Content': 'Section',
      'visible': true,
      'categoryPicRef': 'https://bhaveshedgetest.s3.ap-south-1.amazonaws.com/book.jpg',
    },
    {
      'id': 3,
      'Name': 'PPT',
      'Description': 'Content in the PPT',
      'Content': 'Activity',
      'visible': false,
      'categoryPicRef': 'https://bhaveshedgetest.s3.ap-south-1.amazonaws.com/book.jpg',
    },
    {
      'id': 4,
      'Name': 'Trailer',
      'Description': 'Content in the Trailer',
      'Content': 'Activity',
      'visible': true,
      'categoryPicRef': 'https://bhaveshedgetest.s3.ap-south-1.amazonaws.com/book.jpg',
    },
    {
      'id': 5,
      'Name': 'AV',
      'Description': 'Content in the AV',
      'Content': 'Section',
      'visible': false,
      'categoryPicRef': 'https://bhaveshedgetest.s3.ap-south-1.amazonaws.com/book.jpg',
    },
    {
      'id': 6,
      'Name': 'Images',
      'Description': 'Content in the Images',
      'Content': 'Section',
      'visible': true,
      'categoryPicRef': 'null',
    },

  ]
  ngOnInit() {
// if(this.menuId==4){
//   this.parentSectionId = null;
//   this.nav=[]
// }else{
    if (this.toolsService.fromContent === true) {
      this.parentSectionId = this.toolsService.parentSecId;
      this.nav=this.toolsService.nav
      // this.index=this.toolsService.navObj.index
      // this.secName=this.toolsService.nav[this.index].id.sectionName
    console.log(this.toolsService.navObj.section)
      this.secName=this.toolsService.navObj.section
      this.previousPushObject={
        'sameComp':true,
        'id':this.toolsService.navObj.data,
        'name' : this.toolsService.navObj.section,
        'navigationPath':'/pages/tools'
      }
      this.previousdata=this.toolsService.navObj.data
      // this.previousPushObject=this.toolsService.navObj.data
    }
  // 
    this.toolsService.fromContent = false;
    this.tenantId = this.toolsService.tenantId;
    this.getAllSectionTools(this.parentSectionId);
    this.getDropdownActivitys();

  // }
  }
  noTools: boolean = false;
  searchtext: string;
  SarchFilter(event) {
    this.searchtext = event.target.value
    // if(event.keyCode == 13){
      
    // }
    var searchtext = event.target.value
    if(!searchtext && event.keyCode !== 13 ){
      this.getAllSectionTools(this.parentSectionId)
    }else if(searchtext.length>=3){
    console.log(searchtext);
    const val = searchtext.toLowerCase();
    const tempcc = this.allSection.filter(function (d) {
      return String(d.sectionName).toLowerCase().indexOf(val) !== -1 ||
        d.descriptionon.toLowerCase().indexOf(val) !== -1 ||
        // d.relayDate.toLowerCase().indexOf(val) !== -1 ||
        // d.manager.toLowerCase().indexOf(val) !== -1 ||
        !val
    })
    console.log(tempcc);
    this.allSection = tempcc;
    if (!this.allSection.length) {
      // this.noTools = true;
      this.noDataFound = true;
      this.noDataVal={
        margin:'mt-5',
        imageSrc: '../../../../../assets/images/no-data-bg.svg',
        title:"Sorry we couldn't find any matches please try again",
        desc:".",
        titleShow:true,
        btnShow:false,
        descShow:false,
        btnText:'Learn More',
        btnLink:''
      }
    } else {
      // this.noTools = false;
      this.noDataFound = false;
    }
  }
}
  clear() {
    if(this.searchtext){
      this.noDataFound=false
    this.searchtext = "";
    this.getAllSectionTools(this.parentSectionId);
    }
  }


  editIcon: boolean = true;
  newIcon: boolean = true;
  temp1 = '';
  disFlag: any;

  ////////////////////////
  onIconPickerSelect(icon: any): void {
    const temp = icon;

    if (this.addAction === false) {

          if (temp ===  'fa fa-user-plus' ) {
            icon = this.sectionForm.secIcon;
          }
        console.log('icon', icon);
        this.secIcons.setValue(icon);
        this.secIcon = icon;
        console.log('this.secIcon', this.secIcon);
        this.sectionForm.secIcon = icon;
        console.log('this.secIcon', this.secIcon);
        this.editIcon = true;

    } else {
                if (temp ===  'fa fa-user-plus' ) {
                  icon = this.sectionForm.secIcon;
                }
                // icon = this.sectionForm.secIcon;
                console.log('icon', icon);
                this.secIcons.setValue(icon);
                this.secIcon = icon;
                console.log('this.secIcon', this.secIcon);
                this.sectionForm.secIcon = icon;
                console.log('this.secIcon', this.secIcon);

    }
  }




  onSubmit() {
    console.log(this.conForm);
  }
  // presentToast(type, title, body) {
  //   const toastP: any = {
  //     type: type,
  //     title: title,
  //     body: body,
  //     showCloseButton: true,
  //     timeout: 2000,
  //   };
  //   this.toasterService.pop(toastP);
  // }

  presentToast(type, body) {
    if(type === 'success'){
      this.toastr.success(body, 'Success', {
        closeButton: false,
       });
    } else if(type === 'error'){
      // tslint:disable-next-line: max-line-length
      this.toastr.error('Please contact site administrator and <div (click)="giveFeedback()"> click here <div> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true,
      });
    } else{
      this.toastr.warning(body, 'Warning', {
        closeButton: false,
        });
    }
  }

  //showBellow:boolean=false;
  getAllSectionTools(parent) {
    // if(this.toolsService.nav){
    //   this.nav=this.toolsService.nav
    // }
    this.skeleton = false;
    this.allSection =[];
    this.parentSectionId = parent;
    // this.spinner.show();
    const param = {
      'parSecId': parent,
      'tId': this.tenantId,
    };
    if(this.parentSectionId==null){
      this.header ={
        title:'Tools',
        btnsSearch: true,
        searchBar: true,
        placeHolder:'Search Tools',
        btnAdd: 'Add Section',
        btnAddshow: true,
        showBreadcrumb:true,
        breadCrumbList:[]
      };
      this.noDataVal={
        margin:'mt-3',
        imageSrc: '../../../../../assets/images/no-data-bg.svg',
        title:"No section added.",
        desc:"",
        titleShow:true,
        btnShow:true,
        descShow:false,
        btnText:'Learn More',
        btnLink:'https://faq.edgelearning.co.in/kb/tools-section',
      }
    }
    else{
      this.header ={
        title:this.secName,
        btnsSearch: true,
        btnAdd: 'Add Section',
        btnBackshow: true,
        btnAddshow: true,
        showBreadcrumb: true,
        isSame:true,
        breadCrumbList:[
           {
              'sameComp':true,
              'id':{},
              'name' : 'Tools',
              'navigationPath':'/pages/tools'
            },
        ]
      };
   
      this.header.breadCrumbList=this.nav    
    // this.header.breadCrumbList.push(this.nav)
    this.noDataVal={
      margin:'mt-3',
      imageSrc: '../../../../../assets/images/no-data-bg.svg',
      title:"No hierarchical section added.",
      desc:"",
      titleShow:true,
      btnShow:true,
      descShow:false,
      btnText:'Learn More',
      btnLink:'https://faq.edgelearning.co.in/kb/tools-hierarchical-section',
    }

    }

    console.log('all section param:', param);
    this.toolsService.getAllSection(param)
      .then(rescompData => {
        this.spinner.hide();
        this.skeleton = true;
        var result = rescompData;
        console.log('Section:', rescompData);
        if (result['type'] === true) {

          if (result['data'][0].length === 0) {
            this.oldParentSecId = null;
          } else {
            this.oldParentSecId = result['data'][0][0].parentSectionId;
          }

          if (result['data'][1].length === 0) {
            this.noDataFound = true;
            this.skeleton = true;
           // this.showBellow =false;
           // this.presentToast('warning', result['data'][1].msg);
          } else {
            this.allSection = result['data'][1];
            // for(let i = 0; i< this.allSection.length; i++) {
            //   this.allSection[i].disable =  this.allSection[i].visible;
            //   this.allSection[i].image = this.allSection[i].sectionIcon;
            //   this.allSection[i].title = this.allSection[i].sectionName;
            //   this.allSection[i].discrption = this.allSection[i].sectionDesc;
            //   this.allSection[i].showIcon = true;
            // }
            this.noDataFound = false;
            console.log('this.allSection', this.allSection);

            for (let i = 0; i < this.allSection.length; i++) {
              this.allSection[i].name = this.coverthtmltotext(this.allSection[i].name);
              this.allSection[i].descriptionon = this.coverthtmltotext(this.allSection[i].sectionDesc);
            }
            this.cdf.detectChanges();
          }
          //this.cdf.detectChanges();
          this.skeleton = true;
        } else {
          this.skeleton = true;
          this.noDataFound = true;
          this.presentToast('error', '');        }


      }, error => {
        this.spinner.hide();
        this.skeleton = true;

        this.presentToast('error', '');
     });
  }
  coverthtmltotext(data) {
    if(data){
    var htmlString = data;
    var stripedHtml = htmlString.replace(/<[^>]+>/g, '');
    console.log(stripedHtml);
    return (stripedHtml);
    }
  }

  getDropdownActivitys() {
    // this.spinner.show();
    const param = {
      'tId': this.tenantId,
    };
    this.toolsService.getDropdownActivity(param)
      .then(rescompData => {
        // this.spinner.hide();
        var result = rescompData;
        console.log('getDropdownActivity:', rescompData);
        if (result['type'] === true) {
          if (result['data'][0].length === 0) {
           // this.presentToast('warning', result['data'][0].msg);
           console.log('no dropdown found');

          } else {

          //  this.presentToast('success', result['data'][1].msg);
            this.dropdownActivity = result['data'][0];
            console.log('this.dropdownActivity', this.dropdownActivity);

          }

        } else {

          this.presentToast('error', '');
      }
      this.skeleton = true;

      }, error => {
        this.spinner.hide();
        this.skeleton = true;
        this.presentToast('error', '');
      });
  }


  addEditSectionTool(params) {
    console.log('paramNew', params);
    this.spinner.show();
    this.toolsService.addEditToolsSection(params)
      .then(rescompData => {
        this.spinner.hide();
        const temp = rescompData;
        this.showSection = false;
        console.log('add edit result', temp);
        if (temp['type'] === true) {
          if (this.addAction === true) {
            this.presentToast('success', temp['data'][0][0].msg);
          } else {
            this.presentToast('success', temp['data'][0][0].msg);
          }
          this.parentSectionId = temp['data'][0][0].parentSectionId;
          console.log('this.parentSectionId', this.parentSectionId);
          this.getAllSectionTools(this.parentSectionId);
        } else {
          this.presentToast('error', '');
      }

      },
        resUserError => {
          this.spinner.hide();
          this.presentToast('error', '');
          this.errorMsg = resUserError;
        });
  }



  addEditSection(data, value) {
    console.log('data', data);
    if (value === 0) {
      this.addAction = true;
this.title='Add Section'
      this.sectionForm = {
        id: 0,
        parentSecId: this.parentSectionId,
        secContent: '',
        secDescription: '',
        secIcon: '',
        secName: '',
        visible: '',

      };
      this.showSection = true;
      // this.enableToolData-this.sectionForm;

      console.log('newData', this.sectionForm);
    } else {
      this.addAction = false;
      this.title='Edit Section'
      this.sectionForm = {
        id: data.id,
        parentSecId: data.parentSectionId,
        secContent: data.sectionContent,
        secDescription: data.sectionDesc,
        secIcon: data.sectionIcon,
        secName: data.sectionName,
        visible: data.visible,
      };
      this.showSection = true;
    // this.enableToolData=this.sectionForm;

      console.log('editData', this.sectionForm);
    }

  }

  EditSection(data) {
    this.addAction = false;
this.title='Edit Section'
    this.sectionForm = {
      id: data.id,
      parentSecId: data.parentSectionId,
      secContent: data.sectionContent,
      secDescription: data.sectionDesc,
      secIcon: data.sectionIcon,
      secName: data.sectionName,
      visible: data.visible,
    };
    // this.enableToolData=this.sectionForm;
    this.showSection = true;
  }

  submit(data) {
    data.secIcon = this.secIcon;
    console.log('data', data);
    this.spinner.show();
    const param = {
      'secId': data.id,
      'secName': data.secName,
      'secDesc': data.secDescription,
      'parSecId': data.parentSecId,
      'secCon': data.secContent,
      'secIco': data.secIcon,
      'vis': data.visible,
      'tId': this.tenantId,
      'userId': this.userId,
    }
    console.log('submitData', param);
    this.addEditSectionTool(param);



  }

  closeModal() {
    this.showSection = false;
  }



  disableSection(value, data) {
    console.log('data', data);
    this.enableToolData=value;
    if (value === 1) {
      this.secVisibleStatus = 0;
      this.sectionValue = data;
      // this.sectionForm=data;
      console.log('sectionValue', data);
      this.changeSectionStatus(data.id, this.secVisibleStatus, this.sectionValue);
    } else if (value === 2) {
      this.secVisibleStatus = 1;
      this.sectionValue = data;
      console.log('sectionValue', data);

      this.changeSectionStatus(data.id, this.secVisibleStatus, this.sectionValue);
    }
  }


  changeSectionStatus(secId, secVisible, data) {
    this.spinner.show();
    const param = {
      'secId': secId,
      'secName': data.sectionName,
      'secDesc': data.sectionDesc,
      'parSecId': data.parentSectionId,
      'secCon': data.sectionContent,
      'secIco': data.sectionIcon,
      'vis': secVisible,
      'tId': this.tenantId,
      'userId': this.userId,
    }
    this.toolsService.addEditToolsSection(param)
      .then(rescompData => {

        this.spinner.hide();

        var temp = rescompData;
        var result = temp['data'];
        ////////////////change catId

        if (temp['type'] == true) {
          if(param.vis==1){
            this.toastr.success("Section Enabled Successfully",'Success')
          }else{
            this.toastr.success("Section Disabled Successfully",'Success')

          }
          // this.fetchBadgeCateory();
          console.log('change status ', result)
          this.closeEnableDisableToolModal();
         // this.presentToast('success', result['data'].msg);
          this.getAllSectionTools(this.parentSectionId);

        } else {
          //this.fetchBadgeCateory();
          this.closeEnableDisableToolModal();
          this.presentToast('error', '');        }


      },
        resUserError => {
          this.spinner.hide();
          this.errorMsg = resUserError;
        });


  }

  previousPushObject = null;
  gotodetails(data,eve) {   
    if (data.sectionContent == 1||data.index==0) {
      if(eve == 'eve'){
      //   for(let i=0;i<=this.nav.length;i++){
      //  if(data.sectionName==this.nav[i].name){
      //  this.nav = this.nav.slice(0,i+1)
      //  }
      if(data.index==0){
        this.parentSectionId=null;
      this.nav=[
        {
         'sameComp':true,
          'id':{},
          'name' : 'Tools',
          'navigationPath':'/pages/tools'
        },
      ]
        // this.nav=[];
        this.previousPushObject=null;
      }else{
        this.previousPushObject=this.nav[data.index]
      this.nav = this.nav.slice(0, data.index);
      }
      // this.previousPushObject=null
      // if(data.length==1){
      //   let object = {
      //     'sameComp':true,
      //       'id':data,
      //       'name' : data.sectionName,
      //       'navigationPath':'/pages/tools'
      //     }
      //     this.previousPushObject = object;
          // this.nav.push(this.previousPushObject); 

      // }
        // }
      }else{
      // if( data.parentSectionId != null){
        if(this.previousPushObject){
          this.nav.push(this.previousPushObject); 
        }
        let object = {
        'sameComp':true,
          'id':data,
          'name' : data.sectionName,
          'navigationPath':'/pages/tools'
        }
        this.previousPushObject = object;
       this.previousdata=object.id
      // }
     
    }
      // this.insidesec++;
      this.secName=data.sectionName
      this.toolsService.parentSecId = data.id;
      this.oldParentSecId = data.parentSectionId;
      this.parentSectionId = data.id;
      this.getAllSectionTools(this.parentSectionId);

    } else {
      this.toolsService.parentSecId = data.id;
      let object = {
        'sameComp':true,
        'id':this.previousdata,
        'name' : this.secName,
        'navigationPath':'/pages/tools'
      }
      this.previousPushObject = object;
      this.nav.push(this.previousPushObject); 
      this.toolsService.navObj={
        'routeFrom':null,
        'title':data.sectionName,
        'section':data.sectionName
      }
      this.toolsService.navTool=this.nav
      // this.breadcrum.emit(this.previousPushObject);
      this.router.navigate(['content'], { relativeTo: this.routes });
      this.toolsService.nav=this.nav;
      
    }
  }

  // clickTodisable(item) {
  //   if(item.visible == 1) {
  //     // this.secVisibleStatus = 0;
  //     this.sectionValue = item;
  //     // item.visible = 0;
  //     this.changeSectionStatus(item.id, this.secVisibleStatus, this.sectionValue);
  //     this.closeEnableDisableToolModal();
  //   } else {
  //     // item.visible = 1;
  //     // this.secVisibleStatus = item.vis
  //     this.sectionValue = item;
  //     this.changeSectionStatus(item.id, this.secVisibleStatus, this.sectionValue);
  //     this.closeEnableDisableToolModal();
  //   }
  // }


  goto(data) {
    if (data.sectionContent == 1) {
      this.toolsService.parentSecId = data.id;
      this.oldParentSecId = data.parentSectionId;
      this.parentSectionId = data.id;
      this.getAllSectionTools(this.parentSectionId);

    } else {
      this.toolsService.parentSecId = data.id;
      this.router.navigate(['content'], { relativeTo: this.routes });
    }
  }

  back(event) {
      let oldObj=this.nav[this.nav.length-1];
      this.secName=oldObj.name
      // this.toolsService.parentSecId = oldObj.id;
      // this.oldParentSecId = data.parentSectionId;
      // this.parentSectionId = data.id;
    this.nav.pop();
    if(this.nav.length==0){
      // this.secName=''
      // this.toolsService.parentSecId = '';
      // this.oldParentSecId = '';
      // this.parentSectionId = '';
      this.previousPushObject=null;
    this.nav=[
      {
        'sameComp':true,
        'id':{},
        'name' : 'Tools',
        'navigationPath':'/pages/tools'
      },
    ];
    }
    // this.header.title=this.secName,
    console.log('back pressed',event);
    console.log('this.oldParentSecId', this.oldParentSecId);
    if(this.parentSectionId == null) {
      this.router.navigate(['/pages/dashboard']);

    } else {
      this.getAllSectionTools(this.oldParentSecId);
    }

    // this.router.navigate(['../pages/dashboard']);
  }
  closeEnableDisableToolModal() {
    this.enableDisableToolModal = false;
  }
  enableDisableToolAction(actionType) {

    console.log(this.enableToolData);
    if (actionType == true) {
      if (this.enableToolData.visible == 1) {
        this.enableToolData.visible = 0;
      this.changeSectionStatus(this.enableToolData.id, this.enableToolData.visible, this.enableToolData);
      } else {
        this.enableToolData.visible = 1;
      this.changeSectionStatus(this.enableToolData.id, this.enableToolData.visible, this.enableToolData);

      }
    } else {
      this.closeEnableDisableToolModal();
    }
  }
  clickTodisableTool(item) {
    this.btnName='Yes'
    if (item.visible == 1) {
      this.enableTool = true;
      this.title='Disable Section'
      this.enableToolData = item;
      this.enableDisableToolModal = true;
    } else {
      this.title='Enable Section'
      this.enableTool = false;
      this.enableToolData = item;
      this.enableDisableToolModal = true;
    }
  }

}
