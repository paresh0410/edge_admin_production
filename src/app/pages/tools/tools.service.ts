import { Injectable, Inject } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
// import {AppConfig} from '../../../app.module';
import { HttpClient } from "@angular/common/http";
import { webApi } from '../../service/webApi';
import { AppConfig } from '../../app.module';
import { AuthenticationService } from '../../service/authentication.service';

@Injectable()
export class ToolsService {

    private _urlGetAllToolsSection: string = webApi.domain + webApi.url.get_tools_section;
    private _urlAddEditToolsSection: string = webApi.domain + webApi.url.add_edit_tools_section;
    private _urlGetDropdownActivity: string = webApi.domain + webApi.url.get_dropdown_activity;

    private _urlGetContentForSection: string = webApi.domain + webApi.url.get_content_section;
    private _urlGetDropdownContent: string = webApi.domain + webApi.url.get_content_dropdown;
    private _urlAddEditContent: string = webApi.domain + webApi.url.add_edit_content;
    private _urlhandshakeDamAsset: string = webApi.domain + webApi.url.handshakeDamAsset;
    private _urlhandshakeDamUserVerify: string = webApi.domain + webApi.url.handshakeDamUserVerify;
    private _urlAreaBulEnrol: string = webApi.domain + webApi.url.area_bulk_enrol;



    userData: any;
    tenantId: any;
    addActions: boolean;
    contentDetails: any= [];
    parentSecId: any = null;
    alltoolsSection: any = [];
    alltoolsSubSection: any = [];
    nav:any=[]
    navObj:any={}
    navTool:any=[]
    fromContent: boolean = false;
    //parentSecId : any;
    conId: any;
    contentdata: any = [];
    constructor(@Inject('APP_CONFIG_TOKEN') private config: AppConfig, private _http: Http, private _httpClient: HttpClient,
    private authenticationService: AuthenticationService) {
        //this.busy = this._http.get('...').toPromise();
        if (localStorage.getItem('LoginResData')) {
            this.userData = JSON.parse(localStorage.getItem('LoginResData'));
            console.log('userData', this.userData.data);
            // this.userId = userData.data.data.id;
        }
        this.tenantId = this.userData.data.data.tenantId;
    }


    getAllSection(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlGetAllToolsSection, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }


    addEditToolsSection(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlAddEditToolsSection, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    getContentForSection(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlGetContentForSection, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }


    getContentDropdown(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlGetDropdownContent, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    getDropdownActivity(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlGetDropdownActivity, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    addEditContentSection(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlAddEditContent, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    ApprovedDamHanshakeData(data) {
        // return this._http.post(this._urlGetActivityContent,data)
        //   .map((response:Response) => response.json())
        //   .catch(this._errorHandler);
        return new Promise(resolve => {
          this._httpClient.post(this._urlhandshakeDamAsset, data)
            //.map(res => res.json())
    
            .subscribe(data => {
              resolve(data);
            },
              err => {
                resolve('err');
              });
        });
      }

      generatingToken(userData) {
        return new Promise(resolve => {
          this._httpClient.post(this._urlhandshakeDamUserVerify, userData)
    
            .subscribe(data => {
              resolve(data);
              console.log('Data token response ->', data);
            },
              err => {
                resolve('err');
              });
        });
      }

      areaBulkEnrol(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlAreaBulEnrol, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }
}
