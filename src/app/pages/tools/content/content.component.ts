import { Component, OnInit, ViewChild, ChangeDetectorRef, EventEmitter, Output } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm, FormControl } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
// import { ToasterService } from 'angular2-toaster';
import { ToolsService } from '../tools.service';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { webApi } from '../../../service/webApi';
import { webAPIService } from '../../../service/webAPIService';
import { ToastrService } from 'ngx-toastr';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import { Card } from '../../../models/card.model';
import { SuubHeader } from '../../components/models/subheader.model';
import { noData } from '../../../models/no-data.model';

@Component({
  selector: 'ngx-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss'],
})
export class ContentComponent implements OnInit {
  skeleton = false;
  count:number=8

  @ViewChild('fileUpload') fileUpload: any;
  @ViewChild('video_file') videofile: any;
  @ViewChild('audio_file') audiofile: any;
  @ViewChild('iframe_file') iframe: any;
  @ViewChild('scorm_file') scormfile: any;
  @Output() contentpath  = new EventEmitter<any>();
  toolsenrol: boolean = false;
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No content added.",
    desc:"",
    titleShow:true,
    btnShow:true,
    descShow:false,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/tools-section-content',
  }
  header: SuubHeader 
  cardModify: Card =  {
    titleProp : 'contentName',
    // image: 'image',
    flag: 'content',
    discrption: 'contentDesc',
    contentType: 'contentType',
    showIcon : false,
    showImage:   true,
    hoverlable:   true,
    option:   true,
    eyeIcon:   true,
    copyIcon:   false,
    editIcon:   false,
    bottomDiv:   true,
    bottomTitle:   true,
    bottomDiscription:   true,
    showBottomList:   false,
    showBottomdate:   false,
    showBottomEmployee:   false,
    showBottomManager:   false,
    showBottomCategory:   false,
    showBottomMaximumPoint:   false,
    customCard:  '',
    btnLabel: 'Edit',
    defaultImage: 'assets/images/courseicon.jpg'
  };
  enableDisableContentModal: boolean;
  enableContentData: any;
  enableContent: boolean;
  nav: any;
  previousPushObject: { sameComp: boolean; id: any; name: any; navigationPath: string; };
  title: string;
  btnName: string;
  constructor(private router: Router,
    private commonFunctionService: CommonFunctionsService,
    private routes: ActivatedRoute,
    private cdf: ChangeDetectorRef,
    private webApiService: webAPIService,
    private spinner: NgxSpinnerService,
    private toolsService: ToolsService,
    // private toasterService: ToasterService,
    private toastr: ToastrService) {
    this.parentSectionId = this.toolsService.parentSecId;
    console.log(this.toolsService.nav)
    this.nav=this.toolsService.nav
    // this.toolsService.nav ='gfgfg'
 
    if (localStorage.getItem('LoginResData')) {
      const userData = JSON.parse(localStorage.getItem('LoginResData'));
      this.userId = userData.data.data.id;
    }
    this.labelCard = 'Details';
    this.flag = 'content';


  }
  parSecId: any = null;
  selected: boolean = true;
  assessmentFileData: any = null;
  errorMsg: any;
  conVisibleStatus: any;
  fileUploadRes: any;
  contentValue: any;
  showContent: boolean = false;
  userId: any;
  tenantId;
  noDataFound: boolean = false;
  name: any;
  allContentDropdown: any = [];
  allContent: any = []; 
  labelCard : any;

  parentSectionId;

  flag: any;
  selectFileTitle: any;
  fileReaded: any;
  assetFileData: any;


  ChangeHeader(data){
      // this.header.breadCrumbList=data
  }
  contentData: any = [];


  // onIconPickerSelect(icon: string): void {
  //   console.log('icon', icon);
  //   this.conIcons.setValue(icon);
  //   this.conIcon = icon;
  // }

  addEditContent() {
    // let object()
    this.contentpath.emit();
    this.router.navigate(['add-edit'], { relativeTo: this.routes });
  }

  addEditContentForm(data, value) {
    if (value === 0) {
      const addAction: boolean = true;
      this.contentData = {
        contentDesc: '',
        contentName: '',
        contentType: '',
        id: 0,
        sectionId: this.parentSectionId,
        visible: 1,
        assetName: null,
        description: null,
        assetRef: null,
      }
      this.toolsService.addActions = addAction;
      this.toolsService.contentDetails = this.contentData;
      this.toolsService.conId = data.id;
      this.showContent = true;
      let object = {
        'sameComp':false,
        'id':data,
        'name' : this.toolsService.fromContent==true?this.toolsService.navObj.section:this.toolsService.navObj.title,
        'navigationPath':'/pages/tools/content'
      }
      this.toolsService.navObj={
        'routeFrom':'content',
        'title':'',
        'titleContent':'Add Content',
      }
    this.previousPushObject = object;
    this.nav.push(this.previousPushObject); 
      console.log('newData', this.contentData);
    this.toolsService.nav =this.nav
      this.router.navigate(['add-edit-content'], { relativeTo: this.routes });
    } else {
      const addAction: boolean = false;
      this.contentData = {
        contentDesc: data.contentDesc,
        contentName: data.contentName,
        contentType: data.contentType,
        id: data.id,
        sectionId: data.sectionId,
        visible: data.visible,
        assetName: data.assetName,
        description: data.assetDesc,
        assetRef: data.assetRef,
      };
      this.toolsService.navObj={
        'routeFrom':'content',
        'title':'',
        'titleContent':this.contentData.contentName
      }
      this.showContent = true;
      this.toolsService.addActions = addAction;
      this.toolsService.contentDetails = this.contentData;
      this.toolsService.conId = data.id;
      console.log('editData', this.contentData);
      this.router.navigate(['add-edit-content'], { relativeTo: this.routes });
    }

  }

  EditContent(data) {
    const addAction: boolean = false;
    this.contentData = {
      contentDesc: data.contentDesc,
      contentName: data.contentName,
      contentType: data.contentType,
      id: data.id,
      sectionId: data.sectionId,
      visible: data.visible,
      assetName: data.assetName,
      description: data.assetDesc,
      assetRef: data.assetRef,
    };
    this.showContent = true;
    this.toolsService.addActions = addAction;
    this.toolsService.contentDetails = this.contentData;
    this.toolsService.conId = data.id;
    let object = {
      'sameComp':false,
      'id':data,
      'name' : this.toolsService.fromContent==true?this.toolsService.navObj.section:this.toolsService.navObj.title,
      'navigationPath':'/pages/tools/content'
    }
    this.previousPushObject = object;
    this.nav.push(this.previousPushObject); 
    this.toolsService.navObj={
      'routeFrom':'content',
      'title':'',
      'titleContent':this.contentData.contentName
    }
    this.toolsService.nav =this.nav
    console.log('editData', this.contentData);
    this.router.navigate(['add-edit-content'], { relativeTo: this.routes });
  }
backToTools(data){
  this.nav = this.nav.slice(0, data.index);
  this.toolsService.parentSecId=data.id
  this.toolsService.fromContent = true;
  this.toolsService.navObj={
    'routeFrom':'content',
    'title':'',
    'titleContent':this.contentData.contentName,
    'index':data.index,
    'section':this.toolsService.nav[data.index].name,
    'data':this.toolsService.nav[data.index].id
    }
  this.toolsService.nav=this.nav

    console.log(this.toolsService.navObj.section)
  this.router.navigate(['../pages/tools']);

}

  ngOnInit() {

    console.log(this.contentData)

    this.tenantId = this.toolsService.tenantId;

    this.header= {
      title:this.toolsService.fromContent==true?this.toolsService.navObj.section:this.toolsService.navObj.section,
      btnsSearch: true,
      searchBar: false,
      dropdownlabel: '',
      drplabelshow: false,
      drpName1: '',
      drpName2: ' ',
      drpName3: '',
      drpName1show: false,
      drpName2show: false,
      drpName3show: false,
      btnName1: '',
      btnName2: '',
      btnName3: '',
      btnAdd: 'Add Content',
      btnName1show: false,
      btnName2show: false,
      btnName3show: false,
      btnBackshow: true,
      btnAddshow: true,
      filter: false,
      showBreadcrumb: true,
      breadCrumbList:this.toolsService.nav
    };
    // this.getContentForSection();
    this.getsectioncontent();
    if (this.toolsService.fromContent === true) {
      this.parentSectionId = this.toolsService.parentSecId;
      console.log(this.toolsService.navObj.section)
      this.nav=this.toolsService.nav
    }else{
      this.header.title=this.toolsService.navObj.section
    }
    this.parentSectionId = this.toolsService.parentSecId;
 
   
  }
  disableContent(value, data) {
    console.log('data', data);
    if (value == 1) {
      this.conVisibleStatus = 0;
      this.contentValue = data;
      console.log('sectionValue', data);
      this.changeContentStatus(data.id, this.conVisibleStatus, this.contentValue);
    } else if (value == 2) {
      this.conVisibleStatus = 1;
      this.contentValue = data;
      console.log('sectionValue', data);

      this.changeContentStatus(data.id, this.conVisibleStatus, this.contentValue);
    }
  }

  // clickTodisable(item) {
  //   if(item.visible == 1) {
  //     this.conVisibleStatus = 0;
  //     this.contentValue = item;
  //     item.visible = 0;
  //     this.changeContentStatus(item.id, this.conVisibleStatus, this.contentValue);
  //   } else {
  //     item.visible = 1;
  //     this.conVisibleStatus = 1;
  //     this.contentValue = item;
  //     this.changeContentStatus(item.id, this.conVisibleStatus, this.contentValue);
  //   }
  // }

  changeContentStatus(conId, conVisible, data) {
    this.spinner.show();
    const param = {
      'secId': this.parentSectionId,
      'conId': conId,
      'conName': data.contentName,
      'conDesc': data.contentDesc,
      'conRef': data.contentRef,
      'conType': data.contentType,
      'vis': conVisible,
      'userId': this.userId,
      'tId': this.tenantId,
    }
    const _urlAddEditContent: string = webApi.domain + webApi.url.add_edit_content;
    this.commonFunctionService.httpPostRequest(_urlAddEditContent,param)

    //this.toolsService.addEditContentSection(param)
      .then(rescompData => {

        this.spinner.hide();

        var temp = rescompData;
        var result = temp['data'];
        ////////////////change catId

        if (temp['type'] === true) {
          // this.fetchBadgeCateory();
          console.log('change status ', result);
          this.closeEnableDisableContentModal();
          // this.presentToast('success', result[0][0].msg);
          if(param.vis==1){
          this.toastr.success( 'Content Enabled Successfully','Success');
          }else{
          this.toastr.success( 'Content Disabled Successfully','Success');
          }
          // this.getContentForSection();
          this.getsectioncontent()
        } else {
          //this.fetchBadgeCateory();
          this.closeEnableDisableContentModal();
          this.presentToast('error', '');
        }


      },
        resUserError => {
          this.spinner.hide();
          this.presentToast('error', '');
          this.errorMsg = resUserError;
        });


  }






  getsectioncontent() {
    // this.spinner.show();
    const param = {
      'secId': this.parentSectionId,
      'tId': this.tenantId,
    };
    const _urlGetContentForSection: string = webApi.domain + webApi.url.get_content_section;
    this.commonFunctionService.httpPostRequest(_urlGetContentForSection,param)
    //this.toolsService.getContentForSection(param)
      .then(rescompData => {
        // this.spinner.hide();
        var result = rescompData;
        console.log('Allcontent:', rescompData);
        if (result['type'] === true) {
          if (result['data'][0].length === 0) {
            this.noDataFound = true;
          } else {
            this.parSecId = result['data'][0][0].parentSectionId;
          }

          if (result['data'][1].length === 0) {
            this.noDataFound = true;
            this.skeleton = true;
          } else {
            this.allContent = result['data'][1];
            this.noDataFound = false;
            // for(let i =0; i< this.allContent.length; i++) {
              // this.allContent[i].disable =  this.allContent[i].visible;
              // this.allContent[i].image = this.allContent[i].assetRef;
              // if( this.allContent[i].contentType === 1 || this.allContent[i].contentType === 6) {
              //   this.allContent[i].image = 'assets/images/videoNew.png'
              // } else if(this.allContent[i].contentType === 2) {
              //   this.allContent[i].image = 'assets/images/audioNew.png';
              // } else if(this.allContent[i].contentType === 3) {
              //   this.allContent[i].image = 'assets/images/pdfNew.png';
              // } else if(this.allContent[i].contentType === 7 || this.allContent[i].contentType === 8) {
              //   this.allContent[i].image = 'assets/images/imageNew.png';
              // } else if(this.allContent[i].contentType === 10) {
              //   this.allContent[i].image = 'assets/images/ppt.jpg';
              // } else if(this.allContent[i].contentType === 11) {
              //   this.allContent[i].image = 'assets/images/excel.jpg';
              // } else if(this.allContent[i].contentType === 12) {
              //   this.allContent[i].image = 'assets/images/doc.jpg';
              // } 
              // this.allContent[i].title = this.allContent[i].contentName;
              // this.allContent[i].discrption = this.allContent[i].contentDesc;
              // this.allContent[i].showIcon = false;
           // }
            console.log(' this.allContent', this.allContent);
            // this.presentToast('success', result['data'][1].msg);

          }
          this.skeleton = true;
        } else {

          // this.presentToast('warning', result['data'][1].msg);
        }


      }, error => {
        this.spinner.hide();
        this.skeleton = true;
        this.presentToast('error', '');
      });
  }

  presentToast(type, body) {
    if (type === 'success') {
      this.toastr.success(body, 'Success', {
        closeButton: false,
      });
    } else if (type === 'error') {
      // tslint:disable-next-line: max-line-length
      this.toastr.error('Please contact site administrator and <div (click)="giveFeedback()"> click here <div> to leave your feedback.', 'Error', {
        timeOut: 0,
        closeButton: true,
      });
    } else {
      this.toastr.warning(body, 'Warning', {
        closeButton: false,
      });
    }
  }





  cancelFile() {
    // this.fileUpload.nativeElement.files = [];
    this.assessmentFileData = null;
    // console.log(this.fileUpload.nativeElement.files);
    if (this.fileUpload) {
      if (this.fileUpload.nativeElement) {
        this.fileUpload.nativeElement.value = '';
      }
    }
  }








  addEditContentSectionTool(params) {
    console.log('paramNew', params);
    this.spinner.show();
    const _urlAddEditContent: string = webApi.domain + webApi.url.add_edit_content;
    this.commonFunctionService.httpPostRequest(_urlAddEditContent,params)
    //this.toolsService.addEditContentSection(params)
      .then(rescompData => {

        this.spinner.hide();

        var temp = rescompData;
        var result = temp['data'];
        this.showContent = false;
        ////////////////change catId
        if (params.id == '') {
          if (temp['type'] == true) {
            // this.fetchBadgeCateory();
            console.log('Add Result ', result)
            this.presentToast('success', result['data'].msg);

            this.getsectioncontent();

          } else {
            //this.fetchBadgeCateory();

            this.presentToast('error', '');
          }
        } else {
          console.log('Content Edit Result ', result)
          if (temp['type'] == true) {
            // this.fetchBadgeCateory();

            this.presentToast('success', result['data'].msg);
            // this.getContentForSection();
            this.getsectioncontent();
          } else {
            //this.fetchBadgeCateory();

            this.presentToast('error', '');
          }
        }

      },
        resUserError => {
          this.spinner.hide();
          this.presentToast('error', '');
          this.errorMsg = resUserError;
        });
  }
  back() {
    this.toolsService.fromContent = true;
    this.toolsService.parentSecId = this.parSecId;
    // // this.toolsService.navObj={
    // //   'routeFrom':'content',
    // //   'section':this.toolsService.nav[this.nav.length-1].name

    // // }
    // // this.toolsService.nav=this.nav.pop();
    // this.toolsService.navObj={
    //   'routeFrom':'content',
    //   'title':'',
    //   'titleContent':this.contentData.contentName,
    //   'index':this.toolsService.nav[this.nav.length-1].index,
    //   'section':this.toolsService.nav[this.nav.length-1].name,
    //   'data':this.toolsService.nav[this.nav.length-1].id
    //   }
    // this.toolsService.nav=this.nav
    this.nav = this.nav.slice(0, this.nav.length-1);
    // this.toolsService.parentSecId=this.parSecId
    this.toolsService.navObj={
      'routeFrom':'content',
      'title':'',
      'titleContent':this.contentData.contentName,
      'index':this.nav.length,
      'section':this.toolsService.nav[this.nav.length].name,
      'data':this.toolsService.nav[this.nav.length].id
      }
    this.toolsService.nav=this.nav
  
    this.router.navigate(['../pages/tools']);
  }

  goto(value) {
    this.toolsService.contentdata = value;
    this.router.navigate(['../pages/tools/eneroltools']);
  }
  closeEnableDisableContentModal() {
    this.enableDisableContentModal = false;
  }
  enableDisableContentAction(actionType) {
    console.log(this.enableContentData);
    if (actionType == true) {
      if (this.enableContentData.visible == 1) {
        this.enableContentData.visible = 0;
        var contentData = this.enableContentData;
      this.changeContentStatus(contentData.id,contentData.visible, contentData);
      // this.clickTodisable(ContentData);

      } else {
        this.enableContentData.visible = 1;
        var contentData = this.enableContentData;
        // this.clickTodisable(ContentData);
      this.changeContentStatus(contentData.id,contentData.visible, contentData);
      }
    } else {
      this.closeEnableDisableContentModal();
    }
  }
  clickTodisableContent(ContentData) {
    this.btnName='Yes'
    if (ContentData.visible == 1) {
      this.enableContent = true;
      this.title='Disable Content'
      this.enableContentData = ContentData;
      this.enableDisableContentModal = true;
    } else {
      this.title='Enable Content'
      this.enableContent = false;
      this.enableContentData = ContentData;
      this.enableDisableContentModal = true;
    }
  }
}
