import { Injectable } from '@angular/core';
// import { ErrorDialogService } from '../error-dailog/errordialog.service';
import { ToastrService } from 'ngx-toastr';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import {
    HttpInterceptor,
    HttpRequest,
    HttpResponse,
    HttpHandler,
    HttpEvent,
    HttpErrorResponse,
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { EMPTY, NEVER  } from 'rxjs';
@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {
  // public toastr: ToasterService
  customeHeadersData: any;
  constructor(public toastr: ToastrService) { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token: string = localStorage.getItem('token');

        if (token) {
            request = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + token) });
        }
        this.makeCustomHeaderDataReady();
        if (this.customeHeadersData) {
          request = request.clone({
          headers: request.headers.set('Custom-Header', JSON.stringify(this.customeHeadersData))
          });
      }
        // if (!request.headers.has('Content-Type')) {
        //     request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
        // }

        // request = request.clone({ headers: request.headers.set('Accept', 'application/json') });
        //console.log('request--->>>', request);
        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    // console.log('event--->>>', event);
                    // this.errorDialogService.openDialog(event);
                    // event = event.clone({body: this.modifyBody(event)});
                }
                return event;
            }),
            catchError((err: HttpErrorResponse) => {
                console.log('err',err);
                // let data = {};
                // data = {
                //     reason: err && err.error.reason ? err.error.reason : '',
                //     status: err.status
                // };
                if (err instanceof HttpErrorResponse) {
                    if (err.status === 401) {
                        // redirect to the login route
                        // or show a modal
                        // console.log(err);
                        // var thumbUpload : Toast = {
                        //     type: 'error',
                        //     title: "login",
                        //     body: "Please Login First",
                        //     showCloseButton: true,
                        //     timeout: 5000
                        // };
                        // this.toastr.pop(thumbUpload);

                        this.toastr.warning('Unable to connect!', 'Warning');
                        localStorage.clear();
                        window.open('#/login','_self');
                        console.log("Unauthorised access");
                        return NEVER;
                    }else {
                      return throwError(err);
                    }
                }

            }));
    }

    // private modifyBody(event: any){

    //   if(!event){
    //     return event;
    //   }
    //   const data = {
    //     status: event.status,
    //     type: event.body.type ? event.body.type : false,
    //     data: event.body.data ? event.body.data : event.body,
    //   };
    //   return data;
    // }
    makeCustomHeaderDataReady() {
      if (localStorage.getItem('currentUser')) {
          const currentUserData = JSON.parse(localStorage.getItem('currentUser'));
          if (currentUserData) {
              const fullName = currentUserData.firstname + ' ' + currentUserData.lastname;
              this.customeHeadersData = {
              userId: currentUserData.id,
              userName: currentUserData.username,
              userEmail: currentUserData.email,
              fullName: fullName,
              roleId: currentUserData.roleId,
              roleName: currentUserData.roleName,
              platform: 'admin',
              tenantId: currentUserData.tenantId,
          };
          }
      }
  }
}
