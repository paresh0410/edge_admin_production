import {Injectable, Inject} from '@angular/core';
import {Http, Response, Headers, RequestOptions, Request} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppConfig} from './app.module';

import {webApi} from './service/webApi';
import { AuthenticationService } from './service/authentication.service'

@Injectable()

export class AppService {
	private _url:string = "/api/usermaster/getallDropdowns"
	private _urlAdd:string = "/api/usermaster/adduser"
	private _urlUpdate:string = "/api/usermaster/updateuser"
	private _urlUpload:string = "/api/usermaster/insertuser" // Dev	
	private leave_feedback: boolean = false;

	request: Request;
	data:any;
	features:any=[];
	menus:any=[];


	constructor(@Inject ('APP_CONFIG_TOKEN') private config:AppConfig,private _http: Http,private authenticationService: AuthenticationService){
	    //this.busy = this._http.get('...').toPromise();
	}
	
	userData(user) {
		let url:any = webApi.domain + webApi.url.getUserRoleData;
		let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    	let options = new RequestOptions({ headers: headers });
		return this._http.post(url, user, options ).map((res: Response) => res.json());
	}

	 // getUserdropData(){
  //   let url:any = `${this.config.FINAL_URL}`+this._url;
  //   return this._http.post(url,'')
  //     .map((response:Response) => response.json())
  //     .catch(this._errorHandler);
  // 	}
  		setuserdata(value)
  		{
  			this.data =value;
  		}
  		getuserdata()
  		{
  			return this.data;
  		}

  		setfeatures(value)
  		{
  			this.features=value
  		}
  		getfeatures()
  		{
  			return this.features;
  		}
  		setmenus(value)
  		{
  			this.menus= value
  		}
  		getmenus()
  		{
  			return this.menus;
  		}

	// addUser(user) {
	//    let url:any = `${this.config.FINAL_URL}`+this._urlAdd;
	//    // let url:any = `${this.config.DEV_URL}`+this._urlFilter;
	//    let headers = new Headers({ 'Content-Type': 'application/json' });
	//    let options = new RequestOptions({ headers: headers });
	//    //let body = JSON.stringify(user);
	//    return this._http.post(url, user, options ).map((res: Response) => res.json());
	// }

	// updateUser(user) {
	//    let url:any = `${this.config.FINAL_URL}`+this._urlUpdate;
	//    // let url:any = `${this.config.DEV_URL}`+this._urlFilter;
	//    let headers = new Headers({ 'Content-Type': 'application/json' });
	//    let options = new RequestOptions({ headers: headers });
	//    //let body = JSON.stringify(user);
	//    return this._http.post(url, user, options ).map((res: Response) => res.json());
	// }

	// _errorHandler(error: Response){
	//    console.error(error);
	//    return Observable.throw(error || "Server Error")
	// }

	// public data: any;

}
