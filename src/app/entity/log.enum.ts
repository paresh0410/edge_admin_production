export enum LOGTYPE {
    course = 'course',
    module = 'module',
    activity = 'activity',
}

export enum ACTIVITYLABELS{
      activityName = 'Activity Name',
      activityDuration = 'Activity Duration',
      completionCriteria =  'Completion tracking',
      completionDate =  'Completion Date',
      completionDays =  'Completion Days',
      contentId =  'content',
      dependentActId =  'Dependent Activity',
      description =  'Description',
      startDate =  'Start Date ',
      tags =  'tags',
      craditpointsStr =  'Credit Details',
      byTrainer =  'Attendance By Trainer',
      byLearner =  'Attendance By Learner ',
}