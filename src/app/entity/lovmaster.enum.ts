export enum CourseType {
    ONLINE = 1,
    REGULATORY = 2,
    CALLCOACHING = 3,
    CLASSROOM = 4
}
export enum visible {
    SHOW = 1,
    HIDE = 2
}
export enum CourseLevel {
    BEGINNER = 1,
    INTERMEDIATE = 2,
    ADVANCED = 3
}
export enum LeadForTime {
    TIME_15 = 15,
    TIME_30 = 30,
    TIME_45 = 45,
    TIME_60 = 60,
    TIME_75 = 75,
    TIME_90 = 90
}
export enum POST_FEEDBACK {
    LIKE = 1,
    DISLIKE = 2,
    FAVOURITE = 3
}
export enum QB_TYPE {
    TEXT = 1,
    MEDIA = 2
}
export enum QB_LEVEL {
    BEGINNER = 1,
    INTERMEDIATE = 2,
    ADVANCED = 3
}
export enum QB_FORMAT {
    CHOICE = 1,
    MATCHING = 2,
    SEQUENCE = 3,
    SHORT = 4
}
export enum ENROL_RULE_APPTYPE {
    RETROSPECTIVE = 1,
    PROSPECTIVE = 2
}
export enum ENROL_RULE_APPEVENT {
    IMMEDIATE = 1,
    EOD = 2,
    CUSTOMDATE = 3
}
export enum ACTIVITY_SUPER_TYPE {
    CONTENT = 1,
    ASSESSMENT = 2,
    ASSIGNMENT = 3,
    MANAGEMENT = 4,
    COMMUNICATION = 5,
    EVALUATION = 6
}
export enum LADDER_DISPLAY {
    TOPNPARTICIPANTS = 1,
    NEIGHBOURS_2 = 2,
    NEIGHBOURS_3 = 3
}
