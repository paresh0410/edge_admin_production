import { Injectable } from '@angular/core';

@Injectable()
export class SmartTableService1 {

  data = [{
    username: 'Aditya',
    business: 'Development',
    department: 'Angular',
    manager: 'Bhavesh',
    level: '6'
  }, {
    username: 'Bhavesh',
    business: 'Development',
    department: 'Team Lead',
    manager: 'Ashish',
    level: '9',
  }, {
    username: 'Paresh',
    business: 'Development',
    department: 'Team Lead',
    manager: 'Ashish',
    level: '9',
  }, {
    username: 'Vivek',
    business: 'Development',
    department: 'Angular',
    manager: 'Paresh',
    level: '8',
  }, {
    username:'Ronit',
    business: 'Development',
    department: 'Angular',
    manager: 'Paresh',
    level: '8',
  }, {
    username: 'Dinesh',
    business: 'B.A',
    department: 'Testing',
    manager: 'Ashish',
    level: '8',
  }, {
    username: 'Vaibhav',
    business: 'Development',
    department: 'Angular',
    manager: 'Bhavesh',
    level: '7',
  }, {
    username: 'Niket',
    business: 'Development',
    department: 'Angular',
    manager: 'Paresh',
    level: '9',
  }, {
    username: 'Harsh',
    business: 'Development',
    department: 'Angular',
    manager: 'Ashish',
    level: '8',
  }, {
    username: 'Ashish',
    business: 'Project Manager',
    department: 'SQL',
    manager: 'Unmesh',
    level: '10',
  }];

  data1 =[{
      calls:'call1',
      date:'28/12/2018',
      coach:'Atul',
      manager:'Unmesh',
  },{
      calls:'call2',
      date:'12/09/2018',
      coach:'Nitikesh',
      manager:'Unmesh',
  },{
      calls:'call3',
      date:'01/01/2019',
      coach:'Chetan',
      manager:'Unmesh',
  }]

  getData() {
    return this.data;

  }
  getData1() {
   
    return this.data1;
  }
}
