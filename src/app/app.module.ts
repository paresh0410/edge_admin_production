/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { APP_BASE_HREF } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule , ApplicationRef, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA  } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { CoreModule } from './@core/core.module';
// import {BreadcrumbsModule} from "ng6-breadcrumbs";
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AppService } from './app.service';

import { ThemeModule } from './@theme/theme.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
// import { AuthService } from './shared/services/auth.service';
import { NgxPaginationModule } from 'ngx-pagination';
import { FilterPipeModule  } from 'ngx-filter-pipe';
import { OrderModule  } from 'ngx-order-pipe';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
// import { NgXtruncateModule } from 'ngxtruncate';
import { NgxChartsModule } from '@swimlane/ngx-charts';
// import { AppState, InternalStateType } from './app.service';
// import { GlobalState } from './global.state';
import { PagesModule } from './pages/pages.module';
import { LoginModule} from './pages/login/login.module'

import {TableModule} from 'ngx-easy-table';
// import { Login } from './pages/login/login.component';
import { LoginService} from './pages/login/login.service';
import { ForgotPasswordComponent} from './pages/forgot-password/forgot-password.component';
import { ForgotpassService } from './pages/forgot-password/forgot-password.service';
import { DefaultModal } from './pages/forgot-password/default-model/default-modal.component';
import { PrivacyComponent } from './pages/privacy/privacy/privacy.component';

import { AngularSlickgridModule } from 'angular-slickgrid';
import { ToastrModule } from 'ngx-toastr';

import {ToasterModule, ToasterService} from 'angular2-toaster';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';

// import { MatNativeDateModule } from '@angular/material';
// import { DemoMaterialModule } from './material-module';
import { TagInputModule } from 'ngx-chips';

import { webAPIService } from './service/webAPIService';
import { LogServices } from './service/log.service';
import { AuthenticationService } from './service/authentication.service';
import { ExportAsModule } from 'ngx-export-as';

import { NgxSpinnerModule } from 'ngx-spinner';
import { HttpConfigInterceptor } from './interceptor/interceptor';


import { ChartsModule } from 'ng2-charts';
import { NgxSummernoteModule } from 'ngx-summernote';
import { JoditAngularModule } from 'jodit-angular';
import { webApi } from './service/webApi';

import { FlexmonsterPivotModule } from 'ng-flexmonster';
import { SafePipe } from './safe-pipe-pipe.pipe';
import { SafePipePipe } from './safe-pipe.pipe';
import { DeviceDetectorModule } from 'ngx-device-detector';
import { LogoutComponent } from './pages/logout/logout.component';
//import { ReactionHomeComponent } from './reaction-home/reaction-home.component';
import { ComponentModule } from './component/component.module';
//import { PlanComponent } from './src/app/pages/plan/plan.component';
//import { OpenApiComponent } from './src/app/pages/plan/open-api/open-api.component';
// import { CategoryComponentComponent } from './utils/filter-dashboard-component/filter-component-sidebar/category-component/category-component.component';
import { DomainMapComponent } from './pages/domain-map/domain-map.component';
// import { ContextMenuModule } from 'ngx-contextmenu';
// import { FolderUploadComponent } from './folder-upload/folder-upload.component';
import { PricePlanComponent } from './pages/price-plan/price-plan.component';
import { SignupComponent } from './pages/signup/signup.component';

export interface AppConfig {
  BASE_URL:string,
  UPLOAD_URL:string,
  DATA_URL:string,
  LIVE_URL:string,
  LIVE_URL_HTTPS:string,
  TEST_URL:string,
  DEV_WEB_URL:string,
  TEST_WEB_HTTPS:string,
  DEV_URL:string,
  FINAL_URL:string,
  userData :any,
  PAGES_MENU :any,
  uid:any,
  productid:any,
  producttype:any,
  policyid:any,
  policyData:any,
  networkData:any
}

export const APPCONFIG:AppConfig = {
  //BASE_URL: 'http://52.33.226.70:9804',
  //UPLOAD_URL: 'http://52.33.226.70:9805',
  BASE_URL: 'http://52.33.226.70:9801',
  UPLOAD_URL: 'http://52.33.226.70:9802',
  DATA_URL: 'http://52.33.226.70:9850', // live
  LIVE_URL: 'http://13.232.16.178:9850', // live Edge
  LIVE_URL_HTTPS: 'https://www.edge-lms.co.in:9850', // live
  TEST_URL: 'http://35.154.55.154:9850', // Test
  DEV_WEB_URL: 'http://35.154.55.154:9855', // DevWebServices
  TEST_WEB_HTTPS: 'https://finatics.in:9850', // TestWebHTTPS
  DEV_URL: 'http://52.33.226.70:9852', // Dev
  FINAL_URL: webApi.domain,
  // FINAL_URL: 'https://devwebapi.edgelearning.co.in',
  // FINAL_URL: 'http://13.233.59.0:9845', //Test
  //FINAL_URL: 'http://13.233.171.126:9840',  //Test Bhavesh
   //FINAL_URL: 'https://webapi.edgelearning.co.in',
  // FINAL_URL: 'https://admin.edgelearning.co.in',
  // FINAL_URL: 'https://finatics.in:9850',
  //FINAL_URL:'http://localhost:9845',
  userData : {},
  PAGES_MENU : [],
  uid:'',
  productid: '',
  producttype: '',
  policyid: '',
  policyData:[],
  networkData:[]
};

// Application wide providers\

// const APP_PROVIDERS = [
// AppState,
// GlobalState
// ];

export type StoreType = {
// state: InternalStateType,
restoreInputValues: () => void,
disposeOldHosts: () => void
};

@NgModule({
  declarations: [AppComponent, PrivacyComponent, DomainMapComponent,PricePlanComponent,SignupComponent,
    LogoutComponent, ForgotPasswordComponent, DefaultModal, SafePipePipe,
    // CategoryComponentComponent,
    SafePipe,
    //ReactionHomeComponent,
    //PlanComponent,
   // OpenApiComponent,
    // InfiniteScrollComponent,
    // FolderUploadComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    NgxSpinnerModule,
    ChartsModule,
    NgxSummernoteModule,
    JoditAngularModule,
    // BreadcrumbsModule,
    ToastrModule.forRoot({
      timeOut: 4000,
      preventDuplicates: true,
      enableHtml: true,
    }),
    // ToastrModule.forRoot(),
    ToasterModule.forRoot(),
    ComponentModule.forRoot(),
    NgxPaginationModule,
    // ContextMenuModule.forRoot(),

    FilterPipeModule,
    OrderModule,
    NgxMyDatePickerModule,
    // // NgXtruncateModule,
    // NgxChartsModule,
    // RouterModule,
    AngularMultiSelectModule,
    FormsModule,
    // ReactiveFormsModule,
    // PagesModule,
    HttpModule,
    // LoginModule,

    TableModule,
    // DemoMaterialModule,
    NgbModule.forRoot(),
    ThemeModule.forRoot(),
    CoreModule.forRoot(),
    AngularSlickgridModule.forRoot(),
    TagInputModule,
    ExportAsModule,
    LoginModule,
    FlexmonsterPivotModule,
    DeviceDetectorModule.forRoot()
  ],
  entryComponents: [
    DefaultModal
  ],
  bootstrap: [AppComponent],
  providers: [
    // APP_PROVIDERS,
    // AuthService,
     { provide: 'APP_CONFIG_TOKEN', useValue:APPCONFIG},
    { provide : HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi:true},
    { provide: APP_BASE_HREF, useValue: '/' },
    LoginService,
    ForgotpassService,
    AppService,
    webAPIService,
    LogServices,
    AuthenticationService,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA,
  ]
})
export class AppModule {
  // constructor(public appState: AppState) {

  // }
}
