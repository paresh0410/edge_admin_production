export class noData {
    constructor(public imageSrc?: string,
                public title?:String,
                public margin?:String,
                public titleShow?:boolean,
                public descShow?:boolean,
                public btnShow?:boolean,
                public linkShow?:boolean,
                public link?:String,
                public linkTxt?:String,
                public btnText?:String,
                public btnLink?:string,
                public desc?:String,

        ){

        }
}