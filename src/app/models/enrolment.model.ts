export class EnrolmentConfig {
  constructor(
    public manulEnrolmentData: ManulEnrolmentConfig,
    public ruleBasedEnrolmentData: RuleEnrolmentConfig,
    public selfEnrolmentData: SelfEnrolmentConfig,
    public regulatoryEnrolmentData: RegulatoryEnrolmentConfig,
    public priceBasedEnrolmentData: PriceEnrolmentConfig,
  ) {}
}
export class ManulEnrolmentConfig {
  constructor(
    public show: boolean,
    public data: any[],
    public labels: any[],
    public userList: any[],
    public tabTitle: string,
    public identifer: string,
    public helpContent: any[],
    public selectedUsers: any[]
  ) {}
}
export class RuleEnrolmentConfig {
  constructor(
    public show: boolean,
    public data: any[],
    public labels: any[],
    public tabTitle: string,
    public identifer: string,
    public helpContent: any[],
    public ruleData: any,
    public ruleApplicType: any[],
    public prospectivType: any[],
    public profileFields: any[],
    public controlList: any[],
    public profileFieldSelected: boolean,
    public rowsEnrolRule: any[],
    public enrolUserPopupTableLabel: any[],
    public showAddRuleModal: boolean,
    public showEnroleduserPopup: boolean,
    public enrolDateOption?: boolean,
    public rulesOption?: any[],
  ) {
    this.enrolDateOption = false;
  }
}
export class SelfEnrolmentConfig {
  constructor(
    public show: boolean,
    public data: any[],
    public labels: any[],
    public tabTitle: string,
    public identifer: string,
    public helpContent: any[],
    public controlList: any[],
    public showAddSelfModal: boolean,
    public selfFeildType: any[],
    public selfFieldsData: any,
    public selfType: any[],
    public isFetchingSettings: boolean,
    public profileFieldsSelf: any[]
  ) {}
}
export class RegulatoryEnrolmentConfig {
  constructor(
    public show: boolean,
    public data: any[],
    public labels: any[],
    public tabTitle: string,
    public identifer: string,
    public helpContent: any[],
    public controlList: any[],
    public profileFieldsRegFilter: any[],
    public regFilterProfileFieldSelected: boolean,
    public showAddRegulatoryFilterModal: boolean,
    public regularData: any,
  ) {}
}

export class PriceEnrolmentConfig {
  constructor(
    public show: boolean,
    public data: any[],
    public labels: any[],
    public tabTitle: string,
    public identifer: string,
    public helpContent: any[],
    public courseId: string,
    public currencyTypeDropDown: any[],
    public discountListDropdownList: any[],
    public addEditPriceForm: any,
    public showSidebar: boolean,
  ) {}
}
