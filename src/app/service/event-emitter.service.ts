import { Injectable, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';

@Injectable({
  providedIn: 'root'
})
export class EventEmitterService {
  invokeComponentFunction = new EventEmitter();
  subsVar: Subscription;
  varib: any;
  constructor() { }

  onComponentClick(name: string) {
    this.invokeComponentFunction.emit(name);
  }
}
