import { TestBed } from '@angular/core/testing';

import { JsonToXlsxService } from './json-to-xlsx.service';

describe('JsonToXlsxService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: JsonToXlsxService = TestBed.get(JsonToXlsxService);
    expect(service).toBeTruthy();
  });
});
