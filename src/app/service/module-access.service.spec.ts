import { TestBed } from '@angular/core/testing';

import { ModuleAccessService } from './module-access.service';

describe('ModuleAccessService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ModuleAccessService = TestBed.get(ModuleAccessService);
    expect(service).toBeTruthy();
  });
});
