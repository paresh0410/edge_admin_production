import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import {webApi} from './webApi';

@Injectable()
export class AuthenticationService {
    public token: string;
    public userLoginUrl = webApi.domain + webApi.url.login;

    constructor(private http: Http) {
        // set token if saved in local storage
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.token;
    }

    login(loginData): Observable<boolean> {
        return this.http.post(this.userLoginUrl, loginData)
            .map((response: Response) =>
            {
                // login successful if there's a jwt token in the response
                let token = response.json() && response.json().token;
                if (token) {
                    // set token property
                    this.token = token;

                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    // localStorage.setItem('currentUser', loginData);
                    localStorage.setItem('currentUser', JSON.stringify({ username: loginData.username, token: token }));
                    // return true to indicate successful login
                    return response.json();
                } else {
                    // return false to indicate failed login
                    return response.json();
                }
            }
            );
    }

    logout(): void {
        // clear token remove user from local storage to log user out
        this.token = null;
        localStorage.removeItem('currentUser');
    }
    getToken (): string {
      let token = null;
      try {
        if (localStorage.getItem('token')) {
          token = localStorage.getItem('token');
        }
      } catch(e) {
        console.error(e);
        return token;
      }
      return token;
    }
}
