import {Injectable} from "@angular/core";
import { HttpClient } from "@angular/common/http";
import {Http,Response,RequestOptions,Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {webApi} from './webApi';
import { AuthenticationService } from './authentication.service';

@Injectable() 
export class webAPIService {
    url:any;
    apiUrls:any = webApi;

    constructor(public http: Http, public http1 : HttpClient, private authenticationService: AuthenticationService) {
        this.url = webApi.url;
    }
    
    getService(url,param){
        // add authorization header with jwt token
        let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
        // let headers = new Headers({ 'Authorization': 'Bearer ' + 'myToken' });
        let options = new RequestOptions({ headers: headers });
        return new Promise(resolve => {
            this.http.post(url, param, options)
            .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve('err');
            });
        });
    }


    // getService(url,param){
    //     // add authorization header with jwt token
    //   //  let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    //     // let headers = new Headers({ 'Authorization': 'Bearer ' + 'myToken' });
    //    // let options = new RequestOptions({ headers: headers });
    //     // return new Promise(resolve => {
    //     //     this.http1.post(url, param)
    //     //     //.map(res => res.json())
    //     //     .subscribe(data => {
    //     //         resolve(data);
    //     //     },
    //     //     err => {
    //     //         resolve('err');
    //     //     });
    //     // });
    // }


    // getServiceData(url,param){
    //     let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    //     let options = new RequestOptions({ headers: headers });
    //     return new Promise(resolve => {
    //         this.http.get(url, param)
    //         .map(res => res.json())
    //         .subscribe(data => {
    //             resolve(data);
    //         },
    //         err => {
    //             resolve('err');
    //         });
    //     });
    // }
}