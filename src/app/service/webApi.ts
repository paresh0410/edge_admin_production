export let webApi = {
  // domain: 'https://devwebapi.edgelearning.co.in',
  // domain: 'https://migwebapi.edgelearning.co.in',
  // domain: "http://localhost:9845", // localhost
  // domain: 'https://webapi.edgelearning.co.in',            // EDGE TEST
  // domain: 'http://192.168.0.113:9845',            // Test New
  // domain: 'http://13.233.171.126:9860',            // Test bhavesh
  // domainOld: 'http://35.154.55.154:9855',             // DEV Old
  // domain: 'https://uatwebserver.edgelearning.co.in',  // SSL Configured New UA
  domain: "https://prodwebserver.edgelearning.co.in",   // prod,
  // domain: 'http://192.168.0.124:9845',
  // domain: 'http://13.233.59.0:9945',  // TempServer
  // domain:'https://bajajgeeksapi.edgelearning.co.in',
  // domain:'https://saaswebserver.edgelearning.co.in',
  // domain: 'https://testsaaswebserver.edgelearning.co.in',
  // domain: 'https://testsaaswebserver.edge-lms.co.in',
  url: {
    login: "/api/web/userLogin",
    getUserRoleData: "/api/web/userLogin_data",
    contentSearch: "/api/web/content_search",
    fileUpload: "/api/web/content_save",
    getCategories: "/api/web/getallcat",
    getCategoriesNew: "/api/web/getallcat_demo",
    getCourseCat: "/api/web/get_all_course_catwise",
    addEditCategory: "/api/web/savecat",
    disableCategory: "/api/web/disable_cat",
    checkCategory: "/api/web/check_catcode",
    getVisibiltyDropdown: "/api/web/visible_list",
    getCourses: "/api/web/getallcourse",
    getCourses_demo: "/api/web/getallcourse_demo",
    get_all_course_optmz: "/api/web/get_all_course_optmz",
    get_all_course_credit_tags_optmz:
      "/api/web/get_all_course_credit_tags_optmz",
    getCourseDropdown: "/api/web/getallcoursedropdown",
    addEditCourse: "/api/web/save_course",
    disableCourse: "/api/web/disable_course",
    visibleCheck: "/api/web/check_before_disable_course",
    duplicateCourse: "/api/web/duplicate_course",
    checkCourse: "/api/web/check_course",
    getCourseModules: "/api/web/getall_module",
    getCourseModulesActivity: "/api/web/getall_activity_data",
    addEditModule: "/api/web/save_module",
    removeTrainerEnrolment: "/api/web/remove_trainer_enrolment",
    deleteModule: "/api/web/delete_module",
    deleteActivity: "/api/web/delete_activity",
    getModuleActivityDrop: "/api/web/getmodule_activity_dropdowns",
    addEditActivity: "/api/web/save_activity_data",
    deleteLanguageMapContent:"/api/web/delete_language_map_content",
    getAllEnrolUser: "/api/web/getallcourseenrole",
    fetchBadges: "/api/web/fetch_badges",
    insertupdatebadges: "/api/web/insert_update_badge",
    changebadgestatus: "/api/web/change_badge_status",
    fetchLadders: "/api/web/fetch_ladders",
    changeladderstatus: "/api/web/change_ladders_status",
    insertupdateladders: "/api/web/insert_update_ladders",
    fetchddcoursedisplay: "/api/web/dropdown_course_display",
    insertupdateladdersteps: "/api/web/insert_update_ladders_steps",
    dropdownnotifytemplate: "/api/web/dropdown_notify_template",
    dropdownnotifymethod: "/api/web/fetch_not_methods",
    // getnottemplate:'/api/web/get_nottemplate'     //dont remove this line
    insertupdateladdernotification:
      "/api/web/insert_update_ladders_notifications",
    fetchLaddersSteps: "/api/web/fetch_ladders_steps",
    fetchLaddersNotifications: "/api/web/fetch_ladders_notifications",
    insertrewardtocourse: "/api/web/insertrewardtocourse",
    insertladdertocourse: "/api/web/insertladdertocourse",
    getnotevent: "/api/web/get_notevent",
    getAllUNEnrolUser: "/api/web/getallcourseenrolesearch",
    getAllUNEnrolUserPreon: "/api/web/getallpreoncourseenrolesearch",
    disableuser: "/api/web/enabledisableenrole",
    disableEnrol: "/api/web/enabledisableenrole_prop",
    addenrolUser: "/api/web/addrole",
    getallrule: "/api/web/getallcourseenrolerules",
    disablerule: "/api/web/enabledisablerule",
    addrule: "/api/web/save_rules",
    addrulenew: "/app/web/save_rules_enrolment_new",
    addrulenewother: "/app/web/save_rules_enrolment_new_forother",
    addEditquestionCategory: "/api/web/saveqbcat",
    addeditqbcategoryquestion: "/api/web/saveqbcatquestion",
    addeditqbquestionmultiple: "/api/web/saveqbquestionmultiple",
    addeditqbquestionsequence: "/api/web/saveqbquestionsequence",
    multifileUpload: "/api/web/multiplecontentsave",
    addeditqbquestionmatching: "/api/web/saveqbquestionmatching",
    addeditqbquestionshort: "/api/web/saveqbquestionshort",
    getallregulatorylist: "/api/web/getallenroleregulatory",
    disableregulatory: "/api/web/enabledisableregulatory",
    addregulatory: "/api/web/add_regulatory",
    addregulatorynew: "/app/web/save_regulatory_enrolment_new",
    addregulatoryfilter: "/api/web/add_regulatory_filter",
    getcourseselfsetting: "/api/web/getcourseselfsetting",
    getcourseprofile: "/api/web/getcourseprofile",
    addselfsetting: "/api/web/addselfsetting",
    addselfsetting_new: "/api/web/addselfsetting_new",
    profilefield: "/api/web/ProfileFields",
    getcoursenotification: "/api/web/get_course_notification",
    getallcertificates: "/api/web/fetch_certificates",
    changecertificatestatus: "/api/web/change_certificate_status",
    addeditcertificate: "/api/web/add_edit_certificate",
    getruledropdown: "/api/web/ruledropdown",
    getcourserewards: "/api/web/get_course_rewards",
    getcoursegamification: "/api/web/get_course_gamification",
    addeditquiz: "/api/web/savequizsetting",
    profilemenudropdown: "/api/web/profilemenu",
    fetchnotificationtemplateevents: "/api/web/fetch_not_events",

    //batch bulk
    bulkUpload:"/api/admin/bulkupload/courses",
    bulkUploadSession:"/api/admin/bulkupload/sessions",
    getBatchBulkList:"/api/admin/bulkupload/courses/course_count_userwise",
    getSessionBulkList:"/api/admin/bulkupload/courses/session_count_userwise",
    getSessionUploadedData:"/api/admin/bulkupload/courses/session_uploaded_data",
    getUploadedData:"/api/admin/bulkupload/courses_uploaded_data",
    // fetchnotifymodes:'/api/web/fetch_notify_modes',
    fetchnotificationtemplate: "/api/web/fetch_notification_template",
    fetchbadgecategories: "/api/web/fetch_badge_categories",
    insertupdatebadgecategory: "/api/web/insert_update_badge_category",
    changebadgecategorystatus: "/api/web/change_badge_category_status",
    addeditnottemplate: "/api/web/add_edit_not_template",
    fetchnottempbyid: "/api/web/fetch_not_temp_by_id",
    fetchallsurvey: "/api/web/fetch_survey",
    addeditsurveysetting: "/api/web/add_edit_survey_setting",
    getroledropdownforsurvey: "/api/web/get_role_dropdown_setting",
    changesurveystatus: "/api/web/change_survey_status",
    getallsurveynotification: "/api/web/get_all_survey_notification",
    getsurveyquestiontype: "/api/web/get_survey_question_type",
    addeditsurveyquestion: "/api/web/add_edit_survey_question",
    getexistingsurveyquestions: "/api/web/get_existing_survey_questions",
    changesurveyquestionstatus: "/api/web/change_servey_question_status",
    getsurveyquestiondetails: "/api/web/get_survey_question_details_by_id",
    changesurveyquestionorder: "/api/web/change_survey_question_order",
    copysurvey: "/api/web/copy_survey",
    // addeditquiz:'/api/web/savequizsetting',
    deletequizquestion: "/api/web/delete_quiz_question",
    deletequizsection: "/api/web/delete_quiz_section",
    addeditsection: "/api/web/savequizsection",
    addquizquestion: "/api/web/savequizquestion",
    updatequizquestion: "/api/web/updatequizquestion",
    getquizcategory: "/api/web/getall_qbcat_demo",
    uploadBulkQuizQuestion: "/api/web/bulk_upload_questions",
    uploadBulkQuizQuestionType: "/api/web/get_option_type_dropdown",
    getquiz: "/api/web/getall_quiz_demo",
    // feedback
    feedbackUpadateSortOrder: "/api/web/update_feedback_sort_order",
    feedbackgetqueDataForEdit: "/api/web/fb_que_details_by_id",
    addupdatefeedback: "/api/web/savefeedback",
    getfeedbackquestiontype: "/api/web/get_feedback_question_type",
    addeditfeedbackquestion: "/api/web/add_edit_feedback_question",
    getexistingfeedbackquestions: "/api/web/get_existing_feedback_questions",
    changefeedbackquestionstatus: "/api/web/change_feedback_question_status",
    getfeedbackquestiondetails: "/api/web/get_feedback_question_details_by_id",
    changefeedbackquestionorder: "/api/web/change_feedback_question_order",
    deleteFeedbackQuestion: "/api/web/delete_feedback_question",
    disablequestionbank: "/api/web/disable_question_category",
    disablequestion: "/api/web/disable_category_question",
    disablequiz: "/api/web/disable_quiz",
    disablefeedback: "/api/web/disable_feedback",
    getallpoll: "/api/web/fetch_poll",
    addeditpoll: "/api/web/add_edit_poll",
    getfeebackusertypes: "/api/web/get_feedback_user_types",
    getpollusertypes: "/api/web/get_poll_user_types",
    changepollstatus: "/api/web/change_poll_status",
    getpollnotification: "/api/web/get_poll_notification",
    addeditpollquestion: "/api/web/add_edit_poll_question",
    getpollexistingquestions: "/api/web/get_poll_existing_question",
    getpollquestiondetails: "/api/web/get_poll_question_details_by_id",
    getfeddback: "/api/web/getall_feedback",
    /********** Learning pathway *********/
    add_edit_learning_pathway: "/api/web/cb/add_edit_learning_pathway",
    getalllearningPathwayNotificaion: "/api/web/cb/getall_notification",
    showHideVisibility: "/api/web/hide_show_notification_rule",
    /*************** CHAT ***************/
    getChatList: "/api/web/getChatUserlist",
    getActiveChatList: "/api/web/getActiveChatList",
    checkChatUserName: "/api/web/chatUsernameCheck",
    inviteUserForChat: "/api/web/inviteUserForChat",
    inviteChatResponse: "/api/web/inviteResponse",
    getChatMessage: "/api/web/getMessages",
    getChatUserList: "/api/web/getChatUserlist",
    getChatLogin: "/api/web/chatLogin",
    insertDirectUserFriendMap: "/api/web/insertDirectUserFriendMap",
    // End Chat Module

    /******Call Coaching Api*******/
    getallemployeecc: "/api/web/get_all_employeescc",
    addeditnominatedemployee: "/api/web/add_edit_nominated_employee",
    getallnominatedemployees: "/api/web/get_all_nominated_employees",
    changeparticipantstatus: "/api/web/change_participant_status",
    addeditcall: "/api/web/add_edit_call",
    getcallsbyempid: "/api/web/get_calls_by_empid",
    deletecallbyid: "/api/web/delete_call_by_id",
    getallcoursescc: "/api/web/get_all_courses_cc",
    enrolcourseforcccontent: "/api/web/enrol_course_for_cc_content",
    getallenrolcoursemodule: "/api/web/get_all_enrol_course_modules",
    getfeedbacktemplate: "/api/web/get_feedback_template",
    removeenrolcoursecc: "/api/web/remove_cc_course",
    addeditfeedbackdetails: "/api/web/add_edit_feedback_details",
    getModuleActivity: "/api/web/moduleActivity",
    saveActivityCompletion: "/api/web/activitycompletion",
    getAssessmentTemplate: "/api/web/get_assessment_template",
    addeditassessmentdetails: "/api/web/add_edit_assessment_details",
    getTrainerByPartner: "/api/web/get_trainers_by_partner",
    getObservationTemplate: "/api/web/get_observation_template",
    deleteObservationtemplate: "/api/web/delete_observation_template",
    getdropdownbusiness: "/api/web/get_dropdown_business",
    getasseessmenttemplate: "/api/web/get_assessment_template_master",
    getDropdownObservation: "/api/web/get_dropdown_observation",
    insertDropdownObservation: "/api/web/insert_observation_template",
    insertassessmentemplate: "/api/web/insert_assessment_template_master",
    deletassessmenttemplate: "/api/web/delete_assessment_template_master",
    getFeedbackTemplate: "/api/web/get_feedback_template_master",
    deleteFeedbackTemplate: "/api/web/delete_feedback_template_master",
    insertFeedbackTemplate: "/api/web/insert_feedback_template_master",
    dropdownFeedback: "/api/web/get_feed_template_dropdown",
    getspectr: "/api/web/getspecctr",
    addeditspectr: "/api/web/addeditspectr",
    getcompetency: "/api/web/getcompetencies",
    addeditcompetencies: "/api/web/addeditcompetencies",
    getevalutionarea: "/api/web/getevalutionarea",
    addeditevalutionarea: "/api/web/addeditevalutionares",
    getAllPartners: "/api/web/get_partner_dropdown",
    getobservation: "/api/web/get_manager_observation",
    getChatListCC: "/api/web/get_chat_list",
    getAllEmpListUP: "/api/web/get_all_employeescc",
    getAllpartnersListUP: "/api/web/get_partner_dropdown",
    insertBulkUploadParticipent: "/api/web/bulk_upload_participent",
    getCallNote: "/api/web/get_call_note",
    enabledisble: "/api/web/updateTemplateStatus",
    /**************CC End****************/
    /****************DAM*****************/
    getAllAuthorListForDam: "/api/web/dam/get_all_author_list_for_dam_asset",
    getAllAssetDropDown: "/api/web/get_asset_dropdown",
    addEditAssetCategory: "/api/web/add_edit_asset_category",
    changeAssetCategoryStatus: "/api/web/change_asset_category_status",
    getAllAsset: "/api/web/get_all_asset",
    addEditAsset: "/api/web/add_edit_asset",
    changeAssetStatus: "/api/web/change_asset_status",
    getAssetDetails:"/api/web/get_asset_details",

    // Temp asset
    getAllAssetCategory: "/api/web/get_all_asset_category",
    getAllAssetCategories: "/api/web/get_all_asset_category_asset",
    getAllAssetCatWise: "/api/web/get_asset_category_catwise",
    getAllAssetVersions: "/api/web/get_asset_versions",
    deleteAssetVersion: "/api/web/delete_asset_versions",
    getShareAsset: "/api/web/get_approved_assets_catwise",

    // Tree Asset
    getTree:"/api/web/get_tree",
    deleteFolder:"/api/web/trash_restore_asset",
    downloadFile:"/api/web/dwonload_asset_store_lrs",
    downloadFolder:"/api/damFileUpload/downloadZipFolder",
    deleteForever:'/api/damFileUpload/deleteFileOrFolder',
    getDAMStorageDetails:'/api/web/get_dam_size',

    // getAllEmployeeDAM:'/api/web/get_all_employees',
    approveRejectAsset: "/api/web/approve_reject_asset",
    checkApproveStatus:"/api/web/check_asset_course_mapping",
    getPendingAsset: "/api/web/get_pending_asset",
    getAllEmployeeDAM: "/api/web/get_all_users",
    getAssetShareEmployee: "/api/web/share_asset_employee",
    insertAssetShare: "/api/web/insert_asset_share",
    getReviewFrequency: "/api/web/get_review_frequency",
    addEditAssetReviewPolicy: "/api/web/add_edit_asset_review_policy",
    getApprovedAsset: "/api/web/get_approved_asset",
    getAssetReviewPolicyById: "/api/web/get_asset_review_policy_by_id",
    insertAssetVersion: "/api/web/update_asset_version",
    getAllEdgeEmployee: "/api/web/get_all_edge_employees",
    bulkUploadFile: "/api/web/bulk_upload_file",
    insertAssetBulkUpload: "/api/web/dam_insert_asset_bulk",
    insertAssetBulkUploadNew: "/api/damFileUpload/upload",
    insetValidDataTemp: "/api/web/insert_valid_data_temp",
    getAllTags: "/api/web/get_all_tags",
    handshakeDamUserVerify: "/api/web/handshake_dam_user_verify",
    handshakeTokenUpdate: "/api/web/handshake_token_data_update",
    handshakeDamAsset: "/api/web/handshake_dam_asset",
    // getAllAuthorListForDam : '/api/web/dam/get_all_author_list_for_dam_asset',
    moveDAMAsset: "/api/web/move_asset",
    move_asset_tree: "/api/web/get_asset_move_tree",
    searchDAMContent:"/api/damSearch/search",

    /*************DAM END****************/
    /**************Course Assesment Module****************/
    searchQuiz: "/app/web/serchcoursequiz",
    searchFeedback: "/app/web/serchcoursefeedback",
    /**************Blended Course ---Programs****************/
    getprograms: "/api/web/getallprogram",
    addEditPrograms: "/api/web/saveprogram",
    disableProgram: "/api/web/disableprogram",
    getallcoursesclassroom: "/api/web/get_all_courses_classroom",
    enrolcourseforclassroom: "/api/web/enrol_course_for_classroom_content",
    getallenrolcoursemoduleclassroom:
      "/api/web/get_all_enrol_course_modules_classroom",
    removeenrolcourseclassroom: "/api/web/remove_classroom_course",
    getAllCalls: "/api/web/get_all_calls",
    gettrainerByStandard: "/api/web/get_trainer_by_standard",
    insertCall: "/api/web/insert_call",
    show_hide_calls: "/api/web/change_call_status",
    /**************Blended Course ---Batch****************/
    getBatchDropdown: "/api/web/getallbatchdropdown",
    getAllCreator: "/api/web/getallempsearch",
    getBatches: "/api/web/getallbatch",
    addEditBatch: "/api/web/save_batch",
    disableBatch: "/api/web/disable_batch",
    duplicateBatch: "/api/web/duplicate_batch",
    checkBatch: "/api/web/check_batch",
    getModuleActivityDropBatch: "/api/web/getmodule_activity_dropdowns_batch",
    addEditModuleBatch: "/api/web/save_module_batch",
    getBatchModules: "/api/web/getall_module_batch",
    getBatchModulesActivity: "/api/web/getall_activity_data_batch",
    addEditActivityBatch: "/api/web/save_activity_data_batch",
    roleDrop: "/api/web/getallfeedbackdropdown",
    deleteModuleBatch: "/api/web/delete_module_batch",
    deleteActivityBatch: "/api/web/delete_activity_batch",
    get_batch_cost: "/api/web/get_batch_cost",
    save_batch_cost: "/api/web/save_batch_cost",
    get_workflow_nominees: "/api/web/get_workflow_nominees",
    get_Trainers:"/api/web/get_modules_trainers",
    /******************selfdropdown***************/
    selfdrop: "/api/web/self_dropdown",
    /******************toolsSection***************/
    get_tools_section: "/api/web/get_all_tools_section",
    add_edit_tools_section: "/api/web/add_edit_tool_section",
    get_dropdown_activity: "/api/web/get_dropdown_acivity",
    get_content_section: "/api/web/get_section_content",
    add_edit_content: "/api/web/add_edit_section_content",
    get_content_dropdown: "/api/web/get_content_dropdown_types",

    /*************BULK ENROL****************/
    tempSaveBulkManEnrol: "/api/web/temp_save_manual_enrole_bulk",
    tempSaveBulkManEnrolPreon: "/api/web/temp_save_manual_enrole_bulk_preon",
    saveBulkManEnrol: "/api/web/save_manual_enrole_bulk",
    area_bulk_enrol: "/api/web/area_bulk_enrol",
    /*************BULK ENROL CB****************/
    tempSaveBulkManEnrolCB: "/api/web/cb/Course_bundle_enrole_upload_bulk",
    saveBulkManEnrolCB: "/api/web/cb/Course_bundle_enrole_final_Upload_bulk",
    removeworkflowcourse: "/api/web/cb/removeCoWorkflStru",
    /*******HIDE unhide courses from workflow */
    workflow_courses_enable_disable:
      "/api/web/cb/workflow_courses_enable_disable",
    /*delete CB */
    deleteCB: "/api/web/cb/delete_course_bundle",
    workflow_enable_disable_enrol: "/api/web/cb/enable_disable_Course_bundle",
    /*********************************  TTT Nomination  *********************************** */
    getnomination: "/api/web/get_all_nomination_instance",
    disableNominationInstances: "/api/web/disable_program",
    getallfcodeofconduct: "/api/web/ALL_instance_codeofconduct",
    addeditcodeofconduct: "/api/web/ADD_EDIT_Instance_codeofconduct",
    addEditInstance: "/api/web/add_edit_programs",
    nominationInstanceDropdown: "/api/web/Nomination_instance_dropdown",
    getAllTttNominnes: "/api/web/get_all_ttt_nominees",
    /*nomi-enrol-course */
    getallcoursesinstance: "/api/web/get_all_courses_nomination_instance",
    enrolcourseforinstance:
      "/api/web/enrol_course_for_nomination_instance_content",
    getallenrolcoursemoduleinstance:
      "/api/web/get_all_enrol_course_modules_nomination_instance",
    removeenrolcourseinstance: "/api/web/remove_nomination_instance_course",
    workflow_evaluation: "/api/web/TTT_insert_workflow_evaluation",
    getistanceactivitydropdown: "/api/web/Nomination_instance_steps_dropdown",
    insertWorkflowSteps: "/api/web/ADD_EDIT_Instance_steps",
    getallworkflowsteps: "/api/web/ALL_instance_steps",
    getNomineeDetails: "/api/web/get_ttt_nominee_details",
    getBhapprovalStatus: "/api/web/get_ttt_bhapproval_status",
    updateBhApprovalStatus: "/api/web/update_bhapproval_status",
    getAllWorkflowSlots: "/api/web/get_all_workflow_slots",
    insertWebinarEvent: "/api/web/insert_ttt_webinar_event",
    getExistingWebinarEventByEmpId: "/api/web/ttt_get_existing_webinar_event",
    getEvaluationCallDetail: "/api/web/get_ttt_evaluation_call_details",
    getKnowledgeTestDetails: "/api/web/get_ttt_get_knowledge_test_details",
    getVivaDetails: "/api/web/get_ttt_viva_details",
    //getEvaluationApproval: ' /api/web/ttt_update_evaluation_approval',
    updateEvaluationApproval: "/api/web/ttt_update_evaluation_approval",
    updateFgAssessmentDetails: "/api/web/ttt_fg_assessment_details",
    batchDetailStepTen: "/api/web/ttt_batch_selection_details",
    mockStepEleven: "/api/web/get_ttt_get_knowledge_test_details",
    assesmentStepTwelve: "/api/web/get_ttt_get_knowledge_test_details",
    getCertificateLink: "/api/web/certificate_content?",
    getstepwisedata: "/api/web/ttt_sted_wise_data",
    getstepwiseuserdata: "/api/web/ttt_sted_wise_UserData",
    stepwisebhapproval: "/api/web/ttt_update_bhapproval_stepwise",
    stpwiseevalution: "/api/web/ttt_update_evalutionapproval_stepwise",
    stepwisewebinar: "/api/web/ttt_insert_webinar_event_stepwise",
    getCertificate: "/api/web/certificate",
    getCertificateContent: "/api/web/certificate_content",
    TTTselfenrol: "/api/web/ttt_self_enrol",
    TTTgetWebinarDownloadList: "/api/web/ttt_get_webinar_recordings",
    //: Fetch All Location
    getAllLocation: "/api/admin/fetch_all_location",
    updateLocation: "/api/admin/add_edit_location",

    //: Fetch All Venue
    getAllVenue: "/api/admin/fetch_all_venues",
    updateVenue: "/api/admin/add_edit_venues",

    //Fetch Dropdown of Location Venue
    getDropDown: "/api/admin/fetch_all_location_and_venue_dropData",

    //Fetch all Partner List
    getAllPartnerList: "/api/admin/fetch_all_partner",

    //Fetch all Partner List
    updatePartnerList: "/api/admin/add_edit_partner",

    /**************************************** Trainer ****************************************/
    get_All_Trainers: "/api/web/get_all_trainer",
    update_trainer: "/api/web/upadte_trainer_status",
    add_edit_trainer: "/api/web/add_edit_trainer",
    trainer_dropdowns: "/api/web/trainer_dropdown",
    get_all_employee_trainer: "/api/web/get_all_employees_trainer",
    /**************************************** News and Announcement ****************************************/
    add_edit_news_announcement: "/api/web/add_edit_news_announcement",
    get_all_announcement: "/api/web/get_all_announcement",
    get_all_notify_by_id: "/api/web/get_ann_notify_by_id",
    enable_disable_news_announcement: '/api/web/Enable_disable_announcement',

    /****************************************USER ROLE BASED ACCESS ****************************************/
    getAllRolesForAddEdit: "/api/usermaster/get_user_addedit_roleData",
    getUserDropdownData: "/api/usermaster/get_user_dropdown_data",
    getResultProfileData: "/api/usermaster/get_result_profile_data",
    addEditUserData: "/api/usermaster/add_edit_user_data",
    getEMPInduction: "/api/dashboard/induction_emp_filter_demo",
    getalluser: "/api/usermaster/fetchAlluser_demo",
    getValidationStatus:"/api/usermaster/checkuserData",
    /*********************************************** DASHBOARD ******************************************************* */

    get_dashboard_data: "/api/web/dashboard_data",

    /**
     * Report
     */
    schedule_dropdown: "/api/web/fetch_report_schedule_dropdown",
    add_scheduled_immediate: "/api/reportQueue/report/addEdit",
    schedule_detail: "/api/reportQueue/report/GetScheduleDetail",

    report_fetch_all: "/api/web/report_fetch_all",
    report_course_consumption_fetch_filter:
      "/api/web/report_fetch_detail_course_consumption_filters",
    report_fetch_course_consumption_list:
      "/api/web/report_fetch_course_consumption_list",
    report_fetch_course_consumption_list_for_quiz:
      "/api/web/report_fetch_course_consumption_list_for_qize",
    report_fetch_course_consumption: "/api/web/report_fetch_course_consumption",
    report_fetch_course_consumption_download:
      "/api/web/report_fetch_course_consumption_download",
    report_insert: "/api/web/report_insert",
    report_update: "/api/web/report_update",
    /* Feedback Report */
    report_fetch_feedback_course_consumption_list:
      "/api/web/report_fetch_course_consumption_list_Feed_Only",
    report_fetch_feedback_consumption:
      "/api/web/report_fetch_feedback_consumption",
    report_fetch_feedback_consumption_activitywise:
      "/api/web/report_fetch_feedback_consumption_activitywise",
    report_fetch_feedback_consumption_download:
      "/api/web/report_fetch_feedback_consumption_download",
    /* Activity Report */
    report_fetch_course_activity_consumption:
      "/api/web/report_fetch_course_activity_consumption",
    report_fetch_course_activity_consumption_download:
      "/api/web/report_fetch_course_activity_consumption_download",
    /* Quiz Report */
    report_fetch_quiz_consumption: "/api/web/report_fetch_quiz_consumption",
    report_fetch_quiz_consumption_download:
      "/api/web/report_fetch_quiz_consumption_download",
    report_fetch_quiz_consumption_quiestion_wise:
      "/api/web/report_fetch_quiz_consumption_questionwise",
    report_fetch_quiz_consumption_quiestion_wise_download:
      "/api/web/report_fetch_quiz_consumption_questionwise_download",

    //quiz-only
    report_fetch_course_consumption_list_Quiz_Only:
      "/api/web/report_fetch_course_consumption_list_Quiz_Only",
    //quiz activity
    report_fetch_quiz_consumption_activitywise:
      "/api/web/report_fetch_quiz_consumption_activitywise",
    /* Employee Attendance */
    report_fetch_emp_attendance: "/api/web/report_fetch_emp_attendance_api",
    report_fetch_emp_attendance_download:
      "/api/web/report_fetch_emp_attendance_download",
    /* Workflow report */
    report_fetch_workflow_list: "/api/web/report_fetch_workflow_list",
    report_workflow_filters: "/api/web/report_fetch_detail_workflow_filters",
    report_workflow_structure: "/api/web/report_fetch_workflow_structure_api",
    report_workflow_download:
      "/api/web/report_fetch_workflow_structure_download",
    report_workflow_detail: "/api/web/report_fetch_workflow_detail_api",
    report_workflow_consumption:
      "/api/web/report_fetch_workflow_consumption_api",
    report_workflow_consumption_download:
      "/api/web/report_fetch_workflow_consumption_download",

    /* TTT reports */
    report_ttt_trainer_detail: "/api/web/report_fetch_trainer_details",
    report_fetch_TTT_workflow_list: "/api/web/report_fetch_workflow_details",
    report_fetch_TTT_workflow_reports:
      "/api/web/report_fetch_TTT_Workflow_consumption_api",
    report_fetch_TTT_workflow_reports_download:
      "/api/web/report_fetch_TTT_Workflow_consumption_download",
    report_fetch_TTT_Steps_reports:
      "/api/web/report_fetch_TTT_Step_consumption_api",
    report_fetch_TTT_Steps_reports_download:
      "/api/web/report_fetch_TTT_Step_consumption_api",
    report_fetch_TTT_nominee_detail:
      "/api/web/report_fetch_ttt_nomination_details_api",
    report_fetch_TTT_nominee_detail_download:
      "/api/web/report_fetch_ttt_nomination_details_download",
    report_fetch_TTT_bh_approval: "/api/web/report_fetch_ttt_bh_details_api",
    report_fetch_TTT_bh_approval_download:
      "/api/web/report_fetch_ttt_bh_details_download",
    report_ttt_stepwise: "/api/web/ttt_step_reports",
    /* TA reports */
    reports_TA_filters: "/api/web/report_fetch_detail_TA_filters",
    reports_TA_Fetch_ProgramsList: "/api/web/report_fetch_detail_TA_programs",
    reports_TA_reports: "/api/web/report_fetch_TA_program_consumption_api",
    reports_TA_reports_download:
      "/api/web/report_fetch_TA_program_consumption_download",
    reports_TA_Fetch_BatchList: "/api/web/report_fetch_detail_TA_batch",
    reports_TA_GET_BATCHWISE_REPORT:
      "/api/web/report_fetch_TA_batch_consumption_api",
    reports_TA_GET_BATCHWISE_REPORT_DOWNLOAD:
      "/api/web/report_fetch_TA_batch_consumption_download",
    reports_TA_GET_EMPWISE_REPORT:
      "/api/web/report_fetch_TA_employee_consumption_api",
    reports_TA_GET_EMPWISE_REPORT_DOWNLOAD:
      "/api/web/report_fetch_TA_employee_consumption_download",

    /* Pre-On-boarding Report*/
    report_preonboarding_report:
      "/api/web/report_fetch_pre_onboarding_consumption_api",

    //consolidated reports
    report_consolidated_report: "/api/web/report_Consolidated_course",

    //consolidated workflow reports
    report_consolidated_workflow_report:
      "/api/web/report_Consolidated_workflow",

    //consolidated classroom attendance reports
    report_consolidated_classroom_attendance_report:
      "/api/web/report_Consolidated_classroom_attendance",

    //consolidated classroom feedback reports
    report_consolidated_classroom_feedback_report:
      "/api/web/report_Consolidated_classroom_feedback",

    //Quiz Summary Report
    report_quiz_summary_report:
      "/api/web/report_fetch_quiz_consumption_api_summary",

    //Quiz Cumulative Employee Score Report
    report_quiz_cumulative:
      "/api/web/report_fetch_quiz_consumption_api_summary_cumulative",
    report_quiz_cumulative_download:
      "/api/web/report_fetch_quiz_consumption_api_summary_cumulative_download",

    //Employee List
    fetch_course_employee_list: "/api/web/all_employee_list",
    report_fetch_emp_list:
      "/api/web/report_fetch_course_consumption_api_emp_wise",
    url_report_fetch_emp_list_download:
      "/api/web/report_fetch_course_consumption_download_emp_wise",

    // Get report list to download
    fetch_report_download_list: "/api/reportQueue/report/ReportGetList",
    fetch_report_scheduleList_list: "/api/reportQueue/report/GetScheduleList",
    enable_disable_scedule_List:"/api/reportQueue/report/Scheduleactiveinactive",

    /*Employee detail */
    report_emplyee_consumption:
      "/api/web/report_fetch_employee_login_details_consumption_api",
    report_emplyee_consumption_download:
      "/api/web/report_fetch_employee_login_details_consumption_download",

    report_emplyee_consumption_list: "/api/web/fetch_login_list",

    report_like_dislike: '/api/web/report_like_dislike',

    //  DAM Report
    report_get_dam_file_details:'/api/web/report_get_dam_file_details_api',
    report_get_dam_file_share_details :'/api/web/report_get_dam_file_share_details_api',
    report_get_dam_category_list:'/api/web/report_get_dam_category_list_api',
    report_get_dam_asset_mapping:'/api/web/report_get_dam_asset_mapping_api',
    report_get_dam_asset_version_details :'/api/web/report_get_dam_asset_version_details_api',
    /*EEP WORKFLOW */
    getEEP: "/api/web/get_all_eep_instance",
    disableEEPInstances: "/api/web/disable_eep",
    getallEEPcodeofconduct: "/api/web/eep_ALL_instance_codeofconduct",
    addeditEEPcodeofconduct: "/api/web/ADD_EDIT_EEP_Instance_codeofconduct",
    addEditEEPInstance: "/api/web/add_edit_eep",
    EEPInstanceDropdown: "/api/web/eep_instance_dropdown",
    getAllEEPNominnes: "/api/web/get_all_eep_nominees",
    getallEEPenroluser: "/api/web/getalleepenrole",
    urlEEPBulEnrol: "/api/web/eep_bulk_enrol",
    getSerchEEPenroluser: "/api/web/getalleepenrolesearch",
    enableDisableEEPuser: "/api/web/enabledisableEEPenrole",
    addselfsetting_eep: "/api/web/eep_addselfsetting",
    addeepmanualEnrol: "/api/web/addmanualEnrolEEP",
    EEPenrolserch: "/api/web/getalleepenrolesearch",
    getEEPselfsetting: "/api/web/getEEPselfsetting",
    EEPaddselfsetting: "/api/web/eep_addEDitselfsetting",
    urlEEPPMRDAC: "/api/web/eep_addpmrdacapproval",
    employeeCriteria: "/api/web/eep_get_criteria_status_for_employee",
    /*nomi-enrol-course */
    getallEEPcoursesinstance: "/api/web/get_all_courses_eep_instance",
    enrolcourseforEEPinstance: "/api/web/enrol_course_for_eep_instance_content",
    getallEEPenrolcoursemoduleinstance:
      "/api/web/get_all_enrol_course_modules_eep_instance",
    removeEEPenrolcourseinstance: "/api/web/remove_eep_instance_course",
    getEEPistanceactivitydropdown: "/api/web/EEP_instance_steps_dropdown",
    insertEEPSteps: "/api/web/ADD_EDIT_eep_Instance_steps",
    getallEEPsteps: "/api/web/ALL_EEP_instance_steps",
    getEEPNomineeDetails: "/api/web/get_eep_nominee_details",
    getEEPGBhapprovalStatus: "/api/web/get_eep_gbhapproval_status",
    updateEEPGBhApprovalStatus: "/api/web/update_gbhapproval_status",
    updateEEPAptitudeTestDetails: "/api/web/eep_aptitude_details",
    getEEPAdminapprovalStatus: "/api/web/get_eep_adminapproval_status",
    updateEEPAdminApprovalStatus: "/api/web/update_adminapproval_status",
    getallEEPbuisnessbreak: "/api/web/eep_ALL_instance_buisness_break",
    addeditEEPbuisnessbreak: "/api/web/ADD_EDIT_EEP_Instance_buisness_break",
    ///////////feedback section//////////////////
    insertFeedbackSection: "/api/web/insert_feedback_section",
    deleteFeedbackSection: "/api/web/delete_feedback_section",

    /*********************************  Send Help Feedback  *********************************** */
    insertHelpFeedback: "/api/web/insert_help_feedback_section",
    sendHelpFeedback: "/api/web/insert_help_feedback_section",
    /*Upload Employee */
    getLOVuploadData: "/api/admin/get_emp_update_lov_data",
    uploadEmployess: "/api/usermaster/insertuser",
    /*EMPLOYEES*/
    getAllEMPMasterData: "/api/admin/employees/get_all_emp_master_Data",
    updateEMPAttendanceStatus:
      "/api/admin/employees/update_attendance_status_employee",
    getallemployee: "/api/admin/employees/get_all_emp_master_Data_demo",
    /*Unenrol Type */
    unenrolValidation: "/api/web/validate_user_data_course",
    bulkunenrolCourseBUndle: "/api/web/bulk_unenrol_course_bundle",

    /** PDF Viewer */
    pdfviewerurl: "/pdf-viewer/web/viewer.html?file=",
    getPdfUrl: "/api/studentPortal/getPdfUrl?file=",
    deletePdfUrl: "/api/studentPortal/deletePdfUrl?file=",

    /** TA Dashboard */
    getTrainerList: "/api/web/fetch_trainer_list",
    getTrainerDashboardData: "/api/web/fetch_level_one_data",
    getIntialTrainerDashbaordData: "/api/web/fetch_overall_db_data",
    programList: "/api/web/fetch_program_list",
    getLevel1CourseList: "/api/web/fetch_level_2_clist",
    getLevel1CourseListOA: "/api/web/fetch_level_2_clist_oa",
    getLevel1QuestionList: "/api/web/fetch_level_2_qlist",
    getLevel1QuestionListOA: "/api/web/fetch_level_2_qlist_oa",
    getLevel2CourseList: "/api/web/fetch_level_3_elist",
    getLevel2CourseListOA: "/api/web/fetch_level_3_elist_oa",
    getLevel2QuestionList: "/api/web/fetch_level_3_qlist",
    getLevel2QuestionListOA: "/api/web/fetch_level_3_qlist_oa",

    getAllUshaParticipants: "/api/web/get_all_enrolled_emp_by_batchId",
    getCourseModulesTA: "/api/web/get_enrol_course_modules",
    getFeedbackData: "/api/web/get_feedback_data_by_feedbackId",
    getEvalutionfbquestion: "/api/web/TA_Evalution_feedback_question",
    submitTAevalutionfb: "/api/web/submit_TA_feedback_answers",
    getAssessmentData: "/api/web/get_assessment_data_by_quizId",
    getAllParticipantAttendance: "/api/web/get_all_participants_attendance",
    insertDeleteAttendance: "/api/web/insert_attendance_usha_batch",
    //disable activity
    disable_activity: "/app/web/disable_activity",
    //disable_module
    disable_module: "/app/web/disable_module",
    // log saved
    logSaved: "/api/studentPortal/logsaved",

    // Notification tags
    getNotificationTags: "/app/web/get_notify_tag",

    // get_all_tags
    getAllTagsComan: "/api/admin/get_all_tagList",
    gettagslist: "/api/admin/tag/get_all_tag_list",
    addedittag: "/api/admin/tag/add_edit_tags",
    enableTag: "/api/admin/tag/disable_tag",

    // OpenApi List
    fetchDropdown_List: "/api/openapi/admin/fetchdropdownlist",
    fetchGroupList: "/api/openapi/admin/fetchgrouplist",
    fetchGroupDetail: "/api/openapi/admin/fetchgroupdetail",
    add_editgroup: "/api/openapi/admin/addeditgroups",
    updateVisibility: "/api/openapi/admin/updatevisibilitygroups",
    // EEP Institute
    fetch_institute_list: "/api/admin/institute/get_all_institute_list",
    add_edit_instituts: "/api/admin/institute/add_edit_institute",
    enable_disable_institute: "/api/admin/institute/enable_disable_institute",

    // GBH Mapping Data
    fetch_gbhmapping_list: "/api/admin/gbhmapping/get_all_gbhmapping_list",
    add_edit_gbhmapping: "/api/admin/gbhmapping/add_edit_gbhmapping",
    enable_disable_gbhmapping:
      "/api/admin/gbhmapping/enable_disable_gbhmapping",
    get_all_gbhmapping_dropdown:
      "/api/admin/gbhmapping/get_all_gbhmapping_dropdown",
    get_emp_search_string: "/api/admin/gbhmapping/get_emp_search_string",
    get_unique_system_identifier: "/api/openapi/admin/fetchSystemIdentifier",
    inset_unqiue_system_identifier: "/api/openapi/admin/insertSystemIdentifier",
    update_unqiue_system_identifier:
      "/api/openapi/admin/updateSystemIdentifierVisibility",

    // Stepwise  data eep
    getstepwisedataEEP: "/api/web/EEP_step_wise_data",
    getstepwiseuserdataEEP: "/api/web/EEP_step_wise_UserData",
    stepwisebhapprovalEEP: "/api/web/EEP_update_bhapproval_stepwise",
    stpwiseadminapprovalEEP: "/api/web/EEP_update_adminapproval_stepwise",

    // Generate Link
    genearteFirebaseUrl: "/api/common/generateFirebaseDynamicLink",

    // Survey report
    getSurveyList: "/api/web/report_fetch_survey_list",
    getSurveyReport: "/api/web/report_survey_consumption_api",

    // Poll report
    getPollList: "/api/web/report_fetch_poll_list",
    getPollReport: "/api/web/report_poll_consumption_api",

    //eep report
    geteepList: "/api/web/report_eep_get_cohort_list",
    getnominatedList: "/api/web/report_eep_get_nominated_employeee_details",

    //theme
    getAllThemeData: "/api/admin/theme/get_all_theme_data",
    setThemedata: "/api/admin/theme/set_as_default_theme",
    addEditThemeData: "/api/admin/theme/add_edit_theme_data",
    changeImage: "/api/admin/theme/upload_logo_file",

    //Discount
    getDiscountList: "/api/admin/discount/getDiscountListApi",
    getDiscountDropdown: "/api/admin/discount/getDiscountDropdownApi",
    addEditDiscountData: "/api/admin/discount/addEditDiscountDataApi",

    //pricing
    getPriceDetails: "/api/admin/price/getPriceDetailsApi",
    getPriceDropdown: "/api/admin/price/getPriceDropdownApi",
    addEditPriceData: "/api/admin/price/addEditPriceDataApi",

      //SignUp
      getSignUpDropdownList: "/api/admin/signUp/getSignUpDropdownList",
      insertSignUpData: "/api/admin/signUp/insertSignUpData",
      checkIfDomainNameIsAvailable: "/api/admin/signUp/checkIfDomainNameAlreadyExist",
      createSubDomain: "/api/admin/signUp/createSubDomain",
      resendVerificationMail: "/api/admin/signUp/resendVerificationMail",

      // Plans List
      getPlansList: '/api/admin/getPlanList',

      // Bulk Upload
      getCourseModuleData: '/api/admin/bulkupload/activity/coursemoduleData',
      bulkUploadAcitivityInBulk: '/api/admin/bulkupload/activity',
      getBatchCategoryList:"/api/admin/bulkupload/categoryDropdown",
      getBulkNotificationTemplate: '/api/admin/bulkupload/notificationtemplate',
      getBulkCourseList: '/api/admin/bulkupload/getallcourseListfordropdown',
      insertNotificationTemplate: '/api/admin/bulkupload/insertnotificationTemplate',
      insertNotificationBulk: '/api/admin/bulkupload/insertnotification',
      enrolEmployeesAreaWise :'/api/admin/bulkupload/enrolEmployeesAreaWise',
      unenrolEmployeesAreaWise :'/api/admin/bulkupload/unenrolEmployeesAreaWise',
  },
};
