/**
*logstore for admin
* @author Vivek Adhav
*/

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {webApi} from './webApi';

@Injectable()
export class LogServices{

    areas : any;
    ipAddress : any;
    constructor(private http: HttpClient) {}

    saveLogged(param){
       // var params = this.populateUserInfo(param);
        var url =  webApi.domain + webApi.url.logSaved;
        return new Promise(resolve => {
            this.http.post(url, param)
            // .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve(err);
                if(err){
                    //this.toastr.error('Please check server connection','Server Error!');
                }
            });
        });
    };
}