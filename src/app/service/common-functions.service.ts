import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import * as _moment from 'moment';
import { timeoutWith,timeout, map, catchError} from 'rxjs/operators';
import { throwError as observableThrowError, throwError, TimeoutError } from 'rxjs';

@Injectable({
  providedIn: "root"
})
export class CommonFunctionsService {
  constructor(private toastPopup: ToastrService, private http: HttpClient) {
  }

  getMatchedArray(dataArray, arraytoMatch, variableToMatch) {
    // const IdList = dataToMatchedList.userIds.split(',');
    if(this.isEmpty(dataArray) && this.isEmpty(arraytoMatch) && !variableToMatch){
      return [];
    }


    const matchedItem = [];
    const IdKey = {};
    arraytoMatch.forEach(element => {
      IdKey[element] = true;
    });

    dataArray.forEach((data, index) => {
      if (IdKey[data[variableToMatch]] == true) {
        matchedItem.push(data);
      }
    });

    return matchedItem;
  }

  isEmpty(data){
    if (data && data.length !== 0) {
      return true;
    } else{
      return false;
    }
  }

  sort(property,sortby){
    var sortOrder = sortby;

    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }

    return function (a,b) {
      if(a[property] && b[property]){
        if(sortOrder == 2){
            return b[property].localeCompare(a[property]);
        }else{
            return a[property].localeCompare(b[property]);
        }
      }
    }
  }

  presentToast(status, message, type) {
    if (status >= 200 && status < 300 ) {
      if (type === 'Success'){
        this.toastPopup.success(message, 'Success', {
          closeButton: false,
        });
      }
      if(type === 'Information'){
        this.toastPopup.info(message, 'Information', {
          closeButton: false,
        });
      }
    } else if (status <= 300 && status > 400 ) {
      this.toastPopup.error('Content Moved Permanently', 'Error', {
        timeOut: 0,
        closeButton: true,
      });
    } else if (status <= 400 && status > 500 ) {
      this.toastPopup.error('Bad Request', 'Error', {
        timeOut: 0,
        closeButton: true,
      });
    } else {
      this.toastPopup.error('Internal Server Error', 'Error', {
        timeOut: 0,
        closeButton: true,
      });
    }


  }

  httpPostRequest(url, parameters){
    return new Promise((resolve, reject) => {
      this.http.post(url,parameters).subscribe((success)=> {
        resolve(success);
      },(error) => {
        reject(error);
      });
    });
  }

  reportPostRequestTimeOut(url, data){
    console.log("url",url);
    return this.http.post(url, data).pipe(
      // timeout(3000),
      timeout(120000),
      map((response: any) => { // Success...
              return response;
      }),
      catchError((error: HttpErrorResponse) => this.handleError(error)),
    );
    // .subscribe( (res) =>{
    //   console.log("res", res)
    // },
    // error => console.log('ERROR', error));
  }

  reportPostRequest2(url, data) {
     this.http.post(url, data, {observe: 'response'}).pipe(
      map((response: any) => { // Success...
              return response;
      }),
      catchError(this.handleError)
    ).subscribe( (res) =>{
      console.log("res", res)
    },
    error => console.log('ERROR', error),
    );
  }

  private handleError(error: HttpErrorResponse) {
    // console.log('error', error)
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    }else if (error instanceof TimeoutError) {
      const errorObject = {
        type: 'TimeoutError',
        message : "Report taking too much time, can't be Previewed. Please Schedule the report",
      };
      return throwError(errorObject);
   } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.log(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // Return an observable with a user-facing error message.
    const errorObject = {
      type: 'serverError',
      message : "Unable to connect to Server. Please check internet connection and try again",
    };
    return throwError(errorObject);
  }
  getHelpContent() {
    // return new Promise((resolve, reject) => {
    //   this.http
    //     .get('../../../../../../assets/help-content/addEditCourseContent.json')
    //     .subscribe((success)=> {
    //       resolve(success);
    //     }
    //       // data => {
    //       //   this.helpContent = data;
    //       //   console.log('Help Array', this.helpContent);
    //       // },
    //       resolve(data['data']);
    //       err => {
    //         resolve('err');
    //       }
    //     );
    // });
    // return this.helpContent;
    const url ='assets/help-content/addEditCourseContent.json';
    return new Promise((resolve, reject) => {
      this.http.get(url).subscribe((success)=> {
        resolve(success);
      },(error) => {
        reject(error);
      });
    });
  }
  getLabel(){
    const url ='assets/help-content/help_content.json';
    return new Promise((resolve, reject) => {
      this.http.get(url).subscribe((success)=> {
        resolve(success);
      },(error) => {
        reject(error);
      });
    });
  }

  formatSendDateTime(Dtime){
    const t = new Date(Dtime);
    // const formattedDateTime = this.datepipe.transform(t, 'yyyy-MM-dd h:mm');
    const year = t.getFullYear() +  '-' + (t.getMonth() + 1 )+ '-' + t.getDate();
    const time = ' ' + t.getHours() + ':' + t.getMinutes() + ':' +  t.getSeconds();
    const formattedDateTime = year + time;
    return formattedDateTime;
  }

  formatSendDate(Dtime){
    // if(Dtime!= null){
    const t = new Date(Dtime);
    // const formattedDateTime = this.datepipe.transform(t, 'yyyy-MM-dd h:mm');
    const formatSendDate = t.getFullYear() +  '-' + (t.getMonth() + 1 )+ '-' + t.getDate();
    // const time = ' ' + t.getHours() + ':' + t.getMinutes() + ':' +  t.getSeconds();
    // const formattedDateTime = year + time;
    return formatSendDate;
  // }
    // else{
    //   return null
    // }
  }

  formatDigitDate(Dtime){
    if(Dtime != null){
    const t = new Date(Dtime);
    // const formattedDateTime = this.datepipe.transform(t, 'yyyy-MM-dd h:mm');
    const formatSendDate = t.getFullYear() +  '-' + ("0" + (t.getMonth() + 1)).slice(-2) + '-' + ("0" + (t.getDate())).slice(-2);
    // const time = ' ' + t.getHours() + ':' + t.getMinutes() + ':' +  t.getSeconds();
    // const formattedDateTime = year + time;
    console.log(formatSendDate,"formatSendDate")
    return formatSendDate;
    }else{
      return null
    }
  }

  formatSendTime(Dtime){
    const t = new Date(Dtime);
    // const formattedDateTime = this.datepipe.transform(t, 'yyyy-MM-dd h:mm');
    // const formatSendDate = t.getFullYear() +  '-' + (t.getMonth() + 1 )+ '-' + t.getDate();
    const time = ' ' + t.getHours() + ':' + t.getMinutes() + ':' +  t.getSeconds();
    // const formattedDateTime = year + time;
    return time;
  }

  formatDateTimeForBulkUpload(Dtime){

     // 1st Way
    // let from = '11-04-2017' ;
    // let milliseconds = _moment(Dtime, "DD-MM-YYYY");
    // let f = new Date(milliseconds);
    // console.log('time format ==>' , f);

    // console.log('time format to upload ==>' ,  this.formatSendDate(f));
    // return this.formatSendDate(f);

    // 2nd Way

    const toDate = (dateStr) => {
      if (dateStr) {
        const [day, month, year] = dateStr.includes('/') ? dateStr.split('/') : dateStr.split('-');
        if(day.length > 2) {
          return null
        }
        else{
          return new Date(year, Number(month) - 1, day);
        } 
      } else {
        return null;
      }
    };


    // console.log('iffy function format ==>' ,  toDate(Dtime));
    // console.log('time format to upload ==>' ,  this.formatDigitDate(toDate(Dtime)));
    return this.formatDigitDate(toDate(Dtime));
     // 3rd Way
    // const method3 = new Date(Dtime.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
    // console.log('time format to upload ==>' ,  this.formatSendDate(toDate(method3)));




  }
}
