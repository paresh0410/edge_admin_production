import { Injectable } from '@angular/core';
import { feature } from '../../environments/feature-environment';
@Injectable({
  providedIn: 'root',
})
export class ModuleAccessService {
  featureConfig = feature;
   rolesAccessData = {
    onlineCourse : {
      allowCourseVisibilityModification : true,
    },
  };
  constructor() {

  }

  getRoleAccessData(string){
      const loginResData = JSON.parse(localStorage.getItem('LoginResData'));
      // localStorage.getItem('LoginResData');
      if(loginResData['data']['otherdata'].length !== 0){
        const tenantId = Number(loginResData['data']['data'].tenantId);
        const roleId = Number(loginResData['data']['data'].roleId);
        if(tenantId === 1 && this.featureConfig.allowModuleRestrictition
          && (roleId === 2 || roleId === 3 || roleId === 6 || roleId === 11)) {
          this.rolesAccessData.onlineCourse.allowCourseVisibilityModification = false;
        }else{
          this.rolesAccessData.onlineCourse.allowCourseVisibilityModification = true;
        }
      }
      if (this.rolesAccessData.hasOwnProperty(string)){
        return (this.rolesAccessData[string]);
      }else {
          return ({});
      }
  }
}
