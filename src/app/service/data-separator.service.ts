import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class DataSeparatorService {
  private pipe: string = '';
  private hash: string = '';
  private dollar: string = '';
  constructor() { }

  setValuesToVariable(separators){
    this.pipe = separators.pipeValue;
    this.hash = separators.hashValue;
    this.dollar = separators.dollarValue;
  }
  get Pipe(){
    if (this.pipe !== '' && this.pipe){
      return this.pipe;
    } else{
      if(localStorage.hasOwnProperty('separators') && localStorage.getItem('separators')) {
        const separators = JSON.parse(localStorage.getItem('separators'));
        this.setValuesToVariable(separators);
        return this.pipe;
      }
    }
  }
  get Hash(){
    if (this.hash !=='' && this.hash) {
      return this.hash;
    } else{
      if(localStorage.hasOwnProperty('separators') && localStorage.getItem('separators')) {
        const separators = JSON.parse(localStorage.getItem('separators'));
        this.setValuesToVariable(separators);
        return this.hash;
      }
    }
  }
  get Dollar(){
    if (this.dollar !== '' && this.dollar) {
      return this.dollar;
    } else{
      if(localStorage.hasOwnProperty('separators') && localStorage.getItem('separators')) {
        const separators = JSON.parse(localStorage.getItem('separators'));
        this.setValuesToVariable(separators);
        return this.dollar;
      }
    }
  }
}
