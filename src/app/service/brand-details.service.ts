import { Injectable } from '@angular/core';
import { feature } from '../../environments/feature-environment';

@Injectable({
  providedIn: 'root'
})
export class BrandDetailsService {
  featureConfig = feature;
  brandObject = {
    ILLUME : {
      logo: 'assets/images/512.png',
      banner_image: 'assets/images/banner_new.jpg',
      dashboard_logo: 'assets/images/512.png',
      tagLine: 'Login as employee, click on your organization ',
      termsandcondition: 'https://www.bajajfinserv.in/privacy-policy',
      brandName: 'ILLUME',
      employee: 'Employee',
      ecn: 'ecn',
  },
  EDGE : {
    logo: 'assets/images/Edge/edge-logo.png',
    banner_image: 'assets/images/Edge/dashbord_baner.jpg',
    dashboard_logo: 'assets/images/Edge/edge-logo.png',
    tagLine: 'Learning in the flow of life',
    termsandcondition: 'https://www.edgelearning.co.in/pages/privacy-policy.html',
    brandName: 'Edge',
    employee: 'Learner',
    ecn: 'Learner Code',
},
}

getCurrentBrandData() {
  const currentPageURL = window.location.hostname;
  console.log('currentPageURL' , currentPageURL);
  let data;
  // if(currentPageURL.indexOf('saas') !== -1){
  //   data = this.brandObject['Edge'];
  // }else {
  //   data =  this.brandObject['ILLUME'];
  // }
  // const name = String(this.featureConfig.brandName).toUpperCase();
  switch (String(this.featureConfig.brandName).toUpperCase()) {
    case 'EDGE':
      data = this.brandObject['EDGE'];
      break;
    case 'ILLUME':
        data = this.brandObject['ILLUME'];
        break;
    default:  data = this.brandObject['ILLUME'];
    break;
  }
  // data = this.brandObject['Edge'];
  // data = this.brandObject['ILLUME'];

  console.log('Brand Data -==>' , data);
  return data;
}
  constructor() { }
}
