import { TestBed } from '@angular/core/testing';

import { BrandDetailsService } from './brand-details.service';

describe('BrandDetailsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BrandDetailsService = TestBed.get(BrandDetailsService);
    expect(service).toBeTruthy();
  });
});
