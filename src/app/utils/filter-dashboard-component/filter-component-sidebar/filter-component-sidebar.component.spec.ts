import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterComponentSidebarComponent } from './filter-component-sidebar.component';

describe('FilterComponentSidebarComponent', () => {
  let component: FilterComponentSidebarComponent;
  let fixture: ComponentFixture<FilterComponentSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterComponentSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterComponentSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
