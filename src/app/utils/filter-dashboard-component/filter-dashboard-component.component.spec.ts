import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterDashboardComponentComponent } from './filter-dashboard-component.component';

describe('FilterDashboardComponentComponent', () => {
  let component: FilterDashboardComponentComponent;
  let fixture: ComponentFixture<FilterDashboardComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterDashboardComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterDashboardComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
