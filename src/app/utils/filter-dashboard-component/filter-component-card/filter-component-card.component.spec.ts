import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterComponentCardComponent } from './filter-component-card.component';

describe('FilterComponentCardComponent', () => {
  let component: FilterComponentCardComponent;
  let fixture: ComponentFixture<FilterComponentCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterComponentCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterComponentCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
