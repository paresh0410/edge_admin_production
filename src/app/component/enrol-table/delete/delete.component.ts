import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ngx-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})
export class DeleteComponent implements OnInit {
  @Input() buttonDelete :string;
  @Output() buttonDelClick  = new EventEmitter();
  temp: any;
  constructor() {
  
    this.temp = this.buttonDelete;
   }

  ngOnInit() {

  }

  btndelclick() {
    this.buttonDelClick.emit();
  }

}
