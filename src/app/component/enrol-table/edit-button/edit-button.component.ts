import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ngx-edit-button',
  templateUrl: './edit-button.component.html',
  styleUrls: ['./edit-button.component.scss']
})
export class EditButtonComponent implements OnInit {
  // @Input() iconText :string;
  @Output() iconClick  = new EventEmitter();
  temp: any;
  constructor() { 
    // console.log('this.iconText', this.iconText);
    // this.temp = this.iconText;
  }

  ngOnInit() {
  }

  btnclick() {
    this.iconClick.emit();
  }

}
