import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
@Component({
  selector: 'ngx-text',
  templateUrl: './text.component.html',
  styleUrls: ['./text.component.scss']
})
export class TextComponent implements OnInit {
  @Input() normalText: string;
  @Input() htmlStr: string;
  @Input() fulltext: string;
  @Output() textClick  = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }
  btnclick() {
    this.textClick.emit();
  }
}
