import { Component, OnInit, Output, EventEmitter, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'ngx-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss']
})
export class CheckboxComponent implements OnInit {
  @Input() isChecked: boolean;
  @Output() clickToCheck = new EventEmitter<any>();


  ngOnInit() {
  }
  checkChange(event) {
    // this.clickToCheck.emit(event);
    if(event){
      console.log('checkevent', event.target.checked);
      // this.isChecked = !this.isChecked;
      this.clickToCheck.emit(event.target.checked);
    }

  }
  // ngOnChanges(){
  //   console.log('checkevent', this.isChecked);
  // }
}
