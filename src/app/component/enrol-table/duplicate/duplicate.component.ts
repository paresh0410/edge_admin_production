import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ngx-duplicate',
  templateUrl: './duplicate.component.html',
  styleUrls: ['./duplicate.component.scss']
})
export class DuplicateComponent implements OnInit {

    @Output() iconClick  = new EventEmitter();
    temp: any;
    constructor() { 
    }
  
    ngOnInit() {
    }
  
    btnclick() {
      this.iconClick.emit();
    }
}
