import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ngx-mobile',
  templateUrl: './mobile.component.html',
  styleUrls: ['./mobile.component.scss']
})
export class MobileComponent implements OnInit {
  @Input() normalText: string;

  constructor() { }

  ngOnInit() {
  }

}
