import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnrolTableComponent } from './enrol-table.component';

describe('EnrolTableComponent', () => {
  let component: EnrolTableComponent;
  let fixture: ComponentFixture<EnrolTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnrolTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnrolTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
