import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ngx-date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.scss']
})
export class DateComponent implements OnInit {
  @Input() normalText: string;

  constructor() { }

  ngOnInit() {
  }

}
