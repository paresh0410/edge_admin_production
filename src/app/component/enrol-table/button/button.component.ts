import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ngx-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
  @Input() buttonText :string;
  @Output() buttonClick  = new EventEmitter();
  temp: any;
  constructor() {
  
    this.temp = this.buttonText;
   }

  ngOnInit() {

  }

  btnclick() {
    this.buttonClick.emit();
  }
}
