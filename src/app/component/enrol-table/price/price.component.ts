import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ngx-price',
  templateUrl: './price.component.html',
  styleUrls: ['./price.component.scss']
})
export class PriceComponent implements OnInit {
  @Input() price: string;

  constructor() { }

  ngOnInit() {
  }

}
