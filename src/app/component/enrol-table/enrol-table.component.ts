import { Component, OnInit, Input, EventEmitter, Output, OnChanges } from '@angular/core';

@Component({
  selector: 'ngx-enrol-table',
  templateUrl: './enrol-table.component.html',
  styleUrls: ['./enrol-table.component.scss']
})
export class EnrolTableComponent implements OnInit , OnChanges {
  @Input() label: any = [];
  @Input() tableData: any = {};
  @Input() allCheck: any= false;
  @Input() addPointer: any= false;
  @Input() customCheckAll: any= false;
  @Input() customCheckAllFlag: boolean;
  @Input() paginationObject ? = {
    itemsPerPage: 10,
    id : '',
  };
  @Input() pagination: any= false;

  @Output() clickOnbutton  = new EventEmitter<any>();
  @Output() clickOnicon = new EventEmitter<any>();
  @Output()clickOntext=new EventEmitter<any>();
  @Output() selectRow = new EventEmitter<any>();
  @Output()clickOnCheck=new EventEmitter<any>();
  @Output()click=new EventEmitter<any>();
  @Output() clickOnDelbutton  = new EventEmitter<any>();
  @Output() clickToDownicon  = new EventEmitter<any>();
  @Output() selectedItems  = new EventEmitter<any>();
  @Output() customCheckAllClicked  = new EventEmitter<any>();
  selectedData: any = [];
  checkAllFlag = false;
  p:any;

  @Output() checkAll  = new EventEmitter<any>();
  constructor() { }

  ngOnInit() {
    this.paginationObject.id = Math.random().toString(36).substring(7);
    // this.returnSelectedItem();
  }

  selectAll(event) {
    console.log('tableData==================>',this.tableData);
    this.selectedData = [];
    // for(let i = 0; i< this.tableData.length; i++) {
    //   if(this.tableData[i].checked == undefined ||
    // this.tableData[i].checked == false || this.tableData[i].checked == true) {
    //     this.checkChange(this.tableData[i]);
    //   }
    // }
    // this.checkAll.emit();
    if (this.tableData){
      this.tableData.forEach(element => {
        element['checked'] = event;
        if(element['checked']){
          this.selectedData.push(element);
        }
      });
    }
    if (this.selectedData.length === this.tableData.length){
      this.checkAllFlag = true;
      console.log('Checked All Changed:' , this.checkAllFlag);
    }else{

      this.checkAllFlag = false;
      console.log('Checked All Changed:' , this.checkAllFlag);
    }
    this.selectedData = [...this.tableData];
    this.selectedItems.emit(this.selectedData);
  }
  onRowClick(item) {
    this.selectRow.emit(item);
  }

  clickToButton(item, data) {
    // this.clickOnbutton.emit(item);
    this.clickOnbutton.emit(data);
    console.log("click button", item);
    console.log("click data", data);
  }

  clickTotext(item, data) {
    // this.clickOnbutton.emit(item);
    this.clickOntext.emit(data);
    console.log("click text", item);
    console.log("click data", data);
  }

  clickToicon(item, data) {
    // this.clickOnbutton.emit(item);
    this.clickOnicon.emit(data);
    console.log("click button", item);
    console.log("click data", data);
  }
  clickToTable(item){
    this.click.emit(item);
  }
  checkChange(item) {
    item.checked = !item.checked;
    this.clickOnCheck.emit(item);
    this.returnSelectedItem();
    console.log(item);
  }
  clickToDelButton(item, data) {
    // this.clickOnbutton.emit(item);
    this.clickOnDelbutton.emit(data);
    console.log("click button", item);
    console.log("click data", data);
  }
  // clickTocheck(item, data) {
  //   // this.clickOnbutton.emit(item);
  //   this.clickOnCheck.emit(data);
  //   console.log("click button", item);
  //   console.log("click data", data);
  // }
  clickToDupicon(item, data) {
    // this.clickOnbutton.emit(item);
    this.clickOnicon.emit(data);
    console.log("click button", item);
    console.log("click data", data);
  }
  buttonDownClick(item, data) {
    // this.clickOnbutton.emit(item);
    this.clickToDownicon.emit(data);
    console.log("click button", item);
    console.log("click data", data);
  }

  returnSelectedItem() {
    this.selectedData = [];
    this.tableData.forEach(element => {
        if (element.checked){
          this.selectedData.push(element);
        }
    });
    if (this.selectedData.length === this.tableData.length){
      this.checkAllFlag = true;
    }else{
      this.checkAllFlag = false;
    }
    console.log(this.selectedData,"this.selected Data")
    this.selectedItems.emit(this.selectedData);
  }

  ngOnChanges(){
    console.log('On changed got called', this.customCheckAllFlag);
    if(this.pagination){
      // this.paginationObject.itemsPerPage = 10;
    }else {
      if(this.tableData){
        this.paginationObject.itemsPerPage = this.tableData.length ;
      }
    }
  }

  customCheckAllEmit(event){
    // this.customCheckAllFlag = event;
    this.customCheckAllClicked.emit(event);
  }
}
