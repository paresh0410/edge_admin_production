import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ngx-html',
  templateUrl: './html.component.html',
  styleUrls: ['./html.component.scss']
})
export class HtmlComponent implements OnInit {

  @Input() normalText: string;
  @Output() textClick  = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }
  btnclick() {
    this.textClick.emit();
  }
  truncateHTML(text: string): string {

    let charlimit = 25;
    if(!text || text.length <= charlimit )
    {
        return text;
    }


  let without_html = text.replace(/<(?:.|\n)*?>/gm, '');
  let shortened = without_html.substring(0, charlimit) + "...";
  return shortened;
}
coverthtmltotext(data) {
  if(data){
  var htmlString = data;
  var stripedHtml = htmlString.replace(/<[^>]+>/g, '');
  // console.log(stripedHtml);
  return (stripedHtml);
  }
}
}

