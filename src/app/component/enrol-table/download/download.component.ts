import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ngx-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.scss']
})
export class DownloadComponent implements OnInit {
  @Input() buttonDown: string;
  @Input() tableHead = {};
  @Input() noLinkAvailableText = '';
  @Output() buttonDownClick  = new EventEmitter();
  temp: any;
  constructor() {


   }

  ngOnInit() {
    console.log(this.buttonDown);
    if(this.buttonDown == null) {
      this.temp = 'fa fa-download'
    } else {
      this.temp = 'fa fa-download'
    }

  }

  btnDownclick() {
    this.buttonDownClick.emit();
  }
}
