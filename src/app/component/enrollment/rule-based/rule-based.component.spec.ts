import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RuleBasedComponent } from './rule-based.component';

describe('RuleBasedComponent', () => {
  let component: RuleBasedComponent;
  let fixture: ComponentFixture<RuleBasedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RuleBasedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RuleBasedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
