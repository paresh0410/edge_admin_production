import { Component, OnInit, EventEmitter, Output, Input, OnChanges , SimpleChanges } from '@angular/core';
import * as _moment from "moment";
import {
  DateTimeAdapter,
  OWL_DATE_TIME_FORMATS,
  OWL_DATE_TIME_LOCALE,
} from "ng-pick-datetime";
import { MomentDateTimeAdapter } from "ng-pick-datetime-moment";
import { noData } from '../../../models/no-data.model';
const moment = (_moment as any).default ? (_moment as any).default : _moment;
// import * as _ from "lodash";
export const MY_CUSTOM_FORMATS = {
  fullPickerInput: "DD-MM-YYYY",
  parseInput: "DD-MM-YYYY",
  datePickerInput: "DD-MM-YYYY",
  timePickerInput: "LT",
  monthYearLabel: "MMM YYYY",
  dateA11yLabel: "LL",
  monthYearA11yLabel: "MMMM YYYY",
};

@Component({
  selector: 'ngx-rule-based',
  templateUrl: './rule-based.component.html',
  styleUrls: ['./rule-based.component.scss'],
  providers: [
    {
      provide: DateTimeAdapter,
      useClass: MomentDateTimeAdapter,
      deps: [OWL_DATE_TIME_LOCALE],
    },
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS },
  ],
})
export class RuleBasedComponent implements OnInit, OnChanges {
  // @Output() searchClick = new EventEmitter<any>();
  @Output() sendEventToParent = new EventEmitter<any>();

  @Input() ruleEnrolConfig: any = {};


  showPopup: boolean = false;
  sidebarForm: boolean = false;
  titleName: string = "Add Rule";
  btnName: string = "Save";
  searchText: any;
  Title = '';
  showSearch: boolean = false;
  labels: any = [
    { labelname: 'NO', bindingProperty: 'number', componentType: 'text' },
    { labelname: 'FIRST NAME', bindingProperty: 'firstname', componentType: 'text' },
    { labelname: 'LAST NAME', bindingProperty: 'lastname', componentType: 'text' },
    { labelname: 'EMAIL', bindingProperty: 'email', componentType: 'text' },
    { labelname: 'COUNTRY', bindingProperty: 'country', componentType: 'text' },
  ];
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No Data Found.",
    desc:'To create batches in bulk click on upload button or to know more about bulk batch click on "Learn More"',
    titleShow:true,
    btnShow:false,
    descShow:false,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/learning-how-to-add-a-batch',
  };
  noDataVal1:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No Data Found.",
    // desc:'To create batches in bulk click on upload button or to know more about bulk batch click on "Learn More"',
    titleShow:true,
    btnShow:false,
    descShow:false,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/learning-how-to-add-a-batch',
  };
  tableList: any = [
    { "number": 3, "firstname": "Corina", "lastname": "Carolin", "email": "Corina.Carolin@gmail.com", "country": "Guam" },
    { "number": 4, "firstname": "Elena", "lastname": "Keelia", "email": "Elena.Keelia@gmail.com", "country": "Saint Lucia" },
    { "number": 5, "firstname": "Annabela", "lastname": "Shelba", "email": "Annabela.Shelba@gmail.com", "country": "Switzerland" },
    { "number": 6, "firstname": "Fernande", "lastname": "Garbe", "email": "Fernande.Garbe@gmail.com", "country": "Seychelles" },
    { "number": 7, "firstname": "Corina", "lastname": "Carolin", "email": "Corina.Carolin@gmail.com", "country": "Guam" },
    { "number": 8, "firstname": "Jan", "lastname": "Gower", "email": "Jan.Gower@gmail.com", "country": "Malta" },
    { "number": 9, "firstname": "Ezmeralda", "lastname": "Worda", "email": "Ezmeralda.@gmail.com", "country": "Brazil" },
    { "number": 10, "firstname": "Elena", "lastname": "Keelia", "email": "Elena.Keelia@gmail.com", "country": "Saint Lucia" },
  ];

  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  valid = true;
  nextDay:any;
  constructor() {
    console.log('Component Input ===>', this.ruleEnrolConfig);
    this.Title = 'Enroled User Popup';
   }

  ngOnInit() {
    const day = new Date();
    this.nextDay = new Date(day);
    this.nextDay.setDate(day.getDate() + 1);
    this.dropdownList = [
      { "id": 1, "itemName": "India" },
      { "id": 2, "itemName": "Singapore" },
      { "id": 3, "itemName": "Australia" },
      { "id": 4, "itemName": "Canada" },
      { "id": 5, "itemName": "South Korea" },
      { "id": 6, "itemName": "Germany" },
      { "id": 7, "itemName": "France" },
      { "id": 8, "itemName": "Russia" },
      { "id": 9, "itemName": "Italy" },
      { "id": 10, "itemName": "Sweden" }
    ];
    this.selectedItems = [
      { "id": 2, "itemName": "Singapore" },
      { "id": 3, "itemName": "Australia" },
      { "id": 4, "itemName": "Canada" },
      { "id": 5, "itemName": "South Korea" }
    ];
    this.dropdownSettings = {
      badgeShowLimit: 1,
      lazyLoading: true,
      showCheckbox: true,
      escapeToClose: false,
      singleSelection: false,
      enableSearchFilter: true,
      text: "Select From Below",
      selectAllText: 'Select All',
      noDataLabel: "No data found",
      unSelectAllText: 'UnSelect All',
      classes: "common-multi",
      primaryKey: "id",
      labelKey: "name",
    };
  }

  // ṣearch
  search() {
    this.showSearch = true;
    // this.searchClick.emit();
  }

  clearSearch() {
    this.searchText = '';
  }

  searchBar() {
    console.log('rule based search = ', this.searchText);
  }

  // sidebar-form
  addBtn() {
    this.sidebarForm = true;
  }

  cloeSidebar() {
    this.sidebarForm = false;
  }

  // pop-up
  openPopup() {
    this.showPopup = true;
  }

  closePopup() {
    this.showPopup = false;
    this.ruleEnrolConfig.showEnroleduserPopup = false;
    this.passEventToParent('closeRuleEnrolPopup', '');
  }

  // multi-select
  onItemSelect(item: any) {
    console.log(item);
    console.log(this.selectedItems);
  }
  OnItemDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedItems);
  }
  onSelectAll(items: any) {
    console.log(items);
  }
  onDeSelectAll(items: any) {
    console.log(items);
  }

  save(){
    console.log('save');
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
}

onChange(event){
  console.log(event);
  console.log(this.ruleEnrolConfig);
  if(event){
    this.passEventToParent('updateFormValuesRules', this.ruleEnrolConfig.ruleData);
  }
}

  passEventToParent(action, ...args){
    this.noDataVal={
      margin:'mt-3',
      imageSrc: '../../../../../assets/images/no-data-bg.svg',
      title:"Sorry we couldn't find any matches please try again.",
      desc:"",
      titleShow:true,
      btnShow:false,
      descShow:false,
      btnText:'Learn More',
      btnLink:'',
    }
    // if(action === 'addEditRule'){
    //   this.addBtn();
    // }
    // this.showSearch = false;
    if(action === 'clearRuleEnrol') {
      this.showSearch = false;
    }
    const event = {
      'action': action,
      'argument': args,
    };
    // console.log('action ===>',action);
    console.log('event ===>', event);
    this.sendEventToParent.emit(event);
    if (action === 'clearRuleEnrol') {
      this.searchText = '';
    }
  }
}
