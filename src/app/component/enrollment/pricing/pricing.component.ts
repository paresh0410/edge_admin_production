import { Component, OnInit, EventEmitter, Output, Input, ChangeDetectorRef } from '@angular/core';
import { webApi } from '../../../service/webApi';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
// import { noData } from '../../../models/no-data.model';
import { CommonFunctionsService } from '../../../service/common-functions.service';
import * as _moment from 'moment';
import {
  DateTimeAdapter,
  OWL_DATE_TIME_FORMATS,
  OWL_DATE_TIME_LOCALE,
} from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';
const moment = (_moment as any).default ? (_moment as any).default : _moment;
import * as _ from 'lodash';
import { noData } from '../../../models/no-data.model';
export const MY_CUSTOM_FORMATS = {
  fullPickerInput: 'DD-MM-YYYY',
  parseInput: 'DD-MM-YYYY',
  datePickerInput: 'DD-MM-YYYY',
  timePickerInput: 'LT',
  monthYearLabel: 'MMM YYYY',
  dateA11yLabel: 'LL',
  monthYearA11yLabel: 'MMMM YYYY',
};


@Component({
  selector: 'ngx-pricing',
  templateUrl: './pricing.component.html',
  styleUrls: ['./pricing.component.scss'],
  providers: [
    {
      provide: DateTimeAdapter,
      useClass: MomentDateTimeAdapter,
      deps: [OWL_DATE_TIME_LOCALE],
    },
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS },
  ],
})
export class PricingComponent implements OnInit {
  // @Output() searchClick = new EventEmitter<any>();
  @Output() sendEventToParent = new EventEmitter<any>();

  @Input() pricingConfig: any = {};

  sidebarForm: boolean = false;
  titleName: string = 'Add Pricing Plan';
  btnName: string = 'Save';
  searchText: any;
  showSearch: boolean = false;

  dropdownSettings = {};
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No Data Found.",
    desc:'To create batches in bulk click on upload button or to know more about bulk batch click on "Learn More"',
    titleShow:true,
    btnShow:false,
    descShow:false,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/learning-how-to-add-a-batch',
  };

  selectedItems = [];
  dropdownList = [];
  constructor(
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private commonFunctionsService: CommonFunctionsService,
    private cdf: ChangeDetectorRef,
  ) { }

  ngOnInit() {
    // this.getPriceList();
    // this.getDropDownList(() => {});
    this.dropdownSettings = {
      badgeShowLimit: 1,
      lazyLoading: true,
      showCheckbox: true,
      escapeToClose: false,
      limitSelection: 1,
      enableSearchFilter: true,
      text: 'Select From Below',
      selectAllText: 'Select All',
      noDataLabel: 'No data found',
      unSelectAllText: 'UnSelect All',
      classes: 'smallHeight custom-class-example',
      primaryKey: 'id',
      labelKey: 'InternalName',
      searchBy: ['InternalName', 'coupanCode'],
      // disabled: true,
    };
  }

  // ṣearch
  search() {
    this.showSearch = true;
    // this.searchClick.emit();
  }

  clearSearch() {
    this.searchText = '';
  }



  closeSidebar() {
    // this.sidebarForm = false;
    this.pricingConfig.showSidebar = false;
    this.cdf.detectChanges();
  }


  save(form) {
    console.log('Form ===>',form);
    if(form && form.valid){
      // this.createUpdatePrice();
      this.passEventToParent('createUpdatePrice', this.pricingConfig.addEditPriceForm);
    }else {
      Object.keys(form.controls).forEach(key => {
        form.controls[key].markAsDirty();
      });
    }
  }
  onItemSelect(item: any) {
    console.log(item);
    console.log(this.selectedItems);
  }
  OnItemDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedItems);
  }
  onSelectAll(items: any) {
    console.log(items);
  }
  onDeSelectAll(items: any) {
    console.log(items);
  }
  discountedListNotFound = false;
  // fetchDiscountList() {
  //   const getDiscountedList = webApi.domain + webApi.url.getDiscountList;
  //   const param = {};
  //   // this.spinner.show();
  //   this.commonFunctionsService
  //     .httpPostRequest(getDiscountedList, param)
  //     // this.serveyService.getServey(param)
  //     .then(
  //       (rescompData) => {
  //         // this.spinner.hide();
  //         console.log('getDiscountedList ==>', rescompData);
  //         if(rescompData && rescompData['type'] && rescompData['data'] && rescompData['data'].length !== 0){
  //           // this.makeTableDataReady(rescompData['data']);
  //           this.dropdownList = rescompData['data'];
  //         }else {
  //           this.discountedListNotFound = true;
  //         }
  //       },
  //       (error) => {
  //         this.spinner.hide();
  //         this.discountedListNotFound = true;
  //         this.toastr.warning('Something went wrong.', 'Warning');
  //       }
  //     );
  // }

  // searchBar(event){
  //   console.log(event);
  //   const val = event.target.value.toLowerCase();
  //     if (val.length >= 3 || val.length == 0) {
  //       this.tempDisplayList = [...this.pricingList];
  //       const temp = this.tempDisplayList.filter(function (d) {
  //         return d.currency.toLowerCase().indexOf(val) !== -1 ||
  //           d.name.toLowerCase().indexOf(val) !== -1 ||
  //           d.amount.toLowerCase().indexOf(val) !== -1 ||
  //           !val;
  //       });
  //       this.tempDisplayList = [...temp];
  //     }
  // }

  // radioChange(event){
  //   console.log('Radio Change ==>',event);
  //   console.log('Radio Change ==>',this.addEditForm.isFree);
  //   // if (Number(event) === 1){
  //   //   this.dropdownSettings['disabled'] = true;
  //   // }else {
  //   //   this.dropdownSettings['disabled'] = false;
  //   // }
  //   // this.dropdownSettings = {...this.dropdownSettings};
  // }

  passEventToParent(action, ...args){
    this.noDataVal={
      margin:'mt-3',
      imageSrc: '../../../../../assets/images/no-data-bg.svg',
      title:"Sorry we couldn't find any matches please try again.",
      desc:"",
      titleShow:true,
      btnShow:false,
      descShow:false,
      btnText:'Learn More',
      btnLink:'',
    }
    const event = {
      'action': action,
      'argument': args,
    };
    // console.log('action ===>',action);
    console.log('event ===>', event);
    this.sendEventToParent.emit(event);
    if(action === 'clearSearch'){
      this.searchText = '';
    this.showSearch = false;
    }
  }

  // ngOnChanges(event){
  //   // console.log(event);
  //   // console.log(this.regulatoryEnrolConfig);
  //   if(event){
  //     this.radioChange(this.pricingConfig.addEditPriceForm.isFree)
  //     // this.passEventToParent('updateFormValuesRegulatory', this.pricingConfig.addEditPriceForm);
  //   }
  // }
}
