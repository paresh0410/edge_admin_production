import { Component, OnInit, Input, OnChanges, SimpleChanges, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-enrollment',
  templateUrl: './enrollment.component.html',
  styleUrls: ['./enrollment.component.scss']
})
export class EnrollmentComponent implements OnInit , OnChanges {

  @Input() config: any = {};
  @Output() sendEventToParent = new EventEmitter<any>();

  labels: any = [
    { labelname: 'NO', bindingProperty: 'number', componentType: 'text' },
    { labelname: 'FIRST NAME', bindingProperty: 'firstname', componentType: 'text' },
    { labelname: 'LAST NAME', bindingProperty: 'lastname', componentType: 'text' },
    { labelname: 'EMAIL', bindingProperty: 'email', componentType: 'text' },
    { labelname: 'COUNTRY', bindingProperty: 'country', componentType: 'text' },
  ];

  tableList1: any = [
    {"number": 7,"firstname": "Corina","lastname": "Carolin","email": "Corina.Carolin@gmail.com","country": "Guam"},
    {"number": 8,"firstname": "Jan","lastname": "Gower","email": "Jan.Gower@gmail.com","country": "Malta"},
    {"number": 9,"firstname": "Ezmeralda","lastname": "Worda","email": "Ezmeralda.@gmail.com","country": "Brazil"},
    {"number": 10,"firstname": "Elena","lastname": "Keelia","email": "Elena.Keelia@gmail.com","country": "Saint Lucia"},
  ];
  tableList2: any = [
    {"number": 3,"firstname": "Corina","lastname": "Carolin","email": "Corina.Carolin@gmail.com","country": "Guam"},
    {"number": 4,"firstname": "Elena","lastname": "Keelia","email": "Elena.Keelia@gmail.com","country": "Saint Lucia"},
    {"number": 5,"firstname": "Annabela","lastname": "Shelba","email": "Annabela.Shelba@gmail.com","country": "Switzerland"},
    {"number": 6,"firstname": "Fernande","lastname": "Garbe","email": "Fernande.Garbe@gmail.com","country": "Seychelles"},
    {"number": 10,"firstname": "Elena","lastname": "Keelia","email": "Elena.Keelia@gmail.com","country": "Saint Lucia"},
  ];
  tableList: any = [
    {"number": 3,"firstname": "Corina","lastname": "Carolin","email": "Corina.Carolin@gmail.com","country": "Guam"},
    {"number": 4,"firstname": "Elena","lastname": "Keelia","email": "Elena.Keelia@gmail.com","country": "Saint Lucia"},
    {"number": 5,"firstname": "Annabela","lastname": "Shelba","email": "Annabela.Shelba@gmail.com","country": "Switzerland"},
    {"number": 6,"firstname": "Fernande","lastname": "Garbe","email": "Fernande.Garbe@gmail.com","country": "Seychelles"},
    {"number": 10,"firstname": "Elena","lastname": "Keelia","email": "Elena.Keelia@gmail.com","country": "Saint Lucia"},
  ];

  constructor() { }

  ngOnInit() {
    console.log('enrollmentData ===>' , this.config);
    this.makeDisplayTabArray();
  }

  ngOnChanges(changes: SimpleChanges) {
    // changes.prop contains the old and the new value...
    console.log('enrollmentData ===>' , this.config);
  }

  passEventToParent(event){
    // const event = {
    //   'action': action,
    //   'params': params,
    // };
    // console.log('action ===>',action);
    console.log('event ===>', event);
    this.sendEventToParent.emit(event);
  }

  // doSomethingOnTabSelect(event){
  //   // console.log('event ===>', event);
  //   // const object = Object.keys(this.config);
  //   // console.log('Object to array', object);
  //   // console.log('selected', object[event]);
  //   // if(tabRef){
  //   //   console.log('selected', tabRef['tabs'][event].tabTitle)
  //   // }
  //   // console.log('tab Object', tabRef);
  //   console.log('selected', this.displayTabArray[event]);
  // }
  createDataForTabChange(event){
    let tabCurrent = this.displayTabArray[event];
    const object = {
      action : tabCurrent.identifer,
      argumnet : tabCurrent,
    }
    this.sendEventToParent.emit(object);
  }
  displayTabArray = [];
  makeDisplayTabArray(){
    const objectArray = Object.keys(this.config);
    for(let index = 0; index < objectArray.length; index++) {
      // const element = objectArray[index];
      if(this.config[objectArray[index]].show){
        const element = {};
        element['tabTitle'] = this.config[objectArray[index]].tabTitle;
        element['configName'] = objectArray[index];
        element['identifer'] = this.config[objectArray[index]].identifer;
        // element['configIndentifer'] = objectArray[index];
        this.displayTabArray.push(element);
      }
    }
    console.log('Display Tabs ==>', this.displayTabArray)
  }
}
