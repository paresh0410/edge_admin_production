import { Component, OnInit, EventEmitter, Output, Input, ViewChild } from "@angular/core";
import { noData } from "../../../models/no-data.model";

@Component({
  selector: "ngx-mannual",
  templateUrl: "./mannual.component.html",
  styleUrls: ["./mannual.component.scss"],
})
export class MannualComponent implements OnInit {
  @Output() sendEventToParent = new EventEmitter<any>();
  @ViewChild('name') input; 
  @Input() manulEnrolConfig: any = {};

  titleName: string = "Add Title";
  btnName: string = "Save";
  searchText: any;
  showSearch: boolean = false;
  labels: any = [
    { labelname: "NO", bindingProperty: "number", componentType: "text" },
    {
      labelname: "FIRST NAME",
      bindingProperty: "firstname",
      componentType: "text",
    },
    {
      labelname: "LAST NAME",
      bindingProperty: "lastname",
      componentType: "text",
    },
    { labelname: "EMAIL", bindingProperty: "email", componentType: "text" },
    { labelname: "COUNTRY", bindingProperty: "country", componentType: "text" },
  ];
  noDataVal:noData={
    margin:'mt-3',
    imageSrc: '../../../../../assets/images/no-data-bg.svg',
    title:"No Data Found.",
    desc:'To create batches in bulk click on upload button or to know more about bulk batch click on "Learn More"',
    titleShow:true,
    btnShow:false,
    descShow:false,
    btnText:'Learn More',
    btnLink:'https://faq.edgelearning.co.in/kb/learning-how-to-add-a-batch',
  };
  tableList: any = [
    {
      number: 1,
      firstname: "Ezmeralda",
      lastname: "Worda",
      email: "Ezmeralda.@gmail.com",
      country: "Brazil",
    },
    {
      number: 2,
      firstname: "Jan",
      lastname: "Gower",
      email: "Jan.Gower@gmail.com",
      country: "Malta",
    },
    {
      number: 3,
      firstname: "Corina",
      lastname: "Carolin",
      email: "Corina.Carolin@gmail.com",
      country: "Guam",
    },
    {
      number: 4,
      firstname: "Elena",
      lastname: "Keelia",
      email: "Elena.Keelia@gmail.com",
      country: "Saint Lucia",
    },
    {
      number: 5,
      firstname: "Annabela",
      lastname: "Shelba",
      email: "Annabela.Shelba@gmail.com",
      country: "Switzerland",
    },
    {
      number: 6,
      firstname: "Fernande",
      lastname: "Garbe",
      email: "Fernande.Garbe@gmail.com",
      country: "Seychelles",
    },
    {
      number: 7,
      firstname: "Corina",
      lastname: "Carolin",
      email: "Corina.Carolin@gmail.com",
      country: "Guam",
    },
    {
      number: 8,
      firstname: "Jan",
      lastname: "Gower",
      email: "Jan.Gower@gmail.com",
      country: "Malta",
    },
    {
      number: 9,
      firstname: "Ezmeralda",
      lastname: "Worda",
      email: "Ezmeralda.@gmail.com",
      country: "Brazil",
    },
    {
      number: 10,
      firstname: "Elena",
      lastname: "Keelia",
      email: "Elena.Keelia@gmail.com",
      country: "Saint Lucia",
    },
  ];

  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};

  constructor() {}

  ngOnInit() {
    // this.dropdownList = [
    //   { id: 1, itemName: "India" },
    //   { id: 2, itemName: "Singapore" },
    //   { id: 3, itemName: "Australia" },
    //   { id: 4, itemName: "Canada" },
    //   { id: 5, itemName: "South Korea" },
    //   { id: 6, itemName: "Germany" },
    //   { id: 7, itemName: "France" },
    //   { id: 8, itemName: "Russia" },
    //   { id: 9, itemName: "Italy" },
    //   { id: 10, itemName: "Sweden" },
    // ];
    // this.selectedItems = [
    //   { id: 2, itemName: "Singapore" },
    //   { id: 3, itemName: "Australia" },
    //   { id: 4, itemName: "Canada" },
    //   { id: 5, itemName: "South Korea" },
    // ];
    this.dropdownSettings = {
      badgeShowLimit: 1,
      lazyLoading: true,
      showCheckbox: true,
      escapeToClose: false,
      singleSelection: false,
      enableSearchFilter: true,
      // text: "Select Countries",
      selectAllText: "Select All",
      // noDataLabel: "No data found",
      unSelectAllText: "UnSelect All",
      classes: "common-multi",
      primaryKey: 'ecn',
      labelKey: 'fullname',
      noDataLabel: 'Search Users...',
      // enableSearchFilter: true,
      searchBy: ['ecn', 'fullname'],
    };
  }

  search() {
    this.showSearch = true;
    // this.searchClick.emit();
  }

  // clearSearch() {
  //   this.searchText = "";
  // }

  // searchBar() {
  //   console.log("mannual search = ", this.searchText);
  // }

  addBtn() {
    // this.sidebarForm = true;
  }

  // multi-select
  onItemSelect(item: any) {
    console.log(item);
    console.log(this.selectedItems);
  }
  OnItemDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedItems);
  }
  onSelectAll(items: any) {
    console.log(items);
  }
  onDeSelectAll(items: any) {
    console.log(items);
  }

  cloeSidebar() {}

  // visibilityTableRow(event){

  // }

  passEventToParent(action, ...args){
    this.noDataVal={
      margin:'mt-3',
      imageSrc: '../../../../../assets/images/no-data-bg.svg',
      title:"Sorry we couldn't find any matches please try again.",
      desc:"",
      titleShow:true,
      btnShow:false,
      descShow:false,
      btnText:'Learn More',
      btnLink:'',
    }
    if(action === 'clearesearch') {
      this.showSearch = false;
      this.input.nativeElement.value=''
    }
    const event = {
      'action': action,
      'argument': args,
    };
    // console.log('action ===>',action);
    console.log('event ===>', event);
    this.sendEventToParent.emit(event);
  }
}
