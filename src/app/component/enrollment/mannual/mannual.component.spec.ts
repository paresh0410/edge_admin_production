import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MannualComponent } from './mannual.component';

describe('MannualComponent', () => {
  let component: MannualComponent;
  let fixture: ComponentFixture<MannualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MannualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MannualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
