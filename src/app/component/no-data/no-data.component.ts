import { Component, OnInit, Input} from '@angular/core';
// import { Title } from '@angular/platform-browser';
import { noData } from '../../models/no-data.model';
@Component({
  selector: 'ngx-no-data',
  templateUrl: './no-data.component.html',
  styleUrls: ['./no-data.component.scss']
})
export class NoDataComponent implements OnInit {

  
  @Input() config: noData={
    imageSrc: 'assets/images/no-data-bg.svg',
    title:'',
    desc:'',
    margin:'mt-5',
    titleShow:false,
    btnShow:false,
    linkShow:false,
    descShow:false,
    link:'',
    linkTxt:'',
    btnText:'',
    btnLink:'',
  }
  constructor( ) { }
navigate(data){
  window.open(this.config.btnLink,'_blank')
}
  ngOnInit() {
  }
  
}

