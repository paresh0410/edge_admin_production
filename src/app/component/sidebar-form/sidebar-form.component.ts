import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'ngx-sidebar-form',
  templateUrl: './sidebar-form.component.html',
  styleUrls: ['./sidebar-form.component.scss']
})
export class SidebarFormComponent implements OnInit {
  @Output() outside = new EventEmitter<any>();
  @Output() closeBtn = new EventEmitter<any>();
  @Output() saveBtn = new EventEmitter<any>();
  @Output() saveBtn1 = new EventEmitter<any>();
  @Input() sidebarTitle: any;
  @Input() buttonName: any;
  @Input() buttonName1: any;
  @Input() saveDisabled: any = false;
  @Input() saveDisabled1: any = false;
  @Input() padding :string=''
  @Input() my_Class: any = "inner-from";

  

  selector: any;
  private body = document.querySelector('body');

  constructor() { }

  ngOnInit(): void {
    this.body.style.overflow = 'hidden';
  }

  save() {
    this.saveBtn.emit();
    this.body.style.overflow = 'inherit';
  }

  save1() {
    this.saveBtn1.emit();
    this.body.style.overflow = 'inherit';
  }

  outsideClick() {
    this.outside.emit();
    this.body.style.overflow = 'inherit';
  }

  close() {
    this.closeBtn.emit();
    this.body.style.overflow = 'inherit';
  }
}
