import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, ModuleWithProviders } from '@angular/core';
import { ThemeModule } from '../@theme/theme.module';
import { CommonModule } from '@angular/common';
import { DocumentViewerComponent } from './document-viewer/document-viewer/documentviewer.component';
import { DocumentViewerService } from './document-viewer/document-viewer/documentviewer.service';
import { DragChipsComponentComponent } from '../component/drag-chips-component/drag-chips-component.component';
import { SidemenuDrowerComponent } from './sidemenu-drower/sidemenu-drower.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableComponent } from './table/table.component';
 import { TableModalComponent } from './table-modal/table-modal.component';
 import { DemoMaterialModule } from '../pages/material-module';
import { CardComponent } from './card/card.component';
import { EnrolTableComponent } from './enrol-table/enrol-table.component';
import { ButtonComponent } from './enrol-table/button/button.component';
import { CheckboxComponent } from './enrol-table/checkbox/checkbox.component';
import { TextComponent } from './enrol-table/text/text.component';
import { MobileComponent } from './enrol-table/mobile/mobile.component';
import { DateComponent } from './enrol-table/date/date.component';
import { EditButtonComponent } from './enrol-table/edit-button/edit-button.component';
import { ParentCardComponent } from './parent-card/parent-card.component';
import { TruncateModule } from 'ng2-truncate';
import { SubheaderComponent } from '../pages/components/subheader/subheader.component';
import { DeleteComponent } from './enrol-table/delete/delete.component';
import { DuplicateComponent } from './enrol-table/duplicate/duplicate.component';
import { DownloadComponent } from './enrol-table/download/download.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { HtmlComponent } from './enrol-table/html/html.component';
import { AddEditActivityComponent } from '../pages/plan/add-edit-activity/add-edit-activity.component';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
// import { ModeComponent } from './enrol-table/mode/mode.component';
// import { FmReportComponent } from './fm-report/fm-report.component';
//import { SideMenuFilterComponent } from './side-menu-filter/side-menu-filter.component';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { QuillModule } from 'ngx-quill';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { DndDirective } from './file-upload/dnd.directive';
import { NoDataComponent } from './no-data/no-data.component';

import { AddEditModulesComponent } from '../pages/plan/add-edit-modules/add-edit-modules.component';
import { ModuleActivityListComponent } from '../pages/plan/module-activity-list/module-activity-list.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { SidebarFormComponent } from './sidebar-form/sidebar-form.component';

import { EnrollmentComponent } from './enrollment/enrollment.component';
// import { AdminPanelComponent } from './admin-panel.component';
import { MannualComponent } from './enrollment/mannual/mannual.component';
import { RuleBasedComponent } from './enrollment/rule-based/rule-based.component';
import { ModalPopUpComponent } from './modal-pop-up/modal-pop-up.component';
import { RegulatoryComponent } from './enrollment/regulatory/regulatory.component';
import { SelfComponent } from './enrollment/self/self.component';
import { PricingComponent } from './enrollment/pricing/pricing.component';
import { FilterComponent } from './filter/filter.component';
import { TabsModule } from 'ngx-tabset';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { InfoTooltipComponent } from './info-tooltip/info-tooltip.component';
import { TreeComponent } from '../pages/components/tree/tree.component';
import { ActiveUserComponent } from './widgets/active-user/active-user.component';
import { TotalUserComponent } from './widgets/total-user/total-user.component';
import { TotalCoursesComponent } from './widgets/total-courses/total-courses.component';
import { TotalCourseCompletionComponent } from './widgets/total-course-completion/total-course-completion.component';
// import { GridstackModule } from './gridstack/gridstack.module';
import { GridstackComponent } from './gridstack/components/gridstack.component';
import { GridstackItemComponent } from './gridstack/components/gridstack-item.component';
import { GridstackService } from './gridstack/services/gridstack.service';
import { SkeletonComponent } from './skeleton/skeleton.component';
import { MultilanguagePopupComponent } from './multilanguage-popup/multilanguage-popup.component';
import { PriceComponent } from './enrol-table/price/price.component';
import { BulkCopyComponent } from './bulk-copy/bulk-copy.component';
import { BulkUploadTreeComponent } from './bulk-upload-tree/bulk-upload-tree.component';
import { JoditAngularModule } from 'jodit-angular';
@NgModule({
  imports: [
    CommonModule,
    DemoMaterialModule,
    FormsModule,
    TruncateModule,
    NgxPaginationModule,
    AngularMultiSelectModule,
    QuillModule,
    TimepickerModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    TabsModule,
    BsDatepickerModule,
    ReactiveFormsModule,
    JoditAngularModule,
    // GridstackModule.forRoot(),
  ],
  declarations: [
    DocumentViewerComponent ,
    DragChipsComponentComponent,
    SidemenuDrowerComponent,
    TableComponent,
    TableModalComponent,
    CardComponent,
    EnrolTableComponent,
    ButtonComponent,
    CheckboxComponent,
    TextComponent,
    MobileComponent,
    TreeComponent,
    DateComponent,
    EditButtonComponent,
    ParentCardComponent,
    SubheaderComponent,
    DeleteComponent,
    DuplicateComponent,
    DownloadComponent,
    HtmlComponent,
    // ModeComponent,
    // FmReportComponent,
   // SideMenuFilterComponent
   FileUploadComponent,
   AddEditActivityComponent,
   DndDirective,
   NoDataComponent,
   AddEditModulesComponent,
   ModuleActivityListComponent,
   SidebarFormComponent,
   EnrollmentComponent,
  // AdminPanelComponent,
    MannualComponent,
    RuleBasedComponent,
    ModalPopUpComponent,
    RegulatoryComponent,
    SelfComponent,
    PricingComponent,
    InfoTooltipComponent,
    FilterComponent,
    ActiveUserComponent,
    TotalUserComponent,
    TotalCoursesComponent,
    TotalCourseCompletionComponent,
    GridstackComponent,
    GridstackItemComponent,
    SkeletonComponent,
    MultilanguagePopupComponent,
    PriceComponent,
    BulkCopyComponent,
    BulkUploadTreeComponent,
  ],
  providers: [
    DocumentViewerService,
    GridstackService,
  ],
  exports: [
    DragChipsComponentComponent,
    SidemenuDrowerComponent,
    TableComponent,
    TableModalComponent,
    CardComponent,
    EnrolTableComponent,
    ParentCardComponent,
    SubheaderComponent,
    AddEditActivityComponent,
    FileUploadComponent,
    TreeComponent,
    // DocumentViewerService,
    DndDirective,
    NoDataComponent,
    AddEditModulesComponent,
    ModuleActivityListComponent,
    SidebarFormComponent,
    EnrollmentComponent,
    // AdminPanelComponent,
      MannualComponent,
      RuleBasedComponent,
      ModalPopUpComponent,
      RegulatoryComponent,
      SelfComponent,
      PricingComponent,
      InfoTooltipComponent,
      FilterComponent,
      ActiveUserComponent,
    TotalUserComponent,
    TotalCoursesComponent,
    TotalCourseCompletionComponent,
    GridstackComponent,
    GridstackItemComponent,
    SkeletonComponent,
    MultilanguagePopupComponent,
    BulkCopyComponent,
  ],
  entryComponents: [
    TableComponent,
    TableModalComponent,
    CardComponent,
    EnrolTableComponent,
    ParentCardComponent,
    AddEditActivityComponent,
    FileUploadComponent,
    TreeComponent,
    // DndDirective,
    NoDataComponent,
    SidebarFormComponent,
    FilterComponent,
    ActiveUserComponent,
    TotalUserComponent,
    TotalCoursesComponent,
    TotalCourseCompletionComponent,
    SkeletonComponent,
    MultilanguagePopupComponent,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA ,
  ],
})
export class ComponentModule {
  static forRoot() :ModuleWithProviders {
    return {
      ngModule: ComponentModule,
      //providers: [ AuthService ]
    }
  }
}
