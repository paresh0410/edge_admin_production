import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DragChipsComponentComponent } from './drag-chips-component.component';

describe('DragChipsComponentComponent', () => {
  let component: DragChipsComponentComponent;
  let fixture: ComponentFixture<DragChipsComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DragChipsComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DragChipsComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
