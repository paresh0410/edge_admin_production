import { Component, OnInit, Input , AfterViewInit} from '@angular/core';
import * as $ from 'jquery';
import 'jquery-ui-dist/jquery-ui';
// import * as rangy from 'rangy';
// import 'rangy/lib/rangy-selectionsaverestore';

declare var rangy;
@Component({
  selector: 'ngx-drag-chips-component',
  templateUrl: './drag-chips-component.component.html',
  styleUrls: ['./drag-chips-component.component.scss']
})
export class DragChipsComponentComponent implements OnInit , AfterViewInit {

  @Input('inputTags') inputTags: any = [];
  @Input('vertical') vertical = false;
  constructor() { }
  tagsArray = [
    {
      id: 0 ,
      name: 'PHP'
    },
    {
      id:1 ,
      name: 'Javascript'
    },
    {
      id: 2 ,
      name: 'Javascript'
    },
    {
      id: 3 ,
      name: 'HTML'
    },
    {
      id: 4 ,
      name: ' C'
    },
    {
      id: 5 ,
      name: 'C++'
    },
    {
      id: 6 ,
      name: 'Python'
    },
    {
      id: 7 ,
      name: 'C++'
    },
    {
      id: 8 ,
      name: 'Ruby'
    },
    {
      id: 9,
      name: 'Perl',
    },
  ];

  ngOnInit() {
    console.log('Input Tags passed ==>', this.inputTags);
    // _rangy = rangy;
  }

  ngAfterViewInit() {
    // ...
    const _typescriptThis = this;
    $(document).ready(function(){
      'use strict';
      // $('#reset').on('click', function(){
      //   $('ul').attr('id', 'draggable');
      //   $('.dropped_content').removeAttr('disabled', 'disabled');
      //   $('.dropped_content').val('');
      // });
      $('li').mouseover(function(){
        $(this).css('cursor', 'pointer');
      });
      $( '#draggable li').draggable({helper: 'clone'});
      $('.dropped_content').triggerHandler('change');
      // $(angularThis.receivedClass).droppable({
      //   accept: '#draggable li',
      //   drop: function(ev, ui) {
      //   $(this).insertAtCaret('#' + ui.draggable.text().trim() + ' ');
      //   // $(this).trigger('change');
      //   // $(this).attr('disabled', 'disabled');
      //   // $("#draggable").removeAttr('id');
      //   }
      // });
      $('.dropped_content').droppable({
        accept: '#draggable li',
        drop: function(ev, ui) {
        $(this).insertAtCaret('@' + ui.draggable.text().trim() + ' ');
        // $(this).trigger('change');
        // $(this).attr('disabled', 'disabled');
        // $("#draggable").removeAttr('id');
        }
      });
      $('.dropped_content .jodit_wysiwyg').droppable({
        accept: '#draggable li',
        drop: function(ev, ui) {
        $(this).insertAtCaretEditor('@' + ui.draggable.text().trim() + ' ');
        // $(this).trigger('change');
        // $(this).attr('disabled', 'disabled');
        // $("#draggable").removeAttr('id');
        }
      });
    });

   $.fn.insertAtCaret = function (myValue) {
    return this.each(function(){
    // if (document.selection) {
    //   this.focus();
    //   sel = document.selection.createRange();
    //   sel.text = myValue;
    //   this.focus();
    // }

    if (this.selectionStart || this.selectionStart == '0') {
      var startPos = this.selectionStart;
      var endPos = this.selectionEnd;
      var scrollTop = this.scrollTop;
      console.log($(this));
      console.log(this);
      // this.

      this.value = this.value.substring(0, startPos) + myValue + this.value.substring(endPos, this.value.length);
      // angular.element($(this)).triggerHandler('change');
      // $(this).triggerHandler('change');
      // $(this).trigger('input');
        // this.value.$validate();
        // this.value.$render();
      // this.attributes[3].value = this.value.substring(0, startPos) + myValue + this.value.substring(endPos, this.value.length);
      this.focus();
      this.selectionStart = startPos + myValue.length;
      this.selectionEnd = startPos + myValue.length;
      this.scrollTop = scrollTop;
    } else {
      // this.value = this.value + '# ' + myValue;
      this.focus();
    }
    const collection = document.getElementsByClassName('dropped_content');
    Array.from(collection).forEach(element =>{
      element.dispatchEvent(new Event('input'));
    });
    });

    // document.getElementsByClassName('dropped_content ')[1].dispatchEvent(new Event('input'));
  };
  $.fn.insertAtCaretEditor = function (myValue) {
    return this.each(function(){
    // if (document.selection) {
    //   this.focus();
    //   sel = document.selection.createRange();
    //   sel.text = myValue;
    //   this.focus();
    // }
    const selectionStart = rangy.getSelection().saveCharacterRanges(this);
    console.log('Rangy Selection ==>', selectionStart);
    if (selectionStart.length !== 0) {
      // let position = findPositionInHTML()
      let startPos = selectionStart[0].characterRange.start;
      let endPos = selectionStart[0].characterRange.end;
      if(startPos !== endPos){
        const FinalPosition = _typescriptThis.findPositionInHTML(startPos, endPos, this.innerText , this.innerHTML);
        const EndPosition = FinalPosition + (endPos - startPos);
        // var scrollTop = this.scrollTop;
        // this.innerHTML = this.innerHTML.substring(0, FinalPosition)+ myValue + this.innerHTML.substring(EndPosition,this.innerHTML.length);
        this.innerHTML = this.innerHTML.substring(0, FinalPosition)+ myValue + this.innerHTML.substring(EndPosition,this.innerHTML.length);
        // this.focus();
      }
      if(startPos <= 0){
        this.innerHTML = myValue + this.innerHTML;
      }else if(endPos >= this.innerText.length){
        this.innerHTML = this.innerHTML + myValue ;
      }else{
        this.innerHTML = this.innerHTML + myValue ;
        // this.selectionStart = startPos + myValue.length;
        // this.selectionEnd = startPos + myValue.length;
        // this.scrollTop = scrollTop;

      }
    } else {
      this.innerHTML = this.innerHTML + myValue;
      this.focus();
    }
    });
  };
  }

  findPositionInHTML(start, end, context , htmlText ){
    if(start !== end){
      const word = context.substring(start, end);
      console.log('Selected Word ===>', word);
      const positionOccuranceArray = [];
      let index = htmlText.indexOf(word);
      while (index >= 0) {
          console.log('Index : ', index);
          positionOccuranceArray.push(index);
          index = htmlText.indexOf(word, index + word.length);
      }
      // for (let i = 0; index < positionOccuranceArray.length; i++) {
      //   // const element = array[index];
      //   if
      // }
      if(positionOccuranceArray.length  >= 2){
        const position = positionOccuranceArray.reduce(function(prev, curr) {
          return (Math.abs(curr - start) < Math.abs(prev - start) ? curr : prev);
        });
        return position;
      }
      if(positionOccuranceArray.length == 1){
        return positionOccuranceArray[0];
      }


    }
    }
}
