import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultilanguagePopupComponent } from './multilanguage-popup.component';

describe('MultilanguagePopupComponent', () => {
  let component: MultilanguagePopupComponent;
  let fixture: ComponentFixture<MultilanguagePopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultilanguagePopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultilanguagePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
