import { Component, OnInit, Input, Output, EventEmitter, OnChanges, ChangeDetectorRef } from "@angular/core";
import { noData } from "../../models/no-data.model";
import { ToastrService } from 'ngx-toastr';
import * as _ from "lodash";

@Component({
  selector: "ngx-multilanguage-popup",
  templateUrl: "./multilanguage-popup.component.html",
  styleUrls: ["./multilanguage-popup.component.scss"],
})
export class MultilanguagePopupComponent implements OnInit, OnChanges {
  labels: any = [
    { labelname: "", bindingProperty: "", componentType: "checkbox" },
    { labelname: "Name", bindingProperty: "assetName", componentType: "text" },
    {
      labelname: "Description",
      bindingProperty: "description",
      componentType: "text",
    },
    {
      labelname: "Language",
      bindingProperty: "langName",
      componentType: "text",
    },
    {
      labelname: "Created By",
      bindingProperty: "createdBy",
      componentType: "text",
    },
    {
      labelname: "Created On",
      bindingProperty: "createdOn",
      componentType: "date",
    },
  ];
  customWidthSetting = {
    enabled: true,
    width: '750px',
  };
  @Input() damAssetsList: any = [];
  @Input() languageList: any = [];
  @Output() sendEventToParent = new EventEmitter<any>();
  @Input() selectedList: any = [];
  @Input() noDataFoundActivitySearch: any ;
  popupListContent = [];
  selectedItem: any;
  noAssetFound = false;
  noDataVal: noData = {
    margin: "mt-3",
    imageSrc: "assets/images/no-data-bg.svg",
    title: "No assets found.",
    desc: "",
    titleShow: true,
    btnShow: false,
    descShow: false,
    btnText: "Learn More",
    btnLink: "https://faq.edgelearning.co.in/kb/how-to-add-an-asset-in-the-dam",
  };
  constructor(private toastr: ToastrService, public cdf: ChangeDetectorRef,) {}

  ngOnInit() {

  }

  onSelectSingleItem(event) {
    // event["checked"] = !event["checked"];
    this.selectedItem = event;
    if (event["checked"]) {

      let langugageAlreadySelected = true;
      for (let j = 0; j < this.languageList.length; j++) {
        if ((this.selectedItem["langId"] === this.languageList[j]["languageId"] )&&
          !this.languageList[j]["selected"]
        ) {
          this.languageList[j]["selected"] = true;
          langugageAlreadySelected = false;
          break;
        }
      }
      if (langugageAlreadySelected) {
        const list = [...this.damAssetsList];
        for (let i = 0; i < list.length; i++) {
          if (list[i]['id'] === event['id']){
            list[i]['checked'] = false;
            const message = `${list[i]['langName']} is already selected`;
            this.toastr.warning(message, 'Warning');
          }
        }
        this.damAssetsList = [];
        this.damAssetsList = _.cloneDeepWith(list);
      }
    } else {
      for (let j = 0; j < this.languageList.length; j++) {
        if (
          this.selectedItem["langId"] ===
            this.languageList[j]["languageId"] &&
          this.languageList[j]["selected"]
        ) {
          this.languageList[j]["selected"] = false;
          break;
        }
      }
    }
    this.checkAllFlagSelected();
    console.log("onSelectSingleItem", event);
  }

  selectedItemList(event) {
    console.log("selectedItemList", event);
    this.popupListContent = event;
    // if(event){
    //   for(let i = 0; i < this.damAssetsList.length; i++){
    //     for(let j = 0; i < this.languageList.length; j++){
    //       if(this.damAssetsList[i]['checked']
    //           && (this.damAssetsList[i]['language'] === this.languageList[j]['languageName'])
    //         && !this.languageList[j]['selected']){
    //           this.selectedList.push()
    //       }else {

    //       }
    //     }
    //   }
    // }
  }


  searchtext = '';
  rows = [];


  passEventToParent(action, ...args){
    // if(action === 'addEditRule'){
    //   this.addBtn();
    // }
    const event = {
      'action': action,
      'argument': args,
    };
    // console.log('action ===>',action);
    console.log('event ===>', event);

    if(action === 'clearSearch'){
      this.searchtext = '';
      this.resetListToDefault();
      this.customCheckAllFlag = false;
      this.cdf.detectChanges();
      event[1] = this.languageList;

    }
    if(action === 'searchAsset'){
      this.resetListToDefault();
      this.customCheckAllFlag = false;
      this.cdf.detectChanges();
      event[1] = this.languageList;
    }
    this.sendEventToParent.emit(event);
  }

  setSelectedListContent(){
    if(this.popupListContent && this.popupListContent.length !== 0){
      // this.selectedList.concat(this.popupListContent);
      for (let i = 0; i < this.popupListContent.length; i++) {
        this.popupListContent[i]['cmId'] = 0;
        this.popupListContent[i]["presentInDB"] = false;
      }
      Array.prototype.push.apply(this.selectedList, this.popupListContent);
    }
  }

  resetListToDefault(){
    for (let index = 0; index < this.languageList.length; index++) {
      this.languageList[index]["selected"] = false;
    }
     for (
        let indexj = 0;
        indexj < this.selectedList.length;
        indexj++
      ) {
        const item = this.selectedList[indexj];
        for (let index = 0; index < this.languageList.length; index++) {
          if (
            Number(item["langId"]) ===
            Number(this.languageList[index]["languageId"])
          ) {
            this.languageList[index]["selected"] = true;
            item["langName"] = this.languageList[index]["languageName"];
            break;
          }
        }
      }
  }

  ngOnChanges(){
    console.log("selectedItemList", this.noDataFoundActivitySearch.message);
    if(this.noDataFoundActivitySearch.errorKey === 'noData'){
      this.noDataVal.imageSrc = 'assets/images/no-data-bg.svg';
    }else {
      this.noDataVal.imageSrc = '';
    }
    this.noDataVal.title = this.noDataFoundActivitySearch.message;
  }
  customCheckAllFlag = false;
  checkAllSelected(event){
    console.log('checkAllSelected', event);
    if (event) {
      const selectedLanguageIds = this.damAssetsList.map(item => item.langId);
      console.log('selectedLanguageIds', selectedLanguageIds);
      const hasDuplicate = selectedLanguageIds.some((val, i) => selectedLanguageIds.indexOf(val) !== i);
      console.log('hasDuplicate', hasDuplicate);
      const hasDuplicate2 =  (new Set(selectedLanguageIds)).size !== selectedLanguageIds.length;
      console.log('hasDuplicate2', hasDuplicate2);
      if(hasDuplicate){
        this.customCheckAllFlag = null;
        setTimeout(() =>{
            this.customCheckAllFlag = false;
            this.cdf.detectChanges();
        })
        this.toastr.warning('Asset list contain duplicate languages', 'Warning');
        return null;
      }
      const remainingLanguageIds = this.languageList.filter(item => {
        if(!item['selected']){
          return item.languageId;
        }
      }).map(item => item.languageId);
      console.log('remainingLanguageIds', remainingLanguageIds);
      const checkIfLanguageExist = selectedLanguageIds.every(val => remainingLanguageIds.includes(val));
      if(!checkIfLanguageExist){
        this.customCheckAllFlag = null;
        setTimeout(() =>{
          this.customCheckAllFlag = false;
          this.cdf.detectChanges();
        })

        this.toastr.warning('Asset list contain language(s) which are already selected', 'Warning');
        return null;
      }
      this.selectDeselectLanguage(event);
      console.log('checkIfLanguageExist', checkIfLanguageExist);
    }else {
      this.selectDeselectLanguage(event);
    }
  }

  selectDeselectLanguage(event){
    for (let index = 0; index < this.damAssetsList.length; index++) {
      const element = this.damAssetsList[index];
      element["checked"] = event;
      this.onSelectSingleItem(element);
    }
    if(event){
      this.popupListContent = this.damAssetsList;
    }else {
      this.popupListContent = [];
    }
  }

  checkAllFlagSelected(){
    let selectedAll = true;
    for (let index = 0; index < this.damAssetsList.length; index++) {
      // const element = this.damAssetsList[index];
      // element["checked"] = event;
      // this.onSelectSingleItem(element);
      if(!this.damAssetsList[index]["checked"]){
        selectedAll = false;
        break;
      }
    }
    this.customCheckAllFlag = selectedAll;
    this.cdf.detectChanges();
  }
}
