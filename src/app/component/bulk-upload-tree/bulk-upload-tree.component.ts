import {
  Component,
  OnInit,
  Input,
  EventEmitter,
  Output,
  ViewChild,
  OnChanges,
  SimpleChanges,
} from "@angular/core";
import { AssetService } from "../../pages/dam/asset/asset.service";
import { Router } from "@angular/router";
import { FolderFileComponent } from "../../pages/dam/folder-file/folder-file.component";

@Component({
  selector: 'ngx-bulk-upload-tree',
  templateUrl: './bulk-upload-tree.component.html',
  styleUrls: ['./bulk-upload-tree.component.scss']
})
export class BulkUploadTreeComponent implements OnInit, OnChanges {
  @Input() trees;

  @Input() selectedSession;

  @Input() showCheckedBox = false;

  @Output() getUpdatedCourseList = new EventEmitter<any>();
  // @Output() openMainFolder = new EventEmitter<any>();
  // @Output() bread = new EventEmitter<any>();

  @Output() getCheckedFolder = new EventEmitter<any>();
  @Input() invalidArray = [];

  @ViewChild("Input") file: FolderFileComponent;

  demTree: any = [];

  constructor(private assetservice: AssetService, private router: Router) {}

  ngOnInit() {

    console.log('trees', this.trees);
  }

  folderClicked = null;
  openFolder(folder, value) {

    folder["expand"] = value;

  }
  // getFolder(tree) {

  //   this.getFolders.emit(tree);
  // }

  ngOnChanges(changes: SimpleChanges) {

  }

  checkAllChild(event , folder){
    // if(event != folder["checkBoxChecked"]){
    //   const value = !folder["checkBoxChecked"];
    // }
    // folder["checkBoxChecked"] = event;
   const  value = event;
    console.log(folder, "folder");
    if(folder['children'] && folder['children'].length !=0){
      if(this.selectedSession != '' || this.selectedSession){
        if (this.selectedSession <= folder.children.length){
          if(value){
            folder.children[this.selectedSession - 1]['selected'] = value;
          }else {
            for(let i = 0; i < folder.children.length ; i ++){
              if (Number(this.selectedSession) == (i + 1)){
                folder.children[i]['selected'] = value;
              }else {
                folder.children[i]['selected'] = false;
              }
            }
          }
          folder['childSelected'] = event;
        }else {
          folder['childSelected'] = false;
          if(value){
            this.invalidArray.push(folder);
          }else {
            if(this.invalidArray && this.invalidArray.length !== 0){
              for(let i = 0; i < this.invalidArray.length ; i ++){
                if(this.invalidArray[i]['id'] === folder['id']){
                  this.invalidArray.splice(i, 1);
                }
            }
          }
        }
        }
      }else {
        for(let i = 0; i < folder.children.length ; i ++){
          // if(Number(this.selectedSession) == i){
            folder.children[i]['selected'] = value;
          // }
        }
      }

      this.openFolder(folder, value);
    }else {

    }
    console.log(' this.invalidArray',  this.invalidArray);
    event = {
      trees: this.trees,
      invalidArray: this.invalidArray,
    }
    this.getUpdatedCourseList.emit(this.trees);
    // this.getCheckedFolder.emit(folder);
  }

  checkChild(event , folder, item){
    item['selected'] = event;
    if(this.selectedSession == '' || !this.selectedSession){
      let allSelected = true;
      let selectedOne = false;
      if(folder['children'] && folder['children'].length !=0){
        for(let i = 0; i < folder.children.length ; i ++){
          if(folder.children[i]['selected']){
            selectedOne = true;
          }else {
            allSelected = false;
          }
        }
        folder['childSelected'] = selectedOne;
        // folder['selected'] = allSelected;
        folder['selected'] = selectedOne;
      }
    }else {
      let allSelected = true;
      let selectedOne = false;
      if(folder['children'] && folder['children'].length !=0){
        for(let i = 0; i < folder.children.length ; i ++){
          if(folder.children[i]['selected']){
            selectedOne = true;
          }else {
            allSelected = false;
          }
        }
        folder['childSelected'] = selectedOne;
        // folder['selected'] = allSelected;
        folder['selected'] = selectedOne;
      }
    }

    console.log('folder', folder);
    console.log('folder', this.trees);
    this.getUpdatedCourseList.emit(this.trees);
  }


}
