import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkUploadTreeComponent } from './bulk-upload-tree.component';

describe('BulkUploadTreeComponent', () => {
  let component: BulkUploadTreeComponent;
  let fixture: ComponentFixture<BulkUploadTreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BulkUploadTreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkUploadTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
