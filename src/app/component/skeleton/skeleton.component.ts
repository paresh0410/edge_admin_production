import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-skeleton',
  templateUrl: './skeleton.component.html',
  styleUrls: ['./skeleton.component.scss']
})
export class SkeletonComponent implements OnInit {
  @Input() count: any;

  constructor() { }

  ngOnInit() {
  }

  skeletons(n: number): any[] {
    return Array(n);
  }
}
