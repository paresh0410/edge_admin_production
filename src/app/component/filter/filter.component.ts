import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { Filter } from '../../models/filter.modal';
// import { ToastrService } from 'ngx-toastr';
import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';
import * as _ from "lodash";

export const MY_CUSTOM_FORMATS = {
  fullPickerInput: 'DD-MM-YYYY HH:mm:ss',
  parseInput: 'DD-MM-YYYY HH:mm:ss',
  datePickerInput: 'DD-MM-YYYY',
  timePickerInput: 'LT',
  monthYearLabel: 'MMM YYYY',
  dateA11yLabel: 'LL',
  monthYearA11yLabel: 'MMMM YYYY'
};
@Component({
  selector: 'ngx-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
  providers: [
    {provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE]},
    {provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS},]
})
export class FilterComponent implements OnInit, OnChanges {
  showinnerTag: boolean = false;
  fullHeight: boolean = false;
  showdateRange: boolean = false;
  isChecked: boolean = true;

  @Input() courseTag: any;
  @Input() filterInnertag: any;
  @Input() cacheFilter: any;
  @Input() filterConfig: Filter = {
    ascending: false,
    descending: false,
    showDropdown: false,
    dropdownlabel: '',
    dropdownList: []
  };
  searchText:any = ''
  @Input() clearData?: boolean;
  filteredStore: any = {};
  filteredCount: any = {};
  filteredData: any = {};
  filterValueData: any = {};
  tempcoursetag: any;
  previousItem: any;
  @Output() changeFilterData: EventEmitter<any> = new EventEmitter();
  @Output() dropdownClick: EventEmitter<any> = new EventEmitter();


  selectedLevel:any = 4
  selectedRadio:any = 1
  dupFilterInnerTag: any;
  filterIndex: any;
  dummy: any = [];
  dummyCourse: any = [];

  // selectedLevel = this.levels[0];

  constructor(
    // public toast: ToastrService
    ) {
      if(this.courseTag){
        this.dummyCourse = _.cloneDeep(this.courseTag)
      }
    }

  ngOnInit() {
    console.log('courseTag', this.courseTag);
    console.log('filterInnertag', this.filterInnertag);
    console.log('filterConfig', this.filterConfig);
    const tempfilterdata = this.courseTag;
    this.tempcoursetag = tempfilterdata;
    if (this.courseTag && this.courseTag.length > 0) {
      this.filterInnertag = this.courseTag[0].list;
      this.dupFilterInnerTag = this.courseTag[0].list;
    console.log(this.dupFilterInnerTag,"dupp1")
      this.courseTag.forEach((value, key) => {
        const filterValue = value.filterValue;
        const filterId = value.filterValue;
        // tslint:disable-next-line:max-line-length
        const tagname = value.filterId !== undefined ? String(value.filterId).toLowerCase() : String(value.tagname).toLowerCase();
        if (this.cacheFilter && this.cacheFilter[filterValue] && this.cacheFilter[filterValue].length > 0) {
          value.count = this.cacheFilter[filterValue].length;
          this.filteredData[filterId] = this.cacheFilter[filterValue];
          this.filterValueData[filterValue] = this.cacheFilter[filterValue];
          this.filteredStore[filterId] = this.cacheFilter[filterValue];
        }
        this.filteredCount[tagname] = key;
      });
    } else {
      this.filterInnertag = [];
      this.dupFilterInnerTag = []
      this.courseTag = [];
    }
    if (this.clearData) {
      this.clearalltag();
    }
  }

  changeTobtn(event, item) {
    // this.courseTag = this.dummyCourse
    // this.filterInnertag = this.dummy
    this.courseTag.forEach((value, key) => {
      const filterValue = value.filterValue;
      // tslint:disable-next-line:max-line-length
      const filterId = value.filterId !== undefined ? String(value.filterId).toLowerCase() : String(value.tagname).toLowerCase();
      if (filterId === String(event.target.value).toLowerCase()) {
        value.isChecked = true;
        if (item.type === 'date') {
          if (this.filteredStore[filterId]
          && this.filteredStore[filterId].length > 0) {
            // item.value = this.filteredStore[String(value.tagname).toLowerCase()];
              item.value = value['value'];
          } else {
            if (this.cacheFilter && this.cacheFilter[filterValue] && this.cacheFilter[filterValue] &&
              this.cacheFilter[filterValue].length > 0) {
                item.value = this.cacheFilter[filterValue];
                this.filteredStore[filterId] = item.value;
            } else {
              item.value = [];
            }
          }
        } else {
          if (this.filteredStore[filterId]
          && this.filteredStore[filterId].length > 0) {
            this.filterInnertag = this.filteredStore[filterId];
            this.dupFilterInnerTag = this.filteredStore[filterId];
            // this.dummy = _.cloneDeep(this.filteredStore[filterId])
            // this.dummy = this.dummy
            console.log(this.dummy,"dupp")

          } else {
            if (this.cacheFilter && this.cacheFilter[filterValue] && this.cacheFilter[filterValue] &&
              this.cacheFilter[filterValue].length > 0) {
              let itemname = '';
              const findElement = (element) => element.itemName === itemname;
              this.cacheFilter[value.filterValue].forEach( (element1) => {
                itemname = element1.itemName;
                const indexnumber = value.list.findIndex(findElement);
                value.list[indexnumber]['isSelected'] = true;
              });
            }
            this.filterInnertag = value.list;
            this.dupFilterInnerTag = value.list;
            this.dummy = _.cloneDeep(value.list);

            console.log(this.dupFilterInnerTag,"dupp")
            this.filteredStore[filterId] = value.list;
          }
        }
      } else {
        value.isChecked = false;
      }
    });
    if (item.type === 'date') {
      this.showinnerTag = false;
    } else {
      this.showinnerTag = true;
    }

    //new code
    this.inputSearch(this.searchText)
    //end new code
  }

  checkChange(index, item, event) {
    // this.clickToCheck.emit(this.isChecked);
    console.log(this.dupFilterInnerTag, "dupp")
    const filterValue = item.filterValue;
    // tslint:disable-next-line:max-line-length
    const filterId = item.filterId !== undefined ? String(item.filterId).toLowerCase() : String(item.tagname).toLowerCase();
    this.filteredStore[filterId] = this.filterInnertag;
    let indexnumber;
    if (item.type === 'date') {
      this.filteredData[filterId] = event.value;
      this.filterValueData[filterValue] = event.value;

      //changes neha
      if (this.filterValueData[filterValue]) {
        for (let i = 0; i < this.filterValueData[filterValue].length; i++) {
          this.filterValueData[filterValue][i] = this.formatDigitDate(this.filterValueData[filterValue][i])
        }
      }
      //end changes
      this.courseTag[this.filteredCount[filterId]]['count'] = 1;
    } else {
      const innerTag = this.filterInnertag;
      item.isSelected = !item.isSelected;
      if (this.filteredData[filterId] && this.filteredData[filterId].length > 0) {
        // indexnumber = this.filteredData[filterId].findIndex((value) => {
        //   console.log(value,item,"values")
        //   String(value.tagName).toLowerCase() === String(item.tagName).toLowerCase()
        // })
        if (this.filterValueData[filterValue] && this.filterValueData[filterValue].length === 0) {
          this.filterValueData[filterValue] = [];
        }
        if (item.singleSelection) {
          this.filteredData[filterId] = [];
          this.filterValueData[filterValue] = [];
          if (item.isSelected) {
            this.filterInnertag.forEach((value, key) => {
              value.isSelected = false;
            });
            item.isSelected = !item.isSelected;
            this.filteredData[filterId].push(item);
            this.filterValueData[filterValue].push(item);
          }
        } else {
          const findElement = (element) => element.tagName === item.tagName;
          indexnumber = this.filteredData[filterId].findIndex(findElement);
          if (item.isSelected) {
            this.filteredData[filterId].push(item);
            this.filterValueData[filterValue].push(item);
          } else {
            this.filteredData[filterId].splice(indexnumber, 1);
            this.filterValueData[filterValue].splice(indexnumber, 1);
          }
        }
      } else {
        this.filteredData[filterId] = [];
        this.filteredData[filterId].push(item);
        this.filterValueData[filterValue] = [];
        this.filterValueData[filterValue].push(item);
      }
      if (String(this.filteredCount[filterId]) === 'undefined') {
        this.filteredCount[filterId] = 0;
        this.courseTag[this.filteredCount[filterId]]['count'] = this.filteredData[filterId].length;
      } else {
        // this.filteredCount[filterId] = 0;
        this.courseTag[this.filteredCount[filterId]]['count'] = this.filteredData[filterId].length;
      }
    }
    const filteredCount = this.getFilterCount();
    this.filterValueData['filteredCount'] = filteredCount;
    const errorObj = this.checkForValidFilter();
    this.filterValueData['errorObj'] = errorObj;
    this.filterValueData.empty = this.isEmptyOrNot();
    if (this.changeFilterData && !errorObj.error) {
      this.changeFilterData.emit(this.filterValueData);
    }

    //new
    if (item.isSelected == true) {
      this.filterIndex = index
      var temp = this.filterInnertag[index]
      var pos = 0
      for (let i = index; i >= pos; i--) {
        this.filterInnertag[i] = this.filterInnertag[i - 1];
      }
      this.filterInnertag[pos] = temp
    }
    // else{
    //   var temp = this.dupFilterInnerTag[index]
    //   var pos = 0
    //   for (let i = index; i >= pos; i--)
    //   {
    //       this.dupFilterInnerTag[i] = this.dupFilterInnerTag[i - 1];
    //   }
    //   this.dupFilterInnerTag[pos] = temp
    // }
    //end new

    console.log(this.courseTag, "course Tag")
    console.log(this.filterError, "filtered error")
    console.log(this.dupFilterInnerTag, "DupFilter")
    console.log(this.dummy, "dummy")
    console.log(this.dummyCourse, "dummyCourse")




  }

  formatDigitDate(Dtime){
    if(Dtime != null){
    const t = new Date(Dtime);
    // const formattedDateTime = this.datepipe.transform(t, 'yyyy-MM-dd h:mm');
    const formatSendDate = t.getFullYear() +  '-' + ("0" + (t.getMonth() + 1)).slice(-2) + '-' + ("0" + (t.getDate())).slice(-2);
    // const time = ' ' + t.getHours() + ':' + t.getMinutes() + ':' +  t.getSeconds();
    // const formattedDateTime = year + time;
    console.log(formatSendDate,"formatSendDate")
    return formatSendDate;
    }else{
      return null
    }
  }
  clearTag(item) {
    const filterValue = item.filterValue;
    // tslint:disable-next-line:max-line-length
    const filterId = item.filterId !== undefined ? String(item.filterId).toLowerCase() : String(item.tagname).toLowerCase();
    this.filteredData[filterId] = [];
    this.filterValueData[filterValue] = [];
    this.courseTag[this.filteredCount[filterId]]['count'] = this.filteredData[filterId].length;
    if (item.type === 'date') {
      item.value = [];
    } else {
      this.filterInnertag.forEach((value, key) => {
        value.isSelected = false;
      });
      this.filteredStore[filterId] = this.filterInnertag;
    }
    const filteredCount = this.getFilterCount();
    this.filterValueData['filteredCount'] = filteredCount;
    const errorObj = this.checkForValidFilter();
    this.filterValueData['errorObj'] = errorObj;
    this.filterValueData.empty = this.isEmptyOrNot();
    if (this.changeFilterData && !errorObj.error) {
      this.changeFilterData.emit(this.filterValueData);
    }
  }
  showfull() {
    this.fullHeight = true;
  }

  showDate(item) {
    item.showdateRange = item.showdateRange === undefined ? false : item.showdateRange;
    item.showdateRange = !item.showdateRange;
    item.isChecked = !item.isChecked;
    // this.showdateRange = !this.showdateRange;
  }
  clearalltag() {
    this.filterValueData = {};
    this.searchText = ''
    // this.filterInnertag = this.dupFilterInnerTag
    this.filterInnertag = this.dummy
    if(this.dummyCourse.length>0){
    this.courseTag = this.dummyCourse
    }
    this.courseTag.forEach((value: any, key) => {
      // tslint:disable-next-line:max-line-length
      const filterId = value.filterId !== undefined ? String(value.filterId).toLowerCase() : String(value.tagname).toLowerCase();
      const filterValue = value.filterValue;
      this.filteredData[filterId] = [];
      this.filterValueData[filterValue] = [];
      this.courseTag[this.filteredCount[filterId]]['count'] = this.filteredData[filterId].length;
      this.courseTag[this.filteredCount[filterId]]['isChecked'] = false;
      if (value.type === 'date') {
        value['value'] = [];
      } else {
        value.list.forEach((value, key) => {
          value.isSelected = false;
        });
      }
    })
    this.filteredStore = {};
    const filteredCount = this.getFilterCount();
    this.filterValueData['filteredCount'] = filteredCount;
    const errorObj = this.checkForValidFilter();
    this.filteredStore['errorObj'] = errorObj;
    this.filteredStore.empty = this.isEmptyOrNot();
    if (this.changeFilterData) {
      this.changeFilterData.emit(this.filteredStore);
    }


    // this.filteredCount = {};
  }

  filterError = [];
  checkForValidFilter(){
    // let isError = false;
    let errorMessage = '';
    let errorCount = 0;
    this.courseTag.forEach((value: any, key) => {
      if (value.isMandatory) {
        // tslint:disable-next-line:max-line-length
        const filterId = value.filterId !== undefined ? String(value.filterId).toLowerCase() : String(value.tagname).toLowerCase();
        // this.filteredData[filterId]
        if (this.filteredData[filterId] &&
           this.filteredData[filterId].length !== 0){
            this.filterError[key].isError = false;
          }else {
            errorCount ++;
            // if(!isError){
            //   errorMessage = 'The field(s) ';
            // }
            // if(isError){
            //   errorMessage += ', ' + value.tagname + ' ';
            // }
            if(errorMessage !== '') {
              errorMessage += ', ' + value.tagname + ' ';
            }else {
              errorMessage += ' '+ value.tagname;
            }
            this.filterError[key].isError = true;
          }
      }
    });
    if(errorCount > 1){
      errorMessage = 'Fields ' + errorMessage + ' are mandetory';
      // this.toast.warning(errorMessage, 'Warning');
    }else if (errorCount === 1) {
      errorMessage = 'The field' + errorMessage + ' is mandetory';
      // this.toast.warning(errorMessage, 'Warning');
    }
    const errorObj = {
      'message' : errorMessage,
      error: errorCount > 0,
    }
    console.log('Errored Array ===>', this.filterError);
    return errorObj;

  }

  getFilterCount(){
    let filterCount = 0;
    // console.log('this.filterValueData', this.filterValueData);
    // console.log('this.filteredData', this.filteredData);
    // console.log('this.filteredStore', this.filteredStore);
    if(this.filteredData !== {}){
      let count = 0;
      const items = Object.keys(this.filteredData);
       if(items.length > 0){
        for (let index = 0; index < items.length; index++) {
          const element = this.filteredData[items[index]];
          if(element.length !== 0){
            count ++;
          }
        }
       }
       filterCount = count;
    }
    return filterCount;
  }
  showDropClick() {
    this.selectedRadio = 1
    var event = {
      selectedLevel: this.selectedLevel,
      selectedRadio: this.selectedRadio
    }
    console.log(this.selectedLevel, "selectedLevel")
    this.dropdownClick.emit(event)
  }


  selectedRadioClick(event){
    this.selectedRadio = event
    console.log(event,"selectedRadio")
    var param = {
      selectedLevel: this.selectedLevel,
      selectedRadio: this.selectedRadio
    }
    this.dropdownClick.emit(param)

  }
  isEmptyOrNot() {
    let empty = false;
    for (const x in this.filterValueData) {
      if (this.filterValueData[x] && Array.isArray(this.filterValueData[x]) && this.filterValueData[x].length > 0) {
        empty = false;
        break;
      } else {
        empty = true;
      }
    }
    return empty;
  }

  inputSearch(event) {
    var searchtext
    var temp: any = []
    // this.header.searchtext = event.target.value
    console.log(searchtext);
    // searchtext = event.target.value;
    searchtext = event
    const val = searchtext.toLowerCase();
    if (val.length >= 0 ) {
      var tempcc = this.dupFilterInnerTag.filter(function (d) {
        if (d.isSelected != true) {
          return String(d.tagName).toLowerCase().indexOf(val) !== -1 ||
            // d.fullname.toLowerCase().indexOf(val) !== -1 ||
            !val
        }
        if (d.isSelected == true) {
          temp.push(d)
        }
      });
      console.log(tempcc);
      this.filterInnertag = []
      // this.filterInnertag.push(temp)
      // this.filterInnertag.push(tempcc)
      this.filterInnertag = temp.concat(tempcc)
      // if(this.sessionList.length == 0){
      //   this.noData = true
      // }else{
      //   this.noData = false
      // }
    }
  }

  ngOnChanges(){
    if(this.courseTag){
      // this.dummy = this.courseTag
      // this.dummyCourse = _.cloneDeep(this.courseTag);
      this.filterError = [...this.courseTag];
    }
  }
}


export class filterEntity {
  name: string;
  item: any;
}
