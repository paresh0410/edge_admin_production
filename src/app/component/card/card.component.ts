import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Card } from '../../models/card.model';
import { flattenStyles } from '@angular/platform-browser/src/dom/dom_renderer';

@Component({
  selector: 'ngx-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})

export class CardComponent implements OnInit {
  @Input() cardsList: any;
  @Input() card : any;
  @Input() hoverlabel: any;

  @Input() config: Card = {
    flag: '',
    titleProp: '',
    image: '',
    sectionIcon: '',
    discrption: '',
    date: '',
    endDate: '',
    identifier:false,
    employee: '',
    manager: '',
    lpName:'',
    question: '',
    category: '',
    maxpoints: '',
    maxAttemp: '',
    compleCount : '',
    totalCount : '',
    cnt: '',
    contentType: '',
    courseCount: '',
    courseCompletion: '',
    batches: '',
    showBottomBatches: false,
    batchIcon: false,
    enroll: '',
    showBottomEnroll: false,
    sms: false,
    msg: false,
    mbl: false,
    showCourseCompletion: false,
    showIcon : false,
    showBottomCourseCountr: false,
    showImage:   false,
    showCompletedEnroll: false,
    showBottomQuestion: false,
    shareIcon: false,
    upgradeIcon:false,
    download:false,
    retweet: false,
    hoverlable:   false,
    option:   false,
    eyeIcon:   false,
    copyIcon:   false,
    editIcon:   false,
    bottomDiv:   false,
    bottomTitle:   false,
    bottomDiscription:   false,
    showBottomList:   false,
    showBottomEnddate: false,
    showBottomEmployee:   false,
    showBottomManager:   false,
    showlpName:false,
    showBottomdate:   false,
    showBottomCategory:   false,
    showBottomMaximumPoint:   false,
    showBottomMaximumAttemp: false,
    showBottomAssets: false,
    customCard:  '',
    btnLabel: '',
    bulkCopy: false,
    defaultImage: 'assets/images/noImage1.png',
  };




  // @Input() disable: boolean;
  @Input() downLoadLink: any;
  @Output() copyClick = new EventEmitter();
  @Output() upgrade = new EventEmitter();
  @Output() download = new EventEmitter();
  @Output() disabled  = new EventEmitter<any>();
  @Output() sectionEdit  = new EventEmitter();
  @Output() gotoDetails  = new EventEmitter();
  @Output() smsClick  = new EventEmitter();
  @Output() msgClick  = new EventEmitter();
  @Output() mblClick  = new EventEmitter();
  @Output() goBatch  = new EventEmitter();
  @Output() bulkCopyEmit  = new EventEmitter();





  // show hover in option dots
  show: boolean = true;

  constructor() {



  }

  ngOnInit() {
    // console.log(this.downLoadLink,"this.downLoadLink")
    // console.log(this.config,"config")
    // console.log(this.cardsList,"cardsList")

    // console.log('this.config.totalCount',this.config.totalCount );
    // console.log('this.cardsList[config.totalCount]',this.cardsList[this.config.totalCount]);

  // if(this.config.flag == 'content') {
  //   if(this.cardsList[this.config.contentType] == '7' || this.cardsList[this.config.contentType] == '8') {
  //     this.cardsList[this.config.image] = 'assets/images/imageNew.png';
  //     } else if(this.cardsList[this.config.contentType] == '1' || this.cardsList[this.config.contentType] == '6') {
  //     this.cardsList[this.config.image] = 'assets/images/videoNew.png';
  //     } else if(this.cardsList[this.config.contentType] == '2') {
  //     this.cardsList[this.config.image] = 'assets/images/audioNew.png';
  //     } else if(this.cardsList[this.config.contentType] == '3') {
  //     this.cardsList[this.config.image] = 'assets/images/pdfNew.png';
  //     } else if(this.cardsList[this.config.contentType] == '10') {
  //     this.cardsList[this.config.image] = 'assets/images/ppt.jpg';
  //     } else if(this.cardsList[this.config.contentType] == '11') {
  //     this.cardsList[this.config.image] = 'assets/images/excel.jpg';
  //     } else if(this.cardsList[this.config.contentType] == '12') {
  //     this.cardsList[this.config.image] = 'assets/images/doc.jpg';
  //     }
  // }

  // if(this.config.flag == 'budgetCategory') {
  //   if(this.cardsList[this.config.image] == null) {
  //     this.cardsList[this.config.image] = 'assets/images/courseicon.jpg';
  //   }
  // }

  // if(this.config.flag == 'ntfTemplate') {

  //     this.cardsList[this.config.image] =this.cardsList[this.config.image]?this.cardsList[this.config.image]: 'assets/images/notification.png';

  // }

  // if(this.config.flag == 'certificates') {
  //   if(this.cardsList[this.config.image] == '') {
  //     this.cardsList[this.config.image] = 'assets/images/courseicon.jpg';
  //   }
  // }

  // if(this.config.flag == 'trainer') {
  //     this.cardsList[this.config.image] = this.cardsList[this.config.image]?this.cardsList[this.config.image]:'assets/images/courseicon.jpg';
  // }


  // if(this.config.flag == 'nominationInstance') {
  //   this.cardsList[this.config.image] = this.cardsList[this.config.image]?this.cardsList[this.config.image]:'assets/images/courseicon.jpg';
  // }

//   if(this.config.flag == 'eepWorkflow') {

//     this.cardsList[this.config.image] =this.cardsList[this.config.image]?this.cardsList[this.config.image]: 'assets/images/courseicon.jpg';

// }





  // if(this.config.flag == 'survey') {
  //   if(this.cardsList[this.config.image] == '') {
  //     this.cardsList[this.config.image] = 'assets/images/survey.png';
  //   }
  // }

  // if(this.config.flag == 'leaderboard') {

  //   this.cardsList[this.config.image] =this.cardsList[this.config.image]?this.cardsList[this.config.image]: 'assets/images/courseicon.jpg';

  // }



  // if(this.config.flag == 'learningpath') {
  //   this.cardsList[this.config.image] = this.cardsList[this.config.image]?this.cardsList[this.config.image]:'assets/images/open-book-leaf.jpg';
  // }



  // if(this.config.flag == 'poll') {
  //   this.cardsList[this.config.image] = this.cardsList[this.config.image]?this.cardsList[this.config.image] :'assets/images/survey.png';
  // }
  if(this.config.flag == 'newasset') {
    this.cardsList[this.config.image] = this.cardsList[this.config.image]?this.cardsList[this.config.image] :'assets/images/category.jpg';
  }


  if(this.config.flag == 'assets') {
    if(this.cardsList[this.config.contentType] == '1' || this.cardsList[this.config.contentType] == '4' || this.cardsList[this.config.contentType] == '6'|| this.cardsList[this.config.contentType] == '5') {
      this.cardsList[this.config.image] = 'assets/images/videoNew.png';
      } else if(this.cardsList[this.config.contentType] == '2') {
        this.cardsList[this.config.image] = 'assets/images/audioNew.png';
      } else if(this.cardsList[this.config.contentType] == '7'|| this.cardsList[this.config.contentType] == '8') {
        this.cardsList[this.config.image] = 'assets/images/imageNew.png';
      } else if(this.cardsList[this.config.contentType] == '3' ) {
        this.cardsList[this.config.image] = 'assets/images/pdfNew.png';
      } else if(this.cardsList[this.config.contentType] == '10') {
        this.cardsList[this.config.image] = 'assets/images/ppt.jpg';
      } else if(this.cardsList[this.config.contentType] == '11') {
        this.cardsList[this.config.image] = 'assets/images/excel.jpg';
      } else if(this.cardsList[this.config.contentType] == '12') {
        this.cardsList[this.config.image] = 'assets/images/doc.jpg';
      }

  }




   if(this.config.flag == 'assetCategory') {
    if(this.cardsList[this.config.image] == '') {
      this.cardsList[this.config.image] = 'assets/images/category.jpg';
    }
   }

  //  if(this.config.flag == 'courseContent') {
  //   if(this.cardsList[this.config.image] == '') {
  //     this.cardsList[this.config.image] = 'assets/images/courseicon.jpg';
  //   }
  //  }

  //  if(this.config.flag == 'newsAnnoucement') {
  //   this.cardsList[this.config.image] = this.cardsList[this.config.image]?this.cardsList[this.config.image]: 'assets/images/badgeNew.png';
  //  }

  //  if(this.config.flag == 'badges') {
  //   this.cardsList[this.config.image] = this.cardsList[this.config.image] ? this.cardsList[this.config.image] : 'assets/images/badgeNew.png';
  //  }
  //  if(this.config.flag == 'Quiz') {
  //   this.cardsList[this.config.image] = this.cardsList[this.config.image] ? this.cardsList[this.config.image] : 'assets/images/category.jpg';
  //  }
  //  if(this.config.flag == 'courseCategory') {
  //   this.cardsList[this.config.image] = this.cardsList[this.config.image] ? this.cardsList[this.config.image] : 'assets/images/category.jpg';
  //  }
  //  if(this.config.flag == 'blendedCategory') {
  //   this.cardsList[this.config.image] = this.cardsList[this.config.image] ? this.cardsList[this.config.image] : 'assets/images/category.jpg';
  //  }
  //  if(this.config.flag == 'program') {
  //   this.cardsList[this.config.image] = this.cardsList[this.config.image] ? this.cardsList[this.config.image] : 'assets/images/category.jpg';
  //  }
  //  if(this.config.flag == 'blendedCourse') {
  //   this.cardsList[this.config.image] = this.cardsList[this.config.image] ? this.cardsList[this.config.image] : 'assets/images/courseicon.jpg';
  //  }
  //  if(this.config.flag == 'preonBoardingCategory') {
  //   this.cardsList[this.config.image] = this.cardsList[this.config.image] ? this.cardsList[this.config.image] : 'assets/images/category.jpg';
  //  }
  //  if(this.config.flag == 'preonBoardingCategory') {
  //   this.cardsList[this.config.image] = this.cardsList[this.config.image] ? this.cardsList[this.config.image] : 'assets/images/courseicon.jpg';
  //  }
  //  if(this.config.flag == 'feedback') {
  //   this.cardsList[this.config.image] = this.cardsList[this.config.image] ? this.cardsList[this.config.image] : 'assets/images/category.jpg';
  //  }
  }

  mouseEnter() {
    this.show = false;
  }

  mouseLeave() {
    this.show = true;
  }
  detailsPage() {
    // if(this.card == 'section') {
    //   this.gotoDetails.emit(this.cardsList.id);
    // }
    // if(this.card == 'news') {
    //   this.gotoDetails.emit(this.cardsList.id);
    // }

    this.gotoDetails.emit();

  }

  copyCard() {
    this.copyClick.emit();
  }
  upgradeCard() {
    this.upgrade.emit()
  }
  downloadCard(){
    this.download.emit()
  }

  enable() {
    this.disabled.emit(this.cardsList.visible);
  }

  editCard() {
    this.sectionEdit.emit();
  }

  smsCard() {
    this.smsClick.emit();
  }

  msgCard() {
    this.msgClick.emit();
  }

  mblCard() {
    this.mblClick.emit();
  }

  batchCard() {
    this.goBatch.emit();
  }

  bulkCopy(){
    this.bulkCopyEmit.emit();
  }

  errorHandler(event) {
    if(event){
      // event.target.src = "assets/images/noImage1.png";
      this.cardsList[this.config.image] = this.config.defaultImage;
    }
    // console.debug(event);
 }
}



