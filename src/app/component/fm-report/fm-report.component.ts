import { Component, OnInit,ViewEncapsulation, Input, Output, EventEmitter, ChangeDetectorRef, ViewChild } from '@angular/core';
import * as Flexmonster from 'flexmonster';
import { FlexmonsterPivot } from 'ng-flexmonster';
import { Report } from '../../pages/reports/report';
import { FMToolbar } from './fm-report';
let _this;
import '../../../assets/style/style.scss';
@Component({
  selector: 'ngx-fm-report',
  templateUrl: './fm-report.component.html',
  styleUrls: ['./fm-report.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FmReportComponent implements OnInit {
  @ViewChild('pivot') pivot: any;
  @Input() Data: any = [];
  @Input() Report: any = {};
  @Input() Toolbar: FMToolbar;


  /**
   * Report Events
  */
 @Output() GetReportMethod = new EventEmitter<any>();

  // flexMonsterKey = 'Z712-XID43F-053F1X-026X4V-4K2D3Q-320I29-4S5T5W-211W05-3A2457-2K0L66-6J141M-6P4U25-5M115C-2O1I';
  // Flex monster dev key.
  flexMonsterKey: any = ''; // 'Z7QE-XCC244-3E3C4S-193J3N';
  FMReport: any;
  // flexMonsterDataUrl: any = 'https://cdn.flexmonster.com/reports/report.json';
  // componentFolder: any = 'https://cdn.flexmonster.com/';
  constructor() {
    try {
      _this = this;
      const loginResData = localStorage.getItem('LoginResData');
      if (loginResData) {
        const json = JSON.parse(loginResData).data;
        this.flexMonsterKey = this.isUndefined(json) == true ? this.decryptKey(json['flexmonsterkey']) : null;
      } 
    } catch (error) {
      this.flexMonsterKey = null;
      console.error(error.message);
    }
  }

  ngOnInit() {
    console.log('Data :- ', this.Data);
    console.log('Report :- ', this.Report);
    console.log('Toolbar :- ', this.Toolbar);
    this.bindFMData();
  }

  customizeToolbar(toolbar) {
    // get all tabs
    const tabs = toolbar.getTabs();
    toolbar.getTabs = function () {
        // delete the first tab
        // delete tabs[0];
        // delete tabs[1];
        // delete tabs[2];
        tabs.unshift({
            id: "fm-tab-saveReportTab",
            title: "Report",
            handler: _this.get_report_js,
            icon: this.icons.save
        });
        for(let i =0; i < tabs.length; i++) {
          if (tabs[i].title) {
            if (!_this.Toolbar[tabs[i].title.toLowerCase()]) {
              delete tabs[i];
            }
          }
        }
      return tabs;
    }
  }
  onPivotReady(pivot: Flexmonster.Pivot): void {
    console.log('[ready] FlexmonsterPivot', this.pivot);
  }


  onReportComplete(): void {
    // this.pivot.flexmonster.off('reportcomplete');
    this.removePlaceholder();
    this.removeElement();
  }

  onCustomizeCell(cell: Flexmonster.CellBuilder, data: Flexmonster.CellData): void {
    if (data.isClassicTotalRow) {
      cell.addClass('fm-total-classic-r');
    }
    if (data.isGrandTotalRow) {
      cell.addClass('fm-grand-total-r');
    }
    if (data.isGrandTotalColumn) {
      cell.addClass('fm-grand-total-c');
    }
  }
  ngAfterViewInit() {
  }

  bindFMData () {
    this.Report.dataSource.data = this.Data;
    var pivot = new Flexmonster({
      licenseKey: this.flexMonsterKey,
      container: 'pivot-container',
      toolbar: true,
      report: this.Report,
      width: '100%',
      height: 700,
      beforetoolbarcreated: this.customizeToolbar
    });
    this.FMReport = pivot;
    console.log('pivot: -', this.FMReport);
    this.FMReport.on('reportcomplete', function () {
      // alert('Report completed!');
      if (this.removePlaceholder) {
        this.removePlaceholder();
        this.removeElement();
      } else {
        _this.removePlaceholder();
        _this.removeElement();
      }
    });
  }

  get_report() {
    // let reportJson = this.pivot.getReport();
    let reportJson = this.FMReport.getReport();
    if (reportJson && reportJson.dataSource) {
      reportJson.dataSource.data = [];
    }
    console.log('Report Json :- ', reportJson);
    if (this.GetReportMethod) {
      this.GetReportMethod.emit(reportJson);
    }
  }
  get_report_js = function(event) {
    _this.get_report();
  }
  removePlaceholder() {
    const blocks = document.getElementsByClassName('fm-ui-element fm-ui fm-noselect'); // [0].innerText
    if (blocks && blocks.length > 0) {
      for(var i= 0; i < blocks.length; i++) {
        let block: any = blocks[i];
        if (block.innerText.includes('trial')) {
            block.innerText = '';
        }
      }
    }
  }
  removeElement() {
    // Removes an element from the document
    let element: any = document.getElementById('fm-branding-bar');
    if (element) {
      element.parentNode.removeChild(element);
    }
  }

  decryptKey (encrypt) {
    if (!encrypt) return null;

    const text = encrypt.replace(/ILLUME/g,'A');
    const key = atob(text);
    return key;
  }
  isUndefined(value) {
    if(value) return true;
  }
}
