import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FmReportComponent } from './fm-report.component';

describe('FmReportComponent', () => {
  let component: FmReportComponent;
  let fixture: ComponentFixture<FmReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FmReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FmReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
