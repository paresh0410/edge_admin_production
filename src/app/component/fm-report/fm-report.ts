export class FMToolbar {
    connect: boolean;
    open: boolean;
    save: boolean;
    export: boolean;
    grid: boolean;
    charts: boolean;
    format: boolean;
    options: boolean;
    fields: boolean;
    fullscreen: boolean;
    report: boolean;
}

export class FMReportEntity {
    report_name: string;
    report_id: string;
    metadata: any;
    userid: number;
    report_map_id: number;
    menuId: number;
}

export class FMReportFilterEntity {
    id: number;
    displaytext: string;
    title: string;
}
export class FMReportFilter {
    main: Object[];
    sub: Object[];
}