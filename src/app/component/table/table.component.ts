import { Component, OnInit, Input, Output, EventEmitter, OnChanges, ViewEncapsulation } from '@angular/core';
import { Keys } from '@swimlane/ngx-datatable/release/utils';


@Component({
  selector: 'ngx-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  encapsulation:ViewEncapsulation.None,
})
export class TableComponent implements OnInit, OnChanges {
  @Input('tableContent') data: any;
  @Input('ignoreCol') ignoreCol: any =[];
  @Input('stepIndex') stepIndex: any;
  @Input('stepThreeFlag') stepThreeFlag?: boolean = true;
  @Input('dropDownData') dropDownData: any = [];
  @Input('currenttablabel')currenttablabel:any;

  @Output() sendUpdatedValues = new EventEmitter();
  @Output() callPopUpModal = new EventEmitter();


  eventData = 0;
  headerRow: any = [];
  headerCheckBox = false;
  temData: any = [];

  // Step 3
  userEcn: string= '';
  completedStatus: string = 'all';
  publish = '';
  isPending: boolean = false;

  constructor() {
    // console.log('Table Content Data ====>', this.data);
  }

  ngOnInit() {
    console.log('Table Content Data  ====>', this.data);
    this.temData = this.data;
    this.prepareHeaderData();
  }
  ngOnChanges() {
    if(this.currenttablabel == 'Pending'){
      this.isPending =  true
    }
    else{
      this.isPending = false
    }
    // const name: SimpleChange = changes.name;
    // console.log('prev value: ', name.previousValue);
    // console.log('got name: ', name.currentValue);
    // this._name = name.currentValue.toUpperCase();
    console.log("hello");
    console.log('Dropdown data  ====>', this.dropDownData);
    if(this.dropDownData.length !=0){
      this.publish = this.dropDownData[0].LOVValue;
    }
    // this.publish = this.dropDownData[0].LOVValue;
    this.prepareHeaderData();
  }
  // repeatCssFunction(col) {
  //   return 'repeat(' + col + ', 1fr)';
  // }
  SendDataToParent() { // You can give any function name
    // this.eventData = this.eventData + 1;
    this.sendUpdatedValues.emit(this.data);
  }
  prepareHeaderData() {
    if(this.data){
      this.temData = this.data;

      if (this.data.length !== 0) {
        this.headerRow = Object.keys(this.data[0]);
        for (let index = 0; index < this.ignoreCol.length; index++) {
          this.headerRow = this.headerRow.filter(item => item !== this.ignoreCol[index]);
        }
        console.log('header key rows ======>', this.headerRow);
      }
    }

  }
  // this function is use to dynamicsearch on table
  searchQuestion(event) {
    var val = event.target.value.toLowerCase();
    var keys = [];
    let data = '';
    // filter our data
    if (this.temData.length > 0) {
      keys = Object.keys(this.temData[0]);
    }
    var temp = this.temData.filter((d) => {
      // keys.forEach(element => {

      // });
      for (const key of keys) {
        if (String(d[key]).toLowerCase().indexOf(val) !== -1) {
          return true;
        }
      }
    });

    // update the rows
    this.data = temp;
  }
  updateSingleValue(index, value, event) {
    // event.checked ? value = 1 : value = 0;
    console.log('data to update ===>', index, ' value at', value);
    this.data[index].isSelected = event.checked;
    console.log('updated data ==>', this.data[index]);
    console.log('Final Data Array ==>', this.data);
    for (let i = 0; i < this.data.length; i++) {
      // const element = array[index];
        if(this.data[i].isSelected){
          this.headerCheckBox = true;
        }else {
          this.headerCheckBox = false;
          break;
        }
    }

    this.SendDataToParent();
  }
  updateAllValues(value , event) {
    // event.checked ? value = true : value = 0;
    console.log('Box Checked ===>', event.checked);
    for (let i = 0; i < this.data.length; i++) {
      this.data[i].isSelected = event.checked;
    }
    console.log('Update all records', this.data);
    this.SendDataToParent();
  }
  callPopup(data) {
    console.log('Data called ===>', data);
    this.callPopUpModal.emit(data);
  }

  FilterDate(){

    let array = this.temData;
    console.log('Ecn Search ==>', this.userEcn);
    console.log('Completed Status ==>', this.completedStatus);
    console.log('Publish Status ==>', this.publish);
    if(this.userEcn != ''){
      let keys :any = [];
      if (this.temData.length > 0) {
        // keys = Object.keys(this.temData[0])[0];
        let val = Object.keys(this.temData[0]);
        keys.push(val[1]);
        // keys.push(Object.keys(this.temData[0])[0]);
      }

      array = this.searchString(this.userEcn, array, keys);
    }

    // if (this.completedStatus == 'accepted') {
    //   let keys:any = [];
    //   if (this.temData.length > 0) {
    //     let val = Object.keys(this.temData[0]);
    //     keys.push(val[6]);
    //   }
    //     array = this.searchString('Accepted', array, keys);
    // }
    // if(this.completedStatus == 'rejected'){
    //   let keys:any = [];
    //   if (this.temData.length > 0) {
    //     let val = Object.keys(this.temData[0]);
    //     keys.push(val[6]);
    //   }
    //   array = this.searchString('Rejected', array, keys);
    // }
    // if(this.completedStatus == 'notAvailable'){
    //   let keys:any = [];
    //   if (this.temData.length > 0) {
    //     let val = Object.keys(this.temData[0]);
    //     keys.push(val[6]);
    //   }
    //   array = this.searchString('Not Available', array, keys);
    // }
    // if(this.publish == 'Not Published'){
    //   let keys:any = [];
    //   if (this.temData.length > 0) {
    //     let val = Object.keys(this.temData[0]);
    //     keys.push(val[7]);
    //   }
    //   array = this.searchString('Not Published', array, keys);
    // }
    // if(this.publish == 'Published'){
    //   let keys:any = [];
    //   if (this.temData.length > 0) {
    //     let val = Object.keys(this.temData[0]);
    //     keys.push(val[7]);
    //   }
    //   array = this.searchString('Published', array, keys);
    // }
    // if(this.publish == 'notApplicable'){
    //   let keys:any = [];
    //   if (this.temData.length > 0) {
    //     let val = Object.keys(this.temData[0]);
    //     keys.push(val[7]);
    //   }
    //   array = this.searchString('Not Applicable', array, keys);
    // }
      // if(this.publish. == 'notApplicable'){
      let keys:any = [];
      // if (this.temData.length > 0) {
      //   let val = Object.keys(this.temData[0]);
      //   for(let i = 0; i < va)
      //   keys.push(val[7]);
      // // }
      keys.push('Publish');
      if(this.publish != 'All'){
        let item:any;
        for (let index = 0; index < this.dropDownData.length; index++) {
          // const element = array[index];
          if(this.dropDownData[index].LOVValue === this.publish ){
            item = this.dropDownData[index];
          }
        }
        array = this.searchString(item.LOVName, array, keys);
      }
      this.data = array;
  }

  searchString(string, array , keys) {
    // let  keys = [];
    if(keys[0] == 'Publish'){

      var temp = array.filter((d) => {
        // keys.forEach(element => {

        // });
        for (const key of keys) {
          if (String(d[key]).toLowerCase() == string.toLowerCase()) {
            return true;
          }
        }
      });
    } else {
      var temp = array.filter((d) => {
        // keys.forEach(element => {

        // });
        for (const key of keys) {
          if (String(d[key]).toLowerCase().indexOf(string.toLowerCase()) !== -1) {
            return true;
          }
        }
      });
    }
    return temp;
  }

  getOriginalData(){
    this.data = this.temData;
    this.userEcn = '';
    // this.completedStatus = 'all';
    this.publish = this.dropDownData[0].LOVValue;
  }
}
