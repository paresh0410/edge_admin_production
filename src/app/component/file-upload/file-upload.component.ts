import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss'],
})
export class FileUploadComponent implements OnInit, OnChanges {
  // files: any[] = [];
  OutputfilesToEmit: any[] = [];
  // filesReturn: any[] = [];
  // previewArray: boolean[] = [];
  // url = '';
  // allowMultiple = false;

  @Input() allowMultiple = false;
  @Input() files: any[] = [];
  @Input() allowedTypes = [];
  @Input() resetFlag = false;
  @Input() allowedSize: number;
  @Output() outputFile = new EventEmitter();

  constructor(public sanitizer: DomSanitizer,
    private toastr: ToastrService,
    ) {}

  ngOnInit(): void {

  }

  /**
   * on file drop handler
   */
  onFileDropped($event) {
    this.prepareFilesList($event);
  }

  /**
   * handle file from browsing
   */

  fileBrowseHandler(files) {
    if (files) {
      // this.fileName = file.name;
      // this.file = file;
      this.prepareFilesList(files);
    }
  }

  /**
   * Delete file from files list
   * @param index (File index)
   */
  deleteFile(index: number) {
    // if(this.files.length === this.OutputfilesToEmit.length){
    //   this.files.splice(index, 1);
    //   this.OutputfilesToEmit.splice(index, 1);
    // } else {
    //   if (this.files[index].indexOfValidIndex != null){
    //     this.OutputfilesToEmit.splice(this.files[index].indexOfValidIndex, 1);
    //   }
    // }
    // this.outputFile.emit(this.OutputfilesToEmit);
    this.files.splice(index, 1);
    // this.outputFile.emit(this.files);
    this.outputFile.emit(this.files);
  }

  /**
   * Simulate the upload process
   */
  uploadFilesSimulator(index: number) {
    setTimeout(() => {
      if (index === this.files.length) {
        return;
      } else {
        const progressInterval = setInterval(() => {
          if (this.files[index].progress === 100) {
            clearInterval(progressInterval);
            this.uploadFilesSimulator(index + 1);
          } else {
            this.files[index].progress += 5;
          }
        }, 200);
      }
    }, 1000);
  }

  /**
   * Convert Files list to normal array list
   * @param files (Files List)
   */
  prepareFilesList(files: Array<any>) {

    if (!this.allowMultiple) {
      this.files = [];
      this.OutputfilesToEmit = [];
    }
    let indexOfValid = 0;
    for (const item of files) {
      const reader = new FileReader();
      // const sendItem = item;

      item.progress = 0;
      // for (let i = 0; i < this.allowedTypes.length; i++) {
      //   if (fileType.includes(this.allowedTypes[i])) {
      //     fileExt = true;
      //     break;
      //   }
      // }
      item.indexOfValidIndex = null;
      item.displaySize = this.formatBytes(item.size, 2);
      item.valid = false;
      for (const itemType of this.allowedTypes) {
        if (item.type.includes(itemType)) {
          item.valid = true;
          item.invalidMessage = '';
          // item.invalidMessage = 'Valid File Type.';
          break;
        } else {
          item.valid = false;
          item.invalidMessage = 'Invalid File Type.';
          this.toastr.warning('Please upload valid file.', 'Warning');

          // break;
        }
      }
      item.validSize = false;
      if(this.allowedSize <= item.size){
        item.validSize = false;
        item.validSizeErrorMessage = 'File size should be less than ' + this.formatBytes(this.allowedSize , 2);
        this.toastr.warning('File size should be less than ' + this.formatBytes(this.allowedSize , 2), 'Warning');

      } else {
        item.validSize = true;
        item.validSizeErrorMessage = '';
      }
      // if (!fileExt) {
      //   // this.presentToast('warning', 'Please select a file of type ppt');
      //   // this.removeFile();
      // } else if (size <= this.files[0].size) {
      //   // this.presentToast('warning', 'File size should be less than 100 MB');
      //   // this.removeFile();
      // }
      item.previewItem = false;
      reader.readAsDataURL(item);
      const type = item.type ? item.type.toLowerCase() : null;
      if (type && type.indexOf('image') !== -1) {
        item.fileType = 'image';
      } else if (type && type.indexOf('video') !== -1) {
        item.fileType = 'video';
      } else if (type && type.indexOf('pdf') !== -1) {
        item.fileType = 'pdf';
      } else if (type && type.indexOf('audio') !== -1) {
        item.fileType = 'audio';
      } else {
        item.fileType = 'not_found';
      }
      item.alreadyUploaded = false;
      reader.onload = (event) => {
        item.preViewUrl = this.sanitizer.bypassSecurityTrustResourceUrl(
          reader.result as string
        );
      };
      // if (item.validSize && item.valid){
      //   item.indexOfValidIndex = indexOfValid;
      //   indexOfValid ++ ;
      //   this.OutputfilesToEmit.push(item);
      // }
      this.files.push(item);
    }
    console.log(this.files);
    this.outputFile.emit(this.files);
    this.uploadFilesSimulator(0);
  }

  /**
   * format bytes
   * @param bytes (File size in bytes)
   * @param decimals (Decimals point)
   */
  formatBytes(bytes, decimals) {
    if(!bytes){
      return 'NA';
    }
    if (bytes === 0) {
      return '0 Bytes';
    }
    const k = 1024;
    const dm = decimals <= 0 ? 0 : decimals || 2;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }

  togglePreview(index) {
    this.files[index].previewItem = !this.files[index].previewItem;
  }

  ngOnChanges(){
    if(this.resetFlag){
      this.files = [];
    }
  }

}
