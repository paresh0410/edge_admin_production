import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';

@Component({
  selector: 'ngx-table-modal',
  templateUrl: './table-modal.component.html',
  styleUrls: ['./table-modal.component.scss']
})
export class TableModalComponent implements OnInit, OnChanges {
  @Input('tableContent') data: any;
  @Input('tableModalTitle') Title: any;
  @Input('showPopup') showPopup: any;
  @Input('stepId') stepId: any;
  @Input('workflowType') workflowType?: any= 'ttt';
  showPopup1;
  @Output() close = new EventEmitter();
  headerRow;
  headerCheckBox = false;
  employee_Detail: any = [];
  BHApproved: any = [];
  adminaprove:any =[];
  evaluationCallDetails: any = [];
  feedback: any = [];
  quizData: any = [];
  assessment: any = [];
  fgAssessment: any = [];
  vivaFeedBackObject: any = [];
  batchDetails: any = [];
  mockFeedBackObject: any = [];
  assesmentFeedBackObject: any = [];
  CertificateObject: any = [];
  constructor() {

  }
  ngOnInit() {
    console.log('Table Content Data  ====>', this.data);
    console.log('Table Popup Data  ====>', this.showPopup);
    console.log('Table Popup Data  ====>', this.stepId);
    // this.prepareHeaderData();
    this.setdata(this.data, this.stepId);
    this.preapreToggleSwitch();
  }
  ngOnChanges() {
    // const name: SimpleChange = changes.name;
    // console.log('prev value: ', name.previousValue);
    // console.log('got name: ', name.currentValue);
    // this._name = name.currentValue.toUpperCase();
    console.log("hello");
    // this.popUpToggle();
    // this.preapreToggleSwitch();

    // this.prepareHeaderData();
  }
  preapreToggleSwitch() {
    this.showPopup1 = this.showPopup;
  }

  setdata(data, stepId) {
    this.employee_Detail = [];
    this.BHApproved = [];
    this.adminaprove = [];
    this.evaluationCallDetails = [];
    this.feedback = [];
    this.quizData = [];
    this.assessment = [];
    this.fgAssessment = [];
    this.vivaFeedBackObject = [];
    this.batchDetails = [];
    this.mockFeedBackObject = [];
    this.assesmentFeedBackObject = [];
    this.CertificateObject = [];
    if(this.workflowType =='eep')
    {
      switch (parseInt(stepId)) {
        case 1:
          this.employee_Detail = data;
          break;
        case 3:
          this.BHApproved = data;
          break;
        case 4:
          this.adminaprove = data;
          break;
        case 2:
          this.fgAssessment = data.fgAssessment;
          this.quizData = data.quizData;
          break;
      }
     
    }else{
      switch (parseInt(stepId)) {
        case 1:
          this.employee_Detail = data;
          break;
        case 2:
          this.BHApproved = data;
          break;
        case 3:
          this.evaluationCallDetails = data.evaluationCallDetails;
          this.feedback = data.feedback;
          break;
        case 6:
          this.fgAssessment = data.fgAssessment;
          this.quizData = data.quizData;
          break;
        case 7:
          this.assessment = data;
          break;
        case 8:
          this.vivaFeedBackObject = data;
          break;
        case 9:
          this.batchDetails = data;
          break;
        case 10:
          this.mockFeedBackObject = data;
          break;
        case 11:
          this.assesmentFeedBackObject = data;
          break;
        case 12:
          this.CertificateObject = data;
          break;
      }
    }
  }
  togglePopUp() {
    this.showPopup1 = !this.showPopup1;
    if (this.close) {
      this.close.emit(true);
    }
  }

  prepareHeaderData() {
    if (this.data[0].length !== 0) {
      this.headerRow = Object.keys(this.data[0]);
      console.log('header key rows ======>', this.headerRow);
    }
  }
}