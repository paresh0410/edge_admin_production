import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalCoursesComponent } from './total-courses.component';

describe('TotalCoursesComponent', () => {
  let component: TotalCoursesComponent;
  let fixture: ComponentFixture<TotalCoursesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TotalCoursesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalCoursesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
