import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-active-user',
  templateUrl: './active-user.component.html',
  styleUrls: ['./active-user.component.scss']
})
export class ActiveUserComponent implements OnInit {
  skeleton: boolean = true;
  total_courses: string;
  constructor() { }

  ngOnInit() {
    this.skeleton = false;
    setTimeout(() => {
      this.total_courses = '76';
      this.skeleton = true;
    }, 2000);
  }

}
