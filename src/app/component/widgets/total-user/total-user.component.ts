import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-total-user',
  templateUrl: './total-user.component.html',
  styleUrls: ['./total-user.component.scss']
})
export class TotalUserComponent implements OnInit {
  skeleton: boolean = true;
  learning_Hours: string;
  constructor() { }

  ngOnInit() {
    this.skeleton = false;
    setTimeout(() => {
      this.learning_Hours = '23987';
      this.skeleton = true;
    }, 2000);
  }
}
