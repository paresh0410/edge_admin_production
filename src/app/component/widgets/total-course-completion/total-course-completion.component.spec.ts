import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalCourseCompletionComponent } from './total-course-completion.component';

describe('TotalCourseCompletionComponent', () => {
  let component: TotalCourseCompletionComponent;
  let fixture: ComponentFixture<TotalCourseCompletionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TotalCourseCompletionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalCourseCompletionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
