import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkCopyComponent } from './bulk-copy.component';

describe('BulkCopyComponent', () => {
  let component: BulkCopyComponent;
  let fixture: ComponentFixture<BulkCopyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BulkCopyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkCopyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
