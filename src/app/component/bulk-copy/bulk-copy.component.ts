import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  ChangeDetectorRef,
  Input,
  ViewChild,
} from "@angular/core";
import { NgForm } from '@angular/forms';
import { AssetService } from "../../pages/dam/asset/asset.service";
import { JsonToXlsxCommonService } from "../../service/json-to-xlsx.service";
import { CommonFunctionsService } from "../../service/common-functions.service";
import { DataSeparatorService } from "../../service/data-separator.service";
import { HttpClient } from "@angular/common/http";
import { webApi } from "../../service/webApi";
import * as _ from "lodash";
import { noData } from "../../models/no-data.model";
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: "ngx-bulk-copy",
  templateUrl: "./bulk-copy.component.html",
  styleUrls: ["./bulk-copy.component.scss"],
})
export class BulkCopyComponent implements OnInit {
  // @ViewChild('ngxtab')
  // ngxtab: ElementRef;
  // titleName = "Bulk Copy";
  valueChanged = null;
  valueChanged2 = null;
  showSearch = false;
  currentActiveTab = null;
  @Input() activityClicked: any;
  @Input() courseTypeId: any;
  @ViewChild('formData') addEditNotification: NgForm;
  @Input() titleName = '';
  @Input() basicDetails = {
    NotificationTypeName: null,
    NotificationTypeText: null,
    AppNotificationCheckBox: false,
    AppNotificationSubject: null,
    AppNotificationDescrition: null,
    SMSChecked: false,
    SMSText: null,
    EmailCheckBox: false,
    EmailSubject: null,
    EmailDescrition: null,
    attchRef: null,
    description: "",
    eventId: null,
    aId: null,
    id: 0,
  };
  @Input() config: any = {};
  @Input() componentConfig: string = "";
  // categoriesList = [
  //   {
  //     id: 1341,
  //     categoryName: "@folder26",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-26T12:22:14.000Z",
  //   },
  //   {
  //     id: 1081,
  //     categoryName: "version1",
  //     parentCatId: 1079,
  //     tabId: 1,
  //     timemodified: "2020-08-26T07:48:09.000Z",
  //   },
  //   {
  //     id: 1091,
  //     categoryName: "version",
  //     parentCatId: 1079,
  //     tabId: 1,
  //     timemodified: "2020-08-26T07:47:55.000Z",
  //   },
  //   {
  //     id: 1339,
  //     categoryName: "26-08-2020",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-26T07:44:16.000Z",
  //   },
  //   {
  //     id: 1321,
  //     categoryName: "Test 2400",
  //     parentCatId: 77,
  //     tabId: 1,
  //     timemodified: "2020-08-24T08:45:18.000Z",
  //   },
  //   {
  //     id: 1319,
  //     categoryName: "FOLDER B",
  //     parentCatId: 70,
  //     tabId: 1,
  //     timemodified: "2020-08-24T08:27:09.000Z",
  //   },
  //   {
  //     id: 1318,
  //     categoryName: "A",
  //     parentCatId: 44,
  //     tabId: 1,
  //     timemodified: "2020-08-21T08:19:43.000Z",
  //   },
  //   {
  //     id: 1317,
  //     categoryName: "punit",
  //     parentCatId: 68,
  //     tabId: 1,
  //     timemodified: "2020-08-20T15:21:23.000Z",
  //   },
  //   {
  //     id: 1082,
  //     categoryName: "video",
  //     parentCatId: 1081,
  //     tabId: 1,
  //     timemodified: "2020-08-20T15:06:02.000Z",
  //   },
  //   {
  //     id: 1080,
  //     categoryName: "image",
  //     parentCatId: 1079,
  //     tabId: 1,
  //     timemodified: "2020-08-20T15:06:02.000Z",
  //   },
  //   {
  //     id: 1079,
  //     categoryName: "categoryA fu",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-20T15:06:02.000Z",
  //   },
  //   {
  //     id: 1094,
  //     categoryName: "video",
  //     parentCatId: 1092,
  //     tabId: 1,
  //     timemodified: "2020-08-20T15:06:02.000Z",
  //   },
  //   {
  //     id: 1093,
  //     categoryName: "image",
  //     parentCatId: 1091,
  //     tabId: 1,
  //     timemodified: "2020-08-20T15:06:02.000Z",
  //   },
  //   {
  //     id: 1092,
  //     categoryName: "category b fu",
  //     parentCatId: 1091,
  //     tabId: 1,
  //     timemodified: "2020-08-20T15:06:02.000Z",
  //   },
  //   {
  //     id: 46,
  //     categoryName: "DAM Cat 19-9",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-20T15:05:57.000Z",
  //   },
  //   {
  //     id: 1304,
  //     categoryName: "categoryA fu",
  //     parentCatId: 1292,
  //     tabId: 1,
  //     timemodified: "2020-08-20T15:02:42.000Z",
  //   },
  //   {
  //     id: 1314,
  //     categoryName: "CategoryA Fu",
  //     parentCatId: 61,
  //     tabId: 1,
  //     timemodified: "2020-08-20T13:14:03.000Z",
  //   },
  //   {
  //     id: 1306,
  //     categoryName: "image",
  //     parentCatId: 1304,
  //     tabId: 1,
  //     timemodified: "2020-08-20T11:19:19.000Z",
  //   },
  //   {
  //     id: 1305,
  //     categoryName: "category b fu",
  //     parentCatId: 1304,
  //     tabId: 1,
  //     timemodified: "2020-08-20T11:19:19.000Z",
  //   },
  //   {
  //     id: 1307,
  //     categoryName: "video",
  //     parentCatId: 1305,
  //     tabId: 1,
  //     timemodified: "2020-08-20T11:19:19.000Z",
  //   },
  //   {
  //     id: 1303,
  //     categoryName: "video",
  //     parentCatId: 1301,
  //     tabId: 1,
  //     timemodified: "2020-08-20T09:38:56.000Z",
  //   },
  //   {
  //     id: 1302,
  //     categoryName: "image",
  //     parentCatId: 1300,
  //     tabId: 1,
  //     timemodified: "2020-08-20T09:38:56.000Z",
  //   },
  //   {
  //     id: 1301,
  //     categoryName: "category b fu",
  //     parentCatId: 1300,
  //     tabId: 1,
  //     timemodified: "2020-08-20T09:38:56.000Z",
  //   },
  //   {
  //     id: 1300,
  //     categoryName: "categoryA fu",
  //     parentCatId: 44,
  //     tabId: 1,
  //     timemodified: "2020-08-20T09:38:56.000Z",
  //   },
  //   {
  //     id: 1294,
  //     categoryName: "category b fu",
  //     parentCatId: 1292,
  //     tabId: 1,
  //     timemodified: "2020-08-20T09:20:22.000Z",
  //   },
  //   {
  //     id: 1293,
  //     categoryName: "image",
  //     parentCatId: 1292,
  //     tabId: 1,
  //     timemodified: "2020-08-20T09:20:22.000Z",
  //   },
  //   {
  //     id: 1292,
  //     categoryName: "categoryA fu",
  //     parentCatId: 61,
  //     tabId: 1,
  //     timemodified: "2020-08-20T09:20:22.000Z",
  //   },
  //   {
  //     id: 1295,
  //     categoryName: "video",
  //     parentCatId: 1294,
  //     tabId: 1,
  //     timemodified: "2020-08-20T09:20:22.000Z",
  //   },
  //   {
  //     id: 1219,
  //     categoryName: "video",
  //     parentCatId: 1217,
  //     tabId: 1,
  //     timemodified: "2020-08-19T13:51:22.000Z",
  //   },
  //   {
  //     id: 1218,
  //     categoryName: "image",
  //     parentCatId: 1216,
  //     tabId: 1,
  //     timemodified: "2020-08-19T13:51:22.000Z",
  //   },
  //   {
  //     id: 1217,
  //     categoryName: "category b fu",
  //     parentCatId: 1216,
  //     tabId: 1,
  //     timemodified: "2020-08-19T13:51:22.000Z",
  //   },
  //   {
  //     id: 1216,
  //     categoryName: "categoryA fu",
  //     parentCatId: 87,
  //     tabId: 1,
  //     timemodified: "2020-08-19T13:51:22.000Z",
  //   },
  //   {
  //     id: 61,
  //     categoryName: "test 2400",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-18T10:54:49.000Z",
  //   },
  //   {
  //     id: 1078,
  //     categoryName: "ddddd",
  //     parentCatId: 61,
  //     tabId: 1,
  //     timemodified: "2020-08-18T10:54:49.000Z",
  //   },
  //   {
  //     id: 1122,
  //     categoryName: "image",
  //     parentCatId: 1120,
  //     tabId: 1,
  //     timemodified: "2020-08-18T10:52:20.000Z",
  //   },
  //   {
  //     id: 1121,
  //     categoryName: "category b fu",
  //     parentCatId: 1120,
  //     tabId: 1,
  //     timemodified: "2020-08-18T10:52:20.000Z",
  //   },
  //   {
  //     id: 1120,
  //     categoryName: "categoryA fu",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-18T10:52:20.000Z",
  //   },
  //   {
  //     id: 1123,
  //     categoryName: "video",
  //     parentCatId: 1121,
  //     tabId: 1,
  //     timemodified: "2020-08-18T10:52:20.000Z",
  //   },
  //   {
  //     id: 87,
  //     categoryName: "DAM 13_04",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-18T10:48:58.000Z",
  //   },
  //   {
  //     id: 1095,
  //     categoryName: "Multi-Language",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-18T06:22:01.000Z",
  //   },
  //   {
  //     id: 1095,
  //     categoryName: "Multi-Language",
  //     parentCatId: null,
  //     tabId: 3,
  //     timemodified: "2020-08-18T06:22:01.000Z",
  //   },
  //   {
  //     id: 1040,
  //     categoryName: "category b fu",
  //     parentCatId: 872,
  //     tabId: 1,
  //     timemodified: "2020-08-14T05:03:42.000Z",
  //   },
  //   {
  //     id: 838,
  //     categoryName: "Shashank Multi language",
  //     parentCatId: 872,
  //     tabId: 1,
  //     timemodified: "2020-08-14T05:03:01.000Z",
  //   },
  //   {
  //     id: 1042,
  //     categoryName: "video",
  //     parentCatId: 1040,
  //     tabId: 1,
  //     timemodified: "2020-08-14T04:54:19.000Z",
  //   },
  //   {
  //     id: 832,
  //     categoryName: "Demo Test",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-13T14:02:10.000Z",
  //   },
  //   {
  //     id: 1077,
  //     categoryName: "Multi Content",
  //     parentCatId: 838,
  //     tabId: 1,
  //     timemodified: "2020-08-13T05:28:36.000Z",
  //   },
  //   {
  //     id: 1077,
  //     categoryName: "Multi Content",
  //     parentCatId: 838,
  //     tabId: 3,
  //     timemodified: "2020-08-13T05:28:36.000Z",
  //   },
  //   {
  //     id: 887,
  //     categoryName: "Folder A",
  //     parentCatId: 886,
  //     tabId: 1,
  //     timemodified: "2020-08-13T04:43:13.000Z",
  //   },
  //   {
  //     id: 895,
  //     categoryName: "Folder C",
  //     parentCatId: 889,
  //     tabId: 1,
  //     timemodified: "2020-08-13T04:43:13.000Z",
  //   },
  //   {
  //     id: 886,
  //     categoryName: "Main Folder",
  //     parentCatId: 885,
  //     tabId: 1,
  //     timemodified: "2020-08-13T04:43:13.000Z",
  //   },
  //   {
  //     id: 894,
  //     categoryName: "Folder B",
  //     parentCatId: 889,
  //     tabId: 1,
  //     timemodified: "2020-08-13T04:43:13.000Z",
  //   },
  //   {
  //     id: 893,
  //     categoryName: "Folder A",
  //     parentCatId: 889,
  //     tabId: 1,
  //     timemodified: "2020-08-13T04:43:13.000Z",
  //   },
  //   {
  //     id: 892,
  //     categoryName: "Folder C",
  //     parentCatId: 889,
  //     tabId: 1,
  //     timemodified: "2020-08-13T04:43:13.000Z",
  //   },
  //   {
  //     id: 891,
  //     categoryName: "Folder B",
  //     parentCatId: 889,
  //     tabId: 1,
  //     timemodified: "2020-08-13T04:43:13.000Z",
  //   },
  //   {
  //     id: 890,
  //     categoryName: "Folder A",
  //     parentCatId: 889,
  //     tabId: 1,
  //     timemodified: "2020-08-13T04:43:13.000Z",
  //   },
  //   {
  //     id: 898,
  //     categoryName: "Folder C",
  //     parentCatId: 889,
  //     tabId: 1,
  //     timemodified: "2020-08-13T04:43:13.000Z",
  //   },
  //   {
  //     id: 889,
  //     categoryName: "Folder C",
  //     parentCatId: 886,
  //     tabId: 1,
  //     timemodified: "2020-08-13T04:43:13.000Z",
  //   },
  //   {
  //     id: 897,
  //     categoryName: "Folder B",
  //     parentCatId: 889,
  //     tabId: 1,
  //     timemodified: "2020-08-13T04:43:13.000Z",
  //   },
  //   {
  //     id: 888,
  //     categoryName: "Folder B",
  //     parentCatId: 886,
  //     tabId: 1,
  //     timemodified: "2020-08-13T04:43:13.000Z",
  //   },
  //   {
  //     id: 896,
  //     categoryName: "Folder A",
  //     parentCatId: 889,
  //     tabId: 1,
  //     timemodified: "2020-08-13T04:43:13.000Z",
  //   },
  //   {
  //     id: 836,
  //     categoryName: "Demo Test 5",
  //     parentCatId: 1077,
  //     tabId: 1,
  //     timemodified: "2020-08-13T04:42:58.000Z",
  //   },
  //   {
  //     id: 793,
  //     categoryName: "Main Folder",
  //     parentCatId: 1043,
  //     tabId: 1,
  //     timemodified: "2020-08-11T09:00:10.000Z",
  //   },
  //   {
  //     id: 835,
  //     categoryName: "Demo Test 4",
  //     parentCatId: 1064,
  //     tabId: 1,
  //     timemodified: "2020-08-11T08:20:49.000Z",
  //   },
  //   {
  //     id: 834,
  //     categoryName: "Demo Test 2 - 1 - 1",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-10T11:07:10.000Z",
  //   },
  //   {
  //     id: 1066,
  //     categoryName: "Folder B",
  //     parentCatId: 1064,
  //     tabId: 1,
  //     timemodified: "2020-08-10T10:53:30.000Z",
  //   },
  //   {
  //     id: 1074,
  //     categoryName: "Folder A",
  //     parentCatId: 1067,
  //     tabId: 1,
  //     timemodified: "2020-08-10T10:53:30.000Z",
  //   },
  //   {
  //     id: 1065,
  //     categoryName: "Folder A",
  //     parentCatId: 1064,
  //     tabId: 1,
  //     timemodified: "2020-08-10T10:53:30.000Z",
  //   },
  //   {
  //     id: 1073,
  //     categoryName: "Folder C",
  //     parentCatId: 1066,
  //     tabId: 1,
  //     timemodified: "2020-08-10T10:53:30.000Z",
  //   },
  //   {
  //     id: 1064,
  //     categoryName: "Main Folder",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-10T10:53:30.000Z",
  //   },
  //   {
  //     id: 1072,
  //     categoryName: "Folder B",
  //     parentCatId: 1066,
  //     tabId: 1,
  //     timemodified: "2020-08-10T10:53:30.000Z",
  //   },
  //   {
  //     id: 1071,
  //     categoryName: "Folder A",
  //     parentCatId: 1066,
  //     tabId: 1,
  //     timemodified: "2020-08-10T10:53:30.000Z",
  //   },
  //   {
  //     id: 1070,
  //     categoryName: "Folder C",
  //     parentCatId: 1065,
  //     tabId: 1,
  //     timemodified: "2020-08-10T10:53:30.000Z",
  //   },
  //   {
  //     id: 1069,
  //     categoryName: "Folder B",
  //     parentCatId: 1065,
  //     tabId: 1,
  //     timemodified: "2020-08-10T10:53:30.000Z",
  //   },
  //   {
  //     id: 1068,
  //     categoryName: "Folder A",
  //     parentCatId: 1065,
  //     tabId: 1,
  //     timemodified: "2020-08-10T10:53:30.000Z",
  //   },
  //   {
  //     id: 1076,
  //     categoryName: "Folder C",
  //     parentCatId: 1067,
  //     tabId: 1,
  //     timemodified: "2020-08-10T10:53:30.000Z",
  //   },
  //   {
  //     id: 1067,
  //     categoryName: "Folder C",
  //     parentCatId: 1064,
  //     tabId: 1,
  //     timemodified: "2020-08-10T10:53:30.000Z",
  //   },
  //   {
  //     id: 1075,
  //     categoryName: "Folder B",
  //     parentCatId: 1067,
  //     tabId: 1,
  //     timemodified: "2020-08-10T10:53:30.000Z",
  //   },
  //   {
  //     id: 1058,
  //     categoryName: "Folder A",
  //     parentCatId: 1053,
  //     tabId: 1,
  //     timemodified: "2020-08-10T10:50:10.000Z",
  //   },
  //   {
  //     id: 1057,
  //     categoryName: "Folder C",
  //     parentCatId: 1052,
  //     tabId: 1,
  //     timemodified: "2020-08-10T10:50:10.000Z",
  //   },
  //   {
  //     id: 1056,
  //     categoryName: "Folder B",
  //     parentCatId: 1052,
  //     tabId: 1,
  //     timemodified: "2020-08-10T10:50:10.000Z",
  //   },
  //   {
  //     id: 1055,
  //     categoryName: "Folder A",
  //     parentCatId: 1052,
  //     tabId: 1,
  //     timemodified: "2020-08-10T10:50:10.000Z",
  //   },
  //   {
  //     id: 1063,
  //     categoryName: "Folder C",
  //     parentCatId: 1054,
  //     tabId: 1,
  //     timemodified: "2020-08-10T10:50:10.000Z",
  //   },
  //   {
  //     id: 1054,
  //     categoryName: "Folder C",
  //     parentCatId: 1051,
  //     tabId: 1,
  //     timemodified: "2020-08-10T10:50:10.000Z",
  //   },
  //   {
  //     id: 1062,
  //     categoryName: "Folder B",
  //     parentCatId: 1054,
  //     tabId: 1,
  //     timemodified: "2020-08-10T10:50:10.000Z",
  //   },
  //   {
  //     id: 1053,
  //     categoryName: "Folder B",
  //     parentCatId: 1051,
  //     tabId: 1,
  //     timemodified: "2020-08-10T10:50:10.000Z",
  //   },
  //   {
  //     id: 1061,
  //     categoryName: "Folder A",
  //     parentCatId: 1054,
  //     tabId: 1,
  //     timemodified: "2020-08-10T10:50:10.000Z",
  //   },
  //   {
  //     id: 1052,
  //     categoryName: "Folder A",
  //     parentCatId: 1051,
  //     tabId: 1,
  //     timemodified: "2020-08-10T10:50:10.000Z",
  //   },
  //   {
  //     id: 1060,
  //     categoryName: "Folder C",
  //     parentCatId: 1053,
  //     tabId: 1,
  //     timemodified: "2020-08-10T10:50:10.000Z",
  //   },
  //   {
  //     id: 1051,
  //     categoryName: "Main Folder",
  //     parentCatId: 967,
  //     tabId: 1,
  //     timemodified: "2020-08-10T10:50:10.000Z",
  //   },
  //   {
  //     id: 1059,
  //     categoryName: "Folder B",
  //     parentCatId: 1053,
  //     tabId: 1,
  //     timemodified: "2020-08-10T10:50:10.000Z",
  //   },
  //   {
  //     id: 1046,
  //     categoryName: "video",
  //     parentCatId: 1044,
  //     tabId: 1,
  //     timemodified: "2020-08-10T06:11:08.000Z",
  //   },
  //   {
  //     id: 1045,
  //     categoryName: "image",
  //     parentCatId: 1043,
  //     tabId: 1,
  //     timemodified: "2020-08-10T06:11:08.000Z",
  //   },
  //   {
  //     id: 1044,
  //     categoryName: "category b fu",
  //     parentCatId: 1043,
  //     tabId: 1,
  //     timemodified: "2020-08-10T06:11:08.000Z",
  //   },
  //   {
  //     id: 1043,
  //     categoryName: "categoryA fu",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-10T06:11:08.000Z",
  //   },
  //   {
  //     id: 996,
  //     categoryName: "Folder A",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T17:35:01.000Z",
  //   },
  //   {
  //     id: 1005,
  //     categoryName: "Folder A",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T17:35:01.000Z",
  //   },
  //   {
  //     id: 995,
  //     categoryName: "Main Folder",
  //     parentCatId: 994,
  //     tabId: 1,
  //     timemodified: "2020-08-09T17:35:01.000Z",
  //   },
  //   {
  //     id: 1004,
  //     categoryName: "Folder C",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T17:35:01.000Z",
  //   },
  //   {
  //     id: 1003,
  //     categoryName: "Folder B",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T17:35:01.000Z",
  //   },
  //   {
  //     id: 1002,
  //     categoryName: "Folder A",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T17:35:01.000Z",
  //   },
  //   {
  //     id: 1001,
  //     categoryName: "Folder C",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T17:35:01.000Z",
  //   },
  //   {
  //     id: 999,
  //     categoryName: "Folder A",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T17:35:01.000Z",
  //   },
  //   {
  //     id: 998,
  //     categoryName: "Folder C",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T17:35:01.000Z",
  //   },
  //   {
  //     id: 1007,
  //     categoryName: "Folder C",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T17:35:01.000Z",
  //   },
  //   {
  //     id: 997,
  //     categoryName: "Folder B",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T17:35:01.000Z",
  //   },
  //   {
  //     id: 1006,
  //     categoryName: "Folder B",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T17:35:01.000Z",
  //   },
  //   {
  //     id: 994,
  //     categoryName: "000_TEST_02",
  //     parentCatId: 967,
  //     tabId: 1,
  //     timemodified: "2020-08-09T17:34:37.000Z",
  //   },
  //   {
  //     id: 988,
  //     categoryName: "Folder A",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T17:33:37.000Z",
  //   },
  //   {
  //     id: 987,
  //     categoryName: "Folder C",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T17:33:37.000Z",
  //   },
  //   {
  //     id: 986,
  //     categoryName: "Folder B",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T17:33:37.000Z",
  //   },
  //   {
  //     id: 985,
  //     categoryName: "Folder A",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T17:33:37.000Z",
  //   },
  //   {
  //     id: 993,
  //     categoryName: "Folder C",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T17:33:37.000Z",
  //   },
  //   {
  //     id: 984,
  //     categoryName: "Folder C",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T17:33:37.000Z",
  //   },
  //   {
  //     id: 992,
  //     categoryName: "Folder B",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T17:33:37.000Z",
  //   },
  //   {
  //     id: 983,
  //     categoryName: "Folder B",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T17:33:37.000Z",
  //   },
  //   {
  //     id: 991,
  //     categoryName: "Folder A",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T17:33:37.000Z",
  //   },
  //   {
  //     id: 982,
  //     categoryName: "Folder A",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T17:33:37.000Z",
  //   },
  //   {
  //     id: 990,
  //     categoryName: "Folder C",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T17:33:37.000Z",
  //   },
  //   {
  //     id: 981,
  //     categoryName: "Main Folder",
  //     parentCatId: 968,
  //     tabId: 1,
  //     timemodified: "2020-08-09T17:33:37.000Z",
  //   },
  //   {
  //     id: 989,
  //     categoryName: "Folder B",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T17:33:37.000Z",
  //   },
  //   {
  //     id: 972,
  //     categoryName: "Folder A",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T16:27:20.000Z",
  //   },
  //   {
  //     id: 980,
  //     categoryName: "Folder C",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T16:27:20.000Z",
  //   },
  //   {
  //     id: 971,
  //     categoryName: "Folder C",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T16:27:20.000Z",
  //   },
  //   {
  //     id: 979,
  //     categoryName: "Folder B",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T16:27:20.000Z",
  //   },
  //   {
  //     id: 970,
  //     categoryName: "Folder B",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T16:27:20.000Z",
  //   },
  //   {
  //     id: 978,
  //     categoryName: "Folder A",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T16:27:20.000Z",
  //   },
  //   {
  //     id: 969,
  //     categoryName: "Folder A",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T16:27:20.000Z",
  //   },
  //   {
  //     id: 977,
  //     categoryName: "Folder C",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T16:27:20.000Z",
  //   },
  //   {
  //     id: 968,
  //     categoryName: "Main Folder",
  //     parentCatId: 967,
  //     tabId: 1,
  //     timemodified: "2020-08-09T16:27:20.000Z",
  //   },
  //   {
  //     id: 976,
  //     categoryName: "Folder B",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T16:27:20.000Z",
  //   },
  //   {
  //     id: 975,
  //     categoryName: "Folder A",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T16:27:20.000Z",
  //   },
  //   {
  //     id: 974,
  //     categoryName: "Folder C",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T16:27:20.000Z",
  //   },
  //   {
  //     id: 973,
  //     categoryName: "Folder B",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T16:27:20.000Z",
  //   },
  //   {
  //     id: 967,
  //     categoryName: "000_VRY_TEST",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T16:26:56.000Z",
  //   },
  //   {
  //     id: 956,
  //     categoryName: "Folder B",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T16:22:01.000Z",
  //   },
  //   {
  //     id: 964,
  //     categoryName: "Folder A",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T16:22:01.000Z",
  //   },
  //   {
  //     id: 955,
  //     categoryName: "Folder A",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T16:22:01.000Z",
  //   },
  //   {
  //     id: 963,
  //     categoryName: "Folder C",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T16:22:01.000Z",
  //   },
  //   {
  //     id: 954,
  //     categoryName: "Main Folder",
  //     parentCatId: 907,
  //     tabId: 1,
  //     timemodified: "2020-08-09T16:22:01.000Z",
  //   },
  //   {
  //     id: 962,
  //     categoryName: "Folder B",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T16:22:01.000Z",
  //   },
  //   {
  //     id: 961,
  //     categoryName: "Folder A",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T16:22:01.000Z",
  //   },
  //   {
  //     id: 960,
  //     categoryName: "Folder C",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T16:22:01.000Z",
  //   },
  //   {
  //     id: 959,
  //     categoryName: "Folder B",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T16:22:01.000Z",
  //   },
  //   {
  //     id: 958,
  //     categoryName: "Folder A",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T16:22:01.000Z",
  //   },
  //   {
  //     id: 966,
  //     categoryName: "Folder C",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T16:22:01.000Z",
  //   },
  //   {
  //     id: 957,
  //     categoryName: "Folder C",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T16:22:01.000Z",
  //   },
  //   {
  //     id: 965,
  //     categoryName: "Folder B",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-09T16:22:01.000Z",
  //   },
  //   {
  //     id: 953,
  //     categoryName: "Aditya Test",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-07T09:53:24.000Z",
  //   },
  //   {
  //     id: 952,
  //     categoryName: "TicTacTot",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-06T14:30:08.000Z",
  //   },
  //   {
  //     id: 833,
  //     categoryName: "Demo Test 1 - 2",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-06T06:43:16.000Z",
  //   },
  //   {
  //     id: 951,
  //     categoryName: "ffff",
  //     parentCatId: 105,
  //     tabId: 1,
  //     timemodified: "2020-08-05T14:02:29.000Z",
  //   },
  //   {
  //     id: 940,
  //     categoryName: "Folder B",
  //     parentCatId: 938,
  //     tabId: 1,
  //     timemodified: "2020-08-05T13:16:05.000Z",
  //   },
  //   {
  //     id: 948,
  //     categoryName: "Folder A",
  //     parentCatId: 941,
  //     tabId: 1,
  //     timemodified: "2020-08-05T13:16:05.000Z",
  //   },
  //   {
  //     id: 939,
  //     categoryName: "Folder A",
  //     parentCatId: 938,
  //     tabId: 1,
  //     timemodified: "2020-08-05T13:16:05.000Z",
  //   },
  //   {
  //     id: 947,
  //     categoryName: "Folder C",
  //     parentCatId: 941,
  //     tabId: 1,
  //     timemodified: "2020-08-05T13:16:05.000Z",
  //   },
  //   {
  //     id: 938,
  //     categoryName: "Main Folder",
  //     parentCatId: 885,
  //     tabId: 1,
  //     timemodified: "2020-08-05T13:16:05.000Z",
  //   },
  //   {
  //     id: 946,
  //     categoryName: "Folder B",
  //     parentCatId: 941,
  //     tabId: 1,
  //     timemodified: "2020-08-05T13:16:05.000Z",
  //   },
  //   {
  //     id: 945,
  //     categoryName: "Folder A",
  //     parentCatId: 941,
  //     tabId: 1,
  //     timemodified: "2020-08-05T13:16:05.000Z",
  //   },
  //   {
  //     id: 944,
  //     categoryName: "Folder C",
  //     parentCatId: 941,
  //     tabId: 1,
  //     timemodified: "2020-08-05T13:16:05.000Z",
  //   },
  //   {
  //     id: 943,
  //     categoryName: "Folder B",
  //     parentCatId: 941,
  //     tabId: 1,
  //     timemodified: "2020-08-05T13:16:05.000Z",
  //   },
  //   {
  //     id: 942,
  //     categoryName: "Folder A",
  //     parentCatId: 941,
  //     tabId: 1,
  //     timemodified: "2020-08-05T13:16:05.000Z",
  //   },
  //   {
  //     id: 950,
  //     categoryName: "Folder C",
  //     parentCatId: 941,
  //     tabId: 1,
  //     timemodified: "2020-08-05T13:16:05.000Z",
  //   },
  //   {
  //     id: 941,
  //     categoryName: "Folder C",
  //     parentCatId: 938,
  //     tabId: 1,
  //     timemodified: "2020-08-05T13:16:05.000Z",
  //   },
  //   {
  //     id: 949,
  //     categoryName: "Folder B",
  //     parentCatId: 941,
  //     tabId: 1,
  //     timemodified: "2020-08-05T13:16:05.000Z",
  //   },
  //   {
  //     id: 924,
  //     categoryName: "test",
  //     parentCatId: 843,
  //     tabId: 1,
  //     timemodified: "2020-08-05T13:13:48.000Z",
  //   },
  //   {
  //     id: 911,
  //     categoryName: "Main Folder",
  //     parentCatId: 843,
  //     tabId: 1,
  //     timemodified: "2020-08-05T13:12:46.000Z",
  //   },
  //   {
  //     id: 919,
  //     categoryName: "Folder B",
  //     parentCatId: 914,
  //     tabId: 1,
  //     timemodified: "2020-08-05T13:12:46.000Z",
  //   },
  //   {
  //     id: 918,
  //     categoryName: "Folder A",
  //     parentCatId: 914,
  //     tabId: 1,
  //     timemodified: "2020-08-05T13:12:46.000Z",
  //   },
  //   {
  //     id: 917,
  //     categoryName: "Folder C",
  //     parentCatId: 914,
  //     tabId: 1,
  //     timemodified: "2020-08-05T13:12:46.000Z",
  //   },
  //   {
  //     id: 916,
  //     categoryName: "Folder B",
  //     parentCatId: 914,
  //     tabId: 1,
  //     timemodified: "2020-08-05T13:12:46.000Z",
  //   },
  //   {
  //     id: 915,
  //     categoryName: "Folder A",
  //     parentCatId: 914,
  //     tabId: 1,
  //     timemodified: "2020-08-05T13:12:46.000Z",
  //   },
  //   {
  //     id: 923,
  //     categoryName: "Folder C",
  //     parentCatId: 914,
  //     tabId: 1,
  //     timemodified: "2020-08-05T13:12:46.000Z",
  //   },
  //   {
  //     id: 914,
  //     categoryName: "Folder C",
  //     parentCatId: 911,
  //     tabId: 1,
  //     timemodified: "2020-08-05T13:12:46.000Z",
  //   },
  //   {
  //     id: 922,
  //     categoryName: "Folder B",
  //     parentCatId: 914,
  //     tabId: 1,
  //     timemodified: "2020-08-05T13:12:46.000Z",
  //   },
  //   {
  //     id: 913,
  //     categoryName: "Folder B",
  //     parentCatId: 911,
  //     tabId: 1,
  //     timemodified: "2020-08-05T13:12:46.000Z",
  //   },
  //   {
  //     id: 921,
  //     categoryName: "Folder A",
  //     parentCatId: 914,
  //     tabId: 1,
  //     timemodified: "2020-08-05T13:12:46.000Z",
  //   },
  //   {
  //     id: 912,
  //     categoryName: "Folder A",
  //     parentCatId: 911,
  //     tabId: 1,
  //     timemodified: "2020-08-05T13:12:46.000Z",
  //   },
  //   {
  //     id: 920,
  //     categoryName: "Folder C",
  //     parentCatId: 914,
  //     tabId: 1,
  //     timemodified: "2020-08-05T13:12:46.000Z",
  //   },
  //   {
  //     id: 910,
  //     categoryName: "node",
  //     parentCatId: 909,
  //     tabId: 1,
  //     timemodified: "2020-08-05T13:10:42.000Z",
  //   },
  //   {
  //     id: 909,
  //     categoryName: "2020_6_8",
  //     parentCatId: 908,
  //     tabId: 1,
  //     timemodified: "2020-08-05T13:10:42.000Z",
  //   },
  //   {
  //     id: 908,
  //     categoryName: "hello",
  //     parentCatId: 842,
  //     tabId: 1,
  //     timemodified: "2020-08-05T13:10:42.000Z",
  //   },
  //   {
  //     id: 907,
  //     categoryName: "node",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-05T13:07:11.000Z",
  //   },
  //   {
  //     id: 906,
  //     categoryName: "2020_6_8",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-05T13:07:11.000Z",
  //   },
  //   {
  //     id: 905,
  //     categoryName: "hello",
  //     parentCatId: 842,
  //     tabId: 1,
  //     timemodified: "2020-08-05T13:07:11.000Z",
  //   },
  //   {
  //     id: 903,
  //     categoryName: "2020_6_8",
  //     parentCatId: 902,
  //     tabId: 1,
  //     timemodified: "2020-08-05T12:26:32.000Z",
  //   },
  //   {
  //     id: 902,
  //     categoryName: "hello",
  //     parentCatId: 842,
  //     tabId: 1,
  //     timemodified: "2020-08-05T12:26:32.000Z",
  //   },
  //   {
  //     id: 904,
  //     categoryName: "node",
  //     parentCatId: 903,
  //     tabId: 1,
  //     timemodified: "2020-08-05T12:26:32.000Z",
  //   },
  //   {
  //     id: 901,
  //     categoryName: "node",
  //     parentCatId: 900,
  //     tabId: 1,
  //     timemodified: "2020-08-05T12:24:17.000Z",
  //   },
  //   {
  //     id: 900,
  //     categoryName: "2020_6_8",
  //     parentCatId: 899,
  //     tabId: 1,
  //     timemodified: "2020-08-05T12:24:17.000Z",
  //   },
  //   {
  //     id: 899,
  //     categoryName: "hello",
  //     parentCatId: 842,
  //     tabId: 1,
  //     timemodified: "2020-08-05T12:24:17.000Z",
  //   },
  //   {
  //     id: 885,
  //     categoryName: "Testing Folder",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-05T12:22:26.000Z",
  //   },
  //   {
  //     id: 879,
  //     categoryName: "Folder A",
  //     parentCatId: 875,
  //     tabId: 1,
  //     timemodified: "2020-08-05T12:20:00.000Z",
  //   },
  //   {
  //     id: 878,
  //     categoryName: "Folder C",
  //     parentCatId: 875,
  //     tabId: 1,
  //     timemodified: "2020-08-05T12:20:00.000Z",
  //   },
  //   {
  //     id: 877,
  //     categoryName: "Folder B",
  //     parentCatId: 875,
  //     tabId: 1,
  //     timemodified: "2020-08-05T12:20:00.000Z",
  //   },
  //   {
  //     id: 876,
  //     categoryName: "Folder A",
  //     parentCatId: 875,
  //     tabId: 1,
  //     timemodified: "2020-08-05T12:20:00.000Z",
  //   },
  //   {
  //     id: 884,
  //     categoryName: "Folder C",
  //     parentCatId: 875,
  //     tabId: 1,
  //     timemodified: "2020-08-05T12:20:00.000Z",
  //   },
  //   {
  //     id: 875,
  //     categoryName: "Folder C",
  //     parentCatId: 872,
  //     tabId: 1,
  //     timemodified: "2020-08-05T12:20:00.000Z",
  //   },
  //   {
  //     id: 883,
  //     categoryName: "Folder B",
  //     parentCatId: 875,
  //     tabId: 1,
  //     timemodified: "2020-08-05T12:20:00.000Z",
  //   },
  //   {
  //     id: 874,
  //     categoryName: "Folder B",
  //     parentCatId: 872,
  //     tabId: 1,
  //     timemodified: "2020-08-05T12:20:00.000Z",
  //   },
  //   {
  //     id: 882,
  //     categoryName: "Folder A",
  //     parentCatId: 875,
  //     tabId: 1,
  //     timemodified: "2020-08-05T12:20:00.000Z",
  //   },
  //   {
  //     id: 873,
  //     categoryName: "Folder A",
  //     parentCatId: 872,
  //     tabId: 1,
  //     timemodified: "2020-08-05T12:20:00.000Z",
  //   },
  //   {
  //     id: 881,
  //     categoryName: "Folder C",
  //     parentCatId: 875,
  //     tabId: 1,
  //     timemodified: "2020-08-05T12:20:00.000Z",
  //   },
  //   {
  //     id: 872,
  //     categoryName: "Main Folder",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-05T12:20:00.000Z",
  //   },
  //   {
  //     id: 880,
  //     categoryName: "Folder B",
  //     parentCatId: 875,
  //     tabId: 1,
  //     timemodified: "2020-08-05T12:20:00.000Z",
  //   },
  //   {
  //     id: 871,
  //     categoryName: "node",
  //     parentCatId: 870,
  //     tabId: 1,
  //     timemodified: "2020-08-05T12:14:23.000Z",
  //   },
  //   {
  //     id: 870,
  //     categoryName: "2020_6_8",
  //     parentCatId: 869,
  //     tabId: 1,
  //     timemodified: "2020-08-05T12:14:23.000Z",
  //   },
  //   {
  //     id: 869,
  //     categoryName: "hello",
  //     parentCatId: 842,
  //     tabId: 1,
  //     timemodified: "2020-08-05T12:14:23.000Z",
  //   },
  //   {
  //     id: 863,
  //     categoryName: "Folder A",
  //     parentCatId: 861,
  //     tabId: 1,
  //     timemodified: "2020-08-05T12:02:12.000Z",
  //   },
  //   {
  //     id: 862,
  //     categoryName: "Folder C",
  //     parentCatId: 860,
  //     tabId: 1,
  //     timemodified: "2020-08-05T12:02:12.000Z",
  //   },
  //   {
  //     id: 861,
  //     categoryName: "Folder B",
  //     parentCatId: 860,
  //     tabId: 1,
  //     timemodified: "2020-08-05T12:02:12.000Z",
  //   },
  //   {
  //     id: 860,
  //     categoryName: "Folder A",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-05T12:02:12.000Z",
  //   },
  //   {
  //     id: 868,
  //     categoryName: "Folder C",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-05T12:02:12.000Z",
  //   },
  //   {
  //     id: 859,
  //     categoryName: "Folder C",
  //     parentCatId: 856,
  //     tabId: 1,
  //     timemodified: "2020-08-05T12:02:12.000Z",
  //   },
  //   {
  //     id: 867,
  //     categoryName: "Folder B",
  //     parentCatId: 865,
  //     tabId: 1,
  //     timemodified: "2020-08-05T12:02:12.000Z",
  //   },
  //   {
  //     id: 858,
  //     categoryName: "Folder B",
  //     parentCatId: 856,
  //     tabId: 1,
  //     timemodified: "2020-08-05T12:02:12.000Z",
  //   },
  //   {
  //     id: 866,
  //     categoryName: "Folder A",
  //     parentCatId: 865,
  //     tabId: 1,
  //     timemodified: "2020-08-05T12:02:12.000Z",
  //   },
  //   {
  //     id: 857,
  //     categoryName: "Folder A",
  //     parentCatId: 856,
  //     tabId: 1,
  //     timemodified: "2020-08-05T12:02:12.000Z",
  //   },
  //   {
  //     id: 865,
  //     categoryName: "Folder C",
  //     parentCatId: 864,
  //     tabId: 1,
  //     timemodified: "2020-08-05T12:02:12.000Z",
  //   },
  //   {
  //     id: 856,
  //     categoryName: "Main Folder",
  //     parentCatId: 846,
  //     tabId: 1,
  //     timemodified: "2020-08-05T12:02:12.000Z",
  //   },
  //   {
  //     id: 864,
  //     categoryName: "Folder B",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-05T12:02:12.000Z",
  //   },
  //   {
  //     id: 847,
  //     categoryName: "Folder A",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-05T11:57:19.000Z",
  //   },
  //   {
  //     id: 855,
  //     categoryName: "Folder C",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-05T11:57:19.000Z",
  //   },
  //   {
  //     id: 846,
  //     categoryName: "Folder C",
  //     parentCatId: 843,
  //     tabId: 1,
  //     timemodified: "2020-08-05T11:57:19.000Z",
  //   },
  //   {
  //     id: 854,
  //     categoryName: "Folder B",
  //     parentCatId: 852,
  //     tabId: 1,
  //     timemodified: "2020-08-05T11:57:19.000Z",
  //   },
  //   {
  //     id: 845,
  //     categoryName: "Folder B",
  //     parentCatId: 843,
  //     tabId: 1,
  //     timemodified: "2020-08-05T11:57:19.000Z",
  //   },
  //   {
  //     id: 853,
  //     categoryName: "Folder A",
  //     parentCatId: 852,
  //     tabId: 1,
  //     timemodified: "2020-08-05T11:57:19.000Z",
  //   },
  //   {
  //     id: 852,
  //     categoryName: "Folder C",
  //     parentCatId: 851,
  //     tabId: 1,
  //     timemodified: "2020-08-05T11:57:19.000Z",
  //   },
  //   {
  //     id: 851,
  //     categoryName: "Folder B",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-05T11:57:19.000Z",
  //   },
  //   {
  //     id: 850,
  //     categoryName: "Folder A",
  //     parentCatId: 848,
  //     tabId: 1,
  //     timemodified: "2020-08-05T11:57:19.000Z",
  //   },
  //   {
  //     id: 849,
  //     categoryName: "Folder C",
  //     parentCatId: 847,
  //     tabId: 1,
  //     timemodified: "2020-08-05T11:57:19.000Z",
  //   },
  //   {
  //     id: 848,
  //     categoryName: "Folder B",
  //     parentCatId: 847,
  //     tabId: 1,
  //     timemodified: "2020-08-05T11:57:19.000Z",
  //   },
  //   {
  //     id: 844,
  //     categoryName: "Folder A",
  //     parentCatId: 843,
  //     tabId: 1,
  //     timemodified: "2020-08-05T11:57:18.000Z",
  //   },
  //   {
  //     id: 843,
  //     categoryName: "Main Folder",
  //     parentCatId: 842,
  //     tabId: 1,
  //     timemodified: "2020-08-05T11:57:18.000Z",
  //   },
  //   {
  //     id: 842,
  //     categoryName: "test folder vry",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-05T11:47:10.000Z",
  //   },
  //   {
  //     id: 841,
  //     categoryName: "Demo Test 1 - 22",
  //     parentCatId: 833,
  //     tabId: 1,
  //     timemodified: "2020-08-05T09:55:03.000Z",
  //   },
  //   {
  //     id: 47,
  //     categoryName: "file upload 3000 34",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-05T09:41:54.000Z",
  //   },
  //   {
  //     id: 62,
  //     categoryName: "DAM Assest2 1",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-05T08:50:57.000Z",
  //   },
  //   {
  //     id: 840,
  //     categoryName: "Demo Testh 7",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-05T08:36:08.000Z",
  //   },
  //   {
  //     id: 821,
  //     categoryName: "Folder B",
  //     parentCatId: 819,
  //     tabId: 1,
  //     timemodified: "2020-08-04T10:33:36.000Z",
  //   },
  //   {
  //     id: 829,
  //     categoryName: "Folder A",
  //     parentCatId: 828,
  //     tabId: 1,
  //     timemodified: "2020-08-04T10:33:36.000Z",
  //   },
  //   {
  //     id: 820,
  //     categoryName: "Folder A",
  //     parentCatId: 819,
  //     tabId: 1,
  //     timemodified: "2020-08-04T10:33:36.000Z",
  //   },
  //   {
  //     id: 828,
  //     categoryName: "Folder C",
  //     parentCatId: 827,
  //     tabId: 1,
  //     timemodified: "2020-08-04T10:33:36.000Z",
  //   },
  //   {
  //     id: 819,
  //     categoryName: "Main Folder",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-04T10:33:36.000Z",
  //   },
  //   {
  //     id: 827,
  //     categoryName: "Folder B",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-04T10:33:36.000Z",
  //   },
  //   {
  //     id: 826,
  //     categoryName: "Folder A",
  //     parentCatId: 824,
  //     tabId: 1,
  //     timemodified: "2020-08-04T10:33:36.000Z",
  //   },
  //   {
  //     id: 825,
  //     categoryName: "Folder C",
  //     parentCatId: 823,
  //     tabId: 1,
  //     timemodified: "2020-08-04T10:33:36.000Z",
  //   },
  //   {
  //     id: 824,
  //     categoryName: "Folder B",
  //     parentCatId: 823,
  //     tabId: 1,
  //     timemodified: "2020-08-04T10:33:36.000Z",
  //   },
  //   {
  //     id: 823,
  //     categoryName: "Folder A",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-04T10:33:36.000Z",
  //   },
  //   {
  //     id: 831,
  //     categoryName: "Folder C",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-04T10:33:36.000Z",
  //   },
  //   {
  //     id: 822,
  //     categoryName: "Folder C",
  //     parentCatId: 819,
  //     tabId: 1,
  //     timemodified: "2020-08-04T10:33:36.000Z",
  //   },
  //   {
  //     id: 830,
  //     categoryName: "Folder B",
  //     parentCatId: 828,
  //     tabId: 1,
  //     timemodified: "2020-08-04T10:33:36.000Z",
  //   },
  //   {
  //     id: 813,
  //     categoryName: "Folder A",
  //     parentCatId: 811,
  //     tabId: 1,
  //     timemodified: "2020-08-04T09:17:25.000Z",
  //   },
  //   {
  //     id: 812,
  //     categoryName: "Folder C",
  //     parentCatId: 810,
  //     tabId: 1,
  //     timemodified: "2020-08-04T09:17:25.000Z",
  //   },
  //   {
  //     id: 811,
  //     categoryName: "Folder B",
  //     parentCatId: 810,
  //     tabId: 1,
  //     timemodified: "2020-08-04T09:17:25.000Z",
  //   },
  //   {
  //     id: 810,
  //     categoryName: "Folder A",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-04T09:17:25.000Z",
  //   },
  //   {
  //     id: 818,
  //     categoryName: "Folder C",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-04T09:17:25.000Z",
  //   },
  //   {
  //     id: 809,
  //     categoryName: "Folder C",
  //     parentCatId: 806,
  //     tabId: 1,
  //     timemodified: "2020-08-04T09:17:25.000Z",
  //   },
  //   {
  //     id: 817,
  //     categoryName: "Folder B",
  //     parentCatId: 815,
  //     tabId: 1,
  //     timemodified: "2020-08-04T09:17:25.000Z",
  //   },
  //   {
  //     id: 808,
  //     categoryName: "Folder B",
  //     parentCatId: 806,
  //     tabId: 1,
  //     timemodified: "2020-08-04T09:17:25.000Z",
  //   },
  //   {
  //     id: 816,
  //     categoryName: "Folder A",
  //     parentCatId: 815,
  //     tabId: 1,
  //     timemodified: "2020-08-04T09:17:25.000Z",
  //   },
  //   {
  //     id: 807,
  //     categoryName: "Folder A",
  //     parentCatId: 806,
  //     tabId: 1,
  //     timemodified: "2020-08-04T09:17:25.000Z",
  //   },
  //   {
  //     id: 815,
  //     categoryName: "Folder C",
  //     parentCatId: 814,
  //     tabId: 1,
  //     timemodified: "2020-08-04T09:17:25.000Z",
  //   },
  //   {
  //     id: 806,
  //     categoryName: "Main Folder",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-04T09:17:25.000Z",
  //   },
  //   {
  //     id: 814,
  //     categoryName: "Folder B",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-04T09:17:25.000Z",
  //   },
  //   {
  //     id: 797,
  //     categoryName: "Folder A",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-04T09:02:42.000Z",
  //   },
  //   {
  //     id: 805,
  //     categoryName: "Folder C",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-04T09:02:42.000Z",
  //   },
  //   {
  //     id: 796,
  //     categoryName: "Folder C",
  //     parentCatId: 793,
  //     tabId: 1,
  //     timemodified: "2020-08-04T09:02:42.000Z",
  //   },
  //   {
  //     id: 804,
  //     categoryName: "Folder B",
  //     parentCatId: 802,
  //     tabId: 1,
  //     timemodified: "2020-08-04T09:02:42.000Z",
  //   },
  //   {
  //     id: 795,
  //     categoryName: "Folder B",
  //     parentCatId: 793,
  //     tabId: 1,
  //     timemodified: "2020-08-04T09:02:42.000Z",
  //   },
  //   {
  //     id: 803,
  //     categoryName: "Folder A",
  //     parentCatId: 802,
  //     tabId: 1,
  //     timemodified: "2020-08-04T09:02:42.000Z",
  //   },
  //   {
  //     id: 794,
  //     categoryName: "Folder A",
  //     parentCatId: 793,
  //     tabId: 1,
  //     timemodified: "2020-08-04T09:02:42.000Z",
  //   },
  //   {
  //     id: 802,
  //     categoryName: "Folder C",
  //     parentCatId: 801,
  //     tabId: 1,
  //     timemodified: "2020-08-04T09:02:42.000Z",
  //   },
  //   {
  //     id: 801,
  //     categoryName: "Folder B",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-04T09:02:42.000Z",
  //   },
  //   {
  //     id: 800,
  //     categoryName: "Folder A",
  //     parentCatId: 798,
  //     tabId: 1,
  //     timemodified: "2020-08-04T09:02:42.000Z",
  //   },
  //   {
  //     id: 799,
  //     categoryName: "Folder C",
  //     parentCatId: 797,
  //     tabId: 1,
  //     timemodified: "2020-08-04T09:02:42.000Z",
  //   },
  //   {
  //     id: 798,
  //     categoryName: "Folder B",
  //     parentCatId: 797,
  //     tabId: 1,
  //     timemodified: "2020-08-04T09:02:42.000Z",
  //   },
  //   {
  //     id: 781,
  //     categoryName: "Folder A",
  //     parentCatId: 780,
  //     tabId: 1,
  //     timemodified: "2020-08-04T08:32:55.000Z",
  //   },
  //   {
  //     id: 789,
  //     categoryName: "Folder C",
  //     parentCatId: 788,
  //     tabId: 1,
  //     timemodified: "2020-08-04T08:32:55.000Z",
  //   },
  //   {
  //     id: 780,
  //     categoryName: "Main Folder",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-04T08:32:55.000Z",
  //   },
  //   {
  //     id: 788,
  //     categoryName: "Folder B",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-04T08:32:55.000Z",
  //   },
  //   {
  //     id: 787,
  //     categoryName: "Folder A",
  //     parentCatId: 785,
  //     tabId: 1,
  //     timemodified: "2020-08-04T08:32:55.000Z",
  //   },
  //   {
  //     id: 786,
  //     categoryName: "Folder C",
  //     parentCatId: 784,
  //     tabId: 1,
  //     timemodified: "2020-08-04T08:32:55.000Z",
  //   },
  //   {
  //     id: 785,
  //     categoryName: "Folder B",
  //     parentCatId: 784,
  //     tabId: 1,
  //     timemodified: "2020-08-04T08:32:55.000Z",
  //   },
  //   {
  //     id: 784,
  //     categoryName: "Folder A",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-04T08:32:55.000Z",
  //   },
  //   {
  //     id: 792,
  //     categoryName: "Folder C",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-04T08:32:55.000Z",
  //   },
  //   {
  //     id: 783,
  //     categoryName: "Folder C",
  //     parentCatId: 780,
  //     tabId: 1,
  //     timemodified: "2020-08-04T08:32:55.000Z",
  //   },
  //   {
  //     id: 791,
  //     categoryName: "Folder B",
  //     parentCatId: 789,
  //     tabId: 1,
  //     timemodified: "2020-08-04T08:32:55.000Z",
  //   },
  //   {
  //     id: 782,
  //     categoryName: "Folder B",
  //     parentCatId: 780,
  //     tabId: 1,
  //     timemodified: "2020-08-04T08:32:55.000Z",
  //   },
  //   {
  //     id: 790,
  //     categoryName: "Folder A",
  //     parentCatId: 789,
  //     tabId: 1,
  //     timemodified: "2020-08-04T08:32:55.000Z",
  //   },
  //   {
  //     id: 779,
  //     categoryName: "Folder 2",
  //     parentCatId: 778,
  //     tabId: 1,
  //     timemodified: "2020-08-04T08:12:42.000Z",
  //   },
  //   {
  //     id: 778,
  //     categoryName: "folder 1",
  //     parentCatId: 777,
  //     tabId: 1,
  //     timemodified: "2020-08-04T08:12:42.000Z",
  //   },
  //   {
  //     id: 777,
  //     categoryName: "All content 2",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-04T08:12:41.000Z",
  //   },
  //   {
  //     id: 776,
  //     categoryName: "All content",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-04T08:07:28.000Z",
  //   },
  //   {
  //     id: 765,
  //     categoryName: "Folder B",
  //     parentCatId: 763,
  //     tabId: 1,
  //     timemodified: "2020-08-04T07:09:31.000Z",
  //   },
  //   {
  //     id: 773,
  //     categoryName: "Folder A",
  //     parentCatId: 772,
  //     tabId: 1,
  //     timemodified: "2020-08-04T07:09:31.000Z",
  //   },
  //   {
  //     id: 764,
  //     categoryName: "Folder A",
  //     parentCatId: 763,
  //     tabId: 1,
  //     timemodified: "2020-08-04T07:09:31.000Z",
  //   },
  //   {
  //     id: 772,
  //     categoryName: "Folder C",
  //     parentCatId: 771,
  //     tabId: 1,
  //     timemodified: "2020-08-04T07:09:31.000Z",
  //   },
  //   {
  //     id: 763,
  //     categoryName: "Main Folder",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-04T07:09:31.000Z",
  //   },
  //   {
  //     id: 771,
  //     categoryName: "Folder B",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-04T07:09:31.000Z",
  //   },
  //   {
  //     id: 770,
  //     categoryName: "Folder A",
  //     parentCatId: 768,
  //     tabId: 1,
  //     timemodified: "2020-08-04T07:09:31.000Z",
  //   },
  //   {
  //     id: 769,
  //     categoryName: "Folder C",
  //     parentCatId: 767,
  //     tabId: 1,
  //     timemodified: "2020-08-04T07:09:31.000Z",
  //   },
  //   {
  //     id: 768,
  //     categoryName: "Folder B",
  //     parentCatId: 767,
  //     tabId: 1,
  //     timemodified: "2020-08-04T07:09:31.000Z",
  //   },
  //   {
  //     id: 767,
  //     categoryName: "Folder A",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-04T07:09:31.000Z",
  //   },
  //   {
  //     id: 775,
  //     categoryName: "Folder C",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-04T07:09:31.000Z",
  //   },
  //   {
  //     id: 766,
  //     categoryName: "Folder C",
  //     parentCatId: 763,
  //     tabId: 1,
  //     timemodified: "2020-08-04T07:09:31.000Z",
  //   },
  //   {
  //     id: 774,
  //     categoryName: "Folder B",
  //     parentCatId: 772,
  //     tabId: 1,
  //     timemodified: "2020-08-04T07:09:31.000Z",
  //   },
  //   {
  //     id: 757,
  //     categoryName: "Folder A",
  //     parentCatId: 755,
  //     tabId: 1,
  //     timemodified: "2020-08-04T06:46:41.000Z",
  //   },
  //   {
  //     id: 756,
  //     categoryName: "Folder C",
  //     parentCatId: 754,
  //     tabId: 1,
  //     timemodified: "2020-08-04T06:46:41.000Z",
  //   },
  //   {
  //     id: 755,
  //     categoryName: "Folder B",
  //     parentCatId: 754,
  //     tabId: 1,
  //     timemodified: "2020-08-04T06:46:41.000Z",
  //   },
  //   {
  //     id: 754,
  //     categoryName: "Folder A",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-04T06:46:41.000Z",
  //   },
  //   {
  //     id: 762,
  //     categoryName: "Folder C",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-04T06:46:41.000Z",
  //   },
  //   {
  //     id: 753,
  //     categoryName: "Folder C",
  //     parentCatId: 750,
  //     tabId: 1,
  //     timemodified: "2020-08-04T06:46:41.000Z",
  //   },
  //   {
  //     id: 761,
  //     categoryName: "Folder B",
  //     parentCatId: 759,
  //     tabId: 1,
  //     timemodified: "2020-08-04T06:46:41.000Z",
  //   },
  //   {
  //     id: 752,
  //     categoryName: "Folder B",
  //     parentCatId: 750,
  //     tabId: 1,
  //     timemodified: "2020-08-04T06:46:41.000Z",
  //   },
  //   {
  //     id: 760,
  //     categoryName: "Folder A",
  //     parentCatId: 759,
  //     tabId: 1,
  //     timemodified: "2020-08-04T06:46:41.000Z",
  //   },
  //   {
  //     id: 751,
  //     categoryName: "Folder A",
  //     parentCatId: 750,
  //     tabId: 1,
  //     timemodified: "2020-08-04T06:46:41.000Z",
  //   },
  //   {
  //     id: 759,
  //     categoryName: "Folder C",
  //     parentCatId: 758,
  //     tabId: 1,
  //     timemodified: "2020-08-04T06:46:41.000Z",
  //   },
  //   {
  //     id: 750,
  //     categoryName: "Main Folder",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-04T06:46:41.000Z",
  //   },
  //   {
  //     id: 758,
  //     categoryName: "Folder B",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-04T06:46:41.000Z",
  //   },
  //   {
  //     id: 749,
  //     categoryName: "Demo Folder",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-01T11:58:10.000Z",
  //   },
  //   {
  //     id: 748,
  //     categoryName: "class 2307",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-01T11:33:33.000Z",
  //   },
  //   {
  //     id: 747,
  //     categoryName: "ContributApprove0108",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-01T11:05:53.000Z",
  //   },
  //   {
  //     id: 746,
  //     categoryName: "Contribut0108",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-01T10:56:25.000Z",
  //   },
  //   {
  //     id: 745,
  //     categoryName: "class 2307",
  //     parentCatId: 744,
  //     tabId: 1,
  //     timemodified: "2020-08-01T10:50:14.000Z",
  //   },
  //   {
  //     id: 41,
  //     categoryName: "test 692019",
  //     parentCatId: 625,
  //     tabId: 1,
  //     timemodified: "2020-08-01T10:36:23.000Z",
  //   },
  //   {
  //     id: 744,
  //     categoryName: "collect 0108",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-08-01T10:09:04.000Z",
  //   },
  //   {
  //     id: 712,
  //     categoryName: "PDF",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-07-31T10:17:04.000Z",
  //   },
  //   {
  //     id: 720,
  //     categoryName: "class 2307",
  //     parentCatId: 719,
  //     tabId: 1,
  //     timemodified: "2020-07-29T12:53:56.000Z",
  //   },
  //   {
  //     id: 719,
  //     categoryName: "DATA 29_07",
  //     parentCatId: null,
  //     tabId: 3,
  //     timemodified: "2020-07-29T12:45:33.000Z",
  //   },
  //   {
  //     id: 719,
  //     categoryName: "DATA 29_07",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-07-29T12:45:33.000Z",
  //   },
  //   {
  //     id: 715,
  //     categoryName: "hhh",
  //     parentCatId: 472,
  //     tabId: 1,
  //     timemodified: "2020-07-27T07:55:31.000Z",
  //   },
  //   {
  //     id: 632,
  //     categoryName: "Swapnali",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-07-24T15:05:25.000Z",
  //   },
  //   {
  //     id: 631,
  //     categoryName: "All content 24",
  //     parentCatId: 709,
  //     tabId: 1,
  //     timemodified: "2020-07-24T11:13:30.000Z",
  //   },
  //   {
  //     id: 472,
  //     categoryName: "image",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-07-23T17:22:21.000Z",
  //   },
  //   {
  //     id: 710,
  //     categoryName: "class 2307",
  //     parentCatId: 645,
  //     tabId: 1,
  //     timemodified: "2020-07-23T17:13:11.000Z",
  //   },
  //   {
  //     id: 711,
  //     categoryName: "Explain 23_07",
  //     parentCatId: 709,
  //     tabId: 1,
  //     timemodified: "2020-07-23T11:26:52.000Z",
  //   },
  //   {
  //     id: 709,
  //     categoryName: "folder 2307",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-07-23T11:16:53.000Z",
  //   },
  //   {
  //     id: 704,
  //     categoryName: "Folder Sub B",
  //     parentCatId: 574,
  //     tabId: 1,
  //     timemodified: "2020-07-23T07:51:46.000Z",
  //   },
  //   {
  //     id: 703,
  //     categoryName: "FOLDER B",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-07-22T11:39:11.000Z",
  //   },
  //   {
  //     id: 646,
  //     categoryName: "folder 21",
  //     parentCatId: 669,
  //     tabId: 1,
  //     timemodified: "2020-07-22T10:57:25.000Z",
  //   },
  //   {
  //     id: 669,
  //     categoryName: "folder 13_07",
  //     parentCatId: 659,
  //     tabId: 1,
  //     timemodified: "2020-07-22T10:42:55.000Z",
  //   },
  //   {
  //     id: 683,
  //     categoryName: "bjkbjbj",
  //     parentCatId: 699,
  //     tabId: 1,
  //     timemodified: "2020-07-22T10:41:57.000Z",
  //   },
  //   {
  //     id: 678,
  //     categoryName: "DAM (C& A)",
  //     parentCatId: 573,
  //     tabId: 1,
  //     timemodified: "2020-07-22T10:16:52.000Z",
  //   },
  //   {
  //     id: 659,
  //     categoryName: "image 17",
  //     parentCatId: 702,
  //     tabId: 1,
  //     timemodified: "2020-07-22T08:50:57.000Z",
  //   },
  //   {
  //     id: 698,
  //     categoryName: "FOLDER B",
  //     parentCatId: 659,
  //     tabId: 1,
  //     timemodified: "2020-07-22T08:42:16.000Z",
  //   },
  //   {
  //     id: 702,
  //     categoryName: "video",
  //     parentCatId: 659,
  //     tabId: 1,
  //     timemodified: "2020-07-22T08:41:49.000Z",
  //   },
  //   {
  //     id: 701,
  //     categoryName: "image",
  //     parentCatId: 699,
  //     tabId: 1,
  //     timemodified: "2020-07-22T05:40:37.000Z",
  //   },
  //   {
  //     id: 700,
  //     categoryName: "category b fu",
  //     parentCatId: 699,
  //     tabId: 1,
  //     timemodified: "2020-07-22T05:40:37.000Z",
  //   },
  //   {
  //     id: 699,
  //     categoryName: "categoryA fu",
  //     parentCatId: 698,
  //     tabId: 1,
  //     timemodified: "2020-07-22T05:40:37.000Z",
  //   },
  //   {
  //     id: 675,
  //     categoryName: "sub folder 14_07",
  //     parentCatId: 669,
  //     tabId: 1,
  //     timemodified: "2020-07-16T19:16:26.000Z",
  //   },
  //   {
  //     id: 671,
  //     categoryName: "ccccc",
  //     parentCatId: 659,
  //     tabId: 1,
  //     timemodified: "2020-07-16T07:20:47.000Z",
  //   },
  //   {
  //     id: 670,
  //     categoryName: "image",
  //     parentCatId: 659,
  //     tabId: 1,
  //     timemodified: "2020-07-16T07:20:47.000Z",
  //   },
  //   {
  //     id: 668,
  //     categoryName: "hvgc",
  //     parentCatId: 659,
  //     tabId: 1,
  //     timemodified: "2020-07-16T07:20:47.000Z",
  //   },
  //   {
  //     id: 665,
  //     categoryName: "jnjj",
  //     parentCatId: 659,
  //     tabId: 1,
  //     timemodified: "2020-07-16T07:20:47.000Z",
  //   },
  //   {
  //     id: 664,
  //     categoryName: "deg",
  //     parentCatId: 659,
  //     tabId: 1,
  //     timemodified: "2020-07-16T07:20:47.000Z",
  //   },
  //   {
  //     id: 673,
  //     categoryName: "jyyufy",
  //     parentCatId: 659,
  //     tabId: 1,
  //     timemodified: "2020-07-16T07:20:47.000Z",
  //   },
  //   {
  //     id: 672,
  //     categoryName: "yfyfyf",
  //     parentCatId: 659,
  //     tabId: 1,
  //     timemodified: "2020-07-16T07:20:47.000Z",
  //   },
  //   {
  //     id: 692,
  //     categoryName: "rrrrruu",
  //     parentCatId: 691,
  //     tabId: 1,
  //     timemodified: "2020-07-15T14:06:41.000Z",
  //   },
  //   {
  //     id: 690,
  //     categoryName: "dddddddd",
  //     parentCatId: 683,
  //     tabId: 1,
  //     timemodified: "2020-07-15T14:00:39.000Z",
  //   },
  //   {
  //     id: 689,
  //     categoryName: "vvv",
  //     parentCatId: 687,
  //     tabId: 1,
  //     timemodified: "2020-07-15T13:55:32.000Z",
  //   },
  //   {
  //     id: 688,
  //     categoryName: "hvhvvvcc",
  //     parentCatId: 687,
  //     tabId: 1,
  //     timemodified: "2020-07-15T13:55:00.000Z",
  //   },
  //   {
  //     id: 687,
  //     categoryName: "gfhf",
  //     parentCatId: 683,
  //     tabId: 1,
  //     timemodified: "2020-07-15T13:53:07.000Z",
  //   },
  //   {
  //     id: 682,
  //     categoryName: "b  b   n",
  //     parentCatId: 676,
  //     tabId: 1,
  //     timemodified: "2020-07-15T12:33:31.000Z",
  //   },
  //   {
  //     id: 676,
  //     categoryName: "content",
  //     parentCatId: 669,
  //     tabId: 1,
  //     timemodified: "2020-07-14T09:48:59.000Z",
  //   },
  //   {
  //     id: 661,
  //     categoryName: "category b fu",
  //     parentCatId: 660,
  //     tabId: 1,
  //     timemodified: "2020-07-13T10:37:44.000Z",
  //   },
  //   {
  //     id: 660,
  //     categoryName: "categoryA fu",
  //     parentCatId: 632,
  //     tabId: 1,
  //     timemodified: "2020-07-13T10:37:44.000Z",
  //   },
  //   {
  //     id: 663,
  //     categoryName: "video",
  //     parentCatId: 661,
  //     tabId: 1,
  //     timemodified: "2020-07-13T10:37:44.000Z",
  //   },
  //   {
  //     id: 662,
  //     categoryName: "image",
  //     parentCatId: 660,
  //     tabId: 1,
  //     timemodified: "2020-07-13T10:37:44.000Z",
  //   },
  //   {
  //     id: 654,
  //     categoryName: "asdsadsadsadsa",
  //     parentCatId: 649,
  //     tabId: 1,
  //     timemodified: "2020-07-11T07:29:50.000Z",
  //   },
  //   {
  //     id: 653,
  //     categoryName: "test",
  //     parentCatId: 650,
  //     tabId: 1,
  //     timemodified: "2020-07-11T07:28:43.000Z",
  //   },
  //   {
  //     id: 652,
  //     categoryName: "demoadsdas",
  //     parentCatId: 647,
  //     tabId: 1,
  //     timemodified: "2020-07-11T07:26:22.000Z",
  //   },
  //   {
  //     id: 651,
  //     categoryName: "demo 123",
  //     parentCatId: 646,
  //     tabId: 1,
  //     timemodified: "2020-07-11T07:25:21.000Z",
  //   },
  //   {
  //     id: 650,
  //     categoryName: "te4st324",
  //     parentCatId: 649,
  //     tabId: 1,
  //     timemodified: "2020-07-11T07:22:59.000Z",
  //   },
  //   {
  //     id: 649,
  //     categoryName: "demo 213213",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-07-11T07:22:40.000Z",
  //   },
  //   {
  //     id: 648,
  //     categoryName: "Folder2.2",
  //     parentCatId: 647,
  //     tabId: 1,
  //     timemodified: "2020-07-11T06:59:06.000Z",
  //   },
  //   {
  //     id: 647,
  //     categoryName: "folder 2.1",
  //     parentCatId: 646,
  //     tabId: 1,
  //     timemodified: "2020-07-11T06:58:51.000Z",
  //   },
  //   {
  //     id: 645,
  //     categoryName: "Folder B.1.2",
  //     parentCatId: 634,
  //     tabId: 1,
  //     timemodified: "2020-07-11T06:35:05.000Z",
  //   },
  //   {
  //     id: 644,
  //     categoryName: "Folder B 1.1.",
  //     parentCatId: 634,
  //     tabId: 1,
  //     timemodified: "2020-07-11T06:34:03.000Z",
  //   },
  //   {
  //     id: 637,
  //     categoryName: "Folder B.2",
  //     parentCatId: 635,
  //     tabId: 1,
  //     timemodified: "2020-07-11T05:22:58.000Z",
  //   },
  //   {
  //     id: 636,
  //     categoryName: "Folder A.12",
  //     parentCatId: 633,
  //     tabId: 1,
  //     timemodified: "2020-07-11T05:19:20.000Z",
  //   },
  //   {
  //     id: 635,
  //     categoryName: "Folder B.1",
  //     parentCatId: 634,
  //     tabId: 1,
  //     timemodified: "2020-07-11T05:15:18.000Z",
  //   },
  //   {
  //     id: 634,
  //     categoryName: "Folder B",
  //     parentCatId: 632,
  //     tabId: 1,
  //     timemodified: "2020-07-11T05:13:55.000Z",
  //   },
  //   {
  //     id: 633,
  //     categoryName: "FolderA",
  //     parentCatId: 632,
  //     tabId: 1,
  //     timemodified: "2020-07-11T05:13:25.000Z",
  //   },
  //   {
  //     id: 630,
  //     categoryName: "All content",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-07-10T19:29:37.000Z",
  //   },
  //   {
  //     id: 596,
  //     categoryName: "folder 06_07",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-07-10T15:26:36.000Z",
  //   },
  //   {
  //     id: 629,
  //     categoryName: "new-dam",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-07-10T15:04:06.000Z",
  //   },
  //   {
  //     id: 628,
  //     categoryName: "image",
  //     parentCatId: 625,
  //     tabId: 1,
  //     timemodified: "2020-07-10T14:54:28.000Z",
  //   },
  //   {
  //     id: 627,
  //     categoryName: "image",
  //     parentCatId: 625,
  //     tabId: 1,
  //     timemodified: "2020-07-10T12:37:13.000Z",
  //   },
  //   {
  //     id: 626,
  //     categoryName: "image",
  //     parentCatId: 625,
  //     tabId: 1,
  //     timemodified: "2020-07-10T11:49:28.000Z",
  //   },
  //   {
  //     id: 625,
  //     categoryName: "image",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-07-10T11:46:10.000Z",
  //   },
  //   {
  //     id: 624,
  //     categoryName: "image",
  //     parentCatId: 612,
  //     tabId: 1,
  //     timemodified: "2020-07-10T07:32:36.000Z",
  //   },
  //   {
  //     id: 622,
  //     categoryName: "image 5",
  //     parentCatId: 596,
  //     tabId: 1,
  //     timemodified: "2020-07-09T10:39:57.000Z",
  //   },
  //   {
  //     id: 623,
  //     categoryName: "ctrf",
  //     parentCatId: 604,
  //     tabId: 1,
  //     timemodified: "2020-07-09T10:31:46.000Z",
  //   },
  //   {
  //     id: 619,
  //     categoryName: "demo1",
  //     parentCatId: 618,
  //     tabId: 1,
  //     timemodified: "2020-07-09T06:31:26.000Z",
  //   },
  //   {
  //     id: 618,
  //     categoryName: "js",
  //     parentCatId: 616,
  //     tabId: 1,
  //     timemodified: "2020-07-09T06:31:26.000Z",
  //   },
  //   {
  //     id: 617,
  //     categoryName: "css",
  //     parentCatId: 616,
  //     tabId: 1,
  //     timemodified: "2020-07-09T06:31:26.000Z",
  //   },
  //   {
  //     id: 616,
  //     categoryName: "src",
  //     parentCatId: 614,
  //     tabId: 1,
  //     timemodified: "2020-07-09T06:31:26.000Z",
  //   },
  //   {
  //     id: 615,
  //     categoryName: "dist",
  //     parentCatId: 614,
  //     tabId: 1,
  //     timemodified: "2020-07-09T06:31:26.000Z",
  //   },
  //   {
  //     id: 614,
  //     categoryName: "GooeyTextHoverEffect-master",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-07-09T06:31:26.000Z",
  //   },
  //   {
  //     id: 621,
  //     categoryName: "demo3",
  //     parentCatId: 618,
  //     tabId: 1,
  //     timemodified: "2020-07-09T06:31:26.000Z",
  //   },
  //   {
  //     id: 620,
  //     categoryName: "demo2",
  //     parentCatId: 618,
  //     tabId: 1,
  //     timemodified: "2020-07-09T06:31:26.000Z",
  //   },
  //   {
  //     id: 613,
  //     categoryName: "test 692019",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-07-09T06:28:45.000Z",
  //   },
  //   {
  //     id: 612,
  //     categoryName: "sample",
  //     parentCatId: 596,
  //     tabId: 1,
  //     timemodified: "2020-07-09T06:05:15.000Z",
  //   },
  //   {
  //     id: 611,
  //     categoryName: "image 1",
  //     parentCatId: 596,
  //     tabId: 1,
  //     timemodified: "2020-07-09T06:04:29.000Z",
  //   },
  //   {
  //     id: 610,
  //     categoryName: "image",
  //     parentCatId: 596,
  //     tabId: 1,
  //     timemodified: "2020-07-09T06:03:53.000Z",
  //   },
  //   {
  //     id: 609,
  //     categoryName: "image",
  //     parentCatId: 596,
  //     tabId: 1,
  //     timemodified: "2020-07-09T06:03:52.000Z",
  //   },
  //   {
  //     id: 608,
  //     categoryName: "image",
  //     parentCatId: 596,
  //     tabId: 1,
  //     timemodified: "2020-07-09T06:03:50.000Z",
  //   },
  //   {
  //     id: 607,
  //     categoryName: "image",
  //     parentCatId: 596,
  //     tabId: 1,
  //     timemodified: "2020-07-09T06:03:48.000Z",
  //   },
  //   {
  //     id: 606,
  //     categoryName: "image",
  //     parentCatId: 596,
  //     tabId: 1,
  //     timemodified: "2020-07-09T06:03:48.000Z",
  //   },
  //   {
  //     id: 605,
  //     categoryName: "image",
  //     parentCatId: 596,
  //     tabId: 1,
  //     timemodified: "2020-07-08T16:01:19.000Z",
  //   },
  //   {
  //     id: 604,
  //     categoryName: "gvgvgv",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-07-08T08:40:05.000Z",
  //   },
  //   {
  //     id: 603,
  //     categoryName: "hhhhhhhhh",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-07-08T08:20:04.000Z",
  //   },
  //   {
  //     id: 602,
  //     categoryName: "",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-07-08T08:13:02.000Z",
  //   },
  //   {
  //     id: 601,
  //     categoryName: "",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-07-08T08:12:38.000Z",
  //   },
  //   {
  //     id: 600,
  //     categoryName: "ssssssssssssss",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-07-08T08:05:11.000Z",
  //   },
  //   {
  //     id: 599,
  //     categoryName: "image",
  //     parentCatId: 596,
  //     tabId: 1,
  //     timemodified: "2020-07-07T07:11:16.000Z",
  //   },
  //   {
  //     id: 598,
  //     categoryName: "Demo Assets",
  //     parentCatId: 595,
  //     tabId: 1,
  //     timemodified: "2020-07-06T13:40:27.000Z",
  //   },
  //   {
  //     id: 597,
  //     categoryName: "DAM 10_06_20",
  //     parentCatId: 596,
  //     tabId: 1,
  //     timemodified: "2020-07-06T06:42:54.000Z",
  //   },
  //   {
  //     id: 581,
  //     categoryName: "1 july 2020",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-07-03T15:06:28.000Z",
  //   },
  //   {
  //     id: 595,
  //     categoryName: "Test",
  //     parentCatId: 581,
  //     tabId: 1,
  //     timemodified: "2020-07-03T14:05:09.000Z",
  //   },
  //   {
  //     id: 594,
  //     categoryName: "Test",
  //     parentCatId: 591,
  //     tabId: 1,
  //     timemodified: "2020-07-03T14:04:39.000Z",
  //   },
  //   {
  //     id: 593,
  //     categoryName: "Test",
  //     parentCatId: 584,
  //     tabId: 1,
  //     timemodified: "2020-07-03T14:02:16.000Z",
  //   },
  //   {
  //     id: 592,
  //     categoryName: "image",
  //     parentCatId: 591,
  //     tabId: 1,
  //     timemodified: "2020-07-02T10:07:52.000Z",
  //   },
  //   {
  //     id: 591,
  //     categoryName: "tedfd",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-07-02T10:07:07.000Z",
  //   },
  //   {
  //     id: 590,
  //     categoryName: "image",
  //     parentCatId: 584,
  //     tabId: 1,
  //     timemodified: "2020-07-02T09:43:49.000Z",
  //   },
  //   {
  //     id: 589,
  //     categoryName: "image",
  //     parentCatId: 588,
  //     tabId: 1,
  //     timemodified: "2020-07-02T06:28:07.000Z",
  //   },
  //   {
  //     id: 588,
  //     categoryName: "gggvg",
  //     parentCatId: 586,
  //     tabId: 1,
  //     timemodified: "2020-07-02T06:18:56.000Z",
  //   },
  //   {
  //     id: 587,
  //     categoryName: "image",
  //     parentCatId: 586,
  //     tabId: 1,
  //     timemodified: "2020-07-02T06:12:01.000Z",
  //   },
  //   {
  //     id: 586,
  //     categoryName: "image",
  //     parentCatId: 585,
  //     tabId: 1,
  //     timemodified: "2020-07-02T06:02:10.000Z",
  //   },
  //   {
  //     id: 585,
  //     categoryName: "image",
  //     parentCatId: 584,
  //     tabId: 1,
  //     timemodified: "2020-07-02T05:58:31.000Z",
  //   },
  //   {
  //     id: 584,
  //     categoryName: "testing",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-07-02T05:23:22.000Z",
  //   },
  //   {
  //     id: 583,
  //     categoryName: "jhvv",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-07-01T14:08:11.000Z",
  //   },
  //   {
  //     id: 582,
  //     categoryName: "TestEx",
  //     parentCatId: 581,
  //     tabId: 1,
  //     timemodified: "2020-07-01T13:33:24.000Z",
  //   },
  //   {
  //     id: 580,
  //     categoryName: "DAM folder",
  //     parentCatId: 569,
  //     tabId: 1,
  //     timemodified: "2020-07-01T10:21:17.000Z",
  //   },
  //   {
  //     id: 579,
  //     categoryName: "cccccccc",
  //     parentCatId: 577,
  //     tabId: 1,
  //     timemodified: "2020-07-01T07:18:33.000Z",
  //   },
  //   {
  //     id: 578,
  //     categoryName: "testttt",
  //     parentCatId: 577,
  //     tabId: 1,
  //     timemodified: "2020-07-01T07:02:57.000Z",
  //   },
  //   {
  //     id: 577,
  //     categoryName: "image",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-07-01T07:02:22.000Z",
  //   },
  //   {
  //     id: 576,
  //     categoryName: "Folder Sub A",
  //     parentCatId: 574,
  //     tabId: 1,
  //     timemodified: "2020-07-01T07:01:06.000Z",
  //   },
  //   {
  //     id: 574,
  //     categoryName: "Folder A",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-07-01T07:00:35.000Z",
  //   },
  //   {
  //     id: 572,
  //     categoryName: "Test loading time 3",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-30T13:24:44.000Z",
  //   },
  //   {
  //     id: 573,
  //     categoryName: "dam 30_06",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-30T13:23:42.000Z",
  //   },
  //   {
  //     id: 571,
  //     categoryName: "image",
  //     parentCatId: 550,
  //     tabId: 1,
  //     timemodified: "2020-06-30T12:56:50.000Z",
  //   },
  //   {
  //     id: 570,
  //     categoryName: "dub set",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-30T12:48:54.000Z",
  //   },
  //   {
  //     id: 569,
  //     categoryName: "set 30_06",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-30T12:47:49.000Z",
  //   },
  //   {
  //     id: 568,
  //     categoryName: "video",
  //     parentCatId: 566,
  //     tabId: 1,
  //     timemodified: "2020-06-30T12:14:25.000Z",
  //   },
  //   {
  //     id: 567,
  //     categoryName: "image",
  //     parentCatId: 565,
  //     tabId: 1,
  //     timemodified: "2020-06-30T12:14:25.000Z",
  //   },
  //   {
  //     id: 566,
  //     categoryName: "category b fu",
  //     parentCatId: 565,
  //     tabId: 1,
  //     timemodified: "2020-06-30T12:14:25.000Z",
  //   },
  //   {
  //     id: 565,
  //     categoryName: "categoryA fu",
  //     parentCatId: 561,
  //     tabId: 1,
  //     timemodified: "2020-06-30T12:14:25.000Z",
  //   },
  //   {
  //     id: 562,
  //     categoryName: "category b fu",
  //     parentCatId: 561,
  //     tabId: 1,
  //     timemodified: "2020-06-30T12:07:59.000Z",
  //   },
  //   {
  //     id: 561,
  //     categoryName: "categoryA fu",
  //     parentCatId: 549,
  //     tabId: 1,
  //     timemodified: "2020-06-30T12:07:59.000Z",
  //   },
  //   {
  //     id: 564,
  //     categoryName: "video",
  //     parentCatId: 562,
  //     tabId: 1,
  //     timemodified: "2020-06-30T12:07:59.000Z",
  //   },
  //   {
  //     id: 563,
  //     categoryName: "image",
  //     parentCatId: 561,
  //     tabId: 1,
  //     timemodified: "2020-06-30T12:07:59.000Z",
  //   },
  //   {
  //     id: 560,
  //     categoryName: "image",
  //     parentCatId: 549,
  //     tabId: 1,
  //     timemodified: "2020-06-30T12:07:19.000Z",
  //   },
  //   {
  //     id: 559,
  //     categoryName: "video",
  //     parentCatId: 557,
  //     tabId: 1,
  //     timemodified: "2020-06-30T11:52:30.000Z",
  //   },
  //   {
  //     id: 558,
  //     categoryName: "image",
  //     parentCatId: 556,
  //     tabId: 1,
  //     timemodified: "2020-06-30T11:52:30.000Z",
  //   },
  //   {
  //     id: 557,
  //     categoryName: "category b fu",
  //     parentCatId: 556,
  //     tabId: 1,
  //     timemodified: "2020-06-30T11:52:30.000Z",
  //   },
  //   {
  //     id: 556,
  //     categoryName: "categoryA fu",
  //     parentCatId: 552,
  //     tabId: 1,
  //     timemodified: "2020-06-30T11:52:30.000Z",
  //   },
  //   {
  //     id: 554,
  //     categoryName: "image",
  //     parentCatId: 552,
  //     tabId: 1,
  //     timemodified: "2020-06-30T11:48:07.000Z",
  //   },
  //   {
  //     id: 553,
  //     categoryName: "category b fu",
  //     parentCatId: 552,
  //     tabId: 1,
  //     timemodified: "2020-06-30T11:48:07.000Z",
  //   },
  //   {
  //     id: 552,
  //     categoryName: "categoryA fu",
  //     parentCatId: 549,
  //     tabId: 1,
  //     timemodified: "2020-06-30T11:48:07.000Z",
  //   },
  //   {
  //     id: 555,
  //     categoryName: "video",
  //     parentCatId: 553,
  //     tabId: 1,
  //     timemodified: "2020-06-30T11:48:07.000Z",
  //   },
  //   {
  //     id: 551,
  //     categoryName: "image",
  //     parentCatId: 549,
  //     tabId: 1,
  //     timemodified: "2020-06-30T11:37:25.000Z",
  //   },
  //   {
  //     id: 550,
  //     categoryName: "Files",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-30T11:33:38.000Z",
  //   },
  //   {
  //     id: 549,
  //     categoryName: "image",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-30T11:32:50.000Z",
  //   },
  //   {
  //     id: 548,
  //     categoryName: "image",
  //     parentCatId: 547,
  //     tabId: 1,
  //     timemodified: "2020-06-30T11:32:26.000Z",
  //   },
  //   {
  //     id: 547,
  //     categoryName: "testhhhhh",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-30T11:31:42.000Z",
  //   },
  //   {
  //     id: 546,
  //     categoryName: "Test",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-30T11:19:57.000Z",
  //   },
  //   {
  //     id: 545,
  //     categoryName: "content",
  //     parentCatId: 543,
  //     tabId: 1,
  //     timemodified: "2020-06-30T10:56:55.000Z",
  //   },
  //   {
  //     id: 544,
  //     categoryName: "dub folder 30_06",
  //     parentCatId: 543,
  //     tabId: 1,
  //     timemodified: "2020-06-30T10:55:26.000Z",
  //   },
  //   {
  //     id: 543,
  //     categoryName: "Folder 30_06",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-30T10:52:40.000Z",
  //   },
  //   {
  //     id: 542,
  //     categoryName: "image",
  //     parentCatId: 519,
  //     tabId: 1,
  //     timemodified: "2020-06-30T07:29:48.000Z",
  //   },
  //   {
  //     id: 538,
  //     categoryName: "categoryA fu",
  //     parentCatId: 527,
  //     tabId: 1,
  //     timemodified: "2020-06-30T05:27:41.000Z",
  //   },
  //   {
  //     id: 541,
  //     categoryName: "video",
  //     parentCatId: 539,
  //     tabId: 1,
  //     timemodified: "2020-06-30T05:27:41.000Z",
  //   },
  //   {
  //     id: 540,
  //     categoryName: "image",
  //     parentCatId: 538,
  //     tabId: 1,
  //     timemodified: "2020-06-30T05:27:41.000Z",
  //   },
  //   {
  //     id: 539,
  //     categoryName: "category b fu",
  //     parentCatId: 538,
  //     tabId: 1,
  //     timemodified: "2020-06-30T05:27:41.000Z",
  //   },
  //   {
  //     id: 537,
  //     categoryName: "image",
  //     parentCatId: 527,
  //     tabId: 1,
  //     timemodified: "2020-06-30T05:27:12.000Z",
  //   },
  //   {
  //     id: 536,
  //     categoryName: "video",
  //     parentCatId: 534,
  //     tabId: 1,
  //     timemodified: "2020-06-29T11:44:16.000Z",
  //   },
  //   {
  //     id: 535,
  //     categoryName: "image",
  //     parentCatId: 533,
  //     tabId: 1,
  //     timemodified: "2020-06-29T11:44:16.000Z",
  //   },
  //   {
  //     id: 534,
  //     categoryName: "category b fu",
  //     parentCatId: 533,
  //     tabId: 1,
  //     timemodified: "2020-06-29T11:44:16.000Z",
  //   },
  //   {
  //     id: 533,
  //     categoryName: "categoryA fu",
  //     parentCatId: 530,
  //     tabId: 1,
  //     timemodified: "2020-06-29T11:44:16.000Z",
  //   },
  //   {
  //     id: 530,
  //     categoryName: "category b fu",
  //     parentCatId: 529,
  //     tabId: 1,
  //     timemodified: "2020-06-29T11:36:26.000Z",
  //   },
  //   {
  //     id: 529,
  //     categoryName: "categoryA fu",
  //     parentCatId: 527,
  //     tabId: 1,
  //     timemodified: "2020-06-29T11:36:26.000Z",
  //   },
  //   {
  //     id: 532,
  //     categoryName: "video",
  //     parentCatId: 530,
  //     tabId: 1,
  //     timemodified: "2020-06-29T11:36:26.000Z",
  //   },
  //   {
  //     id: 531,
  //     categoryName: "image",
  //     parentCatId: 529,
  //     tabId: 1,
  //     timemodified: "2020-06-29T11:36:26.000Z",
  //   },
  //   {
  //     id: 528,
  //     categoryName: "image",
  //     parentCatId: 527,
  //     tabId: 1,
  //     timemodified: "2020-06-29T08:36:00.000Z",
  //   },
  //   {
  //     id: 527,
  //     categoryName: "image",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-29T07:44:52.000Z",
  //   },
  //   {
  //     id: 526,
  //     categoryName: "video",
  //     parentCatId: 524,
  //     tabId: 1,
  //     timemodified: "2020-06-29T07:02:01.000Z",
  //   },
  //   {
  //     id: 525,
  //     categoryName: "image",
  //     parentCatId: 523,
  //     tabId: 1,
  //     timemodified: "2020-06-29T07:02:01.000Z",
  //   },
  //   {
  //     id: 524,
  //     categoryName: "category b fu",
  //     parentCatId: 523,
  //     tabId: 1,
  //     timemodified: "2020-06-29T07:02:01.000Z",
  //   },
  //   {
  //     id: 523,
  //     categoryName: "categoryA fu",
  //     parentCatId: 522,
  //     tabId: 1,
  //     timemodified: "2020-06-29T07:02:01.000Z",
  //   },
  //   {
  //     id: 522,
  //     categoryName: "image",
  //     parentCatId: 521,
  //     tabId: 1,
  //     timemodified: "2020-06-29T06:59:46.000Z",
  //   },
  //   {
  //     id: 521,
  //     categoryName: "Demo Assets",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-27T17:20:27.000Z",
  //   },
  //   {
  //     id: 519,
  //     categoryName: "DAM_pdf_2506",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-27T07:56:18.000Z",
  //   },
  //   {
  //     id: 518,
  //     categoryName: "DAM_pdf_2506",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-27T07:54:22.000Z",
  //   },
  //   {
  //     id: 517,
  //     categoryName: "All content",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-27T07:21:51.000Z",
  //   },
  //   {
  //     id: 516,
  //     categoryName: "image_dam",
  //     parentCatId: 513,
  //     tabId: 1,
  //     timemodified: "2020-06-26T17:04:55.000Z",
  //   },
  //   {
  //     id: 515,
  //     categoryName: "image",
  //     parentCatId: 514,
  //     tabId: 1,
  //     timemodified: "2020-06-26T13:54:47.000Z",
  //   },
  //   {
  //     id: 514,
  //     categoryName: "Demo Assets",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-26T11:03:36.000Z",
  //   },
  //   {
  //     id: 513,
  //     categoryName: "Test",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-26T11:01:34.000Z",
  //   },
  //   {
  //     id: 512,
  //     categoryName: "Test",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-26T10:01:00.000Z",
  //   },
  //   {
  //     id: 511,
  //     categoryName: "video",
  //     parentCatId: 509,
  //     tabId: 1,
  //     timemodified: "2020-06-26T10:00:41.000Z",
  //   },
  //   {
  //     id: 510,
  //     categoryName: "image",
  //     parentCatId: 508,
  //     tabId: 1,
  //     timemodified: "2020-06-26T10:00:41.000Z",
  //   },
  //   {
  //     id: 509,
  //     categoryName: "category b fu",
  //     parentCatId: 508,
  //     tabId: 1,
  //     timemodified: "2020-06-26T10:00:41.000Z",
  //   },
  //   {
  //     id: 508,
  //     categoryName: "categoryA fu",
  //     parentCatId: 488,
  //     tabId: 1,
  //     timemodified: "2020-06-26T10:00:41.000Z",
  //   },
  //   {
  //     id: 505,
  //     categoryName: "category b fu",
  //     parentCatId: 504,
  //     tabId: 1,
  //     timemodified: "2020-06-26T09:58:43.000Z",
  //   },
  //   {
  //     id: 504,
  //     categoryName: "categoryA fu",
  //     parentCatId: 500,
  //     tabId: 1,
  //     timemodified: "2020-06-26T09:58:43.000Z",
  //   },
  //   {
  //     id: 507,
  //     categoryName: "video",
  //     parentCatId: 505,
  //     tabId: 1,
  //     timemodified: "2020-06-26T09:58:43.000Z",
  //   },
  //   {
  //     id: 506,
  //     categoryName: "image",
  //     parentCatId: 504,
  //     tabId: 1,
  //     timemodified: "2020-06-26T09:58:43.000Z",
  //   },
  //   {
  //     id: 503,
  //     categoryName: "image",
  //     parentCatId: 500,
  //     tabId: 1,
  //     timemodified: "2020-06-26T09:57:59.000Z",
  //   },
  //   {
  //     id: 502,
  //     categoryName: "image",
  //     parentCatId: 500,
  //     tabId: 1,
  //     timemodified: "2020-06-26T09:57:30.000Z",
  //   },
  //   {
  //     id: 501,
  //     categoryName: "image",
  //     parentCatId: 500,
  //     tabId: 1,
  //     timemodified: "2020-06-26T09:57:04.000Z",
  //   },
  //   {
  //     id: 500,
  //     categoryName: "image",
  //     parentCatId: 498,
  //     tabId: 1,
  //     timemodified: "2020-06-26T09:45:01.000Z",
  //   },
  //   {
  //     id: 499,
  //     categoryName: "image",
  //     parentCatId: 490,
  //     tabId: 1,
  //     timemodified: "2020-06-26T09:40:37.000Z",
  //   },
  //   {
  //     id: 498,
  //     categoryName: "image",
  //     parentCatId: 490,
  //     tabId: 1,
  //     timemodified: "2020-06-26T09:40:16.000Z",
  //   },
  //   {
  //     id: 497,
  //     categoryName: "image",
  //     parentCatId: 491,
  //     tabId: 1,
  //     timemodified: "2020-06-26T09:38:01.000Z",
  //   },
  //   {
  //     id: 496,
  //     categoryName: "image",
  //     parentCatId: 491,
  //     tabId: 1,
  //     timemodified: "2020-06-26T09:37:43.000Z",
  //   },
  //   {
  //     id: 495,
  //     categoryName: "image",
  //     parentCatId: 488,
  //     tabId: 1,
  //     timemodified: "2020-06-26T09:32:53.000Z",
  //   },
  //   {
  //     id: 494,
  //     categoryName: "image",
  //     parentCatId: 488,
  //     tabId: 1,
  //     timemodified: "2020-06-26T09:24:34.000Z",
  //   },
  //   {
  //     id: 492,
  //     categoryName: "image",
  //     parentCatId: 482,
  //     tabId: 1,
  //     timemodified: "2020-06-26T09:17:44.000Z",
  //   },
  //   {
  //     id: 491,
  //     categoryName: "image",
  //     parentCatId: 483,
  //     tabId: 1,
  //     timemodified: "2020-06-26T09:13:54.000Z",
  //   },
  //   {
  //     id: 490,
  //     categoryName: "image",
  //     parentCatId: 483,
  //     tabId: 1,
  //     timemodified: "2020-06-26T09:12:04.000Z",
  //   },
  //   {
  //     id: 489,
  //     categoryName: "image",
  //     parentCatId: 483,
  //     tabId: 1,
  //     timemodified: "2020-06-26T09:11:47.000Z",
  //   },
  //   {
  //     id: 488,
  //     categoryName: "image",
  //     parentCatId: 484,
  //     tabId: 1,
  //     timemodified: "2020-06-26T08:51:18.000Z",
  //   },
  //   {
  //     id: 487,
  //     categoryName: "image",
  //     parentCatId: 484,
  //     tabId: 1,
  //     timemodified: "2020-06-26T08:50:30.000Z",
  //   },
  //   {
  //     id: 486,
  //     categoryName: "image",
  //     parentCatId: 484,
  //     tabId: 1,
  //     timemodified: "2020-06-26T08:40:49.000Z",
  //   },
  //   {
  //     id: 485,
  //     categoryName: "image",
  //     parentCatId: 484,
  //     tabId: 1,
  //     timemodified: "2020-06-26T08:37:48.000Z",
  //   },
  //   {
  //     id: 484,
  //     categoryName: "image",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-26T08:36:32.000Z",
  //   },
  //   {
  //     id: 483,
  //     categoryName: "image",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-26T08:29:01.000Z",
  //   },
  //   {
  //     id: 482,
  //     categoryName: "image",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-26T06:05:04.000Z",
  //   },
  //   {
  //     id: 481,
  //     categoryName: "image",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-26T06:04:11.000Z",
  //   },
  //   {
  //     id: 480,
  //     categoryName: "Demo Assets",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-26T05:49:03.000Z",
  //   },
  //   {
  //     id: 479,
  //     categoryName: "fffffff",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-26T05:38:00.000Z",
  //   },
  //   {
  //     id: 478,
  //     categoryName: "image",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-26T05:27:29.000Z",
  //   },
  //   {
  //     id: 477,
  //     categoryName: "DAM_pdf_2506",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-25T14:26:40.000Z",
  //   },
  //   {
  //     id: 476,
  //     categoryName: "gggggggg",
  //     parentCatId: 473,
  //     tabId: 1,
  //     timemodified: "2020-06-25T13:57:52.000Z",
  //   },
  //   {
  //     id: 475,
  //     categoryName: "admin 25_06",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-25T13:01:31.000Z",
  //   },
  //   {
  //     id: 474,
  //     categoryName: "category 25_06",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-25T12:50:23.000Z",
  //   },
  //   {
  //     id: 473,
  //     categoryName: "image",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-25T12:37:05.000Z",
  //   },
  //   {
  //     id: 471,
  //     categoryName: "con guest",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-25T12:25:43.000Z",
  //   },
  //   {
  //     id: 470,
  //     categoryName: "test",
  //     parentCatId: 465,
  //     tabId: 1,
  //     timemodified: "2020-06-25T12:07:11.000Z",
  //   },
  //   {
  //     id: 469,
  //     categoryName: "node",
  //     parentCatId: 468,
  //     tabId: 1,
  //     timemodified: "2020-06-25T12:00:05.000Z",
  //   },
  //   {
  //     id: 468,
  //     categoryName: "2020_6_8",
  //     parentCatId: 467,
  //     tabId: 1,
  //     timemodified: "2020-06-25T12:00:05.000Z",
  //   },
  //   {
  //     id: 467,
  //     categoryName: "hello",
  //     parentCatId: 466,
  //     tabId: 1,
  //     timemodified: "2020-06-25T12:00:05.000Z",
  //   },
  //   {
  //     id: 466,
  //     categoryName: "dam (c,a & g)",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-25T11:36:49.000Z",
  //   },
  //   {
  //     id: 465,
  //     categoryName: "DAM 15",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-25T11:32:46.000Z",
  //   },
  //   {
  //     id: 464,
  //     categoryName: "cat (c,m & g)",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-25T11:20:13.000Z",
  //   },
  //   {
  //     id: 463,
  //     categoryName: "folder (c,a & m)",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-25T10:19:07.000Z",
  //   },
  //   {
  //     id: 462,
  //     categoryName: "folder (m,a,g)",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-25T09:32:41.000Z",
  //   },
  //   {
  //     id: 461,
  //     categoryName: "dub role",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-25T08:49:42.000Z",
  //   },
  //   {
  //     id: 460,
  //     categoryName: "Demo Assets",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-25T08:01:28.000Z",
  //   },
  //   {
  //     id: 459,
  //     categoryName: "Role folder 25_06",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-25T07:40:34.000Z",
  //   },
  //   {
  //     id: 458,
  //     categoryName: "All content",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-25T06:59:21.000Z",
  //   },
  //   {
  //     id: 457,
  //     categoryName: "All content",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-25T06:53:35.000Z",
  //   },
  //   {
  //     id: 456,
  //     categoryName: "All content",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-25T06:34:41.000Z",
  //   },
  //   {
  //     id: 455,
  //     categoryName: "All content",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-25T06:31:17.000Z",
  //   },
  //   {
  //     id: 454,
  //     categoryName: "All content",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-25T06:25:59.000Z",
  //   },
  //   {
  //     id: 453,
  //     categoryName: "ddddddddddd",
  //     parentCatId: 452,
  //     tabId: 1,
  //     timemodified: "2020-06-25T06:24:58.000Z",
  //   },
  //   {
  //     id: 452,
  //     categoryName: "All content",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-25T06:21:39.000Z",
  //   },
  //   {
  //     id: 451,
  //     categoryName: "DAM_file_2506",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-25T06:17:53.000Z",
  //   },
  //   {
  //     id: 450,
  //     categoryName: "All content",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-25T05:22:34.000Z",
  //   },
  //   {
  //     id: 449,
  //     categoryName: "SCORM 2406",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-24T10:47:35.000Z",
  //   },
  //   {
  //     id: 448,
  //     categoryName: "test2406",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-24T10:05:55.000Z",
  //   },
  //   {
  //     id: 447,
  //     categoryName: "Test",
  //     parentCatId: 446,
  //     tabId: 1,
  //     timemodified: "2020-06-24T06:46:21.000Z",
  //   },
  //   {
  //     id: 446,
  //     categoryName: "TestFolder",
  //     parentCatId: 372,
  //     tabId: 1,
  //     timemodified: "2020-06-24T06:44:03.000Z",
  //   },
  //   {
  //     id: 445,
  //     categoryName: "hhhhhhhhhhh",
  //     parentCatId: 443,
  //     tabId: 1,
  //     timemodified: "2020-06-24T04:59:31.000Z",
  //   },
  //   {
  //     id: 444,
  //     categoryName: "Test",
  //     parentCatId: 443,
  //     tabId: 1,
  //     timemodified: "2020-06-23T14:06:09.000Z",
  //   },
  //   {
  //     id: 443,
  //     categoryName: "New Folder",
  //     parentCatId: 442,
  //     tabId: 1,
  //     timemodified: "2020-06-23T14:04:59.000Z",
  //   },
  //   {
  //     id: 442,
  //     categoryName: "New folder",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-23T13:49:06.000Z",
  //   },
  //   {
  //     id: 441,
  //     categoryName: "Folder 2306",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-23T12:05:31.000Z",
  //   },
  //   {
  //     id: 440,
  //     categoryName: "New folder",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-23T11:19:07.000Z",
  //   },
  //   {
  //     id: 439,
  //     categoryName: "DAM 10_06_20",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-23T09:52:59.000Z",
  //   },
  //   {
  //     id: 438,
  //     categoryName: "New folder",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-23T07:41:54.000Z",
  //   },
  //   {
  //     id: 437,
  //     categoryName: "New folder",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-23T07:39:35.000Z",
  //   },
  //   {
  //     id: 436,
  //     categoryName: "New folder",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-23T07:17:43.000Z",
  //   },
  //   {
  //     id: 435,
  //     categoryName: "image",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-23T07:11:12.000Z",
  //   },
  //   {
  //     id: 434,
  //     categoryName: "sub",
  //     parentCatId: 430,
  //     tabId: 1,
  //     timemodified: "2020-06-23T06:23:31.000Z",
  //   },
  //   {
  //     id: 433,
  //     categoryName: "Test",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-23T06:05:49.000Z",
  //   },
  //   {
  //     id: 432,
  //     categoryName: "gggggggggggggg",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-23T05:42:58.000Z",
  //   },
  //   {
  //     id: 431,
  //     categoryName: "Test",
  //     parentCatId: 373,
  //     tabId: 1,
  //     timemodified: "2020-06-23T05:02:21.000Z",
  //   },
  //   {
  //     id: 430,
  //     categoryName: "All content",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-22T15:03:58.000Z",
  //   },
  //   {
  //     id: 429,
  //     categoryName: "All content",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-22T14:55:56.000Z",
  //   },
  //   {
  //     id: 428,
  //     categoryName: "All content",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-22T14:22:52.000Z",
  //   },
  //   {
  //     id: 427,
  //     categoryName: "All content",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-22T14:15:00.000Z",
  //   },
  //   {
  //     id: 426,
  //     categoryName: "ttt",
  //     parentCatId: 425,
  //     tabId: 1,
  //     timemodified: "2020-06-22T12:18:52.000Z",
  //   },
  //   {
  //     id: 425,
  //     categoryName: "DAM 2206",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-22T11:27:39.000Z",
  //   },
  //   {
  //     id: 424,
  //     categoryName: "dub 22",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-22T11:11:29.000Z",
  //   },
  //   {
  //     id: 423,
  //     categoryName: "double",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-22T11:05:51.000Z",
  //   },
  //   {
  //     id: 422,
  //     categoryName: "All content",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-22T10:47:38.000Z",
  //   },
  //   {
  //     id: 421,
  //     categoryName: "Organized",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-22T10:32:28.000Z",
  //   },
  //   {
  //     id: 420,
  //     categoryName: "All content",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-22T10:28:37.000Z",
  //   },
  //   {
  //     id: 419,
  //     categoryName: "All content",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-22T09:23:43.000Z",
  //   },
  //   {
  //     id: 415,
  //     categoryName: "Con1 (22)",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-22T09:20:25.000Z",
  //   },
  //   {
  //     id: 418,
  //     categoryName: "All content",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-22T09:13:23.000Z",
  //   },
  //   {
  //     id: 417,
  //     categoryName: "All content",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-22T08:38:45.000Z",
  //   },
  //   {
  //     id: 416,
  //     categoryName: "All content",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-22T08:11:40.000Z",
  //   },
  //   {
  //     id: 414,
  //     categoryName: "Con folder 22",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-22T06:31:58.000Z",
  //   },
  //   {
  //     id: 413,
  //     categoryName: "image",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-20T12:14:15.000Z",
  //   },
  //   {
  //     id: 412,
  //     categoryName: "Test Category for L&D Role",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-20T11:40:38.000Z",
  //   },
  //   {
  //     id: 411,
  //     categoryName: "image",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-20T11:27:08.000Z",
  //   },
  //   {
  //     id: 410,
  //     categoryName: "image",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-20T09:56:15.000Z",
  //   },
  //   {
  //     id: 409,
  //     categoryName: "test",
  //     parentCatId: 408,
  //     tabId: 1,
  //     timemodified: "2020-06-20T09:03:05.000Z",
  //   },
  //   {
  //     id: 408,
  //     categoryName: "DAM 10_06_20",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-20T07:28:10.000Z",
  //   },
  //   {
  //     id: 407,
  //     categoryName: "Both 2006",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-20T07:25:07.000Z",
  //   },
  //   {
  //     id: 406,
  //     categoryName: "image",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-20T07:06:32.000Z",
  //   },
  //   {
  //     id: 405,
  //     categoryName: "image",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-20T06:35:19.000Z",
  //   },
  //   {
  //     id: 404,
  //     categoryName: "image",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-20T06:34:05.000Z",
  //   },
  //   {
  //     id: 403,
  //     categoryName: "image",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-20T06:33:46.000Z",
  //   },
  //   {
  //     id: 402,
  //     categoryName: "image",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-20T06:16:06.000Z",
  //   },
  //   {
  //     id: 401,
  //     categoryName: "image",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-20T06:12:18.000Z",
  //   },
  //   {
  //     id: 400,
  //     categoryName: "image",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-20T06:09:22.000Z",
  //   },
  //   {
  //     id: 396,
  //     categoryName: "image",
  //     parentCatId: 395,
  //     tabId: 1,
  //     timemodified: "2020-06-19T13:57:55.000Z",
  //   },
  //   {
  //     id: 395,
  //     categoryName: "image",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-19T13:57:33.000Z",
  //   },
  //   {
  //     id: 394,
  //     categoryName: "image",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-19T13:39:07.000Z",
  //   },
  //   {
  //     id: 389,
  //     categoryName: "6 September 18",
  //     parentCatId: 378,
  //     tabId: 1,
  //     timemodified: "2020-06-19T13:37:21.000Z",
  //   },
  //   {
  //     id: 388,
  //     categoryName: "3 June 18",
  //     parentCatId: 378,
  //     tabId: 1,
  //     timemodified: "2020-06-19T13:37:21.000Z",
  //   },
  //   {
  //     id: 387,
  //     categoryName: "11 February 19",
  //     parentCatId: 378,
  //     tabId: 1,
  //     timemodified: "2020-06-19T13:37:21.000Z",
  //   },
  //   {
  //     id: 393,
  //     categoryName: "Delhi",
  //     parentCatId: 390,
  //     tabId: 1,
  //     timemodified: "2020-06-19T13:37:21.000Z",
  //   },
  //   {
  //     id: 392,
  //     categoryName: "Mumbai",
  //     parentCatId: 389,
  //     tabId: 1,
  //     timemodified: "2020-06-19T13:37:21.000Z",
  //   },
  //   {
  //     id: 391,
  //     categoryName: "Delhi",
  //     parentCatId: 387,
  //     tabId: 1,
  //     timemodified: "2020-06-19T13:37:21.000Z",
  //   },
  //   {
  //     id: 390,
  //     categoryName: "9 December 18",
  //     parentCatId: 378,
  //     tabId: 1,
  //     timemodified: "2020-06-19T13:37:21.000Z",
  //   },
  //   {
  //     id: 373,
  //     categoryName: "Audit",
  //     parentCatId: 372,
  //     tabId: 1,
  //     timemodified: "2020-06-19T13:37:20.000Z",
  //   },
  //   {
  //     id: 381,
  //     categoryName: "Jan 19",
  //     parentCatId: 375,
  //     tabId: 1,
  //     timemodified: "2020-06-19T13:37:20.000Z",
  //   },
  //   {
  //     id: 372,
  //     categoryName: "Client 1",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-19T13:37:20.000Z",
  //   },
  //   {
  //     id: 380,
  //     categoryName: "Dec 18",
  //     parentCatId: 375,
  //     tabId: 1,
  //     timemodified: "2020-06-19T13:37:20.000Z",
  //   },
  //   {
  //     id: 379,
  //     categoryName: "TDS CHALLANS",
  //     parentCatId: 374,
  //     tabId: 1,
  //     timemodified: "2020-06-19T13:37:20.000Z",
  //   },
  //   {
  //     id: 378,
  //     categoryName: "GST",
  //     parentCatId: 374,
  //     tabId: 1,
  //     timemodified: "2020-06-19T13:37:20.000Z",
  //   },
  //   {
  //     id: 386,
  //     categoryName: "Filings",
  //     parentCatId: 377,
  //     tabId: 1,
  //     timemodified: "2020-06-19T13:37:20.000Z",
  //   },
  //   {
  //     id: 377,
  //     categoryName: "ROC",
  //     parentCatId: 373,
  //     tabId: 1,
  //     timemodified: "2020-06-19T13:37:20.000Z",
  //   },
  //   {
  //     id: 385,
  //     categoryName: "Sep 18",
  //     parentCatId: 375,
  //     tabId: 1,
  //     timemodified: "2020-06-19T13:37:20.000Z",
  //   },
  //   {
  //     id: 376,
  //     categoryName: "Final Documents",
  //     parentCatId: 373,
  //     tabId: 1,
  //     timemodified: "2020-06-19T13:37:20.000Z",
  //   },
  //   {
  //     id: 384,
  //     categoryName: "OCT 18",
  //     parentCatId: 375,
  //     tabId: 1,
  //     timemodified: "2020-06-19T13:37:20.000Z",
  //   },
  //   {
  //     id: 375,
  //     categoryName: "Audit Workings",
  //     parentCatId: 373,
  //     tabId: 1,
  //     timemodified: "2020-06-19T13:37:20.000Z",
  //   },
  //   {
  //     id: 383,
  //     categoryName: "NOV 18",
  //     parentCatId: 375,
  //     tabId: 1,
  //     timemodified: "2020-06-19T13:37:20.000Z",
  //   },
  //   {
  //     id: 374,
  //     categoryName: "Tax",
  //     parentCatId: 372,
  //     tabId: 1,
  //     timemodified: "2020-06-19T13:37:20.000Z",
  //   },
  //   {
  //     id: 382,
  //     categoryName: "March 19",
  //     parentCatId: 375,
  //     tabId: 1,
  //     timemodified: "2020-06-19T13:37:20.000Z",
  //   },
  //   {
  //     id: 371,
  //     categoryName: "Demo Assets",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-19T13:30:36.000Z",
  //   },
  //   {
  //     id: 370,
  //     categoryName: "folder 19",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-19T13:01:11.000Z",
  //   },
  //   {
  //     id: 369,
  //     categoryName: "Asset 19",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-19T12:42:22.000Z",
  //   },
  //   {
  //     id: 368,
  //     categoryName: "TEST UPLOAD ASSET CAT",
  //     parentCatId: 347,
  //     tabId: 1,
  //     timemodified: "2020-06-19T10:38:14.000Z",
  //   },
  //   {
  //     id: 367,
  //     categoryName: "new 1",
  //     parentCatId: 364,
  //     tabId: 1,
  //     timemodified: "2020-06-19T09:26:38.000Z",
  //   },
  //   {
  //     id: 366,
  //     categoryName: "Demo Assets",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-19T09:13:35.000Z",
  //   },
  //   {
  //     id: 365,
  //     categoryName: "sub con 19",
  //     parentCatId: 364,
  //     tabId: 1,
  //     timemodified: "2020-06-19T09:00:12.000Z",
  //   },
  //   {
  //     id: 364,
  //     categoryName: "con folder 19",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-19T08:59:52.000Z",
  //   },
  //   {
  //     id: 363,
  //     categoryName: "sub 19_06",
  //     parentCatId: 361,
  //     tabId: 1,
  //     timemodified: "2020-06-19T07:23:25.000Z",
  //   },
  //   {
  //     id: 362,
  //     categoryName: "subfolder 19_06",
  //     parentCatId: 361,
  //     tabId: 1,
  //     timemodified: "2020-06-19T07:20:12.000Z",
  //   },
  //   {
  //     id: 361,
  //     categoryName: "New dam 19_06",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-19T06:45:35.000Z",
  //   },
  //   {
  //     id: 360,
  //     categoryName: "Demo Assets",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-19T05:48:09.000Z",
  //   },
  //   {
  //     id: 357,
  //     categoryName: "category b fu",
  //     parentCatId: 356,
  //     tabId: 1,
  //     timemodified: "2020-06-18T13:44:55.000Z",
  //   },
  //   {
  //     id: 356,
  //     categoryName: "categoryA fu",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-18T13:44:55.000Z",
  //   },
  //   {
  //     id: 359,
  //     categoryName: "video",
  //     parentCatId: 357,
  //     tabId: 1,
  //     timemodified: "2020-06-18T13:44:55.000Z",
  //   },
  //   {
  //     id: 358,
  //     categoryName: "image",
  //     parentCatId: 356,
  //     tabId: 1,
  //     timemodified: "2020-06-18T13:44:55.000Z",
  //   },
  //   {
  //     id: 355,
  //     categoryName: "cat_test",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-18T12:21:05.000Z",
  //   },
  //   {
  //     id: 354,
  //     categoryName: "catad_1806+_01",
  //     parentCatId: 353,
  //     tabId: 1,
  //     timemodified: "2020-06-18T12:11:06.000Z",
  //   },
  //   {
  //     id: 353,
  //     categoryName: "cat_ad_1806",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-18T12:10:27.000Z",
  //   },
  //   {
  //     id: 352,
  //     categoryName: "cat_18_06_02",
  //     parentCatId: 350,
  //     tabId: 1,
  //     timemodified: "2020-06-18T11:54:32.000Z",
  //   },
  //   {
  //     id: 351,
  //     categoryName: "cat_18_06_01",
  //     parentCatId: 350,
  //     tabId: 1,
  //     timemodified: "2020-06-18T11:54:06.000Z",
  //   },
  //   {
  //     id: 350,
  //     categoryName: "Cat18_1806",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-18T11:53:30.000Z",
  //   },
  //   {
  //     id: 349,
  //     categoryName: "test",
  //     parentCatId: 347,
  //     tabId: 1,
  //     timemodified: "2020-06-18T07:11:50.000Z",
  //   },
  //   {
  //     id: 348,
  //     categoryName: "testing",
  //     parentCatId: 347,
  //     tabId: 1,
  //     timemodified: "2020-06-18T06:55:02.000Z",
  //   },
  //   {
  //     id: 347,
  //     categoryName: "Test Category Combo",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-17T15:54:35.000Z",
  //   },
  //   {
  //     id: 346,
  //     categoryName: "test sub category 1",
  //     parentCatId: 345,
  //     tabId: 1,
  //     timemodified: "2020-06-17T10:59:43.000Z",
  //   },
  //   {
  //     id: 345,
  //     categoryName: "Test Asset Category 1",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-17T10:31:35.000Z",
  //   },
  //   {
  //     id: 344,
  //     categoryName: "XYZ",
  //     parentCatId: 317,
  //     tabId: 1,
  //     timemodified: "2020-06-16T14:20:51.000Z",
  //   },
  //   {
  //     id: 343,
  //     categoryName: "sample",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-16T14:02:23.000Z",
  //   },
  //   {
  //     id: 342,
  //     categoryName: "DAM",
  //     parentCatId: 339,
  //     tabId: 1,
  //     timemodified: "2020-06-16T11:47:37.000Z",
  //   },
  //   {
  //     id: 341,
  //     categoryName: "DAM",
  //     parentCatId: 339,
  //     tabId: 1,
  //     timemodified: "2020-06-16T11:47:32.000Z",
  //   },
  //   {
  //     id: 340,
  //     categoryName: "DAM",
  //     parentCatId: 339,
  //     tabId: 1,
  //     timemodified: "2020-06-16T11:47:15.000Z",
  //   },
  //   {
  //     id: 339,
  //     categoryName: "Assets 1606",
  //     parentCatId: 316,
  //     tabId: 1,
  //     timemodified: "2020-06-16T11:45:15.000Z",
  //   },
  //   {
  //     id: 338,
  //     categoryName: "sub dam 15_06",
  //     parentCatId: 315,
  //     tabId: 1,
  //     timemodified: "2020-06-15T11:39:44.000Z",
  //   },
  //   {
  //     id: 317,
  //     categoryName: "Audit",
  //     parentCatId: 316,
  //     tabId: 1,
  //     timemodified: "2020-06-15T10:52:04.000Z",
  //   },
  //   {
  //     id: 325,
  //     categoryName: "Jan 19",
  //     parentCatId: 319,
  //     tabId: 1,
  //     timemodified: "2020-06-15T10:52:04.000Z",
  //   },
  //   {
  //     id: 333,
  //     categoryName: "6 September 18",
  //     parentCatId: 322,
  //     tabId: 1,
  //     timemodified: "2020-06-15T10:52:04.000Z",
  //   },
  //   {
  //     id: 316,
  //     categoryName: "Client 1",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-15T10:52:04.000Z",
  //   },
  //   {
  //     id: 324,
  //     categoryName: "Dec 18",
  //     parentCatId: 319,
  //     tabId: 1,
  //     timemodified: "2020-06-15T10:52:04.000Z",
  //   },
  //   {
  //     id: 332,
  //     categoryName: "3 June 18",
  //     parentCatId: 322,
  //     tabId: 1,
  //     timemodified: "2020-06-15T10:52:04.000Z",
  //   },
  //   {
  //     id: 323,
  //     categoryName: "TDS CHALLANS",
  //     parentCatId: 318,
  //     tabId: 1,
  //     timemodified: "2020-06-15T10:52:04.000Z",
  //   },
  //   {
  //     id: 331,
  //     categoryName: "11 February 19",
  //     parentCatId: 322,
  //     tabId: 1,
  //     timemodified: "2020-06-15T10:52:04.000Z",
  //   },
  //   {
  //     id: 322,
  //     categoryName: "GST",
  //     parentCatId: 318,
  //     tabId: 1,
  //     timemodified: "2020-06-15T10:52:04.000Z",
  //   },
  //   {
  //     id: 330,
  //     categoryName: "Filings",
  //     parentCatId: 321,
  //     tabId: 1,
  //     timemodified: "2020-06-15T10:52:04.000Z",
  //   },
  //   {
  //     id: 321,
  //     categoryName: "ROC",
  //     parentCatId: 317,
  //     tabId: 1,
  //     timemodified: "2020-06-15T10:52:04.000Z",
  //   },
  //   {
  //     id: 329,
  //     categoryName: "Sep 18",
  //     parentCatId: 319,
  //     tabId: 1,
  //     timemodified: "2020-06-15T10:52:04.000Z",
  //   },
  //   {
  //     id: 337,
  //     categoryName: "Delhi",
  //     parentCatId: 334,
  //     tabId: 1,
  //     timemodified: "2020-06-15T10:52:04.000Z",
  //   },
  //   {
  //     id: 320,
  //     categoryName: "Final Documents",
  //     parentCatId: 317,
  //     tabId: 1,
  //     timemodified: "2020-06-15T10:52:04.000Z",
  //   },
  //   {
  //     id: 328,
  //     categoryName: "OCT 18",
  //     parentCatId: 319,
  //     tabId: 1,
  //     timemodified: "2020-06-15T10:52:04.000Z",
  //   },
  //   {
  //     id: 336,
  //     categoryName: "Mumbai",
  //     parentCatId: 333,
  //     tabId: 1,
  //     timemodified: "2020-06-15T10:52:04.000Z",
  //   },
  //   {
  //     id: 319,
  //     categoryName: "Audit Workings",
  //     parentCatId: 317,
  //     tabId: 1,
  //     timemodified: "2020-06-15T10:52:04.000Z",
  //   },
  //   {
  //     id: 327,
  //     categoryName: "NOV 18",
  //     parentCatId: 319,
  //     tabId: 1,
  //     timemodified: "2020-06-15T10:52:04.000Z",
  //   },
  //   {
  //     id: 335,
  //     categoryName: "Delhi",
  //     parentCatId: 331,
  //     tabId: 1,
  //     timemodified: "2020-06-15T10:52:04.000Z",
  //   },
  //   {
  //     id: 318,
  //     categoryName: "Tax",
  //     parentCatId: 316,
  //     tabId: 1,
  //     timemodified: "2020-06-15T10:52:04.000Z",
  //   },
  //   {
  //     id: 326,
  //     categoryName: "March 19",
  //     parentCatId: 319,
  //     tabId: 1,
  //     timemodified: "2020-06-15T10:52:04.000Z",
  //   },
  //   {
  //     id: 334,
  //     categoryName: "9 December 18",
  //     parentCatId: 322,
  //     tabId: 1,
  //     timemodified: "2020-06-15T10:52:04.000Z",
  //   },
  //   {
  //     id: 315,
  //     categoryName: "DAM 15_06_20",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-15T10:26:39.000Z",
  //   },
  //   {
  //     id: 314,
  //     categoryName: "Test",
  //     parentCatId: 299,
  //     tabId: 1,
  //     timemodified: "2020-06-15T08:12:50.000Z",
  //   },
  //   {
  //     id: 313,
  //     categoryName: "",
  //     parentCatId: 312,
  //     tabId: 1,
  //     timemodified: "2020-06-13T12:11:52.000Z",
  //   },
  //   {
  //     id: 312,
  //     categoryName: "Audit Workings",
  //     parentCatId: 309,
  //     tabId: 1,
  //     timemodified: "2020-06-13T12:11:26.000Z",
  //   },
  //   {
  //     id: 311,
  //     categoryName: "Final Documents",
  //     parentCatId: 309,
  //     tabId: 1,
  //     timemodified: "2020-06-13T12:10:38.000Z",
  //   },
  //   {
  //     id: 309,
  //     categoryName: "Audit",
  //     parentCatId: 308,
  //     tabId: 1,
  //     timemodified: "2020-06-13T12:06:51.000Z",
  //   },
  //   {
  //     id: 308,
  //     categoryName: "Truebridge",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-13T12:06:51.000Z",
  //   },
  //   {
  //     id: 310,
  //     categoryName: "Tax",
  //     parentCatId: 308,
  //     tabId: 1,
  //     timemodified: "2020-06-13T12:06:51.000Z",
  //   },
  //   {
  //     id: 307,
  //     categoryName: "Tax",
  //     parentCatId: 305,
  //     tabId: 1,
  //     timemodified: "2020-06-13T12:00:47.000Z",
  //   },
  //   {
  //     id: 306,
  //     categoryName: "Audit",
  //     parentCatId: 305,
  //     tabId: 1,
  //     timemodified: "2020-06-13T12:00:47.000Z",
  //   },
  //   {
  //     id: 305,
  //     categoryName: "Finatics",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-13T12:00:47.000Z",
  //   },
  //   {
  //     id: 304,
  //     categoryName: "Images",
  //     parentCatId: 303,
  //     tabId: 1,
  //     timemodified: "2020-06-13T11:55:22.000Z",
  //   },
  //   {
  //     id: 303,
  //     categoryName: "Mitesh",
  //     parentCatId: 302,
  //     tabId: 1,
  //     timemodified: "2020-06-13T11:54:25.000Z",
  //   },
  //   {
  //     id: 302,
  //     categoryName: "New folder",
  //     parentCatId: 301,
  //     tabId: 1,
  //     timemodified: "2020-06-13T08:05:33.000Z",
  //   },
  //   {
  //     id: 301,
  //     categoryName: "Banner",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-13T07:55:31.000Z",
  //   },
  //   {
  //     id: 300,
  //     categoryName: "",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-12T13:05:27.000Z",
  //   },
  //   {
  //     id: 299,
  //     categoryName: "DAm 1206",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-12T12:44:00.000Z",
  //   },
  //   {
  //     id: 298,
  //     categoryName: "Images",
  //     parentCatId: 296,
  //     tabId: 1,
  //     timemodified: "2020-06-12T12:36:25.000Z",
  //   },
  //   {
  //     id: 297,
  //     categoryName: "PDF",
  //     parentCatId: 294,
  //     tabId: 1,
  //     timemodified: "2020-06-12T12:18:18.000Z",
  //   },
  //   {
  //     id: 296,
  //     categoryName: "PDF",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-12T12:18:00.000Z",
  //   },
  //   {
  //     id: 295,
  //     categoryName: "image",
  //     parentCatId: 293,
  //     tabId: 1,
  //     timemodified: "2020-06-12T10:18:10.000Z",
  //   },
  //   {
  //     id: 294,
  //     categoryName: "image",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-12T10:16:41.000Z",
  //   },
  //   {
  //     id: 293,
  //     categoryName: "image",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-12T10:12:30.000Z",
  //   },
  //   {
  //     id: 292,
  //     categoryName: "image",
  //     parentCatId: 278,
  //     tabId: 1,
  //     timemodified: "2020-06-12T09:42:18.000Z",
  //   },
  //   {
  //     id: 291,
  //     categoryName: "image",
  //     parentCatId: 279,
  //     tabId: 1,
  //     timemodified: "2020-06-12T09:11:54.000Z",
  //   },
  //   {
  //     id: 290,
  //     categoryName: "image",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-12T08:47:27.000Z",
  //   },
  //   {
  //     id: 289,
  //     categoryName: "image",
  //     parentCatId: 280,
  //     tabId: 1,
  //     timemodified: "2020-06-12T08:33:50.000Z",
  //   },
  //   {
  //     id: 288,
  //     categoryName: "image",
  //     parentCatId: 0,
  //     tabId: 1,
  //     timemodified: "2020-06-12T08:24:13.000Z",
  //   },
  //   {
  //     id: 287,
  //     categoryName: "image",
  //     parentCatId: 0,
  //     tabId: 1,
  //     timemodified: "2020-06-12T08:18:17.000Z",
  //   },
  //   {
  //     id: 285,
  //     categoryName: "image",
  //     parentCatId: 283,
  //     tabId: 1,
  //     timemodified: "2020-06-12T07:57:00.000Z",
  //   },
  //   {
  //     id: 284,
  //     categoryName: "category b fu",
  //     parentCatId: 283,
  //     tabId: 1,
  //     timemodified: "2020-06-12T07:57:00.000Z",
  //   },
  //   {
  //     id: 283,
  //     categoryName: "categoryA fu",
  //     parentCatId: 0,
  //     tabId: 1,
  //     timemodified: "2020-06-12T07:57:00.000Z",
  //   },
  //   {
  //     id: 286,
  //     categoryName: "video",
  //     parentCatId: 284,
  //     tabId: 1,
  //     timemodified: "2020-06-12T07:57:00.000Z",
  //   },
  //   {
  //     id: 282,
  //     categoryName: "image",
  //     parentCatId: 0,
  //     tabId: 1,
  //     timemodified: "2020-06-12T07:46:29.000Z",
  //   },
  //   {
  //     id: 281,
  //     categoryName: "file upload",
  //     parentCatId: 273,
  //     tabId: 1,
  //     timemodified: "2020-06-12T06:28:30.000Z",
  //   },
  //   {
  //     id: 280,
  //     categoryName: "file upload",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-12T06:26:12.000Z",
  //   },
  //   {
  //     id: 279,
  //     categoryName: "file upload",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-12T06:21:08.000Z",
  //   },
  //   {
  //     id: 278,
  //     categoryName: "file upload",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-12T06:14:36.000Z",
  //   },
  //   {
  //     id: 275,
  //     categoryName: "Test ABC",
  //     parentCatId: 274,
  //     tabId: 1,
  //     timemodified: "2020-06-12T05:50:12.000Z",
  //   },
  //   {
  //     id: 277,
  //     categoryName: "Test SUb Category",
  //     parentCatId: 274,
  //     tabId: 1,
  //     timemodified: "2020-06-12T05:30:22.000Z",
  //   },
  //   {
  //     id: 276,
  //     categoryName: "file upload",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-12T05:16:41.000Z",
  //   },
  //   {
  //     id: 274,
  //     categoryName: "Test Asset Category 11-06-20",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-11T11:59:45.000Z",
  //   },
  //   {
  //     id: 273,
  //     categoryName: "Images",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-11T10:04:08.000Z",
  //   },
  //   {
  //     id: 272,
  //     categoryName: "DAM 10_06_20",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-10T11:37:37.000Z",
  //   },
  //   {
  //     id: 269,
  //     categoryName: "category b fu",
  //     parentCatId: 268,
  //     tabId: 1,
  //     timemodified: "2020-06-10T09:59:29.000Z",
  //   },
  //   {
  //     id: 268,
  //     categoryName: "categoryA fu",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-10T09:59:29.000Z",
  //   },
  //   {
  //     id: 271,
  //     categoryName: "video",
  //     parentCatId: 269,
  //     tabId: 1,
  //     timemodified: "2020-06-10T09:59:29.000Z",
  //   },
  //   {
  //     id: 270,
  //     categoryName: "image",
  //     parentCatId: 268,
  //     tabId: 1,
  //     timemodified: "2020-06-10T09:59:29.000Z",
  //   },
  //   {
  //     id: 267,
  //     categoryName: "video",
  //     parentCatId: 265,
  //     tabId: 1,
  //     timemodified: "2020-06-10T09:27:31.000Z",
  //   },
  //   {
  //     id: 266,
  //     categoryName: "image",
  //     parentCatId: 264,
  //     tabId: 1,
  //     timemodified: "2020-06-10T09:27:31.000Z",
  //   },
  //   {
  //     id: 265,
  //     categoryName: "category b fu",
  //     parentCatId: 264,
  //     tabId: 1,
  //     timemodified: "2020-06-10T09:27:31.000Z",
  //   },
  //   {
  //     id: 264,
  //     categoryName: "categoryA fu",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-10T09:27:31.000Z",
  //   },
  //   {
  //     id: 263,
  //     categoryName: "DAM",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-10T08:50:01.000Z",
  //   },
  //   {
  //     id: 262,
  //     categoryName: "Demo Assets",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-10T07:52:01.000Z",
  //   },
  //   {
  //     id: 261,
  //     categoryName: "video",
  //     parentCatId: 259,
  //     tabId: 1,
  //     timemodified: "2020-06-10T07:48:44.000Z",
  //   },
  //   {
  //     id: 260,
  //     categoryName: "image",
  //     parentCatId: 258,
  //     tabId: 1,
  //     timemodified: "2020-06-10T07:48:44.000Z",
  //   },
  //   {
  //     id: 259,
  //     categoryName: "category b fu",
  //     parentCatId: 258,
  //     tabId: 1,
  //     timemodified: "2020-06-10T07:48:44.000Z",
  //   },
  //   {
  //     id: 258,
  //     categoryName: "categoryA fu",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-10T07:48:44.000Z",
  //   },
  //   {
  //     id: 257,
  //     categoryName: "PDF",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-10T07:48:13.000Z",
  //   },
  //   {
  //     id: 253,
  //     categoryName: "Demo Assets",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-10T07:34:50.000Z",
  //   },
  //   {
  //     id: 252,
  //     categoryName: "PDF",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-10T07:27:17.000Z",
  //   },
  //   {
  //     id: 250,
  //     categoryName: "image",
  //     parentCatId: 248,
  //     tabId: 1,
  //     timemodified: "2020-06-10T07:23:00.000Z",
  //   },
  //   {
  //     id: 249,
  //     categoryName: "category b fu",
  //     parentCatId: 248,
  //     tabId: 1,
  //     timemodified: "2020-06-10T07:23:00.000Z",
  //   },
  //   {
  //     id: 248,
  //     categoryName: "categoryA fu",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-10T07:23:00.000Z",
  //   },
  //   {
  //     id: 251,
  //     categoryName: "video",
  //     parentCatId: 249,
  //     tabId: 1,
  //     timemodified: "2020-06-10T07:23:00.000Z",
  //   },
  //   {
  //     id: 247,
  //     categoryName: "video",
  //     parentCatId: 245,
  //     tabId: 1,
  //     timemodified: "2020-06-10T07:17:22.000Z",
  //   },
  //   {
  //     id: 246,
  //     categoryName: "image",
  //     parentCatId: 244,
  //     tabId: 1,
  //     timemodified: "2020-06-10T07:17:22.000Z",
  //   },
  //   {
  //     id: 245,
  //     categoryName: "category b fu",
  //     parentCatId: 244,
  //     tabId: 1,
  //     timemodified: "2020-06-10T07:17:22.000Z",
  //   },
  //   {
  //     id: 244,
  //     categoryName: "categoryA fu",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-10T07:17:22.000Z",
  //   },
  //   {
  //     id: 239,
  //     categoryName: "image",
  //     parentCatId: 237,
  //     tabId: 1,
  //     timemodified: "2020-06-10T07:05:25.000Z",
  //   },
  //   {
  //     id: 238,
  //     categoryName: "category b fu",
  //     parentCatId: 237,
  //     tabId: 1,
  //     timemodified: "2020-06-10T07:05:25.000Z",
  //   },
  //   {
  //     id: 237,
  //     categoryName: "categoryA fu",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-10T07:05:25.000Z",
  //   },
  //   {
  //     id: 240,
  //     categoryName: "video",
  //     parentCatId: 238,
  //     tabId: 1,
  //     timemodified: "2020-06-10T07:05:25.000Z",
  //   },
  //   {
  //     id: 236,
  //     categoryName: "video",
  //     parentCatId: 234,
  //     tabId: 1,
  //     timemodified: "2020-06-10T07:02:27.000Z",
  //   },
  //   {
  //     id: 235,
  //     categoryName: "image",
  //     parentCatId: 233,
  //     tabId: 1,
  //     timemodified: "2020-06-10T07:02:27.000Z",
  //   },
  //   {
  //     id: 234,
  //     categoryName: "category b fu",
  //     parentCatId: 233,
  //     tabId: 1,
  //     timemodified: "2020-06-10T07:02:27.000Z",
  //   },
  //   {
  //     id: 233,
  //     categoryName: "categoryA fu",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-10T07:02:27.000Z",
  //   },
  //   {
  //     id: 231,
  //     categoryName: "2020_6_8",
  //     parentCatId: 230,
  //     tabId: 1,
  //     timemodified: "2020-06-10T06:46:33.000Z",
  //   },
  //   {
  //     id: 230,
  //     categoryName: "hello",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-10T06:46:33.000Z",
  //   },
  //   {
  //     id: 232,
  //     categoryName: "node",
  //     parentCatId: 231,
  //     tabId: 1,
  //     timemodified: "2020-06-10T06:46:33.000Z",
  //   },
  //   {
  //     id: 229,
  //     categoryName: "node",
  //     parentCatId: 228,
  //     tabId: 1,
  //     timemodified: "2020-06-10T06:41:29.000Z",
  //   },
  //   {
  //     id: 228,
  //     categoryName: "2020_6_8",
  //     parentCatId: 227,
  //     tabId: 1,
  //     timemodified: "2020-06-10T06:41:28.000Z",
  //   },
  //   {
  //     id: 227,
  //     categoryName: "hello",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-10T06:41:28.000Z",
  //   },
  //   {
  //     id: 223,
  //     categoryName: "categoryA fu",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-10T06:30:39.000Z",
  //   },
  //   {
  //     id: 226,
  //     categoryName: "video",
  //     parentCatId: 224,
  //     tabId: 1,
  //     timemodified: "2020-06-10T06:30:39.000Z",
  //   },
  //   {
  //     id: 225,
  //     categoryName: "image",
  //     parentCatId: 223,
  //     tabId: 1,
  //     timemodified: "2020-06-10T06:30:39.000Z",
  //   },
  //   {
  //     id: 224,
  //     categoryName: "category b fu",
  //     parentCatId: 223,
  //     tabId: 1,
  //     timemodified: "2020-06-10T06:30:39.000Z",
  //   },
  //   {
  //     id: 222,
  //     categoryName: "video",
  //     parentCatId: 220,
  //     tabId: 1,
  //     timemodified: "2020-06-10T06:27:16.000Z",
  //   },
  //   {
  //     id: 221,
  //     categoryName: "image",
  //     parentCatId: 219,
  //     tabId: 1,
  //     timemodified: "2020-06-10T06:27:16.000Z",
  //   },
  //   {
  //     id: 220,
  //     categoryName: "category b fu",
  //     parentCatId: 219,
  //     tabId: 1,
  //     timemodified: "2020-06-10T06:27:16.000Z",
  //   },
  //   {
  //     id: 219,
  //     categoryName: "categoryA fu",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-10T06:27:16.000Z",
  //   },
  //   {
  //     id: 212,
  //     categoryName: "categoryA fu",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-10T06:15:59.000Z",
  //   },
  //   {
  //     id: 215,
  //     categoryName: "video",
  //     parentCatId: 213,
  //     tabId: 1,
  //     timemodified: "2020-06-10T06:15:59.000Z",
  //   },
  //   {
  //     id: 214,
  //     categoryName: "image",
  //     parentCatId: 212,
  //     tabId: 1,
  //     timemodified: "2020-06-10T06:15:59.000Z",
  //   },
  //   {
  //     id: 213,
  //     categoryName: "category b fu",
  //     parentCatId: 212,
  //     tabId: 1,
  //     timemodified: "2020-06-10T06:15:59.000Z",
  //   },
  //   {
  //     id: 211,
  //     categoryName: "sample",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-10T06:09:03.000Z",
  //   },
  //   {
  //     id: 201,
  //     categoryName: "video",
  //     parentCatId: 199,
  //     tabId: 1,
  //     timemodified: "2020-06-09T09:00:43.000Z",
  //   },
  //   {
  //     id: 199,
  //     categoryName: "category b fu",
  //     parentCatId: 198,
  //     tabId: 1,
  //     timemodified: "2020-06-09T09:00:43.000Z",
  //   },
  //   {
  //     id: 198,
  //     categoryName: "categoryA fu",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-09T09:00:43.000Z",
  //   },
  //   {
  //     id: 194,
  //     categoryName: "categoryA fu",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-09T08:17:35.000Z",
  //   },
  //   {
  //     id: 197,
  //     categoryName: "video",
  //     parentCatId: 195,
  //     tabId: 1,
  //     timemodified: "2020-06-09T08:17:35.000Z",
  //   },
  //   {
  //     id: 196,
  //     categoryName: "image",
  //     parentCatId: 194,
  //     tabId: 1,
  //     timemodified: "2020-06-09T08:17:35.000Z",
  //   },
  //   {
  //     id: 195,
  //     categoryName: "category b fu",
  //     parentCatId: 194,
  //     tabId: 1,
  //     timemodified: "2020-06-09T08:17:35.000Z",
  //   },
  //   {
  //     id: 193,
  //     categoryName: "image",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-09T07:33:19.000Z",
  //   },
  //   {
  //     id: 192,
  //     categoryName: "video",
  //     parentCatId: 190,
  //     tabId: 1,
  //     timemodified: "2020-06-09T07:26:06.000Z",
  //   },
  //   {
  //     id: 191,
  //     categoryName: "image",
  //     parentCatId: 189,
  //     tabId: 1,
  //     timemodified: "2020-06-09T07:26:06.000Z",
  //   },
  //   {
  //     id: 190,
  //     categoryName: "category b fu",
  //     parentCatId: 189,
  //     tabId: 1,
  //     timemodified: "2020-06-09T07:26:06.000Z",
  //   },
  //   {
  //     id: 189,
  //     categoryName: "categoryA fu",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-09T07:26:06.000Z",
  //   },
  //   {
  //     id: 186,
  //     categoryName: "category b fu",
  //     parentCatId: 185,
  //     tabId: 1,
  //     timemodified: "2020-06-09T07:20:22.000Z",
  //   },
  //   {
  //     id: 185,
  //     categoryName: "categoryA fu",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-09T07:20:22.000Z",
  //   },
  //   {
  //     id: 188,
  //     categoryName: "video",
  //     parentCatId: 186,
  //     tabId: 1,
  //     timemodified: "2020-06-09T07:20:22.000Z",
  //   },
  //   {
  //     id: 187,
  //     categoryName: "image",
  //     parentCatId: 185,
  //     tabId: 1,
  //     timemodified: "2020-06-09T07:20:22.000Z",
  //   },
  //   {
  //     id: 184,
  //     categoryName: "sample",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-09T07:06:06.000Z",
  //   },
  //   {
  //     id: 183,
  //     categoryName: "sample",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-09T07:04:31.000Z",
  //   },
  //   {
  //     id: 182,
  //     categoryName: "node",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-09T06:55:39.000Z",
  //   },
  //   {
  //     id: 181,
  //     categoryName: "node",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-09T06:54:21.000Z",
  //   },
  //   {
  //     id: 180,
  //     categoryName: "node",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-09T06:53:19.000Z",
  //   },
  //   {
  //     id: 179,
  //     categoryName: "node",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-09T06:51:42.000Z",
  //   },
  //   {
  //     id: 178,
  //     categoryName: "node",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-09T06:50:18.000Z",
  //   },
  //   {
  //     id: 177,
  //     categoryName: "node",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-09T06:49:39.000Z",
  //   },
  //   {
  //     id: 176,
  //     categoryName: "node",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-09T06:48:12.000Z",
  //   },
  //   {
  //     id: 175,
  //     categoryName: "node",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-09T06:47:30.000Z",
  //   },
  //   {
  //     id: 174,
  //     categoryName: "node",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-09T06:45:38.000Z",
  //   },
  //   {
  //     id: 173,
  //     categoryName: "node",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-09T06:42:12.000Z",
  //   },
  //   {
  //     id: 172,
  //     categoryName: "node",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-09T06:40:20.000Z",
  //   },
  //   {
  //     id: 171,
  //     categoryName: "node",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-09T06:37:19.000Z",
  //   },
  //   {
  //     id: 170,
  //     categoryName: "node",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-09T06:33:43.000Z",
  //   },
  //   {
  //     id: 169,
  //     categoryName: "node",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-09T06:25:34.000Z",
  //   },
  //   {
  //     id: 168,
  //     categoryName: "node",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-09T06:22:50.000Z",
  //   },
  //   {
  //     id: 167,
  //     categoryName: "node",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-09T06:21:50.000Z",
  //   },
  //   {
  //     id: 166,
  //     categoryName: "node",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-09T06:20:19.000Z",
  //   },
  //   {
  //     id: 165,
  //     categoryName: "node",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-09T06:11:13.000Z",
  //   },
  //   {
  //     id: 164,
  //     categoryName: "node",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-09T06:04:40.000Z",
  //   },
  //   {
  //     id: 163,
  //     categoryName: "sample",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-09T05:53:50.000Z",
  //   },
  //   {
  //     id: 162,
  //     categoryName: "node",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-09T05:52:18.000Z",
  //   },
  //   {
  //     id: 161,
  //     categoryName: "node",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-09T05:50:28.000Z",
  //   },
  //   {
  //     id: 160,
  //     categoryName: "node",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-09T05:08:03.000Z",
  //   },
  //   {
  //     id: 159,
  //     categoryName: "node",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-08T15:16:02.000Z",
  //   },
  //   {
  //     id: 158,
  //     categoryName: "sub cat 02_06",
  //     parentCatId: 157,
  //     tabId: 1,
  //     timemodified: "2020-06-02T12:50:41.000Z",
  //   },
  //   {
  //     id: 157,
  //     categoryName: "category 02_06",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-02T12:50:02.000Z",
  //   },
  //   {
  //     id: 154,
  //     categoryName: "category AB",
  //     parentCatId: 153,
  //     tabId: 1,
  //     timemodified: "2020-06-01T09:50:46.000Z",
  //   },
  //   {
  //     id: 153,
  //     categoryName: "category A",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-06-01T09:50:46.000Z",
  //   },
  //   {
  //     id: 156,
  //     categoryName: "category BA",
  //     parentCatId: 154,
  //     tabId: 1,
  //     timemodified: "2020-06-01T09:50:46.000Z",
  //   },
  //   {
  //     id: 155,
  //     categoryName: "category B",
  //     parentCatId: 153,
  //     tabId: 1,
  //     timemodified: "2020-06-01T09:50:46.000Z",
  //   },
  //   {
  //     id: 145,
  //     categoryName: "sub Asset cat 30",
  //     parentCatId: 144,
  //     tabId: 1,
  //     timemodified: "2020-05-30T11:50:56.000Z",
  //   },
  //   {
  //     id: 144,
  //     categoryName: "Asset cat 30",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-05-30T11:43:01.000Z",
  //   },
  //   {
  //     id: 143,
  //     categoryName: "#cat A3",
  //     parentCatId: 141,
  //     tabId: 1,
  //     timemodified: "2020-05-29T06:20:49.000Z",
  //   },
  //   {
  //     id: 142,
  //     categoryName: "#cat A 2",
  //     parentCatId: 141,
  //     tabId: 1,
  //     timemodified: "2020-05-29T06:14:03.000Z",
  //   },
  //   {
  //     id: 141,
  //     categoryName: "#cat A 1",
  //     parentCatId: 137,
  //     tabId: 1,
  //     timemodified: "2020-05-29T06:13:48.000Z",
  //   },
  //   {
  //     id: 140,
  //     categoryName: "#category A B",
  //     parentCatId: 138,
  //     tabId: 1,
  //     timemodified: "2020-05-28T15:35:52.000Z",
  //   },
  //   {
  //     id: 139,
  //     categoryName: "#category A A",
  //     parentCatId: 138,
  //     tabId: 1,
  //     timemodified: "2020-05-28T15:35:41.000Z",
  //   },
  //   {
  //     id: 138,
  //     categoryName: "#category B",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-05-28T15:35:23.000Z",
  //   },
  //   {
  //     id: 137,
  //     categoryName: "#category A",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-05-28T15:34:59.000Z",
  //   },
  //   {
  //     id: 136,
  //     categoryName: "vikas test 3",
  //     parentCatId: 135,
  //     tabId: 1,
  //     timemodified: "2020-05-28T07:36:14.000Z",
  //   },
  //   {
  //     id: 135,
  //     categoryName: "vikas test 2",
  //     parentCatId: 134,
  //     tabId: 1,
  //     timemodified: "2020-05-28T07:34:35.000Z",
  //   },
  //   {
  //     id: 134,
  //     categoryName: "vikas test",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-05-28T07:34:11.000Z",
  //   },
  //   {
  //     id: 133,
  //     categoryName: "test test",
  //     parentCatId: 132,
  //     tabId: 1,
  //     timemodified: "2020-05-25T17:45:21.000Z",
  //   },
  //   {
  //     id: 132,
  //     categoryName: "final test",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-05-25T17:44:44.000Z",
  //   },
  //   {
  //     id: 131,
  //     categoryName: "$cat",
  //     parentCatId: 130,
  //     tabId: 1,
  //     timemodified: "2020-05-22T14:12:02.000Z",
  //   },
  //   {
  //     id: 130,
  //     categoryName: "$final",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-05-22T14:10:47.000Z",
  //   },
  //   {
  //     id: 129,
  //     categoryName: "$final testing",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-05-22T13:34:55.000Z",
  //   },
  //   {
  //     id: 128,
  //     categoryName: "#test category contributor",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-05-22T08:22:19.000Z",
  //   },
  //   {
  //     id: 127,
  //     categoryName: "#test category admin",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-05-22T08:21:02.000Z",
  //   },
  //   {
  //     id: 126,
  //     categoryName: "dddd",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-05-21T04:38:28.000Z",
  //   },
  //   {
  //     id: 125,
  //     categoryName: "test cat 205 4",
  //     parentCatId: 124,
  //     tabId: 1,
  //     timemodified: "2020-05-20T13:33:41.000Z",
  //   },
  //   {
  //     id: 124,
  //     categoryName: "test cat 205 3",
  //     parentCatId: 122,
  //     tabId: 1,
  //     timemodified: "2020-05-20T13:32:22.000Z",
  //   },
  //   {
  //     id: 123,
  //     categoryName: "test cat 205 2",
  //     parentCatId: 122,
  //     tabId: 1,
  //     timemodified: "2020-05-20T13:30:48.000Z",
  //   },
  //   {
  //     id: 122,
  //     categoryName: "test cat 205 1",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-05-20T13:30:23.000Z",
  //   },
  //   {
  //     id: 121,
  //     categoryName: "sssss",
  //     parentCatId: 120,
  //     tabId: 1,
  //     timemodified: "2020-05-16T09:05:17.000Z",
  //   },
  //   {
  //     id: 120,
  //     categoryName: "zzzzzzzzzzz",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-05-16T09:05:00.000Z",
  //   },
  //   {
  //     id: 118,
  //     categoryName: "category 1",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-05-16T09:04:41.000Z",
  //   },
  //   {
  //     id: 119,
  //     categoryName: "category2",
  //     parentCatId: 118,
  //     tabId: 1,
  //     timemodified: "2020-05-16T05:07:31.000Z",
  //   },
  //   {
  //     id: 117,
  //     categoryName: "dddddggcgc",
  //     parentCatId: 110,
  //     tabId: 1,
  //     timemodified: "2020-05-15T15:52:26.000Z",
  //   },
  //   {
  //     id: 116,
  //     categoryName: "de",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-05-15T15:51:50.000Z",
  //   },
  //   {
  //     id: 115,
  //     categoryName: "ssss",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-05-15T15:42:36.000Z",
  //   },
  //   {
  //     id: 114,
  //     categoryName: "ssssssssssssss",
  //     parentCatId: 112,
  //     tabId: 1,
  //     timemodified: "2020-05-15T14:29:29.000Z",
  //   },
  //   {
  //     id: 112,
  //     categoryName: "finajjjfffff",
  //     parentCatId: 110,
  //     tabId: 1,
  //     timemodified: "2020-05-15T13:55:30.000Z",
  //   },
  //   {
  //     id: 113,
  //     categoryName: "gggsss",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-05-15T13:55:11.000Z",
  //   },
  //   {
  //     id: 111,
  //     categoryName: "test",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-05-15T11:54:31.000Z",
  //   },
  //   {
  //     id: 110,
  //     categoryName: "finalone",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-04-22T08:48:53.000Z",
  //   },
  //   {
  //     id: 109,
  //     categoryName: "testing",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-04-22T08:48:35.000Z",
  //   },
  //   {
  //     id: 108,
  //     categoryName: "uuumm",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-04-18T11:01:53.000Z",
  //   },
  //   {
  //     id: 107,
  //     categoryName: "tttttttt",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-04-18T10:57:21.000Z",
  //   },
  //   {
  //     id: 106,
  //     categoryName: "rrrr",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-04-18T10:51:35.000Z",
  //   },
  //   {
  //     id: 105,
  //     categoryName: "qqqqqqqq",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-04-18T10:47:49.000Z",
  //   },
  //   {
  //     id: 104,
  //     categoryName: "dddd",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-04-18T10:34:47.000Z",
  //   },
  //   {
  //     id: 103,
  //     categoryName: "ggggg",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-04-18T10:22:06.000Z",
  //   },
  //   {
  //     id: 102,
  //     categoryName: "testing",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-04-18T10:17:40.000Z",
  //   },
  //   {
  //     id: 101,
  //     categoryName: "test",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-04-18T10:12:46.000Z",
  //   },
  //   {
  //     id: 100,
  //     categoryName: "ssssssssrrrrr",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-04-18T10:09:06.000Z",
  //   },
  //   {
  //     id: 99,
  //     categoryName: "tttyuyyu",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-04-18T06:53:13.000Z",
  //   },
  //   {
  //     id: 98,
  //     categoryName: "xxxxxxxFFFFFFFFFFFFFF",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-04-18T06:43:37.000Z",
  //   },
  //   {
  //     id: 97,
  //     categoryName: "gggggggg",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-04-18T06:41:43.000Z",
  //   },
  //   {
  //     id: 96,
  //     categoryName: "ssssssss",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-04-18T06:29:24.000Z",
  //   },
  //   {
  //     id: 95,
  //     categoryName: "ffffff",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-04-18T06:27:56.000Z",
  //   },
  //   {
  //     id: 94,
  //     categoryName: "ppp",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-04-18T06:22:51.000Z",
  //   },
  //   {
  //     id: 93,
  //     categoryName: "xxxxxxxxxx",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-04-18T06:21:21.000Z",
  //   },
  //   {
  //     id: 92,
  //     categoryName: "testinnnnj",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-04-18T06:18:28.000Z",
  //   },
  //   {
  //     id: 91,
  //     categoryName: "finalee",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-04-18T06:12:18.000Z",
  //   },
  //   {
  //     id: 90,
  //     categoryName: "tttttttteee",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-04-18T05:59:53.000Z",
  //   },
  //   {
  //     id: 89,
  //     categoryName: "final123",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-04-18T05:58:33.000Z",
  //   },
  //   {
  //     id: 88,
  //     categoryName: "WPS",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-04-17T07:26:37.000Z",
  //   },
  //   {
  //     id: 86,
  //     categoryName: "Asset 0904",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-04-09T11:51:20.000Z",
  //   },
  //   {
  //     id: 79,
  //     categoryName: "Asset cat 24_02",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-04-03T07:34:27.000Z",
  //   },
  //   {
  //     id: 85,
  //     categoryName: "Asset 0304",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-04-03T07:34:14.000Z",
  //   },
  //   {
  //     id: 84,
  //     categoryName: "Category 30",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-03-30T06:44:54.000Z",
  //   },
  //   {
  //     id: 81,
  //     categoryName: "Example1",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-03-30T06:40:55.000Z",
  //   },
  //   {
  //     id: 83,
  //     categoryName: "category 23",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-03-23T06:39:16.000Z",
  //   },
  //   {
  //     id: 82,
  //     categoryName: "Asset cat 0503",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-03-05T06:25:47.000Z",
  //   },
  //   {
  //     id: 80,
  //     categoryName: "Asset Cat 25 feb",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-02-24T10:43:10.000Z",
  //   },
  //   {
  //     id: 76,
  //     categoryName: "direct",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-02-20T05:48:22.000Z",
  //   },
  //   {
  //     id: 77,
  //     categoryName: "Collections",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-02-18T09:31:42.000Z",
  //   },
  //   {
  //     id: 74,
  //     categoryName: "cat 28_01_20",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-01-29T06:53:32.000Z",
  //   },
  //   {
  //     id: 73,
  //     categoryName: "Asset 16_01",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-01-16T05:55:32.000Z",
  //   },
  //   {
  //     id: 72,
  //     categoryName: "Test DAM 14012020",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-01-14T07:48:45.000Z",
  //   },
  //   {
  //     id: 71,
  //     categoryName: "Asset 09_01",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2020-01-09T10:39:36.000Z",
  //   },
  //   {
  //     id: 70,
  //     categoryName: "DAM Cat 1612",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2019-12-16T05:44:36.000Z",
  //   },
  //   {
  //     id: 69,
  //     categoryName: "Remi Category",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2019-11-25T10:11:21.000Z",
  //   },
  //   {
  //     id: 68,
  //     categoryName: "study",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2019-11-20T11:25:56.000Z",
  //   },
  //   {
  //     id: 67,
  //     categoryName: "DAM cat 2011",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2019-11-20T05:54:23.000Z",
  //   },
  //   {
  //     id: 63,
  //     categoryName: "DAM Assest",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2019-10-11T09:48:23.000Z",
  //   },
  //   {
  //     id: 44,
  //     categoryName: "DAM Cat 10-9",
  //     parentCatId: null,
  //     tabId: 1,
  //     timemodified: "2019-09-10T07:02:23.000Z",
  //   },
  // ];
  categoriesList = [];
  // courseModuleList = [
  //   {
  //     id: 1341,
  //     categoryName: "@folder2632",
  //     parentCatId: null,
  //     tabId: 1,
  //     children: [
  //       {
  //         id: 1081,
  //         categoryName: "version1",
  //         parentCatId: 1079,
  //         tabId: 1,
  //         timemodified: "2020-08-26T07:48:09.000Z",
  //       },
  //       {
  //         id: 1091,
  //         categoryName: "version",
  //         parentCatId: 1079,
  //         tabId: 1,
  //         timemodified: "2020-08-26T07:47:55.000Z",
  //       },
  //       {
  //         id: 1339,
  //         categoryName: "26-08-2020",
  //         parentCatId: null,
  //         tabId: 1,
  //         timemodified: "2020-08-26T07:44:16.000Z",
  //       },
  //       {
  //         id: 1321,
  //         categoryName: "Test 2400",
  //         parentCatId: 77,
  //         tabId: 1,
  //         timemodified: "2020-08-24T08:45:18.000Z",
  //       },
  //       {
  //         id: 1319,
  //         categoryName: "FOLDER B",
  //         parentCatId: 70,
  //         tabId: 1,
  //         timemodified: "2020-08-24T08:27:09.000Z",
  //       },
  //       {
  //         id: 1318,
  //         categoryName: "A",
  //         parentCatId: 44,
  //         tabId: 1,
  //         timemodified: "2020-08-21T08:19:43.000Z",
  //       },
  //     ],
  //     timemodified: "2020-08-26T12:22:14.000Z",
  //   },
  //   {
  //     id: 1082,
  //     categoryName: "video",
  //     parentCatId: 1081,
  //     tabId: 1,
  //     children: [
  //       {
  //         id: 1080,
  //         categoryName: "image",
  //         parentCatId: 1079,
  //         tabId: 1,
  //         timemodified: "2020-08-20T15:06:02.000Z",
  //       },
  //       {
  //         id: 1079,
  //         categoryName: "categoryA fu",
  //         parentCatId: null,
  //         tabId: 1,
  //         timemodified: "2020-08-20T15:06:02.000Z",
  //       },
  //       {
  //         id: 1094,
  //         categoryName: "video",
  //         parentCatId: 1092,
  //         tabId: 1,
  //         timemodified: "2020-08-20T15:06:02.000Z",
  //       },
  //       {
  //         id: 1093,
  //         categoryName: "image",
  //         parentCatId: 1091,
  //         tabId: 1,
  //         timemodified: "2020-08-20T15:06:02.000Z",
  //       },
  //     ],
  //     timemodified: "2020-08-20T15:06:02.000Z",
  //   },
  // ];
  courseModuleList = [];
  selectedSession = "";
  sessionList = [];
  programList = [];
  dropdownSettings = {
    badgeShowLimit: 1,
    lazyLoading: true,
    showCheckbox: true,
    escapeToClose: false,
    enableSearchFilter: true,
    singleSelection: true,
    // text: "Select Countries",
    // selectAllText: "Select All",
    // noDataLabel: "No data found",
    unSelectAllText: "UnSelect All",
    classes: "common-multi",
    primaryKey: 'id',
    labelKey: 'programName',
    noDataLabel: 'Search Users...',
    // enableSearchFilter: true,
    searchBy: ['programName'],
  };
  selectedProgram = [{
    id: null,
    programName: 'Select Program'
  }];

  preparedCategoriesList = [];
  preparedCourseModuleList = [];
  selectAllCourses = false;
  // preparedCourseModuleListCopy = [];
  preparedCategoriesListCopy = [];
  selectedFolderArray = [];

  selectedItem = null;

  labelsCourse: any = [
    { labelname: "Status", bindingProperty: "status", componentType: "fulltext" },
    {
      labelname: "Batch Name",
      bindingProperty: "coursename",
      componentType: "fulltext",
    },
    { labelname: "Session", bindingProperty: "moduleName", componentType: "fulltext" },
    {
      labelname: "Activity Name",
      bindingProperty: "activityname",
      componentType: "text",
    },
    // { labelname: 'COUNTRY', bindingProperty: 'country', componentType: 'text' },
  ];
  direction:any="bottom"
  labels: any = [];
  tableData = [];
  labelsNotification: any = [
    {
      labelname: "Status",
      bindingProperty: "status",
      componentType: "fulltext",
    },
    {
      labelname: "Batch Name",
      bindingProperty: "coursename",
      componentType: "fulltext",
    },
    // { labelname: 'Session', bindingProperty: 'session', componentType: 'text' },
    // { labelname: 'Activity Name', bindingProperty: 'activityName', componentType: 'text' },
    // { labelname: 'COUNTRY', bindingProperty: 'country', componentType: 'text' },
  ];

  tableList: any = [
    // {
    //   status: "Activity Added Succesfullly.",
    //   batchName: "Corina",
    //   session: "Carolin",
    //   activityName: "Know Your System",
    //   country: "Guam",
    // },
    // {
    //   status: "Activity Added Succesfullly.",
    //   batchName: "Elena",
    //   session: "Keelia",
    //   activityName: "Know Your System",
    //   country: "Saint Lucia",
    // },
    // {
    //   status: "Activity Added Succesfullly.",
    //   batchName: "Annabela",
    //   session: "Shelba",
    //   activityName: "Know Your System",
    //   country: "Switzerland",
    // },
    // {
    //   status: "Activity Added Succesfullly.",
    //   batchName: "Fernande",
    //   session: "Garbe",
    //   activityName: "Know Your System",
    //   country: "Seychelles",
    // },
    // { "number": 7, "firstname": "Corina", "lastname": "Carolin", "email": "Corina.Carolin@gmail.com", "country": "Guam" },
    // { "number": 8, "firstname": "Jan", "lastname": "Gower", "email": "Jan.Gower@gmail.com", "country": "Malta" },
    // { "number": 9, "firstname": "Ezmeralda", "lastname": "Worda", "email": "Ezmeralda.@gmail.com", "country": "Brazil" },
    // { "number": 10, "firstname": "Elena", "lastname": "Keelia", "email": "Elena.Keelia@gmail.com", "country": "Saint Lucia" },
  ];

  tableListNotiication: any = [
    // { status: "Notification Added Succesfullly.", batchName: "Corina" },
    // { status: "Notification Updated Succesfullly.", batchName: "Elena" },
    // { status: "Notification Added Succesfullly.", batchName: "Elena" },
    // { "number": 7, "firstname": "Corina", "lastname": "Carolin", "email": "Corina.Carolin@gmail.com", "country": "Guam" },
    // { "number": 8, "firstname": "Jan", "lastname": "Gower", "email": "Jan.Gower@gmail.com", "country": "Malta" },
    // { "number": 9, "firstname": "Ezmeralda", "lastname": "Worda", "email": "Ezmeralda.@gmail.com", "country": "Brazil" },
    // { "number": 10, "firstname": "Elena", "lastname": "Keelia", "email": "Elena.Keelia@gmail.com", "country": "Saint Lucia" },
  ];
  noResultFound: boolean = false;
  noResultFoundConfig: noData = {
    margin: "mt-0, w-100",
    imageSrc: "assets/images/no-data-bg.svg",
    title: "Result list not found.",
    desc: "",
    titleShow: true,
    // btnShow:true,
    descShow: false,
    // btnText:'Learn More',
    // btnLink:'https://faq.edgelearning.co.in/kb/how-to-add-an-asset-in-the-dam',
  };
  resultLoading = false;

  courseListNotFound: boolean = false;
  noCourseListConfig: noData = {
    margin: "mt-0, w-100",
    imageSrc: "assets/images/no-data-bg.svg",
    title: "No Course Found to display.",
    desc: "",
    titleShow: true,
    // btnShow:true,
    descShow: false,
    // btnText:'Learn More',
    // btnLink:'https://faq.edgelearning.co.in/kb/how-to-add-an-asset-in-the-dam',
  };
  courseListLoading = false;

  categoryFoundNotFound: boolean = false;
  nocategoryFound: noData = {
    margin: "mt-0, w-100",
    imageSrc: "assets/images/no-data-bg.svg",
    title: "No Category Found to display.",
    desc: "",
    titleShow: true,
    // btnShow:true,
    descShow: false,
    // btnText:'Learn More',
    // btnLink:'https://faq.edgelearning.co.in/kb/how-to-add-an-asset-in-the-dam',
  };
  categoryListLoading = false;

  // News

  loginUserdata: any;
  @Output() performAction = new EventEmitter<any>();

  constructor(
    private assetservice: AssetService,
    private commonFunctionService: CommonFunctionsService,
    private dataSeparatorService: DataSeparatorService,
    private cdf: ChangeDetectorRef,
    private http1: HttpClient,
    private toast: ToastrService,
    protected exportService: JsonToXlsxCommonService,
  ) {
    if (localStorage.getItem("LoginResData")) {
      this.loginUserdata = JSON.parse(localStorage.getItem("LoginResData"));
    }
  }

  ngOnInit() {
    this.makeInitalComponentDataReady();
  }

  makeInitalComponentDataReady() {
    switch (this.componentConfig) {
      case "course":
        this.makeDataReadyForCourse();
        break;
      case "news":
        this.selectedSession = 'none';
        this.makeDataReadyForNews();
        break;
    }
  }
  makeDataReadyForCourse() {
    this.makeDisplayTabArray();
    this.fetchCategoryList();
    // this.preparedCourseModuleList = this.processTreeData(this.courseModuleList);
    // this.processTreeData2(this.courseModuleList);

  }

  makeDataReadyForNews() {
    this.getHelpContent();
    this.makeDisplayTabArray();
    this.fetchCategoryList();
  }
  passDataToParent(action, ...args) {
    console.log("action to be performed ==>", action, args);
    const event = {
      action: action,
      argument: args,
    };
    console.log("action to be performed ==>", event);
    this.performAction.emit(event);
  }

  displayTabArray = [];
  makeDisplayTabArray() {
    const objectArray = Object.keys(this.config);
    for (let index = 0; index < objectArray.length; index++) {
      // const element = objectArray[index];
      if (this.config[objectArray[index]].show) {
        const element = {};
        element["tabTitle"] = this.config[objectArray[index]].tabTitle;
        element["configName"] = objectArray[index];
        element["identifier"] = this.config[objectArray[index]].identifier;
        element["disabled"] = this.config[objectArray[index]].disabled;
        element["showSave"] = this.config[objectArray[index]].showSave;
        // element['configIndentifer'] = objectArray[index];
        this.displayTabArray.push(element);
      }
    }
    this.currentActiveTab = this.displayTabArray[0];
    console.log("Display Tabs ==>", this.displayTabArray);
  }

  string = 1;
  createDataForTabChange(event, tabs) {
    const tabCurrent = this.displayTabArray[event];
    const object = {
      action: tabCurrent.identifier,
      argumnet: tabCurrent,
    };
    this.currentActiveTab = tabCurrent;
    console.log("tab Changed ==>", object);
    console.log("tab ==>", this.currentActiveTab);
  }

  processTreeData(tree) {
    let demo = [];
    if (tree) {
      for (let i = 0; i < tree.length; i++) {
        if (tree[i].parentCatId == null) {
          demo.push(tree[i]);
        }
      }
    }

    // this.preparedCategoriesList = demo
    return demo;
  }

  // processTreeData2(tree){
  //   let demo = [];
  //   if(tree){
  //     for(let i = 0;i<tree.length;i++){

  //       if(tree[i].parentCatId == null){
  //         demo.push(tree[i])
  //       }
  //     }
  //   }

  //   this.preparedCategoriesList2 = demo
  // }
  getFolders(event, data, item) {
    console.log("event", event);
    console.log("data", data);
    console.log("item", item);
    this.selectedItem = event;
    switch (this.componentConfig) {
      case "course":
        // this.displayTabArray[1]["disabled"] = false;
        this.enableDisableTabByIndentifer('courseList', false);
        setTimeout(() => {
          // this.changeTabToIndex(1);
          this.changeTabIndexByTabIndentifer('courseList');
        });
        this.getCourseModuleList(true);
        break;
      case "news":
        // this.displayTabArray[2]["disabled"] = false;
        this.enableDisableTabByIndentifer('courseList', false);
        setTimeout(() => {
          this.changeTabIndexByTabIndentifer('courseList');
        });
        // this.changeTabToIndex(2);
        this.getCourseList();
        break;
    }

    // this.valueChanged = event.id;
  }

  getUpdatedCourseList(event) {
    this.preparedCourseModuleList = event;
    if(this.preparedCourseModuleList && this.preparedCourseModuleList.length !=0){
      for (let index = 0; index < this.preparedCourseModuleList.length; index++) {
        const element = this.preparedCourseModuleList[index];
        if(element['selected']){
          this.selectAllCourses = true;
        }else {
          this.selectAllCourses = false;
          break;
        }
      }
    }
  }
  openDeepyNestedFolder(event) {
    console.log("event", event);
    if (event && event.length != 0) {
      this.selectedFolderArray = event;
    }
  }

  selectedCheckedFolder(selectedCheckedFolder) {
    console.log("selectedCheckedFolder", selectedCheckedFolder);
  }
  bindData(id) {
    for (let i = 0; i < this.preparedCategoriesList.length; i++) {
      if (this.preparedCategoriesList[i].id == id) {
        this.preparedCategoriesList[i]["expand"] = true;
      }
    }
  }

  // ṣearch
  search() {
    this.showSearch = true;
    // this.searchClick.emit();
  }

  searchText = "";
  clearSearch() {
    this.searchText = "";
    if (
      this.preparedCourseModuleList &&
      Array.isArray(this.preparedCourseModuleList) &&
      this.preparedCourseModuleList.length != 0
    ) {
      let selectedArray = this.preparedCourseModuleList.filter((item) => {
        return item["selected"] || item["childSelected"];
      });
      console.log("selectedArray", selectedArray);
      this.preparedCourseModuleList = _.cloneDeepWith(this.courseModuleList);
      if (selectedArray && selectedArray.length != 0) {
        for (let i = 0; i < this.preparedCourseModuleList.length; i++) {
          for (let j = 0; j < selectedArray.length; j++) {
            if (
              this.preparedCourseModuleList[i]["courseId"] == selectedArray[j]["courseId"]
            ) {
              this.preparedCourseModuleList[i] = selectedArray[j];
              break;
            }
          }
        }
      }
      console.log(
        "this.preparedCourseModuleList",
        this.preparedCourseModuleList,
      );
    }else {
      this.preparedCourseModuleList = _.cloneDeepWith(this.courseModuleList);
    }
    if(this.preparedCourseModuleList.length !=0){
      this.courseListNotFound = false;
    }else {
      this.courseListNotFound = true;
    }
  }

  showNoCourseFind = false;
  // searchCourses(event) {
  //   const searchString = event.target.value;
  //   if (
  //     this.preparedCourseModuleList &&
  //     Array.isArray(this.preparedCourseModuleList) &&
  //     this.preparedCourseModuleList.length != 0
  //   ) {
  //     const newArray = this.preparedCourseModuleList.filter(function (item) {
  //       return (
  //         item["selected"] ||
  //         item["childSelected"] ||
  //         (item["coursenNme"] &&
  //           String(item["coursenNme"]).includes(searchString))
  //       );
  //     });
  //     console.log("newArray", newArray);
  //     this.preparedCourseModuleList = newArray;
  //   } else {
  //     this.showNoCourseFind = true;
  //   }
  // }
  searchCourses(event) {
    const searchString = event.target.value;
    if(event.target.value.length>=3 || event.target.value.length == 0){
    if (
      this.courseModuleList &&
      Array.isArray(this.courseModuleList) &&
      this.courseModuleList.length != 0
    ) {
      const moduleArray = _.cloneDeepWith(this.courseModuleList);
      const selectedArray = this.preparedCourseModuleList.filter(function (item) {
        return (
          item["selected"]
          //  || item["childSelected"]
        );
      });
      const arrayMatched = moduleArray.filter((item) => {
        return (item["courseName"] &&
          String(item["courseName"]).toLowerCase().includes(String(searchString).toLowerCase()))
      });
      this.preparedCourseModuleList = arrayMatched;
      if (selectedArray && selectedArray.length != 0) {
        for (let i = 0; i < this.preparedCourseModuleList.length; i++) {
          for (let j = 0; j < selectedArray.length; j++) {
            if (
              this.preparedCourseModuleList[i]["courseId"] == selectedArray[j]["courseId"]
            ) {
              this.preparedCourseModuleList[i] = _.cloneDeep(selectedArray[j]);
              selectedArray[j]['present'] = true;
              break;
            }
          }
        }
        for (let j = 0; j < selectedArray.length; j++) {
          if (
            !selectedArray[j]['present']
          ) {
            delete selectedArray[j]['present']
            this.preparedCourseModuleList.push(_.cloneDeep(selectedArray[j]))
          }
        }
      }
      if(this.preparedCourseModuleList.length !=0){
        this.courseListNotFound = false;
      }else {
        this.courseListNotFound = true;
      }
      console.log("preparedCourseModuleList", this.preparedCourseModuleList);
      console.log("selectedArray", selectedArray);
      // console.log("newArray", newArray);
      // this.preparedCourseModuleList = newArray;
    } else {
      this.courseListNotFound = true;
    }
  }
  }

  selectedSessionValueChange(event) {
    console.log("event", event);
    this.preparedCourseModuleList = _.cloneDeep(this.courseModuleList);
    this.selectAllCourses = false;
  }

  selectedProgramValueChange(event) {
    console.log("event", event);
    console.log("selected program", this.selectedProgram);
    this.getCourseModuleList(false);
  }

  checkAllCourses(event) {
    console.log("event", event);
    this.invalidArray = [];
    if (
      this.preparedCourseModuleList &&
      this.preparedCourseModuleList.length != 0
    ) {
      for (
        let index = 0;
        index < this.preparedCourseModuleList.length;
        index++
      ) {
        const element = this.preparedCourseModuleList[index];
        element["selected"] = event;
        element["childSelected"] = event;
        this.checkAllChild(event, element);
      }
    }
  }
  invalidArray = [];
  checkAllChild(event, folder) {
    // if(event != folder["checkBoxChecked"]){
    //   const value = !folder["checkBoxChecked"];
    // }
    // folder["checkBoxChecked"] = event;
    const value = event;
    console.log(folder, "folder");
    if (folder["children"] && folder["children"].length != 0) {
      if (this.selectedSession != "" || this.selectedSession) {
        if (this.selectedSession != '' || this.selectedSession) {
          if (this.selectedSession <= folder.children.length) {
            if (value) {
              folder.children[Number(this.selectedSession) - 1]['selected'] = value;
            } else {
              for (let i = 0; i < folder.children.length; i++) {
                if (Number(this.selectedSession) == (i + 1)) {
                  folder.children[i]['selected'] = value;
                } else {
                  folder.children[i]['selected'] = false;
                }
              }
            }
            folder['childSelected'] = event;
          } else {
            folder['childSelected'] = false;
            folder['invalidStatus'] = true;
            if (value) {
              this.invalidArray.push(folder);
            } else {
              if (this.invalidArray && this.invalidArray.length !== 0) {
                for (let i = 0; i < this.invalidArray.length; i++) {
                  if (this.invalidArray[i]['id'] === folder['id']) {
                    this.invalidArray.splice(i, 1);
                  }
                }
              }
              folder['invalidStatus'] = false;
            }
          }
        } else {
          for (let i = 0; i < folder.children.length; i++) {
            // if(Number(this.selectedSession) == i){
            folder.children[i]['selected'] = value;
            // }
          }
        }
      } else {
        for (let i = 0; i < folder.children.length; i++) {
          // if(Number(this.selectedSession) == i){
          folder.children[i]["selected"] = value;
          // }
        }
      }

    } else {
      console.log("Folder dont have sessions");
    }
    this.openFolder(folder, value);
    // this.getCheckedFolder.emit(folder);
  }

  openFolder(folder, value) {
    folder["expand"] = value;
  }

  showSearchCategory = false;
  categoryNotFound = true;
  searchTextCategory = "";
  showSearchCategoryToggel() {
    this.showSearchCategory = true;
  }
  clearSearchCategory() {
    this.categoryFoundNotFound = false
    this.searchTextCategory = "";
    this.valueChanged = Math.random();
    this.preparedCategoriesList = _.cloneDeepWith(
      this.preparedCategoriesListCopy,
    );
    if(this.selectedItem){
      this.valueChanged = this.selectedItem["id"];
    }
  }

  searchInTree(event) {
    const searchString = event.target.value;
    if(event.target.value.length>=3 || event.target.value.length == 0){
    // if (
    //   this.preparedCategoriesList &&
    //   Array.isArray(this.preparedCategoriesList) &&
    //   this.preparedCategoriesList.length != 0
    // ) {
    //   const newArray = this.preparedCategoriesList.filter((item) => {
    //     // return (String(item['categoryName']).includes(searchString)) )
    //     if (String(item["categoryName"]).includes(searchString)) {
    //       return true;
    //     } else {
    //       if (
    //         this.selectedFolderArray &&
    //         item["id"] === this.selectedFolderArray["id"]
    //       ) {
    //         return true;
    //       } else {
    //         return false;
    //       }
    //     }
    //   });
    this.valueChanged = Math.random();
    if (
      this.categoriesList &&
      Array.isArray(this.categoriesList) &&
      this.categoriesList.length != 0
    ) {
      const newArray = this.categoriesList.filter((item) => {
        // return (String(item['categoryName']).includes(searchString)) )
        if (String(item["categoryName"]).toLowerCase().includes(String(searchString).toLowerCase()) && item["parentCatId"] == null) {
          return true;
        } else {
          // return false;
          if (
            this.selectedFolderArray &&  this.selectedFolderArray.length !=0
          ) {
            // for(let i = 0; i < this.selectedFolderArray.length ; i++ ){
            //     if(item["id"] === this.selectedFolderArray[i]["id"] ){
            //       return true;
            //     }else {
            //       return false;
            //     }
            // }
            if(item["id"] === this.selectedFolderArray[0]["id"] ){
                    return true;
                  }else {
                    return false;
                  }
          } else {
            return false;
          }
        }
      });
      console.log("newArray", newArray);
      this.preparedCategoriesList = newArray;
      if(this.selectedItem){
        this.valueChanged = this.selectedItem["id"];
      }

      if (this.preparedCategoriesList.length === 0) {
        this.categoryNotFound = true;
        this.categoryFoundNotFound = true
        this.categoryListLoading = false
      } else {
        this.categoryNotFound = false;
        this.categoryFoundNotFound = false
        // this.categoryListLoading = true
      }
    } else {
      this.showNoCourseFind = true;
    }
  }
  }

  exportToExcel() {

    this.exportService.exportAsExcelFile(this.prepareInvalidArrayData(), "ErrorBatchesList");
  }

  // News
  tagsArray = [];
  init = false;
  appNotification = {
    notificationSubjectMin: null,
    notificationSubjectMax: null,
    appNotificationDescritionMin: null,
  };
  smsNotification = {
    min: null,
    max: null,
  };
  emailNotification = {
    emailSubjectMin: null,
    emailSubjectMax: null,
    // emailBodyMin:
  };
  // setUnsetValidation(notificationName, setUnsetFlag) {
  //   if (notificationName === "app") {
  //     if (setUnsetFlag) {
  //       this.appNotification = {
  //         notificationSubjectMin: 2,
  //         notificationSubjectMax: 200,
  //         appNotificationDescritionMin: 20,
  //       };
  //       //   Object.keys(this.formval.controls).forEach(control => {
  //       //     if(control !== 'newsName'){
  //       //       this.formval.controls[control].markAsPristine();
  //       //     }
  //       // });
  //     } else {
  //       this.appNotification = {
  //         notificationSubjectMin: null,
  //         notificationSubjectMax: null,
  //         appNotificationDescritionMin: null,
  //       };
  //     }
  //   }
  //   if (notificationName === "sms") {
  //     if (setUnsetFlag) {
  //       this.smsNotification = {
  //         min: 20,
  //         max: 140,
  //       };
  //       //   Object.keys(this.formval.controls).forEach(control => {
  //       //     if(control !== 'newsName'){
  //       //       this.formval.controls[control].markAsPristine();
  //       //     }
  //       // });
  //     } else {
  //       this.smsNotification = {
  //         min: null,
  //         max: null,
  //       };
  //     }
  //   }
  //   if (notificationName === "email") {
  //     if (setUnsetFlag) {
  //       this.emailNotification = {
  //         emailSubjectMin: 2,
  //         emailSubjectMax: 1000,
  //         // emailBodyMin:
  //       };
  //       //   Object.keys(this.formval.controls).forEach(control => {
  //       //     if(control !== 'newsName'){
  //       //       this.formval.controls[control].markAsPristine();
  //       //     }
  //       // });
  //     } else {
  //       this.emailNotification = {
  //         emailSubjectMin: null,
  //         emailSubjectMax: null,
  //         // emailBodyMin:
  //       };
  //     }
  //   }
  // }

  // Help Code Start Here //

  setUnsetValidation(notificationName, setUnsetFlag) {
    console.log(this.basicDetails,"basic details")
    if (notificationName === "app") {
      if( this.basicDetails && this.basicDetails['appTemplateId']){
      if (setUnsetFlag.target.checked == true) {
        this.appNotification = {
          notificationSubjectMin: 2,
          notificationSubjectMax: 200,
          appNotificationDescritionMin: 20,
        };
        //   Object.keys(this.formval.controls).forEach(control => {
        //     if(control !== 'newsName'){
        //       this.formval.controls[control].markAsPristine();
        //     }
        // });
      } else {
        this.appNotification = {
          notificationSubjectMin: null,
          notificationSubjectMax: null,
          appNotificationDescritionMin: null,
        };
      }
    }
    else{
      setTimeout( () => {
        setUnsetFlag.target.checked  = false; //!setUnsetFlag.target.checked;
        this.basicDetails['AppNotificationCheckBox'] = false; //  !this.basicDetails['AppNotificationCheckBox'];
        this.cdf.detectChanges()
      }, 100);

      this.toast.warning('You need to fill the respective template');
      setUnsetFlag.preventDefault();
      setUnsetFlag.stopPropagation();
      return false;
    }
  }
    if (notificationName === "sms") {
      if( this.basicDetails && this.basicDetails['SMSTemplateId']){
      if (setUnsetFlag.target.checked == true) {
        this.smsNotification = {
          min: 20,
          max: 140,
        };
      } else {
        this.smsNotification = {
          min: null,
          max: null,
        };
      }
    }
    else{
      setTimeout( () => {
        setUnsetFlag.target.checked  = false; //!setUnsetFlag.target.checked;
        this.basicDetails['SMSChecked'] = false; //  !this.basicDetails['AppNotificationCheckBox'];
        this.cdf.detectChanges()
      }, 100);

      this.toast.warning('You need to fill the respective template');
      setUnsetFlag.preventDefault();
      setUnsetFlag.stopPropagation();
      return false;
      // this.basicDetails['SMSChecked'] = false
      // this.toast.warning('Sorry you cannot access this sms')
    }
  }
  
    if (notificationName === "email") {
      if( this.basicDetails && this.basicDetails['emailTemplateId']){
      if (setUnsetFlag.target.checked == true) {
        this.emailNotification = {
          emailSubjectMin: 2,
          emailSubjectMax: 1000,
          // emailBodyMin:
        };
      } else {
        this.emailNotification = {
          emailSubjectMin: null,
          emailSubjectMax: null,
          // emailBodyMin:
        };
      }
    }
    else{
      setTimeout( () => {
        setUnsetFlag.target.checked  = false; //!setUnsetFlag.target.checked;
        this.basicDetails['EmailCheckBox'] = false; //  !this.basicDetails['AppNotificationCheckBox'];
        this.cdf.detectChanges()
      }, 100);

      this.toast.warning('You need to fill the respective template');
      setUnsetFlag.preventDefault();
      setUnsetFlag.stopPropagation();
      return false;
    // this.basicDetails['EmailCheckBox'] = false
    //   this.toast.warning('Sorry you cannot access this email')   
    }
  }
}

  
  helpContent: any;
  getHelpContent() {
    return new Promise((resolve) => {
      this.http1.get("assets/help-content/addEditCourseContent.json").subscribe(
        (data) => {
          this.helpContent = data;
          console.log("Help Array", this.helpContent);
          this.cdf.detectChanges();
        },
        (err) => {
          resolve("err");
        }
      );
    });
    // return this.helpContent;
  }

  // Help Code Ends Here //
  getTagsData() {
    let menuId;
    if (localStorage["menuId"]) {
      menuId = localStorage.getItem("menuId");
    }
    const param = {
      tId: this.loginUserdata.data.data.tenantId,
      menuId: menuId,
      noteventId: 40,
      // 'noteventId': 43,
    };
    const _urlGetNotificationTags: string =
      webApi.domain + webApi.url.getNotificationTags;
    this.commonFunctionService
      .httpPostRequest(_urlGetNotificationTags, param)
      // this.newsService.getNotificationsTags(param)
      .then(
        (rescompData) => {
          if (rescompData && rescompData["data"]) {
            let dataResponse = [];
            dataResponse = rescompData["data"];
            if (dataResponse[0].length !== 0) {
              this.tagsArray = dataResponse[0];
              if (this.tagsArray.length !== 0) {
                setTimeout(() => {
                  this.init = true;
                }, 450);
              }
              this.cdf.detectChanges();
            }
          }
          // this.spinner.hide();
        },
        (error) => {
          // this.spinner.hide();
          // const toast: Toast = {
          //   type: 'error',
          //   // title: 'Server Error!',
          //   body: 'Something went wrong.please try again later.',
          //   showCloseButton: true,
          //   timeout: 4000,
          // };
          // this.toastr.pop(toast);
          // this.presentToast('error', '');
        }
      );
  }



  getCourseModuleList(clearAll) {
    this.courseListLoading = true;
    this.invalidArray = [];

    if (this.selectedItem) {
      const param = {
        catId: this.selectedItem.id,
        // catId: "263",
        ctypeId: this.courseTypeId,
        programId: this.selectedProgram ? this.selectedProgram[0].id : null,
        // 'noteventId': 43,
      };
      const _urlGetCourseModuleData: string =
        webApi.domain + webApi.url.getCourseModuleData;
      this.commonFunctionService
        .httpPostRequest(_urlGetCourseModuleData, param)
        // this.newsService.getNotificationsTags(param)
        .then(
          (rescompData) => {

            if (rescompData['type']) {
              // this.programList = rescompData['programList'];
              const sessionZero = {
                id: "",
                name: "Select Session",
              };
              if(clearAll){
                const programZero1 = {
                  id: null,
                  programName: "Select Program",
                };
                this.selectedProgram = [programZero1];
              }
              const programZero = {
                id: null,
                programName: "Select Program",
              };
              this.selectAllCourses = false;
              this.searchText = "";
              this.sessionList = [sessionZero, ...rescompData['sessionList']];
              this.programList = [programZero, ...rescompData['programList']];
              console.log('sessionList', this.sessionList);
              console.log('programList', this.programList);
              if (rescompData['output'] && rescompData['output'].length != 0) {
                this.courseModuleList = rescompData['output']
                this.preparedCourseModuleList = _.cloneDeepWith(this.courseModuleList);
                this.courseListNotFound = false;
              } else {
                this.courseListNotFound = true;
              }
              this.courseListLoading = false;
              this.cdf.markForCheck();
              console.log("rescompData", rescompData)
            } else {
              this.courseListLoading = false;
              this.courseListNotFound = true;
              this.toast.warning("Batch List not found", "Warning");
              this.cdf.markForCheck();
            }


          },
          (error) => {
            this.courseListLoading = false;
            this.courseListNotFound = true;
            this.cdf.markForCheck();
          },
        );
    } else {
    }
  }

  getCourseList() {
    this.courseListLoading = true;
    this.invalidArray = [];
    if (this.selectedItem) {
      const param = {
        catId: this.selectedItem.id,
        // catId: "263",
        ctypeId: this.courseTypeId,
        // programId: this.selectedProgram ? this.selectedProgram[0].id : null,
        // 'noteventId': 43,
      };
      const _urlGetBulkCourseList: string =
        webApi.domain + webApi.url.getBulkCourseList;
      this.commonFunctionService
        .httpPostRequest(_urlGetBulkCourseList, param)
        // this.newsService.getNotificationsTags(param)
        .then(
          (rescompData) => {

            if (rescompData['type']) {
              if (rescompData['output'] && rescompData['output'].length != 0) {
                this.courseModuleList = rescompData['output']
                this.preparedCourseModuleList = _.cloneDeepWith(this.courseModuleList);
                this.courseListNotFound = false;
              } else {
                this.courseListNotFound = true;
              }
              this.courseListLoading = false;
              this.selectAllCourses = false;
              this.searchText = "";
              this.cdf.markForCheck();
              console.log("rescompData", rescompData)
            } else {
              this.courseListLoading = false;
              this.courseListNotFound = true;
              this.toast.warning("Batch List not found", "Warning");
              this.cdf.markForCheck();
            }


          },
          (error) => {
            this.courseListLoading = false;
            this.courseListNotFound = true;
            this.cdf.markForCheck();
          },
        );
    } else {
    }
  }

  addActivityDataInBulk() {
    this.saveWorking = true;
    this.cdf.detectChanges();
    this.resultLoading = true;
    const param = {
      activityId: this.activityClicked.activityId,
      moduleId: this.activityClicked.moduleId,
      courseId: this.activityClicked.courseId,
    };
    param["allString"] = this.makeAllStringDataForBulkActivity();
    if (param["allString"] == "") {
      if (this.invalidArray.length == 0) {
        this.toast.warning("Please Select Item First", "Warning");
        this.saveWorking = false;
        this.cdf.detectChanges();
        return null
      } else {
        // this.displayTabArray[2]["disabled"] = false;
        this.enableDisableTabByIndentifer('resultList', false);
        setTimeout(() => {
          // this.changeTabToIndex(2);
          this.changeTabIndexByTabIndentifer('resultList');
          this.disableAterSubmit();
        });
        this.resultLoading = false;
        this.tableList = [];
        this.noResultFound = true;
        this.makeResultDataReady();
        this.saveWorking = false;

        this.cdf.detectChanges();
        return null;
      }
    }
    // this.displayTabArray[2]["disabled"] = false;
    this.enableDisableTabByIndentifer('resultList', false);
    setTimeout(() => {
      // this.changeTabToIndex(2);
      this.changeTabIndexByTabIndentifer('resultList');
    });
    const _urlBulkUploadAcitivityInBulk: string =
      webApi.domain + webApi.url.bulkUploadAcitivityInBulk;
    this.commonFunctionService
      .httpPostRequest(_urlBulkUploadAcitivityInBulk, param)
      .then(
        (rescompData) => {
          if (rescompData['type']) {
            if (rescompData['output'] && rescompData['output'].length != 0) {
              this.tableList = rescompData['output'];
              this.makeResultDataReady();
              this.disableAterSubmit();
              this.noResultFound = false;
            } else {
              this.noResultFound = true;
            }

          } else {
            this.tableList = [];
            this.noResultFound = true;
            this.toast.warning('Something went wrong', 'Warning');
          }
          this.resultLoading = false;
          this.saveWorking = false;
          this.cdf.detectChanges();
          // this.cdf.markForCheck();
          console.log("rescompData", rescompData);

        },
        (error) => {
          this.resultLoading = false;
          this.toast.warning('Something went wrong', 'Warning');
          this.saveWorking = false;
          this.cdf.detectChanges();
          // this.cdf.markForCheck();
        }
      );
  }

  fetchCategoryList() {
    this.categoryListLoading = true;
    const param = {};
    const _urlGetBatchCategoryList: string =
      webApi.domain + webApi.url.getBatchCategoryList;
    this.commonFunctionService
      .httpPostRequest(_urlGetBatchCategoryList, param)
      // this.newsService.getNotificationsTags(param)
      .then(
        (rescompData) => {
          if (rescompData['type'] && rescompData['output'] && rescompData['output'].length != 0) {
            this.categoriesList = rescompData['output'];
            this.assetservice.tree = this.categoriesList;
            this.preparedCategoriesList = this.processTreeData(this.categoriesList);
            this.preparedCategoriesListCopy = _.cloneDeepWith(
              this.preparedCategoriesList,
            );
            this.categoryFoundNotFound = false;
          } else {
            this.categoryFoundNotFound = true;
            this.toast.warning("Category List not found", "Warning");
          }
          this.categoryListLoading = false;
          this.cdf.markForCheck();
          console.log("rescompData", rescompData);

        },
        (error) => {
          this.categoryListLoading = false;
          this.categoryFoundNotFound = true;
          this.cdf.markForCheck();
        },
      );
  }

  makeAllStringDataForBulkActivity() {
    let string = "";
    for (let index = 0; index < this.preparedCourseModuleList.length; index++) {
      const element = this.preparedCourseModuleList[index];
      if (element["selected"] && !element["invalidStatus"]) {
        if (string != "") {
          string = string + this.dataSeparatorService.Pipe;
        }
        string = string + element['courseId'];
        if (element["children"]) {
          let stringChildren = "";
          for (
            let indexj = 0;
            indexj < element["children"].length;
            indexj++
          ) {
            if (element["children"][indexj]['selected']) {
              if (stringChildren != '') {
                stringChildren += this.dataSeparatorService.Dollar;
              }
              stringChildren += element["children"][indexj]["moduleId"];
            }
          }
          if (stringChildren != "") {
            string = string + this.dataSeparatorService.Hash + stringChildren;
          }
        }
      }
    }
    return string;
  }

  makeResultDataReady() {
    switch (this.componentConfig) {
      case "course":
        this.labels = this.labelsCourse;
        this.tableData = this.tableList;
        break;
      case "news":
        this.labels = this.labelsNotification;
        this.tableData = this.tableListNotiication;
        break;
    }
  }

  performSaveAction() {
    switch (this.componentConfig) {
      case "course":
        this.addActivityDataInBulk();
        break;
      case "news":
        this.saveActionForNews();
        break;
    }
  }

  addNewsFormLoading = false;
  saveActionForNews() {
    if (this.currentActiveTab['identifier'] == 'addNewsForm') {
      console.log('addEditNotification', this.addEditNotification);

      this.validate();

    }
    if (this.currentActiveTab['identifier'] == 'courseList') {
      // console.log('addEditNotification', this.addEditNotification);
      // this.validate();
      this.submitNewsInBulk();
    }
  }


  validate() {
    if (this.addEditNotification.valid) {
      if (this.basicDetails.SMSChecked || this.basicDetails.EmailCheckBox || this.basicDetails.AppNotificationCheckBox) {
        this.addNewsFormLoading = true;
        this.cdf.detectChanges();
        this.submitNews(this.addEditNotification);
      } else {
        this.toast.warning('Please select at least one option.');
        Object.keys(this.addEditNotification.controls).forEach(key => {
          this.addEditNotification.controls[key].markAsDirty();
        });
      }
    } else {
      this.toast.warning('Please fill all the mandatory fields.');
      Object.keys(this.addEditNotification.controls).forEach(key => {
        this.addEditNotification.controls[key].markAsDirty();
      });
    }
  }

  saveWorking = false;
  submitNews(addEditNotification) {
    const notificationString = this.getdatareadyforNotifications(this.basicDetails);
    this.saveWorking = true;
    this.cdf.detectChanges();
    console.log("notificationString", notificationString);
    const param = {
      aId: this.basicDetails.aId,
      notEventid: this.basicDetails.eventId,
      finalString: notificationString,
    };
    const _urlInsertNotificationTemplate: string =
      webApi.domain + webApi.url.insertNotificationTemplate;
    this.commonFunctionService
      .httpPostRequest(_urlInsertNotificationTemplate, param)
      // this.newsService.getNotificationsTags(param)
      .then(
        (rescompData) => {
          if(rescompData['type']){
            // this.displayTabArray[1]["disabled"] = false;
            this.enableDisableTabByIndentifer('category', false);
            setTimeout(() => {
              // this.changeTabToIndex(1);
              this.changeTabIndexByTabIndentifer('category');
              // this.displayTabArray[0]["disabled"] = true;
              this.enableDisableTabByIndentifer('addNewsForm', true);
            });
          }else {
            this.toast.warning('Something went wrong', 'Warning');

          }
          this.addNewsFormLoading = false;
          this.saveWorking = false;
          this.cdf.detectChanges();
        },
        (error) => {
          this.addNewsFormLoading = false;
          this.saveWorking = false;
          this.cdf.detectChanges();
          this.toast.warning('Something went wrong', 'Warning');
          // this.cdf.markForCheck();
        },
      );
  }

  submitNewsInBulk(){
    this.saveWorking = true;
    this.cdf.detectChanges();
    this.resultLoading = true;
    const param = {
      notEventid: this.basicDetails.eventId,
    };
    param["allString"] = this.makeAllStringDataForBulkActivity();
    if (param["allString"] == "") {
      if (this.invalidArray.length == 0) {
        this.toast.warning("Please Select Batch First", "Warning");
        this.saveWorking = false;
        this.cdf.detectChanges();
        return null
      } else {
        this.saveWorking = false;
        this.cdf.detectChanges();
        // this.displayTabArray[3]["disabled"] = false;
        this.enableDisableTabByIndentifer('resultList', false);
        setTimeout(() => {
          // this.changeTabToIndex(3);
          this.changeTabIndexByTabIndentifer('resultList');
          this.disableAterSubmit();
        });
        this.resultLoading = false;
        this.tableListNotiication = [];
        this.noResultFound = true;

        this.makeResultDataReady();
        this.cdf.markForCheck();
        return null;
      }
    }
    // this.displayTabArray[3]["disabled"] = false;
    this.enableDisableTabByIndentifer('resultList', false);
    setTimeout(() => {
      // this.changeTabToIndex(3);
      this.changeTabIndexByTabIndentifer('resultList');
    });
    const _urlBulkUploadNotificationInBulk: string =
      webApi.domain + webApi.url.insertNotificationBulk;
    this.commonFunctionService
      .httpPostRequest(_urlBulkUploadNotificationInBulk, param)
      .then(
        (rescompData) => {
          if (rescompData['type']) {
            if (rescompData['output'] && rescompData['output'].length != 0) {
              // this.tableList = rescompData['output'];
              this.tableListNotiication = rescompData['output'];
              this.makeResultDataReady();
              this.disableAterSubmit();
              this.noResultFound = false;
            } else {
              this.noResultFound = true;
            }

          } else {
            this.tableList = [];
            this.noResultFound = true;
            this.toast.warning('Something went wrong', 'Warning');
          }
          this.resultLoading = false;
          this.cdf.markForCheck();
          console.log("rescompData", rescompData);
          this.saveWorking = false;
          this.cdf.detectChanges();
        },
        (error) => {
          this.resultLoading = false;
          this.saveWorking = false;
          this.cdf.detectChanges();
          this.toast.warning('Something went wrong', 'Warning');
          this.cdf.markForCheck();
        }
      );
  }

  getdatareadyforNotifications(notidetails) {
    let ruleName = "";
    let smsString = "";
    let emailString = "";
    let notifyString = "";
    if (notidetails.SMSChecked == true) {
      const modeidsms = 1;
      const templateidsms = notidetails.SMSTemplateId;
      //let rulenamesms = "course" + "_" + this.courseId + "_" + "SMS";
      const rulenamesms = notidetails.NotificationTypeName;
      const ruledescsms = notidetails.description;
      const noteventid = notidetails.eventId;
      const templatesms = notidetails.SMSText;
      const subjectsms = '';

      // let smsString = rulenamesms + "#" + ruledescsms + "#" + noteventid + "#" + modeidsms + "#" + templateidsms + "#" + templatesms + "#" + 1 + '#' + subjectsms;
       smsString = rulenamesms + this.dataSeparatorService.Hash + ruledescsms + this.dataSeparatorService.Hash + noteventid + this.dataSeparatorService.Hash + modeidsms + this.dataSeparatorService.Hash + templateidsms + this.dataSeparatorService.Hash + templatesms + this.dataSeparatorService.Hash + 1 + this.dataSeparatorService.Hash + subjectsms;

    } else {
      const modeidsms = null;
      const templateidsms = null;
    }

    if (notidetails.EmailCheckBox == true) {
      const modeidemail = 2;
      const templateidemail = notidetails.emailTemplateId;
      const rulenameemail = notidetails.NotificationTypeName;
      const ruledescemail = notidetails.description;
      const noteventid = notidetails.eventId;
      const templateemail = notidetails.EmailDescrition;
      const subjectEmail = notidetails.EmailSubject;

       emailString = rulenameemail + this.dataSeparatorService.Hash + ruledescemail + this.dataSeparatorService.Hash + noteventid + this.dataSeparatorService.Hash + modeidemail + this.dataSeparatorService.Hash + templateidemail + this.dataSeparatorService.Hash + templateemail + this.dataSeparatorService.Hash + 1 + this.dataSeparatorService.Hash + subjectEmail;
    } else {
      const modeidemail = null;
      const templateidemail = null;
    }

    if (notidetails.AppNotificationCheckBox == true) {
      const modeidnot = 3;
      const templateidnot = notidetails.appTemplateId;

      const rulenamenotify = notidetails.NotificationTypeName;
      const ruledescnotify = notidetails.description;
      const noteventid = notidetails.eventId;
      const templatenot = notidetails.AppNotificationDescrition;
      const subjectnot = notidetails.AppNotificationSubject;

      notifyString = rulenamenotify + this.dataSeparatorService.Hash + ruledescnotify + this.dataSeparatorService.Hash + noteventid + this.dataSeparatorService.Hash + modeidnot + this.dataSeparatorService.Hash + templateidnot + this.dataSeparatorService.Hash + templatenot + this.dataSeparatorService.Hash + 1 + this.dataSeparatorService.Hash + subjectnot;
    } else {
      const modeidnot = null;
      const templateidnot = null;
    }

    if (notidetails.SMSChecked == true && notidetails.EmailCheckBox == true && notidetails.AppNotificationCheckBox == true) {
      ruleName = smsString + this.dataSeparatorService.Pipe + emailString + this.dataSeparatorService.Pipe + notifyString;
    } else if (notidetails.EmailCheckBox == true && notidetails.AppNotificationCheckBox == true) {
      ruleName = emailString + this.dataSeparatorService.Pipe + notifyString;
    } else if (notidetails.SMSChecked == true && notidetails.AppNotificationCheckBox == true) {
      ruleName = smsString + this.dataSeparatorService.Pipe + notifyString;
    } else if (notidetails.SMSChecked == true && notidetails.EmailCheckBox == true) {
      ruleName = smsString + this.dataSeparatorService.Pipe + emailString;
    } else if (notidetails.AppNotificationCheckBox == true) {
      ruleName = notifyString;
    } else if (notidetails.SMSChecked == true) {
      ruleName = smsString;
    } else if (notidetails.EmailCheckBox == true) {
      ruleName = emailString;
    }
    //this.ruledescription = ruleName;
    console.log('thi.rulename', ruleName)
    return ruleName;
  }

  prepareInvalidArrayData() {
    const array = [];
    for (let index = 0; index < this.invalidArray.length; index++) {
      const element = array[index];
      let obj = {
        "Status": "Unable to add activity",
        "Reason": "Selected Rule Session is not present",
        "Batch Name": this.invalidArray[index]['courseName'],
        'Session': "",
        "Activity Name": this.activityClicked.activityName,
      };
      array.push(obj);
    }

    console.log('prepareInvalidArrayData', array);
    return array;
  }

  disableAterSubmit() {
    for (let i = 0; i < this.displayTabArray.length; i++) {
      this.displayTabArray[i]['disabled'] = true;
    }
  }

  enableDisableTabByIndentifer(identifier, value){
    // for (let i = 0; i < this.displayTabArray.length; i++) {
    //   if(this.displayTabArray[i]['identifier'] == identifier){
    //     this.displayTabArray[i]['disabled'] = value;
    //   }

    // }
    const getIndex = this.getTabIndexByIndentifer(identifier);
    if(getIndex !== -1){
      this.displayTabArray[getIndex]['disabled'] = value;
    }else{
      console.log("Index not found");
    }
  }

  getTabIndexByIndentifer(identifier){
    for (let i = 0; i < this.displayTabArray.length; i++) {
      if(this.displayTabArray[i]['identifier'] == identifier){
        // this.displayTabArray[i]['disabled'] = value;
        return i;
      }

    }
    return -1;
  }

  changeTabIndexByTabIndentifer(identifier){
    const getIndex = this.getTabIndexByIndentifer(identifier);
    if(getIndex !== -1){
      this.changeTabToIndex(getIndex);
    }else{
      console.log("Index not found");
    }
  }

  changeTabToIndex(index) {
    const classArr: any = document.querySelectorAll(".tabClassIndex");
    // classArr.forEach(element=>{
    //   this.render.listen(element, 'click', (target)=>{
    //     console.log('clicked', target);
    //   })
    // });
    if (classArr && classArr.length != 0) {
      const subArray = classArr[0].children;
      if (subArray.length > index) {
        subArray[index].click();
      }
    }
    console.log(classArr);
  }

}
