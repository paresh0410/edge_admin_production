import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidemenuDrowerComponent } from './sidemenu-drower.component';

describe('SidemenuDrowerComponent', () => {
  let component: SidemenuDrowerComponent;
  let fixture: ComponentFixture<SidemenuDrowerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidemenuDrowerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidemenuDrowerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
