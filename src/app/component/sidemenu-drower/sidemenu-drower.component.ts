import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-sidemenu-drower',
  templateUrl: './sidemenu-drower.component.html',
  styleUrls: ['./sidemenu-drower.component.scss']
})
export class SidemenuDrowerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  openNav() {
    document.getElementById('mySidenav').style.width = '170px';
  }
  closeNav() {
    document.getElementById('mySidenav').style.width = '0';
  }
}
