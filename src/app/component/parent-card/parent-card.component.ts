import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ngx-parent-card',
  templateUrl: './parent-card.component.html',
  styleUrls: ['./parent-card.component.scss']
})
export class ParentCardComponent implements OnInit {
  @Input() cardList: any = {};
  @Input() cardcount: boolean;
  @Output() cardClick = new EventEmitter();
  
  constructor() { }

  ngOnInit() {
  }

  boxCard() {
    this.cardClick.emit();
  }
}
