import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'ngx-modal-pop-up',
  templateUrl: './modal-pop-up.component.html',
  styleUrls: ['./modal-pop-up.component.scss']
})
export class ModalPopUpComponent implements OnInit {
  @Input() title: string = '';
  @Input() customWidthSetting = {
    enabled: false,
    width: '',
  };
  @Input() header: boolean = true;
  @Output() closeBtn = new EventEmitter<any>();


  constructor() { }

  ngOnInit() {
    // console.log('popUpTitle  ==>', this.title);
  }

  closePopup() {
    this.closeBtn.emit();
    // this.body.style.overflow = 'inherit';
  }
}
