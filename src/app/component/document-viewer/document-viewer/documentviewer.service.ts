import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

  
@Injectable()
export class DocumentViewerService {
document:any;
provider:any = "google";
    constructor() {

    }
setDocument(document, provider){
  this.document = document;
  this.provider = provider;
}

getDocument(){
  return {document:this.document, provider:this.provider};
}
    
}