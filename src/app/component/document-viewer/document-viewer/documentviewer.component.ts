import { Component, HostBinding, Input, OnDestroy, ChangeDetectorRef,ViewEncapsulation,ViewChild } from '@angular/core';
//import { PlayerService, Track } from '../../@core/utils/player.service';
import {BrowserModule, DomSanitizer} from '@angular/platform-browser'
import {DocumentViewerService} from './documentviewer.service';
import { UriBuilder } from 'uribuilder';
import { cleanPath } from 'cleanpath';
import 'rxjs/operator/toPromise';
import 'rxjs/operator/takeUntil';

@Component({
  selector: 'ngx-documentviewer',
  templateUrl: './documentviewer.component.html',
  styleUrls: ['./documentviewer.component.scss'],
  //encapsulation: ViewEncapsulation.None
})

export class DocumentViewerComponent implements OnDestroy {
  @Input('document') documentref :any; //HTMLVideoElement;
  @ViewChild('iframe') iframe :any; //HTMLVideoElement;

  document:any;
  provider:any;
  documenturl:any;
  ViewProviders:any = {
    /**
       * Google
       */
    google: 'google',
    /**
       * Microsoft
       */
    microsoft: 'microsoft',
};
  constructor(private documentService: DocumentViewerService, public cdf : ChangeDetectorRef,private sanitizer: DomSanitizer) {
    
  }
  ngOnInit(){
    let result:any = this.documentService.getDocument();
    this.document = result.document;
    this.provider = result.provider;
    this.documenturl = this.transform(this.getDocumentUrl());//this.getDocumentUrl();
    //this.iframe.nativeElement.src = this.getDocumentSanitizeUrl();
    //     this._provider = ViewProviders.google;
    //     this.providerChange = new EventEmitter();
    //     this._originSrc = ((this.iframe.nativeElement)).src;
  }
  // getDocumentSanitizeUrl(){
  //   //return this.sanitizer.bypassSecurityTrustUrl(this.getDocumentUrl());
  //   return this.sanitizer.bypassSecurityTrustResourceUrl(this.getDocumentUrl());
  // }
  encodeURI(url) {
    if (UriBuilder.parse(url).isRelative()) {
        url = cleanPath(document.baseURI + '/' + url);
    }
    return encodeURIComponent(url);
};
getDocumentUrl() {
  if (this.provider === this.ViewProviders.google) {
      return ('http://docs.google.com/gview?url=' +
          encodeURI(this.document) +
          '&embedded=true');
  }
  else if (this.provider === this.ViewProviders.microsoft) {
      return ('https://view.officeapps.live.com/op/embed.aspx?src=' +
          encodeURI(this.document));
  }
  return null;
};

  ngOnDestroy() {
    //this.documentService.setDocument(null, null);
  }

  transform(url): any {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

}
